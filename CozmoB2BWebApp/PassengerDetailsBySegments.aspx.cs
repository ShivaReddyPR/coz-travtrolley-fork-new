﻿using CT.BookingEngine;
using CT.Core;
using CT.Corporate;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class PassengerDetailsBySegments : CT.Core.ParentPage
{
    int resultId = 0;
    protected int paxCount = 0;
    string sessionId;
    MetaSearchEngine clsMSE = new MetaSearchEngine();
    protected SearchResult onwardResult;
    protected SearchResult returnResult;
    protected SearchRequest request;
    protected double Tax = 0;
    protected List<KeyValuePair<string, string>> MealPreferenceList = new List<KeyValuePair<string, string>>();
    protected AgentMaster agency;
    protected Dictionary<string, decimal> agentExchangeRates = new Dictionary<string, decimal>();
    protected string onSegment, retSegment;
    protected Fare[] onwardFares;
    protected Fare[] returnFares;
    protected string errorMessage = "";
    protected UAPIdll.Air46.AirPriceRsp onwardPriceRsp;
    protected UAPIdll.Air46.AirPriceRsp returnPriceRsp;
    protected string BookingValuesChanged = string.Empty;
    protected static string PAYTOCHANGE = "PAY TO CHANGE";
    protected static string FREETOCHANGE = "FREE TO CHANGE";
    protected static string NOCHANGE = "NO CHANGE";
    protected static string BASIC = "BASIC";
    private CorporateProfile corpProfile;
    protected SearchResult G9OnwardResult;
    protected SearchResult G9ReturnResult;
    protected decimal onwardTotal = 0, returnTotal = 0, onwardPubFare = 0, returnPubFare = 0, totalPubFare = 0;
    /// <summary>
    /// Airline Required Fields
    /// </summary>
    #region APISMandateFields
    protected bool TitleRequired = true;
    protected bool FirstNameRequired = true;
    protected bool LastNameRequired = true;
    protected bool GenderRequired;
    protected bool DOBRequired;
    protected bool PassportExpiryRequired;
    protected bool PassportNoRequired;
    protected bool MobileNoRequired = true;
    protected bool NationalityRequired;
    protected bool CountryRequired;
    protected bool EmailRequired = true;
    protected bool AddressRequired;
    #endregion

    //Indigo Repricing Varaiables
    protected double onwardOldTotalIndigo = 0;//Variable which holds the original fare particular to indigo flights
    protected double onwardNewTotalIndigo = 0;//Variable which holds the chnaged fare particular to indigo flights after price itineraray calculation
    protected double returnOldTotalIndigo = 0;//Variable which holds the original fare particular to indigo flights
    protected double returnNewTotalIndigo = 0;//Variable which holds the chnaged fare particular to indigo flights after price itineraray calculation
    protected bool indigoPriceChanged = false; //A Flag which determines is there any price change particular to indigo flights.

    //SG Repricing Variables
    protected double onwardOldTotalSG = 0;//Variable which holds the original fare particular to SG flights
    protected double onwardNewTotalSG = 0;//Variable which holds the chnaged fare particular to SG flights after price itineraray calculation
    protected double returnOldTotalSG = 0;//Variable which holds the original fare particular to SG flights
    protected double returnNewTotalSG = 0;//Variable which holds the chnaged fare particular to SG flights after price itineraray calculation
    protected bool sgPriceChanged = false; //A Flag which determines is there any price change particular to SG flights.

    protected decimal Discount = 0m;
    protected bool requiredGSTForSG_6E = false, requiredGSTForUAPI = false;
    protected bool mealRequiredSG_Corporate = false;
    protected LocationMaster location = new LocationMaster();
    protected List<PaxSeatInfo> liPaxSeatInfo = new List<PaxSeatInfo>();
    protected PaxSeatInfo clsPaxSeatInfo = new PaxSeatInfo();
    protected Dictionary<string, DataTable> dctFlexFieldDropdownData = new Dictionary<string, DataTable>();
    protected bool isOnwardCorporateFare = false;
    protected bool isReturnCorporateFare = false;
    protected string[] pageParams = null;

    #region SessionVariable and Constants
    protected Dictionary<string, object> SessionValues = new Dictionary<string, object>();
    protected const string ONWARD_RESULT = "OnwardResult";
    protected const string RETURN_RESULT = "ReturnResult";
    protected const string ONWARD_BAGGAGE = "OnwardBaggage";
    protected const string RETURN_BAGGAGE = "ReturnBaggage";
    protected const string ONWARD_REPRICEDRESULT = "OnwardRepricedResult";
    protected const string RETURN_REPRICEDRESULT = "ReturnRepricedResult";
    protected const string ONWARD_TBOFARE = "OnwardTBOFare";
    protected const string RETURN_TBOFARE = "ReturnTBOFare";
    protected const string ONWARD_OTHERCHARGES = "OnwardOtherCharges";
    protected const string RETURN_OTHERCHARGES = "ReturnOtherCharges";
    protected const string ONWARD_VALUES_CHANGED = "OnwardValuesChanged";
    protected const string RETURN_VALUES_CHANGED = "ReturnValuesChanged";
    protected const string ONWARD_PRICE_CHANGED_TBO = "OnwardIsPriceChangedTBO";
    protected const string RETURN_PRICE_CHANGED_TBO = "ReturnIsPriceChangedTBO";
    protected const string ONWARD_FLIGHT_ITINERARY = "OnwardFlightItinerary";
    protected const string RETURN_FLIGHT_ITINERARY = "ReturnFlightItinerary";
    protected const string ONWARD_SSR_PRICE_CHANGE = "OnwardSSRPriceChange";
    protected const string RETURN_SSR_PRICE_CHANGE = "ReturnSSRPriceChange";
    protected const string ONWARD_G9MEALS = "OnwardG9Meal";
    protected const string RETURN_G9MEALS = "ReturnG9Meal";
    protected bool isG9OnwardBundled = false;
    protected bool isG9ReturnBundled = false;
    #endregion

    protected double originalBaseFare = 0, originalTax = 0, repricedBaseFare = 0, repricedTax = 0;

    protected override void LoadViewState(object savedState)
    {
        if (savedState != null)
        {
            object[] myState = (object[])savedState;
            if (myState[0] != null)
                base.LoadViewState(myState[0]);

            if (myState[1] != null)
                dctFlexFieldDropdownData = (Dictionary<string, DataTable>)myState[1];

        }
    }
    protected override object SaveViewState()
    {
        object baseState = base.SaveViewState();
        return new object[] { baseState, dctFlexFieldDropdownData };
    }

    void DummyValue()
    {
        ddlTitle.SelectedIndex = 1;
        ddlGender.SelectedIndex = 1;
        ddlDay.SelectedIndex = 25;
        ddlMonth.SelectedIndex = 8;
        ddlYear.SelectedIndex = 15;
        ddlNationality.SelectedIndex = -1;
        ddlCountry.SelectedIndex = -1;
        ddlCountry.Items.FindByValue("IN").Selected = true;
        ddlNationality.Items.FindByText("Indian").Selected = true;

        ddlPEDay.SelectedIndex = 2;
        ddlPEMonth.SelectedIndex = 2;
        ddlPEYear.SelectedIndex = 4;


        txtPaxFName.Text = "Shiva";
        txtPaxLName.Text = "Reddy";
        txtPassportNo.Text = "L2212642";
        txtMobileCountryCode.Text = "91";
        txtMobileNo.Text = "9849674003";
        txtAddress1.Text = "Hyderabad";
        txtEmail.Text = "shiva@cozmotravel.com";
        //txtCity.Text = "SHJ";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                pageParams = GenericStatic.GetSetPageParams("PageParams", "", "get").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                hdfCurrentDate.Value = DateTime.Now.Day + "," + DateTime.Now.Month + "," + DateTime.Now.Year;
                Session["Agent"] = null;

                if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                    location = new LocationMaster(Settings.LoginInfo.LocationID);
                }
                else
                {
                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                    location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                }

                SessionValues = Session["PaxPageValues"] != null ? Session["PaxPageValues"] as Dictionary<string, object> : SessionValues;

                Session["Agent"] = agency;

                if (Session["FlightResults"] != null)
                {
                    if (pageParams != null)
                    {
                        sessionId = Session["sessionId"].ToString();
                        if (Session["FlightRequest"] != null)
                        {
                            request = Session["FlightRequest"] as SearchRequest;
                        }
                        resultId = Convert.ToInt32(pageParams[0]);
                        onwardResult = !SessionValues.ContainsKey(ONWARD_RESULT) ? Basket.FlightBookingSession[sessionId].Result[resultId - 1] : (SearchResult)SessionValues[ONWARD_RESULT];
                        //If the search is of one way there will be no return result
                        if (pageParams.Length > 1 && pageParams[1] != null)
                        {
                            resultId = Convert.ToInt32(pageParams[1]);
                            returnResult = !SessionValues.ContainsKey(RETURN_RESULT) ? Basket.FlightBookingSession[sessionId].Result[resultId - 1] : (SearchResult)SessionValues[RETURN_RESULT];
                        }
                        //MealPreferenceList = FlightPassenger.GetMealPreferenceList(result.ResultBookingSource);
                        SaveInSession(ONWARD_RESULT, onwardResult);
                        SaveInSession(RETURN_RESULT, returnResult);

                        //Added by Lokesh on 6-sep-2017 to get the additional services by service type
                        //If the booking source is flight inventory.
                        if (onwardResult.ResultBookingSource == BookingSource.FlightInventory || (returnResult != null && returnResult.ResultBookingSource == BookingSource.FlightInventory))
                        {
                            FINationalityValidation();
                        }
                        if (onwardResult != null && returnResult != null)
                        {
                            //Special Round Trip Fare Validations For S G and 6 E
                            if (onwardResult.IsSpecialRoundTrip && !returnResult.IsSpecialRoundTrip)
                            {
                                throw new Exception("Invalid Combination.");
                            }
                            else if (!onwardResult.IsSpecialRoundTrip && returnResult.IsSpecialRoundTrip)
                            {
                                throw new Exception("Invalid Combination.");
                            }
                            else if (onwardResult.IsSpecialRoundTrip && returnResult.IsSpecialRoundTrip)
                            {
                                //If both the results are special round trip fares
                                //Both the results should have the same signature for both SG and 6E.
                                if (onwardResult.RepriceErrorMessage != returnResult.RepriceErrorMessage)
                                {
                                    throw new Exception("Invalid Combination.");
                                }
                            }
                        }



                        //if CorporateProfileId is there loading CorporateDetails 19.01.2017
                        //if (Settings.LoginInfo.CorporateProfileId > 0)
                        if (request.CorporateTravelProfileId > 0)
                        {
                            corpProfile = new CorporateProfile(request.CorporateTravelProfileId, Settings.LoginInfo.AgentId);
                            //Loading FlexFields
                            //CreateFlexFields();
                            hdnFlexInfo.Value = corpProfile != null && corpProfile.DtProfileFlex != null && corpProfile.DtProfileFlex.Rows.Count > 0 ?
                                JsonConvert.SerializeObject(corpProfile.DtProfileFlex) : string.Empty;
                        }
                        else if (agency.RequiredFlexFields)
                        {
                            DataTable dt = CorporateProfile.GetAgentFlexDetailsByProduct((int)agency.ID, 1);
                            hdnFlexInfo.Value = dt != null && dt.Rows.Count > 0 ? JsonConvert.SerializeObject(dt) : string.Empty;
                        }

                        #region Not IsPostBack Code
                        if (!IsPostBack)
                        {
                            InitSession();
                            //SessionValues[ONWARD_BAGGAGE] = null;//added by lokesh on 9/02/2017.  
                            RemoveFromSession(ONWARD_BAGGAGE);
                            RemoveFromSession(RETURN_BAGGAGE);
                            RemoveFromSession(ONWARD_G9MEALS);
                            RemoveFromSession(RETURN_G9MEALS);
                            for (int i = 1; i <= 31; i++)
                            {
                                ddlDay.Items.Add(i.ToString());
                                ddlPEDay.Items.Add(i.ToString());
                            }

                            for (int j = 1920; j < (1940 + 90); j++)
                            {
                                ddlYear.Items.Add(j.ToString());
                            }

                            for (int i = DateTime.Now.Year; i <= (DateTime.Now.Year + 50); i++)
                            {
                                ddlPEYear.Items.Add(i.ToString());
                            }
                            ddlCountry.DataSource = Country.GetCountryList();
                            ddlCountry.DataTextField = "Key";
                            ddlCountry.DataValueField = "value";
                            ddlCountry.DataBind();

                            ddlNationality.DataSource = Country.GetNationalityList();
                            ddlNationality.DataTextField = "Key";
                            ddlNationality.DataValueField = "Value";
                            ddlNationality.DataBind();

                            if (CT.TicketReceipt.Common.Utility.ToString(ConfigurationManager.AppSettings["TestMode"]) == "True")
                            {
                                DummyValue();// to delete 
                            }
                            //CorporateDetails binding  19.01.2017
                            if (request.CorporateTravelProfileId > 0)
                            {
                                hdnProfileId.Value = request.CorporateTravelProfileId.ToString();
                                chkAddPax.Text = "Update Passenger";
                                LoadingCorporateDetails();
                            }


                            hdfOnwardFareAction.Value = "Get";
                            hdfReturnFareAction.Value = "Get";

                            DoReprice();

                            paxCount = (request.AdultCount) + request.ChildCount + request.InfantCount;
                            liPaxSeatInfo.Add(clsPaxSeatInfo = new PaxSeatInfo());
                            FlightPassenger[] additionalPax = new FlightPassenger[paxCount - 1];

                            for (int j = 1; j < request.AdultCount; j++)
                            {
                                FlightPassenger pax = new FlightPassenger();
                                pax.Type = PassengerType.Adult;
                                additionalPax[j - 1] = pax;
                                liPaxSeatInfo.Add(clsPaxSeatInfo = new PaxSeatInfo());
                            }

                            for (int k = 0; k < request.ChildCount; k++)
                            {
                                FlightPassenger pax = new FlightPassenger();
                                pax.Type = PassengerType.Child;
                                additionalPax[(request.AdultCount - 1) + k] = pax;
                                liPaxSeatInfo.Add(clsPaxSeatInfo = new PaxSeatInfo());
                            }

                            /* To initialize pax seat objects based on number of segments and passengers and 
                             * use the same object in Jquery while assigning the seats */

                            int iSegmentcount = onwardResult.Flights[0].Count();
                            if (returnResult != null)
                            {
                                iSegmentcount += returnResult.Flights[0].Count();
                            }
                            iSegmentcount = (iSegmentcount * liPaxSeatInfo.Count()) - liPaxSeatInfo.Count();
                            for (int z = 0; z < iSegmentcount; z++)
                            {
                                liPaxSeatInfo.Add(clsPaxSeatInfo = new PaxSeatInfo());
                            }
                            hdnPaxSeatInfo.Value = JsonConvert.SerializeObject(liPaxSeatInfo);

                            for (int l = 0; l < request.InfantCount; l++)
                            {
                                FlightPassenger pax = new FlightPassenger();
                                pax.Type = PassengerType.Infant;
                                additionalPax[((request.AdultCount + request.ChildCount) - 1) + l] = pax;
                            }
                            if (onwardResult != null && onwardResult.ResultBookingSource == BookingSource.TBOAir && onwardResult.IsLCC)
                            {
                                lblOnBagWrapper.Visible = true;// modified by suresh

                                LoadTBOBaggageInfo(ddlOnwardBaggage, ddlInwardBaggage, lblOnBag, lblInBag, SearchType.OneWay);
                            }
                            if (returnResult != null && returnResult.ResultBookingSource == BookingSource.TBOAir && returnResult.IsLCC)
                            {
                                lblOnBagWrapper.Visible = true;// modified by suresh
                                LoadTBOBaggageInfo(ddlOnwardBaggage, ddlInwardBaggage, lblOnBag, lblInBag, SearchType.Return);
                            }

                            if (onwardResult != null && onwardResult.ResultBookingSource == BookingSource.AirArabia)
                            {
                                lblOnBagWrapper.Visible = true;// modified by ziya
                                                               
                                clsMSE.SettingsLoginInfo = Settings.LoginInfo;
                                SaveInSession(ONWARD_REPRICEDRESULT, G9OnwardResult);
                                LoadBaggageInfo(SessionValues.ContainsKey(ONWARD_BAGGAGE) && SessionValues[ONWARD_BAGGAGE] != null ? (DataTable)SessionValues[ONWARD_BAGGAGE] :
                                    clsMSE.GetBaggageInfo(onwardResult, sessionId), ddlOnwardBaggage, ONWARD_BAGGAGE, lblOnBag);
                                LoadG9MealInfo(SessionValues.ContainsKey(ONWARD_G9MEALS) && SessionValues[ONWARD_G9MEALS] != null ? (DataTable)SessionValues[ONWARD_G9MEALS] :
                                    clsMSE.GetMealInfo(onwardResult, sessionId), ddlOnwardMeal, ONWARD_G9MEALS, lblOnMeal);
                            }
                            if (returnResult != null && returnResult.ResultBookingSource == BookingSource.AirArabia)
                            {
                                lblOnBagWrapper.Visible = true;// modified by ziya
                                
                                SaveInSession(RETURN_REPRICEDRESULT, G9ReturnResult);
                                LoadBaggageInfo(SessionValues.ContainsKey(RETURN_BAGGAGE) && SessionValues[RETURN_BAGGAGE] != null ? (DataTable)SessionValues[RETURN_BAGGAGE] :
                                    clsMSE.GetBaggageInfo(returnResult, sessionId), ddlInwardBaggage, RETURN_BAGGAGE, lblInBag);
                                LoadG9MealInfo(SessionValues.ContainsKey(RETURN_G9MEALS) && SessionValues[RETURN_G9MEALS] != null ? (DataTable)SessionValues[RETURN_G9MEALS] :
                                    clsMSE.GetMealInfo(returnResult, sessionId), ddlInwardMeal, RETURN_G9MEALS, lblInMeal);
                            }
                            if (onwardResult != null && onwardResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl)// Need to check with Lokesh
                            {
                                lblOnBagWrapper.Visible = true;// modified by ziya
                                int baggageIncluded = 0;
                                if (!String.IsNullOrEmpty(onwardResult.BaggageIncludedInFare))
                                {
                                    baggageIncluded = AssignAirIndiaExpressDefaultBaggage(onwardResult.BaggageIncludedInFare);
                                }
                                //Load Baggage Information for AirIndiaExpress flights
                                if (onwardResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl)
                                {
                                    LoadIXBaggageInfo(ddlOnwardBaggage, ddlInwardBaggage, lblOnBag, lblInBag, onwardResult.BaggageIncludedInFare);
                                }
                                else
                                {
                                    lblOnBag.Visible = true;
                                    lblInBag.Visible = true;
                                    lblOnBag.Text = "Onward: Max baggage included (20Kg)";
                                    if (request.Type == SearchType.Return)
                                    {
                                        lblInBag.Text = "Return: Max baggage included (20Kg)";
                                    }
                                    else
                                    {
                                        lblInBag.Text = "";
                                    }
                                }
                            }
                            if (returnResult != null && returnResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl)// Need to check with Lokesh
                            {
                                lblOnBagWrapper.Visible = true;// modified by ziya
                                int baggageIncluded = 0;
                                if (!String.IsNullOrEmpty(returnResult.BaggageIncludedInFare))
                                {
                                    baggageIncluded = AssignAirIndiaExpressDefaultBaggage(returnResult.BaggageIncludedInFare);
                                }
                                //Load Baggage Information for AirIndiaExpress flights
                                if (returnResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl)
                                {
                                    LoadIXBaggageInfo(ddlOnwardBaggage, ddlInwardBaggage, lblOnBag, lblInBag, returnResult.BaggageIncludedInFare);
                                }
                                else
                                {
                                    lblOnBag.Visible = true;
                                    lblInBag.Visible = true;
                                    lblOnBag.Text = "Onward: Max baggage included (20Kg)";
                                    if (request.Type == SearchType.Return)
                                    {
                                        lblInBag.Text = "Return: Max baggage included (20Kg)";
                                    }
                                    else
                                    {
                                        lblInBag.Text = "";
                                    }
                                }
                            }
                            if (onwardResult != null && onwardResult.ResultBookingSource == BookingSource.FlyDubai)
                            {
                                int baggageIncluded = 0;
                                lblOnBagWrapper.Visible = true;// modified by ziya
                                if (!String.IsNullOrEmpty(onwardResult.BaggageIncludedInFare))
                                {
                                    if (request.Type == SearchType.Return)
                                    {
                                        baggageIncluded = Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split(',')[0]) + Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split(',')[1]);
                                    }
                                    else
                                    {
                                        baggageIncluded = Convert.ToInt32(onwardResult.BaggageIncludedInFare);
                                    }
                                }
                                if (request.Type == SearchType.OneWay && baggageIncluded < 40 || request.Type == SearchType.Return && baggageIncluded < 80)
                                {
                                    //lblOnBagWrapper.Visible = true;

                                    LoadFZBaggageInfo(ddlOnwardBaggage, ddlInwardBaggage, lblOnBag, lblInBag);

                                }
                                else
                                {
                                    //lblOnBagWrapper.Visible = true;
                                    lblOnBag.Visible = true;
                                    lblInBag.Visible = true;
                                    lblOnBag.Text = "Onward: Max baggage included (40Kg)";
                                    if (request.Type == SearchType.Return)
                                    {
                                        lblInBag.Text = "Return: Max baggage included (40Kg)";
                                    }
                                    else
                                    {
                                        lblInBag.Text = "";
                                    }
                                }
                            }
                            if (returnResult != null && returnResult.ResultBookingSource == BookingSource.FlyDubai)
                            {
                                int baggageIncluded = 0;
                                lblOnBagWrapper.Visible = true;// modified by ziya
                                if (!String.IsNullOrEmpty(returnResult.BaggageIncludedInFare))
                                {
                                    if (request.Type == SearchType.Return)
                                    {
                                        baggageIncluded = Convert.ToInt32(returnResult.BaggageIncludedInFare.Split(',')[0]) + Convert.ToInt32(returnResult.BaggageIncludedInFare.Split(',')[1]);
                                    }
                                    else
                                    {
                                        baggageIncluded = Convert.ToInt32(returnResult.BaggageIncludedInFare);
                                    }
                                }
                                if (request.Type == SearchType.OneWay && baggageIncluded < 40 || request.Type == SearchType.Return && baggageIncluded < 80)
                                {
                                    //lblOnBagWrapper.Visible = true;
                                    LoadFZBaggageInfo(ddlOnwardBaggage, ddlInwardBaggage, lblOnBag, lblInBag);
                                }
                                else
                                {
                                    //lblOnBagWrapper.Visible = true;
                                    lblOnBag.Visible = true;
                                    lblInBag.Visible = true;
                                    lblOnBag.Text = "Onward: Max baggage included (40Kg)";
                                    if (request.Type == SearchType.Return)
                                    {
                                        lblInBag.Text = "Return: Max baggage included (40Kg)";
                                    }
                                    else
                                    {
                                        lblInBag.Text = "";
                                    }
                                }
                            }
                            //If the result is of not "Special Round Trip Fare"
                            if (onwardResult != null && (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp) && !onwardResult.IsSpecialRoundTrip)
                            {
                                lblOnBagWrapper.Visible = true;// modified by ziya
                                LoadSGBaggageInfo(ddlOnwardBaggage, lblOnBag, 0);
                                if (SessionValues[ONWARD_BAGGAGE] != null)
                                {
                                    DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                    if (!onwardResult.Flights[0][0].SegmentFareType.Contains("Business")) //Restricting meal option for Spice Jet busniess class
                                    {
                                        LoadSGMealInfo(ddlOnwardMeal, lblOnMeal, dtBaggageInfo);
                                    }
                                    
                                }
                            }
                            //If the result is of not "Special Round Trip Fare"
                            if (returnResult != null && (returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp) && !returnResult.IsSpecialRoundTrip)
                            {
                                lblOnBagWrapper.Visible = true;// modified by ziya
                                LoadSGBaggageInfo(ddlInwardBaggage, lblInBag, 1);
                                if (SessionValues[RETURN_BAGGAGE] != null)
                                {
                                    DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                                    if (!returnResult.Flights[0][0].SegmentFareType.Contains("Business")) //Restricting meal option for Spice Jet busniess class
                                    {
                                        LoadSGMealInfo(ddlInwardMeal, lblInMeal, dtBaggageInfo);
                                    }
                                }
                            }
                            //Code added by Lokesh on 07/10/2016
                            //If the result is of not "Special Round Trip Fare"
                            if (onwardResult != null && (onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp) && !onwardResult.IsSpecialRoundTrip)
                            {
                                lblOnBagWrapper.Visible = true;// modified by ziya
                                Load6EBaggageInfo(ddlOnwardBaggage, lblOnBag, SearchType.OneWay);
                                if (SessionValues[ONWARD_BAGGAGE] != null)
                                {
                                    DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                    Load6EMealInfo(ddlOnwardMeal, ddlInwardMeal, lblOnMeal, lblInMeal, dtBaggageInfo, SearchType.OneWay);
                                }
                            }
                            //If the result is of not "Special Round Trip Fare"
                            if (returnResult != null && (returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.IndigoCorp) && !returnResult.IsSpecialRoundTrip)
                            {
                                lblOnBagWrapper.Visible = true;// modified by ziya
                                Load6EBaggageInfo(ddlInwardBaggage, lblInBag, SearchType.Return);
                                if (SessionValues[RETURN_BAGGAGE] != null)
                                {
                                    DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                                    Load6EMealInfo(ddlOnwardMeal, ddlInwardMeal, lblOnMeal, lblInMeal, dtBaggageInfo, SearchType.Return);
                                }
                            }

                            //If both onward and return results are special round trip then get the baggage ,meals in one single request for 6E
                            if (onwardResult != null && returnResult != null && (onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp) && (returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.IndigoCorp) && onwardResult.IsSpecialRoundTrip && returnResult.IsSpecialRoundTrip)
                            {
                                lblOnBagWrapper.Visible = true;
                                Get6EAvailableSSR(ddlOnwardBaggage, lblOnBag, ddlInwardBaggage, lblInBag, onwardResult, returnResult);
                                if (SessionValues[ONWARD_BAGGAGE] != null && SessionValues[RETURN_BAGGAGE] != null)
                                {
                                    DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                    Load6EMealInfo(ddlOnwardMeal, ddlInwardMeal, lblOnMeal, lblInMeal, dtBaggageInfo);
                                }
                            }

                            //If both onward and return results are special round trip then get the baggage ,meals in one single request for SG
                            if (onwardResult != null && returnResult != null && (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp) && (returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp) && onwardResult.IsSpecialRoundTrip && returnResult.IsSpecialRoundTrip)
                            {
                                lblOnBagWrapper.Visible = true;
                                GetSGAvailableSSR();

                                //Onward Baggage && Meals Binding
                                LoadSGBaggageInfo(ddlOnwardBaggage, lblOnBag, 0);
                                if (SessionValues[ONWARD_BAGGAGE] != null)
                                {
                                    DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                    if (!onwardResult.Flights[0][0].SegmentFareType.Contains("Business")) //Restricting meal option for Spice Jet busniess class
                                    {
                                        LoadSGMealInfo(ddlOnwardMeal, lblOnMeal, dtBaggageInfo);
                                    }
                                }

                                //Return Baggage && Meals Binding
                                LoadSGBaggageInfo(ddlInwardBaggage, lblInBag, 1);
                                if (SessionValues[RETURN_BAGGAGE] != null)
                                {
                                    DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                                    if (!returnResult.Flights[0][0].SegmentFareType.Contains("Business")) //Restricting meal option for Spice Jet busniess class
                                    {
                                        LoadSGMealInfo(ddlInwardMeal, lblInMeal, dtBaggageInfo);
                                    }
                                }
                            }

                            //Go Air Baggage and Meals
                            if (onwardResult != null && (onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp))
                            {
                                lblOnBagWrapper.Visible = true;
                                LoadG8BaggageInfo(ddlOnwardBaggage, lblOnBag, 0);
                                if (SessionValues[ONWARD_BAGGAGE] != null)
                                {
                                    DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                    LoadG8MealInfo(ddlOnwardMeal, lblOnMeal, dtBaggageInfo, onwardResult.ResultBookingSource);
                                }
                            }
                            if (returnResult != null && (returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp))
                            {
                                lblOnBagWrapper.Visible = true;
                                LoadG8BaggageInfo(ddlInwardBaggage, lblInBag, 1);
                                if (SessionValues[RETURN_BAGGAGE] != null)
                                {
                                    DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                                    LoadG8MealInfo(ddlInwardMeal, lblInMeal, dtBaggageInfo, returnResult.ResultBookingSource);
                                }
                            }


                            //Bind additional pax details
                            dlAdditionalPax.DataSource = additionalPax;
                            dlAdditionalPax.DataBind();

                            //To Load for Lead pax changed by shiva
                            AssignCountryAndNationality(ddlCountry, ddlNationality);
                        }
                        #endregion

                        ShowFlightsDetails();

                        SetupAPISMandateFields(onwardResult, returnResult);

                        /************************************************************************************
                         *              Retrieve Latest prices (UAPI RePricing Check)
                         * **********************************************************************************             
                         * Initially hdfFareAction.Value will be "Get", When user chooses to continue
                         * then hdfFareAction.Value will "Update" and when fare is updated then
                         * hdfFareAction.Value will "Updated". After PriceRsp is returned actuall price
                         * Comparison will be done in .aspx in order to show the difference values in popup.
                         * For Popup we are using <div id="FareDiff"> in .aspx
                         * **********************************************************************************/

                        if (onwardResult != null && hdfOnwardFareAction.Value == "Update" && onwardResult.ResultBookingSource == BookingSource.UAPI)//Update the accepted price
                        {
                            UpdateUAPIChangedPrice(onwardResult, SearchType.OneWay);
                        }
                        if (returnResult != null && hdfReturnFareAction.Value == "Update" && returnResult.ResultBookingSource == BookingSource.UAPI)//Update the accepted price
                        {
                            UpdateUAPIChangedPrice(returnResult, SearchType.Return);
                        }
                        if (onwardResult != null && hdfOnwardFareAction.Value == "Update" && (onwardResult.ResultBookingSource == BookingSource.AirArabia || onwardResult.ResultBookingSource == BookingSource.PKFares))
                        {
                            UpdateG9ChangedPrice(onwardResult, SearchType.OneWay);
                        }
                        if (returnResult != null && hdfReturnFareAction.Value == "Update" && (returnResult.ResultBookingSource == BookingSource.AirArabia || returnResult.ResultBookingSource == BookingSource.PKFares))
                        {
                            UpdateG9ChangedPrice(returnResult, SearchType.Return);
                        }
                        //If the user agrees to the new fare change then we will update the latest price particular to indigo flights by calling once again price itinerary.
                        if (onwardResult != null && hdfOnwardFareAction.Value == "Update" && (onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp))//Update the accepted price
                        {
                            hdfOnwardFareAction.Value = "Updated";
                        }
                        if (returnResult != null && hdfReturnFareAction.Value == "Update" && (returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.IndigoCorp))//Update the accepted price
                        {

                            hdfReturnFareAction.Value = "Updated";
                        }
                        //Spice Jet Repricing 
                        if (onwardResult != null && hdfOnwardFareAction.Value == "Update" && (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp))//Update the accepted price
                        {

                            hdfOnwardFareAction.Value = "Updated";
                        }
                        if (returnResult != null && hdfReturnFareAction.Value == "Update" && (returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp))//Update the accepted price
                        {

                            hdfReturnFareAction.Value = "Updated";
                        }
                        //GoAir Repricing 
                        if (onwardResult != null && hdfOnwardFareAction.Value == "Update" && (onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp))//Update the accepted price
                        {

                            hdfOnwardFareAction.Value = "Updated";
                        }
                        if (returnResult != null && hdfReturnFareAction.Value == "Update" && (returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp))//Update the accepted price
                        {

                            hdfReturnFareAction.Value = "Updated";
                        }
                        if (SessionValues[ONWARD_RESULT] != null)
                        {
                            onwardResult = SessionValues[ONWARD_RESULT] as SearchResult;
                        }

                        if (SessionValues[RETURN_RESULT] != null)
                        {
                            returnResult = SessionValues[RETURN_RESULT] as SearchResult;
                        }
                        if (SessionValues[ONWARD_TBOFARE] != null)
                        {
                            onwardFares = SessionValues[ONWARD_TBOFARE] as Fare[];
                        }
                        if (SessionValues[RETURN_TBOFARE] != null)
                        {
                            returnFares = SessionValues[RETURN_TBOFARE] as Fare[];
                        }

                        if (onwardResult.IsTimesChanged || onwardResult.IsBookingClassChanged)
                        {
                            ShowFlightsDetails();
                            lblTimeChanged.Visible = true;
                        }


                        if (onwardFares != null && onwardFares.Length > 0 && hdfOnwardFareAction.Value == "Update" && onwardResult.ResultBookingSource == BookingSource.TBOAir)
                        {
                            UpdateTBOAirChangedPrice(onwardResult, SearchType.OneWay);
                        }
                        if (returnFares != null && returnFares.Length > 0 && hdfReturnFareAction.Value == "Update" && returnResult != null && returnResult.ResultBookingSource == BookingSource.TBOAir)
                        {
                            UpdateTBOAirChangedPrice(returnResult, SearchType.Return);
                        }

                        decimal tboDiscount = 0, tboTotal = 0;


                        if (onwardResult != null && onwardResult.ResultBookingSource == BookingSource.TBOAir)
                        {
                            decimal onmarkUp = 0, onbaseFare = 0, ontax = 0, ondiscount = 0, onk3Tax=0;
                            for (int i = 0; i < onwardResult.FareBreakdown.Length; i++)
                            {
                                onmarkUp += Convert.ToDecimal(onwardResult.FareBreakdown[i].AgentMarkup.ToString("N" + agency.DecimalValue)); // *result.FareBreakdown[i].PassengerCount;
                                ondiscount += Convert.ToDecimal(onwardResult.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue)); // *result.FareBreakdown[i].PassengerCount;
                                onbaseFare += Convert.ToDecimal(onwardResult.FareBreakdown[i].BaseFare.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(onwardResult.FareBreakdown[i].HandlingFee.ToString("N" + agency.DecimalValue));
                                ontax += Convert.ToDecimal(onwardResult.FareBreakdown[i].Tax.ToString("N" + agency.DecimalValue));
                            }
                            onk3Tax = Convert.ToDecimal(onwardResult.Price.K3Tax.ToString("N"+agency.DecimalValue));
                            lblOnwardBaseFare.Text = (onbaseFare).ToString("N" + agency.DecimalValue);
                            tboDiscount = ondiscount;
                            Discount = ondiscount;
                            ondiscount = Convert.ToDecimal(ondiscount.ToString("N" + agency.DecimalValue));
                            ontax += (onmarkUp + (onwardResult.Price.OtherCharges) + onwardResult.Price.AdditionalTxnFee + onwardResult.Price.SServiceFee + onwardResult.Price.TransactionFee);
                            tboTotal = (onbaseFare + ontax - ondiscount);
                            onwardTotal = (onbaseFare + ontax - ondiscount);
                            lblOnwardTax.Text = (ontax-onk3Tax).ToString("N" + agency.DecimalValue);
                            
                            lblOnwardTotal.Text = (onbaseFare + (decimal)ontax - ondiscount).ToString("N" + agency.DecimalValue);
                            lblOnwardGTotal.Text = Math.Ceiling(onbaseFare + (decimal)ontax - ondiscount).ToString("N" + agency.DecimalValue);
                            lblOnwardPubFare.Text = Math.Ceiling(onbaseFare + (decimal)ontax).ToString("N" + agency.DecimalValue);
                            onwardPubFare = (onbaseFare + (decimal)ontax);
                            decimal baggage = 0;
                            lblOnwardBaggage.Text = baggage.ToString("N" + agency.DecimalValue);
                            lblOnwardDiscount.Text = "(-) " + ondiscount.ToString("N" + agency.DecimalValue);
                            lblOnwardK3Tax.Text = onk3Tax.ToString("N" + agency.DecimalValue);
                        }
                        if (onwardResult != null && onwardResult.ResultBookingSource != BookingSource.TBOAir)
                        {
                            decimal onmarkUp = 0, onbaseFare = 0, ontax = 0, ondiscount = 0, onk3Tax=0;
                            for (int i = 0; i < onwardResult.FareBreakdown.Length; i++)
                            {

                                onmarkUp += Convert.ToDecimal(onwardResult.FareBreakdown[i].AgentMarkup.ToString("N" + agency.DecimalValue));
                                ondiscount += Convert.ToDecimal(onwardResult.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue));
                                onbaseFare += Convert.ToDecimal(onwardResult.FareBreakdown[i].BaseFare.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(onwardResult.FareBreakdown[i].HandlingFee.ToString("N" + agency.DecimalValue));
                                ontax += Convert.ToDecimal(onwardResult.FareBreakdown[i].Tax.ToString("N" + agency.DecimalValue));

                            }
                            onk3Tax = Convert.ToDecimal(onwardResult.Price.K3Tax.ToString("N" + agency.DecimalValue)); 
                            lblOnwardBaseFare.Text = (onbaseFare).ToString("N" + agency.DecimalValue);
                            tboDiscount = ondiscount;
                            Discount = ondiscount;
                            ondiscount = Convert.ToDecimal(ondiscount.ToString("N" + agency.DecimalValue));
                            ontax += (onmarkUp + (onwardResult.Price.OtherCharges) + onwardResult.Price.AdditionalTxnFee + onwardResult.Price.SServiceFee + onwardResult.Price.TransactionFee);
                            tboTotal = (onbaseFare + ontax - ondiscount);
                            onwardTotal = (onbaseFare + ontax - ondiscount);
                            lblOnwardTax.Text = (ontax- onk3Tax).ToString("N" + agency.DecimalValue);
                            onwardResult.TotalFare = (double)onwardTotal;
                            lblOnwardTotal.Text = (onbaseFare + ontax - ondiscount).ToString("N" + agency.DecimalValue);
                            lblOnwardGTotal.Text = (onbaseFare + ontax - ondiscount).ToString("N" + agency.DecimalValue);
                            lblOnwardPubFare.Text = ((decimal)onbaseFare + ontax).ToString("N" + agency.DecimalValue);
                            onwardPubFare = (onbaseFare + Convert.ToDecimal(ontax.ToString("N" + agency.DecimalValue)));
                            decimal baggage = 0;
                            decimal meal = 0;
                            lblOnwardMeal.Text = meal.ToString("N" + agency.DecimalValue);
                            lblOnwardBaggage.Text = baggage.ToString("N" + agency.DecimalValue);
                            lblOnwardDiscount.Text = "(-) " + ondiscount.ToString("N" + agency.DecimalValue);
                            lblOnwardK3Tax.Text = onk3Tax.ToString("N" + agency.DecimalValue);


                        }
                        if (returnResult != null && returnResult.ResultBookingSource == BookingSource.TBOAir)
                        {
                            decimal inmarkUp = 0, inbaseFare = 0, intax = 0, indiscount = 0,ink3Tax=0;

                            for (int i = 0; i < returnResult.FareBreakdown.Length; i++)
                            {
                                inmarkUp += Convert.ToDecimal(returnResult.FareBreakdown[i].AgentMarkup.ToString("N" + agency.DecimalValue)); // *result.FareBreakdown[i].PassengerCount;
                                indiscount += Convert.ToDecimal(returnResult.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue)); // *result.FareBreakdown[i].PassengerCount;
                                inbaseFare += Convert.ToDecimal(returnResult.FareBreakdown[i].BaseFare.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(returnResult.FareBreakdown[i].HandlingFee.ToString("N" + agency.DecimalValue));
                                intax += Convert.ToDecimal(returnResult.FareBreakdown[i].Tax.ToString("N" + agency.DecimalValue));
                            }
                            ink3Tax = Convert.ToDecimal(returnResult.Price.K3Tax.ToString("N" + agency.DecimalValue)); 
                            lblReturnBaseFare.Text = (inbaseFare).ToString("N" + agency.DecimalValue);
                            tboDiscount += indiscount;
                            Discount += indiscount;
                            indiscount = Convert.ToDecimal(indiscount.ToString("N" + agency.DecimalValue));
                            intax += (inmarkUp + (returnResult.Price.OtherCharges) + returnResult.Price.AdditionalTxnFee + returnResult.Price.SServiceFee + returnResult.Price.TransactionFee);
                            tboTotal += (inbaseFare + intax - indiscount);
                            returnTotal = (inbaseFare + intax - indiscount);
                            lblReturnTax.Text = (intax-ink3Tax).ToString("N" + agency.DecimalValue);
                            lblReturnTotal.Text = (inbaseFare + intax - indiscount).ToString("N" + agency.DecimalValue);
                            lblReturnGTotal.Text = Math.Ceiling(inbaseFare + intax - indiscount).ToString("N" + agency.DecimalValue);
                            lblReturnPubFare.Text = Math.Ceiling(inbaseFare + intax).ToString("N" + agency.DecimalValue);
                            returnPubFare = (inbaseFare + intax);
                            decimal baggage = 0;
                            lblReturnBaggage.Text = baggage.ToString("N" + agency.DecimalValue);
                            lblReturnDiscount.Text = "(-) " + indiscount.ToString("N" + agency.DecimalValue);
                            //Read ceiling values from Onward & Return Total Label for Booking Review Total fares
                            lblBaseFare.Text = (Convert.ToDecimal(lblOnwardBaseFare.Text) + Convert.ToDecimal(lblReturnBaseFare.Text)).ToString("N" + agency.DecimalValue);
                            lblTax.Text = (Convert.ToDecimal(lblOnwardTax.Text) + Convert.ToDecimal(lblReturnTax.Text)).ToString("N" + agency.DecimalValue);
                            lblTotalFare.Text = (Convert.ToDecimal(lblOnwardGTotal.Text) + Convert.ToDecimal(lblReturnGTotal.Text)).ToString("N" + agency.DecimalValue);
                            //lblTotalFare.Text = (Convert.ToDecimal(lblOnwardTotal.Text) + Convert.ToDecimal(lblReturnTotal.Text)).ToString("N" + agency.DecimalValue);
                            lblTotalPrice.Text = (Convert.ToDecimal(lblOnwardGTotal.Text) + Convert.ToDecimal(lblReturnGTotal.Text)).ToString("N" + agency.DecimalValue);
                            lblDiscount.Text = "(-) " + tboDiscount.ToString("N" + agency.DecimalValue);
                            lblTotalPubFare.Text = Math.Ceiling(Convert.ToDecimal(lblTotalPrice.Text) + tboDiscount).ToString("N" + agency.DecimalValue);
                            totalPubFare = Math.Ceiling(Convert.ToDecimal(lblTotalPrice.Text) + tboDiscount);
                            lblReturnK3Tax.Text = ink3Tax.ToString("N" + agency.DecimalValue);
                        }
                        if (returnResult != null && returnResult.ResultBookingSource != BookingSource.TBOAir)
                        {
                            decimal inmarkUp = 0, inbaseFare = 0, intax = 0, indiscount = 0, inK3tax = 0;

                            for (int i = 0; i < returnResult.FareBreakdown.Length; i++)
                            {
                                inmarkUp += Convert.ToDecimal(returnResult.FareBreakdown[i].AgentMarkup.ToString("N" + agency.DecimalValue));
                                indiscount += Convert.ToDecimal(returnResult.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue));
                                inbaseFare += Convert.ToDecimal(returnResult.FareBreakdown[i].BaseFare.ToString("N" + agency.DecimalValue)) + Convert.ToDecimal(returnResult.FareBreakdown[i].HandlingFee.ToString("N" + agency.DecimalValue));
                                intax += Convert.ToDecimal(returnResult.FareBreakdown[i].Tax.ToString("N" + agency.DecimalValue));
                               
                            }
                            inK3tax = Convert.ToDecimal(returnResult.Price.K3Tax.ToString("N" + agency.DecimalValue));
                            lblReturnBaseFare.Text = (inbaseFare).ToString("N" + agency.DecimalValue);
                            tboDiscount += indiscount;
                            Discount += indiscount;
                            indiscount = Convert.ToDecimal(indiscount.ToString("N" + agency.DecimalValue));
                            intax += (inmarkUp + (returnResult.Price.OtherCharges) + returnResult.Price.AdditionalTxnFee + returnResult.Price.SServiceFee + returnResult.Price.TransactionFee);
                            tboTotal += (inbaseFare + intax - indiscount);
                            returnTotal = (inbaseFare + intax - indiscount);
                            returnResult.TotalFare = (double)returnTotal;
                            lblReturnTax.Text = (intax-inK3tax).ToString("N" + agency.DecimalValue);
                            lblReturnTotal.Text = (inbaseFare + intax - indiscount).ToString("N" + agency.DecimalValue);
                            lblReturnGTotal.Text = (inbaseFare + intax - indiscount).ToString("N" + agency.DecimalValue);
                            lblReturnPubFare.Text = (inbaseFare + intax).ToString("N" + agency.DecimalValue);
                            returnPubFare = (inbaseFare + intax);
                            decimal baggage = 0;
                            decimal meal = 0;
                            lblReturnMeal.Text = meal.ToString("N" + agency.DecimalValue);
                            lblReturnBaggage.Text = baggage.ToString("N" + agency.DecimalValue);
                            lblReturnDiscount.Text = "(-) " + indiscount.ToString("N" + agency.DecimalValue);

                            lblBaseFare.Text = (Convert.ToDecimal(lblOnwardBaseFare.Text) + Convert.ToDecimal(lblReturnBaseFare.Text)).ToString("N" + agency.DecimalValue);
                            lblTax.Text = (Convert.ToDecimal(lblOnwardTax.Text) + Convert.ToDecimal(lblReturnTax.Text)).ToString("N" + agency.DecimalValue);
                            lblTotalFare.Text = (Convert.ToDecimal(lblOnwardGTotal.Text) + Convert.ToDecimal(lblReturnGTotal.Text)).ToString("N" + agency.DecimalValue);
                            //lblTotalFare.Text = (Convert.ToDecimal(lblOnwardTotal.Text) + Convert.ToDecimal(lblReturnTotal.Text)).ToString("N" + agency.DecimalValue);
                            lblTotalPrice.Text = (Convert.ToDecimal(lblOnwardGTotal.Text) + Convert.ToDecimal(lblReturnGTotal.Text)).ToString("N" + agency.DecimalValue);
                            lblDiscount.Text = "(-) " + tboDiscount.ToString("N" + agency.DecimalValue);
                            lblTotalPubFare.Text = (Convert.ToDecimal(lblTotalPrice.Text) + tboDiscount).ToString("N" + agency.DecimalValue);
                            totalPubFare = (Convert.ToDecimal(lblTotalPrice.Text) + tboDiscount);
lblReturnK3Tax.Text = inK3tax.ToString("N" + agency.DecimalValue);
                        }

                        if (returnResult == null)//ONE WAY
                        {
                            if (onwardResult != null && onwardResult.ResultBookingSource != BookingSource.TBOAir)
                            {
                                lblTotalPrice.Text = lblTotalFare.Text = (Convert.ToDecimal(lblOnwardGTotal.Text)).ToString("N" + agency.DecimalValue);
                                lblDiscount.Text = "(-) " + tboDiscount.ToString("N" + agency.DecimalValue);
                                lblTotalPubFare.Text = (Convert.ToDecimal(lblTotalPrice.Text) + tboDiscount).ToString("N" + agency.DecimalValue);
                                totalPubFare = (Convert.ToDecimal(lblTotalPrice.Text) + tboDiscount);
                            }
                            else if (onwardResult != null && onwardResult.ResultBookingSource == BookingSource.TBOAir)
                            {
                                lblTotalFare.Text = (Convert.ToDecimal(lblOnwardTotal.Text)).ToString("N" + agency.DecimalValue);
                                lblTotalPrice.Text = (Convert.ToDecimal(lblOnwardGTotal.Text)).ToString("N" + agency.DecimalValue);
                                lblDiscount.Text = "(-) " + tboDiscount.ToString("N" + agency.DecimalValue);
                                lblTotalPubFare.Text = (Convert.ToDecimal(lblTotalPrice.Text) + tboDiscount).ToString("N" + agency.DecimalValue);
                                totalPubFare = (Convert.ToDecimal(lblTotalPrice.Text) + tboDiscount);
                            }
                        }
                    }
                }

                if (agency != null && !IsPostBack)
                {
                    txtAddress1.Text = agency.Address;
                }
                //After repricing need to intialise pax count.
                //Otherwise Validations will not fire for additional pax.
                paxCount = request.AdultCount + request.ChildCount + request.InfantCount;
                //After re price need to show the GST details pop up for indigo.
                //Also verify the agency country code should fall under india.
                //ONWARD RESULT:FOR UAPI SG ,6E AND G8
                if (onwardResult != null && (onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp || onwardResult.ResultBookingSource == BookingSource.UAPI || onwardResult.ResultBookingSource == BookingSource.UAPI || onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp))
                {
                    if (onwardResult.Flights[0][0].Origin.CountryCode == "IN")
                    {
                        if (agency != null && agency.Country > 0 && Country.GetCountryCodeByCountryId(agency.Country) == "IN" && location.CountryCode == "IN")
                        {
                            if (onwardResult.ResultBookingSource == BookingSource.UAPI)
                            {
                                requiredGSTForUAPI = true;
                                txtGSTNumber.Text = location.GstNumber;
                                txtGSTContactNumber.Text = agency.Phone1;
                            }
                            else//FOR REMAINING SUPPLIERS SG ,6E AND G8
                            {

                                location = Settings.LoginInfo.IsOnBehalfOfAgent ? new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation) : new LocationMaster(Settings.LoginInfo.LocationID);

                                if (location != null)
                                {
                                    if (!string.IsNullOrEmpty(location.GstNumber) && location.GstNumber.Length > 0)
                                    {

                                        txtTaxRegNo_SG_GST.Text = location.GstNumber;
                                        txtTaxRegNo_SG_GST.Enabled = false;
                                    }
                                    if (!string.IsNullOrEmpty(agency.Name) && agency.Name.Length > 0)
                                    {

                                        txtCompanyName_SG_GST.Text = agency.Name;
                                        txtCompanyName_SG_GST.Enabled = false;
                                    }
                                    if (!string.IsNullOrEmpty(agency.Email2) && agency.Email2.Length > 0)
                                    {

                                        txtGSTOfficialEmail.Text = agency.Email2;
                                        txtGSTOfficialEmail.Enabled = false;
                                    }
                                    requiredGSTForSG_6E = true;

                                }

                            }
                        }

                    }
                }

                //RETURN RESULT:FOR UAPI SG ,6E AND G8
                if (returnResult != null && (returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.IndigoCorp || returnResult.ResultBookingSource == BookingSource.UAPI || returnResult.ResultBookingSource == BookingSource.UAPI || returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp))
                {
                    if (returnResult.Flights[0][0].Origin.CountryCode == "IN")
                    {
                        if (agency != null && agency.Country > 0 && Country.GetCountryCodeByCountryId(agency.Country) == "IN" && location.CountryCode == "IN")
                        {
                            if (returnResult.ResultBookingSource == BookingSource.UAPI)
                            {
                                requiredGSTForUAPI = true;
                                txtGSTNumber.Text = location.GstNumber;
                                txtGSTContactNumber.Text = agency.Phone1;
                            }
                            else//FOR REMAINING SUPPLIERS SG ,6E AND G8
                            {
                                location = Settings.LoginInfo.IsOnBehalfOfAgent ? new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation) : new LocationMaster(Settings.LoginInfo.LocationID);

                                if (location != null)
                                {
                                    if (!string.IsNullOrEmpty(location.GstNumber) && location.GstNumber.Length > 0)
                                    {
                                        txtTaxRegNo_SG_GST.Text = location.GstNumber;
                                        txtTaxRegNo_SG_GST.Enabled = false;
                                    }
                                    if (!string.IsNullOrEmpty(agency.Name) && agency.Name.Length > 0)
                                    {
                                        txtCompanyName_SG_GST.Text = agency.Name;
                                        txtCompanyName_SG_GST.Enabled = false;
                                    }
                                    if (!string.IsNullOrEmpty(agency.Email2) && agency.Email2.Length > 0)
                                    {

                                        txtGSTOfficialEmail.Text = agency.Email2;
                                        txtGSTOfficialEmail.Enabled = false;
                                    }
                                    requiredGSTForSG_6E = true;

                                }
                            }
                        }

                    }
                }

                //For Round Trip
                if (onwardResult != null && returnResult != null && (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp || returnResult.ResultBookingSource == BookingSource.GoAir) && (!string.IsNullOrEmpty(onwardResult.FareType) && onwardResult.FareType.ToLower().Contains("corporate") || !string.IsNullOrEmpty(returnResult.FareType) && returnResult.FareType.ToLower().Contains("corporate")))
                {
                    mealRequiredSG_Corporate = true;
                    isOnwardCorporateFare = onwardResult.FareType.ToLower().Contains("corporate"); 
                    isReturnCorporateFare = returnResult.FareType.ToLower().Contains("corporate");

                }
                //For One Way
                if (onwardResult != null && returnResult == null && (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp) && (!string.IsNullOrEmpty(onwardResult.FareType) && onwardResult.FareType.ToLower().Contains("corporate")))
                {
                    mealRequiredSG_Corporate = true;
                    isOnwardCorporateFare = true;
                }

                if (onwardResult != null && onwardResult.ResultBookingSource == BookingSource.AirArabia)
                    isG9OnwardBundled = onwardResult.Flights[0].ToList().Exists(x => x.SegmentFareType.ToLower().Contains("value") || x.SegmentFareType.ToLower().Contains("extra"));

                if(returnResult != null && returnResult.ResultBookingSource == BookingSource.AirArabia)
                    isG9ReturnBundled = returnResult.Flights[0].ToList().Exists(x => x.SegmentFareType.ToLower().Contains("value") || x.SegmentFareType.ToLower().Contains("extra"));

                if (isG9OnwardBundled)
                {
                    for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                    {
                        if (string.IsNullOrEmpty(hdnG9BundleSegments.Value))
                            hdnG9BundleSegments.Value = onwardResult.Flights[0][i].Origin.AirportCode + "-" + onwardResult.Flights[0][i].Destination.AirportCode;
                        else
                            hdnG9BundleSegments.Value += "," + onwardResult.Flights[0][i].Origin.AirportCode + "-" + onwardResult.Flights[0][i].Destination.AirportCode;
                    }
                }
                if (isG9ReturnBundled)
                {
                    for (int i = 0; i < returnResult.Flights[0].Length; i++)
                    {
                        if (string.IsNullOrEmpty(hdnG9BundleSegments.Value))
                            hdnG9BundleSegments.Value = returnResult.Flights[0][i].Origin.AirportCode + "-" + returnResult.Flights[0][i].Destination.AirportCode;
                        else
                            hdnG9BundleSegments.Value += "," + returnResult.Flights[0][i].Origin.AirportCode + "-" + returnResult.Flights[0][i].Destination.AirportCode;
                    }
                }
                //Modified by lokesh on 10-08-2018.
                //If the particular agency requires the flex fields.
                //So before reprice and after reprice need to display the flex fields on the passenger details page.

                //if (agency.RequiredFlexFields && request.CorporateTravelProfileId <= 0)
                //{
                //    DataTable dtFlexFields = CorporateProfile.GetAgentFlexDetailsByProduct((int)agency.ID, 1);
                //    //hdnFlexCount.Value = dtFlexFields.Rows.Count.ToString();
                //    int i = 0;
                //    foreach (DataRow row in dtFlexFields.Rows)
                //    {
                //        HtmlGenericControl divlblFlex1 = new HtmlGenericControl("Div");
                //        divlblFlex1.Attributes.Add("class", "col-md-3");

                //        HiddenField hdnFlexId = new HiddenField();
                //        hdnFlexId.ID = "hdnFlexId" + i;
                //        hdnFlexId.Value = row["flexId"].ToString();
                //        divlblFlex1.Controls.Add(hdnFlexId);

                //        HiddenField hdnFlexControl = new HiddenField();
                //        hdnFlexControl.ID = "hdnFlexControl" + i;
                //        hdnFlexControl.Value = Convert.ToString(row["flexControl"]);
                //        divlblFlex1.Controls.Add(hdnFlexControl);

                //        HiddenField hdnFlexMandatory = new HiddenField();
                //        hdnFlexMandatory.ID = "hdnFlexMandatory" + i;
                //        hdnFlexMandatory.Value = Convert.ToString(row["flexMandatoryStatus"]);
                //        divlblFlex1.Controls.Add(hdnFlexMandatory);

                //        HiddenField hdnFlexLabel = new HiddenField();
                //        hdnFlexLabel.ID = "hdnFlexLabel" + i;
                //        hdnFlexLabel.Value = Convert.ToString(row["flexLabel"]);
                //        divlblFlex1.Controls.Add(hdnFlexLabel);

                //        Label lblFlex1 = new Label();
                //        lblFlex1.ID = "lblFlex" + i;
                //        if (Convert.ToString(row["flexMandatoryStatus"]) == "N")
                //        {
                //            lblFlex1.Text = "<strong>" + Convert.ToString(row["flexLabel"]) + ":</strong>";
                //        }
                //        else
                //        {
                //            lblFlex1.Text = "<strong>" + Convert.ToString(row["flexLabel"]) + ":<span class='red_span'>*</span></strong>";
                //        }

                //        divlblFlex1.Controls.Add(lblFlex1);

                //        HtmlGenericControl divFlexControl = new HtmlGenericControl("Div");
                //        divFlexControl.Attributes.Add("class", "col-md-3");

                //        Label lblError = new Label();
                //        lblError.ID = "lblError" + i;
                //        lblError.Font.Bold = true;
                //        lblError.CssClass = "red_span";

                //        HtmlTableCell tableCell1 = new HtmlTableCell();
                //        switch (row["flexControl"].ToString())
                //        {
                //            case "T": //TextBox
                //                TextBox txtFlex = new TextBox();
                //                txtFlex.ID = "txtFlex" + i;
                //                txtFlex.CssClass = "form-control";
                //                if (row["flexDataType"].ToString() == "N") //N means Numeric
                //                {
                //                    txtFlex.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'1');");
                //                }
                //                if (row["Detail_FlexData"] != DBNull.Value)
                //                {
                //                    txtFlex.Text = Convert.ToString(row["Detail_FlexData"]);
                //                }
                //                divFlexControl.Controls.Add(txtFlex);
                //                divFlexControl.Controls.Add(lblError);
                //                break;
                //            case "D"://Date

                //                DropDownList ddlDay = new DropDownList();
                //                ddlDay.ID = "ddlDay" + i;
                //                ddlDay.Width = new Unit(70, UnitType.Percentage);
                //                ddlDay.CssClass = "form-control pull-left ";
                //                BindDates(ddlDay);

                //                DropDownList ddlMonth = new DropDownList();
                //                ddlMonth.ID = "ddlMonth" + i;
                //                ddlMonth.Width = new Unit(70, UnitType.Percentage);
                //                ddlMonth.CssClass = "form-control pull-left ";
                //                BindMonths(ddlMonth);

                //                DropDownList ddlYear = new DropDownList();
                //                ddlYear.ID = "ddlYear" + i;
                //                ddlYear.Width = new Unit(100, UnitType.Percentage);
                //                ddlYear.CssClass = "form-control pull-left ";
                //                BindYears(ddlYear);

                //                DateTime date;
                //                if (row["Detail_FlexData"] != DBNull.Value)
                //                {
                //                    try
                //                    {
                //                        date = Convert.ToDateTime(row["Detail_FlexData"]);
                //                        ddlDay.SelectedValue = date.Day.ToString();
                //                        ddlMonth.SelectedValue = date.Month.ToString();
                //                        ddlYear.SelectedValue = date.Year.ToString();
                //                    }
                //                    catch { }
                //                }

                //                divFlexControl.Controls.Add(ddlDay);
                //                divFlexControl.Controls.Add(ddlMonth);
                //                divFlexControl.Controls.Add(ddlYear);
                //                divFlexControl.Controls.Add(lblError);
                //                break;
                //            case "L"://DropDown
                //                DropDownList ddlFlex = new DropDownList();
                //                ddlFlex.ID = "ddlFlex" + i;
                //                ddlFlex.CssClass = "form-control";
                //                DataTable dt = dctFlexFieldDropdownData != null && dctFlexFieldDropdownData.ContainsKey(Convert.ToString(row["flexLabel"])) ?
                //                    dctFlexFieldDropdownData[Convert.ToString(row["flexLabel"])] : CorporateProfile.FillDropDown(Convert.ToString(row["flexSqlQuery"]));

                //                if (dt != null && dt.Rows.Count > 0)
                //                {
                //                    if (dctFlexFieldDropdownData != null && !dctFlexFieldDropdownData.ContainsKey(Convert.ToString(row["flexLabel"])))
                //                        dctFlexFieldDropdownData.Add(Convert.ToString(row["flexLabel"]), dt);
                //                    ddlFlex.DataSource = dt;
                //                    ddlFlex.DataTextField = dt.Columns[1].ColumnName;
                //                    ddlFlex.DataValueField = dt.Columns[0].ColumnName;
                //                    ddlFlex.DataBind();
                //                }
                //                ddlFlex.Items.Insert(0, new ListItem("-- Select--", "-1"));
                //                if (row["Detail_FlexData"] != DBNull.Value)
                //                {
                //                    try
                //                    {
                //                        ddlFlex.SelectedValue = Convert.ToString(row["Detail_FlexData"]);
                //                    }
                //                    catch { }
                //                }
                //                divFlexControl.Controls.Add(ddlFlex);
                //                divFlexControl.Controls.Add(lblError);
                //                break;
                //        }
                //        tblFlexFields.Controls.Add(divlblFlex1); //Flex Label Binding
                //        tblFlexFields.Controls.Add(divFlexControl);//Flex Control Binding
                //        i++;
                //    }
                //    BindAddPaxFlex(dtFlexFields);
                //}

                //For HandBaggage No additional baggage will be available
                if (onwardResult != null && !string.IsNullOrEmpty(onwardResult.FareType) && onwardResult.FareType.Contains("HandBaggage Only"))// For Hand Baggage , Addnl baggae is not allowed 
                {
                    ddlOnwardBaggage.Visible = false;
                }
                if (returnResult != null && !string.IsNullOrEmpty(returnResult.FareType) && returnResult.FareType.Contains("HandBaggage Only"))// For Hand Baggage , Addnl baggae is not allowed
                {
                    ddlInwardBaggage.Visible = false;
                }
                //Do not show the baggage div's if there are no items in the baggage && Meal dropdowns for Lead Passenger.
                if (ddlOnwardBaggage != null)//Onward Baggage Dropdown
                {
                    if (!ddlOnwardBaggage.Visible)
                    {
                        leadPaxOnBagDiv.Visible = false;
                    }
                }
                if (ddlInwardBaggage != null)//Return Baggage Dropdown
                {
                    if (!ddlInwardBaggage.Visible)
                    {
                        leadPaxRetBagDiv.Visible = false;
                    }
                }
                //Meal Drop downs only for LCC's.
                if ((onwardResult != null && onwardResult.IsLCC) || (returnResult != null && returnResult.IsLCC))
                {
                    if (ddlOnwardMeal != null)//Onward Meal Dropdown
                    {
                        if (!ddlOnwardMeal.Visible)
                        {
                            leadPaxOnMealDiv.Visible = false;
                        }
                    }
                    if (ddlInwardMeal != null)//Return Meal Dropdown
                    {
                        if (!ddlInwardMeal.Visible)
                        {
                            leadPaxRetMealDiv.Visible = false;
                        }
                    }
                }

                //Added for To restrict the Meal option to User for Spice Jet business class
                if ((onwardResult != null && (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp) && onwardResult.Flights[0][0].SegmentFareType.Contains("Business")) || (returnResult != null && (returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp) && returnResult.Flights[0][0].SegmentFareType.Contains("Business")))  //paranthesis added in condition 
                {
                    leadPaxOnMealDiv.Visible = false;
                    leadPaxRetMealDiv.Visible = false;
                }

                //Do not display the Select Baggage
                //For Onward only
                if (request.Type == SearchType.OneWay && !ddlOnwardBaggage.Visible && lccLeadPaxBaggagelbl != null)
                {
                    lccLeadPaxBaggagelbl.Visible = false;
                }
                //For round trip only
                if (request.Type == SearchType.Return && !ddlOnwardBaggage.Visible && !ddlInwardBaggage.Visible && lccLeadPaxBaggagelbl != null)
                {
                    lccLeadPaxBaggagelbl.Visible = false;
                }

                //Do not display the Select Meal
                //For Onward only
                if (request.Type == SearchType.OneWay && !ddlOnwardMeal.Visible && leadPaxMealLblOnw != null)
                {
                    leadPaxMealLblOnw.Visible = false;
                }
                //For round trip only
                if (request.Type == SearchType.Return && !ddlOnwardMeal.Visible && !ddlInwardMeal.Visible && leadPaxMealLblOnw != null)
                {
                    leadPaxMealLblOnw.Visible = false;
                }

            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.RePrice, Severity.High, (int)Settings.LoginInfo.UserID, "Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
            Response.Redirect("ErrorPage.aspx?Err=" + ex.Message.ToString(), true);
        }

    }

    /// <summary>
    /// Retrieve TWO results for Corporate profile along with current selected result.
    /// These two results will be picked either from current selected departure time minus 5 hours
    /// and if minus 5 hours is falling on same date
    /// OR sorting the current result list with shortest & cheapest to pick the first two results.
    /// Returns selected result indexes for fare rule assigning in Payment confirmation page.
    /// </summary>
    /// <param name="results"></param>
    /// <param name="selectedResult"></param>
    protected List<SearchResult> GetCriteriaResults(SearchResult[] results, SearchResult selectedResult)
    {
        List<SearchResult> CriteriaResults = new List<SearchResult>();
        try
        {
            List<SearchResult> SearchResults = new List<SearchResult>();
            SearchResults.AddRange(results);

            string sessionId = string.Empty;
            if (Session["sessionId"] != null)
            {
                sessionId = Session["sessionId"].ToString();
            }

            ////TODO: Add try catch here
            //CT.MetaSearchEngine.MetaSearchEngine mse = new  CT.MetaSearchEngine.MetaSearchEngine(sessionId);
            //mse.SettingsLoginInfo = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo;


            DateTime criteriaTime = selectedResult.Flights[0][0].DepartureTime.AddHours(-5);
            if (Session["FlightRequest"] != null)
            {
                request = Session["FlightRequest"] as SearchRequest;
            }
            if (criteriaTime.Date == request.Segments[0].PreferredDepartureTime.Date)
            {
                List<SearchResult> similarResults = SearchResults.FindAll(delegate (SearchResult sr) { return sr.Flights[0][0].DepartureTime <= criteriaTime && sr.ResultBookingSource == selectedResult.ResultBookingSource; });

                if (similarResults != null)
                {
                    if (similarResults.Count >= 2)
                    {
                        CriteriaResults.Add(similarResults[0]);
                        CriteriaResults.Add(similarResults[1]);
                    }
                    else
                    {
                        CriteriaResults.Add(SearchResults[0]);
                        CriteriaResults.Add(SearchResults[1]);
                    }
                }
                else
                {
                    CriteriaResults.Add(SearchResults[0]);
                    CriteriaResults.Add(SearchResults[1]);
                }
            }
            else
            {
                CriteriaResults.Add(SearchResults[0]);
                CriteriaResults.Add(SearchResults[1]);
            }



        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "(CorpBooking)Failed to fetch Shortest & Cheapest flights. Reason : " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        return CriteriaResults;
    }

    /// <summary>
    /// Generate THREE Flight policies for saving the policy criteria.
    /// </summary>
    protected FlightPolicy GenerateFlightPolicy(SearchResult result)
    {
        FlightPolicy policy = null;
        int policyReasonId = 0;
        if (Request["trid"] != null)//Policy Reason Id entered by the user
        {
            policyReasonId = Convert.ToInt32(Request["trid"]);
        }
        try
        {
            string policyBreakingRules = string.Empty;

            foreach (KeyValuePair<string, List<string>> pair in result.TravelPolicyResult.PolicyBreakingRules)
            {
                foreach (string rule in pair.Value)
                {
                    if (policyBreakingRules.Length > 0)
                    {
                        policyBreakingRules += "," + rule;
                    }
                    else
                    {
                        policyBreakingRules = rule;
                    }
                }
            }

            string flightNumbers = string.Empty;

            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    if (flightNumbers.Length > 0)
                    {
                        flightNumbers += "-" + result.Flights[i][j].Airline + result.Flights[i][j].FlightNumber;
                    }
                    else
                    {
                        flightNumbers = result.Flights[i][j].Airline + result.Flights[i][j].FlightNumber;
                    }
                }
            }
            if (Session["FlightRequest"] != null)
            {
                request = Session["FlightRequest"] as SearchRequest;
            }
            policy = new FlightPolicy();
            policy.IsUnderPolicy = result.TravelPolicyResult.IsUnderPolicy;
            policy.PolicyBreakingRules = policyBreakingRules;
            policy.PolicyReasonId = policyReasonId;
            policy.ProfileGrade = Settings.LoginInfo.CorporateProfileGrade;
            //policy.ProfileId = Settings.LoginInfo.CorporateProfileId;
            policy.ProfileId = request.CorporateTravelProfileId;

            policy.Status = "A";
            policy.TravelReasonId = request.CorporateTravelReasonId;
            policy.TravelDate = request.Segments[0].PreferredDepartureTime;
            policy.FlightNumbers = flightNumbers;
            policy.SelectedTrip = true;
        }
        catch
        {

        }

        return policy;
    }
    /// <summary>
    /// This method will display all the segment details in the left pane
    /// Written by Venkat 15-Oct-2016
    /// </summary>
    private void ShowFlightsDetails()
    {
        try
        {
            tblOnwardFlightDetails.Rows.Clear();
            tblReturnFlightDetails.Rows.Clear();
            string[] values = new string[0];
            if (SessionValues[ONWARD_VALUES_CHANGED] != null)
            {
                BookingValuesChanged = SessionValues[ONWARD_VALUES_CHANGED].ToString();
                values = BookingValuesChanged.Split(',');
            }
            if (onwardResult != null)
            {
                if (onwardResult.ResultBookingSource != BookingSource.UAPI)
                {
                    clsMSE.BuildBaggageCodes(ref onwardResult);
                }
                for (int i = 0; i < onwardResult.Flights.Length; i++)
                {
                    for (int j = 0; j < onwardResult.Flights[i].Length; j++)
                    {
                        HtmlTableRow trCities = new HtmlTableRow();
                        HtmlTableCell tdOnCities = new HtmlTableCell();
                        tdOnCities.InnerHtml = "<strong>" + onwardResult.Flights[0][j].Origin.CityName + "</strong> (" + onwardResult.Flights[0][j].Origin.CityCode + ") to <strong>" + onwardResult.Flights[0][j].Destination.CityName + "</strong> (" + onwardResult.Flights[0][j].Destination.CityCode + ")";
                        tdOnCities.ColSpan = 2;
                        trCities.Controls.Add(tdOnCities);
                        tblOnwardFlightDetails.Controls.Add(trCities);

                        HtmlTableRow trAirline = new HtmlTableRow();
                        HtmlTableCell tdAirline = new HtmlTableCell();
                        tdAirline.InnerText = "Airline:";
                        HtmlTableCell tdOnAirline = new HtmlTableCell();
                        tdOnAirline.InnerText = onwardResult.Flights[0][j].Airline;
                        trAirline.Controls.Add(tdAirline);
                        trAirline.Controls.Add(tdOnAirline);
                        tblOnwardFlightDetails.Controls.Add(trAirline);

                        HtmlTableRow trFlightNo = new HtmlTableRow();
                        HtmlTableCell tdFlightNo = new HtmlTableCell();
                        tdFlightNo.InnerText = "Flight No:";
                        HtmlTableCell tdOnFlightNo = new HtmlTableCell();
                        tdOnFlightNo.InnerText = onwardResult.Flights[0][j].FlightNumber;
                        trFlightNo.Controls.Add(tdFlightNo);
                        trFlightNo.Controls.Add(tdOnFlightNo);
                        tblOnwardFlightDetails.Controls.Add(trFlightNo);

                        HtmlTableRow trDepDate = new HtmlTableRow();
                        HtmlTableCell tdDepDate = new HtmlTableCell();
                        tdDepDate.InnerText = "Departure Date:";
                        HtmlTableCell tdOnDepDate = new HtmlTableCell();

                        List<string> segments = new List<string>();
                        foreach (string val in values)
                        {
                            foreach (string seg in val.Split('|'))
                            {
                                if (seg == onwardResult.Flights[0][j].Origin.AirportCode + "-" + onwardResult.Flights[0][j].Destination.AirportCode)
                                {
                                    segments.AddRange(val.Split('|'));
                                    break;
                                }
                            }
                        }

                        if (onwardResult.IsTimesChanged && segments.Exists(delegate (string s) { return s == "D"; }))
                        {
                            tdOnDepDate.InnerHtml = "<b style='color:Red'>" + onwardResult.Flights[0][j].DepartureTime.ToString("dd-MMM-yyyy, HH:mm tt") + "</b>";
                        }
                        else
                        {
                            tdOnDepDate.InnerText = onwardResult.Flights[0][j].DepartureTime.ToString("dd-MMM-yyyy, HH:mm tt");
                        }
                        trDepDate.Controls.Add(tdDepDate);
                        trDepDate.Controls.Add(tdOnDepDate);
                        tblOnwardFlightDetails.Controls.Add(trDepDate);

                        if (!string.IsNullOrEmpty(onwardResult.Flights[0][j].DepTerminal))
                        {
                            HtmlTableRow trDepTerminal = new HtmlTableRow();
                            HtmlTableCell tdDepTerminal = new HtmlTableCell();
                            tdDepTerminal.InnerText = "Departure Terminal:";
                            HtmlTableCell tdOnDepTerminal = new HtmlTableCell();
                            tdOnDepTerminal.InnerText = onwardResult.Flights[0][j].DepTerminal;
                            trDepTerminal.Controls.Add(tdDepTerminal);
                            trDepTerminal.Controls.Add(tdOnDepTerminal);
                            tblOnwardFlightDetails.Controls.Add(trDepTerminal);
                        }

                        HtmlTableRow trArrDate = new HtmlTableRow();
                        HtmlTableCell tdArrDate = new HtmlTableCell();
                        tdArrDate.InnerText = "Arrival Date:";
                        HtmlTableCell tdOnArrDate = new HtmlTableCell();
                        if (onwardResult.IsTimesChanged && segments.Exists(delegate (string s) { return s == "A"; }))
                        {
                            tdOnArrDate.InnerHtml = "<b style='color:red'>" + onwardResult.Flights[0][j].ArrivalTime.ToString("dd-MMM-yyyy, HH:mm tt") + "</b>";
                        }
                        else
                        {
                            tdOnArrDate.InnerText = onwardResult.Flights[0][j].ArrivalTime.ToString("dd-MMM-yyyy, HH:mm tt");
                        }
                        trArrDate.Controls.Add(tdArrDate);
                        trArrDate.Controls.Add(tdOnArrDate);
                        tblOnwardFlightDetails.Controls.Add(trArrDate);

                        if (!string.IsNullOrEmpty(onwardResult.Flights[0][j].ArrTerminal))
                        {
                            HtmlTableRow trArrTerminal = new HtmlTableRow();
                            HtmlTableCell tdArrTerminal = new HtmlTableCell();
                            tdArrTerminal.InnerText = "Arrival Terminal:";
                            HtmlTableCell tdOnArrTerminal = new HtmlTableCell();
                            tdOnArrTerminal.InnerText = onwardResult.Flights[0][j].ArrTerminal;
                            trArrTerminal.Controls.Add(tdArrTerminal);
                            trArrTerminal.Controls.Add(tdOnArrTerminal);
                            tblOnwardFlightDetails.Controls.Add(trArrTerminal);
                        }

                        HtmlTableRow trClass = new HtmlTableRow();
                        HtmlTableCell tdClass = new HtmlTableCell();
                        tdClass.InnerText = "Cabin Class:";
                        HtmlTableCell tdOnClass = new HtmlTableCell();

                        if ((onwardResult.Flights[0][j].CabinClass == " " || onwardResult.Flights[0][j].CabinClass == null) && (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp))
                        {
                            tdOnClass.InnerText = "Economy";
                        }
                        else
                        {
                            if (segments.Exists(delegate (string s) { return s == "C"; }))
                            {
                                tdOnClass.InnerHtml = "<b style='color:red'>" + onwardResult.Flights[0][j].CabinClass + "</b>";
                            }
                            else
                            {
                                tdOnClass.InnerText = onwardResult.Flights[0][j].CabinClass;
                            }
                        }
                        trClass.Controls.Add(tdClass);
                        trClass.Controls.Add(tdOnClass);
                        tblOnwardFlightDetails.Controls.Add(trClass);

                        HtmlTableRow trBkgClass = new HtmlTableRow();
                        HtmlTableCell tdBkgClass = new HtmlTableCell();
                        tdBkgClass.InnerText = "Booking Class";
                        HtmlTableCell tdOnBkgClass = new HtmlTableCell();
                        if (onwardResult.IsBookingClassChanged && segments.Exists(delegate (string s) { return s == "B"; }))
                        {
                            tdOnBkgClass.InnerHtml = "<b style='color:red'>" + onwardResult.Flights[0][j].BookingClass + "</b>";
                        }
                        else
                        {
                            tdOnBkgClass.InnerText = onwardResult.Flights[0][j].BookingClass;
                        }
                        trBkgClass.Cells.Add(tdBkgClass);
                        trBkgClass.Cells.Add(tdOnBkgClass);
                        tblOnwardFlightDetails.Rows.Add(trBkgClass);

                        HtmlTableRow trFareType = new HtmlTableRow();
                        HtmlTableCell tdFareType = new HtmlTableCell();
                        tdFareType.InnerText = "Fare Type:";
                        HtmlTableCell tdOnFareType = new HtmlTableCell();

                        if (onwardResult.ResultBookingSource != BookingSource.UAPI)
                        {
                            if (onwardResult.FareType != null)
                            {
                                if (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.FlyDubai || onwardResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl || onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.IndigoCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp || onwardResult.ResultBookingSource==BookingSource.SalamAir || onwardResult.ResultBookingSource == BookingSource.AirArabia)
                                {
                                    if ((onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.IndigoCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp || onwardResult.ResultBookingSource == BookingSource.SalamAir || onwardResult.ResultBookingSource == BookingSource.AirArabia) && !string.IsNullOrEmpty(onwardResult.Flights[0][j].SegmentFareType))
                                    {
                                        tdOnFareType.InnerText = onwardResult.Flights[0][j].SegmentFareType;
                                    }
                                    else
                                    {
                                        tdOnFareType.InnerText = onwardResult.FareType.Split(',')[0];
                                    }
                                }
                                else
                                {
                                    tdOnFareType.InnerText = onwardResult.FareType;
                                }
                            }
                        }
                        else
                        {
                            tdFareType.Visible = false;
                            tdOnFareType.Visible = false;
                        }

                        HtmlTableRow trDefaultBaggage = new HtmlTableRow();
                        HtmlTableCell tdDefaultBaggage = new HtmlTableCell();
                        tdDefaultBaggage.InnerText = "Baggage:";

                        HtmlTableCell tdOnDefaultBaggage = new HtmlTableCell();

                        tdOnDefaultBaggage.InnerText = !string.IsNullOrEmpty(onwardResult.Flights[i][j].DefaultBaggage) ? onwardResult.Flights[i][j].DefaultBaggage : "As Per AirlinePolicy";

                        trDefaultBaggage.Controls.Add(tdDefaultBaggage);
                        trDefaultBaggage.Controls.Add(tdOnDefaultBaggage);
                        tblOnwardFlightDetails.Controls.Add(trDefaultBaggage);

                        trFareType.Controls.Add(tdFareType);
                        trFareType.Controls.Add(tdOnFareType);
                        tblOnwardFlightDetails.Controls.Add(trFareType);
                    }
                }
            }
            if (returnResult != null)
            {
                if (returnResult.ResultBookingSource != BookingSource.UAPI)
                {
                    clsMSE.BuildBaggageCodes(ref returnResult);
                }
                for (int i = 0; i < returnResult.Flights.Length; i++)
                {
                    for (int j = 0; j < returnResult.Flights[i].Length; j++)
                    {
                        HtmlTableRow trCities = new HtmlTableRow();
                        HtmlTableCell tdOnCities = new HtmlTableCell();
                        tdOnCities.InnerHtml = "<strong>" + returnResult.Flights[i][j].Origin.CityName + "</strong> (" + returnResult.Flights[i][j].Origin.CityCode + ") to <strong>" + returnResult.Flights[i][j].Destination.CityName + "</strong> (" + returnResult.Flights[i][j].Destination.CityCode + ")";
                        tdOnCities.ColSpan = 2;
                        trCities.Controls.Add(tdOnCities);
                        tblReturnFlightDetails.Controls.Add(trCities);

                        HtmlTableRow trAirline = new HtmlTableRow();
                        HtmlTableCell tdAirline = new HtmlTableCell();
                        tdAirline.InnerText = "Airline:";
                        HtmlTableCell tdOnAirline = new HtmlTableCell();
                        tdOnAirline.InnerText = returnResult.Flights[i][j].Airline;
                        trAirline.Controls.Add(tdAirline);
                        trAirline.Controls.Add(tdOnAirline);
                        tblReturnFlightDetails.Controls.Add(trAirline);

                        HtmlTableRow trFlightNo = new HtmlTableRow();
                        HtmlTableCell tdFlightNo = new HtmlTableCell();
                        tdFlightNo.InnerText = "Flight No:";
                        HtmlTableCell tdOnFlightNo = new HtmlTableCell();
                        tdOnFlightNo.InnerText = returnResult.Flights[i][j].FlightNumber;
                        trFlightNo.Controls.Add(tdFlightNo);
                        trFlightNo.Controls.Add(tdOnFlightNo);
                        tblReturnFlightDetails.Controls.Add(trFlightNo);

                        HtmlTableRow trDepDate = new HtmlTableRow();
                        HtmlTableCell tdDepDate = new HtmlTableCell();
                        tdDepDate.InnerText = "Departure Date:";
                        HtmlTableCell tdOnDepDate = new HtmlTableCell();

                        List<string> segments = new List<string>();
                        foreach (string val in values)
                        {
                            foreach (string seg in val.Split('|'))
                            {
                                if (seg == returnResult.Flights[i][j].Origin.AirportCode + "-" + returnResult.Flights[i][j].Destination.AirportCode)
                                {
                                    segments.AddRange(val.Split('|'));
                                    break;
                                }
                            }
                        }

                        if (returnResult.IsTimesChanged && segments.Exists(delegate (string s) { return s == "D"; }))
                        {
                            tdOnDepDate.InnerHtml = "<b style='color:red'>" + returnResult.Flights[i][j].DepartureTime.ToString("dd-MMM-yyyy, HH:mm tt") + "</b>";
                        }
                        else
                        {
                            tdOnDepDate.InnerText = returnResult.Flights[i][j].DepartureTime.ToString("dd-MMM-yyyy, HH:mm tt");
                        }
                        trDepDate.Controls.Add(tdDepDate);
                        trDepDate.Controls.Add(tdOnDepDate);
                        tblReturnFlightDetails.Controls.Add(trDepDate);

                        if (!string.IsNullOrEmpty(returnResult.Flights[i][j].DepTerminal))
                        {
                            HtmlTableRow trDepTerminal = new HtmlTableRow();
                            HtmlTableCell tdDepTerminal = new HtmlTableCell();
                            tdDepTerminal.InnerText = "Departure Terminal:";
                            HtmlTableCell tdOnDepTerminal = new HtmlTableCell();
                            tdOnDepTerminal.InnerText = returnResult.Flights[i][j].DepTerminal;
                            trDepTerminal.Controls.Add(tdDepTerminal);
                            trDepTerminal.Controls.Add(tdOnDepTerminal);
                            tblReturnFlightDetails.Controls.Add(trDepTerminal);
                        }
                        HtmlTableRow trArrDate = new HtmlTableRow();
                        HtmlTableCell tdArrDate = new HtmlTableCell();
                        tdArrDate.InnerText = "Arrival Date:";
                        HtmlTableCell tdOnArrDate = new HtmlTableCell();
                        if (returnResult.IsTimesChanged && segments.Exists(delegate (string s) { return s == "A"; }))
                        {
                            tdOnArrDate.InnerHtml = "<b style='color:red'>" + returnResult.Flights[i][j].ArrivalTime.ToString("dd-MMM-yyyy, HH:mm tt") + "</b>";
                        }
                        else
                        {
                            tdOnArrDate.InnerText = returnResult.Flights[i][j].ArrivalTime.ToString("dd-MMM-yyyy, HH:mm tt");
                        }
                        trArrDate.Controls.Add(tdArrDate);
                        trArrDate.Controls.Add(tdOnArrDate);
                        tblReturnFlightDetails.Controls.Add(trArrDate);

                        if (!string.IsNullOrEmpty(returnResult.Flights[i][j].ArrTerminal))
                        {
                            HtmlTableRow trArrTerminal = new HtmlTableRow();
                            HtmlTableCell tdArrTerminal = new HtmlTableCell();
                            tdArrTerminal.InnerText = "Arrival Terminal:";
                            HtmlTableCell tdOnArrTerminal = new HtmlTableCell();
                            tdOnArrTerminal.InnerText = returnResult.Flights[i][j].ArrTerminal;
                            trArrTerminal.Controls.Add(tdArrTerminal);
                            trArrTerminal.Controls.Add(tdOnArrTerminal);
                            tblReturnFlightDetails.Controls.Add(trArrTerminal);
                        }

                        HtmlTableRow trClass = new HtmlTableRow();
                        HtmlTableCell tdClass = new HtmlTableCell();
                        tdClass.InnerText = "Cabin Class:";
                        HtmlTableCell tdOnClass = new HtmlTableCell();
                        if ((returnResult.Flights[i][j].CabinClass == " " || returnResult.Flights[i][j].CabinClass == null) && (returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp))
                        {
                            tdOnClass.InnerText = "Economy";
                        }
                        else
                        {
                            if (segments.Exists(delegate (string s) { return s == "C"; }))
                            {
                                tdOnClass.InnerHtml = "<b style='color:red'>" + returnResult.Flights[i][j].CabinClass + "</b>";
                            }
                            else
                            {
                                tdOnClass.InnerText = returnResult.Flights[i][j].CabinClass;
                            }
                        }
                        trClass.Controls.Add(tdClass);
                        trClass.Controls.Add(tdOnClass);
                        tblReturnFlightDetails.Controls.Add(trClass);

                        HtmlTableRow trBkgClass = new HtmlTableRow();
                        HtmlTableCell tdBkgClass = new HtmlTableCell();
                        tdBkgClass.InnerText = "Booking Class";
                        HtmlTableCell tdRetBkgClass = new HtmlTableCell();
                        if (returnResult.IsBookingClassChanged && segments.Exists(delegate (string s) { return s == "B"; }))
                        {
                            tdRetBkgClass.InnerHtml = "<b style='color:red'>" + returnResult.Flights[i][j].BookingClass + "</b>";
                        }
                        else
                        {
                            tdRetBkgClass.InnerText = returnResult.Flights[i][j].BookingClass;
                        }
                        trBkgClass.Cells.Add(tdBkgClass);
                        trBkgClass.Cells.Add(tdRetBkgClass);
                        tblReturnFlightDetails.Rows.Add(trBkgClass);

                        HtmlTableRow trFareType = new HtmlTableRow();
                        HtmlTableCell tdFareType = new HtmlTableCell();
                        tdFareType.InnerText = "Fare Type:";
                        HtmlTableCell tdOnFareType = new HtmlTableCell();

                        if (returnResult.ResultBookingSource != BookingSource.UAPI )
                        {
                            if (returnResult.FareType != null)
                            {
                                if (returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.FlyDubai || returnResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl || returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.IndigoCorp || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp || returnResult.ResultBookingSource == BookingSource.SalamAir || returnResult.ResultBookingSource == BookingSource.AirArabia)
                                {
                                    if ((returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.IndigoCorp || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp || returnResult.ResultBookingSource == BookingSource.SalamAir || returnResult.ResultBookingSource == BookingSource.AirArabia) && !string.IsNullOrEmpty(returnResult.Flights[i][j].SegmentFareType))
                                    {
                                        tdOnFareType.InnerText = returnResult.Flights[i][j].SegmentFareType;
                                    }
                                    else
                                    {

                                        tdOnFareType.InnerText = (!string.IsNullOrEmpty(returnResult.FareType) && returnResult.FareType.Split(',').Length > 0) ? returnResult.FareType.Split(',')[1] : String.Empty; //Checkinig FareType isnull condition
                                        
                                    }
                                }
                                else
                                {
                                    tdOnFareType.InnerText = returnResult.FareType;
                                }
                            }
                        }
                        else
                        {
                            tdFareType.Visible = false;
                            tdOnFareType.Visible = false;
                        }

                        HtmlTableRow trDefaultBaggage = new HtmlTableRow();
                        HtmlTableCell tdDefaultBaggage = new HtmlTableCell();
                        tdDefaultBaggage.InnerText = "Baggage:";

                        HtmlTableCell tdretDefaultBaggage = new HtmlTableCell();

                        tdretDefaultBaggage.InnerText = !string.IsNullOrEmpty(returnResult.Flights[i][j].DefaultBaggage) ? returnResult.Flights[i][j].DefaultBaggage : "As Per AirlinePolicy";

                        trDefaultBaggage.Controls.Add(tdDefaultBaggage);
                        trDefaultBaggage.Controls.Add(tdretDefaultBaggage);
                        tblReturnFlightDetails.Controls.Add(trDefaultBaggage);

                        trFareType.Controls.Add(tdFareType);
                        trFareType.Controls.Add(tdOnFareType);
                        tblReturnFlightDetails.Controls.Add(trFareType);

                        //tblReturnFlightDetails.Controls.Add(trAirline);
                        //tblReturnFlightDetails.Controls.Add(trFlightNo);
                        //tblReturnFlightDetails.Controls.Add(trDepDate);
                        //tblReturnFlightDetails.Controls.Add(trDepTerminal);
                        //tblReturnFlightDetails.Controls.Add(trArrDate);
                        //tblReturnFlightDetails.Controls.Add(trArrTerminal);
                        //tblReturnFlightDetails.Controls.Add(trClass);
                        //tblReturnFlightDetails.Controls.Add(trFareType);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(Flight Pax Page)Failed to Bind flight info: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    void RepriceUAPI(SearchResult result, SearchType searchType)
    {
        try
        {
            CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
            mse.SettingsLoginInfo = Settings.LoginInfo;
            mse.AppUserId = (int)Settings.LoginInfo.UserID;
            mse.SessionId = sessionId;

            if (searchType == SearchType.OneWay)
            {
                //onwardPriceRsp = mse.RePriceUAPI(result, SearchType.OneWay, ref onwardFares, ref BookingValuesChanged);
                onwardPriceRsp = mse.RePriceUAPI(result, SearchType.OneWay, request, ref onwardFares, ref BookingValuesChanged);
            }
            else
            {
                //returnPriceRsp = mse.RePriceUAPI(result, SearchType.OneWay, ref returnFares, ref BookingValuesChanged);
                returnPriceRsp = mse.RePriceUAPI(result, SearchType.OneWay, request, ref returnFares, ref BookingValuesChanged);
            }
            decimal totalFare = 0m;
            if (!string.IsNullOrEmpty(result.RepriceErrorMessage))
            {
                Response.Redirect("ErrorPage.aspx?Err=" + result.RepriceErrorMessage, false);
            }

            if (searchType == SearchType.OneWay)
            {
                foreach (Fare fare in onwardFares)
                {
                    totalFare += (decimal)fare.TotalFare;
                }
            }
            else
            {
                foreach (Fare fare in returnFares)
                {
                    totalFare += (decimal)fare.TotalFare;
                }
            }

            PriceAccounts price = result.Price;
            decimal tax = 0m;

            mse.CalculateInputVATForFlight(request.Segments[0].Origin, request.Segments[0].Destination, result.ResultBookingSource, ref price, totalFare, ref tax);
            int totalpax = request.AdultCount + request.ChildCount + request.InfantCount;



            if (searchType == SearchType.OneWay)
            {
                foreach (Fare fare in onwardFares)
                {
                    fare.TotalFare += ((double)(tax / totalpax) * fare.PassengerCount);
                }

                hdfOnwardFareAction.Value = "Init";
                SaveInSession(ONWARD_RESULT, result);
                SaveInSession(ONWARD_VALUES_CHANGED, BookingValuesChanged);
                SaveInSession(ONWARD_TBOFARE, onwardFares);
            }
            else
            {
                foreach (Fare fare in returnFares)
                {
                    fare.TotalFare += ((double)(tax / totalpax) * fare.PassengerCount);
                }
                hdfReturnFareAction.Value = "Init";
                SaveInSession(RETURN_RESULT, result);
                SaveInSession(RETURN_VALUES_CHANGED, BookingValuesChanged);
                SaveInSession(RETURN_TBOFARE, returnFares);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void UpdateUAPIChangedPrice(SearchResult result, SearchType searchType)
    {
        try
        {
            request = Session["FlightRequest"] as SearchRequest;
            paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

            //Clear previous fares
            result.TotalFare = 0;
            result.BaseFare = 0;
            result.Tax = 0;

            //Calculate Markup for the new price
            decimal markup = 0, discount = 0;
            for (int count = 0; count < result.FareBreakdown.Length; count++)
            {
                Fare fare = result.FareBreakdown[count];

                CT.BookingEngine.PriceAccounts tempPrice = CT.AccountingEngine.AccountingEngine.GetPrice(result, count, (int)agency.ID, 0, (int)CT.BookingEngine.ProductType.Flight, "B2B");

                result.BaseFare += fare.BaseFare;
                result.Tax += (double)fare.Tax;
                result.TotalFare = (result.BaseFare + result.Tax);
                result.Price.Markup = tempPrice.Markup;
                fare.AgentMarkup = tempPrice.Markup * fare.PassengerCount;
                fare.AgentDiscount = tempPrice.Discount * fare.PassengerCount;
                markup += Math.Round(tempPrice.Markup, agency.DecimalValue) * fare.PassengerCount;
                discount += Math.Round(tempPrice.Discount, agency.DecimalValue) * fare.PassengerCount;
            }
            result.TotalFare += (double)markup;//Add markup to the total fare
            result.TotalFare -= (double)discount;
            if (searchType == SearchType.OneWay)
            {
                hdfOnwardFareAction.Value = "Updated";
                SaveInSession(ONWARD_RESULT, result);
            }
            else
            {
                hdfReturnFareAction.Value = "Updated";
                SaveInSession(RETURN_RESULT, result);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
            lblTimeChanged.Text = "There is an technical error while applying confirmed price. Please search again";
            imgContinue.PostBackUrl = "HotelSearch.aspx?source=Flight";
        }
    }

    void UpdateTBOAirChangedPrice(SearchResult result, SearchType searchType)
    {
        request = Session["FlightRequest"] as SearchRequest;
        paxCount = request.AdultCount + request.ChildCount + request.InfantCount;
        if (searchType == SearchType.OneWay)
        {
            hdfOnwardOtherCharges.Value = result.Price.OtherCharges.ToString();
            result.FareBreakdown = onwardFares;
        }
        else
        {
            hdfReturnOtherCharges.Value = result.Price.OtherCharges.ToString();
            result.FareBreakdown = returnFares;
        }
        List<Fare> NewFares = new List<Fare>();
        NewFares.AddRange((searchType == SearchType.OneWay ? onwardFares : returnFares));
        result.TotalFare = 0;
        result.BaseFare = 0;
        result.Tax = 0;
        decimal markup = 0m, discount = 0m;
        foreach (Fare pax in result.FareBreakdown)
        {
            foreach (Fare fare in (searchType == SearchType.OneWay ? onwardFares : returnFares))
            {
                if (fare.PassengerType == pax.PassengerType)
                {
                    Fare obj = NewFares.Find(delegate (Fare f) { return f.PassengerType == fare.PassengerType; });

                    result.BaseFare += fare.BaseFare;
                    result.Tax += (double)fare.Tax;
                    result.TotalFare = (result.BaseFare + result.Tax);
                    if (result.Price.MarkupType == "P")
                    {
                        if (agency.AgentAirMarkupType == "TF")
                        {
                            decimal total = ((decimal)fare.BaseFare + fare.Tax + ((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / paxCount) * fare.PassengerCount);

                            result.Price.Markup = (total * result.Price.MarkupValue / 100);
                            obj.AgentMarkup = result.Price.Markup;
                        }
                        else if (agency.AgentAirMarkupType == "BF")
                        {
                            result.Price.Markup = ((decimal)fare.BaseFare * result.Price.MarkupValue / 100);
                            obj.AgentMarkup = result.Price.Markup;
                        }
                        else if (agency.AgentAirMarkupType == "TX")
                        {
                            decimal tax = ((decimal)(fare.Tax + (((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / paxCount) * fare.PassengerCount)));
                            result.Price.Markup = (tax * result.Price.MarkupValue / 100);
                            obj.AgentMarkup = result.Price.Markup;
                        }
                    }
                    else
                    {
                        result.Price.Markup = result.Price.MarkupValue * fare.PassengerCount;
                        obj.AgentMarkup = result.Price.MarkupValue * fare.PassengerCount;
                        markup += obj.AgentMarkup;
                    }

                    if (result.Price.DiscountType == "P")
                    {
                        if (agency.AgentAirMarkupType == "BF")
                        {
                            result.Price.Discount = (decimal)fare.BaseFare * result.Price.DiscountValue / 100;
                            fare.AgentDiscount = result.Price.Discount;
                            discount += fare.AgentDiscount;
                        }
                        else if (agency.AgentAirMarkupType == "TX")
                        {
                            result.Price.Discount = (fare.Tax + (((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / paxCount) * fare.PassengerCount)) * result.Price.DiscountValue / 100;
                            fare.AgentDiscount = result.Price.Discount;
                            discount += fare.AgentDiscount;
                        }
                        else if (agency.AgentAirMarkupType == "TF")
                        {
                            result.Price.Discount = (decimal)(fare.TotalFare + (double)(((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / paxCount) * fare.PassengerCount)) * result.Price.DiscountValue / 100;
                            fare.AgentDiscount = result.Price.Discount;
                            discount += fare.AgentDiscount;
                        }
                    }
                    else
                    {
                        result.Price.Discount = result.Price.DiscountValue * fare.PassengerCount;
                        fare.AgentDiscount = result.Price.Discount;
                        discount += fare.AgentDiscount;
                    }

                    //Calculate HandlingFee only if Login user is from India
                    if (result.LoginCountryCode == "IN")
                    {
                        double handlingFee = 0;
                        if (result.Price.HandlingFeeType == "P")
                        {
                            if (obj.PassengerType == PassengerType.Adult && result.ResultBookingSource == BookingSource.TBOAir)
                            {
                                handlingFee = (obj.TotalFare + (double)((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) + obj.AgentMarkup)) * (double)result.Price.HandlingFeeValue / 100;
                            }
                            else
                            {
                                handlingFee = (obj.TotalFare + (double)(obj.AgentMarkup)) * (double)result.Price.HandlingFeeValue / 100;
                            }
                            fare.HandlingFee = (decimal)handlingFee;
                        }
                        else
                        {
                            fare.HandlingFee = result.Price.HandlingFeeValue * fare.PassengerCount;
                        }
                        result.BaseFare += (double)fare.HandlingFee;
                    }
                    break;
                }
            }
        }
        markup = Math.Round(markup, agency.DecimalValue);
        result.Tax += (double)(markup);
        result.TotalFare += (double)(markup);
        result.TotalFare -= (double)discount;
        result.TotalFare += (double)result.Price.OtherCharges;
        result.FareBreakdown = (searchType == SearchType.OneWay ? onwardFares : returnFares);
        if (searchType == SearchType.OneWay)
        {
            SaveInSession(ONWARD_PRICE_CHANGED_TBO, true);
            hdfOnwardFareAction.Value = "Updated";
            SaveInSession(ONWARD_RESULT, result);
        }
        else
        {
            SaveInSession(RETURN_PRICE_CHANGED_TBO, true);
            hdfReturnFareAction.Value = "Updated";
            SaveInSession(RETURN_RESULT, result);
        }
    }

    void UpdateG9ChangedPrice(SearchResult result, SearchType searchType)
    {
        try
        {
            request = Session["FlightRequest"] as SearchRequest;
            paxCount = request.AdultCount + request.ChildCount + request.InfantCount;
            if (searchType == SearchType.OneWay && SessionValues[ONWARD_REPRICEDRESULT] != null)
            {
                onwardResult = SessionValues[ONWARD_RESULT] as SearchResult;
                G9OnwardResult = SessionValues[ONWARD_REPRICEDRESULT] as SearchResult;

                //Clear previous fares
                onwardResult.TotalFare = G9OnwardResult.TotalFare;
                onwardResult.BaseFare = G9OnwardResult.BaseFare;
                onwardResult.Tax = G9OnwardResult.Tax;
                onwardResult.FareBreakdown = G9OnwardResult.FareBreakdown;
                onwardResult = G9OnwardResult;

                hdfOnwardFareAction.Value = "Updated";
                SaveInSession(ONWARD_RESULT, onwardResult);
                RemoveFromSession(ONWARD_REPRICEDRESULT);
                resultId = Convert.ToInt32(pageParams[0]);
                Basket.FlightBookingSession[sessionId].Result[resultId - 1] = onwardResult;
            }
            else if (searchType == SearchType.Return && SessionValues[RETURN_RESULT] != null)
            {
                returnResult = SessionValues[RETURN_RESULT] as SearchResult;
                G9ReturnResult = SessionValues[RETURN_REPRICEDRESULT] as SearchResult;

                //Clear previous fares
                returnResult.TotalFare = G9ReturnResult.TotalFare;
                returnResult.BaseFare = G9ReturnResult.BaseFare;
                returnResult.Tax = G9ReturnResult.Tax;
                returnResult.FareBreakdown = G9ReturnResult.FareBreakdown;
                returnResult = G9ReturnResult;

                hdfReturnFareAction.Value = "Updated";
                SaveInSession(RETURN_RESULT, returnResult);
                RemoveFromSession(RETURN_REPRICEDRESULT);
                resultId = Convert.ToInt32(pageParams[1]);
                Basket.FlightBookingSession[sessionId].Result[resultId - 1] = returnResult;
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
            lblTimeChanged.Text = "There is an technical error while applying confirmed price. Please search again";
            imgContinue.PostBackUrl = "HotelSearch.aspx?source=Flight";
        }
    }

    /// <summary>
    /// This method calculates the Indigo Repricing.
    /// </summary>
    /// <param name="resultObj"></param>
    /// <param name="request"></param>
    private void IndigoRepricing(ref SearchResult resultObj, SearchRequest request)
    {
        try
        {
            //Here we calculate the original price from the ItineraryPrice response.
            //So,we will clear all the previously calculated price component values here.
            resultObj.Price = null;
            resultObj.BaseFare = 0;
            resultObj.Tax = 0;
            resultObj.TotalFare = 0;
            resultObj.FareBreakdown = null;
            int fareBreakDownCount = 0;
            if (request.AdultCount > 0)
            {
                fareBreakDownCount = 1;
            }
            if (request.ChildCount > 0)
            {
                fareBreakDownCount++;
            }
            if (request.InfantCount > 0)
            {
                fareBreakDownCount++;
            }
            resultObj.FareBreakdown = new Fare[fareBreakDownCount];
            GetItineraryPriceForLCCFlights(ref resultObj, request); //Gets the Latest Itenarary Price
            CalculatePriceComponent(ref resultObj, request);//MarkuP Calculation for the result.

        }
        catch (Exception ex)
        {
            throw ex;

            //Response.Redirect("ErrorPage.aspx?Err="+ ex.Message.ToString(), false);
        }
    }

    /// <summary>
    /// Repricing for PKFares.
    /// </summary>
    /// <param name="request"></param>
    /// <param name="resultIndex"></param>
    /// <param name="sessionId"></param>
    private void PKFaresRepricing(SearchRequest request, int resultIndex, string sessionId, SearchType searchType)
    {
        try
        {
            if (searchType == SearchType.OneWay)
            {
                if (SessionValues[ONWARD_REPRICEDRESULT] == null || (SessionValues[ONWARD_REPRICEDRESULT] != null && resultIndex != (SessionValues[ONWARD_REPRICEDRESULT] as SearchResult).ResultId - 1))
                {
                    CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(Session["sessionId"].ToString());
                    mse.SettingsLoginInfo = Settings.LoginInfo;
                    G9OnwardResult = mse.RepricePKFares(resultIndex, request, "B2B");
                    SaveInSession(ONWARD_REPRICEDRESULT, G9OnwardResult);
                }
                else
                {
                    if (resultIndex == (SessionValues[ONWARD_REPRICEDRESULT] as SearchResult).ResultId - 1)
                    {
                        G9OnwardResult = SessionValues[ONWARD_REPRICEDRESULT] as SearchResult;
                    }
                }
            }
            else
            {
                if (SessionValues[RETURN_REPRICEDRESULT] == null || (SessionValues[RETURN_REPRICEDRESULT] != null && resultIndex != (SessionValues[RETURN_REPRICEDRESULT] as SearchResult).ResultId - 1))
                {
                    CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(Session["sessionId"].ToString());
                    mse.SettingsLoginInfo = Settings.LoginInfo;
                    G9ReturnResult = mse.RepricePKFares(resultIndex, request, "B2B");
                    SaveInSession(RETURN_REPRICEDRESULT, G9ReturnResult);
                }
                else
                {
                    if (resultIndex == (SessionValues[RETURN_REPRICEDRESULT] as SearchResult).ResultId - 1)
                    {
                        G9ReturnResult = SessionValues[RETURN_REPRICEDRESULT] as SearchResult;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(PKFares Reprice)Failed to Reprice. Reason : " + ex.ToString(), Request["REMOTE_ADDR"]);
            Response.Redirect("ErrorPage.aspx?Err=Farequote is Mandatory before booking", false);
        }
    }


    /// <summary>
    /// Prices a complete itinerary, computes fees, charges and taxes, and prices using the supplied parameters and returns a response containing the total price including all fees, charges and taxes
    /// The purpose of GetItineraryPrice is to provide the cost for an itinerary.
    /// </summary>
    /// <param name="resultObj"></param>
    /// <param name="request"></param>
    private void GetItineraryPriceForLCCFlights(ref SearchResult resultObj, SearchRequest request)
    {
        try
        {

            CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
            mse.SettingsLoginInfo = Settings.LoginInfo;
            mse.SessionId = Session["sessionId"].ToString();
            mse.GetItineraryPrice(ref resultObj, request);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// This method will recalculate the price which inclues markup calculation for Indigo Flights after receiving the response from PriceItenaray
    /// </summary>
    /// <param name="resultObj"></param>
    /// <param name="request"></param>
    private void CalculatePriceComponent(ref SearchResult resultObj, SearchRequest request)
    {
        try
        {
            int paxCount = 0;
            decimal totalServiceFee = 0;
            int seatCount = 0;
            paxCount = 0;
            totalServiceFee = 0;
            int decimalValue = 0;
            int agencyId;
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agencyId = Settings.LoginInfo.OnBehalfAgentID;
                decimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
            }
            else
            {
                agencyId = Settings.LoginInfo.AgentId;
                decimalValue = Settings.LoginInfo.DecimalValue;
            }
            if (resultObj != null && resultObj.Price != null)
            {
                if (!request.IsDomestic)
                {
                    if (resultObj.Price == null)
                    {
                        resultObj.Price = new PriceAccounts();
                    }
                }
                if (resultObj.Price == null)
                {
                    resultObj.Price = new PriceAccounts();
                }
                for (int count = 0; count < resultObj.FareBreakdown.Length; count++)
                {
                    if (resultObj.FareBreakdown[count].PassengerType != PassengerType.Infant)
                    {
                        seatCount += resultObj.FareBreakdown[count].PassengerCount;
                    }
                    paxCount += resultObj.FareBreakdown[count].PassengerCount;
                    totalServiceFee += resultObj.FareBreakdown[count].AdditionalTxnFee;

                    PriceAccounts tempPrice = CT.AccountingEngine.AccountingEngine.GetPrice(resultObj, count, agencyId, 0, (int)ProductType.Flight, "B2B");

                    resultObj.Price.MarkupType = tempPrice.MarkupType;
                    resultObj.Price.Markup = Math.Round(tempPrice.Markup, decimalValue);
                    resultObj.Price.MarkupValue = tempPrice.MarkupValue;
                    resultObj.Price.DiscountType = tempPrice.DiscountType;
                    resultObj.Price.DiscountValue = tempPrice.DiscountValue;
                    resultObj.FareBreakdown[count].AgentMarkup = Math.Round(tempPrice.Markup, decimalValue) * resultObj.FareBreakdown[count].PassengerCount;
                    resultObj.FareBreakdown[count].AgentDiscount = Math.Round(tempPrice.Discount,decimalValue) * resultObj.FareBreakdown[count].PassengerCount; //Rounding to agent decimal value to avoid decimal issues in discount.
                    resultObj.TotalFare += (double)(resultObj.FareBreakdown[count].AgentMarkup - resultObj.FareBreakdown[count].AgentDiscount);
                    resultObj.FareBreakdown[count].SellingFare = (Convert.ToDouble(tempPrice.PublishedFare - tempPrice.AgentCommission - tempPrice.AgentPLB) + ((resultObj.FareBreakdown[count].TotalFare - resultObj.FareBreakdown[count].BaseFare) / resultObj.FareBreakdown[count].PassengerCount)) * resultObj.FareBreakdown[count].PassengerCount;
                }
            }
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, (int)Settings.LoginInfo.UserID, "(Indigo)Failed to calculate the price component.Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }

    }


    void LoadG9BaggageInfo(DropDownList ddlOnwardBaggage, DropDownList ddlInwardBaggage)
    {
        try
        {
            if (SessionValues[ONWARD_RESULT] != null && Session["sessionId"] != null)
            {
                string sessionId = Session["sessionId"].ToString();
                SearchResult result = SessionValues[ONWARD_RESULT] as SearchResult;

                if (result.ResultBookingSource == BookingSource.AirArabia)
                {
                    CT.BookingEngine.GDS.AirArabia aaObj = new CT.BookingEngine.GDS.AirArabia();

                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        aaObj.UserName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].UserID;
                        aaObj.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].Password;
                        aaObj.Code = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].HAP;
                        aaObj.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                        aaObj.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                    }
                    else
                    {
                        aaObj.UserName = Settings.LoginInfo.AgentSourceCredentials["G9"].UserID;
                        aaObj.Password = Settings.LoginInfo.AgentSourceCredentials["G9"].Password;
                        aaObj.Code = Settings.LoginInfo.AgentSourceCredentials["G9"].HAP;
                        aaObj.AgentCurrency = Settings.LoginInfo.Currency;
                        aaObj.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                    }
                    aaObj.AppUserId = Settings.LoginInfo.UserID.ToString();
                    aaObj.SessionID = sessionId;

                    //Modified by lokesh on 6-April-2018.
                    //No Need to pass multiple onward and Return baggage requests to the API.
                    //Load from session or get from AirArabiaAPI
                    //Clear the session once the booking is completed.
                    DataTable dtBaggageDetails = null;
                    if (SessionValues[ONWARD_BAGGAGE] == null)
                    {
                        dtBaggageDetails = dtBaggageDetails = aaObj.GetBaggageDetails(result, sessionId);
                    }
                    else
                    {
                        dtBaggageDetails = SessionValues[ONWARD_BAGGAGE] as DataTable;
                    }
                    //DataTable dtBaggageDetails = aaObj.GetBaggageDetails(result, sessionId);
                    if (dtBaggageDetails.Rows.Count == 0)
                    {
                        //DataRow row = dtBaggageDetails.NewRow();
                        //row["baggageCode"] = "30 Kg";
                        //row["baggageCharge"] = 50.000;
                        //row["segmentId"] = result.Flights[0][0].FareInfoKey;
                        //dtBaggageDetails.Rows.Add(row);

                        //if (result.Flights[0].Length > 1)
                        //{
                        //    DataRow row1 = dtBaggageDetails.NewRow();
                        //    row1["baggageCode"] = "30 Kg";
                        //    row1["baggageCharge"] = 50.000;
                        //    row1["segmentId"] = result.Flights[0][1].FareInfoKey;
                        //    dtBaggageDetails.Rows.Add(row1);
                        //}
                    }
                    if (dtBaggageDetails != null && dtBaggageDetails.Rows.Count > 0)
                    {

                        SessionValues[ONWARD_BAGGAGE] = dtBaggageDetails;
                        lblBaggage.Text = "Select Baggage:";
                        int segmentId1 = 0, segmentId2 = 0;

                        for (int i = 0; i < result.Flights.Length; i++)
                        {
                            if (i == 0)
                            {
                                if (result.Flights[i].Length == 1)
                                {
                                    FlightInfo flight = result.Flights[i][0];
                                    segmentId1 = Convert.ToInt32(flight.FareInfoKey);
                                }
                                else if (result.Flights[i].Length == 2)
                                {
                                    FlightInfo flight = result.Flights[i][1];
                                    segmentId1 = Convert.ToInt32(flight.FareInfoKey);
                                }
                            }
                            else
                            {
                                if (result.Flights[i].Length == 1)
                                {
                                    FlightInfo flight = result.Flights[i][0];
                                    segmentId2 = Convert.ToInt32(flight.FareInfoKey);
                                }
                                else if (result.Flights[i].Length == 2)
                                {
                                    FlightInfo flight = result.Flights[i][1];
                                    segmentId2 = Convert.ToInt32(flight.FareInfoKey);
                                }
                            }
                        }

                        DataRow[] outbound = dtBaggageDetails.Select("SegmentId=" + segmentId1);

                        if (outbound != null && outbound.Length > 0)
                        {
                            lblBaggage.Visible = true;
                            lblOnBag.Visible = true;
                            ddlOnwardBaggage.Visible = true;
                            ddlOnwardBaggage.Items.Add(new ListItem("Select Baggage", "0"));
                            foreach (DataRow row in outbound)
                            {
                                ListItem item = new ListItem(row["baggageCode"].ToString(), row["baggageCharge"].ToString());
                                ddlOnwardBaggage.Items.Add(item);
                            }
                        }

                        DataRow[] inbound = dtBaggageDetails.Select("SegmentId=" + segmentId2);

                        if (inbound != null && inbound.Length > 0)
                        {
                            ddlInwardBaggage.Visible = true;
                            lblInBag.Visible = true;
                            ddlInwardBaggage.Items.Add(new ListItem("Select Baggage", "0"));
                            foreach (DataRow row in inbound)
                            {
                                ListItem item = new ListItem(row["baggageCode"].ToString(), row["baggageCharge"].ToString());
                                ddlInwardBaggage.Items.Add(item);
                            }
                        }
                    }
                }
            }
            else
            {
                //Response.Redirect("HotelSearch.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.AirArabiaSearch, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    void LoadFZBaggageInfo(DropDownList ddlOnwardBaggage, DropDownList ddlInwardBaggage, Label lblOnBag, Label lblInBag)
    {
        if (onwardResult == null)
        {
            onwardResult = SessionValues[ONWARD_RESULT] as SearchResult;
        }

        List<int> onwardLFIDS = new List<int>();
        List<int> returnLFIDS = new List<int>();

        for (int i = 0; i < onwardResult.Flights.Length; i++)
        {
            for (int j = 0; j < onwardResult.Flights[i].Length; j++)
            {
                if (i == 0)
                {
                    onwardLFIDS.Add(Convert.ToInt32(onwardResult.Flights[i][j].UapiSegmentRefKey));
                }
                else
                {
                    returnLFIDS.Add(Convert.ToInt32(onwardResult.Flights[i][j].UapiSegmentRefKey));
                }
            }
        }

        CT.BookingEngine.GDS.FlyDubaiApi flyDubai = new CT.BookingEngine.GDS.FlyDubaiApi(onwardResult.Flights[0][0].Origin.AirportCode, Session["sessionId"].ToString());
        SourceDetails agentDetails = new SourceDetails();

        if (Settings.LoginInfo.IsOnBehalfOfAgent)
        {
            flyDubai.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
            flyDubai.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
            flyDubai.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["FZ"];
        }
        else
        {
            flyDubai.AgentBaseCurrency = Settings.LoginInfo.Currency;
            flyDubai.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
            flyDubai.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
            agentDetails = Settings.LoginInfo.AgentSourceCredentials["FZ"];
        }

        flyDubai.LoginName = agentDetails.UserID;
        flyDubai.Password = agentDetails.Password;

        DataTable dtBaggageInfo = null;

        if (SessionValues[ONWARD_BAGGAGE] == null)
        {
            dtBaggageInfo = flyDubai.GetBaggageServicesQuotes(onwardResult, onwardResult.GUID);
        }
        else
        {
            dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
            if (request.Type == SearchType.OneWay)
            {
                DataRow[] bags = dtBaggageInfo.Select("LogicalFlightID=" + onwardLFIDS[0]);

                if (bags == null || bags.Length == 0)
                {
                    dtBaggageInfo = flyDubai.GetBaggageServicesQuotes(onwardResult, onwardResult.GUID);
                }
            }
            else
            {
                DataRow[] bags = dtBaggageInfo.Select("LogicalFlightID=" + onwardLFIDS[0]);

                if (bags == null || bags.Length == 0)
                {
                    dtBaggageInfo = flyDubai.GetBaggageServicesQuotes(onwardResult, onwardResult.GUID);
                }
                bags = dtBaggageInfo.Select("LogicalFlightID=" + returnLFIDS[0]);
                if (bags.Length == 0)
                {
                    dtBaggageInfo = flyDubai.GetBaggageServicesQuotes(onwardResult, onwardResult.GUID);
                }
            }
        }

        if (dtBaggageInfo != null && dtBaggageInfo.Rows.Count > 0)
        {
            SessionValues[ONWARD_BAGGAGE] = dtBaggageInfo;
            dtBaggageInfo = dtBaggageInfo.DefaultView.ToTable(true, new string[0]);
            DataRow[] baggageData = new DataRow[0];
            DataRow[] returnBaggage = new DataRow[0];

            if (!onwardResult.IsBaggageIncluded)
            {
                if (request.Type == SearchType.Return)
                {
                    //if (result.FareType.Split(',')[0].ToUpper() == PAYTOCHANGE || result.FareType.Split(',')[0].ToUpper() == NOCHANGE)
                    {
                        if (Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split(',')[0]) > 0)
                        {
                            baggageData = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND LogicalFlightID=" + onwardLFIDS[0]);
                        }
                        else
                        {
                            baggageData = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND LogicalFlightID=" + onwardLFIDS[0]);
                        }
                    }
                    //else if (result.FareType.Split(',')[0].ToUpper() == FREETOCHANGE)
                    //{
                    //    baggageData = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND LogicalFlightID=" + onwardLFIDS[0]);
                    //}

                    //if (result.FareType.Split(',')[1].ToUpper() == PAYTOCHANGE || result.FareType.Split(',')[1].ToUpper() == NOCHANGE)
                    //{
                    if (Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split(',')[1]) > 0)
                    {
                        returnBaggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND LogicalFlightID=" + returnLFIDS[0]);
                    }
                    else
                    {
                        returnBaggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND LogicalFlightID=" + returnLFIDS[0]);
                    }
                    //}
                    //else if (result.FareType.Split(',')[1].ToUpper() == FREETOCHANGE)
                    //{
                    //    returnBaggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND LogicalFlightID=" + returnLFIDS[0]);
                    //}
                }
                else
                {
                    //if (result.FareType.ToUpper() == PAYTOCHANGE || result.FareType.ToUpper() == NOCHANGE)
                    {
                        if (Convert.ToInt32(onwardResult.BaggageIncludedInFare) > 0)
                        {
                            baggageData = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND LogicalFlightID=" + onwardLFIDS[0]);
                        }
                        else
                        {
                            baggageData = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND LogicalFlightID=" + onwardLFIDS[0]);
                        }
                    }
                    //else if (result.FareType.ToUpper() == FREETOCHANGE)
                    //{
                    //    baggageData = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND LogicalFlightID=" + onwardLFIDS[0]);
                    //}
                }
            }
            else if (onwardResult.IsBaggageIncluded)
            {
                baggageData = null;
                //baggageData = dtBaggageInfo.Select("CodeType LIKE 'BUP%'");
            }

            if (baggageData != null)
            {
                lblBaggage.Text = "Select Baggage:";
                lblBaggage.Visible = true;
                lblOnBag.Visible = true;
                ddlOnwardBaggage.Visible = true;
                if (request.Type == SearchType.Return)
                {
                    ddlInwardBaggage.Visible = true;
                    lblInBag.Visible = true;
                }

                //if (result.IsBaggageIncluded)
                if (baggageData.Length > 0 || returnBaggage.Length > 0)
                {
                    if (request.Type == SearchType.Return)
                    {
                        //if (!result.FareType.Contains("Pay To Change") )
                        //{
                        //    if (!result.IsBaggageIncluded)
                        //    {
                        //        ListItem item = new ListItem("Upgrade Baggage", "0");
                        //        ddlOnwardBaggage.Items.Add(item);
                        //        ddlInwardBaggage.Items.Add(item);
                        //    }
                        //}
                        //else
                        {
                            //if (result.FareType.Split(',')[0].ToUpper() == PAYTOCHANGE || result.FareType.Split(',')[0].ToUpper() == NOCHANGE)
                            {
                                if (Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split(',')[0]) > 0)
                                {
                                    ListItem item = new ListItem("Upgrade Baggage", "0");
                                    ddlOnwardBaggage.Items.Add(item);
                                }
                                else
                                {
                                    ListItem item = new ListItem("No Bag", "0");
                                    ddlOnwardBaggage.Items.Add(item);
                                }
                                if (request.Type == SearchType.Return && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split(',')[1]) > 0)
                                {
                                    ListItem item = new ListItem("Upgrade Baggage", "0");
                                    ddlInwardBaggage.Items.Add(item);
                                }
                                else
                                {
                                    ListItem item = new ListItem("No Bag", "0");
                                    ddlInwardBaggage.Items.Add(item);
                                }
                            }
                            //else if (result.FareType.Split(',')[0].ToUpper() == FREETOCHANGE)
                            //{
                            //    ListItem item = new ListItem("Upgrade Baggage", "0");
                            //    ddlOnwardBaggage.Items.Add(item);
                            //}
                            //else
                            //{
                            //    lblOnBag.Text = "Onward: Max baggage included (40 kg).";
                            //    ddlOnwardBaggage.Visible = false;
                            //}
                            //if (result.FareType.Split(',')[1].ToUpper() == PAYTOCHANGE || result.FareType.Split(',')[1].ToUpper() == NOCHANGE)
                            //{
                            //    if (Convert.ToInt32(result.BaggageIncludedInFare.Split(',')[1]) > 0)
                            //    {
                            //        ListItem item = new ListItem("Upgrade Baggage", "0");
                            //        ddlInwardBaggage.Items.Add(item);
                            //    }
                            //    else
                            //    {
                            //        ListItem item = new ListItem("No Bag", "0");
                            //        ddlInwardBaggage.Items.Add(item);
                            //    }
                            //}
                            //else if (result.FareType.Split(',')[1].ToUpper() == FREETOCHANGE)
                            //{
                            //    ListItem item = new ListItem("Upgrade Baggage", "0");
                            //    ddlInwardBaggage.Items.Add(item);
                            //}
                            if (onwardResult.BaggageIncludedInFare.Split(',')[0].Contains("40"))
                            {
                                lblOnBag.Text = "Return: Max baggage included (40Kg).";
                                ddlOnwardBaggage.Visible = false;
                            }
                            if (onwardResult.BaggageIncludedInFare.Split(',')[1].Contains("40"))
                            {
                                lblInBag.Text = "Return: Max baggage included (40Kg).";
                                ddlInwardBaggage.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        //if (result.FareType.ToUpper() == PAYTOCHANGE || result.FareType.Split(',')[0].ToUpper() == NOCHANGE)
                        {
                            if (Convert.ToInt32(onwardResult.BaggageIncludedInFare) > 0)
                            {
                                ListItem item = new ListItem("Upgrade Baggage", "0");
                                ddlOnwardBaggage.Items.Add(item);
                            }
                            else
                            {
                                ListItem item = new ListItem("No Bag", "0");
                                ddlOnwardBaggage.Items.Add(item);
                            }
                        }
                        //else if (result.FareType.ToUpper() == FREETOCHANGE)
                        //{
                        //    ListItem item = new ListItem("Upgrade Baggage", "0");
                        //    ddlOnwardBaggage.Items.Add(item);
                        //}
                        //else
                        //{
                        //    lblOnBag.Text = "Onward: Max baggage included (40 kg).";
                        //    ddlOnwardBaggage.Visible = false;
                        //}
                        if (onwardResult.BaggageIncludedInFare.Contains("40"))
                        {
                            lblOnBag.Text = "Return: Max baggage included (40Kg).";
                            ddlOnwardBaggage.Visible = false;
                        }
                    }
                }
                else if (baggageData.Length <= 0)
                {
                    lblBaggage.Visible = false;
                    ListItem item = new ListItem("No Bag", "0");
                    ddlOnwardBaggage.Items.Add(item);
                }
                else if (returnBaggage.Length <= 0)
                {
                    ListItem item = new ListItem("No Bag", "0");
                    ddlInwardBaggage.Items.Add(item);
                }


                if (!onwardResult.IsBaggageIncluded)
                {
                    if (onwardResult.BaggageIncludedInFare != null)
                    {
                        if (onwardResult.BaggageIncludedInFare.Split(',')[0] == "20")
                        {
                            lblOnBag.Text += " 20Kg included in Fare";
                            foreach (DataRow row in baggageData)
                            {
                                if (onwardResult.BaggageIncludedInFare.Split(',')[0] == "20")
                                {
                                    if (Convert.ToInt32(row["QtyAvailable"]) > 0 && (row["CodeType"].ToString().Trim() == "BUPL" || row["CodeType"].ToString().Trim() == "BUPX"))
                                    {
                                        ListItem item = new ListItem(row["Description"].ToString(), row["Amount"].ToString());
                                        item.Attributes.Add("CodeType", row["CodeType"].ToString().Trim());
                                        item.Attributes.Add("CategoryId", row["CategoryId"].ToString());
                                        if (onwardLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                        {
                                            if (!ddlOnwardBaggage.Items.Contains(item))
                                            {
                                                ddlOnwardBaggage.Items.Add(item);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                    {
                                        ListItem item = new ListItem(row["Description"].ToString().Replace("Baggage Allowance", ""), row["Amount"].ToString());
                                        item.Attributes.Add("CodeType", row["CodeType"].ToString().Trim());
                                        item.Attributes.Add("CategoryId", row["CategoryId"].ToString());
                                        if (onwardLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                        {
                                            if (!ddlOnwardBaggage.Items.Contains(item))
                                            {
                                                ddlOnwardBaggage.Items.Add(item);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (onwardResult.BaggageIncludedInFare.Split(',')[0] == "30")
                        {
                            lblOnBag.Text += " 30Kg included in Fare";
                            foreach (DataRow row in baggageData)
                            {
                                if (onwardResult.BaggageIncludedInFare.Split(',')[0] == "30")
                                {
                                    if (Convert.ToInt32(row["QtyAvailable"]) > 0 && row["CodeType"].ToString().Trim() == "BUPZ")
                                    {
                                        ListItem item = new ListItem(row["Description"].ToString(), row["Amount"].ToString());
                                        item.Attributes.Add("CodeType", row["CodeType"].ToString().Trim());
                                        item.Attributes.Add("CategoryId", row["CategoryId"].ToString());
                                        if (onwardLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                        {
                                            if (!ddlOnwardBaggage.Items.Contains(item))
                                            {
                                                ddlOnwardBaggage.Items.Add(item);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                    {
                                        ListItem item = new ListItem(row["Description"].ToString().Replace("Baggage Allowance", ""), row["Amount"].ToString());
                                        item.Attributes.Add("CodeType", row["CodeType"].ToString().Trim());
                                        item.Attributes.Add("CategoryId", row["CategoryId"].ToString());
                                        if (onwardLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                        {
                                            if (!ddlOnwardBaggage.Items.Contains(item))
                                            {
                                                ddlOnwardBaggage.Items.Add(item);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (onwardResult.BaggageIncludedInFare.Split(',')[0] == "10")
                        {
                            lblOnBag.Text += " 10Kg included in Fare";
                            foreach (DataRow row in baggageData)
                            {
                                if (onwardResult.BaggageIncludedInFare.Split(',')[0] == "10")
                                {
                                    if (Convert.ToInt32(row["QtyAvailable"]) > 0 && (row["CodeType"].ToString().Trim() == "BUPB" || row["CodeType"].ToString().Trim() == "BUPX"))
                                    {
                                        ListItem item = new ListItem(row["Description"].ToString(), row["Amount"].ToString());
                                        item.Attributes.Add("CodeType", row["CodeType"].ToString().Trim());
                                        item.Attributes.Add("CategoryId", row["CategoryId"].ToString());
                                        if (onwardLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                        {
                                            if (!ddlOnwardBaggage.Items.Contains(item))
                                            {
                                                ddlOnwardBaggage.Items.Add(item);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (onwardResult.BaggageIncludedInFare.Split(',')[0] == "0")
                        {
                            foreach (DataRow row in baggageData)
                            {
                                if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                {
                                    ListItem item = new ListItem(row["Description"].ToString().Replace("Baggage Allowance", ""), row["Amount"].ToString());
                                    item.Attributes.Add("CodeType", row["CodeType"].ToString().Trim());
                                    item.Attributes.Add("CategoryId", row["CategoryId"].ToString());
                                    if (onwardLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                    {
                                        if (!ddlOnwardBaggage.Items.Contains(item))
                                        {
                                            ddlOnwardBaggage.Items.Add(item);
                                        }
                                    }
                                    //if (request.Type == SearchType.Return && result.BaggageIncludedInFare.Split(',')[1] == "0")
                                    //{
                                    //    if (returnLFIDS.Exists(delegate(int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                    //    {
                                    //        ddlInwardBaggage.Items.Add(item);
                                    //    }
                                    //}
                                }
                            }
                        }
                        else if (onwardResult.BaggageIncludedInFare.Split(',')[0] == "40")
                        {
                            lblOnBag.Text = "Onward: Max baggage included (40 kg).";
                            ddlOnwardBaggage.Visible = false;
                        }
                        if (request.Type == SearchType.Return)
                        {
                            if (onwardResult.BaggageIncludedInFare.Split(',')[1] == "20")
                            {
                                lblInBag.Text += " 20Kg included in Fare";
                                foreach (DataRow row in returnBaggage)
                                {
                                    if (request.Type == SearchType.Return && onwardResult.BaggageIncludedInFare.Split(',')[1] == "20")
                                    {
                                        if (Convert.ToInt32(row["QtyAvailable"]) > 0 && (row["CodeType"].ToString().Trim() == "BUPL" || row["CodeType"].ToString().Trim() == "BUPX"))
                                        {
                                            ListItem item = new ListItem(row["Description"].ToString(), row["Amount"].ToString());
                                            item.Attributes.Add("CodeType", row["CodeType"].ToString().Trim());
                                            item.Attributes.Add("CategoryId", row["CategoryId"].ToString());
                                            if (returnLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                            {
                                                if (!ddlInwardBaggage.Items.Contains(item))
                                                {
                                                    ddlInwardBaggage.Items.Add(item);
                                                }
                                            }
                                        }
                                    }

                                    else if (request.Type == SearchType.Return && onwardResult.BaggageIncludedInFare.Split(',')[1] == "0")
                                    {
                                        if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                        {
                                            ListItem item = new ListItem(row["Description"].ToString().Replace("Baggage Allowance", ""), row["Amount"].ToString());
                                            item.Attributes.Add("CodeType", row["CodeType"].ToString().Trim());
                                            item.Attributes.Add("CategoryId", row["CategoryId"].ToString());

                                            if (returnLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                            {
                                                if (!ddlInwardBaggage.Items.Contains(item))
                                                {
                                                    ddlInwardBaggage.Items.Add(item);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (onwardResult.BaggageIncludedInFare.Split(',')[1] == "30")
                            {
                                lblInBag.Text += " 30Kg included in Fare";
                                foreach (DataRow row in returnBaggage)
                                {
                                    if (request.Type == SearchType.Return && onwardResult.BaggageIncludedInFare.Split(',')[1] == "30")
                                    {
                                        if (Convert.ToInt32(row["QtyAvailable"]) > 0 && (row["CodeType"].ToString().Trim() == "BUPZ"))
                                        {
                                            ListItem item = new ListItem(row["Description"].ToString(), row["Amount"].ToString());
                                            item.Attributes.Add("CodeType", row["CodeType"].ToString().Trim());
                                            item.Attributes.Add("CategoryId", row["CategoryId"].ToString());
                                            if (returnLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                            {
                                                if (!ddlInwardBaggage.Items.Contains(item))
                                                {
                                                    ddlInwardBaggage.Items.Add(item);
                                                }
                                            }
                                        }
                                    }
                                    else if (request.Type == SearchType.Return && onwardResult.BaggageIncludedInFare.Split(',')[1] == "0")
                                    {
                                        if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                        {
                                            ListItem item = new ListItem(row["Description"].ToString().Replace("Baggage Allowance", ""), row["Amount"].ToString());
                                            item.Attributes.Add("CodeType", row["CodeType"].ToString().Trim());
                                            item.Attributes.Add("CategoryId", row["CategoryId"].ToString());

                                            if (returnLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                            {
                                                if (!ddlInwardBaggage.Items.Contains(item))
                                                {
                                                    ddlInwardBaggage.Items.Add(item);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (onwardResult.BaggageIncludedInFare.Split(',')[1] == "10")
                            {
                                lblInBag.Text += " 10Kg included in Fare";
                                foreach (DataRow row in baggageData)
                                {
                                    if (onwardResult.BaggageIncludedInFare.Split(',')[1] == "10")
                                    {
                                        if (Convert.ToInt32(row["QtyAvailable"]) > 0 && (row["CodeType"].ToString().Trim() == "BUPB" || row["CodeType"].ToString().Trim() == "BUPX"))
                                        {
                                            ListItem item = new ListItem(row["Description"].ToString(), row["Amount"].ToString());
                                            item.Attributes.Add("CodeType", row["CodeType"].ToString().Trim());
                                            item.Attributes.Add("CategoryId", row["CategoryId"].ToString());
                                            if (onwardLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                            {
                                                if (!ddlInwardBaggage.Items.Contains(item))
                                                {
                                                    ddlInwardBaggage.Items.Add(item);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (onwardResult.BaggageIncludedInFare.Split(',')[1] == "0")
                            {
                                foreach (DataRow row in returnBaggage)
                                {
                                    if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                    {
                                        ListItem item = new ListItem(row["Description"].ToString().Replace("Baggage Allowance", ""), row["Amount"].ToString());
                                        item.Attributes.Add("CodeType", row["CodeType"].ToString().Trim());
                                        item.Attributes.Add("CategoryId", row["CategoryId"].ToString());
                                        //if (onwardLFIDS.Exists(delegate(int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                        //{
                                        //    ddlOnwardBaggage.Items.Add(item);
                                        //}
                                        //if (request.Type == SearchType.Return)
                                        {
                                            if (returnLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                                            {
                                                if (!ddlInwardBaggage.Items.Contains(item))
                                                {
                                                    ddlInwardBaggage.Items.Add(item);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (onwardResult.BaggageIncludedInFare.Split(',')[1] == "40")
                            {
                                lblInBag.Text = "Return: Max baggage included (40 kg).";
                                ddlInwardBaggage.Visible = false;
                            }
                        }
                    }
                }
            }

            if (ddlOnwardBaggage.Visible && ddlOnwardBaggage.Items.Count == 1)
            {
                lblBaggage.Visible = false;
                ddlOnwardBaggage.Visible = false;
            }

            if (ddlInwardBaggage.Visible && ddlInwardBaggage.Items.Count == 1)
            {
                lblBaggage.Visible = false;
                ddlInwardBaggage.Visible = false;
            }
        }
        else
        {
            Response.Redirect("HotelSearch.aspx?source=Flight");
        }
    }

    void LoadSGBaggageInfo(DropDownList ddlBag, Label lblBag, int type)
    {
        SearchResult sgResult = null;
        if (type == 0) //ONWARD
        {
            sgResult = SessionValues[ONWARD_RESULT] as SearchResult;
        }
        else if (type == 1)//Return
        {
            sgResult = SessionValues[RETURN_RESULT] as SearchResult;
        }
        request = Session["FlightRequest"] as SearchRequest;

        CT.BookingEngine.GDS.SpiceJetAPIV1 spiceJet = new CT.BookingEngine.GDS.SpiceJetAPIV1();
        SourceDetails agentDetails = new SourceDetails();
        if (sgResult != null && sgResult.ResultBookingSource == BookingSource.SpiceJet)
        {
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                spiceJet.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                spiceJet.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"];

                spiceJet.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"].UserID;
                spiceJet.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"].Password;
                spiceJet.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"].HAP;
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                spiceJet.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                spiceJet.PromoCode = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"].PCC;//Added For Corporate Booking Purpose
            }
            else
            {
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.Currency;
                spiceJet.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                spiceJet.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                agentDetails = Settings.LoginInfo.AgentSourceCredentials["SG"];

                spiceJet.LoginName = Settings.LoginInfo.AgentSourceCredentials["SG"].UserID;
                spiceJet.Password = Settings.LoginInfo.AgentSourceCredentials["SG"].Password;
                spiceJet.AgentDomain = Settings.LoginInfo.AgentSourceCredentials["SG"].HAP;
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.Currency;
                spiceJet.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                spiceJet.PromoCode = Settings.LoginInfo.AgentSourceCredentials["SG"].PCC;//Added For Corporate Booking Purpose
            }
            spiceJet.BookingSourceFlag = "SG";
        }
        else if (sgResult != null && sgResult.ResultBookingSource == BookingSource.SpiceJetCorp)
        {
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                spiceJet.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                spiceJet.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"];

                spiceJet.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"].UserID;
                spiceJet.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"].Password;
                spiceJet.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"].HAP;
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                spiceJet.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                spiceJet.PromoCode = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"].PCC;//Added For Corporate Booking Purpose
            }
            else
            {
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.Currency;
                spiceJet.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                spiceJet.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                agentDetails = Settings.LoginInfo.AgentSourceCredentials["SGCORP"];

                spiceJet.LoginName = Settings.LoginInfo.AgentSourceCredentials["SGCORP"].UserID;
                spiceJet.Password = Settings.LoginInfo.AgentSourceCredentials["SGCORP"].Password;
                spiceJet.AgentDomain = Settings.LoginInfo.AgentSourceCredentials["SGCORP"].HAP;
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.Currency;
                spiceJet.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                spiceJet.PromoCode = Settings.LoginInfo.AgentSourceCredentials["SGCORP"].PCC;//Added For Corporate Booking Purpose
            }
            spiceJet.BookingSourceFlag = "SGCORP";
        }

        //Added by Lokesh
        //For India GST Implementation.
        AgentMaster agentMaster = null;
        if (Settings.LoginInfo != null && Settings.LoginInfo.IsOnBehalfOfAgent)
        {
            agentMaster = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
            if (agentMaster != null && !string.IsNullOrEmpty(agentMaster.AgentCurrency) && agentMaster.AgentCurrency.Length > 0 && agentMaster.AgentCurrency == "INR")
            {
                spiceJet.CurrencyCode = "INR";
            }
            else
            {
                spiceJet.CurrencyCode = "AED";
            }
            if (agentMaster != null && agentMaster.Country > 0 && Country.GetCountryCodeByCountryId(agentMaster.Country) == "IN" && sgResult != null)
            {
                location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                if (location != null && location.CountryCode == "IN")
                {
                    if (!string.IsNullOrEmpty(location.GstNumber) && location.GstNumber.Length > 0)
                    {
                        spiceJet.GSTNumber = location.GstNumber;
                        txtTaxRegNo_SG_GST.Text = location.GstNumber;
                        txtTaxRegNo_SG_GST.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(agency.Name) && agency.Name.Length > 0)
                    {
                        spiceJet.GSTCompanyName = agency.Name;
                        txtCompanyName_SG_GST.Text = agency.Name;
                        txtCompanyName_SG_GST.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(agency.Email2) && agency.Email2.Length > 0)
                    {
                        spiceJet.GSTOfficialEmail = agency.Email2.Split(',')[0];
                        txtGSTOfficialEmail.Text = spiceJet.GSTOfficialEmail;
                        txtGSTOfficialEmail.Enabled = false;
                    }
                    requiredGSTForSG_6E = true;
                }

            }
        }
        else
        {
            if (Settings.LoginInfo != null && !string.IsNullOrEmpty(Settings.LoginInfo.Currency) && Settings.LoginInfo.Currency.Length > 0 && Settings.LoginInfo.Currency == "INR")
            {
                spiceJet.CurrencyCode = "INR";
            }
            else
            {
                spiceJet.CurrencyCode = "AED";
            }
            agentMaster = new AgentMaster(Settings.LoginInfo.AgentId);

            if (agentMaster != null && agentMaster.Country > 0 && Country.GetCountryCodeByCountryId(agentMaster.Country) == "IN" && sgResult != null )
            {
                location = new LocationMaster(Settings.LoginInfo.LocationID);
                if (location != null && location.CountryCode == "IN")
                {
                    if (!string.IsNullOrEmpty(location.GstNumber) && location.GstNumber.Length > 0)
                    {
                        spiceJet.GSTNumber = location.GstNumber;
                        txtTaxRegNo_SG_GST.Text = location.GstNumber;
                        txtTaxRegNo_SG_GST.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(agency.Name) && agency.Name.Length > 0)
                    {
                        spiceJet.GSTCompanyName = agency.Name;
                        txtCompanyName_SG_GST.Text = agency.Name;
                        txtCompanyName_SG_GST.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(agency.Email2) && agency.Email2.Length > 0)
                    {
                        spiceJet.GSTOfficialEmail = agency.Email2.Split(',')[0];
                        txtGSTOfficialEmail.Text = spiceJet.GSTOfficialEmail;
                        txtGSTOfficialEmail.Enabled = false;
                    }
                    requiredGSTForSG_6E = true;
                }

            }
        }
        if (Session["sessionId"] != null)
        {
            spiceJet.SessionID = Session["sessionId"].ToString();
        }
        else
        {
            spiceJet.SessionID = Guid.NewGuid().ToString();
        }
        spiceJet.AppUserID = Convert.ToInt32(Settings.LoginInfo.UserID);
        spiceJet.SearchByAvailability = true;
        DataTable dtBaggageInfo = null;
        if (SessionValues[ONWARD_BAGGAGE] == null && type == 0)//ONWARD
        {
            dtBaggageInfo = spiceJet.GetAvailableSSR(sgResult);
            SaveInSession(ONWARD_BAGGAGE, dtBaggageInfo);
        }
        else if (SessionValues[ONWARD_BAGGAGE] != null && type == 0)
        {
            dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
        }
        else if (SessionValues[RETURN_BAGGAGE] == null && type == 1)//Return
        {
            dtBaggageInfo = spiceJet.GetAvailableSSR(sgResult);
            SaveInSession(RETURN_BAGGAGE, dtBaggageInfo);
        }
        else if (SessionValues[RETURN_BAGGAGE] != null && type == 1)//Return
        {
            dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;


        }
        if (dtBaggageInfo != null && dtBaggageInfo.Rows.Count > 0)
        {
            dtBaggageInfo = dtBaggageInfo.DefaultView.ToTable(true, new string[0]);
            DataRow[] baggageData = new DataRow[0];
            baggageData = dtBaggageInfo.Select("Group=0 AND (Code ='EB05' OR Code ='EB10' OR Code ='EB15' OR Code ='EB20' OR Code ='EB30')");

            if (baggageData != null)
            {
                lblBaggage.Visible = true;
                lblBag.Visible = true;
                ddlBag.Visible = true;
                if (!string.IsNullOrEmpty(sgResult.BaggageIncludedInFare)) //Replacing baggage  weight 'KG' to empty .
                {
                    sgResult.BaggageIncludedInFare = sgResult.BaggageIncludedInFare.Replace(" KG", string.Empty);
                }
                //Case:1: If  Additional Baggage Available.
                if (baggageData.Length > 0)
                {
                    //Case 1.1:  Default baggage Available
                    if (sgResult != null && !string.IsNullOrEmpty(sgResult.BaggageIncludedInFare) && Convert.ToInt32(sgResult.BaggageIncludedInFare.Split(',')[0]) > 0)
                    {
                        ListItem item = new ListItem("Upgrade Baggage", "0");
                        ddlBag.Items.Add(item);
                    }
                    else  //Case 1.2: no  Default baggage Available
                    {
                        ListItem item = new ListItem("No Bag", "0");
                        ddlBag.Items.Add(item);
                    }
                }

                //Case2: If no  additional baggage available.
                if (baggageData.Length <= 0)
                {
                    ListItem item = new ListItem("No Bag", "0");
                    ddlBag.Items.Add(item);
                }
                if (sgResult != null && !string.IsNullOrEmpty(sgResult.BaggageIncludedInFare))
                {
                    if (sgResult.BaggageIncludedInFare.Split(',')[0] == "20")
                    {
                        lblBag.Text += " 20Kg included in Fare";
                        foreach (DataRow row in baggageData)
                        {
                            if (sgResult.BaggageIncludedInFare.Split(',')[0] == "20")
                            {
                                //if (Convert.ToInt32(row["QtyAvailable"]) > 0 && (row["Code"].ToString().Trim() != "EB20"))
                                {
                                    ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString()+"-"+ row["Code"].ToString().Trim());
                                    item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                    if (0 == Convert.ToInt32(row["Group"]))
                                    {
                                        if (!ddlBag.Items.Contains(item))
                                        {
                                            ddlBag.Items.Add(item);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                {
                                    ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                    item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                    if (0 == Convert.ToInt32(row["Group"]))
                                    {
                                        if (!ddlBag.Items.Contains(item))
                                        {
                                            ddlBag.Items.Add(item);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (sgResult.BaggageIncludedInFare.Split(',')[0] == "30")
                    {
                        lblBag.Text += " 30Kg included in Fare";
                        foreach (DataRow row in baggageData)
                        {
                            if (sgResult.BaggageIncludedInFare.Split(',')[0] == "30")
                            {
                                if (Convert.ToInt32(row["QtyAvailable"]) > 0 && row["Code"].ToString().Trim() != "EB30")
                                {
                                    ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                    item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                    if (0 == Convert.ToInt32(row["Group"]))
                                    {
                                        if (!ddlBag.Items.Contains(item))
                                        {
                                            ddlBag.Items.Add(item);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                {
                                    ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                    item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                    if (0 == Convert.ToInt32(row["Group"]))
                                    {
                                        if (!ddlBag.Items.Contains(item))
                                        {
                                            ddlBag.Items.Add(item);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (sgResult.BaggageIncludedInFare.Split(',')[0] == "25") //Added for Spice jet Busniess class default baggage is 25 KG
                    {
                        lblBag.Text += " 25Kg included in Fare";
                        foreach (DataRow row in baggageData)
                        {
                            if (sgResult.BaggageIncludedInFare.Split(',')[0] == "25")
                            {
                                if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                {
                                    ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                    item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                    if (0 == Convert.ToInt32(row["Group"]))
                                    {
                                        if (!ddlBag.Items.Contains(item))
                                        {
                                            ddlBag.Items.Add(item);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                {
                                    ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                    item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                    if (0 == Convert.ToInt32(row["Group"]))
                                    {
                                        if (!ddlBag.Items.Contains(item))
                                        {
                                            ddlBag.Items.Add(item);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (sgResult.BaggageIncludedInFare.Split(',')[0] == "15")
                    {
                        lblBag.Text += " 15Kg included in Fare";
                        foreach (DataRow row in baggageData)
                        {
                            if (sgResult.BaggageIncludedInFare.Split(',')[0] == "15")
                            {
                                //if (Convert.ToInt32(row["QtyAvailable"]) > 0 && row["Code"].ToString().Trim() != "EB15")
                                {
                                    ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                    item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                    if (0 == Convert.ToInt32(row["Group"]))
                                    {
                                        if (!ddlBag.Items.Contains(item))
                                        {
                                            ddlBag.Items.Add(item);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                {
                                    ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                    item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                    if (0 == Convert.ToInt32(row["Group"]))
                                    {
                                        if (!ddlBag.Items.Contains(item))
                                        {
                                            ddlBag.Items.Add(item);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (sgResult.BaggageIncludedInFare.Split(',')[0] == "10")
                    {
                        lblBag.Text += " 10Kg included in Fare";
                        foreach (DataRow row in baggageData)
                        {
                            if (sgResult.BaggageIncludedInFare.Split(',')[0] == "10")
                            {
                                //if (Convert.ToInt32(row["QtyAvailable"]) > 0 && (row["Code"].ToString().Trim() != "EB10"))
                                {
                                    ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                    item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                    if (0 == Convert.ToInt32(row["Group"]))
                                    {
                                        if (!ddlBag.Items.Contains(item))
                                        {
                                            ddlBag.Items.Add(item);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (sgResult.BaggageIncludedInFare.Split(',')[0] == "0")
                    {
                        foreach (DataRow row in baggageData)
                        {
                            if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                            {
                                ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                if (0 == Convert.ToInt32(row["Group"]))
                                {
                                    if (!ddlBag.Items.Contains(item))
                                    {
                                        ddlBag.Items.Add(item);
                                    }
                                }
                            }
                        }
                    }
                }
                else //No Default Baggage But Additional Baggage Available
                {
                    if (baggageData.Length > 0)
                    {
                        foreach (DataRow row in baggageData)
                        {
                            if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                            {
                                ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                if (0 == Convert.ToInt32(row["Group"]))
                                {
                                    if (!ddlBag.Items.Contains(item))
                                    {
                                        ddlBag.Items.Add(item);
                                    }
                                }
                            }
                        }
                    }

                }
            }
            if (ddlBag.Visible && ddlBag.Items.Count == 1)
            {
                ddlBag.Visible = false;
            }
        }
    }

    void LoadTBOBaggageInfo(DropDownList ddlOnwardBaggage, DropDownList ddlInwardBaggage, Label lblOnBag, Label lblInBag, SearchType type)
    {
        SearchResult result = (type == SearchType.OneWay ? SessionValues[ONWARD_RESULT] as SearchResult : SessionValues[RETURN_RESULT] as SearchResult);

        TBOAir.AirV10 tbo = new TBOAir.AirV10();
        SourceDetails agentDetails = new SourceDetails();
        if (Settings.LoginInfo.IsOnBehalfOfAgent)
        {
            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["TA"];
            tbo.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
            tbo.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
        }
        else
        {
            agentDetails = Settings.LoginInfo.AgentSourceCredentials["TA"];
            tbo.AgentBaseCurrency = Settings.LoginInfo.Currency;
            tbo.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
        }

        tbo.LoginName = agentDetails.UserID;
        tbo.Password = agentDetails.Password;
        tbo.SessionId = sessionId;
        tbo.AppUserId = (int)Settings.LoginInfo.UserID;

        DataTable dtBaggage = new DataTable();

        if (type == SearchType.OneWay)
        {
            if (SessionValues[ONWARD_BAGGAGE] == null)
            {
                dtBaggage = tbo.GetBaggage(ref result);
            }
            else
            {
                dtBaggage = SessionValues[ONWARD_BAGGAGE] as DataTable;
            }

            SaveInSession(ONWARD_BAGGAGE, dtBaggage);
        }
        else
        {
            if (SessionValues[RETURN_BAGGAGE] == null)
            {
                dtBaggage = tbo.GetBaggage(ref result);
                foreach (DataRow row in dtBaggage.Rows)
                {
                    row["Group"] = 1;
                }
            }
            else
            {
                dtBaggage = SessionValues[RETURN_BAGGAGE] as DataTable;
            }

            SaveInSession(RETURN_BAGGAGE, dtBaggage);
        }



        if (dtBaggage != null)
        {
            DataRow[] outbound = dtBaggage.Select("Group=0");
            DataRow[] inbound = dtBaggage.Select("Group=1");

            if (type == SearchType.OneWay)
            {
                if (outbound != null && outbound.Length > 0)
                {
                    lblBaggage.Text = "Select Baggage:";
                    lblBaggage.Visible = true;
                    lblOnBag.Visible = true;
                    ddlOnwardBaggage.Visible = true;
                    ddlOnwardBaggage.Items.Add(new ListItem("Select Baggage", "0"));
                    lblOnBag.Text += "(" + outbound[0]["Origin"].ToString() + "-" + outbound[0]["Destination"].ToString() + ")";
                    foreach (DataRow row in outbound)
                    {
                        if (row["Weight"].ToString() == "0")
                        {
                            ListItem item = new ListItem(row["Code"].ToString(), row["Price"].ToString());
                            ddlOnwardBaggage.Items.Add(item);
                        }
                        else
                        {
                            ListItem item = new ListItem((row["Weight"].ToString().Contains("kg") ? row["Weight"].ToString() : row["Weight"].ToString() + " Kg"), row["Price"].ToString());
                            ddlOnwardBaggage.Items.Add(item);
                        }
                    }
                }
                else if (outbound != null && outbound.Length > 0 && result.Flights.Length == 1) //One way
                {
                    lblBaggage.Text = "Select Baggage:";
                    lblBaggage.Visible = true;
                    lblOnBag.Visible = true;
                    ddlOnwardBaggage.Visible = true;
                    ddlOnwardBaggage.Items.Add(new ListItem("Select Baggage", "0"));
                    lblOnBag.Text += "(" + outbound[0]["Origin"].ToString() + "-" + outbound[0]["Destination"].ToString() + ")";
                    foreach (DataRow row in outbound)
                    {
                        if (row["Weight"].ToString() == "0")
                        {
                            ListItem item = new ListItem(row["Code"].ToString(), row["Price"].ToString());
                            ddlOnwardBaggage.Items.Add(item);
                        }
                        else
                        {
                            ListItem item = new ListItem((row["Weight"].ToString().Contains("kg") ? row["Weight"].ToString() : row["Weight"].ToString() + " Kg"), row["Price"].ToString());
                            ddlOnwardBaggage.Items.Add(item);
                        }

                    }
                }
                else
                {

                    lblOnBag.Visible = false;
                    ddlOnwardBaggage.Visible = false;
                }
            }

            if (inbound != null && inbound.Length > 0 && type == SearchType.Return)
            {
                lblBaggage.Text = "Select Baggage:";
                lblInBag.Visible = true;
                ddlInwardBaggage.Visible = true;
                lblInBag.Text += "(" + inbound[0]["Origin"].ToString() + "-" + inbound[0]["Destination"].ToString() + ")";
                ddlInwardBaggage.Items.Add(new ListItem("Select Baggage", "0"));
                foreach (DataRow row in inbound)
                {
                    if (row["Weight"].ToString() == "0")
                    {
                        ListItem item = new ListItem(row["Code"].ToString(), row["Price"].ToString());
                        ddlInwardBaggage.Items.Add(item);
                    }
                    else
                    {
                        ListItem item = new ListItem((row["Weight"].ToString().Contains("kg") ? row["Weight"].ToString() : row["Weight"].ToString() + " Kg"), row["Price"].ToString());
                        ddlInwardBaggage.Items.Add(item);
                    }
                }
            }
            else
            {

                lblInBag.Visible = false;
                ddlInwardBaggage.Visible = false;
            }
        }

    }

    // //Code added by Lokesh on 07/10/2016
    void Load6EBaggageInfo(DropDownList ddlBag, Label lblBag, SearchType type)
    {
        try
        {
            SearchResult igResult = null;

            if (type == SearchType.OneWay) //ONWARD
            {
                igResult = SessionValues[ONWARD_RESULT] as SearchResult;
            }
            else if (type == SearchType.Return)//Return
            {
                igResult = SessionValues[RETURN_RESULT] as SearchResult;
            }

            request = Session["FlightRequest"] as SearchRequest;
            CT.BookingEngine.GDS.IndigoAPI Indigo = new CT.BookingEngine.GDS.IndigoAPI();
            SourceDetails agentDetails = new SourceDetails();
            Indigo.SearchByAvailability = true;
            if (igResult != null && igResult.ResultBookingSource == BookingSource.Indigo)
            {
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    Indigo.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    Indigo.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                    Indigo.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                    agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"];

                    Indigo.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"].UserID;
                    Indigo.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"].Password;
                    Indigo.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"].HAP;
                    Indigo.PromoCode = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"].PCC;//Added For Corporate booking Purpose
                }
                else
                {
                    Indigo.AgentBaseCurrency = Settings.LoginInfo.Currency;
                    Indigo.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                    Indigo.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                    agentDetails = Settings.LoginInfo.AgentSourceCredentials["6E"];

                    Indigo.LoginName = Settings.LoginInfo.AgentSourceCredentials["6E"].UserID;
                    Indigo.Password = Settings.LoginInfo.AgentSourceCredentials["6E"].Password;
                    Indigo.AgentDomain = Settings.LoginInfo.AgentSourceCredentials["6E"].HAP;
                    Indigo.PromoCode = Settings.LoginInfo.AgentSourceCredentials["6E"].PCC;//Added For Corporate booking Purpose
                }
                Indigo.AppUserId = Convert.ToInt32(Settings.LoginInfo.UserID);
                Indigo.BookingSourceFlag = "6E";

            }

            else if (igResult != null && igResult.ResultBookingSource == BookingSource.IndigoCorp)
            {
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    Indigo.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    Indigo.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                    Indigo.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                    agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"];

                    Indigo.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"].UserID;
                    Indigo.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"].Password;
                    Indigo.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"].HAP;
                    Indigo.PromoCode = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"].PCC;//Added For Corporate booking Purpose
                }
                else
                {
                    Indigo.AgentBaseCurrency = Settings.LoginInfo.Currency;
                    Indigo.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                    Indigo.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                    agentDetails = Settings.LoginInfo.AgentSourceCredentials["6ECORP"];

                    Indigo.LoginName = Settings.LoginInfo.AgentSourceCredentials["6ECORP"].UserID;
                    Indigo.Password = Settings.LoginInfo.AgentSourceCredentials["6ECORP"].Password;
                    Indigo.AgentDomain = Settings.LoginInfo.AgentSourceCredentials["6ECORP"].HAP;
                    Indigo.PromoCode = Settings.LoginInfo.AgentSourceCredentials["6ECORP"].PCC;//Added For Corporate booking Purpose
                }
                Indigo.AppUserId = Convert.ToInt32(Settings.LoginInfo.UserID);
                Indigo.BookingSourceFlag = "6ECORP";

            }
            //Added by Lokesh
            //For India GST Implementation.
            AgentMaster agentMaster = null;
            if (Settings.LoginInfo != null && Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agentMaster = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                if (agentMaster != null && !string.IsNullOrEmpty(agentMaster.AgentCurrency) && agentMaster.AgentCurrency.Length > 0 && agentMaster.AgentCurrency == "INR")
                {
                    Indigo.CurrencyCode = "INR";
                }
                else
                {
                    Indigo.CurrencyCode = "AED";
                }
                if (agentMaster != null && agentMaster.Country > 0 && Country.GetCountryCodeByCountryId(agentMaster.Country) == "IN" )
                {
                    location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                    if (location != null && location.CountryCode == "IN")
                    {
                        if (!string.IsNullOrEmpty(location.GstNumber) && location.GstNumber.Length > 0)
                        {
                            Indigo.GSTNumber = location.GstNumber;
                            txtTaxRegNo_SG_GST.Text = location.GstNumber;
                            txtTaxRegNo_SG_GST.Enabled = false;
                        }
                        if (!string.IsNullOrEmpty(agency.Name) && agency.Name.Length > 0)
                        {
                            Indigo.GSTCompanyName = agency.Name;
                            txtCompanyName_SG_GST.Text = agency.Name;
                            txtCompanyName_SG_GST.Enabled = false;
                        }
                        if (!string.IsNullOrEmpty(agency.Email2) && agency.Email2.Length > 0)
                        {
                            Indigo.GSTOfficialEmail = agency.Email2.Split(',')[0];
                            txtGSTOfficialEmail.Text = Indigo.GSTOfficialEmail;
                            txtGSTOfficialEmail.Enabled = false;
                        }
                        requiredGSTForSG_6E = true;
                    }
                }
            }
            else
            {
                if (Settings.LoginInfo != null && !string.IsNullOrEmpty(Settings.LoginInfo.Currency) && Settings.LoginInfo.Currency.Length > 0 && Settings.LoginInfo.Currency == "INR")
                {
                    Indigo.CurrencyCode = "INR";
                }
                else
                {
                    Indigo.CurrencyCode = "AED";
                }
                agentMaster = new AgentMaster(Settings.LoginInfo.AgentId);
                if (agentMaster != null && agentMaster.Country > 0 && Country.GetCountryCodeByCountryId(agentMaster.Country) == "IN")
                {
                    location = new LocationMaster(Settings.LoginInfo.LocationID);
                    if (location != null && location.CountryCode == "IN")
                    {
                        if (!string.IsNullOrEmpty(location.GstNumber) && location.GstNumber.Length > 0)
                        {
                            Indigo.GSTNumber = location.GstNumber;
                            txtTaxRegNo_SG_GST.Text = location.GstNumber;
                            txtTaxRegNo_SG_GST.Enabled = false;
                        }
                        if (!string.IsNullOrEmpty(agency.Name) && agency.Name.Length > 0)
                        {
                            Indigo.GSTCompanyName = agency.Name;
                            txtCompanyName_SG_GST.Text = agency.Name;
                            txtCompanyName_SG_GST.Enabled = false;
                        }
                        if (!string.IsNullOrEmpty(agency.Email2) && agency.Email2.Length > 0)
                        {
                            Indigo.GSTOfficialEmail = agency.Email2.Split(',')[0];
                            txtGSTOfficialEmail.Text = Indigo.GSTOfficialEmail;
                            txtGSTOfficialEmail.Enabled = false;
                        }
                        requiredGSTForSG_6E = true;
                    }
                }
            }
            if (Session["sessionId"] != null)
            {
                Indigo.SessionId = Session["sessionId"].ToString();
            }
            else
            {
                Indigo.SessionId = Guid.NewGuid().ToString();
            }
            Indigo.AppUserId = Convert.ToInt32(Settings.LoginInfo.UserID);

            DataTable dtBaggageInfo = null;
            if (SessionValues[ONWARD_BAGGAGE] == null && type == SearchType.OneWay)//ONWARD
            {

                dtBaggageInfo = Indigo.GetAvailableSSR(igResult);
                SaveInSession(ONWARD_BAGGAGE, dtBaggageInfo);

            }
            else if (SessionValues[ONWARD_BAGGAGE] != null && type == SearchType.OneWay)
            {
                dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
            }
            else if (SessionValues[RETURN_BAGGAGE] == null && type == SearchType.Return)//Return
            {
                dtBaggageInfo = Indigo.GetAvailableSSR(igResult);
                SaveInSession(RETURN_BAGGAGE, dtBaggageInfo);
            }
            else if (SessionValues[RETURN_BAGGAGE] != null && type == SearchType.Return)//Return
            {
                dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;

            }
            //if (dtBaggageInfo != null)
            //{
            //    DataRow[] bags = dtBaggageInfo.Select("Group=0 AND (Code ='XBPA' OR Code ='XBPB' OR Code ='XBPC' OR Code ='XBPD')");
            //}
            if (dtBaggageInfo != null && dtBaggageInfo.Rows.Count > 0)
            {

                dtBaggageInfo = dtBaggageInfo.DefaultView.ToTable(true, new string[0]);
                DataRow[] baggageData = new DataRow[0];
                //DataRow[] returnBaggage = new DataRow[0];
                //returnBaggage = dtBaggageInfo.Select("Group=0 AND (Code ='XBPA' OR Code ='XBPB' OR Code ='XBPC' OR Code ='XBPD')");
                baggageData = dtBaggageInfo.Select("Group=0 AND (Code ='XBPA' OR Code ='XBPB' OR Code ='XBPC' OR Code ='XBPD' OR Code ='XBPE' OR Code ='IXBA' OR Code ='IXBB' OR Code ='IXBC')");

                if (baggageData != null)
                {
                    lblBaggage.Visible = true;
                    lblBag.Visible = true;
                    ddlBag.Visible = true;
                    if (type == SearchType.OneWay)
                    {
                        ddlOnwardBaggage.Visible = true;
                    }
                    else
                    {
                        ddlInwardBaggage.Visible = true;
                    }

                    //Case:1: If  Additional Baggage Available.
                    if (baggageData.Length > 0)
                    {
                        //Case 1.1:  Default baggage Available
                        if (igResult != null && !string.IsNullOrEmpty(igResult.BaggageIncludedInFare) && Convert.ToInt32(igResult.BaggageIncludedInFare.Split(',')[0]) > 0)
                        {
                            ListItem item = new ListItem("Upgrade Baggage", "0");
                            ddlBag.Items.Add(item);
                        }
                        else  //Case 1.2: no  Default baggage Available
                        {
                            ListItem item = new ListItem("No Bag", "0");
                            ddlBag.Items.Add(item);
                        }
                    }

                    //Case2: If no  additional baggage available.
                    if (baggageData.Length <= 0)
                    {
                        ListItem item = new ListItem("No Bag", "0");
                        ddlBag.Items.Add(item);
                    }
                    if (igResult != null && !string.IsNullOrEmpty(igResult.BaggageIncludedInFare))
                    {
                        if (igResult.BaggageIncludedInFare.Split(',')[0] == "20")
                        {
                            lblBag.Text += " 20Kg included in Fare";
                            foreach (DataRow row in baggageData)
                            {
                                if (igResult.BaggageIncludedInFare.Split(',')[0] == "20")
                                {
                                    //if (Convert.ToInt32(row["QtyAvailable"]) > 0 && (row["Code"].ToString().Trim() != "EB20"))
                                    {
                                        ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString()+"-"+row["Code"].ToString().Trim());
                                        item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                        if (0 == Convert.ToInt32(row["Group"]))
                                        {
                                            if (!ddlBag.Items.Contains(item))
                                            {
                                                ddlBag.Items.Add(item);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                    {
                                        ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                        item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                        if (0 == Convert.ToInt32(row["Group"]))
                                        {
                                            if (!ddlBag.Items.Contains(item))
                                            {
                                                ddlBag.Items.Add(item);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (igResult.BaggageIncludedInFare.Split(',')[0] == "30")
                        {
                            lblBag.Text += " 30Kg included in Fare";
                            foreach (DataRow row in baggageData)
                            {
                                if (igResult.BaggageIncludedInFare.Split(',')[0] == "30")
                                {
                                    if (Convert.ToInt32(row["QtyAvailable"]) > 0 && row["Code"].ToString().Trim() != "EB30")
                                    {
                                        ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                        item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                        if (0 == Convert.ToInt32(row["Group"]))
                                        {
                                            if (!ddlBag.Items.Contains(item))
                                            {
                                                ddlBag.Items.Add(item);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                    {
                                        ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                        item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                        if (0 == Convert.ToInt32(row["Group"]))
                                        {

                                            if (!ddlBag.Items.Contains(item))
                                            {
                                                ddlBag.Items.Add(item);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (igResult.BaggageIncludedInFare.Split(',')[0] == "15")
                        {
                            lblBag.Text += " 15Kg included in Fare";
                            foreach (DataRow row in baggageData)
                            {
                                if (igResult.BaggageIncludedInFare.Split(',')[0] == "15")
                                {
                                    //if (Convert.ToInt32(row["QtyAvailable"]) > 0 && row["Code"].ToString().Trim() != "EB15")
                                    {
                                        ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                        item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                        if (0 == Convert.ToInt32(row["Group"]))
                                        {
                                            if (!ddlBag.Items.Contains(item))
                                            {
                                                ddlBag.Items.Add(item);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                    {
                                        ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                        item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                        if (0 == Convert.ToInt32(row["Group"]))
                                        {
                                            if (!ddlBag.Items.Contains(item))
                                            {
                                                ddlBag.Items.Add(item);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (igResult.BaggageIncludedInFare.Split(',')[0] == "10")
                        {
                            lblBag.Text += " 10Kg included in Fare";
                            foreach (DataRow row in baggageData)
                            {
                                if (igResult.BaggageIncludedInFare.Split(',')[0] == "10")
                                {
                                    //if (Convert.ToInt32(row["QtyAvailable"]) > 0 && (row["Code"].ToString().Trim() != "EB10"))
                                    {
                                        ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                        item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                        if (0 == Convert.ToInt32(row["Group"]))
                                        {
                                            if (!ddlBag.Items.Contains(item))
                                            {
                                                ddlBag.Items.Add(item);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (igResult.BaggageIncludedInFare.Split(',')[0] == "0")
                        {
                            foreach (DataRow row in baggageData)
                            {
                                if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                {
                                    ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                    item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                    if (0 == Convert.ToInt32(row["Group"]))
                                    {
                                        if (!ddlBag.Items.Contains(item))
                                        {
                                            ddlBag.Items.Add(item);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else //No Default Baggage But Additional Baggage Available
                    {
                        if (baggageData.Length > 0)
                        {
                            foreach (DataRow row in baggageData)
                            {
                                if (Convert.ToInt32(row["QtyAvailable"]) > 0)
                                {
                                    ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                                    item.Attributes.Add("Code", row["Code"].ToString().Trim());

                                    if (0 == Convert.ToInt32(row["Group"]))
                                    {
                                        if (!ddlBag.Items.Contains(item))
                                        {
                                            ddlBag.Items.Add(item);
                                        }
                                    }
                                }
                            }
                        }

                    }
                    if (ddlBag.Visible && ddlBag.Items.Count == 1)
                    {
                        ddlBag.Visible = false;
                    }
                }

            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }


    }

    // Air India Express Baggage Code -- Written by Lokesh on 08/02/2017
    void LoadIXBaggageInfo(DropDownList ddlOnwardBaggage, DropDownList ddlInwardBaggage, Label lblOnBag, Label lblInBag, string baggageIncludedInFare)
    {
        #region BaggageCases
        //The following are the baggage cases available for AirIndiaExpress
        //****************Onward***********************************
        //Case -1: "ADT 30kgs/CHD 30kgs/INF 10Kgs"
        //Case-2 : "0"
        //**********************************************************

        //****************Return************************************
        //Case -1: "ADT 30kgs/CHD 30kgs/INF 10Kgs" ,"ADT 30kgs/CHD 30kgs/INF 10Kgs" 
        //Case-2 : "0" ,"ADT 30kgs/CHD 30kgs/INF 10Kgs".
        //Case-3 : "ADT 30kgs/CHD 30kgs/INF 10Kgs" ,"0".
        //Case-4 : "0" ,"0".
        //**********************************************************


        string defaultBaggageAIE = string.Empty; //Default baggage for AirIndiaExpress from fareQuoteResponse
        string defaultBaggageOnwardAIE = string.Empty;//Default Baggage Onward
        string defaultBaggageWeightOnwardAIE = string.Empty;//Default baggage weight onward 
        string defaultBaggageReturnAIE = string.Empty;//Default Baggage Return
        string defaultBaggageWeightReturnAIE = string.Empty;//Default baggage weight return


        defaultBaggageAIE = baggageIncludedInFare;
        defaultBaggageOnwardAIE = defaultBaggageAIE.Split(',')[0];

        if (defaultBaggageOnwardAIE.ToLower().Contains("free baggage"))
        {
            defaultBaggageWeightOnwardAIE = defaultBaggageOnwardAIE.Split(' ')[2].Substring(0, 2);
        }
        else
        {
            defaultBaggageWeightOnwardAIE = defaultBaggageOnwardAIE;
        }


        if (defaultBaggageAIE.Split(',').Length > 1 && request.Type == SearchType.Return)
        {
            defaultBaggageReturnAIE = defaultBaggageAIE.Split(',')[1];
        }

        if (!string.IsNullOrEmpty(defaultBaggageReturnAIE))
        {

            if (defaultBaggageReturnAIE.ToLower().Contains("free baggage"))
            {
                defaultBaggageWeightReturnAIE = defaultBaggageReturnAIE.Split(' ')[2].Substring(0, 2);
            }
            else
            {
                defaultBaggageWeightReturnAIE = defaultBaggageReturnAIE;
            }
        }
        #endregion

        if (onwardResult == null)
        {
            onwardResult = SessionValues[ONWARD_RESULT] as SearchResult;
        }

        List<int> onwardLFIDS = new List<int>(); //Onward logical flight id's
        List<int> returnLFIDS = new List<int>();//return logical flight id's

        for (int i = 0; i < onwardResult.Flights.Length; i++)
        {
            for (int j = 0; j < onwardResult.Flights[i].Length; j++)
            {
                if (i == 0)
                {
                    onwardLFIDS.Add(Convert.ToInt32(onwardResult.Flights[i][j].UapiSegmentRefKey));
                }
                else
                {
                    returnLFIDS.Add(Convert.ToInt32(onwardResult.Flights[i][j].UapiSegmentRefKey));
                }
            }
        }

        CT.BookingEngine.GDS.AirIndiaExpressApi airIndiaExpress = new CT.BookingEngine.GDS.AirIndiaExpressApi();
        SourceDetails agentDetails = new SourceDetails();
        if (Settings.LoginInfo.IsOnBehalfOfAgent)
        {
            airIndiaExpress.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
            airIndiaExpress.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
            airIndiaExpress.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["IX"];
        }
        else
        {
            airIndiaExpress.AgentBaseCurrency = Settings.LoginInfo.Currency;
            airIndiaExpress.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
            airIndiaExpress.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
            agentDetails = Settings.LoginInfo.AgentSourceCredentials["IX"];
        }
        airIndiaExpress.LogonId = agentDetails.UserID;
        airIndiaExpress.Password = agentDetails.Password;
        if (Session["sessionId"] != null)
        {
            airIndiaExpress.SessionId = Session["sessionId"].ToString();
        }
        else
        {
            airIndiaExpress.SessionId = Guid.NewGuid().ToString();
        }

        DataTable dtBaggageInfo = null;
        //Load from session or get from AirIndia Express Api
        if (SessionValues[ONWARD_BAGGAGE] == null)
        {
            dtBaggageInfo = airIndiaExpress.GetBaggageServicesQuotes(onwardResult, onwardResult.GUID);
        }
        else
        {
            dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
        }

        if (dtBaggageInfo != null && dtBaggageInfo.Rows.Count > 0)
        {
            SaveInSession(ONWARD_BAGGAGE, dtBaggageInfo);
            dtBaggageInfo = dtBaggageInfo.DefaultView.ToTable(true, new string[0]);
            lblBaggage.Visible = true;

            lblOnBag.Visible = true;
            ddlOnwardBaggage.Visible = true;

            if (request.Type == SearchType.Return) //SearchType:Return
            {
                ddlInwardBaggage.Visible = true;
                lblInBag.Visible = true;

                if (Convert.ToInt32(defaultBaggageWeightOnwardAIE) >= 0)
                {
                    ListItem item = new ListItem("Upgrade Baggage", "0");
                    ddlOnwardBaggage.Items.Add(item);
                }
                else
                {
                    lblOnBag.Text = "Onward: Max baggage included (20 kg).";
                    ddlOnwardBaggage.Visible = false;
                }

                if (Convert.ToInt32(defaultBaggageWeightReturnAIE) >= 0)
                {
                    ListItem item = new ListItem("Upgrade Baggage", "0");
                    ddlInwardBaggage.Items.Add(item);
                }
                else
                {
                    lblInBag.Text = "Return: Max baggage included (20Kg).";
                    ddlInwardBaggage.Visible = false;
                }
            }
            else
            {
                ddlInwardBaggage.Visible = false;
                lblInBag.Visible = false;
                if (Convert.ToInt32(defaultBaggageWeightOnwardAIE) >= 0)
                {
                    ListItem item = new ListItem("Upgrade Baggage", "0");
                    ddlOnwardBaggage.Items.Add(item);
                }
                else
                {
                    lblOnBag.Text = "Onward: Max baggage included (20 kg).";
                    ddlOnwardBaggage.Visible = false;
                }
            }


            switch (defaultBaggageWeightOnwardAIE)
            {

                case "10":
                    lblOnBag.Text += " 10Kg included in Fare";
                    break;
                case "20":
                    lblOnBag.Text += " 20Kg included in Fare";
                    break;
                case "30":
                    lblOnBag.Text += " 30Kg included in Fare";
                    break;
                case "40":
                    lblOnBag.Text += " 40Kg included in Fare";
                    break;
            }




            foreach (DataRow row in dtBaggageInfo.Rows)
            {
                if (Convert.ToInt32(row["QtyAvailable"]) > 0 && row["CodeType"].ToString().Contains("XB"))
                {
                    //Onward Additional bagage List items formation.
                    //Baggage List Item : Description - Baggage Code
                    //Example : 5 KG EXTRA BAGGAGE - XB05
                    //Need to pass the Baggage Code in GetSummaryPNR Request.
                    //Recent change:Baggage description now includes the origin city.
                    //row["Description"] = 5 KG EXTRA BAGGAGE - FROM INDIA
                    //So we will trim the origin city and form the list item as "Description - Baggage Code"

                    ListItem item = new ListItem(row["Description"].ToString().Split('-')[0].Trim() + "-" + row["CodeType"].ToString().Trim(), row["Amount"].ToString());
                    item.Attributes.Add("CategoryId", row["CategoryId"].ToString());
                    if (onwardLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                    {
                        if (!ddlOnwardBaggage.Items.Contains(item))
                        {
                            ddlOnwardBaggage.Items.Add(item);
                        }
                    }
                }
            }
            if (request.Type == SearchType.Return)
            {

                switch (defaultBaggageWeightReturnAIE)
                {
                    case "10":
                        lblInBag.Text += " 10Kg included in Fare";
                        break;
                    case "20":
                        lblInBag.Text += " 20Kg included in Fare";
                        break;
                    case "30":
                        lblInBag.Text += " 30Kg included in Fare";
                        break;
                    case "40":
                        lblInBag.Text += " 40Kg included in Fare";
                        break;
                }



                foreach (DataRow row in dtBaggageInfo.Rows)
                {
                    if (Convert.ToInt32(row["QtyAvailable"]) > 0 && row["CodeType"].ToString().Contains("XB"))
                    {
                        //Return additional bagage list items formation.
                        //Baggage List Item : Description - Baggage Code
                        //Example : 5 KG EXTRA BAGGAGE - XB05
                        //Need to pass the Baggage Code in GetSummaryPNR Request.
                        //Recent change:Baggage description now includes the origin city.
                        //row["Description"] = 5 KG EXTRA BAGGAGE - FROM INDIA
                        //So we will trim the origin city and form the list item as "Description - Baggage Code"

                        ListItem item = new ListItem(row["Description"].ToString().Split('-')[0].Trim() + "-" + row["CodeType"].ToString().Trim(), row["Amount"].ToString());
                        item.Attributes.Add("CategoryId", row["CategoryId"].ToString());
                        if (returnLFIDS.Exists(delegate (int lfid) { return lfid == Convert.ToInt32(row["LogicalFlightID"]); }))
                        {
                            if (!ddlInwardBaggage.Items.Contains(item))
                            {
                                ddlInwardBaggage.Items.Add(item);
                            }
                        }
                    }
                }
            }





            if (ddlOnwardBaggage.Visible && ddlOnwardBaggage.Items.Count == 1)
            {
                lblBaggage.Visible = false;
                ddlOnwardBaggage.Visible = false;
            }


            if (ddlInwardBaggage.Visible && ddlInwardBaggage.Items.Count == 1)
            {
                lblBaggage.Visible = false;
                ddlInwardBaggage.Visible = false;
            }
        }
        else
        {
            Response.Redirect("HotelSearch.aspx?source=Flight");
        }
    }

    protected void dlAddtionalPax_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            FlightPassenger pax = e.Item.DataItem as FlightPassenger;
            DropDownList ddlCountry = e.Item.FindControl("ddlCountry") as DropDownList;
            DropDownList ddlNationality = e.Item.FindControl("ddlNationality") as DropDownList;
            DropDownList ddlDay = e.Item.FindControl("ddlDay") as DropDownList;
            DropDownList ddlPEDay = e.Item.FindControl("ddlPEDay") as DropDownList;
            DropDownList ddlYear = e.Item.FindControl("ddlYear") as DropDownList;
            DropDownList ddlPEYear = e.Item.FindControl("ddlPEYear") as DropDownList;
            DropDownList ddlInwardBaggage = e.Item.FindControl("ddlInwardBaggage") as DropDownList;
            DropDownList ddlOnwardBaggage = e.Item.FindControl("ddlOnwardBaggage") as DropDownList;

            DropDownList ddlInwardMeal = e.Item.FindControl("ddlInwardMeal") as DropDownList;
            DropDownList ddlOnwardMeal = e.Item.FindControl("ddlOnwardMeal") as DropDownList;

            DropDownList ddlPaxTitle = e.Item.FindControl("ddlPaxTitle") as DropDownList;
            Label lblPaxType = e.Item.FindControl("lblPaxType") as Label;
            Label lblBaggage = e.Item.FindControl("lblBaggage") as Label;
            Label lblOnBag = e.Item.FindControl("lblOnBag") as Label;
            Label lblInBag = e.Item.FindControl("lblInBag") as Label;

            Label lblOnMeal = e.Item.FindControl("lblOnMeal") as Label;
            Label lblInMeal = e.Item.FindControl("lblInMeal") as Label;

            //Added by lokesh on 29 - Mar - 2018 for G9 Source only if the customer is travelling from India.
            Label lblG9BaggageAlertAddPax = e.Item.FindControl("lblG9BaggageAlertAddPax") as Label;
            //Label lblOnBagWrapperAddPax = e.Item.FindControl("lblOnBagWrapperAddPax") as Label;
            HtmlGenericControl lblOnBagWrapperAddPax = e.Item.FindControl("lblOnBagWrapperAddPax") as HtmlGenericControl;

            HtmlGenericControl addPaxOnwardMeal = e.Item.FindControl("addPaxOnwardMeal") as HtmlGenericControl;
            HtmlGenericControl addPaxReturnMeal = e.Item.FindControl("addPaxReturnMeal") as HtmlGenericControl;

            HtmlGenericControl addPaxOnBag = e.Item.FindControl("addPaxOnBag") as HtmlGenericControl;
            HtmlGenericControl addPaxRetBag = e.Item.FindControl("addPaxRetBag") as HtmlGenericControl;
            HtmlGenericControl addPaxlblMeal = e.Item.FindControl("addPaxlblMeal") as HtmlGenericControl;

            ddlCountry.DataSource = Country.GetCountryList();
            ddlCountry.DataTextField = "Key";
            ddlCountry.DataValueField = "Value";
            ddlCountry.DataBind();

            ddlNationality.DataSource = Country.GetNationalityList();
            ddlNationality.DataTextField = "Key";
            ddlNationality.DataValueField = "Value";
            ddlNationality.DataBind();

            AssignCountryAndNationality(ddlCountry, ddlNationality);

            for (int i = 1; i <= 31; i++)
            {
                ddlDay.Items.Add(i.ToString());
                ddlPEDay.Items.Add(i.ToString());
            }
            for (int j = 1920; j < (1940 + 90); j++)
            {
                ddlYear.Items.Add(j.ToString());
            }
            for (int i = DateTime.Now.Year; i <= (DateTime.Now.Year + 50); i++)
            {
                ddlPEYear.Items.Add(i.ToString());
            }

            onwardResult = SessionValues[ONWARD_RESULT] as SearchResult;
            returnResult = SessionValues[RETURN_RESULT] as SearchResult;
            if (onwardResult != null)
            {
                if (onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp || onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp || onwardResult.ResultBookingSource == BookingSource.AirArabia)
                {
                    //Added contion for to Restrict meal option for spice jet Business class
                    if ((onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp) && onwardResult.Flights[0][0].SegmentFareType.Contains("Business")) //paranthesis added in condition 
                        addPaxOnwardMeal.Visible = false;
                   else
                         addPaxOnwardMeal.Visible = true;
                    
                }
                else
                {
                    addPaxOnwardMeal.Visible = false;
                }
            }
            if (returnResult != null)
            {
                if (returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.IndigoCorp || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp || returnResult.ResultBookingSource == BookingSource.AirArabia)
                { 
                    //Added contion for to Restrict meal option for spice jet Business class
                    if ((returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp) && returnResult.Flights[0][0].SegmentFareType.Contains("Business")) //paranthesis added in condition 
                        addPaxReturnMeal.Visible = false;
                    else 
                        addPaxReturnMeal.Visible = true;
                    
                }
                else
                {
                    addPaxReturnMeal.Visible = false;
                }
            }

            //Added by lokesh on 27-Mar-2018
            //Applicable to only AirArabiaSource
            //Display the below label only if the customer is travelling from india. 
            if (onwardResult != null && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia && onwardResult.Flights[0][0].Origin.CountryCode == "IN")
            {
                lblG9BaggageAlertAddPax.Visible = true;
            }
            else
            {
                lblG9BaggageAlertAddPax.Visible = false;
            }

            if (returnResult != null && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia && returnResult.Flights[0][0].Origin.CountryCode == "IN")
            {
                lblG9BaggageAlertAddPax.Visible = true;
            }
            else
            {
                lblG9BaggageAlertAddPax.Visible = false;
            }

            if (onwardResult != null && onwardResult.ResultBookingSource != BookingSource.FlightInventory && (onwardResult.ResultBookingSource == BookingSource.AirArabia || onwardResult.ResultBookingSource == BookingSource.FlyDubai || onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.TBOAir || onwardResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl || onwardResult.IsLCC || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.IndigoCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp) && lblPaxType.Text != "Infant")
            {
                lblOnBagWrapperAddPax.Visible = true;// modified by ziya
                lblBaggage.Visible = true;
                if (request.Type == SearchType.Return)
                {
                    lblInBag.Visible = true;
                }
                lblOnBag.Visible = true;
                if (onwardResult.ResultBookingSource == BookingSource.AirArabia)
                {
                    LoadBaggageInfo(SessionValues.ContainsKey(ONWARD_BAGGAGE) && SessionValues[ONWARD_BAGGAGE] != null ? (DataTable)SessionValues[ONWARD_BAGGAGE] :
                        clsMSE.GetBaggageInfo(onwardResult, sessionId), ddlOnwardBaggage, ONWARD_BAGGAGE, lblOnBag);
                    LoadG9MealInfo(SessionValues.ContainsKey(ONWARD_G9MEALS) && SessionValues[ONWARD_G9MEALS] != null ? (DataTable)SessionValues[ONWARD_G9MEALS] :
                       clsMSE.GetMealInfo(onwardResult, sessionId), ddlOnwardMeal, ONWARD_G9MEALS, lblOnMeal);
                }
                if (onwardResult.ResultBookingSource == BookingSource.TBOAir)
                {
                    if (onwardResult.IsLCC)
                    {
                        LoadTBOBaggageInfo(ddlOnwardBaggage, ddlInwardBaggage, lblOnBag, lblInBag, SearchType.OneWay);
                        if (!lblOnBag.Visible)
                        {
                            lblBaggage.Visible = false;
                        }
                    }
                    else
                    {
                        lblOnBagWrapperAddPax.Visible = false;
                        lblBaggage.Visible = false;
                        lblOnBag.Visible = false;
                        lblInBag.Visible = false;
                    }
                }
                int baggageIncluded = 0;
                if (onwardResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl)
                {
                    if (!String.IsNullOrEmpty(onwardResult.BaggageIncludedInFare))
                    {
                        baggageIncluded = AssignAirIndiaExpressDefaultBaggage(onwardResult.BaggageIncludedInFare);
                    }
                    if (request.Type == SearchType.OneWay || request.Type == SearchType.Return)
                    {
                        LoadIXBaggageInfo(ddlOnwardBaggage, ddlInwardBaggage, lblOnBag, lblInBag, onwardResult.BaggageIncludedInFare);
                        if (!lblOnBag.Visible)
                        {
                            lblBaggage.Visible = false;
                        }
                    }
                    else
                    {
                        lblOnBag.Text = "Onward: Max baggage included (20Kg)";

                    }
                }

                if (onwardResult.ResultBookingSource == BookingSource.FlyDubai)
                {
                    if (request.Type == SearchType.Return)
                    {
                        baggageIncluded = Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split(',')[0]) + Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split(',')[1]);
                    }
                    else
                    {
                        baggageIncluded = Convert.ToInt32(onwardResult.BaggageIncludedInFare);
                    }

                    if (request.Type == SearchType.OneWay && baggageIncluded < 40 || request.Type == SearchType.Return && baggageIncluded < 80)
                    {
                        LoadFZBaggageInfo(ddlOnwardBaggage, ddlInwardBaggage, lblOnBag, lblInBag);
                        if (!lblOnBag.Visible)
                        {
                            lblBaggage.Visible = false;
                        }
                    }
                    else
                    {
                        lblOnBag.Text = "Onward: Max baggage included (40Kg)";

                    }
                }

                if (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp)
                {
                    LoadSGBaggageInfo(ddlOnwardBaggage, lblOnBag, 0);
                    if (SessionValues[ONWARD_BAGGAGE] != null)
                    {
                        DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                        if (!onwardResult.Flights[0][0].SegmentFareType.Contains("Business")) //Restricted Meal option fro Spice Jet Business class
                        {
                            LoadSGMealInfo(ddlOnwardMeal, lblOnMeal, dtBaggageInfo);
                        }
                    }
                    if (!lblOnBag.Visible)
                    {
                        lblBaggage.Visible = false;
                    }
                }

                if ((onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp) && !onwardResult.IsSpecialRoundTrip)
                {
                    Load6EBaggageInfo(ddlOnwardBaggage, lblOnBag, SearchType.OneWay);
                    if (SessionValues[ONWARD_BAGGAGE] != null)
                    {
                        DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                        Load6EMealInfo(ddlOnwardMeal, ddlInwardMeal, lblOnMeal, lblInMeal, dtBaggageInfo, SearchType.OneWay);
                    }
                    if (!lblOnBag.Visible)
                    {
                        lblBaggage.Visible = false;
                    }

                }

                if (onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp)
                {
                    LoadG8BaggageInfo(ddlOnwardBaggage, lblOnBag, 0);
                    if (SessionValues[ONWARD_BAGGAGE] != null)
                    {
                        DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                        LoadG8MealInfo(ddlOnwardMeal, lblOnMeal, dtBaggageInfo, onwardResult.ResultBookingSource);
                    }
                    if (!lblOnBag.Visible)
                    {
                        lblBaggage.Visible = false;
                    }
                }

            }

            if (returnResult != null && returnResult.ResultBookingSource != BookingSource.FlightInventory && (returnResult.ResultBookingSource == BookingSource.AirArabia || returnResult.ResultBookingSource == BookingSource.FlyDubai || returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.TBOAir || returnResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl || returnResult.IsLCC || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.IndigoCorp || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp) && lblPaxType.Text != "Infant")
            {
                lblOnBagWrapperAddPax.Visible = true;
                lblBaggage.Visible = true;
                lblInBag.Visible = true;
                if (returnResult.ResultBookingSource == BookingSource.AirArabia)
                {
                    LoadBaggageInfo(SessionValues.ContainsKey(RETURN_BAGGAGE) && SessionValues[RETURN_BAGGAGE] != null ? (DataTable)SessionValues[RETURN_BAGGAGE] :
                        clsMSE.GetBaggageInfo(returnResult, sessionId), ddlInwardBaggage, RETURN_BAGGAGE, lblInBag);
                    LoadG9MealInfo(SessionValues.ContainsKey(RETURN_G9MEALS) && SessionValues[RETURN_G9MEALS] != null ? (DataTable)SessionValues[RETURN_G9MEALS] :
                      clsMSE.GetMealInfo(returnResult, sessionId), ddlInwardMeal, RETURN_G9MEALS, lblInMeal);
                }
                if (returnResult.ResultBookingSource == BookingSource.TBOAir)
                {
                    if (returnResult.IsLCC)
                    {
                        LoadTBOBaggageInfo(ddlOnwardBaggage, ddlInwardBaggage, lblOnBag, lblInBag, SearchType.Return);
                        if (!lblInBag.Visible)
                        {
                            lblBaggage.Visible = false;
                        }
                    }
                    else if (!onwardResult.IsLCC)
                    {
                        lblOnBagWrapperAddPax.Visible = false;
                        lblBaggage.Visible = false;
                        lblOnBag.Visible = false;
                        lblInBag.Visible = false;
                    }
                }
                int baggageIncluded = 0;
                if (returnResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl)
                {
                    if (!String.IsNullOrEmpty(returnResult.BaggageIncludedInFare))
                    {
                        baggageIncluded = AssignAirIndiaExpressDefaultBaggage(returnResult.BaggageIncludedInFare);
                    }
                    if (request.Type == SearchType.OneWay || request.Type == SearchType.Return)
                    {
                        LoadIXBaggageInfo(ddlOnwardBaggage, ddlInwardBaggage, lblOnBag, lblInBag, returnResult.BaggageIncludedInFare);
                        if (!lblInBag.Visible)
                        {
                            lblBaggage.Visible = false;
                        }
                    }
                    else
                    {
                        lblInBag.Text = "Return: Max baggage included (20Kg)";
                    }
                }

                if (returnResult.ResultBookingSource == BookingSource.FlyDubai)
                {
                    if (request.Type == SearchType.Return)
                    {
                        baggageIncluded = Convert.ToInt32(returnResult.BaggageIncludedInFare.Split(',')[0]) + Convert.ToInt32(returnResult.BaggageIncludedInFare.Split(',')[1]);
                    }
                    else
                    {
                        baggageIncluded = Convert.ToInt32(returnResult.BaggageIncludedInFare);
                    }

                    if (request.Type == SearchType.OneWay && baggageIncluded < 40 || request.Type == SearchType.Return && baggageIncluded < 80)
                    {
                        LoadFZBaggageInfo(ddlOnwardBaggage, ddlInwardBaggage, lblOnBag, lblInBag);
                        if (!lblOnBag.Visible)
                        {
                            lblBaggage.Visible = false;
                        }
                    }
                    else
                    {
                        lblInBag.Text = "Return: Max baggage included (40Kg)";
                    }
                }

                if (returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp)
                {
                    LoadSGBaggageInfo(ddlInwardBaggage, lblInBag, 1);
                    if (SessionValues[RETURN_BAGGAGE] != null)
                    {
                        DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                        if (!returnResult.Flights[0][0].SegmentFareType.Contains("Business")) //Restricted Meal option for Spice Jet business class 
                        {
                            LoadSGMealInfo(ddlInwardMeal, lblInMeal, dtBaggageInfo);
                        }
                    }
                    if (!lblInBag.Visible)
                    {
                        lblBaggage.Visible = false;
                    }
                }

                if ((returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.IndigoCorp) && !returnResult.IsSpecialRoundTrip)
                {
                    Load6EBaggageInfo(ddlInwardBaggage, lblInBag, SearchType.Return);
                    if (SessionValues[RETURN_BAGGAGE] != null)
                    {
                        DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                        Load6EMealInfo(ddlOnwardMeal, ddlInwardMeal, lblOnMeal, lblInMeal, dtBaggageInfo, SearchType.Return);
                    }
                    if (!lblInBag.Visible)
                    {
                        lblBaggage.Visible = false;
                    }
                }


                //If both onward and return results are special round trip then get the baggage ,meals in one single request
                if ((onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp) && (returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.IndigoCorp) && onwardResult.IsSpecialRoundTrip && returnResult.IsSpecialRoundTrip)
                {
                    Get6EAvailableSSR(ddlOnwardBaggage, lblOnBag, ddlInwardBaggage, lblInBag, onwardResult, returnResult);
                    if (SessionValues[ONWARD_BAGGAGE] != null && SessionValues[RETURN_BAGGAGE] != null)
                    {
                        DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                        Load6EMealInfo(ddlOnwardMeal, ddlInwardMeal, lblOnMeal, lblInMeal, dtBaggageInfo);
                    }
                }

                if (returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp)
                {
                    LoadG8BaggageInfo(ddlInwardBaggage, lblInBag, 1);
                    if (SessionValues[RETURN_BAGGAGE] != null)
                    {
                        DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                        LoadG8MealInfo(ddlInwardMeal, lblInMeal, dtBaggageInfo, returnResult.ResultBookingSource);
                    }
                    if (!lblInBag.Visible)
                    {
                        lblBaggage.Visible = false;
                    }
                }

            }
            ddlPaxTitle.Attributes.Add("onchange", "TitleChanged(this.id," + e.Item.ItemIndex + ");");

            //For HandBaggage No additional baggage will be available
            if (onwardResult != null && !string.IsNullOrEmpty(onwardResult.FareType) && onwardResult.FareType.Contains("HandBaggage Only"))// For Hand Baggage , Addnl baggae is not allowed 
            {
                ddlOnwardBaggage.Visible = false;
            }
            if (returnResult != null && !string.IsNullOrEmpty(returnResult.FareType) && returnResult.FareType.Contains("HandBaggage Only"))// For Hand Baggage , Addnl baggae is not allowed
            {
                ddlInwardBaggage.Visible = false;
            }

            //Do not show the baggage div's if there are no items in the baggage && Meal dropdowns for Additional Passenger.
            if (ddlOnwardBaggage != null)//Onward Baggage Dropdown
            {
                if (!ddlOnwardBaggage.Visible)
                {
                    addPaxOnBag.Visible = false;
                }
            }
            if (ddlInwardBaggage != null)//Return Baggage Dropdown
            {
                if (!ddlInwardBaggage.Visible)
                {
                    addPaxRetBag.Visible = false;
                }
            }
            //Meal Drop downs only for LCC's.
            if ((onwardResult != null && onwardResult.IsLCC) || (returnResult != null && returnResult.IsLCC))
            {
                if (ddlOnwardMeal != null)//Onward Meal Dropdown
                {
                    if (!ddlOnwardMeal.Visible)
                    {
                        addPaxOnwardMeal.Visible = false;
                    }
                }
                if (ddlInwardMeal != null)//Return Meal Dropdown
                {
                    if (!ddlInwardMeal.Visible)
                    {
                        addPaxReturnMeal.Visible = false;
                    }
                }
            }
            //Do not display the select baggage label
            if (!ddlOnwardBaggage.Visible && !ddlInwardBaggage.Visible && lblBaggage != null)
            {
                lblBaggage.Visible = false;
            }
            if (!ddlInwardMeal.Visible && !ddlOnwardMeal.Visible && addPaxlblMeal != null)
            {
                addPaxlblMeal.Visible = false;
            }
        }
    }

    /// <summary>
    /// Create flex fields in the data list for additional pax
    /// </summary>
    /// <param name="dtFlexFields"></param>
    protected void BindAddPaxFlex(DataTable dtFlexFields)
    {
        foreach (DataListItem item in dlAdditionalPax.Items)
        {
            HtmlGenericControl tblFlexFieldsAd = item.FindControl("tblFlexFieldsAd") as HtmlGenericControl;

            int i = 0;
            foreach (DataRow row in dtFlexFields.Rows)
            {
                HtmlGenericControl divlblFlex1 = new HtmlGenericControl("Div");
                divlblFlex1.Attributes.Add("class", "col-md-3");

                HiddenField hdnFlexId = new HiddenField();
                hdnFlexId.ID = "hdnFlexIdAd" + i;
                hdnFlexId.Value = row["flexId"].ToString();
                divlblFlex1.Controls.Add(hdnFlexId);

                HiddenField hdnFlexControl = new HiddenField();
                hdnFlexControl.ID = "hdnFlexControlAd" + i;
                hdnFlexControl.Value = Convert.ToString(row["flexControl"]);
                divlblFlex1.Controls.Add(hdnFlexControl);

                HiddenField hdnFlexMandatory = new HiddenField();
                hdnFlexMandatory.ID = "hdnFlexMandatoryAd" + i;
                hdnFlexMandatory.Value = Convert.ToString(row["flexMandatoryStatus"]);
                divlblFlex1.Controls.Add(hdnFlexMandatory);

                HiddenField hdnFlexLabel = new HiddenField();
                hdnFlexLabel.ID = "hdnFlexLabelAd" + i;
                hdnFlexLabel.Value = Convert.ToString(row["flexLabel"]);
                divlblFlex1.Controls.Add(hdnFlexLabel);

                Label lblFlex1 = new Label();
                lblFlex1.ID = "lblFlexAd" + i;
                if (Convert.ToString(row["flexMandatoryStatus"]) == "N")
                {
                    lblFlex1.Text = "<strong>" + Convert.ToString(row["flexLabel"]) + ":</strong>";
                }
                else
                {
                    lblFlex1.Text = "<strong>" + Convert.ToString(row["flexLabel"]) + ":<span class='red_span'>*</span></strong>";
                }

                divlblFlex1.Controls.Add(lblFlex1);

                HtmlGenericControl divFlexControl = new HtmlGenericControl("Div");
                divFlexControl.Attributes.Add("class", "col-md-3");

                Label lblError = new Label();
                lblError.ID = "lblErrorAd" + i;
                lblError.Font.Bold = true;
                lblError.CssClass = "red_span";

                HtmlTableCell tableCell1 = new HtmlTableCell();
                switch (row["flexControl"].ToString())
                {
                    case "T": //TextBox
                        TextBox txtFlex = new TextBox();
                        txtFlex.ID = "txtFlexAd" + i;
                        txtFlex.CssClass = "form-control";
                        if (row["flexDataType"].ToString() == "N") //N means Numeric
                        {
                            txtFlex.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'1');");
                        }
                        if (row["Detail_FlexData"] != DBNull.Value)
                        {
                            txtFlex.Text = Convert.ToString(row["Detail_FlexData"]);
                        }
                        divFlexControl.Controls.Add(txtFlex);
                        divFlexControl.Controls.Add(lblError);
                        break;
                    case "D"://Date

                        DropDownList ddlDay1 = new DropDownList();
                        ddlDay1.ID = "ddlDayAd" + i;
                        ddlDay1.Width = new Unit(70, UnitType.Percentage);
                        ddlDay1.CssClass = "form-control pull-left ";
                        BindDates(ddlDay1);

                        DropDownList ddlMonth = new DropDownList();
                        ddlMonth.ID = "ddlMonthAd" + i;
                        ddlMonth.Width = new Unit(70, UnitType.Percentage);
                        ddlMonth.CssClass = "form-control pull-left ";
                        BindMonths(ddlMonth);

                        DropDownList ddlYear1 = new DropDownList();
                        ddlYear1.ID = "ddlYearAd" + i;
                        ddlYear1.Width = new Unit(100, UnitType.Percentage);
                        ddlYear1.CssClass = "form-control pull-left ";
                        BindYears(ddlYear1);

                        DateTime date;
                        if (row["Detail_FlexData"] != DBNull.Value)
                        {
                            try
                            {
                                date = Convert.ToDateTime(row["Detail_FlexData"]);
                                ddlDay1.SelectedValue = date.Day.ToString();
                                ddlMonth.SelectedValue = date.Month.ToString();
                                ddlYear1.SelectedValue = date.Year.ToString();
                            }
                            catch { }
                        }

                        divFlexControl.Controls.Add(ddlDay1);
                        divFlexControl.Controls.Add(ddlMonth);
                        divFlexControl.Controls.Add(ddlYear1);
                        divFlexControl.Controls.Add(lblError);
                        break;
                    case "L"://DropDown
                        DropDownList ddlFlex = new DropDownList();
                        ddlFlex.ID = "ddlFlexAd" + i;
                        ddlFlex.CssClass = "form-control";
                        DataTable dt = dctFlexFieldDropdownData != null && dctFlexFieldDropdownData.ContainsKey(Convert.ToString(row["flexLabel"])) ?
                            dctFlexFieldDropdownData[Convert.ToString(row["flexLabel"])] : CorporateProfile.FillDropDown(Convert.ToString(row["flexSqlQuery"]));

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            if (dctFlexFieldDropdownData != null && !dctFlexFieldDropdownData.ContainsKey(Convert.ToString(row["flexLabel"])))
                                dctFlexFieldDropdownData.Add(Convert.ToString(row["flexLabel"]), dt);
                            ddlFlex.DataSource = dt;
                            ddlFlex.DataTextField = dt.Columns[1].ColumnName;
                            ddlFlex.DataValueField = dt.Columns[0].ColumnName;
                            ddlFlex.DataBind();
                        }
                        ddlFlex.Items.Insert(0, new ListItem("-- Select--", "-1"));
                        if (row["Detail_FlexData"] != DBNull.Value)
                        {
                            try
                            {
                                ddlFlex.SelectedValue = Convert.ToString(row["Detail_FlexData"]);
                            }
                            catch { }
                        }
                        divFlexControl.Controls.Add(ddlFlex);
                        divFlexControl.Controls.Add(lblError);
                        break;
                }
                tblFlexFieldsAd.Controls.Add(divlblFlex1); //Flex Label Binding
                tblFlexFieldsAd.Controls.Add(divFlexControl);//Flex Control Binding
                i++;
            }
        }
    }

    protected void imgContinue_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid)
            {
                //Form Individual Itineraries   
                //onwardResult --Onward Itinerary
                //Return Result -- Return Itinerary

                decimal rateOfExchange = 1;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                paxCount = request.AdultCount + request.ChildCount + request.InfantCount;
                SearchResult onwardResult = SessionValues[ONWARD_RESULT] as SearchResult;
                if (Session["FlightRequest"] != null)
                {
                    request = Session["FlightRequest"] as SearchRequest;
                }
                agency = Session["Agent"] as AgentMaster;
                FlightItinerary onwardItinerary = new FlightItinerary();
                onwardItinerary.Sendmail = chksendmail.Checked ? true : false;
                if (onwardResult != null)
                {
                    decimal k3tax = onwardResult.Price.K3Tax / paxCount;
                    //1.AgencyId ,LocationId
                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        onwardItinerary.AgencyId = Settings.LoginInfo.OnBehalfAgentID;
                        onwardItinerary.LocationId = Settings.LoginInfo.OnBehalfAgentLocation;
                        if (Settings.LoginInfo.OnBehalfAgentExchangeRates.ContainsKey(onwardResult.Price.SupplierCurrency))
                        {
                            rateOfExchange = Settings.LoginInfo.OnBehalfAgentExchangeRates[onwardResult.Price.SupplierCurrency];
                        }
                        else
                        {
                            rateOfExchange = 1;
                        }
                    }
                    else
                    {
                        onwardItinerary.LocationId = Convert.ToInt32(Settings.LoginInfo.LocationID);
                        onwardItinerary.AgencyId = Settings.LoginInfo.AgentId;
                        if (Settings.LoginInfo.AgentExchangeRates.ContainsKey(onwardResult.Price.SupplierCurrency))
                        {
                            rateOfExchange = Settings.LoginInfo.AgentExchangeRates[onwardResult.Price.SupplierCurrency];
                        }
                        else
                        {
                            rateOfExchange = 1;
                        }
                    }
                    if (onwardResult.Flights[0][0].FlightId > 0)
                    {
                        onwardItinerary.BookingId = BookingDetail.GetBookingIdByFlightId(onwardResult.Flights[0][0].FlightId);
                    }
                    onwardItinerary.BookingMode = BookingMode.BookingAPI;
                    onwardItinerary.CreatedBy = (int)Settings.LoginInfo.UserID;
                    onwardItinerary.CreatedOn = DateTime.Now;
                    onwardItinerary.ProductType = ProductType.Flight;
                    if (!string.IsNullOrEmpty(onwardResult.LastTicketDate))
                    {
                        onwardItinerary.LastTicketDate = Convert.ToDateTime(onwardResult.LastTicketDate);
                    }

                    onwardItinerary.Destination = request.Segments[0].Destination;
                    onwardItinerary.FareRules = onwardResult.FareRules;
                    onwardItinerary.FareType = (onwardResult.FareType != null ? onwardResult.FareType : FareType.Net.ToString());
                    onwardItinerary.FlightBookingSource = onwardResult.ResultBookingSource;
                    onwardItinerary.FlightId = onwardResult.Flights[0][0].FlightId;
                    onwardItinerary.ETicketHit = 1;
                    onwardItinerary.IsDomestic = false;
                    onwardItinerary.NonRefundable = onwardResult.NonRefundable;
                    onwardItinerary.Origin = request.Segments[0].Origin;
                    onwardItinerary.PaymentMode = ModeOfPayment.Credit;
                    onwardItinerary.GUID = onwardResult.GUID;
                    onwardItinerary.SpecialRequest = onwardResult.JourneySellKey;
                    onwardItinerary.TicketAdvisory = onwardResult.FareSellKey;

                    //2.Assigning Segments
                    onwardItinerary.Segments = SearchResult.GetSegments(onwardResult);
                    foreach (FlightInfo segment in onwardItinerary.Segments)
                    {
                        segment.CreatedBy = (int)Settings.LoginInfo.UserID;
                        segment.CreatedOn = DateTime.Now;
                        segment.LastModifiedBy = segment.CreatedBy;
                        segment.LastModifiedOn = segment.CreatedOn;
                        if (segment.BookingClass.Trim().Length <= 0)
                        {
                            segment.BookingClass = "C";
                        }
                    }


                    onwardItinerary.Ticketed = true;
                    onwardItinerary.TravelDate = onwardResult.Flights[0][0].DepartureTime;
                    onwardItinerary.UapiPricingSolution = onwardResult.UapiPricingSolution;
                    onwardItinerary.ValidatingAirlineCode = "";
                    onwardItinerary.IsLCC = onwardResult.IsLCC;//Will be True for Air Arabia, Fly Dubai, SpiceJet and for TBOAir LCC

                    //==============START :TBO:ASIGN GST FIELDS======================
                    if (onwardResult.ResultBookingSource == BookingSource.TBOAir)
                    {
                        onwardItinerary.SupplierLocatorCode = onwardResult.FareType;//LCCSpecialReturn/Normal Return/GDS Special Return
                        onwardItinerary.TicketAdvisory = onwardResult.FareSellKey;//SessionId of the onwardResult
                        onwardItinerary.UniversalRecord = onwardResult.JourneySellKey;//Save TBO Sources with | symbol
                        onwardItinerary.FareType = "PUB";
                        //Assign GST related fields for TBOAir
                        if (onwardResult.ResultBookingSource == BookingSource.TBOAir && onwardResult.IsGSTMandatory)
                        {
                            onwardItinerary.GstCompanyAddress = txtGSTCompanyAddress.Text;
                            onwardItinerary.GstCompanyContactNumber = txtGSTContactNumber.Text;
                            onwardItinerary.GstCompanyEmail = txtGSTCompanyEmail.Text;
                            onwardItinerary.GstCompanyName = txtGSTCompanyName.Text;
                            onwardItinerary.GstNumber = txtGSTNumber.Text;
                        }
                    }

                    //==============END :TBO:ASIGN GST FIELDS======================

                    //==========================START:FLIGHT INVENTORY=====================
                    if (onwardResult.ResultBookingSource == BookingSource.FlightInventory)
                    {
                        onwardItinerary.GUID = onwardResult.GUID;//(SeatQuota)
                        onwardItinerary.PNR = onwardResult.FareSellKey;//(PNR's)
                        onwardItinerary.SpecialRequest = onwardResult.JourneySellKey;//(InventoryId)
                    }
                    //==========================END : FLIGHT INVENTORY=====================

                    //==============START :UAPI:ASIGN GST FIELDS======================
                    if (onwardResult.ResultBookingSource == BookingSource.UAPI && requiredGSTForUAPI)
                    {
                        onwardItinerary.GstCompanyAddress = txtGSTCompanyAddress.Text;
                        onwardItinerary.GstCompanyContactNumber = txtGSTContactNumber.Text;
                        onwardItinerary.GstCompanyEmail = txtGSTCompanyEmail.Text;
                        onwardItinerary.GstCompanyName = txtGSTCompanyName.Text;
                        onwardItinerary.GstNumber = txtGSTNumber.Text;
                    }
                    //==============END :UAPI:ASIGN GST FIELDS======================

                    //====================START:LEAD PASSENGER OBJECT FORMATION==================

                    FlightPassenger onwardPax = new FlightPassenger();

                    onwardPax.CorpProfileId = chkAddPax.Checked ? string.IsNullOrEmpty(hdnProfileId.Value) ? "-1" : hdnProfileId.Value + "-U" : hdnProfileId.Value;
                    onwardPax.Title = ddlTitle.SelectedValue.Replace(".", "");
                    onwardPax.FirstName = txtPaxFName.Text;
                    onwardPax.LastName = txtPaxLName.Text;
                    onwardPax.Type = PassengerType.Adult;

                    //Check whether address is a mandatory or non mandatory field and assign as per the condition.
                    //Remove any new line chars if any as TBOAir will not allow booking

                    onwardPax.AddressLine1 = (txtAddress1.Text.Length == 0 ? "Shj" : txtAddress1.Text.Replace(Environment.NewLine, "").Replace("\\n", "").Replace("\n", ""));
                    onwardPax.AddressLine2 = (txtAddress2.Text.Length == 0 ? "Shj" : txtAddress2.Text.Replace(Environment.NewLine, "").Replace("\\n", "").Replace("\n", ""));
                    onwardPax.CellPhone = txtMobileCountryCode.Text + '-' + txtMobileNo.Text;
                    onwardPax.City = "";

                    //Check whether Country is a mandatory or non mandatory field and assign as per the condition.
                    onwardPax.Country = (ddlCountry.SelectedIndex == 0 ? Country.GetCountry("AE") : Country.GetCountry(ddlCountry.SelectedItem.Value));
                    onwardPax.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                    onwardPax.CreatedOn = DateTime.Now;
                    Airport destinationAirport = new Airport(request.Segments[0].Destination);//Search Type : One Way or Return.
                    Airport originAirport = new Airport(request.Segments[0].Origin);

                    //If TBOAir Itinerary is booked India origin and destination then DOB should not be assigned as it not mandatory
                    if (originAirport.CountryCode == "IN" && destinationAirport.CountryCode == "IN" && onwardResult.ResultBookingSource == BookingSource.TBOAir)
                    {
                        onwardPax.DateOfBirth = ((ddlDay.SelectedIndex > 0) && (ddlMonth.SelectedIndex > 0) && (ddlYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlDay.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue, dateFormat) : DateTime.MinValue);
                    }
                    else
                    {
                        //Check whether DateOfBirth is a mandatory or non mandatory field and assign as per the condition.
                        onwardPax.DateOfBirth = ((ddlDay.SelectedIndex > 0) && (ddlMonth.SelectedIndex > 0) && (ddlYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlDay.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue, dateFormat) : DateTime.Now.AddYears(-45));
                    }

                    onwardPax.Email = txtEmail.Text;
                    onwardPax.FFAirline = txtAirline.Text;
                    onwardPax.FFNumber = txtFlight.Text;
                    onwardPax.FlightId = onwardResult.Flights[0][0].FlightId;

                    //Check Gender DateOfBirth is a mandatory or non mandatory field and assign as per the condition.
                    onwardPax.Gender = (ddlGender.SelectedIndex > 0 ? (Gender)Convert.ToInt32(ddlGender.SelectedValue) : Gender.Male);
                    onwardPax.IsLeadPax = true;
                    onwardPax.Meal = new Meal();

                    //Check whether Nationality is a mandatory or non mandatory field and assign as per the condition.
                    onwardPax.Nationality = (ddlNationality.SelectedIndex == 0 ? Country.GetCountry("AE") : Country.GetCountry(ddlNationality.SelectedValue));

                    //Lead Pax :Irrespective of whether  PassportExpiry and PassportNo are  mandatory or not mandatory.
                    //By chance if the user enters any passport related information need to capture in that case.
                    if (txtPassportNo.Text.Length > 0 && !string.IsNullOrEmpty(txtPassportNo.Text))
                    {
                        onwardPax.PassportExpiry = ((ddlPEDay.SelectedIndex > 0) && (ddlPEMonth.SelectedIndex > 0) && (ddlPEYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPEDay.SelectedValue + "/" + ddlPEMonth.SelectedValue + "/" + ddlPEYear.SelectedValue, dateFormat) : DateTime.Now.AddYears(1));
                        onwardPax.PassportNo = txtPassportNo.Text;
                    }

                    //Added by lokesh on 25-sept-2017 regarding pax destination phone.
                    if (txtDestCntCode.Text.Length > 0 && txtDestPhoneNo.Text.Length > 0)
                    {
                        onwardPax.DestinationPhone = txtDestCntCode.Text + '-' + txtDestPhoneNo.Text;
                    }
                    else
                    {
                        onwardPax.DestinationPhone = string.Empty;
                    }

                    //Modified by Lokesh on 04/04/2018 For G9 Source only for lead Pax 
                    //if the customer is travelling from India and provides any GST info (StateCode and Tax RegNo)
                    if (onwardResult != null && onwardResult.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia)
                    {
                        /* Using AliasAirlineCode property to maintain resultkey and to retrieve air arabia booking session data */
                        onwardItinerary.AliasAirlineCode = onwardResult.ResultKey;

                        //&& hdnSelStateId.Value != "-1" && pax.Country.CountryCode == "IN" Added by shiva
                        //Send StateCode only when Country is India
                        if (!string.IsNullOrEmpty(hdnSelStateId.Value) && Convert.ToString(hdnSelStateId.Value).Length > 0 && hdnSelStateId.Value != "-1" && onwardPax.Country.CountryCode == "IN")
                        {
                            onwardPax.GSTStateCode = hdnSelStateId.Value;
                        }
                        else
                        {
                            onwardPax.GSTStateCode = string.Empty;
                        }
                        if (!string.IsNullOrEmpty(txtGSTRegNum.Text) && txtGSTRegNum.Text.Length > 0)
                        {
                            onwardPax.GSTTaxRegNo = txtGSTRegNum.Text;
                        }
                        else
                        {
                            onwardPax.GSTTaxRegNo = string.Empty;
                        }
                    }
                    //If the customer origin country code is india
                    //Capture tax reg number for spice jet source
                    //Capture company name for spice jet source
                    //it is not mandatory field.it depends upon choice.
                    else if (onwardResult != null && (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.IndigoCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp) && (onwardResult.Flights[0][0].Origin.CountryCode == "IN") && !string.IsNullOrEmpty(txtTaxRegNo_SG_GST.Text) && txtTaxRegNo_SG_GST.Text.Length > 0 && !string.IsNullOrEmpty(txtCompanyName_SG_GST.Text) && txtCompanyName_SG_GST.Text.Length > 0)
                    {
                        onwardPax.GSTTaxRegNo = txtTaxRegNo_SG_GST.Text;
                        onwardPax.GSTStateCode = txtCompanyName_SG_GST.Text;
                    }
                    else//For Remaining Air Sources the below 2 properties will be empty.
                    {
                        onwardPax.GSTStateCode = string.Empty;
                        onwardPax.GSTTaxRegNo = string.Empty;
                    }



                    onwardPax.Seat = new Seat();
                    onwardPax.Price = new PriceAccounts();

                    onwardPax.Price.TaxDetails = onwardResult.Price.TaxDetails;
                    onwardPax.Price.InputVATAmount = onwardResult.Price.InputVATAmount / paxCount;
                    onwardPax.Price.AccPriceType = (onwardResult.FareType == "NetFare" ? PriceType.NetFare : PriceType.PublishedFare);
                    if (onwardPax.Price.AccPriceType == PriceType.NetFare)
                    {
                        onwardPax.Price.NetFare = (decimal)onwardResult.FareBreakdown[0].BaseFare / onwardResult.FareBreakdown[0].PassengerCount;
                    }
                    else
                    {
                        onwardPax.Price.PublishedFare = (decimal)onwardResult.FareBreakdown[0].BaseFare / onwardResult.FareBreakdown[0].PassengerCount;
                    }
                    onwardPax.Price.SupplierCurrency = onwardResult.Price.SupplierCurrency;
                    if (onwardResult.ResultBookingSource == BookingSource.TBOAir)
                    {
                        onwardPax.Price.SupplierPrice = (decimal)onwardResult.Price.SupplierPrice;
                    }
                    else
                    {
                        onwardPax.Price.SupplierPrice = (decimal)onwardResult.FareBreakdown[0].SupplierFare / onwardResult.FareBreakdown[0].PassengerCount;
                    }

                    onwardPax.Price.Markup = onwardResult.Price.Markup;
                    onwardPax.Price.MarkupType = onwardResult.Price.MarkupType;
                    onwardPax.Price.MarkupValue = onwardResult.Price.MarkupValue;
                    onwardPax.Price.DecimalPoint = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentDecimalValue : Settings.LoginInfo.DecimalValue);
                    onwardPax.Price.Tax = Convert.ToDecimal(onwardResult.FareBreakdown[0].Tax.ToString("N" + agency.DecimalValue)) / onwardResult.FareBreakdown[0].PassengerCount;
                    onwardPax.Price.K3Tax = Convert.ToDecimal(k3tax.ToString("N" + agency.DecimalValue));

                    onwardPax.Price.Markup = Convert.ToDecimal(onwardResult.FareBreakdown[0].AgentMarkup.ToString("N" + agency.DecimalValue)) / onwardResult.FareBreakdown[0].PassengerCount;
                    onwardPax.Price.Discount = Convert.ToDecimal(onwardResult.FareBreakdown[0].AgentDiscount.ToString("N" + agency.DecimalValue)) / onwardResult.FareBreakdown[0].PassengerCount;
                    onwardPax.Price.DiscountType = onwardResult.Price.DiscountType;
                    onwardPax.Price.DiscountValue = onwardResult.Price.DiscountValue;
                    onwardPax.Price.HandlingFeeAmount = Convert.ToDecimal(onwardResult.FareBreakdown[0].HandlingFee.ToString("N" + agency.DecimalValue)) / onwardResult.FareBreakdown[0].PassengerCount;
                    onwardPax.Price.HandlingFeeType = onwardResult.Price.HandlingFeeType;
                    onwardPax.Price.HandlingFeeValue = onwardResult.Price.HandlingFeeValue;
                    onwardPax.Price.RateOfExchange = rateOfExchange;
                    onwardPax.Price.Currency = onwardResult.Currency;
                    onwardPax.Price.CurrencyCode = onwardResult.Currency;
                    onwardPax.Price.WhiteLabelDiscount = 0;
                    if (onwardResult.ResultBookingSource == BookingSource.FlyDubai || onwardResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl || onwardResult.ResultBookingSource == BookingSource.SalamAir)
                    {
                        onwardPax.Price.TransactionFee = onwardResult.Price.TransactionFee;
                    }
                    else
                    {
                        onwardPax.Price.TransactionFee = 0;
                    }


                    onwardPax.Price.AdditionalTxnFee = onwardResult.FareBreakdown[0].AdditionalTxnFee; //sai
                    if (onwardResult.ResultBookingSource == BookingSource.FlyDubai || onwardResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl || onwardResult.ResultBookingSource == BookingSource.SalamAir)
                    {
                        onwardPax.Price.FareInformationID = onwardResult.FareInformationId[PassengerType.Adult.ToString()];
                    }

                    if (!string.IsNullOrEmpty(hdfAsvAmount.Value))
                    {
                        onwardPax.Price.AsvAmount = Convert.ToDecimal(hdfAsvAmount.Value) / paxCount;
                    }
                    else
                    {
                        onwardPax.Price.AsvAmount = 0;
                    }

                    if (!string.IsNullOrEmpty(hdfAsvType.Value))
                    {
                        onwardPax.Price.AsvType = hdfAsvType.Value;
                    }
                    else
                    {
                        onwardPax.Price.AsvType = null;
                    }
                    if (!string.IsNullOrEmpty(hdfAsvElement.Value))
                    {
                        onwardPax.Price.AsvElement = hdfAsvElement.Value;
                    }
                    else
                    {
                        onwardPax.Price.AsvElement = null;
                    }

                    if (onwardItinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        onwardPax.Price.ChargeBU = onwardResult.Price.ChargeBU;
                        onwardPax.Price.YQTax = onwardResult.Price.YQTax;
                        onwardPax.Price.AgentCommission = onwardResult.Price.AgentCommission;
                        onwardPax.Price.SServiceFee = onwardResult.Price.SServiceFee;
                        onwardPax.Price.IncentiveEarned = onwardResult.Price.IncentiveEarned;
                        onwardPax.Price.TDSIncentive = onwardResult.Price.TDSIncentive;
                        onwardPax.Price.AgentPLB = onwardResult.Price.AgentPLB;
                        onwardPax.Price.TDSPLB = onwardResult.Price.TDSPLB;
                        onwardPax.Price.TdsCommission = onwardResult.Price.TdsCommission;
                        onwardPax.Price.AdditionalTxnFee = onwardResult.Price.AdditionalTxnFee;
                        onwardPax.Price.TransactionFee = onwardResult.Price.TransactionFee;
                        onwardPax.Price.OtherCharges = onwardResult.Price.OtherCharges;
                        onwardPax.Price.SupplierCurrency = onwardResult.Price.SupplierCurrency;

                        onwardPax.TBOPrice = new PriceAccounts();
                        onwardPax.TBOPrice.AccPriceType = PriceType.PublishedFare;
                        onwardPax.TBOPrice.PublishedFare = (decimal)onwardResult.TBOPrice.PublishedFare / paxCount;
                        onwardPax.TBOPrice.BaseFare = onwardResult.TBOPrice.BaseFare;
                        onwardPax.TBOPrice.Tax = (decimal)onwardResult.TBOPrice.Tax;
                        onwardPax.TBOPrice.Markup = onwardResult.TBOPrice.Markup;
                        onwardPax.TBOPrice.MarkupType = onwardResult.TBOPrice.MarkupType;
                        onwardPax.TBOPrice.MarkupValue = onwardResult.TBOPrice.MarkupValue;
                        onwardPax.TBOPrice.DecimalPoint = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentDecimalValue : Settings.LoginInfo.DecimalValue);
                        onwardPax.TBOPrice.Markup = onwardResult.FareBreakdown[0].AgentMarkup / onwardResult.FareBreakdown[0].PassengerCount;
                        onwardPax.TBOPrice.Discount = onwardResult.FareBreakdown[0].AgentDiscount / onwardResult.FareBreakdown[0].PassengerCount;
                        onwardPax.TBOPrice.RateOfExchange = 1;
                        onwardPax.TBOPrice.Currency = onwardResult.TBOPrice.Currency;
                        onwardPax.TBOPrice.CurrencyCode = onwardResult.TBOPrice.Currency;
                        onwardPax.TBOPrice.WhiteLabelDiscount = 0;
                        onwardPax.TBOPrice.WLCharge = onwardResult.TBOPrice.WLCharge;
                        onwardPax.TBOPrice.NetFare = onwardResult.TBOPrice.NetFare;
                        onwardPax.TBOPrice.OtherCharges = onwardResult.TBOPrice.OtherCharges;
                        onwardPax.TBOPrice.TransactionFee = onwardResult.TBOPrice.TransactionFee;
                        onwardPax.TBOPrice.AdditionalTxnFee = onwardResult.TBOPrice.AdditionalTxnFee; //sai               
                        onwardPax.TBOPrice.ChargeBU = onwardResult.TBOPrice.ChargeBU;
                        onwardPax.TBOPrice.AgentCommission = onwardResult.TBOPrice.AgentCommission;
                        onwardPax.TBOPrice.AirlineTransFee = onwardResult.TBOPrice.AirlineTransFee;
                        onwardPax.TBOPrice.AgentPLB = onwardResult.TBOPrice.AgentPLB;
                        onwardPax.TBOPrice.IncentiveEarned = onwardResult.TBOPrice.IncentiveEarned;
                        onwardPax.TBOPrice.TDSIncentive = onwardResult.TBOPrice.TDSIncentive;
                        onwardPax.TBOPrice.TDSPLB = onwardResult.TBOPrice.TDSPLB;
                        onwardPax.TBOPrice.TdsCommission = onwardResult.TBOPrice.TdsCommission;
                        onwardItinerary.TBOFareBreakdown = onwardResult.TBOFareBreakdown;
                    }

                    onwardPax.Price.IsServiceTaxOnBaseFarePlusYQ = false;
                    if (onwardItinerary.FlightBookingSource == BookingSource.PKFares)
                    {
                        onwardItinerary.TBOFareBreakdown = onwardResult.FareBreakdown;
                        onwardPax.BaggageCode = onwardResult.BaggageIncludedInFare;
                    }


                    if (onwardResult.ResultBookingSource == BookingSource.FlyDubai && onwardResult.IsBaggageIncluded)
                    {
                        onwardItinerary.IsBaggageIncluded = true;
                        onwardPax.Price.BaggageCharge = 0;
                        onwardPax.BaggageCode = onwardResult.BaggageIncludedInFare + "kg";
                        if (request.Type == SearchType.Return)
                        {
                            onwardPax.BaggageCode += "," + onwardPax.BaggageCode;
                        }
                    }



                    #region AirArabia
                    if (onwardResult.ResultBookingSource == BookingSource.AirArabia)
                    {
                        if (ddlOnwardBaggage.SelectedIndex > 0)
                        {
                            if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split(',')[0]) > 0)
                            {
                                string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split(',')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        onwardPax.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text;
                                    }
                                    else
                                    {
                                        onwardPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text;
                                    }
                                }
                                onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                            }
                            else
                            {
                                for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        onwardPax.BaggageCode += "," + ddlOnwardBaggage.SelectedItem.Text;
                                    }
                                    else
                                    {
                                        onwardPax.BaggageCode = ddlOnwardBaggage.SelectedItem.Text;
                                    }
                                }
                                onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                            }
                            onwardPax.BaggageType = ddlOnwardBaggage.SelectedValue.Split('-')[1];
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split(',')[0]) > 0)
                            {
                                string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        onwardPax.BaggageCode = segmentBaggageDetails[i] + "Kg";
                                    }
                                    else
                                    {
                                        onwardPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg";
                                    }
                                }
                            }
                            else
                            {
                                onwardPax.BaggageCode = "No Bag";
                            }

                            onwardPax.BaggageType = "No Bag";
                        }

                        //
                        //onwardPax.BaggageCode = ddlOnwardBaggage.SelectedItem.Text;
                        //onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                        //onwardPax.BaggageCode = ddlOnwardBaggage.SelectedIndex == 0 ? onwardPax.BaggageCode.Replace("Select Baggage", "No Bag") : onwardPax.BaggageCode;
                    }
                    #endregion

                    //Code added by Lokesh on 07/10/2016.
                    #region Baggage Information for Indigo Lead passenger

                    if (onwardItinerary.FlightBookingSource == BookingSource.Indigo || onwardItinerary.FlightBookingSource == BookingSource.IndigoCorp)
                    {
                        DataTable dtBaggage = null;

                        if (SessionValues[ONWARD_BAGGAGE] != null)
                        {
                            dtBaggage = SessionValues[ONWARD_BAGGAGE] as DataTable;
                        }

                        if (dtBaggage != null && dtBaggage.Rows.Count > 0 && ddlOnwardBaggage.SelectedIndex > 0)
                        {
                            DataRow[] data = dtBaggage.Select("Description='" + ddlOnwardBaggage.SelectedItem.Text + "'");
                            if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                            {
                                onwardPax.BaggageType = data[0]["Code"].ToString();
                                onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                                string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        onwardPax.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    }
                                    else
                                    {
                                        onwardPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    }
                                }
                            }
                            else
                            {
                                onwardPax.BaggageType = data[0]["Code"].ToString();
                                onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                                string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        onwardPax.BaggageCode = ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    }
                                    else
                                    {
                                        onwardPax.BaggageCode += "," + ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    }

                                }


                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare))
                            {
                                string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        if (ddlOnwardBaggage.SelectedIndex > 0)
                                        {
                                            onwardPax.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                        }
                                        else
                                        {
                                            onwardPax.BaggageCode = segmentBaggageDetails[i] + "Kg ";
                                        }
                                    }
                                    else
                                    {
                                        if (ddlOnwardBaggage.SelectedIndex > 0)
                                        {
                                            onwardPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");

                                        }
                                        else
                                        {
                                            onwardPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg ";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        onwardPax.BaggageCode = "No Bag";
                                    }
                                    else
                                    {
                                        onwardPax.BaggageCode += "," + "No Bag";
                                    }
                                }
                            }
                        }
                    }


                    #endregion

                    #region Baggage  SpiceJet
                    if (onwardItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp)
                    {
                        DataTable dtBaggage = null;
                        if (onwardResult == null)
                        {
                            onwardResult = SessionValues[ONWARD_RESULT] as SearchResult;
                        }
                        if (SessionValues[ONWARD_BAGGAGE] != null)
                        {
                            dtBaggage = SessionValues[ONWARD_BAGGAGE] as DataTable;
                        }
                        if (dtBaggage != null && dtBaggage.Rows.Count > 0 && ddlOnwardBaggage.SelectedIndex > 0)
                        {
                            DataRow[] data = dtBaggage.Select("Description='" + ddlOnwardBaggage.SelectedItem.Text + "'");
                            if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                            {
                                onwardPax.BaggageType = data[0]["Code"].ToString();
                                onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                                string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        onwardPax.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                    }
                                    else
                                    {
                                        onwardPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                    }
                                }
                            }
                            else
                            {
                                onwardPax.BaggageType = data[0]["Code"].ToString();
                                for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        onwardPax.BaggageCode = ddlOnwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                    }
                                    else
                                    {
                                        onwardPax.BaggageCode += "," + ddlOnwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                    }
                                }
                                //onwardPax.BaggageCode = ddlOnwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                            {
                                string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        if (ddlOnwardBaggage.SelectedIndex > 0) //Added Condition for to avoid to display 'UpgradeBaggage' in E-ticket page 
                                        {
                                            onwardPax.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                        }
                                        else
                                        {
                                            onwardPax.BaggageCode = segmentBaggageDetails[i] + "Kg ";
                                        }
                                    }
                                    else
                                    {
                                        if (ddlOnwardBaggage.SelectedIndex > 0) //Added Condition for to avoid to display 'UpgradeBaggage' in E-ticket page 
                                        {
                                            onwardPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");

                                        }
                                        else
                                        {
                                            onwardPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg ";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        onwardPax.BaggageCode = "No Bag";
                                    }
                                    else
                                    {
                                        onwardPax.BaggageCode += "," + "No Bag";
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region Baggage for TBOAir
                    if (onwardItinerary.FlightBookingSource == BookingSource.TBOAir)  //Added by Ravi. To load the bagggage in the passenger details.
                    {
                        DataTable dtBaggage = SessionValues[ONWARD_BAGGAGE] as DataTable;

                        if (dtBaggage != null)
                        {
                            foreach (DataRow dr in dtBaggage.Rows)
                            {
                                if (ddlOnwardBaggage.SelectedIndex > 0 && ddlOnwardBaggage.SelectedItem != null && dr["Price"].ToString() == ddlOnwardBaggage.SelectedValue && dr["Group"].ToString() == "0")
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        onwardPax.BaggageCode = dr["Code"].ToString();
                                        onwardPax.BaggageType = "WayType:" + dr["WayType"].ToString() + ",Code:" + dr["Code"].ToString() + ",Description:" + dr["Description"].ToString() + ",Weight:" + dr["Weight"].ToString() + ",Currency:" + dr["Currency"].ToString() + ",Price:" + dr["SupplierPrice"].ToString() + ",Origin:" + dr["Origin"].ToString() + ",Destination:" + dr["Destination"].ToString();
                                        onwardPax.Price.BaggageCharge = Convert.ToDecimal(dr["Price"]);
                                    }
                                    else
                                    {
                                        onwardPax.BaggageCode += "," + dr["Code"].ToString();
                                        onwardPax.BaggageType += "|WayType:" + dr["WayType"].ToString() + ",Code:" + dr["Code"].ToString() + ",Description:" + dr["Description"].ToString() + ",Weight:" + dr["Weight"].ToString() + ",Currency:" + dr["Currency"].ToString() + ",Price:" + dr["SupplierPrice"].ToString() + ",Origin:" + dr["Origin"].ToString() + ",Destination:" + dr["Destination"].ToString();
                                        onwardPax.Price.BaggageCharge += Convert.ToDecimal(dr["Price"]);
                                    }
                                }
                            }
                        }
                    }

                    #endregion

                    #region Baggage FlyDubai
                    if (onwardItinerary.FlightBookingSource == BookingSource.FlyDubai)
                    {
                        if (request.Type == SearchType.Return)
                        {
                            if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible)
                            {
                                onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) + Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                onwardPax.FlyDubaiBaggageCharge = new List<decimal>();
                                onwardPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                onwardPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                            }
                            else if (ddlOnwardBaggage.Visible)
                            {
                                onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) + 0;
                                onwardPax.FlyDubaiBaggageCharge = new List<decimal>();
                                onwardPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                onwardPax.FlyDubaiBaggageCharge.Add(0);
                            }
                            else if (ddlInwardBaggage.Visible)
                            {
                                onwardPax.Price.BaggageCharge = 0 + Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                onwardPax.FlyDubaiBaggageCharge = new List<decimal>();
                                onwardPax.FlyDubaiBaggageCharge.Add(0);
                                onwardPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                            }
                            else
                            {
                                onwardPax.Price.BaggageCharge = 0;
                                onwardPax.FlyDubaiBaggageCharge = new List<decimal>();
                                onwardPax.FlyDubaiBaggageCharge.Add(0);
                                onwardPax.FlyDubaiBaggageCharge.Add(0);
                            }
                            if (onwardItinerary.FlightBookingSource == BookingSource.FlyDubai)
                            {
                                if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible)
                                {
                                    if (SessionValues[ONWARD_BAGGAGE] != null)
                                    {
                                        DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                        if (ddlOnwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            if (onwardResult.BaggageIncludedInFare.Split(',')[0] == "10" || onwardResult.BaggageIncludedInFare.Split(',')[0] == "20" || onwardResult.BaggageIncludedInFare.Split(',')[0] == "30")
                                            {
                                                DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    onwardPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                    onwardPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    onwardPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                    onwardPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            switch (onwardResult.BaggageIncludedInFare.Split(',')[0])
                                            {
                                                case "20":
                                                    onwardPax.BaggageType = "BAGB";
                                                    break;
                                                case "30":
                                                    onwardPax.BaggageType = "BAGL";
                                                    break;
                                                case "40":
                                                    onwardPax.BaggageType = "BAGX";
                                                    break;
                                            }
                                            if (onwardPax.BaggageType.Length > 0)
                                            {
                                                onwardPax.CategoryId = "99";
                                            }
                                            else
                                            {
                                                onwardPax.CategoryId = "";
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] == "40")
                                        {
                                            onwardPax.BaggageType = "JBAG";
                                            onwardPax.CategoryId = "99";
                                        }
                                        else
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }


                                        if (ddlInwardBaggage.SelectedIndex > 0 && request.Type == SearchType.Return && onwardResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0 && request.Type == SearchType.Return && onwardResult.BaggageIncludedInFare.Split(',')[1] != "0" && (onwardResult.FareType.Split(',')[1].ToUpper() == PAYTOCHANGE || onwardResult.FareType.Split(',')[1].ToUpper() == NOCHANGE))
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex <= 0 && request.Type == SearchType.Return && onwardResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            switch (onwardResult.BaggageIncludedInFare.Split(',')[1])
                                            {
                                                case "20":
                                                    onwardPax.BaggageType += ",BAGB";
                                                    break;
                                                case "30":
                                                    onwardPax.BaggageType += ",BAGL";
                                                    break;
                                                case "40":
                                                    onwardPax.BaggageType += ",BAGX";
                                                    break;
                                            }
                                            if (onwardPax.BaggageType.IndexOf(",") > 0)
                                            {
                                                onwardPax.CategoryId += ",99";
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex <= 0 && request.Type == SearchType.Return && onwardResult.BaggageIncludedInFare.Split(',')[1] == "40")
                                        {
                                            onwardPax.BaggageType += ",JBAG";
                                            onwardPax.CategoryId += ",99";
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            onwardPax.BaggageType += ",";
                                        }
                                    }
                                }
                                else if (ddlOnwardBaggage.Visible)
                                {
                                    if (SessionValues[ONWARD_BAGGAGE] != null)
                                    {
                                        DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                        if (ddlOnwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            if (onwardResult.BaggageIncludedInFare.Split(',')[0] == "10" || onwardResult.BaggageIncludedInFare.Split(',')[0] == "20" || onwardResult.BaggageIncludedInFare.Split(',')[0] == "30")
                                            {
                                                DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    onwardPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                    onwardPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    onwardPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                    onwardPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] == "0")
                                        {
                                            switch (onwardResult.BaggageIncludedInFare)
                                            {
                                                case "0":
                                                    onwardPax.BaggageType = "";
                                                    break;
                                                case "20":
                                                    onwardPax.BaggageType = "BAGB";
                                                    break;
                                                case "30":
                                                    onwardPax.BaggageType = "BAGL";
                                                    break;
                                                case "40":
                                                    onwardPax.BaggageType = "BAGX";
                                                    break;
                                            }
                                            if (onwardPax.BaggageType.Length > 0)
                                            {
                                                onwardPax.CategoryId = "99";
                                            }

                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] == "40")
                                        {
                                            onwardPax.BaggageType = "JBAG";
                                            onwardPax.CategoryId = "99";
                                        }
                                        else
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        if (onwardResult.FareType.Split(',')[1].ToUpper() == BASIC)
                                        {
                                            onwardPax.BaggageType += ",JBAG";
                                            onwardPax.CategoryId += ",99";
                                        }
                                        else
                                        {
                                            onwardPax.BaggageType += ",BAGX";
                                            onwardPax.CategoryId += ",99";
                                        }
                                    }
                                }
                                else if (ddlInwardBaggage.Visible)
                                {
                                    if (SessionValues[ONWARD_BAGGAGE] != null)
                                    {
                                        if (onwardResult.FareType.Split(',')[0].ToUpper() == BASIC)
                                        {
                                            onwardPax.BaggageType = "JBAG";
                                            onwardPax.CategoryId = "99";
                                        }
                                        else
                                        {
                                            onwardPax.BaggageType = "BAGX";
                                            onwardPax.CategoryId = "99";
                                        }

                                        DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                        if (ddlInwardBaggage.SelectedIndex > 0 && request.Type == SearchType.Return && onwardResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (onwardResult.BaggageIncludedInFare.Split(',')[1] == "10" || onwardResult.BaggageIncludedInFare.Split(',')[1] == "20" || onwardResult.BaggageIncludedInFare.Split(',')[1] == "30")
                                            {
                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    onwardPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                    onwardPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    onwardPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                    onwardPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex <= 0 && request.Type == SearchType.Return && onwardResult.BaggageIncludedInFare.Split(',')[1] == "0")
                                        {

                                            switch (onwardResult.BaggageIncludedInFare)
                                            {
                                                case "20":
                                                    onwardPax.BaggageType += ",BAGB";
                                                    break;
                                                case "30":
                                                    onwardPax.BaggageType += ",BAGL";
                                                    break;
                                                case "40":
                                                    onwardPax.BaggageType += ",BAGX";
                                                    break;
                                            }
                                            if (onwardPax.BaggageType.IndexOf(",") > 0)
                                            {
                                                onwardPax.CategoryId += ",99";
                                            }
                                            onwardPax.BaggageType += ",";
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex <= 0 && request.Type == SearchType.Return && onwardResult.BaggageIncludedInFare.Split(',')[1] == "40")
                                        {
                                            onwardPax.BaggageType += ",BAGX";
                                            onwardPax.CategoryId += ",99";
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            onwardPax.BaggageType += ",";
                                        }
                                    }
                                }
                                else
                                {
                                    onwardPax.BaggageType = "JBAG,JBAG";
                                    onwardPax.CategoryId = "99,99";
                                }
                            }

                            AssignPaxBaggageCodes(onwardPax, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                        }
                        else if (request.Type == SearchType.OneWay)
                        {
                            if (ddlOnwardBaggage.Visible)
                            {
                                onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue);
                                onwardPax.FlyDubaiBaggageCharge = new List<decimal>();
                                onwardPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                            }
                            else
                            {
                                onwardPax.Price.BaggageCharge = 0;
                                onwardPax.FlyDubaiBaggageCharge = new List<decimal>();
                                onwardPax.FlyDubaiBaggageCharge.Add(0);
                            }
                            if (onwardItinerary.FlightBookingSource == BookingSource.FlyDubai)
                            {
                                if (ddlOnwardBaggage.Visible)
                                {
                                    if (SessionValues[ONWARD_BAGGAGE] != null)
                                    {
                                        DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                        if (ddlOnwardBaggage.SelectedIndex > 0 && (onwardResult.FareType.ToUpper() == FREETOCHANGE || onwardResult.FareType.ToUpper() == NOCHANGE))
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare != "0")
                                        {

                                            switch (onwardResult.BaggageIncludedInFare)
                                            {
                                                case "20":
                                                    onwardPax.BaggageType = "BAGB";
                                                    break;
                                                case "30":
                                                    onwardPax.BaggageType = "BAGL";
                                                    break;
                                                case "40":
                                                    onwardPax.BaggageType = "BAGX";
                                                    break;
                                            }
                                            if (onwardPax.BaggageType.Length > 0)
                                            {
                                                onwardPax.CategoryId = "99";
                                            }
                                        }
                                        else
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                    }
                                }
                                else if (onwardResult.FareType.ToUpper() == NOCHANGE)
                                {
                                    onwardPax.BaggageType = "BAGX";
                                    onwardPax.CategoryId = "99";
                                }
                                else if (onwardResult.FareType.ToUpper() == BASIC)
                                {
                                    onwardItinerary.IsBaggageIncluded = true;
                                    onwardPax.BaggageType = "JBAG";
                                    onwardPax.CategoryId = "99";
                                }
                            }

                            AssignPaxBaggageCodes(onwardPax, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                        }
                    }

                    #endregion

                    #region  Baggage AirIndia express   
                    if (onwardItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl)
                    {
                        if (SessionValues[ONWARD_BAGGAGE] != null)
                        {
                            DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                            if (request.Type == SearchType.Return)
                            {
                                if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible)
                                {
                                    onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) + Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                    onwardPax.FlyDubaiBaggageCharge = new List<decimal>();
                                    onwardPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                    onwardPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                                }
                                else if (ddlOnwardBaggage.Visible)
                                {
                                    onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue);
                                    onwardPax.FlyDubaiBaggageCharge = new List<decimal>();
                                    onwardPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                    onwardPax.FlyDubaiBaggageCharge.Add(0);
                                }
                                else if (ddlInwardBaggage.Visible)
                                {
                                    onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                    onwardPax.FlyDubaiBaggageCharge = new List<decimal>();
                                    onwardPax.FlyDubaiBaggageCharge.Add(0);
                                    onwardPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                                }
                                else
                                {
                                    onwardPax.Price.BaggageCharge = 0;
                                    onwardPax.FlyDubaiBaggageCharge = new List<decimal>();
                                    onwardPax.FlyDubaiBaggageCharge.Add(0);
                                    onwardPax.FlyDubaiBaggageCharge.Add(0);
                                }

                                //In return journey if both onward and return additional baggage are available.
                                if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible && ddlOnwardBaggage.SelectedIndex > 0 && ddlInwardBaggage.SelectedIndex > 0)
                                {
                                    DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlOnwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                    if (baggage != null && baggage.Length > 0)
                                    {
                                        onwardPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                        onwardPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                    }
                                    if (ddlInwardBaggage.SelectedIndex > 0)
                                    {
                                        baggage = dtBaggageInfo.Select("CodeType='" + ddlInwardBaggage.SelectedItem.Text.Split('-')[1] + "'");

                                        if (baggage != null && baggage.Length > 0)
                                        {
                                            onwardPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                            onwardPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                        }
                                    }
                                }
                                else if (ddlOnwardBaggage.Visible && ddlOnwardBaggage.SelectedIndex > 0) // If only onward additional baggage is available.
                                {
                                    if (ddlOnwardBaggage.SelectedIndex > 0)
                                    {
                                        DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlOnwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                        if (baggage != null && baggage.Length > 0)
                                        {
                                            onwardPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                            onwardPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                        }
                                    }
                                }
                                else if (ddlInwardBaggage.Visible && ddlInwardBaggage.SelectedIndex > 0) // If only return baggage is available.
                                {
                                    DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlInwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                    if (baggage != null && baggage.Length > 0)
                                    {
                                        onwardPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                        onwardPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                    }
                                }

                                AssignAirIndiaExpressPaxBaggage(onwardPax, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                            }
                            else if (request.Type == SearchType.OneWay)
                            {
                                if (ddlOnwardBaggage.Visible)
                                {
                                    onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue);
                                    onwardPax.FlyDubaiBaggageCharge = new List<decimal>();
                                    onwardPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                }
                                else
                                {
                                    onwardPax.Price.BaggageCharge = 0;
                                    onwardPax.FlyDubaiBaggageCharge = new List<decimal>();
                                    onwardPax.FlyDubaiBaggageCharge.Add(0);
                                }
                                if (ddlOnwardBaggage.Visible && ddlOnwardBaggage.SelectedIndex > 0)
                                {
                                    DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlOnwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                    if (baggage != null && baggage.Length > 0)
                                    {
                                        onwardPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                        onwardPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                    }
                                }
                                AssignAirIndiaExpressPaxBaggage(onwardPax, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                            }
                        }
                    }

                    #endregion

                    #region Baggage GoAir
                    if (onwardItinerary.FlightBookingSource == BookingSource.GoAir || onwardItinerary.FlightBookingSource == BookingSource.GoAirCorp)
                    {
                        DataTable dtBaggage = null;
                        if (onwardResult == null)
                        {
                            onwardResult = SessionValues[ONWARD_RESULT] as SearchResult;
                        }
                        if (SessionValues[ONWARD_BAGGAGE] != null)
                        {
                            dtBaggage = SessionValues[ONWARD_BAGGAGE] as DataTable;
                        }
                        if (dtBaggage != null && dtBaggage.Rows.Count > 0 && ddlOnwardBaggage.SelectedIndex > 0)
                        {
                            DataRow[] data = dtBaggage.Select("Description='" + ddlOnwardBaggage.SelectedItem.Text + "'");
                            if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                            {
                                onwardPax.BaggageType = data[0]["Code"].ToString();
                                onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                                string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        onwardPax.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    }
                                    else
                                    {
                                        onwardPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    }
                                }
                            }
                            else
                            {
                                onwardPax.BaggageType = data[0]["Code"].ToString();
                                onwardPax.BaggageCode = ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                onwardPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                            {
                                string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        onwardPax.BaggageCode = segmentBaggageDetails[i] + "Kg ";
                                    }
                                    else
                                    {
                                        onwardPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg";
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                {
                                    if (string.IsNullOrEmpty(onwardPax.BaggageCode))
                                    {
                                        onwardPax.BaggageCode = "No Bag";
                                    }
                                    else
                                    {
                                        onwardPax.BaggageCode += "," + "No Bag";
                                    }
                                }
                            }
                        }
                    }

                    #endregion


                    #region  Meal : Indigo,SpiceJet,GoAir 

                    if ((onwardItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardItinerary.FlightBookingSource == BookingSource.Indigo || onwardItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || onwardItinerary.FlightBookingSource == BookingSource.IndigoCorp || onwardItinerary.FlightBookingSource == BookingSource.GoAir || onwardItinerary.FlightBookingSource == BookingSource.GoAirCorp) && SessionValues[ONWARD_BAGGAGE] != null)
                    {
                        DataTable dtBaggage = SessionValues[ONWARD_BAGGAGE] as DataTable;
                        if (!string.IsNullOrEmpty(hdnOutMealSelection.Value) && hdnOutMealSelection.Value.Length > 0)
                        {
                            string[] onwardSelection = hdnOutMealSelection.Value.Split(',');
                            if (Convert.ToInt32(onwardSelection[0]) > 0)
                            {
                                DataRow[] data = dtBaggage.Select("Description='" + ddlOnwardMeal.Items[Convert.ToInt32(onwardSelection[0])].Text + "'");
                                if (data != null && data.Length > 0)
                                {
                                    onwardPax.MealType = data[0]["Code"].ToString();
                                    onwardPax.Price.MealCharge = Convert.ToDecimal(ddlOnwardMeal.Items[Convert.ToInt32(onwardSelection[0])].Value.Split('-')[0]);
                                }

                                for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(onwardPax.MealDesc))
                                    {
                                        onwardPax.MealDesc += ',' + ddlOnwardMeal.Items[Convert.ToInt32(onwardSelection[0])].Text;
                                    }
                                    else
                                    {
                                        onwardPax.MealDesc = ddlOnwardMeal.Items[Convert.ToInt32(onwardSelection[0])].Text;
                                    }
                                }
                            }
                            else //No Onward Meal Selected
                            {
                                for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(onwardPax.MealDesc))
                                    {
                                        onwardPax.MealDesc += ',' + "No Meal";
                                    }
                                    else
                                    {
                                        onwardPax.MealDesc = "No Meal";
                                    }
                                }
                            }
                        }
                        else //No Onward Meal Selected
                        {
                            for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                            {
                                if (!string.IsNullOrEmpty(onwardPax.MealDesc))
                                {
                                    onwardPax.MealDesc += ',' + "No Meal";
                                }
                                else
                                {
                                    onwardPax.MealDesc = "No Meal";
                                }
                            }
                        }

                    }
                    #endregion
                    //Salam Air Lead Pax default Baggage for Lead Pax
                    if (onwardItinerary.FlightBookingSource == BookingSource.SalamAir && onwardPax.Type != PassengerType.Infant)
                    {
                        AssignSalamAirBagCodes(onwardPax, onwardResult.BaggageIncludedInFare);
                    }

                    //G9 Onward Meals
                    if (onwardItinerary.FlightBookingSource == BookingSource.AirArabia && onwardPax.Type != PassengerType.Infant)
                    {
                        DataTable dtBaggage =  SessionValues.ContainsKey("OnwardG9Meal") ? SessionValues[ONWARD_G9MEALS] as DataTable : null;
                        if (ddlOnwardMeal.SelectedIndex > 0)
                        {
                            DataRow[] data = dtBaggage.Select("mealDescription='" + ddlOnwardMeal.SelectedItem.Text + "'");
                            if (data != null && data.Length > 0)
                            {
                                onwardPax.MealType = data[0]["mealCode"].ToString();
                                onwardPax.Price.MealCharge = Convert.ToDecimal(ddlOnwardMeal.SelectedValue.Split('-')[1]);
                            }

                            for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                            {
                                if (!string.IsNullOrEmpty(onwardPax.MealDesc))
                                {
                                    onwardPax.MealDesc += ',' + ddlOnwardMeal.SelectedItem.Text;
                                }
                                else
                                {
                                    onwardPax.MealDesc = ddlOnwardMeal.SelectedItem.Text;
                                }
                            }
                        }
                        else //No Onward Meal Selected
                        {
                            for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                            {
                                if (!string.IsNullOrEmpty(onwardPax.MealDesc))
                                {
                                    onwardPax.MealDesc += ',' + "No Meal";
                                }
                                else
                                {
                                    onwardPax.MealDesc = "No Meal";
                                }
                            }
                            onwardPax.MealType = "No Meal";
                        }
                    }

                    //Flex Details saving
                    FlightFlexDetails[] arrPaxFlex = null;

                    if (!string.IsNullOrEmpty(hdnPaxFlexInfo.Value))
                    {
                        arrPaxFlex = JsonConvert.DeserializeObject<FlightFlexDetails[]>(hdnPaxFlexInfo.Value);
                        onwardPax.FlexDetailsList = arrPaxFlex.Where(x => x.PaxId == 0 && !string.IsNullOrEmpty(x.FlexLabel)).ToList();
                    }

                    //if (tblFlexFields != null && Convert.ToInt32(hdnFlexCount.Value) > 0)
                    //{
                    //    onwardPax.FlexDetailsList = new List<FlightFlexDetails>();

                    //    for (int i = 0; i < Convert.ToInt32(hdnFlexCount.Value); i++)
                    //    {
                    //        FlightFlexDetails flexDetails = new FlightFlexDetails();
                    //        HiddenField hdnFlexControl = tblFlexFields.FindControl("hdnFlexControl" + i) as HiddenField;
                    //        HiddenField hdnFlexId = tblFlexFields.FindControl("hdnFlexId" + i) as HiddenField;
                    //        HiddenField hdnFlexLabel = tblFlexFields.FindControl("hdnFlexLabel" + i) as HiddenField;
                    //        flexDetails.FlexId = Convert.ToInt32(hdnFlexId.Value);
                    //        flexDetails.FlexLabel = hdnFlexLabel.Value;
                    //        switch (hdnFlexControl.Value)
                    //        {
                    //            case "T":
                    //                TextBox txtFlex = tblFlexFields.FindControl("txtFlex" + i) as TextBox;
                    //                flexDetails.FlexData = txtFlex.Text;
                    //                break;
                    //            case "D":
                    //                DropDownList ddlPaxDay = tblFlexFields.FindControl("ddlDay" + i) as DropDownList;
                    //                DropDownList ddlPaxMonth = tblFlexFields.FindControl("ddlMonth" + i) as DropDownList;
                    //                DropDownList ddlPaxYear = tblFlexFields.FindControl("ddlYear" + i) as DropDownList;
                    //                flexDetails.FlexData = (ddlPaxDay.SelectedIndex > 0 && ddlPaxMonth.SelectedIndex > 0 && ddlPaxYear.SelectedIndex > 0) ?
                    //                            Convert.ToString(Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat)) : string.Empty;
                    //                break;
                    //            case "L":
                    //                DropDownList ddlFlex = tblFlexFields.FindControl("ddlFlex" + i) as DropDownList;
                    //                flexDetails.FlexData = ddlFlex.SelectedIndex > 0 ? ddlFlex.SelectedItem.Value : string.Empty;
                    //                break;
                    //        }
                    //        flexDetails.CreatedBy = (int)Settings.LoginInfo.UserID;
                    //        flexDetails.ProductID = 1;
                    //        onwardPax.FlexDetailsList.Add(flexDetails);

                    //    }
                    //}

                    /* To read and assign pax seat information from Jason string and assign 
                     * the same to Lead passenger object for both onward and return
                     * ---- Code Starts here ---- */

                    if (!string.IsNullOrEmpty(txtPaxSeatInfo.Text))
                        liPaxSeatInfo = JsonConvert.DeserializeObject<List<PaxSeatInfo>>(txtPaxSeatInfo.Text);

                    onwardPax.liPaxSeatInfo = new List<PaxSeatInfo>();


                    var objonwardFlights = onwardResult.Flights[0].Select(p => p.FlightNumber).Distinct();
                    List<string> seglst = new List<string>();
                    foreach (string fltno in objonwardFlights)
                    {
                        string segmnt = string.Empty;
                        segmnt = onwardResult.Flights[0].ToList().Where(y => y.FlightNumber == fltno).Select(x => x.Origin.AirportCode).FirstOrDefault();
                        segmnt += "-" + onwardResult.Flights[0].ToList().Where(y => y.FlightNumber == fltno).Select(x => x.Destination.AirportCode).Last();
                        seglst.Add(segmnt);
                    }

                    liPaxSeatInfo.Where(y => y.PaxNo == 0 && !string.IsNullOrEmpty(y.Segment)).ToList().ForEach(x =>
                    {
                        int cnt = seglst.Where(z => z == x.Segment).Count();
                        if (cnt > 0)
                        {
                            if (!string.IsNullOrEmpty(x.SeatNo) && x.SeatNo != "NoSeat")
                                onwardPax.SeatInfo = string.IsNullOrEmpty(onwardPax.SeatInfo) ? x.SeatNo + "(" + x.Segment + ")" : onwardPax.SeatInfo + " | " + x.SeatNo + "(" + x.Segment + ")";
                            onwardPax.liPaxSeatInfo.Add(x);
                        }

                    });

                    onwardPax.Price.SeatPrice = onwardPax.liPaxSeatInfo.Sum(x => x.Price);
                    ddlOnwardBaggage.Enabled = ddlOnwardMeal.Enabled = ddlInwardBaggage.Enabled = ddlInwardMeal.Enabled = false;
                    /* ---- Code ends here ---- */

                    //====================END:LEAD PASSENGER OBJECT FORMATION==================

                    //====================START:ADDITIONAL PASSENGER OBJECT FORMATION==================

                    FlightPassenger[] onwardPaxList = new FlightPassenger[paxCount];
                    onwardPaxList[0] = onwardPax;

                    int counter = 1;

                    foreach (DataListItem item in dlAdditionalPax.Items)
                    {
                        Label lblPaxType = item.FindControl("lblPaxType") as Label;
                        DropDownList ddlPaxTitle = item.FindControl("ddlPaxTitle") as DropDownList;
                        TextBox txtPaxFirstName = item.FindControl("txtPaxFName") as TextBox;
                        TextBox txtPaxLastName = item.FindControl("txtPaxLName") as TextBox;
                        DropDownList ddlPaxGender = item.FindControl("ddlGender") as DropDownList;
                        DropDownList ddlPaxDay = item.FindControl("ddlDay") as DropDownList;
                        DropDownList ddlPaxMonth = item.FindControl("ddlMonth") as DropDownList;
                        DropDownList ddlPaxYear = item.FindControl("ddlYear") as DropDownList;
                        TextBox txtPaxPassportNo = item.FindControl("txtPassportNo") as TextBox;
                        DropDownList ddlPaxPEDay = item.FindControl("ddlPEDay") as DropDownList;
                        DropDownList ddlPaxPEMonth = item.FindControl("ddlPEMonth") as DropDownList;
                        DropDownList ddlPaxPEYear = item.FindControl("ddlPEYear") as DropDownList;
                        DropDownList ddlPaxCountry = item.FindControl("ddlCountry") as DropDownList;
                        DropDownList ddlPaxNationality = item.FindControl("ddlNationality") as DropDownList;
                        DropDownList ddlOnwardBaggage = item.FindControl("ddlOnwardBaggage") as DropDownList;
                        DropDownList ddlInwardBaggage = item.FindControl("ddlInwardBaggage") as DropDownList;

                        DropDownList ddlOnwardMeal = item.FindControl("ddlOnwardMeal") as DropDownList;
                        DropDownList ddlInwardMeal = item.FindControl("ddlInwardMeal") as DropDownList;
                        TextBox txtAirline = item.FindControl("txtAirline") as TextBox;
                        TextBox txtFlight = item.FindControl("txtFlight") as TextBox;
                        HiddenField hdnProfileId = item.FindControl("hdnProfileId") as HiddenField;
                        CheckBox chkAddPax = item.FindControl("chkAddPax") as CheckBox;
                        HtmlGenericControl tblFlexFieldsAd = item.FindControl("tblFlexFieldsAd") as HtmlGenericControl;

                        ddlOnwardBaggage.Enabled = ddlInwardBaggage.Enabled = ddlOnwardMeal.Enabled = ddlInwardMeal.Enabled = false;

                        FlightPassenger onwardPax1 = new FlightPassenger();

                        onwardPax1.liPaxSeatInfo = new List<PaxSeatInfo>();


                        /* To assign pax seat information for all passengers (except lead passenger) 
                         * to passenger object for both onward and return, 
                         * also disable baagage and meal selection to avaoid the changes from user in case of seat assignment failure 
                         * ---- Code Starts here ---- */

                        ddlOnwardBaggage.Enabled = ddlInwardBaggage.Enabled = ddlOnwardMeal.Enabled = ddlInwardMeal.Enabled = false;

                        if (lblPaxType.Text != "Infant")
                        {
                            liPaxSeatInfo.Where(y => y.PaxNo != 0 && y.PaxNo - 1 == item.ItemIndex && !string.IsNullOrEmpty(y.Segment)).ToList().ForEach(x =>
                            {
                                int cnt = seglst.Where(z => z == x.Segment).Count();
                                if (cnt > 0)
                                {
                                    if (!string.IsNullOrEmpty(x.SeatNo) && x.SeatNo != "NoSeat")
                                        onwardPax1.SeatInfo = string.IsNullOrEmpty(onwardPax1.SeatInfo) ? x.SeatNo + "(" + x.Segment + ")" : onwardPax1.SeatInfo + " | " + x.SeatNo + "(" + x.Segment + ")";
                                    onwardPax1.liPaxSeatInfo.Add(x);
                                }

                            });
                        }

                        /*----Code ends here ----*/

                        onwardPax1.CorpProfileId = chkAddPax.Checked ? string.IsNullOrEmpty(hdnProfileId.Value) ? "-1" : hdnProfileId.Value + "-U" : hdnProfileId.Value;
                        onwardPax1.Title = ddlPaxTitle.SelectedValue.Replace(".", "");
                        onwardPax1.FirstName = txtPaxFirstName.Text;
                        onwardPax1.LastName = txtPaxLastName.Text;

                        //If TBOAir Itinerary is booked India origin and destination then DOB should not be assigned as it not mandatory
                        if (originAirport.CountryCode == "IN" && destinationAirport.CountryCode == "IN" && onwardResult.ResultBookingSource == BookingSource.TBOAir)
                        {
                            switch (lblPaxType.Text)
                            {
                                //Based on the pax type assign the date of birth if they are mandatory or non mandatory fields
                                case "Adult":
                                    onwardPax1.Type = PassengerType.Adult;
                                    onwardPax1.DateOfBirth = ((ddlPaxDay.SelectedIndex > 0) && (ddlPaxMonth.SelectedIndex > 0) && (ddlPaxYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat) : DateTime.MinValue);
                                    break;
                                case "Child":
                                    onwardPax1.Type = PassengerType.Child;
                                    onwardPax1.DateOfBirth = ((ddlPaxDay.SelectedIndex > 0) && (ddlPaxMonth.SelectedIndex > 0) && (ddlPaxYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat) : DateTime.MinValue);
                                    break;
                                case "Infant":
                                    onwardPax1.Type = PassengerType.Infant;
                                    onwardPax1.DateOfBirth = ((ddlPaxDay.SelectedIndex > 0) && (ddlPaxMonth.SelectedIndex > 0) && (ddlPaxYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat) : DateTime.MinValue);
                                    break;
                            }
                        }
                        else
                        {
                            switch (lblPaxType.Text)
                            {
                                //Based on the pax type assign the date of birth if they are mandatory or non mandatory fields
                                case "Adult":
                                    onwardPax1.Type = PassengerType.Adult;
                                    onwardPax1.DateOfBirth = ((ddlPaxDay.SelectedIndex > 0) && (ddlPaxMonth.SelectedIndex > 0) && (ddlPaxYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat) : DateTime.Now.AddYears(-45));
                                    break;
                                case "Child":
                                    onwardPax1.Type = PassengerType.Child;
                                    onwardPax1.DateOfBirth = ((ddlPaxDay.SelectedIndex > 0) && (ddlPaxMonth.SelectedIndex > 0) && (ddlPaxYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat) : DateTime.Now.AddYears(-10));
                                    break;
                                case "Infant":
                                    onwardPax1.Type = PassengerType.Infant;
                                    onwardPax1.DateOfBirth = ((ddlPaxDay.SelectedIndex > 0) && (ddlPaxMonth.SelectedIndex > 0) && (ddlPaxYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat) : DateTime.Now.AddYears(-1));
                                    break;
                            }
                        }



                        //Check whether address is a mandatory field or not and assign as per the condition.
                        onwardPax1.AddressLine1 = (txtAddress1.Text.Length == 0 ? "Shj" : txtAddress1.Text);
                        onwardPax1.AddressLine2 = (txtAddress2.Text.Length == 0 ? "Shj" : txtAddress2.Text);
                        onwardPax1.CellPhone = txtMobileCountryCode.Text + '-' + txtMobileNo.Text;
                        onwardPax1.City = "";
                        //Check whether country is a mandatory field or not and assign as per the condition.
                        onwardPax1.Country = (ddlPaxCountry.SelectedIndex > 0 ? Country.GetCountry(ddlPaxCountry.SelectedItem.Value) : Country.GetCountry("AE"));
                        onwardPax1.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                        onwardPax1.CreatedOn = DateTime.Now;
                        onwardPax1.Email = txtEmail.Text;
                        onwardPax1.FFAirline = txtAirline.Text;
                        onwardPax1.FFNumber = txtFlight.Text;
                        onwardPax1.FlightId = onwardResult.Flights[0][0].FlightId;
                        //Check whether Gender is a mandatory field or not and assign as per the condition.
                        onwardPax1.Gender = (ddlPaxGender.SelectedIndex > 0 ? (Gender)Convert.ToInt32(ddlPaxGender.SelectedValue) : Gender.Male);
                        onwardPax1.IsLeadPax = false;
                        onwardPax1.Meal = new Meal();
                        //Check whether Nationality is a mandatory field or not and assign as per the condition.
                        onwardPax1.Nationality = (ddlPaxNationality.SelectedIndex > 0 ? Country.GetCountry(ddlPaxNationality.SelectedValue) : Country.GetCountry("AE"));
                        //Additional Pax :Irrespective of whether  PassportExpiry and PassportNo are  mandatory or not mandatory 
                        //By chance if the user enters any passport related information need to capture in that case.
                        if (txtPaxPassportNo.Text.Length > 0 && !string.IsNullOrEmpty(txtPaxPassportNo.Text))
                        {
                            onwardPax1.PassportExpiry = (ddlPaxPEDay.SelectedIndex > 0 && ddlPaxPEMonth.SelectedIndex > 0 && ddlPaxPEYear.SelectedIndex > 0 ? Convert.ToDateTime(ddlPaxPEDay.SelectedValue + "/" + ddlPaxPEMonth.SelectedValue + "/" + ddlPaxPEYear.SelectedValue, dateFormat) : DateTime.Now.AddYears(1));
                            onwardPax1.PassportNo = txtPaxPassportNo.Text;
                        }

                        onwardPax1.Price = new PriceAccounts();

                        onwardPax1.Price.SeatPrice = onwardPax1.liPaxSeatInfo.Sum(x => x.Price);

                        onwardPax1.Price.TaxDetails = onwardResult.Price.TaxDetails;
                        onwardPax1.Price.InputVATAmount = onwardResult.Price.InputVATAmount / paxCount;
                        for (int i = 0; i < onwardResult.FareBreakdown.Length; i++)
                        {
                            if (onwardPax1.Type == onwardResult.FareBreakdown[i].PassengerType)
                            {
                                onwardPax1.Price.AccPriceType = (onwardResult.FareType == "NetFare" ? PriceType.NetFare : PriceType.PublishedFare);
                                if (onwardPax1.Price.AccPriceType == PriceType.NetFare)
                                {
                                    onwardPax1.Price.NetFare = (decimal)onwardResult.FareBreakdown[i].BaseFare / onwardResult.FareBreakdown[i].PassengerCount;
                                }
                                else
                                {
                                    onwardPax1.Price.PublishedFare = (decimal)onwardResult.FareBreakdown[i].BaseFare / onwardResult.FareBreakdown[i].PassengerCount;
                                }
                                onwardPax1.Price.SupplierCurrency = onwardResult.Price.SupplierCurrency;
                                if (onwardResult.ResultBookingSource == BookingSource.TBOAir)
                                {
                                    onwardPax1.Price.SupplierPrice = 0;
                                }
                                else
                                {
                                    onwardPax1.Price.SupplierPrice = (decimal)onwardResult.FareBreakdown[i].SupplierFare / onwardResult.FareBreakdown[i].PassengerCount;
                                }
                                onwardPax1.Price.Markup = onwardResult.Price.Markup;
                                onwardPax1.Price.MarkupType = onwardResult.Price.MarkupType;
                                onwardPax1.Price.MarkupValue = onwardResult.Price.MarkupValue;
                                onwardPax1.Price.DecimalPoint = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentDecimalValue : Settings.LoginInfo.DecimalValue);
                                onwardPax1.Price.Tax = Convert.ToDecimal(onwardResult.FareBreakdown[i].Tax.ToString("N"+agency.DecimalValue)) / onwardResult.FareBreakdown[i].PassengerCount;
                                onwardPax1.Price.K3Tax = Convert.ToDecimal(k3tax.ToString("N"+agency.DecimalValue));

                                onwardPax1.Price.AdditionalTxnFee = (decimal)onwardResult.FareBreakdown[i].AdditionalTxnFee / onwardResult.FareBreakdown[i].PassengerCount;
                                onwardPax1.Price.Markup = Convert.ToDecimal(onwardResult.FareBreakdown[i].AgentMarkup.ToString("N"+agency.DecimalValue)) / onwardResult.FareBreakdown[i].PassengerCount;
                                onwardPax1.Price.Discount = Convert.ToDecimal(onwardResult.FareBreakdown[i].AgentDiscount.ToString("N"+agency.DecimalValue)) / onwardResult.FareBreakdown[i].PassengerCount;
                                onwardPax1.Price.DiscountType = onwardResult.Price.DiscountType;
                                onwardPax1.Price.DiscountValue = onwardResult.Price.DiscountValue;
                                onwardPax1.Price.HandlingFeeAmount = Convert.ToDecimal(onwardResult.FareBreakdown[i].HandlingFee.ToString("N"+agency.DecimalValue)) / onwardResult.FareBreakdown[i].PassengerCount;
                                onwardPax1.Price.HandlingFeeType = onwardResult.Price.HandlingFeeType;
                                onwardPax1.Price.HandlingFeeValue = onwardResult.Price.HandlingFeeValue;
                            }
                            if (onwardResult.ResultBookingSource == BookingSource.FlyDubai || onwardResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl)
                            {
                                onwardPax1.Price.FareInformationID = onwardResult.FareInformationId[onwardPax1.Type.ToString()];
                                onwardPax1.Price.TransactionFee = onwardResult.Price.TransactionFee;
                            }
                            else
                            {
                                onwardPax1.Price.TransactionFee = 0;
                            }
                            if (onwardItinerary.FlightBookingSource == BookingSource.TBOAir)
                            {
                                onwardPax1.Price.ChargeBU = onwardResult.Price.ChargeBU;
                                onwardPax1.Price.YQTax = 0;
                                onwardPax1.Price.AgentCommission = 0;
                                onwardPax1.Price.SServiceFee = 0;
                                onwardPax1.Price.IncentiveEarned = 0;
                                onwardPax1.Price.TDSIncentive = 0;
                                onwardPax1.Price.AgentPLB = 0;
                                onwardPax1.Price.TDSPLB = 0;
                                onwardPax1.Price.TdsCommission = 0;
                                onwardPax1.Price.AdditionalTxnFee = 0;
                                onwardPax1.Price.TransactionFee = 0;
                                onwardPax1.Price.OtherCharges = 0;
                                onwardPax1.Price.SupplierCurrency = onwardResult.Price.SupplierCurrency;

                                onwardPax1.TBOPrice = new PriceAccounts();
                                onwardPax1.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                onwardPax1.TBOPrice.PublishedFare = (decimal)onwardResult.TBOPrice.PublishedFare / paxCount;
                                onwardPax1.TBOPrice.BaseFare = onwardResult.TBOPrice.BaseFare;
                                onwardPax1.TBOPrice.Tax = (decimal)onwardResult.TBOPrice.Tax;
                                onwardPax1.TBOPrice.Markup = onwardResult.TBOPrice.Markup;
                                onwardPax1.TBOPrice.MarkupType = onwardResult.TBOPrice.MarkupType;
                                onwardPax1.TBOPrice.MarkupValue = onwardResult.TBOPrice.MarkupValue;
                                onwardPax1.TBOPrice.DecimalPoint = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentDecimalValue : Settings.LoginInfo.DecimalValue);
                                onwardPax1.TBOPrice.Markup = onwardResult.FareBreakdown[i].AgentMarkup / onwardResult.FareBreakdown[i].PassengerCount;
                                onwardPax1.TBOPrice.Discount = onwardResult.FareBreakdown[i].AgentDiscount / onwardResult.FareBreakdown[i].PassengerCount;
                                onwardPax1.TBOPrice.RateOfExchange = 1;
                                onwardPax1.TBOPrice.Currency = onwardResult.TBOPrice.Currency;
                                onwardPax1.TBOPrice.CurrencyCode = onwardResult.TBOPrice.Currency;
                                onwardPax1.TBOPrice.WhiteLabelDiscount = 0;
                                onwardPax1.TBOPrice.WLCharge = onwardResult.TBOPrice.WLCharge;
                                onwardPax1.TBOPrice.NetFare = onwardResult.TBOPrice.NetFare;
                                onwardPax1.TBOPrice.OtherCharges = onwardResult.TBOPrice.OtherCharges;
                                onwardPax1.TBOPrice.TransactionFee = onwardResult.TBOPrice.TransactionFee;
                                onwardPax1.TBOPrice.AdditionalTxnFee = onwardResult.TBOPrice.AdditionalTxnFee; //sai               
                                onwardPax1.TBOPrice.ChargeBU = onwardResult.TBOPrice.ChargeBU;
                                onwardPax1.TBOPrice.AgentCommission = onwardResult.TBOPrice.AgentCommission;

                                onwardPax1.TBOPrice.AirlineTransFee = onwardResult.TBOPrice.AirlineTransFee;
                                onwardPax1.TBOPrice.AgentPLB = onwardResult.TBOPrice.AgentPLB;

                                onwardPax1.TBOPrice.IncentiveEarned = onwardResult.TBOPrice.IncentiveEarned;
                                onwardPax1.TBOPrice.TDSIncentive = onwardResult.TBOPrice.TDSIncentive;
                                onwardPax1.TBOPrice.TDSPLB = onwardResult.TBOPrice.TDSPLB;
                                onwardPax1.TBOPrice.TdsCommission = onwardResult.TBOPrice.TdsCommission;
                            }

                        }


                        //sai
                        if (!string.IsNullOrEmpty(hdfAsvAmount.Value))
                        {
                            onwardPax1.Price.AsvAmount = Convert.ToDecimal(hdfAsvAmount.Value) / paxCount;
                        }
                        else
                        {
                            onwardPax1.Price.AsvAmount = 0;
                        }

                        if (!string.IsNullOrEmpty(hdfAsvType.Value))
                        {
                            onwardPax1.Price.AsvType = hdfAsvType.Value;
                        }
                        else
                        {
                            onwardPax1.Price.AsvType = null;
                        }

                        if (!string.IsNullOrEmpty(hdfAsvElement.Value))
                        {
                            onwardPax1.Price.AsvElement = hdfAsvElement.Value;
                        }
                        else
                        {
                            onwardPax1.Price.AsvElement = null;
                        }

                        onwardPax1.Price.RateOfExchange = rateOfExchange;
                        onwardPax1.Price.Currency = onwardResult.Currency;
                        onwardPax1.Price.CurrencyCode = onwardResult.Currency;
                        onwardPax1.Price.WhiteLabelDiscount = 0;

                        onwardPax1.Price.IsServiceTaxOnBaseFarePlusYQ = false;

                        if (onwardResult.ResultBookingSource == BookingSource.PKFares)
                        {
                            onwardPax1.BaggageCode = onwardResult.BaggageIncludedInFare;
                        }

                        if (onwardResult.ResultBookingSource == BookingSource.AirArabia && onwardPax1.Type != PassengerType.Infant)
                        {

                            if (ddlOnwardBaggage.SelectedIndex > 0)
                            {
                                if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split(',')[0]) > 0)
                                {
                                    string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split(',')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text;
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text;
                                        }
                                    }
                                    onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                                }
                                else
                                {
                                    for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                    {
                                        if (!string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode += "," + ddlOnwardBaggage.SelectedItem.Text;
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode = ddlOnwardBaggage.SelectedItem.Text;
                                        }
                                    }
                                    onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                                }
                                onwardPax1.BaggageType = ddlOnwardBaggage.SelectedValue.Split('-')[1];
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split(',')[0]) > 0)
                                {
                                    string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = segmentBaggageDetails[i] + "Kg";
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg";
                                        }
                                    }
                                }
                                else
                                {
                                    onwardPax1.BaggageCode = "No Bag";
                                }
                                onwardPax1.BaggageType = "No Bag";
                            }

                            //onwardPax1.BaggageCode = ddlOnwardBaggage.SelectedItem.Text;
                            //onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                           // onwardPax1.BaggageCode = ddlOnwardBaggage.SelectedIndex == 0 ? onwardPax1.BaggageCode.Replace("Select Baggage", "No Bag") : onwardPax1.BaggageCode;
                        }
                        #region SpiceJet Additional Baggage
                        if ((onwardItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardItinerary.FlightBookingSource == BookingSource.SpiceJetCorp) && onwardPax1.Type != PassengerType.Infant)
                        {
                            DataTable dtBaggage = null;
                            if (onwardResult == null)
                            {
                                onwardResult = SessionValues[ONWARD_RESULT] as SearchResult;
                            }
                            if (SessionValues[ONWARD_BAGGAGE] != null)
                            {
                                dtBaggage = dtBaggage = SessionValues[ONWARD_BAGGAGE] as DataTable;
                            }
                            if (dtBaggage != null && dtBaggage.Rows.Count > 0 && ddlOnwardBaggage.SelectedIndex > 0)
                            {
                                DataRow[] data = dtBaggage.Select("Description='" + ddlOnwardBaggage.SelectedItem.Text + "'");
                                if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                                {
                                    onwardPax1.BaggageType = data[0]["Code"].ToString();
                                    onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                                    string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                        }
                                    }
                                }
                                else
                                {
                                    onwardPax1.BaggageType = data[0]["Code"].ToString();
                                    for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = ddlOnwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + ddlOnwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                        }
                                    }
                                    //onwardPax1.BaggageCode = ddlOnwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                    onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                                {
                                    string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = segmentBaggageDetails[i] + "Kg";
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg";
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = "No Bag";
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + "No Bag";
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region Indigo Additional Baggage info.
                        if ((onwardItinerary.FlightBookingSource == BookingSource.Indigo || onwardItinerary.FlightBookingSource == BookingSource.IndigoCorp) && onwardPax1.Type != PassengerType.Infant)
                        {


                            DataTable dtBaggage = null;
                            if (SessionValues[ONWARD_BAGGAGE] != null)
                            {
                                dtBaggage = SessionValues[ONWARD_BAGGAGE] as DataTable;
                            }
                            if (dtBaggage != null && dtBaggage.Rows.Count > 0 && ddlOnwardBaggage.SelectedIndex > 0)
                            {
                                DataRow[] data = dtBaggage.Select("Description='" + ddlOnwardBaggage.SelectedItem.Text + "'");
                                if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                                {
                                    onwardPax1.BaggageType = data[0]["Code"].ToString();
                                    onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                                    string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = segmentBaggageDetails[i] + "Kg +" + ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");

                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg +" + ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                        }
                                    }
                                }
                                else
                                {
                                    onwardPax1.BaggageType = data[0]["Code"].ToString();
                                    onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                                    string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                        }

                                    }
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                                {
                                    string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = segmentBaggageDetails[i] + "Kg";
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg";
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = "No Bag";
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + "No Bag";
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region TBOAirBaggageInfo for additional pax
                        if (onwardItinerary.FlightBookingSource == BookingSource.TBOAir && onwardPax1.Type != PassengerType.Infant)
                        {
                            DataTable dtBaggage = SessionValues[ONWARD_BAGGAGE] as DataTable;

                            if (dtBaggage != null)
                            {
                                foreach (DataRow dr in dtBaggage.Rows)
                                {

                                    if (ddlOnwardBaggage.SelectedItem != null && dr["Weight"].ToString().Trim() == ddlOnwardBaggage.SelectedItem.Text.ToLower().Replace("kg", "").Trim() && dr["Group"].ToString() == "0")
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = dr["Code"].ToString();
                                            onwardPax1.BaggageType = "WayType:" + dr["WayType"].ToString() + ",Code:" + dr["Code"].ToString() + ",Description:" + dr["Description"].ToString() + ",Weight:" + dr["Weight"].ToString() + ",Currency:" + dr["Currency"].ToString() + ",Price:" + dr["SupplierPrice"].ToString() + ",Origin:" + dr["Origin"].ToString() + ",Destination:" + dr["Destination"].ToString();
                                            onwardPax1.Price.BaggageCharge = Convert.ToDecimal(dr["Price"]);
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + dr["Code"].ToString();
                                            onwardPax1.BaggageType += "|WayType:" + dr["WayType"].ToString() + ",Code:" + dr["Code"].ToString() + ",Description:" + dr["Description"].ToString() + ",Weight:" + dr["Weight"].ToString() + ",Currency:" + dr["Currency"].ToString() + ",Price:" + dr["SupplierPrice"].ToString() + ",Origin:" + dr["Origin"].ToString() + ",Destination:" + dr["Destination"].ToString();
                                            onwardPax1.Price.BaggageCharge += Convert.ToDecimal(dr["Price"]);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region FlyDubai Baggage info for additional pax
                        if (onwardItinerary.FlightBookingSource == BookingSource.FlyDubai && onwardPax1.Type != PassengerType.Infant)
                        {
                            if (request.Type == SearchType.Return)
                            {
                                if (SessionValues[ONWARD_BAGGAGE] != null && onwardItinerary.FlightBookingSource == BookingSource.FlyDubai)
                                {
                                    if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible)
                                    {
                                        DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                        if (ddlOnwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] == "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            switch (onwardResult.BaggageIncludedInFare.Split(',')[0])
                                            {
                                                case "20":
                                                    onwardPax1.BaggageType += "BAGB";
                                                    break;
                                                case "30":
                                                    onwardPax1.BaggageType += "BAGL";
                                                    break;
                                                case "40":
                                                    onwardPax1.BaggageType += "BAGX";
                                                    break;
                                            }
                                            if (onwardPax1.BaggageType.Length > 0)
                                            {
                                                onwardPax1.CategoryId += "99";
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.FareType.Split(',')[0].ToUpper() == BASIC)
                                        {
                                            onwardPax1.BaggageType = "JBAG";
                                            onwardPax1.CategoryId = "99";
                                        }
                                        else
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        if (ddlInwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            switch (onwardResult.BaggageIncludedInFare.Split(',')[1])
                                            {
                                                case "20":
                                                    onwardPax1.BaggageType += ",BAGB";
                                                    break;
                                                case "30":
                                                    onwardPax1.BaggageType += ",BAGL";
                                                    break;
                                                case "40":
                                                    onwardPax1.BaggageType += ",BAGX";
                                                    break;
                                            }
                                            if (onwardPax1.BaggageType.IndexOf(",") > 0)
                                            {
                                                onwardPax1.CategoryId += ",99";
                                            }

                                        }
                                        else if (ddlInwardBaggage.SelectedIndex <= 0 && onwardResult.FareType.Split(',')[1].ToUpper() == BASIC)
                                        {
                                            onwardPax1.BaggageType += ",JBAG";
                                            onwardPax1.CategoryId += ",99";
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0 && onwardResult.FareType.Split(',')[1].ToUpper() == NOCHANGE || onwardResult.FareType.Split(',')[1].ToUpper() == PAYTOCHANGE)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageType += ",";
                                        }
                                    }
                                    else if (ddlOnwardBaggage.Visible)
                                    {
                                        DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                        if (ddlOnwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] == "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            switch (onwardResult.BaggageIncludedInFare.Split(',')[0])
                                            {
                                                case "20":
                                                    onwardPax1.BaggageType = "BAGB";
                                                    break;
                                                case "30":
                                                    onwardPax1.BaggageType = "BAGL";
                                                    break;
                                                case "40":
                                                    onwardPax1.BaggageType = "BAGX";
                                                    break;
                                            }
                                            if (onwardPax1.BaggageType.Length > 0)
                                            {
                                                onwardPax1.CategoryId = "99";
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.FareType.Split(',')[0].ToUpper() == BASIC)
                                        {
                                            onwardPax1.BaggageType = "JBAG";
                                            onwardPax1.CategoryId = "99";
                                        }
                                        else
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        if (onwardResult.FareType.Split(',')[1].ToUpper() == BASIC)
                                        {
                                            onwardPax1.BaggageType += ",JBAG";
                                            onwardPax1.CategoryId += ",99";
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageType += ",BAGX";
                                            onwardPax1.CategoryId += ",99";
                                        }
                                    }
                                    else if (ddlInwardBaggage.Visible)
                                    {
                                        if (onwardResult.FareType.Split(',')[0].ToUpper() == BASIC)
                                        {
                                            onwardPax1.BaggageType += "JBAG";
                                            onwardPax1.CategoryId += "99";
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageType = "BAGX";
                                            onwardPax1.CategoryId = "99";
                                        }

                                        DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                        if (ddlInwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            switch (onwardResult.BaggageIncludedInFare.Split(',')[1])
                                            {
                                                case "20":
                                                    onwardPax1.BaggageType += ",BAGB";
                                                    break;
                                                case "30":
                                                    onwardPax1.BaggageType += ",BAGL";
                                                    break;
                                                case "40":
                                                    onwardPax1.BaggageType += ",BAGX";
                                                    break;
                                            }
                                            if (onwardPax1.BaggageType.IndexOf(",") > 0)
                                            {
                                                onwardPax1.CategoryId += ",99";
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex <= 0 && onwardResult.FareType.Split(',')[1].ToUpper() == BASIC)
                                        {
                                            onwardPax1.BaggageType += ",JBAG";
                                            onwardPax1.CategoryId += ",99";
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[1] == "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageType += ",";
                                        }

                                    }
                                    else
                                    {
                                        onwardPax1.BaggageType = "JBAG,JBAG";
                                        onwardPax1.CategoryId = "99,99";
                                    }
                                }
                                if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible)
                                {
                                    onwardPax1.Price.BaggageCharge = (Convert.ToDecimal(ddlOnwardBaggage.SelectedValue)) + (Convert.ToDecimal(ddlInwardBaggage.SelectedValue));
                                    onwardPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                    onwardPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                    onwardPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                                }
                                else if (ddlOnwardBaggage.Visible)
                                {
                                    onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) + 0;
                                    onwardPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                    onwardPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                    onwardPax1.FlyDubaiBaggageCharge.Add(0);
                                }
                                else if (ddlInwardBaggage.Visible)
                                {
                                    onwardPax1.Price.BaggageCharge = 0 + Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                    onwardPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                    onwardPax1.FlyDubaiBaggageCharge.Add(0);
                                    onwardPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                                }
                                else
                                {
                                    onwardPax1.Price.BaggageCharge = 0;
                                    onwardPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                    onwardPax1.FlyDubaiBaggageCharge.Add(0);
                                    onwardPax1.FlyDubaiBaggageCharge.Add(0);
                                }

                                AssignPaxBaggageCodes(onwardPax1, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                            }
                            else if (request.Type == SearchType.OneWay && onwardPax1.Type != PassengerType.Infant)
                            {
                                if (SessionValues[ONWARD_BAGGAGE] != null && onwardItinerary.FlightBookingSource == BookingSource.FlyDubai)
                                {
                                    if (ddlOnwardBaggage.Visible)
                                    {
                                        DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                        if (ddlOnwardBaggage.SelectedIndex > 0 && (onwardResult.FareType.ToUpper() == FREETOCHANGE || onwardResult.FareType.ToUpper() == NOCHANGE))
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare != "0")
                                        {
                                            switch (onwardResult.BaggageIncludedInFare)
                                            {
                                                case "20":
                                                    onwardPax1.BaggageType = "BAGB";
                                                    break;
                                                case "30":
                                                    onwardPax1.BaggageType = "BAGL";
                                                    break;
                                                case "40":
                                                    onwardPax1.BaggageType = "BAGX";
                                                    break;
                                            }
                                            if (onwardPax1.BaggageType.Length > 0)
                                            {
                                                onwardPax1.CategoryId = "99";
                                            }
                                        }
                                        else if (onwardResult.FareType.ToUpper() == BASIC)
                                        {
                                            onwardItinerary.IsBaggageIncluded = true;
                                            onwardPax1.BaggageType = "JBAG";
                                            onwardPax1.CategoryId = "99";
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex > 0 && onwardResult.FareType.ToUpper() == PAYTOCHANGE)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageType = "";
                                            onwardPax1.CategoryId = "";
                                        }
                                    }
                                    else if (onwardResult.FareType.ToUpper() == NOCHANGE)
                                    {
                                        onwardPax1.BaggageType = "BAGX";
                                        onwardPax1.CategoryId = "99";
                                    }

                                }
                                if (ddlOnwardBaggage.Visible)
                                {
                                    onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue);
                                    onwardPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                    onwardPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                }
                                else
                                {
                                    onwardPax1.Price.BaggageCharge = 0;
                                    onwardPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                    onwardPax1.FlyDubaiBaggageCharge.Add(0);
                                }

                                AssignPaxBaggageCodes(onwardPax1, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                            }
                        }
                        #endregion

                        #region Baggage Information for AirIndia express for additional Pax
                        if (onwardItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl)
                        {
                            if (onwardPax1.Type == PassengerType.Infant)
                            {
                                AssignAirIndiaExpressPaxBaggage(onwardPax1, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                            }
                            else if (onwardPax1.Type != PassengerType.Infant)
                            {
                                if (SessionValues[ONWARD_BAGGAGE] != null)
                                {
                                    DataTable dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
                                    if (request.Type == SearchType.Return)
                                    {
                                        if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible)
                                        {
                                            onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) + Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                            onwardPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                            onwardPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                            onwardPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                                        }
                                        else if (ddlOnwardBaggage.Visible)
                                        {
                                            onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue);
                                            onwardPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                            onwardPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                            onwardPax1.FlyDubaiBaggageCharge.Add(0);
                                        }
                                        else if (ddlInwardBaggage.Visible)
                                        {
                                            onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                            onwardPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                            onwardPax1.FlyDubaiBaggageCharge.Add(0);
                                            onwardPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                                        }
                                        else
                                        {
                                            onwardPax1.Price.BaggageCharge = 0;
                                            onwardPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                            onwardPax1.FlyDubaiBaggageCharge.Add(0);
                                            onwardPax1.FlyDubaiBaggageCharge.Add(0);
                                        }

                                        //In return journey if both onward and return additional baggage are available.
                                        if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible && ddlOnwardBaggage.SelectedIndex > 0 && ddlInwardBaggage.SelectedIndex > 0)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlOnwardBaggage.SelectedItem.Text.Split('-')[1] + "'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                            if (ddlInwardBaggage.SelectedIndex > 0)
                                            {
                                                baggage = dtBaggageInfo.Select("CodeType='" + ddlInwardBaggage.SelectedItem.Text.Split('-')[1] + "'");

                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    onwardPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                    onwardPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                        }
                                        else if (ddlOnwardBaggage.Visible && ddlOnwardBaggage.SelectedIndex > 0) // If only onward additional baggage is available.
                                        {
                                            if (ddlOnwardBaggage.SelectedIndex > 0)
                                            {
                                                DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlOnwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    onwardPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                    onwardPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                        }
                                        else if (ddlInwardBaggage.Visible && ddlInwardBaggage.SelectedIndex > 0 && request.Type == SearchType.Return) // If only return baggage is available.
                                        {
                                            if (ddlInwardBaggage.SelectedIndex > 0 && request.Type == SearchType.Return)
                                            {
                                                DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlInwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    onwardPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                    onwardPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                        }

                                        AssignAirIndiaExpressPaxBaggage(onwardPax1, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                                    }
                                    else if (request.Type == SearchType.OneWay)
                                    {
                                        if (ddlOnwardBaggage.Visible)
                                        {
                                            onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue);
                                            onwardPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                            onwardPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                        }
                                        else
                                        {
                                            onwardPax1.Price.BaggageCharge = 0;
                                            onwardPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                            onwardPax1.FlyDubaiBaggageCharge.Add(0);
                                        }
                                        if (ddlOnwardBaggage.Visible && ddlOnwardBaggage.SelectedIndex > 0)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlOnwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                onwardPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                onwardPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }

                                        AssignAirIndiaExpressPaxBaggage(onwardPax1, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                                    }
                                }
                            }
                        }
                        #endregion

                        #region GoAir Additional Baggage
                        if ((onwardItinerary.FlightBookingSource == BookingSource.GoAir || onwardItinerary.FlightBookingSource == BookingSource.GoAirCorp) && onwardPax1.Type != PassengerType.Infant)
                        {
                            DataTable dtBaggage = null;
                            if (onwardResult == null)
                            {
                                onwardResult = SessionValues[ONWARD_RESULT] as SearchResult;
                            }
                            if (SessionValues[ONWARD_BAGGAGE] != null)
                            {
                                dtBaggage = dtBaggage = SessionValues[ONWARD_BAGGAGE] as DataTable;
                            }
                            if (dtBaggage != null && dtBaggage.Rows.Count > 0 && ddlOnwardBaggage.SelectedIndex > 0)
                            {
                                DataRow[] data = dtBaggage.Select("Description='" + ddlOnwardBaggage.SelectedItem.Text + "'");
                                if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                                {
                                    onwardPax1.BaggageType = data[0]["Code"].ToString();
                                    onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                                    string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                        }
                                    }
                                }
                                else
                                {
                                    onwardPax1.BaggageType = data[0]["Code"].ToString();
                                    onwardPax1.BaggageCode = ddlOnwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    onwardPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue.Split('-')[0]);
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(onwardResult.BaggageIncludedInFare) && Convert.ToInt32(onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                                {
                                    string[] segmentBaggageDetails = onwardResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = segmentBaggageDetails[i] + "Kg";
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg";
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(onwardPax1.BaggageCode))
                                        {
                                            onwardPax1.BaggageCode = "No Bag";
                                        }
                                        else
                                        {
                                            onwardPax1.BaggageCode += "," + "No Bag";
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region  Meal Selection for Indigo , SpiceJet ,GoAir For AdditionalPax

                        if ((onwardItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardItinerary.FlightBookingSource == BookingSource.Indigo || onwardItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || onwardItinerary.FlightBookingSource == BookingSource.IndigoCorp || onwardItinerary.FlightBookingSource == BookingSource.GoAir || onwardItinerary.FlightBookingSource == BookingSource.GoAirCorp) && SessionValues[ONWARD_BAGGAGE] != null && onwardPax1.Type != PassengerType.Infant)
                        {
                            DataTable dtBaggage = SessionValues[ONWARD_BAGGAGE] as DataTable;
                            if (!string.IsNullOrEmpty(hdnOutMealSelection.Value) && hdnOutMealSelection.Value.Length > 0)
                            {
                                string[] onwardSelection = hdnOutMealSelection.Value.Split(',');
                                if (Convert.ToInt32(onwardSelection[counter]) > 0)
                                {
                                    DataRow[] data = dtBaggage.Select("Description='" + ddlOnwardMeal.Items[Convert.ToInt32(onwardSelection[counter])].Text + "'");
                                    if (data != null && data.Length > 0)
                                    {
                                        onwardPax1.MealType = data[0]["Code"].ToString();
                                        onwardPax1.Price.MealCharge += Convert.ToDecimal(ddlOnwardMeal.Items[Convert.ToInt32(onwardSelection[counter])].Value.Split('-')[0]);
                                    }
                                    for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                    {
                                        if (!string.IsNullOrEmpty(onwardPax1.MealDesc))
                                        {
                                            onwardPax1.MealDesc += ',' + ddlOnwardMeal.Items[Convert.ToInt32(onwardSelection[counter])].Text;
                                        }
                                        else
                                        {
                                            onwardPax1.MealDesc = ddlOnwardMeal.Items[Convert.ToInt32(onwardSelection[counter])].Text;
                                        }
                                    }
                                }
                                else //No Onward Meal Selected
                                {
                                    for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                    {
                                        if (!string.IsNullOrEmpty(onwardPax1.MealDesc))
                                        {
                                            onwardPax1.MealDesc += ',' + "No Meal";
                                        }
                                        else
                                        {
                                            onwardPax1.MealDesc = "No Meal";
                                        }
                                    }
                                }
                            }
                            else //No Onward Meal Selected
                            {
                                for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(onwardPax1.MealDesc))
                                    {
                                        onwardPax1.MealDesc += ',' + "No Meal";
                                    }
                                    else
                                    {
                                        onwardPax1.MealDesc = "No Meal";
                                    }
                                }
                            }

                        }


                        #endregion

                        if (onwardItinerary.FlightBookingSource == BookingSource.AirArabia)
                        {
                            DataTable dtBaggage = SessionValues.ContainsKey("OnwardG9Meal") ? SessionValues[ONWARD_G9MEALS] as DataTable : null;
                            if (ddlOnwardMeal.SelectedIndex > 0 && dtBaggage != null && dtBaggage.Rows.Count > 0)
                            {

                                DataRow[] data = dtBaggage.Select("mealDescription='" + ddlOnwardMeal.SelectedItem.Text + "'");
                                if (data != null && data.Length > 0)
                                {
                                    onwardPax1.MealType = data[0]["mealCode"].ToString();
                                    onwardPax1.Price.MealCharge += Convert.ToDecimal(ddlOnwardMeal.SelectedValue.Split('-')[1]);
                                }
                                for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(onwardPax1.MealDesc))
                                    {
                                        onwardPax1.MealDesc += ',' + ddlOnwardMeal.SelectedItem.Text;
                                    }
                                    else
                                    {
                                        onwardPax1.MealDesc = ddlOnwardMeal.SelectedItem.Text;
                                    }
                                }
                            }
                            else //No Onward Meal Selected
                            {
                                for (int i = 0; i < onwardResult.Flights[0].Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(onwardPax1.MealDesc))
                                    {
                                        onwardPax1.MealDesc += ',' + "No Meal";
                                    }
                                    else
                                    {
                                        onwardPax1.MealDesc = "No Meal";
                                    }
                                }
                                onwardPax1.MealType = "No Meal";
                            }
                        }
                        
                        //Salam Air is not permiting additional baggage so only adding default baggage to Addl Pax
                        #region Salam Air deafult baggage for Additional pax
                        if (onwardItinerary.FlightBookingSource == BookingSource.SalamAir && onwardPax1.Type != PassengerType.Infant)
                        {
                            AssignSalamAirBagCodes(onwardPax1, onwardResult.BaggageIncludedInFare);
                        }
                        #endregion
                        //Flex Details saving
                        if (arrPaxFlex != null && arrPaxFlex.Length > 0)
                            onwardPax1.FlexDetailsList = arrPaxFlex.Where(x => x.PaxId == counter && !string.IsNullOrEmpty(x.FlexLabel)).ToList();

                        //if (tblFlexFieldsAd != null && Convert.ToInt32(hdnFlexCount.Value) > 0)
                        //{
                        //    onwardPax1.FlexDetailsList = new List<FlightFlexDetails>();

                        //    for (int i = 0; i < Convert.ToInt32(hdnFlexCount.Value); i++)
                        //    {
                        //        FlightFlexDetails flexDetails = new FlightFlexDetails();
                        //        HiddenField hdnFlexControl = tblFlexFieldsAd.FindControl("hdnFlexControlAd" + i) as HiddenField;
                        //        HiddenField hdnFlexId = tblFlexFieldsAd.FindControl("hdnFlexIdAd" + i) as HiddenField;
                        //        HiddenField hdnFlexLabel = tblFlexFieldsAd.FindControl("hdnFlexLabelAd" + i) as HiddenField;
                        //        flexDetails.FlexId = Convert.ToInt32(hdnFlexId.Value);
                        //        flexDetails.FlexLabel = hdnFlexLabel.Value;
                        //        switch (hdnFlexControl.Value)
                        //        {
                        //            case "T":
                        //                TextBox txtFlex = tblFlexFieldsAd.FindControl("txtFlexAd" + i) as TextBox;
                        //                flexDetails.FlexData = txtFlex.Text;
                        //                break;
                        //            case "D":
                        //                DropDownList ddlAdPaxDay = tblFlexFieldsAd.FindControl("ddlDayAd" + i) as DropDownList;
                        //                DropDownList ddlAdPaxMonth = tblFlexFieldsAd.FindControl("ddlMonthAd" + i) as DropDownList;
                        //                DropDownList ddlAdPaxYear = tblFlexFieldsAd.FindControl("ddlYearAd" + i) as DropDownList;
                        //                flexDetails.FlexData = (ddlAdPaxDay.SelectedIndex > 0 && ddlAdPaxMonth.SelectedIndex > 0 && ddlAdPaxYear.SelectedIndex > 0) ?
                        //                    Convert.ToString(Convert.ToDateTime(ddlAdPaxDay.SelectedValue + "/" + ddlAdPaxMonth.SelectedValue + "/" + ddlAdPaxYear.SelectedValue, dateFormat)) : string.Empty;
                        //                break;
                        //            case "L":
                        //                DropDownList ddlFlex = tblFlexFieldsAd.FindControl("ddlFlexAd" + i) as DropDownList;
                        //                flexDetails.FlexData = ddlFlex.SelectedIndex > 0 ? ddlFlex.SelectedItem.Value : string.Empty;
                        //                break;
                        //        }
                        //        flexDetails.CreatedBy = (int)Settings.LoginInfo.UserID;
                        //        flexDetails.ProductID = 1;
                        //        onwardPax1.FlexDetailsList.Add(flexDetails);
                        //    }
                        //}

                        onwardPaxList[counter] = onwardPax1;

                        counter++;
                    }

                    onwardItinerary.Passenger = onwardPaxList;


                    //Assign the Flight Policy created
                    onwardItinerary.FlightPolicy = GenerateFlightPolicy(onwardResult);

                    //Create the Itinerary for two additional results 
                    //if (Settings.LoginInfo.IsCorporate == "Y" && Settings.LoginInfo.CorporateProfileId > 0 && request.AppliedPolicy)
                    if (false && Settings.LoginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId > 0 && request.AppliedPolicy)
                    {
                        GenerateItineraryForCorporateCriteria();
                    }

                    FlightItinerary tmponward = SessionValues[ONWARD_FLIGHT_ITINERARY] != null ? SessionValues[ONWARD_FLIGHT_ITINERARY] as FlightItinerary : null;
                    FlightItinerary tmpretrun = SessionValues[RETURN_FLIGHT_ITINERARY] != null ? SessionValues[RETURN_FLIGHT_ITINERARY] as FlightItinerary : null;

                    onwardItinerary.BookRequestType = tmponward != null ? tmponward.BookRequestType : onwardItinerary.BookRequestType;
                    onwardItinerary.ItineraryAmountDue = tmponward != null ? tmponward.ItineraryAmountDue : onwardItinerary.ItineraryAmountDue;

                    //Need to assign the tax breakup of individual pax from the onwardResult object.
                    //Reason: Only from the price itinerary response we will get the individual tax break up .
                    //This applies only for SG and 6E.

                    if (onwardResult != null &&
                        (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp || onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp)
                        && !string.IsNullOrEmpty(Convert.ToString(Session["sessionId"])) && (!string.IsNullOrEmpty(txtTaxRegNo_SG_GST.Text) && txtTaxRegNo_SG_GST.Text.Length > 0) && (!string.IsNullOrEmpty(txtCompanyName_SG_GST.Text) && txtCompanyName_SG_GST.Text.Length > 0))
                    {
                        if (onwardItinerary != null)
                        {
                            CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(Session["sessionId"].ToString());
                            mse.SettingsLoginInfo = Settings.LoginInfo;
                            //If the onward result is not special round trip fare type
                            if ((onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp) && !onwardResult.IsSpecialRoundTrip && tmponward == null)
                            {
                                mse.GetSGUpdatedGSTFareBreakUp(ref onwardResult, request);
                            }
                            //If the onward result is not special round trip fare type
                            else if ((onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp) && !onwardResult.IsSpecialRoundTrip && tmponward == null)
                            {
                                mse.Get6EUpdatedGSTFareBreakUp(ref onwardResult, request);
                            }
                            else if ((onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp) && tmponward == null)
                            {
                                mse.GetG8UpdatedGSTFareBreakUp(ref onwardResult, request);
                            }

                            if (!onwardResult.IsSpecialRoundTrip && tmponward == null)
                            {
                                CalculatePriceComponent(ref onwardResult, request);
                                k3tax = onwardResult.Price.K3Tax / paxCount;
                                foreach (FlightPassenger passenger in onwardItinerary.Passenger)
                                {
                                    for (int i = 0; i < onwardResult.FareBreakdown.Length; i++)
                                    {
                                        if (passenger.Type != PassengerType.Infant && passenger.Type.ToString() == onwardResult.FareBreakdown[i].PassengerType.ToString())
                                        {
                                            passenger.Price.Tax = (decimal)onwardResult.FareBreakdown[i].Tax / onwardResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.Markup = onwardResult.FareBreakdown[i].AgentMarkup / onwardResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.Discount = Convert.ToDecimal(onwardResult.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue)) / onwardResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.HandlingFeeAmount = onwardResult.FareBreakdown[i].HandlingFee / onwardResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.HandlingFeeValue = onwardResult.Price.HandlingFeeValue;
                                            passenger.Price.K3Tax=k3tax;

                                        }
                                    }

                                }
                            }
                        }
                    }

                    if (onwardResult != null && (onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.IndigoCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp || onwardResult.ResultBookingSource == BookingSource.Jazeera))
                    {
                        if (onwardItinerary != null && onwardItinerary.Passenger != null && onwardItinerary.Passenger.Length > 0 && onwardResult.Price != null && onwardResult.Price.PaxTypeTaxBreakUp != null && onwardResult.Price.PaxTypeTaxBreakUp.Count > 0)
                        {
                            //ADT -- Adult pax tax break up.
                            //CHD -- Child pax tax break up.
                            //INF -- Infant pax tax break up.

                            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                            List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

                            foreach (KeyValuePair<string, List<KeyValuePair<string, decimal>>> breakup in onwardResult.Price.PaxTypeTaxBreakUp)
                            {
                                if (breakup.Key == "ADT" && breakup.Value.Count > 0)
                                {
                                    adtPaxTaxBreakUp.AddRange(breakup.Value);
                                }
                                else if (breakup.Key == "CHD" && breakup.Value.Count > 0)
                                {
                                    chdPaxTaxBreakUp.AddRange(breakup.Value);
                                }
                                else if (breakup.Key == "INF" && breakup.Value.Count > 0)
                                {
                                    inftPaxTaxBreakUp.AddRange(breakup.Value);
                                }
                            }

                            for (int i = 0; i < onwardItinerary.Passenger.Length; i++)
                            {
                                if (onwardItinerary.Passenger[i].Type == PassengerType.Adult && adtPaxTaxBreakUp.Count > 0)
                                {
                                    onwardItinerary.Passenger[i].TaxBreakup = adtPaxTaxBreakUp;
                                }
                                else if (onwardItinerary.Passenger[i].Type == PassengerType.Child && chdPaxTaxBreakUp.Count > 0)
                                {
                                    onwardItinerary.Passenger[i].TaxBreakup = chdPaxTaxBreakUp;
                                }
                                else if (onwardItinerary.Passenger[i].Type == PassengerType.Infant && inftPaxTaxBreakUp.Count > 0)
                                {
                                    onwardItinerary.Passenger[i].TaxBreakup = inftPaxTaxBreakUp;
                                }
                            } 
                        }
                    }

                    //Added by lokesh on 24/12/2018.
                    //For SpiceJet and Indigo Till Search To Book One Signature Should be carried
                    //We are not assigning from the basket.
                    //We assign the signature to the result object to the property RepriceErrorMessage.
                    if (onwardResult != null && onwardItinerary != null && (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp || onwardResult.ResultBookingSource == BookingSource.Jazeera || onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp))
                    {
                        if (!string.IsNullOrEmpty(onwardResult.RepriceErrorMessage))
                        {
                            onwardItinerary.Endorsement = onwardResult.RepriceErrorMessage;
                        }

                    }
                    //If both the results are Special Round Trip Fares Then Allow Booking and Tiketing for the first itinerary
                    if (onwardResult.IsSpecialRoundTrip)
                    {
                        onwardItinerary.IsSpecialRoundTrip = true;
                    }
                    SaveInSession(ONWARD_FLIGHT_ITINERARY, onwardItinerary);
                    if (onwardResult.ResultBookingSource != BookingSource.TBOAir)
                    {
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        ////////////////////////////// Agent Currency, Rate of Exchange & Fare Check up ///////////////////////////////////////
                        StaticData sd = new StaticData();
                        sd.BaseCurrency = agency.AgentCurrency;

                        decimal roe = sd.CurrencyROE[onwardResult.Price.SupplierCurrency];
                        decimal baseFare = 0, tax = 0, resultBaseFare = 0, resultTax = 0;

                        if (onwardResult.Price.MarkupType == "P")
                        {
                            if (agency.AgentAirMarkupType == "BF")
                            {
                                baseFare = ((decimal)onwardResult.Price.PublishedFare) * roe;
                                baseFare += (baseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                                resultBaseFare = ((decimal)onwardResult.Price.PublishedFare) * rateOfExchange;
                                resultBaseFare += (resultBaseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                            }
                            else if (agency.AgentAirMarkupType == "TX")
                            {
                                baseFare = ((decimal)onwardResult.Price.Tax / roe) * roe;
                                baseFare += (baseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                                resultBaseFare = ((decimal)onwardResult.Price.Tax / rateOfExchange) * roe;
                                resultBaseFare += (resultBaseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                            }
                            else
                            {
                                baseFare = ((decimal)onwardResult.Price.SupplierPrice) * roe;
                                baseFare += (baseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                                resultBaseFare = ((decimal)onwardResult.Price.SupplierPrice) * rateOfExchange;
                                resultBaseFare += (resultBaseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                            }
                        }
                        else
                        {
                            baseFare = (onwardResult.Price.SupplierPrice * roe) + (onwardResult.Price.MarkupValue * paxCount);
                            resultBaseFare = (onwardResult.Price.SupplierPrice * rateOfExchange) + (onwardResult.Price.MarkupValue * paxCount);
                        }

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if (agency.AgentCurrency == onwardResult.Currency)
                        {
                            SaveInSession(ONWARD_FLIGHT_ITINERARY, onwardItinerary);
                            //SaveInSession(RETURN_FLIGHT_ITINERARY, null);

                            if (rateOfExchange == roe && Math.Round(resultBaseFare, agency.DecimalValue) >= Math.Round(baseFare, agency.DecimalValue))
                            {
                                if (onwardResult != null && returnResult == null)//ONE WAY
                                {
                                    BookingResponse clsBookingResponse = UpdatePaxAndAssignSeats(onwardItinerary, null);
                                    if (string.IsNullOrEmpty(clsBookingResponse.Error))
                                    {
                                        Session["Agent"] = null;
                                        RemoveFromSession(ONWARD_BAGGAGE);
                                        RemoveFromSession(RETURN_BAGGAGE);
                                        string priceChange = string.Empty;
                                        if (SessionValues.ContainsKey(ONWARD_SSR_PRICE_CHANGE) && SessionValues[ONWARD_SSR_PRICE_CHANGE] != null)
                                            priceChange = "&onward=" + SessionValues[ONWARD_SSR_PRICE_CHANGE].ToString();

                                        onwardItinerary.AirLocatorCode = string.Empty;
                                        Response.Redirect("PaymentConfirmationBySegments.aspx", false);
                                    }
                                    else
                                    {
                                        CT.TicketReceipt.Common.Utility.StartupScript(this, "Showseatalert('" + clsBookingResponse.Error + "');", "script");
                                    }
                                }
                            }
                            else
                            {
                                //Show message
                                lblMessage.Text = "Price has been revised for the Flight. Please search again or contact Customer Support.";
                            }
                        }
                        else
                        {
                            Session["ValuesChanged"] = null;//Clear the values used for UAPI Repricing
                            lblMessage.Text = "Price has been revised for the Flight. Please search again or contact Customer Support.";
                        }
                    }
                    else
                    {
                        Response.Redirect("PaymentConfirmationBySegments.aspx?id=" + Request.QueryString["id"], false);
                    }
                }

                if (returnResult != null)
                {
                    FlightItinerary returnItinerary = new FlightItinerary();
                    decimal k3tax = returnResult.Price.K3Tax / paxCount;
                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        returnItinerary.AgencyId = Settings.LoginInfo.OnBehalfAgentID;
                        returnItinerary.LocationId = Settings.LoginInfo.OnBehalfAgentLocation;
                        if (Settings.LoginInfo.OnBehalfAgentExchangeRates.ContainsKey(onwardResult.Price.SupplierCurrency))
                        {
                            rateOfExchange = Settings.LoginInfo.OnBehalfAgentExchangeRates[onwardResult.Price.SupplierCurrency];
                        }
                        else
                        {
                            rateOfExchange = 1;
                        }
                    }
                    else
                    {

                        returnItinerary.LocationId = Convert.ToInt32(Settings.LoginInfo.LocationID);
                        returnItinerary.AgencyId = Settings.LoginInfo.AgentId;
                        if (Settings.LoginInfo.AgentExchangeRates.ContainsKey(onwardResult.Price.SupplierCurrency))
                        {
                            rateOfExchange = Settings.LoginInfo.AgentExchangeRates[onwardResult.Price.SupplierCurrency];
                        }
                        else
                        {
                            rateOfExchange = 1;
                        }
                    }

                    returnItinerary.BookingMode = BookingMode.BookingAPI;
                    returnItinerary.CreatedBy = (int)Settings.LoginInfo.UserID;
                    returnItinerary.CreatedOn = DateTime.Now;
                    returnItinerary.ProductType = ProductType.Flight;

                    if (!string.IsNullOrEmpty(returnResult.LastTicketDate))
                    {
                        returnItinerary.LastTicketDate = Convert.ToDateTime(returnResult.LastTicketDate);
                    }

                    returnItinerary.Destination = request.Segments[0].Destination;
                    returnItinerary.FareRules = returnResult.FareRules;
                    returnItinerary.FareType = (returnResult.FareType != null ? returnResult.FareType : FareType.Net.ToString());
                    returnItinerary.FlightBookingSource = returnResult.ResultBookingSource;
                    returnItinerary.FlightId = returnResult.Flights[0][0].FlightId;
                    returnItinerary.ETicketHit = 1;
                    returnItinerary.IsDomestic = false;
                    returnItinerary.NonRefundable = returnResult.NonRefundable;
                    returnItinerary.Origin = request.Segments[0].Destination;
                    returnItinerary.PaymentMode = ModeOfPayment.Credit;
                    returnItinerary.GUID = returnResult.GUID;
                    returnItinerary.SpecialRequest = returnResult.JourneySellKey;
                    returnItinerary.TicketAdvisory = returnResult.FareSellKey;

                    returnItinerary.Segments = SearchResult.GetSegments(returnResult);
                    foreach (FlightInfo segment in returnItinerary.Segments)
                    {
                        segment.CreatedBy = (int)Settings.LoginInfo.UserID;
                        segment.CreatedOn = DateTime.Now;
                        segment.LastModifiedBy = segment.CreatedBy;
                        segment.LastModifiedOn = segment.CreatedOn;
                        if (segment.BookingClass.Trim().Length <= 0)
                        {
                            segment.BookingClass = "C";
                        }
                    }

                    returnItinerary.Ticketed = true;
                    returnItinerary.TravelDate = returnResult.Flights[0][0].DepartureTime;
                    returnItinerary.UapiPricingSolution = returnResult.UapiPricingSolution;
                    returnItinerary.ValidatingAirlineCode = "";
                    returnItinerary.IsLCC = returnResult.IsLCC;

                    ////////////////////////TBO Air changes //////////////////////////

                    if (returnResult.ResultBookingSource == BookingSource.TBOAir)
                    {
                        returnItinerary.SupplierLocatorCode = returnResult.FareType;//LCCSpecialReturn/Normal Return/GDS Special Return
                        returnItinerary.TicketAdvisory = returnResult.FareSellKey;//SessionId of the onwardResult
                        returnItinerary.UniversalRecord = returnResult.JourneySellKey;//Save TBO Sources with | symbol
                        returnItinerary.FareType = "PUB";
                        //Assign GST related fields for TBOAir
                        if (returnResult.ResultBookingSource == BookingSource.TBOAir && returnResult.IsGSTMandatory)
                        {
                            returnItinerary.GstCompanyAddress = txtGSTCompanyAddress.Text;
                            returnItinerary.GstCompanyContactNumber = txtGSTContactNumber.Text;
                            returnItinerary.GstCompanyEmail = txtGSTCompanyEmail.Text;
                            returnItinerary.GstCompanyName = txtGSTCompanyName.Text;
                            returnItinerary.GstNumber = txtGSTNumber.Text;
                        }
                    }
                    //////////////////// End TBO Air changes /////////////////////////

                    if (returnResult.ResultBookingSource == BookingSource.FlightInventory)
                    {
                        returnItinerary.GUID = returnResult.GUID;//(SeatQuota)
                        returnItinerary.PNR = returnResult.FareSellKey;//(PNR's)
                        returnItinerary.SpecialRequest = returnResult.JourneySellKey;//(InventoryId)
                    }
                    //Default Mandatory Fields are : Title,FirstName,LastName,Phone,Email
                    //Conditionary Mandate Fields are :Address,Country,DOB,Gender,Nationality,PassportExpiry,PassportNo

                    if (returnResult.ResultBookingSource == BookingSource.UAPI && requiredGSTForUAPI)
                    {
                        returnItinerary.GstCompanyAddress = txtGSTCompanyAddress.Text;
                        returnItinerary.GstCompanyContactNumber = txtGSTContactNumber.Text;
                        returnItinerary.GstCompanyEmail = txtGSTCompanyEmail.Text;
                        returnItinerary.GstCompanyName = txtGSTCompanyName.Text;
                        returnItinerary.GstNumber = txtGSTNumber.Text;
                    }

                    FlightPassenger returnPax = new FlightPassenger();
                    returnPax.CorpProfileId = chkAddPax.Checked && string.IsNullOrEmpty(hdnProfileId.Value) ? "RN" : hdnProfileId.Value;
                    returnPax.Title = ddlTitle.SelectedValue.Replace(".", "");
                    returnPax.FirstName = txtPaxFName.Text;
                    returnPax.LastName = txtPaxLName.Text;
                    returnPax.Type = PassengerType.Adult;
                    //Check whether address is a mandatory or non mandatory field and assign as per the condition.
                    //Remove any new line chars if any as TBOAir will not allow booking
                    returnPax.AddressLine1 = (txtAddress1.Text.Length == 0 ? "Shj" : txtAddress1.Text.Replace(Environment.NewLine, "").Replace("\\n", "").Replace("\n", ""));
                    returnPax.AddressLine2 = (txtAddress2.Text.Length == 0 ? "Shj" : txtAddress2.Text.Replace(Environment.NewLine, "").Replace("\\n", "").Replace("\n", ""));
                    returnPax.CellPhone = txtMobileCountryCode.Text + '-' + txtMobileNo.Text;
                    returnPax.City = "";
                    //Check whether Country is a mandatory or non mandatory field and assign as per the condition.
                    returnPax.Country = (ddlCountry.SelectedIndex == 0 ? Country.GetCountry("AE") : Country.GetCountry(ddlCountry.SelectedItem.Value));
                    returnPax.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                    returnPax.CreatedOn = DateTime.Now;
                    Airport destinationAirport = new Airport(request.Segments[0].Destination);//Search Type : One Way or Return.
                    Airport originAirport = new Airport(request.Segments[0].Origin);
                    //If TBOAir Itinerary is booked India origin and destination then DOB should not be assigned as it not mandatory
                    if (originAirport.CountryCode == "IN" && destinationAirport.CountryCode == "IN" && onwardResult.ResultBookingSource == BookingSource.TBOAir)
                    {
                        returnPax.DateOfBirth = ((ddlDay.SelectedIndex > 0) && (ddlMonth.SelectedIndex > 0) && (ddlYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlDay.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue, dateFormat) : DateTime.MinValue);
                    }
                    else
                    {
                        //Check whether DateOfBirth is a mandatory or non mandatory field and assign as per the condition.
                        returnPax.DateOfBirth = ((ddlDay.SelectedIndex > 0) && (ddlMonth.SelectedIndex > 0) && (ddlYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlDay.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue, dateFormat) : DateTime.Now.AddYears(-45));
                    }

                    returnPax.Email = txtEmail.Text;
                    returnPax.FFAirline = txtAirline.Text;
                    returnPax.FFNumber = txtFlight.Text;
                    returnPax.FlightId = returnResult.Flights[0][0].FlightId;
                    //Check Gender DateOfBirth is a mandatory or non mandatory field and assign as per the condition.
                    returnPax.Gender = (ddlGender.SelectedIndex > 0 ? (Gender)Convert.ToInt32(ddlGender.SelectedValue) : Gender.Male);
                    returnPax.IsLeadPax = true;
                    returnPax.Meal = new Meal();
                    //Check whether Nationality is a mandatory or non mandatory field and assign as per the condition.
                    returnPax.Nationality = (ddlNationality.SelectedIndex == 0 ? Country.GetCountry("AE") : Country.GetCountry(ddlNationality.SelectedValue));
                    //Lead Pax :Irrespective of whether  PassportExpiry and PassportNo are  mandatory or not mandatory.
                    //By chance if the user enters any passport related information need to capture in that case.
                    if (txtPassportNo.Text.Length > 0 && !string.IsNullOrEmpty(txtPassportNo.Text))
                    {

                        returnPax.PassportExpiry = ((ddlPEDay.SelectedIndex > 0) && (ddlPEMonth.SelectedIndex > 0) && (ddlPEYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPEDay.SelectedValue + "/" + ddlPEMonth.SelectedValue + "/" + ddlPEYear.SelectedValue, dateFormat) : DateTime.Now.AddYears(1));
                        returnPax.PassportNo = txtPassportNo.Text;
                    }

                    //Added by lokesh on 25-sept-2017 regarding pax destination phone.
                    if (txtDestCntCode.Text.Length > 0 && txtDestPhoneNo.Text.Length > 0)
                    {
                        returnPax.DestinationPhone = txtDestCntCode.Text + '-' + txtDestPhoneNo.Text;
                    }
                    else
                    {
                        returnPax.DestinationPhone = string.Empty;
                    }



                    if (returnResult != null && returnResult.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia)
                    {
                        /* Using AliasAirlineCode property to maintain resultkey and to retrieve air arabia booking session data */
                        returnItinerary.AliasAirlineCode = returnResult.ResultKey;

                        if (!string.IsNullOrEmpty(hdnSelStateId.Value) && Convert.ToString(hdnSelStateId.Value).Length > 0 && hdnSelStateId.Value != "-1" && returnPax.Country.CountryCode == "IN")
                        {
                            returnPax.GSTStateCode = hdnSelStateId.Value;
                        }
                        else
                        {
                            returnPax.GSTStateCode = string.Empty;
                        }
                        if (!string.IsNullOrEmpty(txtGSTRegNum.Text) && txtGSTRegNum.Text.Length > 0)
                        {
                            returnPax.GSTTaxRegNo = txtGSTRegNum.Text;
                        }
                        else
                        {
                            returnPax.GSTTaxRegNo = string.Empty;
                        }
                    }
                    //If the customer origin country code is india
                    //Capture tax reg number for spice jet source
                    //Capture company name for spice jet source
                    //it is not mandatory field.it depends upon choice.
                    else if (returnResult != null && (returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.IndigoCorp || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp) && (returnResult.Flights[0][0].Origin.CountryCode == "IN") && !string.IsNullOrEmpty(txtTaxRegNo_SG_GST.Text) && txtTaxRegNo_SG_GST.Text.Length > 0 && !string.IsNullOrEmpty(txtCompanyName_SG_GST.Text) && txtCompanyName_SG_GST.Text.Length > 0)
                    {
                        returnPax.GSTTaxRegNo = txtTaxRegNo_SG_GST.Text;
                        returnPax.GSTStateCode = txtCompanyName_SG_GST.Text;
                    }
                    else//For Remaining Air Sources the below 2 properties will be empty.
                    {
                        returnPax.GSTStateCode = string.Empty;
                        returnPax.GSTTaxRegNo = string.Empty;
                    }

                    returnPax.Seat = new Seat();

                    returnPax.Price = new PriceAccounts();
                    returnPax.Price.TaxDetails = returnResult.Price.TaxDetails;
                    returnPax.Price.InputVATAmount = returnResult.Price.InputVATAmount / paxCount;
                    returnPax.Price.AccPriceType = (onwardResult.FareType == "NetFare" ? PriceType.NetFare : PriceType.PublishedFare);
                    if (returnPax.Price.AccPriceType == PriceType.NetFare)
                    {
                        returnPax.Price.NetFare = (decimal)returnResult.FareBreakdown[0].BaseFare / returnResult.FareBreakdown[0].PassengerCount;
                    }
                    else
                    {
                        returnPax.Price.PublishedFare = (decimal)returnResult.FareBreakdown[0].BaseFare / returnResult.FareBreakdown[0].PassengerCount;
                    }
                    returnPax.Price.SupplierCurrency = onwardResult.Price.SupplierCurrency;

                    if (returnResult.ResultBookingSource == BookingSource.TBOAir)
                    {
                        returnPax.Price.SupplierPrice = (decimal)returnResult.Price.SupplierPrice;
                    }
                    else
                    {
                        returnPax.Price.SupplierPrice = (decimal)returnResult.FareBreakdown[0].SupplierFare / returnResult.FareBreakdown[0].PassengerCount;
                    }
                    returnPax.Price.Markup = returnResult.Price.Markup;
                    returnPax.Price.MarkupType = returnResult.Price.MarkupType;
                    returnPax.Price.MarkupValue = returnResult.Price.MarkupValue;
                    returnPax.Price.DecimalPoint = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentDecimalValue : Settings.LoginInfo.DecimalValue);
                    returnPax.Price.Tax = Convert.ToDecimal(returnResult.FareBreakdown[0].Tax.ToString("N"+agency.DecimalValue)) / returnResult.FareBreakdown[0].PassengerCount;
                    returnPax.Price.K3Tax = Convert.ToDecimal(k3tax.ToString("N"+agency.DecimalValue));
                    returnPax.Price.Markup = Convert.ToDecimal(returnResult.FareBreakdown[0].AgentMarkup.ToString("N"+agency.DecimalValue)) / returnResult.FareBreakdown[0].PassengerCount;
                    returnPax.Price.Discount = Convert.ToDecimal(returnResult.FareBreakdown[0].AgentDiscount.ToString("N" + agency.DecimalValue)) / returnResult.FareBreakdown[0].PassengerCount;
                    returnPax.Price.DiscountType = returnResult.Price.DiscountType;
                    returnPax.Price.DiscountValue = returnResult.Price.DiscountValue;
                    returnPax.Price.HandlingFeeAmount = Convert.ToDecimal(returnResult.FareBreakdown[0].HandlingFee.ToString("N"+agency.DecimalValue)) / returnResult.FareBreakdown[0].PassengerCount;
                    returnPax.Price.HandlingFeeType = returnResult.Price.HandlingFeeType;
                    returnPax.Price.HandlingFeeValue = returnResult.Price.HandlingFeeValue;
                    returnPax.Price.RateOfExchange = rateOfExchange;
                    returnPax.Price.Currency = onwardResult.Currency;
                    returnPax.Price.CurrencyCode = onwardResult.Currency;
                    returnPax.Price.WhiteLabelDiscount = 0;

                    if (returnResult.ResultBookingSource == BookingSource.FlyDubai || returnResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl || returnResult.ResultBookingSource==BookingSource.SalamAir)
                    {
                        returnPax.Price.TransactionFee = returnResult.Price.TransactionFee;
                    }
                    else
                    {
                        returnPax.Price.TransactionFee = 0;
                    }

                    returnPax.Price.AdditionalTxnFee = returnResult.FareBreakdown[0].AdditionalTxnFee;

                    if (returnResult.ResultBookingSource == BookingSource.FlyDubai || returnResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl || returnResult.ResultBookingSource==BookingSource.SalamAir)
                    {
                        returnPax.Price.FareInformationID = returnResult.FareInformationId[PassengerType.Adult.ToString()];
                    }


                    if (returnItinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        returnPax.Price.ChargeBU = returnResult.Price.ChargeBU;
                        returnPax.Price.YQTax = returnResult.Price.YQTax;
                        returnPax.Price.AgentCommission = returnResult.Price.AgentCommission;
                        returnPax.Price.SServiceFee = returnResult.Price.SServiceFee;
                        returnPax.Price.IncentiveEarned = returnResult.Price.IncentiveEarned;
                        returnPax.Price.TDSIncentive = returnResult.Price.TDSIncentive;
                        returnPax.Price.AgentPLB = returnResult.Price.AgentPLB;
                        returnPax.Price.TDSPLB = returnResult.Price.TDSPLB;
                        returnPax.Price.TdsCommission = returnResult.Price.TdsCommission;
                        returnPax.Price.AdditionalTxnFee = returnResult.Price.AdditionalTxnFee;
                        returnPax.Price.TransactionFee = returnResult.Price.TransactionFee;
                        returnPax.Price.OtherCharges = returnResult.Price.OtherCharges;
                        returnPax.Price.SupplierCurrency = returnResult.Price.SupplierCurrency;

                        returnPax.TBOPrice = new PriceAccounts();
                        returnPax.TBOPrice.AccPriceType = PriceType.PublishedFare;
                        returnPax.TBOPrice.PublishedFare = (decimal)returnResult.TBOPrice.PublishedFare / paxCount;
                        returnPax.TBOPrice.BaseFare = returnResult.TBOPrice.BaseFare;
                        returnPax.TBOPrice.Tax = (decimal)returnResult.TBOPrice.Tax;
                        returnPax.TBOPrice.Markup = returnResult.TBOPrice.Markup;
                        returnPax.TBOPrice.MarkupType = returnResult.TBOPrice.MarkupType;
                        returnPax.TBOPrice.MarkupValue = returnResult.TBOPrice.MarkupValue;
                        returnPax.TBOPrice.DecimalPoint = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentDecimalValue : Settings.LoginInfo.DecimalValue);
                        returnPax.TBOPrice.Markup = returnResult.FareBreakdown[0].AgentMarkup / returnResult.FareBreakdown[0].PassengerCount;
                        returnPax.TBOPrice.Discount = returnResult.FareBreakdown[0].AgentDiscount / returnResult.FareBreakdown[0].PassengerCount;
                        returnPax.TBOPrice.RateOfExchange = 1;
                        returnPax.TBOPrice.Currency = returnResult.TBOPrice.Currency;
                        returnPax.TBOPrice.CurrencyCode = returnResult.TBOPrice.Currency;
                        returnPax.TBOPrice.WhiteLabelDiscount = 0;
                        returnPax.TBOPrice.WLCharge = returnResult.TBOPrice.WLCharge;
                        returnPax.TBOPrice.NetFare = returnResult.TBOPrice.NetFare;
                        returnPax.TBOPrice.OtherCharges = returnResult.TBOPrice.OtherCharges;
                        returnPax.TBOPrice.TransactionFee = returnResult.TBOPrice.TransactionFee;
                        returnPax.TBOPrice.AdditionalTxnFee = returnResult.TBOPrice.AdditionalTxnFee; //sai               
                        returnPax.TBOPrice.ChargeBU = returnResult.TBOPrice.ChargeBU;
                        returnPax.TBOPrice.AgentCommission = returnResult.TBOPrice.AgentCommission;
                        returnPax.TBOPrice.AirlineTransFee = returnResult.TBOPrice.AirlineTransFee;
                        returnPax.TBOPrice.AgentPLB = returnResult.TBOPrice.AgentPLB;
                        returnPax.TBOPrice.IncentiveEarned = returnResult.TBOPrice.IncentiveEarned;
                        returnPax.TBOPrice.TDSIncentive = returnResult.TBOPrice.TDSIncentive;
                        returnPax.TBOPrice.TDSPLB = returnResult.TBOPrice.TDSPLB;
                        returnPax.TBOPrice.TdsCommission = returnResult.TBOPrice.TdsCommission;
                        returnItinerary.TBOFareBreakdown = returnResult.TBOFareBreakdown;
                    }
                    returnPax.Price.IsServiceTaxOnBaseFarePlusYQ = false;


                    if (returnItinerary.FlightBookingSource == BookingSource.PKFares)
                    {
                        returnItinerary.TBOFareBreakdown = returnResult.FareBreakdown;
                        returnPax.BaggageCode = returnResult.BaggageIncludedInFare;
                    }


                    if (returnResult.ResultBookingSource == BookingSource.FlyDubai && returnResult.IsBaggageIncluded)
                    {
                        returnItinerary.IsBaggageIncluded = true;
                        returnPax.Price.BaggageCharge = 0;
                        returnPax.BaggageCode = returnResult.BaggageIncludedInFare + "kg";
                        if (request.Type == SearchType.Return)
                        {
                            returnPax.BaggageCode += "," + returnPax.BaggageCode;
                        }
                    }

                    #region AirArabia

                    if (returnResult.ResultBookingSource == BookingSource.AirArabia)
                    {
                        if (ddlInwardBaggage.SelectedIndex > 0)
                        {
                            if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split(',')[0]) > 0)
                            {
                                string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split(',')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        returnPax.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text;
                                    }
                                    else
                                    {
                                        returnPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text;
                                    }
                                }
                                returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                            }
                            else
                            {
                                for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        returnPax.BaggageCode += "," + ddlInwardBaggage.SelectedItem.Text;
                                    }
                                    else
                                    {
                                        returnPax.BaggageCode = ddlInwardBaggage.SelectedItem.Text;
                                    }
                                }
                                returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                            }
                            returnPax.BaggageType = ddlInwardBaggage.SelectedValue.Split('-')[1];
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split(',')[0]) > 0)
                            {
                                string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        returnPax.BaggageCode = segmentBaggageDetails[i] + "Kg";
                                    }
                                    else
                                    {
                                        returnPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg";
                                    }
                                }
                            }
                            else
                            {
                                returnPax.BaggageCode = "No Bag";
                            }
                            returnPax.BaggageType = "No Bag";
                        }
                        //
                        //returnPax.BaggageCode = ddlInwardBaggage.SelectedItem.Text;
                        //returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                        //returnPax.BaggageCode = ddlInwardBaggage.SelectedIndex == 0 ? returnPax.BaggageCode.Replace("Select Baggage", "No Bag") : returnPax.BaggageCode;
                    }
                    #endregion

                    //Code added by Lokesh on 07/10/2016.
                    #region Baggage Information for Indigo Lead passenger


                    if (returnItinerary.FlightBookingSource == BookingSource.Indigo || returnItinerary.FlightBookingSource == BookingSource.IndigoCorp)
                    {
                        DataTable dtBaggage = null;
                        if (SessionValues[RETURN_BAGGAGE] != null)
                        {
                            dtBaggage = SessionValues[RETURN_BAGGAGE] as DataTable;
                        }
                        if (ddlInwardBaggage.SelectedIndex > 0)
                        {
                            DataRow[] data = dtBaggage.Select("Description='" + ddlInwardBaggage.SelectedItem.Text + "'");
                            if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                            {
                                returnPax.BaggageType = data[0]["Code"].ToString();
                                returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                                string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        returnPax.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    }
                                    else
                                    {
                                        returnPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    }
                                }
                            }
                            else
                            {
                                returnPax.BaggageType = data[0]["Code"].ToString();
                                returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                                string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        returnPax.BaggageCode = ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    }
                                    else
                                    {
                                        returnPax.BaggageCode += "," + ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    }

                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare))
                            {
                                string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        if (ddlInwardBaggage.SelectedIndex > 0)
                                        {
                                            returnPax.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");

                                        }
                                        else
                                        {
                                            returnPax.BaggageCode = segmentBaggageDetails[i] + "Kg ";

                                        }
                                    }
                                    else
                                    {
                                        if (ddlInwardBaggage.SelectedIndex > 0)
                                        {
                                            returnPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");

                                        }
                                        else
                                        {
                                            returnPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg ";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                {
                                    if (string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        returnPax.BaggageCode = "No Bag";
                                    }
                                    else
                                    {
                                        returnPax.BaggageCode += "," + "No Bag";
                                    }
                                }
                            }
                        }
                    }

                    #endregion

                    #region Lead Pax  Baggage Information For SpiceJet
                    if (returnItinerary.FlightBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp)
                    {
                        DataTable dtBaggage = null;
                        if (returnResult == null)
                        {
                            returnResult = SessionValues[RETURN_RESULT] as SearchResult;
                        }
                        if (SessionValues[RETURN_BAGGAGE] != null)
                        {
                            dtBaggage = SessionValues[RETURN_BAGGAGE] as DataTable;
                        }
                        if (dtBaggage != null && ddlInwardBaggage.SelectedIndex > 0 && dtBaggage.Rows.Count > 0)
                        {

                            DataRow[] data = dtBaggage.Select("Description='" + ddlInwardBaggage.SelectedItem.Text + "'");
                            if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                            {
                                returnPax.BaggageType = data[0]["Code"].ToString();
                                returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                                string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        returnPax.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                    }
                                    else
                                    {
                                        returnPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                    }
                                }
                            }
                            else
                            {
                                returnPax.BaggageType = data[0]["Code"].ToString();
                                for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                {
                                    if (string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        returnPax.BaggageCode = ddlInwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                    }
                                    else
                                    {
                                        returnPax.BaggageCode += "," + ddlInwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                    }
                                }
                                returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                            }

                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                            {
                                string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(returnPax.BaggageCode)) //Added Condition for to avoid to display 'UpgradeBaggage' in E-ticket page 
                                    {
                                        if (ddlInwardBaggage.SelectedIndex > 0)
                                        {
                                            returnPax.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");

                                        }
                                        else
                                        {
                                            returnPax.BaggageCode = segmentBaggageDetails[i] + "Kg ";

                                        }
                                        
                                    }
                                    else
                                    {
                                        if (ddlInwardBaggage.SelectedIndex > 0) //Added Condition for to avoid to display 'UpgradeBaggage' in E-ticket page 
                                        {
                                            returnPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");

                                        }
                                        else
                                        {
                                            returnPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg ";
                                        }
                                       
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                {
                                    if (string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        returnPax.BaggageCode = "No Bag";
                                    }
                                    else
                                    {
                                        returnPax.BaggageCode += "," + "No Bag";
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region Baggage for TBOAir
                    if (returnItinerary.FlightBookingSource == BookingSource.TBOAir)  //Added by Ravi. To load the bagggage in the passenger details.
                    {
                        DataTable dtBaggage = SessionValues[RETURN_BAGGAGE] as DataTable;

                        if (dtBaggage != null)
                        {
                            foreach (DataRow dr in dtBaggage.Rows)
                            {
                                if (ddlInwardBaggage.SelectedIndex > 0 && ddlInwardBaggage.SelectedItem != null && dr["Price"].ToString() == ddlInwardBaggage.SelectedValue && dr["Group"].ToString() == "1")
                                {
                                    if (!string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        returnPax.BaggageCode += "," + dr["Code"].ToString();
                                        returnPax.BaggageType += "|WayType:" + dr["WayType"].ToString() + ",Code:" + dr["Code"].ToString() + ",Description:" + dr["Description"].ToString() + ",Weight:" + dr["Weight"].ToString() + ",Currency:" + dr["Currency"].ToString() + ",Price:" + dr["SupplierPrice"].ToString() + ",Origin:" + dr["Origin"].ToString() + ",Destination:" + dr["Destination"].ToString();
                                        returnPax.Price.BaggageCharge += Convert.ToDecimal(dr["Price"]);
                                    }
                                    else
                                    {
                                        returnPax.BaggageCode = dr["Code"].ToString();
                                        returnPax.BaggageType = "WayType:" + dr["WayType"].ToString() + ",Code:" + dr["Code"].ToString() + ",Description:" + dr["Description"].ToString() + ",Weight:" + dr["Weight"].ToString() + ",Currency:" + dr["Currency"].ToString() + ",Price:" + dr["SupplierPrice"].ToString() + ",Origin:" + dr["Origin"].ToString() + ",Destination:" + dr["Destination"].ToString();
                                        returnPax.Price.BaggageCharge += Convert.ToDecimal(dr["Price"]);
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region Baggage FlyDubai
                    if (returnItinerary.FlightBookingSource == BookingSource.FlyDubai)
                    {
                        if (request.Type == SearchType.Return)
                        {
                            if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible)
                            {
                                returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) + Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                returnPax.FlyDubaiBaggageCharge = new List<decimal>();
                                returnPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                returnPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                            }
                            else if (ddlOnwardBaggage.Visible)
                            {
                                returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) + 0;
                                returnPax.FlyDubaiBaggageCharge = new List<decimal>();
                                returnPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                returnPax.FlyDubaiBaggageCharge.Add(0);
                            }
                            else if (ddlInwardBaggage.Visible)
                            {
                                returnPax.Price.BaggageCharge = 0 + Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                returnPax.FlyDubaiBaggageCharge = new List<decimal>();
                                returnPax.FlyDubaiBaggageCharge.Add(0);
                                returnPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                            }
                            else
                            {
                                returnPax.Price.BaggageCharge = 0;
                                returnPax.FlyDubaiBaggageCharge = new List<decimal>();
                                returnPax.FlyDubaiBaggageCharge.Add(0);
                                returnPax.FlyDubaiBaggageCharge.Add(0);
                            }
                            if (returnItinerary.FlightBookingSource == BookingSource.FlyDubai)
                            {
                                if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible)
                                {
                                    if (SessionValues[RETURN_BAGGAGE] != null)
                                    {
                                        DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                                        if (ddlOnwardBaggage.SelectedIndex > 0 && returnResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            if (returnResult.BaggageIncludedInFare.Split(',')[0] == "10" || returnResult.BaggageIncludedInFare.Split(',')[0] == "20" || returnResult.BaggageIncludedInFare.Split(',')[0] == "30")
                                            {
                                                DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    returnPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                    returnPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    returnPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                    returnPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && returnResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            switch (returnResult.BaggageIncludedInFare.Split(',')[0])
                                            {
                                                case "20":
                                                    returnPax.BaggageType = "BAGB";
                                                    break;
                                                case "30":
                                                    returnPax.BaggageType = "BAGL";
                                                    break;
                                                case "40":
                                                    returnPax.BaggageType = "BAGX";
                                                    break;
                                            }
                                            if (returnPax.BaggageType.Length > 0)
                                            {
                                                returnPax.CategoryId = "99";
                                            }
                                            else
                                            {
                                                returnPax.CategoryId = "";
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && returnResult.BaggageIncludedInFare.Split(',')[0] == "40")
                                        {
                                            returnPax.BaggageType = "JBAG";
                                            returnPax.CategoryId = "99";
                                        }
                                        else
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }


                                        if (ddlInwardBaggage.SelectedIndex > 0 && request.Type == SearchType.Return && returnResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                returnPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0 && request.Type == SearchType.Return && returnResult.BaggageIncludedInFare.Split(',')[1] != "0" && (returnResult.FareType.Split(',')[1].ToUpper() == PAYTOCHANGE || returnResult.FareType.Split(',')[1].ToUpper() == NOCHANGE))
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                returnPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex <= 0 && request.Type == SearchType.Return && returnResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            switch (returnResult.BaggageIncludedInFare.Split(',')[1])
                                            {
                                                case "20":
                                                    returnPax.BaggageType += ",BAGB";
                                                    break;
                                                case "30":
                                                    returnPax.BaggageType += ",BAGL";
                                                    break;
                                                case "40":
                                                    returnPax.BaggageType += ",BAGX";
                                                    break;
                                            }
                                            if (returnPax.BaggageType.IndexOf(",") > 0)
                                            {
                                                returnPax.CategoryId += ",99";
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex <= 0 && request.Type == SearchType.Return && returnResult.BaggageIncludedInFare.Split(',')[1] == "40")
                                        {
                                            returnPax.BaggageType += ",JBAG";
                                            returnPax.CategoryId += ",99";
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                returnPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            returnPax.BaggageType += ",";
                                        }
                                    }
                                }
                                else if (ddlOnwardBaggage.Visible)
                                {
                                    if (SessionValues[RETURN_BAGGAGE] != null)
                                    {
                                        DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                                        if (ddlOnwardBaggage.SelectedIndex > 0 && returnResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            if (returnResult.BaggageIncludedInFare.Split(',')[0] == "10" || returnResult.BaggageIncludedInFare.Split(',')[0] == "20" || returnResult.BaggageIncludedInFare.Split(',')[0] == "30")
                                            {
                                                DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    returnPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                    returnPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    returnPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                    returnPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && returnResult.BaggageIncludedInFare.Split(',')[0] == "0")
                                        {
                                            switch (returnResult.BaggageIncludedInFare)
                                            {
                                                case "0":
                                                    returnPax.BaggageType = "";
                                                    break;
                                                case "20":
                                                    returnPax.BaggageType = "BAGB";
                                                    break;
                                                case "30":
                                                    returnPax.BaggageType = "BAGL";
                                                    break;
                                                case "40":
                                                    returnPax.BaggageType = "BAGX";
                                                    break;
                                            }
                                            if (returnPax.BaggageType.Length > 0)
                                            {
                                                returnPax.CategoryId = "99";
                                            }

                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && returnResult.BaggageIncludedInFare.Split(',')[0] == "40")
                                        {
                                            returnPax.BaggageType = "JBAG";
                                            returnPax.CategoryId = "99";
                                        }
                                        else
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        if (returnResult.FareType.Split(',')[1].ToUpper() == BASIC)
                                        {
                                            returnPax.BaggageType += ",JBAG";
                                            returnPax.CategoryId += ",99";
                                        }
                                        else
                                        {
                                            returnPax.BaggageType += ",BAGX";
                                            returnPax.CategoryId += ",99";
                                        }
                                    }
                                }
                                else if (ddlInwardBaggage.Visible)
                                {
                                    if (SessionValues[RETURN_BAGGAGE] != null)
                                    {
                                        if (returnResult.FareType.Split(',')[0].ToUpper() == BASIC)
                                        {
                                            returnPax.BaggageType = "JBAG";
                                            returnPax.CategoryId = "99";
                                        }
                                        else
                                        {
                                            returnPax.BaggageType = "BAGX";
                                            returnPax.CategoryId = "99";
                                        }

                                        DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                                        if (ddlInwardBaggage.SelectedIndex > 0 && request.Type == SearchType.Return && returnResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (returnResult.BaggageIncludedInFare.Split(',')[1] == "10" || returnResult.BaggageIncludedInFare.Split(',')[1] == "20" || returnResult.BaggageIncludedInFare.Split(',')[1] == "30")
                                            {
                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    returnPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                    returnPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    returnPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                    returnPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex <= 0 && request.Type == SearchType.Return && returnResult.BaggageIncludedInFare.Split(',')[1] == "0")
                                        {

                                            switch (returnResult.BaggageIncludedInFare)
                                            {
                                                case "20":
                                                    returnPax.BaggageType += ",BAGB";
                                                    break;
                                                case "30":
                                                    returnPax.BaggageType += ",BAGL";
                                                    break;
                                                case "40":
                                                    returnPax.BaggageType += ",BAGX";
                                                    break;
                                            }
                                            if (returnPax.BaggageType.IndexOf(",") > 0)
                                            {
                                                returnPax.CategoryId += ",99";
                                            }
                                            returnPax.BaggageType += ",";
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex <= 0 && request.Type == SearchType.Return && returnResult.BaggageIncludedInFare.Split(',')[1] == "40")
                                        {
                                            returnPax.BaggageType += ",BAGX";
                                            returnPax.CategoryId += ",99";
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                returnPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            returnPax.BaggageType += ",";
                                        }
                                    }
                                }
                                else
                                {
                                    returnPax.BaggageType = "JBAG,JBAG";
                                    returnPax.CategoryId = "99,99";
                                }
                            }

                            AssignPaxBaggageCodes(returnPax, returnResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                        }
                        else if (request.Type == SearchType.OneWay)
                        {
                            if (ddlOnwardBaggage.Visible)
                            {
                                returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue);
                                returnPax.FlyDubaiBaggageCharge = new List<decimal>();
                                returnPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                            }
                            else
                            {
                                returnPax.Price.BaggageCharge = 0;
                                returnPax.FlyDubaiBaggageCharge = new List<decimal>();
                                returnPax.FlyDubaiBaggageCharge.Add(0);
                            }
                            if (returnItinerary.FlightBookingSource == BookingSource.FlyDubai)
                            {
                                if (ddlOnwardBaggage.Visible)
                                {
                                    if (SessionValues[RETURN_BAGGAGE] != null)
                                    {
                                        DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                                        if (ddlOnwardBaggage.SelectedIndex > 0 && (returnResult.FareType.ToUpper() == FREETOCHANGE || returnResult.FareType.ToUpper() == NOCHANGE))
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && returnResult.BaggageIncludedInFare != "0")
                                        {

                                            switch (returnResult.BaggageIncludedInFare)
                                            {
                                                case "20":
                                                    returnPax.BaggageType = "BAGB";
                                                    break;
                                                case "30":
                                                    returnPax.BaggageType = "BAGL";
                                                    break;
                                                case "40":
                                                    returnPax.BaggageType = "BAGX";
                                                    break;
                                            }
                                            if (returnPax.BaggageType.Length > 0)
                                            {
                                                returnPax.CategoryId = "99";
                                            }
                                        }
                                        else
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                    }
                                }
                                else if (returnResult.FareType.ToUpper() == NOCHANGE)
                                {
                                    returnPax.BaggageType = "BAGX";
                                    returnPax.CategoryId = "99";
                                }
                                else if (returnResult.FareType.ToUpper() == BASIC)
                                {
                                    returnItinerary.IsBaggageIncluded = true;
                                    returnPax.BaggageType = "JBAG";
                                    returnPax.CategoryId = "99";
                                }
                            }

                            AssignPaxBaggageCodes(returnPax, returnResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                        }
                    }
                    #endregion

                    #region Additional Baggage Information for AirIndia express for Lead Pax
                    if (returnItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl)
                    {
                        if (SessionValues[RETURN_BAGGAGE] != null)
                        {
                            DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                            if (request.Type == SearchType.Return)
                            {
                                if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible)
                                {
                                    returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) + Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                    returnPax.FlyDubaiBaggageCharge = new List<decimal>();
                                    returnPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                    returnPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                                }
                                else if (ddlOnwardBaggage.Visible)
                                {
                                    returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue);
                                    returnPax.FlyDubaiBaggageCharge = new List<decimal>();
                                    returnPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                    returnPax.FlyDubaiBaggageCharge.Add(0);
                                }
                                else if (ddlInwardBaggage.Visible)
                                {
                                    returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                    returnPax.FlyDubaiBaggageCharge = new List<decimal>();
                                    returnPax.FlyDubaiBaggageCharge.Add(0);
                                    returnPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                                }
                                else
                                {
                                    returnPax.Price.BaggageCharge = 0;
                                    returnPax.FlyDubaiBaggageCharge = new List<decimal>();
                                    returnPax.FlyDubaiBaggageCharge.Add(0);
                                    returnPax.FlyDubaiBaggageCharge.Add(0);
                                }

                                //In return journey if both onward and return additional baggage are available.
                                if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible && ddlOnwardBaggage.SelectedIndex > 0 && ddlInwardBaggage.SelectedIndex > 0)
                                {
                                    DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlOnwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                    if (baggage != null && baggage.Length > 0)
                                    {
                                        returnPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                        returnPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                    }
                                    if (ddlInwardBaggage.SelectedIndex > 0)
                                    {
                                        baggage = dtBaggageInfo.Select("CodeType='" + ddlInwardBaggage.SelectedItem.Text.Split('-')[1] + "'");

                                        if (baggage != null && baggage.Length > 0)
                                        {
                                            returnPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                            returnPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                        }
                                    }
                                }
                                else if (ddlOnwardBaggage.Visible && ddlOnwardBaggage.SelectedIndex > 0) // If only onward additional baggage is available.
                                {
                                    if (ddlOnwardBaggage.SelectedIndex > 0)
                                    {
                                        DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlOnwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                        if (baggage != null && baggage.Length > 0)
                                        {
                                            returnPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                            returnPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                        }
                                    }
                                }
                                else if (ddlInwardBaggage.Visible && ddlInwardBaggage.SelectedIndex > 0) // If only return baggage is available.
                                {
                                    DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlInwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                    if (baggage != null && baggage.Length > 0)
                                    {
                                        returnPax.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                        returnPax.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                    }
                                }

                                AssignAirIndiaExpressPaxBaggage(returnPax, returnResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                            }
                            else if (request.Type == SearchType.OneWay)
                            {
                                if (ddlOnwardBaggage.Visible)
                                {
                                    returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue);
                                    returnPax.FlyDubaiBaggageCharge = new List<decimal>();
                                    returnPax.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                }
                                else
                                {
                                    returnPax.Price.BaggageCharge = 0;
                                    returnPax.FlyDubaiBaggageCharge = new List<decimal>();
                                    returnPax.FlyDubaiBaggageCharge.Add(0);
                                }
                                if (ddlOnwardBaggage.Visible && ddlOnwardBaggage.SelectedIndex > 0)
                                {
                                    DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlOnwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                    if (baggage != null && baggage.Length > 0)
                                    {
                                        returnPax.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                        returnPax.CategoryId = baggage[0]["CategoryId"].ToString();
                                    }
                                }
                                AssignAirIndiaExpressPaxBaggage(returnPax, returnResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                            }
                        }
                    }
                    #endregion

                    #region Lead Pax  Baggage Information For GoAir
                    if (returnItinerary.FlightBookingSource == BookingSource.GoAir || returnItinerary.FlightBookingSource == BookingSource.GoAirCorp)
                    {
                        DataTable dtBaggage = null;
                        if (returnResult == null)
                        {
                            returnResult = SessionValues[RETURN_RESULT] as SearchResult;
                        }
                        if (SessionValues[RETURN_BAGGAGE] != null)
                        {
                            dtBaggage = SessionValues[RETURN_BAGGAGE] as DataTable;
                        }
                        if (dtBaggage != null && ddlInwardBaggage.SelectedIndex > 0 && dtBaggage.Rows.Count > 0)
                        {

                            DataRow[] data = dtBaggage.Select("Description='" + ddlInwardBaggage.SelectedItem.Text + "'");
                            if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                            {
                                returnPax.BaggageType = data[0]["Code"].ToString();
                                returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                                string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        returnPax.BaggageCode = segmentBaggageDetails[i] + "Kg +" + ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    }
                                    else
                                    {
                                        returnPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg +" + ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    }
                                }
                            }
                            else
                            {
                                returnPax.BaggageType = data[0]["Code"].ToString();
                                returnPax.BaggageCode = ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                returnPax.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                            }

                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                            {
                                string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        returnPax.BaggageCode = segmentBaggageDetails[i] + "Kg";
                                    }
                                    else
                                    {
                                        returnPax.BaggageCode += "," + segmentBaggageDetails[i] + "Kg";
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                {
                                    if (string.IsNullOrEmpty(returnPax.BaggageCode))
                                    {
                                        returnPax.BaggageCode = "No Bag";
                                    }
                                    else
                                    {
                                        returnPax.BaggageCode += "," + "No Bag";
                                    }
                                }
                            }
                        }
                    }
                    #endregion


                    #region  Meal Selection for Indigo,SpiceJet,GoAir For LeadPax

                    if ((returnItinerary.FlightBookingSource == BookingSource.SpiceJet || returnItinerary.FlightBookingSource == BookingSource.Indigo || returnItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || returnItinerary.FlightBookingSource == BookingSource.IndigoCorp || returnItinerary.FlightBookingSource == BookingSource.GoAir || returnItinerary.FlightBookingSource == BookingSource.GoAirCorp) && SessionValues[RETURN_BAGGAGE] != null)
                    {
                        DataTable dtBaggage = SessionValues[RETURN_BAGGAGE] as DataTable;
                        if (!string.IsNullOrEmpty(hdnInMealSelection.Value) && hdnInMealSelection.Value.Length > 0)
                        {
                            string[] inwardSelection = hdnInMealSelection.Value.Split(',');
                            if (Convert.ToInt32(inwardSelection[0]) > 0)
                            {
                                DataRow[] data = dtBaggage.Select("Description='" + ddlInwardMeal.Items[Convert.ToInt32(inwardSelection[0])].Text + "'");
                                if (data != null && data.Length > 0)
                                {
                                    returnPax.MealType = data[0]["Code"].ToString();
                                    returnPax.Price.MealCharge = Convert.ToDecimal(ddlInwardMeal.Items[Convert.ToInt32(inwardSelection[0])].Value.Split('-')[0]);
                                }
                                for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(returnPax.MealDesc))
                                    {
                                        returnPax.MealDesc += ',' + ddlInwardMeal.Items[Convert.ToInt32(inwardSelection[0])].Text;
                                    }
                                    else
                                    {
                                        returnPax.MealDesc = ddlInwardMeal.Items[Convert.ToInt32(inwardSelection[0])].Text;
                                    }
                                }
                            }
                            else //No return meal selected
                            {
                                for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(returnPax.MealDesc))
                                    {
                                        returnPax.MealDesc += ',' + "No Meal";
                                    }
                                    else
                                    {
                                        returnPax.MealDesc = "No Meal";
                                    }
                                }
                            }
                        }
                        else//No return meal selected
                        {
                            for (int i = 0; i < returnResult.Flights[0].Length; i++)
                            {
                                if (!string.IsNullOrEmpty(returnPax.MealDesc))
                                {
                                    returnPax.MealDesc += ',' + "No Meal";
                                }
                                else
                                {
                                    returnPax.MealDesc = "No Meal";
                                }
                            }
                        }
                    }
                    #endregion
                    //G9 Return pax meals
                    if (returnItinerary.FlightBookingSource == BookingSource.AirArabia)
                    {
                        DataTable dtBaggage = SessionValues.ContainsKey("ReturnG9Meal") ? SessionValues[RETURN_G9MEALS] as DataTable : null ;
                        if (ddlInwardMeal.SelectedIndex > 0 && dtBaggage != null && dtBaggage.Rows.Count > 0)
                        {

                            DataRow[] data = dtBaggage.Select("mealDescription='" + ddlInwardMeal.SelectedItem.Text + "'");
                            if (data != null && data.Length > 0)
                            {
                                returnPax.MealType = data[0]["mealCode"].ToString();
                                returnPax.Price.MealCharge = Convert.ToDecimal(ddlInwardMeal.SelectedValue.Split('-')[1]);
                            }
                            for (int i = 0; i < returnResult.Flights[0].Length; i++)
                            {
                                if (!string.IsNullOrEmpty(returnPax.MealDesc))
                                {
                                    returnPax.MealDesc += ',' + ddlInwardMeal.SelectedItem.Text;
                                }
                                else
                                {
                                    returnPax.MealDesc = ddlInwardMeal.SelectedItem.Text;
                                }
                            }
                        }
                        else //No return meal selected
                        {
                            for (int i = 0; i < returnResult.Flights[0].Length; i++)
                            {
                                if (!string.IsNullOrEmpty(returnPax.MealDesc))
                                {
                                    returnPax.MealDesc += ',' + "No Meal";
                                }
                                else
                                {
                                    returnPax.MealDesc = "No Meal";
                                }
                            }
                            returnPax.MealType = "No Meal";
                        }
                    }
                    //Salam Air is not permiting additional baggage so only adding default baggage to Lead Pax
                    #region Salam Air deafult baggage for Additional pax
                    else if (returnItinerary.FlightBookingSource == BookingSource.SalamAir && returnPax.Type != PassengerType.Infant)
                    {
                        AssignSalamAirBagCodes(returnPax, returnResult.BaggageIncludedInFare);
                    }
                    #endregion
                    //Flex Details saving
                    FlightFlexDetails[] arrPaxFlex = null;

                    if (!string.IsNullOrEmpty(hdnPaxFlexInfo.Value))
                    {
                        arrPaxFlex = JsonConvert.DeserializeObject<FlightFlexDetails[]>(hdnPaxFlexInfo.Value);
                        returnPax.FlexDetailsList = arrPaxFlex.Where(x => x.PaxId == 0 && !string.IsNullOrEmpty(x.FlexLabel)).ToList();
                    }

                    //if (tblFlexFields != null && Convert.ToInt32(hdnFlexCount.Value) > 0)
                    //{
                    //    returnPax.FlexDetailsList = new List<FlightFlexDetails>();
                    //    for (int i = 0; i < Convert.ToInt32(hdnFlexCount.Value); i++)
                    //    {
                    //        FlightFlexDetails flexDetails = new FlightFlexDetails();
                    //        HiddenField hdnFlexControl = tblFlexFields.FindControl("hdnFlexControl" + i) as HiddenField;
                    //        HiddenField hdnFlexId = tblFlexFields.FindControl("hdnFlexId" + i) as HiddenField;
                    //        HiddenField hdnFlexLabel = tblFlexFields.FindControl("hdnFlexLabel" + i) as HiddenField;
                    //        flexDetails.FlexId = Convert.ToInt32(hdnFlexId.Value);
                    //        flexDetails.FlexLabel = hdnFlexLabel.Value;
                    //        switch (hdnFlexControl.Value)
                    //        {
                    //            case "T":
                    //                TextBox txtFlex = tblFlexFields.FindControl("txtFlex" + i) as TextBox;
                    //                flexDetails.FlexData = txtFlex.Text;
                    //                break;
                    //            case "D":
                    //                DropDownList ddlPaxDay = tblFlexFields.FindControl("ddlDay" + i) as DropDownList;
                    //                DropDownList ddlPaxMonth = tblFlexFields.FindControl("ddlMonth" + i) as DropDownList;
                    //                DropDownList ddlPaxYear = tblFlexFields.FindControl("ddlYear" + i) as DropDownList;
                    //                flexDetails.FlexData = (ddlPaxDay.SelectedIndex > 0 && ddlPaxMonth.SelectedIndex > 0 && ddlPaxYear.SelectedIndex > 0) ?
                    //                            Convert.ToString(Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat)) : string.Empty;
                    //                break;
                    //            case "L":
                    //                DropDownList ddlFlex = tblFlexFields.FindControl("ddlFlex" + i) as DropDownList;
                    //                flexDetails.FlexData = ddlFlex.SelectedIndex > 0 ? ddlFlex.SelectedItem.Value : string.Empty;
                    //                break;
                    //        }
                    //        flexDetails.CreatedBy = (int)Settings.LoginInfo.UserID;
                    //        flexDetails.ProductID = 1;
                    //        returnPax.FlexDetailsList.Add(flexDetails);
                    //    }
                    //}

                    /* To read and assign pax seat information from Jason string and assign 
                     * the same to Lead passenger object for both onward and return
                     * ---- Code Starts here ---- */

                    if (!string.IsNullOrEmpty(txtPaxSeatInfo.Text))
                        liPaxSeatInfo = JsonConvert.DeserializeObject<List<PaxSeatInfo>>(txtPaxSeatInfo.Text);

                    returnPax.liPaxSeatInfo = new List<PaxSeatInfo>();

                    var objonwardFlights = onwardResult.Flights[0].Select(p => p.FlightNumber).Distinct();
                    List<string> seglst = new List<string>();
                    foreach (string fltno in objonwardFlights)
                    {
                        string segmnt = string.Empty;
                        segmnt = onwardResult.Flights[0].ToList().Where(y => y.FlightNumber == fltno).Select(x => x.Origin.AirportCode).FirstOrDefault();
                        segmnt += "-" + onwardResult.Flights[0].ToList().Where(y => y.FlightNumber == fltno).Select(x => x.Destination.AirportCode).Last();
                        seglst.Add(segmnt);
                    }

                    liPaxSeatInfo.Where(y => y.PaxNo == 0 && !string.IsNullOrEmpty(y.Segment)).ToList().ForEach(x =>
                    {
                        int cnt = seglst.Where(z => z == x.Segment).Count();
                        if (cnt > 0)
                        {
                            //if (!string.IsNullOrEmpty(x.SeatNo))
                            //    onwardPax.SeatInfo = string.IsNullOrEmpty(onwardPax.SeatInfo) ? x.SeatNo + "(" + x.Segment + ")" : onwardPax.SeatInfo + " | " + x.SeatNo + "(" + x.Segment + ")";
                            //onwardPax.liPaxSeatInfo.Add(x);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(x.SeatNo) && x.SeatNo != "NoSeat")
                                returnPax.SeatInfo = string.IsNullOrEmpty(returnPax.SeatInfo) ? x.SeatNo + "(" + x.Segment + ")" : returnPax.SeatInfo + " | " + x.SeatNo + "(" + x.Segment + ")";
                            returnPax.liPaxSeatInfo.Add(x);
                        }
                    });

                    returnPax.Price.SeatPrice = returnPax.liPaxSeatInfo.Sum(x => x.Price);
                    ddlOnwardBaggage.Enabled = ddlOnwardMeal.Enabled = ddlInwardBaggage.Enabled = ddlInwardMeal.Enabled = false;
                    /* ---- Code ends here ---- */



                    FlightPassenger[] returnPaxList = new FlightPassenger[paxCount];
                    returnPaxList[0] = returnPax;

                    int counter = 1;

                    foreach (DataListItem item in dlAdditionalPax.Items)
                    {
                        Label lblPaxType = item.FindControl("lblPaxType") as Label;
                        DropDownList ddlPaxTitle = item.FindControl("ddlPaxTitle") as DropDownList;
                        TextBox txtPaxFirstName = item.FindControl("txtPaxFName") as TextBox;
                        TextBox txtPaxLastName = item.FindControl("txtPaxLName") as TextBox;
                        DropDownList ddlPaxGender = item.FindControl("ddlGender") as DropDownList;
                        DropDownList ddlPaxDay = item.FindControl("ddlDay") as DropDownList;
                        DropDownList ddlPaxMonth = item.FindControl("ddlMonth") as DropDownList;
                        DropDownList ddlPaxYear = item.FindControl("ddlYear") as DropDownList;
                        TextBox txtPaxPassportNo = item.FindControl("txtPassportNo") as TextBox;
                        DropDownList ddlPaxPEDay = item.FindControl("ddlPEDay") as DropDownList;
                        DropDownList ddlPaxPEMonth = item.FindControl("ddlPEMonth") as DropDownList;
                        DropDownList ddlPaxPEYear = item.FindControl("ddlPEYear") as DropDownList;
                        DropDownList ddlPaxCountry = item.FindControl("ddlCountry") as DropDownList;
                        DropDownList ddlPaxNationality = item.FindControl("ddlNationality") as DropDownList;
                        DropDownList ddlOnwardBaggage = item.FindControl("ddlOnwardBaggage") as DropDownList;
                        DropDownList ddlInwardBaggage = item.FindControl("ddlInwardBaggage") as DropDownList;

                        DropDownList ddlOnwardMeal = item.FindControl("ddlOnwardMeal") as DropDownList;
                        DropDownList ddlInwardMeal = item.FindControl("ddlInwardMeal") as DropDownList;
                        TextBox txtAirline = item.FindControl("txtAirline") as TextBox;
                        TextBox txtFlight = item.FindControl("txtFlight") as TextBox;
                        HiddenField hdnProfileId = item.FindControl("hdnProfileId") as HiddenField;
                        CheckBox chkAddPax = item.FindControl("chkAddPax") as CheckBox;
                        HtmlGenericControl tblFlexFieldsAd = item.FindControl("tblFlexFieldsAd") as HtmlGenericControl;

                        ddlOnwardBaggage.Enabled = ddlInwardBaggage.Enabled = ddlOnwardMeal.Enabled = ddlInwardMeal.Enabled = false;


                        FlightPassenger returnPax1 = new FlightPassenger();

                        returnPax1.liPaxSeatInfo = new List<PaxSeatInfo>();

                        /* To assign pax seat information for all passengers (except lead passenger) 
                         * to passenger object for both onward and return, 
                         * also disable baagage and meal selection to avaoid the changes from user in case of seat assignment failure 
                         * ---- Code Starts here ---- */

                        ddlOnwardBaggage.Enabled = ddlInwardBaggage.Enabled = ddlOnwardMeal.Enabled = ddlInwardMeal.Enabled = false;

                        if (lblPaxType.Text != "Infant")
                        {
                            liPaxSeatInfo.Where(y => y.PaxNo != 0 && y.PaxNo - 1 == item.ItemIndex && !string.IsNullOrEmpty(y.Segment)).ToList().ForEach(x =>
                            {
                                int cnt = seglst.Where(z => z == x.Segment).Count();
                                if (cnt > 0)
                                {
                                    //if (!string.IsNullOrEmpty(x.SeatNo))
                                    //    onwardPax1.SeatInfo = string.IsNullOrEmpty(onwardPax1.SeatInfo) ? x.SeatNo + "(" + x.Segment + ")" : onwardPax1.SeatInfo + " | " + x.SeatNo + "(" + x.Segment + ")";
                                    //onwardPax1.liPaxSeatInfo.Add(x);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(x.SeatNo) && x.SeatNo != "NoSeat")
                                        returnPax1.SeatInfo = string.IsNullOrEmpty(returnPax1.SeatInfo) ? x.SeatNo + "(" + x.Segment + ")" : returnPax1.SeatInfo + " | " + x.SeatNo + "(" + x.Segment + ")";
                                    returnPax1.liPaxSeatInfo.Add(x);
                                }
                            });
                        }

                        /*----Code ends here ----*/

                        returnPax1.CorpProfileId = chkAddPax.Checked && string.IsNullOrEmpty(hdnProfileId.Value) ? "RN" : hdnProfileId.Value;
                        returnPax1.Title = ddlPaxTitle.SelectedValue.Replace(".", "");
                        returnPax1.FirstName = txtPaxFirstName.Text;
                        returnPax1.LastName = txtPaxLastName.Text;



                        if (originAirport.CountryCode == "IN" && destinationAirport.CountryCode == "IN" && returnResult.ResultBookingSource == BookingSource.TBOAir)
                        {
                            switch (lblPaxType.Text)
                            {
                                //Based on the pax type assign the date of birth if they are mandatory or non mandatory fields
                                case "Adult":
                                    returnPax1.Type = PassengerType.Adult;
                                    returnPax1.DateOfBirth = ((ddlPaxDay.SelectedIndex > 0) && (ddlPaxMonth.SelectedIndex > 0) && (ddlPaxYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat) : DateTime.MinValue);
                                    break;
                                case "Child":
                                    returnPax1.Type = PassengerType.Child;
                                    returnPax1.DateOfBirth = ((ddlPaxDay.SelectedIndex > 0) && (ddlPaxMonth.SelectedIndex > 0) && (ddlPaxYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat) : DateTime.MinValue);
                                    break;
                                case "Infant":
                                    returnPax1.Type = PassengerType.Infant;
                                    returnPax1.DateOfBirth = ((ddlPaxDay.SelectedIndex > 0) && (ddlPaxMonth.SelectedIndex > 0) && (ddlPaxYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat) : DateTime.MinValue);
                                    break;
                            }
                        }
                        else
                        {
                            switch (lblPaxType.Text)
                            {
                                //Based on the pax type assign the date of birth if they are mandatory or non mandatory fields
                                case "Adult":
                                    returnPax1.Type = PassengerType.Adult;
                                    returnPax1.DateOfBirth = ((ddlPaxDay.SelectedIndex > 0) && (ddlPaxMonth.SelectedIndex > 0) && (ddlPaxYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat) : DateTime.Now.AddYears(-45));
                                    break;
                                case "Child":
                                    returnPax1.Type = PassengerType.Child;
                                    returnPax1.DateOfBirth = ((ddlPaxDay.SelectedIndex > 0) && (ddlPaxMonth.SelectedIndex > 0) && (ddlPaxYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat) : DateTime.Now.AddYears(-10));
                                    break;
                                case "Infant":
                                    returnPax1.Type = PassengerType.Infant;
                                    returnPax1.DateOfBirth = ((ddlPaxDay.SelectedIndex > 0) && (ddlPaxMonth.SelectedIndex > 0) && (ddlPaxYear.SelectedIndex > 0) ? Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat) : DateTime.Now.AddYears(-1));
                                    break;
                            }
                        }

                        //Check whether address is a mandatory field or not and assign as per the condition.
                        returnPax1.AddressLine1 = (txtAddress1.Text.Length == 0 ? "Shj" : txtAddress1.Text);
                        returnPax1.AddressLine2 = (txtAddress2.Text.Length == 0 ? "Shj" : txtAddress2.Text);
                        returnPax1.CellPhone = txtMobileCountryCode.Text + '-' + txtMobileNo.Text;
                        returnPax1.City = "";
                        //Check whether country is a mandatory field or not and assign as per the condition.
                        returnPax1.Country = (ddlPaxCountry.SelectedIndex > 0 ? Country.GetCountry(ddlPaxCountry.SelectedItem.Value) : Country.GetCountry("AE"));
                        returnPax1.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                        returnPax1.CreatedOn = DateTime.Now;
                        returnPax1.Email = txtEmail.Text;
                        returnPax1.FFAirline = txtAirline.Text;
                        returnPax1.FFNumber = txtFlight.Text;
                        returnPax1.FlightId = returnResult.Flights[0][0].FlightId;
                        //Check whether Gender is a mandatory field or not and assign as per the condition.
                        returnPax1.Gender = (ddlPaxGender.SelectedIndex > 0 ? (Gender)Convert.ToInt32(ddlPaxGender.SelectedValue) : Gender.Male);
                        returnPax1.IsLeadPax = false;
                        returnPax1.Meal = new Meal();
                        //Check whether Nationality is a mandatory field or not and assign as per the condition.
                        returnPax1.Nationality = (ddlPaxNationality.SelectedIndex > 0 ? Country.GetCountry(ddlPaxNationality.SelectedValue) : Country.GetCountry("AE"));
                        //Additional Pax :Irrespective of whether  PassportExpiry and PassportNo are  mandatory or not mandatory 
                        //By chance if the user enters any passport related information need to capture in that case.
                        if (txtPaxPassportNo.Text.Length > 0 && !string.IsNullOrEmpty(txtPaxPassportNo.Text))
                        {
                            returnPax1.PassportExpiry = (ddlPaxPEDay.SelectedIndex > 0 && ddlPaxPEMonth.SelectedIndex > 0 && ddlPaxPEYear.SelectedIndex > 0 ? Convert.ToDateTime(ddlPaxPEDay.SelectedValue + "/" + ddlPaxPEMonth.SelectedValue + "/" + ddlPaxPEYear.SelectedValue, dateFormat) : DateTime.Now.AddYears(1));
                            returnPax1.PassportNo = txtPaxPassportNo.Text;
                        }

                        returnPax1.Price = new PriceAccounts();

                        returnPax1.Price.SeatPrice = returnPax1.liPaxSeatInfo.Sum(x => x.Price);

                        returnPax1.Price.TaxDetails = returnResult.Price.TaxDetails;
                        returnPax1.Price.InputVATAmount = returnResult.Price.InputVATAmount / paxCount;


                        for (int i = 0; i < returnResult.FareBreakdown.Length; i++)
                        {
                            if (returnPax1.Type == returnResult.FareBreakdown[i].PassengerType)
                            {
                                returnPax1.Price.AccPriceType = (returnResult.FareType == "NetFare" ? PriceType.NetFare : PriceType.PublishedFare);
                                if (returnPax1.Price.AccPriceType == PriceType.NetFare)
                                {
                                    returnPax1.Price.NetFare = (decimal)returnResult.FareBreakdown[i].BaseFare / returnResult.FareBreakdown[i].PassengerCount;
                                }
                                else
                                {
                                    returnPax1.Price.PublishedFare = (decimal)returnResult.FareBreakdown[i].BaseFare / returnResult.FareBreakdown[i].PassengerCount;
                                }
                                returnPax1.Price.SupplierCurrency = returnResult.Price.SupplierCurrency;
                                if (returnResult.ResultBookingSource == BookingSource.TBOAir)
                                {
                                    returnPax1.Price.SupplierPrice = 0;
                                }
                                else
                                {
                                    returnPax1.Price.SupplierPrice = (decimal)returnResult.FareBreakdown[i].SupplierFare / returnResult.FareBreakdown[i].PassengerCount;
                                }
                                returnPax1.Price.Markup = returnResult.Price.Markup;
                                returnPax1.Price.MarkupType = returnResult.Price.MarkupType;
                                returnPax1.Price.MarkupValue = returnResult.Price.MarkupValue;
                                returnPax1.Price.DecimalPoint = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentDecimalValue : Settings.LoginInfo.DecimalValue);
                                returnPax1.Price.Tax = Convert.ToDecimal(returnResult.FareBreakdown[i].Tax.ToString("N"+agency.DecimalValue)) / returnResult.FareBreakdown[i].PassengerCount;
                                returnPax1.Price.K3Tax = Convert.ToDecimal(k3tax.ToString("N"+agency.DecimalValue));

                                returnPax1.Price.AdditionalTxnFee = (decimal)returnResult.FareBreakdown[i].AdditionalTxnFee / returnResult.FareBreakdown[i].PassengerCount;
                                returnPax1.Price.Markup = Convert.ToDecimal(returnResult.FareBreakdown[i].AgentMarkup.ToString("N"+agency.DecimalValue)) / returnResult.FareBreakdown[i].PassengerCount;
                                returnPax1.Price.Discount = Convert.ToDecimal(returnResult.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue)) / returnResult.FareBreakdown[i].PassengerCount;
                                returnPax1.Price.DiscountType = returnResult.Price.DiscountType;
                                returnPax1.Price.DiscountValue = returnResult.Price.DiscountValue;
                                returnPax1.Price.HandlingFeeAmount = Convert.ToDecimal(returnResult.FareBreakdown[i].HandlingFee.ToString("N"+agency.DecimalValue)) / returnResult.FareBreakdown[i].PassengerCount;
                                returnPax1.Price.HandlingFeeType = returnResult.Price.HandlingFeeType;
                                returnPax1.Price.HandlingFeeValue = returnResult.Price.HandlingFeeValue;
                            }
                            if (returnResult.ResultBookingSource == BookingSource.FlyDubai || returnResult.ResultBookingSource == BookingSource.AirIndiaExpressIntl)
                            {
                                returnPax1.Price.FareInformationID = returnResult.FareInformationId[returnPax1.Type.ToString()];
                                returnPax1.Price.TransactionFee = returnResult.Price.TransactionFee;
                            }
                            else
                            {
                                returnPax1.Price.TransactionFee = 0;
                            }
                            if (returnItinerary.FlightBookingSource == BookingSource.TBOAir)
                            {
                                returnPax1.Price.ChargeBU = returnResult.Price.ChargeBU;
                                returnPax1.Price.YQTax = 0;
                                returnPax1.Price.AgentCommission = 0;
                                returnPax1.Price.SServiceFee = 0;
                                returnPax1.Price.IncentiveEarned = 0;
                                returnPax1.Price.TDSIncentive = 0;
                                returnPax1.Price.AgentPLB = 0;
                                returnPax1.Price.TDSPLB = 0;
                                returnPax1.Price.TdsCommission = 0;
                                returnPax1.Price.AdditionalTxnFee = 0;
                                returnPax1.Price.TransactionFee = 0;
                                returnPax1.Price.OtherCharges = 0;
                                returnPax1.Price.SupplierCurrency = returnResult.Price.SupplierCurrency;

                                returnPax1.TBOPrice = new PriceAccounts();
                                returnPax1.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                returnPax1.TBOPrice.PublishedFare = (decimal)returnResult.TBOPrice.PublishedFare / paxCount;
                                returnPax1.TBOPrice.BaseFare = returnResult.TBOPrice.BaseFare;
                                returnPax1.TBOPrice.Tax = (decimal)returnResult.TBOPrice.Tax;
                                returnPax1.TBOPrice.Markup = returnResult.TBOPrice.Markup;
                                returnPax1.TBOPrice.MarkupType = returnResult.TBOPrice.MarkupType;
                                returnPax1.TBOPrice.MarkupValue = returnResult.TBOPrice.MarkupValue;
                                returnPax1.TBOPrice.DecimalPoint = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentDecimalValue : Settings.LoginInfo.DecimalValue);
                                returnPax1.TBOPrice.Markup = returnResult.FareBreakdown[i].AgentMarkup / returnResult.FareBreakdown[i].PassengerCount;
                                returnPax1.TBOPrice.Discount = returnResult.FareBreakdown[i].AgentDiscount / returnResult.FareBreakdown[i].PassengerCount;
                                returnPax1.TBOPrice.RateOfExchange = 1;
                                returnPax1.TBOPrice.Currency = returnResult.TBOPrice.Currency;
                                returnPax1.TBOPrice.CurrencyCode = returnResult.TBOPrice.Currency;
                                returnPax1.TBOPrice.WhiteLabelDiscount = 0;
                                returnPax1.TBOPrice.WLCharge = returnResult.TBOPrice.WLCharge;
                                returnPax1.TBOPrice.NetFare = returnResult.TBOPrice.NetFare;
                                returnPax1.TBOPrice.OtherCharges = returnResult.TBOPrice.OtherCharges;
                                returnPax1.TBOPrice.TransactionFee = returnResult.TBOPrice.TransactionFee;
                                returnPax1.TBOPrice.AdditionalTxnFee = returnResult.TBOPrice.AdditionalTxnFee; //sai               
                                returnPax1.TBOPrice.ChargeBU = returnResult.TBOPrice.ChargeBU;
                                returnPax1.TBOPrice.AgentCommission = returnResult.TBOPrice.AgentCommission;

                                returnPax1.TBOPrice.AirlineTransFee = returnResult.TBOPrice.AirlineTransFee;
                                returnPax1.TBOPrice.AgentPLB = returnResult.TBOPrice.AgentPLB;

                                returnPax1.TBOPrice.IncentiveEarned = returnResult.TBOPrice.IncentiveEarned;
                                returnPax1.TBOPrice.TDSIncentive = returnResult.TBOPrice.TDSIncentive;
                                returnPax1.TBOPrice.TDSPLB = returnResult.TBOPrice.TDSPLB;
                                returnPax1.TBOPrice.TdsCommission = returnResult.TBOPrice.TdsCommission;
                            }

                        }

                        returnPax1.Price.RateOfExchange = rateOfExchange;
                        returnPax1.Price.Currency = returnResult.Currency;
                        returnPax1.Price.CurrencyCode = returnResult.Currency;
                        returnPax1.Price.WhiteLabelDiscount = 0;



                        if (returnResult.ResultBookingSource == BookingSource.PKFares)
                        {
                            returnPax1.BaggageCode = returnResult.BaggageIncludedInFare;
                        }
                        if (returnResult.ResultBookingSource == BookingSource.AirArabia && returnPax1.Type != PassengerType.Infant)
                        {
                            if (ddlInwardBaggage.SelectedIndex > 0)
                            {
                                if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split(',')[0]) > 0)
                                {
                                    string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split(',')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text;
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text;
                                        }
                                    }
                                    returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                                }
                                else
                                {
                                    for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                    {
                                        if (!string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode += "," + ddlInwardBaggage.SelectedItem.Text;
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode = ddlInwardBaggage.SelectedItem.Text;
                                        }
                                    }
                                    returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                                }
                                returnPax1.BaggageType = ddlInwardBaggage.SelectedValue.Split('-')[1];
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split(',')[0]) > 0)
                                {
                                    string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(returnPax.BaggageCode))
                                        {
                                            returnPax1.BaggageCode = segmentBaggageDetails[i] + "Kg";
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg";
                                        }
                                    }
                                }
                                else
                                {
                                    returnPax1.BaggageCode = "No Bag";
                                }
                                returnPax1.BaggageType = "No Bag";
                            }
                            //
                            //returnPax1.BaggageCode = ddlInwardBaggage.SelectedItem.Text;
                            //returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                           // returnPax1.BaggageCode = ddlInwardBaggage.SelectedIndex == 0 ? returnPax1.BaggageCode.Replace("Select Baggage", "No Bag") : returnPax1.BaggageCode;
                        }
                        #region SpiceJet Additional Baggage
                        if ((returnItinerary.FlightBookingSource == BookingSource.SpiceJet || returnItinerary.FlightBookingSource == BookingSource.SpiceJetCorp) && returnPax1.Type != PassengerType.Infant)
                        {
                            if (returnResult == null)
                            {
                                returnResult = SessionValues[RETURN_RESULT] as SearchResult;
                            }
                            DataTable dtBaggage = null;
                            if (SessionValues[RETURN_BAGGAGE] != null)
                            {
                                dtBaggage = SessionValues[RETURN_BAGGAGE] as DataTable;
                            }

                            if (dtBaggage != null && dtBaggage.Rows.Count > 0 && ddlInwardBaggage.SelectedIndex > 0)
                            {
                                DataRow[] data = dtBaggage.Select("Description='" + ddlInwardBaggage.SelectedItem.Text + "'");
                                if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                                {
                                    returnPax1.BaggageType = data[0]["Code"].ToString();
                                    returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                                    string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                        }
                                    }
                                }
                                else
                                {
                                    returnPax1.BaggageType = data[0]["Code"].ToString();
                                    for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode = ddlInwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode += "," + ddlInwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                        }
                                    }
                                    //returnPax1.BaggageCode = ddlInwardBaggage.SelectedItem.Text.Replace("Excess Baggage", "");
                                    returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                                {
                                    string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode = segmentBaggageDetails[i] + "Kg";
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg";
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode = "No Bag";
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode += "," + "No Bag";
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region Indigo Additional Baggage info.
                        if ((returnItinerary.FlightBookingSource == BookingSource.Indigo || returnItinerary.FlightBookingSource == BookingSource.IndigoCorp) && returnPax1.Type != PassengerType.Infant)
                        {
                            //DataTable dtBaggage = SessionValues[ONWARD_BAGGAGE] as DataTable;

                            DataTable dtBaggage = null;
                            if (SessionValues[RETURN_BAGGAGE] != null)
                            {
                                dtBaggage = SessionValues[RETURN_BAGGAGE] as DataTable;
                            }
                            if (dtBaggage != null && dtBaggage.Rows.Count > 0 && ddlInwardBaggage.SelectedIndex > 0)
                            {
                                DataRow[] data = dtBaggage.Select("Description='" + ddlInwardBaggage.SelectedItem.Text + "'");
                                if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                                {
                                    returnPax1.BaggageType = data[0]["Code"].ToString();
                                    returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                                    string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode = segmentBaggageDetails[i] + "Kg +" + ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");

                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg +" + ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                        }
                                    }
                                }
                                else
                                {

                                    returnPax1.BaggageType = data[0]["Code"].ToString();
                                    returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                                    string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode = ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode += "," + ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                        }
                                    }

                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                                {
                                    string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode = segmentBaggageDetails[i] + "Kg";
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg";
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode = "No Bag";
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode += "," + "No Bag";
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region TBOAirBaggageInfo for additional pax
                        if (returnItinerary.FlightBookingSource == BookingSource.TBOAir && returnPax1.Type != PassengerType.Infant)
                        {
                            DataTable dtBaggage = SessionValues[RETURN_BAGGAGE] as DataTable;

                            if (dtBaggage != null)
                            {
                                foreach (DataRow dr in dtBaggage.Rows)
                                {
                                    if (ddlInwardBaggage.SelectedItem != null && dr["Weight"].ToString().Trim() == ddlInwardBaggage.SelectedItem.Text.ToLower().Replace("kg", "").Trim() && dr["Group"].ToString() == "1")
                                    {
                                        if (!string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode += "," + dr["Code"].ToString();
                                            returnPax1.BaggageType += "|WayType:" + dr["WayType"].ToString() + ",Code:" + dr["Code"].ToString() + ",Description:" + dr["Description"].ToString() + ",Weight:" + dr["Weight"].ToString() + ",Currency:" + dr["Currency"].ToString() + ",Price:" + dr["SupplierPrice"].ToString() + ",Origin:" + dr["Origin"].ToString() + ",Destination:" + dr["Destination"].ToString();
                                            returnPax1.Price.BaggageCharge += Convert.ToDecimal(dr["Price"]);
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode = dr["Code"].ToString();
                                            returnPax1.BaggageType = "WayType:" + dr["WayType"].ToString() + ",Code:" + dr["Code"].ToString() + ",Description:" + dr["Description"].ToString() + ",Weight:" + dr["Weight"].ToString() + ",Currency:" + dr["Currency"].ToString() + ",Price:" + dr["SupplierPrice"].ToString() + ",Origin:" + dr["Origin"].ToString() + ",Destination:" + dr["Destination"].ToString();
                                            returnPax1.Price.BaggageCharge += Convert.ToDecimal(dr["Price"]);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region FlyDubai Baggage info for additional pax
                        if (returnItinerary.FlightBookingSource == BookingSource.FlyDubai && returnPax1.Type != PassengerType.Infant)
                        {
                            if (request.Type == SearchType.Return)
                            {
                                if (SessionValues[RETURN_BAGGAGE] != null && returnItinerary.FlightBookingSource == BookingSource.FlyDubai)
                                {
                                    if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible)
                                    {
                                        DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                                        if (ddlOnwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] == "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            switch (onwardResult.BaggageIncludedInFare.Split(',')[0])
                                            {
                                                case "20":
                                                    returnPax1.BaggageType += "BAGB";
                                                    break;
                                                case "30":
                                                    returnPax1.BaggageType += "BAGL";
                                                    break;
                                                case "40":
                                                    returnPax1.BaggageType += "BAGX";
                                                    break;
                                            }
                                            if (returnPax1.BaggageType.Length > 0)
                                            {
                                                returnPax1.CategoryId += "99";
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.FareType.Split(',')[0].ToUpper() == BASIC)
                                        {
                                            returnPax1.BaggageType = "JBAG";
                                            returnPax1.CategoryId = "99";
                                        }
                                        else
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        if (ddlInwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            switch (onwardResult.BaggageIncludedInFare.Split(',')[1])
                                            {
                                                case "20":
                                                    returnPax1.BaggageType += ",BAGB";
                                                    break;
                                                case "30":
                                                    returnPax1.BaggageType += ",BAGL";
                                                    break;
                                                case "40":
                                                    returnPax1.BaggageType += ",BAGX";
                                                    break;
                                            }
                                            if (returnPax1.BaggageType.IndexOf(",") > 0)
                                            {
                                                returnPax1.CategoryId += ",99";
                                            }

                                        }
                                        else if (ddlInwardBaggage.SelectedIndex <= 0 && onwardResult.FareType.Split(',')[1].ToUpper() == BASIC)
                                        {
                                            returnPax1.BaggageType += ",JBAG";
                                            returnPax1.CategoryId += ",99";
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0 && onwardResult.FareType.Split(',')[1].ToUpper() == NOCHANGE || onwardResult.FareType.Split(',')[1].ToUpper() == PAYTOCHANGE)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            returnPax1.BaggageType += ",";
                                        }
                                    }
                                    else if (ddlOnwardBaggage.Visible)
                                    {
                                        DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                                        if (ddlOnwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] == "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare.Split(',')[0] != "0")
                                        {
                                            switch (onwardResult.BaggageIncludedInFare.Split(',')[0])
                                            {
                                                case "20":
                                                    returnPax1.BaggageType = "BAGB";
                                                    break;
                                                case "30":
                                                    returnPax1.BaggageType = "BAGL";
                                                    break;
                                                case "40":
                                                    returnPax1.BaggageType = "BAGX";
                                                    break;
                                            }
                                            if (returnPax1.BaggageType.Length > 0)
                                            {
                                                returnPax1.CategoryId = "99";
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.FareType.Split(',')[0].ToUpper() == BASIC)
                                        {
                                            returnPax1.BaggageType = "JBAG";
                                            returnPax1.CategoryId = "99";
                                        }
                                        else
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        if (onwardResult.FareType.Split(',')[1].ToUpper() == BASIC)
                                        {
                                            returnPax1.BaggageType += ",JBAG";
                                            returnPax1.CategoryId += ",99";
                                        }
                                        else
                                        {
                                            returnPax1.BaggageType += ",BAGX";
                                            returnPax1.CategoryId += ",99";
                                        }
                                    }
                                    else if (ddlInwardBaggage.Visible)
                                    {
                                        if (onwardResult.FareType.Split(',')[0].ToUpper() == BASIC)
                                        {
                                            returnPax1.BaggageType += "JBAG";
                                            returnPax1.CategoryId += "99";
                                        }
                                        else
                                        {
                                            returnPax1.BaggageType = "BAGX";
                                            returnPax1.CategoryId = "99";
                                        }

                                        DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                                        if (ddlInwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            switch (onwardResult.BaggageIncludedInFare.Split(',')[1])
                                            {
                                                case "20":
                                                    returnPax1.BaggageType += ",BAGB";
                                                    break;
                                                case "30":
                                                    returnPax1.BaggageType += ",BAGL";
                                                    break;
                                                case "40":
                                                    returnPax1.BaggageType += ",BAGX";
                                                    break;
                                            }
                                            if (returnPax1.BaggageType.IndexOf(",") > 0)
                                            {
                                                returnPax1.CategoryId += ",99";
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex <= 0 && onwardResult.FareType.Split(',')[1].ToUpper() == BASIC)
                                        {
                                            returnPax1.BaggageType += ",JBAG";
                                            returnPax1.CategoryId += ",99";
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[1] != "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0 && onwardResult.BaggageIncludedInFare.Split(',')[1] == "0")
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlInwardBaggage.SelectedIndex > 0)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlInwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            returnPax1.BaggageType += ",";
                                        }

                                    }
                                    else
                                    {
                                        returnPax1.BaggageType = "JBAG,JBAG";
                                        returnPax1.CategoryId = "99,99";
                                    }
                                }
                                if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible)
                                {
                                    returnPax1.Price.BaggageCharge = (Convert.ToDecimal(ddlOnwardBaggage.SelectedValue)) + (Convert.ToDecimal(ddlInwardBaggage.SelectedValue));
                                    returnPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                    returnPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                    returnPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                                }
                                else if (ddlOnwardBaggage.Visible)
                                {
                                    returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) + 0;
                                    returnPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                    returnPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                    returnPax1.FlyDubaiBaggageCharge.Add(0);
                                }
                                else if (ddlInwardBaggage.Visible)
                                {
                                    returnPax1.Price.BaggageCharge = 0 + Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                    returnPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                    returnPax1.FlyDubaiBaggageCharge.Add(0);
                                    returnPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                                }
                                else
                                {
                                    returnPax1.Price.BaggageCharge = 0;
                                    returnPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                    returnPax1.FlyDubaiBaggageCharge.Add(0);
                                    returnPax1.FlyDubaiBaggageCharge.Add(0);
                                }

                                AssignPaxBaggageCodes(returnPax1, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                            }
                            else if (request.Type == SearchType.OneWay && returnPax1.Type != PassengerType.Infant)
                            {
                                if (SessionValues[RETURN_BAGGAGE] != null && returnItinerary.FlightBookingSource == BookingSource.FlyDubai)
                                {
                                    if (ddlOnwardBaggage.Visible)
                                    {
                                        DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                                        if (ddlOnwardBaggage.SelectedIndex > 0 && (onwardResult.FareType.ToUpper() == FREETOCHANGE || onwardResult.FareType.ToUpper() == NOCHANGE))
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BUP%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex <= 0 && onwardResult.BaggageIncludedInFare != "0")
                                        {
                                            switch (onwardResult.BaggageIncludedInFare)
                                            {
                                                case "20":
                                                    returnPax1.BaggageType = "BAGB";
                                                    break;
                                                case "30":
                                                    returnPax1.BaggageType = "BAGL";
                                                    break;
                                                case "40":
                                                    returnPax1.BaggageType = "BAGX";
                                                    break;
                                            }
                                            if (returnPax1.BaggageType.Length > 0)
                                            {
                                                returnPax1.CategoryId = "99";
                                            }
                                        }
                                        else if (onwardResult.FareType.ToUpper() == BASIC)
                                        {
                                            returnItinerary.IsBaggageIncluded = true;
                                            returnPax1.BaggageType = "JBAG";
                                            returnPax1.CategoryId = "99";
                                        }
                                        else if (ddlOnwardBaggage.SelectedIndex > 0 && onwardResult.FareType.ToUpper() == PAYTOCHANGE)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType LIKE 'BAG%' AND Description LIKE '" + ddlOnwardBaggage.SelectedItem.Text + "%'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            returnPax1.BaggageType = "";
                                            returnPax1.CategoryId = "";
                                        }
                                    }
                                    else if (onwardResult.FareType.ToUpper() == NOCHANGE)
                                    {
                                        returnPax1.BaggageType = "BAGX";
                                        returnPax1.CategoryId = "99";
                                    }

                                }
                                if (ddlOnwardBaggage.Visible)
                                {
                                    returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue);
                                    returnPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                    returnPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                }
                                else
                                {
                                    returnPax1.Price.BaggageCharge = 0;
                                    returnPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                    returnPax1.FlyDubaiBaggageCharge.Add(0);
                                }

                                AssignPaxBaggageCodes(returnPax1, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                            }
                        }
                        #endregion

                        #region Baggage Information for AirIndia express for additional Pax
                        if (returnItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl)
                        {
                            if (returnPax1.Type == PassengerType.Infant)
                            {
                                AssignAirIndiaExpressPaxBaggage(returnPax1, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                            }
                            else if (returnPax1.Type != PassengerType.Infant)
                            {
                                if (SessionValues[RETURN_BAGGAGE] != null)
                                {
                                    DataTable dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
                                    if (request.Type == SearchType.Return)
                                    {
                                        if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible)
                                        {
                                            returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) + Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                            returnPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                            returnPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                            returnPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                                        }
                                        else if (ddlOnwardBaggage.Visible)
                                        {
                                            returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue);
                                            returnPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                            returnPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                            returnPax1.FlyDubaiBaggageCharge.Add(0);
                                        }
                                        else if (ddlInwardBaggage.Visible)
                                        {
                                            returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue);
                                            returnPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                            returnPax1.FlyDubaiBaggageCharge.Add(0);
                                            returnPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlInwardBaggage.SelectedValue) / rateOfExchange);
                                        }
                                        else
                                        {
                                            returnPax1.Price.BaggageCharge = 0;
                                            returnPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                            returnPax1.FlyDubaiBaggageCharge.Add(0);
                                            returnPax1.FlyDubaiBaggageCharge.Add(0);
                                        }

                                        //In return journey if both onward and return additional baggage are available.
                                        if (ddlOnwardBaggage.Visible && ddlInwardBaggage.Visible && ddlOnwardBaggage.SelectedIndex > 0 && ddlInwardBaggage.SelectedIndex > 0)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlOnwardBaggage.SelectedItem.Text.Split('-')[1] + "'");

                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                            if (ddlInwardBaggage.SelectedIndex > 0)
                                            {
                                                baggage = dtBaggageInfo.Select("CodeType='" + ddlInwardBaggage.SelectedItem.Text.Split('-')[1] + "'");

                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    returnPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                    returnPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                        }
                                        else if (ddlOnwardBaggage.Visible && ddlOnwardBaggage.SelectedIndex > 0) // If only onward additional baggage is available.
                                        {
                                            if (ddlOnwardBaggage.SelectedIndex > 0)
                                            {
                                                DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlOnwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    returnPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                    returnPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                        }
                                        else if (ddlInwardBaggage.Visible && ddlInwardBaggage.SelectedIndex > 0 && request.Type == SearchType.Return) // If only return baggage is available.
                                        {
                                            if (ddlInwardBaggage.SelectedIndex > 0 && request.Type == SearchType.Return)
                                            {
                                                DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlInwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                                if (baggage != null && baggage.Length > 0)
                                                {
                                                    returnPax1.BaggageType += "," + baggage[0]["CodeType"].ToString().Trim();
                                                    returnPax1.CategoryId += "," + baggage[0]["CategoryId"].ToString();
                                                }
                                            }
                                        }

                                        AssignAirIndiaExpressPaxBaggage(returnPax1, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                                    }
                                    else if (request.Type == SearchType.OneWay)
                                    {
                                        if (ddlOnwardBaggage.Visible)
                                        {
                                            returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlOnwardBaggage.SelectedValue);
                                            returnPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                            returnPax1.FlyDubaiBaggageCharge.Add(Convert.ToDecimal(ddlOnwardBaggage.SelectedValue) / rateOfExchange);
                                        }
                                        else
                                        {
                                            returnPax1.Price.BaggageCharge = 0;
                                            returnPax1.FlyDubaiBaggageCharge = new List<decimal>();
                                            returnPax1.FlyDubaiBaggageCharge.Add(0);
                                        }
                                        if (ddlOnwardBaggage.Visible && ddlOnwardBaggage.SelectedIndex > 0)
                                        {
                                            DataRow[] baggage = dtBaggageInfo.Select("CodeType='" + ddlOnwardBaggage.SelectedItem.Text.Split('-')[1] + "'");
                                            if (baggage != null && baggage.Length > 0)
                                            {
                                                returnPax1.BaggageType = baggage[0]["CodeType"].ToString().Trim();
                                                returnPax1.CategoryId = baggage[0]["CategoryId"].ToString();
                                            }
                                        }

                                        AssignAirIndiaExpressPaxBaggage(returnPax1, onwardResult, request.Type, ddlOnwardBaggage, ddlInwardBaggage);
                                    }
                                }
                            }
                        }
                        #endregion

                        #region GoAir Additional Baggage
                        if ((returnItinerary.FlightBookingSource == BookingSource.GoAir || returnItinerary.FlightBookingSource == BookingSource.GoAirCorp) && returnPax1.Type != PassengerType.Infant)
                        {
                            if (returnResult == null)
                            {
                                returnResult = SessionValues[RETURN_RESULT] as SearchResult;
                            }
                            DataTable dtBaggage = null;
                            if (SessionValues[RETURN_BAGGAGE] != null)
                            {
                                dtBaggage = SessionValues[RETURN_BAGGAGE] as DataTable;
                            }

                            if (dtBaggage != null && dtBaggage.Rows.Count > 0 && ddlInwardBaggage.SelectedIndex > 0)
                            {
                                DataRow[] data = dtBaggage.Select("Description='" + ddlInwardBaggage.SelectedItem.Text + "'");
                                if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                                {
                                    returnPax1.BaggageType = data[0]["Code"].ToString();
                                    returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                                    string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode = segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg + " + ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                        }
                                    }
                                }
                                else
                                {
                                    returnPax1.BaggageType = data[0]["Code"].ToString();
                                    returnPax1.BaggageCode = ddlInwardBaggage.SelectedItem.Text.Replace("Prepaid Excess Baggage –", "");
                                    returnPax1.Price.BaggageCharge = Convert.ToDecimal(ddlInwardBaggage.SelectedValue.Split('-')[0]);
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(returnResult.BaggageIncludedInFare) && Convert.ToInt32(returnResult.BaggageIncludedInFare.Split('|')[0].Split(',')[0]) > 0)
                                {
                                    string[] segmentBaggageDetails = returnResult.BaggageIncludedInFare.Split('|')[0].Split(',');
                                    for (int i = 0; i < segmentBaggageDetails.Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode = segmentBaggageDetails[i] + "Kg";
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode += "," + segmentBaggageDetails[i] + "Kg";
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                    {
                                        if (string.IsNullOrEmpty(returnPax1.BaggageCode))
                                        {
                                            returnPax1.BaggageCode = "No Bag";
                                        }
                                        else
                                        {
                                            returnPax1.BaggageCode += "," + "No Bag";
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region  Meal Selection for Indigo , SpiceJet ,GoAir For AdditionalPax


                        if ((returnItinerary.FlightBookingSource == BookingSource.SpiceJet || returnItinerary.FlightBookingSource == BookingSource.Indigo || returnItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || returnItinerary.FlightBookingSource == BookingSource.IndigoCorp || returnItinerary.FlightBookingSource == BookingSource.GoAir || returnItinerary.FlightBookingSource == BookingSource.GoAirCorp) && SessionValues[RETURN_BAGGAGE] != null && returnPax1.Type != PassengerType.Infant)
                        {
                            DataTable dtBaggage = SessionValues[RETURN_BAGGAGE] as DataTable;

                            if (!string.IsNullOrEmpty(hdnInMealSelection.Value) && hdnInMealSelection.Value.Length > 0)
                            {
                                string[] inwardSelection = hdnInMealSelection.Value.Split(',');
                                if (Convert.ToInt32(inwardSelection[counter]) > 0)
                                {
                                    DataRow[] data = dtBaggage.Select("Description='" + ddlInwardMeal.Items[Convert.ToInt32(inwardSelection[counter])].Text + "'");
                                    if (data != null && data.Length > 0)
                                    {
                                        returnPax1.MealType = data[0]["Code"].ToString();
                                        returnPax1.Price.MealCharge = Convert.ToDecimal(ddlInwardMeal.Items[Convert.ToInt32(inwardSelection[counter])].Value.Split('-')[0]);
                                    }
                                    for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                    {
                                        if (!string.IsNullOrEmpty(returnPax1.MealDesc))
                                        {
                                            returnPax1.MealDesc += ',' + ddlInwardMeal.Items[Convert.ToInt32(inwardSelection[counter])].Text;
                                        }
                                        else
                                        {
                                            returnPax1.MealDesc = ddlInwardMeal.Items[Convert.ToInt32(inwardSelection[counter])].Text;
                                        }
                                    }
                                }
                                else //No return meal selected
                                {
                                    for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                    {
                                        if (!string.IsNullOrEmpty(returnPax1.MealDesc))
                                        {
                                            returnPax1.MealDesc += ',' + "No Meal";
                                        }
                                        else
                                        {
                                            returnPax1.MealDesc = "No Meal";
                                        }
                                    }
                                }
                            }
                            else//No return meal selected
                            {
                                for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(returnPax1.MealDesc))
                                    {
                                        returnPax1.MealDesc += ',' + "No Meal";
                                    }
                                    else
                                    {
                                        returnPax1.MealDesc = "No Meal";
                                    }
                                }
                            }

                        }
                        #endregion
                        if (returnItinerary.FlightBookingSource == BookingSource.AirArabia)
                        {
                            DataTable dtBaggage = SessionValues.ContainsKey("ReturnG9Meal") ? SessionValues[RETURN_G9MEALS] as DataTable : null;
                            if (ddlInwardMeal.SelectedIndex > 0 && dtBaggage != null && dtBaggage.Rows.Count > 0)
                            {
                                DataRow[] data = dtBaggage.Select("mealDescription='" + ddlInwardMeal.SelectedItem.Text + "'");
                                if (data != null && data.Length > 0)
                                {
                                    returnPax1.MealType = data[0]["mealCode"].ToString();
                                    returnPax1.Price.MealCharge = Convert.ToDecimal(ddlInwardMeal.SelectedValue.Split('-')[1]);
                                }
                                for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(returnPax1.MealDesc))
                                    {
                                        returnPax1.MealDesc += ',' + ddlInwardMeal.SelectedItem.Text;
                                    }
                                    else
                                    {
                                        returnPax1.MealDesc = ddlInwardMeal.SelectedItem.Text;
                                    }
                                }
                            }
                            else //No return meal selected
                            {
                                for (int i = 0; i < returnResult.Flights[0].Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(returnPax1.MealDesc))
                                    {
                                        returnPax1.MealDesc += ',' + "No Meal";
                                    }
                                    else
                                    {
                                        returnPax1.MealDesc = "No Meal";
                                    }
                                }
                                returnPax1.MealType = "No Meal";
                            }
                        }

                        //Salam Air is not permiting additional baggage so only adding default baggage to Addl Pax
                        #region Salam Air deafult baggage for Additional pax
                        if (returnItinerary.FlightBookingSource == BookingSource.SalamAir && returnPax1.Type != PassengerType.Infant)
                        {
                            AssignSalamAirBagCodes(returnPax1, returnResult.BaggageIncludedInFare);
                        }
                        #endregion
                        //Flex Details saving
                        if (arrPaxFlex != null && arrPaxFlex.Length > 0)
                            returnPax1.FlexDetailsList = arrPaxFlex.Where(x => x.PaxId == counter && !string.IsNullOrEmpty(x.FlexLabel)).ToList();

                        //if (tblFlexFieldsAd != null && Convert.ToInt32(hdnFlexCount.Value) > 0)
                        //{

                        //    for (int i = 0; i < Convert.ToInt32(hdnFlexCount.Value); i++)
                        //    {
                        //        FlightFlexDetails flexDetails = new FlightFlexDetails();
                        //        HiddenField hdnFlexControl = tblFlexFieldsAd.FindControl("hdnFlexControlAd" + i) as HiddenField;
                        //        HiddenField hdnFlexId = tblFlexFieldsAd.FindControl("hdnFlexIdAd" + i) as HiddenField;
                        //        HiddenField hdnFlexLabel = tblFlexFieldsAd.FindControl("hdnFlexLabelAd" + i) as HiddenField;
                        //        flexDetails.FlexId = Convert.ToInt32(hdnFlexId.Value);
                        //        flexDetails.FlexLabel = hdnFlexLabel.Value;
                        //        switch (hdnFlexControl.Value)
                        //        {
                        //            case "T":
                        //                TextBox txtFlex = tblFlexFieldsAd.FindControl("txtFlexAd" + i) as TextBox;
                        //                flexDetails.FlexData = txtFlex.Text;
                        //                break;
                        //            case "D":
                        //                DropDownList ddlAdPaxDay = tblFlexFieldsAd.FindControl("ddlDayAd" + i) as DropDownList;
                        //                DropDownList ddlAdPaxMonth = tblFlexFieldsAd.FindControl("ddlMonthAd" + i) as DropDownList;
                        //                DropDownList ddlAdPaxYear = tblFlexFieldsAd.FindControl("ddlYearAd" + i) as DropDownList;
                        //                flexDetails.FlexData = (ddlAdPaxDay.SelectedIndex > 0 && ddlAdPaxMonth.SelectedIndex > 0 && ddlAdPaxYear.SelectedIndex > 0) ?
                        //                    Convert.ToString(Convert.ToDateTime(ddlAdPaxDay.SelectedValue + "/" + ddlAdPaxMonth.SelectedValue + "/" + ddlAdPaxYear.SelectedValue, dateFormat)) : string.Empty;
                        //                break;
                        //            case "L":
                        //                DropDownList ddlFlex = tblFlexFieldsAd.FindControl("ddlFlexAd" + i) as DropDownList;
                        //                flexDetails.FlexData = ddlFlex.SelectedIndex > 0 ? ddlFlex.SelectedItem.Value : string.Empty;
                        //                break;
                        //        }
                        //        flexDetails.CreatedBy = (int)Settings.LoginInfo.UserID;
                        //        flexDetails.ProductID = 1;
                        //    }
                        //}

                        returnPaxList[counter] = returnPax1;
                        counter++;
                    }

                    returnItinerary.Passenger = returnPaxList;



                    //Create the Itinerary for two additional results 
                    //if (Settings.LoginInfo.IsCorporate == "Y" && Settings.LoginInfo.CorporateProfileId > 0 && request.AppliedPolicy)
                    if (false && Settings.LoginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId > 0 && request.AppliedPolicy)
                    {
                        GenerateItineraryForCorporateCriteria();
                    }

                    FlightItinerary tmponward = SessionValues[ONWARD_FLIGHT_ITINERARY] != null ? SessionValues[ONWARD_FLIGHT_ITINERARY] as FlightItinerary : null;
                    FlightItinerary tmpretrun = SessionValues[RETURN_FLIGHT_ITINERARY] != null ? SessionValues[RETURN_FLIGHT_ITINERARY] as FlightItinerary : null;

                    returnItinerary.BookRequestType = tmpretrun != null ? tmpretrun.BookRequestType : returnItinerary.BookRequestType;
                    returnItinerary.ItineraryAmountDue = tmpretrun != null ? tmpretrun.ItineraryAmountDue : returnItinerary.ItineraryAmountDue;

                    //Need to assign the tax breakup of individual pax from the onwardResult object.
                    //Reason: Only from the price itinerary response we will get the individual tax break up .
                    //This applies only for SG and 6E.

                    if (onwardResult != null &&
                        (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp || onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp)
                        && !string.IsNullOrEmpty(Convert.ToString(Session["sessionId"])) && (!string.IsNullOrEmpty(txtTaxRegNo_SG_GST.Text) && txtTaxRegNo_SG_GST.Text.Length > 0) && (!string.IsNullOrEmpty(txtCompanyName_SG_GST.Text) && txtCompanyName_SG_GST.Text.Length > 0))
                    {
                        if (onwardItinerary!=null)
                        {
                            CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(Session["sessionId"].ToString());
                            mse.SettingsLoginInfo = Settings.LoginInfo;

                            //If the onward result and return results are special round trip fare types For 6E
                            if ((onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp) && (returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.IndigoCorp) && onwardResult.IsSpecialRoundTrip && returnResult.IsSpecialRoundTrip && tmpretrun == null)
                            {
                                mse.Get6EUpdatedGSTFareBreakUp(ref onwardResult, ref returnResult, request);
                            }
                            //If the onward result and return results are special round trip fare types For SG
                            else if ((onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp) && (returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp) && onwardResult.IsSpecialRoundTrip && returnResult.IsSpecialRoundTrip && tmpretrun == null)
                            {
                                mse.GetSGUpdatedGSTFareBreakUp(ref onwardResult, ref returnResult, request);
                            }
                            //If both the results are special round trip fares
                            if (onwardResult.IsSpecialRoundTrip && returnResult.IsSpecialRoundTrip && tmpretrun == null)
                            {
                                CalculatePriceComponent(ref onwardResult, request);
                                CalculatePriceComponent(ref returnResult, request);
                                k3tax = onwardResult.Price.K3Tax/paxCount;
                                //Update Onward Itinerary
                                foreach (FlightPassenger passenger in onwardItinerary.Passenger)
                                {
                                    for (int i = 0; i < onwardResult.FareBreakdown.Length; i++)
                                    {
                                        if (passenger.Type != PassengerType.Infant && passenger.Type.ToString() == onwardResult.FareBreakdown[i].PassengerType.ToString())
                                        {
                                            if (onwardResult.ResultBookingSource == BookingSource.GoAirCorp)
                                                passenger.Price.PublishedFare = (decimal)onwardResult.FareBreakdown[i].BaseFare / onwardResult.FareBreakdown[i].PassengerCount; // Base Fare is Changing in GST Tax break up call so reassigning the fare.

                                            passenger.Price.Tax = (decimal)onwardResult.FareBreakdown[i].Tax / onwardResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.Markup = onwardResult.FareBreakdown[i].AgentMarkup / onwardResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.Discount = Convert.ToDecimal(onwardResult.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue)) / onwardResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.HandlingFeeAmount = onwardResult.FareBreakdown[i].HandlingFee / onwardResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.HandlingFeeValue = onwardResult.Price.HandlingFeeValue;
                                            passenger.Price.K3Tax = k3tax;

                                        }
                                    }
                                }
                                k3tax = returnResult.Price.K3Tax / paxCount;
                                //Update returnItinerary Itinerary
                                foreach (FlightPassenger passenger in returnItinerary.Passenger)
                                {
                                    for (int i = 0; i < returnResult.FareBreakdown.Length; i++)
                                    {
                                        if (passenger.Type != PassengerType.Infant && passenger.Type.ToString() == returnResult.FareBreakdown[i].PassengerType.ToString())
                                        {
                                            passenger.Price.Tax = (decimal)returnResult.FareBreakdown[i].Tax / returnResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.Markup = returnResult.FareBreakdown[i].AgentMarkup / returnResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.Discount = Convert.ToDecimal(returnResult.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue)) / returnResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.HandlingFeeAmount = returnResult.FareBreakdown[i].HandlingFee / returnResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.HandlingFeeValue = returnResult.Price.HandlingFeeValue;
                                            passenger.Price.K3Tax = k3tax;
                                        }
                                    }
                                }
                            }

                        }
                    }


                    if (returnResult != null &&
                        (returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp || returnResult.ResultBookingSource == BookingSource.GoAirCorp || returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.IndigoCorp)
                        && !string.IsNullOrEmpty(Convert.ToString(Session["sessionId"])) && (!string.IsNullOrEmpty(txtTaxRegNo_SG_GST.Text) && txtTaxRegNo_SG_GST.Text.Length > 0) && (!string.IsNullOrEmpty(txtCompanyName_SG_GST.Text) && txtCompanyName_SG_GST.Text.Length > 0))
                    {
                        if (returnItinerary != null)
                        {
                            CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(Session["sessionId"].ToString());
                            mse.SettingsLoginInfo = Settings.LoginInfo;
                            if ((returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp) && !returnResult.IsSpecialRoundTrip && tmpretrun == null)
                            {
                                mse.GetSGUpdatedGSTFareBreakUp(ref returnResult, request);
                            }
                            else if ((returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.IndigoCorp) && !returnResult.IsSpecialRoundTrip && tmpretrun == null)
                            {
                                mse.Get6EUpdatedGSTFareBreakUp(ref returnResult, request);
                            }
                            else if (returnResult.ResultBookingSource == BookingSource.GoAir && tmpretrun == null)
                            {
                                mse.GetG8UpdatedGSTFareBreakUp(ref returnResult, request);
                            }
                            //The return result fare type should not be SPECIAL ROUND TRIP FARE
                            if ((returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.IndigoCorp) && !returnResult.IsSpecialRoundTrip && tmpretrun == null)
                            {
                                CalculatePriceComponent(ref returnResult, request);
                                k3tax = returnResult.Price.K3Tax / paxCount;
                                foreach (FlightPassenger passenger in returnItinerary.Passenger)
                                {
                                    for (int i = 0; i < returnResult.FareBreakdown.Length; i++)
                                    {
                                        if (passenger.Type != PassengerType.Infant && passenger.Type.ToString() == returnResult.FareBreakdown[i].PassengerType.ToString())
                                        {
                                            if(returnResult.ResultBookingSource==BookingSource.GoAirCorp)
                                                passenger.Price.PublishedFare = (decimal)returnResult.FareBreakdown[i].BaseFare / returnResult.FareBreakdown[i].PassengerCount; // Base Fare is Changing in GST Tax break up call so reassigning the fare.
                                            passenger.Price.Tax = (decimal)returnResult.FareBreakdown[i].Tax / returnResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.Markup = returnResult.FareBreakdown[i].AgentMarkup / returnResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.Discount = Convert.ToDecimal(returnResult.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue)) / returnResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.HandlingFeeAmount = returnResult.FareBreakdown[i].HandlingFee / returnResult.FareBreakdown[i].PassengerCount;
                                            passenger.Price.HandlingFeeValue = returnResult.Price.HandlingFeeValue;
                                            passenger.Price.K3Tax = k3tax;
                                        }
                                    }
                                }
                            }
                        }
                    }



                    if (returnResult != null && (returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.IndigoCorp || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp || returnResult.ResultBookingSource == BookingSource.Jazeera))
                    {
                        if (returnItinerary != null && returnItinerary.Passenger != null && returnItinerary.Passenger.Length > 0 && returnResult.Price != null && returnResult.Price.PaxTypeTaxBreakUp != null && returnResult.Price.PaxTypeTaxBreakUp.Count > 0)
                        {
                            //ADT -- Adult pax tax break up.
                            //CHD -- Child pax tax break up.
                            //INF -- Infant pax tax break up.

                            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                            List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

                            foreach (KeyValuePair<string, List<KeyValuePair<string, decimal>>> breakup in returnResult.Price.PaxTypeTaxBreakUp)
                            {
                                if (breakup.Key == "ADT" && breakup.Value.Count > 0)
                                {
                                    adtPaxTaxBreakUp.AddRange(breakup.Value);
                                }
                                else if (breakup.Key == "CHD" && breakup.Value.Count > 0)
                                {
                                    chdPaxTaxBreakUp.AddRange(breakup.Value);
                                }
                                else if (breakup.Key == "INF" && breakup.Value.Count > 0)
                                {
                                    inftPaxTaxBreakUp.AddRange(breakup.Value);
                                }
                            }

                            for (int i = 0; i < returnItinerary.Passenger.Length; i++)
                            {
                                if (returnItinerary.Passenger[i].Type == PassengerType.Adult && adtPaxTaxBreakUp.Count > 0)
                                {
                                    returnItinerary.Passenger[i].TaxBreakup = adtPaxTaxBreakUp;
                                }
                                else if (returnItinerary.Passenger[i].Type == PassengerType.Child && chdPaxTaxBreakUp.Count > 0)
                                {
                                    returnItinerary.Passenger[i].TaxBreakup = chdPaxTaxBreakUp;
                                }
                                else if (returnItinerary.Passenger[i].Type == PassengerType.Infant && inftPaxTaxBreakUp.Count > 0)
                                {
                                    returnItinerary.Passenger[i].TaxBreakup = inftPaxTaxBreakUp;
                                }
                            }
                        }

                        //ADDED FOR k3Tax Components
                        //AFTER GST UPDATION NEED TO UPDATE TO THE LATEST PRICE COMPONENNTS
                        if (onwardItinerary != null && onwardItinerary.Passenger != null && onwardItinerary.Passenger.Length > 0 && onwardResult.Price != null && onwardResult.Price.PaxTypeTaxBreakUp != null && onwardResult.Price.PaxTypeTaxBreakUp.Count > 0 && onwardResult.IsSpecialRoundTrip)
                        {
                            //ADT -- Adult pax tax break up.
                            //CHD -- Child pax tax break up.
                            //INF -- Infant pax tax break up.

                            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                            List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

                            foreach (KeyValuePair<string, List<KeyValuePair<string, decimal>>> breakup in onwardResult.Price.PaxTypeTaxBreakUp)
                            {
                                if (breakup.Key == "ADT" && breakup.Value.Count > 0)
                                {
                                    adtPaxTaxBreakUp.AddRange(breakup.Value);
                                    onwardItinerary.Passenger.ToList().FindAll(p => p.Type == PassengerType.Adult).ForEach(p => p.TaxBreakup = adtPaxTaxBreakUp);

                                }
                                else if (breakup.Key == "CHD" && breakup.Value.Count > 0)
                                {
                                    chdPaxTaxBreakUp.AddRange(breakup.Value);
                                    List<FlightPassenger> childPassengers = onwardItinerary.Passenger.ToList().FindAll(p => p.Type == PassengerType.Child);
                                    if (childPassengers != null)
                                    {
                                        childPassengers.ForEach(p => p.TaxBreakup = chdPaxTaxBreakUp);
                                    }
                                }
                                else if (breakup.Key == "INF" && breakup.Value.Count > 0)
                                {
                                    inftPaxTaxBreakUp.AddRange(breakup.Value);
                                    List<FlightPassenger> infantPassenger = onwardItinerary.Passenger.ToList().FindAll(p => p.Type == PassengerType.Infant);
                                    if (infantPassenger != null)
                                    {
                                        infantPassenger.ForEach(p => p.TaxBreakup = inftPaxTaxBreakUp);
                                    }
                                }
                            }
                        }

                    }

                    //Added by lokesh on 24/12/2018.
                    //For SpiceJet and Indigo Till Search To Book One Signature Should be carried
                    //We are not assigning from the basket.
                    //We assign the signature to the result object to the property RepriceErrorMessage.

                    if (returnResult != null && returnItinerary != null && (returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp || returnResult.ResultBookingSource == BookingSource.Jazeera || returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.IndigoCorp))
                    {
                        if (!string.IsNullOrEmpty(returnResult.RepriceErrorMessage))
                        {
                            returnItinerary.Endorsement = returnResult.RepriceErrorMessage;
                        }

                    }

                    //If both the results are Special Round Trip Fares Then Allow Booking and Tiketing for the first itinerary

                    if (returnResult.IsSpecialRoundTrip)
                    {
                        returnItinerary.IsSpecialRoundTrip = true;
                    }
                    SaveInSession(RETURN_FLIGHT_ITINERARY, returnItinerary);
                    if (onwardResult.ResultBookingSource != BookingSource.TBOAir || returnResult.ResultBookingSource != BookingSource.TBOAir)
                    {
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        ////////////////////////////// Agent Currency, Rate of Exchange & Fare Check up ///////////////////////////////////////
                        StaticData sd = new StaticData();
                        sd.BaseCurrency = agency.AgentCurrency;

                        decimal roe = sd.CurrencyROE[onwardResult.Price.SupplierCurrency];
                        decimal baseFare = 0, tax = 0, resultBaseFare = 0, resultTax = 0;

                        if (onwardResult.Price.MarkupType == "P")
                        {
                            if (agency.AgentAirMarkupType == "BF")
                            {
                                baseFare = ((decimal)onwardResult.Price.PublishedFare) * roe;
                                baseFare += (baseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                                resultBaseFare = ((decimal)onwardResult.Price.PublishedFare) * rateOfExchange;
                                resultBaseFare += (resultBaseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                            }
                            else if (agency.AgentAirMarkupType == "TX")
                            {
                                baseFare = ((decimal)onwardResult.Price.Tax / roe) * roe;
                                baseFare += (baseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                                resultBaseFare = ((decimal)onwardResult.Price.Tax / rateOfExchange) * roe;
                                resultBaseFare += (resultBaseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                            }
                            else
                            {
                                baseFare = ((decimal)onwardResult.Price.SupplierPrice) * roe;
                                baseFare += (baseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                                resultBaseFare = ((decimal)onwardResult.Price.SupplierPrice) * rateOfExchange;
                                resultBaseFare += (resultBaseFare * (onwardResult.Price.MarkupValue / 100) * paxCount);
                            }
                        }
                        else
                        {
                            baseFare = (onwardResult.Price.SupplierPrice * roe) + (onwardResult.Price.MarkupValue * paxCount);
                            resultBaseFare = (onwardResult.Price.SupplierPrice * rateOfExchange) + (onwardResult.Price.MarkupValue * paxCount);
                        }

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if (agency.AgentCurrency == onwardResult.Currency)
                        {


                            SaveInSession(RETURN_FLIGHT_ITINERARY, returnItinerary);

                            if (rateOfExchange == roe && Math.Round(resultBaseFare, agency.DecimalValue) >= Math.Round(baseFare, agency.DecimalValue))
                            {
                                BookingResponse clsBookingResponse = UpdatePaxAndAssignSeats(onwardItinerary, returnItinerary);

                                if (string.IsNullOrEmpty(clsBookingResponse.Error))
                                {
                                    Session["Agent"] = null;
                                    RemoveFromSession(ONWARD_BAGGAGE);
                                    RemoveFromSession(RETURN_BAGGAGE);
                                    string priceChange = string.Empty;
                                    if (SessionValues.ContainsKey(ONWARD_SSR_PRICE_CHANGE) && SessionValues[ONWARD_SSR_PRICE_CHANGE] != null)
                                        priceChange = "&onward=" + SessionValues[ONWARD_SSR_PRICE_CHANGE].ToString();

                                    if (SessionValues.ContainsKey(RETURN_SSR_PRICE_CHANGE) && SessionValues[RETURN_SSR_PRICE_CHANGE] != null)
                                        priceChange += "&return=" + SessionValues[RETURN_SSR_PRICE_CHANGE].ToString();

                                    onwardItinerary.AirLocatorCode = string.Empty;
                                    returnItinerary.AirLocatorCode = string.Empty;
                                    Response.Redirect("PaymentConfirmationBySegments.aspx", false);
                                }
                                else
                                    CT.TicketReceipt.Common.Utility.StartupScript(this, "Showseatalert('" + clsBookingResponse.Error + "');", "script");
                            }
                            else
                            {
                                //Show message
                                lblMessage.Text = "Price has been revised for the Flight. Please search again or contact Customer Support.";
                            }
                        }
                        else
                        {
                            Session["ValuesChanged"] = null;//Clear the values used for UAPI Repricing
                                                            //Show message
                            lblMessage.Text = "Price has been revised for the Flight. Please search again or contact Customer Support.";
                        }
                    }
                    else
                    {
                        Response.Redirect("PaymentConfirmationBySegments.aspx", false);
                    }
                }

            }

        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
            Response.Redirect("ErrorPage.aspx?Err=" + ex.Message.ToString(), false);
        }

    }

    /// <summary>
    /// Assigns baggage codes for the passenger chosen from dropdown or pre included baggage if not chosen
    /// </summary>
    /// <param name="pax"></param>
    /// <param name="result"></param>
    /// <param name="type"></param>
    /// <param name="ddlOutBag"></param>
    /// <param name="ddlInBag"></param>
    void AssignPaxBaggageCodes(FlightPassenger pax, SearchResult result, SearchType type, DropDownList ddlOutBag, DropDownList ddlInBag)
    {
        if (type == SearchType.Return)
        {
            #region Basic and Basic
            if (result.FareType.Split(',')[0].ToUpper() == BASIC && result.FareType.Split(',')[1].ToUpper() == BASIC)//************************************************************************** Basic ******************** Basic *********************************************************
            {
                pax.BaggageCode = "40Kg, 40Kg";
            }
            #endregion
            #region Pay To Change and Pay To Change
            else if (result.FareType.Split(',')[0].ToUpper() == PAYTOCHANGE && result.FareType.Split(',')[1].ToUpper() == PAYTOCHANGE)
            {
                if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex > 0))
                {
                    if (Convert.ToInt32(result.BaggageIncludedInFare.Split(',')[0]) > 0 && Convert.ToInt32(result.BaggageIncludedInFare.Split(',')[1]) > 0)
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (Convert.ToInt32(result.BaggageIncludedInFare.Split(',')[0]) > 0)
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (Convert.ToInt32(result.BaggageIncludedInFare.Split(',')[1]) > 0)
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if (ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", No Bag";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", No Bag";
                    }
                }
                else if (ddlInBag.Visible && ddlInBag.SelectedIndex > 0)
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = "No Bag, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = "No Bag, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }

                }
                else if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0))
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if (!ddlOutBag.Visible && !ddlInBag.Visible)
                {
                    pax.BaggageCode = "No Bag, No Bag";
                }
                else
                {
                    pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                }
            }
            #endregion
            #region No Change and No Change
            else if (result.FareType.Split(',')[0].ToUpper() == NOCHANGE && result.FareType.Split(',')[1].ToUpper() == NOCHANGE)
            {
                if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex > 0))
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[0] == "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[0] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if (ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                {
                    if (ddlInBag.Visible)
                    {
                        if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[0] != "0")
                        {
                            pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                        }
                        else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[0] == "0")
                        {
                            pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                        }
                        else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[0] != "0")
                        {
                            pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                        }
                        else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[0] == "0")
                        {
                            pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", No Bag";
                        }
                    }
                    else
                    {
                        if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[0] != "0")
                        {
                            pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                        }
                        else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[0] == "0")
                        {
                            pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", No Bag";
                        }
                        else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[0] != "0")
                        {
                            pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                        }
                        else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[0] == "0")
                        {
                            pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", No Bag";
                        }
                    }
                }
                else if (ddlInBag.Visible && ddlInBag.SelectedIndex > 0)
                {
                    if (ddlOutBag.Visible)
                    {
                        if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[0] != "0")
                        {
                            pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                        }
                        else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[0] != "0")
                        {
                            pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                        }
                        else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[0] == "0")
                        {
                            pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                        }
                        else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[0] == "0")
                        {
                            pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                        }
                    }
                    else
                    {
                        if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[0] != "0")
                        {
                            pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                        }
                        else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[0] != "0")
                        {
                            pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                        }
                        else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[0] == "0")
                        {
                            pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                        }
                        else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[0] == "0")
                        {
                            pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                        }
                    }
                }
                else if (!ddlOutBag.Visible && !ddlInBag.Visible)
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[0] == "0")
                    {
                        pax.BaggageCode = "No Bag, No Bag";
                    }
                }
                else if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0))
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[0] == "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if ((!ddlOutBag.Visible && (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0)))
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = "No Bag, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = "No Bag, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                }
                else if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0) && !ddlInBag.Visible)
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, No Bag";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", No Bag";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                }
                else
                {
                    pax.BaggageCode = "No Bag, No Bag";
                }

            }
            #endregion
            #region Free To Change and Free To Change
            else if (result.FareType.Split(',')[0].ToUpper() == FREETOCHANGE && result.FareType.Split(',')[1].ToUpper() == FREETOCHANGE)
            {
                if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex > 0))
                {
                    pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                }
                else if (ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                {
                    pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                }
                else if (ddlInBag.Visible && ddlInBag.SelectedIndex > 0)
                {
                    pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                }
                else if ((!ddlOutBag.Visible && (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0)) || ((ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0) && !ddlInBag.Visible))
                {
                    pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                }
                else if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0))
                {
                    pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                }
                else if (!ddlOutBag.Visible && !ddlInBag.Visible)
                {
                    pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                }
            }
            #endregion
            #region Pay To Change and Free To Change
            else if (result.FareType.Split(',')[0].ToUpper() == PAYTOCHANGE && result.FareType.Split(',')[1].ToUpper() == FREETOCHANGE)//**************************************************** Pay To Change ************ Free To Change ************************************************
            {
                if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex > 0))
                {
                    if (Convert.ToInt32(result.BaggageIncludedInFare.Split(',')[0]) > 0)
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Allowance", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0))
                {
                    if (Convert.ToInt32(result.BaggageIncludedInFare.Split(',')[0]) > 0)
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Allowance", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                }
                else if ((ddlInBag.Visible && ddlInBag.SelectedIndex > 0))
                {
                    if (Convert.ToInt32(result.BaggageIncludedInFare.Split(',')[0]) > 0)
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Allowance", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if ((!ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0) && (!ddlInBag.Visible && ddlInBag.SelectedIndex <= 0) && Convert.ToInt32(result.BaggageIncludedInFare.Split(',')[0]) > 0)
                {
                    pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                }
                else if (ddlOutBag.SelectedIndex <= 0 && ddlInBag.SelectedIndex <= 0)
                {
                    pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                }
            }
            #endregion
            #region Pay To Change and No Change
            else if (result.FareType.Split(',')[0].ToUpper() == PAYTOCHANGE && result.FareType.Split(',')[1].ToUpper() == NOCHANGE)//************************************************************ Pay To Change ************** No Change **********************************************************
            {
                if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex > 0))
                {
                    if ((result.BaggageIncludedInFare.Split(',')[0]) != "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if ((result.BaggageIncludedInFare.Split(',')[0]) == "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if ((result.BaggageIncludedInFare.Split(',')[0]) != "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else if ((result.BaggageIncludedInFare.Split(',')[0]) == "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if (ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                {

                    if ((result.BaggageIncludedInFare.Split(',')[0]) != "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if ((result.BaggageIncludedInFare.Split(',')[0]) == "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else if ((result.BaggageIncludedInFare.Split(',')[0]) != "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else if ((result.BaggageIncludedInFare.Split(',')[0]) == "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", No Bag";
                    }

                }
                else if (ddlInBag.Visible && ddlInBag.SelectedIndex > 0)
                {

                    if ((result.BaggageIncludedInFare.Split(',')[0]) != "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if ((result.BaggageIncludedInFare.Split(',')[0]) == "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = "No Bag, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if ((result.BaggageIncludedInFare.Split(',')[0]) != "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if ((result.BaggageIncludedInFare.Split(',')[0]) == "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = "No Bag, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }

                }
                else if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0))
                {
                    if (Convert.ToInt32(result.BaggageIncludedInFare.Split(',')[0]) > 0 && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else if (Convert.ToInt32(result.BaggageIncludedInFare.Split(',')[0]) <= 0 && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = "No Bag, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = "No Bag, No Bag";
                    }
                }
                else if (!ddlOutBag.Visible && !ddlInBag.Visible)
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = "No Bag, No Bag";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, No Bag";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = "No Bag, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                }
                else if (ddlOutBag.SelectedIndex <= 0 && (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0 && result.BaggageIncludedInFare.Split(',')[1] != "0"))
                {
                    pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Allowance", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                }
                else if (ddlOutBag.SelectedIndex <= 0 && ddlInBag.Visible && ddlInBag.SelectedIndex <= 0 && result.BaggageIncludedInFare.Split(',')[1] == "0")
                {
                    pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Allowance", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                }
                else if (ddlOutBag.SelectedIndex <= 0 && ddlInBag.Visible && ddlInBag.SelectedIndex <= 0)
                {
                    pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Allowance", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                }
                else if (ddlOutBag.SelectedIndex <= 0 && !ddlInBag.Visible)
                {
                    pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Allowance", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                }
            }
            #endregion
            #region Pay To Change and Basic
            else if (result.FareType.Split(',')[0].ToUpper() == PAYTOCHANGE && result.FareType.Split(',')[1].ToUpper() == BASIC)
            {
                if (result.BaggageIncludedInFare.Split(',')[0] != "0" && ddlOutBag.Visible)
                {
                    pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", 40Kg";
                }
                else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && ddlOutBag.Visible)
                {
                    pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", 40Kg";
                }
                else
                {
                    pax.BaggageCode = "No Bag, 40Kg";
                }
            }
            #endregion
            #region Free To Change and Pay To Change
            else if (result.FareType.Split(',')[0].ToUpper() == FREETOCHANGE && result.FareType.Split(',')[1].ToUpper() == PAYTOCHANGE)
            {
                if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex > 0))
                {
                    if (result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if (ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                {
                    if (result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", No Bag";
                    }
                }
                else if (ddlInBag.Visible && ddlInBag.SelectedIndex > 0)
                {
                    if (result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if (ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0 && ddlInBag.Visible && ddlInBag.SelectedIndex <= 0)
                {
                    if (result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if (!ddlOutBag.Visible && !ddlInBag.Visible)
                {
                    pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, No Bag";
                }
            }
            #endregion
            #region Free To Change and Basic
            else if (result.FareType.Split(',')[0].ToUpper() == FREETOCHANGE && result.FareType.Split(',')[1].ToUpper() == BASIC)
            {
                if (ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                {
                    pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + "40Kg";
                }
                else
                {
                    pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + "40Kg";
                }
            }
            #endregion
            #region Free To Change and No Change
            else if (result.FareType.Split(',')[0].ToUpper() == FREETOCHANGE && result.FareType.Split(',')[1].ToUpper() == NOCHANGE)
            {
                if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex > 0))
                {
                    if (result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if (ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                {
                    if (result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", No Bag";
                    }
                }
                else if (ddlInBag.Visible && ddlInBag.SelectedIndex > 0)
                {
                    if (result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0))
                {
                    if (result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, No Bag";
                    }
                }
                else if (!ddlOutBag.Visible && !ddlInBag.Visible)
                {
                    pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, No Bag";
                }
            }
            #endregion
            #region No Change and Pay To Change
            else if (result.FareType.Split(',')[0].ToUpper() == NOCHANGE && result.FareType.Split(',')[1].ToUpper() == PAYTOCHANGE)
            {
                if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex > 0))
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if (ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = "No Bag, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", No Bag";
                    }
                }
                else if (ddlInBag.Visible && ddlInBag.SelectedIndex > 0)
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = "No Bag, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] != "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (result.BaggageIncludedInFare.Split(',')[0] == "0" && result.BaggageIncludedInFare.Split(',')[1] == "0")
                    {
                        pax.BaggageCode = "No Bag, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0))
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + ddlInBag.SelectedItem.Text.Replace("Baggage Allowance", "");
                    }
                    else
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + ddlInBag.SelectedItem.Text.Replace("Baggage Allowance", "");
                    }
                }
                else if (!ddlOutBag.Visible && (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0))
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + ddlInBag.SelectedItem.Text.Replace("Baggage Allowance", "");
                    }
                    else
                    {
                        pax.BaggageCode = "No Bag, " + ddlInBag.SelectedItem.Text.Replace("Baggage Allowance", "");
                    }
                }
                else if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0) && !ddlInBag.Visible)
                {
                    if (result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + "Kg, " + result.BaggageIncludedInFare.Split(',')[1];
                    }
                    else
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", No Bag";
                    }
                }
                else if (!ddlOutBag.Visible && !ddlInBag.Visible)
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, No Bag";
                    }
                    else
                    {
                        pax.BaggageCode = "No Bag, No Bag";
                    }
                }
            }
            #endregion
            #region No Change and Free To Change
            else if (result.FareType.Split(',')[0].ToUpper() == NOCHANGE && result.FareType.Split(',')[1].ToUpper() == FREETOCHANGE)
            {
                if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex > 0))
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if (ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                }
                else if (ddlInBag.Visible && ddlInBag.SelectedIndex > 0)
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else
                    {
                        pax.BaggageCode = "No Bag, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                }
                else if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0) && (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0))
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                }
                else if (!ddlOutBag.Visible && (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0))
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + ddlInBag.SelectedItem.Text.Replace("Baggage Allowance", "");
                    }
                    else
                    {
                        pax.BaggageCode = "No Bag, " + ddlInBag.SelectedItem.Text.Replace("Baggage Allowance", "");
                    }
                }
                else if ((ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0) && !ddlInBag.Visible)
                {
                    if (result.BaggageIncludedInFare.Split(',')[1] != "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + "Kg, " + result.BaggageIncludedInFare.Split(',')[1];
                    }
                    else
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", No Bag";
                    }
                }
                else if (!ddlOutBag.Visible && !ddlInBag.Visible)
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                    else
                    {
                        pax.BaggageCode = "No Bag, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                    }
                }
            }
            #endregion
            #region No Change and Basic
            else if (result.FareType.Split(',')[0].ToUpper() == NOCHANGE && result.FareType.Split(',')[1].ToUpper() == BASIC)
            {
                if (ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                {
                    if (result.BaggageIncludedInFare.Split(',')[0] != "0")
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", 40Kg";
                    }
                    else
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "") + ", 40Kg";
                    }
                }
                else
                {
                    pax.BaggageCode = result.BaggageIncludedInFare.Split(',')[0] + "Kg" + ", 40Kg";
                }
            }
            #endregion
            #region Basic and Pay To Change
            else if (result.FareType.Split(',')[0].ToUpper() == BASIC && result.FareType.Split(',')[1].ToUpper() == PAYTOCHANGE)
            {
                if (ddlInBag.Visible && result.BaggageIncludedInFare.Split(',')[1] != "0")
                {
                    pax.BaggageCode = "40Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                }
                else if (ddlInBag.Visible && result.BaggageIncludedInFare.Split(',')[1] == "0")
                {
                    pax.BaggageCode = "40Kg, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                }
                else if (!ddlInBag.Visible)
                {
                    pax.BaggageCode = "40Kg, No Bag";
                }
            }
            #endregion
            #region Basic and Free To Change
            else if (result.FareType.Split(',')[0].ToUpper() == BASIC && result.FareType.Split(',')[1].ToUpper() == FREETOCHANGE)
            {
                if (ddlInBag.Visible && ddlInBag.SelectedIndex > 0)
                {
                    pax.BaggageCode = "40Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                }
                else
                {
                    pax.BaggageCode = "40Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                }
            }
            #endregion
            #region Basic and No Change
            else if (result.FareType.Split(',')[0].ToUpper() == BASIC && result.FareType.Split(',')[1].ToUpper() == NOCHANGE)
            {
                if (ddlInBag.Visible && ddlInBag.SelectedIndex > 0 && result.BaggageIncludedInFare.Split(',')[1] != "0")
                {
                    pax.BaggageCode = "40Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg + " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                }
                else if (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0 && result.BaggageIncludedInFare.Split(',')[1] != "0")
                {
                    pax.BaggageCode = "40Kg, " + result.BaggageIncludedInFare.Split(',')[1] + "Kg";
                }
                else if (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0)
                {
                    pax.BaggageCode = "40Kg, " + ddlInBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                }
                else if (!ddlInBag.Visible)
                {
                    pax.BaggageCode = "40Kg, No Bag";
                }
            }
            #endregion

        }
        else//*************************************************************** One-Way *****************************************************************************
        {
            if (result.FareType.ToUpper() == PAYTOCHANGE)
            {
                if (Convert.ToInt32(result.BaggageIncludedInFare) > 0 && ddlOutBag.Visible)
                {
                    pax.BaggageCode = result.BaggageIncludedInFare + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Allowance", "");
                }
                else if (Convert.ToInt32(result.BaggageIncludedInFare) > 0 && !ddlOutBag.Visible)
                {
                    pax.BaggageCode = result.BaggageIncludedInFare + "Kg";
                }
                else
                {
                    pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Allowance", "");
                }
            }
            else if (result.FareType.ToUpper() == FREETOCHANGE)
            {
                if (ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                {
                    pax.BaggageCode = result.BaggageIncludedInFare + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                }
                else
                {
                    pax.BaggageCode = result.BaggageIncludedInFare + "kg";
                }
            }
            else if (result.FareType.ToUpper() == NOCHANGE)
            {
                if (ddlOutBag.Visible)
                {
                    if (ddlOutBag.SelectedIndex > 0)
                    {
                        if (Convert.ToInt32(result.BaggageIncludedInFare) > 0)
                        {
                            pax.BaggageCode = result.BaggageIncludedInFare + "Kg + " + ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                        }
                        else
                        {
                            pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                        }
                    }
                    else if (Convert.ToInt32(result.BaggageIncludedInFare) > 0 && ddlOutBag.SelectedIndex <= 0)
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare + "Kg";
                    }
                    else if (ddlOutBag.SelectedIndex <= 0 && result.BaggageIncludedInFare == "0")
                    {
                        pax.BaggageCode = ddlOutBag.SelectedItem.Text.Replace("Baggage Upgrade", "");
                    }
                    else if (ddlOutBag.SelectedIndex <= 0)
                    {
                        pax.BaggageCode = "No Bag";
                    }
                }
                else if (!ddlOutBag.Visible)
                {
                    if (Convert.ToInt32(result.BaggageIncludedInFare) > 0)
                    {
                        pax.BaggageCode = result.BaggageIncludedInFare + "Kg";
                    }
                    else if (Convert.ToInt32(result.BaggageIncludedInFare) <= 0)
                    {
                        pax.BaggageCode = "No Bag";
                    }
                }
            }
            else if (result.FareType.ToUpper().ToUpper() == BASIC)
            {
                pax.BaggageCode = "40Kg";
            }
        }
    }

    //Loading CorporatDetails
    void LoadingCorporateDetails()
    {
        try
        {
            if (corpProfile != null)
            {
                ddlTitle.SelectedValue = corpProfile.Title;
                ddlGender.SelectedValue = corpProfile.Gender;
                ddlDay.SelectedValue = corpProfile.DateOfBirth.Day.ToString();
                ddlMonth.SelectedValue = corpProfile.DateOfBirth.ToString("MM");
                ddlYear.SelectedValue = corpProfile.DateOfBirth.Year.ToString();
                ddlNationality.SelectedValue = corpProfile.NationalityCode;
                ddlCountry.SelectedValue = corpProfile.PassportCOI;

                ddlPEDay.SelectedValue = corpProfile.DateOfExpiry.Day.ToString();
                ddlPEMonth.SelectedValue = corpProfile.DateOfExpiry.ToString("MM");
                ddlPEYear.SelectedValue = corpProfile.DateOfExpiry.Year.ToString();

                txtPaxFName.Text = corpProfile.SurName;
                txtPaxLName.Text = corpProfile.Name;
                txtPassportNo.Text = corpProfile.PassportNo;
                string[] mobileNo = corpProfile.Mobilephone.Split('-');
                if (mobileNo.Length > 1)
                {
                    txtMobileCountryCode.Text = mobileNo[0];
                    txtMobileNo.Text = mobileNo[1];
                }
                else
                {
                    txtMobileNo.Text = corpProfile.Mobilephone;
                }
                txtAddress1.Text = corpProfile.Address1;
                txtAddress2.Text = corpProfile.Address2;
                txtEmail.Text = corpProfile.Email;
                if (corpProfile.DtProfileDetails.Rows.Count > 0)
                {
                    //Loading depature Airline
                    txtAirline.Text = onwardResult.Flights[0][0].Airline;
                    DataView dv = corpProfile.DtProfileDetails.DefaultView;
                    dv.RowFilter = "DetailType='FF' AND DisplayValue='" + onwardResult.Flights[0][0].Airline + "'";
                    DataTable dtcorp = dv.ToTable();
                    if (dtcorp != null && dtcorp.Rows.Count > 0)
                    {
                        txtFlight.Text = Convert.ToString(dtcorp.Rows[0]["Value"]);
                    }
                }
            }
        }
        catch { }
    }

    /// <summary>
    /// binding Flex Details
    /// </summary>
    //void CreateFlexFields()
    //{
    //    try
    //    {
    //        if (corpProfile != null)
    //        {
    //            //hdnFlexCount.Value = corpProfile.DtProfileFlex.Rows.Count.ToString();
    //            int i = 0;
    //            foreach (DataRow row in corpProfile.DtProfileFlex.Rows)
    //            {
    //                HtmlGenericControl divlblFlex1 = new HtmlGenericControl("Div");
    //                divlblFlex1.Attributes.Add("class", "col-md-3");

    //                HiddenField hdnFlexId = new HiddenField();
    //                hdnFlexId.ID = "hdnFlexId" + i;
    //                hdnFlexId.Value = row["flexId"].ToString();
    //                divlblFlex1.Controls.Add(hdnFlexId);

    //                HiddenField hdnFlexControl = new HiddenField();
    //                hdnFlexControl.ID = "hdnFlexControl" + i;
    //                hdnFlexControl.Value = Convert.ToString(row["flexControl"]);
    //                divlblFlex1.Controls.Add(hdnFlexControl);

    //                HiddenField hdnFlexMandatory = new HiddenField();
    //                hdnFlexMandatory.ID = "hdnFlexMandatory" + i;
    //                hdnFlexMandatory.Value = Convert.ToString(row["flexMandatoryStatus"]);
    //                divlblFlex1.Controls.Add(hdnFlexMandatory);

    //                HiddenField hdnFlexLabel = new HiddenField();
    //                hdnFlexLabel.ID = "hdnFlexLabel" + i;
    //                hdnFlexLabel.Value = Convert.ToString(row["flexLabel"]);
    //                divlblFlex1.Controls.Add(hdnFlexLabel);

    //                Label lblFlex1 = new Label();
    //                lblFlex1.ID = "lblFlex" + i;
    //                if (Convert.ToString(row["flexMandatoryStatus"]) == "N")
    //                {
    //                    lblFlex1.Text = "<strong>" + Convert.ToString(row["flexLabel"]) + ":</strong>";
    //                }
    //                else
    //                {
    //                    lblFlex1.Text = "<strong>" + Convert.ToString(row["flexLabel"]) + ":<span class='red_span'>*</span></strong>";
    //                }

    //                divlblFlex1.Controls.Add(lblFlex1);

    //                HtmlGenericControl divFlexControl = new HtmlGenericControl("Div");
    //                divFlexControl.Attributes.Add("class", "col-md-3");

    //                Label lblError = new Label();
    //                lblError.ID = "lblError" + i;
    //                lblError.Font.Bold = true;
    //                lblError.CssClass = "red_span";

    //                HtmlTableCell tableCell1 = new HtmlTableCell();
    //                switch (row["flexControl"].ToString())
    //                {
    //                    case "T": //TextBox
    //                        TextBox txtFlex = new TextBox();
    //                        txtFlex.ID = "txtFlex" + i;
    //                        txtFlex.CssClass = "form-control";
    //                        if (row["flexDataType"].ToString() == "N") //N means Numeric
    //                        {
    //                            txtFlex.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'1');");
    //                        }
    //                        if (row["Detail_FlexData"] != DBNull.Value)
    //                        {
    //                            txtFlex.Text = Convert.ToString(row["Detail_FlexData"]);
    //                        }
    //                        divFlexControl.Controls.Add(txtFlex);
    //                        divFlexControl.Controls.Add(lblError);
    //                        break;
    //                    case "D"://Date

    //                        DropDownList ddlDay = new DropDownList();
    //                        ddlDay.ID = "ddlDay" + i;
    //                        ddlDay.Width = new Unit(30, UnitType.Percentage);
    //                        ddlDay.CssClass = "form-control pull-left ";
    //                        BindDates(ddlDay);

    //                        DropDownList ddlMonth = new DropDownList();
    //                        ddlMonth.ID = "ddlMonth" + i;
    //                        ddlMonth.Width = new Unit(30, UnitType.Percentage);
    //                        ddlMonth.CssClass = "form-control pull-left ";
    //                        BindMonths(ddlMonth);

    //                        DropDownList ddlYear = new DropDownList();
    //                        ddlYear.ID = "ddlYear" + i;
    //                        ddlYear.Width = new Unit(40, UnitType.Percentage);
    //                        ddlYear.CssClass = "form-control pull-left ";
    //                        BindYears(ddlYear);

    //                        DateTime date;
    //                        if (row["Detail_FlexData"] != DBNull.Value)
    //                        {
    //                            try
    //                            {
    //                                date = Convert.ToDateTime(row["Detail_FlexData"]);
    //                                ddlDay.SelectedValue = date.Day.ToString();
    //                                ddlMonth.SelectedValue = date.Month.ToString();
    //                                ddlYear.SelectedValue = date.Year.ToString();
    //                            }
    //                            catch { }
    //                        }

    //                        divFlexControl.Controls.Add(ddlDay);
    //                        divFlexControl.Controls.Add(ddlMonth);
    //                        divFlexControl.Controls.Add(ddlYear);
    //                        divFlexControl.Controls.Add(lblError);
    //                        break;
    //                    case "L"://DropDown
    //                        DropDownList ddlFlex = new DropDownList();
    //                        ddlFlex.ID = "ddlFlex" + i;
    //                        ddlFlex.CssClass = "form-control";
    //                        DataTable dt = dctFlexFieldDropdownData != null && dctFlexFieldDropdownData.ContainsKey(Convert.ToString(row["flexLabel"])) ?
    //                            dctFlexFieldDropdownData[Convert.ToString(row["flexLabel"])] : CorporateProfile.FillDropDown(Convert.ToString(row["flexSqlQuery"]));

    //                        if (dt != null && dt.Rows.Count > 0)
    //                        {
    //                            if (dctFlexFieldDropdownData != null && !dctFlexFieldDropdownData.ContainsKey(Convert.ToString(row["flexLabel"])))
    //                                dctFlexFieldDropdownData.Add(Convert.ToString(row["flexLabel"]), dt);
    //                            ddlFlex.DataSource = dt;
    //                            ddlFlex.DataTextField = dt.Columns[1].ColumnName;
    //                            ddlFlex.DataValueField = dt.Columns[0].ColumnName;
    //                            ddlFlex.DataBind();
    //                        }
    //                        ddlFlex.Items.Insert(0, new ListItem("-- Select--", "-1"));
    //                        if (row["Detail_FlexData"] != DBNull.Value)
    //                        {
    //                            try
    //                            {
    //                                ddlFlex.SelectedValue = Convert.ToString(row["Detail_FlexData"]);
    //                            }
    //                            catch { }
    //                        }
    //                        divFlexControl.Controls.Add(ddlFlex);
    //                        divFlexControl.Controls.Add(lblError);
    //                        break;
    //                }
    //                HtmlGenericControl divClear = new HtmlGenericControl("Div");
    //                divClear.Attributes.Add("class", "clearfix");
    //                //divflex.Visible = true;
    //                tblFlexFields.Controls.Add(divlblFlex1); //Flex Label Binding
    //                tblFlexFields.Controls.Add(divFlexControl);//Flex Control Binding
    //                tblFlexFields.Controls.Add(divClear);
    //                i++;
    //            }
    //        }
    //    }
    //    catch { }
    //}

    /// <summary>
    /// 
    /// </summary>
    protected void GenerateItineraryForCorporateCriteria()
    {
        List<FlightItinerary> CriteriaItineries = new List<FlightItinerary>();
        if (Session["CriteriaItineraries"] == null)
        {
            sessionId = Session["sessionId"].ToString();
            SearchResult resultObj = SessionValues[ONWARD_RESULT] as SearchResult;
            if (Session["FlightRequest"] != null)
            {
                request = Session["FlightRequest"] as SearchRequest;
            }
            List<SearchResult> CriteriaResults = GetCriteriaResults(Basket.FlightBookingSession[sessionId].Result, resultObj);
            if (resultObj.ResultBookingSource == BookingSource.AirArabia)
            {
                Session["CriteriaResults"] = CriteriaResults;//This session will be cleared in CorpEmailAjax.aspx.cs after booking
            }
            FlightItinerary flightItinerary = Session["FlightItinerary"] as FlightItinerary;

            //======================================Reset PassengerTypes for Main Itinerary==============================================//
            //Assigning PassengerTypes for UAPI according to the passenger count to the main Itinerary 
            if (flightItinerary.FlightBookingSource == BookingSource.UAPI)
            {
                int adults = request.AdultCount, childs = request.ChildCount, infants = request.InfantCount;
                flightItinerary.UapiPricingSolution.AirPricingInfo[0].PassengerType = new UAPIdll.Air46.PassengerType[adults];
                for (int i = 0; i < adults; i++)
                {
                    flightItinerary.UapiPricingSolution.AirPricingInfo[0].PassengerType[i] = new UAPIdll.Air46.PassengerType();
                    flightItinerary.UapiPricingSolution.AirPricingInfo[0].PassengerType[i].Code = "ADT";
                    flightItinerary.UapiPricingSolution.AirPricingInfo[0].PassengerType[i].Age = "40";
                }
                if (request.ChildCount > 0)
                {
                    flightItinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType = new UAPIdll.Air46.PassengerType[childs];
                    for (int i = 0; i < childs; i++)
                    {
                        flightItinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType[i] = new UAPIdll.Air46.PassengerType();
                        flightItinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType[i].Code = "CNN";
                        flightItinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType[i].Age = "5";
                    }
                }
                if (request.ChildCount > 0 && request.InfantCount > 0)
                {
                    flightItinerary.UapiPricingSolution.AirPricingInfo[2].PassengerType = new UAPIdll.Air46.PassengerType[infants];
                    for (int i = 0; i < infants; i++)
                    {
                        flightItinerary.UapiPricingSolution.AirPricingInfo[2].PassengerType[i] = new UAPIdll.Air46.PassengerType();
                        flightItinerary.UapiPricingSolution.AirPricingInfo[2].PassengerType[i].Code = "INF";
                        flightItinerary.UapiPricingSolution.AirPricingInfo[2].PassengerType[i].Age = "1";
                    }
                }
                else if (request.InfantCount > 0)
                {
                    flightItinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType = new UAPIdll.Air46.PassengerType[infants];
                    for (int i = 0; i < infants; i++)
                    {
                        flightItinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType[i] = new UAPIdll.Air46.PassengerType();
                        flightItinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType[i].Code = "INF";
                        flightItinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType[i].Age = "1";
                    }
                }
            }
            //==============================================End PassengerTypes assigning===================================================//

            decimal rateOfExchange = 1;

            foreach (SearchResult result in CriteriaResults)
            {
                FlightItinerary itinerary = new FlightItinerary();

                //Assign the Flight Policy created
                itinerary.FlightPolicy = GenerateFlightPolicy(result);

                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    itinerary.AgencyId = Settings.LoginInfo.OnBehalfAgentID;//(int)Session["BookingAgencyID"];
                    itinerary.LocationId = Settings.LoginInfo.OnBehalfAgentLocation;  // on behalf of booking passing location id as -2
                    if (Settings.LoginInfo.OnBehalfAgentExchangeRates.ContainsKey(result.Price.SupplierCurrency))
                    {
                        rateOfExchange = Settings.LoginInfo.OnBehalfAgentExchangeRates[result.Price.SupplierCurrency];
                    }
                    else
                    {
                        rateOfExchange = 1;
                    }
                }
                else
                {
                    itinerary.LocationId = Convert.ToInt32(Settings.LoginInfo.LocationID);
                    itinerary.AgencyId = Settings.LoginInfo.AgentId;
                    if (Settings.LoginInfo.AgentExchangeRates.ContainsKey(result.Price.SupplierCurrency))
                    {
                        rateOfExchange = Settings.LoginInfo.AgentExchangeRates[result.Price.SupplierCurrency];
                    }
                    else
                    {
                        rateOfExchange = 1;
                    }
                }
                if (result.Flights[0][0].FlightId > 0)
                {
                    itinerary.BookingId = BookingDetail.GetBookingIdByFlightId(result.Flights[0][0].FlightId);
                }
                itinerary.BookingMode = BookingMode.BookingAPI;
                itinerary.CreatedBy = (int)Settings.LoginInfo.UserID;
                itinerary.CreatedOn = DateTime.Now;
                if (!string.IsNullOrEmpty(result.LastTicketDate))
                {
                    itinerary.LastTicketDate = Convert.ToDateTime(result.LastTicketDate);
                }
                //itinerary.Destination = result.Flights[0][0].Destination.CityName;
                itinerary.Destination = result.Flights[0][0].Destination.CityCode;
                itinerary.FareRules = result.FareRules;
                itinerary.FareType = (result.FareType != null ? result.FareType : FareType.Net.ToString());
                itinerary.FlightBookingSource = result.ResultBookingSource;
                itinerary.FlightId = result.Flights[0][0].FlightId;
                itinerary.ETicketHit = 1;
                itinerary.IsDomestic = false;
                itinerary.NonRefundable = result.NonRefundable;
                //itinerary.Origin = result.Flights[0][0].Origin.CityName;
                itinerary.Origin = result.Flights[0][0].Origin.CityCode;
                itinerary.PaymentMode = ModeOfPayment.Credit;
                itinerary.GUID = result.GUID;
                itinerary.SpecialRequest = result.JourneySellKey;
                itinerary.TicketAdvisory = result.FareSellKey;

                itinerary.Segments = SearchResult.GetSegments(result);
                foreach (FlightInfo segment in itinerary.Segments)
                {
                    segment.CreatedBy = (int)Settings.LoginInfo.UserID;
                    segment.CreatedOn = DateTime.Now;
                    segment.LastModifiedBy = segment.CreatedBy;
                    segment.LastModifiedOn = segment.CreatedOn;
                    if (segment.BookingClass.Trim().Length <= 0)
                    {
                        segment.BookingClass = "C";
                    }
                }
                //itinerary.TicketAdvisory = result.TicketAdvisory;
                itinerary.Ticketed = true;
                itinerary.TravelDate = result.Flights[0][0].DepartureTime;
                itinerary.UapiPricingSolution = result.UapiPricingSolution;
                //Assigning PassengerTypes for UAPI according to the passenger count
                if (itinerary.FlightBookingSource == BookingSource.UAPI)
                {
                    int adults = request.AdultCount, childs = request.ChildCount, infants = request.InfantCount;
                    itinerary.UapiPricingSolution.AirPricingInfo[0].PassengerType = new UAPIdll.Air46.PassengerType[adults];
                    for (int i = 0; i < adults; i++)
                    {
                        itinerary.UapiPricingSolution.AirPricingInfo[0].PassengerType[i] = new UAPIdll.Air46.PassengerType();
                        itinerary.UapiPricingSolution.AirPricingInfo[0].PassengerType[i].Code = "ADT";
                        itinerary.UapiPricingSolution.AirPricingInfo[0].PassengerType[i].Age = "40";
                    }
                    if (request.ChildCount > 0)
                    {
                        itinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType = new UAPIdll.Air46.PassengerType[childs];
                        for (int i = 0; i < childs; i++)
                        {
                            itinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType[i] = new UAPIdll.Air46.PassengerType();
                            itinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType[i].Code = "CNN";
                            itinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType[i].Age = "5";
                        }
                    }
                    if (request.ChildCount > 0 && request.InfantCount > 0)
                    {
                        itinerary.UapiPricingSolution.AirPricingInfo[2].PassengerType = new UAPIdll.Air46.PassengerType[infants];
                        for (int i = 0; i < infants; i++)
                        {
                            itinerary.UapiPricingSolution.AirPricingInfo[2].PassengerType[i] = new UAPIdll.Air46.PassengerType();
                            itinerary.UapiPricingSolution.AirPricingInfo[2].PassengerType[i].Code = "INF";
                            itinerary.UapiPricingSolution.AirPricingInfo[2].PassengerType[i].Age = "1";
                        }
                    }
                    else if (request.InfantCount > 0)
                    {
                        itinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType = new UAPIdll.Air46.PassengerType[infants];
                        for (int i = 0; i < infants; i++)
                        {
                            itinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType[i] = new UAPIdll.Air46.PassengerType();
                            itinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType[i].Code = "INF";
                            itinerary.UapiPricingSolution.AirPricingInfo[1].PassengerType[i].Age = "1";
                        }
                    }
                }
                itinerary.ValidatingAirlineCode = "";
                itinerary.IsLCC = result.IsLCC;//Will be True for Air Arabia, Fly Dubai, SpiceJet and for TBOAir LCC

                ////////////////////////TBO Air changes //////////////////////////
                if (result.ResultBookingSource == BookingSource.TBOAir)
                {
                    itinerary.SupplierLocatorCode = result.FareType;//LCCSpecialReturn/Normal Return/GDS Special Return
                    itinerary.TicketAdvisory = result.FareSellKey;//SessionId of the result
                    itinerary.UniversalRecord = result.JourneySellKey;//Save TBO Sources with | symbol
                    itinerary.FareType = "PUB";
                }
                //////////////////// End TBO Air changes /////////////////////////

                FlightPassenger pax = new FlightPassenger();
                pax.Title = ddlTitle.SelectedValue.Replace(".", "");
                pax.FirstName = txtPaxFName.Text;
                pax.LastName = txtPaxLName.Text;
                pax.Type = PassengerType.Adult;
                pax.AddressLine1 = txtAddress1.Text;
                pax.AddressLine2 = txtAddress2.Text;
                pax.CellPhone = txtMobileCountryCode.Text + '-' + txtMobileNo.Text;
                //Added by Lokesh on 25-sept-2017 regarding Pax Destination phone Number
                if (txtDestCntCode.Text.Length > 0 && txtDestPhoneNo.Text.Length > 0)
                {
                    pax.DestinationPhone = txtDestCntCode.Text + '-' + txtDestPhoneNo.Text;
                }
                else
                {
                    pax.DestinationPhone = string.Empty;
                }
                pax.City = "";

                //pax.City = txtCity.Text;

                if (ddlCountry.SelectedIndex > 0)
                {
                    pax.Country = Country.GetCountry(ddlCountry.SelectedItem.Value);
                }

                pax.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                pax.CreatedOn = DateTime.Now;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                if (ddlDay.SelectedIndex > 0 && ddlMonth.SelectedIndex > 0 && ddlYear.SelectedIndex > 0)
                {
                    pax.DateOfBirth = Convert.ToDateTime(ddlDay.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue, dateFormat);
                }
                pax.Email = txtEmail.Text;
                pax.FFAirline = txtAirline.Text;
                pax.FFNumber = txtFlight.Text;
                pax.FlightId = result.Flights[0][0].FlightId;
                pax.Gender = (Gender)Convert.ToInt32(ddlGender.SelectedValue);
                pax.IsLeadPax = true;
                pax.Meal = new Meal();
                if (ddlNationality.SelectedIndex > 0)
                {
                    pax.Nationality = Country.GetCountry(ddlNationality.SelectedValue);
                }
                if (ddlPEDay.SelectedIndex > 0 && ddlPEMonth.SelectedIndex > 0 && ddlPEYear.SelectedIndex > 0)
                {
                    pax.PassportExpiry = Convert.ToDateTime(ddlPEDay.SelectedValue + "/" + ddlPEMonth.SelectedValue + "/" + ddlPEYear.SelectedValue, dateFormat);
                }
                if (!string.IsNullOrEmpty(txtPassportNo.Text) && txtPassportNo.Text.Length > 0)
                {
                    pax.PassportNo = txtPassportNo.Text;
                }
                pax.Type = PassengerType.Adult;
                pax.Seat = new Seat();

                pax.Price = new PriceAccounts();
                pax.Price.AccPriceType = (result.FareType == "NetFare" ? PriceType.NetFare : PriceType.PublishedFare);
                if (pax.Price.AccPriceType == PriceType.NetFare)
                {
                    pax.Price.NetFare = (decimal)result.FareBreakdown[0].BaseFare / result.FareBreakdown[0].PassengerCount;
                }
                else
                {
                    pax.Price.PublishedFare = (decimal)result.FareBreakdown[0].BaseFare / result.FareBreakdown[0].PassengerCount;
                }
                pax.Price.SupplierCurrency = result.Price.SupplierCurrency;
                if (result.ResultBookingSource == BookingSource.TBOAir)
                {
                    pax.Price.SupplierPrice = (decimal)result.Price.SupplierPrice;
                }
                else
                {
                    pax.Price.SupplierPrice = (decimal)result.FareBreakdown[0].SupplierFare / result.FareBreakdown[0].PassengerCount;
                }
                pax.Price.Markup = result.Price.Markup;
                pax.Price.MarkupType = result.Price.MarkupType;
                pax.Price.MarkupValue = result.Price.MarkupValue;
                pax.Price.DecimalPoint = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentDecimalValue : Settings.LoginInfo.DecimalValue);
                pax.Price.Tax = result.FareBreakdown[0].Tax / result.FareBreakdown[0].PassengerCount;
                pax.Price.K3Tax = result.Price.K3Tax / paxCount;
                pax.Price.Markup = result.FareBreakdown[0].AgentMarkup / result.FareBreakdown[0].PassengerCount;
                pax.Price.Discount = Convert.ToDecimal(result.FareBreakdown[0].AgentDiscount.ToString("N" + agency.DecimalValue)) / result.FareBreakdown[0].PassengerCount;
                pax.Price.RateOfExchange = rateOfExchange;
                pax.Price.Currency = result.Currency;
                pax.Price.CurrencyCode = result.Currency;
                pax.Price.WhiteLabelDiscount = 0;
                if (result.ResultBookingSource == BookingSource.FlyDubai)
                {
                    pax.Price.TransactionFee = result.Price.TransactionFee;
                }
                else
                {
                    pax.Price.TransactionFee = 0;
                }
                pax.Price.CurrencyCode = result.Currency;
                pax.Price.AdditionalTxnFee = result.FareBreakdown[0].AdditionalTxnFee; //sai
                if (result.ResultBookingSource == BookingSource.FlyDubai)
                {
                    pax.Price.FareInformationID = result.FareInformationId[PassengerType.Adult.ToString()];
                }

                if (!string.IsNullOrEmpty(hdfAsvAmount.Value))
                {
                    pax.Price.AsvAmount = Convert.ToDecimal(hdfAsvAmount.Value) / paxCount;
                }
                else
                {
                    pax.Price.AsvAmount = 0;
                }
                if (!string.IsNullOrEmpty(hdfAsvType.Value))
                {
                    pax.Price.AsvType = hdfAsvType.Value;
                }
                else
                {
                    pax.Price.AsvType = null;
                }
                if (!string.IsNullOrEmpty(hdfAsvElement.Value))
                {
                    pax.Price.AsvElement = hdfAsvElement.Value;
                }
                else
                {
                    pax.Price.AsvElement = null;
                }

                if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                {
                    pax.Price.ChargeBU = result.Price.ChargeBU;
                    pax.Price.YQTax = result.Price.YQTax;
                    pax.Price.AgentCommission = result.Price.AgentCommission;
                    pax.Price.SServiceFee = result.Price.SServiceFee;
                    pax.Price.IncentiveEarned = result.Price.IncentiveEarned;
                    pax.Price.TDSIncentive = result.Price.TDSIncentive;
                    pax.Price.AgentPLB = result.Price.AgentPLB;
                    pax.Price.TDSPLB = result.Price.TDSPLB;
                    pax.Price.TdsCommission = result.Price.TdsCommission;
                    pax.Price.AdditionalTxnFee = result.Price.AdditionalTxnFee;
                    pax.Price.TransactionFee = result.Price.TransactionFee;
                    pax.Price.OtherCharges = result.Price.OtherCharges;
                    pax.Price.SupplierCurrency = result.Price.SupplierCurrency;
                }
                pax.Price.IsServiceTaxOnBaseFarePlusYQ = false;

                if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                {
                    pax.TBOPrice = new PriceAccounts();
                    pax.TBOPrice.AccPriceType = PriceType.PublishedFare;
                    pax.TBOPrice.PublishedFare = (decimal)result.TBOPrice.PublishedFare / paxCount;
                    pax.TBOPrice.BaseFare = result.TBOPrice.BaseFare;
                    pax.TBOPrice.Tax = (decimal)result.TBOPrice.Tax;
                    pax.TBOPrice.Markup = result.TBOPrice.Markup;
                    pax.TBOPrice.MarkupType = result.TBOPrice.MarkupType;
                    pax.TBOPrice.MarkupValue = result.TBOPrice.MarkupValue;
                    pax.TBOPrice.DecimalPoint = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentDecimalValue : Settings.LoginInfo.DecimalValue);
                    pax.TBOPrice.Markup = result.FareBreakdown[0].AgentMarkup / result.FareBreakdown[0].PassengerCount;
                    pax.TBOPrice.Discount = result.FareBreakdown[0].AgentDiscount / result.FareBreakdown[0].PassengerCount;
                    pax.TBOPrice.RateOfExchange = 1;
                    pax.TBOPrice.Currency = result.TBOPrice.Currency;
                    pax.TBOPrice.CurrencyCode = result.TBOPrice.Currency;
                    pax.TBOPrice.WhiteLabelDiscount = 0;
                    pax.TBOPrice.WLCharge = result.TBOPrice.WLCharge;
                    pax.TBOPrice.NetFare = result.TBOPrice.NetFare;
                    pax.TBOPrice.OtherCharges = result.TBOPrice.OtherCharges;
                    pax.TBOPrice.TransactionFee = result.TBOPrice.TransactionFee;
                    pax.TBOPrice.AdditionalTxnFee = result.TBOPrice.AdditionalTxnFee; //sai               
                    pax.TBOPrice.ChargeBU = result.TBOPrice.ChargeBU;
                    pax.TBOPrice.AgentCommission = result.TBOPrice.AgentCommission;
                    pax.TBOPrice.AirlineTransFee = result.TBOPrice.AirlineTransFee;
                    pax.TBOPrice.AgentPLB = result.TBOPrice.AgentPLB;
                    pax.TBOPrice.IncentiveEarned = result.TBOPrice.IncentiveEarned;
                    pax.TBOPrice.TDSIncentive = result.TBOPrice.TDSIncentive;
                    pax.TBOPrice.TDSPLB = result.TBOPrice.TDSPLB;
                    pax.TBOPrice.TdsCommission = result.TBOPrice.TdsCommission;

                    itinerary.TBOFareBreakdown = result.TBOFareBreakdown;
                }
                pax.Price.IsServiceTaxOnBaseFarePlusYQ = false;
                if (result.IsLCC)
                {
                    pax.BaggageCode = flightItinerary.Passenger[0].BaggageCode;
                    pax.BaggageType = flightItinerary.Passenger[0].BaggageType;
                    pax.Price.BaggageCharge = flightItinerary.Passenger[0].Price.BaggageCharge;
                }
                else
                {
                    pax.BaggageCode = string.Empty;
                    pax.BaggageType = string.Empty;
                }
                pax.FlyDubaiBaggageCharge = new List<decimal>();

                FlightPassenger[] paxList = new FlightPassenger[paxCount];
                paxList[0] = pax;

                int counter = 1;

                foreach (DataListItem item in dlAdditionalPax.Items)
                {
                    Label lblPaxType = item.FindControl("lblPaxType") as Label;
                    DropDownList ddlPaxTitle = item.FindControl("ddlPaxTitle") as DropDownList;
                    TextBox txtPaxFirstName = item.FindControl("txtPaxFName") as TextBox;
                    TextBox txtPaxLastName = item.FindControl("txtPaxLName") as TextBox;
                    DropDownList ddlPaxGender = item.FindControl("ddlGender") as DropDownList;
                    DropDownList ddlPaxDay = item.FindControl("ddlDay") as DropDownList;
                    DropDownList ddlPaxMonth = item.FindControl("ddlMonth") as DropDownList;
                    DropDownList ddlPaxYear = item.FindControl("ddlYear") as DropDownList;
                    TextBox txtPaxPassportNo = item.FindControl("txtPassportNo") as TextBox;
                    DropDownList ddlPaxPEDay = item.FindControl("ddlPEDay") as DropDownList;
                    DropDownList ddlPaxPEMonth = item.FindControl("ddlPEMonth") as DropDownList;
                    DropDownList ddlPaxPEYear = item.FindControl("ddlPEYear") as DropDownList;
                    DropDownList ddlPaxCountry = item.FindControl("ddlCountry") as DropDownList;
                    DropDownList ddlPaxNationality = item.FindControl("ddlNationality") as DropDownList;
                    DropDownList ddlOnwardBaggage = item.FindControl("ddlOnwardBaggage") as DropDownList;
                    DropDownList ddlInwardBaggage = item.FindControl("ddlInwardBaggage") as DropDownList;

                    FlightPassenger pax1 = new FlightPassenger();
                    pax1.Title = ddlPaxTitle.SelectedValue.Replace(".", "");
                    pax1.FirstName = txtPaxFirstName.Text;
                    pax1.LastName = txtPaxLastName.Text;
                    switch (lblPaxType.Text)
                    {
                        case "Adult":
                            pax1.Type = PassengerType.Adult;
                            break;
                        case "Child":
                            pax1.Type = PassengerType.Child;
                            break;
                        case "Infant":
                            pax1.Type = PassengerType.Infant;
                            break;
                    }
                    pax1.AddressLine1 = txtAddress1.Text;
                    pax1.AddressLine2 = txtAddress2.Text;
                    pax1.CellPhone = txtMobileCountryCode.Text + '-' + txtMobileNo.Text;
                    pax1.City = "";
                    if (ddlPaxCountry.SelectedIndex > 0)
                    {
                        pax1.Country = Country.GetCountry(ddlPaxCountry.SelectedItem.Value);
                    }
                    pax1.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                    pax1.CreatedOn = DateTime.Now;
                    if (ddlPaxDay.SelectedIndex > 0 && ddlPaxMonth.SelectedIndex > 0 && ddlPaxYear.SelectedIndex > 0)
                    {
                        pax1.DateOfBirth = Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue, dateFormat);
                    }
                    pax1.Email = txtEmail.Text;
                    pax1.FFAirline = txtAirline.Text;
                    pax1.FFNumber = txtFlight.Text;
                    pax1.FlightId = result.Flights[0][0].FlightId;
                    pax1.Gender = (Gender)Convert.ToInt32(ddlPaxGender.SelectedValue);
                    pax1.IsLeadPax = false;
                    pax1.Meal = new Meal();
                    if (ddlPaxNationality.SelectedIndex > 0)
                    {
                        pax1.Nationality = Country.GetCountry(ddlPaxNationality.SelectedValue);
                    }

                    if (ddlPaxPEDay.SelectedIndex > 0 && ddlPaxPEMonth.SelectedIndex > 0 && ddlPaxPEYear.SelectedIndex > 0)
                    {
                        pax1.PassportExpiry = Convert.ToDateTime(ddlPaxPEDay.SelectedValue + "/" + ddlPaxPEMonth.SelectedValue + "/" + ddlPaxPEYear.SelectedValue, dateFormat);
                    }
                    if (!string.IsNullOrEmpty(txtPaxPassportNo.Text) && txtPaxPassportNo.Text.Length > 0)
                    {
                        pax1.PassportNo = txtPaxPassportNo.Text;
                    }

                    pax1.Price = new PriceAccounts();
                    for (int i = 0; i < result.FareBreakdown.Length; i++)
                    {
                        if (pax1.Type == result.FareBreakdown[i].PassengerType)
                        {
                            pax1.Price.AccPriceType = (result.FareType == "NetFare" ? PriceType.NetFare : PriceType.PublishedFare);
                            if (pax1.Price.AccPriceType == PriceType.NetFare)
                            {
                                pax1.Price.NetFare = (decimal)result.FareBreakdown[i].BaseFare / result.FareBreakdown[i].PassengerCount;
                            }
                            else
                            {
                                pax1.Price.PublishedFare = (decimal)result.FareBreakdown[i].BaseFare / result.FareBreakdown[i].PassengerCount;
                            }
                            pax1.Price.SupplierCurrency = result.Price.SupplierCurrency;
                            if (result.ResultBookingSource == BookingSource.TBOAir)
                            {
                                pax1.Price.SupplierPrice = 0;
                            }
                            else
                            {
                                pax1.Price.SupplierPrice = (decimal)result.FareBreakdown[i].SupplierFare / result.FareBreakdown[i].PassengerCount;
                            }
                            pax1.Price.Markup = result.Price.Markup;
                            pax1.Price.MarkupType = result.Price.MarkupType;
                            pax1.Price.MarkupValue = result.Price.MarkupValue;
                            pax1.Price.DecimalPoint = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentDecimalValue : Settings.LoginInfo.DecimalValue);
                            pax1.Price.Tax = (decimal)result.FareBreakdown[i].Tax / result.FareBreakdown[i].PassengerCount;
                            pax1.Price.K3Tax = result.Price.K3Tax / paxCount;
                            pax1.Price.AdditionalTxnFee = (decimal)result.FareBreakdown[i].AdditionalTxnFee / result.FareBreakdown[i].PassengerCount;
                            pax1.Price.Markup = (decimal)result.FareBreakdown[i].AgentMarkup / result.FareBreakdown[i].PassengerCount;
                            pax1.Price.Discount = Convert.ToDecimal(result.FareBreakdown[i].AgentDiscount.ToString("N" + agency.DecimalValue)) / result.FareBreakdown[i].PassengerCount;
                        }
                        if (result.ResultBookingSource == BookingSource.FlyDubai)
                        {
                            pax1.Price.FareInformationID = result.FareInformationId[pax1.Type.ToString()];
                            pax1.Price.TransactionFee = result.Price.TransactionFee;
                        }
                        else
                        {
                            pax1.Price.TransactionFee = 0;
                        }
                        if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                        {
                            pax1.Price.ChargeBU = result.Price.ChargeBU;
                            pax1.Price.YQTax = 0;
                            pax1.Price.AgentCommission = 0;
                            pax1.Price.SServiceFee = 0;
                            pax1.Price.IncentiveEarned = 0;
                            pax1.Price.TDSIncentive = 0;
                            pax1.Price.AgentPLB = 0;
                            pax1.Price.TDSPLB = 0;
                            pax1.Price.TdsCommission = 0;
                            pax1.Price.AdditionalTxnFee = 0;
                            pax1.Price.TransactionFee = 0;
                            pax1.Price.OtherCharges = 0;
                            pax1.Price.SupplierCurrency = result.Price.SupplierCurrency;
                        }

                        if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                        {
                            pax1.TBOPrice = new PriceAccounts();
                            pax1.TBOPrice.AccPriceType = PriceType.PublishedFare;
                            pax1.TBOPrice.PublishedFare = (decimal)result.TBOPrice.PublishedFare / paxCount;
                            pax1.TBOPrice.BaseFare = result.TBOPrice.BaseFare;
                            pax1.TBOPrice.Tax = (decimal)result.TBOPrice.Tax;
                            pax1.TBOPrice.Markup = result.TBOPrice.Markup;
                            pax1.TBOPrice.MarkupType = result.TBOPrice.MarkupType;
                            pax1.TBOPrice.MarkupValue = result.TBOPrice.MarkupValue;
                            pax1.TBOPrice.DecimalPoint = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentDecimalValue : Settings.LoginInfo.DecimalValue);
                            pax1.TBOPrice.Markup = result.FareBreakdown[i].AgentMarkup / result.FareBreakdown[i].PassengerCount;
                            pax1.TBOPrice.Discount = result.FareBreakdown[i].AgentDiscount / result.FareBreakdown[i].PassengerCount;
                            pax1.TBOPrice.RateOfExchange = 1;
                            pax1.TBOPrice.Currency = result.TBOPrice.Currency;
                            pax1.TBOPrice.CurrencyCode = result.TBOPrice.Currency;
                            pax1.TBOPrice.WhiteLabelDiscount = 0;
                            pax1.TBOPrice.WLCharge = result.TBOPrice.WLCharge;
                            pax1.TBOPrice.NetFare = result.TBOPrice.NetFare;
                            pax1.TBOPrice.OtherCharges = result.TBOPrice.OtherCharges;
                            pax1.TBOPrice.TransactionFee = result.TBOPrice.TransactionFee;
                            pax1.TBOPrice.AdditionalTxnFee = result.TBOPrice.AdditionalTxnFee; //sai               
                            pax1.TBOPrice.ChargeBU = result.TBOPrice.ChargeBU;
                            pax1.TBOPrice.AgentCommission = result.TBOPrice.AgentCommission;

                            pax1.TBOPrice.AirlineTransFee = result.TBOPrice.AirlineTransFee;
                            pax1.TBOPrice.AgentPLB = result.TBOPrice.AgentPLB;

                            pax1.TBOPrice.IncentiveEarned = result.TBOPrice.IncentiveEarned;
                            pax1.TBOPrice.TDSIncentive = result.TBOPrice.TDSIncentive;
                            pax1.TBOPrice.TDSPLB = result.TBOPrice.TDSPLB;
                            pax1.TBOPrice.TdsCommission = result.TBOPrice.TdsCommission;
                        }
                    }
                    //sai
                    if (!string.IsNullOrEmpty(hdfAsvAmount.Value))
                    {
                        pax1.Price.AsvAmount = Convert.ToDecimal(hdfAsvAmount.Value) / paxCount;
                    }
                    else
                    {
                        pax1.Price.AsvAmount = 0;
                    }
                    if (!string.IsNullOrEmpty(hdfAsvType.Value))
                    {
                        pax1.Price.AsvType = hdfAsvType.Value;
                    }
                    else
                    {
                        pax1.Price.AsvType = null;
                    }
                    if (!string.IsNullOrEmpty(hdfAsvElement.Value))
                    {
                        pax1.Price.AsvElement = hdfAsvElement.Value;
                    }
                    else
                    {
                        pax1.Price.AsvElement = null;
                    }
                    if (result.IsLCC)
                    {
                        pax1.BaggageCode = flightItinerary.Passenger[counter].BaggageCode;
                        pax1.BaggageType = flightItinerary.Passenger[counter].BaggageType;
                        pax1.Price.BaggageCharge = flightItinerary.Passenger[counter].Price.BaggageCharge;
                    }
                    else
                    {
                        pax1.BaggageCode = pax.BaggageCode;
                        pax1.BaggageType = pax.BaggageType;
                    }
                    pax1.FlyDubaiBaggageCharge = pax.FlyDubaiBaggageCharge;
                    pax1.Price.RateOfExchange = rateOfExchange;
                    pax1.Price.Currency = result.Currency;
                    pax1.Price.CurrencyCode = result.Currency;
                    pax1.Price.WhiteLabelDiscount = 0;

                    pax1.Price.CurrencyCode = result.Currency;

                    pax1.Price.IsServiceTaxOnBaseFarePlusYQ = false;

                    paxList[counter] = pax1;
                    counter++;
                }
                itinerary.Passenger = paxList;
                CriteriaItineries.Add(itinerary);
            }

            Session["CriteriaItineraries"] = CriteriaItineries;

        }
        else
        {
            CriteriaItineries = Session["CriteriaItineraries"] as List<FlightItinerary>;
        }
    }

    void BindDates(DropDownList ddlDay)
    {
        ddlDay.Items.Add(new ListItem("Day", "-1"));
        for (int i = 1; i <= 31; i++)
        {
            ddlDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }

    void BindMonths(DropDownList ddlMonth)
    {
        ddlMonth.Items.Add(new ListItem("Month", "-1"));
        string[] months = new string[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        for (int i = 0; i < months.Length; i++)
        {
            ddlMonth.Items.Add(new ListItem(months[i], (i + 1).ToString()));
        }
    }

    void BindYears(DropDownList ddlYear)
    {
        ddlYear.Items.Add(new ListItem("Year", "-1"));
        for (int i = 1930; i <= DateTime.Now.Year; i++)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }

    /// <summary>
    /// Calculate the default baggage weight for AirIndiaExpressFlights
    /// </summary>
    /// <param name="baggageIncludedInFare"></param>
    /// <returns></returns>
    private int AssignAirIndiaExpressDefaultBaggage(string baggageIncludedInFare)
    {
        int baggageIncluded = 0;
        try
        {
            #region BaggageCases
            //The following are the baggage cases available for AirIndiaExpress
            //****************Onward***********************************
            //Case -1: "ADT 30kgs/CHD 30kgs/INF 10Kgs"
            //Case-2 : "0"
            //**********************************************************

            //****************Return************************************
            //Case -1: "ADT 30kgs/CHD 30kgs/INF 10Kgs" ,"ADT 30kgs/CHD 30kgs/INF 10Kgs" 
            //Case-2 : "0" ,"ADT 30kgs/CHD 30kgs/INF 10Kgs".
            //Case-3 : "ADT 30kgs/CHD 30kgs/INF 10Kgs" ,"0".
            //Case-4 : "0" ,"0".
            //**********************************************************


            string defaultBaggageAIE = string.Empty; //Default baggage for AirIndiaExpress from fareQuoteResponse
            string defaultBaggageOnwardAIE = string.Empty;//Default Baggage Onward
            string defaultBaggageWeightOnwardAIE = string.Empty;//Default baggage weight onward 
            string defaultBaggageReturnAIE = string.Empty;//Default Baggage Return
            string defaultBaggageWeightReturnAIE = string.Empty;//Default baggage weight return

            //Free baggage 20Kgs,0
            //0,Free baggage 20Kgs
            defaultBaggageAIE = baggageIncludedInFare;
            defaultBaggageOnwardAIE = defaultBaggageAIE.Split(',')[0];
            if (defaultBaggageOnwardAIE.ToLower().Contains("free baggage"))
            {
                defaultBaggageWeightOnwardAIE = defaultBaggageOnwardAIE.Split(' ')[2].Substring(0, 2);
            }
            else
            {
                defaultBaggageWeightOnwardAIE = defaultBaggageOnwardAIE;
            }

            if (defaultBaggageAIE.Split(',').Length > 1 && request.Type == SearchType.Return)
            {
                defaultBaggageReturnAIE = defaultBaggageAIE.Split(',')[1];
            }

            if (!string.IsNullOrEmpty(defaultBaggageReturnAIE))
            {
                if (defaultBaggageReturnAIE.ToLower().Contains("free baggage") && request.Type == SearchType.Return)
                {
                    defaultBaggageWeightReturnAIE = defaultBaggageReturnAIE.Split(' ')[2].Substring(0, 2);
                }
                else
                {
                    defaultBaggageWeightReturnAIE = defaultBaggageReturnAIE;
                }
            }

            if (request.Type == SearchType.Return)//SearchType : RoundTrip 
            {
                baggageIncluded = Convert.ToInt32(defaultBaggageWeightOnwardAIE) + Convert.ToInt32(defaultBaggageWeightReturnAIE);
            }
            else //SearchType : OneWay 
            {
                baggageIncluded = Convert.ToInt32(defaultBaggageWeightOnwardAIE);
            }
            #endregion
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return baggageIncluded;
    }

    /// <summary>
    /// Assign pax baggage codes for Air India Express Flights
    /// </summary>
    /// <param name="pax"></param>
    /// <param name="result"></param>
    /// <param name="type"></param>
    /// <param name="ddlOutBag"></param>
    /// <param name="ddlInBag"></param>
    void AssignAirIndiaExpressPaxBaggage(FlightPassenger pax, SearchResult result, SearchType type, DropDownList ddlOutBag, DropDownList ddlInBag)
    {
        try
        {
            int paxIndex = 0;
            if (pax.Type == PassengerType.Child)
            {
                paxIndex = 1;
            }
            else if (pax.Type == PassengerType.Infant)
            {
                paxIndex = 2;
            }

            #region BaggageCases
            //The following are the baggage cases available for AirIndiaExpress
            //****************Onward***********************************
            //Case -1: "ADT 30kgs/CHD 30kgs/INF 10Kgs"
            //Case-2 : "0"
            //**********************************************************

            //****************Return************************************
            //Case -1: "ADT 30kgs/CHD 30kgs/INF 10Kgs" ,"ADT 30kgs/CHD 30kgs/INF 10Kgs" 
            //Case-2 : "0" ,"ADT 30kgs/CHD 30kgs/INF 10Kgs".
            //Case-3 : "ADT 30kgs/CHD 30kgs/INF 10Kgs" ,"0".
            //Case-4 : "0" ,"0".
            //**********************************************************


            string defaultBaggage = string.Empty; //Default baggage for AirIndiaExpress from fareQuoteResponse
            string defaultBaggageOnward = string.Empty;//Default Baggage Onward
            string defaultBaggageWeightOnward = string.Empty;//Default baggage weight onward 
            string defaultBaggageReturn = string.Empty;//Default Baggage Return
            string defaultBaggageWeightReturn = string.Empty;//Default baggage weight return


            defaultBaggage = result.BaggageIncludedInFare;
            defaultBaggageOnward = defaultBaggage.Split(',')[0];
            if (defaultBaggageOnward.ToLower().Contains("free baggage"))
            {
                defaultBaggageWeightOnward = defaultBaggageOnward.Split(' ')[2].Substring(0, 2);
            }
            else
            {
                defaultBaggageWeightOnward = defaultBaggageOnward;
            }


            if (defaultBaggage.Split(',').Length > 1 && request.Type == SearchType.Return)
            {
                defaultBaggageReturn = defaultBaggage.Split(',')[1];
            }

            if (!string.IsNullOrEmpty(defaultBaggageReturn))
            {
                if (defaultBaggageReturn.ToLower().Contains("free baggage"))
                {
                    defaultBaggageWeightReturn = defaultBaggageReturn.Split(' ')[2].Substring(0, 2);
                }
                else
                {
                    defaultBaggageWeightReturn = defaultBaggageReturn;
                }
            }
            #endregion

            if (request.Type == SearchType.Return && pax.Type != PassengerType.Infant)
            {
                // For pax type adult if both onward and return additional baggage is available and selected both of them
                if (ddlInBag.Visible && ddlOutBag.Visible && ddlInBag.SelectedIndex > 0 && ddlOutBag.SelectedIndex > 0)
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG +" + ddlOutBag.SelectedItem.Text.Substring(0, 5);
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG +" + ddlInBag.SelectedItem.Text.Substring(0, 5);
                }
                // For pax type adult if both onward and return additional baggage is available and only onward additional baggage is selected
                else if (ddlInBag.Visible && ddlOutBag.Visible && ddlInBag.SelectedIndex <= 0 && ddlOutBag.SelectedIndex > 0)
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG +" + ddlOutBag.SelectedItem.Text.Substring(0, 5);
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG";
                }
                // For pax type adult if both onward and return additional baggage is available and only return additional baggage is selected
                else if (ddlInBag.Visible && ddlOutBag.Visible && ddlInBag.SelectedIndex > 0 && ddlOutBag.SelectedIndex <= 0)
                {

                    pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG +" + ddlInBag.SelectedItem.Text.Substring(0, 5);
                }

                // For pax type adult if only onward  additional baggage is available and only onward additional baggage is selected
                else if (ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG +" + ddlOutBag.SelectedItem.Text.Substring(0, 5);
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG";
                }
                // For pax type adult if only onward  additional baggage is available and  onward additional baggage is not selected
                else if (ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0)
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG";
                }
                // For pax type adult if only return  additional baggage is available and only return additional baggage is selected
                else if (ddlInBag.Visible && ddlInBag.SelectedIndex > 0)
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG +" + ddlInBag.SelectedItem.Text.Substring(0, 5);
                }
                // For pax type adult if only return  additional baggage is available and return additional baggage is not selected
                else if (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0)
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG";
                }
                // For pax type adult both onward and return additional baggage available and selected none of them.
                else if (ddlInBag.Visible && ddlInBag.SelectedIndex <= 0 && ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0)
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG";
                }
                // For pax type adult both onward and return additional baggage is not  available
                else if (!ddlInBag.Visible && !ddlOutBag.Visible)
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG";
                }
                //For pax type adult only  onward additional baggage is   available and selected
                else if (!ddlInBag.Visible && ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG +" + ddlOutBag.SelectedItem.Text.Substring(0, 5);
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG";
                }
                //For pax type adult only  onward additional baggage is   available and not selected
                else if (!ddlInBag.Visible && ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0)
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG";
                }
                // For pax type adult if only return  additional baggage is available and only return additional baggage is selected
                else if (!ddlOutBag.Visible && ddlInBag.Visible && ddlInBag.SelectedIndex > 0)
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG +" + ddlInBag.SelectedItem.Text.Substring(0, 5);
                }
                // For pax type adult if only return  additional baggage is available and only return additional baggage is not selected
                else if (!ddlOutBag.Visible && ddlInBag.Visible && ddlInBag.SelectedIndex <= 0)
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG";
                }
                else
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                    pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG";
                }
            }
            else if (request.Type == SearchType.Return && pax.Type == PassengerType.Infant)
            {
                pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                pax.BaggageCode += "," + defaultBaggageWeightReturn + "KG";
            }
            else//One Way
            {
                if (pax.Type != PassengerType.Infant)
                {
                    //Onward additional Baggage available and selected the same
                    if (ddlOutBag.Visible && ddlOutBag.SelectedIndex > 0)
                    {
                        pax.BaggageCode = defaultBaggageWeightOnward + "KG +" + ddlOutBag.SelectedItem.Text.Substring(0, 5);
                    }

                    //Onward additional Baggage available and not selected
                    else if (ddlOutBag.Visible && ddlOutBag.SelectedIndex <= 0)
                    {
                        pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                    }

                    //Onward additional Baggage not available only default baggage available
                    else if (!ddlOutBag.Visible)
                    {
                        pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                    }
                }
                else if (pax.Type == PassengerType.Infant)
                {
                    pax.BaggageCode = defaultBaggageWeightOnward + "KG";
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// This method returns the mandatory fields for flight product based on the result booking source ,airline and counrty code.
    /// </summary>
    /// <param name="result"></param>
    protected void SetupAPISMandateFields(SearchResult onwardResult, SearchResult returnResult)
    {
        try
        {
            /****************************Start:Mandate Fields**********************/
            /*The below fields are mandatory irrespective of country ,airline or common(includes both country and airline)*/
            /*Title,FirstName,LastName,Phone,Email*/
            /*For any source if the destination country falls with in the range and valiadtes against all the airlines then this is considered as highest priority.*/
            /****************************End:Mandate Fields**********************/

            //The below fields are mandatory common to "Indigo,SpiceJet" and "TBo Air"
            /*  if (result.ResultBookingSource == BookingSource.TBOAir || result.ResultBookingSource == BookingSource.Indigo || result.ResultBookingSource == BookingSource.SpiceJet)
              {
                  GenderRequired = true;
                  AddressRequired = true;
                  CountryRequired = true;
                  DOBRequired = true;
              }
              if (result.ResultBookingSource == BookingSource.Indigo)
              {
                  NationalityRequired = true;
              }
              //The below fields are mandatory common to "Fly Dubai, and AirIndiaExpress"
              if (result.ResultBookingSource == BookingSource.FlyDubai || result.ResultBookingSource == BookingSource.AirIndiaExpressIntl)
              {
                  DOBRequired = true;
                  CountryRequired = true;
                  PassportNoRequired = true; //Since Passport Number required ,passport expiry is also mandatory
                  GenderRequired = true;
                  PassportExpiryRequired = true;

              }
              //The below fields are mandatory common to "UAPI Source only"
              if (result.ResultBookingSource == BookingSource.UAPI)
              {
                  DOBRequired = true;
                  CountryRequired = true;
                  GenderRequired = true;
              }

              //The below fields are mandatory common to "UAPI Source only"
              if (result.ResultBookingSource == BookingSource.AirArabia)
              {
                  CountryRequired = true;
                  NationalityRequired = true;
              } */



            DataTable dtOnwardFields = new DataTable();
            DataTable dtReturnFields = new DataTable();
            AirlineMinimalFields apisFields = new AirlineMinimalFields();
            //Get the destination country code since we validate against the destination country code.
            Airport destinationAirport = new Airport(request.Segments[0].Destination);//Search Type : One Way or Return.
            Airport originAirport = new Airport(request.Segments[0].Origin);

            string listCountries = string.Empty;
            string listAirlines = string.Empty;
            bool isDomestic = false;
            string flightType = "INTERNATIONAL";
            if (originAirport.CountryCode == destinationAirport.CountryCode)
            {
                isDomestic = true;
                flightType = "DOMESTIC";
            }
            if (request.Type == SearchType.MultiWay || request.Type == SearchType.OneWay || request.Type == SearchType.Return)
            {
                //Get the result object destination cities airport codes
                if (request.Segments.Length > 0)
                {
                    for (int i = 0; i < request.Segments.Length; i++)
                    {
                        //Lokesh : 18May2017 -- Need to validate origin airport country code also.
                        //So,Get both the origin && destination airport country code since we have to validate against both the origin && destination airport country code.

                        destinationAirport = new Airport(request.Segments[i].Destination);
                        if (listCountries.Length > 0)
                        {
                            if (!listCountries.Contains(destinationAirport.CountryCode))
                            {
                                //At the database side we are checking in operator so we append single quotes for destination cities
                                listCountries += "," + "'" + destinationAirport.CountryCode + "'";
                            }
                        }
                        else
                        {
                            listCountries = "'" + originAirport.CountryCode + "'" + "," + "'" + destinationAirport.CountryCode + "'";
                        }
                    }
                }
                if (onwardResult != null)
                {
                    //Get the result object airline codes
                    for (int p = 0; p < onwardResult.Flights.Length; p++)
                    {
                        for (int k = 0; k < onwardResult.Flights[p].Length; k++)
                        {
                            if (listAirlines.Length > 0)
                            {
                                if (!listAirlines.Contains(onwardResult.Flights[p][k].Airline))
                                {
                                    //At the database side we are checking in operator so we append single quotes for airlines
                                    listAirlines += "," + "'" + onwardResult.Flights[p][k].Airline + "'";
                                }
                            }
                            else
                            {
                                listAirlines = "'" + onwardResult.Flights[p][k].Airline + "'";
                            }
                        }
                    }
                }
                if (returnResult != null)
                {
                    for (int p = 0; p < returnResult.Flights.Length; p++)
                    {
                        for (int k = 0; k < returnResult.Flights[p].Length; k++)
                        {
                            if (listAirlines.Length > 0)
                            {
                                if (!listAirlines.Contains(returnResult.Flights[p][k].Airline))
                                {
                                    //At the database side we are checking in operator so we append single quotes for airlines
                                    listAirlines += "," + "'" + returnResult.Flights[p][k].Airline + "'";
                                }
                            }
                            else
                            {
                                listAirlines = "'" + returnResult.Flights[p][k].Airline + "'";
                            }
                        }
                    }
                }
            }
            //For any search we will return all the fields which matches against the destination counrty code or airline code 
            if (request.Type == SearchType.MultiWay || request.Type == SearchType.OneWay || request.Type == SearchType.Return)
            {
                dtOnwardFields = apisFields.GetFlightPassengerMandateFields(ProductType.Flight, onwardResult.ResultBookingSource, listAirlines, listCountries, flightType, "B2B");
                if (returnResult != null)
                {
                    dtReturnFields = apisFields.GetFlightPassengerMandateFields(ProductType.Flight, returnResult.ResultBookingSource, listAirlines, listCountries, flightType, "B2B");
                }
            }

            if (dtOnwardFields.Rows.Count > 0)
            {

                DataTable dtFields = dtOnwardFields.Clone();
                foreach (DataRow row in dtOnwardFields.Rows)
                {
                    DataRow dataRow = dtFields.NewRow();
                    dataRow.ItemArray = row.ItemArray;
                    dtFields.Rows.Add(dataRow);
                }
                if (dtReturnFields != null && dtReturnFields.Rows.Count > 0)
                {
                    foreach (DataRow row in dtReturnFields.Rows)
                    {
                        DataRow dataRow = dtFields.NewRow();
                        dataRow.ItemArray = row.ItemArray;
                        dtFields.Rows.Add(dataRow);
                    }
                }

                foreach (DataRow field in dtFields.DefaultView.ToTable("Field").Rows)
                {
                    if (field["Field"] != DBNull.Value)
                    {
                        switch (field["Field"].ToString())
                        {
                            case "Gender":
                                GenderRequired = true;
                                break;
                            case "Address":
                                AddressRequired = true;
                                break;
                            case "Country":
                                CountryRequired = true;
                                break;
                            case "DOB":
                                //if (onwardResult.ResultBookingSource == BookingSource.TBOAir && isDomestic)
                                //    DOBRequired = false;
                                //else
                                DOBRequired = true;
                                break;
                            case "PassportNo":  //Either passport number or passprt expiry is provided both of them should be mandatory.
                            case "PassportExpiry":
                                //If TBOAir result is Domestic (i.e both origin & destination are in INDIA) then dont validate Passport fields
                                //i.e passport no is not mandatory
                                //if (isDomestic)
                                //{
                                //    PassportNoRequired = false;
                                //    PassportExpiryRequired = false;
                                //}
                                //else
                                {
                                    PassportNoRequired = true;
                                    PassportExpiryRequired = true;
                                }
                                break;
                            case "Nationality":
                                NationalityRequired = true;
                                break;
                        }
                    }
                }
            }

            //If TBOAir result is Domestic (i.e both origin & destination are in INDIA) then dont validate Passport & DOB fields
            //i.e passport no is not mandatory
            //if ((onwardResult.ResultBookingSource == BookingSource.TBOAir || returnResult.ResultBookingSource == BookingSource.TBOAir || onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.Indigo) && isDomestic)
            //{
            //    PassportNoRequired = false;
            //    PassportExpiryRequired = false;
            //    DOBRequired = false;//DOB non mandatory for India 
            //}
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, 1, "Failed to read mandate fields.Error:" + ex.ToString(), "");
        }
    }

    /// <summary>
    /// If the result booking source is flight inventory only then this method validates the pax additional services
    /// </summary>
    private void FINationalityValidation()
    {
        try
        {
            ItineraryAddServiceMaster serMaster = new ItineraryAddServiceMaster();
            serMaster.GetActiveServiceListByServiceType(Settings.LoginInfo.AgentId);
            if (!string.IsNullOrEmpty(serMaster.Service_nationality) && serMaster.Service_nationality.Length > 0)
            {
                hdnAddNationlities.Value = serMaster.Service_nationality;
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, 1, "Failed to read Additional Services .Error:" + ex.ToString(), "");
        }
    }

    public void AssignCountryAndNationality(DropDownList ddlCountry, DropDownList ddlNationality)
    {

        try
        {
            if (ddlCountry != null && ddlNationality != null && agency != null && (agency.Country) > 0)
            {
                ddlCountry.ClearSelection();
                ddlNationality.ClearSelection();
                string countryCode = Country.GetCountryCodeByCountryId(Convert.ToInt32(agency.Country));
                if (!string.IsNullOrEmpty(countryCode))
                {
                    if (ddlCountry.Items.FindByValue(countryCode) != null)
                    {
                        ddlCountry.Items.FindByValue(countryCode).Selected = true;
                    }
                    if (ddlCountry.SelectedItem.Value == "IN")
                    {
                        if (ddlNationality.Items.FindByText("Indian") != null)
                        {
                            ddlNationality.Items.FindByText("Indian").Selected = true;
                        }

                    }
                }
                else
                {
                    if (ddlCountry != null)
                    {
                        ddlCountry.SelectedIndex = -1;
                    }
                    if (ddlNationality != null)
                    {
                        ddlNationality.SelectedIndex = -1;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "B2B Flight Pax Details : Failed to get Countries & Nationalities. Reason: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    void LoadSGMealInfo(DropDownList ddlMeal, Label lblMeal, DataTable dtMealInfo)
    {
        if (dtMealInfo != null && dtMealInfo.Rows.Count > 0)
        {
            dtMealInfo = dtMealInfo.DefaultView.ToTable(true, new string[0]);
            DataRow[] mealData = new DataRow[0];
            mealData = dtMealInfo.Select("Group=0 AND (Code='VGML' OR Code='NVML' OR Code='NVSW' OR Code='VGSW' OR Code='NCC1' OR Code='NCC2' OR Code='NCC6' OR Code='NCC4' OR Code='NCC5'  OR Code='VCC5' OR Code='VCC2' OR Code='VCC6' OR Code='JNML' OR Code='JNSW' OR Code='DNVL' OR Code='DBML' OR Code='GFNV' OR Code='GFVG' OR Code='GFCM' OR Code='NVRT' OR Code='FPML' OR Code='LCVS' OR Code='LCNS' OR Code='VMAX' OR Code='NMAX' OR Code='GOBX')");

            lblMeal.Visible = true;
            ddlMeal.Visible = true;

            if (mealData.Length > 0)
            {
                ListItem item = new ListItem("Select Meal", "0");
                ddlMeal.Items.Add(item);
                foreach (DataRow row in mealData)
                {
                    ListItem mealitem = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                    mealitem.Attributes.Add("Code", row["Code"].ToString().Trim());
                    ddlMeal.Items.Add(mealitem);
                }
            }
            if (mealData.Length <= 0)
            {
                ListItem item = new ListItem("No Meal", "0");
                ddlMeal.Items.Add(item);

            }
            if (ddlMeal.Visible && ddlMeal.Items.Count == 1)
            {
                ddlMeal.Visible = false;
                lblMeal.Visible = false;
            }
        }

    }

    void Load6EMealInfo(DropDownList ddlOnwardMeal, DropDownList ddlInwardMeal, Label lblOnMeal, Label lblInMeal, DataTable dtMealInfo, SearchType type)
    {
        try
        {
            if (dtMealInfo != null && dtMealInfo.Rows.Count > 0)
            {


                dtMealInfo = dtMealInfo.DefaultView.ToTable(true, new string[0]);
                DataRow[] mealData = new DataRow[0];
                DataRow[] returnMeal = new DataRow[0];
                if (type == SearchType.Return)
                {
                    ddlInwardMeal.Items.Clear();
                    returnMeal = dtMealInfo.Select("Group=0 AND (Code='VGML' OR Code='NVML' OR Code='TCSW' OR Code='PTSW'  OR Code='CJSW' OR Code='CTSW' OR Code='POHA' OR Code='UPMA' OR Code='SACH' OR Code='VBIR' OR Code='DACH' OR Code='CHFR' OR Code='CHCH' OR Code='MASP' OR Code='SMAL')");
                    if (returnMeal.Length > 0)
                    {
                        ddlInwardMeal.Visible = true;
                        lblInMeal.Visible = true;
                    }
                    else
                    {
                        ddlInwardMeal.Visible = false;
                        lblInMeal.Visible = false;
                    }

                }
                else
                {
                    ddlOnwardMeal.Items.Clear();
                    mealData = dtMealInfo.Select("Group=0 AND (Code='VGML' OR Code='NVML' OR Code='TCSW' OR Code='PTSW'  OR Code='CJSW' OR Code='CTSW' OR Code='POHA' OR Code='UPMA' OR Code='SACH' OR Code='VBIR' OR Code='DACH' OR Code='CHFR' OR Code='CHCH' OR Code='MASP' OR Code='SMAL')");
                    if (mealData.Length > 0)
                    {
                        lblOnMeal.Visible = true;
                        ddlOnwardMeal.Visible = true;
                    }
                    else
                    {
                        lblOnMeal.Visible = false;
                        ddlOnwardMeal.Visible = false;
                    }
                }

                if (mealData.Length > 0)
                {
                    ListItem item = new ListItem("Select Meal", "0");
                    ddlOnwardMeal.Items.Add(item);
                    foreach (DataRow row in mealData)
                    {
                        ListItem mealitem = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                        mealitem.Attributes.Add("Code", row["Code"].ToString().Trim());
                        ddlOnwardMeal.Items.Add(mealitem);
                    }
                }
                if (returnMeal.Length > 0 && type == SearchType.Return)
                {
                    ListItem item = new ListItem("Select Meal", "0");
                    ddlInwardMeal.Items.Add(item);

                    foreach (DataRow row in returnMeal)
                    {
                        ListItem mealitem = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                        mealitem.Attributes.Add("Code", row["Code"].ToString().Trim());
                        ddlInwardMeal.Items.Add(mealitem);
                    }
                }
                if (mealData.Length <= 0 && type == SearchType.OneWay)
                {
                    ListItem item = new ListItem("No Meal", "0");
                    ddlOnwardMeal.Items.Add(item);

                }
                if (returnMeal.Length <= 0 && type == SearchType.Return)
                {
                    ListItem item = new ListItem("No Meal", "0");
                    ddlInwardMeal.Items.Add(item);
                }
                if (ddlOnwardMeal.Visible && ddlOnwardMeal.Items.Count == 1)
                {
                    ddlOnwardMeal.Visible = false;
                    lblOnMeal.Visible = false;
                }
                if (ddlInwardMeal.Visible && ddlInwardMeal.Items.Count == 1)
                {
                    ddlInwardMeal.Visible = false;
                    lblInMeal.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }

    }

    void SaveInSession(string key, object data)
    {
        if (Session["PaxPageValues"] != null)
        {
            SessionValues = Session["PaxPageValues"] as Dictionary<string, object>;
            if (!SessionValues.ContainsKey(key))
            {
                SessionValues.Add(key, data);
            }
            else
            {
                SessionValues[key] = data;
            }
            Session["PaxPageValues"] = SessionValues;
        }
        else
        {
            if (!SessionValues.ContainsKey(key))
            {
                SessionValues.Add(key, data);
            }
            else
            {
                SessionValues[key] = data;
            }

            Session["PaxPageValues"] = SessionValues;
        }
    }

    void RemoveFromSession(string key)
    {
        if (Session["PaxPageValues"] != null)
        {
            SessionValues = Session["PaxPageValues"] as Dictionary<string, object>;
            if (SessionValues.ContainsKey(key))
            {
                SessionValues[key] = null;
            }
            Session["PaxPageValues"] = SessionValues;
        }
    }

    protected Dictionary<string, object> GetSession()
    {
        if (Session["PaxPageValues"] != null)
        {
            SessionValues = Session["PaxPageValues"] as Dictionary<string, object>;
        }

        return SessionValues;
    }

    void InitSession()
    {
        SaveInSession(ONWARD_BAGGAGE, null);
        SaveInSession(ONWARD_OTHERCHARGES, null);
        SaveInSession(ONWARD_REPRICEDRESULT, null);
        SaveInSession(ONWARD_PRICE_CHANGED_TBO, null);
        SaveInSession(ONWARD_TBOFARE, null);
        SaveInSession(ONWARD_VALUES_CHANGED, null);

        SaveInSession(RETURN_BAGGAGE, null);
        SaveInSession(RETURN_OTHERCHARGES, null);
        SaveInSession(RETURN_REPRICEDRESULT, null);
        SaveInSession(RETURN_PRICE_CHANGED_TBO, null);
        SaveInSession(RETURN_TBOFARE, null);
        SaveInSession(RETURN_VALUES_CHANGED, null);
        SaveInSession(ONWARD_FLIGHT_ITINERARY, null);
        SaveInSession(RETURN_FLIGHT_ITINERARY, null);
        Session["PaxPageValues"] = SessionValues;
    }

    void DoReprice()
    {

        try
        {
            int decimalPoint = agency.DecimalValue; //Assigning agenct decimal point to avoid using agency.DecimalValue in multiple places.
            if (hdfOnwardFareAction.Value == "Get" && hdfReturnFareAction.Value == "Get")
            {
                clsMSE.SettingsLoginInfo = Settings.LoginInfo;
                clsMSE.SessionId = sessionId;
                if (onwardResult != null)
                {
                    //If the onward result is of not Special Round Trip Fare
                    if ((onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp) && !onwardResult.IsSpecialRoundTrip)
                    {
                        originalBaseFare = Math.Round(onwardResult.TotalFare, decimalPoint); //before reprice assigning total fare .
                        IndigoRepricing(ref onwardResult, request);//Calculates the new fare from PriceItinerary.
                        repricedBaseFare = Math.Round(onwardResult.TotalFare,decimalPoint); //after reprice assigning new total fare.
                    }
                    if (onwardResult.ResultBookingSource == BookingSource.TBOAir && onwardFares == null)
                    {
                        sessionId = Session["sessionId"].ToString();

                        if (SessionValues[ONWARD_OTHERCHARGES] == null)
                        {
                            hdfOnwardFareAction.Value = (onwardResult.Price.OtherCharges + onwardResult.Price.AdditionalTxnFee + onwardResult.Price.SServiceFee + onwardResult.Price.TransactionFee).ToString();
                            SaveInSession(ONWARD_OTHERCHARGES, hdfOnwardFareAction.Value);
                        }
                        TBOAir.AirV10 tbo = new TBOAir.AirV10();

                        SourceDetails agentDetails = new SourceDetails();
                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["TA"];
                            tbo.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            tbo.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        }
                        else
                        {
                            agentDetails = Settings.LoginInfo.AgentSourceCredentials["TA"];
                            tbo.AgentBaseCurrency = Settings.LoginInfo.Currency;
                            tbo.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        }
                        SearchResult res = onwardResult;

                        tbo.LoginName = agentDetails.UserID;
                        tbo.Password = agentDetails.Password;
                        tbo.SessionId = sessionId;
                        tbo.AppUserId = (int)Settings.LoginInfo.UserID;
                        tbo.ShowPubFaresForCorp = (!string.IsNullOrEmpty(agentDetails.HAP) && agentDetails.HAP.ToLower() == "corp" ? true : false);

                        decimal markUp = 0, baseFares = 0, discount = 0, tax = 0;
                        for (int i = 0; i < onwardResult.FareBreakdown.Length; i++)
                        {
                            markUp += (onwardResult.FareBreakdown[i].AgentMarkup); // *result.FareBreakdown[i].PassengerCount;
                            discount += onwardResult.FareBreakdown[i].AgentDiscount; // *result.FareBreakdown[i].PassengerCount;
                            baseFares += ((decimal)onwardResult.FareBreakdown[i].BaseFare + onwardResult.FareBreakdown[i].HandlingFee);
                            tax += (onwardResult.FareBreakdown[i].Tax);
                        }
                        tax += (markUp + (onwardResult.Price.OtherCharges) + onwardResult.Price.AdditionalTxnFee + onwardResult.Price.SServiceFee + onwardResult.Price.TransactionFee - discount);

                        originalBaseFare = Math.Ceiling((double)(baseFares + tax));

                        onwardFares = tbo.GetFareQuote(ref res, ref errorMessage);

                        decimal totalFare = 0m;
                        foreach (Fare fare in onwardFares)
                        {
                            totalFare += (decimal)fare.TotalFare;
                        }

                        PriceAccounts price = onwardResult.Price;
                        decimal taxValue = 0m;
                        CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
                        mse.CalculateInputVATForFlight(request.Segments[0].Origin, request.Segments[0].Destination, onwardResult.ResultBookingSource, ref price, totalFare, ref taxValue);

                        res.TotalFare += (double)taxValue;
                        res.Tax += (double)taxValue;

                        int totalpax = request.AdultCount + request.ChildCount + request.InfantCount;

                        foreach (Fare fare in onwardFares)
                        {
                            fare.TotalFare += Math.Round((double)(taxValue / totalpax) * fare.PassengerCount, agency.DecimalValue);
                        }

                        hdfOnwardFareAction.Value = "Quoted";

                        if (onwardFares.Length > 0)
                        {
                            SaveInSession(ONWARD_TBOFARE, onwardFares);

                            double baseFare = 0, newBaseFare = 0; decimal newTax = 0, handlingFee = 0, markup = 0;
                            tax = 0; discount = 0;

                            if (onwardFares != null && onwardFares.Length > 0 && errorMessage.Length == 0)
                            {
                                int count = 0;
                                foreach (Fare objFare in onwardFares)
                                {
                                    count += objFare.PassengerCount;
                                }

                                for (int i = 0; i < onwardFares.Length; i++)
                                {
                                    if (onwardResult.FareBreakdown.Length > i)
                                    {
                                        //if (onwardResult.FareBreakdown[i].BaseFare != fares[i].BaseFare || onwardResult.FareBreakdown[i].Tax != fares[i].Tax)
                                        {
                                            baseFare += onwardResult.FareBreakdown[i].BaseFare + (double)onwardResult.FareBreakdown[i].HandlingFee;
                                            newBaseFare += onwardFares[i].BaseFare;
                                            tax += onwardResult.FareBreakdown[i].Tax + onwardResult.FareBreakdown[i].AgentMarkup - onwardResult.FareBreakdown[i].AgentDiscount;
                                            newTax += onwardFares[i].Tax;
                                            CT.BookingEngine.Fare fare = onwardFares[i];
                                            if (i == onwardResult.FareBreakdown.Length - 1)
                                            {
                                                tax += Convert.ToDecimal(hdfOnwardOtherCharges.Value);
                                                newTax += ((onwardResult.Price.AdditionalTxnFee + onwardResult.Price.OtherCharges + onwardResult.Price.SServiceFee + onwardResult.Price.TransactionFee));

                                                if (onwardResult.Price.MarkupType == "P")
                                                {
                                                    if (agency.AgentAirMarkupType == "TF")
                                                    {
                                                        markup += (((decimal)((decimal)newBaseFare + newTax)) * onwardResult.Price.MarkupValue / 100);
                                                    }
                                                    else if (agency.AgentAirMarkupType == "BF")
                                                    {
                                                        markup += (((decimal)newBaseFare * onwardResult.Price.MarkupValue / 100));
                                                    }
                                                    else if (agency.AgentAirMarkupType == "TX")
                                                    {
                                                        markup += (((decimal)newTax) * onwardResult.Price.MarkupValue / 100);
                                                    }
                                                }

                                                if (onwardResult.Price.DiscountType == "P")
                                                {
                                                    if (agency.AgentAirMarkupType == "BF")
                                                    {
                                                        discount += (decimal)newBaseFare * (onwardResult.Price.DiscountValue / 100);
                                                    }
                                                    else if (agency.AgentAirMarkupType == "TX")
                                                    {
                                                        discount += (decimal)newTax * (onwardResult.Price.DiscountValue / 100);
                                                    }
                                                    else if (agency.AgentAirMarkupType == "TF")
                                                    {
                                                        discount += ((decimal)newBaseFare + newTax) * (onwardResult.Price.DiscountValue / 100);
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                                if (onwardResult.Price.MarkupType == "F")
                                {
                                    markup += (onwardResult.Price.MarkupValue * count);
                                }
                                if (onwardResult.Price.DiscountType == "F")
                                {
                                    discount += (onwardResult.Price.DiscountValue * count);
                                }
                                if (onwardResult.LoginCountryCode == "IN")
                                {
                                    if (onwardResult.Price.HandlingFeeType == "P")
                                    {
                                        handlingFee = ((decimal)newBaseFare + newTax + markup) * onwardResult.Price.HandlingFeeValue / 100;
                                        newBaseFare += (double)handlingFee;
                                    }
                                    else
                                    {
                                        newBaseFare += (double)onwardResult.Price.HandlingFeeValue * count;
                                    }
                                }
                                newBaseFare = Math.Round(newBaseFare, agency.DecimalValue);
                                baseFare = Math.Round(baseFare, agency.DecimalValue);
                                newTax = Math.Round(newTax + markup - discount, agency.DecimalValue);
                                tax = Math.Round(tax, agency.DecimalValue);

                                repricedBaseFare = Math.Ceiling(newBaseFare + (double)newTax);
                            }
                        }
                        else
                        {
                            //Repriced fares will also be same if there is no price change
                            repricedBaseFare = Math.Ceiling((double)(baseFares + tax));
                        }
                    }
                    if (request.InfantCount <= 0)
                    {
                        //Retrieve the new fares by calling PriceItinerary
                        //If the onward result is of not Special Round Trip Fare
                        if ((onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp) && !onwardResult.IsSpecialRoundTrip)
                        {
                            double OldTotalSG = onwardResult.TotalFare;//before repricing grand total;
                            originalBaseFare = Math.Round(OldTotalSG,decimalPoint);
                            if (onwardResult.ResultBookingSource == BookingSource.SpiceJet)
                            {
                                SGRepricing(ref onwardResult, request);//Calculates the new fare from PriceItinerary.
                            }
                            else if (onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp)
                            {
                                G8Repricing(ref onwardResult, request);//Calculates the new fare from PriceItinerary.
                            }
                            repricedBaseFare = Math.Round(onwardResult.TotalFare,decimalPoint);//after repricing grand total;
                        }
                    }
                    if (request.InfantCount > 0 && (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp) && !onwardResult.IsSpecialRoundTrip)
                    {
                        //The price will not change for spicejet if there is any infant in the request.
                        //If the onward result is of not Special Round Trip Fare
                        originalBaseFare = onwardResult.TotalFare;
                        repricedBaseFare = onwardResult.TotalFare;
                    }

                    if (onwardResult.ResultBookingSource == BookingSource.PKFares)
                    {
                        sessionId = Session["sessionId"].ToString();
                        PKFaresRepricing(request, onwardResult.ResultId, sessionId, SearchType.OneWay);

                        originalBaseFare = onwardResult.BaseFare;
                        originalTax = onwardResult.Tax;

                        repricedBaseFare = G9OnwardResult.BaseFare;
                        repricedTax = G9OnwardResult.Tax;
                    }

                    if (onwardResult.ResultBookingSource == BookingSource.UAPI)
                    {
                        RepriceUAPI(onwardResult, SearchType.OneWay);

                        originalBaseFare = repricedBaseFare = onwardResult.TotalFare;

                        if (onwardFares != null && onwardFares.Length > 0)
                        {
                            decimal Total = (decimal)onwardResult.TotalFare;
                            decimal newTotal = 0;
                            decimal newTax = 0;
                            decimal newBasePrice = 0;

                            //Deduct the original fares from the repricedBaseFare and Tax
                            //repricedBaseFare -= onwardResult.BaseFare;
                            //repricedTax -= onwardResult.Tax;

                            CT.BookingEngine.SearchResult res = new CT.BookingEngine.SearchResult();
                            res.Currency = onwardResult.Currency;
                            res.ResultBookingSource = onwardResult.ResultBookingSource;
                            res.ResultId = onwardResult.ResultId;
                            res.Flights = onwardResult.Flights;
                            res.Price = onwardResult.Price;
                            res.FareBreakdown = onwardFares;
                            res.LoginCountryCode = onwardResult.LoginCountryCode;
                            //Calculate Markup for the new price
                            decimal markup = 0, discount = 0;
                            for (int count = 0; count < onwardFares.Length; count++)
                            {
                                CT.BookingEngine.PriceAccounts tempPrice = CT.AccountingEngine.AccountingEngine.GetPrice(res, count, (int)agency.ID, 0, (int)CT.BookingEngine.ProductType.Flight, "B2B");
                                newBasePrice += (decimal)(res.FareBreakdown[count].BaseFare) + res.FareBreakdown[count].HandlingFee;
                                newTax += (res.FareBreakdown[count].Tax);
                                newTotal = newBasePrice + newTax;
                                markup += (tempPrice.Markup) * onwardResult.FareBreakdown[count].PassengerCount;
                                discount += (tempPrice.Discount) * onwardResult.FareBreakdown[count].PassengerCount;
                            }
                            newTotal += Math.Round(markup, agency.DecimalValue);//Add markup to the total fare
                            newTotal -= Math.Round(discount, agency.DecimalValue);
                            //If any of the values are not same, show the popup with price difference
                            if (Math.Round(Total, agency.DecimalValue) != Math.Round(newTotal, agency.DecimalValue))
                            {
                                repricedBaseFare = (double)Math.Round(newTotal, agency.DecimalValue);
                            }
                        }
                    }

                    if (onwardResult.ResultBookingSource == BookingSource.AirArabia)
                    {
                        resultId = Convert.ToInt32(pageParams[0]);
                        G9OnwardResult = clsMSE.RepriceAirArabia(resultId, request, "B2B");

                        originalBaseFare = onwardResult.TotalFare;

                        repricedBaseFare = G9OnwardResult.TotalFare;

                        onwardResult.ResultKey = G9OnwardResult.ResultKey;
                    }
                }
                if (returnResult != null)
                {

                    if (returnResult.ResultBookingSource == BookingSource.TBOAir && returnFares == null)
                    {
                        sessionId = Session["sessionId"].ToString();

                        if (SessionValues[RETURN_OTHERCHARGES] == null)
                        {
                            hdfReturnFareAction.Value = (returnResult.Price.OtherCharges + returnResult.Price.AdditionalTxnFee + returnResult.Price.SServiceFee + returnResult.Price.TransactionFee).ToString();
                            SaveInSession(RETURN_OTHERCHARGES, hdfReturnFareAction.Value);
                        }
                        TBOAir.AirV10 tbo = new TBOAir.AirV10();

                        SourceDetails agentDetails = new SourceDetails();
                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["TA"];
                            tbo.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            tbo.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        }
                        else
                        {
                            agentDetails = Settings.LoginInfo.AgentSourceCredentials["TA"];
                            tbo.AgentBaseCurrency = Settings.LoginInfo.Currency;
                            tbo.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        }
                        SearchResult res = returnResult;

                        tbo.LoginName = agentDetails.UserID;
                        tbo.Password = agentDetails.Password;
                        tbo.SessionId = sessionId;
                        tbo.AppUserId = (int)Settings.LoginInfo.UserID;
                        tbo.ShowPubFaresForCorp = (!string.IsNullOrEmpty(agentDetails.HAP) && agentDetails.HAP.ToLower() == "corp" ? true : false);

                        decimal markUp = 0, baseFares = 0, discount = 0, tax = 0;
                        for (int i = 0; i < returnResult.FareBreakdown.Length; i++)
                        {
                            markUp += (returnResult.FareBreakdown[i].AgentMarkup); // *result.FareBreakdown[i].PassengerCount;
                            discount += returnResult.FareBreakdown[i].AgentDiscount; // *result.FareBreakdown[i].PassengerCount;
                            baseFares += ((decimal)returnResult.FareBreakdown[i].BaseFare + returnResult.FareBreakdown[i].HandlingFee);
                            tax += (returnResult.FareBreakdown[i].Tax);
                        }
                        tax += (markUp + (returnResult.Price.OtherCharges) + returnResult.Price.AdditionalTxnFee + returnResult.Price.SServiceFee + returnResult.Price.TransactionFee - discount);

                        originalBaseFare += Math.Ceiling((double)(baseFares + tax));

                        returnFares = tbo.GetFareQuote(ref res, ref errorMessage);

                        int totalpax = request.AdultCount + request.ChildCount + request.InfantCount;
                        decimal totalFare = 0m;
                        foreach (Fare fare in returnFares)
                        {
                            totalFare += (decimal)fare.TotalFare;
                        }

                        PriceAccounts price = returnResult.Price;
                        decimal taxValue = 0m;
                        CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
                        mse.CalculateInputVATForFlight(request.Segments[0].Origin, request.Segments[0].Destination, returnResult.ResultBookingSource, ref price, totalFare, ref taxValue);

                        res.TotalFare += (double)taxValue;
                        res.Tax += (double)taxValue;

                        foreach (Fare fare in returnFares)
                        {
                            fare.TotalFare += Math.Round((double)(taxValue / totalpax) * fare.PassengerCount, agency.DecimalValue);
                        }

                        hdfReturnFareAction.Value = "Quoted";

                        if (returnFares.Length > 0)
                        {
                            SaveInSession(RETURN_TBOFARE, returnFares);

                            double baseFare = 0, newBaseFare = 0;
                            decimal newTax = 0, handlingFee = 0, markup = 0; discount = 0; tax = 0;

                            if (returnFares != null && returnFares.Length > 0 && errorMessage.Length == 0)
                            {
                                int count = 0;
                                foreach (CT.BookingEngine.Fare objFare in returnFares)
                                {
                                    count += objFare.PassengerCount;
                                }

                                for (int i = 0; i < returnFares.Length; i++)
                                {
                                    if (returnResult.FareBreakdown.Length > i)
                                    {
                                        baseFare += returnResult.FareBreakdown[i].BaseFare + (double)returnResult.FareBreakdown[i].HandlingFee;
                                        newBaseFare += returnFares[i].BaseFare;
                                        tax += returnResult.FareBreakdown[i].Tax + returnResult.FareBreakdown[i].AgentMarkup - returnResult.FareBreakdown[i].AgentDiscount;
                                        newTax += returnFares[i].Tax;
                                        CT.BookingEngine.Fare fare = returnFares[i];
                                        if (i == returnResult.FareBreakdown.Length - 1)
                                        {
                                            tax += Convert.ToDecimal(hdfOnwardOtherCharges.Value);
                                            newTax += ((returnResult.Price.AdditionalTxnFee + returnResult.Price.OtherCharges + returnResult.Price.SServiceFee + returnResult.Price.TransactionFee));

                                            if (returnResult.Price.MarkupType == "P")
                                            {
                                                if (agency.AgentAirMarkupType == "TF")
                                                {
                                                    markup += (((decimal)((decimal)newBaseFare + newTax)) * returnResult.Price.MarkupValue / 100);
                                                }
                                                else if (agency.AgentAirMarkupType == "BF")
                                                {
                                                    markup += (((decimal)newBaseFare * returnResult.Price.MarkupValue / 100));
                                                }
                                                else if (agency.AgentAirMarkupType == "TX")
                                                {
                                                    markup += (((decimal)newTax) * returnResult.Price.MarkupValue / 100);
                                                }
                                            }

                                            if (returnResult.Price.DiscountType == "P")
                                            {
                                                if (agency.AgentAirMarkupType == "BF")
                                                {
                                                    discount += (decimal)newBaseFare * (returnResult.Price.DiscountValue / 100);
                                                }
                                                else if (agency.AgentAirMarkupType == "TX")
                                                {
                                                    discount += (decimal)newTax * (returnResult.Price.DiscountValue / 100);
                                                }
                                                else if (agency.AgentAirMarkupType == "TF")
                                                {
                                                    discount += ((decimal)newBaseFare + newTax) * (returnResult.Price.DiscountValue / 100);
                                                }
                                            }

                                        }
                                    }
                                }
                                if (returnResult.Price.MarkupType == "F")
                                {
                                    markup += (returnResult.Price.MarkupValue * count);
                                }
                                if (returnResult.Price.DiscountType == "F")
                                {
                                    discount += (returnResult.Price.DiscountValue * count);
                                }
                                if (returnResult.LoginCountryCode == "IN")
                                {
                                    if (returnResult.Price.HandlingFeeType == "P")
                                    {
                                        handlingFee = ((decimal)newBaseFare + newTax + markup) * returnResult.Price.HandlingFeeValue / 100;
                                        newBaseFare += (double)handlingFee;
                                    }
                                    else
                                    {
                                        newBaseFare += (double)returnResult.Price.HandlingFeeValue * count;
                                    }
                                }
                                newBaseFare = Math.Round(newBaseFare, agency.DecimalValue);
                                baseFare = Math.Round(baseFare, agency.DecimalValue);
                                newTax = Math.Round(newTax + markup - discount, agency.DecimalValue);
                                tax = Math.Round(tax, agency.DecimalValue);

                                repricedBaseFare += Math.Ceiling(newBaseFare + (double)newTax);
                            }
                        }
                        else
                        {
                            //Repriced fares will also be same if there is no price change
                            repricedBaseFare += Math.Ceiling((double)(baseFares + tax));
                        }
                    }

                    if (returnResult.ResultBookingSource == BookingSource.PKFares)
                    {
                        sessionId = Session["sessionId"].ToString();
                        PKFaresRepricing(request, returnResult.ResultId, sessionId, SearchType.Return);

                        originalBaseFare += returnResult.BaseFare;
                        originalTax += returnResult.Tax;

                        repricedBaseFare += G9ReturnResult.BaseFare;
                        repricedTax += G9ReturnResult.Tax;

                    }

                    if (returnResult.ResultBookingSource == BookingSource.UAPI)
                    {
                        RepriceUAPI(returnResult, SearchType.Return);

                        originalBaseFare += returnResult.TotalFare;

                        if (returnFares != null && returnFares.Length > 0)
                        {
                            decimal Total = (decimal)returnResult.TotalFare;

                            decimal newTotal = 0;
                            decimal newTax = 0;
                            decimal newBasePrice = 0;

                            //Deduct the original fares from the repricedBaseFare and Tax
                            //repricedBaseFare -= returnResult.BaseFare;
                            //repricedTax -= returnResult.Tax;

                            CT.BookingEngine.SearchResult res = new CT.BookingEngine.SearchResult();
                            res.Currency = returnResult.Currency;
                            res.ResultBookingSource = returnResult.ResultBookingSource;
                            res.ResultId = returnResult.ResultId;
                            res.Flights = returnResult.Flights;
                            res.Price = returnResult.Price;
                            res.FareBreakdown = returnFares;
                            res.LoginCountryCode = returnResult.LoginCountryCode;
                            //Calculate Markup for the new price
                            decimal markup = 0, discount = 0;
                            for (int count = 0; count < returnFares.Length; count++)
                            {
                                CT.BookingEngine.PriceAccounts tempPrice = CT.AccountingEngine.AccountingEngine.GetPrice(res, count, (int)agency.ID, 0, (int)CT.BookingEngine.ProductType.Flight, "B2B");
                                newBasePrice += (decimal)(res.FareBreakdown[count].BaseFare) + res.FareBreakdown[count].HandlingFee;
                                newTax += (res.FareBreakdown[count].Tax);
                                newTotal = newBasePrice + newTax;
                                markup += (tempPrice.Markup) * returnResult.FareBreakdown[count].PassengerCount;
                                discount += (tempPrice.Discount) * returnResult.FareBreakdown[count].PassengerCount;
                            }
                            newTotal += Math.Round(markup, agency.DecimalValue);//Add markup to the total fare
                            newTotal -= Math.Round(discount, agency.DecimalValue);
                            //If any of the values are not same, show the popup with price difference
                            if (Math.Round(Total, agency.DecimalValue) != Math.Round(newTotal, agency.DecimalValue))
                            {
                                repricedBaseFare += (double)Math.Round(newTotal, agency.DecimalValue);
                            }
                            else
                            {
                                repricedBaseFare += returnResult.TotalFare;
                            }
                        }
                        else
                        {
                            repricedBaseFare += returnResult.TotalFare;
                        }
                    }

                    if (returnResult.ResultBookingSource == BookingSource.AirArabia)
                    {
                        resultId = Convert.ToInt32(pageParams[1]);
                        G9ReturnResult = clsMSE.RepriceAirArabia(resultId, request, "B2B");

                        originalBaseFare += returnResult.TotalFare;

                        repricedBaseFare += G9ReturnResult.TotalFare;

                        returnResult.ResultKey = G9ReturnResult.ResultKey;
                    }

                    if (request.InfantCount <= 0)
                    {
                        //Retrieve the new fares by calling PriceItinerary
                        //If the return result is of not Special Round Trip Fare
                        if ((returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp) && !returnResult.IsSpecialRoundTrip)
                        {
                            double OldTotalSG = returnResult.TotalFare;//before repricing grand total;
                            originalBaseFare += Math.Round(OldTotalSG,decimalPoint); //Rounding to agent decimal value
                            if (returnResult.ResultBookingSource == BookingSource.SpiceJet)
                            {
                                SGRepricing(ref returnResult, request);//Calculates the new fare from PriceItinerary.
                            }
                            else if (returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp)
                            {
                                G8Repricing(ref returnResult, request);
                            }
                            repricedBaseFare += Math.Round(returnResult.TotalFare,decimalPoint);//after repricing grand total;
                        }
                    }
                    if (request.InfantCount > 0 && (returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp) && !returnResult.IsSpecialRoundTrip)
                    {
                        //The price will not change for spicejet if there is any infant in the request.
                        //If the return result is of not Special Round Trip Fare
                        originalBaseFare += returnResult.TotalFare;
                        repricedBaseFare += returnResult.TotalFare;
                    }

                    if ((returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.IndigoCorp) && !returnResult.IsSpecialRoundTrip)
                    {
                        //Intially save the old fares for informing to the user if there is any price chnage.
                        //If the return result is of not Special Round Trip Fare
                        originalBaseFare += Math.Round(returnResult.TotalFare,decimalPoint); //before reprice assigning total fare 
                        IndigoRepricing(ref returnResult, request);//Calculates the new fare from PriceItinerary.
                        repricedBaseFare += Math.Round(returnResult.TotalFare,decimalPoint); //after reprice assigning new TotalFare
                    }
                }

                if (onwardResult != null && returnResult != null)
                {
                    //Repricing For Special Round Trip Fares for SG and 6E
                    //For Indigo and SpiceJet
                    //If both are SPECIAL ROUNDTRIP FARE then we need to compute the price from single price itinerary request.

                    if (onwardResult.IsSpecialRoundTrip && returnResult.IsSpecialRoundTrip && (onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp) && (returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp))
                    {

                        //Before re-price 
                        originalBaseFare = onwardResult.TotalFare;
                        originalBaseFare += returnResult.TotalFare;

                        //Calculates the new fare from PriceItinerary for both onward and return results.
                        SpecialRoundTripRepricing(ref onwardResult, ref returnResult, request);

                        //After re-price 
                        repricedBaseFare = onwardResult.TotalFare;
                        repricedBaseFare += returnResult.TotalFare;
                    }
                    if (onwardResult.IsSpecialRoundTrip && returnResult.IsSpecialRoundTrip && (onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.IndigoCorp) && (returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.IndigoCorp))
                    {

                        //Before re-price 
                        originalBaseFare = onwardResult.TotalFare;
                        originalBaseFare += returnResult.TotalFare;

                        //Calculates the new fare from PriceItinerary for both onward and return results.
                        SpecialRoundTripRepricing(ref onwardResult, ref returnResult, request);

                        //After re-price 
                        repricedBaseFare = onwardResult.TotalFare;
                        repricedBaseFare += returnResult.TotalFare;
                    }
                }

            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    /// <summary>
    /// This method calculates the Spice Jet Repricing.
    /// </summary>
    /// <param name="resultObj"></param>
    /// <param name="request"></param>
    private void SGRepricing(ref SearchResult resultObj, SearchRequest request)
    {
        try
        {
            //Here we calculate the original price from the ItineraryPrice response.
            //So,we will clear all the previously calculated price component values here.
            resultObj.Price = null;
            resultObj.BaseFare = 0;
            resultObj.Tax = 0;
            resultObj.TotalFare = 0;
            resultObj.FareBreakdown = null;
            int fareBreakDownCount = 0;
            if (request.AdultCount > 0)
            {
                fareBreakDownCount = 1;
            }
            if (request.ChildCount > 0)
            {
                fareBreakDownCount++;
            }
            if (request.InfantCount > 0)
            {
                fareBreakDownCount++;
            }
            resultObj.FareBreakdown = new Fare[fareBreakDownCount];
            GetItineraryPriceForLCCFlights(ref resultObj, request); //Gets the Latest Itenarary Price
            CalculatePriceComponent(ref resultObj, request);//MarkuP Calculation for the result.

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //Go Air Lead Pax and Additional Pax Baggage Information
    private void LoadG8BaggageInfo(DropDownList ddlBag, Label lblBag, int type)
    {
        SearchResult sgResult = null;
        if (type == 0) //ONWARD
        {
            sgResult = SessionValues[ONWARD_RESULT] as SearchResult;
        }
        else if (type == 1)//Return
        {
            sgResult = SessionValues[RETURN_RESULT] as SearchResult;
        }
        request = Session["FlightRequest"] as SearchRequest;

        CT.BookingEngine.GDS.GoAirAPI goAirObj = new CT.BookingEngine.GDS.GoAirAPI();
        if(sgResult.ResultBookingSource==BookingSource.GoAir)
        {
            goAirObj.BookingSourceFlag = "G8";
        }
        else
        {
            goAirObj.BookingSourceFlag = "G8CORP";
        }
        if (Settings.LoginInfo.IsOnBehalfOfAgent)
        {
            goAirObj.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials[goAirObj.BookingSourceFlag].UserID;
            goAirObj.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials[goAirObj.BookingSourceFlag].Password;
            goAirObj.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials[goAirObj.BookingSourceFlag].HAP;
            goAirObj.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
            goAirObj.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
            goAirObj.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
            goAirObj.PromoCode = Settings.LoginInfo.OnBehalfAgentSourceCredentials[goAirObj.BookingSourceFlag].PCC;//Added For corporate booking purpose
        }
        else
        {
            goAirObj.LoginName = Settings.LoginInfo.AgentSourceCredentials[goAirObj.BookingSourceFlag].UserID;
            goAirObj.Password = Settings.LoginInfo.AgentSourceCredentials[goAirObj.BookingSourceFlag].Password;
            goAirObj.AgentDomain = Settings.LoginInfo.AgentSourceCredentials[goAirObj.BookingSourceFlag].HAP;
            goAirObj.AgentBaseCurrency = Settings.LoginInfo.Currency;
            goAirObj.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
            goAirObj.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
            goAirObj.PromoCode = Settings.LoginInfo.AgentSourceCredentials[goAirObj.BookingSourceFlag].PCC;//Added For Corporate booking Purpose
        }
        goAirObj.AppUserId = (int)Settings.LoginInfo.UserID;
        goAirObj.SessionId = sessionId;

        //Added by Lokesh
        //For India GST Implementation.
        AgentMaster agentMaster = null;
        if (Settings.LoginInfo != null && Settings.LoginInfo.IsOnBehalfOfAgent)
        {
            agentMaster = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
            if (agentMaster != null && !string.IsNullOrEmpty(agentMaster.AgentCurrency) && agentMaster.AgentCurrency.Length > 0 && agentMaster.AgentCurrency == "INR")
            {
                goAirObj.CurrencyCode = "INR";
            }
            else
            {
                goAirObj.CurrencyCode = "AED";
            }
        }
        else
        {
            agentMaster = new AgentMaster(Settings.LoginInfo.AgentId);
            if (Settings.LoginInfo != null && !string.IsNullOrEmpty(Settings.LoginInfo.Currency) && Settings.LoginInfo.Currency.Length > 0 && Settings.LoginInfo.Currency == "INR")
            {
                goAirObj.CurrencyCode = "INR";//For Indian Agency
            }
            else
            {
                goAirObj.CurrencyCode = "AED";
            }
        }

        if (agentMaster != null && agentMaster.Country > 0 && Country.GetCountryCodeByCountryId(agentMaster.Country) == "IN")
        {
            
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
            }
            else
            {
                location = new LocationMaster(Settings.LoginInfo.LocationID);
            }
            if (location != null && location.CountryCode == "IN")
            {
                if (!string.IsNullOrEmpty(location.GstNumber) && location.GstNumber.Length > 0)
                {
                    goAirObj.GSTNumber = location.GstNumber;
                    txtTaxRegNo_SG_GST.Text = location.GstNumber;
                    txtTaxRegNo_SG_GST.Enabled = false;
                }
                if (!string.IsNullOrEmpty(agency.Name) && agency.Name.Length > 0)
                {
                    goAirObj.GSTCompanyName = agency.Name;
                    txtCompanyName_SG_GST.Text = agency.Name;
                    txtCompanyName_SG_GST.Enabled = false;
                }
                if (!string.IsNullOrEmpty(agency.Email2) && agency.Email2.Length > 0)
                {
                    goAirObj.GSTOfficialEmail = agency.Email2.Split(',')[0];
                    txtGSTOfficialEmail.Text = goAirObj.GSTOfficialEmail;
                    txtGSTOfficialEmail.Enabled = false;
                }
                requiredGSTForSG_6E = true;
            }

        }
        goAirObj.SearchByAvailability = true;
        DataTable dtBaggageInfo = null;
        if (SessionValues[ONWARD_BAGGAGE] == null && type == 0)//ONWARD
        {
            dtBaggageInfo = goAirObj.GetAvailableSSR(sgResult);
            SaveInSession(ONWARD_BAGGAGE, dtBaggageInfo);
        }
        else if (SessionValues[ONWARD_BAGGAGE] != null && type == 0)
        {
            dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
        }
        else if (SessionValues[RETURN_BAGGAGE] == null && type == 1)//Return
        {
            dtBaggageInfo = goAirObj.GetAvailableSSR(sgResult);
            SaveInSession(RETURN_BAGGAGE, dtBaggageInfo);
        }
        else if (SessionValues[RETURN_BAGGAGE] != null && type == 1)//Return
        {
            dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
        }
        if (dtBaggageInfo != null && dtBaggageInfo.Rows.Count > 0)
        {
            dtBaggageInfo = dtBaggageInfo.DefaultView.ToTable(true, new string[0]);
            DataRow[] baggageData = new DataRow[0];
            baggageData = dtBaggageInfo.Select("Group=0 AND (Code ='XC05' OR Code ='XC10' OR Code ='XC15' OR Code ='XC30' OR Code='ABAG')");
            if (baggageData != null)
            {
                lblBaggage.Visible = true;
                lblBag.Visible = true;
                ddlBag.Visible = true;
                ListItem upgradeBaggageItem = new ListItem("Upgrade Baggage", "0");
                ListItem noBaggageItem = new ListItem("No Bag", "0");

                if (baggageData.Length > 0)//:Additional Baggage Options Available 
                {
                    ddlBag.Items.Add(upgradeBaggageItem);
                    foreach (DataRow row in baggageData)
                    {
                        ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                        item.Attributes.Add("Code", row["Code"].ToString().Trim());
                        if (!ddlBag.Items.Contains(item))
                        {
                            ddlBag.Items.Add(item);
                        }
                    }
                }
                else//Onward :No Additional Baggage Options Available
                {
                    ddlBag.Items.Add(noBaggageItem);
                }
            }
            if (ddlBag.Visible && ddlBag.Items.Count == 1)
            {
                ddlBag.Visible = false;
            }
        }
    }

    //Go Air Lead Pax and Additional Pax Meal Information
    private void LoadG8MealInfo(DropDownList ddlMeal, Label lblMeal, DataTable dtMealInfo, BookingSource source)
    {
        if (dtMealInfo != null && dtMealInfo.Rows.Count > 0)
        {
            dtMealInfo = dtMealInfo.DefaultView.ToTable(true, new string[0]);
            DataRow[] mealData = new DataRow[0];

            if (source == BookingSource.GoAirCorp)
                mealData = dtMealInfo.Select("Group=0 AND (Code='CRVS' OR Code='CRNV' OR Code='CRJN')");//CORPORATE FREE MEALS
            else
                mealData = dtMealInfo.Select("Group=0 AND (Code='SAVO' OR Code='JNML' OR Code='SUNV' OR Code='SUVG' OR Code='SWNV' OR Code='SWVG' OR Code='OLSC' OR Code='OLNB' OR Code='OLNS' OR Code='OLVB' OR Code='OLVS' OR Code='FEST' OR Code='PMVG' OR Code='PMNV' OR Code='SSNV' OR Code='SSVG' OR Code='KDML' OR Code='DCPP' OR Code='KCPP' OR Code='SRPP' OR Code='RAWC' OR Code='PBPM' OR Code='CASH' OR Code='BHPH' OR Code='BRNC' OR Code='BHGA' OR Code='ALMD' OR Code='POPC' OR Code='XMSV' OR Code='XMSN')");

            lblMeal.Visible = true;
            ddlMeal.Visible = true;

            if (mealData.Length > 0)
            {
                ListItem item = new ListItem("Select Meal", "0");
                ddlMeal.Items.Add(item);
                foreach (DataRow row in mealData)
                {
                    ListItem mealitem = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                    mealitem.Attributes.Add("Code", row["Code"].ToString().Trim());
                    ddlMeal.Items.Add(mealitem);
                }
            }
            if (mealData.Length <= 0)
            {
                ListItem item = new ListItem("No Meal", "0");
                ddlMeal.Items.Add(item);

            }
            if (ddlMeal.Visible && ddlMeal.Items.Count == 1)
            {
                ddlMeal.Visible = false;
                lblMeal.Visible = false;
            }
        }

    }

    //Go Air RePricing
    private void G8Repricing(ref SearchResult resultObj, SearchRequest request)
    {
        try
        {
            //Here we calculate the original price from the ItineraryPrice response.
            //So,we will clear all the previously calculated price component values here.
            resultObj.Price = null;
            resultObj.BaseFare = 0;
            resultObj.Tax = 0;
            resultObj.TotalFare = 0;
            resultObj.FareBreakdown = null;
            int fareBreakDownCount = 0;
            if (request.AdultCount > 0)
            {
                fareBreakDownCount = 1;
            }
            if (request.ChildCount > 0)
            {
                fareBreakDownCount++;
            }
            if (request.InfantCount > 0)
            {
                fareBreakDownCount++;
            }
            resultObj.FareBreakdown = new Fare[fareBreakDownCount];
            GetItineraryPriceForLCCFlights(ref resultObj, request); //Gets the Latest Itenarary Price
            CalculatePriceComponent(ref resultObj, request);//MarkuP Calculation for the result.

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>This web Method will call from client side ajax to call respective supplier API and get seat availability for requested segment based on flight no. 
    /// <para>Segments will be formed based on unique flight no, when we pass both origin and destination as empty assuming it as a first click for seat selection
    /// if seats are not available or any error from the supplier it will send empty response which will raise alert on the client side.
    /// Inputs are selected segment origin, destination and user entered pax names.
    /// </summary>

    [WebMethod(EnableSession = true)]
    public static string GetSeatMap(string sOrigin, string sDestination, string sPaxnames)
    {
        try
        {
            SeatAvailabilityResp clsSeatAvailabilityResp = new SeatAvailabilityResp();
            clsSeatAvailabilityResp.SegmentSeat = new List<SegmentSeats>();
            SegmentSeats sg = new SegmentSeats();
            string sessionId = HttpContext.Current.Session["sessionId"] != null ? HttpContext.Current.Session["sessionId"].ToString() : Guid.NewGuid().ToString();
            string JSONString = string.Empty; int segid = 1;

            MetaSearchEngine mse = new MetaSearchEngine(sessionId);
            mse.SettingsLoginInfo = Settings.LoginInfo;

            SearchResult clsOnwardSearchResult = new SearchResult();
            SearchResult clsReturnSearchResult = new SearchResult();

            Dictionary<string, object> SessionValues = HttpContext.Current.Session["PaxPageValues"] as Dictionary<string, object>;
            clsOnwardSearchResult = (SearchResult)SessionValues[ONWARD_RESULT];
            clsReturnSearchResult = (SearchResult)SessionValues[RETURN_RESULT];

            if (!string.IsNullOrEmpty(sOrigin) && !string.IsNullOrEmpty(sDestination))
                clsSeatAvailabilityResp = (SeatAvailabilityResp)HttpContext.Current.Session["SEATRESPONSES"];
            else
            {
                HttpContext.Current.Session["SEATRESPONSES"] = null;

                var objonwardFlights = clsOnwardSearchResult.Flights[0].Select(p => p.FlightNumber).Distinct();

                foreach (string flightno in objonwardFlights)
                {
                    sg = new SegmentSeats();
                    sg.Origin = clsOnwardSearchResult.Flights[0].Where(y => y.FlightNumber == flightno).Select(z => z.Origin.AirportCode).FirstOrDefault();
                    sg.FlightNo = flightno; sg.STD = clsOnwardSearchResult.Flights[0].Where(y => y.FlightNumber == flightno).Select(z => z.DepartureTime).FirstOrDefault();
                    sg.Destination = clsOnwardSearchResult.Flights[0].Where(y => y.FlightNumber == flightno).Count() == 1 ?
                        clsOnwardSearchResult.Flights[0].Where(y => y.FlightNumber == flightno).Select(z => z.Destination.AirportCode).FirstOrDefault() :
                        clsOnwardSearchResult.Flights[0].Where(y => y.FlightNumber == flightno).Select(z => z.Destination.AirportCode).Last();
                    sOrigin = string.IsNullOrEmpty(sOrigin) ? sg.Origin : sOrigin; sDestination = string.IsNullOrEmpty(sDestination) ? sg.Destination : sDestination;
                    clsSeatAvailabilityResp.SegmentSeat.Add(sg);
                }

                if (clsReturnSearchResult != null)
                {
                    var objreturnFlights = clsReturnSearchResult.Flights[0].Select(p => p.FlightNumber).Distinct();

                    foreach (string flightno in objreturnFlights)
                    {
                        sg = new SegmentSeats();
                        sg.Origin = clsReturnSearchResult.Flights[0].Where(y => y.FlightNumber == flightno).Select(z => z.Origin.AirportCode).FirstOrDefault();
                        sg.FlightNo = flightno; sg.STD = clsReturnSearchResult.Flights[0].Where(y => y.FlightNumber == flightno).Select(z => z.DepartureTime).FirstOrDefault();
                        sg.Destination = clsReturnSearchResult.Flights[0].Where(y => y.FlightNumber == flightno).Count() == 1 ?
                            clsReturnSearchResult.Flights[0].Where(y => y.FlightNumber == flightno).Select(z => z.Destination.AirportCode).FirstOrDefault() :
                            clsReturnSearchResult.Flights[0].Where(y => y.FlightNumber == flightno).Select(z => z.Destination.AirportCode).Last();
                        sOrigin = string.IsNullOrEmpty(sOrigin) ? sg.Origin : sOrigin; sDestination = string.IsNullOrEmpty(sDestination) ? sg.Destination : sDestination;
                        clsSeatAvailabilityResp.SegmentSeat.Add(sg);
                    }
                }
            }


            if (HttpContext.Current.Session["SEATRESPONSES"] != null)
            {
                string sFlightNo = clsSeatAvailabilityResp.SegmentSeat.Where(y => y.Origin == sOrigin && y.Destination == sDestination).Select(x => x.FlightNo).FirstOrDefault();
                clsSeatAvailabilityResp = mse.GetSeatAvailablity
                    (clsOnwardSearchResult.Flights[0].ToList().Where(x => x.FlightNumber == sFlightNo).Count() > 0 || HttpContext.Current.Session["SEATRESPONSES"] == null
                    ? clsOnwardSearchResult : clsReturnSearchResult, sOrigin, sDestination, clsSeatAvailabilityResp, sessionId, sPaxnames);
            }

            int count = clsSeatAvailabilityResp.SegmentSeat.Where(x => x.SeatInfoDetails != null && x.Origin == sOrigin && x.Destination == sDestination).
                Select(y => y.SeatInfoDetails.Count()).FirstOrDefault();
            if (count > 0 || HttpContext.Current.Session["SEATRESPONSES"] == null)
            {
                clsSeatAvailabilityResp.SegmentSeat.ForEach(x => { x.SegmntID = "Seg" + segid.ToString(); segid++; });
                HttpContext.Current.Session["SEATRESPONSES"] = clsSeatAvailabilityResp;
                JSONString = JsonConvert.SerializeObject(clsSeatAvailabilityResp);
            }

            return JSONString;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(GetSeatMap)Failed to get seat availability response. Reason : " + ex.ToString(), "");
            return "";
        }
    }

    /// <summary>Method will call respective supplier API to update pasenger information and to reqeust for SSR if any
    /// <para>The functionality will work for both onward and return itinerary. If there is a failure in any SSR reqeust or update passenger call 
    /// it will go to error page, if any failure in seat assignment it will update itinerary status as Assign seat and show the error 
    /// in popup and allow user to select different seats and will call the API to assign new seats.
    /// if all the calls are success then itinerary status is pax updated and will go to payment confirmation page
    /// <see cref=""/>Will pass both onward and return itinerary and get the booking response</para>
    /// <seealso cref="PassengerDetailsBySegments.imgContinue_Click(object, EventArgs)"/> We are calling this method in imgContinue_Click event
    /// </summary>

    protected BookingResponse UpdatePaxAndAssignSeats(FlightItinerary onwardFlightItinerary, FlightItinerary returnFlightItinerary)
    {
        BookingResponse bookingResponse = new BookingResponse();
        MetaSearchEngine mse = new MetaSearchEngine(Session["sessionId"].ToString());
        mse.SettingsLoginInfo = Settings.LoginInfo;

        int agencyId = Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentID : Settings.LoginInfo.AgentId;

        onwardFlightItinerary.BookRequestType = onwardFlightItinerary.BookRequestType == BookingFlowStatus.NoStatus ? BookingFlowStatus.UpdatePax :
            onwardFlightItinerary.BookRequestType == BookingFlowStatus.AssignSeatFailed ? BookingFlowStatus.AssignSeat : onwardFlightItinerary.BookRequestType;

        if (onwardFlightItinerary.BookRequestType != BookingFlowStatus.PaxUpdated)
            bookingResponse = mse.BookSeats(ref onwardFlightItinerary, agencyId, Convert.ToInt32(pageParams[0]), new UserMaster(Settings.LoginInfo.UserID), true, Request.ServerVariables["REMOTE_ADDR"]);
        else
        {
            bookingResponse.Status = BookingResponseStatus.Successful; bookingResponse.Error = string.Empty;
        }

        if (bookingResponse.Status == BookingResponseStatus.PriceChanged)
            SaveInSession(ONWARD_SSR_PRICE_CHANGE, bookingResponse.SSRMessage);

        if (bookingResponse.Status != BookingResponseStatus.Successful && bookingResponse.Status != BookingResponseStatus.Assignseatfailed && !string.IsNullOrEmpty(bookingResponse.Error))
        {
            Response.Redirect("ErrorPage.aspx?Err=" + bookingResponse.Error, false);
        }
        else
        {
            if (string.IsNullOrEmpty(bookingResponse.Error))
            {
                onwardFlightItinerary.Passenger.ToList().ForEach(y => y.liPaxSeatInfo.ForEach(s => s.SeatStatus = "S"));
                SaveInSession(ONWARD_FLIGHT_ITINERARY, onwardFlightItinerary);
                if (returnFlightItinerary != null)
                {
                    returnFlightItinerary.BookRequestType = returnFlightItinerary.BookRequestType == BookingFlowStatus.NoStatus ? BookingFlowStatus.UpdatePax :
                        returnFlightItinerary.BookRequestType == BookingFlowStatus.AssignSeatFailed ? BookingFlowStatus.AssignSeat : returnFlightItinerary.BookRequestType;
                    bookingResponse = mse.BookSeats(ref returnFlightItinerary, agencyId, Convert.ToInt32(pageParams[1]), new UserMaster(Settings.LoginInfo.UserID), true, Request.ServerVariables["REMOTE_ADDR"]);
                }

                if (!string.IsNullOrEmpty(bookingResponse.Error) && bookingResponse.Status != BookingResponseStatus.Successful && bookingResponse.Status != BookingResponseStatus.Assignseatfailed)
                {
                    Response.Redirect("ErrorPage.aspx?Err=" + bookingResponse.Error, false);
                }
                else
                {

                    if (bookingResponse.Status == BookingResponseStatus.PriceChanged)
                        SaveInSession(RETURN_SSR_PRICE_CHANGE, bookingResponse.SSRMessage);

                    if (string.IsNullOrEmpty(bookingResponse.Error))
                    {
                        if (returnFlightItinerary != null)
                        {
                            for (int c = 0; c < returnFlightItinerary.Passenger.Length; c++)
                            {
                                onwardFlightItinerary.Passenger[c].SeatInfo = string.IsNullOrEmpty(onwardFlightItinerary.Passenger[c].SeatInfo) ? returnFlightItinerary.Passenger[c].SeatInfo
                                    : string.IsNullOrEmpty(returnFlightItinerary.Passenger[c].SeatInfo) ? onwardFlightItinerary.Passenger[c].SeatInfo :
                                    onwardFlightItinerary.Passenger[c].SeatInfo + " | " + returnFlightItinerary.Passenger[c].SeatInfo;
                            }
                        }
                        SaveInSession(ONWARD_FLIGHT_ITINERARY, onwardFlightItinerary);
                        SaveInSession(RETURN_FLIGHT_ITINERARY, returnFlightItinerary);
                    }
                    else
                    {
                        liPaxSeatInfo = new List<PaxSeatInfo>();
                        onwardFlightItinerary.Passenger.ToList().ForEach(f => liPaxSeatInfo.AddRange(f.liPaxSeatInfo));
                        if (returnFlightItinerary != null)
                        {
                            returnFlightItinerary.Passenger.ToList().ForEach(f => liPaxSeatInfo.AddRange(f.liPaxSeatInfo));
                        }
                        txtPaxSeatInfo.Text = JsonConvert.SerializeObject(liPaxSeatInfo);
                    }
                }
            }
            else
            {
                liPaxSeatInfo = new List<PaxSeatInfo>();
                onwardFlightItinerary.Passenger.ToList().ForEach(f => liPaxSeatInfo.AddRange(f.liPaxSeatInfo));
                if (returnFlightItinerary != null)
                {
                    returnFlightItinerary.Passenger.ToList().ForEach(f => liPaxSeatInfo.AddRange(f.liPaxSeatInfo));
                }
                txtPaxSeatInfo.Text = JsonConvert.SerializeObject(liPaxSeatInfo);

                /* Adding journey type flag to identify the failure message journey type while showing the failed seat error message on client side */
                bookingResponse.Error = "O|" + bookingResponse.Error;
            }
        }
        return bookingResponse;
    }


    /// <summary>
    /// This method calculates the Indigo RoundTripFare Repricing.
    /// </summary>
    /// <param name="resultObj"></param>
    /// <param name="request"></param>
    private void SpecialRoundTripRepricing(ref SearchResult resONW, ref SearchResult resRET, SearchRequest request)
    {
        try
        {
            //Here we calculate the original price from the ItineraryPrice response.
            //So,we will clear all the previously calculated price component values here.

            //1.Clear the Onward Result Price Values
            resONW.Price = null;
            resONW.BaseFare = 0;
            resONW.Tax = 0;
            resONW.TotalFare = 0;
            resONW.FareBreakdown = null;
            int fareBreakDownCount = 0;
            if (request.AdultCount > 0)
            {
                fareBreakDownCount = 1;
            }
            if (request.ChildCount > 0)
            {
                fareBreakDownCount++;
            }
            if (request.InfantCount > 0)
            {
                fareBreakDownCount++;
            }
            resONW.FareBreakdown = new Fare[fareBreakDownCount];

            //1.Clear the return Result Price Values
            resRET.Price = null;
            resRET.BaseFare = 0;
            resRET.Tax = 0;
            resRET.TotalFare = 0;
            resRET.FareBreakdown = null;
            resRET.FareBreakdown = new Fare[fareBreakDownCount];

            resONW.Price = new PriceAccounts();
            resRET.Price = new PriceAccounts();

            //Gets the Latest Itenarary Price
            RoundTripPriceRePrice(ref resONW, ref resRET, request);
            //MarkuP Calculation for the onward result.
            CalculatePriceComponent(ref resONW, request);

            //MarkuP Calculation for the return result.
            CalculatePriceComponent(ref resRET, request);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Prices a complete itinerary, computes fees, charges and taxes, and prices using the supplied parameters and returns a response containing the total price including all fees, charges and taxes
    /// The purpose of GetItineraryPrice is to provide the cost for an itinerary.
    /// </summary>
    /// <param name="resultObj"></param>
    /// <param name="request"></param>
    private void RoundTripPriceRePrice(ref SearchResult resONW, ref SearchResult resRET, SearchRequest request)
    {
        try
        {
            CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
            mse.SettingsLoginInfo = Settings.LoginInfo;
            mse.SessionId = Session["sessionId"].ToString();
            mse.SpecialRoundTripReprice(ref resONW, ref resRET, request);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //Indigo Special round Trip Combination Search Additonal Bagage
    private void Get6EAvailableSSR(DropDownList ddlOnBag, Label lblOnBag, DropDownList ddlRetBag, Label lblRetBag, SearchResult onwResult, SearchResult retResult)
    {

        CT.BookingEngine.GDS.IndigoAPI Indigo = new CT.BookingEngine.GDS.IndigoAPI();
        SourceDetails agentDetails = new SourceDetails();
        //  Indigo.SearchByAvailability = true;
        if (onwResult != null && onwResult.ResultBookingSource == BookingSource.Indigo && retResult != null && retResult.ResultBookingSource == BookingSource.Indigo)
        {
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                Indigo.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                Indigo.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                Indigo.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"];

                Indigo.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"].UserID;
                Indigo.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"].Password;
                Indigo.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"].HAP;
                Indigo.PromoCode = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"].PCC;//Added For Corporate booking Purpose
            }
            else
            {
                Indigo.AgentBaseCurrency = Settings.LoginInfo.Currency;
                Indigo.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                Indigo.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                agentDetails = Settings.LoginInfo.AgentSourceCredentials["6E"];

                Indigo.LoginName = Settings.LoginInfo.AgentSourceCredentials["6E"].UserID;
                Indigo.Password = Settings.LoginInfo.AgentSourceCredentials["6E"].Password;
                Indigo.AgentDomain = Settings.LoginInfo.AgentSourceCredentials["6E"].HAP;
                Indigo.PromoCode = Settings.LoginInfo.AgentSourceCredentials["6E"].PCC;//Added For Corporate booking Purpose
            }
            Indigo.AppUserId = Convert.ToInt32(Settings.LoginInfo.UserID);
            Indigo.BookingSourceFlag = "6E";

        }

        else if (onwResult != null && onwResult.ResultBookingSource == BookingSource.IndigoCorp && retResult != null && retResult.ResultBookingSource == BookingSource.IndigoCorp)
        {
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                Indigo.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                Indigo.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                Indigo.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"];

                Indigo.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"].UserID;
                Indigo.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"].Password;
                Indigo.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"].HAP;
                Indigo.PromoCode = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"].PCC;//Added For Corporate booking Purpose
            }
            else
            {
                Indigo.AgentBaseCurrency = Settings.LoginInfo.Currency;
                Indigo.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                Indigo.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                agentDetails = Settings.LoginInfo.AgentSourceCredentials["6ECORP"];

                Indigo.LoginName = Settings.LoginInfo.AgentSourceCredentials["6ECORP"].UserID;
                Indigo.Password = Settings.LoginInfo.AgentSourceCredentials["6ECORP"].Password;
                Indigo.AgentDomain = Settings.LoginInfo.AgentSourceCredentials["6ECORP"].HAP;
                Indigo.PromoCode = Settings.LoginInfo.AgentSourceCredentials["6ECORP"].PCC;//Added For Corporate booking Purpose
            }
            Indigo.AppUserId = Convert.ToInt32(Settings.LoginInfo.UserID);
            Indigo.BookingSourceFlag = "6ECORP";

        }
        //Added by Lokesh
        //For India GST Implementation.
        AgentMaster agentMaster = null;
        if (Settings.LoginInfo != null && Settings.LoginInfo.IsOnBehalfOfAgent)
        {
            agentMaster = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
            if (agentMaster != null && !string.IsNullOrEmpty(agentMaster.AgentCurrency) && agentMaster.AgentCurrency.Length > 0 && agentMaster.AgentCurrency == "INR")
            {
                Indigo.CurrencyCode = "INR";
            }
            else
            {
                Indigo.CurrencyCode = "AED";
            }
            if (agentMaster != null && agentMaster.Country > 0 && Country.GetCountryCodeByCountryId(agentMaster.Country) == "IN" && onwResult.Flights[0][0].Origin.CountryCode == "IN")
            {
                location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                if (location != null && location.CountryCode=="IN")
                {
                    if (!string.IsNullOrEmpty(location.GstNumber) && location.GstNumber.Length > 0)
                    {
                        Indigo.GSTNumber = location.GstNumber;
                        txtTaxRegNo_SG_GST.Text = location.GstNumber;
                        txtTaxRegNo_SG_GST.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(agency.Name) && agency.Name.Length > 0)
                    {
                        Indigo.GSTCompanyName = agency.Name;
                        txtCompanyName_SG_GST.Text = agency.Name;
                        txtCompanyName_SG_GST.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(agency.Email2) && agency.Email2.Length > 0)
                    {
                        Indigo.GSTOfficialEmail = agency.Email2.Split(',')[0];
                        txtGSTOfficialEmail.Text = Indigo.GSTOfficialEmail;
                        txtGSTOfficialEmail.Enabled = false;
                    }
                    requiredGSTForSG_6E = true;
                }
            }
        }
        else
        {
            if (Settings.LoginInfo != null && !string.IsNullOrEmpty(Settings.LoginInfo.Currency) && Settings.LoginInfo.Currency.Length > 0 && Settings.LoginInfo.Currency == "INR")
            {
                Indigo.CurrencyCode = "INR";
            }
            else
            {
                Indigo.CurrencyCode = "AED";
            }
            agentMaster = new AgentMaster(Settings.LoginInfo.AgentId);
            if (agentMaster != null && agentMaster.Country > 0 && Country.GetCountryCodeByCountryId(agentMaster.Country) == "IN" && onwResult.Flights[0][0].Origin.CountryCode == "IN")
            {
                location = new LocationMaster(Settings.LoginInfo.LocationID);
                if (location != null && location.CountryCode=="IN")
                {
                    if (!string.IsNullOrEmpty(location.GstNumber) && location.GstNumber.Length > 0)
                    {
                        Indigo.GSTNumber = location.GstNumber;
                        txtTaxRegNo_SG_GST.Text = location.GstNumber;
                        txtTaxRegNo_SG_GST.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(agency.Name) && agency.Name.Length > 0)
                    {
                        Indigo.GSTCompanyName = agency.Name;
                        txtCompanyName_SG_GST.Text = agency.Name;
                        txtCompanyName_SG_GST.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(agency.Email2) && agency.Email2.Length > 0)
                    {
                        Indigo.GSTOfficialEmail = agency.Email2.Split(',')[0];
                        txtGSTOfficialEmail.Text = Indigo.GSTOfficialEmail;
                        txtGSTOfficialEmail.Enabled = false;
                    }
                    requiredGSTForSG_6E = true;
                }
            }
        }
        if (Session["sessionId"] != null)
        {
            Indigo.SessionId = Session["sessionId"].ToString();
        }
        else
        {
            Indigo.SessionId = Guid.NewGuid().ToString();
        }
        Indigo.AppUserId = Convert.ToInt32(Settings.LoginInfo.UserID);
        DataTable dtBaggageInfo = null;
        if (SessionValues[ONWARD_BAGGAGE] == null && SessionValues[RETURN_BAGGAGE] == null)
        {
            dtBaggageInfo = Indigo.GetSpecialRoundTripSSR(onwResult, retResult);
            SaveInSession(ONWARD_BAGGAGE, dtBaggageInfo);
            SaveInSession(RETURN_BAGGAGE, dtBaggageInfo);
        }
        if (SessionValues[ONWARD_BAGGAGE] != null)//ONWARD
        {
            dtBaggageInfo = SessionValues[ONWARD_BAGGAGE] as DataTable;
        }
        if (SessionValues[RETURN_BAGGAGE] != null)//Return
        {
            dtBaggageInfo = SessionValues[RETURN_BAGGAGE] as DataTable;
        }
        if (dtBaggageInfo != null && dtBaggageInfo.Rows.Count > 0)
        {
            dtBaggageInfo = dtBaggageInfo.DefaultView.ToTable(true, new string[0]);
            DataRow[] baggageData = new DataRow[0];
            baggageData = dtBaggageInfo.Select("Group=0 AND (Code ='XBPA' OR Code ='XBPB' OR Code ='XBPC' OR Code ='XBPD')");

            //Onward DropDown
            if (baggageData != null)
            {
                lblOnBag.Visible = true;
                ddlOnBag.Visible = true;
                ListItem upgradeBaggageItem = new ListItem("Upgrade Baggage", "0");
                ListItem noBaggageItem = new ListItem("No Bag", "0");

                if (baggageData.Length > 0)//:Additional Baggage Options Available 
                {
                    ddlOnBag.Items.Add(upgradeBaggageItem);
                    foreach (DataRow row in baggageData)
                    {
                        ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString());
                        item.Attributes.Add("Code", row["Code"].ToString().Trim());
                        if (!ddlOnBag.Items.Contains(item))
                        {
                            ddlOnBag.Items.Add(item);
                        }
                    }
                }
                else//Onward :No Additional Baggage Options Available
                {
                    ddlOnBag.Items.Add(noBaggageItem);
                }
            }
            if (ddlOnBag.Visible && ddlOnBag.Items.Count == 1)
            {
                ddlOnBag.Visible = false;
            }

            baggageData = dtBaggageInfo.Select("Group=1 AND (Code ='XBPA' OR Code ='XBPB' OR Code ='XBPC' OR Code ='XBPD')");

            //Return Dropdown
            if (baggageData != null)
            {
                lblRetBag.Visible = true;
                ddlRetBag.Visible = true;
                ListItem upgradeBaggageItem = new ListItem("Upgrade Baggage", "0");
                ListItem noBaggageItem = new ListItem("No Bag", "0");

                if (baggageData.Length > 0)//:Additional Baggage Options Available 
                {
                    ddlRetBag.Items.Add(upgradeBaggageItem);
                    foreach (DataRow row in baggageData)
                    {
                        ListItem item = new ListItem(row["Description"].ToString(), row["Price"].ToString());
                        item.Attributes.Add("Code", row["Code"].ToString().Trim());
                        if (!ddlRetBag.Items.Contains(item))
                        {
                            ddlRetBag.Items.Add(item);
                        }
                    }
                }
                else//Onward :No Additional Baggage Options Available
                {
                    ddlRetBag.Items.Add(noBaggageItem);
                }
            }
            if (ddlRetBag.Visible && ddlRetBag.Items.Count == 1)
            {
                ddlRetBag.Visible = false;
            }
        }
    }

    //Indigo Special round Trip Combination Search Additonal Meals
    void Load6EMealInfo(DropDownList ddlOnwardMeal, DropDownList ddlInwardMeal, Label lblOnMeal, Label lblInMeal, DataTable dtMealInfo)
    {
        try
        {
            if (dtMealInfo != null && dtMealInfo.Rows.Count > 0)
            {
                dtMealInfo = dtMealInfo.DefaultView.ToTable(true, new string[0]);
                DataRow[] mealData = new DataRow[0];
                DataRow[] returnMeal = new DataRow[0];
                if (ddlInwardMeal != null)//RETURN MEAL DROPDOWN
                {
                    ddlInwardMeal.Items.Clear();
                    returnMeal = dtMealInfo.Select("Group=1 AND (Code='VGML' OR Code='NVML' OR Code='TCSW' OR Code='PTSW'  OR Code='CJSW' OR Code='CTSW' OR Code='POHA' OR Code='UPMA' OR Code='SACH' OR Code='VBIR' OR Code='DACH' OR Code='CHFR' OR Code='CHCH' OR Code='MASP' OR Code='SMAL')");
                    if (returnMeal.Length > 0)
                    {
                        ddlInwardMeal.Visible = true;
                        lblInMeal.Visible = true;
                    }
                    else
                    {
                        ddlInwardMeal.Visible = false;
                        lblInMeal.Visible = false;
                    }

                }
                if (ddlOnwardMeal != null)//ONWARD MEAL DROPDOWN
                {
                    ddlOnwardMeal.Items.Clear();
                    mealData = dtMealInfo.Select("Group=0 AND (Code='VGML' OR Code='NVML' OR Code='TCSW' OR Code='PTSW'  OR Code='CJSW' OR Code='CTSW' OR Code='POHA' OR Code='UPMA' OR Code='SACH' OR Code='VBIR' OR Code='DACH' OR Code='CHFR' OR Code='CHCH' OR Code='MASP' OR Code='SMAL')");
                    if (mealData.Length > 0)
                    {
                        lblOnMeal.Visible = true;
                        ddlOnwardMeal.Visible = true;
                    }
                    else
                    {
                        lblOnMeal.Visible = false;
                        ddlOnwardMeal.Visible = false;
                    }
                }

                if (mealData.Length > 0)
                {
                    ListItem item = new ListItem("Select Meal", "0");
                    ddlOnwardMeal.Items.Add(item);
                    foreach (DataRow row in mealData)
                    {
                        ListItem mealitem = new ListItem(row["Description"].ToString(), row["Price"].ToString()+"-"+ row["Code"].ToString().Trim());
                        mealitem.Attributes.Add("Code", row["Code"].ToString().Trim());
                        ddlOnwardMeal.Items.Add(mealitem);
                    }
                }
                if (returnMeal.Length > 0)
                {
                    ListItem item = new ListItem("Select Meal", "0");
                    ddlInwardMeal.Items.Add(item);

                    foreach (DataRow row in returnMeal)
                    {
                        ListItem mealitem = new ListItem(row["Description"].ToString(), row["Price"].ToString() + "-" + row["Code"].ToString().Trim());
                        mealitem.Attributes.Add("Code", row["Code"].ToString().Trim());
                        ddlInwardMeal.Items.Add(mealitem);
                    }
                }
                if (mealData.Length <= 0)
                {
                    ListItem item = new ListItem("No Meal", "0");
                    ddlOnwardMeal.Items.Add(item);

                }
                if (returnMeal.Length <= 0)
                {
                    ListItem item = new ListItem("No Meal", "0");
                    ddlInwardMeal.Items.Add(item);
                }
                if (ddlOnwardMeal.Visible && ddlOnwardMeal.Items.Count == 1)
                {
                    ddlOnwardMeal.Visible = false;
                    lblOnMeal.Visible = false;
                }
                if (ddlInwardMeal.Visible && ddlInwardMeal.Items.Count == 1)
                {
                    ddlInwardMeal.Visible = false;
                    lblInMeal.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }

    }

    //Spicejet Special round Trip Combination Search Additonal Bagage
    private void GetSGAvailableSSR()
    {
        CT.BookingEngine.GDS.SpiceJetAPIV1 spiceJet = new CT.BookingEngine.GDS.SpiceJetAPIV1();
        SourceDetails agentDetails = new SourceDetails();
        if (onwardResult != null && onwardResult.ResultBookingSource == BookingSource.SpiceJet && returnResult != null && returnResult.ResultBookingSource == BookingSource.SpiceJet)
        {
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                spiceJet.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                spiceJet.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"];

                spiceJet.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"].UserID;
                spiceJet.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"].Password;
                spiceJet.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"].HAP;
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                spiceJet.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                spiceJet.PromoCode = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"].PCC;//Added For Corporate Booking Purpose
            }
            else
            {
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.Currency;
                spiceJet.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                spiceJet.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                agentDetails = Settings.LoginInfo.AgentSourceCredentials["SG"];

                spiceJet.LoginName = Settings.LoginInfo.AgentSourceCredentials["SG"].UserID;
                spiceJet.Password = Settings.LoginInfo.AgentSourceCredentials["SG"].Password;
                spiceJet.AgentDomain = Settings.LoginInfo.AgentSourceCredentials["SG"].HAP;
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.Currency;
                spiceJet.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                spiceJet.PromoCode = Settings.LoginInfo.AgentSourceCredentials["SG"].PCC;//Added For Corporate Booking Purpose
            }
            spiceJet.BookingSourceFlag = "SG";
        }
        else if (onwardResult != null && onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp && returnResult != null && returnResult.ResultBookingSource == BookingSource.SpiceJetCorp)
        {
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                spiceJet.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                spiceJet.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"];

                spiceJet.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"].UserID;
                spiceJet.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"].Password;
                spiceJet.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"].HAP;
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                spiceJet.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                spiceJet.PromoCode = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"].PCC;//Added For Corporate Booking Purpose
            }
            else
            {
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.Currency;
                spiceJet.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                spiceJet.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                agentDetails = Settings.LoginInfo.AgentSourceCredentials["SGCORP"];

                spiceJet.LoginName = Settings.LoginInfo.AgentSourceCredentials["SGCORP"].UserID;
                spiceJet.Password = Settings.LoginInfo.AgentSourceCredentials["SGCORP"].Password;
                spiceJet.AgentDomain = Settings.LoginInfo.AgentSourceCredentials["SGCORP"].HAP;
                spiceJet.AgentBaseCurrency = Settings.LoginInfo.Currency;
                spiceJet.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                spiceJet.PromoCode = Settings.LoginInfo.AgentSourceCredentials["SGCORP"].PCC;//Added For Corporate Booking Purpose
            }
            spiceJet.BookingSourceFlag = "SGCORP";
        }

        //Added by Lokesh
        //For India GST Implementation.
        AgentMaster agentMaster = null;
        if (Settings.LoginInfo != null && Settings.LoginInfo.IsOnBehalfOfAgent)
        {
            agentMaster = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
            if (agentMaster != null && !string.IsNullOrEmpty(agentMaster.AgentCurrency) && agentMaster.AgentCurrency.Length > 0 && agentMaster.AgentCurrency == "INR")
            {
                spiceJet.CurrencyCode = "INR";
            }
            else
            {
                spiceJet.CurrencyCode = "AED";
            }
            if (agentMaster != null && agentMaster.Country > 0 && Country.GetCountryCodeByCountryId(agentMaster.Country) == "IN" && onwardResult != null && onwardResult.Flights[0][0].Origin.CountryCode == "IN")
            {
                location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                if (location != null && location.CountryCode == "IN")
                {
                    if (!string.IsNullOrEmpty(location.GstNumber) && location.GstNumber.Length > 0)
                    {
                        spiceJet.GSTNumber = location.GstNumber;
                        txtTaxRegNo_SG_GST.Text = location.GstNumber;
                        txtTaxRegNo_SG_GST.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(agency.Name) && agency.Name.Length > 0)
                    {
                        spiceJet.GSTCompanyName = agency.Name;
                        txtCompanyName_SG_GST.Text = agency.Name;
                        txtCompanyName_SG_GST.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(agency.Email2) && agency.Email2.Length > 0)
                    {
                        spiceJet.GSTOfficialEmail = agency.Email2.Split(',')[0];
                        txtGSTOfficialEmail.Text = spiceJet.GSTOfficialEmail;
                        txtGSTOfficialEmail.Enabled = false;
                    }
                    requiredGSTForSG_6E = true;
                }

            }
        }
        else
        {
            if (Settings.LoginInfo != null && !string.IsNullOrEmpty(Settings.LoginInfo.Currency) && Settings.LoginInfo.Currency.Length > 0 && Settings.LoginInfo.Currency == "INR")
            {
                spiceJet.CurrencyCode = "INR";
            }
            else
            {
                spiceJet.CurrencyCode = "AED";
            }
            agentMaster = new AgentMaster(Settings.LoginInfo.AgentId);

            if (agentMaster != null && agentMaster.Country > 0 && Country.GetCountryCodeByCountryId(agentMaster.Country) == "IN" && onwardResult != null && onwardResult.Flights[0][0].Origin.CountryCode == "IN")
            {
                location = new LocationMaster(Settings.LoginInfo.LocationID);
                if (location != null && location.CountryCode == "IN")
                {
                    if (!string.IsNullOrEmpty(location.GstNumber) && location.GstNumber.Length > 0)
                    {
                        spiceJet.GSTNumber = location.GstNumber;
                        txtTaxRegNo_SG_GST.Text = location.GstNumber;
                        txtTaxRegNo_SG_GST.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(agency.Name) && agency.Name.Length > 0)
                    {
                        spiceJet.GSTCompanyName = agency.Name;
                        txtCompanyName_SG_GST.Text = agency.Name;
                        txtCompanyName_SG_GST.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(agency.Email2) && agency.Email2.Length > 0)
                    {
                        spiceJet.GSTOfficialEmail = agency.Email2.Split(',')[0];
                        txtGSTOfficialEmail.Text = spiceJet.GSTOfficialEmail;
                        txtGSTOfficialEmail.Enabled = false;
                    }
                    requiredGSTForSG_6E = true;
                }

            }
        }
        if (Session["sessionId"] != null)
        {
            spiceJet.SessionID = Session["sessionId"].ToString();
        }
        else
        {
            spiceJet.SessionID = Guid.NewGuid().ToString();
        }
        spiceJet.AppUserID = Convert.ToInt32(Settings.LoginInfo.UserID);
        spiceJet.SearchByAvailability = true;
        DataTable dtBaggageInfo = null;
        if (SessionValues[ONWARD_BAGGAGE] == null && SessionValues[RETURN_BAGGAGE] == null)
        {
            dtBaggageInfo = spiceJet.GetSpecialRoundTripSSR(onwardResult, returnResult);
            SaveInSession(ONWARD_BAGGAGE, dtBaggageInfo);
            SaveInSession(RETURN_BAGGAGE, dtBaggageInfo);
        }
    }

    /// <summary>
    /// Common method to load baggage details
    /// </summary>
    /// <param name="dtBaggageDetails"></param>
    /// <param name="ddlBaggage"></param>
    /// <param name="sessionkey"></param>
    /// <param name="lblBagg"></param>
    void LoadBaggageInfo(DataTable dtBaggageDetails, DropDownList ddlBaggage, string sessionkey, Label lblBagg)
    {
        try
        {
            if (dtBaggageDetails != null && dtBaggageDetails.Rows.Count > 0)
            {
                SaveInSession(sessionkey, dtBaggageDetails);
                lblBaggage.Text = "Select Baggage:";
                lblBaggage.Visible = lblBagg.Visible = ddlBaggage.Visible = true;
                ddlBaggage.DataSource = dtBaggageDetails;
                ddlBaggage.DataTextField = "baggageCode";
                ddlBaggage.DataValueField = "BagValue";
                ddlBaggage.DataBind();
                ddlBaggage.Items.Insert(0, new ListItem("Select Baggage", "0"));
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    void LoadG9MealInfo(DataTable dtMealDetails, DropDownList ddlmeal, string sessionkey, Label lblmeal)
    {
        try
        {
            if (dtMealDetails != null && dtMealDetails.Rows.Count > 0)
            {
                SaveInSession(sessionkey, dtMealDetails);
                lblmeal.Visible = ddlmeal.Visible = true;
                ddlmeal.DataSource = dtMealDetails;
                ddlmeal.DataTextField = "mealDescription";
                ddlmeal.DataValueField = "mealValue";
                ddlmeal.DataBind();
                ddlmeal.Items.Insert(0, new ListItem("Select Meal", "0-0"));
            }
           
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }


    //Assigns The Salam Air Baggage Codes 
    void AssignSalamAirBagCodes(FlightPassenger pax, string defaultBaggage)
    {
        try
        {
            pax.FlyDubaiBaggageCharge = new List<decimal>();

            pax.Price.BaggageCharge = 0;
            pax.FlyDubaiBaggageCharge.Add(0);

            //With Default Baggage
            if (!string.IsNullOrEmpty(defaultBaggage))
            {
                // DEFAULT BAGGAGE for ONWARD
                if (Convert.ToDecimal(defaultBaggage.Split(',')[0].Split('-')[0]) > 0 && defaultBaggage.Split(',')[0].Contains("-"))
                {
                    pax.BaggageCode = defaultBaggage.Split(',')[0].Split('-')[0] + "KG";
                    pax.BaggageType = defaultBaggage.Split(',')[0].Split('-')[1];
                }
                else
                    pax.BaggageCode = "NO BAG";
            }
            else//WITHOUT DEFAULT BAGGAGE
            {
                pax.BaggageCode = "NO BAG";
                if (request.Type == SearchType.Return)
                {
                    pax.BaggageCode += "," + "NO BAG";
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to execute AssignSalamAirBagCodes.Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
}
