﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.Core;
using System.IO;
using CT.HolidayDeals;
using CT.TicketReceipt.BusinessLayer;

public partial class HolidayDealPackageGUI :CT.Core.ParentPage
{
    protected Dictionary<string, string> hotelVariables = new Dictionary<string, string>();
    protected Dictionary<string, string[]> hotelVariablesArray = new Dictionary<string, string[]>();
    protected string[] splitter = { "#&#" };
    protected bool requestEdit = false;
    private string HDEAL_SESSION = "Hdealdetails";
  //  protected AgentMaster agency = new AgentMaster();
    //protected UserMaster member = new UserMaster();
    protected List<int> choosenThemeList = new List<int>();
    protected HolidayPackage package;
    protected List<PackageTheme> themeList = new List<PackageTheme>();
    string[] emptyStringArr = new string[1];
    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Text = string.Empty;
        //AuthorizationCheck(); //commented BY vijesh

        try
        {
            //commented by chandan
            //agency = new AgentMaster(Convert.ToInt32(Settings.LoginInfo.AgentId));
            //member = new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));
            if (Convert.ToInt32(Request["dealId"]) > 0)
            {
                chkCopy.Checked = false;
                chkCopy.Enabled = false;
                ddlAgent.Enabled = false;
            }
            else
            {
                if (!chkCopy.Checked)
                {
                    if (CurrentObject != null) CurrentObject = null;
                }
            }
           

            emptyStringArr[0] = string.Empty;
            if (!IsPostBack)
            {
                int dealId = Convert.ToInt32(Request["dealId"]);
                if (dealId > 0)
                    LoadPackage(dealId);

                //txtDuration.Attributes.Add("readonly", "true");
                if (dealId <= 0)
                {
                    InitializePageControls();

                    int inclusionsCount = 0;
                    if (inclusionsCount < 2)
                    {
                        inclusionsCount = 2;
                        hotelVariablesArray.Add("inclusionsValue", emptyStringArr);
                    }
                    int exclusionsCount = 0;
                    if (exclusionsCount < 2)
                    {
                        exclusionsCount = 2;
                        hotelVariablesArray.Add("exclusionsValue", emptyStringArr);
                    }

                    int priceExcludeCount = 0;
                    if (priceExcludeCount < 2)
                    {
                        priceExcludeCount = 2;
                        hotelVariablesArray.Add("priceExcludeValue", emptyStringArr);
                    }

                    int tncValueCount = 0;
                    if (tncValueCount < 2)
                    {
                        tncValueCount = 2;
                        hotelVariablesArray.Add("tncValue", emptyStringArr);
                    }


                }

                else
                {
                   //Method
                    BindDetails();
                }
            }
            else
            {
                LoadPageControls();
                BindItinerary();
            }
            hdfinclusion.Value = "";
            hdfExclusions.Value = "";
            hdfpriceExclude.Value = "";
            hdftnc.Value = "";
            hotelVariables.Add("exclusionsCount", hdfExclCounter.Value);
            hotelVariables.Add("inclusionsCount", hdfinclCounter.Value);
            hotelVariables.Add("priceExcludeCount", hdfpriceExcludeCounter.Value);
            hotelVariables.Add("tncCount", hdftncCounter.Value);

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.HotelCMS, Severity.High, (int)Settings.LoginInfo.UserID, "Holiday Deal Package Pageload "+ex.ToString()+" "+ DateTime.Now, "");
        }
    }
    private void InitializePageControls()
    {
        try
        {
            BindNights();
            BindAgent();
            hdfAgencyImagePath.Value = HolidayPackage.GetAgencyImagePath(Convert.ToInt16(ddlAgent.SelectedItem.Value), "P").ToString();
            BindPackage(Convert.ToInt32(ddlAgent.SelectedItem.Value));
            BindAgentCopy(ddlAgent.SelectedItem.Value);
            Getthemes();
            BindCurrency(Settings.LoginInfo.AgentId);
           

            
        }
        catch
        {
            throw;
        }
    }
    private void LoadPageControls()
    {
        try
        {
           emptyStringArr = new string[1];
           emptyStringArr[0] = string.Empty;
           int inclusionsCount = Convert.ToInt16(hdfinclCounter.Value);
           emptyStringArr[0] = string.Empty;

           string inclusions = string.Empty;
           for (int i = 1; i < inclusionsCount; i++)
           {
               inclusions += Request["InclusionsText-" + i];
               inclusions += "#&#";
           }
           if (inclusions == "#&#")
           {
               hotelVariablesArray.Add("inclusionsValue", emptyStringArr);
               hdfinclCounter.Value = "2";
           }
           else
           {
               if (inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
               {
                   hotelVariablesArray.Add("inclusionsValue", emptyStringArr);
                   hdfinclCounter.Value = "2";
               }
               else
               {
                   hotelVariablesArray.Add("inclusionsValue", inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                   hdfinclCounter.Value = Convert.ToString(inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
               }
           }


            //Exclusion

           int exclusionsCount = Convert.ToInt16(hdfExclCounter.Value);
           emptyStringArr[0] = string.Empty;
           string exclusions = string.Empty;
           for (int i = 1; i < exclusionsCount; i++)
           {
               exclusions += Request["ExclusionsText-" + i];
               exclusions += "#&#";
           }
           if (exclusions == "#&#")
           {
               hotelVariablesArray.Add("exclusionsValue", emptyStringArr);
               hdfExclCounter.Value = "2";
           }
           else
           {
               if (exclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
               {
                   hotelVariablesArray.Add("exclusionsValue", emptyStringArr);
                   hdfExclCounter.Value = "2";
               }
               else
               {
                   hotelVariablesArray.Add("exclusionsValue", exclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                   hdfExclCounter.Value = Convert.ToString(exclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
               }
           }

            //Price does not include
           int priceExcludeCount = Convert.ToInt16(hdfpriceExcludeCounter.Value);
           emptyStringArr[0] = string.Empty;
           string priceExclude = string.Empty;
           for (int i = 1; i < priceExcludeCount; i++)
           {
               priceExclude += Request["PriceIncludeText-" + i];
               priceExclude += "#&#";
           }
           if (priceExclude == "#&#")
           {
               hotelVariablesArray.Add("priceExcludeValue", emptyStringArr);
               hdfpriceExcludeCounter.Value = "2";
           }
           else
           {
               if (priceExclude.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
               {
                   hotelVariablesArray.Add("priceExcludeValue", emptyStringArr);
                   hdfpriceExcludeCounter.Value = "2";
               }
               else
               {
                   hotelVariablesArray.Add("priceExcludeValue", priceExclude.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                   hdfpriceExcludeCounter.Value = Convert.ToString(priceExclude.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
               }
           }
          

         //Terms and Conditions

           int tncValueCount = Convert.ToInt16(hdftncCounter.Value);
           emptyStringArr[0] = string.Empty;
           string tnc = string.Empty;
           for (int i = 1; i < tncValueCount; i++)
           {
               tnc += Request["T&CText-" + i];
               tnc += "#&#";
           }
           if (tnc == "#&#")
           {
               hotelVariablesArray.Add("tncValue", emptyStringArr);
               hdftncCounter.Value = "2";
           }
           else
           {
               if (tnc.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length == 0)
               {
                   hotelVariablesArray.Add("tncValue", emptyStringArr);
                   hdftncCounter.Value = "2";
               }
               else
               {
                   hotelVariablesArray.Add("tncValue", tnc.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
                   hdftncCounter.Value = Convert.ToString(tnc.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
               }
           }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private CT.HolidayDeals.HolidayPackage CurrentObject
    {
        get
        {
            return (CT.HolidayDeals.HolidayPackage)Session[HDEAL_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(HDEAL_SESSION);
            }
            else
            {
                Session[HDEAL_SESSION] = value;
            }

        }

    }

    private void Getthemes()
    {
        try
        {
            chkTheme.DataSource = CT.HolidayDeals.PackageTheme.Load();
            chkTheme.DataValueField = "themeId";
            chkTheme.DataTextField = "themeName";
            chkTheme.DataBind();
        }
        catch
        {
            throw;
        }
    }

    private void BindNights()
    {
        try
        {
            if (ddlNights.Items.Count > 0)
            {
                ddlNights.Items.Clear();//@@@@  added by chandan for restrict duplicate data
                ddlNights.Items.Add("All");
                for (int i = 1; i < 13; i++)
                {
                    ddlNights.Items.Insert(i, new ListItem(i.ToString()));
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    protected void ddlNights_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            tblitinerary.Controls.Clear();
            BindItinerary();
            txtCity.Focus();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            
           
            if (ddlAgent.SelectedIndex > 0)
            {
                Clear();
                int agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                BindPackage(agentId);
                BindAgentCopy(Convert.ToString(agentId));
                BindCurrency(agentId);
               
                //Commented by chandan on 10/10/2015 @@@   
                hdfAgencyImagePath.Value = CT.HolidayDeals.HolidayPackage.GetAgencyImagePath(agentId, "P").ToString();
                //hdfAgencyImagePath.Value = Server.MapPath("~/images/PackageImages/");
                
               
            }
            else
            {
                //ddlPackages.Style.Add("display", "block");
               // ddlAgentCopy.Style.Add("display", "block");
                //lblcopyAgent.Style.Add("display", "block");
                //lblPackage.Style.Add("display", "block");
                //ddlPackages.SelectedIndex = -1;
                //ddlAgentCopy.SelectedIndex = -1;
            }
            chkCopy.Checked = false;
            chkCopy.Enabled = true;
            ddlPackages.Style.Add("display", "none");
            ddlAgentCopy.Style.Add("display", "none");
            lblcopyAgent.Style.Add("display", "none");
            lblPackage.Style.Add("display", "none");
            ddlPackages.SelectedIndex = -1;
            ddlAgentCopy.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    protected void ddlAgentCopy_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlAgentCopy.SelectedIndex > 0)
            {
                
               
               
                int agentId = Convert.ToInt32(ddlAgentCopy.SelectedItem.Value);     
                        
                hdfAgencyImagePath.Value = CT.HolidayDeals.HolidayPackage.GetAgencyImagePath(agentId, "P").ToString();
               // hdfAgencyImagePath.Value = Server.MapPath("~/images/PackageImages/");
                ddlPackages.Style.Add("display", "block");
                ddlAgentCopy.Style.Add("display", "block");
                lblcopyAgent.Style.Add("display", "block");
                lblPackage.Style.Add("display", "block");

                

                    AgentMaster agent=new AgentMaster(Convert.ToInt32(ddlAgentCopy.SelectedValue));
                    ddlSupplierCurrency.SelectedItem.Text = agent.AgentCurrency;
                    //getting here rateOfExchange values by selected agent
                    StaticData sd = new StaticData();
                    sd.BaseCurrency = agent.AgentCurrency;
                    string agentCurrency = AgentMaster.GetAgentCurrency(Convert.ToInt32(ddlAgent.SelectedValue));
                    decimal rateOfExchange = sd.CurrencyROE.ContainsKey(agentCurrency) ? sd.CurrencyROE[agentCurrency] : 1;
                    txtStartFrom.Text = Math.Round(Convert.ToDecimal(txtStartFrom.Text.Trim()) * rateOfExchange, agent.DecimalValue).ToString();
                    ddlAgentCopy.Enabled = false;

            }
            
            chkCopy.Enabled =false ;
            
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.HotelCMS, Severity.High, (int)Settings.LoginInfo.UserID, "Execption : " + ex.ToString(), "");
           
        }

    }

    private void Clear()
    {
        try
        {
            txtPackage.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtCountry.Text = string.Empty;
            txtOverView.Text = string.Empty;
            txtRoomRates.Text = string.Empty;
            txtStartFrom.Text = string.Empty;
            chkTheme.Dispose();
            chkTheme.SelectedIndex = -1;
            CurrentObject = null;
            tblitinerary.Controls.Clear();
            hotelVariables = new Dictionary<string, string>();
            hotelVariablesArray = new Dictionary<string, string[]>();
            hdfinclCounter.Value = "2";
            hdfExclCounter.Value = "2";
            hdftncCounter.Value = "2";
            hdfpriceExcludeCounter.Value = "2";
            int inclusionsCount = 0;
            if (inclusionsCount < 2)
            {
                inclusionsCount = 2;
                hotelVariablesArray.Add("inclusionsValue", emptyStringArr);
            }
            int exclusionsCount = 0;
            if (exclusionsCount < 2)
            {
                exclusionsCount = 2;
                hotelVariablesArray.Add("exclusionsValue", emptyStringArr);
            }

            int priceExcludeCount = 0;
            if (priceExcludeCount < 2)
            {
                priceExcludeCount = 2;
                hotelVariablesArray.Add("priceExcludeValue", emptyStringArr);
            }

            int tncValueCount = 0;
            if (tncValueCount < 2)
            {
                tncValueCount = 2;
                hotelVariablesArray.Add("tncValue", emptyStringArr);
            }
            hdfinclusion.Value = "";
            hdfExclusions.Value = "";
            hdfpriceExclude.Value = "";
            hdftnc.Value = "";
            hotelVariables.Add("exclusionsCount", hdfExclCounter.Value);
            hotelVariables.Add("inclusionsCount", hdfinclCounter.Value);
            hotelVariables.Add("priceExcludeCount", hdfpriceExcludeCounter.Value);
            hotelVariables.Add("tncCount", hdftncCounter.Value);
            ddlPackages.Style.Add("display", "none");
            ddlAgentCopy.Style.Add("display", "none");
            lblcopyAgent.Style.Add("display", "none");
            lblPackage.Style.Add("display", "none");
            //ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            //ddlSupplierCurrency.SelectedItem.Text = Settings.LoginInfo.Currency;
            ddlTourType.SelectedIndex = -1;
            ddlPackages.SelectedIndex = -1;
            ddlAgentCopy.SelectedIndex = -1;
            txtDescription.Text = string.Empty;

            lblUpload3.Text = string.Empty;
            lblUplaod2.Text = string.Empty;
            lblUpload1.Text = string.Empty;

            //added on 02032016 for clear MainImage1&2 label control
            lblUploadMainImg1.Text = string.Empty;
            lblUploadMainImg2.Text = string.Empty;

            fuImage1.Dispose();
            fuImage2.Dispose();
            fuImage3.Dispose();
            //added on 02032016 for clear MainImage1&2 upload control
            mainImage1.Dispose();
            mainImage2.Dispose();
            lbUploadMsg.Text = string.Empty;
            lbUpload2Msg.Text = string.Empty;
            lbUpload3Msg.Text = string.Empty;
            //added on 02032016 for clear MainImage1&2 LinkButton control
            lbUploadMainImg1.Text = string.Empty;
            lbUploadMainImg2.Text = string.Empty;


            chkTheme.Dispose();
           

            ddlNights.SelectedIndex = -1;
            chkPackageIncludes.Dispose();
            chkPackageIncludes.SelectedIndex = -1;
            ddlAgentCopy.Enabled = true;
            ddlAgent.Enabled = true;
            
            
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void Save()
    {
        try
        {


            int dealId = 0;
            string seasonIdToDelete = string.Empty;
            CT.HolidayDeals.HolidayPackage holidayPackage;


            if (txtOverView.Text == "") throw new Exception(lblError.Text = "OverView cannot be blank !");
            if (txtRoomRates.Text == "") throw new Exception(lblError.Text = "RoomRates cannot be blank !");
            if (lbUploadMsg.Text == "") throw new Exception(lblError.Text = "Main Image should be uploaded  !");
            if (lbUpload2Msg.Text == "") throw new Exception(lblError.Text = "Side Image should be uploaded  !");
            if (lbUpload3Msg.Text == "") throw new Exception(lblError.Text = "Thumbnail should be uploaded  !");
            if (lbUploadMainImg1.Text == "") throw new Exception(lblError.Text = "Thumbnail should be uploaded  !"); //added on 02032016 for Main Image1
            if (lbUploadMainImg2.Text == "") throw new Exception(lblError.Text = "Thumbnail should be uploaded  !"); //added on 02032016 for Main Image2
            if (CurrentObject == null)
            {
                holidayPackage = new CT.HolidayDeals.HolidayPackage();
                holidayPackage.DealId = 0;
                holidayPackage.AgencyId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            }
            else
            {
                holidayPackage = CurrentObject;
                if (chkCopy.Checked)
                {
                    dealId = 0;
                    holidayPackage.DealId = 0;
                    holidayPackage.AgencyId = Convert.ToInt32(ddlAgentCopy.SelectedItem.Value);
                }
                else
                {
                    dealId = CurrentObject.DealId;
                    holidayPackage.DealId = CurrentObject.DealId;
                    //holidayPackage.AgencyId = (int)agency.ID;
                    holidayPackage.AgencyId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
            }
            holidayPackage.DealName = txtPackage.Text.Trim();
            holidayPackage.City = txtCity.Text.Trim();
            holidayPackage.Country = txtCountry.Text.Trim();
            holidayPackage.Description = txtDescription.Text.Trim();
            holidayPackage.IsInternational = true;

            holidayPackage.Nights = Convert.ToInt32(ddlNights.SelectedItem.Value);
            for (int i = 0; i < chkPackageIncludes.Items.Count; i++)
            {
                if (chkPackageIncludes.Items[i].Selected)
                {
                    if (chkPackageIncludes.Items[i].Value == "AirFare")
                    {
                        holidayPackage.HasAirfare = true;
                    }
                    if (chkPackageIncludes.Items[i].Value == "Hotel")
                    {
                        holidayPackage.HasHotel = true;
                    }
                    if (chkPackageIncludes.Items[i].Value == "SightSeeing")
                    {
                        holidayPackage.HasSightSeeing = true;
                    }
                    if (chkPackageIncludes.Items[i].Value == "Meals")
                    {
                        holidayPackage.HasMeals = true;
                    }
                    if (chkPackageIncludes.Items[i].Value == "Transport")
                    {
                        holidayPackage.HasTransport = true;
                    }
                }
                //@@@@ added by chandan on 02/12/2015
                else
                {
                    if (chkPackageIncludes.Items[i].Value == "AirFare")
                    {
                        holidayPackage.HasAirfare = false;
                    }
                    if (chkPackageIncludes.Items[i].Value == "Hotel")
                    {
                        holidayPackage.HasHotel = false;
                    }
                    if (chkPackageIncludes.Items[i].Value == "SightSeeing")
                    {
                        holidayPackage.HasSightSeeing = false;
                    }
                    if (chkPackageIncludes.Items[i].Value == "Meals")
                    {
                        holidayPackage.HasMeals = false;
                    }
                    if (chkPackageIncludes.Items[i].Value == "Transport")
                    {
                        holidayPackage.HasTransport = false;
                    }

                }
            }

            holidayPackage.Overview = txtOverView.Text.Trim();
            holidayPackage.TourType = ddlTourType.SelectedItem.Value;
            int incCounter = Convert.ToInt32(Request["InclusionsCurrent"]);
            string inclusions = string.Empty;
            for (int i = 1; i < incCounter; i++)
            {
                inclusions += Request["InclusionsText-" + i];
                inclusions += "#&#";
            }
            holidayPackage.Inclusions = inclusions;
            string itinerary = string.Empty;
            string day = string.Empty;
            string desc = string.Empty;
            string meal = string.Empty;
            string oth = string.Empty;
            string daySupplierName = string.Empty;
            string itnSupplierEmail = string.Empty;
            for (int i = 0; i < Convert.ToInt32(ddlNights.SelectedItem.Value) + 1; i++)
            {
                TextBox txtDays = tblitinerary.Rows[i].Cells[0].FindControl("txtDay_" + i.ToString()) as TextBox;
                TextBox txtDesc = tblitinerary.Rows[i].Cells[0].FindControl("txtDesc_" + i.ToString()) as TextBox;
                TextBox txtMeal = tblitinerary.Rows[i].Cells[0].FindControl("txtMeals_" + i.ToString()) as TextBox;
                TextBox txtOptional = tblitinerary.Rows[i].Cells[0].FindControl("txtOptional_" + i.ToString()) as TextBox;
                TextBox txtSupp = tblitinerary.Rows[i].Cells[0].FindControl("txtSuppName_" + i.ToString()) as TextBox;
                TextBox txtSuppEmail = tblitinerary.Rows[i].Cells[0].FindControl("txtSuppNameEmail_" + i.ToString()) as TextBox;
                int a = i + 1;
                day = string.Empty;
                desc = string.Empty;
                meal = string.Empty;
                oth = string.Empty;
                daySupplierName = string.Empty;
                itnSupplierEmail = string.Empty;

                if (txtDays.Text.Trim() != null && txtDays.Text.Trim() != "")
                {
                    day = txtDays.Text.Trim();
                }
                else
                {
                    day = "N.A";
                }
                if (txtDesc.Text.Trim() != null && txtDesc.Text.Trim() != "")
                {
                    desc = txtDesc.Text.Trim().Replace("\r\n", " ");
                }
                else
                {
                    desc = "N.A";
                }
                if (txtMeal.Text.Trim() != null && txtMeal.Text.Trim() != "")
                {
                    meal = txtMeal.Text.Trim();
                }
                else
                {
                    meal = "N.A";
                }
                if (txtOptional.Text.Trim() != null && txtOptional.Text.Trim() != "")
                {
                    oth = txtOptional.Text.Trim();
                }
                else
                {
                    oth = "N.A";
                }
                if (txtSupp.Text.Trim() != null && txtSupp.Text.Trim() != "")
                {
                    daySupplierName = txtSupp.Text.Trim();
                }
                else
                {
                    daySupplierName = "N.A";
                }
                if (txtSuppEmail.Text.Trim() != null && txtSuppEmail.Text.Trim() != "")
                {
                    itnSupplierEmail = txtSuppEmail.Text.Trim();
                }
                else
                {
                    itnSupplierEmail = "N.A";
                }

                itinerary = string.Empty;
                itinerary = day + "#&#" + desc + "#&#" + meal + "#&#" + oth + "#&#" + daySupplierName + "#&#" + itnSupplierEmail + "#&#";

                switch (a)
                {
                    case 1:

                        holidayPackage.Itinerary1 = itinerary;
                        break;

                    case 2:
                        holidayPackage.Itinerary2 = itinerary;
                        break;

                    case 3:
                        holidayPackage.Itinerary3 = itinerary;
                        break;

                    case 4:
                        holidayPackage.Itinerary4 = itinerary;
                        break;

                    case 5:
                        holidayPackage.Itinerary5 = itinerary;
                        break;

                    case 6:
                        holidayPackage.Itinerary6 = itinerary;
                        break;

                    case 7:
                        holidayPackage.Itinerary7 = itinerary;
                        break;

                    case 8:
                        holidayPackage.Itinerary8 = itinerary;
                        break;

                    case 9:
                        holidayPackage.Itinerary9 = itinerary;
                        break;

                    case 10:
                        holidayPackage.Itinerary10 = itinerary;
                        break;

                    case 11:
                        holidayPackage.Itinerary11 = itinerary;
                        break;
                    case 12:
                        holidayPackage.Itinerary12 = itinerary;
                        break;
                    case 13:
                        holidayPackage.Itinerary13 = itinerary;
                        break;
                }
            }

            int roomCurrent = 0;// Convert.ToInt32(Request["roomCurrent"]);
            int roomCount = 1;// Convert.ToInt32(Request["roomCount"]);
            CT.HolidayDeals.HolidayPackageSeason season;
            if (holidayPackage.DealId <= 0)
            {
                holidayPackage.HotelSeasonList = new List<CT.HolidayDeals.HolidayPackageSeason>();
            }
            for (int i = 0; i < roomCount; i++)
            {
                if (holidayPackage.DealId <= 0)
                {
                    season = new CT.HolidayDeals.HolidayPackageSeason();
                }
                else
                {
                    season = holidayPackage.HotelSeasonList[i];
                }

                season.StartDate = DateTime.Now;
                season.EndDate = DateTime.Now;

                //season.HotelName = Convert.ToString(Request["hotel-" + i]);
                season.HotelName = string.Empty;
                //season.Star = (HolidayPackageHotelRating)Convert.ToInt32(Request["star-" + i]);
                season.Star = (HolidayPackageHotelRating)1;
                //season.SinglePrice = Convert.ToDecimal(Request["single-" + i]);
                season.SinglePrice = 0;

                //added on 26/05/2016

                season.SupplierCurrency = ddlSupplierCurrency.SelectedItem.Text;
                season.TwinSharingPrice = Convert.ToDecimal(txtStartFrom.Text.Trim());

                //season.TripleSharingPrice = Convert.ToDecimal(Request["triple-" + i]);
                season.TripleSharingPrice = 0;
                //season.ChildWithBedPrice = Convert.ToDecimal(Request["childBed-" + i]);
                //season.ChildWithoutBedPrice = Convert.ToDecimal(Request["childNoBed-" + i]);

                season.ChildWithBedPrice = 0; // No need of it as we are only maintainiing price of hotel with twin field
                season.ChildWithoutBedPrice = 0; // No need of it 

                //season.HotelDescription = Convert.ToString(Request["hotelDescription-" + i]);
                //season.HotelSupplierName = Convert.ToString(Request["hotelSupplierName-" + i]);
                //season.HotelSupplierEmail = Convert.ToString(Request["hotelSupplierEmail-" + i]);
                season.HotelDescription = string.Empty;
                season.HotelSupplierName = string.Empty;
                season.HotelSupplierEmail = string.Empty;
                //if (Request["seasonsId-" + i] != null)
                //{
                //    if (Convert.ToInt32(Request["seasonsId-" + i]) > 0)
                //    {
                //        season.SeasonId = Convert.ToInt32(Request["seasonsId-" + i]);
                //    }
                //}
                holidayPackage.HotelSeasonList.Add(season);
            }
            if (roomCurrent != roomCount)
            {
                //for (int i = roomCurrent; i < roomCount; i++)
                //{
                //    if (Request["seasonsId-" + i] != null)
                //    {
                //        if (Convert.ToInt32(Request["seasonsId-" + i]) > 0)
                //        {
                //            seasonIdToDelete += Request["seasonsId-" + i] + ",";
                //        }
                //    }
                //}
                if (seasonIdToDelete != string.Empty)
                {
                    seasonIdToDelete = seasonIdToDelete.Substring(0, seasonIdToDelete.Length - 1);
                }
            }
            int excCounter = Convert.ToInt32(Request["ExclusionsCurrent"]);
            string exclusions = string.Empty;
            for (int i = 1; i < excCounter; i++)
            {
                exclusions += Request["ExclusionsText-" + i];
                exclusions += "#&#";
            }
            holidayPackage.Notes = exclusions;
            int priceCounter = Convert.ToInt32(Request["PriceIncludeCurrent"]);
            string priceExclusions = string.Empty;
            for (int i = 1; i < priceCounter; i++)
            {
                priceExclusions += Request["PriceIncludeText-" + i];
                priceExclusions += "#&#";
            }
            holidayPackage.PriceExclude = priceExclusions;

            int termsCounter = Convert.ToInt32(Request["T&CCurrent"]);
            string terms = string.Empty;
            for (int i = 1; i < termsCounter; i++)
            {
                terms += Request["T&CText-" + i];
                terms += "#&#";
            }
            holidayPackage.TermsAndConditions = terms;
            holidayPackage.RoomRates = txtRoomRates.Text.Trim();
            bool saveMainImg = false;
            bool saveSideImg = false;
            bool saveThumbImg = false;
            //added for MainImage1 & 2
            bool saveMainImg1 = false;
            bool saveMainImg2 = false;
            if (dealId > 0 || chkCopy.Checked)
            {
                if (hdfAgencyImagePath.Value.Length > 0)
                {
                    if (holidayPackage.MainImagePath != lblUpload1.Text)
                    {
                        saveMainImg = true;
                        holidayPackage.MainImagePath = lblUpload1.Text;
                    }
                    else if (chkCopy.Checked)
                    {
                        CopyFiles(holidayPackage.MainImagePath, hdfAgencyImagePath.Value);
                    }
                    if (holidayPackage.ImagePath != lblUplaod2.Text)
                    {
                        saveSideImg = true;
                        holidayPackage.ImagePath = lblUplaod2.Text;
                    }
                    else if (chkCopy.Checked)
                    {
                        CopyFiles(holidayPackage.ImagePath, hdfAgencyImagePath.Value);
                    }
                    if (holidayPackage.ThumbnailPath != lblUpload3.Text)
                    {
                        saveThumbImg = true;
                        holidayPackage.ThumbnailPath = lblUpload3.Text;
                    }
                    else if (chkCopy.Checked)
                    {
                        CopyFiles(holidayPackage.ThumbnailPath, hdfAgencyImagePath.Value);
                    }

                    //addded for Main Image1 and Image 2
                    if (holidayPackage.MainImage1Path != lblUploadMainImg1.Text)
                    {
                        saveMainImg1 = true;
                        holidayPackage.MainImage1Path = lblUploadMainImg1.Text;
                    }
                    else if (chkCopy.Checked)
                    {
                        CopyFiles(holidayPackage.MainImage1Path, hdfAgencyImagePath.Value);
                    }

                    if (holidayPackage.MainImage2Path != lblUploadMainImg2.Text)
                    {
                        saveMainImg2 = true;
                        holidayPackage.MainImage2Path = lblUploadMainImg2.Text;
                    }
                    else if (chkCopy.Checked)
                    {
                        CopyFiles(holidayPackage.MainImage2Path, hdfAgencyImagePath.Value);
                    }
                }
                else
                {
                    lblError.Text = "Package Image path is not configured for the Copy Agent.";
                    lblError.Attributes.Add("style", "color:red");
                    throw new Exception();
                }
                //if (lbUploadMsg.Text == string.Empty || lbUpload2Msg.Text == string.Empty || lbUpload3Msg.Text == string.Empty)
                //{
                //    lblError.Text = "Please Upload All Images.";
                //    lblError.Attributes.Add("style", "color:red");
                //    throw new Exception();
                //}
            }
            else
            {
                holidayPackage.MainImagePath = lblUpload1.Text;
                holidayPackage.ImagePath = lblUplaod2.Text;
                holidayPackage.ThumbnailPath = lblUpload3.Text;

                //added on 02032016
                holidayPackage.MainImage1Path = lblUploadMainImg1.Text;
                holidayPackage.MainImage2Path = lblUploadMainImg2.Text;
            }

            int themeCount = chkTheme.Items.Count;
            List<int> themeIdList = new List<int>();
            for (int i = 0; i < chkTheme.Items.Count; i++)
            {
                if (chkTheme.Items[i].Selected)
                {
                    themeIdList.Add(Convert.ToInt32(chkTheme.Items[i].Value));
                }
            }

            //if (!imageUploadError)
            //{
            int hotelDealId = 0;
            int themeAdded = 1;
            //commented by Vijesh
            //using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
            //{
            holidayPackage.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
            holidayPackage.LastModifiedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
            if (Convert.ToUInt16(ddlAgent.SelectedValue) > 0) //To be check
            {
                try
                {

                    hotelDealId = holidayPackage.Save();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HotelCMS, Severity.High, (int)Settings.LoginInfo.UserID, "Execption : " + ex.ToString(), "");
                }
            }
            if (dealId <= 0)
            {
                if (hotelDealId > 0)
                {
                    themeAdded = PackageTheme.AddThemes(hotelDealId, themeIdList);
                }
                else
                {
                    themeAdded = 0;
                }
            }
            else
            {
                themeAdded = PackageTheme.AddThemes(dealId, themeIdList);
            }
            int rowMainImg = 1;
            int rowSideImg = 1;
            int rowThumbImg = 1;

            //added on 02032016 for Main Image1 and Main Image2        
            int rowMainImg1 = 1;
            int rowMainImg2 = 1;
            int deleteSeasonIdReturnValue = 1;
            if (dealId > 0)
            {
                if (saveMainImg)
                {
                    rowMainImg = CT.HolidayDeals.HolidayPackage.UpdateMainImagePath(holidayPackage.MainImagePath, holidayPackage.DealId);
                }
                if (saveSideImg)
                {
                    rowSideImg = CT.HolidayDeals.HolidayPackage.UpdateSideImagePath(holidayPackage.ImagePath, holidayPackage.DealId);
                }
                if (saveThumbImg)
                {
                    rowThumbImg = CT.HolidayDeals.HolidayPackage.UpdateThumbnailImagePath(holidayPackage.ThumbnailPath, holidayPackage.DealId);
                }

                //added on 02032016 for Main Image1 and Main image2
                if (saveMainImg1)
                {
                    rowMainImg1 = CT.HolidayDeals.HolidayPackage.UpdateMainImage1Path(holidayPackage.MainImage1Path, holidayPackage.DealId);
                }
                if (saveMainImg2)
                {
                    rowMainImg2 = CT.HolidayDeals.HolidayPackage.UpdateMainImage2Path(holidayPackage.MainImage2Path, holidayPackage.DealId);
                }
            }
            if (seasonIdToDelete != string.Empty)
            {
                deleteSeasonIdReturnValue = CT.HolidayDeals.HolidayPackageSeason.Delete(seasonIdToDelete);
            }
            if ((hotelDealId > 0) && (rowMainImg > 0) && (rowSideImg > 0) && (rowThumbImg > 0) && (deleteSeasonIdReturnValue > 0) && (themeAdded > 0))
            {
                // updateTransaction.Complete(); commented by vijesh
            }
            else
            {
                hotelDealId = 0;
            }
            //}commented by vijesh
            if (hotelDealId > 0)
            {
                if (CurrentObject != null)
                {
                    if (!chkCopy.Checked)
                    {
                        DeleteOldImages();
                        //Response.Redirect("HotelDealSetting.aspx?memberId=" + member.ID + "&agencyId=" + holidayPackage.AgencyId, false);
                        Response.Redirect("HotelDealSetting.aspx?agencyId=" + holidayPackage.AgencyId, false);
                    }
                    else
                    {
                        Clear();
                        //Response.Redirect("HolidayDealPackage.aspx", false);
                        lblSucess.Visible = true;
                        //lblSucess.Text = "Your package deal was successfully saved. Kindly activate it to show it to customers.";
                        lblSucess.Text = "Your package deal was successfully saved.";
                        InitializePageControls();
                        
                        DeleteOldImages();
                    }
                }
                else
                {
                    Clear();
                    lblSucess.Visible = true;
                    //lblSucess.Text = "Your package deal was successfully saved. Kindly activate it to show it to customers.";
                    lblSucess.Text = "Your package deal was successfully saved.";
                    InitializePageControls();
                       
                    DeleteOldImages();
                }
            }
            else
            {
                lblError.Text = "Unable to save your package deal. Please try again later.";
                lblError.Visible = true;
            }
            //CurrentObject = null;
            chkCopy.Enabled = true;

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.HotelCMS, Severity.High, 0, "Error: Exception during save of hotel package deal. | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
            lblError.Visible = true;
        }

    }

    private void BindItinerary()
    {
        try
        {
            if (ddlNights.SelectedIndex > 0)
            {

                for (int i = 0; i < Convert.ToInt32(ddlNights.SelectedItem.Value) + 1; i++)
                {


                    HtmlTableRow hr1 = new HtmlTableRow();
                    hr1.Height = "25px";
                    HtmlTableCell tc11 = new HtmlTableCell();
                    tc11.Width = "150px";
                    Label lblMandatory1 = new Label();
                    lblMandatory1.ForeColor = System.Drawing.Color.Red;
                    lblMandatory1.Text = "*";
                    Label lblDay = new Label();
                    lblDay.ID = "lblDay_" + i.ToString();
                    lblDay.Text = "Day :" + (i + 1).ToString();

                    lblDay.Font.Bold = true;
                    lblDay.Width = new Unit(45, UnitType.Pixel);
                    HtmlTableCell tc12 = new HtmlTableCell();
                    tc12.Width = "521px";
                    TextBox txtDay = new TextBox();
                    txtDay.ID = "txtDay_" + i.ToString();
                    txtDay.Width = new Unit(300, UnitType.Pixel);
                    tc11.Controls.Add(lblDay);
                    tc11.Controls.Add(lblMandatory1);
                    tc12.Controls.Add(txtDay);

                    HtmlTableRow hr2 = new HtmlTableRow();
                    hr2.Height = "80px";
                    HtmlTableCell tc21 = new HtmlTableCell();
                    Label lblMandatory2 = new Label();
                    lblMandatory2.ForeColor = System.Drawing.Color.Red;
                    lblMandatory2.Text = "*";
                    Label lblDesc = new Label();
                    lblDesc.ID = "lblDesc_" + i.ToString();
                    lblDesc.Text = "Description :";
                    lblDesc.Font.Bold = true;
                    lblDesc.Width = new Unit(80, UnitType.Pixel);
                    HtmlTableCell tc22 = new HtmlTableCell();
                    TextBox txtDesc = new TextBox();
                    txtDesc.ID = "txtDesc_" + i.ToString();
                    txtDesc.Width = new Unit(450, UnitType.Pixel);
                    txtDesc.Height = new Unit(90, UnitType.Pixel);
                    txtDesc.TextMode = TextBoxMode.MultiLine;
                    tc21.Controls.Add(lblDesc);
                    tc21.Controls.Add(lblMandatory2);
                    tc22.Controls.Add(txtDesc);

                    HtmlTableRow hr3 = new HtmlTableRow();
                    hr3.Height = "25px";
                    HtmlTableCell tc31 = new HtmlTableCell();
                    Label lblMandatory = new Label();
                    lblMandatory.ForeColor = System.Drawing.Color.Red;
                    lblMandatory.Text = "*";
                    Label lblMeals = new Label();
                    lblMeals.ID = "lblMeals_" + i.ToString();
                    lblMeals.Text = "Meals :";
                    lblMeals.Font.Bold = true;
                    lblMeals.Width = new Unit(45, UnitType.Pixel);
                    HtmlTableCell tc32 = new HtmlTableCell();
                    TextBox txtMeals = new TextBox();
                    txtMeals.ID = "txtMeals_" + i.ToString();
                    txtMeals.Width = new Unit(300, UnitType.Pixel);
                    tc31.Controls.Add(lblMeals);
                    tc31.Controls.Add(lblMandatory);
                    tc32.Controls.Add(txtMeals);

                    HtmlTableRow hr4 = new HtmlTableRow();
                    hr4.Height = "25px";
                    HtmlTableCell tc41 = new HtmlTableCell();
                    Label lblOptional = new Label();
                    lblOptional.ID = "lblOptional_" + i.ToString();
                    lblOptional.Text = "Optional :";
                    lblOptional.Font.Bold = true;
                    lblOptional.Width = new Unit(120, UnitType.Pixel);
                    HtmlTableCell tc42 = new HtmlTableCell();
                    TextBox txtOptional = new TextBox();
                    txtOptional.ID = "txtOptional_" + i.ToString();
                    txtOptional.Width = new Unit(300, UnitType.Pixel);
                    tc41.Controls.Add(lblOptional);
                    tc42.Controls.Add(txtOptional);

                    HtmlTableRow hr5 = new HtmlTableRow();
                    hr5.Height = "25px";
                    HtmlTableCell tc51 = new HtmlTableCell();
                    Label lblSuppName = new Label();
                    lblSuppName.ID = "lblSuppName_" + i.ToString();
                    lblSuppName.Text = "Supplier Name :";
                    lblSuppName.Font.Bold = true;
                    lblSuppName.Width = new Unit(120, UnitType.Pixel);
                    HtmlTableCell tc52 = new HtmlTableCell();
                    TextBox txtSuppName = new TextBox();
                    txtSuppName.ID = "txtSuppName_" + i.ToString();
                    txtSuppName.Width = new Unit(300, UnitType.Pixel);
                    tc51.Controls.Add(lblSuppName);
                    tc52.Controls.Add(txtSuppName);

                    HtmlTableRow hr6 = new HtmlTableRow();
                    hr6.Height = "25px";
                    HtmlTableCell tc61 = new HtmlTableCell();
                    Label lblSuppNameEmail = new Label();
                    lblSuppNameEmail.ID = "lblSuppNameEmail_" + i.ToString();
                    lblSuppNameEmail.Text = "Supplier Email :";
                    lblSuppNameEmail.Font.Bold = true;
                    lblSuppNameEmail.Width = new Unit(120, UnitType.Pixel);
                    HtmlTableCell tc62 = new HtmlTableCell();
                    TextBox txtSuppNameEmail = new TextBox();
                    txtSuppNameEmail.ID = "txtSuppNameEmail_" + i.ToString();
                    txtSuppNameEmail.Width = new Unit(300, UnitType.Pixel);
                    tc61.Controls.Add(lblSuppNameEmail);
                    tc62.Controls.Add(txtSuppNameEmail);

                    HtmlTableRow hr7 = new HtmlTableRow();
                    HtmlTableCell line = new HtmlTableCell();
                    line.ColSpan = 5;
                    line.Height = "10px";
                    line.InnerHtml = "<p class=\"fleft width-100\" style=\"padding-top:10px; margin-top:10px; border-top:solid 1px #aaa;\">";


                    hr1.Cells.Add(tc11);
                    hr1.Cells.Add(tc12);

                    hr2.Cells.Add(tc21);
                    hr2.Cells.Add(tc22);

                    hr3.Cells.Add(tc31);
                    hr3.Cells.Add(tc32);

                    hr4.Cells.Add(tc41);
                    hr4.Cells.Add(tc42);

                    hr5.Cells.Add(tc51);
                    hr5.Cells.Add(tc52);

                    hr6.Cells.Add(tc61);
                    hr6.Cells.Add(tc62);
                    hr7.Cells.Add(line);

                    tblitinerary.Rows.Add(hr1);
                    tblitinerary.Rows.Add(hr2);
                    tblitinerary.Rows.Add(hr3);
                    tblitinerary.Rows.Add(hr4);
                    tblitinerary.Rows.Add(hr5);
                    tblitinerary.Rows.Add(hr6);
                }

            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.HotelCMS, Severity.High, 0, "Error: Bind Itinerary. | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
        }
    }

    protected void btnUpload3_Click(object sender, EventArgs e)
    {
        try
        {
            if (fuImage3.HasFile == true)
            {
                if (!agencyCheck())
                {
                    lblError.Visible = true;
                    string script = "showCopyControls();";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "showCopyControls", script, true);
                   
                }
                else if (fuImage3.PostedFile.ContentLength < 2097152)
                {
                    
                    string fileExtension = Path.GetExtension(fuImage3.FileName);
                    string filename = DateTime.Now.ToString("ddMMyyyyHHmmssms");
                    //commented temp by vijesh
                   // string folderName =Convert.ToString(CT.Configuration.ConfigurationSystem.HolidayDealConfig["hotelDealImagePath"]);
                    string folderName = hdfAgencyImagePath.Value;
                    if (!Directory.Exists(folderName)) Directory.CreateDirectory(folderName);
                    string imagePath = folderName + Convert.ToString(filename + "_T") + fileExtension;
                    fuImage3.SaveAs(imagePath);
                    lblUpload3.Text = Convert.ToString(filename + "_T")  + fileExtension;
                    lbUpload3Msg.Text = " Thumbnail Image uploaded.";
                    hdfImage3.Value = imagePath;
                    hdfImage3Extn.Value = fileExtension;
                    hdfUploadYNImage3.Value = "1";
                }
                else
                {
                    lblError.Text =  "Please Upload thumbnail image file less than 2 MB size";
                    lblError.Visible = true;
                    
                }
            }
        }
        catch
        {
            throw;
        }
    }
    protected void lbUpload3Msg_Click(object sender, EventArgs e)
    {
        try
        {
            string imgPath = hdfImage3.Value;            
            img3PreView.ImageUrl = "ImageShow.aspx?FilePath=" + imgPath;

            string script = "showImage('S','3');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "showImage", script, true);
        }
        catch { throw; }

    }

    protected void btnUpload2_Click(object sender, EventArgs e)
    {
        try
        {
            if (fuImage2.HasFile == true)
            {
                if (!agencyCheck())
                {
                    lblError.Visible = true;
                    string script = "showCopyControls();";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "showCopyControls", script, true);
                }
                else if (fuImage2.PostedFile.ContentLength < 2097152)
                {


                    string fileExtension = Path.GetExtension(fuImage2.FileName);
                    string filename = DateTime.Now.ToString("ddMMyyyyHHmmssms");
                    //commented temp by vijesh
                    //string folderName = Convert.ToString(CT.Configuration.ConfigurationSystem.HolidayDealConfig["hotelDealImagePath"]);
                    string folderName = hdfAgencyImagePath.Value;
                    if (!Directory.Exists(folderName)) Directory.CreateDirectory(folderName);
                    string imagePath = folderName + Convert.ToString(filename + "_L")  + fileExtension;
                    fuImage2.SaveAs(imagePath);
                    lblUplaod2.Text = Convert.ToString(filename + "_L") + fileExtension;
                    lbUpload2Msg.Text = " Side Image uploaded.";
                    hdfImage2.Value = imagePath;
                    hdfImage2Extn.Value = fileExtension;
                    hdfUploadYNImage2.Value = "1";
                }
                else
                {
                    lblError.Text =  "Please Upload side image file less than 2 MB size";
                    lblError.Visible = true;
                }
            }
        }
        catch
        {
            throw;
        }
    }
    protected void lbUpload2Msg_Click(object sender, EventArgs e)
    {
        try
        {
            string imgPath = hdfImage2.Value;
            img2PreView.ImageUrl = "ImageShow.aspx?FilePath=" + imgPath;

            string script = "showImage('S','2');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "showImage", script, true);
        }
        catch { throw; }

    }

    protected void btnUpload1_Click(object sender, EventArgs e)
    {
        try
        {
            if (fuImage1.HasFile)
            {
                if (!agencyCheck())
                {
                    lblError.Visible = true;
                    string script = "showCopyControls();";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "showCopyControls", script, true);
                  
                }
                else if (fuImage1.PostedFile.ContentLength < 2097152)
                {
                    string fileExtension = Path.GetExtension(fuImage1.FileName);
                    string filename = DateTime.Now.ToString("ddMMyyyyHHmmssms");
                    //commented temp by vijesh
                    //string folderName = Convert.ToString(CT.Configuration.ConfigurationSystem.HolidayDealConfig["hotelDealImagePath"]);
                    string folderName=hdfAgencyImagePath.Value;
                    if (!Directory.Exists(folderName)) Directory.CreateDirectory(folderName);
                    string imagePath = folderName + Convert.ToString(filename + "_M")+ fileExtension;
                    fuImage1.SaveAs(imagePath); 
                    lblUpload1.Text = Convert.ToString(filename + "_M") + fileExtension;
                    lbUploadMsg.Text = " Main Image uploaded.";
                    hdfImage1.Value = imagePath;
                    hdfImage1Extn.Value = fileExtension;
                    hdfUploadYNImage1.Value = "1";
                    //imgupload1.ImageUrl = imagePath ;// +"?" + DateTime.Now.ToString("ddyyhhmmss"); ;
            
                    
                }
                else
                {
                    lblError.Text = "Please enter valid file Format for main image!";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Please Upload the file";
                lblError.Visible = true;
            }

        }
        catch
        {

            throw;
        }

    }
    protected void lbUploadMsg_Click(object sender, EventArgs e)
    {
        try
        {
            string imgPath = hdfImage1.Value;            
            img1PreView.ImageUrl = "ImageShow.aspx?FilePath=" + imgPath;

            string script = "showImage('S','1');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "showImage", script, true);
        }
        catch { throw; }

    }


    // added for Main image 1 on 02032016
    protected void btnMainImg1_Click(object sender, EventArgs e) 
    {
        try
        {
            if (mainImage1.HasFile)
            {
                if (!agencyCheck())
                {
                    lblError.Visible = true;
                    string script = "showCopyControls();";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "showCopyControls", script, true);
                }
                else if (mainImage1.PostedFile.ContentLength < 2097152)
                {
                    string fileExtension = Path.GetExtension(mainImage1.FileName);
                    string filename = DateTime.Now.ToString("ddMMyyyyHHmmssms");
                    string folderName = hdfAgencyImagePath.Value;
                    if (!Directory.Exists(folderName)) Directory.CreateDirectory(folderName);
                    string imagePath = folderName + Convert.ToString(filename + "_M1") + fileExtension; //M1 is for mainImage1
                    mainImage1.SaveAs(imagePath);
                    lblUploadMainImg1.Text = Convert.ToString(filename + "_M1") + fileExtension;
                    lbUploadMainImg1.Text = " Main Image1 uploaded."; //@@@@ to be check
                    hdfImage4.Value = imagePath;
                    hdfImage4Extn.Value = fileExtension;
                    hdfUploadYNImage4.Value = "1";
                }
                else
                {
                    lblError.Text = "Please enter valid file Format for main image1!";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Please Upload the file";
                lblError.Visible = true;
            }

        }
        catch
        {

            throw;
        }
    }
    protected void lbUploadMainImg1_Click(object sender, EventArgs e)
    {
        try
        {
            string imgPath = hdfImage4.Value;
            img4PreView.ImageUrl = "ImageShow.aspx?FilePath=" + imgPath;

            string script = "showImage('S','4');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "showImage", script, true);
        }
        catch { throw; }

    }

    // added for Main image 2 on 02032016
    protected void btnMainImg2_Click(object sender, EventArgs e) 
    {
        try
        {
            if (mainImage2.HasFile)
            {
                if (!agencyCheck())
                {
                    lblError.Visible = true;
                    string script = "showCopyControls();";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "showCopyControls", script, true);
                }
                else if (mainImage2.PostedFile.ContentLength < 2097152)
                {
                    string fileExtension = Path.GetExtension(mainImage2.FileName);
                    string filename = DateTime.Now.ToString("ddMMyyyyHHmmssms");
                    string folderName = hdfAgencyImagePath.Value;
                    if (!Directory.Exists(folderName)) Directory.CreateDirectory(folderName);
                    string imagePath = folderName + Convert.ToString(filename + "_M2") + fileExtension; //M2 is for mainImage2
                    mainImage2.SaveAs(imagePath);
                    lblUploadMainImg2.Text = Convert.ToString(filename + "_M2") + fileExtension;
                    lbUploadMainImg2.Text = " Main Image2 uploaded."; //@@@@ to be check
                    hdfImage5.Value = imagePath;
                    hdfImage5Extn.Value = fileExtension;
                    hdfUploadYNImage5.Value = "1";
                }
                else
                {
                    lblError.Text = "Please enter valid file Format for main image2!";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Please Upload the file";
                lblError.Visible = true;
            }

        }
        catch
        {

            throw;
        }
    }
    protected void lbUploadMainImg2_Click(object sender, EventArgs e)
    {
        try
        {
            string imgPath = hdfImage5.Value;
            img5PreView.ImageUrl = "ImageShow.aspx?FilePath=" + imgPath;

            string script = "showImage('S','5');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "showImage", script, true);
        }
        catch { throw; }

    }

    private void LoadPackage(int dealId)
    {
        try
        {
            try
            {
                choosenThemeList = CT.HolidayDeals.DealThemeIndex.Load(dealId);
            }
            catch (Exception ex)
            {
                choosenThemeList = new List<int>();
                Audit.Add(EventType.HotelCMS, Severity.Low, 0, "Error: Exception during Load() in Theme.cs,DealThemeIndex for dealId= " + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
            }
            package = CT.HolidayDeals.HolidayPackage.LoadHotelDealForEdit(dealId);
            BindAgent();
            ddlAgent.SelectedValue = Convert.ToString(package.AgencyId);
            //@@@
            hdfAgencyImagePath.Value = CT.HolidayDeals.HolidayPackage.GetAgencyImagePath(package.AgencyId, "P").ToString(); 
           // hdfAgencyImagePath.Value = Server.MapPath("~/images/PackageImages/");  
            BindPackage(Convert.ToInt32(ddlAgent.SelectedItem.Value));
            BindAgentCopy(ddlAgent.SelectedItem.Value);
            CurrentObject = package;
            txtPackage.Text = package.DealName;
            txtCity.Text = package.City;
            txtCountry.Text = package.Country;
            BindNights();
            ddlNights.SelectedValue = Convert.ToString(package.Nights);
            BindCurrency(Convert.ToInt32(ddlAgent.SelectedValue));
            //string folderName = Convert.ToString(CT.Configuration.ConfigurationSystem.HolidayDealConfig["hotelDealImagePath"]) + agency.ID.ToString()+"/";

            lblUpload1.Text = package.MainImagePath;
            hdfImage1Extn.Value = Path.GetExtension(package.MainImagePath);
//            hdfImage1.Value = folderName + lblUpload1.Text;
            hdfImage1.Value = hdfAgencyImagePath.Value + lblUpload1.Text;
            hdfOldImage1.Value = hdfAgencyImagePath.Value + lblUpload1.Text;
            if (!string.IsNullOrEmpty(package.MainImagePath) && !string.IsNullOrEmpty(hdfImage1Extn.Value) && File.Exists(hdfOldImage1.Value))
            {
                lbUploadMsg.Text = " Main Image uploaded.";
                // lbUploadMsg.OnClientClick = "return showimag1('dUpload1Msg','imgupload1','" + hdfImage1.ClientID + "')";
                hdfUploadYNImage1.Value = "1";
            }
            else
            {
                hdfUploadYNImage1.Value = "0";
            }


            lblUplaod2.Text = package.ImagePath;
            hdfImage2Extn.Value = Path.GetExtension(package.ImagePath);
//            hdfImage2.Value = folderName + lblUplaod2.Text;
            hdfImage2.Value = hdfAgencyImagePath.Value + lblUplaod2.Text;
            hdfOldImage2.Value = hdfAgencyImagePath.Value + lblUplaod2.Text;
            if (!string.IsNullOrEmpty(package.ImagePath) && !string.IsNullOrEmpty(hdfImage2Extn.Value) && File.Exists(hdfOldImage2.Value))
            {
                lbUpload2Msg.Text = " Side Image uploaded.";
                //lbUpload2Msg.OnClientClick = "return showimag1('dUpload2Msg','imgupload2','" + hdfImage2.ClientID + "')";
                hdfUploadYNImage2.Value = "1";
            }
            else
            {
                hdfUploadYNImage2.Value = "0";
            }
           


            lblUpload3.Text = package.ThumbnailPath;
            hdfImage3Extn.Value = Path.GetExtension(package.ThumbnailPath);
            //hdfImage3.Value = folderName + lblUpload3.Text;
            hdfImage3.Value = hdfAgencyImagePath.Value + lblUpload3.Text;
            hdfOldImage3.Value = hdfAgencyImagePath.Value + lblUpload3.Text;
            if (!string.IsNullOrEmpty(package.ThumbnailPath) && !string.IsNullOrEmpty(hdfImage3Extn.Value) && File.Exists(hdfOldImage3.Value))
            {
                lbUpload3Msg.Text = " Thumbnail Image uploaded.";
                //lbUpload3Msg.OnClientClick = "return showimag1('dUpload3Msg','imgupload3','" + hdfImage3.ClientID + "')";
                hdfUploadYNImage3.Value = "1";
            }
            else
            {
                hdfUploadYNImage3.Value = "0";
            }
           


            //Added code for Main Image1 on 02032016
            lblUploadMainImg1.Text = package.MainImage1Path;
            hdfImage4Extn.Value = Path.GetExtension(package.MainImage1Path);
            hdfImage4.Value = hdfAgencyImagePath.Value + lblUploadMainImg1.Text;
            hdfOldImage4.Value = hdfAgencyImagePath.Value + lblUploadMainImg1.Text;
            if (!string.IsNullOrEmpty(package.MainImage1Path) && !string.IsNullOrEmpty(hdfImage4Extn.Value) && File.Exists(hdfOldImage4.Value))
            {
                lbUploadMainImg1.Text = " Main Image1 uploaded.";
                hdfUploadYNImage4.Value = "1";
            }
            else
            {
                hdfUploadYNImage4.Value = "0";
            }

           


            //Added code for Main Image2 on 02032016
            lblUploadMainImg2.Text = package.MainImage2Path;
            hdfImage5Extn.Value = Path.GetExtension(package.MainImage2Path);
            hdfImage5.Value = hdfAgencyImagePath.Value + lblUploadMainImg2.Text;
            hdfOldImage5.Value = hdfAgencyImagePath.Value + lblUploadMainImg2.Text;
            if (!string.IsNullOrEmpty(package.MainImage2Path) && !string.IsNullOrEmpty(hdfImage5Extn.Value) && File.Exists(hdfOldImage5.Value))
            {
                lbUploadMainImg2.Text = " Main Image2 uploaded.";
                hdfUploadYNImage5.Value = "1";
            }
            else
            {
                hdfUploadYNImage5.Value = "0";
            }
           

            for (int i = 0; i < chkPackageIncludes.Items.Count; i++)
            {
                if (chkPackageIncludes.Items[i].Value == "AirFare")
                {
                    if (package.HasAirfare == true)
                    {
                        chkPackageIncludes.Items[i].Selected = true;
                    }
                }
                else if (chkPackageIncludes.Items[i].Value == "Hotel")
                {
                    if (package.HasHotel == true)
                    {
                        chkPackageIncludes.Items[i].Selected = true;
                    }
                }
                else if (chkPackageIncludes.Items[i].Value == "SightSeeing")
                {
                    if (package.HasSightSeeing == true)
                    {
                        chkPackageIncludes.Items[i].Selected = true;
                    }
                }
                else if (chkPackageIncludes.Items[i].Value == "Meals")
                {
                    if (package.HasMeals == true)
                    {
                        chkPackageIncludes.Items[i].Selected = true;
                    }
                }
                else if (chkPackageIncludes.Items[i].Value == "Transport")
                {
                    if (package.HasTransport == true)
                    {
                        chkPackageIncludes.Items[i].Selected = true;
                    }
                }
            }
            txtDescription.Text = package.Description;
            txtOverView.Text = package.Overview;
            if(package.TourType!=string.Empty)
            {
                ddlTourType.SelectedValue = package.TourType;
            }
            Getthemes();
            for(int i = 0; i < choosenThemeList.Count; i++)
            {
                if (chkTheme.Items.FindByValue(choosenThemeList[i].ToString()) != null)
                {
                    chkTheme.Items.FindByValue(choosenThemeList[i].ToString()).Selected = true;
                }
            }

            if (package.Inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
            {
                hdfinclCounter.Value = Convert.ToString(package.Inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
            }
            else
            {
                hdfinclCounter.Value = "2";
            }
            if (package.Notes.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
            {
                hdfExclCounter.Value = Convert.ToString(package.Notes.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
            }
            else
            {
                hdfExclCounter.Value = "2";
            }

            if (package.PriceExclude.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
            {
                hdfpriceExcludeCounter.Value = Convert.ToString(package.PriceExclude.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
            }
            else
            {
                hdfpriceExcludeCounter.Value = "2";
            }

            if (package.TermsAndConditions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1 > 1)
            {
                hdftncCounter.Value = Convert.ToString(package.TermsAndConditions.Split(splitter, StringSplitOptions.RemoveEmptyEntries).Length + 1);
            }
            else
            {
                hdftncCounter.Value = "2";
            }

            hdfinclusion.Value = package.Inclusions;
            hdfExclusions.Value = package.Notes;
            hdfpriceExclude.Value = package.PriceExclude;
            hdftnc.Value = package.TermsAndConditions;
            txtRoomRates.Text = package.RoomRates;

            //added on 26/05/2016
            AgentMaster agent = new AgentMaster(Convert.ToInt32(ddlAgent.SelectedItem.Value));
            ddlSupplierCurrency.SelectedItem.Text = agent.AgentCurrency;
            txtStartFrom.Text = package.HotelSeasonList[0].TwinSharingPrice.ToString("N" + agent.DecimalValue);
            BindItinerary();
            for (int i = 0; i < Convert.ToInt32(ddlNights.SelectedItem.Value) + 1; i++)
            {
                TextBox txtDays = tblitinerary.Rows[i].Cells[0].FindControl("txtDay_" + i.ToString()) as TextBox;
                TextBox txtDesc = tblitinerary.Rows[i].Cells[0].FindControl("txtDesc_" + i.ToString()) as TextBox;
                TextBox txtMeal = tblitinerary.Rows[i].Cells[0].FindControl("txtMeals_" + i.ToString()) as TextBox;
                TextBox txtOptional = tblitinerary.Rows[i].Cells[0].FindControl("txtOptional_" + i.ToString()) as TextBox;
                TextBox txtSupp = tblitinerary.Rows[i].Cells[0].FindControl("txtSuppName_" + i.ToString()) as TextBox;
                TextBox txtSuppEmail = tblitinerary.Rows[i].Cells[0].FindControl("txtSuppNameEmail_" + i.ToString()) as TextBox;
                if (i == 0)
                {
                    if (package.Itinerary1 != string.Empty)
                    {
                        txtDays.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0], @"""|\\", "");
                        txtDesc.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1], @"""|\\", "");
                        txtMeal.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[2], @"""|\\", "");
                        txtOptional.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[3], @"""|\\", "");
                        txtSupp.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[4], @"""|\\", "");
                        txtSuppEmail.Text = package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[5];
                    }
                }
                if (i == 1)
                {
                    if (package.Itinerary2 != string.Empty)
                    {
                        txtDays.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary2.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0], @"""|\\", "");
                        txtDesc.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary2.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1], @"""|\\", "");
                        txtMeal.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary2.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[2], @"""|\\", "");
                        txtOptional.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary2.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[3], @"""|\\", "");
                        txtSupp.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary2.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[4], @"""|\\", "");
                        txtSuppEmail.Text = package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[5];
                    }
                }
                if (i == 2)
                {
                    if (package.Itinerary3 != string.Empty)
                    {
                        txtDays.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary3.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0], @"""|\\", "");
                        txtDesc.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary3.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1], @"""|\\", "");
                        txtMeal.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary3.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[2], @"""|\\", "");
                        txtOptional.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary3.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[3], @"""|\\", "");
                        txtSupp.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary3.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[4], @"""|\\", "");
                        txtSuppEmail.Text = package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[5];
                    }
                }
                if (i == 3)
                {
                    if (package.Itinerary4 != string.Empty)
                    {
                        txtDays.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary4.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0], @"""|\\", "");
                        txtDesc.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary4.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1], @"""|\\", "");
                        txtMeal.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary4.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[2], @"""|\\", "");
                        txtOptional.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary4.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[3], @"""|\\", "");
                        txtSupp.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary4.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[4], @"""|\\", "");
                        txtSuppEmail.Text = package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[5];
                    }
                }
                if (i == 4)
                {
                    if (package.Itinerary5 != string.Empty)
                    {
                        txtDays.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary5.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0], @"""|\\", "");
                        txtDesc.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary5.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1], @"""|\\", "");
                        txtMeal.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary5.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[2], @"""|\\", "");
                        txtOptional.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary5.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[3], @"""|\\", "");
                        txtSupp.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary5.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[4], @"""|\\", "");
                        txtSuppEmail.Text = package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[5];
                    }
                }
                if (i == 5)
                {
                    if (package.Itinerary6 != string.Empty)
                    {
                        txtDays.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary6.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0], @"""|\\", "");
                        txtDesc.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary6.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1], @"""|\\", "");
                        txtMeal.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary6.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[2], @"""|\\", "");
                        txtOptional.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary6.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[3], @"""|\\", "");
                        txtSupp.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary6.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[4], @"""|\\", "");
                        txtSuppEmail.Text = package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[5];
                    }
                }
                if (i == 6)
                {
                    if (package.Itinerary7 != string.Empty)
                    {
                        txtDays.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary7.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0], @"""|\\", "");
                        txtDesc.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary7.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1], @"""|\\", "");
                        txtMeal.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary7.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[2], @"""|\\", "");
                        txtOptional.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary7.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[3], @"""|\\", "");
                        txtSupp.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary7.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[4], @"""|\\", "");
                        txtSuppEmail.Text = package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[5];
                    }
                }
                if (i == 7)
                {
                    if (package.Itinerary8 != string.Empty)
                    {
                        txtDays.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary8.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0], @"""|\\", "");
                        txtDesc.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary8.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1], @"""|\\", "");
                        txtMeal.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary8.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[2], @"""|\\", "");
                        txtOptional.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary8.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[3], @"""|\\", "");
                        txtSupp.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary8.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[4], @"""|\\", "");
                        txtSuppEmail.Text = package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[5];
                    }
                }
                if (i == 8)
                {
                    if (package.Itinerary9 != string.Empty)
                    {
                        txtDays.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary9.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0], @"""|\\", "");
                        txtDesc.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary9.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1], @"""|\\", "");
                        txtMeal.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary9.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[2], @"""|\\", "");
                        txtOptional.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary9.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[3], @"""|\\", "");
                        txtSupp.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary9.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[4], @"""|\\", "");
                        txtSuppEmail.Text = package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[5];
                    }
                }
                if (i == 9)
                {
                    if (package.Itinerary10 != string.Empty)
                    {
                        txtDays.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary10.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0], @"""|\\", "");
                        txtDesc.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary10.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1], @"""|\\", "");
                        txtMeal.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary10.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[2], @"""|\\", "");
                        txtOptional.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary10.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[3], @"""|\\", "");
                        txtSupp.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary10.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[4], @"""|\\", "");
                        txtSuppEmail.Text = package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[5];
                    }
                }
                if (i == 10)
                {
                    if (package.Itinerary11 != string.Empty)
                    {
                        txtDays.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary11.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0], @"""|\\", "");
                        txtDesc.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary11.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1], @"""|\\", "");
                        txtMeal.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary11.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[2], @"""|\\", "");
                        txtOptional.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary11.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[3], @"""|\\", "");
                        txtSupp.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary11.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[4], @"""|\\", "");
                        txtSuppEmail.Text = package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[5];
                    }
                }
                if (i == 11)
                {
                    if (package.Itinerary12 != string.Empty)
                    {
                        txtDays.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary12.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0], @"""|\\", "");
                        txtDesc.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary12.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1], @"""|\\", "");
                        txtMeal.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary12.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[2], @"""|\\", "");
                        txtOptional.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary12.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[3], @"""|\\", "");
                        txtSupp.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary12.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[4], @"""|\\", "");
                        txtSuppEmail.Text = package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[5];
                    }
                }
                if (i == 12)
                {
                    if (package.Itinerary13 != string.Empty)
                    {
                        txtDays.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary13.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0], @"""|\\", "");
                        txtDesc.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary13.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1], @"""|\\", "");
                        txtMeal.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary13.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[2], @"""|\\", "");
                        txtOptional.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary13.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[3], @"""|\\", "");
                        txtSupp.Text = System.Text.RegularExpressions.Regex.Replace(package.Itinerary13.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[4], @"""|\\", "");
                        txtSuppEmail.Text = package.Itinerary1.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[5];
                    }
                }
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.HotelCMS, Severity.High, 0, "Error: Loadpackages mthod. | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.HotelCMS, Severity.High, 0, "Error: Exception during save of hotel package deal. | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
            lblError.Visible = true;
        }
        
    }

    private void BindAgent()
    {
        try
        {
            DataTable dtAgents;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT-ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            }
            else
            {
                dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            }

            //DataView dv = dtAgents.DefaultView;
            //if (Settings.LoginInfo.AgentType == AgentType.Agent)
            //{
            //    DataRowView drv = dv.AddNew();
            //    drv["AGENT_ID"] = Settings.LoginInfo.AgentId;
            //    drv["AGENT_CODE"] = "";
            //    drv["AGENT_NAME"] = Settings.LoginInfo.AgentName;
            //    drv["agent_type"] = (int)Settings.LoginInfo.AgentType;
            //    drv.EndEdit();
            //}
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("Select Agent", "-1"));
            ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            if (Convert.ToInt32(Request["dealId"]) < 0)
            {
                if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
                {
                    ddlAgent.Enabled = false;
                }
                else
                {
                    ddlAgent.Enabled = true;
                }
            }
           
          
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindAgentCopy(string agentid)
    {
        try
        {
            DataTable dtAgent;
            int agentType = AgentMaster.GetAgentType(Convert.ToInt32(agentid));
            if (agentType == (int)AgentType.BaseAgent)
            {
                dtAgent = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "ALL", Convert.ToInt32(agentid), ListStatus.Short, RecordStatus.Activated);
            }
            else if (agentType == (int)AgentType.Agent)
            {
                dtAgent = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT-ALL", Convert.ToInt32(agentid), ListStatus.Short, RecordStatus.Activated);
            }
            else
            {
                dtAgent = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Convert.ToInt32(agentid), ListStatus.Short, RecordStatus.Activated);
            }

            DataView dv = dtAgent.DefaultView;
            dv.RowFilter = "agent_id NOt IN('" + agentid + "')";
            ddlAgentCopy.DataSource = dv.ToTable();
            ddlAgentCopy.DataValueField = "agent_id";
            ddlAgentCopy.DataTextField = "agent_name";
            ddlAgentCopy.DataBind();
            ddlAgentCopy.Items.Insert(0, new ListItem("Select Copy Agent", "-1"));
            ddlAgentCopy.Enabled = false;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindPackage(int agentid)
    {
        try
        {
            ddlPackages.DataSource = CT.HolidayDeals.HolidayPackage.GetPackageList(agentid);
            ddlPackages.DataValueField = "dealId";
            ddlPackages.DataTextField = "dealName";
            ddlPackages.DataBind();
            ddlPackages.Items.Insert(0, new ListItem("Select Package", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void ddlPackages_SelectedIndexChanged(object sender, EventArgs e)
    {
        //clear all linkButton text for load new package info <02032016>
        lbUploadMsg.Text = string.Empty;
        lbUpload2Msg.Text = string.Empty;
        lbUpload3Msg.Text = string.Empty;
        lbUploadMainImg1.Text = string.Empty;
        lbUploadMainImg2.Text = string.Empty;
        try
        {
            if (ddlPackages.SelectedIndex > 0)
            {
               
                int packageId = Convert.ToInt32(ddlPackages.SelectedItem.Value);
                tblitinerary.Controls.Clear();
                LoadPackage(packageId);
                
                //lbUploadMsg.Text = string.Empty;
                //lbUpload2Msg.Text = string.Empty;
                //lbUpload3Msg.Text = string.Empty;
                hotelVariablesArray.Clear();
                hotelVariables.Clear();
                BindDetails();


                //hotelVariables.Add("exclusionsCount", hdfExclCounter.Value);
                //hotelVariables.Add("inclusionsCount", hdfinclCounter.Value);
                //hotelVariables.Add("priceExcludeCount", hdfpriceExcludeCounter.Value);
                //hotelVariables.Add("tncCount", hdftncCounter.Value);

                hdfinclusion.Value = "";
                hdfExclusions.Value = "";
                hdfpriceExclude.Value = "";
                hdftnc.Value = "";
                hotelVariables.Add("exclusionsCount", hdfExclCounter.Value);
                hotelVariables.Add("inclusionsCount", hdfinclCounter.Value);
                hotelVariables.Add("priceExcludeCount", hdfpriceExcludeCounter.Value);
                hotelVariables.Add("tncCount", hdftncCounter.Value);
                ddlPackages.Style.Add("display", "block");
                ddlAgentCopy.Style.Add("display", "block");
                lblcopyAgent.Style.Add("display", "block");
                lblPackage.Style.Add("display", "block");
                ddlPackages.SelectedValue = Convert.ToString(packageId);
                chkCopy.Enabled = false;
                ddlAgentCopy.Enabled = true;
            }
            else
            {
                //clear
                Clear();
                //chkCopy.Checked = false;
                //ddlAgent.se
                chkCopy.Enabled = true;
                ddlAgentCopy.Enabled = false;
                ddlPackages.Style.Add("display", "block");
                ddlAgentCopy.Style.Add("display", "block");
                lblcopyAgent.Style.Add("display", "block");
                lblPackage.Style.Add("display", "block");
                //ddlAgent.SelectedIndex = -1;
                //ddlTourType.SelectedIndex = -1;
                ddlPackages.SelectedIndex = -1;
                //ddlAgentCopy.SelectedIndex = -1;
            }
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }
 
    //Commented by Ziyad Sir 07/10/2015
    //private void AuthorizationCheck()
    //{
    //    if (Session["roleId"] == null)
    //    {
    //        String values = "?errMessage=Login Required to access " + Page.Title + " page.";
    //        values += "&requestUri=" + Request.Url.ToString();
    //        Response.Redirect("Default.aspx" + values, true);
    //    }
    //    else if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.ManageWhiteLabel))
    //    {
    //        String values = "?errMessage=You are not authorised to access " + Page.Title + " page.";
    //        values += "&requestUri=" + Request.Url.ToString();
    //        Response.Redirect("Default.aspx" + values, true);
    //    }


    //}

    private bool agencyCheck()
    {
        bool agencyCheck = true;
        if (chkCopy.Checked)
        {
            if (ddlPackages.SelectedIndex == 0)
            {
                lblError.Text = "Please Select Package";
                agencyCheck = false;
            }
            if (ddlAgentCopy.SelectedIndex == 0)
            {
                lblError.Text = "Please Select Copy Agent";
                agencyCheck = false;
            }
            else if (string.IsNullOrEmpty(hdfAgencyImagePath.Value))
            {
                lblError.Text = "Package Image path is not configured for the Copy Agent";
                agencyCheck = false;
            }
           
        }
        else
        {
            if (ddlAgent.SelectedIndex == 0)
            {
                lblError.Text = "Please Select Agent";
                agencyCheck = false;
            }
            else if (string.IsNullOrEmpty(hdfAgencyImagePath.Value))
            {
                lblError.Text = "Package Image path is not configured for the Agent";
                agencyCheck = false;
            }
        }
        return agencyCheck;
    }
    private void DeleteOldImages()
    {
        if (!chkCopy.Checked)
        {
            if (hdfOldImage1.Value != hdfImage1.Value)
            {
                if (System.IO.File.Exists(hdfOldImage1.Value)) System.IO.File.Delete(hdfOldImage1.Value);
            }
            if (hdfOldImage2.Value != hdfImage2.Value)
            {
                if (System.IO.File.Exists(hdfOldImage2.Value)) System.IO.File.Delete(hdfOldImage2.Value);
            }
            if (hdfOldImage3.Value != hdfImage3.Value)
            {
                if (System.IO.File.Exists(hdfOldImage3.Value)) System.IO.File.Delete(hdfOldImage3.Value);
            }

            //added on 02032016 for clear old MainImage1&2
            if (hdfOldImage4.Value != hdfImage4.Value)
            {
                if (System.IO.File.Exists(hdfOldImage4.Value)) System.IO.File.Delete(hdfOldImage4.Value);
            }
            if (hdfOldImage5.Value != hdfImage5.Value)
            {
                if (System.IO.File.Exists(hdfOldImage5.Value)) System.IO.File.Delete(hdfOldImage5.Value);
            }
        }
        hdfImage1.Value = string.Empty;
        hdfImage2.Value = string.Empty;
        hdfImage3.Value = string.Empty;
        //added on 02032016 for clear hidden MainImage1&2
        hdfImage4.Value = string.Empty;
        hdfImage5.Value = string.Empty;

        hdfOldImage1.Value = string.Empty;
        hdfOldImage2.Value = string.Empty;
        hdfOldImage3.Value = string.Empty;
        //added on 02032016 for clear old MainImage1&2
        hdfOldImage4.Value = string.Empty;
        hdfOldImage5.Value = string.Empty;
        chkCopy.Checked = false;    
    }

    private void CopyFiles(string sourcePath, string destinationPath)
    {
        if (!Directory.Exists(destinationPath)) Directory.CreateDirectory(destinationPath);
        destinationPath = destinationPath + "" + sourcePath;
        sourcePath = HolidayPackage.GetAgencyImagePath(Convert.ToInt16(ddlAgent.SelectedItem.Value), "P").ToString() + "" + sourcePath;
        if (!File.Exists(destinationPath))
        {
            if (File.Exists(sourcePath))
            {
                File.Copy(sourcePath, destinationPath);
            }
            else
            {
                throw new Exception(lblError.Text = "Selected Pcakage's Images Doesn't Exist.Please Upload New Images!");
            }
        }
    }
    private void BindDetails()
    {

        lblSucess.Text = string.Empty;
        //Inclusion
        int inclusionsCount = Convert.ToInt16(hdfinclCounter.Value);
        emptyStringArr[0] = string.Empty;
        string inclusions = hdfinclusion.Value;
        //Commented by chandan on 15/10/2015
        //BindItinerary();
        if (inclusionsCount <= 2 && inclusions == "#&#")
        {
            inclusionsCount = 2;
            hotelVariablesArray.Add("inclusionsValue", emptyStringArr);

        }
        else
        {
            hotelVariablesArray.Add("inclusionsValue", inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
        }


        //Exclusion

        int exclusionsCount = Convert.ToInt16(hdfExclCounter.Value);
        emptyStringArr[0] = string.Empty;
        string exclusions = hdfExclusions.Value;
        if (exclusionsCount <= 2 && exclusions == "#&#")
        {
            exclusionsCount = 2;
            hotelVariablesArray.Add("exclusionsValue", emptyStringArr);
        }
        else
        {
            hotelVariablesArray.Add("exclusionsValue", exclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
        }

        //Price
        int priceExcludeCount = Convert.ToInt16(hdfpriceExcludeCounter.Value);
        emptyStringArr[0] = string.Empty;
        string priceExclude = hdfpriceExclude.Value;
        if (priceExcludeCount <= 2 && priceExclude == "#&#")
        {
            priceExcludeCount = 2;
            hotelVariablesArray.Add("priceExcludeValue", emptyStringArr);
        }
        else
        {
            hotelVariablesArray.Add("priceExcludeValue", priceExclude.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
        }
        //Terms and Conditions

        int tncCount = Convert.ToInt16(hdftncCounter.Value);
        emptyStringArr[0] = string.Empty;
        string tnc = hdftnc.Value;


        if (tncCount <= 2 && tnc == "#&#")
        {
            tncCount = 2;
            hotelVariablesArray.Add("tncValue", emptyStringArr);
        }
        else
        {
            hotelVariablesArray.Add("tncValue", tnc.Split(splitter, StringSplitOptions.RemoveEmptyEntries));
        }
    }
    protected void BindCurrency(int agentId)
    {
        try
        {
            ddlSupplierCurrency.DataSource = CurrencyMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            ddlSupplierCurrency.DataValueField = "CURRENCY_ID";
            ddlSupplierCurrency.DataTextField = "CURRENCY_CODE";
            ddlSupplierCurrency.DataBind();
            ddlSupplierCurrency.SelectedItem.Text = AgentMaster.GetAgentCurrency(agentId);
           
            //ddlSupplierCurrency.Items.Insert(0, new ListItem("Select Currency"));
        }
        catch
        {
            throw;
        }
    }

}

