﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="VisaTypeList" Title="Cozmo Travels" Codebehind="VisaTypeList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
  
  
  
  
        <div> <h4>Visa Type List</h4> </div>
   
        <div class=" bg_white bor_gray pad_10 paramcon">
    <div class="col-md-2"> <label class="pull-right fl_xs">
                 Search by country:</label></div>
    
    <div class="col-md-2"> 
    <asp:DropDownList ID="ddlCountry" runat="server"  CssClass="form-control">
                </asp:DropDownList></div>
                
    <div class="col-md-2"> <label class="pull-right fl_xs">
                    Agent:
                </label></div>
    <div class="col-md-2"> <asp:DropDownList ID="ddlAgent" CssClass=" form-control" runat="server">
                    </asp:DropDownList></div>
    <div class="col-md-2 xspadtop10">
    
    <asp:Button ID="btnSearch" CssClass="but but_b" runat="server" Text="Search" OnClick="btnSearch_Click" />
    
    
     </div>
    <div class="col-md-2"> <asp:HyperLink ID="HyperLink1" CssClass="fcol_blue" runat="server" NavigateUrl="~/AddVisaType.aspx">Add New Visa Type </asp:HyperLink></div>
       
    
    <div class="clearfix"> </div> 
    </div>
  
  


  
    
    <div>
        <asp:Label ID="lbl_msg" runat="server" Text=""></asp:Label>
    </div>
    
     <div class="table-responsive margin-top-10">  
     
     
    <div class="grid_view">
        <asp:GridView ID="GridView1" CssClass="datagrid" runat="server" AutoGenerateColumns="False"
            CellPadding="2" Width="100%" DataKeyNames="visaTypeId" ForeColor="#333333" GridLines="None"
            OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" AllowPaging="True"
            OnPageIndexChanging="GridView1_PageIndexChanging">
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="visaTypeId" HeaderText="ID" ReadOnly="True">
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="visaTypeName" HeaderText="Name">
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="agentname" HeaderText="Agent Name">
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="countryCode" HeaderText="Country Code">
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="description" HeaderText="Description">
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="isActive" HeaderText="Is Active" ReadOnly="True">
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:HyperLinkField Text="Edit" DataNavigateUrlFields="visaTypeId" HeaderText="Edit"
                    DataNavigateUrlFormatString="AddVisaType.aspx?id={0}"></asp:HyperLinkField>
              
                <asp:TemplateField HeaderText="Activate/Deactivate">
                    <HeaderStyle HorizontalAlign="left" />
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:LinkButton ID="linkButtonStatus" runat="server" Text="Activate" CommandName="ChangeStatus">
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
                <RowStyle />
        <EditRowStyle />
        <SelectedRowStyle  Font-Bold="True" />
        <PagerStyle HorizontalAlign="Left" Font-Bold="True" />
        <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
         <AlternatingRowStyle CssClass="altrow" />
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
            NextPageText="Next" PreviousPageText="Previous" />
    </asp:GridView>
    </div>
    
    
    </div>
</asp:Content>
