﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
public partial class ActivityResultsGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected DataTable CityList;
    protected DataTable ThemeList;
    protected List<string> Duration;
    protected DataTable Country;
    DataTable Activities;
    protected string activityImgFolder;
    string isFixedDeparture = "N";

    PagedDataSource pagedData = new PagedDataSource();
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        activityImgFolder = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"].ToString();
        if (Settings.LoginInfo != null)
        {
            if (!IsPostBack)
            {
                CurrentPage = 0;
                BindActivities();
            }
            else
            {
                DataTable dt = (DataTable)Session["Activities"];
                if (hdfClearFilters.Value == "no")
                {
                    if (hdfCity.Value != null && hdfCity.Value != string.Empty)
                    {
                        string str = hdfCity.Value;
                        SortCity(str);
                    }
                    if (hdfTheme.Value != null && hdfTheme.Value != string.Empty)
                    {
                        string str = hdfTheme.Value;
                        SortTheme(str);
                    }
                    if (hdfDuration.Value != null && hdfDuration.Value != string.Empty)
                    {
                        //int val = Convert.ToInt32(hdfDuration.Value);
                        string val = hdfDuration.Value;
                        SortDuration(val);
                    }
                    if (hdfCountry.Value != null && hdfCountry.Value != string.Empty)
                    {
                        string str = hdfCountry.Value;
                        SortCountry(str);
                    }
                    sortBy();
                }
                else
                {
                    BindActivities();
                    ViewState["Activity"] = dt;
                    sortBy();
                }
            }

        InitializePageControls();
        doPaging();

        }
        else
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
    private void Clear()
    {
        hdfCity.Value = null;
        hdfTheme.Value = null;
        hdfDuration.Value = null;
        hdfCountry.Value = null;
        //ddlSort.SelectedIndex = 0;
    }

    private void fill_Tree2()
    {

        //if (CityList != null && CountryList != null)
        //{
        //    ds.Tables.Add(CityList);
        //    ds.Tables.Add(CountryList);
        //}
        TreeView1.Nodes.Clear();
        foreach (DataRow row in Country.Rows)
        {

            TreeNode tnParent = new TreeNode();
            tnParent.Text = row["countryName"].ToString();
            tnParent.Value = row["countryCode"].ToString();

            tnParent.ToolTip = "Click to get Child";
            tnParent.SelectAction = TreeNodeSelectAction.SelectExpand;
            tnParent.NavigateUrl = "javascript:SortCountryJS('" + tnParent.Text + "')";
            tnParent.Expand();
            tnParent.Selected = true;
            FillChild(tnParent, tnParent.Value);
            TreeView1.Nodes.Add(tnParent);

        }
    }
    public void FillChild(TreeNode parent, string ParentId)
    {
        //DataSet ds = PDataset("Select * from ChildTable where ParentId =" + ParentId);
        parent.ChildNodes.Clear();
        //foreach (DataRow row in CityList.Rows )
        {
            DataRow[] cities = CityList.Select("countryCode='" + ParentId + "'");
            if (cities != null && cities.Length > 0)
            {
                foreach (DataRow row in cities)
                {
                    TreeNode child = new TreeNode();
                    child.Text = row["cityName"].ToString();
                    child.Value = row["cityCode"].ToString();
                    if (child.ChildNodes.Count == 0)
                    {
                        child.PopulateOnDemand = true;
                    }
                    child.NavigateUrl = "javascript:SortCityJS('" + child.Text + "')";
                    child.ToolTip = "Click to get Child";
                    child.SelectAction = TreeNodeSelectAction.SelectExpand;
                    child.CollapseAll();

                    //bool notfound = true;
                    //if (parent.ChildNodes.Count > 0)
                    //{
                    //    for (int i = 1; i < parent.ChildNodes.Count; i++)
                    //    {
                    //        TreeNode node = parent.ChildNodes[i];
                    //        if (node.Text != child.Text)
                    //        {
                    //            notfound = false;
                    //        }
                    //    }
                    //    if (!notfound)
                    //    {
                    //        parent.ChildNodes.Add(child);
                    //    }
                    //}
                    //else
                    {
                        parent.ChildNodes.Add(child);
                    }
                }
            }
        }
    }

    private void InitializePageControls()
    {
        ActivityDetails ad = new ActivityDetails();
        CityList = ad.LoadCity(isFixedDeparture,Settings.LoginInfo.AgentId);
        Country = ad.LoadCountry(isFixedDeparture, Settings.LoginInfo.AgentId);
        ThemeList = ad.LoadTheme("A");
        Duration = ad.LoadDuration(isFixedDeparture, Settings.LoginInfo.AgentId);
        fill_Tree2();

    }
    private void BindActivities()
    {
        try
        {
            string status = "A";

            ActivityDetails.AgentCurrency = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency;
            ActivityDetails.ExchangeRates = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentExchangeRates;
            Activities = ActivityDetails.GetActivities(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId,isFixedDeparture);
            Session["Activities"] = Activities;
            BindDataList(Activities);
            sortBy();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindDataList(DataTable dt)
    {
        try
        {

            DLActivities.DataSource = dt;
            DLActivities.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }


    protected void ddlSort_SelectedIndexChanged1(object sender, EventArgs e)
    {
        //DataTable dt;
        //DataView dv;
        try
        {
            sortBy();
            //DataTable pdt = (DataTable)ViewState["Activity"];
            //if (pdt != null && pdt.Rows.Count > 0)
            //{
            //    dt = (DataTable)ViewState["Activity"];
            //    dv = new DataView(dt);
            //}
            //else
            //{
            //    dt = (DataTable)Session["Activities"];
            //    dv = new DataView(dt);
            //}

            //if (ddlSort.SelectedValue == "price")
            //{
            //    dv.Sort = "amount";
            //    dt = dv.ToTable();
            //    // BindDataList(dt);
            //    ViewState["Activity"] = dt;
            //    doPaging();
            //}
            //else if (ddlSort.SelectedValue == "alphabetical")
            //{
            //    dv.Sort = "activityName";
            //    dt = dv.ToTable();
            //    // BindDataList(dt);
            //    ViewState["Activity"] = dt;
            //    doPaging();
            //}
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    void sortBy()
    {
        DataTable dt;
        DataView dv;
        string sortType = ddlSort.SelectedItem.Value == "alphabetical" ? "activityName" : ddlSort.SelectedItem.Value == "price" ? "amount" : "";
        try
        {
            DataTable pdt = (DataTable)ViewState["Activity"];
            if (pdt != null && pdt.Rows.Count > 0)
            {
                dt = (DataTable)ViewState["Activity"];
                dv = new DataView(dt);
            }
            else
            {
                dt = (DataTable)Session["Activities"];
                dv = new DataView(dt);
            }

            if (!string.IsNullOrEmpty(sortType))
            {
                dv.Sort = sortType;
                dt = dv.ToTable();
                // BindDataList(dt);
                ViewState["Activity"] = dt;
                doPaging();
            }
            //else if (ddlSort.SelectedValue == "alphabetical")
            //{
            //    dv.Sort = "activityName";
            //    dt = dv.ToTable();
            //    // BindDataList(dt);
            //    ViewState["Activity"] = dt;
            //    doPaging();
            //}
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void doPaging()
    {
        DataTable pdt = (DataTable)ViewState["Activity"];
        //if (pdt!=null && pdt.Rows.Count>0)
        if (pdt != null)
        {
            DataTable dt = (DataTable)ViewState["Activity"];
            pagedData.DataSource = dt.DefaultView;
        }
        else
        {
            DataTable dt = (DataTable)Session["Activities"];
            pagedData.DataSource = dt.DefaultView;
        }
        pagedData.AllowPaging = true;
        pagedData.PageSize = 10;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        DLActivities.DataSource = pagedData;
        DLActivities.DataBind();
        Clear();
        //ViewState["Activity"] = null;
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }


    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }

    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }

    #region Filtering Button Clcik event and Methods
    //protected void lnkDubai_Click(object sender, EventArgs e)
    //{
    //    string str = "Dubai";
    //    SortCity(str);
    //}
    //protected void LinkButton1_Click(object sender, EventArgs e)
    //{
    //    string str = "Sharjah";
    //    SortCity(str);
    //}
    //protected void LinkButton2_Click(object sender, EventArgs e)
    //{
    //    string str = "Ummul Queen";
    //    SortCity(str);
    //}
    //protected void lnkCountry1_Click(object sender, EventArgs e)
    //{
    //    string str = "United Arab Emirates";
    //    SortCountry(str);
    //}
    //protected void LinkCountry2_Click(object sender, EventArgs e)
    //{
    //    string str = "Qatar";
    //    SortCountry(str);
    //}
    //protected void LinkButton4_Click(object sender, EventArgs e)
    //{
    //    string str = "Doha";
    //    SortCity(str);
    //}
    //protected void LinkButton5_Click(object sender, EventArgs e)
    //{
    //    string str = "Sharjah";
    //    SortCity(str);
    //}
    //protected void LinkButton6_Click(object sender, EventArgs e)
    //{
    //    string str = "Ummul Queen";
    //    SortCity(str);
    //}
    //protected void LinkCountry3_Click(object sender, EventArgs e)
    //{
    //    string str = "Saudia Arabia";
    //    SortCountry(str);
    //}
    //protected void LinkButton7_Click(object sender, EventArgs e)
    //{
    //    string str = "Riyadh";
    //    SortCity(str);
    //}
    //protected void LinkButton8_Click(object sender, EventArgs e)
    //{
    //    string str = "Jeddah";
    //    SortCity(str);
    //}
    //protected void LinkButton9_Click(object sender, EventArgs e)
    //{
    //    string str = "Najd";
    //    SortCity(str);
    //}
    //protected void LinkButton3_Click(object sender, EventArgs e)
    //{
    //    string str = LinkButton3.Text;
    //    SortTheme(str);
    //}
    //protected void LinkButton10_Click(object sender, EventArgs e)
    //{
    //    string str = LinkButton10.Text;
    //    SortTheme(str);
    //}
    //protected void LinkButton11_Click(object sender, EventArgs e)
    //{
    //    string str = LinkButton11.Text;
    //    SortTheme(str);
    //}
    //protected void LinkButton12_Click(object sender, EventArgs e)
    //{
    //    string str = LinkButton12.Text;
    //    SortTheme(str);
    //}
    public void SortCity()// not using
    {
        try
        {
            DataTable table1 = (DataTable)Session["Activities"];
            DataRow[] rows = table1.Select("cityName=Dubai");
            DataTable dt = table1.Clone();
            foreach (DataRow row in rows)
            {
                dt.ImportRow(row);
            }
            ViewState["Activity"] = dt;
            CurrentPage = 0;
            doPaging();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void SortCity(string str)
    {
        try
        {
            DataTable table1 = (DataTable)Session["Activities"];
            DataRow[] rows = table1.Select("cityName='" + str + "'");
            DataTable dt = table1.Clone();
            foreach (DataRow row in rows)
            {
                dt.ImportRow(row);
            }
            ViewState["Activity"] = dt;
            CurrentPage = 0;
            doPaging();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void SortCountry(string str)
    {
        try
        {
            DataTable table1 = (DataTable)Session["Activities"];
            DataRow[] rows = table1.Select("countryName='" + str + "'");
            DataTable dt = table1.Clone();
            foreach (DataRow row in rows)
            {
                dt.ImportRow(row);
            }
            ViewState["Activity"] = dt;
            CurrentPage = 0;
            doPaging();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void SortTheme(string str)
    {
        try
        {
            DataTable table1 = (DataTable)Session["Activities"];
            DataRow[] rows = table1.Select("ThemeId like ('%" + str + "%')");
            DataTable dt = table1.Clone();
            foreach (DataRow row in rows)
            {
                dt.ImportRow(row);
            }
            ViewState["Activity"] = dt;
            CurrentPage = 0;
            doPaging();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void SortDuration(string val)
    {
        try
        {
            DataTable table1 = (DataTable)Session["Activities"];
            DataRow[] rows = table1.Select("durationHours='" + val + "'");
            DataTable dt = table1.Clone();
            foreach (DataRow row in rows)
            {
                dt.ImportRow(row);
            }
            ViewState["Activity"] = dt;
            CurrentPage = 0;
            doPaging();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion


    protected void DLActivities_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            Label lblCategory = e.Item.FindControl("lblCategory") as Label;

            DataRowView view = e.Item.DataItem as DataRowView;
            ActivityDetails obj = new ActivityDetails();
            ThemeList = obj.LoadTheme("A");
            DataRow[] filteredRows = ThemeList.Select("themeid in (" + view["themeId"].ToString() + ")");

            if (filteredRows != null && filteredRows.Length > 0)
            {
                foreach (DataRow fr in filteredRows)
                {
                    if (lblCategory.Text.Length > 0)
                    {
                        lblCategory.Text += ", " + fr["ThemeName"].ToString();
                    }
                    else
                    {
                        lblCategory.Text = fr["ThemeName"].ToString();
                    }
                }
            }
        }
    }
    protected string CTCurrencyFormat(object currency)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
    }
    
    # region GridBindMethods
    protected object restrictLength(object value)
    {
        string txt = value.ToString();
        if (txt.Length > 200)
        {
            txt = txt.Substring(0, 199) + "....";
        }
        return txt;

    }
    # endregion
}

