﻿using System;
using System.Data;
using System.Web.UI;
using CT.Core;
using System.Collections.Generic;
using CT.Configuration;
using System.Data.SqlClient;
using Visa;
using CT.TicketReceipt.BusinessLayer;
using System.Web.UI.WebControls;

public partial class AdminVisaQueue : CT.Core.ParentPage
{
    protected string message = string.Empty;
    protected bool isAdmin = false;
    protected int recordsPerPage = 5; // By Default it is 5
    protected int noOfPages;
    protected int pageNo;
    protected List<DataRow[]> listOfVisaBookings = new List<DataRow[]>();
    protected int queueCount;
    protected string show = string.Empty;
    protected List<VisaType> visaTypeList = new List<VisaType>();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            AuthorizationCheck();
            // visaTypeList = VisaType.GetAllVisaTypeList();
            if (Session["agencyId"] != null && (int)Session["agencyId"] == 0)
            {
                isAdmin = true;
            }
            if (!IsPostBack)
            {
                InitializeControls();                            
                txtFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                //if (!isAdmin)
                //{
                //    AgentMaster LoginAgency = new AgentMaster(Convert.ToInt16(Session["agencyId"]));
                //    string agentName = LoginAgency.Name;
                //    SearchBox.Text = agentName;
                //    Page.ClientScript.RegisterStartupScript(this.GetType(), "agency", "SearchAgent(" + Convert.ToInt16(Session["agencyId"]) + ");", true);
                //    SearchBox.Enabled = false;
                //}
                //else 
                //{
                //    SearchBox.Enabled = true;
                //}
                DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
                IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
                if (txtFromDate.Text != string.Empty)
                {
                    try
                    {
                        startDate = Convert.ToDateTime(txtFromDate.Text, provider);
                    }
                    catch { }
                }
                else
                {
                    startDate = DateTime.Now;
                }

                if (txtToDate.Text != string.Empty)
                {
                    try
                    {
                        endDate = Convert.ToDateTime(Convert.ToDateTime(txtToDate.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
                    }
                    catch { }
                }
                else
                {
                    endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
                }

            }
            //if (Request["visaId"] != null)
            //{
            //    try
            //    {
            //        Visa.VisaQueue visaQueue = new Visa.VisaQueue();
            //        visaQueue.VisaId = Convert.ToInt32(Request["visaId"]);
            //        visaQueue.VisaStatusId = Convert.ToInt32(Request["statusId"]);
            //        int Status = visaQueue.UpdateVisaStatus();
            //        if (Status > 0)
            //        {
            //            message = "Status Updated successfully for " + Convert.ToInt32(Request["visaId"]) + ".";

            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        message = "Update Status Failed for : " + Convert.ToInt32(Request["visaId"]) + ".";
            //    }
            //}

            if (Request["PageNoString"] != null)
            {
                pageNo = Convert.ToInt16(Request["PageNoString"]);
            }
            else
            {
                pageNo = 1;
            }


            if (Session["agencyId"] != null && (int)Session["agencyId"] == 0)
            {
                isAdmin = true;
            }
            else
            {
                isAdmin = false;
            }

            recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["BookingQueueRecordsPerPage"]);
            Page.Title = "Visa Queue";
            if (Request["message"] != null)
            {
                message = Request["message"];
            }
            string orderByString = string.Empty;
            string whereString = string.Empty;
            VisaQueue visaQueue = GetVisaQueueObject();
            visaQueue.PageNumber = pageNo;
            visaQueue.NumberOfDataOnEachPage = recordsPerPage;
            try
            {
                int duplicateRecordsFlag = 1;
                if (ddlVisaStatus.SelectedItem.Text != "Duplicate")
                {
                    duplicateRecordsFlag = 0;
                }
                listOfVisaBookings = visaQueue.GetData(duplicateRecordsFlag);
                queueCount = visaQueue.QueueCount;
                pageNo = visaQueue.PageNumber;
                string url = "testpage.aspx?pageType=bookingdate";
                if ((queueCount % recordsPerPage) > 0)
                {
                    noOfPages = (queueCount / recordsPerPage) + 1;
                }
                else
                {
                    noOfPages = (queueCount / recordsPerPage);
                }

                show = CT.MetaSearchEngine.MetaSearchEngine.PagingJavascript(noOfPages, url, pageNo);
            }
            catch (SqlException SqlExcep)
            {
                Audit.Add(EventType.Visa, Severity.High, 0, "Exception in admin visa queue ,message: " + SqlExcep.ToString(), "");
            }
            catch (ArgumentException ArgExcep)
            {
                Audit.Add(EventType.Visa, Severity.High, 0, "Exception in admin visa queue ,message: " + ArgExcep.ToString(), "");
            }
            catch (Exception excep)
            {
                Audit.Add(EventType.Visa, Severity.High, 0, "Exception in admin visa queue ,message: " + excep.ToString(), "");
            }
        }
        catch(Exception ex)
        {
            Label lblmsg = (Label)this.Master.FindControl("lblError");
            lblmsg.Visible = true;
            lblmsg.Text = ex.Message;
            Audit.Add(EventType.Visa, Severity.High,Convert.ToInt32(Settings.LoginInfo.UserID), "Exception in admin visa queue ,message: " + ex.ToString(), "");
        }    
    }

    //protected void btnSearch_Click(object sender, EventArgs e)
    //{
    //    LoadQueue();
    //}

    //void LoadQueue()
    //{
    //    DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
    //    IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
    //    if (txtFromDate.Text != string.Empty)
    //    {
    //        try
    //        {
    //            startDate = Convert.ToDateTime(txtFromDate.Text, provider);
    //        }
    //        catch { }
    //    }
    //    else
    //    {
    //        startDate = DateTime.Now;
    //    }

    //    if (txtToDate.Text != string.Empty)
    //    {
    //        try
    //        {
    //            endDate = Convert.ToDateTime(Convert.ToDateTime(txtToDate.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
    //        }
    //        catch { }
    //    }
    //    else
    //    {
    //        endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
    //    }
    //}
    protected override void Render(System.Web.UI.HtmlTextWriter writer)
    {
        //string countrycode = ddlCountry.SelectedValue;
        //ClientScript.RegisterForEventValidation("ddlVisaType", "English");
        //ClientScript.RegisterForEventValidation("ddlVisaType", "Tamil");

        //ClientScript.RegisterForEventValidation("ddlVisaType", "Hindi");
        //visaType.Items.Add(ddlCountry.SelectedValue);
        base.Render(writer);

    }

    /// <summary>
    /// Method Gets Where String from the filters(Html Controls)
    /// </summary>
    /// <returns></returns>

    #region Private Methods
    private VisaQueue GetVisaQueueObject()
    {
        try
        {
            VisaQueue visaQueue = new VisaQueue();

            if (Request["Pax"] != null && Request["Pax"] != string.Empty)
            {

                visaQueue.PaxName = Request["Pax"].Replace("'", "''").Trim();
            }
            if (Request["VisaId"] != null && Request["VisaId"] != string.Empty)
            {
                string[] visaIdArray = Request["VisaId"].Split(',');

                string Str = "";
                for (int i = 0; i < visaIdArray.Length; i++)
                {
                    if (visaIdArray[i] != null)
                    {
                        Str += "'" + visaIdArray[i] + "'";
                    }
                    Str += ",";
                }
                Str = Str.Substring(0, Str.Length - 1);
                visaQueue.VisaNumber = Str;
            }
            if (Session["agencyId"] != null && (int)Session["agencyId"] != 0)
            {
                visaQueue.AgencyId = Session["agencyId"].ToString();
            }
            if (Request["AgentName"] != null && Request["AgentName"] != string.Empty)
            {
                visaQueue.AgencyId = Request["AgentName"].Replace("'", "''").Trim();
            }
            if (Request["visaType"] != "-1")
            {
                visaQueue.VisaTypeId = Convert.ToInt32(Request["visaType"]);
            }
            if (ddlVisaStatus.SelectedValue != "Select")
            {
                visaQueue.VisaStatusId = (int)(VisaStatus)Enum.Parse(typeof(VisaStatus), ddlVisaStatus.SelectedValue);
            }

            if (ddlAgent.SelectedValue != "Select Agent")
            {
                visaQueue.AgencyId = ddlAgent.SelectedValue.Replace("'", "''").Trim();
            }
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
            visaQueue.FromDate = Convert.ToDateTime(txtFromDate.Text, provider);
            visaQueue.ToDate = Convert.ToDateTime(Convert.ToDateTime(txtToDate.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);

            if (ddlCountry.SelectedValue != "Select Country")
            {
                visaQueue.CountryCode = ddlCountry.SelectedValue.Replace("'", "''").Trim();
            }
            if (ddlUser.SelectedValue != "Select User")
            {
                visaQueue.UserId = Convert.ToInt32(ddlUser.SelectedValue);
            }
            return visaQueue;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void AuthorizationCheck()
    {
        //if (Session["roleId"] == null)
        //{
        //    String values = "?errMessage=Login Required to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
        //else if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.CozmoVisa))
        //{
        //    String values = "?errMessage=You are not authorised to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString() + "&turnBack=no";
        //    Response.Redirect("Default.aspx" + values, true);
        //}

    }
    private void InitializeControls()
    {
        try
        {
            BindVisaStatus();
            BindCountry();
            BindAgent();
            BindUser(Settings.LoginInfo.AgentId);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindVisaStatus()
    {
        try
        {
            ddlVisaStatus.DataSource = Enum.GetNames(typeof(VisaStatus));
            ddlVisaStatus.DataBind();
            ddlVisaStatus.Items.Insert(0, "Select");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindCountry()
    {
        try
        {
            List<VisaCountry> countryList = VisaCountry.GetCountryList();
            ddlCountry.DataSource = countryList;
            ddlCountry.DataTextField = "countryName";
            ddlCountry.DataValueField = "countryCode";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, "Select Country");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindAgent()
    {
        try
        {
            ddlAgent.DataSource = AgentMaster.GetB2CAgentList();
            ddlAgent.DataValueField = "agent_Id";
            ddlAgent.DataTextField = "agent_Name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, "Select Agent");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindUser(int agentid)
    {
        try
        {
            if(agentid==1)
            {
                DataTable dtusers= UserMaster.GetList(agentid, ListStatus.Short, RecordStatus.Activated);
                //DataRow[] drresult = dtusers.Select("user_location_id="+37);
                DataRow[] drresult = dtusers.Select("user_location_id in (" + System.Configuration.ConfigurationManager.AppSettings["B2CUserLocation"]+")");
                DataTable dtFilteredUsers = new DataTable();
                dtFilteredUsers.Columns.Add("USER_FULL_NAME");
                dtFilteredUsers.Columns.Add("USER_ID");
                foreach (DataRow dr in drresult)
                {
                    DataRow dataRow = dtFilteredUsers.NewRow();
                    dataRow["USER_ID"] = dr[0];
                    dataRow["USER_FULL_NAME"] = dr[1];
                    dtFilteredUsers.Rows.Add(dataRow);
                }
                ddlUser.DataSource = dtFilteredUsers;
                ddlUser.DataTextField = "USER_FULL_NAME";
                ddlUser.DataValueField = "USER_ID";               
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, "Select User");
            }          
        }
        catch
        {
            throw;
        }
    }
    #endregion


}
