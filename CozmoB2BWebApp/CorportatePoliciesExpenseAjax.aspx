﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="CorportatePoliciesExpenseAjax" Codebehind="CorportatePoliciesExpenseAjax.aspx.cs" %>

<%if (Request["requestSource"] != null && Request["requestSource"] == "getExpenseAllowanceHtml")
  { %>
<!--  The below html is for rendering Cap Per Expense and Cap on Allowance -->
<div class="col-md-4 pad_left0" id="capturedExpense<%=ctrlId%>">
    <table>
        <tbody>
            <tr>
                <td id="cc<% =ctrlId %>" class=" bg-info pad_left10">
                    <% = countryCode %>
                    ,<% =cityCode %>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>
                                    Expense Type
                                </td>
                                <td>
                                    :
                                </td>
                                <td id="et<% =ctrlId %>">
                                    <% =expenseType %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Daily Actuals
                                </td>
                                <td>
                                    :
                                </td>
                                <td id="da<% =ctrlId %>">
                                    <% =dailyActuals %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Maximum Amount
                                </td>
                                <td>
                                    :
                                </td>
                                <td id="cur<% =ctrlId %>">
                                    <% =currency %>
                                    
                                    <% = amount %>
                                </td>
                                
                            </tr>
                            <tr>
                                <td>
                                    Include Travel Days
                                </td>
                                <td>
                                    :
                                </td>
                                <td id="td<% = ctrlId %>">
                                    <% = includeTravelDays %>
                                </td>
                                </tr>
                                <tr>
                                
                                <td id="ctrlRow<% =ctrlId %>">
                <a onclick="javascript:EditExpenseAllowance('<%=ctrlId%>' ,'<% =type %>');" href="javascript:void(0)"><span class="glyphicon glyphicon-edit"> </span></a>
                               
                               
   <a onclick="javascript:RemoveExpensesAllowancesDiv('<%=ctrlId%>', '<% =type %>');" href="javascript:void(0)">
   <span class="glyphicon glyphicon-remove-circle"> </span>
   </a>
                                </td>
                                
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <span style="display:none" id="rowInfo<% =ctrlId%>"><% = rowInfo %></span>
    
</div>
<%} %>


<%if (Request["requestSource"] != null && Request["requestSource"] == "getApproversHtml")
  { %>
  
  <!--  The below html is for rendering all tciket approvers and expense approvers. -->
  
  <div class="col-md-4 pad_left0" id="capturedApprover<%=ctrlId%>">
    <table>
        <tbody>
            
            <tr>
                <td>
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>
                                    Approver Name
                                </td>
                                <td>
                                    :
                                </td>
                                <td id="an<% =ctrlId %>">
                                    <% =approverName %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Hierarchy Order
                                </td>
                                <td>
                                    :
                                </td>
                                <td id="ho<% =ctrlId %>">
                                    <% =hierarchyOrder %>
                                </td>
                            </tr>
                           
                                <tr>
                                
                                <td id="ctrlRow<% =ctrlId %>">
                <a onclick="javascript:EditApprover('<%=ctrlId%>' ,'<% =type %>');" href="javascript:void(0)"><span class="glyphicon glyphicon-edit"> </span></a>
                               
                               
   <a onclick="javascript:CancelApprover('<%=ctrlId%>', '<% =type %>');" href="javascript:void(0)">
   <span class="glyphicon glyphicon-remove-circle"> </span>
   </a>
                                </td>
                                
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <span style="display:none" id="rowInfo<% =ctrlId%>"><% = rowInfo %></span>
    
</div>
  
  <%} %>
  
 
 
 <%if (Request["requestSource"] != null && Request["requestSource"] == "getExpenseReportsHtml")
  { %>
<!--  The below html is for rendering Cap Per Expense and Cap on Allowance -->

                            <tr>
                                
                                <td id="rptet<% =ctrlId %>">
                                    <% =reportType%>
                                </td>
                            
                                <td id="rptdt<% =ctrlId %>">
                                    <% =reportDate%>
                                </td>
                            
                                <td id="rptEmpName<% =ctrlId %>">
                                    <% =reportEmpName%>
                                    
                                    
                                </td>
                                
                            
                                <td id="rptCountry<% = ctrlId %>">
                                    <% = reportCountry%>
                                </td>
                               
                              
                                <td id="rptCity<% = ctrlId %>">
                                    <% = reportCity%>
                                </td>
                                
                                <td id="rptCostCenter<% = ctrlId %>">
                                    <% = reportCostCenter%>
                                </td>
                                
                                <td id="rptExpenseType<% = ctrlId %>">
                                    <% = reportExpType%>
                                </td>
                                
                                <td id="rptRefCode<% = ctrlId %>">
                                    <% = reportRefCode%>
                                </td>
                                
                                <td id="rptDesc<% = ctrlId %>">
                                    <% = reportDesc%>
                                </td>
                                
                                <td id="rptCurrency<% = ctrlId %>">
                                    <% = reportCurrency%>
                                </td>
                                
                                <td id="rptAmount<% = ctrlId %>">
                                    <% = reportAmount%>
                                </td>
                                
                             <td style="word-break: break-all;" id="rptComment<% = ctrlId %>">
                                    <% = reportComment%>
                                    <span style="display:none" id="rowInfo<% =ctrlId%>"><% = reportRowInfo%></span>
                                </td>
                                </tr>
                                                               
                                
                                
                            
    
    

<%} %>


<%if (Request["requestSource"] != null && Request["requestSource"] == "getVisaDetailsHtml")
  { %>
  
  <!--  The below html is for rendering all visa Details. -->
  
  <div class="col-md-4 pad_left0" id="capturedApprover<%=Convert.ToInt32(Request["id"])%>">
    <table>
        <tbody>
            
            <tr>
                <td>
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>
                                    Visa Country
                                </td>
                                <td>
                                    :
                                </td>
                                <td id="vc<% =Convert.ToInt32(Request["id"]) %>">
                                    <% =Convert.ToString(Request["visaCountry"])%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Visa Number
                                </td>
                                <td>
                                    :
                                </td>
                                <td id="vn<% =Convert.ToInt32(Request["id"]) %>">
                                    <% =Convert.ToString(Request["visaNo"])%>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    Visa Issue Date
                                </td>
                                <td>
                                    :
                                </td>
                                <td id="vid<% =Convert.ToInt32(Request["id"]) %>">
                                    <% =Convert.ToString(Request["issueDate"])%>
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td>
                                    Visa Expiry Date
                                </td>
                                <td>
                                    :
                                </td>
                               <td id="ved<% =Convert.ToInt32(Request["id"]) %>">
                                    <% =Convert.ToString(Request["ExpDate"])%>
                                </td>
                            </tr>
                            
                            
                            
                           
                                <tr>
                                
                                <td id="ctrlRow<% =Convert.ToInt32(Request["id"]) %>">
                <a onclick="javascript:EditTicketExpenseApprover('<%=Convert.ToInt32(Request["id"])%>' ,'<% =Convert.ToString(Request["type"]) %>');" href="javascript:void(0)"><span class="glyphicon glyphicon-edit"> </span></a>
                               
                               
   <a onclick="javascript:RemoveVisaDetailsDiv('<%=Convert.ToInt32(Request["id"])%>', '<% =Convert.ToString(Request["type"]) %>');" href="javascript:void(0)">
   <span class="glyphicon glyphicon-remove-circle"> </span>
   </a>
                                </td>
                                
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <span style="display:none" id="rowInfo<%=Convert.ToInt32(Request["id"])%>"><% = Convert.ToString(Request["rowInfo"])%></span>
    
</div>
  
  <%} %>
