﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CorporateViewExpenseDetailsUI"
    Title="View Expense Details" Codebehind="CorporateViewExpenseDetails.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

<script type ="text/javascript" >

    var Ajax;
    if (window.XMLHttpRequest) {
        Ajax = new window.XMLHttpRequest();
    }
    else {
        Ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    $(document).ready(function() {
    var expDetailId = parseInt(GetQueryStringByParameter("ExpId"));

    getExpenseDetailDocuments(expDetailId);
    })

    function getExpenseDetailDocuments(expDetailId) {
        var paramList = 'requestSource=getExpenseDocDetails&ExpDetailId=' + expDetailId;
    var url = "CorportatePoliciesExpenseAjax";
    Ajax.onreadystatechange = bindDocDetails;
    Ajax.open('POST', url, false);
    Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    Ajax.send(paramList);
}

function bindDocDetails() {
        if (Ajax.readyState == 4) {
            if (Ajax.status == 200) {
                if (Ajax.responseText.length > 0) {
                    $("#bookingRecords:last").append(Ajax.responseText);
                }
            }
        }
    }



function GetQueryStringByParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


    function Download(path, docName) {
        var open = window.open("DownloadeDoc.aspx?path=" + path + "&docName=" + docName);
        return false;
    }
</script>

    <div class="body_container">
        <div class="table-responsive bg_white">
            <asp:DataList ID="dlExpenseDetails" runat="server" AutoGenerateColumns="false" GridLines="Both"
                BorderWidth="0" CellPadding="0" CellSpacing="0" Width="100%" ForeColor="" ShowHeader="true"
                DataKeyField="ExpDetailId">
                <HeaderStyle BackColor="#333333" Font-Bold="true" ForeColor="White" HorizontalAlign="Left" />
                <ItemTemplate>
                <h4  style= 'color: #1c498a;'>
                                        Expense Details
                                    </h4>
                <div class="col-md-12">
                    <table class="table table-bordered b2b-corp-table">
                    <tbody>
                 <tr>
                     <th> DATE </th>
                     <th> EMPLOYEE NAME</th>
                     <th> DESTINATION</th>
                     <th> COST CENTER </th>
                     <th> EXPENSE TYPE</th>
                     <th> REFERENCE CODE</th> 
                     <th> DESCRIPTION</th>  
                     <th> CURRENCY</th> 
                     <th> AMOUNT</th> 
                     <th> COMMENT</th> 
                                     
                 </tr>
                 </tbody> 
                    
                    
                        <tr>
                            
                            <td>
                                 <%#Eval("DATE")%>
                            </td>
                        
                            
                            <td>
                             <%#Eval("EMP_NAME")%>
                            </td>
                        
                            <td>
                                 <%#Eval("DESTINATION")%>
                            </td>
                        
                            <td>
                               <%#Eval("EMP_COST_CENTER")%>
                            </td>
                        
                            <td>
                               <%#Eval("EMP_EXP_TYPE")%>
                            </td>
                        
                            <td>
                            
                            <%#Eval("REF_CODE")%>
                            </td>
                        
                            <td>
                              <%#Eval("DESCRIPTION")%>
                            </td>
                       
                            <td>
                            <%#Eval("CURRENCY")%>
                            
                            </td>
                        
                            <td>
                             <%#Eval("AMOUNT")%>
                            </td>
                        
                            <td>
                             <%#Eval("COMMENT")%>
                            </td>
                        </tr>
                        
                    </table>
                    </div>
                </ItemTemplate>
            </asp:DataList>
        </div>
        
        
        <div class="table-responsive bg_white">
           <table class="table table-bordered b2b-corp-table" id="tblDocRecords">
           <tbody id="bookingRecords">
                 <tr>
                     <th> DOC ID</th>
                     <th> DOWNLOAD</th>                    
                 </tr>
                 </tbody>                 
           </table>
        </div>
    </div>
</asp:Content>
