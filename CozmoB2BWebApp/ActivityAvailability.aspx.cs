﻿using System;
using System.Data;
using CT.BookingEngine;

public partial class ActivityAvailabilityGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected ActivityDetails Details = new ActivityDetails();
    protected Activity activity = new Activity();
    protected string activityImgFolder;
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        activityImgFolder = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"].ToString();
        {

            LoadData();
        }
    }
    public void LoadData()
    {
        try
        {
            if (Session["Activity"] != null)
            {
                activity = Session["Activity"] as Activity;
                lblTitle.Text = activity.Name;
                //imgActivity.ImageUrl = activity.ImagePath1;
                imgActivity.ImageUrl = activityImgFolder + activity.ImagePath2;
                dlRoomRates.DataSource = activity.PriceDetails;
                dlRoomRates.DataBind();
            }
            else
            {
                Response.Redirect("ActivityResults.aspx", false);
            }
        }
        catch (Exception ex)
        {
            throw ex;
           // Audit.Add(EventType.PakageQueries, Severity.High, 1, ex.Message, "0");
        }
    }
    protected void imgCheck_Click(object sender, EventArgs e)
    {
        if (Session["Activity"] != null)
        {
            activity = Session["Activity"] as Activity;
            bool isAvailable = true;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            DateTime excursionDate = Convert.ToDateTime(txtDepDate.Text, dateFormat);

            /************************************************************************************************
             * ------------------------------------Checks for Booking---------------------------------------*
             * 1. Check whether booking date is after Excursion start date and before closing date.         *         
             * 2. Booking cut off days check against the Booking Date / Excursion Date. If available then   *
             *    proceed otherwise prompt user to choose another                                           *
             * 3. Check whether booking date falls between available date ranges.                           *
             * 4. Check whether booking date falls between unavailable date ranges.                         *
             * 5. Check whether stock is available to proceed booking.                                      *
             * ---------------------------------------------------------------------------------------------*
             * **********************************************************************************************/
            //Check 1
            //DateTime StartFrom = Convert.ToDateTime(activity.StartFrom, dateFormat);
            //DateTime EndTo = Convert.ToDateTime(activity.EndTo, dateFormat);
            DateTime StartFrom = Convert.ToDateTime(activity.AvailableFrom, dateFormat);
            DateTime EndTo = Convert.ToDateTime(activity.AvailableTo, dateFormat);
            //if (StartFrom.CompareTo(excursionDate) < 0 && EndTo.CompareTo(excursionDate) >= 0)
            {
                //Check 1
                int totalDays = 0;
                if (excursionDate.CompareTo(DateTime.Now) > 0)
                {
                    totalDays = Convert.ToInt32(Math.Ceiling(excursionDate.Subtract(DateTime.Now).TotalDays));
                }
                else
                {
                    totalDays = Convert.ToInt32(DateTime.Now.Subtract(excursionDate).TotalDays);
                }
                if (activity.BookingCutOff <= totalDays)
                {
                    //Audit.Add(EventType.Email, Severity.High, 0, "Check 1 :true, BookingCutOff" + activity.BookingCutOff.ToString(), "");
                    //Check 2
                    if (excursionDate.CompareTo(activity.AvailableFrom) >= 0 && excursionDate.CompareTo(activity.AvailableTo) < 0)
                    {
                        //Audit.Add(EventType.Email, Severity.High, 0, "Check 2 :true, excursionDate." + excursionDate.ToString(), "");
                        foreach (DateTime uad in activity.UnavailableDates)
                        {
                            if (excursionDate.CompareTo(uad) != 0)
                            {
                                isAvailable = true;
                            }
                            else
                            {
                                isAvailable = false;
                                break;
                            }
                        }

                        if (isAvailable)
                        {
                            //Check 4
                            //if (activity.TransactionHeader == null)
                            //{

                            //    Audit.Add(EventType.Email, Severity.High, 0, "Check 2 :true, excursionDate." + excursionDate.ToString(), "");
                            //}
                            //else
                            //{
                            //    Audit.Add(EventType.Email, Severity.High, 0, "Check 2 :true, excursionDate." + excursionDate.ToString(), "");

                            //}
                            if ((activity.StockInHand - activity.StockUsed) > 0)
                            {
                                DataRow row = null;
                                if (activity.TransactionHeader.Rows.Count > 0)
                                {
                                    row = activity.TransactionHeader.Rows[0];
                                }
                                else
                                {
                                    row = activity.TransactionHeader.NewRow();
                                }
                                
                                row["ActivityId"] = activity.Id;
                                row["ActivityName"] = activity.Name;
                                row["TransactionDate"] = DateTime.Now;
                                row["Adult"] = ddlAdult.SelectedValue;
                                row["Child"] = ddlChild.SelectedValue;
                                row["Infant"] = ddlInfant.SelectedValue;
                                row["Booking"] = Convert.ToDateTime(txtDepDate.Text, dateFormat);
                                row["TransactionType"] = "B2B";
                                row["isFixedDeparture"] = "N";// Activity

                                if (activity.TransactionHeader.Rows.Count <= 0)
                                {
                                    activity.TransactionHeader.Rows.Add(row);
                                }
                                Session["Activity"] = activity;

                                //Proceed Activity booking
                                Response.Redirect("ActivityCategoriesList.aspx", false);
                            }
                            else
                            {

                                // Stock not available
                                //lblError.Text = "Stock is not available for this Activity ! Please contact Cozmo Travel";
                                lblError.Visible = true;
                                Image1.Visible = true;
                            }
                        }
                        else
                        {
                            lblError.Visible = true;
                            Image1.Visible = true;
                        }
                    }
                    else
                    {
                        lblError.Visible = true;
                        Image1.Visible = true;
                    }
                }
                else
                {
                    lblError.Visible = true;
                    Image1.Visible = true;
                }
            }
            //else
            //{
            //    lblError.Visible = true;
            //    Image1.Visible = true;
            //}
        }
        else
        {
            Response.Redirect("ActivityResults.aspx", false);
        }
    }
    protected string CTCurrencyFormat(object currency)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue);
        }
    }
}
