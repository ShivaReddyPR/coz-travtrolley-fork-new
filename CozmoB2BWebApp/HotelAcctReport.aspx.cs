﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using CT.BookingEngine;
using System.IO;
using CT.Core;

public partial class HotelAcctReportGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    private string HOTELACCTREPORT = "_HotelAcctReport";
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null)
            {
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.btnExport);
                
                lblSuccessMsg.Text = string.Empty;
                hdfParam.Value = "1";
                Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
                if (!IsPostBack)
                {
                    ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                    InitializePageControls();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void InitializePageControls()
    {
        try
        {
            dcFromDate.Value = Utility.ToDate(DateTime.Now.Date);
            dcToDate.Value = Utility.ToDate(DateTime.Now.Date.ToString("MMM-dd-yyyy 23:59"));
            Array Sources = Enum.GetValues(typeof(HotelBookingSource));
            foreach (HotelBookingSource source in Sources)
            {
                if (source == HotelBookingSource.DOTW || source == HotelBookingSource.RezLive || source == HotelBookingSource.LOH || source == HotelBookingSource.HotelBeds || source == HotelBookingSource.RezLive || source == HotelBookingSource.GTA || source == HotelBookingSource.TBOHotel || source == HotelBookingSource.JAC || source == HotelBookingSource.Miki ||  source == HotelBookingSource.HotelConnect || source == HotelBookingSource.WST) //sources Binding to dropdown
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(HotelBookingSource), source), ((int)source).ToString());
                    ddlSource.Items.Add(item);
                }
            }
            Array Statuses = Enum.GetValues(typeof(HotelBookingStatus));
            foreach (HotelBookingStatus status in Statuses)
            {
                if (status == HotelBookingStatus.Confirmed || status == HotelBookingStatus.Cancelled)
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(HotelBookingStatus), status), ((int)status).ToString());
                    ddlStatus.Items.Add(item);
                }

            }
            BindAgent();
            int b2bAgentId;
            int b2b2bAgentId;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgent.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgent.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }
            BindB2BAgent(Convert.ToInt32(ddlAgent.SelectedItem.Value));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }
            BindGrid();

        }
        catch
        { throw; }

    }
    #endregion

    //Binding BaseAgent And Agents
    private void BindAgent()
    {
        try
        {
            ddlAgent.DataSource = AgentMaster.GetList(1, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//TODO
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("-- All --", "0"));
            ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
        }
        catch { throw; }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindGrid();
        }
        catch { throw; }
    }

    protected void btnRefSubmit_Click(object sender, EventArgs e)
    {
        try
        {           
            BindGrid();
        }
        catch { throw; }
    }

    # region Session
    private DataTable HotelAcctReport
    {
        get
        {
            return (DataTable)Session[HOTELACCTREPORT];
        }
        set
        {
            if (value == null)
                Session.Remove(HOTELACCTREPORT);
            else
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["hotelId"] };
                Session[HOTELACCTREPORT] = value;
            }
        }
    }


    # endregion

    private void BindGrid()
    {
        try
        {
            DateTime fromDate = Utility.ToDate(dcFromDate.Value);
            DateTime toDate = Utility.ToDate(dcToDate.Value);
            int agent = Utility.ToInteger(ddlAgent.SelectedValue);
            int source = Utility.ToInteger(ddlSource.SelectedValue);
            int status = Utility.ToInteger(ddlStatus.SelectedItem.Value);
            int acctStatus = Utility.ToInteger(ddlAcctStatus.SelectedItem.Value);
            string agentType = string.Empty;
            bool ischeckby = Utility.ToBoolean(Checkdates.Checked);
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MaxValue;
            DateTime CheckinDate = DateTime.MinValue;
            DateTime CheckoutDate = DateTime.MaxValue;
            string agentref = Utility.ToString(txtRefNum.Text);
            string transType = string.Empty;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
            
            if ((agentref == "") && (ischeckby == false))
            {
                startDate = fromDate;
                endDate = toDate;
                CheckinDate = Convert.ToDateTime("01/01/1753 00:00:00", provider);
                CheckoutDate = Convert.ToDateTime(Convert.ToDateTime(CheckoutDate, provider).ToString("dd/MM/yyyy 23:59"), provider);
            }
            else if ((agentref == "") && (ischeckby == true))
            {
                CheckinDate = fromDate;
                CheckoutDate = toDate;
                startDate = Convert.ToDateTime("01/01/1753 00:00:00", provider);
                endDate = Convert.ToDateTime(Convert.ToDateTime(endDate, provider).ToString("dd/MM/yyyy 23:59"), provider);
            }
            else
            {
                CheckinDate = Convert.ToDateTime("01/01/1753 00:00:00", provider);
                CheckoutDate = Convert.ToDateTime(Convert.ToDateTime(CheckoutDate, provider).ToString("dd/MM/yyyy 23:59"), provider);
                startDate = Convert.ToDateTime("01/01/1753 00:00:00", provider);
                endDate = Convert.ToDateTime(Convert.ToDateTime(endDate, provider).ToString("dd/MM/yyyy 23:59"), provider);
                source = -1;
                status = -1;
                acctStatus = -1;
                transType = null;
               
            }
            if (agent == 0)
            {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = string.Empty;// null Means binding in list all BOOKINGS
                }
            }
            if (agent > 0 && ddlB2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else
                    {
                        agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }

            }
            if (agent > 0 && ddlB2B2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else
                {
                    agent = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                        agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                }
            }

            #region B2C purpose
            //string transType=string.Empty;
            if (Settings.LoginInfo.TransType == "B2B")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType="B2B";
            }
            else if (Settings.LoginInfo.TransType == "B2C")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType = "B2C";
            }
            else
            {
                ddlTransType.Visible = true;
                lblTransType.Visible = true;
                if (ddlTransType.SelectedItem.Value == "-1")
                {
                    transType = null;
                }
                else
                {
                    transType = ddlTransType.SelectedItem.Value;
                }
            }
            #endregion

            HotelAcctReport = ProductReport.HotelReportGetList(startDate, endDate, agent, source, status, acctStatus, agentType, transType, CheckinDate, CheckoutDate, agentref);
            Session["HotelAcctReport"] = HotelAcctReport;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvHotelAcctReport, HotelAcctReport);
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(EventType.Exception, Severity.High, 0, "Exception from Hotel Report:" + ex.ToString(), "");
        }
    }

    protected void Filter_Click(object sender, EventArgs e)
    {
        try
        {
            string[,] textboxesNColumns ={ { "HTtxtAgentCode", "agent_code" }, { "HTtxtAgentName", "agent_name" }, { "HTtxtBookingDate", "createdOn" },
                                            { "HTtxtHotelRef", "bookingId" }, {"HTtxtSupplier","hotelSource"},{"HTtxtSupConfirm","confirmationNo"} ,{"HTtxtSupReference","BookingRefNo"},
                                            {"HTtxtCheckIn","checkInDate"},{"HTtxtCheckOut","checkOutDate"},{"HTtxtHotel","hotelName"},{"HTtxtCity","cityRef"},
                                            {"HTtxtPayableAmount","Payableamount"},{"HTtxtInvoiceAmount","TotalAmount"},{"HTtxtProfit","profit"},{"HTtxtDiscount","Discount"},{"HTtxtInputVat","InVatAmount"},{"HTtxtOutputVat","OutVatAmount"},
                                            {"HTtxtAgentSF","AgentSF"},{"HTtxtInvoiceNo","InvoiceNo"},{"HTtxtStatus","status"},{"HTtxtAcctStatus","AccountedStatus"}
                                         ,{"HTtxtPassengerName","PassengerName"},{"HTtxtAdults","AdultCount"},{"HTtxtChilds","ChildCount"},{"HTtxtRoomType","RoomType"}
                                         ,{"HTtxtUserName","user_full_name"},{"HTtxtLocation","LocationName"}};

            CommonGrid g = new CommonGrid();
            HotelAcctReport = (DataTable)Session["HotelAcctReport"];
            g.FilterGridView(gvHotelAcctReport, HotelAcctReport.Copy(), textboxesNColumns);
            //Session["HotelAcctReport"] = ((DataTable)gvHotelAcctReport.DataSource).Copy();
            HotelAcctReport = ((DataTable)gvHotelAcctReport.DataSource).Copy();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void gvHotelAcctReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            selectedItem();
            gvHotelAcctReport.PageIndex = e.NewPageIndex;
            gvHotelAcctReport.EditIndex = -1;
            Filter_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnUpdateStatus_Click(object sender, EventArgs e)
    {
        try
        {

            selectedItem();
            isTrackingEmpty();

            //int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            foreach (DataRow dr in HotelAcctReport.Rows)
            {
                if (dr != null)
                {
                    bool selected = Utility.ToString(dr["isAccounted"]) == "Y" ? true : false;
                    long hotelId = Utility.ToLong(dr["hotelId"]);
                    bool isUpdated = Utility.ToString(dr["isAccounted"]) == "U" ? true : false;
                    if (selected && !isUpdated)
                    {
                        ProductReport.UpdateHotelAcctStatus(hotelId, (int)Settings.LoginInfo.UserID);
                    }
                }

            }
            BindGrid();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = "Updated Successfully";
            //hdfSelectCount.Value = "0";


        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.StartupScript(this.Page, "alert('" + ex.Message + "');", "Err");
        }

    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            //string script = "window.open('ExportExcelHotelAcctList.aspx?','','width=0,height=0,toolbar=yes,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            //Utility.StartupScript(this.Page, script, "Excel");
            string attachment = "attachment; filename=HotelAcctReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            dgHotelAcctReportList.AllowPaging = false;
            dgHotelAcctReportList.DataSource = HotelAcctReport;
            dgHotelAcctReportList.DataBind();
            dgHotelAcctReportList.RenderControl(htw);
            Response.Write(sw.ToString());
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);


        }
        finally
        {
            Response.End();
        }

    }

    protected void dgHotelAcctReportList_ItemDataBound(Object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        { 
            if (Settings.LoginInfo.AgentType == AgentType.B2B || Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                e.Item.Cells[4].Visible = false;
                e.Item.Cells[15].Visible = false;
                e.Item.Cells[17].Visible = false;
                e.Item.Cells[18].Visible = false;
                e.Item.Cells[24].Visible = false;
            }
            if(e.Item.ItemType != ListItemType.Header && e.Item.Cells[4].Text=="GIMMONIX")
            {
                DataRow[] dataRow =   HotelAcctReport.Select("confirmationNo='"+ e.Item.Cells[5].Text + "'");
                e.Item.Cells[4].Text = dataRow[0]["gxsupplierName"].ToString();
            }

        }
    }


    protected void gvHotelAcctReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (Settings.LoginInfo.AgentType == AgentType.B2B || Settings.LoginInfo.AgentType == AgentType.B2B2B)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSupplier = (Label)e.Row.FindControl("ITlblSupplier");
                Label lblProfit = (Label)e.Row.FindControl("ITlblProfit");
                Label lblDiscount = (Label)e.Row.FindControl("ITlblDiscount");
                Label lblAcctStatus = (Label)e.Row.FindControl("ITlblAcctStatus");
                Label lblPayableAmount = (Label)e.Row.FindControl("ITlblPayableAmount");
                lblSupplier.Visible = false;
                lblProfit.Visible = false;
                lblDiscount.Visible = false;
                lblAcctStatus.Visible = false;
                lblPayableAmount.Visible = false;

            }
            else
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    CT.TicketReceipt.Web.UI.Controls.Filter HTtxtSupplier = (CT.TicketReceipt.Web.UI.Controls.Filter)e.Row.FindControl("HTtxtSupplier");
                    CT.TicketReceipt.Web.UI.Controls.Filter HTtxtProfit = (CT.TicketReceipt.Web.UI.Controls.Filter)e.Row.FindControl("HTtxtProfit");
                    CT.TicketReceipt.Web.UI.Controls.Filter HTtxtDiscount = (CT.TicketReceipt.Web.UI.Controls.Filter)e.Row.FindControl("HTtxtDiscount");
                    CT.TicketReceipt.Web.UI.Controls.Filter HTtxtAcctStatus = (CT.TicketReceipt.Web.UI.Controls.Filter)e.Row.FindControl("HTtxtAcctStatus");
                    CT.TicketReceipt.Web.UI.Controls.Filter HTtxtPayableAmount = (CT.TicketReceipt.Web.UI.Controls.Filter)e.Row.FindControl("HTtxtPayableAmount");
                    HTtxtSupplier.Visible = false;
                    HTtxtProfit.Visible = false;
                    HTtxtDiscount.Visible = false;
                    HTtxtAcctStatus.Visible = false;
                    HTtxtPayableAmount.Visible = false;

                }
            }
        }
    }

    private void selectedItem()
    {
        try
        {
            foreach (System.Data.DataColumn col in HotelAcctReport.Columns) col.ReadOnly = false;
            foreach (GridViewRow gvRow in gvHotelAcctReport.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                HiddenField hdfHotelId = (HiddenField)gvRow.FindControl("IThdfHotelId");

                foreach (DataRow dr in HotelAcctReport.Rows)
                {
                    if (dr != null)
                    {
                        dr.BeginEdit();
                        if (Utility.ToString(dr["hotelId"]) == hdfHotelId.Value)
                        {
                            if (Utility.ToString(dr["isAccounted"]) != "U") dr["isAccounted"] = chkSelect.Checked ? "Y" : "N";
                        }
                        dr.EndEdit();
                    }
                }
            }
        }
        catch { throw; }
    }
    private void isTrackingEmpty()
    {
        try
        {
            bool _selected = false;
            foreach (DataRow dr in HotelAcctReport.Rows)
            {
                if (dr != null)
                {
                    bool isUpdated = Utility.ToString(dr["isAccounted"]) == "U" ? true : false; ;
                    if (!isUpdated && Utility.ToString(dr["isAccounted"]) == "Y")
                    {
                        _selected = true;
                        return;
                    }
                }
            }
            string strMsg = "Please Select atleast one Item ! ";
            if (!_selected) 
                Utility.Alert(this.Page, strMsg);
        }
        catch { throw; }
    }              

    //Binding B2B Agents
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //Binding B2B2B Agents
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
            BindB2BAgent(agentId);
            BindB2B2BAgent(agentId);
            hdfParam.Value = "0";
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            hdfParam.Value = "0";

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }


    #region Date Format
    protected string CTDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string CTDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }

    protected string CTCurrencyFormat(object currency, object decimalPoint)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + decimalPoint);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + decimalPoint);
        }
    }
    #endregion
    
}
