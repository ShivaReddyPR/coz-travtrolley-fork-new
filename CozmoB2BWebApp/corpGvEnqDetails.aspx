﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="corpGvEnqDetailsGUI" MasterPageFile="~/TransactionBE.master" Title="CorpGv Enqiry Details" Codebehind="corpGvEnqDetails.aspx.cs" %>
<%@ Import Namespace="CT.TicketReceipt.Common" %>
<%@ Import Namespace="CT.GlobalVisa" %>
<%@ Import Namespace="CT.Core" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphTransaction">
    <style>
        .tab-content {
            max-height: 300px;
            overflow-y: scroll;
        }
    </style>
    <%if (string.IsNullOrEmpty(errormessage) && enqRequestSession != null)
        {%>
    <div class="body_container">
        <div class="col_md_12">
            <div class="well bg_white martop_14 select_visa_for">
                <div class="row">
                    <div class="col-md-4">
                        Enquiry for:  
                           <b>
                               <%=enqRequestSession.TravelToCountryName %>
                           </b>
                    </div>
                    <div class="col-md-4">
                        for citizens of: <b><%=enqRequestSession.NationalityName %></b>
                    </div>
                    <div class="col-md-4">
                        living in: <b><%=enqRequestSession.ResidenseName %></b>
                    </div>

                </div>
            </div>
        </div>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#VisaRequirements">Visa Requirements </a></li>
            <li><a data-toggle="tab" href="#TermsandConditions">Terms and Conditions</a></li>
        </ul>

        <div class="tab-content tabouter">

            <div id="VisaRequirements" class="tab-pane fade in active" style="background-color: white;">
                <%if (reqMaster != null)
                    {
                %>

                <%if (!string.IsNullOrEmpty(reqMaster.Header))
                    { %>
                <h3><%=reqMaster.Header %></h3>
                <%} %>
                <p>
                    <ul class="list_custom">

                        <%if (!string.IsNullOrEmpty(reqMaster.Description))
                            { %>
                        <%=reqMaster.Description %>
                        <%}%>
                    </ul>
                </p>
                <%} %>
            </div>
            <div id="TermsandConditions" class="tab-pane fade" style="background-color: white;">
                <%if (gvTerms != null)
                    {
                %>
                <%if (!string.IsNullOrEmpty(gvTerms.Header))
                    { %>
                <h3><%=gvTerms.Header%></h3>
                <%} %>
                <p>
                    <%if (!string.IsNullOrEmpty(gvTerms.Description))
                        { %><%=gvTerms.Description %>
                    <%} %>
                </p>
                <%}
                %>
            </div>
            <br />
        </div>
        <div class="row">
            <div class="col-md-12">
                <br />
                <style>
                    .tooltip {
                        position: absolute;
                        z-index: 9999;
                    }
                </style>
                <h4>
                    <%if (gvFeeList != null)
                        { %>
                    <%=enqRequestSession.TravelToCountryName %> <%=enqRequestSession.VisaTypeName %> Fees 
                      <%} %>
                </h4>
                <div class="table-responsive">
                    <table class="table table-striped custom-checkbox-table">
                        <tbody>
                            <tr>
                                <td align="left"><strong>Visa Category</strong></td>
                                <td align="left"><strong>Processing  </strong></td>
                                <td align="left"><strong>Visa fee </strong></td>
                                <td align="left"><strong>Appointment Fee</strong></td>
                                <td align="left"><strong>Total Cost </strong></td>
                            </tr>
                            <%if (gvFeeList != null && gvFeeList.Count > 0)
                                {%>
                            <%for (int i = 0; i < gvFeeList.Count; i++)
                                {
                                    if (gvFeeList[i] != null)
                                    {%>
                            <tr>

                                <td align="left">
                                    <%=gvFeeList[i].VisaCategory%>                
                                </td>
                                <td align="left"><%=gvFeeList[i].ProcessingDays%> business days   </td>
                                <% 
                                    decimal TotalAdultsCharge = 0;
                                    decimal TotalChildCharge = 0;
                                    decimal TotalInfantCharge = 0;
                                    //decimal adSvcTax = 0;
                                    //decimal chSvcTax = 0;
                                    //decimal inSvcTax = 0;
                                    //decimal totalSvcTax = 0;
                                    //adSvcTax = Utility.ToDecimal(gvFeeList[i].AdultService)*adultCount ;
                                    //chSvcTax = Utility.ToDecimal(gvFeeList[i].ChildService)*childCount;
                                    //inSvcTax = Utility.ToDecimal(gvFeeList[i].InfantService)*infantCount ;
                                    //totalSvcTax = (adSvcTax + chSvcTax + inSvcTax);
                                    decimal adadd1charge = 0;
                                    decimal chadd1charge = 0;
                                    decimal inadd1charge = 0;
                                    decimal adHandlingFee = 0;
                                    decimal chHandlingFee = 0;
                                    decimal inHandlingFee = 0;
                                    decimal totalCharge = 0;
                                    decimal totaladd1charge = 0;
                                    decimal totalhandlingfee = 0;
                                    int adultCount = 0;
                                    int childCount = 0;
                                    int infantCount = 0;
                                    if (enqRequestSession != null)
                                    {
                                        adultCount = enqRequestSession.AdultCount;
                                        childCount = enqRequestSession.ChildCount;
                                        infantCount = enqRequestSession.InfantCount;
                                    }
                                    TotalAdultsCharge = Utility.ToDecimal(gvFeeList[i].TotalChargeAdult) * adultCount;
                                    TotalChildCharge = Utility.ToDecimal(gvFeeList[i].TotalChargeChild) * childCount;
                                    TotalInfantCharge = Utility.ToDecimal(gvFeeList[i].TotalChargeInfant) * infantCount;
                                    adadd1charge = Utility.ToDecimal(gvFeeList[i].AdultAdd1Fee) * adultCount;
                                    chadd1charge = Utility.ToDecimal(gvFeeList[i].ChildAdd1Fee) * childCount;
                                    inadd1charge = Utility.ToDecimal(gvFeeList[i].InfantAdd1Fee) * infantCount;
                                    for (int j = i; j < gvHandlingFee.Count; j++)
                                    {
                                        decimal HandlingFee = gvHandlingFee[j].HandlingFee;
                                        adHandlingFee = Utility.ToDecimal(gvHandlingFee[j].HandlingFee) * adultCount;
                                        chHandlingFee = Utility.ToDecimal(gvHandlingFee[j].HandlingFee) * childCount;
                                        inHandlingFee = Utility.ToDecimal(gvHandlingFee[j].HandlingFee) * infantCount;
                                        break;
                                    }
                                    totaladd1charge = (adadd1charge + chadd1charge + inadd1charge);
                                    totalhandlingfee = (adHandlingFee + chHandlingFee + chHandlingFee);
                                    totalCharge = (TotalAdultsCharge + TotalChildCharge + TotalInfantCharge + totalhandlingfee);
                                %>
                                <td align="left"><%=Formatter.ToCurrency(totalCharge - totaladd1charge-totalhandlingfee) %> <span class="text-danger"></span></td>
                                <td align="left"><%=Formatter.ToCurrency(totaladd1charge +totalhandlingfee) %> <span class="text-danger"></span></td>

                                <td align="left"><%=Formatter.ToCurrency(totalCharge) %><span class="text-danger"></span></td>
                            </tr>
                            <%}
                                    }
                                }%>
                        </tbody>
                    </table>
                </div>
            </div>
            <div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <%}
        else
        {%>
    <div class="body_container">
        <div>
            <%=errormessage %>
            <br />
            <a href="corpGvEnqRequest.aspx">Please Enquiry Again </a>
        </div>
    </div>
    <%} %>
</asp:Content>
