﻿<%@ Page MaintainScrollPositionOnPostback="true" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" ValidateRequest="false"
    AutoEventWireup="true" Inherits="ActivityMasterGUI"
    Title="Activity Master" EnableEventValidation="false" Codebehind="ActivityMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script type="text/javascript" src="Scripts/Common/Common.js"></script>

    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
    <%-- <script type="text/javascript" src="js2/Common.js"></script>--%>

    <script src="Scripts/jsBE/Utils.js" type="text/javascript"></script>

    <script type="text/javascript" src="ash.js"></script>

    <script language="javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js" type="text/javascript"></script>

    <style>
        .label {
            font-size: 13px;
        }
    </style>
    <script type="text/javascript">
        function DisplayTransfersDiv() {
            if (document.getElementById('ctl00_cphTransaction_ddlTransferInclude').value == 'Yes') {
                 //document.getElementById('divTransfers').style.display = "block";
                document.getElementById('ctl00_cphTransaction_txtPickLocation').disabled = false;
                document.getElementById('ctl00_cphTransaction_txtPickUpHr').disabled = false;
                document.getElementById('ctl00_cphTransaction_txtPickUpMin').disabled = false;
                 //document.getElementById('ctl00_cphTransaction_txtPickUpPoint').disabled = false;
                document.getElementById('ctl00_cphTransaction_txtPickUpHrRange').disabled = false;
                document.getElementById('ctl00_cphTransaction_txtPickUpMinRange').disabled = false;
                //document.getElementById('ctl00_cphTransaction_txtDropOffLocation').disabled = false;
                document.getElementById('addPickUp').style.display = "block";
                //document.getElementById('addTransfers').style.display = "block";
            }
            else {
                 //document.getElementById('divTransfers').style.display = 'none';
                document.getElementById('ctl00_cphTransaction_txtPickLocation').disabled = true;
                document.getElementById('ctl00_cphTransaction_txtPickLocation').value = null;
                document.getElementById('ctl00_cphTransaction_txtPickUpHr').disabled = true;
                document.getElementById('ctl00_cphTransaction_txtPickUpHr').value = null;
                document.getElementById('ctl00_cphTransaction_txtPickUpMin').disabled = true;
                document.getElementById('ctl00_cphTransaction_txtPickUpMin').value = null;
                //document.getElementById('ctl00_cphTransaction_txtPickUpPoint').disabled = true;
                //document.getElementById('ctl00_cphTransaction_txtPickUpPoint').value = null;
                document.getElementById('ctl00_cphTransaction_txtPickUpHrRange').disabled = true;
                document.getElementById('ctl00_cphTransaction_txtPickUpHrRange').value = null;
                document.getElementById('ctl00_cphTransaction_txtPickUpMinRange').disabled = true;
                document.getElementById('ctl00_cphTransaction_txtPickUpMinRange').value = null;
                //document.getElementById('ctl00_cphTransaction_txtDropUpHr').disabled = true;
                //document.getElementById('ctl00_cphTransaction_txtDropUpMin').disabled = true;
                //document.getElementById('ctl00_cphTransaction_txtDropOffLocation').disabled = true;
                document.getElementById('addPickUp').style.display = "none";

                var pickupCount = document.getElementById('<%=hdnPickupCount.ClientID%>').value;
                document.getElementById('addTransfers').innerHTML="";
                //for (var i = 0; i < parseInt(pickupCount) ; i++)
                //{
                //    document.getElementById('txtPickLocation-' + i).value = null;
                //    document.getElementById('txtPickUpHr-' + i).value = null;
                //    document.getElementById('txtPickUpMin-' + i).value = null;
                //}
                document.getElementById('<%=hdnPickupCount.ClientID%>').value = 0;

            }
        }
    </script>


    <script type="text/javascript" language="javascript">
        tinyMCE.init({
            mode: "textareas",
            theme: "advanced",
            width: '100%',
            theme_advanced_buttons1_add: "bullist,numlist,outdent,indent,undo,redo,fontselect,fontsizeselect",
            theme_advanced_buttons2: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_disable: "styleselect,anchor,formatselect,justifyfull,help,cleanup",
            convert_fonts_to_spans: false,
            element_format: "html",            
            content_css: "/style.css"
        });
        var Ajax;
        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        //var region;
        var country;
        var city;

        //var regions=[];
        var countries = [];
        var cities = [];
        var rc;

        function ShowCountriesList(response) {
            country = response.responseText.split('#')[0];
            var ddl = document.getElementById(country);
            if (ddl != null) {
                ddl.options.length = 0;
                //                for (var i = 0; i <= ddl.length; i++) {
                //                    ddl.remove(i);
                //                }                 
                var values = response.responseText.split('#')[1].split(',');
                var el = document.createElement("option");
                el.textContent = "Select Country";
                el.value = "0";
                ddl.add(el, 0);
                var values = response.responseText.split('#')[1].split(',');

                for (var i = 0; i < values.length; i++) {
                    var opt = values[i];
                    if (opt.length > 0 && opt.indexOf('|') > 0) {
                        var el = document.createElement("option");
                        el.textContent = opt.split('|')[0];
                        el.value = opt.split('|')[1];
                        ddl.appendChild(el);
                    }
                }

                //Set the Country and load its cities
                if (countries.length > 0 && countries[0].trim().length > 0) {
                    var id = eval(country.split('-')[1]);
                    if (id == undefined) {
                        id = 0;
                    }
                    else {
                        id = eval(id - 1);
                    }
                    ddl.value = countries[id];
                    LoadCities(country);
                }
                else {
                    ddl.value = ddl.options[0].value;
                }
            }
            //LoadCities(country);
        }

        function LoadCities(id) {
            city = 'ctl00_cphTransaction_CityText1';
            var sel = document.getElementById('<%=hdfCountries.ClientID %>');
            sel.value = document.getElementById(id).value;
            var paramList = 'requestSource=getCountryIdByCities' + '&Country=' + document.getElementById(id).value + '&id=' + city;
            var url = "CityAjax";
            Ajax.onreadystatechange = ShowCitiesList;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
            //new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: ShowCitiesList });
        }
        function ShowCitiesList(response) {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        city = Ajax.responseText.split('#')[0];
                        var ddl = document.getElementById(city);
                        if (document.getElementById('ctl00_cphTransaction_CountryText1').value != "0") {
                            //assiging selcted values to hidden filds to retain the value
                            document.getElementById('<%=hdfSelCountry.ClientID %>').value = document.getElementById('ctl00_cphTransaction_CountryText1').value;
                            // alert(document.getElementById('<%=hdfSelCountry.ClientID %>').value);
                        }
                        if (document.getElementById('ctl00_cphTransaction_CityText1').value != "0") {
                            document.getElementById('<%=hdfSelCity.ClientID %>').value = document.getElementById('ctl00_cphTransaction_CityText1').value;
                        }
                        if (ddl != null) {
                            ddl.options.length = 0;
                            //                for (var i = 0; i <= ddl.length; i++) {
                            //                    ddl.remove(i);
                            //                }

                            var el = document.createElement("option");
                            el.textContent = "Select City";
                            el.value = "0";
                            ddl.add(el, 0);
                            var values = Ajax.responseText.split('#')[1].split(',');

                            for (var i = 0; i < values.length; i++) {
                                var opt = values[i];
                                if (opt.length > 0 && opt.indexOf('|') > 0) {
                                    var el = document.createElement("option");
                                    el.textContent = opt.split('|')[0];
                                    el.value = opt.split('|')[1];
                                    ddl.appendChild(el);
                                }
                            }

                            if (cities.length > 0 && cities[0].trim().length > 0) {
                                var id = eval(city.split('-')[1]);
                                if (id == undefined) {
                                    id = 0;
                                }
                                else {
                                    id = eval(id - 1);
                                }

                                ddl.value = cities[id];
                            }
                        }
                    }
                }
            }
        }

        function SetCity(id) {
            //assiging selcted values to hidden filds to retain the value
            //document.getElementById('<%=hdfSelCity.ClientID %>').value = document.getElementById('ctl00_ContentPlaceHolder1_CityText1').value;
            //alert(document.getElementById('<%=hdfSelCity.ClientID %>').value);
            if (id != "ctl00_cphTransaction_CityText1") {
                var sel = document.getElementById('<%=hdfCities.ClientID %>');


                if (sel.value.indexOf(document.getElementById(id).value) < 0 && sel.value != "0") {
                    if (sel.value.trim().length > 0) {
                        sel.value += "," + document.getElementById(id).value;
                    }
                    else {
                        sel.value = document.getElementById(id).value;
                    }
                    //alert(sel.value);
                }
            } else {
                //assiging selcted values to hidden filds to retain the value
                if (document.getElementById('ctl00_cphTransaction_CityText1').value != "0") {
                    document.getElementById('<%=hdfSelCity.ClientID %>').value = document.getElementById('ctl00_cphTransaction_CityText1').value;
                }
                //alert(document.getElementById('<%=hdfSelCity.ClientID %>').value);
            }
        }

        function init1() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal3 = new YAHOO.widget.Calendar("cal3", "container3");
            cal3.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal3.cfg.setProperty("title", "Select From Date");
            cal3.cfg.setProperty("close", true);
            cal3.selectEvent.subscribe(setDate3);
            cal3.render();

            cal4 = new YAHOO.widget.Calendar("cal4", "container4");
            cal4.cfg.setProperty("title", "Select To Date");
            cal4.selectEvent.subscribe(setDate4);
            cal4.cfg.setProperty("close", true);
            cal4.render();
        }
        function init2() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal5 = new YAHOO.widget.Calendar("cal5", "container5");
            cal5.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal5.cfg.setProperty("title", "Select From Date");
            cal5.cfg.setProperty("close", true);
            cal5.selectEvent.subscribe(setDate5);
            cal5.render();

            cal6 = new YAHOO.widget.Calendar("cal6", "container6");
            cal6.cfg.setProperty("title", "Select To Date");
            cal6.selectEvent.subscribe(setDate6);
            cal6.cfg.setProperty("close", true);
            cal6.render();
        }
        function init3() {

            //    showReturn();

            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar
            cal7 = new YAHOO.widget.Calendar("cal7", "container7");
            cal7.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal7.cfg.setProperty("title", "Select From Date");
            cal7.cfg.setProperty("close", true);
            cal7.selectEvent.subscribe(setDate7);
            cal7.render();

            cal8 = new YAHOO.widget.Calendar("cal8", "container8");
            cal8.cfg.setProperty("title", "Select To Date");
            cal8.selectEvent.subscribe(setDate8);
            cal8.cfg.setProperty("close", true);
            cal8.render();
        }
        function init4() {
            cal1 = new YAHOO.widget.Calendar("cal1", "callContainer");
            cal1.selectEvent.subscribe(setDate9);
            cal1.cfg.setProperty("close", true);
            cal1.render();
        }


        function setDate9() {
            var date1 = cal1.getSelectedDates()[0];
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            var availDateFrom = new Date();
            var availDateTo = new Date();
            var unAvDate = new Date();
            availDateFrom = document.getElementById('<% = txtFrom.ClientID %>').value;
            availDateTo = document.getElementById('<% = txtTo.ClientID %>').value;
            unAvDate = day + "/" + (month) + "/" + date1.getFullYear(); //document.getElementById('<% = Date.ClientID %>').value;
            var d1 = availDateFrom.split("/");
            var d2 = availDateTo.split("/");
            var c = unAvDate.split("/");

            var from = new Date(d1[2], d1[1] - 1, d1[0]);  // -1 because months are from 0 to 11
            var to = new Date(d2[2], d2[1] - 1, d2[0]);
            var check = new Date(c[2], c[1] - 1, c[0]);
            if (check < from || check > to) {
                document.getElementById('lblErrorMessage').style.display = "block";
                document.getElementById('lblErrorMessage').innerHTML = "UnAvail Dates should be in between from Date and To Date ";
                return false;
            }
            else {
                document.getElementById('lblErrorMessage').style.display = "none";
                document.getElementById('lblErrorMessage').innerHTML = "";
            }


            document.getElementById('<% = Date.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();


            var counter = document.getElementById("<%=hdfUnavailDateCounter.ClientID%>").value;
            counter = counter - 1;

            document.getElementById('txtUnAvailDatesText-' + counter).value = document.getElementById('<% = Date.ClientID %>').value;
            document.getElementById('<%=txtUnavailableDays.ClientID%>').value = counter;
            document.getElementById('<%=hdnUnavailableDays.ClientID %>').value = counter;
            document.getElementById('<% = Date.ClientID %>').value = "";
            document.getElementById('callContainer').style.display = "none";
        }

        function ShowCalender(container) {
            if (container === 'callContainer') {
                if (document.getElementById('ctl00_cphTransaction_txtFrom').value.length == 0) {
                    document.getElementById('lblErrorMessage').innerHTML = "Please select From Date and To Date";
                    document.getElementById('lblErrorMessage').style.display = "block";
                    document.getElementById('lblErrorMessage').style.color = "red";
                }
                else if (document.getElementById('ctl00_cphTransaction_txtTo').value.length == 0) {
                    document.getElementById('lblErrorMessage').innerHTML = "Please select From Date and To Date";
                    document.getElementById('lblErrorMessage').style.display = "block";
                    document.getElementById('lblErrorMessage').style.color = "red";
                }
                else {
                    var containerId = container;
                    if (document.getElementById(containerId).style.display == "none") {
                        document.getElementById(containerId).style.display = "block"
                    }
                }

            }
            else {
                var containerId = container;
                if (document.getElementById(containerId).style.display == "none") {
                    document.getElementById(containerId).style.display = "block"
                }
            }
        }
        //    function showCalendar1() {
        //        document.getElementById('container2').style.display = "none";
        //        document.getElementById('container1').style.display = "block";
        //        document.getElementById('container2').style.display = "none";
        //        document.getElementById('container1').style.display = "block"

        //    }
        function showCalendar3() {
            //        document.getElementById('container4').style.display = "none";
            //        document.getElementById('container3').style.display = "block";
            document.getElementById('container4').style.display = "none";
            document.getElementById('container3').style.display = "block";
            document.getElementById('lblErrorMessage').innerHTML = "";
            document.getElementById('lblErrorMessage').style.display = "none";
        }
        function showCalendar5() {

            document.getElementById('container6').style.display = "none";
            document.getElementById('container5').style.display = "block";
            //        document.getElementById('container6').style.display = "none";
            //        document.getElementById('container5').style.display = "block";

        }
        function showCalendar7() {
            //        document.getElementById('container8').style.display = "none";
            //        document.getElementById('container7').style.display = "block";
            document.getElementById('container8').style.display = "none";
            document.getElementById('container7').style.display = "block";
        }
        var departureDate = new Date();
        function showCalendar2() {

            //vij document.getElementById('container1').style.display = "none";
            document.getElementById('container1').style.display = "none";
            cal1.hide();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function showCalendar4() {

            document.getElementById('lblErrorMessage').style.display = "none";
            document.getElementById('lblErrorMessage').innerHTML = "";
            document.getElementById('container3').style.display = "none";
            cal3.hide();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal4.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal4.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal4.render();
            }
            document.getElementById('container4').style.display = "block";
        }

        function showCalendar8() {
            //document.getElementById('container7').style.display = "none";
            document.getElementById('container7').style.display = "none";
            cal5.hide();
            // setting Calender2 min date acoording to calendar1 selected date
            //var date1 = document.getElementById("txtPickUpHr.ClientID%>").value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal8.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal8.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal8.render();
            }
            document.getElementById('container8').style.display = "block";
        }

        //YAHOO.util.Event.addListener(window, "load", init);

        function ShowTab(val) {
            //alert(val);
            $('a[href="#' + val + '"]').tab('show')

        }
        function setDate3() {
            var date1 = cal3.getSelectedDates()[0];
            //alert(date1);
            document.getElementById('IShimFrame').style.display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();
            var thisTime = this.today.getHours();
            var thisMinutes = this.today.getMinutes();
            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                return false;
            }
            departureDate = cal3.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();
            var time = "";
            var timePM = "";

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }
            if (thisMinutes < 10) {
                thisMinutes = "0" + thisMinutes
            }
            if (thisTime > 11) {
                timePM = ("PM")
            } else {
                timePM = ("AM")
            }
            time = (thisTime + ":" + thisMinutes + timePM + " ");
            document.getElementById("<%=txtFrom.ClientID%>").value = day + "/" + (month) + "/" + date1.getFullYear();
            document.getElementById("<%=txtFromTime.ClientID%>").value = time;

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal3.hide();

        }
        function setDate4() {
            var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select from date.";
                return false;
            }

            var date2 = cal4.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid To Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
            this.today = depdate;
            var thisTime = this.today.getHours();
            var thisMinutes = this.today.getMinutes();
            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "To Date should be greater than  or equal to date of From (" + date1 + ")";
                return false;
            }

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }
            if (thisMinutes < 10) {
                thisMinutes = "0" + thisMinutes
            }
            if (thisTime > 11) {
                timePM = ("PM")
            } else {
                timePM = ("AM")
            }
            time = (thisTime + ":" + thisMinutes + timePM + " ");

            document.getElementById("<%=txtTo.ClientID%>").value = day + "/" + month + "/" + date2.getFullYear();
            document.getElementById("<%=txtToTime.ClientID%>").value = time;

            cal4.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init1);

        function setDate5() {


        }
        function setDate6() {
        }
        YAHOO.util.Event.addListener(window, "load", init2);

        function setDate7() {
            var date1 = cal7.getSelectedDates()[0];

            document.getElementById('IShimFrame').style.display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();
            var thisTime = this.today.getHours();
            var thisMinutes = this.today.getMinutes();
            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                return false;
            }
            departureDate = cal7.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();
            var time = "";
            var timePM = "";

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }
            if (thisMinutes < 10) {
                thisMinutes = "0" + thisMinutes
            }
            if (thisTime > 11) {
                timePM = ("PM")
            } else {
                timePM = ("AM")
            }
            time = (thisTime + ":" + thisMinutes + timePM + " ");
            //document.getElementById("txtPickUpHr.ClientID%>").value = day + "/" + (month) + "/" + date1.getFullYear();
            //document.getElementById("txtPickUpMin.ClientID%>").value = time;

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal7.hide();

        }
        function setDate8() {

            //var date1 = document.getElementById("txtPickUpHr.ClientID%>").value;

            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select from date.";
                return false;
            }

            var date2 = cal8.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid To Date";
                return false;
            }

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
            var thisTime = this.today.getHours();
            var thisMinutes = this.today.getMinutes();
            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "To Date should be greater than  or equal to date of From (" + date1 + ")";
                return false;
            }

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }
            if (thisMinutes < 10) {
                thisMinutes = "0" + thisMinutes
            }
            if (thisTime > 11) {
                timePM = ("PM")
            } else {
                timePM = ("AM")
            }
            time = (thisTime + ":" + thisMinutes + timePM + " ");

            document.getElementById("=txtDropUpHr.ClientID%>").value = day + "/" + month + "/" + date2.getFullYear();
            document.getElementById("txtDropUpMin.ClientID%>").value = time;

            cal8.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init3);

        YAHOO.util.Event.addListener(window, "load", init4);

        function validateDtlAddUpdate(action, id) {
            try {

                var msg = '';
                var rowId = id.substring(0, id.lastIndexOf('_'));
                if (action == 'I') {

                    if (document.getElementById(rowId + '_FTtxtLabel').value == '') msg += '\n Label cannot be blank!';
                    if (document.getElementById(rowId + '_FTddlControl').selectedIndex <= 0) msg += '\n Please select a Control! ';
                    if (document.getElementById(rowId + '_FTddlControl').value == 'L') {
                        if (document.getElementById(rowId + '_FTtxtSqlQuery').value == '') msg += 'SqlQuery cannot be blank!';
                    }
                    if (document.getElementById(rowId + '_FTddlDatatype').selectedIndex <= 0) msg += '\n Please select a DataType! ';
                    if (document.getElementById(rowId + '_FTddlMandatory').selectedIndex <= 0) msg += '\n Please select a MandatoryStatus!';
                    if (document.getElementById(rowId + '_FTtxtOrder').value == '') msg += '\n Order cannot be blank!';

                }
                else if (action == 'U')//Update
                {
                    if (document.getElementById(rowId + '_EITtxtLabel').value == '') msg += 'Label cannot be blank!';
                    //if (document.getElementById(rowId + '_EITddlControl').selectedIndex <= 0) msg += '\n Please select a Control! ';  
                    if (document.getElementById(rowId + '_EITddlControl').value == 'L') {
                        if (document.getElementById(rowId + '_EITtxtSqlQuery').value == '') msg += 'SqlQuery cannot be blank!';
                    }
                    //if (document.getElementById(rowId + '_EITddlDatatype').selectedIndex <= 0) msg += '\n Please select a DataType! ';
                    //if (document.getElementById(rowId + '_EITddlMandatory').selectedIndex <= 0) msg += '\n Please select a MandatoryStatus!';
                    if (document.getElementById(rowId + '_EITtxtOrder').value == '') msg += '\n Order cannot be blank!';
                }
                if (msg != '') {
                    alert(msg);
                    return false;
                }
                return true;
            }
            catch (e) {
                return true;
            }
        }
        function validateDtlAddUpdate1(action, id) {
            try {

                var msg = '';
                var rowId = id.substring(0, id.lastIndexOf('_'));
                if (action == 'I') {
                    if (document.getElementById(rowId + '_FTtxtLabel').value == '') msg += '\n Label cannot be Blank!';
                    if (document.getElementById(rowId + '_FTtxtSupplierCost').value == '') msg += '\n Supplier Cost cannot be Blank!';
                    if (document.getElementById(rowId + '_FTtxtTax').value == '') msg += '\n Tax cannot be Blank!';
                    if (document.getElementById(rowId + '_FTtxtMarkup').value == '') msg += '\n Markup cannot be Blank!';
                    if (document.getElementById(rowId + '_FTtxtMinPax').value == '') {
                        msg += '\n MinPax cannot be Blank!';
                    }
                    else if (parseInt(document.getElementById(rowId + '_FTtxtMinPax').value) <= 0) {
                        msg += '\n At least 1 min pax is Required!';
                    }
                }
                else if (action == 'U')//Update
                {
                    if (document.getElementById(rowId + '_EITtxtLabel').value == '') msg += '\n Label cannot be Blank!';
                    if (document.getElementById(rowId + '_EITtxtSupplierCost').value == '') msg += '\n Supplier Cost cannot be Blank!';
                    if (document.getElementById(rowId + '_EITtxtTax').value == '') msg += '\n Tax cannot be Blank!';
                    if (document.getElementById(rowId + '_EITtxtMarkup').value == '') msg += '\n Markup cannot be Blank!';
                    if (document.getElementById(rowId + '_EITtxtMinPax').value == '') {
                        msg += '\n MinPax cannot be Blank!';
                    }
                    else if (parseInt(document.getElementById(rowId + '_EITtxtMinPax').value) <= 0) {
                        msg += '\n At least 1 min pax is Required!';
                    }
                }
                if (msg != '') {
                    alert(msg);
                    return false;
                }
                return true;
            }
            catch (e) {
                return true;
            }
        }

        function trim(stringToTrim) {
            return stringToTrim.replace(/^\s+|\s+$/g, "");
        }

        function Save() {
            document.getElementById('errMess').style.display = "none";

            if (document.getElementById("<%=ddlAgent.ClientID%>").value <= 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select agent from the list!";
                return false;
            }
            if (document.getElementById("<%=txtActivityName.ClientID%>").value == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "ActivityName cannot be blank!"
                return false;
            }

            if (document.getElementById("<%=txtStockInHand.ClientID%>").value == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Stock in Hand cannot be Blank !";
                return false;
            }

            if (document.getElementById("<%=txtStartingFrom.ClientID%>").value == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "StartingFrom cannot be blank!";
                return false;
            }

            if (Trim(document.getElementById('<%=CountryText1.ClientID %>').value) == "0") {

                // document.getElementById('errorMessage').innerHTML = "Please select Country for the Fixed Departure.";
                //document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select Country from the list .";
                return false;
            }
            else if (Trim(document.getElementById('<%=CityText1.ClientID %>').value) == "0") {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select City from the list .";
                return false;
            }
            var chklist = document.getElementById('<%= chkTheme.ClientID %>');
            var chkListinputs = chklist.getElementsByTagName("input");
            var count = 0;
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    count = count + 1;
                }
            }
            if (count == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Atleast one theme should be selected !";
                $("#apackageTab1").trigger('click');
                return false;
            }

            var regexStr = /<\S[^><]*>/g;
            var regexStr1 = /&nbsp;/g;
            var regexStr2 = /<BR>/g;
            var changed = window.frames.mce_editor_0.document.body.innerHTML.replace(regexStr, '');
            var changed = changed.replace(regexStr1, '');

            if (trim(changed) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter Intoduction.";
                return false;
            }

            changed = window.frames.mce_editor_1.document.body.innerHTML.replace(regexStr, '');
            var changed = changed.replace(regexStr1, '');
            var changed = changed.replace(regexStr2, '');

            if (trim(changed) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter OverView.";
                return false;
            }

            var startHr = "";
            var startMin = "";
            var endHr = "";
            var endMin = "";


            var changed = changed.replace(regexStr2, '');
            if (document.getElementById("<%=txtStartFromHr.ClientID%>").value.length > 0) {
                startHr = parseInt(document.getElementById("<%=txtStartFromHr.ClientID%>").value);
            }
            if (document.getElementById("<%=txtStartTimeMin.ClientID%>").value.length > 0) {
                startMin = parseInt(document.getElementById("<%=txtStartTimeMin.ClientID%>").value);
            }
            if (document.getElementById("<%=txtEndToHr.ClientID%>").value.length > 0) {
                endHr = parseInt(document.getElementById("<%=txtEndToHr.ClientID%>").value);
            }
            if (document.getElementById("<%=txtEndTimeMin.ClientID%>").value.length > 0) {
                endMin = parseInt(document.getElementById("<%=txtEndTimeMin.ClientID%>").value);
            }
            if (document.getElementById("<%=txtStartFromHr.ClientID%>").value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Start from Hour cannot be Blank !";
                return false;
            }
            else if (document.getElementById("<%=txtStartTimeMin.ClientID%>").value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Start from Minut cannot be Blank !";
                return false;
            }
            else if (document.getElementById("<%=txtEndToHr.ClientID%>").value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "End to Hour cannot be Blank !";
                return false;
            }
            else if (document.getElementById("<%=txtEndTimeMin.ClientID%>").value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "End to Minut cannot be Blank !";
                return false;
            }
            else if (startHr > 23 || endHr > 23) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Invalid Time!";
                document.getElementById("<%=txtDuration.ClientID%>").value = "Invalid Time!";
                return false;
            }
            else if (startMin > 59 || endMin > 59) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Invalid Time!";
                document.getElementById("<%=txtDuration.ClientID%>").value = "Invalid Time!";
                return false;
            }
            else if (startHr > endHr) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Start Time can not be later than End Time!";
                document.getElementById("<%=txtDuration.ClientID%>").value = "Start Time can not be later than End Time!";
                return false;
            }
            else if (startHr == endHr && startMin > endMin) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Start Time can not be later than End Time!";
                document.getElementById("<%=txtDuration.ClientID%>").value = "Start Time can not be later than End Time!";
                return false;
            }

            changed = window.frames.mce_editor_2.document.body.innerHTML.replace(regexStr, '');
            var changed = changed.replace(regexStr1, '');
            var changed = changed.replace(regexStr2, '');
            if (trim(changed) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter Itinerary details.";
                $("#apackageTab1").trigger('click');
                return false;
            }
            //if (document.getElementById('ctl00_cphTransaction_txtSupplierEmail') != null && document.getElementById('ctl00_cphTransaction_txtSupplierEmail').value != "" && document.getElementById('ctl00_cphTransaction_txtSupplierEmail').value.trim().length <= 0) {
            //    document.getElementById('errMess').style.display = "block";
            //    document.getElementById('errorMessage').innerHTML = "Please enter email.";
            //    $("#apackageTab1").trigger('click');
            //    return false;
            //}
            if (document.getElementById('ctl00_cphTransaction_txtSupplierEmail') != null && document.getElementById('ctl00_cphTransaction_txtSupplierEmail').value != "" && !ValidEmail.test(document.getElementById('ctl00_cphTransaction_txtSupplierEmail').value)) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter valid email.";
                $("#apackageTab1").trigger('click');
                return false;
            }
            if (document.getElementById("<%=hdfPriceCount.ClientID%>").value == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Price Details cannot be blank!";
                $("#apackageTab2").trigger('click');
                return false;
            }
            if (document.getElementById("<%=hdfPrice.ClientID%>").value != 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Price Details in Edit Mode!";
                $("#apackageTab2").trigger('click');
                return false;
            }
            if (document.getElementById("<%=hdfUploadYNImage1.ClientID%>").value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Image 1 should be uploaded !";
                $("#apackageTab3").trigger('click');
                return false;
            }
            if (document.getElementById("<%=hdfUploadYNImage2.ClientID%>").value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Image 2 should be uploaded !";
                $("#apackageTab3").trigger('click');
                return false;
            }
            if (document.getElementById("<%=hdfUploadYNImage2.ClientID%>").value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Image 3 should be uploaded !";
                $("#apackageTab3").trigger('click');
                return false;
            }
            if (!ValidDate(document.getElementById('<%= txtFrom.ClientID %>').value)) {
                document.getElementById('errorMessage').innerHTML = "Please fill valid Available Start To date";
                document.getElementById('errMess').style.display = "block";
                $("#apackageTab4").trigger('click');
                return false;
            }
            if (document.getElementById("<%=txtTo.ClientID%>").value == "" || document.getElementById("<%=txtFromTime.ClientID%>").value == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Invalid Available Start From !";
                return false;
            }

            if (document.getElementById('ctl00_cphTransaction_ddlTransferInclude').value == "Yes") {
                if (document.getElementById('ctl00_cphTransaction_txtPickLocation').value == null || document.getElementById('ctl00_cphTransaction_txtPickLocation').value == "") {
                    document.getElementById('errorMessage').innerHTML = "Please fill PickupLocation.";
                    $("#apackageTab1").trigger('click');
                    document.getElementById('errMess').style.display = "block";
                    return false;
                }
           
                //if (document.getElementById('ctl00_cphTransaction_txtPickUpHr').value == null || document.getElementById('ctl00_cphTransaction_txtPickUpHr').value == "") {
                //    document.getElementById('errorMessage').innerHTML = "Please fill PickupTime.";
                //    $("#apackageTab1").trigger('click');
                //    document.getElementById('errMess').style.display = "block";
                //    return false;
                //}
                //if (document.getElementById('ctl00_cphTransaction_txtPickUpMin').value == null || document.getElementById('ctl00_cphTransaction_txtPickUpMin').value == "") {
                //    document.getElementById('errorMessage').innerHTML = "Please fill PickupTime.";
                //    $("#apackageTab1").trigger('click');
                //    document.getElementById('errMess').style.display = "block";
                //    return false;
                //}
            }

            var pickupCount = document.getElementById('<%=hdnPickupCount.ClientID %>').value;
            if (pickupCount >= 0) {
                for (var z = 1; z <= pickupCount; z++) {
                    if (document.getElementById('txtPickLocation-' + z) == null || document.getElementById('txtPickLocation-' + z).value == null || document.getElementById('txtPickLocation-' + z).value == "") {
                        document.getElementById('errorMessage').innerHTML = "Please fill PickupLocation.";
                        $("#apackageTab1").trigger('click');
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                    //if (document.getElementById('txtPickUpHr-' + z).value == null || document.getElementById('txtPickUpHr-' + z).value == "") {
                    //    document.getElementById('errorMessage').innerHTML = "Please fill PickupTime.";
                    //    $("#apackageTab1").trigger('click');
                    //    document.getElementById('errMess').style.display = "block";
                    //    return false;
                    //}
                    //if (document.getElementById('txtPickUpMin-' + z).value == null || document.getElementById('txtPickUpMin-' + z).value == "") {
                    //    document.getElementById('errorMessage').innerHTML = "Please fill PickupTime.";
                    //    $("#apackageTab1").trigger('click');
                    //    document.getElementById('errMess').style.display = "block";
                    //    return false;
                    //}
                }
            }

           // document.getElementById('divSubmit').style.display = "none";
            var exclValue = eval(document.getElementById('ExclusionsCurrent').value); // its value is +1 the current
            for (var i = 1; i < exclValue; i++) {
                //alert(i);
                if (Trim(document.getElementById('ExclusionsText-' + i).value) == "") {
                    if (i == 1) {
                        document.getElementById('errorMessage').innerHTML = "Please enter Exclusions for the Activity.";
                        $("#apackageTab4").trigger('click');
                        $("#apackageTab5").trigger('click');
                        //document.getElementById('divSubmit').style.display = "none";
                        //document.getElementById('SaveButton.ClientID %>').style.display = "block";
                        //document.getElementById('divTotal').style.display = "block";
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                    else {
                        document.getElementById('errorMessage').innerHTML = "Please fill all Exclusions for the Activity.";
                        $("#apackageTab5").trigger('click');
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                }
            }

            var inclValue = eval(document.getElementById('InclusionsCurrent').value); // its value is +1 the current
            for (var i = 1; i < inclValue; i++) {
                //alert(i);
                if (Trim(document.getElementById('InclusionsText-' + i).value) == "") {
                    if (i == 1) {
                        document.getElementById('errorMessage').innerHTML = "Please enter Inclusions for the Activity.";
                        $("#apackageTab4").trigger('click');
                        $("#apackageTab5").trigger('click');
                        //document.getElementById('divSubmit').style.display = "none";
                        //document.getElementById('SaveButton.ClientID %>').style.display = "block";
                        //document.getElementById('divTotal').style.display = "block";
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                    else {
                        document.getElementById('errorMessage').innerHTML = "Please fill all Inclusions for the Activity.";
                        $("#apackageTab5").trigger('click');
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                }
            }

            var cancelPolicyCurrent = eval(document.getElementById('CancellPolicyCurrent').value);
            for (var j = 1; j < cancelPolicyCurrent; j++) {
                if (Trim(document.getElementById('CancellPolicyText-' + j).value) == "") {
                    if (j == 1) {
                        document.getElementById('errorMessage').innerHTML = "Please enter Cancellation Policy for the Activity.";
                        $("#apackageTab4").trigger('click');
                        $("#apackageTab5").trigger('click');
                        // document.getElementById('divSubmit').style.display = "none";
                        //document.getElementById('SaveButton.ClientID %>').style.display = "block";
                        //document.getElementById('divTotal').style.display = "block";
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                    else {
                        document.getElementById('errorMessage').innerHTML = "Please fill all Cancellation Policy for the Activity.";
                        $("#apackageTab5").trigger('click');
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                }
            }

            if (document.getElementById("<%=hdfAFCDetails.ClientID%>").value != 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Activity Flex Details in Edit Mode!";
                return false;
            }

            document.getElementById('savePage').value = "true";
            document.getElementById('<%=SaveButton.ClientID%>').click();
        }

        function validateSave() {
            $("#apackageTab5").trigger('click');

            var exclValue = eval(document.getElementById('ExclusionsCurrent').value); // its value is +1 the current

            for (var i = 1; i < exclValue; i++) {
                //alert(i);
                if (Trim(document.getElementById('ExclusionsText-' + i).value) == "") {
                    if (i == 1) {
                        document.getElementById('errorMessage').innerHTML = "Please enter Exclusions for the Activity.";
                        $("#apackageTab5").trigger('click');
                        document.getElementById('divSubmit').style.display = "none";
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                    else {
                        document.getElementById('errorMessage').innerHTML = "Please fill all Exclusions for the Activity.";
                        $("#apackageTab5").trigger('click');
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                }
            }

            var incValue = eval(document.getElementById('InclusionsCurrent').value); // its value is +1 the current

            for (var i = 1; i < incValue; i++) {
                if (Trim(document.getElementById('InclusionsText-' + i).value) == "") {
                    if (i == 1) {
                        document.getElementById('errorMessage').innerHTML = "Please enter Inclusions for Activity.";
                        $("#apackageTab5").trigger('click');
                        document.getElementById('divSubmit').style.display = "none";
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                    else {
                        document.getElementById('errorMessage').innerHTML = "Please fill all Inclusions for Activity.";
                        $("#apackageTab5").trigger('click');
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                }
            }
        }

        //    function Save() {
        //        if (getElement('txtActivityName').value == '') addMessage('ActivityName cannot be blank!', '');
        //        if (getElement('txtStartingFrom').value == '') addMessage('StartingFrom cannot be blank!', '');

        //        if (getElement('hdfAFCDetails').value != "0") addMessage('ACF Details in Edit Mode!', '');
        //        if (getElement('hdfPrice').value != "0") addMessage('ACF Price in Edit Mode!', '');

        //        if (getMessage() != '') {
        //            alert(getMessage());
        //            clearMessage(); return false;
        //        }
        //    }

        //    function OnTimeExit(dcId) {
        //        var rowId = dcId.substring(0, dcId.lastIndexOf('_'));
        //        var svcRowId = rowId.substring(0, rowId.lastIndexOf('_'));
        //        var id = rowId.split("_");
        //        var inDate = "";
        //        var outDate = "";
        //        var unit = "";

        //        if (id[4] == '_dcEndTo') {
        //            inDate = GetDateTimeObject(svcRowId + '_dcStartFrom');
        //            outDate = GetDateTimeObject(rowId);
        //            unit = outDate - inDate;
        //            var noOfDays = ((outDate.getTime() - inDate.getTime()) / (1000 * 60 * 60 * 24));
        //            unit = Math.floor(noOfDays / 24);
        //            document.getElementById(svcRowId + '_txtDuration').value = unit;
        //        }

        //    }
        // 

        function AddTextBox(divName) {
            //alert(divName);
            var idCount = eval(document.getElementById(divName + 'Counter').value);
            var idCurrent = eval(document.getElementById(divName + 'Current').value);
            //alert(idCount);
            if (document.getElementById(divName + "-" + idCurrent)) {
                document.getElementById(divName + "-" + idCurrent).style.display = "block";
                document.getElementById(divName + "Img-" + idCurrent).style.display = "block";
                Remove(divName + "Img-" + eval(idCurrent - 1), 'img');
                idCurrent++;
                document.getElementById(divName + 'Current').value = idCurrent;
            }
            else {
                var stringHTML = "";
                var newdiv = document.createElement('div');
                if (divName == "Inclusions") {
                    stringHTML += "<p class=\"fleft width-100 padding-top-5\" style=\"\" id=\"" + divName + "-" + idCount + "\">";
                }
                if (divName == "CancellPolicy" || divName == "ThingsToBring" || divName == "Exclusions") {
                    stringHTML += "<p class=\"fleft width-100 padding-top-5\" style=\"\" id=\"" + divName + "-" + idCount + "\">";
                }
                stringHTML += "<span class=\"fleft width-300\"><input type=\"text\" id=\"" + divName + "Text-" + idCount + "\" name=\"" + divName + "Text-" + idCount + "\" class=\"form-control\"  onkeypress=\"return isAlphaNumeric(event);\"/></span>";
                stringHTML += "<i class=\"fleft margin-left-10\" id=\"" + divName + "Img-" + idCount + "\"><img src=\"Images/delete.gif\" alt=\"Remove\" onclick=\"javascript:Remove('" + divName + "-" + idCount + "','text')\"/></i>";
                stringHTML += "</p>";
                newdiv.innerHTML = stringHTML;
                document.getElementById(divName + "Div").appendChild(newdiv)
                //document.getElementById(divName+"Div").innerHTML += stringHTML;       
                Remove(divName + "Img-" + eval(idCount - 1), 'img');

                idCount++;
                document.getElementById(divName + 'Counter').value = idCount;
                idCurrent++;
                document.getElementById(divName + 'Current').value = idCurrent;

                if (divName == "Inclusions") {
                    document.getElementById("<%=hdfinclCounter.ClientID%>").value = idCount;
                }
                else if (divName == "Exclusions") {
                    document.getElementById("<%=hdfExclCounter.ClientID%>").value = idCount;
                }
                else if (divName == "CancellPolicy") {
                    document.getElementById("<%=hdfCanPolCounter.ClientID%>").value = idCount;
                }
                else if (divName == "ThingsToBring") {
                    document.getElementById("<%=hdfThingsCounter.ClientID%>").value = idCount;
                }

                //        alert(document.getElementById('InclusionsCounter').value);
            }
        }

        function AddDateMore(divName) {
            var idCount = eval(document.getElementById(divName + 'Counter').value);
            var idCurrent = eval(document.getElementById(divName + 'Current').value);
            if (document.getElementById(divName + "-" + idCurrent)) {
                document.getElementById(divName + "-" + idCurrent).style.display = "block";
                document.getElementById(divName + "Img-" + idCurrent).style.display = "block";
                Remove(divName + "Img-" + eval(idCurrent), 'img');
                idCurrent++;
                document.getElementById(divName + 'Current').value = idCurrent;
            }
            else {
                if (document.getElementById('txtUnAvailDatesText-' + (idCurrent - 1)).value.length != 0 && divName == "UnAvailDates") {
                    var stringHTML = "";
                    var newdiv = document.createElement('div');
                    if (divName == "UnAvailDates") {
                        stringHTML += "<p class=\"fleft width-100 padding-top-5\" style=\"\" id=\"" + divName + "-" + idCount + "\">";
                    }
                    //        stringHTML += "<div class=\"input-group fleft\"><div class=\"input-group-addon\"><span class=\"glyphicon glyphicon-calendar\"></span></div><input type=\"text\" id=\"txt" + divName + "Text-" + idCount + "\" name=\"txt" + divName + "Text-" + idCount + "\" class=\"fleft form-control undatewidth\" /></div>";
                    stringHTML += "<span class=\"fleft width-300\"><input type=\"text\" id=\"txt" + divName + "Text-" + idCount + "\" name=\"txt" + divName + "Text-" + idCount + "\" class=\"width-300 fleft form-control\"  readonly=\"false\"  /></span>";
                    stringHTML += "<i class=\"fleft margin-left-10\" id=\"" + divName + "Img-" + idCount + "\"><img src=\"Images/delete.gif\" alt=\"Remove\" onclick=\"javascript:Remove('" + divName + "-" + idCount + "','text')\"/></i>";
                    stringHTML += "</p>";
                    newdiv.innerHTML = stringHTML;
                    document.getElementById(divName + "Div").appendChild(newdiv)
                    //document.getElementById(divName+"Div").innerHTML += stringHTML;       
                    Remove(divName + "Img-" + eval(idCount - 1), 'img');

                    idCount++;
                    document.getElementById(divName + 'Counter').value = idCount;
                    idCurrent++;
                    document.getElementById(divName + 'Current').value = idCurrent;
                    if (divName == "UnAvailDates") {
                        document.getElementById("<%=hdfUnavailDateCounter.ClientID%>").value = idCount;
                    }
                }
                else {
                    document.getElementById('lblErrorMessage').innerHTML = "Please select the unavailable date";
                    document.getElementById('lblErrorMessage').style.display = "block";
                    document.getElementById('lblErrorMessage').style.color = "red";
                }
            }
        }

//   function RemoveAll(id, count)
//  {
//  alert('hi');
// alert(id);
// alert(count);
////    for(var i=1; i<=count;i++)
////    {
////         document.getElementById(id+i).style.display="block";   
////    }
//        
//  }
        function Remove(id, val) {

            if (id.split('-')[1] != 1) {
                //alert(document.getElementById(id));
                document.getElementById(id).value = "";
                document.getElementById(id).style.display = "none";
                //alert(id);
                if (val == "text") {

                    var count = eval(id.split('-')[1]);
                    var idSplit = id.split('-')[0];
                    //alert(id);
                    if (eval(count - 1) != 1) {
                        document.getElementById(idSplit + "Img-" + eval(count - 1)).style.display = "block";
                    }
                    var idCurrent = eval(document.getElementById(id.split('-')[0] + 'Current').value);
                    idCurrent--;
                    document.getElementById(id.split('-')[0] + 'Current').value = idCurrent;
                    document.getElementById('<%=txtUnavailableDays.ClientID %>').value = (parseInt(idCurrent) - 1);
                    document.getElementById('<%= hdnUnavailableDays.ClientID %>').value = (parseInt(idCurrent) - 1);
                    if (idSplit == "Inclusions") {
                        document.getElementById("<%=hdfinclCounter.ClientID%>").value = idCurrent;
                        document.getElementById("InclusionsText-" + count).value = "";
                    }
                    else if (idSplit == "Exclusions") {
                        document.getElementById("<%=hdfExclCounter.ClientID%>").value = idCurrent;
                        document.getElementById("ExclusionsText-" + count).value = "";

                    }
                    else if (idSplit == "CancellPolicy") {
                        document.getElementById("<%=hdfCanPolCounter.ClientID%>").value = idCurrent;
                        document.getElementById("CancellPolicyText-" + count).value = "";
                    }
                    else if (idSplit == "ThingsToBring") {
                        document.getElementById("<%=hdfThingsCounter.ClientID%>").value = idCurrent;
                        document.getElementById("ThingsToBringText-" + count).value = "";
                    }
                    else if (idSplit == "UnAvailDates") {
                        document.getElementById("<%=hdfUnavailDateCounter.ClientID%>").value = idCurrent;
                        document.getElementById('txtUnAvailDatesText-' + id.split('-')[1]).value = "";
                    }

                }
            }
        }

        function CalcDuration(hrOrMin) {

            var startHr = parseInt(document.getElementById("<%=txtStartFromHr.ClientID%>").value);
            //alert(startHr);     
            var startMin = parseInt(document.getElementById("<%=txtStartTimeMin.ClientID%>").value);
            var endHr = parseInt(document.getElementById("<%=txtEndToHr.ClientID%>").value);
            var endMin = parseInt(document.getElementById("<%=txtEndTimeMin.ClientID%>").value);


            if (startHr > 23 || endHr > 23) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Invalid Time!";
                document.getElementById("<%=txtDuration.ClientID%>").value = "Invalid Time!";
                return false;
            }
            else if (startMin > 59 || endMin > 59) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Invalid Time!";
                document.getElementById("<%=txtDuration.ClientID%>").value = "Invalid Time!";
                return false;
            }
            else if (startHr > endHr) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Start Time can not be later than End Time!";
                document.getElementById("<%=txtDuration.ClientID%>").value = "Start Time can not be later than End Time!";
                return false;
            }
            else if (startHr == endHr && startMin > endMin) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Start Time can not be later than End Time!";
                document.getElementById("<%=txtDuration.ClientID%>").value = "Start Time can not be later than End Time!";
                return false;
            }
            else {
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
            }

            // alert(parseInt(endHr)+parseInt(startHr));
            var durationHr = endHr - startHr;
            var durationMin = endMin - startMin;
            if (durationMin < 0) {
                durationHr = durationHr - 1
                durationMin = 60 + durationMin;
            }
            var duration = durationHr + 'Hrs ' + durationMin + 'Min';
            // alert(duration);   
            document.getElementById("<%=txtDuration.ClientID%>").value = duration;
            //document.getElementById("<%=txtDuration.ClientID%>").disabled=true;
        }

        function ValidateTime(hrOrMin, value, id) {
            if (hrOrMin == 'H' && value > 23) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Invalid Time!";
                document.getElementById(id).value = "";
                return false;
            }
            else if (hrOrMin == 'M' && value > 59) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Invalid Time!";
                document.getElementById(id).value = "";
                return false;
            }
            else {
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
            }
        }

        function getTotalAmount(rowId, action) {
            var id = rowId.substring(0, rowId.lastIndexOf('_') + 1);
            //var quantity = parseFloat(isNaN(document.getElementById(id+'FTtxtQty').value));// document.getElementById(id+'FTtxtQty').value;
            var supplierCost = 0;
            var tax = 0;
            var markup = 0;
            var totalAmount = 0;
            if (action == "I") {
                supplierCost = parseFloat((isNaN(document.getElementById(id + 'FTtxtSupplierCost').value) || document.getElementById(id + 'FTtxtSupplierCost').value == '') ? '0' : document.getElementById(id + 'FTtxtSupplierCost').value);
                tax = parseFloat((isNaN(document.getElementById(id + 'FTtxtTax').value) || document.getElementById(id + 'FTtxtTax').value == '') ? '0' : document.getElementById(id + 'FTtxtTax').value);
                markup = parseFloat((isNaN(document.getElementById(id + 'FTtxtMarkup').value) || document.getElementById(id + 'FTtxtMarkup').value == '') ? '0' : document.getElementById(id + 'FTtxtMarkup').value);

                totalAmount = supplierCost + tax + markup;
                document.getElementById(id + 'FTtxtAmount').value = totalAmount;
                document.getElementById(id + 'FTtxtAmount').value = totalAmount.toFixed(2);
                //alert(document.getElementById(id+'FTtxtRate').value);
            }
            else if (action == "U") {

                supplierCost = parseFloat((isNaN(document.getElementById(id + 'EITtxtSupplierCost').value) || document.getElementById(id + 'EITtxtSupplierCost').value == '') ? '0' : document.getElementById(id + 'EITtxtSupplierCost').value);
                tax = parseFloat((isNaN(document.getElementById(id + 'EITtxtTax').value) || document.getElementById(id + 'EITtxtTax').value == '') ? '0' : document.getElementById(id + 'EITtxtTax').value);
                markup = parseFloat((isNaN(document.getElementById(id + 'EITtxtMarkup').value) || document.getElementById(id + 'EITtxtMarkup').value == '') ? '0' : document.getElementById(id + 'EITtxtMarkup').value);

                totalAmount = supplierCost + tax + markup;
                document.getElementById(id + 'EITtxtAmount').value = totalAmount;
                document.getElementById(id + 'EITtxtAmount').value = totalAmount.toFixed(2);
            }
        }

        function restrictNumeric(fieldId, kind) {
            try {
                return (maskNumeric(fieldId, (kind == '3' || kind == '4' ? 'false' : 'true'), (kind == '1' || kind == '3' ? 'true' : 'false')))
            }
            catch (err) {
                showError(err, 'Validation', 'restrictNumeric'); return (false)
            }
        }

        function maskNumeric(fieldId, ignoreNegative, IgnoreDecimal) {
            var key; var keychar
            if (ignoreNegative == null) ignoreNegative = 'true'
            if (IgnoreDecimal == null) IgnoreDecimal = 'true'
            if (window.event) {
                if (navigator.appName.substring(0, 1) == 'M') key = window.event.keyCode
                else key = window.event.charCode
            }
            else if (event) key = event.which
            else return true
            keychar = String.fromCharCode(key)
            if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27)) return true
            var strSet = "0123456789" + (ignoreNegative == 'true' ? '' : '-') + (IgnoreDecimal == 'true' ? '' : '.')
            if ((strSet.indexOf(keychar) > -1)) {
                var inputbox = document.getElementById(fieldId)
                if (ignoreNegative == 'false' && key == 45) {
                    if (inputbox.value.indexOf('-') == -1) inputbox.value = '-' + inputbox.value
                    return (false)
                }
                if (IgnoreDecimal == 'false' && inputbox.value.indexOf('.') > -1 && key == 46) return (false)
                return true
            }
            return (false)
        }


        function ValidDate(text) {
            var textArray = text.split('/')
            if (textArray.length != 3 || textArray.length != 3) {
                return false;
            }
            if (!CheckValidDate(textArray[0], textArray[1], textArray[2])) {
                return false;
            }
            return true;
        }
        function showimag1(id, imgid, hdfid, hideid) {
            //    var div = document.getElementById(id);
            //    if (div.style.display == "block") {
            //        div.style.display = "none";
            //    }
            //    else {
            //        div.style.display = "block";;
            //    }
            //    document.getElementById(imgid).src = document.getElementById(hdfid).value;
            window.open(document.getElementById(hdfid).value);
            return false;
        }
        function showImage(stat, img) {
            if (stat == "S") {
                // alert(document.getElementById('<%=pnlImages.ClientID %>'));
                document.getElementById('<%=pnlImages.ClientID %>').style.display = "block";

                if (img == "1") {
                    document.getElementById('<%=img1PreView.ClientID %>').style.display = "block";
                    document.getElementById('<%=img2PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img3PreView.ClientID %>').style.display = "none";
                }
                else if (img == "2") {
                    document.getElementById('<%=img1PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img2PreView.ClientID %>').style.display = "block";
                    document.getElementById('<%=img3PreView.ClientID %>').style.display = "none";
                }
                else {
                    document.getElementById('<%=img1PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img2PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img3PreView.ClientID %>').style.display = "block";
                }
            }
            else {
                document.getElementById('<%=pnlImages.ClientID %>').style.display = "none";
                document.getElementById('<%=img1PreView.ClientID %>').style.display = "none";
                document.getElementById('<%=img2PreView.ClientID %>').style.display = "none";
                document.getElementById('<%=img3PreView.ClientID %>').style.display = "none";
                return false;
            }
        }

        function deleteClonedSupplierDiv() {
            //            console.log($('.cloneSupplierDetails').children().length);
            //            if ($('.cloneSupplierDetails').children().length > 1) {
            //                $('.cloneSupplierDetails').children().last().remove();
            //            }
            $(".cloneSupplierDetails div:first").remove();
        }
        function tabsClear() {
            if ($("#apackageTab1").parent().hasClass('active')) {
                document.getElementById('<%=hdnClear.ClientID %>').value = "Tour Details";
                return true;
            }
            else if ($("#apackageTab2").parent().hasClass('active')) {
                document.getElementById('<%=hdnClear.ClientID %>').value = "Price Details";
                return true;
            }
            else if ($("#apackageTab3").parent().hasClass('active')) {
                document.getElementById('<%=hdnClear.ClientID %>').value = "Gallery";
                return true;
            }
            else if ($("#apackageTab4").parent().hasClass('active')) {
                document.getElementById('<%=hdnClear.ClientID %>').value = "Available Dates";
                return true;
            }
            else if ($("#apackageTab5").parent().hasClass('active')) {
                document.getElementById('<%=hdnClear.ClientID %>').value = "Inclusions / Exclusions";
                return true;
            }
            return true;
        }

        function ShowTab(val) {
            //alert(val);
            $('a[href="#' + val + '"]').tab('show')
        }

        function ShowAlertMessage(msg) {
            alert(msg);
            window.location.href = "ActivityMaster.aspx";

        }

//Added by lokesh on 12 july2017

//The below functions are used for tabs navigation.
//Initally only submit button is used to navigate through the tabs
//Only at the last tab the user is able to see the price and the finally save button.
        $(document).ready(function () {
            $('ul.nav-tabs li').click(function (e) {
                navigatePackageTabs();
            });
        });

        function navigateTabs() {
            navigatePackageTabs();
        }

        function navigatePackageTabs() {
            if ($('#tabTourDetails').hasClass('active')) {
                $('#tabTourDetails').removeClass('active');
                $('#tabPriceDetails').addClass('active');
                $("#packageTab1").removeClass('active');
                $("#packageTab2").addClass('active');
                $('#tblControls').hide();
                $('#btnSubmit').show();
            }
            else if ($('#tabPriceDetails').hasClass('active')) {
                $('#tabPriceDetails').removeClass('active');
                $('#tabGallery').addClass('active');
                $("#packageTab2").removeClass('active');
                $("#packageTab3").addClass('active');
                $('#tblControls').hide();
                $('#btnSubmit').show();
            }

            else if ($('#tabGallery').hasClass('active')) {
                $('#tabGallery').removeClass('active');
                $('#tabAvailableDates').addClass('active');
                $("#packageTab3").removeClass('active');
                $("#packageTab4").addClass('active');
                $('#tblControls').hide();
                $('#btnSubmit').show();
            }

            else if ($('#tabAvailableDates').hasClass('active')) {
                $('#tabAvailableDates').removeClass('active');
                $('#tabInclusionsExclusions').addClass('active');
                $("#packageTab4").removeClass('active');
                $("#packageTab5").addClass('active');
                $('#tblControls').show();
                $('#btnSubmit').hide();
            }
            else if ($('#tabInclusionsExclusions').hasClass('active')) {
                $('#tabInclusionsExclusions').removeClass('active');
                $('#tabTourDetails').addClass('active');
                $("#packageTab5").removeClass('active');
                $("#packageTab1").addClass('active');
                $('#tblControls').hide();
                $('#btnSubmit').show();
            }
        }

// Regular expression for valid email id
        var ValidEmail = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z][a-zA-Z]+)$/;


// Trim based on Regular Expression
        function Trim(str) {
            return str.replace(/^\s+|\s+$/g, '');
        }
        function isAlpha(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || keyCode == 9 || keyCode == 8 || keyCode == 11);
            return ret;
        }
        function isAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 9 || keyCode == 8 || keyCode == 11 || keyCode == 32);
            return ret;
        }
        function isNumber(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || keyCode == 9 || keyCode == 8 || keyCode == 11);
            return ret;
        }
        function AddPickup() {
            var pickupCount = parseInt(document.getElementById('<%=hdnPickupCount.ClientID %>').value);
            pickupCount = (parseInt(pickupCount)+1);
            document.getElementById('<%=hdnPickupCount.ClientID %>').value = (parseInt(pickupCount));
            //var transfersDiv = document.getElementById('divTransfers').innerHTML;
            if (document.getElementById('transfers-' + pickupCount) == null) {
                var newdiv = document.createElement('div');
                // htmlstring = "<div class=\"fleft\" id=\"transfers-" + pickupCount + "\"><div class=\"col-md-4\" id=\"transfersDiv-" + pickupCount + "\">";
                var htmlstring = "<div class=\"row\" id=\"transfers-" + pickupCount + "\"><div class=\"col-md-4\" id=\"transfersDiv-" + pickupCount + "\"><div class=\"form-group\">";
                htmlstring += "<label id=\"lblPickLocation-" + pickupCount + "\">Pick Location<span class=\"req-star\" style=\"color: red\">*</span>:";
                htmlstring += "</label>";//id=\"txt" + divName + "Text-" + idCount + "\" 
                htmlstring += "<div class=\"input-group\">" + "<div class=\"input-group-addon\">" + "<span class=\"glyphicon glyphicon-map-marker\" aria-hidden=\"true\"></span>" + "</div>";
                htmlstring += "<input type=\"text\" id=\"txtPickLocation-" + pickupCount + "\" name=\"txtPickLocation-" + pickupCount + "\" class=\"form-control\" onkeypress=\"return isAlphaNumeric(event);\" maxlength=\"100\" />";
                htmlstring += "</div>" + "</div>" + "</div>" + "<div class=\"col-md-4\">" + "<div class=\"form-group\">" + "<label>" + "Pick Up Date/Time:" + "</label>" + "<div class=\"input-group\">";
                htmlstring += "<div class=\"input-group-addon\">" + "<span class=\"glyphicon glyphicon-time\" aria-hidden=\"true\"></span>" + "</div>";
                htmlstring += "<input type=\"text\" id=\"txtPickUpHr-" + pickupCount + "\" name=\"txtPickUpHr-" + pickupCount + "\" class=\"form-control\" style=\"width:60px;\" maxlength=\"2\" onkeypress=\"return isNumber(event);\" onchange=\"return ValidateTime('H', this.value,this.id)\" value=\"\" />";
                htmlstring += "<input type=\"text\" id=\"txtPickUpMin-" + pickupCount + "\" name=\"txtPickUpMin-" + pickupCount + "\" class=\"form-control\" maxlength=\"2\" style=\"width:60px;\" onkeypress=\"return isNumber(event);\" onchange=\"return ValidateTime('M', this.value,this.id)\" value=\"\" />";
                htmlstring +="<div class=\"form-group\">" + "<label>" + "Pick Up Time Range:" + "</label>" + "<div class=\"input-group\">";
                htmlstring += "<div class=\"input-group-addon\">" + "<span class=\"glyphicon glyphicon-time\" aria-hidden=\"true\"></span>" + "</div>";
                 htmlstring += "<input type=\"text\" id=\"txtPickUpHrRange-" + pickupCount + "\" name=\"txtPickUpHrRange-" + pickupCount + "\" class=\"form-control\" style=\"width:60px;\" maxlength=\"2\" onkeypress=\"return isNumber(event);\" onchange=\"return ValidateTime('H', this.value,this.id)\" value=\"\" />";
                htmlstring += "<input type=\"text\" id=\"txtPickUpMinRange-" + pickupCount + "\" name=\"txtPickUpMinRange-" + pickupCount + "\" class=\"form-control\" maxlength=\"2\" style=\"width:60px;\" onkeypress=\"return isNumber(event);\" onchange=\"return ValidateTime('M', this.value,this.id)\" value=\"\" />";
                htmlstring += "<i class=\"fleft margin-left-10\" id=\"PickupImg-" + pickupCount + "\">" + "<img src=\"Images/delete.gif\" alt=\"Remove\" onclick=\"javascript:removePickup(" + pickupCount + ")\" /></i>";
                htmlstring += "</div>" + "</div>" + "</div>" + "</div>";

                newdiv.innerHTML = htmlstring;
                document.getElementById('addTransfers').appendChild(newdiv);

            }
            else {
                document.getElementById('transfers-' + pickupCount).style.display = "block";
            }

            if (parseInt(pickupCount) > 1) {
                removePickupImg(parseInt(pickupCount) - 1);
            }
            
        }

        function removePickup(id) {
            document.getElementById('transfers-' + id).style.display = "none";
            document.getElementById('txtPickLocation-' + id).value = null;
            document.getElementById('txtPickUpHr-' + id).value = null;
            document.getElementById('txtPickUpMin-' + id).value = null;
             document.getElementById('txtPickUpHrRange-' + id).value = null;
            document.getElementById('txtPickUpMinRange-' + id).value = null;
            if (parseInt(id) > 0) {
                document.getElementById('<%=hdnPickupCount.ClientID %>').value = (parseInt(id)) - 1;
            }
            else if (parseInt(id) == 0)
            {
                document.getElementById('<%=hdnPickupCount.ClientID %>').value = 0;
            }
            if ((parseInt(id)) > 1) {
                document.getElementById('PickupImg-' + (parseInt(id) - 1)).style.display = "block";
            }
        }
        function removePickupImg(id) {
            document.getElementById('PickupImg-' + id).style.display = "none";
        }

        function LoadPickupDetails(details) {
            if (details != null && details != "") {
                var pickupDetails = details.split('|');
                var locationTime = null;
                var pickupLocation = null;
                var pickupHr = "";
                var pickupMin = "";
                var pickupRangeHr = "";
                var pickupRangeMin = "";
                var pickupCount = 0;
                var startTime = "";
                var EndTime = "";
                for (var i = 0; i < pickupDetails.length; i++) {
                    locationTime = null;
                    pickupLocation = null;
                    pickupHr = "";
                    pickupMin = "";
                    pickupRangeHr = "";
                    pickupRangeMin = "";
                    startTime = "";
                     EndTime = "";
                    if (pickupDetails[i] != null && pickupDetails[i] != "") {
                        locationTime = pickupDetails[i].split(',');
                        if (locationTime.length > 2) {
                            for (var j = 0; j < locationTime.length; j++) {
                                if (j < locationTime.length - 1) {
                                    pickupLocation += locationTime[j];
                                }
                                else if (j == locationTime.length - 1) {
                                    if (locationTime[j] != "" && locationTime[j] != " ") {
                                        startTime = locationTime[j].split('-')[0];
                                        EndTime=locationTime[j].split('-')[1];
                                        pickupHr = startTime.split(':')[0];
                                        pickupHr = pickupHr.trim();
                                        pickupMin = startTime.split(':')[1];
                                        pickupMin = pickupMin.trim();
                                        pickupRangeHr = EndTime.split(':')[0];
                                        pickupRangeHr = pickupRangeHr.trim();
                                        pickupRangeMin = EndTime.split(':')[1];
                                        pickupRangeMin = pickupRangeMin.trim();
                                    }
                                }
                            }
                        }
                        else if (locationTime.length == 2) {
                            pickupLocation = locationTime[0];
                            if (locationTime[1] != "" && locationTime[1] != " ") {
                                startTime = locationTime[1].split('-')[0];
                                 EndTime=locationTime[1].split('-')[1];
                                pickupHr = startTime.split(':')[0];
                                pickupHr = pickupHr.trim();
                                pickupMin = startTime.split(':')[1];
                                 pickupMin = pickupMin.trim();
                                pickupRangeHr = EndTime.split(':')[0];
                                 pickupRangeHr = pickupRangeHr.trim();
                                pickupRangeMin = EndTime.split(':')[1];
                                 pickupRangeMin = pickupRangeMin.trim();
                            }
                        }
                        //pickupCount = parseInt(document.getElementById('<%=hdnPickupCount.ClientID %>').value);
                        if (document.getElementById('transfers-' + pickupCount) == null) {
                            var newdiv = document.createElement('div');
                            // htmlstring = "<div class=\"fleft\" id=\"transfers-" + pickupCount + "\"><div class=\"col-md-4\" id=\"transfersDiv-" + pickupCount + "\">";
                            var htmlstring = "<div class=\"row\" id=\"transfers-" + (i + 1) + "\"><div class=\"col-md-4\" id=\"transfersDiv-" + (i + 1) + "\"><div class=\"form-group\">";
                            htmlstring += "<label id=\"lblPickLocation-" + (i + 1) + "\">Pick Location<span class=\"req-star\" style=\"color: red\">*</span>:";
                            htmlstring += "</label>";//id=\"txt" + divName + "Text-" + idCount + "\" 
                            htmlstring += "<div class=\"input-group\">" + "<div class=\"input-group-addon\">" + "<span class=\"glyphicon glyphicon-map-marker\" aria-hidden=\"true\"></span>" + "</div>";
                            htmlstring += "<input type=\"text\" id=\"txtPickLocation-" + (i + 1) + "\" name=\"txtPickLocation-" + (i + 1) + "\" class=\"form-control\" onkeypress=\"return isAlphaNumeric(event);\" maxlength=\"100\" value=\"" + pickupLocation + "\" />";
                            htmlstring += "</div>" + "</div>" + "</div>" + "<div class=\"col-md-4\">" + "<div class=\"form-group\">" + "<label>" + "Pick Up Date/Time:" + "</label>" + "<div class=\"input-group\">";
                            htmlstring += "<div class=\"input-group-addon\">" + "<span class=\"glyphicon glyphicon-time\" aria-hidden=\"true\"></span>" + "</div>";
                            htmlstring += "<input type=\"text\" id=\"txtPickUpHr-" + (i + 1) + "\" name=\"txtPickUpHr-" + (i + 1) + "\" class=\"form-control\" style=\"width:60px;\" maxlength=\"2\" onkeypress=\"return isNumber(event);\" onchange=\"return ValidateTime('H', this.value,this.id)\" value=\"" + pickupHr + "\" />";
                            htmlstring += "<input type=\"text\" id=\"txtPickUpMin-" + (i + 1) + "\" name=\"txtPickUpMin-" + (i + 1) + "\" class=\"form-control\" maxlength=\"2\" style=\"width:60px;\" onkeypress=\"return isNumber(event);\" onchange=\"return ValidateTime('M', this.value,this.id)\" value=\"" + pickupMin + "\" />";
                            htmlstring +="<div class=\"form-group\">" + "<label>" + "Pick Up Time Range:" + "</label>" + "<div class=\"input-group\">";
                            htmlstring += "<div class=\"input-group-addon\">" + "<span class=\"glyphicon glyphicon-time\" aria-hidden=\"true\"></span>" + "</div>";
                            htmlstring += "<input type=\"text\" id=\"txtPickUpHrRange-" +  (i + 1) + "\" name=\"txtPickUpHrRange-" + (i + 1) + "\" class=\"form-control\" style=\"width:60px;\" maxlength=\"2\" onkeypress=\"return isNumber(event);\" onchange=\"return ValidateTime('H', this.value,this.id)\" value=\"" + pickupRangeHr + "\" />";
                            htmlstring += "<input type=\"text\" id=\"txtPickUpMinRange-" + (i + 1) + "\" name=\"txtPickUpMinRange-" +  (i + 1) + "\" class=\"form-control\" maxlength=\"2\" style=\"width:60px;\" onkeypress=\"return isNumber(event);\" onchange=\"return ValidateTime('M', this.value,this.id)\" value=\"" + pickupRangeMin + "\" />";
                            htmlstring += "<i class=\"fleft margin-left-10\" id=\"PickupImg-" + (i + 1) + "\">" + "<img src=\"Images/delete.gif\" alt=\"Remove\" onclick=\"javascript:removePickup(" + (i + 1) + ")\" /></i>";
                            htmlstring += "</div>" + "</div>" + "</div>" + "</div>";

                            newdiv.innerHTML = htmlstring;
                            document.getElementById('addTransfers').appendChild(newdiv);

                        }
                        else {
                            document.getElementById('transfers-' + pickupCount).style.display = "block";
                        }

                        if (parseInt(i) > 0) {
                            removePickupImg(i);
                        }
                        document.getElementById('<%=hdnPickupCount.ClientID %>').value = (parseInt(i)) + 1;
                    }
                }
            }
            document.getElementById('addPickUp').style.display = "block";
        }
    </script>

    <asp:HiddenField runat="server" ID="hdfImage1" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage2" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage3" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage1Extn" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage2Extn" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage3Extn" Value="0" />
    <asp:HiddenField runat="server" ID="hdfUploadYNImage1" Value="0" />
    <asp:HiddenField runat="server" ID="hdfUploadYNImage2" Value="0" />
    <asp:HiddenField runat="server" ID="hdfUploadYNImage3" Value="0" />
    <asp:HiddenField runat="server" ID="hdfCountries" Value="" />
    <asp:HiddenField runat="server" ID="hdfCities" Value="" />
    <asp:HiddenField runat="server" ID="hdfSelCountry" Value="" />
    <asp:HiddenField runat="server" ID="hdfSelCity" Value="" />
    <input type="hidden" id="savePage" name="savePage" value="false" />
   
    <asp:Label runat="server" Style="color: Red" ID="lblError" Visible="false" Text=""></asp:Label>
    <%--<div id="errMess" class="error_module" style="display:none;"> <div id="errorMessage" style="float:left; color:Red;" class="padding-5 yellow-back width-100 center margin-top-5"> </div></div>--%>
    <div class="clear" style="margin-left: 30px">
        <div id="container1" style="position: absolute; top: 990px; left: 10%; z-index: 9999; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container3" style="position: absolute; top: 240px; left: 10%; z-index: 9999; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container5" style="position: absolute; top: 220px; left: 215px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container7" style="position: absolute; top: 220px; left: 215px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 252px; left: 215px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container4" style="position: absolute; top: 240px; left: 23%; z-index: 9999; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container6" style="position: absolute; top: 252px; left: 215px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container8" style="position: absolute; top: 252px; left: 215px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="callContainer" style="position: absolute; top: 300px; left: 10%; z-index: 9999; display: none;">
        </div>
    </div>
    <div class="body_container">
        <label id="lblSuccessMessage" runat="server"></label>
        <h2>Create Tours</h2>
        <div class="tabbed-panel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li id="tabTourDetails" role="presentation" class="active"><a id="apackageTab1" href="#packageTab1" aria-controls="home"
                    role="tab" data-toggle="tab">Tour Details</a></li>
                <li id="tabPriceDetails" role="presentation"><a id="apackageTab2" href="#packageTab2" aria-controls="settings"
                    role="tab" data-toggle="tab">Price Details</a></li>
                <li id="tabGallery" role="presentation"><a id="apackageTab3" href="#packageTab3" aria-controls="profile"
                    role="tab" data-toggle="tab">Gallery</a></li>
                <li id="tabAvailableDates" role="presentation"><a id="apackageTab4" href="#packageTab4" aria-controls="messages"
                    role="tab" data-toggle="tab">Available Dates</a></li>
                <li id="tabInclusionsExclusions" role="presentation"><a id="apackageTab5" href="#packageTab5" aria-controls="settings"
                    role="tab" data-toggle="tab">Inclusions / Exclusions</a></li>
            </ul>
            <!-- Tab panes -->
            <asp:HiddenField ID="hdnClear" runat="server" />
            <div class="tab-content rb-form">
                <asp:Label ID="lblMessage" runat="server" Font-Bold="true" Width="250"></asp:Label>
                <div role="tabpanel" class="tab-pane active rb-container-wrap" id="packageTab1">
                    <div class="form">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Label ID="lblAgent" runat="server" Text="Agent:"> </asp:Label><span class="req-star" style="color: red">*</span>
                                    <asp:DropDownList ID="ddlAgent" runat="server" CssClass=" form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="lblActivityName" runat="server" Text="Activity Name:"> </asp:Label><span class="req-star" style="color: red">*</span>
                                    <asp:TextBox ID="txtActivityName" placeholder="Activity" runat="server" CssClass=" form-control" MaxLength="100" onkeypress="return isAlphaNumeric(event);"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <asp:Label ID="lblDays" runat="server" Text="Days:"> </asp:Label><span class="req-star" style="color: red">*</span>
                                    <asp:UpdatePanel ID="UpdatePanel1_ddlDays" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlDays" CssClass="form-control" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlDays_SelectedIndexChanged">
                                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="ddlDays" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>
                                        <asp:Label ID="lblStockInHand" runat="server" Text="StockInHand:"> </asp:Label><span class="req-star" style="color:red">*</span>

                                    </label>
                                    <asp:TextBox ID="txtStockInHand" placeholder="StockInHand:" runat="server" CssClass=" form-control"
                                        onkeypress="return restrictNumeric(this.id,'0');" onpaste="return false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4" id="divStockUsed" runat="server" visible="false">
                                <div class="form-group">
                                    <label>
                                        <asp:Label ID="lblStockUsed" runat="server" Text="Stock Used:"> </asp:Label>

                                    </label>
                                    <asp:TextBox Enabled="false" ID="txtStockUsed" runat="server" CssClass=" form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>
                                        <asp:Label ID="lblStartingFrom" runat="server" Text="Starting From:"> </asp:Label><span class="req-star" style="color:red">*</span>
                                    </label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <asp:Label ID="lblCurrency" runat="server"></asp:Label>
                                        </div>
                                        <asp:TextBox ID="txtStartingFrom" runat="server" placeholder="Starting From" CssClass=" form-control" onpaste="return false"
                                            onkeypress="return restrictNumeric(this.id,'2');"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <%--     <div class="col-md-4">
                              <div class="form-group">
                                <label>Package Origin:<span class="req-star">*</span></label>
                                <input type="text" class="form-control" placeholder="Package Origin">
                               </div>
                           </div>
                            --%>
                            <%-- <asp:DropDownList ID="ddlCountry" Style="width: 50%;" 
             runat="server" AutoPostBack="True"  CssClass="inputDdlEnabled"
             onselectedindexchanged="ddlCountry_SelectedIndexChanged"  >
                            </asp:DropDownList> --%>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>
                                        <asp:Label ID="lblCountry" runat="server" Text="Country:"></asp:Label><span class="req-star" style="color: red">*</span></label>
                                    <asp:DropDownList ID="CountryText1" runat="server" CssClass=" form-control" AppendDataBoundItems="true"
                                        onchange="LoadCities(this.id)">
                                        <asp:ListItem Selected="True" Value="0" Text="Select Country"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>
                                        <asp:Label ID="lblCity" runat="server" Text="City:"></asp:Label><span class="req-star" style="color: red">*</span></label>
                                    <asp:DropDownList ID="CityText1" CssClass=" form-control" runat="server" onchange="SetCity(this.id)">
                                        <asp:ListItem Selected="True" Value="0" Text="Select City"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <asp:Label ID="lblTheme" runat="server" Text="Select Theme:"></asp:Label><span class="req-star" style="color:red">*</span></label>
                                    <div class="clearfix">
                                    </div>
                                    <div class="rb-custom-checkbox pull-left">
                                        <asp:CheckBoxList ID="chkTheme" runat="server" RepeatDirection="Horizontal">
                                        </asp:CheckBoxList>
                                        <%-- <input type="checkbox">
                                    <label>Cruise</label>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>
                                        <asp:Label ID="lblIntroduction" runat="server" Text="Introduction:"></asp:Label><span class="req-star" style="color:red">*</span>
                                    </label>
                                    <asp:TextBox CssClass="form-control" ID="txtIntroduction" runat="server" TextMode="MultiLine" Height="100"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>
                                        <asp:Label ID="lblOverView" runat="server" Text="OverView:"></asp:Label><span class="req-star" style="color:red">*</span></label>
                                    <asp:TextBox ID="txtOverView" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>
                                        Start From :<span class="req-star" style="color:red">*</span></label>
                                    <div class="row no-gutter">
                                        <div class="col-xs-6">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                                </div>
                                                <asp:TextBox ID="txtStartFromHr" runat="server" CssClass="form-control" MaxLength="2"
                                                    onkeypress="return restrictNumeric(this.id,'0');" placeholder="Hr" onchange="CalcDuration()"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                                </div>
                                                <asp:TextBox ID="txtStartTimeMin" placeholder="Min" runat="server" CssClass="form-control"
                                                    Width="60" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0');" onchange="CalcDuration()"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>
                                        End To :<span class="req-star" style="color:red">*</span></label>
                                    <div class="row no-gutter">
                                        <div class="col-xs-6">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                                </div>
                                                <asp:TextBox ID="txtEndToHr" placeholder="Hr" runat="server" CssClass="form-control"
                                                    MaxLength="2" onkeypress="return restrictNumeric(this.id,'0');" onchange="CalcDuration()"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                                </div>
                                                <asp:TextBox ID="txtEndTimeMin" runat="server" CssClass="form-control" placeholder="Min"
                                                    MaxLength="2" onkeypress="return restrictNumeric(this.id,'0');" onchange="CalcDuration()"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="Activity Duration:"></asp:Label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                        </div>
                                        <asp:TextBox CssClass="form-control" ID="txtDuration" EnableViewState="true" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>
                                        <asp:Label ID="lblItinerary1" runat="server" Text="Itinerary 1:"></asp:Label></label>
                                    <asp:TextBox ID="txtItinerary1" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100" onkeypress="return isAlpha(event);"></asp:TextBox>

                                    <asp:Label ID="lblItinerary2" runat="server" Text="Itinerary 2:" Visible="false"></asp:Label>
                                    <asp:TextBox ID="txtItinerary2" runat="server" Width="210px" TextMode="MultiLine"
                                        Height="100px" Visible="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>
                                        <asp:Label ID="lblDetails" runat="server" Text="Details:"></asp:Label></label>
                                    <asp:TextBox ID="txtDetails" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="well" data-clone-content="supplierDetails">
                                
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>
                                                    Supplier Name</label>
                                                <asp:TextBox ID="txtSupplierName" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" MaxLength="100"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>
                                                    Supplier E-mail</label>
                                                <asp:TextBox ID="txtSupplierEmail" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Supplier Address</label>
                                               <asp:TextBox ID="txtServiceName" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Supplier Telephone</label>
                                               <asp:TextBox ID="txtSupplierTelephone" runat="server" CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2')"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>
                                                    <asp:Label ID="lblMealsInclude" runat="server" Text="Meals Include:"> </asp:Label>
                                                </label>
                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlMealsInclude">
                                                    <asp:ListItem Selected="True" Text="No" Value="No">

                                                    </asp:ListItem>
                                                    <asp:ListItem  Text="Yes" Value="Yes">

                                                    </asp:ListItem>
                                                </asp:DropDownList>
                                                <%--<asp:TextBox ID="txtMealsInclude" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" MaxLength="100"></asp:TextBox>--%>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <asp:Label ID="lblTrasferInclude" runat="server" Text="Transfer Include:"> </asp:Label>
                                                <asp:DropDownList onchange="DisplayTransfersDiv()" CssClass="form-control" runat="server" ID="ddlTransferInclude">
                                                    <asp:ListItem Selected="True" Text="No" Value="No"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                </asp:DropDownList>

                                                <%--<asp:TextBox ID="txtTrasferInclude" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" MaxLength="100"></asp:TextBox>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="hdnPickupCount" runat="server" value="0" />
                                    <input type="hidden" id="hdnPickupValue" runat="server" />
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lblPickUpPoint" style="display:none">Pickup Point<span class="req-star" style="color: red">*</span>:</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="display:none">
                                                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true" style="display:none"></span>
                                                    </div>
                                                    <asp:TextBox Enabled="false" ID="txtPickUpPoint" runat="server" CssClass="form-control"  MaxLength="100" Visible="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="divTransfers">
                                        <%--<div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lblPickUpPoint">Pickup Point<span class="req-star" style="color: red">*</span>:</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                                    </div>
                                                    <asp:TextBox Enabled="false" ID="txtPickUpPoint" runat="server" CssClass="form-control"  MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>--%>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lblPickLocation">Pick Location<span class="req-star" style="color: red">*</span>:</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                                    </div>
                                                    <asp:TextBox Enabled="false" ID="txtPickLocation" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lblPickUpTime">
                                                    Pick Up Date/Time:
                                                </label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                                    </div>
                                                    <asp:TextBox Enabled="false" ID="txtPickUpHr" runat="server" CssClass="form-control" Width="60px"
                                                        MaxLength="2" onkeypress="return isNumber(event);" onchange="return ValidateTime('H',this.value,this.id)"></asp:TextBox>
                                                    <asp:TextBox Enabled="false" ID="txtPickUpMin" runat="server" CssClass="form-control" MaxLength="2"
                                                        Width="60px" onkeypress="return isNumber(event);" onchange="return ValidateTime('M',this.value,this.id)"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lblPickUpTimeRange">
                                                    Pick Up Time Range:
                                                </label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                                    </div>
                                                    <asp:TextBox Enabled="false" ID="txtPickUpHrRange" runat="server" CssClass="form-control" Width="60px"
                                                        MaxLength="2" onkeypress="return isNumber(event);" onchange="return ValidateTime('H',this.value,this.id)"></asp:TextBox>
                                                    <asp:TextBox Enabled="false" ID="txtPickUpMinRange" runat="server" CssClass="form-control" MaxLength="2"
                                                        Width="60px" onkeypress="return isNumber(event);" onchange="return ValidateTime('M',this.value,this.id)"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="addTransfers" class="fleft">
                                    </div>
                                    <br />
                                    <div class="marbot_20">
                                        <a id="addPickUp" style="display: none;width:100px;" href="javascript:void(0)" onclick="javascript:AddPickup();"><span class=" glyphicon glyphicon-plus-sign"></span>Add More</a>
                                    </div>
                                </div>

                                <%-- <div class="col-md-4" style="display:none">
                                            <div class="form-group"  >
                                                <label>
                                                    <asp:Label ID="lblDropOffLocation" runat="server" Text="Drop off Location:"> </asp:Label>
                                                </label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                                    </div>
                                                    <asp:TextBox Enabled="false" ID="txtDropOffLocation" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3"  style="display:none">
                                            <div class="form-group">
                                                <label>
                                                    <asp:Label ID="lblDropOffDate" runat="server" Text="Drop off Date/Time:"></asp:Label></label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                                    </div>
                                                    <asp:TextBox Enabled="false" ID="txtDropUpHr" runat="server" CssClass="form-control pull-left" Width="60px"
                                                        MaxLength="2" onkeypress="return restrictNumeric(this.id,'0');" onchange="return ValidateTime('H',this.value,this.id)"></asp:TextBox>
                                                    <asp:TextBox Enabled="false" ID="txtDropUpMin" runat="server" CssClass="form-control pull-left" Width="60px"
                                                        MaxLength="2" onkeypress="return restrictNumeric(this.id,'0');" onchange="return ValidateTime('M',this.value,this.id)"></asp:TextBox>
                                                    <%--<input  name="FromDate" type="text" value="DD/MM/YYYY" id="FromDate" />
                                                </div>
                                            </div>

                                            <div>
                                               
                                            </div>
                                        </div>--%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <strong>Additional Details </strong>
                            </div>
                            <div class="well">
                                <div style="width: 100%; overflow: hidden; overflow-x: auto;">
                                    <div id="div1" class="">
                                        <asp:HiddenField ID="hdfAFCDetails" runat="server"></asp:HiddenField>
                                        <asp:HiddenField runat="server" ID="hdfAFCCount" Value="0" />
                                        <asp:GridView CssClass="gridviewbornone" ID="gvAFCDetails" runat="server" AutoGenerateColumns="False"
                                            AllowPaging="true" PageSize="10" DataKeyNames="flexId" CellPadding="0" ShowFooter="true"
                                            CellSpacing="0" BorderWidth="0" OnRowCommand="gvAFCDetails_RowCommand" Width="100%"
                                            OnRowEditing="gvAFCDetails_RowEditing" OnRowDeleting="gvAFCDetails_RowDeleting"
                                            OnRowUpdating="gvAFCDetails_RowUpdating" OnRowCancelingEdit="gvAFCDetails_RowCancelingEdit">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <label style="color: Black; font-weight: bold">
                                                            Label</label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITLabel" runat="server" Text='<%#Eval("flexLabel") %>' CssClass="labelDtl grdof"
                                                            ToolTip='<%#Eval("flexLabel") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="EITtxtLabel" runat="server" Enabled="true" Text='<%#Eval("flexLabel") %>'
                                                            Width="100px" CssClass="inputDdlEnabled" onkeypress="return isAlphaNumeric(event);" MaxLength="50"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="FTtxtLabel" runat="server" Enabled="true" CssClass="form-control"
                                                            Width="100px" onkeypress="return isAlphaNumeric(event);" MaxLength="50"></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="left" />
                                                    <HeaderTemplate>
                                                        <label style="color: Black; font-weight: bold">
                                                            <span class="mdt"></span>Control</label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%-- <asp:Label ID="ITlblControl" runat="server" Text='<%# Eval("flexControlDESC") %>' CssClass="labelDtl grdof"
                                                                    ToolTip='<%# Eval("flexControlDESC") %>' Width="110px"></asp:Label>--%>
                                                        <asp:Label ID="ITlblControl" runat="server" Text='<%# Eval("flexControlDESC") %>'
                                                            CssClass="labelDtl grdof" ToolTip='<%# Eval("flexControlDESC") %>' Width="110px"></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList CssClass="form-control" ID="EITddlControl" runat="server" Enabled="true"
                                                            Width="120px">
                                                            <asp:ListItem Text="TextBox" Value="T"></asp:ListItem>
                                                            <asp:ListItem Text="DateControl" Value="D"></asp:ListItem>
                                                            <asp:ListItem Text="DropDownList" Value="L"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:HiddenField ID="EIThdfControl" runat="server" Value='<%# Eval("flexControl") %>'>
                                                        </asp:HiddenField>
                                                    </EditItemTemplate>
                                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                    <FooterTemplate>
                                                        <asp:DropDownList CssClass="form-control" ID="FTddlControl" runat="server" Enabled="true"
                                                            Maxlength="50">
                                                            <asp:ListItem Text="-SelectControl-" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="TextBox" Value="T"></asp:ListItem>
                                                            <asp:ListItem Text="DateControl" Value="D"></asp:ListItem>
                                                            <asp:ListItem Text="DropDownList" Value="L"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <label style="color: Black; font-weight: bold">
                                                            Sql Query</label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITSqlQuery" runat="server" Text='<%#Eval("flexSqlQuery") %>' CssClass="labelDtl grdof"
                                                            ToolTip='<%#Eval("flexSqlQuery") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="EITtxtSqlQuery" runat="server" Enabled="true" Text='<%#Eval("flexSqlQuery") %>'
                                                            CssClass="form-control"  MaxLength="500" ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="FTtxtSqlQuery" runat="server" Enabled="true" CssClass="form-control"  MaxLength="500" Width="100px"></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="left" />
                                                    <HeaderTemplate>
                                                        <label style="color: Black; font-weight: bold">
                                                            <span class="mdt"></span>DataType</label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="ITlblDataType" runat="server" Text='<%# Eval("flexDataTypeDESC") %>' CssClass="labelDtl grdof"
                                                                    ToolTip='<%# Eval("flexDataTypeDESC") %>' Width="110px"></asp:Label>--%>
                                                        <asp:Label ID="ITlblDataType" runat="server" Text='<%# Eval("flexDataTypeDESC") %>'
                                                            CssClass="labelDtl grdof" ToolTip='<%# Eval("flexDataTypeDESC") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList CssClass="form-control" ID="EITddlDataType" runat="server" Enabled="true">
                                                            <asp:ListItem Text="AlphaNumeric" Value="A"></asp:ListItem>
                                                            <asp:ListItem Text="Numeric" Value="N"></asp:ListItem>
                                                            <asp:ListItem Text="TextOnly" Value="T"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:HiddenField ID="EIThdfDataType" runat="server" Value='<%# Eval("flexDataType") %>'>
                                                        </asp:HiddenField>
                                                    </EditItemTemplate>
                                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                    <FooterTemplate>
                                                        <asp:DropDownList CssClass="form-control" ID="FTddlDatatype" runat="server" Enabled="true">
                                                            <asp:ListItem Text="-SelectDataType-" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="AlphaNumeric" Value="A"></asp:ListItem>
                                                            <asp:ListItem Text="Numeric" Value="N"></asp:ListItem>
                                                            <asp:ListItem Text="TextOnly" Value="T"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="left" />
                                                    <HeaderTemplate>
                                                        <label style="color: Black; font-weight: bold">
                                                            <span class="mdt"></span>Mandatory</label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%-- <asp:Label ID="ITlblMandatory" runat="server" Text='<%# Eval("flexStatusDESC") %>' CssClass="labelDtl grdof"
                                                                    ToolTip='<%# Eval("flexStatusDESC") %>' Width="110px"></asp:Label>--%>
                                                        <asp:Label ID="ITlblMandatory" runat="server" Text='<%# Eval("flexStatusDESC") %>'
                                                            CssClass="labelDtl grdof" ToolTip='<%# Eval("flexStatusDESC") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList CssClass="form-control" ID="EITddlMandatory" runat="server" Enabled="true">
                                                            <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                            <asp:ListItem Text="NO" Value="N"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:HiddenField ID="EIThdfStatus" runat="server" Value='<%# Eval("flexMandatoryStatus") %>'>
                                                        </asp:HiddenField>
                                                    </EditItemTemplate>
                                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                    <FooterTemplate>
                                                        <asp:DropDownList CssClass="form-control" ID="FTddlMandatory" runat="server" Enabled="true">
                                                            <asp:ListItem Text="-SelectStatus-" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                            <asp:ListItem Text="NO" Value="N"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <label style="color: Black; font-weight: bold">
                                                            Order</label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITOrder" runat="server" Text='<%#Eval("flexOrder") %>' CssClass="labelDtl grdof"
                                                            ToolTip='<%#Eval("flexOrder") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="EITtxtOrder" runat="server" Enabled="true" Text='<%#Eval("flexOrder") %>'
                                                            Width="100px" CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="3"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="FTtxtOrder" runat="server" Enabled="true" CssClass="form-control"
                                                            onkeypress="return restrictNumeric(this.id,'2');" MaxLength="3"></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label runat="server" ID="id" Width="100px"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:UpdatePanel ID="UpdatePanel_lnkFEdit" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="lnkFEdit" runat="server" Text="Edit" CommandName="Edit" Width="50px">
                                                                </asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lnkFEdit" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <asp:UpdatePanel ID="UpdatePanel_ibtnFDelete" runat="server">
                                                            <ContentTemplate>
                                                                <asp:ImageButton ID="ibtnFDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif"
                                                                    CausesValidation="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the user?');">
                                                                </asp:ImageButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="ibtnFDelete" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:UpdatePanel ID="UpdatePanel_lnkFUpdate" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="lnkFUpdate" runat="server" Text="Update" Width="50px" CommandName="Update"
                                                                    OnClientClick="return validateDtlAddUpdate('U',this.id)"></asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lnkFUpdate" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <asp:UpdatePanel ID="UpdatePanel_lnkFCancel" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="lnkFCancel" runat="server" Text="Cancel" Width="50px" CommandName="Cancel"></asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lnkFCancel" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </EditItemTemplate>
                                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                    <FooterTemplate>
                                                        <asp:UpdatePanel ID="UpdatePanel_lnkFAdd" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="lnkFAdd" CommandName="Add" runat="server" Text="Add" CssClass="marright_10"
                                                                    OnClientClick="return validateDtlAddUpdate('I',this.id)"> </asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lnkFAdd" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="" />
                                            <RowStyle HorizontalAlign="left" />
                                            <AlternatingRowStyle CssClass="" />
                                        </asp:GridView>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>

                </div>

                <div role="tabpanel" class="tab-pane" id="packageTab2">
                    <style>
                        .gridviewbornone td, th {
                            border: none;
                        }

                        .undatewidth {
                            width: 101px !important;
                            float: left;
                        }
                    </style>
                    <div id="divPODetails" class="well" data-clone-content="PriceDetail">
                        <asp:HiddenField ID="hdfPrice" runat="server"></asp:HiddenField>
                        <asp:HiddenField runat="server" ID="hdfPriceCount" Value="0" />
                        <asp:GridView CssClass="gridviewbornone" ID="GvPrice" runat="server" AutoGenerateColumns="False"
                            AllowPaging="true" BorderStyle="None" BorderWidth="0" PageSize="10" DataKeyNames="priceId"
                            CellPadding="0" ShowFooter="true" CellSpacing="0" Width="100%" GridLines="Both"
                            OnRowCommand="GvPrice_RowCommand" OnRowDeleting="GvPrice_RowDeleting" OnRowEditing="GvPrice_RowEditing"
                            OnRowUpdating="GvPrice_RowUpdating" OnRowCancelingEdit="GvPrice_RowCancelingEdit">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <div class="col-md-12">
                                            <label style="color: Black; font-weight: bold">
                                                Label</label>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="col-md-12">
                                            <asp:Label ID="ITLabel" runat="server" Text='<%#Eval("label") %>' CssClass="labelDtl grdof"
                                                ToolTip='<%#Eval("label") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <%--<div class="col-md-12">
                                            <asp:TextBox ID="EITtxtLabel" runat="server" Enabled="true" Text='<%#Eval("Label") %>'
                                                CssClass="form-control" onkeypress="return isAlphaNumeric(event);" MaxLength="70"></asp:TextBox>
                                        </div>--%>
                                        <div class="col-md-12"> 
                                        <asp:DropDownList CssClass="form-control" ID="EITddlPaxType" runat="server" Enabled="true">
                                             <asp:ListItem Text="-Select PaxType-" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Adult" Value="Adult"></asp:ListItem>
                                            <asp:ListItem Text="Child" Value="Child"></asp:ListItem>
                                            <asp:ListItem Text="Infant" Value="Infant"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="EIThdfPaxType" runat="server" Value='<%# Eval("label") %>'>
                                        </asp:HiddenField>
                                            </div>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                       <%-- <div class="col-md-12">
                                            <asp:TextBox ID="FTtxtLabel" runat="server" Enabled="true" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" MaxLength="70"></asp:TextBox>
                                        </div>--%>
                                        <div class="col-md-12"> 
                                    <asp:DropDownList CssClass="form-control" ID="FTddlPaxType" runat="server" Enabled="true">
                                            <asp:ListItem Text="-Select PaxType-" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Adult" Value="Adult"></asp:ListItem>
                                            <asp:ListItem Text="Child" Value="Child"></asp:ListItem>
                                            <asp:ListItem Text="Infant" Value="Infant"></asp:ListItem>
                                        </asp:DropDownList>
</div>    
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <label style="color: Black; font-weight: bold">
                                            Supplier Cost</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITSupplierCost" runat="server" Text='<%#Eval("SupplierCost") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("SupplierCost") %>' Width="80px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EITtxtSupplierCost" runat="server" Enabled="true" Text='<%#Eval("SupplierCost") %>'
                                            Width="80px" CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="20" onpaste="return false;"
                                            onChange="getTotalAmount(this.id,'U')"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTtxtSupplierCost" runat="server" Enabled="true" CssClass=" form-control"
                                            onChange="getTotalAmount(this.id,'I')" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="20" onpaste="return false;"
                                            Width="80px"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <label style="color: Black; font-weight: bold">
                                            Tax</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITTax" runat="server" Text='<%#Eval("tax") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("tax") %>' Width="80px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EITtxtTax" runat="server" Enabled="true" Text='<%#Eval("tax") %>'
                                            Width="80px" CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="20" onpaste="return false;"
                                            onChange="getTotalAmount(this.id,'U')"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTtxtTax" runat="server" Enabled="true" CssClass="form-control"
                                            onkeypress="return restrictNumeric(this.id,'2');" onChange="getTotalAmount(this.id,'I')" MaxLength="20" onpaste="return false;"
                                            Width="80px"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <label style="color: Black; font-weight: bold">
                                            Markup</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITMarkup" runat="server" Text='<%#Eval("markup") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("markup") %>' Width="80px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EITtxtMarkup" runat="server" Enabled="true" Text='<%#Eval("markup") %>'
                                            Width="80px" CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="20" onpaste="return false;"
                                            onChange="getTotalAmount(this.id,'U')"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTtxtMarkup" runat="server" Enabled="true" CssClass="form-control"
                                            onkeypress="return restrictNumeric(this.id,'2');" onChange="getTotalAmount(this.id,'I')" MaxLength="20" onpaste="return false;"
                                            Width="80px"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <label style="color: Black; font-weight: bold">
                                            Amount</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITAmount" runat="server" Text='<%#Eval("amount") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("amount") %>' Width="70px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EITtxtAmount" Enabled="false" runat="server" Text='<%#Eval("amount") %>'
                                            Width="70px" CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="20"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTtxtAmount" Enabled="false" runat="server" CssClass="form-control"
                                            onkeypress="return restrictNumeric(this.id,'2');" Width="70px" MaxLength="20"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <label style="color: Black; font-weight: bold">
                                            Min Pax</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITMinPax" runat="server" Text='<%#Eval("MinPax") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("MinPax") %>' Width="70px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EITtxtMinPax" Enabled="true" runat="server" Text='<%#Eval("MinPax") %>'
                                            Width="70px" CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="3" onpaste="return false;"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTtxtMinPax" Enabled="true" runat="server" CssClass="form-control"
                                            onkeypress="return restrictNumeric(this.id,'2');" Width="70px" MaxLength="3" onpaste="return false;"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField Visible="false" >
                                                            <HeaderStyle HorizontalAlign="left" />
                                                            <HeaderTemplate>
                                                                <label style="color: Black;">
                                                                    <span class="mdt"></span>Type</label>
                                                            </HeaderTemplate>
                                                        <ItemTemplate>
                                                           <asp:Label ID="ITlblType" runat="server" Text='<%# Eval("flexTypeDESC") %>' CssClass="labelDtl grdof"
                                                                    ToolTip='<%# Eval("flexTypeDESC") %>' Width="10px"></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                              <asp:DropDownList ID="EITddlType" runat="server" Enabled="true" Maxlength="200" Width="10px">
                                                          
                                                            <asp:ListItem Text="SupplierCost" Value="S"></asp:ListItem>
                                                            <asp:ListItem Text="Tax" Value="T"></asp:ListItem>
                                                            <asp:ListItem Text="Mark Up" Value="M"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            
                                                             <asp:HiddenField id="EIThdfType" runat="server" value='<%# Eval("type") %>'></asp:HiddenField>
                                                         </EditItemTemplate>
                                                           <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                            <FooterTemplate>
                                                                 <asp:DropDownList ID="FTddltype" runat="server" Enabled="true" Maxlength="200" Width="10px">
                                                              <asp:ListItem Text="-SelectType-" Value="0"></asp:ListItem>
                                                             <asp:ListItem Text="SupplierCost" Value="S"></asp:ListItem>
                                                            <asp:ListItem Text="Tax" Value="T"></asp:ListItem>
                                                            <asp:ListItem Text="Mark Up" Value="M"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>--%>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label runat="server" ID="id" Width="200px"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel1_lnkEdit" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit" Width="50px">
                                                </asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkEdit" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="UpdatePanel1_ibtnDelete" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif"
                                                    CausesValidation="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the user?');"></asp:ImageButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="ibtnDelete" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel1_lnkUpdate" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" Width="50px" CommandName="Update"
                                                    OnClientClick="return validateDtlAddUpdate1('U',this.id)"></asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkUpdate" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="UpdatePanel1_lnkCancel" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" Width="50px" CommandName="Cancel"></asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkCancel" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel1_lnkAdd" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkAdd" CommandName="Add" runat="server" Text="Add" OnClientClick="return validateDtlAddUpdate1('I',this.id)"></asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkAdd" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="" />
                            <RowStyle CssClass="" HorizontalAlign="left" />
                            <AlternatingRowStyle CssClass="" />
                        </asp:GridView>
                    </div>
                    <%--      <a href="javascript:void(0)" data-clone-button="PriceDetail" class="pull-left"><span class=" glyphicon glyphicon-plus-sign"></span> Add More</a>
                        <div class="clear"></div>--%>
                </div>
                <div role="tabpanel" class="tab-pane" id="packageTab3">
                    <table>
                        <tr>
                            <td valign="top">
                                <table border="0">
                                    <tr>
                                        <td>
                                            <label>
                                                Image1(145X120):</label><span class="req-star">*</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel1_fuImage1" runat="server">
                                                            <ContentTemplate>
                                                                <asp:FileUpload ID="fuImage1" runat="server" Width="200px" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnUpload1" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Button Style="margin-top: 0px;" class="btn btn-primary rbpackage-btn" ID="btnUpload1"
                                                            runat="server" Text="Upload" OnClick="btnUpload1_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel1_lbUploadMsg" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="lbUploadMsg" runat="server" OnClick="lbUploadMsg_Click"></asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lbUploadMsg" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <asp:Label ID="lblUpload1" runat="server" Visible="false"></asp:Label>
                                                        <div id="dUpload1Msg" style="display: none;">
                                                            <img id="imgupload1" height="50px" width="50px" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Image2(625X250):</label><span class="req-star">*</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 500px">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel2_fuImage2" runat="server">
                                                            <ContentTemplate>
                                                                <asp:FileUpload ID="fuImage2" runat="server" Width="200px" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnUpload2" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td>
                                                        <asp:Button Style="margin-top: 0px;" class="btn btn-primary rbpackage-btn marxs_top5"
                                                            ID="btnUpload2" runat="server" Text="Upload" OnClick="btnUpload2_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel2_lbUpload2Msg" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="lbUpload2Msg" runat="server" OnClick="lbUpload2Msg_Click"></asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lbUpload2Msg" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <asp:Label ID="lblUplaod2" runat="server" Visible="false"></asp:Label>
                                                        <div id="dUpload2Msg" style="display: none;">
                                                            <img id="imgupload2" height="50px" width="50px" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Image3(145X120):</label><span class="req-star">*</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 500px">
                                            <table border="0">
                                                <tr>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel_fuImage3" runat="server">
                                                            <ContentTemplate>
                                                                <asp:FileUpload ID="fuImage3" runat="server" Width="200px" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnUplaod3" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td>
                                                        <asp:Button Style="margin-top: 0px;" class="btn btn-primary rbpackage-btn" ID="btnUplaod3"
                                                            runat="server" Text="Upload" OnClick="btnUpload3_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel_lbUpload3Msg" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="lbUpload3Msg" runat="server" OnClick="lbUpload3Msg_Click"></asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lbUpload3Msg" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <asp:Label ID="lblUpload3" runat="server" Visible="false"></asp:Label>
                                                        <div id="dUpload3Msg" style="display: none;">
                                                            <img id="imgupload3" height="50px" width="50px" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <div id="pnlImages" runat="server" style="display: none; z-index: 9999">
                                    <%--<asp:Panel id="pnlImages" runat="server"  style="position:fixed;right:250px;display:none" >--%>
                                    <div st class="thepet pdac bg_white bor_gray">
                                        <table border="0">
                                            <tr>
                                                <td>
                                                    <div class="ns-h3">
                                                        Preview
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Image runat="server" Style="height: 100px; width: 150px; display: block" ID="img1PreView" />
                                                    <asp:Image runat="server" Style="height: 100px; width: 150px; display: block" ID="img2PreView" />
                                                    <asp:Image runat="server" Style="height: 100px; width: 150px; display: block" ID="img3PreView" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   <input class="button" type="button" onclick="showImage('H', '')" value="Close" />
                                                     <%-- <asp:Button OnClientClick="showImage('H','')" runat="server" ID="btnClose" Text="Close"
                                                                CssClass="button" />--%>
                                                    <%--<asp:UpdatePanel ID="UpdatePanel_btnClose" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Button OnClientClick="showImage('H','')" runat="server" ID="btnClose" Text="Close"
                                                                CssClass="button" />
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnClose" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <%--</asp:Panel>--%>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane rb-container-wrap" id="packageTab4">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label id="lblErrorMessage" style="display: none"></label>
                                <label>
                                    Available Dates<span class="req-star" style="color: red">*</span>
                                </label>
                                <div class="row">
                                    <div class="col-xs-6 col-md-3">
                                        <label>
                                            From
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                            </div>
                                            <%--   <a href="javascript:void()" onclick="showCalendar3()">
                                        <img src="images/call-cozmo.png" alt="Pick Date" /></a>   --%>
                                            <asp:TextBox ID="txtFrom" runat="server" onclick="showCalendar3()" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <label>
                                            &nbsp;</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                            </div>
                                            <asp:TextBox ID="txtFromTime" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <label>
                                            To
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                            </div>
                                            <asp:TextBox onclick="showCalendar4()" ID="txtTo" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <label>
                                            &nbsp;</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                            </div>
                                            <asp:TextBox ID="txtToTime" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label>
                                UnAvailable Dates<span class="req-star"></span></label>
                            <div class="form-group" data-clone-content="UnAvailableDates">
                                <div class="row">
                                    <div class="col-xs-6 col-md-10">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                            </div>
                                            <asp:TextBox onclick="ShowCalender('callContainer')" ID="Date" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <asp:Label ID="lblUnavailableDays" Text="No.of Unavailable Days :" runat="server" Font-Bold="true"> </asp:Label>
                            </div>
                            <div class="marbot_10">
                                <asp:TextBox ID="txtUnavailableDays" runat="server" Width="210px" CssClass="form-control" Text="0" Enabled="false"></asp:TextBox>
                            </div>
                            <asp:HiddenField ID="hdnUnavailableDays" runat="server" />
                        </div>
                    </div>
                    <div>
                        <table width="100%">
                            <tr>
                                <td height="23px" align="right">
                                    <div style="display: none;">
                                        <%if (activityVariables.Count > 0) %>
                                        <%{ %>
                                        <input type="hidden" id="UnAvailDatesCounter" value="<%=activityVariables["UnAvailDatesCount"] %>" />
                                        <input type="hidden" id="UnAvailDatesCurrent" name="UnAvailDatesCurrent" value="<%=activityVariables["UnAvailDatesCount"] %>" />
                                        <%} %>

                                    </div>
                                    <div>
                                        <div id="UnAvailDatesDiv">
                                            <%if (activityVariables.Count > 0 && Convert.ToInt32(activityVariables["UnAvailDatesCount"]) > 1) %>
                                            <%{ %>
                                            <%for (int i = 1; i < Convert.ToInt32(activityVariables["UnAvailDatesCount"]); i++)
                                              {
                                                  if (i == 1)
                                                  { %>
                                            <div id="UnAvailDates-<%=i %>">
                                                <span class="fleft width-300">
                                                    <input id="txtUnAvailDatesText-<%=i %>" name="txtUnAvailDatesText-<%=i %>" class="form-control" type="text" value="<%=(activityVariablesArray["UnAvailDatesValue"].Length==0 || string.IsNullOrEmpty(activityVariablesArray["UnAvailDatesValue"][i - 1])  ? "":activityVariablesArray["UnAvailDatesValue"][i - 1] ) %>" readonly="true" />
                                                </span>
                                                <br />
                                                <br />
                                            </div>
                                            <%}
                                                  else
                                                  { %>
                                            <%if (i < Convert.ToInt32(activityVariables["UnAvailDatesCount"]) - 1)
                                              { %>
                                            <div id="UnAvailDates-<%=i %>">
                                                <span class="fleft width-300">
                                                    <input id="txtUnAvailDatesText-<%=i %>" name="txtUnAvailDatesText-<%=i %>" class="form-control" type="text" value="<%=(activityVariablesArray["UnAvailDatesValue"].Length==0 || string.IsNullOrEmpty(activityVariablesArray["UnAvailDatesValue"][i - 1]) ? "":activityVariablesArray["UnAvailDatesValue"][i - 1] ) %>" readonly="true" />
                                                </span>
                                                <i class="fleft margin-left-10" id="UnAvailDatesImg-<%=i %>" style="display: none">
                                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('UnAvailDates-<%=i %>','text')" />
                                                </i>
                                                <br />
                                                <br />
                                            </div>
                                            <%}
                                              else
                                              { %>
                                            <div id="UnAvailDates-<%=i %>">
                                                <span class="fleft width-300">
                                                    <input id="txtUnAvailDatesText-<%=i %>" name="txtUnAvailDatesText-<%=i %>" class="form-control" type="text" value="<%=(activityVariablesArray["UnAvailDatesValue"].Length==0 || string.IsNullOrEmpty(activityVariablesArray["UnAvailDatesValue"][i - 1]) ? "":activityVariablesArray["UnAvailDatesValue"][i - 1] ) %>" readonly="true" />
                                                </span>
                                                <i class="fleft margin-left-10" id="UnAvailDatesImg-<%=i %>">
                                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('UnAvailDates-<%=i %>','text')" /></i>
                                                <br />
                                                <br />
                                            </div>
                                            <%} %>
                                            <%} %>
                                            <%} %>
                                            <%} %>
                                        </div>

                                        <a href="javascript:void(0)" onclick="javascript:AddDateMore('UnAvailDates')" class="pull-left">
                                            <span class=" glyphicon glyphicon-plus-sign"></span>Add More</a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane rb-container-wrap" id="packageTab5">
                    <div class="row">
                        <table>
                            <tr>
                                <td>
                                </td>
                                <td colspan="5" style="height: 21px">
                                    <div style="display: none;">
                                        <%if (activityVariables.Count > 0) %>
                                        <%{ %>
                                        <input type="hidden" id="ExclusionsCounter" value="<%=activityVariables["exclusionsCount"] %>" />
                                        <input type="hidden" id="ExclusionsCurrent" name="ExclusionsCurrent" value="<%=activityVariables["exclusionsCount"] %>" />
                                        <%} %>
                                    </div>
                                    <div class="col-md-12">
                                        <div>
                                            <b>Exclusions:<span class="req-star" style="color: red">*</span></b>
                                        </div>
                                        <div id="ExclusionsDiv">
                                            <%if (activityVariables.Count > 0) %>
                                            <%{ %>
                                            <%for (int i = 1; i < Convert.ToInt32(activityVariables["exclusionsCount"]); i++)
                                              {
                                                  if (i == 1)
                                                  { %>
                                            <p class="fleft">
                                                <%-- <span style=" width:470px" class="fleft"><input style="width:460px" type="text" id="ExclusionsText-<%=i %>" value='<%=Page.Request["ExclusionsText-"+i] %>' name="ExclusionsText-<%=i %>" class="width-300 fleft" value="<%=activityVariablesArray["exclusionsValue"][i - 1] %>" /></span>--%>
                                                <span style="width: 470px" class="fleft">
                                                    <input type="text" id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>" class="width-300 fleft form-control"
                                                        value="<%=activityVariablesArray["exclusionsValue"].Length == 0  || string.IsNullOrEmpty(activityVariablesArray["exclusionsValue"][i - 1]) ? "" : activityVariablesArray["exclusionsValue"][i - 1] %>" onkeypress="return isAlphaNumeric(event);" /></span>
                                            </p>
                                            <%}
                                                  else
                                                  { %>
                                            <%if (i < Convert.ToInt32(activityVariables["exclusionsCount"]) - 1)
                                              { %>
                                            <p class="fleft width-100 padding-top-5" id="Exclusions-<%=i %>">
                                                <span class="fleft">
                                                    <input id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>" class="width-300 fleft form-control"
                                                        type="text" value="<%=activityVariablesArray["exclusionsValue"].Length == 0  || string.IsNullOrEmpty(activityVariablesArray["exclusionsValue"][i - 1]) ? "" : activityVariablesArray["exclusionsValue"][i - 1] %>" onkeypress="return isAlphaNumeric(event);" /></span>
                                                <%--<span class="fleft width-300""><input id="Text1" name="ExclusionsText-<%=i %>" value='<%=Page.Request["ExclusionsText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                                <i style="display: none;" class="fleft margin-left-10" id="ExclusionsImg-<%=i %>">
                                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Exclusions-<%=i %>','text')" /></i>
                                            </p>
                                            <%}
                                              else
                                              { %>
                                            <p class="fleft width-100 padding-top-5" id="Exclusions-<%=i %>">
                                                <span class="fleft width-300">
                                                    <input id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>" class="width-300 fleft form-control"
                                                        type="text" value="<%=activityVariablesArray["exclusionsValue"].Length == 0  || string.IsNullOrEmpty(activityVariablesArray["exclusionsValue"][i - 1]) ? "" : activityVariablesArray["exclusionsValue"][i - 1] %>" onkeypress="return isAlphaNumeric(event);" /></span>
                                                <%--<span class="fleft width-300"><input id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>"  value='<%=Page.Request["ExclusionsText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                                <i class="fleft margin-left-10" id="ExclusionsImg-<%=i %>">
                                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Exclusions-<%=i %>','text')" /></i>
                                            </p>
                                            <%} %>
                                            <%} %>
                                            <%} %>
                                            <%} %>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <div class="marbot_20">
                                            <a href="javascript:void(0)" onclick="javascript:AddTextBox('Exclusions')"><span
                                                class=" glyphicon glyphicon-plus-sign"></span>Add More</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="5">
                                    <asp:HiddenField runat="server" ID="hdfinclCounter" Value="2" />
                                    <asp:HiddenField runat="server" ID="hdfExclCounter" Value="2" />
                                    <asp:HiddenField runat="server" ID="hdfCanPolCounter" Value="2" />
                                    <asp:HiddenField runat="server" ID="hdfThingsCounter" Value="2" />
                                    <asp:HiddenField runat="server" ID="hdfUnavailDateCounter" Value="2" />
                                    <asp:HiddenField runat="server" ID="hdfinclusion" Value="" />
                                    <asp:HiddenField runat="server" ID="hdfExclusions" Value="" />
                                    <asp:HiddenField runat="server" ID="hdfCanPolicy" Value="" />
                                    <asp:HiddenField runat="server" ID="hdfThings" Value="" />
                                    <asp:HiddenField runat="server" ID="hdfUnavailDates" Value="" />
                                    <%--<td align="left"><asp:TextBox ID="txtInclusion" runat="server"></asp:TextBox></td>
    <td><asp:Button ID="btnInclusion" Text ="Add" runat="server" Width="68"
            onclick="btnInclusion_Click" /></td>--%>
                                    <%-- <input name="tst" type="text" value='<%=Page.Request["tst"] %>' />--%>
                                    <div style="display: none;">
                                        <%if (activityVariables.Count > 0) %>
                                        <%{ %>
                                        <input type="hidden" id="InclusionsCounter" name="hdInclusionsCounter" value="<%=activityVariables["inclusionsCount"] %>" />
                                        <input type="hidden" id="InclusionsCurrent" name="InclusionsCurrent" value="<%=activityVariables["inclusionsCount"] %>" />
                                        <%} %>
                                    </div>
                                    <div class="col-md-12">
                                        <div>
                                            <b>Inclusions :<span class="req-star" style="color: red">*</span></b>
                                        </div>
                                        <div id="InclusionsDiv">
                                            <%if (activityVariables.Count > 0)
                                              {
                                                  for (int i = 1; i < Convert.ToInt32(activityVariables["inclusionsCount"]); i++)
                                                  {
                                                      if (i == 1)
                                                      { %>
                                            <p class="fleft">
                                                <span style="width: 470px" class="fleft">
                                                    <input type="text" id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" class="width-300 fleft form-control"
                                                        value="<%=activityVariablesArray["inclusionsValue"].Length == 0 || string.IsNullOrEmpty(activityVariablesArray["inclusionsValue"][i - 1]) ? "" : activityVariablesArray["inclusionsValue"][i - 1] %>" onkeypress="return isAlphaNumeric(event);" /></span>
                                            </p>
                                            <%}
                                                      else
                                                      { %>
                                            <%if (i < Convert.ToInt32(activityVariables["inclusionsCount"]) - 1)
                                              { %>
                                            <p class="fleft width-100 padding-top-5" id="Inclusions-<%=i %>">
                                                <%--<span class="fleft"><input id="Text1" name="InclusionsText-<%=i %>" value='<%=Page.Request["InclusionsText-"+i] %>' class="width-300 fleft" type="text" value="<%=activityVariablesArray["inclusionsValue"][i - 1] %>" /></span>--%>
                                                <span class="fleft width-300">
                                                    <input id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" value="<%=activityVariablesArray["inclusionsValue"].Length == 0 || string.IsNullOrEmpty(activityVariablesArray["inclusionsValue"][i - 1]) ? "" : activityVariablesArray["inclusionsValue"][i - 1] %>" class="width-300 fleft form-control" type="text" onkeypress="return isAlphaNumeric(event);" />
                                                </span>
                                                <i style="display: none;" class="fleft margin-left-10" id="InclusionsImg-<%=i %>">
                                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Inclusions-<%=i %>','text')" /></i>
                                            </p>
                                            <%}
                                              else
                                              { %>
                                            <p class="fleft width-100 padding-top-5" id="Inclusions-<%=i %>">
                                                <%--<span class="fleft width-300"><input id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" value='<%=Page.Request["InclusionsText-"+i] %>' class="width-300 fleft" type="text" value="<%=activityVariablesArray["inclusionsValue"][i - 1] %>" /></span>--%>
                                                <span class="fleft width-300">
                                                    <input id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" value="<%=activityVariablesArray["inclusionsValue"].Length == 0 || string.IsNullOrEmpty(activityVariablesArray["inclusionsValue"][i - 1]) ? "" : activityVariablesArray["inclusionsValue"][i - 1] %>" class="width-300 fleft  form-control" type="text" onkeypress="return isAlphaNumeric(event);" />
                                                </span>
                                                <i class="fleft margin-left-10" id="InclusionsImg-<%=i %>">
                                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Inclusions-<%=i %>','text')" /></i>
                                            </p>
                                            <%} %>
                                            <%} %>
                                            <%} %>
                                            <%} %>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <div class="marbot_20">
                                            <a href="javascript:void(0)" onclick="javascript:AddTextBox('Inclusions')"><span
                                                class=" glyphicon glyphicon-plus-sign"></span>Add More</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="5" style="height: 21px">
                                    <div style="display: none;">
                                        <%if (activityVariables.Count > 0) %>
                                        <%{ %>
                                        <input type="hidden" id="CancellPolicyCounter" value="<%=Convert.ToInt32(activityVariables["CancellPolicyCount"])%>" />
                                        <input type="hidden" id="CancellPolicyCurrent" name="CancellPolicyCurrent" value="<%=Convert.ToInt32(activityVariables["CancellPolicyCount"])%>" />
                                        <%} %>
                                    </div>
                                    <div class="col-md-12">
                                        <div>
                                            <b>Cancellation Policy :<span class="req-star" style="color: red">*</span></b>
                                        </div>
                                        <div id="CancellPolicyDiv">
                                            <%if (activityVariables.Count > 0) %>
                                            <%{ %>

                                            <%for (int i = 1; i < Convert.ToInt32(activityVariables["CancellPolicyCount"]); i++)
                                              {
                                                  if (i == 1)
                                                  { %>
                                            <p class="fleft">
                                                <%-- <span style=" width:470px;" class="fleft"><input style=" width:460px" type="text" id="CancellPolicyText-<%=i %>" value='<%=Page.Request["CancellPolicyText-"+i] %>' name="CancellPolicyText-<%=i %>"  class="width-300 fleft" value="<%=activityVariablesArray["CancellPolicyValue"][i - 1] %>" /></span>--%>
                                                <span style="width: 470px;" class="fleft">
                                                    <input type="text" id="CancellPolicyText-<%=i %>" name="CancellPolicyText-<%=i %>" class="width-300 fleft form-control"
                                                        value="<%=activityVariablesArray["CancellPolicyValue"].Length == 0 || string.IsNullOrEmpty(activityVariablesArray["CancellPolicyValue"][i - 1]) ? "" : activityVariablesArray["CancellPolicyValue"][i - 1] %>" onkeypress="return isAlphaNumeric(event);" /></span>
                                            </p>
                                            <%}
                                                  else
                                                  { %>
                                            <%if (i < Convert.ToInt32(activityVariables["CancellPolicyCount"]) - 1)
                                              { %>
                                            <p class="fleft width-100 padding-top-5" id="CancellPolicy-<%=i %>">
                                                <span class="fleft width-300">
                                                    <input id="CancellPolicyText-<%=i %>" name="CancellPolicyText-<%=i %>" class="width-300 fleft form-control" type="text"
                                                        value="<%=activityVariablesArray["CancellPolicyValue"].Length == 0 || string.IsNullOrEmpty(activityVariablesArray["CancellPolicyValue"][i - 1]) ? "" : activityVariablesArray["CancellPolicyValue"][i - 1] %>" onkeypress="return isAlphaNumeric(event);" /></span>
                                                <%-- <span class="fleft width-300"><input id="Text8" name="CancellPolicyText-<%=i %>" value='<%=Page.Request["CancellPolicyText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                                <i style="display: none;" class="fleft margin-left-10" id="CancellPolicyImg-<%=i %>">
                                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('CancellPolicy-<%=i %>','text')" /></i>
                                            </p>
                                            <%}
                                              else
                                              { %>
                                            <p class="fleft width-100 padding-top-5" id="CancellPolicy-<%=i %>">
                                                <span class="fleft width-300">
                                                    <input id="CancellPolicyText-<%=i %>" name="CancellPolicyText-<%=i %>" class="width-300 fleft form-control"
                                                        type="text" value="<%=activityVariablesArray["CancellPolicyValue"].Length == 0 || string.IsNullOrEmpty(activityVariablesArray["CancellPolicyValue"][i - 1]) ? "" : activityVariablesArray["CancellPolicyValue"][i - 1] %>" onkeypress="return isAlphaNumeric(event);" /></span>
                                                <%--<span class="fleft width-300"><input id="Text7" name="CancellPolicyText-<%=i %>" value='<%=Page.Request["CancellPolicyText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                                <i class="fleft margin-left-10" id="CancellPolicyImg-<%=i %>">
                                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('CancellPolicy-<%=i %>','text')" /></i>
                                            </p>
                                            <%} %>
                                            <%} %>
                                            <%} %>
                                            <%} %>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="marbot_20">
                                            <a href="javascript:void(0)" onclick="javascript:AddTextBox('CancellPolicy')" class="pull-left">
                                                <span class=" glyphicon glyphicon-plus-sign"></span>Add More</a>
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="row">
                        <table>
                            <tr>
                                <td>
                                </td>
                                <td colspan="5" style="height: 21px">
                                    <div style="display: none;">
                                        <%if (activityVariables.Count > 0) %>
                                        <%{ %>
                                        <input type="hidden" id="ThingsToBringCounter" value="<%=Convert.ToInt32(activityVariables["ThingsToBringCount"])%>" />
                                        <input type="hidden" id="ThingsToBringCurrent" name="ThingsToBringCurrent" value="<%=Convert.ToInt32(activityVariables["ThingsToBringCount"])%>" />
                                         <%} %>
                                    </div>
                                    <div class="col-md-12">
                                        <div>
                                            <b>Things to Bring :</b>
                                        </div>
                                        <div id="ThingsToBringDiv">
                                            <%if (activityVariables.Count > 0) %>
                                            <%{ %>
                                            <%for (int i = 1; i < Convert.ToInt32(activityVariables["ThingsToBringCount"]); i++)
                                              {
                                                  if (i == 1)
                                                  { %>
                                            <p class="fleft">
                                                <%-- <span style=" width:470px;" class="fleft"><input style=" width:460px" type="text" id="Text3" name="ThingsToBringText-<%=i %>" value='<%=Page.Request["ThingsToBringText-"+i] %>' class="width-300 fleft" value="<%=activityVariablesArray["ThingsToBringValue"][i - 1] %>" /></span>--%>
                                                <span style="width: 470px;" class="fleft">
                                                    <input type="text" id="ThingsToBringText-<%=i %>" name="ThingsToBringText-<%=i %>" class="width-300 fleft form-control"
                                                        value="<%=activityVariablesArray["ThingsToBringValue"].Length == 0 || string.IsNullOrEmpty(activityVariablesArray["ThingsToBringValue"][i - 1]) ? "" : activityVariablesArray["ThingsToBringValue"][i - 1] %>" onkeypress="return isAlphaNumeric(event);" /></span>
                                            </p>
                                            <%}
                                                  else
                                                  { %>
                                            <%if (i < Convert.ToInt32(activityVariables["ThingsToBringCount"]) - 1)
                                              { %>
                                            <p class="fleft width-100 padding-top-5" id="ThingsToBring-<%=i %>">
                                                <span class="fleft width-300">
                                                    <input id="ThingsToBringText-<%=i %>" name="ThingsToBringText-<%=i %>" class="width-300 fleft form-control" type="text"
                                                        value="<%=activityVariablesArray["ThingsToBringValue"].Length == 0 || string.IsNullOrEmpty(activityVariablesArray["ThingsToBringValue"][i - 1]) ? "" : activityVariablesArray["ThingsToBringValue"][i - 1] %>" onkeypress="return isAlphaNumeric(event);" /></span>
                                                <%--<span class="fleft width-300"><input id="Text2" name="ThingsToBringText-<%=i %>" value='<%=Page.Request["ThingsToBringText-"+i] %>' class="width-300 fleft" type="text" /></span>--%>
                                                <i style="display: none;" class="fleft margin-left-10" id="ThingsToBringImg-<%=i %>">
                                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('ThingsToBring-<%=i %>','text')" /></i>
                                            </p>
                                            <%}
                                              else
                                              { %>
                                            <p class="fleft width-100 padding-top-5" id="ThingsToBring-<%=i %>">
                                                <span class="fleft width-300">
                                                    <input id="ThingsToBringText-<%=i %>" name="ThingsToBringText-<%=i %>" class="width-300 fleft form-control"
                                                        type="text" value="<%=activityVariablesArray["ThingsToBringValue"].Length == 0 || string.IsNullOrEmpty(activityVariablesArray["ThingsToBringValue"][i - 1]) ? "" : activityVariablesArray["ThingsToBringValue"][i - 1] %>" onkeypress="return isAlphaNumeric(event);" /></span>
                                                <%--<span class="fleft width-300"><input id="ThingsToBringText-<%=i %>" name="ThingsToBringText-<%=i %>"value='<%=Page.Request["ThingsToBringText-"+i] %>'  class="width-300 fleft" type="text"  /></span>--%>
                                                <i class="fleft margin-left-10" id="ThingsToBringImg-<%=i %>">
                                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('ThingsToBring-<%=i %>','text')" /></i>
                                            </p>
                                            <%} %>
                                            <%} %>
                                            <%} %>
                                            <%} %>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="marbot_20">
                                            <a href="javascript:void(0)" onclick="javascript:AddTextBox('ThingsToBring')"><span
                                                class=" glyphicon glyphicon-plus-sign"></span>Add More</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <asp:Label Font-Bold="true" ID="lblBookingCutOff" runat="server" Text="Booking CutOff:"> </asp:Label>
                            </div>
                            <div>
                                <asp:DropDownList ID="ddlBookingCutOff" runat="server" CssClass="form-control" Width="210px">
                                    <asp:ListItem Text="1" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--<button type="submit" class="but but_b pull-right btn-xs-block margin-top-10">Submit</button>--%>
    
    <table>
        <tr>
            <td class="col-lg-12"></td>
            <td>
                <div id="divTotal">
                    <table id="tblControls" style="display: none;">
                        <tr>
                            <td>
                                <asp:Label ID="lblTotal" Text="Total:" runat="server" CssClass="label"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtTotal" runat="server" Enabled="false" CssClass="inputDisabled form-control"
                                    Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button class="fright button" ID="SaveButton" Text="Save" runat="server" OnClick="btnSave_Click"
                                    OnClientClick="return Save();" />
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td>
                <div id="divSubmit">
                    <input class="fright button" type="button" id="btnSubmit" value="Submit" onclick="javascript: navigateTabs()" />
                </div>
            </td>
            <td>
                <div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnClear" Text="Clear" runat="server" Font-Bold="true" CssClass="button"
                                OnClick="btnClear_Click" OnClientClick="return tabsClear();"></asp:Button>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnClear" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </td>
        </tr>
    </table>
    <table cellpadding="0" width="100%" cellspacing="0" border="0">
        <tr>
            <td>
                <div id="errMess" class="error_module" style="display: none;">
                    <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
    <script>
        $(function () {
            var dataCloneBtn = $('[data-clone-button]'),
        dataCloneid = 0;
            dataCloneBtn.each(function () {
                var $this = $(this),
            btnID = $this.attr('data-clone-button'),
            dataCloneCnt = $('[data-clone-content="' + btnID + '"]');
                dataCloneCnt.attr('id', btnID + '-' + ++dataCloneid);
                $this.click(function () {
                    dataCloneCnt.clone(true, true).insertBefore($this);
                    dataCloneCnt.attr('id', btnID + '-' + ++dataCloneid);
                })
            })
        })
    </script>

    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0"></iframe>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
