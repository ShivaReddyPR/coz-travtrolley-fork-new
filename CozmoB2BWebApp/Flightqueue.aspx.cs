﻿using System.Collections;
using System.Web.UI.WebControls;
using CT.Core;
using CT.BookingEngine;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Web.Services;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Web.Script.Serialization;
using System.Linq;
using CT.AccountingEngine;
using System.Configuration;

namespace CozmoB2BWebApp
{
    public partial class Flightqueue : CT.Core.ParentPage
    {
        protected DateTime FromDate;
        protected DateTime ToDate;
        protected int Status;
        protected String paxName;
        protected String PnrNo;
        protected String source;
        protected int LocationId;
        protected int AgentFilter;
        protected string AgentType;
        protected String TransType;
        protected String RoutingTripId;
        protected DataSet DropdownDs = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {

                Settings.LoginInfo.IsOnBehalfOfAgent = false;
                if (Request["FromDate"] != null)
                {
                    txtFromDate.Text = Request["FromDate"];
                }
                else
                {
                    txtFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    //txtFromDate.Text = "01/01/2018";
                }

                txtToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                hdfLAgentType.Value = Settings.LoginInfo.AgentType.ToString();
                InitializeControls();
            }
        }
        private void InitializeControls()
        {
            try
            {
                DropdownDs = GetDropDownValues(Settings.LoginInfo.AgentId);
                Array Statuses = Enum.GetValues(typeof(BookingStatus));
                Array Sources = Enum.GetValues(typeof(BookingSource));
                foreach (BookingStatus status in Statuses)
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(BookingStatus), status), ((int)status).ToString());
                    if ((int)status == 1 || (int)status == 2 || (int)status == 4 || (int)status == 5 || (int)status == 6 || (int)status == 6 || (int)status == 7 || (int)status == 12 || (int)status == 15 || (int)status == 16 || (int)status == 17 || (int)status == 18 || (int)status == 19)
                    {
                        ddlBookingStatus.Items.Add(item);
                    }
                }
                foreach (BookingSource source in Sources)
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(BookingSource), source), ((int)source).ToString());

                    ddlsource.Items.Add(item);
                }
                BindAgent();//Binding BaseAgent and Agent in PageLoad
                BindB2BAgent(); //Binding B2B in PageLoad                 
                BindB2B2BAgent(); //Binding B2B2B in PageLoad
                //Binding Location in PageLoad
                ddlLocation.DataSource = DropdownDs.Tables[3];
                ddlLocation.DataTextField = "location_name";
                ddlLocation.DataValueField = "location_id";
                ddlLocation.DataBind();
                ddlLocation.Items.Insert(0, new ListItem("--All--", "0"));
                ddlLocation.SelectedValue = Convert.ToString(Settings.LoginInfo.LocationID);
                if (Settings.LoginInfo.AgentType.ToString() == "BaseAgent" || Settings.LoginInfo.AgentType.ToString() == "Agent")
                {
                    if (Settings.LoginInfo.AgentType.ToString() == "Agent")
                    {
                        ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                        ddlAgency.Enabled = false;
                    }
                    ddlAgency.Items.Insert(0, new ListItem("--All--", "0"));
                    ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                        ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select --", "-1"));
                        ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
                        ddlB2BAgent.Items.Insert(0, new ListItem("-- Select --", "-1"));
                        ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
                }
                else if (Settings.LoginInfo.AgentType.ToString() == "B2B")
                {
                    ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select --", "-1"));
                    ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
                    ddlAgency.Enabled = false;
                    ddlB2BAgent.Enabled = false;
                }
                else if (Settings.LoginInfo.AgentType.ToString() == "B2B2B")
                {
                    ddlAgency.Enabled = false;
                    ddlB2BAgent.Enabled = false;
                    ddlB2B2BAgent.Enabled = false;
                }
                if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
                {
                    ddlLocation.Enabled = true;
                }
                else
                {
                    ddlLocation.Enabled = false;
                }
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
                {
                    ddlB2B2BAgent.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindAgent()
        {
            try
            {
                if (DropdownDs.Tables[0] != null && DropdownDs.Tables[0].Rows != null && DropdownDs.Tables[0].Rows.Count > 0)
                {
                    DataTable dtAgents = DropdownDs.Tables[0];// AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
                    ddlAgency.DataSource = dtAgents;
                    ddlAgency.DataTextField = "Agent_Name";
                    ddlAgency.DataValueField = "agent_id";
                    ddlAgency.DataBind();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindB2BAgent()
        {
            try
            {
                if (DropdownDs.Tables[1] != null && DropdownDs.Tables[1].Rows != null && DropdownDs.Tables[1].Rows.Count > 0)
                {
                    DataTable dtB2BAgents = DropdownDs.Tables[1];
                    ddlB2BAgent.DataSource = dtB2BAgents;
                    ddlB2BAgent.DataTextField = "Agent_Name";
                    ddlB2BAgent.DataValueField = "agent_id";
                    ddlB2BAgent.DataBind();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindB2B2BAgent()
        {
            try
            {
                if (DropdownDs.Tables[2] != null && DropdownDs.Tables[2].Rows != null && DropdownDs.Tables[2].Rows.Count > 0)
                {
                    DataTable dtB2B2BAgents = DropdownDs.Tables[2];
                    ddlB2B2BAgent.DataSource = dtB2B2BAgents;
                    ddlB2B2BAgent.DataTextField = "Agent_Name";
                    ddlB2B2BAgent.DataValueField = "agent_id";
                    ddlB2B2BAgent.DataBind();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region WEBMETHODS
        [WebMethod]
        public static ArrayList BindAllAgentTypes(int agentId, string AgentType)
        {
            ArrayList list = new ArrayList();
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, AgentType, agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            if (dtAgents != null && dtAgents.Rows.Count > 0)
            {
                foreach (DataRow dr in dtAgents.Rows)
                {
                    list.Add(new ListItem(
              dr["Agent_Name"].ToString(),
              dr["agent_id"].ToString()
               ));
                }
            }
            return list;
        }
        [WebMethod]
        public static ArrayList BindLocation(int agentId, string type)
        {
            ArrayList list = new ArrayList();
            DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);
            if (dtLocations != null && dtLocations.Rows.Count > 0)
            {
                foreach (DataRow dr in dtLocations.Rows)
                {
                    list.Add(new ListItem(
              dr["location_name"].ToString(),
              dr["location_id"].ToString()
               ));
                }
            }
            return list;
        }
        [WebMethod]
        public static string Search(String FromDate,String ToDate,int Status,string PaxName,string PnrNo,int LocationId,string RoutingTripId,int source,int AgentFilter,string agentType,string transType)
        {
            try
            {
                DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
                IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
                startDate = (FromDate != string.Empty) ? Convert.ToDateTime(FromDate, provider) : DateTime.Now;
                endDate = (ToDate != string.Empty) ? Convert.ToDateTime(Convert.ToDateTime(ToDate, provider).ToString("dd/MM/yyyy 23:59"), provider) : DateTime.Now;
                string whereString = CT.Core.Queue.GetWhereStringForAgentBookingQueue(AgentFilter, PaxName, PnrNo, Status, false, "", "", LocationId, startDate, endDate, agentType, transType, source, RoutingTripId);
                DataTable dt = CT.Core.Queue.GetFlightQueueList(whereString);
                // DataTable dt =CT.Core.Queue.GetFlightQueueList(startDate, endDate, AgentFilter, LocationId, PnrNo, PaxName, RoutingTripId, transType, agentType, Status, source);
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                var Result = (from c in dt.AsEnumerable()
                              select new
                              {
                                  AgentDecimal = c.Field<int>("AgentDecimal"),
                                  AgentId = c.Field<int>("AgentId"),
                                  BookingAgentName = c.Field<string>("BookingAgentName"),
                                  AgentBalance = c.Field<decimal>("AgentBalance"),
                                  AgentphoneNO1 = c.Field<string>("AgentphoneNO1"),
                                  AgentphoneNO2 = c.Field<string>("AgentphoneNO2"),
                                  AgentEmail = c.Field<string>("AgentEmail"),
                                  AgentCurrency = c.Field<string>("AgentCurrency"),
                                  AgentCountry = c.Field<int>("AgentCountry"),
                                  bookingId = c.Field<int>("bookingId"),
                                  bookingstatus = c.Field<string>("bookingstatus"),
                                  UserName = c.Field<string>("UserName"),
                                  createdOn = c.Field<DateTime>("createdOn"),
                                  flightId = c.Field<int>("flightId"),
                                  pnr = c.Field<string>("pnr"),
                                  routing = c.Field<string>("routing"),
                                  source = c.Field<string>("source"),
                                  airlineCOde = c.Field<string>("airlineCOde"),
                                  travelDate = c.Field<DateTime>("travelDate"),
                                  TotalFare = c.Field<decimal>("TotalFare"),
                                  RoutingTripID = c.Field<string>("RoutingTripID"),
                                  locationName = c.Field<string>("locationName"),
                                  TransType = c.Field<string>("TransType"),
                                  TripId = c.Field<string>("TripId"),
                                  //AdultCount = c.Field<int>("AdultCount"),
                                  leadpaxName = c.Field<string>("leadpaxName"),
                                  paymentMode = c.Field<string>("paymentMode"),
                                  FlightNum = c.Field<string>("FlightNum"),
                                  OnBehalf=c.Field<string>("OnBehalf")
                              }).ToList();
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(Result);
                return JSONString;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        [WebMethod]
        public static List<string> GetPaymentInfo(int bookingId)
        {
            CreditCardPaymentInformation paymentInfo = new CreditCardPaymentInformation();
            paymentInfo.Load(bookingId);
            List<string> Payment = new List<string>();
            if (paymentInfo.PaymentId != null) { 
            Payment.Add(paymentInfo.TrackId);
            Payment.Add(paymentInfo.PaymentId);
            Payment.Add(paymentInfo.Amount.ToString("#0.00"));
            Payment.Add(paymentInfo.Charges.ToString("#0.00"));
            Payment.Add(Convert.ToString((CT.AccountingEngine.PaymentGatewaySource)paymentInfo.PaymentGateway));
        }
            return Payment;
        }
        [WebMethod]
        public static string SubmitRequest(int FlightId,string Remarks, string RequestText, int RequestId )
        {
            BookingDetail booking = new BookingDetail();
                    FlightItinerary itinerary = new FlightItinerary(FlightId);
                    int bookingId = itinerary.BookingId;
                    booking = new BookingDetail(bookingId);
                    string data = Remarks;
                    AgentMaster agency = new AgentMaster(itinerary.AgencyId);
                    if (itinerary != null)
                    {
                        ServiceRequest serviceRequest = new ServiceRequest();
                        serviceRequest.BookingId = bookingId;
                        serviceRequest.ReferenceId = itinerary.FlightId;
                        serviceRequest.ProductType = ProductType.Flight;
                        serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType),RequestId.ToString());
                        serviceRequest.Data = data;
                        serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;
                        serviceRequest.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                        serviceRequest.ItemTypeId = InvoiceItemTypeId.Ticketed;
                        serviceRequest.AgencyId = itinerary.AgencyId;
                        serviceRequest.AgencyTypeId = Agencytype.Cash;
                        serviceRequest.IsDomestic = itinerary.IsDomestic;
                        serviceRequest.PaxName = itinerary.Passenger[0].Title + " " + itinerary.Passenger[0].FirstName + " " + itinerary.Passenger[0].LastName; //tempTicket.Title + " " + tempTicket.PaxFirstName + " " + tempTicket.PaxLastName;
                        serviceRequest.Pnr = itinerary.PNR;
                        serviceRequest.ReferenceNumber = itinerary.PNR;
                        serviceRequest.Source = (int)itinerary.FlightBookingSource;
                        serviceRequest.PriceId = itinerary.Passenger[0].Price.PriceId;
                        serviceRequest.SupplierName = itinerary.AirlineCode;
                        serviceRequest.StartDate = itinerary.TravelDate;
                        serviceRequest.RequestSourceId = RequestSource.BookingAPI;
                        serviceRequest.AirlineCode = itinerary.ValidatingAirline;
                        BookingHistory bh = new BookingHistory();
                        bh.BookingId = bookingId;
                        bh.EventCategory = EventCategory.Ticketing;
                        bh.Remarks = "Request sent to refund " + itinerary.PNR;
                        bh.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                        using (System.Transactions.TransactionScope setTransaction = new System.Transactions.TransactionScope())
                        {
                            try
                            {
                                switch (RequestText)
                                {
                                    case "Void":
                                        booking.SetBookingStatus(BookingStatus.VoidInProgress, Convert.ToInt32(Settings.LoginInfo.UserID));
                                        break;
                                    case "Refund":
                                        booking.SetBookingStatus(BookingStatus.RefundInProgress, Convert.ToInt32(Settings.LoginInfo.UserID));
                                        break;
                                    case "Modification":
                                        booking.SetBookingStatus(BookingStatus.ModificationInProgress, Convert.ToInt32(Settings.LoginInfo.UserID));
                                        break;
                                }
                                serviceRequest.Save();
                                bh.Save();
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            setTransaction.Complete();
                        }
                        //Sending email.
                        Hashtable table = new Hashtable();
                        table.Add("agentName", agency.Name);
                        table.Add("pnrNo", itinerary.PNR);
                        table.Add("reqType",RequestText);

                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                        UserMaster bookedBy = new UserMaster(itinerary.CreatedBy);
                        AgentMaster bookedAgency = new AgentMaster(itinerary.AgencyId);
                        toArray.Add(bookedBy.Email);
                        toArray.Add(bookedAgency.Email1);
                        toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
                        string message = ConfigurationManager.AppSettings["FLIGHT_CANCEL_REQUEST"];
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Flight)Request for cancellation. PNR No:(" + itinerary.PNR + ")", message, table);
                    }
                return "Success";
        }
        #endregion
        public static DataSet GetDropDownValues(int Agent_id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_LOGIN_AGENT_ID", Agent_id);
                return DBGateway.ExecuteQuery("USP_FlightQueue_Get_DropDown_values", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
