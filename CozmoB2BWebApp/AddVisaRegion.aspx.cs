﻿using System;
using CT.Core;
using System.Web.UI;
using CT.TicketReceipt.BusinessLayer;
using Visa;

public partial class AddVisaRegion : CT.Core.ParentPage
{
    public int UpdateRegion = 0;
    protected void Page_Load(object sender, EventArgs e)
    {

        AuthorizationCheck();
        if (!IsPostBack)
        {
            Page.Title = "Add Region";

            if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
            {
                int Id;
                bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out Id);
                if (!isnumerice)
                {
                    Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                    Response.End();

                }
                try
                {
                    VisaRegion region = new VisaRegion(Id);

                    if (region != null)
                    {
                        UpdateRegion = 1;
                        txtRegionCode.Text = region.RegionCode;
                        txtRegionName.Text = region.RegionName;
                        btnSave.Text = "Update";
                        Page.Title = "Update Region";
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaRegion.aspx,Err:" + ex.Message,"");
                }

            }
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        int errMsg = 0;
        try
        {
           
            VisaRegion region = new VisaRegion();
            region.RegionCode = txtRegionCode.Text.Trim();
            region.RegionName = txtRegionName.Text.Trim();
            if (btnSave.Text == "Update")
            {
                if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
                {
                    region.RegionId = Convert.ToInt32(Request.QueryString[0]);
                    region.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                    errMsg = region.UpdateRegion();
                }
            }
            else
            {
                region.IsActive = true;
                region.CreatedBy = (int)Settings.LoginInfo.UserID;
                errMsg = region.Save();
            }

            
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaRegion.aspx,Err:" + ex.Message, "");
        }
        if (errMsg == 0)
        {
            Response.Redirect("VisaRegionList.aspx");
        }
        else if (errMsg == 1)
        {
            lblErrorMessage.Text = "Visa region code already exists";
        }
        else if (errMsg == 2)
        {
            lblErrorMessage.Text = "Visa region name already exists";
        }
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
