﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using System.Collections.Generic;
using CT.Core;
public partial class B2CSettings : CT.Core.ParentPage
{
    private string USER_SESSION = "_UserMaster";
    private string USER_ROLE_SESSION = "_UserRoleList";
    private const string ACTIVE = "A";
    private const string DELETED = "D";
    private enum PageMode
    {
        Add = 1,
        Update = 2
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;

        if (Settings.LoginInfo != null)
        {
            try
            {

                if (!IsPostBack)
                {
                    ViewState["Pwd"] = string.Empty;
                    ViewState["ConPwd"] = string.Empty;
                    ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                    Mode = PageMode.Add;
                    InitializePageControls();
                    lblSuccessMsg.Text = string.Empty;
                }
                else
                {
                    ViewState["Pwd"] = txtPassword.Text;
                    ViewState["ConPwd"] = txtConfirmPassword.Text;
                    lblSuccessMsg.Text = string.Empty;
                    lnkAir.Style.Add("display", "none");
                    lnkHotel.Style.Add("display", "none");
                    lnkInsurance.Style.Add("display", "none");
                    lnkVisa.Style.Add("display", "none");
                    lnkActivity.Style.Add("display", "none");
                    lnkFixDep.Style.Add("display", "none");
                    lnkSightseeing.Style.Add("display", "none");
                    lnkTransfer.Style.Add("display", "none");
                    hdnAgentId.Value = "0";
                    hdnUserId.Value = "0";
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (Settings.LoginInfo != null ? (int)Settings.LoginInfo.UserID : 1), ex.ToString(), "");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        else
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
    private void InitializePageControls()
    {
        try
        {
             
            BindAgent();
            
            Clear();
            long userID = UserMaster.GetPrimaryUserId(Utility.ToLong(ddlParentAgent.SelectedItem.Value));
            if (userID > 0)
            {
                Edit(userID);
                BindB2CMenus(Convert.ToInt32(ddlParentAgent.SelectedValue), userID);
            }
            else
            {
                BindSubAgent();
                BindLocation(Utility.ToInteger(ddlParentAgent.SelectedItem.Value));
                LoadAgentDetails(Utility.ToLong(ddlParentAgent.SelectedItem.Value));
                if (ddlParentAgent.SelectedIndex == 0) ddlLocation.Enabled = false;
            }
        }
        catch { throw; }
    }

    private void BindLocation(int agentId)
    {
        try
        {
            ddlLocation.DataSource = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, "");
            ddlLocation.DataValueField = "LOCATION_ID";
            ddlLocation.DataTextField = "LOCATION_NAME";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("--Select Location--", "-1"));
        }
        catch { throw; }
    }
    private void BindAgent()
    {
        try
        {
            int agentId = 0;
            if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
            string agentType = (Settings.LoginInfo.AgentType.ToString() != null ? Settings.LoginInfo.AgentType.ToString() : "BASEAGENT");
            ddlParentAgent.DataSource = AgentMaster.GetList(1, agentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//TODO
            ddlParentAgent.DataValueField = "agent_id";
            ddlParentAgent.DataTextField = "agent_name";
            ddlParentAgent.DataBind();
            ddlParentAgent.Items.Insert(0, new ListItem("--Select Agent--", "-1"));

            if (agentType == AgentType.B2B.ToString() || agentType == AgentType.B2B2B.ToString())
            {
                ddlParentAgent.Items.Insert(1, new ListItem(Settings.LoginInfo.AgentName, Settings.LoginInfo.AgentId.ToString()));
            }
            ddlParentAgent.SelectedValue = Settings.LoginInfo.AgentId.ToString();

        }
        catch { throw; }
    }

    private void BindSubAgent()
    {
        try
        {
            int agentId = Convert.ToInt32(ddlParentAgent.SelectedValue);
            int agentType = AgentMaster.GetAgentType(agentId);
            if (agentType == (int)AgentType.BaseAgent)
            {
                ddlSubAgent.Enabled = false;
                lblSubAgent.Enabled = false;
            }
            else if (agentType == (int)AgentType.Agent)
            {
                lblSubAgent.Enabled = false;
                ddlSubAgent.Enabled = false;
            }
            else
            {
                if (agentId > 0)
                {
                    lblSubAgent.Enabled = true;
                    ddlSubAgent.Enabled = true;
                }
                else
                {
                    lblSubAgent.Enabled = false;
                    ddlSubAgent.Enabled = false;
                }
            }
            AgentMaster agent = new AgentMaster(agentId);
            DataTable dt = null;
            if (agent.AgentType == (int)AgentType.BaseAgent)
            {
                dt = AgentMaster.GetList(1, "B2B-ALL", agentId, ListStatus.Short, RecordStatus.Activated);
            }
            else if (agent.AgentType == (int)AgentType.Agent)
            {
                dt = AgentMaster.GetList(1, "B2B-ALL", agentId, ListStatus.Short, RecordStatus.Activated);
            }
            else if (agent.AgentType == (int)AgentType.B2B)
            {
                dt = AgentMaster.GetList(1, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);
            }

            ddlSubAgent.DataSource = dt;
            ddlSubAgent.DataTextField = "agent_name";
            ddlSubAgent.DataValueField = "agent_id";
            ddlSubAgent.DataBind();
            ddlSubAgent.Items.Insert(0, new ListItem("--Select SubAgent--", "-1"));
        }
        catch { }
    }

    public void BindB2CMenus(int agentId, long userId)
    {
        string products = AgentMaster.AgentProducts(agentId);
        if (chkListB2CMenu.Items.Count == 0)
        {
            if (products.Contains("1"))
            {
                ListItem item = new ListItem("Flight", WLPreferenceKeys.AllowB2CFlights);
                item.Attributes.Add("onchange", "ShowHideSelection();");
                chkListB2CMenu.Items.Add(item);
            }

            if (products.Contains("2"))
            {
                ListItem item = new ListItem("Hotels", WLPreferenceKeys.AllowB2CHotels);
                item.Attributes.Add("onchange", "ShowHideSelection();");
                chkListB2CMenu.Items.Add(item);
            }

            if (products.Contains("3"))
            {
                ListItem item = new ListItem("Holidays", WLPreferenceKeys.AllowB2CHolidays);
                item.Attributes.Add("onchange", "ShowHideSelection();");
                chkListB2CMenu.Items.Add(item);
            }

            if (products.Contains("12"))
            {
                ListItem item = new ListItem("Visa", WLPreferenceKeys.AllowB2CVisa);
                item.Attributes.Add("onchange", "ShowHideSelection();");
                chkListB2CMenu.Items.Add(item);
            }

            if (products.Contains("5"))
            {
                ListItem item = new ListItem("Insurance", WLPreferenceKeys.AllowB2CInsurance);
                item.Attributes.Add("onchange", "ShowHideSelection();");
                chkListB2CMenu.Items.Add(item);
            }

            if (products.Contains("6"))
            {
                ListItem item = new ListItem("SightSeeing", WLPreferenceKeys.AllowB2CSightSeeing);
                item.Attributes.Add("onchange", "ShowHideSelection();");
                chkListB2CMenu.Items.Add(item);
            }

            if (products.Contains("9"))
            {
                ListItem item = new ListItem("Transfers", WLPreferenceKeys.AllowB2CTransfers);
                item.Attributes.Add("onchange", "ShowHideSelection();");
                chkListB2CMenu.Items.Add(item);
            }
            
            ListItem Firstitem = new ListItem("Select All", "-1");
            Firstitem.Attributes.Add("onchange", "SelectAllMenus();");

            chkListB2CMenu.Items.Insert(0, Firstitem);
        }

        Dictionary<string, string> preferenceList = UserPreference.GetPreferenceList((int)userId, ItemType.Flight);
        
        if (preferenceList != null && preferenceList.Count > 0)
        {
            if (preferenceList[WLPreferenceKeys.AllowB2CFlights] != null && preferenceList[WLPreferenceKeys.AllowB2CFlights] == "True" && chkListB2CMenu.Items.FindByText("Flight") != null)
            {
                chkListB2CMenu.Items.FindByText("Flight").Selected = true;
            }
            if (preferenceList[WLPreferenceKeys.AllowB2CHotels] != null && preferenceList[WLPreferenceKeys.AllowB2CHotels] == "True" && chkListB2CMenu.Items.FindByText("Hotels") != null)
            {
                chkListB2CMenu.Items.FindByText("Hotels").Selected = true;
            }
            if (preferenceList[WLPreferenceKeys.AllowB2CHolidays] != null && preferenceList[WLPreferenceKeys.AllowB2CHolidays] == "True" && chkListB2CMenu.Items.FindByText("Holidays") != null)
            {
                chkListB2CMenu.Items.FindByText("Holidays").Selected = true;
            }
            if (preferenceList[WLPreferenceKeys.AllowB2CVisa] != null && preferenceList[WLPreferenceKeys.AllowB2CVisa] == "True" && chkListB2CMenu.Items.FindByText("Visa") != null)
            {
                chkListB2CMenu.Items.FindByText("Visa").Selected = true;
            }
            if (preferenceList[WLPreferenceKeys.AllowB2CInsurance] != null && preferenceList[WLPreferenceKeys.AllowB2CInsurance] == "True" && chkListB2CMenu.Items.FindByText("Insurance") != null)
            {
                chkListB2CMenu.Items.FindByText("Insurance").Selected = true;
            }
            if (preferenceList[WLPreferenceKeys.AllowB2CSightSeeing] != null && preferenceList[WLPreferenceKeys.AllowB2CSightSeeing] == "True" && chkListB2CMenu.Items.FindByText("SightSeeing") != null)
            {
                chkListB2CMenu.Items.FindByText("SightSeeing").Selected = true;
            }
            if (preferenceList[WLPreferenceKeys.AllowB2CTransfers] != null && preferenceList[WLPreferenceKeys.AllowB2CTransfers] == "True" && chkListB2CMenu.Items.FindByText("Transfers") != null)
            {
                chkListB2CMenu.Items.FindByText("Transfers").Selected = true;
            }
        }
    }

    private UserMaster CurrentObject
    {
        get
        {
            return (UserMaster)Session[USER_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(USER_SESSION);
            }
            else
            {
                Session[USER_SESSION] = value;
            }

        }
    }



    private PageMode Mode
    {
        get { return (PageMode)ViewState["_UserPageMode"]; }
        set { ViewState["_UserPageMode"] = value; }
    }

    private DataTable UserRoleDetails
    {
        get
        {
            return (DataTable)Session[USER_ROLE_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["Role_id"] };
            Session[USER_ROLE_SESSION] = value;
        }
    }

    private void Clear()
    {
        try
        {
            UserMaster tempUser = new UserMaster(-1, Settings.LoginInfo.AgentType.ToString());
            UserRoleDetails = tempUser.UseRole;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtEmailID.Text = string.Empty;
            txtLoginName.Text = string.Empty;
            txtLoginSuffix.Text = string.Empty;
            txtLoginName.Enabled = true;
            txtPassword.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
            txtPassword.Enabled = true;
            txtConfirmPassword.Enabled = true;
            ddlLocation.SelectedIndex = -1;
            txtAddress.Text = string.Empty;
            CurrentObject = null;
            hdfMode.Value = "0";
            btnSave.Text = "Save";
            BindGrid();
            ViewState["Pwd"] = string.Empty;
            ViewState["ConPwd"] = string.Empty;
            txtWebAddress.Text = string.Empty;
            txtWebTitle.Text = string.Empty;
            chkVoucherLogo.Checked = false;
            chkInvoiceLogo.Checked = false;
            chkListB2CMenu.Items.Clear();
            BindB2CMenus(Settings.LoginInfo.AgentId, Settings.LoginInfo.UserID);
            if (Settings.LoginInfo.MemberType == MemberType.SUPER || Settings.LoginInfo.MemberType == MemberType.ADMIN)
            {
                ddlParentAgent.Enabled = true;
            }
            else ddlParentAgent.Enabled = false;

            if (Settings.LoginInfo.AgentId == 0)
            {
                ddlParentAgent.SelectedValue = Settings.LoginInfo.AgentId == 0 ? Utility.ToString(-1) : Utility.ToString(Settings.LoginInfo.AgentId);
            }

            Utility.StartupScript(this.Page, "setLoginSuffix('" + (ddlSubAgent.SelectedIndex > 0 ? ddlSubAgent.ClientID : ddlParentAgent.ClientID) + "');", "setLoginSuffix");

        }
        catch
        {
            throw;
        }
    }
    protected void ddlParentAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlParentAgent.SelectedIndex > 0)
            {
                ddlLocation.Enabled = true;
                long userID = UserMaster.GetPrimaryUserId(Utility.ToLong(ddlParentAgent.SelectedItem.Value));
                if (userID > 0)
                {
                    Edit(userID);
                    BindB2CMenus(Convert.ToInt32(ddlParentAgent.SelectedValue), userID);
                }
                else
                {
                    BindLocation(Utility.ToInteger(ddlParentAgent.SelectedItem.Value));
                    LoadAgentDetails(Utility.ToLong(ddlParentAgent.SelectedItem.Value));
                    BindSubAgent();
                    Clear();
                }                
            }
            else
            {
                ddlLocation.Enabled = false;
                BindSubAgent();
                txtCountry.Text = string.Empty;
                txtCurrency.Text = string.Empty;
                txtAgentType.Text = string.Empty;
                txtPhoneNo.Text = string.Empty;
                Clear();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "");
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void ddlSubAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSubAgent.SelectedIndex > 0)
        {
            long userID = UserMaster.GetPrimaryUserId(Utility.ToLong(ddlSubAgent.SelectedItem.Value));
            if (userID > 0)
            {
                Edit(userID);
            }
            else
            {
                BindLocation(Convert.ToInt32(ddlSubAgent.SelectedValue));
                LoadAgentDetails(Utility.ToLong(ddlSubAgent.SelectedItem.Value));
                Clear();
            }
        }
        else
        {
            long userID = UserMaster.GetPrimaryUserId(Utility.ToLong(ddlParentAgent.SelectedItem.Value));
            if (userID > 0)
            {
                Edit(userID);
            }
            else
            {
                BindLocation(Convert.ToInt32(ddlParentAgent.SelectedValue));
                LoadAgentDetails(Utility.ToLong(ddlParentAgent.SelectedItem.Value));
                Clear();
            }
        }
    }

    protected void HTchkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvHdrRow = (GridViewRow)((CheckBox)sender).NamingContainer;
            CheckBox chkSelectAll = (CheckBox)gvHdrRow.FindControl("HTchkSelectAll");


            foreach (GridViewRow gvRow in gvUserRoleDetails.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                chkSelect.Checked = chkSelectAll.Checked;
                long funcId = Utility.ToLong(gvUserRoleDetails.DataKeys[gvRow.RowIndex].Value);
                SetRowStatus(funcId, chkSelect.Checked);

            }
        }
        catch(Exception ex)
        { Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), ""); }
    }

    protected void ITchkSelect_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvHdrRow = (GridViewRow)((CheckBox)sender).NamingContainer;
            CheckBox chkSelectOne = (CheckBox)gvHdrRow.FindControl("ITchkSelect");
            long functionId = Utility.ToLong(gvUserRoleDetails.DataKeys[gvHdrRow.RowIndex].Value);
            SetRowStatus(functionId, chkSelectOne.Checked);
        }
        catch (Exception ex) { Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), ""); }

    }
    private void SetRowStatus(long functionId, Boolean check)
    {
        try
        {
            DataRow dr = UserRoleDetails.Rows.Find(functionId);
            if (dr != null)
            {
                if (check)
                {
                    UserRoleDetails.Rows.Find(functionId)["urd_status"] = (char)RecordStatus.Activated;
                }
                else
                {
                    UserRoleDetails.Rows.Find(functionId)["urd_status"] = (char)RecordStatus.Deactivated;
                }
            }

            if (Utility.ToLong(UserRoleDetails.Rows.Find(functionId)["URD_ROLE_ID"]) > 0)
            {
                dr.AcceptChanges();
                dr.SetModified();
            }
            else
            {
                dr.AcceptChanges();
                dr.SetAdded();
            }


        }
        catch { throw; }

    }
    protected void Filter_Click(object sender, EventArgs e)
    {
        try
        {
            string[,] textboxesNColumns = { { "HTtxtRoleName", "Role_name" } };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvUserRoleDetails, UserRoleDetails.Copy(), textboxesNColumns);

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "");
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void gvUserRoleDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvUserRoleDetails.PageIndex = e.NewPageIndex;
            gvUserRoleDetails.EditIndex = -1;
            BindGrid();

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "");
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    private void BindGrid()
    {
        try
        {
            CommonGrid grid = new CommonGrid();
            grid.BindGrid(gvUserRoleDetails, UserRoleDetails);
        }
        catch
        {
            throw;
        }
    }

    private void Save()
    {
        try
        {
            UserMaster user;
            if (CurrentObject == null)
            {
                user = new UserMaster();
            }
            else
            {
                user = CurrentObject;
            }
            user.FirstName = txtFirstName.Text.Trim();
            user.LastName = txtLastName.Text.Trim();
            user.Email = txtEmailID.Text;
            user.LoginName = txtLoginName.Text.Trim() + "" + txtLoginSuffix.Text.Trim();
            user.Password = txtPassword.Text;


            user.MemeberType = MemberType.B2C;
            user.LocationID = Utility.ToLong(ddlLocation.SelectedItem.Value);
            user.AgentId = (ddlSubAgent.SelectedIndex > 0 ? Utility.ToInteger(ddlSubAgent.SelectedValue) : Utility.ToInteger(ddlParentAgent.SelectedValue));
            user.Address = txtAddress.Text;
            user.Status = ACTIVE;
            user.CreatedBy = Settings.LoginInfo.UserID;
            user.UseRole = UserRoleDetails;
            user.TransType = "B2C";
            user.PrimaryAgent = true;
            user.Save();

            Dictionary<string, string> Upreference = new Dictionary<string, string>();
            Upreference.Add("SITENAME", txtWebAddress.Text.Trim());
            Upreference.Add("SITETITLE", txtWebTitle.Text.Trim());
            Upreference.Add("VOUCHERLOGO", chkVoucherLogo.Checked.ToString());
            Upreference.Add("INVOICELOGO", chkInvoiceLogo.Checked.ToString());
            //Save B2C Menu preferences
            foreach (ListItem item in chkListB2CMenu.Items)
            {
                switch (item.Text)
                {
                    case "Flights":
                        Upreference.Add(WLPreferenceKeys.AllowB2CFlights, (item.Selected ? "True" : "False"));
                        break;
                    case "Hotels":
                        Upreference.Add(WLPreferenceKeys.AllowB2CHotels, (item.Selected ? "True" : "False"));
                        break;
                    case "Holidays":
                        Upreference.Add(WLPreferenceKeys.AllowB2CHolidays, (item.Selected ? "True" : "False"));
                        break;
                    case "Visa":
                        Upreference.Add(WLPreferenceKeys.AllowB2CVisa, (item.Selected ? "True" : "False"));
                        break;
                    case "Insurance":
                        Upreference.Add(WLPreferenceKeys.AllowB2CInsurance, (item.Selected ? "True" : "False"));
                        break;
                    case "SightSeeing":
                        Upreference.Add(WLPreferenceKeys.AllowB2CSightSeeing, (item.Selected ? "True" : "False"));
                        break;
                    case "Transfers":
                        Upreference.Add(WLPreferenceKeys.AllowB2CTransfers, (item.Selected ? "True" : "False"));
                        break;
                    case "Activities":
                        Upreference.Add(WLPreferenceKeys.AllowB2CActivities, (item.Selected ? "True" : "False"));
                        break;
                }
            }
            

            CT.Core.UserPreference preference=new CT.Core.UserPreference();
            foreach (KeyValuePair<string, string> prefe in Upreference)
            {
                if (prefe.Key == WLPreferenceKeys.AllowB2CFlights || prefe.Key == WLPreferenceKeys.AllowB2CHolidays || prefe.Key == WLPreferenceKeys.AllowB2CHotels || prefe.Key == WLPreferenceKeys.AllowB2CInsurance || prefe.Key == WLPreferenceKeys.AllowB2CSightSeeing || prefe.Key == WLPreferenceKeys.AllowB2CTransfers || prefe.Key == WLPreferenceKeys.AllowB2CVisa || prefe.Key == WLPreferenceKeys.AllowB2CActivities)
                {
                    preference.Save((int)user.ID, prefe.Key.ToString(), prefe.Value.ToString(), "SETTINGS", ItemType.Flight);// item type 0 value is for common settngs
                }
                else
                {
                    preference.Save((int)user.ID, prefe.Key.ToString(), prefe.Value.ToString(), "SETTINGS", 0);// item type 0 value is for common settngs
                }
            }
            CT.Core.UserPreference preferance = new CT.Core.UserPreference();
            preferance.Key = "";
            lblSuccessMsg.Text = Formatter.ToMessage("User ", txtFirstName.Text.Trim(), Mode != PageMode.Add ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);
            //Clear();
        }
        catch
        {
            throw;
        }

    }
    private void LoadAgentDetails(long agentId)
    {
        try
        {
            AgentMaster agent = new AgentMaster(agentId);
            txtAgentType.Text = ((AgentType)agent.AgentType).ToString();
            txtCurrency.Text = agent.AgentCurrency;
            CountryMaster country = new CountryMaster(agent.Country);
            txtCountry.Text = country.Name.ToString();
            txtPhoneNo.Text = agent.Phone1;
          
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
            lnkAir.Style.Add("display", "block");
            lnkInsurance.Style.Add("display", "block");
            lnkHotel.Style.Add("display", "block");
            lnkVisa.Style.Add("display", "block");
            lnkActivity.Style.Add("display", "block");
            lnkFixDep.Style.Add("display", "block");
            lnkSightseeing.Style.Add("display", "block");
            lnkTransfer.Style.Add("display", "block");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "");
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
            lblSuccessMsg.Text = String.Empty;
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
     
    private void Edit(long id)
    {
        try
        {
            txtLoginSuffix.Text = string.Empty;
            UserMaster user = new UserMaster(id, Settings.LoginInfo.AgentType.ToString());
            CurrentObject = user;
            txtFirstName.Text = user.FirstName;
            txtLastName.Text = user.LastName;
            txtEmailID.Text = user.Email;
            txtLoginName.Text = user.LoginName;
            txtLoginName.Enabled = false;
            txtPassword.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
            txtPassword.Enabled = false;
            txtConfirmPassword.Enabled = false;
            int agentType = AgentMaster.GetAgentType(user.AgentId);
            if (agentType==(int)AgentType.B2B2B)
            {
                try
                {
                    ddlParentAgent.SelectedValue = user.ParentAgentId.ToString();
                    BindSubAgent();
                    ddlSubAgent.SelectedValue = user.AgentId.ToString();
                }
                catch { }
            }
            else
            {
                ddlParentAgent.SelectedValue = Utility.ToString(user.AgentId);
                BindSubAgent();
            }
            BindLocation(Utility.ToInteger(user.AgentId));
            LoadAgentDetails(Utility.ToLong(user.AgentId));
            try
            {
                ddlLocation.SelectedValue = Utility.ToString(user.LocationID);
            }
            catch { }

            txtAddress.Text = user.Address;
            UserRoleDetails = user.UseRole;
            hdnAgentId.Value=Convert.ToString(user.AgentId);
            hdnUserId.Value = Convert.ToString(user.ID);
            Dictionary<string, string> Upreference = CT.Core.UserPreference.GetPreferenceList((int)user.ID, ItemType.ALL); //ItemType.ALL
            foreach (KeyValuePair<string, string> prefe in Upreference)
            {
                if (prefe.Key == "SITENAME")
                {
                    txtWebAddress.Text = prefe.Value;
                }
                else if (prefe.Key == "SITETITLE")
                {
                    txtWebTitle.Text = prefe.Value;
                }
                else if (prefe.Key == "VOUCHERLOGO")
                {
                    chkVoucherLogo.Checked = Convert .ToBoolean(prefe.Value);
                }
                else if (prefe.Key == "INVOICELOGO")
                {
                    chkInvoiceLogo.Checked = Convert.ToBoolean(prefe.Value);
                }
            }
            btnSave.Text = "Update";
            btnCancel.Text = "Cancel";
            BindGrid();
            hdfMode.Value = "1";
            lnkAir.Style.Add("display", "block");
            lnkInsurance.Style.Add("display", "block");
            lnkHotel.Style.Add("display", "block");
            lnkVisa.Style.Add("display", "block");
            lnkActivity.Style.Add("display", "block");
            lnkFixDep.Style.Add("display", "block");
            lnkSightseeing.Style.Add("display", "block");
            lnkTransfer.Style.Add("display", "block");
        }
        catch
        {
            throw;
        }
    }
}
