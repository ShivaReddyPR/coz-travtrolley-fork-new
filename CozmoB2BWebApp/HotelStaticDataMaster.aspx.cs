﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;

namespace CozmoB2BWebApp
{
    public partial class HotelStaticDataMaster :CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.Master.PageRole = true;
                if (Settings.LoginInfo != null)
                {

                    lblSuccessMsg.Text = string.Empty;
                    if (!IsPostBack)
                    {

                        BindCountries();
                        if (Request.QueryString.Count > 0 && Request.QueryString["ref"] != null)
                        {
                            Edit(Convert.ToInt32(Request.QueryString["ref"]));

                        }

                    }
                }
                else
                {
                    Response.Redirect("AbandonSession.aspx");
                }
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);

            }


        }
        private void BindCountries()
        {
            try
            {
                ddlCountry.DataSource = GimmonixStaticData.GetCountryList();
                ddlCountry.DataTextField = "countryName";
                ddlCountry.DataValueField = "countryCode";
                ddlCountry.DataBind();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void Clear()
        {
            txtHotelId.Text = string.Empty;
            txtHotelName.Text = string.Empty;
            ddlCountry.SelectedIndex = -1;
            txtCityName.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtZipcode.Text = string.Empty;
            ddlRating.SelectedIndex = -1;
            txtLat.Text = string.Empty;
            txtLng.Text = string.Empty;
            txtRoom.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtWebSite.Text = string.Empty;
            txtChainCode.Text = string.Empty;
            txtFacility.Text = string.Empty;
            txtImageUrl.Text = string.Empty;
            txtDescription.Text = string.Empty;
			txtRezliveid.Text = string.Empty;
			txtIlluCityCode.Text = string.Empty;
			txtIlluHotelCode.Text = string.Empty;
			txtHisID.Text = string.Empty;
            btnSave.Text = "Save";
            btnClear.Text = "Clear";
        }
        private void Save()
        {
            try
            {
                GimmonixStaticData staticData = new GimmonixStaticData();
                if (Utility.ToInteger(hid.Value) > 0)
                {
                    staticData.ID = Utility.ToInteger(hid.Value);
                }
                else
                {
                    staticData.ID = -1;
                }
                staticData.HotleId = txtHotelId.Text;
                staticData.HotelName = txtHotelName.Text;
                staticData.CountryName = ddlCountry.SelectedItem.Value;
                staticData.CityName = txtCityName.Text;
                staticData.Address = txtAddress.Text;
                staticData.ZipCode = txtZipcode.Text;
                staticData.Rating =Utility.ToInteger( ddlRating.SelectedItem.Value);
                staticData.LAT = txtLat.Text;
                staticData.LNG = txtLng.Text;
                staticData.RoomCount =Utility.ToInteger( txtRoom.Text);
                staticData.Phone = txtPhone.Text;
                staticData.Email = txtEmail.Text;
                staticData.WebSite = txtWebSite.Text;
                staticData.PCategory = ddlCategory.SelectedItem.Value;
                staticData.ChainCode = txtChainCode.Text;
                staticData.HFacility = txtFacility.Text;
                staticData.ImageUrl = txtImageUrl.Text;
                staticData.Description = txtDescription.Text;
                staticData.ModifiedBy =Utility.ToInteger (Settings.LoginInfo.UserID);
				staticData.RezliveID =Utility.ToInteger(txtRezliveid.Text);
				staticData.HisID = Utility.ToInteger(txtHisID.Text);
				staticData.IllusionsHotelCode = txtIlluHotelCode.Text;
				staticData.IllusionsCityCode = txtIlluCityCode.Text;
				staticData.Save();
                Clear();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage("Details Successfully", "", hid.Value == "0" ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
        private void Edit(int id)
        {
            try
            {
                GimmonixStaticData staticData = new GimmonixStaticData(id);
                Clear();
                hid.Value =Utility.ToString( staticData.ID);
                txtHotelId.Text = staticData.HotleId;
                txtHotelName.Text = staticData.HotelName;
                ddlCountry.SelectedValue = staticData.CountryName;
                txtCityName.Text = staticData.CityName;
                txtAddress.Text = staticData.Address;
                txtZipcode.Text = staticData.ZipCode;
                ddlRating.SelectedValue =Utility.ToString( staticData.Rating);
                txtLat.Text = staticData.LAT;
                txtLng.Text = staticData.LNG;
                txtRoom.Text =Utility.ToString( staticData.RoomCount);
                txtPhone.Text = staticData.Phone;
                txtEmail.Text = staticData.Email;
                txtWebSite.Text = staticData.WebSite;
                txtChainCode.Text = staticData.ChainCode;
                txtFacility.Text = staticData.HFacility;
                txtImageUrl.Text = staticData.ImageUrl;
                txtDescription.Text =HttpUtility.HtmlDecode(staticData.Description);
				txtHisID.Text =Utility.ToString(staticData.HisID);
				txtRezliveid.Text = Utility.ToString(staticData.RezliveID);
				txtIlluHotelCode.Text = staticData.IllusionsHotelCode;
				txtIlluCityCode.Text = staticData.IllusionsCityCode;
				btnSave.Text = "Update";
                btnClear.Text = "Cancel";
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);

            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);

            }


        }
    }
}
