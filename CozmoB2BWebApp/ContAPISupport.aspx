﻿<%@ Page Language="C#" MasterPageFile="~/Transaction.master" AutoEventWireup="true" Inherits="ContAPISupportUI" Title="" Codebehind="ContAPISupport.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
 
 
 
  
 <style> 
 
 .body_container { line-height:24px; font-size:14px; }

 </style>
 
 
 <div class="body_container">
 
 <div class="col-md-12"> 
 <h4 class=" paddingtop_10 paddingbot_10"> API</h4>
 




Our feature-rich API (Flights and Visa) enables you to seamlessly integrate our rich content with your system.  We have competitive rates inventory on our Flight API, including major LCCs and GDS. Our Visa API has currently Qatar and UAE visa application process and its fully automated process.  <br /> <br /> 

API - Flights and Visa <br /> <br /> 


<strong> What is an API?</strong> <br /> <br /> 

An Application Programming Interface (API) is a simple communication method used in computer programming language to interchange information between one entity to other using XML technology. We will be providing all required information as an API for you to integrate on your system to access our content. We have currently enabled Flight API and Visa application form API for our customers. [genericapi image - optional] <br /> <br /> 


You can access major airlines and low cost carriers content through our Flight API once its integrated to your system. The API supports major activities like search for best economical fares, search and book the itinerary, view your existing bookings, cancellation / amendments, reissue tickets, etc.  [Flight api image - optional] <br /> <br /> 


Our unique feature of Visa API enables you to configure the online visa application form on real-time basis. This will help reducing the processing time and you will be able to serve your customers without keeping the customers waiting for more days. We have enabled online visa application for Qatar and UAE currently. <br /> <br /> 

 
 </div>
 
 <div class="clearfix"> </div>
 </div>

 
 
 
 
 
 



 
</asp:Content>

