﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true"
    Inherits="AddVisaCountry" Title="Cozmo Travels" ValidateRequest="false" Codebehind="AddVisaCountry.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
  
  
   <div> 
    <div class="col-md-6"> <h4> 
    
 <% if (UpdateCountry > 0)
                           {%>
                               <span>Update Visa Country </span>

                          <% }
                           else
                           {  %>
                               <span>Add Visa Country</span>
                          <% }
                               %>
    
    </h4> </div>
    
    <div class="col-md-6">
    
    <asp:HyperLink ID="HyperLink1" class="fcol_blue pull-right" runat="server" NavigateUrl="~/VisaCountryList.aspx">Go to Visa Country List</asp:HyperLink>
    
    
    
   

 </div>     
    
    <div class="clearfix"> </div> 
    </div>
    <div>
      <asp:Label class="font-red" runat="server" ID="lblErrorMessage" Text=""></asp:Label>
      
      </div>
    
        <div class="paramcon"> 

  
  
  
          <div> 
    <div class="col-md-2"><asp:Label runat="server" ID="lblregion" Text=" Select Region">
                                </asp:Label> </div>
    
    <div class="col-md-2"> <p><span><asp:DropDownList ID="ddlRegion" runat="server" CssClass="form-control">
                                    <asp:ListItem>Select</asp:ListItem>
                                    </asp:DropDownList></span><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please select region"
                                    ControlToValidate="ddlRegion" InitialValue="Select"></asp:RequiredFieldValidator></p></div>   
 
    <div class="col-md-2"><asp:Label runat="server" ID="lblCountryName" Text="Country Name">
                                </asp:Label> </div>
    
                                
    <div class="col-md-2"> <p><span> <asp:TextBox CssClass="form-control" ID="txtCountryName" runat="server"></asp:TextBox>&nbsp;</span>
    
                                
                                    
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please fill country name "
                                    ControlToValidate="txtCountryName"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rgCountryName" runat="server" ErrorMessage="Please enter only character"
                                    ValidationExpression="[a-zA-Z\s]+" ControlToValidate="txtCountryName" Display="Dynamic"></asp:RegularExpressionValidator>
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Please enter maximum 50 character"
                                      ValidationExpression="[\S\s]{0,50}" ControlToValidate="txtCountryName" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </p>
    
     </div>    


              <div class="col-md-2"><asp:Label runat="server" ID="lblARCountryName" Text="Arabic Country Name">
                                </asp:Label> </div>
    
    <div class="col-md-2"> 
        <span>
            <asp:TextBox CssClass="form-control" ID="txtARCountryName" onkeypress="return CheckArabicCharactersOnly(event);" runat="server"></asp:TextBox>&nbsp;</span>
                           <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ErrorMessage="Please fill Arabic country name "
                                    ControlToValidate="txtARCountryName"></asp:RequiredFieldValidator>                                   
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Please enter maximum 50 character"
                                      ValidationExpression="[\S\s]{0,50}" Display="Dynamic" ControlToValidate="txtARCountryName"></asp:RegularExpressionValidator>
                                  
    
     </div>    

    <div class="col-md-2"> <asp:Label runat="server" ID="lblCountryCode" Text="Country Code">
                                </asp:Label> </div>
    

 <div class="col-md-2"> <p>
                                
                                <span>
                                    <asp:TextBox CssClass="form-control" ID="txtCountryCode" runat="server" MaxLength="2"></asp:TextBox>&nbsp;</span>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Please fill two character"
                                    ValidationExpression="^[a-z,A-Z]{2}$" ControlToValidate="txtCountryCode" Display="Dynamic"></asp:RegularExpressionValidator>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ErrorMessage="Please enter country Code "
                                    ControlToValidate="txtCountryCode"></asp:RequiredFieldValidator>
                            </p></div>
  
  
  <div class="clearfix"> </div> 
 
    </div>

    
    
    
    <div class="col-md-12"> <asp:Button ID="btnSave" runat="server" Text=" Save " CssClass="but but_b pull-right" 
                                        OnClick="btnSave_Click" />  </div>
    
    
    
  
    
         </div>          
            
                 <script  type="text/javascript">
// Allow Arabic Characters only
function CheckArabicCharactersOnly(e) {
var unicode = e.charCode ? e.charCode : e.keyCode
if (unicode != 8) { //if the key isn't the backspace key (which we should allow)
if (unicode == 32)
return true;
else {
if ((unicode < 0x0600 || unicode > 0x06FF)) //if not a number or arabic
return false; //disable key press
}
}
}
</script>


</asp:Content>
