﻿<%@ WebHandler Language="C#" Class="CorpFileUploadHandler" %>

using System;
using System.Web;
using System.Net;
using System.Configuration;
using System.IO;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
public class CorpFileUploadHandler : IHttpHandler, System.Web.SessionState.IReadOnlySessionState
{


    public void ProcessRequest(HttpContext context)
    {
        try
        {
            var fileName = context.Request["qqfilename"];
            context.Response.ContentType = "text/plain";
            if (context.Session["CorpProfileId"] != null)
            {
                int profileId = Convert.ToInt32(context.Session["CorpProfileId"]);
                if (profileId > 0 && context.Request.Files.Count > 0)
                {
                    var tempPath = ConfigurationManager.AppSettings["ProfileImage"];
                    tempPath = tempPath + "/" + profileId;
                    var dirFullPath = context.Server.MapPath(tempPath);
                    if (!Directory.Exists(dirFullPath))
                    {
                        Directory.CreateDirectory(dirFullPath);
                    }
                    foreach (string s in context.Request.Files)
                    {
                        var file = context.Request.Files[s];
                        var fileExtension = file.ContentType;
                        CorpProfileDocuments profileDoc = new CorpProfileDocuments();
                        profileDoc.ProfileId = profileId;
                        profileDoc.DocTypeName = fileName.Split('.')[0];
                        profileDoc.DocFileName = fileName;
                        profileDoc.DocFilePath = dirFullPath;
                        profileDoc.CreatedBy = (int)Settings.LoginInfo.UserID;
                        profileDoc.Save();
                        if (profileDoc.RetDocId > 0)
                        {
                            var pathToSave_100 = HttpContext.Current.Server.MapPath(tempPath) + "\\" + fileName;
                            file.SaveAs(pathToSave_100);
                        }
                        else
                        {
                            throw new Exception("File Not Uploaded");
                        }
                    }
                    //context.Session["CorpProfileId"] = null;
                    context.Response.StatusCode = (int)HttpStatusCode.OK;
                    context.Response.Write("{\"success\":true}");
                }
            }
            else
            {
                context.Response.Redirect("AbandonSession.aspx", false);
            }
        }
        catch (Exception ex)
        {
            context.Response.Write("Error: " + ex.Message);
        }
    }
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}