﻿using CT.BookingEngine;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

namespace CozmoB2BWebApp
{
    public partial class DynamicMarkupMaster : CT.Core.ParentPage
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    IntilizeControls();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        #region Methods
        #region BindProductType
        private void BindProdcutType()
        {
            try
            {
                Array values = Enum.GetValues(typeof(ProductType));

                foreach (ProductType pType in values)
                {
                    ddlProduct.Items.Add(new ListItem(pType.ToString(), ((int)pType).ToString()));
                }


            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
        #region Bind FromSource
        private void BindSource()
        {
            try
            {
                ddlFromSource.DataSource = DynamicMarkup.GetAllActiveSourcesForFlight(ProductType.Flight);
                ddlFromSource.DataValueField = "Name";
                ddlFromSource.DataTextField = "Name";
                ddlFromSource.DataBind();
                ddlFromSource.Items.Insert(0, new ListItem("Select FromSource", "-1"));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region BindCompareSources
        private void BindCompareSources()
        {
            try
            {
                if (ddlFromSource.SelectedIndex > 0)
                {
                    DataTable dtSources = DynamicMarkup.GetAllActiveSourcesForFlight(ProductType.Flight);
                    dtSources.DefaultView.RowFilter = "Name<>'" + ddlFromSource.SelectedValue + "'";
                    ddlCompareSource.DataSource = dtSources.DefaultView.ToTable();
                    ddlCompareSource.DataValueField = "Name";
                    ddlCompareSource.DataTextField = "Name";
                    ddlCompareSource.DataBind();
                }

            }
            catch
            {
                throw;
            }
        }
        #endregion
        #region BindAgent
        private void BindAgent()
        {
            try
            {
                DataTable dtAgents = DynamicMarkup.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
                ddlAgent.DataSource = dtAgents;
                ddlAgent.DataTextField = "Agent_Name";
                ddlAgent.DataValueField = "agent_id";
                ddlAgent.DataBind();
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region BindGridView
        private void BindSearch()
        {
            try
            {
                DataTable dt = DynamicMarkup.GetDynamicMarkupMasterDetails();
                if (dt.Rows.Count > 0)
                {
                    gvSearch.DataSource = dt;
                    gvSearch.DataBind();
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion
        #region Clear Method
        private void Clear()
        {
   
                ddlAgent.SelectedIndex = -1;
                ddlCompareSource.SelectedIndex = -1;
                ddlFromSource.SelectedIndex = -1;
                ddlProduct.SelectedIndex = -1;
                //TxtAirlineCode.Text = string.Empty;
                //TxtBooking.Text = string.Empty;
                //TxtRoutes.Text = string.Empty;
                ChkAirline.Checked = false;
                ChkBooking.Checked = false;
                ChkFlight.Checked = false;
                ChkRoute.Checked = false;
            ddlTransactionType.SelectedIndex = -1;

            }
           
       
        #endregion
        #region Intilize Method
        private void IntilizeControls()
        {
            try
            {
                BindSource();
                BindProdcutType();
                BindAgent();
                //BindCompareSources();
            }
            catch
            {
                throw;
            }
        }
        #endregion
        #region Save
        private void Save()
        {
            try
            {
                DynamicMarkup dynamic = new DynamicMarkup();
                if (Utility.ToInteger(hdfid.Value) > 0)
                {
                    dynamic.DynID = Utility.ToInteger(hdfid.Value);
                }
                else
                {
                    dynamic.DynID = -1;
                }
                dynamic.FromSource = Utility.ToString(ddlFromSource.SelectedItem.Value);
                dynamic.CompareSource = Utility.ToString(ddlCompareSource.SelectedItem.Value);
                dynamic.ProductID = Utility.ToInteger(ddlProduct.SelectedItem.Value);
                dynamic.AgentID = Utility.ToInteger(ddlAgent.SelectedItem.Value);
                dynamic.AirlineCodeCheck = ChkAirline.Checked == true ? true : false;
                dynamic.RouteCheck = ChkRoute.Checked == true ? true : false;
                dynamic.BookingClassCheck = ChkBooking.Checked == true ? true : false;
                dynamic.FlightNo = ChkFlight.Checked == true ? true : false;

                //string code = txtAirline.Text;
                //if (Request["txtAirline0"].Length > 0)
                //{
                //    for (int i = 0; i < 10; i++)
                //    {
                //            code += ',' + Request["txtAirline" + i].ToString();

                //    }

                //    dynamic.AirlineCodes = code;
                //}
                //else
                //{
                //    dynamic.AirlineCodes = txtAirline.Text;
                //}

                //dynamic.AirlineCodes = Airlinecodes;
                //dynamic.Routes = TxtRoutes.Text.ToUpper();
                //dynamic.BookingClasses = TxtBooking.Text.ToUpper();
                dynamic.TransactionType = Utility.ToString(ddlTransactionType.SelectedItem.Value);
                dynamic.Created_by = Settings.LoginInfo.UserID;
                dynamic.Save();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage("Details Successfully", "", hdfid.Value == "0" ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);
                btnSave.Text = "Save";
                Clear();
            }
            catch
            {
                throw;
            }
        }
        #endregion
        #region Edit
        private void Edit(int dynID)
        {
            try
            {
                DynamicMarkup dynamic = new DynamicMarkup(dynID);
                hdfid.Value = Utility.ToString(dynamic.DynID);
                ddlFromSource.SelectedValue = Utility.ToString(dynamic.FromSource);
                BindCompareSources();
                ddlCompareSource.SelectedValue = Utility.ToString(dynamic.CompareSource);
                ddlProduct.SelectedValue = Utility.ToString(dynamic.ProductID);
                ddlAgent.SelectedValue = Utility.ToString(dynamic.AgentID);
                if (dynamic.AirlineCodeCheck == true)
                    ChkAirline.Checked = true;
                else
                    ChkAirline.Checked = false;
                if (dynamic.RouteCheck == true)
                    ChkRoute.Checked = true;
                else
                    ChkRoute.Checked = false;
                if (dynamic.BookingClassCheck == true)
                    ChkBooking.Checked = true;
                else
                    ChkBooking.Checked = false;
                if (dynamic.FlightNo == true)
                    ChkFlight.Checked = true;
                else
                    ChkFlight.Checked = false;
                //txtAirline.Text = dynamic.AirlineCodes;
                //TxtBooking.Text = dynamic.BookingClasses;
                //TxtRoutes.Text = dynamic.Routes;
                ddlTransactionType.SelectedValue = Utility.ToString(dynamic.TransactionType);
                btnSave.Text = "Update";
                btnClear.Text = "Cancel";
                if (dynamic.DynID > 0)
                {
                    lnkDynamicMarkupThreshold.Visible = true;
                }
                else
                {
                    lnkDynamicMarkupThreshold.Visible = false;
                }
            }
            catch { throw; }
        }
        #endregion
        #endregion
        #region Button Clicking events
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();

            }
            catch(Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();

            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Master.ShowSearch("Search");
                BindSearch();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);

            }
        }
        #endregion
        #region  Dropdown select Index
        protected void ddlFromSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompareSources();
            }
            catch
            {
                throw;
            }
        }
        #endregion
        #region Gridview events  
        protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                int dynID = Utility.ToInteger(gvSearch.SelectedValue);
                Edit(dynID);
                this.Master.HideSearch();

            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }

        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSearch.PageIndex = e.NewPageIndex;
                BindSearch();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        #endregion
    }
}
