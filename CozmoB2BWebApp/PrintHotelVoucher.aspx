﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" Inherits="PrintHotelVoucherUI" CodeBehind="PrintHotelVoucher.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="System" %>
<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">

  <link href="css/override.css" rel="stylesheet" />
<head id="Head1" runat="server">
    <title>Hotel Reservation Voucher</title>
   
    <link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />
    <!--according-->

    <script type="text/javascript">
        function printPage() {
            document.getElementById('btnPrint').style.display = "none";
            window.print();
            //alert('2');
            setTimeout('showButtons()', 1000);
            // alert('3');
            return false;
        }
        function showButtons() {
            document.getElementById('btnPrint').style.display = "block";
        }
        function Validate() {
            var isValid = true;
            var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
            document.getElementById('err').style.display = 'none';
            if (document.getElementById('<%#txtEmailId.ClientID %>').value.length <= 0) {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Email address";
                isValid = false;
            }
            else if (reg.test(document.getElementById('<%#txtEmailId.ClientID %>').value)) {
                document.getElementById('err').style.display = 'none';
            }
            else {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Correct Email";
                isValid = false;
            }
            if (isValid == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function ShowPopUp(id) {
            document.getElementById('txtEmailId').value = "";
            document.getElementById('err').style.display = 'none';
            document.getElementById('emailBlock').style.display = "block";
//            var positions = getRelativePositions(document.getElementById(id));
            //            document.getElementById('emailBlock').style.left = (530) + 'px';
            //            document.getElementById('emailBlock').style.top = (400) + 'px';
            return false;
        }
        function HidePopUp() {
            document.getElementById('emailBlock').style.display = "none";
        }
    </script>
    
        
</head>


<style> 

.email__iten {  position:absolute;display:none; left:400px;top:200px;width:300px;}

.visible-xs { display:none }



@media (max-width: 767px)
{
	
.email__iten {  position:absolute;display:none; top:10px; width:250px; position: absolute; 
  left: 0; 
  right: 0; 
  margin-left: auto; 
  margin-right: auto; }
	


.visible-xs { display:block }

.hidden-xs { display:none }
	
}





</style>
<body>
    <form id="form1" runat="server">
                       <%
                           
                           if (HttpContext.Current.Request.Url.Host.ToLower().Contains("ibyta.com"))
                           { dynamicAgentLabel = "Ibyta"; }%>
                           
  
    <div id="emailBlock" class="showmsg email__iten">
         
          <div class="showMsgHeading"> <label style=" color:White; line-height:26px; font-weight:bold; padding-left:10px;">  Enter Your Email Address</label>   </div>
          
          <a style=" position:absolute; right:5px; top:3px;" onclick="return HidePopUp();" href="#" class="closex"> X</a>
           <div class="padding-5"> 
           <div style=" background:#fff">
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       
                           <tr>
                           <td height="40" align="center">
                            <b style="display: none; color:Red" id="err"></b>
                           <asp:TextBox style=" border: solid 1px #ccc; width:90%; padding:2px;"  ID="txtEmailId" runat="server" CausesValidation="True" ></asp:TextBox>
                           </td>
                           </tr>
                           <tr>
                           
                           <td height="40" align="center">
                           <asp:Button CssClass="button-normal" ID="btnEmailVoucher" runat="server" Text="Send Email" OnClientClick="return Validate()" OnClick="btnEmailVoucher_Click" />
                           </td>
                       </tr>
                   </table>
            </div>
           
           
           
             </div>
           
           
               
           </div>



           
    <div id="PrintDiv" runat="server" style="width: 100%">
        <div style="padding: 10px">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; ">
                <tr>
                    <th class="themecol1" style="border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding:5px;   color:#000000;"
                        width="50%" align="left">
                        Reservation Voucher
                    </th>
                    <th class="themecol1" style="border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-right: solid 1px #ccc;padding:5px;  color:#000000;"
                        width="25%" align="right">
                        <%--<input style="width: 100px;" id="btnPrint" onclick="return printPage();" type="button"
                            value="Print Voucher" />--%>
                        <asp:Button ID="btnPrint" runat="server" Text="Print Voucher" OnClientClick="return printPage();" />
                    </th>
                      <th class="themecol1" style="border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-right: solid 1px #ccc;padding:5px;  color:#000000;"
                        width="25%" align="right">
                          <%--<input style="width: 100px;" id="btnEmail" onclick="return ShowPopUp(this.id);" type="button"
                            value="Email Voucher" />--%>
                          <asp:Button ID="btnEmail" runat="server" Text="Email Voucher" OnClientClick="return ShowPopUp(this.id);"/>
                          <%--<input style="width: 100px;" id="btndownload" onclick="return printPage();" type="button" value="Download" />--%>
                    </th>
                </tr>
                <tr>


<td colspan="3" style="padding:5px; border: solid 1px #c6c6c6;">
                      
                      
                      
<table class="hidden-xsDEL" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50%" valign="top">
                                    <asp:Image ID="imgLogo" runat="server" AlternateText="AgentLogo"  />
                                    
                                    

                                </td>
                                <td width="50%">
                                    <div style="line-height: 22px">
                                        <strong>
                                            <%=itinerary.HotelName %>
                                            <%if(itinerary.Rating!=HotelRating.All)
                                              { %>
                                            (<%=itinerary.Rating%>)</strong>
                                            <%} %>
                                            <br />
                                        <strong>Address :</strong>
                                        <%=itinerary.HotelAddress1 %><br />
                                        <%=itinerary.HotelAddress2 %>
                                    </div>
                                </td>
                            </tr>
                        </table>

 <%--<table class="visible-xs" width="100%" border="0" cellspacing="0" cellpadding="0">
              
              
              <tr> 
              <td width="100%"> 
              
                    <div> <asp:Image ID="imgLogo2" runat="server" AlternateText="AgentLogo"  /> </div> removed as duplicated
                    
                    <div> <div style="line-height: 22px">
                                        <strong>
                                            <%=itinerary.HotelName %>
                                            <%if(itinerary.Rating!=HotelRating.All)
                                              { %>
                                            (<%=itinerary.Rating%>)</strong>
                                            <%} %>
                                            <br />
                                        <strong>Address :</strong>
                                        <%=itinerary.HotelAddress1 %><br />      
                                        <%=itinerary.HotelAddress2 %>
                                    </div> </div>
              
              </td>
              
              </tr>
              
              </table>--%>



 </td>



                


                </tr>
                 <%if (itinerary.Source == HotelBookingSource.Agoda && !string.IsNullOrEmpty(itinerary.VatDescription))
                    { %>
                <tr>
                    <td colspan='3'>
                       <strong style="color:red"> <%=itinerary.VatDescription %></strong>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td colspan="3">
                        <br />
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            
                            <%if (agent.AgentParantId > 0 && parentAgent != null)
                                { %>
                                <tr>
                                
                                    <td align="left">
                                    <strong>Booked By:</strong>
                                    <%=parentAgent.Name %>
                                </td>
                                 </tr>
                                <%} %>
                                   
                            <tr>
                                <td align="left">
                                    <strong>Date of Issue:</strong>
                                    <%=itinerary.CreatedOn.ToString("dd MMM yyyy hh:mm tt") %>
                                </td>
                                 <%var parentid =CT.TicketReceipt.BusinessLayer.AgentMaster.GetParentId(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId);
                                                                                         if (parentid ==2125 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId==2125)

                                                                                         {
                                                                                      CT.TicketReceipt.BusinessLayer.LocationMaster location = new CT.TicketReceipt.BusinessLayer.LocationMaster(itinerary.LocationId); %>

                                                                                        <td align="center"><strong>GSTIN: </strong><%=location.GstNumber %></td>
                                                                                    <%} %>
                                                                                        <td aliign="center">
                                                                                             <strong>Status: </strong>
                                                                                            <% if (itinerary.Status == HotelBookingStatus.Pending)
                                                                                                {%>
                                                                                            <b style="color: #FF5722"><%=itinerary.Status%> </b>
                                                                                                <%} %>
                                                                                                    <%else
    { %> <b style="color: #009933">Vouchered </b>
                                                                                                        <%} %>
                                                                                        </td>
                                <td align="right">
                                    <strong>Confirmation No:</strong>
                                    <%if (itinerary.Source != HotelBookingSource.HotelConnect)
                  {%>
                <%=itinerary.ConfirmationNo%>
                (Reference No:
                <%=itinerary.BookingRefNo.Replace("|", "")%>|)
                <%}
                  else
                  { %>
                <%=itinerary.ConfirmationNo%>
                <%} %>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--
   <tr>
    <td class="innertdpad"><strong>Cozmo Ref No:</strong></td>
    <td class="innertdpad"> <%=bookingResponse.BookingId %></td>
  </tr>
  --%> <td colspan="3">
     <%--<% string allchildAges=string.Empty; %>--%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
               
                    <td style="width:50%;padding:5px; border: solid 1px #c6c6c6;">
                        <strong><%=dynamicAgentLabel %> Ref No:</strong>
                            
                    </td>
                    <td style="padding:5px; border: solid 1px #c6c6c6;">
                        HTL-CT-<%=booking.BookingId %>
                    </td>
                </tr>
               
                <tr>
                    <td style="width:50%;padding:5px; border: solid 1px #c6c6c6;">
                        <strong>Address:</strong>
                    </td>
                    <td style="padding:5px; border: solid 1px #c6c6c6;">
                        <%=agent.Address %>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;padding:5px; border: solid 1px #c6c6c6;">
                        <strong>Emergency Contact No: </strong>
                    </td>
                    <td style="padding:5px; border: solid 1px #c6c6c6;">
                        <%=agent.Phone1 %>
                    </td>
                </tr>
                 </table>
                    </td>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <strong>Room no.</strong>
                                </td>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <strong>Room Type</strong>
                                </td>
                                  <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <strong>Pax Name(s)</strong>
                                </td>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                <%if (itinerary.Source != HotelBookingSource.TBOHotel)
                                  { %>
                                    <strong>Meal Plan</strong>
                                    <%}
                                  else
                                  { %>
                                  <strong>Inclusions</strong>
                                  <%} %>
                                </td>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <strong>No. of Guests</strong>
                                </td>
                            </tr>
                            <%for (int i = 0; i < itinerary.NoOfRooms; i++)  %>
                            <%{ %>
                            <tr>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <%=i+1%>
                                </td>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <%=(itinerary.Roomtype[i].RoomName.Contains("|") ? itinerary.Roomtype[i].RoomName.Split('|')[0] : itinerary.Roomtype[i].RoomName)%>
                                </td>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                  
                                  <% string paxname = string.Empty;
                                     for (int k = 0; k < itinerary.Roomtype[i].PassenegerInfo.Count; k++)
                                     {
                                         if (paxname == string.Empty)
                                         {
                                             paxname = itinerary.Roomtype[i].PassenegerInfo[k].Firstname + " " + itinerary.Roomtype[i].PassenegerInfo[k].Lastname;
                                         }
                                         else
                                         {
                                             paxname += "," + itinerary.Roomtype[i].PassenegerInfo[k].Firstname + " " + itinerary.Roomtype[i].PassenegerInfo[k].Lastname;
                                         }
                                     }
                                        
                                         %>
                                         <%=paxname%>
                                </td>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <%= (!string.IsNullOrEmpty(itinerary.Roomtype[i].MealPlanDesc) ? itinerary.Roomtype[i].MealPlanDesc : "Room Only")%>
                                </td>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <%=itinerary.Roomtype[i].AdultCount%>
                                    Adult(s)
                                    <%=itinerary.Roomtype[i].ChildCount%>
                                    Child(s)
                                    <%
                                        string childAges=string.Empty;
                                        for (int j = 0; j < itinerary.Roomtype[i].ChildCount; j++)
                                        {
                                            if (itinerary.Roomtype[i].ChildAge.Count > j)
                                            {
                                                if (childAges == string.Empty)
                                                {
                                                    childAges = itinerary.Roomtype[i].ChildAge[j].ToString();
                                                }
                                                else
                                                {
                                                    childAges += "," + itinerary.Roomtype[i].ChildAge[j].ToString();
                                                }
                                            }
                                            //if(allchildAges == string.Empty)
                                            //{
                                            //    allchildAges = childAges;
                                            //}
                                            //else
                                            //{
                                            //    allchildAges += "," + childAges;
                                            //}
                                        } %>
                                    
                                     <%--Child Ages(<%=childAges%>)--%>
                                          <%if (childAges != "")
                                         { %>
                                     Child Ages(<%=childAges%>)
                                     <%}%>
                                </td>
                            </tr>
                            <%} %>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th  class="themecol1" colspan="3" style="border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding:5px;  color:#000000;"
                        width="21%" align="left">
                        Lead Guest
                    </th>
                    
                </tr>
                <tr>
                    <td colspan="3">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="padding:5px; border: solid 1px #c6c6c6;" width="25%">
                                    <strong>Name :<br />
                                    </strong>
                                </td>
                                <td style="padding:5px; border: solid 1px #c6c6c6;" width="75%">
                                    <%  string paxName = "";
                                        if (itinerary.HotelPassenger.LeadPassenger)
                                        {
                                            paxName = itinerary.HotelPassenger.Firstname + " " + itinerary.HotelPassenger.Lastname;
                                    %>
                                    <%=paxName%><br />
                                </td>
                            </tr>
                            <%--<tr>
                                                    <td>
                                                        <strong>Address :
                                                            <br />
                                                        </strong>
                                                    </td>
                                                    <td>
                                                        <%=itinerary.HotelPassenger.Addressline1%> <%=itinerary.HotelPassenger.Addressline2%><br />
                                                    </td>
                                                </tr>--%>
                                 <%if (itinerary.Source != HotelBookingSource.UAH)
                                     { %>
                            <tr>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <strong>Nationality:<br />
                                    </strong>
                                </td>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <%=itinerary.HotelPassenger.Nationality%><br />
                                </td>
                            </tr>
                                     <%} %>
                            <tr>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <strong>Check In Date:
                                        <br />
                                    </strong>
                                </td>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <%=itinerary.StartDate.ToString("dd-MMM-yyy") %><br />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <strong>Check Out Date:
                                        <br />
                                    </strong>
                                </td>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <%=itinerary.EndDate.ToString("dd-MMM-yyy")%><br />
                                </td>
                            </tr>
                            <%int adults = 0, childs = 0, total = 0;
                              string mealPlanDesc = string.Empty;
                              foreach (HotelRoom room in itinerary.Roomtype)
                              {
                                  adults += room.AdultCount;
                                  childs += room.ChildCount;
                                  mealPlanDesc = room.MealPlanDesc;


                              }
                              total = adults + childs;
                            %>
                            <tr>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <strong>No. of Guests:<br />
                                    </strong>
                                </td>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <%=total %>,
                                    <%=adults%>(Adult(s)),
                                    <%=childs%>
                                    (Child(s))
                                      <%--  <%if (allchildAges != "")
                                         { %>
                                     ,Child Ages(<%=allchildAges%>)
                                     <%}%>--%>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <strong>No. of Nights :<br />
                                    </strong>
                                </td>
                                <td style="padding:5px; border: solid 1px #c6c6c6;">
                                    <%=nights %><br />
                                </td>
                            </tr>
                            <%-- <tr>
                                                    <td>
                                                        <strong> Meal Plan<br />
                                                        </strong>
                                                    </td>
                                                    <td>
                                                        <%=mealPlanDesc%><br />
                                                    </td>
                                                </tr>--%>
                            <%} %>
                        </table>
                    </td>
                </tr>
                  <%if (itinerary.Source == HotelBookingSource.GTA || itinerary.Source == HotelBookingSource.Agoda)
                    {
                        if (itinerary.SpecialRequest != null && itinerary.SpecialRequest.Length > 0)
                        {%>
                          <tr>
                           <th class="themecol1"  colspan="3" style="border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding:5px;  color:#000000;"
                        width="21%" align="left">
                        Essential Information / Please Note 
                    </th>
                    </tr>
                      <tr>
                    <td colspan="3" style="padding:5px; border: solid 1px #c6c6c6;">  
                          <div style="padding: 0px 10px 0px 10px">
                
                              <%string[] EssentialInf = itinerary.SpecialRequest.Split('|'); %>
                                <ul style="float: left;">
                                <%foreach (string Essential in EssentialInf)
                                  {
                                      if (Essential.Length > 0)
                                      { %>
                                  <li><%=Essential%></li>
                                <%}
                                  } %>
                                  </ul>
                               </div>
                    </td>
                </tr>
                            <%}
                    }%>
                <tr>
                <th class="themecol1"  colspan="3" style="border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding:5px;  color:#000000;"
                        width="21%" align="left">
                        Hotel Norms
                    </th>
                </tr>
                <tr>
                    <td colspan="3" style="padding: 5px; border: solid 1px #c6c6c6;">
                        <div style="padding: 0px 10px 0px 10px">
                            <ul style="float: left;">
                                <%if (!string.IsNullOrEmpty(itinerary.HotelPolicyDetails))
                                  { %>
                                <%string[] hotelPolicy = itinerary.HotelPolicyDetails.Split('|');
                                  foreach (string norm in hotelPolicy)
                                  {
                                      if (norm.Length > 0)
                                      {%>
                                <li>
                                    <%=norm.Replace("<br>", "").Replace("#&#", "").Split('^')[0]%></li>
                                <%}
                                  }
                                  if (hotelDetails.HotelPolicy != null)
                                  {
                                      string[] policies = hotelDetails.HotelPolicy.Split('|');
                                      foreach (string norm in policies)
                                      {
                        %>
                        <li><%=norm.Replace("<br>", "").Replace("#&#", "").Split('^')[0]%></li>
                        <%}
                                  } %>
                              <%}

                                  else
                                  { %>
                              Check-in time at 1400hrs and check-out time 1200hrs Early check-in or late check-out is subject to availability at the time of check-in/check-out at the hotel and cannot be guaranteed at any given point in time 
                              
                              <%} %>
                              
                             </ul>                   
                      </div>
                    </td>
                </tr>

              <% if (itinerary.Source == HotelBookingSource.GIMMONIX)
                               {   %>
      <tr>
               <th class="themecol1"  colspan="3" style="border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding:5px;  color:#000000;"
                   width="21%" align="left">
                       Disclaimer
                    </th>
                </tr>
                 <tr>
                    <td colspan="3" style="padding: 5px; border: solid 1px #c6c6c6;">
                        <div style="padding: 0px 10px 0px 10px">
                            <ul style="float: left;">
                           <%if (!string.IsNullOrEmpty(itinerary.HotelPolicyDetails) && itinerary.HotelPolicyDetails.Split('^').Length > 0 )                                
                                {
                                    var disclaimer = itinerary.HotelPolicyDetails.Split('^');
                                    if (!string.IsNullOrEmpty(disclaimer[1]))
                                    {%>
             
                                <li><%=disclaimer[1].Split('|')[0].Replace("<br>", "").Replace("#&#", "")%></li>

                               <%}%>
                             
                              <%} %>
                                  <li><label class="bold"> Bed type availabilities depends @ Check-In time </label></li>
                             </ul>                   
                      </div>
                    </td>
                </tr>
                             
      <%} %> 



                               <%if (showVoucherTerms == true)
                                    { %>
                <tr>
                 
                    <th  class="themecol1" colspan="3" style="border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding:5px; color:#000000;"
                        width="21%" align="left">
                        Cancellation & Charges
                    </th>
                    
                </tr>
                <tr>
                <td colspan="3" style="padding:5px; border: solid 1px #c6c6c6;">
                 <div style="padding: 0px 10px 0px 15px;">
                  <ul  style="padding: 0px 10px 0px 10px;display:block">
                  <% string cancelData = "", remarks = "";
                      Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
                      if (itinerary.Source == HotelBookingSource.DOTW)
                      {
                          cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                      }
                      else if (itinerary.Source == HotelBookingSource.RezLive)
                      {
                          if (itinerary.Source == HotelBookingSource.RezLive)
                          {
                              cancelData = itinerary.HotelCancelPolicy;
                              cancelData = cancelData.Replace(". ", "|");
                          }
                          cancellationInfo.Add("CancelPolicy", cancelData);
                      }
                      //else if (itinerary.Source == HotelBookingSource.GIMMONIX)
                      //{
                      //    if (itinerary.Source == HotelBookingSource.GIMMONIX)
                      //    {
                      //        cancelData = itinerary.HotelCancelPolicy;
                      //        cancelData = cancelData.Replace("^", "|");
                      //    }
                      //    cancellationInfo.Add("CancelPolicy", cancelData);
                      //}
                      else
                      {
                          cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                      }
                      foreach (KeyValuePair<string, string> pair in cancellationInfo)
                      {
                          switch (pair.Key)
                          {
                              case "lastCancellationDate":
                                  break;
                              case "CancelPolicy":
                                  cancelData = pair.Value;

                                  break;
                              case "HotelPolicy":
                                  remarks = pair.Value;
                                  break;
                          }
                      }
                     %>
                    <%
                         cancelData = cancelData.Replace("^Date and time is calculated based on local time of destination|", "|");
                                     cancelData = cancelData.Replace("^", "|");
                        string[] cdata = cancelData.Split('|');%>
                                        <%foreach (string data in cdata)
                                          {
                                              if (data.Length > 0)
                                              {%>
                                        <li>
                                            <%=data%></li>
                                        <%}
                                 } %>
                                    </ul>

                     <!-- TAx Info -->
                      <ul style="padding: 0px 10px 0px 10px;display:block">
                      <% if (itinerary.Roomtype[0].TaxInfo != null && itinerary.Roomtype[0].TaxInfo.Count > 0)
    {
        List<HotelTaxBreakup> hotelTaxList = itinerary.Roomtype[0].TaxInfo;
        //var distinctList=hotelTaxList.Distinct();
        var distinctList = hotelTaxList.GroupBy(x => x.TaxName).Select(cl => new HotelTaxBreakup
        {
            TaxName = cl.Select(c => c.TaxName).FirstOrDefault(),
            IsIncluded = cl.Select(c => c.IsIncluded).FirstOrDefault(),
            UnitType = cl.Select(c => c.UnitType).FirstOrDefault(),
            IsMandatory = cl.Select(c => c.IsMandatory).FirstOrDefault(),
            FrequencyType = cl.Select(c => c.FrequencyType).FirstOrDefault(),
            TaxValue = cl.Sum(c => c.TaxValue)
        }).ToList();

        decimal totExl = 0;
           foreach (var Breakups in distinctList)
              {
                  if (Breakups.IsIncluded)
                  {%>
             <%-- <li><% =Breakups.TaxName.Replace("_", " ") %>-<%= Breakups.UnitType %> -Incl: <strong><%=itinerary.Roomtype[0].Price.Currency %><%= Breakups.TaxValue %></strong>-<%=Breakups.FrequencyType %>--%>
                  <li><% =Breakups.TaxName.Replace("_", " ") %>-<%= Breakups.UnitType %> -Incl: <strong><%=itinerary.Roomtype[0].Price.Currency %><%= Breakups.TaxValue.ToString("N" + Settings.LoginInfo.DecimalValue) %></strong>
                                            
                                        </li>
              <%}
    } 
        foreach (var Breakups in distinctList)
        {
            if (!Breakups.IsIncluded)
            {
                totExl = totExl + Breakups.TaxValue;
                                          %>

                                                 <%--<li><% =Breakups.TaxName.Replace("_"," ") %>-<%= Breakups.UnitType %> -Excl (Pay @ Hotel): <strong><%=itinerary.Roomtype[0].Price.Currency %><%= Breakups.TaxValue %></strong>-<%=Breakups.FrequencyType %> --%>
                                                     <li><% =Breakups.TaxName.Replace("_"," ") %>-<%= Breakups.UnitType %> -Excl (Pay @ Hotel): <strong><%=itinerary.Roomtype[0].Price.Currency %><%= Breakups.TaxValue.ToString("N" + Settings.LoginInfo.DecimalValue) %></strong>
                                            
                                        </li>
                                    <%} } if (totExl>0) { %>
                                    <li>Total (pay @ Hotel) :<strong><%=itinerary.Roomtype[0].Price.Currency %> <%=totExl.ToString("N" + Settings.LoginInfo.DecimalValue) %> </strong></li>
                          <%} }    %>
                                </ul>
                     <!-- end tax info -->
                 </div>
                </td>
                </tr>
                
                <tr>
                 
                    <th class="themecol1" colspan="3" style="border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding:5px;   color:#000000;"
                        width="21%" align="left">
                        Terms & Conditions
                    </th>
                   
                </tr>
                <%--added by phani for only INDIA SPECIFIC terms and conditions --%>
               <%  if ( itinerary.Roomtype[0].Price.Currency == "INR")
                   {%>
                   <tr>
                          <td colspan="3" style="padding:5px; border: solid 1px #c6c6c6;">
                        <div style="padding: 0px 10px 0px 10px">
                            <li style="list-style-type: circle">All rooms are guaranteed on the day of arrival. 
                                In the case of no-show, your room(s) will be released and you will subject to the
                                terms and condition of the Cancellation/No-show policy specified at the time you
                                made the booking as well as noted in the confirmation email. </li>
                            <li style="list-style-type: circle">The total price for these booking does not 
                                include mini-bar items, telephone bills, laundry service, etc. The hotel will
                                bill you directly.</li>
                            <li style="list-style-type: circle"> Any complaints related to the respective hotel services,
                                with regards to location, rooms, food, cleaning or other services, the guest will have 
                                to directly deal with the hotel. Cozmo Travel World will not be responsible for such
                                complaints. </li>
                            <li style="list-style-type: circle"> The General Hotel Policy: Check-in time at 1400hrs 
                                and check-out time 1200hrs.Early check-in or Late check-out is subject to availability 
                                and cannot be guaranteed at any given point in (of) time </li>
                            <li style="list-style-type: circle">Interconnecting / Adjoining rooms/any special requests 
                                are always subject to availability at the time of check-in, and Cozmo Travel World
                                will not be responsible for any denial of such rooms to the Customer.</li>
                            <li style="list-style-type: circle">Most of the hotels will be asking for credit card
                                or cash amount to be paid upon check-in as guaranteed against any expected extras
                                by the guest, Cozmo Travel World will not be responsible in case the guest doesn’t
                                carry a credit card or enough cash money for the same, and the guest has to follow 
                                up directly with the hotel for the refund upon check out, Cozmo Travel World is not
                                responsible in case of any delay from central bank for credit card refunds. </li>
                                
                        </div>
                    </td>
                </tr>
                <%}
    else
    {%>
                <tr>
                    <td colspan="3" style="padding:5px; border: solid 1px #c6c6c6;">
                        <div style="padding: 0px 10px 0px 10px">
                            <li style="list-style-type: circle"> All rooms are guaranteed on the day of arrival.
                                In the case of no-show, your room(s) will be released and you will subject to the
                                terms and condition of the Cancellation/No-show policy specified at the time you
                                made the booking as well as noted in the confirmation Email. </li>
                            <li style="list-style-type: circle">The total price for these booking fees not include
                                mini-bar items, telephone bills, laundry service, etc. The hotel will bill you directly.
                            </li>
                            <li style="list-style-type: circle">In case where breakfast is included with the room
                                rate, please note that certain hotels may charge extra for children travelling with
                                their parents. If applicable, the hotel will bill you directly. Upon arrival, if
                                you have any questions, please verify with the hotel. </li>
                            <li style="list-style-type: circle">Any complaints related to the respective hotel services,
                                with regards to location, rooms, food, cleaning or other services, the guest will
                                have to directly deal with the hotel. <%=dynamicAgentLabel %> will not be responsible for
                                such complaints. </li>
                            <li style="list-style-type: circle">The General Hotel Policy: Check-in time at 1400hrs
                                and check-out time 1200hrs Early check-in or late check-out is subject to availability
                                at the time of check-in/check-out at the hotel and cannot be guaranteed at any given
                                point in time </li>
                            <li style="list-style-type: circle">Interconnecting/ Adjoining rooms/any special requests
                                are always subject to availability at the time of check-in, and <%=dynamicAgentLabel %> will
                                not be responsible for any denial of such rooms to the Customer. </li>
                            <li style="list-style-type: circle">Most of the hotels will be asking for credit card
                                or cash amount to be paid upon check-in as guaranteed against any expected extras
                                by the guest, <%=dynamicAgentLabel %> will not be responsible in case the guest doesn’t carry
                                a credit card or enough cash money for the same, and the guest has to follow up
                                directly with the hotel for the refund upon check out, <%=dynamicAgentLabel %> is not responsible
                                in case of any delay from central bank for credit card refunds. </li>
                                
                        </div>
                    </td>
                </tr><%} %>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                    <%} %>
                <tr>
                    <td colspan="3" align="center">
                   
                   <div style="font-size:10px"> 
   <%if (itinerary.Source == HotelBookingSource.GIMMONIX)
        {
            for (int i = 0; i < itinerary.NoOfRooms; i++)  %>
                            <%{  if (!string.IsNullOrEmpty(itinerary.Roomtype[i].Gxsupplier))
                                    {%> 
                                <% if (!itinerary.Roomtype[0].RoomTypeCode.Replace("||", "|").Split('|')[3].Contains("EPS"))

    {%>
                      <label>Booked and  payable by :</label><%=itinerary.Roomtype[i].Gxsupplier%><br />
                       <%}
            }                  }
    }
                                       else
                                       { %>
 <%=itinerary.PaymentGuaranteedBy%>
  <%} %>
  </div>
                   
                   
                    
                      
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <%// string cancelData = "", remarks = "";
                // if (Session["cSessionId"] != null)
                //{
                //    List<HotelPenality> penaltyInfo = itinerary.PenalityInfo;
                //    CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi(Session["cSessionId"].ToString());
                //    //DOTWApi dotw = new DOTWApi(Session["cSessionId"].ToString());

                //    if (penaltyInfo == null)
                //    {
                //        penaltyInfo = new List<HotelPenality>();
                //    }
                //    Dictionary<string, string> cancellationInfo = dotw.GetCancellationPolicy(itinerary, ref penaltyInfo, true);
                //    foreach (KeyValuePair<string, string> pair in cancellationInfo)
                //    {
                //        switch (pair.Key)
                //        {
                //            case "lastCancellationDate":
                //                break;
                //            case "CancelPolicy":
                //                cancelData = pair.Value;
                //                break;
                //            case "HotelPolicy":
                //                remarks = pair.Value;
                //                break;
                //        }
                //    }
                //}
                        
            %>
        </div>
        <%--
<table width="98%" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <%foreach (HotelRoom room in itinerary.Roomtype)
                                      { %>
                                    <tr>
                                        <td valign="middle" align="center">
                                            <img src="images/hotel.png" />
                                        </td>
                                    </tr>
                                    <%} %>
                                </table>--%>
    </div>
    </form>
</body>
</html>
