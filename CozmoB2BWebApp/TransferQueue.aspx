﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="TransferQueue.aspx.cs" Inherits="CozmoB2BWebApp.TransferQueue" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="scripts/bootstrap-datepicker.js"></script>

    <script type="text/javascript" src="scripts/jquery.twbsPagination.js"></script>
     <script src="build/js/nprogress.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>   

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <script src="scripts/toastr.min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="css/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="css/toastr.css" />
    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    
     <link href="css/style.css" rel="stylesheet" type="text/css" /> <!--Added by chandan on  13062016 -->


    <div class="co-md-12">
        <div class="col-md-4">
            <div class="col-md-4">
                From Date               
            </div>
            <div class="col-md-8">                 
               <input type="text" id="fromDate" class="form-control" readonly="readonly"/>                 
            </div>           
        </div> 
        <div class="col-md-4">
            <div class="col-md-4">To Date</div>
            <div class="col-md-8">
                <input type="text" id="toDate" class="form-control" readonly="readonly" />
            </div>
        </div> 
        <div class="col-md-4">
            <div class="col-md-4">Status</div>
            <div class="col-md-8">
                <select id="bookingStatus" class="select2 form-control" >
                    <option value="-1" selected="selected">All</option>
                </select>
            </div>
        </div> 
    </div>
     
    <div class="clearfix"> </div> 
    
    <div class="co-md-12 margin-top-5">
        <div class="col-md-4">
            <div class="col-md-4">Source</div>
            <div class="col-md-8">
                <select class="select2 form-control"id="sourceSelect">
                    <option value="-1" selected="selected">All</option>
                </select>
            </div>
        </div> 
        <div class="col-md-4">
            <div class="col-md-4">Agent</div>
            <div class="col-md-8">
                <select class="select2 form-control"id="agentSelect">

                </select>
            </div>
        </div> 
        <div class="col-md-4">
            <div class="col-md-4">B2B Agent</div>
            <div class="col-md-8">
                <select class="select2 form-control"id="b2bAgentSelect">

                </select>
            </div>
        </div> 
    </div>

     <div class="clearfix"> </div> 

     <div class="co-md-12 margin-top-5">
        <div class="col-md-4">
            <div class="col-md-4">B2B2B Agent</div>
            <div class="col-md-8">
                <select class="select2 form-control"id="b2b2bAgentSelect" disabled="disabled">

                </select>
            </div>
        </div> 
        <div class="col-md-4">
            <div class="col-md-4">Pax Name</div>
            <div class="col-md-8">
               <input type="text" class="form-control" id="paxName" />
            </div>
        </div> 
        <div class="col-md-4">
            <div class="col-md-4">Confirmation No</div>
            <div class="col-md-8">
               <input type="text" class="form-control" id="confirmationNo" />
            </div>
        </div> 
    </div>

    <div class="clearfix"> </div> 
    <div class="col-md-12 margin-top-5">
        <button class="btn but_b pull-right" type="button" id="btnSearch">Search</button>
    </div>

    <div class="col-md-12" id="SearchResponse" >
        <div class="col-md-12 center">
            <ul class="pagination pull-right" id="paginationHead"></ul>
        </div>
        <div id="divSearchResult">

        </div>
        <div class="col-md-12 center">
            <ul class="pagination" id="pagination"></ul>
        </div>
        
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="RefundModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Refund Confirm</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div class="row">              
                <div class="col-md-12">
                    <label id="fortxtAdminFee">Admin Fee AED</label>
                    <div class="">                                                   
                        <input type="text" id="txtAdminFee" class="form-control" onkeypress="return allowNumerics(event)" value="0" /> 
                    </div>
                </div>                  
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label id="fortxtSupplierFee">Supplier Fee AED</label>
                    <div class="">                                                 
                       <input type="text" id="txtSupplierFee" class="form-control" onkeypress="return allowNumerics(event)" value="0" />
                    </div>
                </div>
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning" id="ConfirmRefund">Refund</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    </div>
     <input type="hidden" id="hdnUserId" runat="server" />
    <input type="hidden" id="hdnAgentId" runat="server" />
    <input type="hidden" id="hdnBehalfLocation" runat="server" />        
    <input type="hidden" id="hdnSession" runat="server" />
    <script>
        
        var AgentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
        var UserId = JSON.parse(document.getElementById('<%=hdnUserId.ClientID %>').value);
        var BehalfLocation = JSON.parse(document.getElementById('<%=hdnBehalfLocation.ClientID %>').value);
        var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["TransferWebApiUrl"]%>';
   
    </script>
    <script src="scripts/Transfers/transferQueue.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
