<%@ Page Language="C#" AutoEventWireup="true" Inherits="PrintInvoice" Codebehind="PrintInvoice.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace = "CT.BookingEngine" %>
<%@ Import Namespace = "CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace = "CT.Core" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="System.Data" %>

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Print Invoice</title>
    <link href="style.css" rel="stylesheet" />
    <script type="text/javascript" src="CT.js"></script>
    </head>
<script language="JavaScript" type="text/javascript">
function ClosePop()
{            
    window.close();
}
</script>
<script language="JavaScript" type="text/javascript">
<!--

/* The actual print function */

function prePrint()
{
    document.getElementById('Print').style.display="none";
	window.print();		
	document.getElementById('Print').style.display="block";
}

// -->
</script>
<body>
    <form id="form1" runat="server">
    <%if(Request["isSave"]!="true")
      { %>
    <div>
    <div class="invoice-parent-block fleft">
        <div class="fleft border-bottom-black width-100">
            <div class="padding-10 fleft">
                <div class="fleft font-16 bold width-240">
                    <span>Invoice No-</span><span class="margin-left-15"><%=invoice.CompleteInvoiceNumber%></span>
                </div>
                <div class="fleft" style="margin-left: 50px;">
                   Invoice Date :   <b><% = Util.UTCToIST(invoice.CreatedOn).ToString("dd MMMM yyyy") %></b>
                </div>
                <div class="fright bold width-240 right-align">
                    PNR: <% = pnr %></div>
            </div>
        </div>
        <div class="padding-10 fleft" id="InvoicePP" bordercolor="AliceBlue" borderstyle="Solid">
                <div class="fleft">       
                <div class="fleft">
                <%if (Request["fromAgent"] == null && invoice.AgencyId == 1)
                  { %>
                <div class="font-11 padding-top-5" style="width:200px">
                <div class="fleft bold">
                    Billing Address
                </div>
                <div class="fleft">
                    <%--<textarea id="BillingAddress" rows="3" cols="40" style="overflow:auto"></textarea>--%>
               <asp:TextBox ID="BillingAddress" Columns="0" Width="310px" Rows="3" runat="server" TextMode="MultiLine" BorderColor="white" CssClass="noScroll"></asp:TextBox>
                </div>
                </div>
                <%}
                  else
                  {
                      AgentMaster selfAgency = new AgentMaster(1);
                      %>
                    <div class="font-11 padding-top-5 width-240">
                    <div><%=selfAgency.Name %></div>
                    <div><%=selfAgency.Address%> 
                        
                        <%if (selfAgency.City.Length > 0){ %>
                        <%--  
                        <%=RegCity.GetCity(selfAgency.Address.CityId).CityName%>
                        <%}else--%>
                          
                          { %>
                        <%=selfAgency.City%>
                          <%} %>
                    </div>
                    <div>
                        <%  if (selfAgency.Phone1 != null && selfAgency.Phone1.Length != 0)
                            {%>
                        Phone:
                        <% = agency.Phone1%>
                        <%  }
                            else if (selfAgency.Phone2 != null && selfAgency.Phone2.Length != 0)
                            { %>
                        Phone:
                        <% = selfAgency.Phone2%>
                        <%  } %>
                        <%  if (selfAgency.Fax != null && selfAgency.Fax.Length > 0)
                            { %>
                        Fax:
                        <% = selfAgency.Fax%>
                        <%  } %>
                    </div>
                     <b> Service Tax Reg. No : <%=ConfigurationManager.AppSettings["ServiceTaxRegNo"].ToString() %></b>
                  </div>
                  <%} %>
                </div>
                <div class="fleft bold font-size-15" style="margin-left: 100px;">
                    INVOICE
                </div>
                    <div class="font-11 padding-top-5 fright width-240 right-align">
                    <div class="bold">
                        <% = agency.Name %>
                    </div>
                    <div>
                        <% = agencyAddress %>
                        <% = agencyCity.CityName %>
                        <%  %>
                    </div>
                    <div>
                        <%  if (agency.Phone1 != null && agency.Phone1.Length != 0)
                            {%>
                        Phone:
                        <% = agency.Phone1%>
                        <%  }
                            else if(agency.Phone2 != null && agency.Phone2.Length != 0)
                            { %>
                        Phone:
                        <% = agency.Phone2%>
                        <%  } %>
                        <%  if (agency.Fax != null && agency.Fax.Length > 0)
                            { %>
                        Fax:
                        <% = agency.Fax%>
                        <%  } %>
                    </div>
                </div>
               
            </div>
             Travel Date:<%=traveldate.ToString("dd MMM yyyy") %>
            <div class="fleft margin-top-15 font-11 border-y" style="padding: 5px;">
                <div class="fleft width-100 bold">
                    <div class="fleft width-20">
                        Serial No</div>                         
                    <div class="fleft margin-left-3 width-110" style="width:80px;">Ticket No</div>
                    <div class="fleft margin-left-10 width-110" style="width:70px;">Sectors</div>
                   <div class="fleft margin-left-10 width-50">Flight</div>
                    <div class="fleft margin-left-10 width-90">PAX Name</div>
                    <div class="fleft margin-left-10 width-30">Type</div>
                    <div class="fleft margin-left-10 width-50">Class</div>
                    <div class="fleft margin-left-10 width-50">Fare</div>
                    <div class="fleft margin-left-10 width-50">Tax</div>
                    <div class="fleft margin-left-10 width-50">O/C</div>
                </div>
                <%  int i = 0; %>
                <%  foreach (Ticket ticket in ticketList)
                    { 
                       string flightNum = string.Empty; 
                        string bookingClass = string.Empty;
                       // FlightInfo[] segments = FlightInfo.GetSegments(ticket.FlightId);

                        DataRow[] row;
                        if (ticket.TicketType == "C" || ticket.ConjunctionNumber.StartsWith("C"))
                        {
                            row = segment[ticket.ConjunctionNumber];
                        }
                        else
                        {
                            row = segment["C0"];
                        }

                        routing = string.Empty;
                        int k = 0;
                        for (; k < row.Length; k++)
                        {
                            bookingClass = bookingClass + row[k]["bookingClass"].ToString();
                            if (k == 0)
                            {
                                routing = row[k]["source"].ToString();
                            }
                            else
                            {
                                routing = routing + "-" + row[k]["source"].ToString();
                            }
                        }

                        routing = routing + "-" + row[k - 1]["destination"].ToString();

                       flightNum = row[0]["AirLine"].ToString() + row[0]["flightNumber"].ToString();
                        //foreach (FlightInfo temp in segments)
                        //{
                        //    bookingClass = bookingClass + temp.BookingClass;
                        //}
                        //flightNum = segments[0].Airline + segments[0].FlightNumber; 
                        i++;
                %>
                <div class="fleft width-100 margin-top-8">
                    <div class="fleft width-20">
                        <% = i %></div>
                    <div class="fleft margin-left-3 width-110" style="width:80px;">
                      <%=ticket.ValidatingAriline%><% = ticket.TicketNumber %></div>
                    <div class="fleft margin-left-10 width-110" style="width:70px;">
                        <% = routing %></div>
                          <div class="fleft margin-left-10 width-50">
                        <% = flightNum%></div>
                    <div class="fleft margin-left-10 width-90">
                        <% = FlightPassenger.GetPaxFullName(ticket.PaxId) %></div>
                    <div class="fleft margin-left-10 width-30">
                        <% = FlightPassenger.GetPTC(ticket.PaxType) %></div>
                    <div class="fleft margin-left-10 width-50 ">
                        <%=bookingClass%></div>
                    <div class="fleft margin-left-10 width-50">
                        <% = ticket.Price.PublishedFare.ToString("N2")%>
                    </div>
                    <div class="fleft margin-left-10 width-50">
                        <% = ticket.Price.Tax.ToString("N2")   %></div>
                    <div class="fleft margin-left-10 width-50 center">
                        <% = ticket.Price.OtherCharges.ToString("N2")   %>
                    </div>
                </div>
                <%  } %>              
               
            </div>
            <%--<%if (invoice.Remarks.Trim().Length > 0)
              { %>
             <div class="fleft width-570 margin-top-10 margin-right-20">
                <div class="fleft width-100 margin-top-5 bold">
                    Remarks:</div> 
                <%=invoice.Remarks%>
                    
            </div>
            <%} %>--%>
            <div class="fleft width-300 margin-top-10 margin-right-20">
                <div class="fleft width-100 margin-top-5 bold">
                    Notes:</div>                    
                    *** Voidation Rs 500
                <div class="clear">
                    All Penalties as per fare rules
                </div>
                
                    
            </div>
            <div class="fright width-300 margin-top-10">
                <div class="fleft width-100 margin-top-5 bold">
                    <div class="fleft width-50">&nbsp;
                    </div>
                    <div class="fleft width-140">
                    Gross:</div>
                    <div class="width-60 fleft text-right">
                    <%decimal totalPrice=0;
                      decimal handlingCharge=0;
                      decimal serviceTax=0;
                      decimal tdsCommission=0;
                      decimal tdsPLB=0;
                      decimal plb = 0;
                      decimal transactionFee = 0;
                      decimal discount = 0;
                      decimal reverseHandlingCharge = 0;
                        for(int k=0;k<ticketList.Count;k++)
                        {
                            totalPrice = totalPrice + (ticketList[k].Price.PublishedFare + ticketList[k].Price.Tax + ticketList[k].Price.OtherCharges);
                            handlingCharge = handlingCharge + (ticketList[k].Price.AgentCommission);
                            serviceTax = serviceTax + (ticketList[k].Price.SeviceTax);
                            tdsCommission = tdsCommission + ticketList[k].Price.TdsCommission;
                            plb=plb+ticketList[k].Price.AgentPLB;
                            tdsPLB = tdsPLB + ticketList[k].Price.TDSPLB;
                            transactionFee = transactionFee + ticketList[k].Price.TransactionFee;
                            discount += ticketList[k].Price.Discount;
                            reverseHandlingCharge += ticketList[k].Price.ReverseHandlingCharge;
                         %>
                        <%}%>
                        
                    <%=totalPrice.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) %></div>
                </div>
                <input type="hidden" id="MaxHandling" value='<%=handlingCharge+plb %>' />
                <% if (!isServiceAgency)
                   { %>
                <div class="fleft width-100 margin-top-5">
                    <div class="fleft width-50 italic">
                    Less
                    </div>
                    <div class="fleft width-140">
                        Commission Earned</div>
                    <div class="width-60 fleft text-right">
                    <%HandlingBox.Text = (handlingCharge + plb).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ); %>
                    <asp:TextBox ID="HandlingBox" runat="server" BorderColor="white" CssClass="text-right font-12"
                        Text="" Width="60"></asp:TextBox>
                        <asp:CustomValidator ID="ValidatorHandlingCharge" runat="server" ErrorMessage="Invalid handling charge" ClientValidationFunction="IsValidHandling" Display="Dynamic"></asp:CustomValidator>
                        </div>
                </div>
                <% } %>
                <%if (reverseHandlingCharge > 0)
                  { %>
                <div class="fleft width-100 margin-top-5">
                    <div class="fleft width-50 italic">
                        Add
                    </div>
                    <div class="fleft width-140">
                        Comm. Reversed</div>
                    <div class="width-60 fleft text-right">
                        <%=reverseHandlingCharge.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] )%></div>
                </div>
                <%} %>
                <div class="fleft width-100 margin-top-5">
                    <div class="fleft width-50 italic">
                        Add
                    </div>
                    <div class="fleft width-140">
                        Service Tax</div>
                    <div class="width-60 fleft text-right">
                        <%=serviceTax.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) %></div>
                </div>
                <div class="fleft width-100 margin-top-5">
                    <div class="fleft width-50 italic">
                        Add
                    </div>
                    <div class="fleft width-140">
                        Tra Fee</div>
                    <div class="width-60 fleft text-right">
                        <%=transactionFee.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) %></div>
                </div>
                <% if(!isServiceAgency)
                   { %>
                <div class="fleft width-100 margin-top-5 bottom-border">
                    <div class="fleft width-50 italic">
                        Add
                    </div>
                    <div class="fleft width-140" id="tdsDeducted">
                        TDS Deducted</div>
                    <div class="width-60 text-right fleft">
                        <%=(tdsCommission + tdsPLB).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] )%></div>
                </div>
                <% } %>
                <%  decimal netAmount = 0;
                    decimal netReceivable = 0;
                    if (!isServiceAgency)
                    {
                        netAmount = totalPrice - handlingCharge - plb + serviceTax + tdsCommission + tdsPLB + transactionFee + reverseHandlingCharge;
                        netReceivable = totalPrice - handlingCharge - plb + serviceTax + tdsCommission + tdsPLB + transactionFee + reverseHandlingCharge;
                    }
                    else
                    {
                        netAmount = totalPrice + serviceTax + transactionFee;
                        netReceivable = totalPrice + serviceTax + transactionFee;
                    }   %>
                <div class="fleft width-100 margin-top-5">
                    <div class="fleft width-50">
                        &nbsp;
                    </div>
                    <div class="fleft width-140">
                        Net Amount</div>
                    <div class="width-60 fleft text-right">
                        <% =Math.Round(netAmount).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) %></div>
                </div>
                <div class="fleft width-280 margin-top-5">                    
                    <div class="fleft width-140 padding-left-50">
                        Net Receivable</div>
                    <div class="width-60 fleft text-right">
                        <% =Math.Round(netReceivable).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) %></div>
                </div>
                <div class="fleft width-140 margin-top-5">
                    <div class="fleft width-140 padding-left-50 italic">
                        (Amount in <%=CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"]  %>)</div>
                </div>
            </div>
             <%if (discount > 0)
              { %>
               <div class="width-100">
            You are eligible for a discount of <%=CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"]  %><%=discount %> on this invoice</div>
          <%} %>
            <div class="fleft width-300">
               
                <div class="width-100 fleft">
                    
                </div>
                <div class="width-100 fleft">
                    Invoice Status :<%= invoice.Status%>
                </div>
                
                <div class="width-100 fleft">
                    
                </div>
                
                <% UserMaster member = new UserMaster(invoice.CreatedBy); %>
                <div class="width-100 margin-top-10 fleft">
                    Billed by : <%= CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"]  %>
                </div>
                <div class="width-100 fleft">
                    Ticketed by :
                    <%= member.FirstName + " " + member.LastName%>
                </div>
                 
                
            </div>
         
         
            
        </div>
        <div class="text-right width-100 fright">
            <input id="Print" onclick="prePrint()" type="button" value="Print" />
        </div>
    </div>
        
    </div>
    <%} %>
    </form>
</body>
</html>
