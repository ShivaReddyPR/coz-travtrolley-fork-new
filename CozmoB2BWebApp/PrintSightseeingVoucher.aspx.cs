﻿using System;
using System.Collections;
using System.Configuration;
using System.Web.UI;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using System.IO;
using System.Data;

public partial class PrintSightseeingVoucherGUI : CT.Core.ParentPage
{

    protected SightseeingItinerary itinerary;
    protected decimal rateOfExchange = 1;
    protected int nights;
    protected AgentMaster agent;
    protected BookingDetail booking;
    protected string logoPath = string.Empty;
    protected string pickupLocationDetails = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }

            if (Request.QueryString["ConfNo"] != null)
            {
                itinerary = new SightseeingItinerary();
               
                string confirmationNo = Request["ConfNo"].ToString();

                string logoPath = "";
               
                //if (Source.Length > 0 )
                {
                    booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(SightseeingItinerary.GetSightseeingId(confirmationNo), ProductType.SightSeeing));
                    try
                    {
                        Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
                        agent = new AgentMaster(booking.AgencyId);
                        itinerary.Load(products[0].ProductId);
                      
                        
                        //to show agent logo
                        if (agent.ID > 0)
                        {

                            logoPath = ConfigurationManager.AppSettings["AgentImage"] + Settings.LoginInfo.AgentLogoPath;
                            //logoPath = "http://ctb2bstage.cozmotravel.com/" + ConfigurationManager.AppSettings["AgentImage"] + agent.ImgFileName;
                           imgHeaderLogo.ImageUrl = logoPath;

                        }
                        else
                        {
                           imgHeaderLogo.ImageUrl = "images/logo.jpg";
                        }
                            BindData();


                    }
                    catch (Exception exp)
                    {
                        
                        Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Exception Loading itinerary. Message: " + exp.Message, "");
                    }

                }
            }
      
            else if (Request.QueryString["ConfNo"] == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }

           
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.SightseeingBooking, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get Voucher. Error: " + ex.Message, Request["REMOTE_ADDR"]);
        }
    }
    private void BindData()
    {
        if (itinerary != null)
        {
            lblTourName.Text = itinerary.ItemName + ", " + itinerary.AdultCount.ToString() + "Adults," + itinerary.ChildCount.ToString() + "Children";
            lblServiceType.Text = "Tour";
            lblBookedBy.Text = Settings.LoginInfo.FirstName + " " + Settings.LoginInfo.LastName;
            lblBookingStatus.Text = itinerary.BookingStatus.ToString();
            lblBookingDate.Text = itinerary.CreatedOn.ToString("dd MMM yyyy");
            lblTransactionDate.Text = itinerary.TourDate.ToString("dd MMM yyyy");
            lblCity.Text = itinerary.CityName;
            //lblPassengers.Text = (itinerary.AdultCount + itinerary.ChildCount).ToString();
            //lblCountry.Text = itinerary.CountryName;

            //lblDuration.Text = itinerary.Duration == "" ? "N/A" : itinerary.Duration;
            //lblTourAddress.Text = itinerary.Address == "" ? "N/A" : itinerary.Address;

            //Use ceiling instead of Round, Changed on 06082016
            //lblTotal.Text = (Math.Ceiling(itinerary.Price.NetFare + itinerary.Price.Markup + itinerary.Price.B2CMarkup) +  Math.Ceiling(itinerary.Price.OutputVATAmount)).ToString("N" + agent.DecimalValue);
            if (itinerary.DepPointInfo != null && itinerary.DepPointInfo.Length > 0)
            {
                string[] aData = itinerary.DepPointInfo.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string depoint in aData)
                {
                    //lblPickupPoint.Text += depoint.Split(',')[0];
                    lblPickupTime.Text += depoint;
                }
                //lblPickupPoint.Text += depoint.Split(',')[0];
                //lblPickupTime.Text = depoint.Split(',')[1];
            }
            else
            {
                //lblPickupPoint.Text = "N/A";
                lblPickupTime.Text = "N/A";
            }

            //lblPickupTime.Text = itinerary.DepTime.Length>0?itinerary.DepTime:"N/A";
            //lblAdultCount.Text = itinerary.AdultCount.ToString();
            //lblChildCount.Text = itinerary.ChildCount.ToString();
            lblBookingRef.Text = itinerary.SightseeingId.ToString();
            //lblStartingPoint.Text = lblPickupPoint.Text;
            //lblEndingPoint.Text = lblPickupPoint.Text;
            lblBookingRef.Text = itinerary.SightseeingId.ToString();
            lblConfirmation.Text = itinerary.ConfirmationNo;
            lblGuestName.Text = itinerary.PaxNames[0];
            lblAdditional.Text = "none";
            lblPickPoint.Text = itinerary.PickupPoint;
            //lblTourLanguage.Text = itinerary.Language.Split('#')[0];
            //lblPhone.Text = itinerary.TelePhno;
            lblSupplierInfo.Text = itinerary.BookingReference;
            //lblSpecialName.Text = itinerary.SpecialName.Length>0?itinerary.SpecialName:"N/A";
            DataTable dtSupplierdetails = SightseeingItinerary.GetSupplierEmail(Convert.ToInt32(itinerary.ItemCode));

            if (dtSupplierdetails != null && dtSupplierdetails.Rows.Count > 0)
            {
                lblSupplierName.Text = Convert.ToString(dtSupplierdetails.DefaultView[0]["supplierName"]);
                lblServiceName.Text = Convert.ToString(dtSupplierdetails.DefaultView[0]["SupplierServiceName"]);
                lblTelphone.Text = Convert.ToString(dtSupplierdetails.DefaultView[0]["SupplierTelephone"]);
                //lblEmail.Text = Convert.ToString(dtSupplierdetails.DefaultView[0]["supplierEmail"]);
                //lblPickPoint.Text= Convert.ToString(dtSupplierdetails.DefaultView[0]["pickup_point"]);
            }

            int agentId = 0;
            if (Settings.LoginInfo != null)
            {
                agentId = Settings.LoginInfo.AgentId;
            }
            string serverPath = "http://ctb2b.cozmotravel.com/";

            if (agentId > 1)
            {
                logoPath = serverPath + ConfigurationManager.AppSettings["AgentImage"] + Settings.LoginInfo.AgentLogoPath;
                //imgLogo.ImageUrl = logoPath;
            }
            else
            {
                logoPath = serverPath + ConfigurationManager.AppSettings["AgentImage"] + "3.png"; //@@@@ image name required to check in server location
            }


        }


    }
    protected void btnEmailVoucher_Click(object sender, EventArgs e)
    {
        try
        {
            #region Sending Email
            Hashtable table = new Hashtable();
            //table.Add("agentName", agent.Name);
            //table.Add("itemName", itinerary.ItemName);
            //table.Add("confirmationNo", itinerary.ConfirmationNo);
                
            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();

                toArray.Add(txtEmailId.Text.Trim());
                btnEmail.Visible = false;
                btnPrint.Visible = false;

                string message = "";

                StringWriter sWriter = new StringWriter();
                HtmlTextWriter htWriter = new HtmlTextWriter(sWriter);
                printableArea.RenderControl(htWriter);
            
                message = sWriter.ToString();
            
                message = message.Replace("class=\"themecol1\"", "style=\"background: #1c498a; height: 24px; line-height: 22px; font-size: 12px;color: #fff; padding-left: 10px;\"");
                try
                {
                   // CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], txtEmailId.Text.Trim(), "Sightseeing Voucher", message);
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], txtEmailId.Text.Trim(), toArray, "Sightseeing Voucher", message, table);
                }

                catch (System.Net.Mail.SmtpException ex)
                {
                    throw ex;
                }
                finally
                {
                    btnEmail.Visible = true;
                    btnPrint.Visible = true;
                }
            
            #endregion

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.Normal, 0, "Failed to send SightSeeing Voucher to " + txtEmailId.Text + " Reason " + ex.Message, Request["REMOTE_ADDR"]);
        }
    }

}
