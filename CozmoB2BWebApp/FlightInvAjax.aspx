﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="FlightInvAjax" Codebehind="FlightInvAjax.aspx.cs" %>


 <%if (Request["requestSource"] != null && Request["requestSource"] == "getFareCabinBookingHtml"){ %>
 <!-- The below html is for airline,isLCC,Cabin Class,FareBasis and Booking Class -->
<div class="paramcon" id="fareBooking<%=ctrlId%>">
                <div class=" paddingbot_10 paddingtop_10">
                    <div class="col-md-10">
                        <div class="col-md-2">
                            <table>
                                <tbody><tr>
                                    <td>
                                        Airline:<span class="fcol_red">*</span>
                                    </td>
                                    <td>
                                        <select onchange="verifyIsLCC('ddlAirLine' + <%=ctrlId%>)" id="ddlAirLine<%=ctrlId%>" class="form-control">
                                        <option selected="selected" value="Select">Select</option>
                                        </select>
                                    </td>
                                                                      
                                </tr>
                            </tbody></table>
                        </div>
                        
                        <div class="col-md-1">
                        <table>
                        <tr>
                        <td>
                        IsLcc :
                        </td>
                        <td>
                        <input disabled ="disabled" id="chkIsLCC<%=ctrlId%>" type="checkbox">
                        </td>
                        </tr>
                        </table>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <table>
                                <tbody><tr>
                                    <td>
                                        Cabin Class:<span class="fcol_red">*</span>
                                    </td>
                                    <td>
                                        <select id="ddlCabinClass<%=ctrlId%>" class="form-control">
	<option selected="selected" value="Select">Select</option>

</select>
                                    </td>
                                </tr>
                            </tbody></table>
                        </div>
                        <div class="col-md-3">
                            <table>
                                <tbody><tr>
                                    <td>
                                        Fare Basis:
                                    </td>
                                    <td>
                                    <input maxlength="15" onkeypress="return IsAlphaNumeric(event);" class="form-control" type="text" id="txtFareBasis<%=ctrlId%>" />                                                                          
                                    </td>
                                </tr>
                            </tbody></table>
                        </div>
                        <div class="col-md-3">
                            <table>
                                <tbody><tr>
                                    <td>
                                        Booking Class:
                                    </td>
                                    <td>
           <input maxlength="10" class="form-control" onkeypress="return IsAlphaNumeric(event);"  type="text" id="txtBookClass<%=ctrlId%>" />
                                    </td>
                                </tr>
                            </tbody></table>
                        </div>
                    </div>
                    <div class="col-md-2">
       <a class=" pull-right" onclick="javascript:RemoveFareCabinBookingDiv('<%=ctrlId%>');">Remove</a>
                        <div class="clearfix">
                        </div>
                    </div>
                    
                    <div class="clearfix">
                    </div>
                </div>
                <div class="clearfix">
                </div>
            </div>
   <%} %>
   

 
 <%if (Request["requestSource"] != null && Request["requestSource"] == "getSeatsPNRHtml")
   { %>
    <!--The below html is for Seats and PNR -->
 <div class="paramcon" id="seatsPNR<%=ctrlId%>">
                <div class=" paddingbot_10 paddingtop_10">
                    <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    PNR:<span class="fcol_red">*</span>
                                </td>
                                <td>
                                
                                
                                
 <input maxlength="50" onkeypress="return IsAlphaNumeric(event);"  type="text" ID="txtPNR<%=ctrlId%>"  class="form-control" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    No. of Seats(Available):<span class="fcol_red">*</span>
                                </td>
                                <td>
                                
                                <select id="ddlNoOfSeats<%=ctrlId%>" class="form-control">
	<option selected="selected" value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	<option value="6">6</option>
	<option value="7">7</option>
	<option value="8">8</option>
	<option value="9">9</option>
	

</select>
                                 
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                    
                    
                    <div class="col-md-3">
      <a class=" pull-right" onclick="javascript:RemoveSeatsPNRDiv('<%=ctrlId%>');">Remove </a>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                </div>
            </div>
<%} %>




<%if (Request["requestSource"] != null && Request["requestSource"] == "getSeatsPNRHtmlEditMode")
   { %>
    <!--The below html is for Seats and PNR -->
 <div class="paramcon" id="Div1">
                <div class=" paddingbot_10 paddingtop_10">
                    <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    PNR:<span class="fcol_red">*</span>
                                </td>
                                <td>
                                
                                
                                
 <input maxlength="50" onkeypress="return IsAlphaNumeric(event);"  type="text" ID="txtPNR<%=ctrlId%>"  class="form-control" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                    <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    No. of Seats(Available):<span class="fcol_red">*</span>
                                </td>
                                <td>
                                
                                <select id="ddlNoOfSeats<%=ctrlId%>" class="form-control">
	<option selected="selected" value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	<option value="6">6</option>
	<option value="7">7</option>
	<option value="8">8</option>
	<option value="9">9</option>
	

</select>
                                 
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                      <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    No. of Seats(Used):<span class="fcol_red">*</span>
                                </td>
                                <td>
                                
                                <select  id="ddlNoOfSeatsUsed<%=ctrlId%>" class="form-control">
     <option selected="selected" value="0">0</option>                           
	<option  value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	<option value="6">6</option>
	<option value="7">7</option>
	<option value="8">8</option>
	<option value="9">9</option>
	

</select>
                                 
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                   
                </div>
                <div class="clearfix">
                </div>
            </div>
<%} %>



