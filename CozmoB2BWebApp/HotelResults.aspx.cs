﻿using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Globalization;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.Configuration;
using CT.CMS;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using CT.TicketReceipt.Common;

public partial class HotelResults :CT.Core.ParentPage// System.Web.UI.Page
{
    //Local protected variables.
    #region Members
    protected HotelSearchResult[] hotelSearchResult = new HotelSearchResult[0];
    protected HotelRequest request = new HotelRequest();
    protected List<HotelDeal> allHotels = new List<HotelDeal>();
    protected bool ErrorFlag = false;
    protected string errorMessage = string.Empty;
    protected double rateofExchange = 1;
    protected Dictionary<string, decimal> rateOfEx = new Dictionary<string, decimal>();
    protected string symbol = string.Empty;
    protected string type = "";
    //filtering variales Hotel Name, Address or star Rating
    protected string hotelAddress = string.Empty;
    protected string hotelName = string.Empty;
    protected string hotelRating = "0";//Default Show All
    //order variales Hotel Price , Name or star Rating
    protected int orderBy = 0;//Default By Price Low To high
    //paging variables
    protected int recordsPerPage;// load it from config
    protected int noOfPages;
    protected int pageNo;
    protected string showPageLink = string.Empty;
    protected PriceType priceType;
    protected List<string> locations = new List<string>();
    protected System.Collections.Generic.List<string> topDestination = new List<string>();
    //For Amenities and Photos popup
    protected string lblFare = "", images = "";
    protected string cityName = "Enter city name";
    public string adults = "", childs = "", childAges = "";
    protected int startIndex = 0;
    protected int decimalValue =2;
    protected string location = string.Empty;  //location Filter purose Added by brahmam 
    protected AgentMaster agencydetails;//for itinerary downloading option Added by Harish
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["req"] == null || Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            else
            {
                //if (!IsPostBack )
                //{
                //    ViewState["tmpRowCount"] = 0;// testing prefmance
                //}
                
                Page.Title = "Hotel Search Results";
                //Get static Rate of Exchange values
                //CT.Core1.StaticData staticInfo = new CT.Core1.StaticData();
                //staticInfo.BaseCurrency = Settings.LoginInfo.Currency;
                //rateOfEx = staticInfo.CurrencyROE;
                //rateOfEx = Settings.LoginInfo.AgentExchangeRates;
                               
                //Retrieve Search Request
                request = Session["req"] as HotelRequest;
                if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    agencydetails = new AgentMaster(Settings.LoginInfo.AgentId);
                }
                else
                {
                    agencydetails = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                }
                SetModifySearchValues();

                //////////////////////////////////////////////////////////////////////////////////////////
                ////                              Modify Search called
                //////////////////////////////////////////////////////////////////////////////////////////
                //if (hdnSubmit.Value.Length > 0)
                //{
                //    if (hdnSubmit.Value == "search")
                //    {
                //        ChangeHotelSearch();
                //    }
                //}

                if (hdnSubmit.Value == "Book" && request.NoOfRooms == 1)
                {
                    Session["hCode"] = hotelCode.Value;
                    Session["rCode"] = roomCode.Value;
                    Session["DiscType"] = hdnDiscountType.Value;

                    Response.Redirect("HotelReview.aspx", true);
                }

                //if (Request["action"] != null && Request["action"] == "ChangeDetail")
                //{
                //    //Session["req"] = request;
                //    Response.Redirect("HotelSearch.aspx?change=true", false);
                //}
                // records per page from configuration
                recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["HotelSearchResultPerPage"]);

                //////////////////////////////////////////////////////////////////////////////////////////
                //                      Called when clicked on any page number
                //////////////////////////////////////////////////////////////////////////////////////////
                //get the page no if clicked on the page link otherwise 1st page.
                if (!string.IsNullOrEmpty(PageNoString.Value))
                {
                    pageNo = Convert.ToInt16(PageNoString.Value);
                    UpdateFilterPageNo();//update page no only to session
                    GetFilterStatus();//set the values in filter UI                   
                }
                else
                {
                    pageNo = 1;
                }                

               

                ///////////////////////////////////////////////////////////////////////////////////////////
                //                              Apply Filters
                ///////////////////////////////////////////////////////////////////////////////////////////
                if (ApplyFilter.Value == "true")
                {
                    ApplyFilters();
                }
                else if (ApplyFilter.Value == "false")
                {
                    ClearAllFilter();
                }
                if (IsPostBack || Session["SearchResults"] != null)
                {
                    GetDataFromDB();
                }

                if (request != null && (Change.Value == "false" && ApplyFilter.Value.Length == 0) && !IsPostBack)
                {
                    ///////////////////////////////////////////////////////////////////////////////////////
                    //                         Search called from Hotel Search Page
                    ///////////////////////////////////////////////////////////////////////////////////////
                    if (hdnSubmit.Value.Length <= 0)
                    {
                       // DateTime before = DateTime.Now;
                        SearchHotels();
                        //DateTime after = DateTime.Now;
                        //TimeSpan ts = after - before;

                        //Audit.Add(EventType.Search, Severity.Normal, 1, "LotsOfHotels Page Search returned in " + ts.Seconds + " Seconds", "1");
                        
                    }                    
                }
                else
                {
                    //errorMessage = "No Hotels found.Please" + "<a href=\"HotelSearch.aspx?change=true\"" + "\">" + " Retry" + "</a>.";
                    //ErrorFlag = true;
                }
            }
        }
        catch (Exception ex)
        {
            if (Settings.LoginInfo != null)
            {
                Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get Hotel Search Results. Error: " + ex.Message, Request["REMOTE_ADDR"]);
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }

    }

    protected void imgModifySearch_Click(object sender,EventArgs e)
    {
        //if (hdnSubmit.Value == "search")
        {
            ChangeHotelSearch();
            SetModifySearchValues();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    void SetModifySearchValues()
    {
        if (!IsPostBack)
        {
            if (ApplyFilter.Value != "true")
            {
                if (request.MinRating == 0 && request.MaxRating == 0)
                {
                    chkAll.Checked = true;
                    chkFive.Checked = true;
                    chkFour.Checked = true;
                    chkThree.Checked = true;
                    chkTwo.Checked = true;
                    chkOne.Checked = true;
                    hdnRating.Value = "0";
                }
                else if (request.MinRating == 0 && request.MaxRating == 5)
                {
                    chkAll.Checked = true;
                    chkFive.Checked = true;
                    chkFour.Checked = true;
                    chkThree.Checked = true;
                    chkTwo.Checked = true;
                    chkOne.Checked = true;
                    hdnRating.Value = "0";
                }
                else if (request.MinRating == 0 && request.MaxRating == 4)
                {
                    chkAll.Checked = false;
                    chkFive.Checked = false;
                    chkFour.Checked = true;
                    chkThree.Checked = true;
                    chkTwo.Checked = true;
                    chkOne.Checked = true;
                    hdnRating.Value = "4";
                }
                else if (request.MinRating == 0 && request.MaxRating == 3)
                {
                    chkAll.Checked = false;
                    chkFive.Checked = false;
                    chkFour.Checked = false;
                    chkThree.Checked = true;
                    chkTwo.Checked = true;
                    chkOne.Checked = true;
                    hdnRating.Value = "3";
                }
                else if (request.MinRating == 0 && request.MaxRating == 2)
                {
                    chkAll.Checked = false;
                    chkFive.Checked = false;
                    chkFour.Checked = false;
                    chkThree.Checked = false;
                    chkTwo.Checked = true;
                    chkOne.Checked = true;
                    hdnRating.Value = "2";
                }
                else if (request.MinRating == 0 && request.MaxRating == 1)
                {
                    chkAll.Checked = false;
                    chkFive.Checked = false;
                    chkFour.Checked = false;
                    chkThree.Checked = false;
                    chkTwo.Checked = false;
                    chkOne.Checked = true;
                    hdnRating.Value = "1";
                }
            }

            /////////////////////////////////////////////////////////////////////////////////////////////
            //          Set the searched parameters to the Hotel Modify search controls
            /////////////////////////////////////////////////////////////////////////////////////////////
            try
            {

                if (request != null)
                {
                    if (request.CityName != null)
                    {
                        cityName = Session["city"].ToString();
                        //try
                        //{
                        //    //ddlCity.SelectedIndex = -1;
                        //    //ddlCity.Items.FindByText(request.CityName).Selected = true;
                        //    foreach (ListItem item in ddlCity.Items)
                        //    {
                        //        if (item.Value.Split(',')[0] == request.CityName)
                        //        {
                        //            ddlCity.SelectedIndex = -1;
                        //            item.Selected = true;
                        //            break;
                        //        }
                        //    }

                        //}
                        //catch { ddlCity.SelectedIndex = 0; }
                    }
                    if (request.StartDate != null && request.StartDate != DateTime.MinValue)
                    {
                        CheckIn.Text = request.StartDate.ToString("dd/MM/yyyy");
                    }
                    if (request.EndDate != null && request.EndDate != DateTime.MinValue)
                    {
                        CheckOut.Text = request.EndDate.ToString("dd/MM/yyyy");
                    }
                    if (request.HotelName != null)
                    {
                        //Request.Form["HotelName"] = request.HotelName;
                        //request.HotelName=Request.Form["HotelName"];
                        txtHotel.Text = request.HotelName;
                    }
                    
                    for (int i = 0; i < request.NoOfRooms; i++)
                    {
                        if (adults.Length > 0)
                        {
                            adults += "," + (i + 1) + "-" + request.RoomGuest[i].noOfAdults;
                        }
                        else
                        {
                            adults = (i + 1) + "-" + request.RoomGuest[i].noOfAdults;
                        }
                        if (childs.Length > 0)
                        {
                            childs += "," + (i + 1) + "-" + request.RoomGuest[i].noOfChild;
                        }
                        else
                        {
                            childs = (i + 1) + "-" + request.RoomGuest[i].noOfChild;
                        }
                        if (request.RoomGuest[i].childAge != null)
                        {
                            foreach (int age in request.RoomGuest[i].childAge)
                            {
                                if (childAges.Length > 0)
                                {
                                    childAges += "," + age.ToString();
                                }
                                else
                                {
                                    childAges = age.ToString();
                                }
                            }
                        }
                    }
                    hdnAdults.Value = adults;
                    hdnChilds.Value = childs;
                    hdnChildAges.Value = childAges;

                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request.ServerVariables["REMOTE_ADDR"]);
            }
        }
    }


    /// <summary>
    /// This section of code will be called when any filters are applied like Rating, Hotel Name search and Price sorting
    /// </summary>
    void ApplyFilters()
    {

        if (Change.Value == "true" || ApplyFilter.Value.Length > 0)
        {
            if (Change.Value == "true")
            {
                //get the filter criteria from the session
                GetFilterStatus();
                //GetDataFromDB(); 
            }
            if (ApplyFilter.Value == "true")
            {
                // set the filter criteria and put into session
                SetFilterStatus();
                //GetDataFromDB();
            }
            else if (ApplyFilter.Value == "false")
            {
                //clear all filter criteria in the UI page & ask 1st page result
                ClearAllFilter();
                //GetDataFromDB();
            }

            if (hotelSearchResult.Length == 0)
            {
                //no need to message for no result found instead ask to change the filter
                //only for the selected filter there is no result other wise search available
                ErrorFlag = false;
            }
            
        }
    }

    /// <summary>
    /// This method will be used when Searched hotels internally
    /// </summary>
    private void ChangeHotelSearch()
    {
        RoomGuestData[] guestInfo = new RoomGuestData[0];
        //Clear previous session
        Session["cSessionId"] = null;

        HotelRequest reqObj = Session["req"] as HotelRequest;
        Session["memberId"] = 1;
        //gets the city details.
        string cityDetail = string.Empty;
        if (Request["dest_type"] == "domestic")
        {
            reqObj.IsDomestic = true;
            type = "Dom";
        }
        else
        {
            reqObj.IsDomestic = false;
            type = "Intl";
            //if (Request["isMultiIntl"] == "true")
            //{
            //    reqObj.IsMultiRoom = true;
            //}
            //else
            //{
            //    reqObj.IsMultiRoom = false;
            //}
        }

        if (Request["searchByArea"] == "on")
        {
            reqObj.SearchByArea = true;
        }
        else
        {
            reqObj.SearchByArea = false;
        }
        HotelSource sourceInfo = new HotelSource();
        List<string> sourceList = new List<string>();
        List<string> sources = new List<string>();
        //stores only city Name.
        //string city;
        if (reqObj.IsDomestic)
        {
            sourceList = sourceInfo.Load(HotelSourceGeographyType.Domestic);

            if (Request["destination"] != null && Request["destination"].Length > 0)
            {
                cityDetail = Request["destination"].ToUpper();
                cityDetail = cityDetail.Trim();
                string[] cityArray = cityDetail.Split(',');

                reqObj.CityName = cityArray[0];
                reqObj.CityId = HotelCity.GetCityIdFromCityName(reqObj.CityName);
            }
            #region old code for reference
            //else if (Request["destination1"] != null && Request["destination1"].Length > 0)
            //{
            //    cityDetail = Request["destination1"].ToUpper();
            //    cityDetail = cityDetail.Trim();
            //    string[] cityArray = cityDetail.Split(',');
            //    if (cityArray.Length > 1)
            //    {
            //        city = cityArray[0];
            //        //reqObj.CityCode = cityArray[0];
            //        reqObj.CityId = Convert.ToInt32(cityArray[0]);
            //        reqObj.CityName = cityArray[1];
            //    }
            //}
            /*string[] cityArray = cityDetail.Split(',');
            if (cityArray.Length > 1)
            {
                city = cityArray[0];
                //reqObj.CityCode = cityArray[0];
                reqObj.CityId = Convert.ToInt32(cityArray[0]);
                string[] cityNme1 = cityArray[1].Split('(');
                string[] cityNme2 = cityArray[1].Split('-');
                if (cityNme1.Length > 1)
                {
                    reqObj.CityName = cityNme1[0];
                }
                else if (cityNme2.Length > 1)
                {
                    reqObj.CityName = cityNme2[0];
                }
                else
                {
                    reqObj.CityName = cityArray[1];
                }
                       
            }*/
            //else
            //{
            //reqObj.CityName = cityArray[0];
            //GTACity gtaCityInfo = new GTACity();
            //reqObj.CityCode = gtaCityInfo.GetCityCode(reqObj.CityName, "India");
            //}
            //reqObj.CityId = HotelCity.GetCityIdFromCityName(reqObj.CityName);
            #endregion
            reqObj.CountryName = "India";
            Session["city"] = cityDetail;
        }
        else
        {
            sourceList = sourceInfo.Load(HotelSourceGeographyType.International);
            if (!string.IsNullOrEmpty(Request.Form["cityCode"]))
            {
                //reqObj.CityId = HotelCity.GetCityIdFromCityName(Request.Form["City"].Split(',')[0], Request.Form["City"].Split(',')[1].Trim());
                reqObj.CityId = Convert.ToInt32(Request.Form["cityCode"]);
            }
            //if (Request.Form["City"].Split(',').Length == 3)
            //{
            //    reqObj.CityId = HotelCity.GetCityIdFromCityName(Request.Form["City"].Split(',')[0], Request.Form["City"].Split(',')[1].Trim());
            //}
            //else
            //{
            //    reqObj.CityId = HotelCity.GetCityIdFromCityName(Request.Form["City"].Split(',')[0]);
            //}
            //Session["city"] = ddlCity.SelectedValue;
            reqObj.CityName = Request.Form["City"].Split(',')[0];
            Session["city"] = Request.Form["City"];
        }
        
        //if (Convert.ToBoolean(Session["isB2B2BAgent"]) && sources.Contains("IAN"))
        //{
        //    sources.Remove("IAN");
        //}
        //reqObj.Sources = sources;
        //stores the details.
        string sDate = CheckIn.Text;//Request["CheckIn"];
        string eDate = CheckOut.Text;//Request["CheckOut"];

        IFormatProvider format = new CultureInfo("en-GB", true);

        reqObj.StartDate = DateTime.Parse(sDate, format);
        reqObj.EndDate = DateTime.Parse(eDate, format);

        switch (Convert.ToInt16(Request["rating"]))
        {
            case 0:
                reqObj.MinRating = 0;
                reqObj.MaxRating = 5;
                break;
            case 1:
                reqObj.MinRating = 0;
                reqObj.MaxRating = 1;
                break;
            case 2:
                reqObj.MinRating = 0;
                reqObj.MaxRating = 2;
                break;
            case 3:
                reqObj.MinRating = 0;
                reqObj.MaxRating = 3;
                break;
            case 4:
                reqObj.MinRating = 0;
                reqObj.MaxRating = 4;
                break;
            case 5:
                reqObj.MinRating = 0;
                reqObj.MaxRating = 5;
                break;
            case 6: reqObj.MinRating = 1;
                reqObj.MaxRating = 5;
                break;
            case 7: reqObj.MinRating = 2;
                reqObj.MaxRating = 5;
                break;
            case 8: reqObj.MinRating = 3;
                reqObj.MaxRating = 5;
                break;
            case 9: reqObj.MinRating = 4;
                reqObj.MaxRating = 5;
                break;
            case 10: reqObj.MinRating = 5;
                reqObj.MaxRating = 5;
                break;
        }

        //reqObj.CityName = ddlCity.SelectedValue.Split(',')[0];
        reqObj.HotelName = txtHotel.Text;
        
        //Session["BookingAgencyID"] = Request["BookingAgencyID"];
        //Session["AgentName"] = Request["AgentId"]; ;
        if (reqObj.StartDate.Equals(reqObj.EndDate))
        {
            errorMessage = "The Checkin CheckOut should not be same!";
            // Response.Redirect("HotelSearch.aspx?change=true");
            return;
        }
        reqObj.HotelName = Request["HotelName"];

        string rooms = Request["NoOfRooms"];
        reqObj.NoOfRooms = Convert.ToInt16(rooms);
        if (reqObj.NoOfRooms > 1)
        {
            reqObj.IsMultiRoom = true;
        }

        guestInfo = new RoomGuestData[reqObj.NoOfRooms];

        for (int i = 0; i < reqObj.NoOfRooms; i++)
        {
            //Checks whether Number of children more than 1.
            string request = "chdRoom-" + (i + 1);
            if (Request[request] != "none" && Convert.ToInt16(Request[request]) > 0)
            {
                guestInfo[i].noOfChild = Convert.ToInt16(Request[request]);
                List<int> childInfo = new List<int>();
                string numChild = string.Empty; ;
                for (int j = 1; j <= guestInfo[i].noOfChild; j++)
                {
                    numChild = "ChildBlock-" + (i + 1) + "-ChildAge-" + j;
                    childInfo.Add(Convert.ToInt16(Request[numChild]));
                }
                guestInfo[i].childAge = childInfo;
            }
            else
            {
                guestInfo[i].noOfChild = 0;
            }
            string adultStr = "adtRoom-" + (i + 1);
            guestInfo[i].noOfAdults = Convert.ToInt16(Request[adultStr]);
        }
        reqObj.RoomGuest = guestInfo;

        Session["req"] = reqObj;
        hdnSubmit.Value = "";
        this.SearchHotels();

    }


    private void SearchHotels()
    {
        UserMaster member = new UserMaster(Convert.ToInt32(Session["memberId"]));
        try
        {
            HotelDeal HotelData = new HotelDeal();
            allHotels = HotelData.GetHotelDealByCity(request.CityName);
            //load the member info for the current session

            //Get the hotel results from MSE            
            MetaSearchEngine mse = new MetaSearchEngine();
            LocationMaster locationMaster = new LocationMaster();
            int agencyId = 0;
            if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agencyId = Convert.ToInt32(Settings.LoginInfo.AgentId);
                decimalValue = Settings.LoginInfo.DecimalValue;
                request.LoginCountryCode = Settings.LoginInfo.LocationCountryCode;
            }
            else
            {
                agencyId = Settings.LoginInfo.OnBehalfAgentID; //Selected Agency
                decimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                locationMaster = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                request.LoginCountryCode = locationMaster.CountryCode;
            }
            
            try
            {
                request.CountryCode = Country.GetCountryCodeFromCountryName(request.CountryName);
            }
            catch { }
            //agent = new AgentMaster(agencyId);
            //CT.Core1.StaticData sd = new CT.Core1.StaticData();
            //sd.BaseCurrency = agent.AgentCurrency;
            //mse.RateOfExchange = sd.CurrencyROE;
            //mse.AgentBaseCurrency = agent.AgentCurrency;
            mse.SettingsLoginInfo = Settings.LoginInfo;
            hotelSearchResult = mse.GetHotelResults(request, Convert.ToInt64(agencyId));
            if (hotelSearchResult.Length > 0)
            {
                Session["cSessionId"] = mse.SessionId;
            }

            if (hotelSearchResult.Length == 0)
            {
                errorMessage = "<p>Sorry, we could not found the hotel you have requested but we would like to provide the best option for the same destination. To avail a better and cheaper option please call us at " + Settings.LoginInfo.AgentPhone + " or email us at <a href='mailto:" + Settings.LoginInfo.AgentEmail + "'>" + Settings.LoginInfo.AgentEmail + "</a></p>" + "</br><p>Go to <a href=\"HotelSearch.aspx?source=Hotel\"" + "\">" + "Search Page" + "</a></p>.";
                ErrorFlag = true;
            }


            if (hotelSearchResult != null)
            {
                Session["ResultCount"] = hotelSearchResult.Length;
                Session["SearchResults"] = hotelSearchResult;
            }
            else
            {
                if (Session["ResultCount"] == null)
                {
                    Session["ResultCount"] = 0;
                }
            }
            //location Filter purose Added by brahmam 24.10.2016
            //Dotw and WST at the time only we are filtering location oather source filtering Address
            //Dotw and WST  at the time only binding dropdwon oather wise textBox
            if (request != null && request.Sources.Contains("DOTW") || request.Sources.Contains("WST"))
            {
                ddlLocation.Visible = true;
                List<string> hotelLocations = new List<string>();
                foreach (HotelSearchResult result in hotelSearchResult)
                {
                    if (result.BookingSource == HotelBookingSource.DOTW || result.BookingSource == HotelBookingSource.WST)
                    {
                        if (!string.IsNullOrEmpty(result.HotelLocation) && !hotelLocations.Contains(result.HotelLocation))
                        {
                            hotelLocations.Add(result.HotelLocation);
                        }
                    }
                }
                //sorting by location name
                hotelLocations.Sort(delegate(string x, string y)
                {
                    if (x == null && y == null) return 0;
                    else if (x == null) return -1;
                    else if (y == null) return 1;
                    else return x.CompareTo(y);
                });
                //Binding All locations
                ddlLocation.DataSource = hotelLocations;
                ddlLocation.DataBind();
            }
            else
            {
                txtLocation.Visible = true;
            }
            Session["req"] = request;

            bool samePaxCount = false;
            int adults = request.RoomGuest[0].noOfAdults;
            foreach (RoomGuestData room in request.RoomGuest)
            {
                if (room.noOfAdults == adults)
                {
                    samePaxCount = true;
                }
            }
            ViewState["SamePax"] = samePaxCount;



                #region Old Sorting Code


            //Get the Markup for the Agent once for All Sources searched
            //Dictionary<string, DataTable> agentMarkups = new Dictionary<string, DataTable>();
            //foreach (string source in request.Sources)
            //{
            //    DataTable dtMarkup = UpdateMarkup.Load(agencyId, source, (int)ProductType.Hotel);
            //    if (dtMarkup != null )
            //    {
            //        agentMarkups.Add(source, dtMarkup);
            //    }
            //}

            //if (hotelSearchResult != null && hotelSearchResult.Length > 0)
            //{
                // Update price
        //        for (int i = 0; i < hotelSearchResult.Length; i++)
        //        {
        //            if (hotelSearchResult[i] != null && hotelSearchResult[i].BookingSource != HotelBookingSource.IAN && hotelSearchResult[i].BookingSource != HotelBookingSource.TBOConnect)
        //            {
        //                if (hotelSearchResult[i].BookingSource == HotelBookingSource.HotelConnect)
        //                {
        //                    //Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "HotelConnect Search Result in page : " + hotelSearchResult[i].HotelName, "1");
        //                }
        //                //Get Hotel source commission type
        //                HotelSource hSource = new HotelSource();
        //                hSource.Load(hotelSearchResult[i].BookingSource.ToString());

        //                //since there are two enums of similar type where one is in BookingEngine(PriceAccounts) & other in CoreLogic(Enumerator).
        //                if (hSource.FareTypeId == FareType.Net)
        //                {
        //                    priceType = PriceType.NetFare;
        //                }
        //                else
        //                {
        //                    priceType = PriceType.PublishedFare;
        //                }
        //                for (int count = 0; count < hotelSearchResult[i].RoomDetails.Length; count++)
        //                {
        //                    System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
        //                    int nights = DiffResult.Days;
        //                    decimal totalSellingFare = 0;
        //                    if (rateOfEx.ContainsKey(hotelSearchResult[i].Currency) && hotelSearchResult[i].Currency != null)
        //                    {
        //                        rateofExchange = Convert.ToDouble(rateOfEx[hotelSearchResult[i].Currency]);
        //                    }
        //                    if (hotelSearchResult[i].BookingSource == HotelBookingSource.HotelBeds || hotelSearchResult[i].BookingSource == HotelBookingSource.IAN)
        //                    {
        //                        hotelSearchResult[i].RoomDetails[count].Amenities = new List<string>();
        //                    }
        //                    PriceAccounts tempEGPrice = new PriceAccounts();
        //                    // Extra Guest Charge offered (selling) rate calculation
        //                    //if (hotelSearchResult[i].BookingSource == HotelBookingSource.Desiya || hotelSearchResult[i].BookingSource == HotelBookingSource.IAN || hotelSearchResult[i].BookingSource == HotelBookingSource.TBOConnect)
        //                    //{
        //                    //    //since desiya is providing the extra guest charges per night so send night = 1
        //                    //    tempEGPrice = AccountingEngine.GetPrice(hotelSearchResult[i], count, agencyId, 0, -1, request.NoOfRooms, nights, priceType);
        //                    //}
        //                    //else if (hotelSearchResult[i].BookingSource == HotelBookingSource.Desiya)
        //                    //{
        //                    //    //price for only sigle room so noOfRooms==1
        //                    //    tempEGPrice = AccountingEngine.GetPrice(hotelSearchResult[i], count, agencyId, 0, -1, 1, nights, priceType);
        //                    //    hotelSearchResult[i].Price = tempEGPrice;

        //                    //    if (priceType == PriceType.PublishedFare)
        //                    //    {
        //                    //        hotelSearchResult[i].RoomDetails[count].SellExtraGuestCharges = tempEGPrice.PublishedFare - tempEGPrice.AgentCommission - tempEGPrice.AgentPLB;
        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        hotelSearchResult[i].RoomDetails[count].SellExtraGuestCharges = (hSource.CommissionTypeId == CommissionType.Percentage) ? (tempEGPrice.NetFare + tempEGPrice.Markup) : tempEGPrice.NetFare;
        //                    //    }
        //                    //}
        //                    if (hotelSearchResult[i].Price == null)
        //                    {
        //                        hotelSearchResult[i].Price = tempEGPrice;
        //                    }
        //                    if (hotelSearchResult[i].RoomDetails != null && hotelSearchResult[i].RoomDetails[count].Rates != null)
        //                    {
        //                        for (int j = 0; j < hotelSearchResult[i].RoomDetails[count].Rates.Length; j++)
        //                        {
        //                            PriceAccounts tempPrice = new PriceAccounts();

        //                            decimal markup = 0;
        //                            decimal discount = 0;
        //                            string markupType = string.Empty; //  Commission Type Id, either percentage/Fixed    
        //                            string discountType = string.Empty; // Fare Type Id, either Net/Published 

        //                            if (hotelSearchResult[i].BookingSource == HotelBookingSource.DOTW )
        //                            {
        //                                DataTable dtMarkup = agentMarkups[hotelSearchResult[i].BookingSource.ToString()];
        //                                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
        //                                {
        //                                    DataRow dr = dtMarkup.Rows[0];
        //                                    markup = Convert.ToDecimal(dr["Markup"]);
        //                                    discount = Convert.ToDecimal(dr["Discount"]);
        //                                    markupType = Convert.ToString(dr["MarkupType"]); // Percentage, fixed
        //                                    discountType = Convert.ToString(dr["DiscountType"]);// Percentage, fixed
        //                                }
        //                                tempPrice.MarkupType = markupType; 
        //                                tempPrice.MarkupValue = markup;
        //                                tempPrice.DiscountType = discountType;
        //                                tempPrice.DiscountValue = discount;
        //                                //price for only sigle room so noOfRooms==1
        //                                tempPrice.Markup = (markupType == "F" ? markup : Convert.ToDecimal(hotelSearchResult[i].RoomDetails[count].TotalPrice) * (markup / 100));//clari
        //                                // tempPrice.Markup = (markupType == "F" ? markup * request.NoOfRooms : (Convert.ToDecimal(hotelSearchResult[i].RoomDetails[count].Rates[j].Amount / request.NoOfRooms) * (markup / 100)) * request.NoOfRooms);
        //                                tempPrice.NetFare = Convert.ToDecimal(hotelSearchResult[i].RoomDetails[count].TotalPrice);
        //                                tempPrice.Discount = (discountType == "F" ? discount : discount / 100);

        //                                hDOTWDiscount.Value = discount.ToString();
        //                                hDOTWDiscountType.Value = discountType;
        //                            }
        //                            else if (hotelSearchResult[i].BookingSource == HotelBookingSource.RezLive)
        //                            {
        //                                try
        //                                {
        //                                    DataTable dtMarkup = agentMarkups[hotelSearchResult[i].BookingSource.ToString()];
        //                                    if (dtMarkup != null && dtMarkup.Rows.Count > 0)
        //                                    {
        //                                        DataRow dr = dtMarkup.Rows[0];
        //                                        markup = Convert.ToDecimal(dr["Markup"]);
        //                                        discount = Convert.ToDecimal(dr["Discount"]);
        //                                        markupType = Convert.ToString(dr["MarkupType"]); // Percentage, fixed
        //                                        discountType = Convert.ToString(dr["DiscountType"]);// Percentage, fixed
        //                                    }
        //                                }
        //                                catch (Exception ex)
        //                                {

        //                                }
        //                                tempPrice.MarkupType = markupType;
        //                                tempPrice.MarkupValue = markup;
        //                                tempPrice.DiscountType = discountType;
        //                                tempPrice.DiscountValue = discount;
        //                                //For Rezlive Whole amount is given
        //                                tempPrice.Markup = (markupType == "F" ? markup * request.NoOfRooms : (Convert.ToDecimal(hotelSearchResult[i].RoomDetails[count].Rates[j].Amount / request.NoOfRooms) * (markup / 100)) * request.NoOfRooms);
        //                                tempPrice.NetFare = Convert.ToDecimal(hotelSearchResult[i].RoomDetails[count].TotalPrice);
        //                                tempPrice.Discount = (discountType == "F" ? discount : Convert.ToDecimal(tempPrice.NetFare + tempPrice.Markup) * (discount / 100));

        //                                hRezDiscount.Value = discount.ToString();
        //                                hRezDiscountType.Value = discountType;
        //                            }
        //                            else if (hotelSearchResult[i].BookingSource == HotelBookingSource.LOH)
        //                            {
        //                                try
        //                                {
        //                                    DataTable dtMarkup = agentMarkups[hotelSearchResult[i].BookingSource.ToString()];
        //                                    if (dtMarkup != null && dtMarkup.Rows.Count > 0)
        //                                    {
        //                                        DataRow dr = dtMarkup.Rows[0];
        //                                        markup = Convert.ToDecimal(dr["Markup"]);
        //                                        discount = Convert.ToDecimal(dr["Discount"]);
        //                                        markupType = Convert.ToString(dr["MarkupType"]); // Percentage, fixed
        //                                        discountType = Convert.ToString(dr["DiscountType"]);// Percentage, fixed
        //                                    }
        //                                }
        //                                catch (Exception ex)
        //                                {

        //                                }
        //                                tempPrice.MarkupType = markupType;
        //                                tempPrice.MarkupValue = markup;
        //                                tempPrice.DiscountType = discountType;
        //                                tempPrice.DiscountValue = discount;
        //                                //For LOH Whole amount is given
        //                                tempPrice.Markup = (markupType == "F" ? markup : (Convert.ToDecimal(hotelSearchResult[i].RoomDetails[count].SellingFare) * (markup / 100)));
        //                                tempPrice.NetFare = Convert.ToDecimal(hotelSearchResult[i].RoomDetails[count].TotalPrice);

        //                                tempPrice.Discount = (discountType == "F" ? discount : discount / 100); //(Convert.ToDecimal(hotelSearchResult[i].TotalPrice + (tempPrice.Markup * request.NoOfRooms))) * (discount / 100));

        //                                //tempPrice.Discount = Math.Round(tempPrice.Discount, 2);
        //                                hLOHDiscount.Value = discount.ToString();
        //                                hLOHDiscountType.Value = discountType;
        //                            }

        //                            //if (hotelSearchResult[i].BookingSource == HotelBookingSource.Desiya || hotelSearchResult[i].BookingSource == HotelBookingSource.IAN || hotelSearchResult[i].BookingSource == HotelBookingSource.TBOConnect)
        //                            //{
        //                            //    tempPrice = AccountingEngine.GetPrice(hotelSearchResult[i], count, agencyId, 0, j, request.NoOfRooms, 1, priceType);
        //                            //}
        //                            //else if (hotelSearchResult[i].BookingSource == HotelBookingSource.DOTW)
        //                            //{
        //                            //    //price for only sigle room so noOfRooms==1
        //                            //    tempPrice = AccountingEngine.GetPrice(hotelSearchResult[i], count, agencyId, 0, j, request.NoOfRooms, 1, priceType);
        //                            //}
        //                            //else if (hotelSearchResult[i].BookingSource == HotelBookingSource.RezLive)
        //                            //{
        //                            //    tempPrice = AccountingEngine.GetPrice(hotelSearchResult[i], count, agencyId, 0, j, request.NoOfRooms, 1, priceType);
        //                            //}


        //                            if (hotelSearchResult[i].BookingSource != HotelBookingSource.HotelConnect)
        //                            {
        //                                if (priceType == PriceType.PublishedFare)
        //                                {
        //                                    hotelSearchResult[i].RoomDetails[count].Rates[j].SellingFare = (tempPrice.PublishedFare - tempPrice.AgentCommission - tempPrice.AgentPLB - tempPrice.Discount);
        //                                    hotelSearchResult[i].RateType = RateType.Published;
        //                                    hotelSearchResult[i].TotalPrice += tempPrice.PublishedFare;
        //                                    hotelSearchResult[i].Price.Discount = tempPrice.Discount;
        //                                }
        //                                else
        //                                {
        //                                    if (hotelSearchResult[i].BookingSource == HotelBookingSource.DOTW)
        //                                    {
        //                                        hotelSearchResult[i].RoomDetails[count].Rates[j].SellingFare = ((tempPrice.NetFare)) / hotelSearchResult[i].RoomDetails[count].Rates.Length;
        //                                    }
        //                                    else if (hotelSearchResult[i].BookingSource != HotelBookingSource.DOTW)
        //                                    {
        //                                        hotelSearchResult[i].RoomDetails[count].Rates[j].SellingFare = (tempPrice.NetFare);
        //                                    }
        //                                    hotelSearchResult[i].RateType = RateType.Negotiated;
        //                                    //hotelSearchResult[i].TotalPrice += tempPrice.NetFare;
        //                                    //hotelSearchResult[i].Price.Markup = tempPrice.Markup;
        //                                    //hotelSearchResult[i].Price.Discount = tempPrice.Discount;
        //                                    hotelSearchResult[i].RoomDetails[count].DiscountType = discountType;
        //                                    hotelSearchResult[i].RoomDetails[count].Discount = tempPrice.Discount;
        //                                    hotelSearchResult[i].RoomDetails[count].Markup = tempPrice.Markup;
        //                                    hotelSearchResult[i].RoomDetails[count].MarkupType = tempPrice.MarkupType;
        //                                    hotelSearchResult[i].RoomDetails[count].MarkupValue = tempPrice.MarkupValue;
        //                                    hotelSearchResult[i].RoomDetails[count].DiscountType = tempPrice.DiscountType;
        //                                    hotelSearchResult[i].RoomDetails[count].DiscountValue = tempPrice.DiscountValue;
        //                                }
        //                            }
        //                            if (hotelSearchResult[i].BookingSource == HotelBookingSource.DOTW)
        //                            {
        //                                totalSellingFare += hotelSearchResult[i].RoomDetails[count].Rates[j].SellingFare;
        //                            }
        //                            else if (hotelSearchResult[i].BookingSource != HotelBookingSource.DOTW)
        //                            {
        //                                totalSellingFare = hotelSearchResult[i].RoomDetails[count].Rates[j].SellingFare;
        //                            }
        //                        }
        //                    }
        //                    //if (hotelSearchResult[i].BookingSource == HotelBookingSource.Travco)
        //                    //{
        //                    //    HotelSource hotelS = new HotelSource();
        //                    //    hotelS.Load("Travco", agencyId);
        //                    //    decimal ourCommission = hotelS.OurCommission;
        //                    //    int commissionType = 0;
        //                    //    commissionType = (int)hotelS.CommissionTypeId;

        //                    //    if (hotelS.FareTypeId == FareType.Net)
        //                    //    {
        //                    //        if (commissionType == (int)CommissionType.Percentage)
        //                    //        {
        //                    //            hotelSearchResult[i].RoomDetails[count].Discount += Convert.ToDecimal(ourCommission) * hotelSearchResult[i].RoomDetails[count].Discount / 100;
        //                    //        }
        //                    //        if (commissionType == (int)CommissionType.Fixed)
        //                    //        {
        //                    //            if (hotelSearchResult[i].RoomDetails[count].Discount > 0)
        //                    //            {
        //                    //                hotelSearchResult[i].RoomDetails[count].Discount += Convert.ToDecimal(Convert.ToDouble(ourCommission) / rateofExchange);
        //                    //            }
        //                    //        }
        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        if (commissionType == (int)CommissionType.Percentage)
        //                    //        {
        //                    //            hotelSearchResult[i].RoomDetails[count].Discount += Convert.ToDecimal(ourCommission) * hotelSearchResult[i].RoomDetails[count].Discount / 100;
        //                    //        }
        //                    //        if (commissionType == (int)CommissionType.Fixed)
        //                    //        {
        //                    //            if (hotelSearchResult[i].RoomDetails[count].Discount > 0)
        //                    //            {
        //                    //                hotelSearchResult[i].RoomDetails[count].Discount += Convert.ToDecimal(Convert.ToDouble(ourCommission) / rateofExchange);
        //                    //            }
        //                    //        }
        //                    //    }
        //                    //}

        //                    hotelSearchResult[i].RoomDetails[count].SellingFare = totalSellingFare;
        //                }
        //                if (hotelSearchResult[i].BookingSource == HotelBookingSource.Desiya)
        //                {
        //                    if (!locations.Contains(hotelSearchResult[i].HotelLocation))
        //                    {
        //                        locations.Add(hotelSearchResult[i].HotelLocation);
        //                    }
        //                }

        //                if (hotelSearchResult[i].Price == null)
        //                {
        //                    hotelSearchResult[i].Price = new PriceAccounts();
        //                    if (hotelSearchResult[i].RateType == RateType.Negotiated)
        //                    {
        //                        hotelSearchResult[i].Price.NetFare = hotelSearchResult[i].SellingFare;
        //                    }
        //                    else
        //                    {
        //                        hotelSearchResult[i].Price.PublishedFare = hotelSearchResult[i].TotalPrice;
        //                    }
        //                }
        //                hotelSearchResult[i].Price.AccPriceType = priceType;

        //    }
        //}
#endregion
                Session.Add("locations", locations);
                Session["SearchResults"] = hotelSearchResult;
                #region Send mail

        AgentMaster agency = new AgentMaster(agencyId);

                string fromEmail = ConfigurationManager.AppSettings["fromEmail"].ToString();
                string toEmail = ConfigurationManager.AppSettings["hotelToEmail"].ToString(); ;
                // TODO: send stack trace also in the mail
                string messageText = "Dear  CozmoOnline,\n\n " + "We made a Hotel Request for the following Details.\n\n" + "City Name:" + request.CityName + "\nCheck In :" + request.StartDate.ToString("dd MMM yy") + "\nCheck Out :" + request.EndDate.ToString("dd MMM yy") + "\n No.of Rooms:" + request.NoOfRooms + "\n \nPassenger Information: ";
                for (int k = 0; k < request.RoomGuest.Length; k++)
                {
                    messageText += "\nRoom " + (k + 1) + ":";
                    messageText += " Adults :" + request.RoomGuest[k].noOfAdults;
                    messageText += "  Children:" + request.RoomGuest[k].noOfChild;
                }
                messageText += "\n\n\nThanking you,\n";
                messageText += agency.Name;
                messageText += "\n" + agency.City;
                messageText += "\nPhone No: " + agency.Phone1;
                if (agency.Phone2 != null && agency.Phone2.Length > 0)
                {
                    messageText += "\n Mobile:" + agency.Phone2;
                }
                //if (Session["agencyId"] != null && (int)Session["agencyId"] > 0)
                //{
                //    try
                //    {
                //        //Email.Send(fromEmail, toEmail, "Hotel Request" + (ConfigurationManager.AppSettings["TestMode"].Equals("True")?"(Test)":""), messageText);
                //    }
                //    catch (System.Net.Mail.SmtpException excep)
                //    {
                //        Audit.Add(EventType.DesiyaAvailSearch, Severity.High, 0, "SMTP Exception returned from Hotel Results Page. Error Message:" + excep.Message + " | " + DateTime.Now, "");
                //    }
                //}
#endregion
                string IPAddr = Request.ServerVariables["REMOTE_ADDR"];
               // Audit.Add(EventType.DesiyaAvailSearch, Severity.High, Convert.ToInt32(member.ID), "Hotel Request Details : " + " | " + messageText + DateTime.Now + "|Agency Details:" + agency.Name + "|" + agency.Phone2, IPAddr);
                //Session["req"] = request;commented by shiva
                HotelSearchResult hData = new HotelSearchResult();
                if (hotelSearchResult != null && hotelSearchResult.Length > 0)
                {
                    hData.Save(Session["cSessionId"].ToString(), hotelSearchResult, request.NoOfRooms, request,ResultCacheType.CoreHotelResult);
                }
                SetFilterStatus();
                int totalFilteredRes = hotelSearchResult.Length;
                Session["ResultCount"] = totalFilteredRes;
                if (Session["cSessionId"] != null || totalFilteredRes > 0)
                {
                    GetDataFromDB();
                }
                else
                {
                    hotelSearchResult = new HotelSearchResult[0];
                    DataList1.DataSource = hotelSearchResult;
                    DataList1.DataBind();
                }
           // }
        }
        catch (BookingEngineException ex)
        {
            string url = Request.ServerVariables["HTTP_REFERER"].ToString();
            errorMessage = "Error: There is some error. Please " + "<a href=\"" + url + "\">" + "Retry" + "</a>.";
            ErrorFlag = true;
            throw ex;
        }
        catch (Exception excep)
        {
            errorMessage = "Error: " + excep.Message;
            ErrorFlag = true;
            Audit.Add(EventType.HotelSearch, Severity.High, (int)Settings.LoginInfo.UserID, excep.ToString(), "0");
        }
    }

    /// <summary>
    /// This is for Authentication.
    /// </summary>
    private void AuthorizationCheck()
    {
        //if (Session["roleId"] == null)
        //{
        //    String values = "?errMessage=Login Required to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
        //else if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.HotelSearch))
        //{
        //    String values = "?errMessage=You are not authorised to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
    }

    /// <summary>
    /// Gets the data from the database when filters applied or for pagination.
    /// </summary>
    private void GetDataFromDB()
    {
        noOfPages = 0;
        //Audit.Add(EventType.GetHotelDetails, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Filtering Hotel Results", Request["REMOTE_ADDR"]);
        string sessionId = string.Empty;
        try
        {
            if (Session["cSessionId"] != null)
            {
                sessionId = Session["cSessionId"].ToString();
                Dictionary<string, string> filterCriteria = new Dictionary<string, string>();
                if (Session["hFilterCriteria"] != null)
                {
                    filterCriteria = (Dictionary<string, string>)Session["hFilterCriteria"];
                }
                else
                {
                    filterCriteria.Add("recordsPerPage", recordsPerPage.ToString());
                    filterCriteria.Add("pageNo", pageNo.ToString());
                }
                HotelSearchResult hResult = new HotelSearchResult();
                hotelSearchResult = hResult.GetFilteredResult(sessionId, filterCriteria, ref noOfPages);
                if (hotelSearchResult.Length == 0)
                {
                    pageNo = 1;
                    filterCriteria["pageNo"] = pageNo.ToString();
                }
                if (hotelSearchResult != null && hotelSearchResult.Length > 0)
                {
                    showPageLink = CT.MetaSearchEngine.MetaSearchEngine.PagingJavascript(noOfPages, "HotelResults.aspx?change=true", pageNo);
                    showPageLink = showPageLink.Replace("|", ""); //added by brahmam 24.08.2016 New design purpose
                    //for (int i = 0; i < hotelSearchResult.Length; i++)// remove ziya
                    //{
                    //    if (hotelSearchResult[i].BookingSource == HotelBookingSource.HotelConnect)
                    //    {
                    //        //Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "Filtered Search Results: " + hotelSearchResult[i].HotelName, "1");
                    //    }
                    //}
                }

                DataList1.DataSource = hotelSearchResult;
                DataList1.DataBind();
            }
            else
            {
                //errorMessage = "Your Booking Session is Expired!";
                errorMessage = "<p>Sorry, we could not found the hotel you have requested but we would like to provide the best option for the same destination. To avail a better and cheaper option please call us at " + Settings.LoginInfo.AgentPhone + " or email us at <a href='mailto:" + Settings.LoginInfo.AgentEmail + "'>" + Settings.LoginInfo.AgentEmail + "</a></p>" + "</br><p>Go to <a href=\"HotelSearch.aspx?change=true\"" + "\">" + "Search Page" + "</a></p>.";
                ErrorFlag = true;
                hotelSearchResult = new HotelSearchResult[0];
                DataList1.DataSource = hotelSearchResult;
                DataList1.DataBind();
            }
            hdnSubmit.Value = "";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.ToString(), "");
            Utility.WriteLog(ex, "Failed to filter hotel results");
        }
    }

    /// <summary>
    /// Sets the filters applied
    /// </summary>
    private void SetFilterStatus()
    {
        Dictionary<string, string> filterCriteria = new Dictionary<string, string>();
        if (Request["hotelAddress"] != null)
        {
            hotelAddress = Request["hotelAddress"];
            filterCriteria.Add("hotelAddress", hotelAddress);
        }
        if (!txtHotelName.Text.Contains("Search"))
        {
            hotelName = txtHotelName.Text;
            filterCriteria.Add("hotelName", hotelName);
        }
        hotelRating = "";
        if (chkAll.Checked)
        {
            hotelRating = "0,1,2,3,4,5";
            filterCriteria.Add("hotelRating", hotelRating);
        }
        else
        {
            if (chkOne.Checked)
            {
                hotelRating = "1,";
                filterCriteria.Add("hotelRating", hotelRating.ToString());
            }
             if (chkTwo.Checked)
            {
                hotelRating += "2,";
                if (!filterCriteria.ContainsKey("hotelRating"))
                {
                    filterCriteria.Add("hotelRating", hotelRating.ToString());
                }
                else
                {
                    filterCriteria["hotelRating"] = hotelRating.ToString();
                }

            }
             if (chkThree.Checked)
            {
                hotelRating += "3,";
                if (!filterCriteria.ContainsKey("hotelRating"))
                {
                    filterCriteria.Add("hotelRating", hotelRating.ToString());
                }
                else
                {
                    filterCriteria["hotelRating"] = hotelRating.ToString();
                }
            }
             if (chkFour.Checked)
            {
                hotelRating += "4,";
                if (!filterCriteria.ContainsKey("hotelRating"))
                {
                    filterCriteria.Add("hotelRating", hotelRating.ToString());
                }
                else
                {
                    filterCriteria["hotelRating"] = hotelRating.ToString();
                }
            }
             if (chkFive.Checked)
             {
                 hotelRating += "5,";
                 if (!filterCriteria.ContainsKey("hotelRating"))
                 {
                     filterCriteria.Add("hotelRating", hotelRating.ToString());
                 }
                 else
                 {
                     filterCriteria["hotelRating"] = hotelRating.ToString();
                 }
             }
             if (!chkOne.Checked && !chkTwo.Checked && !chkThree.Checked && !chkFour.Checked && !chkFive.Checked)
             {
                 hotelRating = "0";
                 filterCriteria.Add("hotelRating", hotelRating.ToString());
             }
        }
        if (ddlOrderBy.SelectedIndex >= 0)
        {
            orderBy = Convert.ToInt16(ddlOrderBy.SelectedValue);
            filterCriteria.Add("orderBy", orderBy.ToString());
        }
        //filter by location
        //Dotw and WST at the time only we are filtering location oather source filtering Address
        if (request != null && request.Sources.Contains("DOTW") || request.Sources.Contains("WST"))
        {
            if (ddlLocation.SelectedIndex > 0)
            {
                location = ddlLocation.SelectedValue;
                filterCriteria.Add("location", location.ToString());
            }
        }
        else
        {
            if (!txtLocation.Text.Contains("Search By Location"))
            {
                location = txtLocation.Text.Trim();
                filterCriteria.Add("location", location.ToString());
            }
        }

        //set the no of records to show per page 
        filterCriteria.Add("recordsPerPage", recordsPerPage.ToString());
        //set the page no for the request
        filterCriteria.Add("pageNo", pageNo.ToString());
        //Add this object to the session
        Session.Add("hFilterCriteria", filterCriteria);

    }

    /// <summary>
    /// Gets all the filters applied
    /// </summary>
    private void GetFilterStatus()
    {
        Dictionary<string, string> filterCriteria = new Dictionary<string, string>();
        if (Session["hFilterCriteria"] != null)
        {
            filterCriteria = (Dictionary<string, string>)Session["hFilterCriteria"];
        }
        if (filterCriteria.ContainsKey("hotelAddress"))
        {
            hotelAddress = filterCriteria["hotelAddress"];
        }
        if (filterCriteria.ContainsKey("hotelName"))
        {
            hotelName = filterCriteria["hotelName"];
        }
        if (filterCriteria.ContainsKey("hotelRating"))
        {
            hotelRating = (filterCriteria["hotelRating"]);
        }
        if (filterCriteria.ContainsKey("orderBy"))
        {
            orderBy = Convert.ToInt16((filterCriteria["orderBy"]));
        }
        if (filterCriteria.ContainsKey("recordsPerPage"))
        {
            recordsPerPage = Convert.ToInt16((filterCriteria["recordsPerPage"]));
        }
        if (filterCriteria.ContainsKey("pageNo"))
        {
            pageNo = Convert.ToInt16((filterCriteria["pageNo"]));
        }
        //Added by brahmam filter location purpose
        if (filterCriteria.ContainsKey("location"))
        {
            location = filterCriteria["location"];
        }
       
    }

    /// <summary>
    /// Clears filters such as Hotel Name, Rating etc applied
    /// </summary>
    private void ClearAllFilter()
    {
        chkAll.Checked = true;
        chkFive.Checked = true;
        chkFour.Checked = true;
        chkThree.Checked = true;
        chkTwo.Checked = true;
        chkOne.Checked = true;

        ddlOrderBy.SelectedIndex = 0;
        txtHotelName.Text = string.Empty;

        hotelAddress = string.Empty;
        hotelName = string.Empty;
        hotelRating = "1,2,3,4,5";
        orderBy = 0;
        //Dotw and WST at the time only we are filtering location oather source filtering Address
        if (request != null && request.Sources.Contains("DOTW") || request.Sources.Contains("WST"))
        {
            ddlLocation.SelectedIndex = 0; //clear location
        }
        else
        {
            txtLocation.Text = "Search By Location";
        }
        if (Session["hFilterCriteria"] != null)
        {
            Session.Remove("hFilterCriteria");
        }
        SetFilterStatus();
        GetDataFromDB();
    }

    /// <summary>
    /// Updates the filter page no to retrieve data
    /// </summary>
    private void UpdateFilterPageNo()
    {
        Dictionary<string, string> filterCriteria = (Dictionary<string, string>)Session["hFilterCriteria"];
        filterCriteria["pageNo"] = pageNo.ToString();
        Session["hFilterCriteria"] = filterCriteria;
    }

    /// <summary>
    /// Event which binds search results data in the Data List
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            //if ((int)ViewState["tmpRowCount"] == 0)
            //{
            //    DateTime before = DateTime.Now;
            //    ViewState["tmpStartTime"] = before;
            //    //SearchHotels();
            //    //DateTime after = DateTime.Now;
            //    //TimeSpan ts = after - before;

            //    Audit.Add(EventType.Search, Severity.Normal, 1, "Hotel Page ItemBound start Time" + before.TimeOfDay+ " Seconds", "1");

            //}
            DataList1.Visible = true;
            HotelSearchResult resultObj = e.Item.DataItem as HotelSearchResult;
            HtmlTable roomtype = e.Item.FindControl("tblRoomDetails") as HtmlTable;
            HtmlInputHidden hSelRooms = e.Item.FindControl("hSelRooms") as HtmlInputHidden;
            hSelRooms.Value = "";
            
            Button ibtnBook = e.Item.FindControl("imgBtnBookNow") as Button;
            Label lblRating = e.Item.FindControl("lblRating") as Label;
            Label lblLowestPrice = e.Item.FindControl("lblLowestPrice") as Label;
            Label lblFare = e.Item.FindControl("lblFare") as Label;
            Label lblFareSummary = e.Item.FindControl("lblFareSummary") as Label;
            Label lblPROMO = e.Item.FindControl("lblPROMO") as Label;
            Label lblDeal = e.Item.FindControl("lblDeal") as Label;
            //Session[resultObj.HotelCode] = resultObj;
            lblFareSummary.Text = "<a onmouseover=\"javascript:show('fare_summary_" + resultObj.HotelCode + "');\" onmouseout=\"javascript:hide('fare_summary_" + resultObj.HotelCode + "');\" href='#'>";
            lblFareSummary.Text += "<img src='images/fare_summary.jpg' /></a>";
            lblFareSummary.Visible = false;
            if (resultObj != null)
            {
                //---------------------------------------Hotel Main Image-------------------------------------------//
                #region Hotel Main Image
                string imagePath = string.Empty;
                string showImg = string.Empty;
               // if (resultObj.BookingSource == HotelBookingSource.GTA)
               // {
               //     imagePath = ConfigurationSystem.GTAConfig["imgPathForServer"] + "/";
               // }
                    //Commented by brahmam 26.09.2014
                //else if (resultObj.BookingSource == HotelBookingSource.HotelBeds)
                //{
                //    imagePath = ConfigurationSystem.HotelBedsConfig["imgPathForServer"] + "/";
                //}
                //else if (resultObj.BookingSource == HotelBookingSource.Tourico)
                //{
                //  imagePath = ConfigurationSystem.TouricoConfig["imgPathForServer"] + "/";
                //}
                if (resultObj.BookingSource == HotelBookingSource.HotelConnect)
                {
                    imagePath = ConfigurationSystem.HotelConnectConfig["imgPathForServer"] ;
                   
                }
                //Commented by brahmam 27.06.2016
                //else if (resultObj.BookingSource == HotelBookingSource.WST)
                //{
                //    imagePath = ConfigurationSystem.WSTConfig["imgPathForServer"] + "/";
                //}
                if (string.IsNullOrEmpty(resultObj.HotelPicture))
                {
                    showImg = "images/HotelNA.jpg";
                }
                else
                {
                    if (resultObj.BookingSource == HotelBookingSource.HotelConnect)
                    {
                        showImg = imagePath + resultObj.HotelPicture.Split(',')[0];
                    }
                    else
                    {
                        showImg = resultObj.HotelPicture; //imagePath Commented by Brahmam Not required
                    }
                    /*if (resultObj.BookingSource == HotelBookingSource.GTA) use this code for showing the images from local folder, if not skipp this
                     * 
                    {
                       // imagePath = ConfigurationSystem.GTAConfig["imgPathForServer"] + "/";
                        imagePath = string.Empty;
                        
                    }
                    if (resultObj.BookingSource != HotelBookingSource.DOTW)
                    {
                        showImg = imagePath + resultObj.HotelPicture;
                    }
                    else if (resultObj.BookingSource == HotelBookingSource.RezLive)
                    {
                        showImg = resultObj.HotelPicture;
                    }
                    else if (resultObj.BookingSource == HotelBookingSource.DOTW)
                    {
                        string Path = CT.Configuration.ConfigurationSystem.DOTWConfig["imgPathForServer"];
                        if (!string.IsNullOrEmpty(resultObj.HotelPicture))
                        {
                            showImg = Path + resultObj.HotelPicture.Split(',')[0];
                        }
                    }*/

                }
                // Replacing HTTPS
                showImg = showImg.Replace("http:", "https:");
                Image img = e.Item.FindControl("Image1") as Image;
                img.ImageUrl = showImg; 
                #endregion
                //---------------------------------------------End--------------------------------------------------//
                //-------------------------------------Display Lowest price-----------------------------------------//
                #region Lowest Price
                if (resultObj.PromoMessage.Length > 0)
                {
                    lblPROMO.Text += "<div style='position: relative; display: block'>";
                    if (resultObj.BookingSource != HotelBookingSource.GRN && resultObj.BookingSource != HotelBookingSource.Yatra && resultObj.BookingSource != HotelBookingSource.OYO )
                    {
                        lblPROMO.Text += " <table border='0' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                    lblPROMO.Text += "<label style='font-size: 11px; color: Red'>";
                   
                        lblPROMO.Text += resultObj.PromoMessage;
                        lblPROMO.Text += "</label></td></tr></table>";
                    }
                    lblPROMO.Text += "</div>";
                    lblDeal.Visible = true;
                }
                else
                {
                    lblDeal.Visible = false;
                }
               /* if (resultObj.BookingSource == HotelBookingSource.DOTW) // unecessary code commented ziya
                {
                    double price = 0;
                    foreach (HotelRoomsDetails room in resultObj.RoomDetails)
                    {
                        if (request.NoOfRooms == resultObj.RoomDetails.Length)// clarification
                        {
                            if (resultObj.BookingSource != HotelBookingSource.RezLive)
                            {
                                //decimal price = (((resultObj.RoomDetails[0].SellingFare + resultObj.RoomDetails[0].TotalTax - resultObj.RoomDetails[0].Discount + resultObj.RoomDetails[0].SellExtraGuestCharges) * rateofExchange)) * request.NoOfRooms;
                                // Round off to the smallest integral value that is greater than or equal to the specified double-precision floating-point number
                                price += Math.Ceiling(((Convert.ToDouble(room.SellingFare + room.TotalTax - room.Discount + room.SellExtraGuestCharges) * rateofExchange)) * request.NoOfRooms);
                            }
                            else
                            {
                                price += Math.Ceiling(((Convert.ToDouble(room.TotalPrice + room.TotalTax - room.Discount + room.SellExtraGuestCharges) * rateofExchange)));
                            }
                        }
                    }
                    if (resultObj.Currency != "AED")
                    {
                        price = price * (double)rateOfEx[resultObj.Currency];
                    }
                    //lblLowestPrice.Text = price.ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]);
                    //lblLowestPrice.Text = price.ToString();
                    lblLowestPrice.Text = Math.Ceiling(price).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]);// ziya ( unneccesory code)
                    lblLowestPrice.ID += resultObj.HotelCode;
                } */
                #endregion
                //---------------------------------------------End--------------------------------------------------//
                //-----------------------------------Display Hotel Rating-------------------------------------------//
                #region Rating
                
                if (Convert.ToInt32(resultObj.Rating) > 0)
                {
                    int rating = Convert.ToInt32(resultObj.Rating);
                    switch (rating)
                    {
                        case 1:
                            lblRating.Text = "<div class='one-star-hotel'></div>";
                            break;
                        case 2:
                            lblRating.Text = "<div class='two-star-hotel'></div>";
                            break;
                        case 3:
                            lblRating.Text = "<div class='three-star-hotel'></div>";
                            break;
                        case 4:
                            lblRating.Text = "<div class='four-star-hotel'></div>";
                            break;
                        case 5:
                            lblRating.Text = "<div class='five-star-hotel'></div>";
                            break;
                    }
                } 
                #endregion
                //--------------------------------------------End---------------------------------------------------//
                //-----------------------------Display Hotel Description--------------------------------------------//
                #region Description
                Label lblDesc = e.Item.FindControl("lblDescription") as Label;

            string _return = string.Empty;
            if (resultObj.HotelDescription != null && resultObj.HotelDescription.Length > 0)
            {
                string[] Words = resultObj.HotelDescription.Split(' ');

                    if (Words.Length <= 23)
                    {
                        _return = resultObj.HotelDescription;
                    }
                    else
                    {
                        //for (int i = 0; i < 23; i++)
                        if (resultObj.HotelDescription.Length > 150)
                        {
                            _return = resultObj.HotelDescription.Substring(0, 150) + " ";
                        }
                        else
                        {
                            _return = resultObj.HotelDescription + " ";
                        }
                        _return += "...";
                    }
                }
                lblDesc.Text = _return; 
                #endregion
                //--------------------------------------------End---------------------------------------------------//

                //--------------------------Display room types along with Cancellation details----------------------//
                #region Show Room Details

                Label lblMultiRoomSelect = e.Item.FindControl("lblMultiRoomSelect") as Label;
                HtmlInputHidden rCode = e.Item.FindControl("rCode" + resultObj.HotelCode) as HtmlInputHidden;
                HtmlInputHidden rpCode = e.Item.FindControl("rpCode" + resultObj.HotelCode) as HtmlInputHidden;
                
                //decimal total = 0;
                //int adults = 0, childs = 0;
                //if (request.NoOfRooms > 1)
                {
                    /*if (resultObj.BookingSource == HotelBookingSource.DOTW)// clarification ziya unecessary code commented ziya
                    {
                        //lblLowestPrice.Text = (resultObj.RoomDetails[0].SellingFare).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]);
                        lblLowestPrice.Text = Math.Ceiling(resultObj.RoomDetails[0].SellingFare).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]);// ziya ( unneccesory code)
                        //lblLowestPrice.Text = "001";//ziya

                    }
                    else
                    {
                        if (resultObj.Currency != "AED")
                        {
                            lblLowestPrice.Text = Math.Ceiling(resultObj.RoomDetails[0].TotalPrice * rateOfEx[resultObj.Currency]).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]);
                        }
                        else
                        {
                            lblLowestPrice.Text = Math.Ceiling(resultObj.RoomDetails[0].TotalPrice).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]);
                        }
                    }*/
                    roomtype.Visible = false;
                    lblMultiRoomSelect.Text = "<input  type='hidden' name='hCode' value='" + e.Item.ItemIndex + "' />";                    
                    lblMultiRoomSelect.Text += "<input type='hidden' name='hCurrency" + e.Item.ItemIndex + "' id='hCurrency" + e.Item.ItemIndex + "' value='" + resultObj.Currency + "' />";
                    lblMultiRoomSelect.Text += "<input type='hidden' name='hROE" + e.Item.ItemIndex + "' id='hROE" + e.Item.ItemIndex + "' value='" + rateofExchange + "' />";
                    lblMultiRoomSelect.Text += "<div id='maindiv-" + e.Item.ItemIndex + "' style='display:none;'>";
                    if (resultObj.BookingSource != HotelBookingSource.DOTW)
                    {                        
                        lblMultiRoomSelect.Text += "<div class='hotel_details' ><div class='showMsgHeading' >";
                        lblMultiRoomSelect.Text += resultObj.HotelName + "<div class='clear'></div></div>";
                        lblMultiRoomSelect.Text += "  <div id='middle-container-" + e.Item.ItemIndex + "'  >";

                        //lblMultiRoomSelect.Text += "  <div class='first_row fleft'>";
                        //lblMultiRoomSelect.Text += "  <div class='first_col fleft'>";
                        //lblMultiRoomSelect.Text += "<p class='margin-3-8 fleft'><a href=\"javascript:HideMRPopUp('" + e.Item.ItemIndex + "')\">Choose Another Hotel</a></p>";
                        //lblMultiRoomSelect.Text += "<p class='width-400 fleft'><b style='font-size:17px'>" + resultObj.HotelName + "</b></p>";
                        //lblMultiRoomSelect.Text += "<p class='width-400 fleft'><b>Check-in Date:<span>" + request.StartDate.ToString("dd MMM yyyy") + "</span></b> </p>";
                        //lblMultiRoomSelect.Text += "<p class='width-400 fleft'><b>Check-out Date:<span>" + request.EndDate.ToString("dd MMM yyyy") + "</span></b> </p>";
                        //lblMultiRoomSelect.Text += "</div>";
                    }
                    decimal totFare = 0;
                    //decimal tempTotFare = 0;
                    int roomType = 0;
                    decimal roomTypePrice = 0;
                    decimal discount = 0;
                    if (resultObj.BookingSource == HotelBookingSource.HotelConnect)
                    {
                        //roomType = Convert.ToInt32(resultObj.RoomDetails[0].RoomTypeCode);
                    }
                    //else if (resultObj.BookingSource == HotelBookingSource.RezLive)
                    //{
                    //    //roomTypePrice = resultObj.RoomDetails[0].TotalPrice;
                    //}
                    if (resultObj.BookingSource != HotelBookingSource.TBOHotel)
                    {
                        for (int x = 0; x < request.NoOfRooms; x++)// price displaying
                        {
                            discount = 0;
                            for (int y = 0; y < resultObj.RoomDetails.Length; y++)
                            {
                                //tempTotFare = 0;
                                if (resultObj.BookingSource == HotelBookingSource.DOTW)
                                {
                                    int nights = request.EndDate.Subtract(request.StartDate).Days;
                                    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()) && resultObj.RoomDetails[y].Occupancy["minStay"] <= nights)
                                    {
                                        totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup + (resultObj.RoomDetails[y].SellExtraGuestCharges * resultObj.RoomDetails[y].Rates.Length) + resultObj.RoomDetails[y].TotalTax;//clarification 
                                        //decimal tempTotFare = resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup + (resultObj.RoomDetails[y].SellExtraGuestCharges * resultObj.RoomDetails[y].Rates.Length) + resultObj.RoomDetails[y].TotalTax;
                                        //totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup + (resultObj.RoomDetails[y].SellExtraGuestCharges * resultObj.RoomDetails[y].Rates.Length) + resultObj.RoomDetails[y].TotalTax;
                                        if (hDOTWDiscountType.Value == "P")// getting discount value
                                        {
                                            discount = (totFare * resultObj.RoomDetails[y].Discount);
                                            resultObj.RoomDetails[y].DiscountType = "P";
                                        }
                                        else
                                        {
                                            discount = Utility.ToDecimal(hDOTWDiscount.Value);
                                            resultObj.RoomDetails[y].DiscountType = "F";
                                        }
                                        //resultObj.RoomDetails[y].Discount = discount;
                                        break;
                                    }
                                }
                                else if (resultObj.BookingSource == HotelBookingSource.HotelConnect)
                                {
                                    if (Convert.ToBoolean(ViewState["SamePax"]))
                                    {
                                        if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo) == x + 1)
                                        {
                                            totFare += resultObj.RoomDetails[y].SellingFare + (resultObj.RoomDetails[y].SellExtraGuestCharges * resultObj.RoomDetails[y].Rates.Length) + resultObj.RoomDetails[y].TotalTax + resultObj.RoomDetails[y].Markup;
                                            break;
                                        }
                                        //else
                                        //{
                                        //    //roomType = (resultObj.RoomDetails[y].RoomTypeCode);
                                        //    break;
                                        //}
                                    }
                                    else
                                    {
                                        if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo) == request.RoomGuest[x].noOfAdults)
                                        {
                                            totFare += resultObj.RoomDetails[y].SellingFare + (resultObj.RoomDetails[y].SellExtraGuestCharges * resultObj.RoomDetails[y].Rates.Length) + resultObj.RoomDetails[y].TotalTax + resultObj.RoomDetails[y].Markup;
                                            break;
                                        }
                                    }
                                }
                                else if (resultObj.BookingSource == HotelBookingSource.RezLive) //Modofied by brahmam 18.09.2015 ....req Rezlive purpose
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                                    {
                                        totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup + (resultObj.RoomDetails[y].SellExtraGuestCharges * resultObj.RoomDetails[y].Rates.Length) + resultObj.RoomDetails[y].TotalTax;
                                        break;
                                    }
                                }
                                else if (resultObj.BookingSource == HotelBookingSource.LOH)
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Split(',').Length > 1)
                                    {

                                        for (int k = 0; k < resultObj.RoomDetails[y].SequenceNo.Split(',').Length; k++)
                                        {
                                            if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo.Split(',')[k]) == (x + 1))
                                            {
                                                totFare += (resultObj.RoomDetails[y].SellingFare) + resultObj.RoomDetails[y].Markup + resultObj.RoomDetails[y].TotalTax;
                                                //if (hLOHDiscountType.Value == "F")
                                                //{
                                                //    discount = resultObj.RoomDetails[y].Discount;
                                                //}
                                                //else
                                                //{
                                                //    discount = Math.Ceiling(totFare) * Convert.ToDecimal(hLOHDiscount.Value) / 100;
                                                //    //resultObj.RoomDetails[y].Discount = discount;
                                                //    resultObj.RoomDetails[y].DiscountType = "P";
                                                //}
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                    else if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo) == (x + 1))
                                    {
                                        {
                                            totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup + resultObj.RoomDetails[y].TotalTax;
                                            if (hLOHDiscountType.Value == "F")
                                            {
                                                discount = resultObj.RoomDetails[y].Discount;
                                            }
                                            else
                                            {
                                                discount = Math.Ceiling(totFare) * Convert.ToDecimal(hLOHDiscount.Value) / 100;
                                                //resultObj.RoomDetails[y].Discount = discount;
                                                resultObj.RoomDetails[y].DiscountType = "P";
                                            }
                                        }
                                        break;
                                    }
                                }
                                //Added by brahmam 24.09.2014
                                else if (resultObj.BookingSource == HotelBookingSource.HotelBeds)
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                                    {
                                        totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup + (resultObj.RoomDetails[y].SellExtraGuestCharges * resultObj.RoomDetails[y].Rates.Length) + resultObj.RoomDetails[y].TotalTax;
                                        if (hDOTWDiscountType.Value == "P")
                                        {
                                            discount = (totFare * resultObj.RoomDetails[y].Discount);
                                            resultObj.RoomDetails[y].DiscountType = "P";
                                        }
                                        else
                                        {
                                            discount = Utility.ToDecimal(hDOTWDiscount.Value);
                                            resultObj.RoomDetails[y].DiscountType = "F";
                                        }
                                        break;
                                    }
                                }
                                //Added by brahmam 17.11.2014
                                else if (resultObj.BookingSource == HotelBookingSource.GTA)
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                                    {
                                        totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup + (resultObj.RoomDetails[y].SellExtraGuestCharges * resultObj.RoomDetails[y].Rates.Length) + resultObj.RoomDetails[y].TotalTax;
                                        break;
                                    }
                                }

                                else if (resultObj.BookingSource == HotelBookingSource.Miki)
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                                    {
                                        totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup + (resultObj.RoomDetails[y].SellExtraGuestCharges * resultObj.RoomDetails[y].Rates.Length) + resultObj.RoomDetails[y].TotalTax;
                                        break;
                                    }
                                }
                                else if (resultObj.BookingSource == HotelBookingSource.WST)
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                                    {
                                        totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup;
                                        break;
                                    }
                                }
                                else if (resultObj.BookingSource == HotelBookingSource.JAC) //Added by brahmam 02.07.2016
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                                    {
                                        totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup;
                                        break;
                                    }
                                }
                                else if (resultObj.BookingSource == HotelBookingSource.EET)
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Split(',').Length > 1)
                                    {
                                        for (int k = 0; k < resultObj.RoomDetails[y].SequenceNo.Split(',').Length; k++)
                                        {
                                            if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo.Split(',')[k]) == (x + 1))
                                            {
                                                totFare += (resultObj.RoomDetails[y].SellingFare) + resultObj.RoomDetails[y].Markup + resultObj.RoomDetails[y].TotalTax;
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                    else if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo) == (x + 1))
                                    {
                                        totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup + resultObj.RoomDetails[y].TotalTax;
                                        break;
                                    }
                                }
                                //Added by brahmam 08.02.2018
                                else if (resultObj.BookingSource == HotelBookingSource.Agoda)
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                                    {
                                        totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup + (resultObj.RoomDetails[y].SellExtraGuestCharges * resultObj.RoomDetails[y].Rates.Length) + resultObj.RoomDetails[y].TotalTax;
                                        break;
                                    }
                                }
                                //Added by Somasekhar on 28/08/2018
                                else if (resultObj.BookingSource == HotelBookingSource.Yatra)
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                                    {
                                        totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup;// + (resultObj.RoomDetails[y].SellExtraGuestCharges * resultObj.RoomDetails[y].Rates.Length) + resultObj.RoomDetails[y].TotalTax;
                                        break;
                                    }
                                }
 #region GRN Added by Harish
                                else if (resultObj.BookingSource == HotelBookingSource.GRN)
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                                    {
                                        totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup;
                                        break;
                                    }
                                }
                                #endregion
                                //Added by Somasekhar on 13/12/2018
                                #region OYO source
                                else if (resultObj.BookingSource == HotelBookingSource.OYO)
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                                    {
                                        totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].Markup;
                                        break;
                                    }
                                }
                                #endregion
                                // =========================
                            }// end room details loop   
                        }// end no.of room loop
                    }
                    else
                    {
                        totFare = resultObj.TotalPrice;
                    }

                   // if (resultObj.Currency != "AED")
                   // {
                        //totFare = totFare * rateOfEx[resultObj.Currency];
                   // }
                    //totFare = Math.Round(totFare, Convert.ToInt32(ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                    //totFare = Math.Ceiling(totFare);
                    
                    
                    totFare = Math.Ceiling(totFare) - discount;

                    if (resultObj.BookingSource != HotelBookingSource.DOTW)
                    {
                        ////lblMultiRoomSelect.Text += "</div>";
                        ////lblMultiRoomSelect.Text += "<div id='roomSelectionError" + e.Item.ItemIndex + "' class='error_module' style='display:none;'> ";
                        ////lblMultiRoomSelect.Text += "<div  style='float:left; color:Red;' class='padding-5 yellow-back width-100 center margin-top-5'>Please select same room type for all room options.</div>";
                        ////lblMultiRoomSelect.Text += "</div>";
                        ////lblMultiRoomSelect.Text += "<p class='fleft width-600 text-align-left blue-color'><b>Select your room type</b></p>";
                        ////lblMultiRoomSelect.Text += "<hr class='width-700 fleft' />";
                        //lblMultiRoomSelect.Text += "<div id='selectRoomDiv' ><div class='selectRoomL'>";
                        //lblMultiRoomSelect.Text += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                        ////lblMultiRoomSelect.Text += "<b></b></h1>";
                        ////lblMultiRoomSelect.Text += "<div><div class='padbot10' style='text-align:right'><a href=\"javascript:HideMRPopUp('" + e.Item.ItemIndex + "')\"><b>Choose other Hotel</b></a></div>";
                        ////lblMultiRoomSelect.Text += "</td></tr></table></td></tr>";
                        //lblMultiRoomSelect.Text += "<tr><td valign='top'><div><table width='100%' height='100%' border='0' cellpadding='0' cellspacing='0' class='selectRoom'>";
                    }
                    bool isTourico = false;
                    if (resultObj.BookingSource == HotelBookingSource.Tourico)
                    {
                        isTourico = true;
                    }
                    for (int x = 0; x < request.NoOfRooms; x++)
                    {
                        if (resultObj.BookingSource != HotelBookingSource.DOTW)
                        {
                            //lblMultiRoomSelect.Text += " <tr><td colspan='3'>";
                            //lblMultiRoomSelect.Text += "<input  type='hidden' name='rCode" + e.Item.ItemIndex + "-" + x + "' id='rCode" + e.Item.ItemIndex + "-" + x + "' /> ";
                            //lblMultiRoomSelect.Text += "<input  type='hidden' name='rCodeDefault" + e.Item.ItemIndex + "-" + x + "' id='rCodeDefault" + e.Item.ItemIndex + "-" + x + "' /> ";
                            //lblMultiRoomSelect.Text += "<input  type='hidden' name='priceTag" + e.Item.ItemIndex + "-" + x + "' id='priceTag" + e.Item.ItemIndex + "-" + x + "' />";
                            //lblMultiRoomSelect.Text += "<b>Select Room " + (x + 1) + ": " + request.RoomGuest[x].noOfAdults + " Adult ";
                            //if (request.RoomGuest[x].noOfChild > 0)
                            //    lblMultiRoomSelect.Text += request.RoomGuest[x].noOfChild + "' Child' </b></td></tr>";
                            //else
                            //    lblMultiRoomSelect.Text += "</b></td></tr>";

                            //lblMultiRoomSelect.Text += "<tr><th width='50%' align='left'>Room Types</th><th width='24%' align='center'>Rates</th><th width='26%'>Cancellation Charges</th></tr>";
                            ////lblMultiRoomSelect.Text += " <table class='hotel_res_table' cellpadding='0' cellspacing='0'>";
                            ////lblMultiRoomSelect.Text += "<tr class='header-bg'>";
                            ////lblMultiRoomSelect.Text += "<td class='cl1'>&nbsp;</td>";
                            ////lblMultiRoomSelect.Text += "<td class='cl2'>";
                            ////lblMultiRoomSelect.Text += "<b>Room Types</b></td>";
                            ////lblMultiRoomSelect.Text += "<td class='cl5'><b>Rates</b></td></tr>";
                        }
                        int firstRoom = 0;
                        for (int y = 0; y < resultObj.RoomDetails.Length; y++)// unnecessary codes
                        {
                            # region DOTW
                            if (resultObj.BookingSource == HotelBookingSource.DOTW)
                            {
                            //    int nights = request.EndDate.Subtract(request.StartDate).Days;
                            //    if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()) && resultObj.RoomDetails[y].Occupancy["minStay"] <= nights)
                            //    {
                                    
                            //        double totalSellRate = 0;
                            //        double totalPubRate = 0;
                            //        HotelRoomsDetails roomResult = resultObj.RoomDetails[y];
                                    
                            //        totalSellRate = (Convert.ToDouble(roomResult.SellingFare +resultObj.Price.Markup  + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length) + roomResult.TotalTax));
                            //        if (resultObj.RoomDetails[y].Occupancy["minStay"] > 0 && request.EndDate.Subtract(request.StartDate).Days < resultObj.RoomDetails[y].Occupancy["minStay"])
                            //        {
                            //            totalSellRate = totalSellRate * resultObj.RoomDetails[y].Occupancy["minStay"];
                            //            //totFare = (decimal)totalSellRate * request.NoOfRooms;
                            //        }
                                    
                            //        lblMultiRoomSelect.Text += "    <tr>";
                            //        if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                            //        {
                            //            lblMultiRoomSelect.Text += " <td>";
                            //            lblMultiRoomSelect.Text += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";
                                        

                            //            if (firstRoom == 0)
                            //                lblMultiRoomSelect.Text += "checked=\'checked\'";
                            //            else
                            //                lblMultiRoomSelect.Text += "";

                            //            lblMultiRoomSelect.Text += " onclick=\"javascript:SelectRoomType(\'" + e.Item.ItemIndex + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + isTourico + "\');\"/>";
                            //            lblMultiRoomSelect.Text += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";
                            //        }

                            //        if (resultObj.RoomDetails[y].Occupancy["minStay"] > 0)
                            //        {
                                        
                            //            lblMultiRoomSelect.Text += "<table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/img_mandatory.gif' /></td><td>";
                            //            lblMultiRoomSelect.Text += "<label style='font-size: 11px; color: Red'>";
                            //            lblMultiRoomSelect.Text += "Min Stay Nights: " + resultObj.RoomDetails[y].Occupancy["minStay"];
                            //            lblMultiRoomSelect.Text += "</label></td></tr></table>";
                            //        }

                            //        lblMultiRoomSelect.Text += "<div class='room_amenities' style='display: none;text-align:left;' id='AmenityDiv-" + e.Item.ItemIndex.ToString() + y + "'>";
                            //        lblMultiRoomSelect.Text += "<span id='amenityClo' title='close' style='font-size: 24px; cursor: pointer; position: absolute;right: 10px; top: 5px' onclick=\"hideAmenities('AmenityDiv-" + e.Item.ItemIndex.ToString() + y + "')\"><a class='closex' href='#'>X</a></span>";
                            //        if (resultObj.RoomDetails[y].Amenities != null && resultObj.RoomDetails[y].Amenities.Count > 0)
                            //        {
                            //            lblMultiRoomSelect.Text += "<div class='showMsgHeading'>Room Amenities</div>";
                            //            lblMultiRoomSelect.Text += "<div style='overflow:auto;height:170px'><ul>";
                                        
                            //            foreach (string amenity in resultObj.RoomDetails[y].Amenities)
                            //            {
                            //                lblMultiRoomSelect.Text += "<li style='padding-left:10px; padding-top:3px'>" + amenity + "</li>";
                            //            }
                            //            lblMultiRoomSelect.Text += "</ul></div>";
                            //        }

                            //        lblMultiRoomSelect.Text+="</div>";
                            //        lblMultiRoomSelect.Text += "<div style='text-align: right'><span onclick=\"javascript:ShowRoomAmenities('AmenityDiv-" + e.Item.ItemIndex.ToString() + y + "','" + e.Item.ItemIndex + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                            //        lblMultiRoomSelect.Text += "</div></td>";
                            //        //lblMultiRoomSelect.Text += "<td align='center'><div class='style1'>AED " + Math.Round(totalSellRate, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</div>";
                            //        lblMultiRoomSelect.Text += "<td align='center'><div class='style1'>AED " + Math.Round(totalSellRate,2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</div>";
                            //        lblMultiRoomSelect.Text += "<span onclick=\"javascript:GetRoomInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + e.Item.ItemIndex + "','"+y+"')\"><a style=' line-height:20px; ' href='#'>Rate Breakup</a></span>";
                                                           
                            //        #region Fare Breakup
                                    
                            //        lblMultiRoomSelect.Text += "<div class='links'>";

                            //        DateTime d1 = resultObj.StartDate;
                            //        DateTime d2 = resultObj.EndDate;
                            //        d2 = d2.AddDays(-1);
                            //        int result = 0;
                            //        // Get first full week
                            //        DateTime firstDayOfFirstFullWeek = d1;
                            //        if (d1.DayOfWeek != DayOfWeek.Sunday)
                            //        {
                            //            result += 1;
                            //            firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                            //        }
                            //        // Get last full week
                            //        DateTime lastDayOfLastFullWeek = d2;
                            //        if (d2.DayOfWeek != DayOfWeek.Saturday)
                            //        {
                            //            result += 1;
                            //            lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                            //        }
                            //        System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                            //        // Add number of full weeks
                            //        result += (diffResult.Days + 1) / 7;

                            //        int minStay = 1;
                            //        if (request.EndDate.Subtract(request.StartDate).Days < resultObj.RoomDetails[y].Occupancy["minStay"])
                            //        {
                            //            minStay = resultObj.RoomDetails[y].Occupancy["minStay"];
                                        
                            //        }
                            //        //lblMultiRoomSelect.Text += "<table><tr><td colspan='3'>";
                            //        lblMultiRoomSelect.Text += "<div style='width: 400px; background: #fff; float: left;border: 4px solid #ccc;display:none;z-index:100;margin:0px 0 0 -200px;' class='fare_breakup_popup' id='fareP" + e.Item.ItemIndex.ToString() + y + "' >";
                            //        lblMultiRoomSelect.Text += "<div class='showMsgHeading'>Rate Breakup</div>";
                            //        lblMultiRoomSelect.Text += "<div class='week_days'>";
                            //        lblMultiRoomSelect.Text += "<ul><li>Sun</li><li>Mon</li><li>Tue</li><li>Wed</li><li>Thu</li><li>Fri</li><li>Sat</li></ul></div>";
                            //        d1 = resultObj.StartDate;
                            //        d2 = resultObj.EndDate;
                            //        decimal totalRoomSellRate = 0;
                            //        for (int i = 0, j = 0; i < result; i++)
                            //        {
                            //            lblMultiRoomSelect.Text += "<div class='week_number'>";
                            //            lblMultiRoomSelect.Text += "<ul><li class='week_sno'>Week " + (i + 1) + "</li>";
                            //            int l = 0;
                            //            if (d1.DayOfWeek != DayOfWeek.Sunday)
                            //            {
                            //                int predeser = Convert.ToInt16(d1.DayOfWeek);
                            //                for (; l < predeser; l++)
                            //                {
                            //                    lblMultiRoomSelect.Text += "<li>&nbsp;</li><li>&nbsp;</li>";
                            //                }
                            //            }

                            //            for (; l < 7 && d1 < d2; l++)
                            //            {
                            //                RoomRates rmInfo = new RoomRates();
                            //                rmInfo = roomResult.Rates[j++];
                            //                d1 = d1.AddDays(1);
                                            
                            //                lblMultiRoomSelect.Text += "<li>" + Math.Round(rmInfo.SellingFare, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";

                            //                for (int k = 1; k < minStay; k++)
                            //                {
                            //                    lblMultiRoomSelect.Text += "<li>" + Math.Round(rmInfo.SellingFare, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                            //                }
                            //                totalRoomSellRate += rmInfo.SellingFare;
                            //            }


                            //            lblMultiRoomSelect.Text += "</ul></div>";
                            //        }
                            //        lblMultiRoomSelect.Text += "<div class='sum_totalhotel'><ul><li>Total : ";
                            //        lblMultiRoomSelect.Text += "<b>" + Math.Round((totalRoomSellRate + resultObj.Price.Markup) * minStay, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li>";
                            //        //lblMultiRoomSelect.Text += "</ul>";
                            //        //if (resultObj.Price.Discount > 0)
                            //        //{
                            //        //    lblMultiRoomSelect.Text += "<ul><li>Discount : ";
                            //        //    lblMultiRoomSelect.Text += "<b>" + (resultObj.Price.Discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                            //        //}
                            //        //lblMultiRoomSelect.Text += "<ul><li>Tax</li>";
                            //        //lblMultiRoomSelect.Text += " <li><b>" + (roomResult.TotalTax).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";

                            //        lblMultiRoomSelect.Text += "<ul><li>Extra Guest Charge : ";

                            //        decimal extraGuestRate = 0;
                            //        if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                            //        {
                            //            extraGuestRate = roomResult.SellExtraGuestCharges * roomResult.Rates.Length;
                            //        }
                            //        else
                            //        {
                            //            extraGuestRate = roomResult.SellExtraGuestCharges;
                            //        }

                            //        lblMultiRoomSelect.Text += " <b>" + Math.Round(extraGuestRate,2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                            //        if (resultObj.BookingSource == HotelBookingSource.TBOConnect)
                            //        {
                            //            lblMultiRoomSelect.Text += " <ul><li>Child Charge</li>";
                            //            lblMultiRoomSelect.Text += " <li><b>" + roomResult.ChildCharges.ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                            //        }

                            //        lblMultiRoomSelect.Text += "<ul><li>Total Price : ";


                            //        decimal tempTotalP = 0;

                            //        tempTotalP = (roomResult.SellingFare +resultObj.Price.Markup + roomResult.TotalTax + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length));

                            //        //tempTotalP -= resultObj.Price.Discount;

                            //        lblMultiRoomSelect.Text += " <b>" + Math.Round(tempTotalP * minStay, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                            //        lblMultiRoomSelect.Text += "<div id='divMeal" + resultObj.RoomDetails[y].RoomTypeCode + "' ></div>";

                            //        lblMultiRoomSelect.Text += "<a style='position:absolute; top: 5px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + e.Item.ItemIndex.ToString() + y + "')\"> X</a></div></div>";
                            //        #endregion
                        
                            //        lblMultiRoomSelect.Text += "</td><td>";

                            //        if (firstRoom == 0)
                            //        {
                                        
                            //            lblMultiRoomSelect.Text += " <script type='text/javascript'>";
                            //            lblMultiRoomSelect.Text += " document.getElementById('rCode" + e.Item.ItemIndex + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                            //            lblMultiRoomSelect.Text += " document.getElementById('rCodeDefault" + e.Item.ItemIndex + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";

                            //            if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                            //            {
                            //                lblMultiRoomSelect.Text += " document.getElementById('priceTag" + e.Item.ItemIndex + "-" + x + "').value = '" + totalSellRate + "';";
                            //            }
                            //            else
                            //            {
                            //                lblMultiRoomSelect.Text += " document.getElementById('priceTag" + e.Item.ItemIndex + "-" + x + "').value = '" + totalPubRate + "';";
                            //            }
                            //            lblMultiRoomSelect.Text += "</script>";
                            //        }
                                   
                            //        firstRoom++;
                                    
                            //        lblMultiRoomSelect.Text += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;' id='room" + roomResult.RoomTypeCode + "'></div> ";

                            //        lblMultiRoomSelect.Text += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + e.Item.ItemIndex + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></a></div></td>";
                            //        //lblMultiRoomSelect.Text += "<span  class='fright width-140' onclick=\"javascript:ShowFare('fareP" + y + "" + roomResult.RoomTypeCode + "" + roomResult.RatePlanCode + "')\" style='font-size:11px; text-align:left;'><a style=' line-height:20px; ' href='#'>Rate Breakup</a></span>";

                            //        //lblMultiRoomSelect.Text += "</div><div class='close_window' ><span><a class='hand' onclick=\"javascript:hide('fareP" + y + "" + roomResult.RoomTypeCode + "" + roomResult.RatePlanCode + "')\">Close</a></span></div></div></td></tr>";
                                   
                                    
                            //    }
                            }
                            # endregion
                            #region Hotel Connect
                            else if (resultObj.BookingSource == HotelBookingSource.HotelConnect)
                            {
                                //if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo) == request.RoomGuest[x].noOfAdults)
                                //{
                                //    double totalSellRate = 0;
                                //    double totalPubRate = 0;
                                //    HotelRoomsDetails roomResult = resultObj.RoomDetails[y];
                                //    if (resultObj.BookingSource == HotelBookingSource.HotelConnect)
                                //    {
                                //        totalSellRate = (Convert.ToDouble(roomResult.SellingFare + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length) + roomResult.TotalTax - roomResult.Discount));
                                //    }
                                //    else
                                //    {
                                //        totalSellRate = (Convert.ToDouble(roomResult.TotalPrice + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax - roomResult.Discount));
                                //        //totalPubRate = totalSellRate + (totalSellRate - Convert.ToDouble(roomResult.SellExtraGuestCharges) - Convert.ToDouble(roomResult.ChildCharges)) * Convert.ToDouble(resultObj.Price.AgentCommission) / 100;

                                //    }

                                //    lblMultiRoomSelect.Text += "    <tr>";
                                //    if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                //    {

                                //        lblMultiRoomSelect.Text += " <td>";
                                //        lblMultiRoomSelect.Text += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";
                                //        if (firstRoom == 0)
                                //            lblMultiRoomSelect.Text += "checked=\'checked\'";
                                //        else
                                //            lblMultiRoomSelect.Text += "";
                                //        lblMultiRoomSelect.Text += " onclick=\"javascript:SelectRoomType(\'" + e.Item.ItemIndex + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + isTourico + "\');\"/></td>";
                                //    }
                                //    else
                                //    {
                                //        lblMultiRoomSelect.Text += "<td>";
                                //        lblMultiRoomSelect.Text += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";
                                //        if (firstRoom == 0)
                                //            lblMultiRoomSelect.Text += "checked=\'true\'";
                                //        else
                                //            lblMultiRoomSelect.Text += "''";
                                //        lblMultiRoomSelect.Text += " onclick=\"javascript:SelectRoomType('" + e.Item.ItemIndex + "','" + x + "','" + y + "','" + roomResult.RoomTypeCode + "','" + totalPubRate + "','" + isTourico + "');\"/> </td>";
                                //    }
                                //    if (firstRoom == 0)
                                //    {

                                //        lblMultiRoomSelect.Text += " <script type='text/javascript'>";
                                //        lblMultiRoomSelect.Text += " document.getElementById('rCode" + e.Item.ItemIndex + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                //        lblMultiRoomSelect.Text += " document.getElementById('rCodeDefault" + e.Item.ItemIndex + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                //        if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                //        {
                                //            lblMultiRoomSelect.Text += " document.getElementById('priceTag" + e.Item.ItemIndex + "-" + x + "').value = '" + totalSellRate + "';";
                                //        }
                                //        else
                                //        {
                                //            lblMultiRoomSelect.Text += " document.getElementById('priceTag" + e.Item.ItemIndex + "-" + x + "').value = '" + totalPubRate + "';";
                                //        }
                                //        lblMultiRoomSelect.Text += "</script>";
                                //    }
                                //    firstRoom++;
                                //    lblMultiRoomSelect.Text += "<td>" + roomResult.RoomTypeName;
                                //    if (roomResult.Occupancy.ContainsKey("ExtraBed") && roomResult.Occupancy["ExtraBed"] == 1)
                                //    {
                                //        lblMultiRoomSelect.Text += " + &nbsp;&nbsp;<b>Extra Bed available</b>";
                                //    }

                                //    lblMultiRoomSelect.Text += "<br /><em>";
                                //    if (roomResult.Amenities != null && roomResult.Amenities.Count > 0)
                                //    {
                                //        lblMultiRoomSelect.Text += " Incl:";
                                //        string extras = "";
                                //        foreach (string amenity in roomResult.Amenities)
                                //        {
                                //            if (extras.Length > 0)
                                //            {
                                //                extras += ", " + amenity;
                                //            }
                                //            else
                                //            {
                                //                extras = amenity;
                                //            }
                                //        }
                                //        lblMultiRoomSelect.Text += extras;
                                //    }

                                //    lblMultiRoomSelect.Text += " </em></td><td class='cl5'>";

                                //    lblMultiRoomSelect.Text += "</div>";
                                //    //if (resultObj.Currency != "AED")
                                //    //{
                                //    //    totalSellRate = totalSellRate * (double)rateOfEx[resultObj.Currency];
                                //    //}

                                //    lblMultiRoomSelect.Text += "AED" + "</b> " + (totalSellRate * rateofExchange);
                                //    //if (symbol != "" + ConfigurationSystem.LocaleConfig["CurrencySign"] + "")
                                //    //{
                                //    //    lblMultiRoomSelect.Text += "(" + symbol + "&nbsp;" + totalSellRate + ")";
                                //    //}
                                //    lblMultiRoomSelect.Text += "<span  class='fright width-140' onclick=\"javascript:ShowFare('fareP" + y + "" + roomResult.RoomTypeCode + "" + roomResult.RatePlanCode + "')\" style='font-size:11px; text-align:left;'><a style=' line-height:20px; ' href='#'>Rate Breakup</a></span>";
                                //    lblMultiRoomSelect.Text += "</div>      <div class='links'>";

                                //    lblMultiRoomSelect.Text += "</div></td></tr><tr>";

                                //DateTime d1 = resultObj.StartDate;
                                //DateTime d2 = resultObj.EndDate;
                                //d2 = d2.AddDays(-1);
                                //int result = 0;
                                //// Get first full week
                                //DateTime firstDayOfFirstFullWeek = d1;
                                //if (d1.DayOfWeek != DayOfWeek.Sunday)
                                //{
                                //    result += 1;
                                //    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                //}
                                //// Get last full week
                                //DateTime lastDayOfLastFullWeek = d2;
                                //if (d2.DayOfWeek != DayOfWeek.Saturday)
                                //{
                                //    result += 1;
                                //    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                //}
                                //System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                //// Add number of full weeks
                                //result += (diffResult.Days + 1) / 7;



                                //lblMultiRoomSelect.Text += "</tr><tr><td colspan='3'>";
                                //lblMultiRoomSelect.Text += "<div style='width: 400px; background: #fff; float: left;border: 1px solid #f1e5d0;display:none;' class='sfare_breakup_popup' id='fareP" + y + "" + roomResult.RoomTypeCode + "" + roomResult.RatePlanCode + "' >";
                                //lblMultiRoomSelect.Text += "<div class='week_days'>";
                                //lblMultiRoomSelect.Text += "<ul><li>Sun</li><li>Mon</li><li>Tue</li><li>Wed</li><li>Thu</li><li>Fri</li><li>Sat</li></ul></div>";
                                //d1 = resultObj.StartDate;
                                //d2 = resultObj.EndDate;
                                //decimal totalRoomSellRate = 0;
                                //for (int i = 0, j = 0; i < result; i++)
                                //{
                                //    lblMultiRoomSelect.Text += "<div class='week_number'>";
                                //    lblMultiRoomSelect.Text += "<ul><li class='week_sno'>Week " + i + 1 + "</li>";
                                //    int l = 0;
                                //    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                //    {
                                //        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                //        for (; l < predeser; l++)
                                //        {
                                //            lblMultiRoomSelect.Text += "<li>  </li>";
                                //        }
                                //    }

                                //        for (; l < 7 && d1 < d2; l++)
                                //        {
                                //            RoomRates rmInfo = new RoomRates();
                                //            rmInfo = roomResult.Rates[j++];
                                //            d1 = d1.AddDays(1);
                                //            if (resultObj.BookingSource != HotelBookingSource.RezLive)
                                //            {
                                //                lblMultiRoomSelect.Text += "  <li>" + rmInfo.SellingFare.ToString() + "</li>";
                                //                totalRoomSellRate += rmInfo.SellingFare;
                                //            }
                                //            else
                                //            {
                                //                lblMultiRoomSelect.Text += "  <li>" + rmInfo.Totalfare.ToString() + "</li>";
                                //                totalRoomSellRate += rmInfo.Totalfare;

                                //                if (resultObj.Currency != "AED")
                                //                {
                                //                    totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                //                }
                                //            }
                                //        }


                                //        lblMultiRoomSelect.Text += "</ul></div>";
                                //    }
                                //    lblMultiRoomSelect.Text += "<div class='sum_totalhotel'><ul><li>Total</li>";
                                //    lblMultiRoomSelect.Text += "<li><b>" + totalRoomSellRate.ToString() + "</b></li>";
                                //    lblMultiRoomSelect.Text += "</ul>";
                                //    if (roomResult.Discount > 0)
                                //    {
                                //        lblMultiRoomSelect.Text += "<ul><li>Discount</li> ";
                                //        lblMultiRoomSelect.Text += "<li><b>" + (roomResult.Discount).ToString() + "</b></li></ul>";
                                //    }
                                //    lblMultiRoomSelect.Text += "<ul><li>Tax</li>";
                                //    lblMultiRoomSelect.Text += " <li><b>" + (roomResult.TotalTax).ToString() + "</b></li></ul>";

                                //lblMultiRoomSelect.Text += "<ul><li>Extra Guest Charge</li> ";

                                //    decimal extraGuestRate = 0;


                                //    extraGuestRate = roomResult.SellExtraGuestCharges;


                                //    lblMultiRoomSelect.Text += " <li><b>" + (extraGuestRate).ToString() + "</b></li></ul>";


                                //lblMultiRoomSelect.Text += "<ul><li>Total Price</li> ";


                                //    decimal tempTotalP = 0;
                                //    if (resultObj.BookingSource != HotelBookingSource.RezLive)
                                //    {
                                //        tempTotalP = (roomResult.SellingFare + roomResult.TotalTax + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length) - roomResult.Discount);
                                //    }
                                //    else
                                //    {
                                //        tempTotalP = (roomResult.TotalPrice + roomResult.TotalTax + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length) - roomResult.Discount);
                                //    }


                                //    lblMultiRoomSelect.Text += " <li><b>" + (tempTotalP.ToString()) + "</b></li></ul></div></div></td></tr>";
                                //    //lblMultiRoomSelect.Text += "</div><div class='close_window' ><span><a class='hand' onclick=\"javascript:hide('fareP" + y + "" + roomResult.RoomTypeCode + "" + roomResult.RatePlanCode + "')\">Close</a></span></div></div></td></tr>";
                                //    if (resultObj.BookingSource == HotelBookingSource.TBOConnect)
                                //    {
                                //        lblMultiRoomSelect.Text += " <tr><td colspan='3'>";
                                //        lblMultiRoomSelect.Text += "<div style='text-align:left; padding:4px; display:none' id='cancelP" + y + "''" + roomResult.RoomTypeCode + "''" + roomResult.RatePlanCode + "'>" + resultObj.RoomDetails[y].CancellationPolicy + "</div>";
                                //        lblMultiRoomSelect.Text += " </td></tr><tr><td colspan='3'>";
                                //        lblMultiRoomSelect.Text += "<div style='text-align:left; padding:4px; display:none' id='showRoomDetP" + y + "''" + roomResult.RoomTypeCode + "''" + roomResult.RatePlanCode + "'></div>";
                                //        lblMultiRoomSelect.Text += "<span><a class='hand' style='display:none' onclick=\"javascript:hide('showRoomDetP" + y + "''" + roomResult.RoomTypeCode + "''" + roomResult.RatePlanCode + "')\">Close</a></span>";
                                //        lblMultiRoomSelect.Text += "</td></tr>";
                                //    }
                                //}
                            } 
                            #endregion
                            # region Rezlive
                            else if (resultObj.BookingSource == HotelBookingSource.RezLive)
                            {
                               
                                //if (resultObj.RoomDetails[y].SequenceNo.Contains("|"))
                                //{
                                //    if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo.Split('|')[x]) == request.RoomGuest[x].noOfAdults)
                                //    {
                                        
                                //        double totalSellRate = 0;
                                //        double totalPubRate = 0;
                                //        HotelRoomsDetails roomResult = resultObj.RoomDetails[y];
                                //        if (roomResult.RoomTypeName.Contains("|"))
                                //        {
                                //            string[] names = roomResult.RoomTypeName.Split('|');

                                //            totalSellRate = (Convert.ToDouble((roomResult.SellingFare / names.Length) + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax ));
                                //            //totalPubRate = totalSellRate + (totalSellRate - Convert.ToDouble(roomResult.SellExtraGuestCharges) - Convert.ToDouble(roomResult.ChildCharges)) * Convert.ToDouble(resultObj.Price.AgentCommission) / 100;
                                //            if (resultObj.Currency == "USD")
                                //            {
                                //                totalSellRate = totalSellRate * (double)rateOfEx[resultObj.Currency];
                                //            }
                                //        }
                                //        else
                                //        {
                                //            totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax ) / request.NoOfRooms);
                                //            //totalPubRate = totalSellRate + (totalSellRate - Convert.ToDouble(roomResult.SellExtraGuestCharges) - Convert.ToDouble(roomResult.ChildCharges)) * Convert.ToDouble(resultObj.Price.AgentCommission) / 100;
                                //            if (resultObj.Currency == "USD")
                                //            {
                                //                totalSellRate = totalSellRate * (double)rateOfEx[resultObj.Currency];
                                //            }
                                //        }

                                //        lblMultiRoomSelect.Text += "<tr>";

                                //        lblMultiRoomSelect.Text += " <td>";
                                //        lblMultiRoomSelect.Text += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                //        if (firstRoom == 0)
                                //            lblMultiRoomSelect.Text += "checked=\'checked\'";
                                //        else
                                //            lblMultiRoomSelect.Text += "";
                                //        lblMultiRoomSelect.Text += " onclick=\"javascript:SelectRoomType(\'" + e.Item.ItemIndex + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + isTourico + "\');\"/>";
                                //        if (roomResult.RoomTypeName.Split('|').Length > 0)
                                //        {
                                //            lblMultiRoomSelect.Text += "<b>" + (roomResult.RoomTypeName.Contains("|") ? roomResult.RoomTypeName.Split('|')[x] : roomResult.RoomTypeName) + " Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</b>";
                                //        }
                                //        else
                                //        {
                                //            lblMultiRoomSelect.Text += "<b>" + roomResult.RoomTypeName + " - " + roomResult.RoomTypeCode + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only");
                                //        }
                                //        if (roomResult.Occupancy.ContainsKey("ExtraBed") && roomResult.Occupancy["ExtraBed"] == 1)
                                //        {
                                //            lblMultiRoomSelect.Text += " + &nbsp;&nbsp;<b>Extra Bed available</b>";
                                //        }
                                //        if (request.NoOfRooms == 1)
                                //        {
                                //            lblMultiRoomSelect.Text += "<input  type='hidden' name='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' id='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                //        }
                                //        else
                                //        {
                                //            lblMultiRoomSelect.Text += "<input  type='hidden' name='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' id='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                //        }
                                //        lblMultiRoomSelect.Text += "<input  type='hidden' name='rSource" + e.Item.ItemIndex + "-" + x + "' id='rSource" + e.Item.ItemIndex + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                //        lblMultiRoomSelect.Text += "<div class='room_amenities' style='display: none;text-align:left;' id='AmenityDiv-" + e.Item.ItemIndex.ToString() + y + "'>";
                                //        //lblMultiRoomSelect.Text += "<span id='amenityClo' title='close' style='font-size: 24px; cursor: pointer; position: absolute;right: 20px; top: 10px' onclick=\"hideAmenities('AmenityDiv-"+e.Item.ItemIndex.ToString() + y + "')\"><a href='#'><em class='close_button_cancellation'><img src='Images/close.gif' alt='Close' /></em></a></span>";
                                        
                                //        //if (hotelDetails.RoomFacilities != null && hotelDetails.RoomFacilities.Count > 0)
                                //        //{
                                //        //    lblMultiRoomSelect.Text += "<b>Room Amenities</b>:</br><ul>";
                                            
                                //        //    foreach (string amenity in hotelDetails.RoomFacilities)
                                //        //    {
                                //        //        lblMultiRoomSelect.Text += "<li>" + amenity + "</li>";
                                //        //    }
                                //        //    lblMultiRoomSelect.Text += "</ul>";
                                //        //}

                                //        lblMultiRoomSelect.Text += "</div>";
                                //        lblMultiRoomSelect.Text += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + e.Item.ItemIndex.ToString() + y + "','" + e.Item.ItemIndex + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                //        lblMultiRoomSelect.Text += "</div></td>";
                                //        lblMultiRoomSelect.Text += "<td align='center'><div class='style1'>AED " + Math.Round(totalSellRate,2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</div>";

                                //        lblMultiRoomSelect.Text += "<span onclick=\"javascript:ShowFare('fareP" + e.Item.ItemIndex.ToString() + y + "','" + e.Item.ItemIndex + "','" + x + "','" + y + "')\"><a style=' line-height:20px; ' href='#'>Rate Breakup</a></span>";

                                //        #region Fare Breakup
                                //        lblMultiRoomSelect.Text += "</div>      <div class='links'>";

                                //        //lblMultiRoomSelect.Text += "</div></td></tr><tr>";

                                //        DateTime d1 = resultObj.StartDate;
                                //        DateTime d2 = resultObj.EndDate;
                                //        d2 = d2.AddDays(-1);
                                //        int result = 0;
                                //        // Get first full week
                                //        DateTime firstDayOfFirstFullWeek = d1;
                                //        if (d1.DayOfWeek != DayOfWeek.Sunday)
                                //        {
                                //            result += 1;
                                //            firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                //        }
                                //        // Get last full week
                                //        DateTime lastDayOfLastFullWeek = d2;
                                //        if (d2.DayOfWeek != DayOfWeek.Saturday)
                                //        {
                                //            result += 1;
                                //            lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                //        }
                                //        System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                //        // Add number of full weeks
                                //        result += (diffResult.Days + 1) / 7;



                                //        ///lblMultiRoomSelect.Text += "</tr><tr><td colspan='3'>";
                                //        lblMultiRoomSelect.Text += "<div style='width: 400px; background:#fff; float: left; border: solid 4px #ccc; display:none; z-index:100; margin:0px 0 0 -200px;' class='fare_breakup_popup' id='fareP" + e.Item.ItemIndex.ToString() + y + "' >";
                                //        lblMultiRoomSelect.Text += "<div class='showMsgHeading'>Rate Breakup </div>";
                                //        lblMultiRoomSelect.Text += "<div class='week_days'>";
                                //        lblMultiRoomSelect.Text += "<ul><li>Sun</li><li>Mon</li><li>Tue</li><li>Wed</li><li>Thu</li><li>Fri</li><li>Sat</li></ul></div>";
                                //        d1 = resultObj.StartDate;
                                //        d2 = resultObj.EndDate;
                                //        decimal totalRoomSellRate = 0;
                                //        int days = d2.Subtract(d1).Days;
                                //        for (int i = 0, j = 0; i < result; i++)
                                //        {
                                //            int week = i + 1;
                                //            lblMultiRoomSelect.Text += "<div class='week_number'>";
                                //            lblMultiRoomSelect.Text += "<ul><li class='week_sno'>Week " + week + "</li>";
                                //            int l = 0;
                                //            if (d1.DayOfWeek != DayOfWeek.Sunday)
                                //            {
                                //                int predeser = Convert.ToInt16(d1.DayOfWeek);
                                //                for (; l < predeser; l++)
                                //                {
                                //                    lblMultiRoomSelect.Text += "<li>  </li>";
                                //                }
                                //            }

                                //            for (; l < 7 && d1 < d2; l++)
                                //            {
                                //                RoomRates rmInfo = new RoomRates();
                                //                //if (roomResult.Rates.Length > j)
                                //                {
                                //                    rmInfo = roomResult.Rates[j++];
                                //                }
                                //                d1 = d1.AddDays(1);
                                //                if (resultObj.BookingSource != HotelBookingSource.RezLive)
                                //                {
                                //                    lblMultiRoomSelect.Text += "  <li>" + Math.Round(rmInfo.SellingFare,2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                //                    totalRoomSellRate += rmInfo.SellingFare;
                                //                }
                                //                else
                                //                {
                                //                    if (roomResult.RoomTypeName.Contains("|"))
                                //                    {
                                //                        string[] names = roomResult.RoomTypeName.Split('|');
                                //                        decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                //                        lblMultiRoomSelect.Text += "  <li style='color: white; width:50px;'>" + Math.Round((rate),2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                //                        if (request.NoOfRooms > 1)
                                //                        {
                                //                            totalRoomSellRate += rate;
                                //                        }
                                //                        else
                                //                        {
                                //                            totalRoomSellRate = rate;
                                //                        }

                                //                        if (resultObj.Currency != "AED")
                                //                        {
                                //                            totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                //                        }
                                //                    }
                                //                    else
                                //                    {
                                //                        lblMultiRoomSelect.Text += "  <li>" + Math.Round((totalSellRate / days),2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                //                        if (request.NoOfRooms > 1)
                                //                        {
                                //                            totalRoomSellRate += ((decimal)totalSellRate / days);
                                //                        }
                                //                        else
                                //                        {
                                //                            totalRoomSellRate = ((decimal)totalSellRate / days);
                                //                        }
                                //                        if (resultObj.Currency != "AED")
                                //                        {
                                //                            totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                //                        }
                                //                    }
                                //                }
                                //            }


                                //            lblMultiRoomSelect.Text += "</ul></div>";
                                //        }
                                //        lblMultiRoomSelect.Text += "<div class='sum_totalhotel'><ul><li>Total : ";
                                //        lblMultiRoomSelect.Text += "<b>" + Math.Round(totalRoomSellRate,2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li>";
                                //        lblMultiRoomSelect.Text += "</ul>";
                                //        //if (roomResult.Discount > 0)
                                //        //{
                                //        //    lblMultiRoomSelect.Text += "<ul><li>Discount</li> ";
                                //        //    lblMultiRoomSelect.Text += "<li><b>" + (roomResult.Discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                //        //}
                                //        //lblMultiRoomSelect.Text += "<ul><li>Tax</li>";
                                //        //lblMultiRoomSelect.Text += " <li><b>" + (roomResult.TotalTax).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";

                                //        lblMultiRoomSelect.Text += "<ul><li>Extra Guest Charge : ";

                                //        decimal extraGuestRate = 0;


                                //        extraGuestRate = roomResult.SellExtraGuestCharges;


                                //        lblMultiRoomSelect.Text += "<b>" + Math.Round(extraGuestRate,2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";


                                //        lblMultiRoomSelect.Text += "<ul><li>Total Price : ";


                                //        decimal tempTotalP = 0;
                                //        if (resultObj.BookingSource != HotelBookingSource.RezLive)
                                //        {
                                //            tempTotalP = (roomResult.SellingFare + roomResult.TotalTax + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length));
                                //        }
                                //        else
                                //        {
                                //            if (roomResult.RoomTypeName.Contains("|"))
                                //            {
                                //                string[] names = roomResult.RoomTypeName.Split('|');
                                //                if (request.NoOfRooms > 1)
                                //                {
                                //                    tempTotalP = ((roomResult.SellingFare / names.Length) + roomResult.TotalTax + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length) );
                                //                }
                                //                else
                                //                {
                                //                    tempTotalP = ((roomResult.SellingFare / names.Length) + roomResult.TotalTax + (roomResult.SellExtraGuestCharges) );
                                //                }
                                //            }
                                //            else
                                //            {
                                //                if (request.NoOfRooms > 1)
                                //                {
                                //                    tempTotalP = ((roomResult.SellingFare / request.NoOfRooms) + roomResult.TotalTax + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length) );
                                //                }
                                //                else
                                //                {
                                //                    tempTotalP = (roomResult.SellingFare + roomResult.TotalTax + (roomResult.SellExtraGuestCharges) );
                                //                }
                                //            }
                                //        }


                                //        lblMultiRoomSelect.Text += "<b>" + Math.Round(tempTotalP,2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                //        lblMultiRoomSelect.Text += "<a style='position:absolute; top: 5px; right:10px' class='closex' onclick=\"javascript:hideFare('fareP" + e.Item.ItemIndex.ToString() + y + "')\">X</a></div>";
                                //        #endregion
                                //        lblMultiRoomSelect.Text += "</td><td>";

                                //        if (firstRoom == 0)
                                //        {
                                //            lblMultiRoomSelect.Text += " <script type='text/javascript'>";
                                //            lblMultiRoomSelect.Text += " document.getElementById('rCode" + e.Item.ItemIndex + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                //            lblMultiRoomSelect.Text += " document.getElementById('rCodeDefault" + e.Item.ItemIndex + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                //            if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                //            {
                                //                lblMultiRoomSelect.Text += " document.getElementById('priceTag" + e.Item.ItemIndex + "-" + x + "').value = '" + totalSellRate + "';";
                                //            }
                                //            else
                                //            {
                                //                lblMultiRoomSelect.Text += " document.getElementById('priceTag" + e.Item.ItemIndex + "-" + x + "').value = '" + totalPubRate + "';";
                                //            }
                                //            lblMultiRoomSelect.Text += "</script>";
                                //        }                                       
                                        
                                //        firstRoom++;
                                //        string roomTypeCode = "";
                                //        if (roomResult.RoomTypeCode.Length <= 0)
                                //        {
                                //            roomTypeCode = e.Item.ItemIndex.ToString() + x.ToString();
                                //        }
                                //        else
                                //        {
                                //            roomTypeCode = roomResult.RoomTypeCode;
                                //        }                  

                                //        lblMultiRoomSelect.Text += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;' id='room" + roomTypeCode + "-" + y.ToString() + "'></div> ";
                                //        lblMultiRoomSelect.Text += "<span class='fright width-140' onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomTypeCode + "-" + y.ToString() + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + e.Item.ItemIndex + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></div></td>";

                                //    }
                                //}
                                //else if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo) == request.RoomGuest[x].noOfAdults)
                                //{
                                   
                                //    double totalSellRate = 0;
                                //    double totalPubRate = 0;
                                //    HotelRoomsDetails roomResult = resultObj.RoomDetails[y];
                                //    if (roomResult.RoomTypeName.Contains("|"))
                                //    {
                                //        string[] names = roomResult.RoomTypeName.Split('|');

                                //        totalSellRate = (Convert.ToDouble((roomResult.SellingFare / names.Length) + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax ));

                                //        //totalPubRate = totalSellRate + (totalSellRate - Convert.ToDouble(roomResult.SellExtraGuestCharges) - Convert.ToDouble(roomResult.ChildCharges)) * Convert.ToDouble(resultObj.Price.AgentCommission) / 100;
                                //        if (resultObj.Currency == "USD")
                                //        {
                                //            totalSellRate = totalSellRate * (double)rateOfEx[resultObj.Currency];
                                //        }
                                //    }
                                //    else
                                //    {
                                //        totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax ) / request.NoOfRooms);
                                //        //totalPubRate = totalSellRate + (totalSellRate - Convert.ToDouble(roomResult.SellExtraGuestCharges) - Convert.ToDouble(roomResult.ChildCharges)) * Convert.ToDouble(resultObj.Price.AgentCommission) / 100;
                                //        if (resultObj.Currency == "USD")
                                //        {
                                //            totalSellRate = totalSellRate * (double)rateOfEx[resultObj.Currency];
                                //        }
                                //    }

                                //    lblMultiRoomSelect.Text += "    <tr>";



                                //    lblMultiRoomSelect.Text += " <td>";
                                //    lblMultiRoomSelect.Text += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                //    if (firstRoom == 0)
                                //        lblMultiRoomSelect.Text += "checked=\'checked\'";
                                //    else
                                //        lblMultiRoomSelect.Text += "";
                                //    lblMultiRoomSelect.Text += " onclick=\"javascript:SelectRoomType(\'" + e.Item.ItemIndex + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + isTourico + "\');\"/>";

                                //    if (roomResult.RoomTypeName.Split('|').Length > 0)
                                //    {
                                //        lblMultiRoomSelect.Text += "<b>" + (roomResult.RoomTypeName.Contains("|") ? roomResult.RoomTypeName.Split('|')[x] : roomResult.RoomTypeName) + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                //    }
                                //    else
                                //    {
                                //        lblMultiRoomSelect.Text += "<b>" + roomResult.RoomTypeName + " - " + roomResult.RoomTypeCode + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                //    }

                                //    if (roomResult.Occupancy.ContainsKey("ExtraBed") && roomResult.Occupancy["ExtraBed"] == 1)
                                //    {
                                //        lblMultiRoomSelect.Text += " + &nbsp;&nbsp;<b>Extra Bed available</b>";
                                //    }


                                //    if (request.NoOfRooms == 1)
                                //    {
                                //        lblMultiRoomSelect.Text += "<input  type='hidden' name='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' id='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                //    }
                                //    else
                                //    {
                                //        lblMultiRoomSelect.Text += "<input  type='hidden' name='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' id='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                //    }
                                //    lblMultiRoomSelect.Text += "<input  type='hidden' name='rSource" + e.Item.ItemIndex + "-" + x + "' id='rSource" + e.Item.ItemIndex + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                //    lblMultiRoomSelect.Text += "<div class='room_amenities' style='display: none;text-align:left;' id='AmenityDiv-" + e.Item.ItemIndex.ToString() + y + "'>";
                                    
                                //    lblMultiRoomSelect.Text += "</div>";
                                //    lblMultiRoomSelect.Text += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + e.Item.ItemIndex.ToString() + y + "','" + e.Item.ItemIndex + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                //    lblMultiRoomSelect.Text += "</div></td>";
                                //    lblMultiRoomSelect.Text += "<td align='center'><div class='style1'>AED " + Math.Round(totalSellRate,2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</div>";
                                //    lblMultiRoomSelect.Text += "<span onclick=\"javascript:ShowFare('fareP" + e.Item.ItemIndex.ToString() + y + "','" + e.Item.ItemIndex + "','" + x + "','" + y + "')\"><a style=' line-height:20px; ' href='#'>Rate Breakup</a></span>";

                                //    #region Fare Breakup
                                //    lblMultiRoomSelect.Text += "</div>      <div class='links'>";

                                //    //lblMultiRoomSelect.Text += "</div></td></tr><tr>";

                                //    DateTime d1 = resultObj.StartDate;
                                //    DateTime d2 = resultObj.EndDate;
                                //    d2 = d2.AddDays(-1);
                                //    int result = 0;
                                //    // Get first full week
                                //    DateTime firstDayOfFirstFullWeek = d1;
                                //    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                //    {
                                //        result += 1;
                                //        firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                //    }
                                //    // Get last full week
                                //    DateTime lastDayOfLastFullWeek = d2;
                                //    if (d2.DayOfWeek != DayOfWeek.Saturday)
                                //    {
                                //        result += 1;
                                //        lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                //    }
                                //    System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                //    // Add number of full weeks
                                //    result += (diffResult.Days + 1) / 7;



                                //    //lblMultiRoomSelect.Text += "</tr><tr><td colspan='3'>";
                                //    lblMultiRoomSelect.Text += "<div style='width: 400px; background: #fff; float: left; display:none;z-index:100; border: solid 4px #ccc; margin:0px 0 0 -200px;' class='fare_breakup_popup' id='fareP" + e.Item.ItemIndex.ToString() + y + "' >";
                                //    lblMultiRoomSelect.Text += "<div class='showMsgHeading'>Rate Breakup</div>";
                                //    lblMultiRoomSelect.Text += "<div class='week_days'>";
                                //    lblMultiRoomSelect.Text += "<ul><li>Sun</li><li>Mon</li><li>Tue</li><li>Wed</li><li>Thu</li><li>Fri</li><li>Sat</li></ul></div>";
                                //    d1 = resultObj.StartDate;
                                //    d2 = resultObj.EndDate;
                                //    decimal totalRoomSellRate = 0;
                                //    int days = d2.Subtract(d1).Days;
                                //    for (int i = 0, j = 0; i < result; i++)
                                //    {
                                //        int week = i + 1;
                                //        lblMultiRoomSelect.Text += "<div class='week_number'>";
                                //        lblMultiRoomSelect.Text += "<ul><li class='week_sno'>Week " + week + "</li>";
                                //        int l = 0;
                                //        if (d1.DayOfWeek != DayOfWeek.Sunday)
                                //        {
                                //            int predeser = Convert.ToInt16(d1.DayOfWeek);
                                //            for (; l < predeser; l++)
                                //            {
                                //                lblMultiRoomSelect.Text += "<li>  </li>";
                                //            }
                                //        }

                                //        for (; l < 7 && d1 < d2; l++)
                                //        {
                                //            RoomRates rmInfo = new RoomRates();
                                //            j++;
                                //            if (roomResult.Rates.Length > j)
                                //            {
                                //                rmInfo = roomResult.Rates[j];
                                //            }
                                //            else
                                //            {
                                //                rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                //            }
                                //            d1 = d1.AddDays(1);
                                //            if (resultObj.BookingSource != HotelBookingSource.RezLive)
                                //            {
                                //                lblMultiRoomSelect.Text += "  <li>" + Math.Round(rmInfo.SellingFare, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                //                totalRoomSellRate += rmInfo.SellingFare;
                                //            }
                                //            else
                                //            {

                                //                if (roomResult.RoomTypeName.Contains("|"))
                                //                {
                                //                    string[] names = roomResult.RoomTypeName.Split('|');
                                //                    decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                //                    lblMultiRoomSelect.Text += "  <li>" + Math.Round(rate, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                //                    totalRoomSellRate += rate;

                                //                    if (resultObj.Currency != "AED")
                                //                    {
                                //                        totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                //                    }
                                //                }
                                //                else
                                //                {
                                //                    lblMultiRoomSelect.Text += "  <li>" + Math.Round(totalSellRate / days, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                //                    totalRoomSellRate += ((decimal)totalSellRate / days);

                                //                    if (resultObj.Currency != "AED")
                                //                    {
                                //                        totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                //                    }
                                //                }
                                //            }
                                //        }


                                //        lblMultiRoomSelect.Text += "</ul></div>";
                                //    }
                                //    lblMultiRoomSelect.Text += "<div class='sum_totalhotel'><ul><li>Total : ";
                                //    lblMultiRoomSelect.Text += "<b>" + Math.Round(totalRoomSellRate, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li>";
                                //    lblMultiRoomSelect.Text += "</ul>";
                                //    //if (roomResult.Discount > 0)
                                //    //{
                                //    //    lblMultiRoomSelect.Text += "<ul><li>Discount</li> ";
                                //    //    lblMultiRoomSelect.Text += "<li><b>" + (roomResult.Discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                //    //}
                                //    //lblMultiRoomSelect.Text += "<ul><li>Tax</li>";
                                //    //lblMultiRoomSelect.Text += " <li><b>" + (roomResult.TotalTax).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";

                                //    lblMultiRoomSelect.Text += "<ul><li>Extra Guest Charge : ";

                                //    decimal extraGuestRate = 0;


                                //    extraGuestRate = roomResult.SellExtraGuestCharges;


                                //    lblMultiRoomSelect.Text += "<b>" + (extraGuestRate).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";


                                //    lblMultiRoomSelect.Text += "<ul><li>Total Price : ";


                                //    decimal tempTotalP = 0;
                                //    if (resultObj.BookingSource != HotelBookingSource.RezLive)
                                //    {
                                //        tempTotalP = (roomResult.SellingFare + roomResult.TotalTax + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length) );
                                //    }
                                //    else
                                //    {
                                //        if (roomResult.RoomTypeName.Contains("|"))
                                //        {
                                //            string[] names = roomResult.RoomTypeName.Split('|');
                                //            if (request.NoOfRooms > 1)
                                //            {
                                //                tempTotalP = ((roomResult.SellingFare / names.Length) + roomResult.TotalTax + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length) );
                                //            }
                                //            else
                                //            {
                                //                tempTotalP = ((roomResult.SellingFare / names.Length) + roomResult.TotalTax + (roomResult.SellExtraGuestCharges) );
                                //            }
                                //        }
                                //        else
                                //        {
                                //            if (request.NoOfRooms > 1)
                                //            {
                                //                tempTotalP = ((roomResult.SellingFare / request.NoOfRooms) + roomResult.TotalTax + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length) );
                                //            }
                                //            else
                                //            {
                                //                tempTotalP = (roomResult.SellingFare + roomResult.TotalTax + (roomResult.SellExtraGuestCharges) );
                                //            }
                                //        }
                                //    }


                                //    lblMultiRoomSelect.Text += "<b>" + Math.Round(tempTotalP, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                //    lblMultiRoomSelect.Text += "<a style='cursor: pointer; position: absolute;right: 10px; top: 5px' class='closex' onclick=\"javascript:hideFare('fareP" + e.Item.ItemIndex.ToString() + y + "')\">X</a></div>";
                                //    #endregion

                                //    lblMultiRoomSelect.Text += "</td><td>";
                                //    if (firstRoom == 0)
                                //    {
                                //        lblMultiRoomSelect.Text += " <script type='text/javascript'>";
                                //        lblMultiRoomSelect.Text += " document.getElementById('rCode" + e.Item.ItemIndex + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                //        lblMultiRoomSelect.Text += " document.getElementById('rCodeDefault" + e.Item.ItemIndex + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                //        if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                //        {
                                //            lblMultiRoomSelect.Text += " document.getElementById('priceTag" + e.Item.ItemIndex + "-" + x + "').value = '" + totalSellRate + "';";
                                //        }
                                //        else
                                //        {
                                //            lblMultiRoomSelect.Text += " document.getElementById('priceTag" + e.Item.ItemIndex + "-" + x + "').value = '" + totalPubRate + "';";
                                //        }
                                //        lblMultiRoomSelect.Text += "</script>";
                                //    }
                                //    firstRoom++;

                                //    string roomTypeCode = "";
                                //    //if (roomResult.RoomTypeCode.Length <= 0)
                                //    {
                                //        roomTypeCode = e.Item.ItemIndex.ToString() + x.ToString();
                                //    }
                                //    //else
                                //    //{
                                //    //    roomTypeCode = roomResult.RoomTypeCode;
                                //    //}

                                //    lblMultiRoomSelect.Text += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;' id='room" + roomTypeCode + "-" + y.ToString() + "'></div> ";
                                //    lblMultiRoomSelect.Text += "<span class='fright width-140' onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomTypeCode + "-" + y.ToString() + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + e.Item.ItemIndex + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></div></td>";

                                //}
                            }
                            # endregion
                            # region LOH
                            else if (resultObj.BookingSource == HotelBookingSource.LOH)
                            {
                                //if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Split(',').Length > 1)
                                //{
                                //    for (int k = 0; k < resultObj.RoomDetails[y].SequenceNo.Split(',').Length; k++)
                                //    {
                                //        if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo.Split(',')[k]) == (x + 1))
                                //        {
                                //            double totalSellRate = 0;
                                //            double totalPubRate = 0;
                                //            HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                //            totalSellRate = (Convert.ToDouble(roomResult.SellingFare + resultObj.Price.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax));
                                            
                                //            if (resultObj.Currency == "USD")
                                //            {
                                //                totalSellRate = totalSellRate * (double)rateOfEx[resultObj.Currency];
                                //            }

                                //            lblMultiRoomSelect.Text += "    <tr>";
                                //            lblMultiRoomSelect.Text += " <td>";
                                //            lblMultiRoomSelect.Text += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                //            if (firstRoom == 0)
                                //                lblMultiRoomSelect.Text += "checked=\'checked\'";
                                //            else
                                //                lblMultiRoomSelect.Text += "";

                                //            if (x > 0)
                                //            {
                                //                lblMultiRoomSelect.Text += " disabled='disabled'";
                                //            }

                                //            lblMultiRoomSelect.Text += " onclick=\"javascript:SelectRoomType(\'" + e.Item.ItemIndex + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + isTourico + "\');\"/>";

                                //            if (roomResult.RoomTypeName.Split('|').Length > 0)
                                //            {
                                //                lblMultiRoomSelect.Text += "<b>" + (roomResult.RoomTypeName.Contains("|") ? roomResult.RoomTypeName.Split('|')[x] : roomResult.RoomTypeName) + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                //            }
                                //            else
                                //            {
                                //                lblMultiRoomSelect.Text += "<b>" + roomResult.RoomTypeName + " - " + roomResult.RoomTypeCode + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                //            }

                                //            if (roomResult.Occupancy != null && roomResult.Occupancy.ContainsKey("ExtraBed") && roomResult.Occupancy["ExtraBed"] == 1)
                                //            {
                                //                lblMultiRoomSelect.Text += " + &nbsp;&nbsp;<b>Extra Bed available</b>";
                                //            }

                                //            if (request.NoOfRooms == 1)
                                //            {
                                //                lblMultiRoomSelect.Text += "<input  type='hidden' name='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' id='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                //            }
                                //            else
                                //            {
                                //                lblMultiRoomSelect.Text += "<input  type='hidden' name='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' id='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                //            }
                                //            lblMultiRoomSelect.Text += "<input  type='hidden' name='rSource" + e.Item.ItemIndex + "-" + x + "' id='rSource" + e.Item.ItemIndex + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                //            if (roomResult.PromoMessage != null)
                                //            {
                                //                //lblMultiRoomSelect.Text += "<div style='position: relative; top: 20px; left: 10px; display: block'>";
                                //                lblMultiRoomSelect.Text += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                //                lblMultiRoomSelect.Text += "<label style='font-size: 11px; color: Red'>";
                                //                lblMultiRoomSelect.Text += roomResult.PromoMessage;
                                //                lblMultiRoomSelect.Text += "</label></td></tr></table>";
                                //            }

                                //            lblMultiRoomSelect.Text += "<div class='room_amenities' style='display: none;text-align:left;' id='AmenityDiv-" + e.Item.ItemIndex.ToString() + y + "'>";
                                           
                                //            lblMultiRoomSelect.Text += "</div>";
                                //            lblMultiRoomSelect.Text += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + e.Item.ItemIndex.ToString() + y + "','" + e.Item.ItemIndex + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                //            lblMultiRoomSelect.Text += "</div></td>";
                                //            lblMultiRoomSelect.Text += "<td align='center'><div class='style1'>AED " + Math.Round(totalSellRate, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</div>";
                                //            lblMultiRoomSelect.Text += "<span onclick=\"javascript:ShowFare('fareP" + e.Item.ItemIndex.ToString() + y + "','" + e.Item.ItemIndex + "','" + x + "','" + y + "')\"><a style=' line-height:20px; ' href='#'>Rate Breakup</a></span>";

                                //            #region Fare Breakup
                                //            lblMultiRoomSelect.Text += "</div>      <div class='links'>";

                                //            //lblMultiRoomSelect.Text += "</div></td></tr><tr>";

                                //            DateTime d1 = request.StartDate;
                                //            DateTime d2 = request.EndDate;
                                //            d2 = d2.AddDays(-1);
                                //            int result = 0;
                                //            // Get first full week
                                //            DateTime firstDayOfFirstFullWeek = d1;
                                //            if (d1.DayOfWeek != DayOfWeek.Sunday)
                                //            {
                                //                result += 1;
                                //                firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                //            }
                                //            // Get last full week
                                //            DateTime lastDayOfLastFullWeek = d2;
                                //            if (d2.DayOfWeek != DayOfWeek.Saturday)
                                //            {
                                //                result += 1;
                                //                lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                //            }
                                //            System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                //            // Add number of full weeks
                                //            result += (diffResult.Days + 1) / 7;



                                //            //lblMultiRoomSelect.Text += "</tr><tr><td colspan='3'>";
                                //            lblMultiRoomSelect.Text += "<div style='width: 400px; background: #fff;  float: left; display:none;z-index:100; margin:0px 0 0 -200px;' class='fare_breakup_popup' id='fareP" + e.Item.ItemIndex.ToString() + y + "' >";
                                //            lblMultiRoomSelect.Text += "<div class='showMsgHeading'>Rate Breakup</div>";
                                //            lblMultiRoomSelect.Text += "<div class='week_days'>";
                                //            lblMultiRoomSelect.Text += "<ul><li>Sun</li><li>Mon</li><li>Tue</li><li>Wed</li><li>Thu</li><li>Fri</li><li>Sat</li></ul></div>";
                                //            d1 = resultObj.StartDate;
                                //            d2 = resultObj.EndDate;
                                //            decimal totalRoomSellRate = 0;
                                //            int days = d2.Subtract(d1).Days;
                                //            for (int i = 0, j = 0; i < result; i++)
                                //            {
                                //                int week = i + 1;
                                //                lblMultiRoomSelect.Text += "<div class='week_number'>";
                                //                lblMultiRoomSelect.Text += "<ul><li class='week_sno'>Week " + week + "</li>";
                                //                int l = 0;
                                //                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                //                {
                                //                    int predeser = Convert.ToInt16(d1.DayOfWeek);
                                //                    for (; l < predeser; l++)
                                //                    {
                                //                        lblMultiRoomSelect.Text += "<li>  </li>";
                                //                    }
                                //                }

                                //                for (; l < 7 && d1 < d2; l++)
                                //                {
                                //                    RoomRates rmInfo = new RoomRates();
                                //                    j++;
                                //                    if (roomResult.Rates.Length > j)
                                //                    {
                                //                        rmInfo = roomResult.Rates[j];
                                //                    }
                                //                    else
                                //                    {
                                //                        rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                //                    }
                                //                    d1 = d1.AddDays(1);
                                //                    //if (resultObj.BookingSource != HotelBookingSource.RezLive)
                                //                    //{
                                //                    //    lblMultiRoomSelect.Text += "  <li>" + Math.Round(rmInfo.SellingFare, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                //                    //    totalRoomSellRate += rmInfo.SellingFare;
                                //                    //}
                                //                    //else
                                //                    {

                                //                        if (roomResult.RoomTypeName.Contains("|"))
                                //                        {
                                //                            string[] names = roomResult.RoomTypeName.Split('|');
                                //                            decimal rate = (rmInfo.SellingFare + roomResult.Markup / names.Length) / days;
                                //                            lblMultiRoomSelect.Text += "  <li>" + Math.Round(rate, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                //                            totalRoomSellRate += rate;

                                //                            if (resultObj.Currency != "AED")
                                //                            {
                                //                                totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                //                            }
                                //                        }
                                //                        else
                                //                        {
                                //                            lblMultiRoomSelect.Text += "  <li>" + Math.Round(totalSellRate / days, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                //                            totalRoomSellRate += ((decimal)totalSellRate / days);

                                //                            if (resultObj.Currency != "AED")
                                //                            {
                                //                                totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                //                            }
                                //                        }
                                //                    }
                                //                }


                                //                lblMultiRoomSelect.Text += "</ul></div>";
                                //            }
                                //            totalRoomSellRate += roomResult.Markup;
                                //            lblMultiRoomSelect.Text += "<div class='sum_totalhotel'><ul><li>Total : ";
                                //            lblMultiRoomSelect.Text += "<b>" + Math.Round(totalRoomSellRate, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li>";
                                //            lblMultiRoomSelect.Text += "</ul>";
                                //            //if (roomResult.Discount > 0)
                                //            //{
                                //            //    lblMultiRoomSelect.Text += "<ul><li>Discount</li> ";
                                //            //    lblMultiRoomSelect.Text += "<li><b>" + (roomResult.Discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                //            //}
                                //            //lblMultiRoomSelect.Text += "<ul><li>Tax</li>";
                                //            //lblMultiRoomSelect.Text += " <li><b>" + (roomResult.TotalTax).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";

                                //            lblMultiRoomSelect.Text += "<ul><li>Extra Guest Charge : ";

                                //            decimal extraGuestRate = 0;


                                //            extraGuestRate = roomResult.SellExtraGuestCharges;


                                //            lblMultiRoomSelect.Text += "<b>" + (extraGuestRate).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";


                                //            lblMultiRoomSelect.Text += "<ul><li>Total Price : ";


                                //            decimal tempTotalP = 0;
                                //            if (resultObj.BookingSource == HotelBookingSource.LOH)
                                //            {
                                //                tempTotalP = (roomResult.SellingFare + roomResult.Markup + roomResult.TotalTax );
                                //            }
                                //            //else
                                //            //{
                                //            //    if (roomResult.RoomTypeName.Contains("|"))
                                //            //    {
                                //            //        string[] names = roomResult.RoomTypeName.Split('|');
                                //            //        if (request.NoOfRooms > 1)
                                //            //        {
                                //            //            tempTotalP = ((roomResult.SellingFare / names.Length) +resultObj.Price.Markup + roomResult.TotalTax + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length));
                                //            //        }
                                //            //        else
                                //            //        {
                                //            //            tempTotalP = ((roomResult.SellingFare / names.Length) +resultObj.Price.Markup + roomResult.TotalTax + (roomResult.SellExtraGuestCharges));
                                //            //        }
                                //            //    }
                                //            //    else
                                //            //    {
                                //            //        if (request.NoOfRooms > 1)
                                //            //        {
                                //            //            tempTotalP = ((roomResult.SellingFare / request.NoOfRooms)+resultObj.Price.Markup + roomResult.TotalTax + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length));
                                //            //        }
                                //            //        else
                                //            //        {
                                //            //            tempTotalP = (roomResult.SellingFare + resultObj.Price.Markup + roomResult.TotalTax + (roomResult.SellExtraGuestCharges));
                                //            //        }
                                //            //    }
                                //            //}


                                //            lblMultiRoomSelect.Text += "<b>" + Math.Round(tempTotalP, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                //            lblMultiRoomSelect.Text += "<a style='position:absolute; top: 5px; right:10px' class='closex' onclick=\"javascript:hideFare('fareP" + e.Item.ItemIndex.ToString() + y + "')\">X</a></div>";
                                //            #endregion

                                //            lblMultiRoomSelect.Text += "</td><td>";
                                //            if (firstRoom == 0)
                                //            {
                                //                lblMultiRoomSelect.Text += " <script type='text/javascript'>";
                                //                lblMultiRoomSelect.Text += " document.getElementById('rCode" + e.Item.ItemIndex + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                //                lblMultiRoomSelect.Text += " document.getElementById('rCodeDefault" + e.Item.ItemIndex + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                //                if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                //                {
                                //                    lblMultiRoomSelect.Text += " document.getElementById('priceTag" + e.Item.ItemIndex + "-" + x + "').value = '" + totalSellRate + "';";
                                //                }
                                //                else
                                //                {
                                //                    lblMultiRoomSelect.Text += " document.getElementById('priceTag" + e.Item.ItemIndex + "-" + x + "').value = '" + totalPubRate + "';";
                                //                }
                                //                lblMultiRoomSelect.Text += "</script>";
                                //            }
                                //            firstRoom++;

                                //            string roomTypeCode = "";
                                //            //if (roomResult.RoomTypeCode.Length <= 0)
                                //            {
                                //                roomTypeCode = e.Item.ItemIndex.ToString() + x.ToString();
                                //            }
                                //            //else
                                //            //{
                                //            //    roomTypeCode = roomResult.RoomTypeCode;
                                //            //}

                                //            lblMultiRoomSelect.Text += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;' id='room" + roomTypeCode + "-" + y.ToString() + "'></div> ";
                                //            lblMultiRoomSelect.Text += "<span class='fright width-140' onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomTypeCode + "-" + y.ToString() + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + e.Item.ItemIndex + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></div></td>";
                                //        }
                                //    }
                                //}
                                //else if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo) == (x + 1))
                                //{
                                //    double totalSellRate = 0;
                                //    double totalPubRate = 0;
                                //    HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                //    totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax));
                                    
                                //    if (resultObj.Currency == "USD")
                                //    {
                                //        totalSellRate = totalSellRate * (double)rateOfEx[resultObj.Currency];
                                //    }

                                //    lblMultiRoomSelect.Text += "    <tr>";
                                //    lblMultiRoomSelect.Text += " <td>";
                                //    lblMultiRoomSelect.Text += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                //    if (firstRoom == 0)
                                //        lblMultiRoomSelect.Text += "checked=\'checked\'";
                                //    else
                                //        lblMultiRoomSelect.Text += "";

                                //    if (x > 0)
                                //    {
                                //        //lblMultiRoomSelect.Text += " disabled='disabled'";
                                //    }
                                //    lblMultiRoomSelect.Text += " onclick=\"javascript:SelectRoomType(\'" + e.Item.ItemIndex + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + isTourico + "\');\"/>";

                                //    if (roomResult.RoomTypeName.Split('|').Length > 0)
                                //    {
                                //        lblMultiRoomSelect.Text += "<b>" + (roomResult.RoomTypeName.Contains("|") ? roomResult.RoomTypeName.Split('|')[x] : roomResult.RoomTypeName) + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                //    }
                                //    else
                                //    {
                                //        lblMultiRoomSelect.Text += "<b>" + roomResult.RoomTypeName + " - " + roomResult.RoomTypeCode + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                //    }

                                //    if (roomResult.Occupancy != null && roomResult.Occupancy.ContainsKey("ExtraBed") && roomResult.Occupancy["ExtraBed"] == 1)
                                //    {
                                //        lblMultiRoomSelect.Text += " + &nbsp;&nbsp;<b>Extra Bed available</b>";
                                //    }

                                //    if (request.NoOfRooms == 1)
                                //    {
                                //        lblMultiRoomSelect.Text += "<input  type='hidden' name='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' id='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                //    }
                                //    else
                                //    {
                                //        lblMultiRoomSelect.Text += "<input  type='hidden' name='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' id='rPrice" + e.Item.ItemIndex.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                //    }
                                //    lblMultiRoomSelect.Text += "<input  type='hidden' name='rSource" + e.Item.ItemIndex + "-" + x + "' id='rSource" + e.Item.ItemIndex + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                //    if (roomResult.PromoMessage != null)
                                //    {
                                //        //lblMultiRoomSelect.Text += "<div style='position: relative; top: 20px; left: 10px; display: block'>";
                                //        lblMultiRoomSelect.Text += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                //        lblMultiRoomSelect.Text += "<label style='font-size: 11px; color: Red'>";
                                //        lblMultiRoomSelect.Text += roomResult.PromoMessage;
                                //        lblMultiRoomSelect.Text += "</label></td></tr></table>";
                                //    }

                                //    lblMultiRoomSelect.Text += "<div class='room_amenities' style='display: none;text-align:left;' id='AmenityDiv-" + e.Item.ItemIndex.ToString() + y + "'>";
                                   
                                //    lblMultiRoomSelect.Text += "</div>";
                                //    lblMultiRoomSelect.Text += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + e.Item.ItemIndex.ToString() + y + "','" + e.Item.ItemIndex + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                //    lblMultiRoomSelect.Text += "</div></td>";
                                //    lblMultiRoomSelect.Text += "<td align='center'><div class='style1'>AED " + Math.Round(totalSellRate, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</div>";
                                //    lblMultiRoomSelect.Text += "<span onclick=\"javascript:ShowFare('fareP" + e.Item.ItemIndex.ToString() + y + "','" + e.Item.ItemIndex + "','" + x + "','" + y + "')\"><a style=' line-height:20px; ' href='#'>Rate Breakup</a></span>";

                                //    #region Fare Breakup
                                //    lblMultiRoomSelect.Text += "</div>      <div class='links'>";

                                //    //lblMultiRoomSelect.Text += "</div></td></tr><tr>";

                                //    DateTime d1 = request.StartDate;
                                //    DateTime d2 = request.EndDate;
                                //    d2 = d2.AddDays(-1);
                                //    int result = 0;
                                //    // Get first full week
                                //    DateTime firstDayOfFirstFullWeek = d1;
                                //    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                //    {
                                //        result += 1;
                                //        firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                //    }
                                //    // Get last full week
                                //    DateTime lastDayOfLastFullWeek = d2;
                                //    if (d2.DayOfWeek != DayOfWeek.Saturday)
                                //    {
                                //        result += 1;
                                //        lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                //    }
                                //    System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                //    // Add number of full weeks
                                //    result += (diffResult.Days + 1) / 7;



                                //    //lblMultiRoomSelect.Text += "</tr><tr><td colspan='3'>";
                                //    lblMultiRoomSelect.Text += "<div style='width: 400px; background: #fff;  float: left; display:none;z-index:100; margin:0px 0 0 -200px;' class='fare_breakup_popup' id='fareP" + e.Item.ItemIndex.ToString() + y + "' >";
                                //    lblMultiRoomSelect.Text += "<div class='showMsgHeading'>Rate Breakup</div>";
                                //    lblMultiRoomSelect.Text += "<div class='week_days'>";
                                //    lblMultiRoomSelect.Text += "<ul><li>Sun</li><li>Mon</li><li>Tue</li><li>Wed</li><li>Thu</li><li>Fri</li><li>Sat</li></ul></div>";
                                //    d1 = resultObj.StartDate;
                                //    d2 = resultObj.EndDate;
                                //    decimal totalRoomSellRate = 0;
                                //    int days = d2.Subtract(d1).Days;
                                //    for (int i = 0, j = 0; i < result; i++)
                                //    {
                                //        int week = i + 1;
                                //        lblMultiRoomSelect.Text += "<div class='week_number'>";
                                //        lblMultiRoomSelect.Text += "<ul><li class='week_sno'>Week " + week + "</li>";
                                //        int l = 0;
                                //        if (d1.DayOfWeek != DayOfWeek.Sunday)
                                //        {
                                //            int predeser = Convert.ToInt16(d1.DayOfWeek);
                                //            for (; l < predeser; l++)
                                //            {
                                //                lblMultiRoomSelect.Text += "<li>  </li>";
                                //            }
                                //        }

                                //        for (; l < 7 && d1 < d2; l++)
                                //        {
                                //            RoomRates rmInfo = new RoomRates();
                                //            j++;
                                //            if (roomResult.Rates.Length > j)
                                //            {
                                //                rmInfo = roomResult.Rates[j];
                                //            }
                                //            else
                                //            {
                                //                rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                //            }
                                //            d1 = d1.AddDays(1);
                                //            //if (resultObj.BookingSource != HotelBookingSource.RezLive)
                                //            //{
                                //            //    lblMultiRoomSelect.Text += "  <li>" + Math.Round(rmInfo.SellingFare, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                //            //    totalRoomSellRate += rmInfo.SellingFare;
                                //            //}
                                //            //else
                                //            {

                                //                if (roomResult.RoomTypeName.Contains("|"))
                                //                {
                                //                    string[] names = roomResult.RoomTypeName.Split('|');
                                //                    decimal rate = (rmInfo.SellingFare + roomResult.Markup / names.Length) / days;
                                //                    lblMultiRoomSelect.Text += "  <li>" + Math.Round(rate, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                //                    totalRoomSellRate += rate;

                                //                    if (resultObj.Currency != "AED")
                                //                    {
                                //                        totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                //                    }
                                //                }
                                //                else
                                //                {
                                //                    lblMultiRoomSelect.Text += "  <li>" + Math.Round(totalSellRate / days, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                //                    totalRoomSellRate += ((decimal)totalSellRate / days);

                                //                    if (resultObj.Currency != "AED")
                                //                    {
                                //                        totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                //                    }
                                //                }
                                //            }
                                //        }


                                //        lblMultiRoomSelect.Text += "</ul></div>";
                                //    }
                                //    lblMultiRoomSelect.Text += "<div class='sum_totalhotel'><ul><li>Total : ";
                                //    lblMultiRoomSelect.Text += "<b>" + Math.Round(totalRoomSellRate, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li>";
                                //    lblMultiRoomSelect.Text += "</ul>";
                                //    //if (roomResult.Discount > 0)
                                //    //{
                                //    //    lblMultiRoomSelect.Text += "<ul><li>Discount</li> ";
                                //    //    lblMultiRoomSelect.Text += "<li><b>" + (roomResult.Discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                //    //}
                                //    //lblMultiRoomSelect.Text += "<ul><li>Tax</li>";
                                //    //lblMultiRoomSelect.Text += " <li><b>" + (roomResult.TotalTax).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";

                                //    lblMultiRoomSelect.Text += "<ul><li>Extra Guest Charge : ";

                                //    decimal extraGuestRate = 0;


                                //    extraGuestRate = roomResult.SellExtraGuestCharges;


                                //    lblMultiRoomSelect.Text += "<b>" + (extraGuestRate).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";


                                //    lblMultiRoomSelect.Text += "<ul><li>Total Price : ";


                                //    decimal tempTotalP = 0;
                                //    if (resultObj.BookingSource == HotelBookingSource.LOH)
                                //    {
                                //        tempTotalP = (roomResult.SellingFare + roomResult.Markup + roomResult.TotalTax );
                                //    }
                                //    //else
                                //    //{
                                //    //    if (roomResult.RoomTypeName.Contains("|"))
                                //    //    {
                                //    //        string[] names = roomResult.RoomTypeName.Split('|');
                                //    //        if (request.NoOfRooms > 1)
                                //    //        {
                                //    //            tempTotalP = (roomResult.SellingFare / names.Length) + resultObj.Price.Markup + roomResult.TotalTax;
                                //    //        }
                                //    //        else
                                //    //        {
                                //    //            tempTotalP = (roomResult.SellingFare / names.Length) + resultObj.Price.Markup + roomResult.TotalTax;
                                //    //        }
                                //    //    }
                                //    //    else
                                //    //    {
                                //    //        if (request.NoOfRooms > 1)
                                //    //        {
                                //    //            tempTotalP = (roomResult.SellingFare / request.NoOfRooms) + resultObj.Price.Markup + roomResult.TotalTax;
                                //    //        }
                                //    //        else
                                //    //        {
                                //    //            tempTotalP = (roomResult.SellingFare + resultObj.Price.Markup + roomResult.TotalTax);
                                //    //        }
                                //    //    }
                                //    //}


                                //    lblMultiRoomSelect.Text += "<b>" + Math.Round(tempTotalP, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                //    lblMultiRoomSelect.Text += "<a style='position:absolute; top: 5px; right:10px' class='closex' onclick=\"javascript:hideFare('fareP" + e.Item.ItemIndex.ToString() + y + "')\">X</a></div>";
                                //    #endregion

                                //    lblMultiRoomSelect.Text += "</td><td>";
                                //    if (firstRoom == 0)
                                //    {
                                //        lblMultiRoomSelect.Text += " <script type='text/javascript'>";
                                //        lblMultiRoomSelect.Text += " document.getElementById('rCode" + e.Item.ItemIndex + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                //        lblMultiRoomSelect.Text += " document.getElementById('rCodeDefault" + e.Item.ItemIndex + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                //        if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                //        {
                                //            lblMultiRoomSelect.Text += " document.getElementById('priceTag" + e.Item.ItemIndex + "-" + x + "').value = '" + totalSellRate + "';";
                                //        }
                                //        else
                                //        {
                                //            lblMultiRoomSelect.Text += " document.getElementById('priceTag" + e.Item.ItemIndex + "-" + x + "').value = '" + totalPubRate + "';";
                                //        }
                                //        lblMultiRoomSelect.Text += "</script>";
                                //    }
                                //    firstRoom++;

                                //    string roomTypeCode = "";
                                //    //if (roomResult.RoomTypeCode.Length <= 0)
                                //    {
                                //        roomTypeCode = e.Item.ItemIndex.ToString() + x.ToString();
                                //    }
                                //    //else
                                //    //{
                                //    //    roomTypeCode = roomResult.RoomTypeCode;
                                //    //}

                                //    lblMultiRoomSelect.Text += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;' id='room" + roomTypeCode + "-" + y.ToString() + "'></div> ";
                                //    lblMultiRoomSelect.Text += "<span class='fright width-140' onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomTypeCode + "-" + y.ToString() + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + e.Item.ItemIndex + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></div></td>";

                                //}
                            }
                            # endregion
                        }                        
                    }
                    if (resultObj.BookingSource != HotelBookingSource.DOTW)
                    {
                        //lblMultiRoomSelect.Text += "</table></div></td></tr><tr><td>&nbsp;</td></tr></table></div>";
                        //lblMultiRoomSelect.Text += "<div class='selectRoomR'><div class='padbot10'>";
                        //lblMultiRoomSelect.Text += "<a href=\"javascript:HideMRPopUp('" + e.Item.ItemIndex + "')\"><b>Choose other Hotel</b></a></div>";
                        //lblMultiRoomSelect.Text += "<div class='SelectRoomWidget'>";
                        ////lblMultiRoomSelect.Text += "<div class='Aedfff padbot10'><h1>AED " + Math.Round(totFare) + "</h1>";
                        ////lblMultiRoomSelect.Text += "<div class='perRoomNight'>Per Room Per Night</label></div>";
                        //lblMultiRoomSelect.Text += "<div class='checkInDate'><b>Check-In</b> " + request.StartDate.ToString("dd MMM yyyy") + "</div>";
                        //lblMultiRoomSelect.Text += "<div class='checkInDate'><b>Check-Out</b> " + request.EndDate.ToString("dd MMM yyyy") + "</div>";
                        //lblMultiRoomSelect.Text += "<div class='Aedfff padtop10'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";                        
                        ////lblMultiRoomSelect.Text += "    <div id = 'TotalSelectedFare" + e.Item.ItemIndex + "' >" + ConfigurationSystem.LocaleConfig["CurrencySign"] + " " + Math.Ceiling(Convert.ToDouble(totFare) * rateofExchange) + "</div>";
                        //lblMultiRoomSelect.Text += "<tr><td width='50%' align='right'><b>Total Rate</b></td><td width='50%' align='left'><table width='80%'><tr><td style='width:50%' align='right'><b>AED</b>&nbsp;</td><td><div id='TotalSelectedFare" + e.Item.ItemIndex + "' style='width:50%' >" + Math.Ceiling(totFare+discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</div></td></tr></table></td></tr>";
                        //if (discount > 0)
                        //{
                        //    lblMultiRoomSelect.Text += "<tr><td width='50%' align='right'><b>Discount</b></td><td width='50%' align='left'><table width='90%'><tr><td style='width:50%' align='right'><b>AED</b>&nbsp;</td><td><div id='DiscountFare" + e.Item.ItemIndex + "' style='width:50%' >" + Math.Ceiling(discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</div></td></tr></table></td></tr>";
                        //}
                        //lblMultiRoomSelect.Text += "<tr><td width='50%' align='right'><b>Grand Total</b></td><td width='50%' align='left'><table width='80%'><tr><td style='width:50%' align='right'><b>AED</b>&nbsp;</td><td><div id='TotalFare" + e.Item.ItemIndex + "' style='width:50%' >" + Math.Ceiling(totFare).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</div></td></tr></table></td></tr>";
                        //lblMultiRoomSelect.Text += "</table></div><div class='padbot10 bortopblue'><a class='buttonBookNow' href='#' id ='continueButton" + e.Item.ItemIndex + "' alt='continue'  onclick=\"submitForm('" + e.Item.ItemIndex + "','" + resultObj.RoomDetails.Length + "','" + resultObj.HotelCode + "');\"/><b>Book Now</b></a></div></div>";
                        //lblMultiRoomSelect.Text += "<div class='clear'></div></div><div class='clear'></div></div><div class='clear'></div></div></div></div>";
                    }
                    //if (resultObj.BookingSource == HotelBookingSource.DOTW)
                    {
                        ibtnBook.OnClientClick = "javascript:GetRooms('" + e.Item.ItemIndex + "','" + totFare + "','" + resultObj.HotelCode + "','" + resultObj.HotelName.Replace("'", "\\'") + "'); return false;";
                        //lblMultiRoomSelect.Text += "</div>";
                    }
                    //else
                    //{
                    //    ibtnBook.OnClientClick = "javascript:ShowModalPopUp('" + e.Item.ItemIndex + "'); return false;";
                    //}
                    int agencyId = 0;
                    if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        agencyId = Convert.ToInt32(Settings.LoginInfo.AgentId);
                        lblLowestPrice.Text = resultObj.Currency + " " + Math.Ceiling(totFare).ToString("N" + Settings.LoginInfo.DecimalValue);//ziya
                    }
                    else
                    {
                        agencyId = Settings.LoginInfo.OnBehalfAgentID; //Selected Agency
                        lblLowestPrice.Text = resultObj.Currency + " " + Math.Ceiling(totFare).ToString("N" + Settings.LoginInfo.OnBehalfAgentDecimalValue);//ziya
                    }
                    
                    
                    ibtnBook.UseSubmitBehavior = false;
                    
                    //lblLowestPrice.Text = Math.Round(totFare, MidpointRounding.ToEven).ToString();
                    lblFare.Visible = false;
                    lblFareSummary.Visible = false;
                }
                #region Old Single Room Code
                //else
                //{
                //    lblFareSummary.Visible = true;
                //    lblFare.Visible = true;

                //    //for (int i = 0; i < resultObj.RoomDetails.Length; i++)
                //    //{
                //        HtmlTableRow row = new HtmlTableRow();
                //        HtmlTableCell cell = new HtmlTableCell();
                //        HtmlTableCell cell1 = new HtmlTableCell();
                //        HtmlTableCell cell2 = new HtmlTableCell();
                //        HtmlTableCell cell3 = new HtmlTableCell();


                //        decimal total = 0;
                //        if (resultObj.BookingSource != HotelBookingSource.RezLive)
                //        {
                //            total = resultObj.RoomDetails[0].SellingFare + resultObj.RoomDetails[0].TotalTax - resultObj.RoomDetails[0].Discount + resultObj.RoomDetails[0].SellExtraGuestCharges * resultObj.RoomDetails[0].Rates.Length;
                //        }
                //        else
                //        {
                //            total = resultObj.RoomDetails[0].TotalPrice + resultObj.RoomDetails[0].TotalTax - resultObj.RoomDetails[0].Discount + resultObj.RoomDetails[0].SellExtraGuestCharges * resultObj.RoomDetails[0].Rates.Length;
                //            if (resultObj.Currency == "USD")
                //            {                                
                //                total = total * Convert.ToDecimal(3.67);
                //            }                            
                //        }
                //        lblLowestPrice.Text = (Math.Ceiling(total)).ToString();
                //        //cell.InnerHtml += "<input type='radio' id='RoomChoice" + e.Item.ItemIndex + i.ToString() + "'" + (i==0 ? "checked='checked'" : "") + " name='" + e.Item.ItemIndex + "' onclick=\"SelectRoom('" + e.Item.ItemIndex + "','" + 0 + "','" + resultObj.RoomDetails.Length + "','" + resultObj.RoomDetails[0].RoomTypeCode + "','" + lblLowestPrice.Text + "','" + resultObj.HotelCode + "','" + lblLowestPrice.ClientID + "')\";/>";
                //        if (resultObj.BookingSource == HotelBookingSource.RezLive)
                //        {
                //            cell.InnerHtml += resultObj.RoomDetails[0].RoomTypeName + " - " + resultObj.RoomDetails[0].RoomTypeCode + " (Adult(s) - " + request.RoomGuest[0].noOfAdults + ")";
                //        }
                //        else
                //        {
                //            cell.InnerHtml += resultObj.RoomDetails[0].RoomTypeName + " (Adult(s) - " + request.RoomGuest[0].noOfAdults + ")";
                //        }
                //        cell.InnerHtml += "<input type='hidden' id='rCode" + e.Item.ItemIndex + "-0" + "'" + " name='rCode" + e.Item.ItemIndex + "-0'"  + " value='" + resultObj.RoomDetails[0].RoomTypeCode + "'/>";
                //        cell.InnerHtml += "<input type='hidden' id='rpCode" + e.Item.ItemIndex + "-0"  + "'" + " name='rpCode" + e.Item.ItemIndex + "-0'" + " value='" + resultObj.RoomDetails[0].RatePlanCode + "'/>";
                //        cell.InnerHtml += "<input type='hidden' id='priceTag" + e.Item.ItemIndex + "-0"  + "'" + " name='priceTag" + e.Item.ItemIndex + "-0'" + " value='" + ((resultObj.RoomDetails[0].TotalPrice + resultObj.RoomDetails[0].TotalTax - resultObj.RoomDetails[0].Discount + resultObj.RoomDetails[0].SellExtraGuestCharges) * request.NoOfRooms).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "'/>";
                //        cell.InnerHtml += "<input type='hidden' id='hCurrency" + e.Item.ItemIndex + "'" + " name='hCurrency" + e.Item.ItemIndex + "' value='" + 0 + "'/>";
                //        //cell.InnerHtml += "<input type='hidden' id='hSellingFare" + e.Item.ItemIndex + 0 + "'" + " name=hSellingFare" + e.Item.ItemIndex + "-" + 0 + " value='" + resultObj.RoomDetails[0].SellingFare * request.NoOfRooms + "'/>";
                //        //cell.InnerHtml += "<input type='hidden' id='hTotalTax" + e.Item.ItemIndex + 0 + "'" + " name=hTotalTax" + e.Item.ItemIndex + "-" + 0 + " value='" + resultObj.RoomDetails[0].TotalTax * request.NoOfRooms + "'/>";
                //        //cell.InnerHtml += "<input type='hidden' id='hDiscount" + e.Item.ItemIndex + 0 + "'" + " name=hDiscount" + e.Item.ItemIndex + "-" + 0 + " value='" + resultObj.RoomDetails[0].Discount * request.NoOfRooms + "'/>";
                //        //cell.InnerHtml += "<input type='hidden' id='hChildCharges" + e.Item.ItemIndex + 0 + "'" + " name=hChildCharges" + e.Item.ItemIndex + "-" + 0 + " value='" + resultObj.RoomDetails[0].ChildCharges * request.NoOfRooms + "'/>";
                //        //cell.InnerHtml += "<input type='hidden' id='hExtraGuestCharges" + e.Item.ItemIndex + 0 + "'" + " name=hExtraGuestCharges" + e.Item.ItemIndex + "-" + 0 + " value='" + resultObj.RoomDetails[0].SellExtraGuestCharges * request.NoOfRooms + "'/>";
                //        //cell.InnerHtml += "<input type='hidden' id='hStartDay" + e.Item.ItemIndex + 0 + "'" + " name=hStartDay" + e.Item.ItemIndex + "-" + 0 + " value='" + resultObj.StartDate.Day + "'/>";
                //        //cell.InnerHtml += "<input type='hidden' id='hTotalDays" + e.Item.ItemIndex + 0 + "'" + " name=hTotalDays" + e.Item.ItemIndex + "-" + 0 + " value='" + resultObj.EndDate.Subtract(resultObj.StartDate).TotalDays + "'/>";
                //        cell.InnerHtml += "<input type='hidden' id='hSelRooms" + e.Item.ItemIndex + "'" + " name='hSelRooms" + e.Item.ItemIndex + "'/>";
                //        cell.InnerHtml += "<input type='hidden' id='hSelRoomsP" + e.Item.ItemIndex + "'" + " name='hSelRoomsP" + e.Item.ItemIndex + "'/>";
                //        cell1.InnerHtml = "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;' id='room" + resultObj.RoomDetails[0].RoomTypeCode + "'> </div>";
                //        cell1.InnerHtml += "<div class='cancellation_deatail'>";
                //        cell1.InnerHtml += "<a href='#' onclick=\"CancelRoom('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + resultObj.RoomDetails[0].RatePlanCode + "','" + resultObj.RoomDetails[0].RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "')\">Cancellation Details</a></div>";

                //        cell2.InnerHtml += "<div class='AED_price'>";
                //        cell2.InnerHtml += "<strong>AED " + Math.Ceiling(total);
                //        cell2.InnerHtml += "</strong></div>";

                //        //cell3.InnerHtml += "<input type='checkbox' name='checkbox7' id='checkbox7' />";

                //        row.Cells.Add(cell);
                //        row.Cells.Add(cell1);
                //        row.Cells.Add(cell2);
                //        //row.Cells.Add(cell3); removed Email as temp
                //        //row.Cells.Add(cell3);
                //        //ibtnBook.Attributes.Add("onclick", "window.location.href='HotelReview.aspx?hCode=" + e.Item.ItemIndex + "&rCode=" + resultObj.RoomDetails[0].RoomTypeCode + "&rpCode=" + resultObj.RoomDetails[0].RatePlanCode + "'");
                //        //ibtnBook.OnClientClick = "window.location.href='HotelReview.aspx?hCode=" + e.Item.ItemIndex + "&rCode=" + resultObj.RoomDetails[0].RoomTypeCode + "&rpCode=" + resultObj.RoomDetails[0].RatePlanCode + "'";
                //        //ibtnBook.Attributes.Add("onclick", "submitForm('" + e.Item.ItemIndex + "','" + ((resultObj.RoomDetails.Length)-1).ToString() + "','" + resultObj.HotelCode + "');");
                //        ibtnBook.OnClientClick = "return submitForm('" + e.Item.ItemIndex + "','" + (resultObj.RoomDetails.Length).ToString() + "','" + resultObj.HotelCode + "');";
                //        // ziya to check
                //        //Session["hCode"] = e.Item.ItemIndex;
                //        //Session["rCode"] = resultObj.RoomDetails[0].RoomTypeCode;
                //        //Session["rpCode"] = resultObj.RoomDetails[0].RatePlanCode;
                //        roomtype.Rows.Add(row);
                //    //}

                //    #region Fare Breakup


                //    ////Display Hotel images in the popup
                //    ////Display fare breakup
                //    IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

                //    DateTime fd1 = Convert.ToDateTime(resultObj.StartDate, dateFormat);
                //    DateTime fd2 = Convert.ToDateTime(resultObj.EndDate, dateFormat);
                //    fd2 = fd2.AddDays(-1);
                //    int fresult = 0;
                //    // Get first full week
                //    DateTime ffirstDayOfFirstFullWeek = fd1;
                //    if (fd1.DayOfWeek != DayOfWeek.Sunday)
                //    {
                //        fresult += 1;
                //        ffirstDayOfFirstFullWeek = fd1.AddDays((7 - (int)fd1.DayOfWeek));
                //    }
                //    // Get last full week
                //    DateTime flastDayOfLastFullWeek = fd2;
                //    if (fd2.DayOfWeek != DayOfWeek.Saturday)
                //    {
                //        fresult += 1;
                //        flastDayOfLastFullWeek = fd2.AddDays(-(int)fd2.DayOfWeek - 1);
                //    }
                //    System.TimeSpan fdiffResult = flastDayOfLastFullWeek.Subtract(ffirstDayOfFirstFullWeek);
                //    // Add number of full weeks
                //    fresult += (fdiffResult.Days + 1) / 7;
                //    fd1 = resultObj.StartDate;
                //    fd2 = resultObj.EndDate;


                //    lblFare.Text = "<div class='week_days'><ul><li style='width:50px;'>Sun</li><li style='width:50px;'>Mon</li><li style='width:50px;'>Tue</li>";
                //    lblFare.Text += "<li style='width:50px;'>Wed</li><li style='width:50px;'>Thu</li><li style='width:50px;'>Fri</li><li style='width:50px;'>Sat</li></ul></div>";

                //    decimal ftotalRoomSellRate = 0;
                //    //int counter = 1;
                //    //if (resultObj.RoomDetails.Length == request.NoOfRooms)
                //    {
                //        //for (int k = 0; k < resultObj.RoomDetails.Length; k++)
                //        //{
                //        //    //if (counter <= request.NoOfRooms)
                //        //    {
                //        //        totalRoomSellRate += resultObj.RoomDetails[k].TotalPrice;
                //        //    }
                //        //    counter++;
                //        //}
                //        if (resultObj.BookingSource != HotelBookingSource.RezLive)
                //        {
                //            ftotalRoomSellRate = resultObj.RoomDetails[0].SellingFare;
                //        }
                //        else
                //        {
                //            ftotalRoomSellRate = resultObj.RoomDetails[0].TotalPrice;
                //            if (resultObj.Currency == "USD")
                //            {
                //                ftotalRoomSellRate = ftotalRoomSellRate * Convert.ToDecimal(3.67);
                //            }
                //        }
                //    }
                //    //else
                //    //{
                //    //    for (int k = 0; k < resultObj.RoomDetails.Length; k = k + request.NoOfRooms)
                //    //    {
                //    //        if (counter <= request.NoOfRooms)
                //    //        {
                //    //            totalRoomSellRate += resultObj.RoomDetails[k].TotalPrice;
                //    //        }
                //    //        //else
                //    //        //{
                //    //        //    lblFare.Text += " <li id='SellingFare_" + e.Item.ItemIndex + d1.Day + "' style='color: #ffffff;width:54px;'>" + (totalRoomSellRate / request.NoOfRooms).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + " </li>";
                //    //        //}
                //    //    }



                //    for (int i = 0; i < fresult; i++)
                //    {
                //        lblFare.Text += " <div class='week_number'><ul><li class='week_sno' style='color: #ffffff'>Week " + (i + 1) + "</li>";
                //        int l = 0;
                //        if (fd1.DayOfWeek != DayOfWeek.Sunday)
                //        {
                //            int predeser = Convert.ToInt16(fd1.DayOfWeek);
                //            for (; l < predeser; l++)
                //            {
                //                lblFare.Text += "<li style='color: #ffffff;width:10px;'>  </li>";
                //            }
                //        }

                //        for (; l < 7 && fd1 < fd2; l++)
                //        {
                //            fd1 = fd1.AddDays(1);
                //            //if (resultObj.RoomDetails.Length == request.NoOfRooms)
                //            {
                //                lblFare.Text += " <li id='SellingFare_" + e.Item.ItemIndex + fd1.Day + "' style='color: #ffffff;width:54px;'>" + (ftotalRoomSellRate / Convert.ToDecimal((request.EndDate.Subtract(request.StartDate).TotalDays))).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + " </li>";
                //            }
                //            //else
                //            //{
                //            //    lblFare.Text += " <li id='SellingFare_" + e.Item.ItemIndex + d1.Day + "' style='color: #ffffff;width:54px;'>" + (totalRoomSellRate / request.NoOfRooms).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + " </li>";
                //            //}
                //        }

                //        lblFare.Text += "</ul>";
                //        lblFare.Text += "</div>";
                //    }

                //    lblFare.Text += "<div style='color:#000000' class='sum_total'><ul><li style='color:#000000' class='week_sno'>Total</li>";
                //    //if (resultObj.RoomDetails.Length == request.NoOfRooms)
                //    //{
                //    //    lblFare.Text += "<li ><b id='TotalSellingFare_" + e.Item.ItemIndex + "'>" + ((totalRoomSellRate)).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul><ul>";
                //    //}
                //    //lblFare.Text += "<li class='week_sno'>Tax</li> ";
                //    //decimal totalTax = 0, discount = 0, sellExtraGuestCharges = 0;
                //    //foreach (HotelRoomsDetails room in resultObj.RoomDetails)
                //    //{
                //    //    totalTax += room.TotalTax;
                //    //    discount += room.Discount;
                //    //    sellExtraGuestCharges += room.SellExtraGuestCharges;
                //    //}
                //    ////if (resultObj.RoomDetails.Length == request.NoOfRooms)
                //    ////{
                //    ////    lblFare.Text += "<li ><b id='TotalTax_" + e.Item.ItemIndex + "'>" + ((totalTax)).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                //    ////}
                //    ////else
                //    //{
                //    //    lblFare.Text += "<li ><b id='TotalTax_" + e.Item.ItemIndex + "'>" + ((totalTax * request.NoOfRooms)).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                //    //}
                //    //if (discount > 0)
                //    //{
                //    //    lblFare.Text += "<ul><li class='week_sno'>Discount</li> ";
                //    //    lblFare.Text += "<li ><b id='Discount_" + e.Item.ItemIndex + "'>" + (discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                //    //}
                //    //lblFare.Text += "<ul><li class='week_sno'>Extra Guest Charge</li> ";
                //    //decimal extraGuestRate = 0;
                //    //if (resultObj.BookingSource == HotelBookingSource.TBOConnect)
                //    //{
                //    //    extraGuestRate = sellExtraGuestCharges;
                //    //}
                //    //else
                //    {
                //        lblFare.Text += "<li style='color:#000000' ><b id='TotalSellingFare_" + e.Item.ItemIndex + "'>" + ((ftotalRoomSellRate)).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul><ul>";
                //    }
                //    lblFare.Text += "<li style='color:#000000' class='week_sno'>Tax</li> ";
                //    decimal totalTax = 0, discount = 0, sellExtraGuestCharges = 0;
                //    foreach (HotelRoomsDetails room in resultObj.RoomDetails)
                //    {
                //        totalTax += room.TotalTax;
                //        discount += room.Discount;
                //        sellExtraGuestCharges += room.SellExtraGuestCharges;
                //    }
                //    //if (resultObj.RoomDetails.Length == request.NoOfRooms)
                //    //{
                //    //    lblFare.Text += "<li ><b id='TotalTax_" + e.Item.ItemIndex + "'>" + ((totalTax)).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                //    //}
                //    //else
                //    {
                //        lblFare.Text += "<li style='color:#000000'><b id='TotalTax_" + e.Item.ItemIndex + "'>" + ((totalTax * request.NoOfRooms)).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                //    }
                //    if (discount > 0)
                //    {
                //        lblFare.Text += "<ul style='color:#000000'><li class='week_sno'>Discount</li> ";
                //        lblFare.Text += "<li ><b id='Discount_" + e.Item.ItemIndex + "'>" + (discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                //    }
                //    lblFare.Text += "<ul style='color:#000000'><li class='week_sno'>Extra Guest Charge</li> ";
                //    decimal fextraGuestRate = 0;
                //    if (resultObj.BookingSource == HotelBookingSource.TBOConnect)
                //    {
                //        fextraGuestRate = sellExtraGuestCharges;
                //    }
                //    else
                //    {
                //        fextraGuestRate = sellExtraGuestCharges * resultObj.RoomDetails[0].Rates.Length;
                //    }
                //    if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                //    {
                //        lblFare.Text += "<li ><b id='ExtraGuestRate_" + e.Item.ItemIndex + "'>" + ((fextraGuestRate * request.NoOfRooms)).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li>";
                //    }
                //    else
                //    {
                //        lblFare.Text += "<li ><b id='ExtraGuestRate_" + e.Item.ItemIndex + "'>" + fextraGuestRate.ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li>";
                //    }
                //    lblFare.Text += "</ul>";
                //    if (resultObj.BookingSource == HotelBookingSource.TBOConnect)
                //    {
                //        lblFare.Text += "<ul style='color:#000000'><li class='week_sno'>Child Charges</li> ";
                //        decimal childCharges = resultObj.RoomDetails[0].ChildCharges;
                //        lblFare.Text += "<li ><b id='ChildCharges_" + e.Item.ItemIndex + "'>" + (childCharges * request.NoOfRooms).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li>";
                //        lblFare.Text += "</ul>";
                //    }
                //    lblFare.Text += "<ul style='color:#000000'><li class='week_sno'>No. of Rooms</li> <li><b>" + request.NoOfRooms + "</b></li></ul><ul>";

                //    decimal ftempTotalP = 0;
                //    if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                //    {
                //        //if (resultObj.RoomDetails.Length == request.NoOfRooms)
                //        //{
                //        //    tempTotalP = (totalRoomSellRate + totalTax + sellExtraGuestCharges - discount);
                //        //}
                //        //else
                //        {
                //            ftempTotalP = (ftotalRoomSellRate + totalTax + sellExtraGuestCharges - discount);
                //        }
                //    }
                //    else
                //    {
                //        ftempTotalP = ftotalRoomSellRate + totalTax + sellExtraGuestCharges - discount;
                //    }


                //    lblFare.Text += "<li style='color:#000000' class='week_sno'>Total Price</li>";
                //    lblFare.Text += "<li style='color:#000000'><b id='TotalRate_" + e.Item.ItemIndex + "'>" + Math.Ceiling(ftempTotalP).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul></div>";
                //    ////lblFare.Text+= "<div class='close_window' ><span><a class='hand' onclick='javascript:ShowFare('fare<%# Eval(''HotelCode'')%>')'>Close Window</a></span></div>"; 
                //    #endregion

                //}
                #endregion
                #endregion

                #region Old Code
                //--------------------------------------------End---------------------------------------------------//

                //--------------------------------------Display Amenities in the popup------------------------------//
                #region Amenities
                //Label lblAmemnities = e.Item.FindControl("lblAmenities") as Label;
                //Label ltlLiteral = e.Item.FindControl("ltlFadeShowScript") as Label;
                //HotelDetails hotelDetails = new HotelDetails();
                //if (resultObj.BookingSource == HotelBookingSource.DOTW)
                //{
                //    try
                //    {
                //        CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi(Session["cSessionId"].ToString());
                //        hotelDetails = dotw.GetItemInformation(resultObj.CityCode, resultObj.HotelName, resultObj.HotelCode);
                //    }
                //    catch (Exception ex)
                //    {
                //        Audit.Add(EventType.HotelBook, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.Message, "0");
                //    }
                //}
                //if (hotelDetails.HotelFacilities != null && lblAmemnities != null)
                //{
                //    foreach (string amenity in hotelDetails.HotelFacilities)
                //    {
                //        if (lblAmemnities.Text.Length <= 0)
                //        {
                //            lblAmemnities.Text += "<li>" + amenity + "</li>";
                //        }
                //        else
                //        {
                //            lblAmemnities.Text += "<li>" + amenity + "</li>";
                //        }
                //    }
                //}
                #endregion
                //--------------------------------------------End---------------------------------------------------//
                //--------------------------------Display Hotel Images in the popup---------------------------------//
                #region Hotel Popup Images

                //ltlLiteral.Text = "<script type='text/javascript'>";
                //ltlLiteral.Text += "var mygallery" + resultObj.HotelCode + " = new fadeSlideShow({" + Environment.NewLine;
                //ltlLiteral.Text += "wrapperid: 'fadeshow" + resultObj.HotelCode + "'," + Environment.NewLine;
                //ltlLiteral.Text += "dimensions: [715, 373]," + Environment.NewLine;
                //ltlLiteral.Text += "imagearray: [" + Environment.NewLine;

                //if (hotelDetails.Images != null)
                //{
                //    string Path = CT.Configuration.ConfigurationSystem.DOTWConfig["imgPathForServer"].ToString();
                   // string Path = Request.Url.Scheme+"://localhost/DownloadDOTWImages/";
                //    foreach (string image in hotelDetails.Images)
                //    {
                //        if (image.Length <= 0)
                //        {
                //            ltlLiteral.Text += "['" + resultObj.HotelPicture + "', '#', '_new', 'Photo Title will goes here']," + Environment.NewLine;
                //            //ltlLiteral.Text += "['" + image + "', '#', '_new', 'Photo Title will goes here']," + Environment.NewLine;
                //        }
                //        else
                //        {
                //            ltlLiteral.Text += "['" + Path + image + "', '#', '_new', 'Photo Title will goes here']," + Environment.NewLine;
                //        }
                //    }
                //}
                //ltlLiteral.Text += "]," + Environment.NewLine;
                //ltlLiteral.Text += " displaymode: { type: 'manual', pause: 3000, cycles: 0, wraparound: false }," + Environment.NewLine;
                //ltlLiteral.Text += "persist: false, //remember last viewed slide and recall within same session?" + Environment.NewLine;
                //ltlLiteral.Text += "fadeduration: 500, //transition duration (milliseconds)" + Environment.NewLine;
                //ltlLiteral.Text += "descreveal: 'always'," + Environment.NewLine;
                //ltlLiteral.Text += "togglerid: 'fadeshow4toggler'" + Environment.NewLine;
                //ltlLiteral.Text += " })    </script>";
                #endregion
                //--------------------------------------------End---------------------------------------------------//
                //-------------------------Prepare Fare Breakup DIV for the Hotel-----------------------------------//
                
                //--------------------------------------------End---------------------------------------------------//
                #endregion
            }

            //// to see last row time
            //if ((int)ViewState["tmpRowCount"] == hotelSearchResult.Length-1)
            //{
            //    DateTime beforeLast = (DateTime)ViewState["tmpStartTime"];
            //    //SearchHotels();
            //    DateTime after = DateTime.Now;
            //    TimeSpan ts = after - beforeLast;
            //    Audit.Add(EventType.Search, Severity.Normal, 1, "Hotel Page ItemBound End Time" + after.TimeOfDay + " Seconds", "1");

            //    Audit.Add(EventType.Search, Severity.Normal, 1, "Hotel Page ItemBound Taken Time" + beforeLast.Second+ " Seconds", "1");

            //}

            //int rowCount = (int)ViewState["tmpRowCount"];
            //rowCount++;
            //ViewState["tmpRowCount"] = rowCount;

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GetHotelDetails, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Bind Hotel Details " + ex.ToString(), "127.0.0.1");
            Utility.WriteLog(ex, "Bind Hotel Results");
        }

    }

    
//protected void imgBtnBookNow_Click(object sender, ImageClickEventArgs e)
    //protected void imgBtnBookNow_Click(object sender, EventArgs e)
    //{
    //    if (request.NoOfRooms == 1)
    //    {
    //        Session["hCode"] = hotelCode.Value;
    //        Session["rCode"] = roomCode.Value;
    //        //Session["rpCode"] = ratePlanCode.Value;
    //    }
    //    Response.Redirect("HotelReview.aspx", false);
    //}
}
