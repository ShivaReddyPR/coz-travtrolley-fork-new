﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using Visa;
using System.Collections.Generic;

public partial class VisaRegionList : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        Page.Title = "Region List";
        AuthorizationCheck();

        if (!IsPostBack)
        {
            BindData();

        }

    }

    private void BindData()
    {


        List<VisaRegion> allRegion = VisaRegion.GetRegionList();
        if (allRegion == null || allRegion.Count == 0)
        {
            lbl_msg.Text = "There are no Record.";
            lbl_msg.Visible = true;

        }
        else
        {
            lbl_msg.Visible = false;
        }
        GridView1.DataSource = allRegion;
        GridView1.DataBind();

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lb = ((LinkButton)e.Row.Cells[5].Controls[1]);
            lb.CommandArgument = GridView1.DataKeys[e.Row.RowIndex].Value.ToString();
            if (e.Row.Cells[3].Text == "True")
            {
                lb.Text = "Deactivate";
                e.Row.Cells[3].Text = "Active";
            }
            else
            {
                lb.Text = "Activate";
                e.Row.Cells[3].Text = "Inactive";
            }
        }

    }


    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChangeStatus")
        {
            bool isActive;
            string status = ((LinkButton)e.CommandSource).Text;
            int rowNumber = Convert.ToInt32(e.CommandArgument);
            // int i = Convert.ToInt32(GridView1.DataKeys[rowNumber].Value);
            if (status == "Activate")
            {
                isActive = true;
            }
            else
            {
                isActive = false;
            }
            VisaRegion.ChangeStatus(rowNumber, isActive);
            BindData();


        }

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();

    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }

}
