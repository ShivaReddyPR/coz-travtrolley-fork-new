﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CT.Core;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Configuration;

namespace CozmoB2BWebApp
{
    
    public partial class ViewBookingForBaggageInsuranceGUI : CT.Core.ParentPage
    {
        protected List<BaggageInsuranceVoucherCls> BaggageInsuranceVoucherList = new List<BaggageInsuranceVoucherCls>();
        protected BaggageInsuranceHeader header;
        protected DataSet dsPaxDetails;
        protected int HeaderId = 0;
        protected AgentMaster agent;
        protected decimal amount = 0;
        protected decimal servicefee = 0;
        protected decimal markup = 0;
        protected decimal discount = 0;
        protected decimal gst = 0;
        protected decimal inputVAT = 0;
        protected decimal outputVAt = 0;
        protected decimal totalAmount=0;


        /// <summary>
        /// Variable declaration only for Send email as Like Voucher page
        /// </summary>
        protected decimal baseFare = 0;
        protected decimal TotalAmount = 0;
        protected decimal CGSTTaxValue = 0;
        protected decimal SGSTTaxValue = 0;
        protected decimal IGSTTaxValue = 0;
        protected decimal TaxAmount = 0;
        protected decimal Markup = 0;
        protected decimal Discount = 0;
        protected decimal InputVAT = 0;
        protected decimal OutputVAT = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            
                HeaderId=Convert.ToInt32(Request.QueryString["bookingId"].ToString());
                header = new BaggageInsuranceHeader();
                dsPaxDetails = header.LoadBaggageHeader(HeaderId);
                agent = new CT.TicketReceipt.BusinessLayer.AgentMaster(header.AgentID);
                if (dsPaxDetails!=null)
                {
                    amount = Convert.ToDecimal(dsPaxDetails.Tables[0].Rows[0]["BI_TOTAL_PRICE"]);
   
                }
                GetBaggageInsuranceVoucherDetails(HeaderId);
        }

        private void GetBaggageInsuranceVoucherDetails(int bid)
        {
            try
            {
                BaggageInsuranceVoucherCls objinsuranceVoucher = new BaggageInsuranceVoucherCls();
                DataSet dsVoucherDetails = objinsuranceVoucher.GetVoucherDetails(bid);
                if (dsVoucherDetails != null && dsVoucherDetails.Tables[0].Rows.Count > 0)
                {

                    int agentID = Convert.ToInt32(dsVoucherDetails.Tables[0].Rows[0]["BI_AGENT_ID"].ToString());
                    AgentMaster agent = new AgentMaster(agentID);
                    TotalAmount = Math.Round(Convert.ToDecimal(dsVoucherDetails.Tables[0].Rows[0]["BI_TOTAL_PRICE"]), agent.DecimalValue);

                    for (int i = 0; i < dsVoucherDetails.Tables[0].Rows.Count; i++)
                    {
                        BaggageInsuranceVoucherCls insuranceVoucher = new BaggageInsuranceVoucherCls();
                        insuranceVoucher.PaxName = dsVoucherDetails.Tables[0].Rows[i]["PAX_FIRST_NAME"].ToString() + " " + dsVoucherDetails.Tables[0].Rows[i]["PAX_LAST_NAME"].ToString();
                        insuranceVoucher.TicketNo = dsVoucherDetails.Tables[0].Rows[i]["PAX_TICKET_NO"].ToString();
                        insuranceVoucher.PNR = dsVoucherDetails.Tables[0].Rows[i]["PAX_PNR"].ToString();
                        insuranceVoucher.PlanID = Convert.ToInt32(dsVoucherDetails.Tables[0].Rows[0]["PLAN_ID"].ToString());
                        insuranceVoucher.PlanName = dsVoucherDetails.Tables[0].Rows[0]["PLAN_TITLE"].ToString();
                        insuranceVoucher.PlanDescription = dsVoucherDetails.Tables[0].Rows[0]["PLAN_DESCRIPTION"].ToString();
                        //insuranceVoucher.PlanPrice = Convert.ToDecimal(dsVoucherDetails.Tables[0].Rows[0]["BI_TOTAL_PRICE"].ToString());
                        insuranceVoucher.PlanPrice = Convert.ToDecimal(dsVoucherDetails.Tables[0].Rows[0]["PAX_PRICE"].ToString());
                        insuranceVoucher.PolicyNo = dsVoucherDetails.Tables[0].Rows[0]["BI_POLICY_NO"].ToString();
                        BaggageInsuranceVoucherList.Add(insuranceVoucher);
                        Markup += Convert.ToDecimal(dsVoucherDetails.Tables[0].Rows[0]["PAX_MarkUp"].ToString());
                        Discount += Convert.ToDecimal(dsVoucherDetails.Tables[0].Rows[0]["PAX_Discount"].ToString());
                        //InputVAT += Convert.ToDecimal(dsVoucherDetails.Tables[0].Rows[0]["PAX_INPUTVAT"].ToString());
                        //OutputVAT += Convert.ToDecimal(dsVoucherDetails.Tables[0].Rows[0]["PAX_OUTPUTVAT"].ToString());
                        baseFare += Math.Round(insuranceVoucher.PlanPrice, agent.DecimalValue);

                    }

                    if (dsVoucherDetails.Tables[0].Rows[0]["CGST"] != null)
                    {
                        CGSTTaxValue = Math.Round(Convert.ToDecimal(dsVoucherDetails.Tables[0].Rows[0]["CGST"].ToString()), agent.DecimalValue);
                    }
                    if (dsVoucherDetails.Tables[0].Rows[0]["SGST"] != null)
                    {
                        SGSTTaxValue = Math.Round(Convert.ToDecimal(dsVoucherDetails.Tables[0].Rows[0]["SGST"].ToString()), agent.DecimalValue);
                    }
                    if (dsVoucherDetails.Tables[0].Rows[0]["IGST"] != null)
                    {
                        IGSTTaxValue = Math.Round(Convert.ToDecimal(dsVoucherDetails.Tables[0].Rows[0]["IGST"].ToString()), agent.DecimalValue);
                    }

                    baseFare += Markup - Discount;
                    baseFare = Math.Round(baseFare, agent.DecimalValue);

                }
                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "(BaggageInsuranceVoucher)Error in GetBaggageInsuranceVoucherDetails." + ex.ToString(), Request["REMOTE_ADDR"]);
                Response.Redirect("ErrorPage.aspx");
            }
        }
        protected void btnEmailVoucher_Click(object sender, EventArgs e)
        {
            //Sending Email 
            string paxEmailId = string.Empty;
            BaggageInsuranceHeader header = new BaggageInsuranceHeader();
            DataSet dsPaxDetails = header.LoadBaggageHeader(Convert.ToInt32(Request.QueryString["bookingId"].ToString()));

            if (dsPaxDetails != null)
            {
                // amount = Convert.ToDecimal(dsPaxDetails.Tables[0].Rows[0]["BI_TOTAL_PRICE"]);
                paxEmailId = dsPaxDetails.Tables[0].Rows[0]["BI_EMAIL"].ToString();
            }
            System.IO.StringWriter sWriter = new System.IO.StringWriter();
            HtmlTextWriter htWriter = new HtmlTextWriter(sWriter);
            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            toArray.Add(paxEmailId);
            divVoucher.RenderControl(htWriter);
            string message = "";
            message = sWriter.ToString();
            message = message.Replace("none", "block");//to display header in manual email, change text color from white to black
            try
            {
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "Baggage Insurance", message, null);
            }
            catch { }

        }
    }
}
