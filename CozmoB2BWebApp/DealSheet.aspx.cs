﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
public partial class DealSheetGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    DataSet NoticeList = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            NoticeList = AgentNotice.GetMessages(Convert.ToInt32(Settings.LoginInfo.AgentId));
            gvNotice.DataSource = NoticeList;
            gvNotice.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void gvNotice_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvNotice.PageIndex = e.NewPageIndex;
        gvNotice.EditIndex = -1;
        gvNotice.DataSource = NoticeList;
        gvNotice.DataBind();
    }
}
