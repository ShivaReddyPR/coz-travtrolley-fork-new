using System;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;

public partial class ChangePasswordUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;

            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
        }
        
    }
    
    protected void butSave_Click(object sender, EventArgs e)
    {
        try
        {
            UserMaster.ChangePassword(Settings.LoginInfo.LoginName,txtCurrentPassword.Text.Trim(), txtNewPassword.Text.Trim());
            lblError.Text = string.Empty;
           // this.Master.ShowStatus("Change Password", "Password has been Reset!");
            lblSuccessMsg.Text = "Password is update successfully!";
        }
        catch (Exception ex)
        {

            //this.Master.ShowStatus("Error While Saving", ex);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);

        }

    }
}
