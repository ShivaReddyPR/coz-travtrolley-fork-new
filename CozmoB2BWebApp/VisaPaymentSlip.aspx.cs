﻿using System;
using Visa;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;



public partial class VisaPaymentSlip : CT.Core.ParentPage
{
    protected VisaApplication application;
    protected int Adult = 0;
    protected int Child = 0;
    protected int Infant = 0;
    protected string visaTypeName = string.Empty;
    protected int visaId = 0;
    protected string paymentId = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString.Count > 0)
        {
            bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out visaId);
            if (!isnumerice)
            {
                Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                Response.End();
            }
            try
            {
                application = new VisaApplication(visaId);
                VisaPaymentInfo PaymentInfo = new VisaPaymentInfo(visaId);
                paymentId = Convert.ToString(PaymentInfo.PaymentId);
                VisaType visaType = new VisaType(application.VisaTypeId);
                visaTypeName = visaType.VisaTypeName;
                foreach (VisaPassenger Passenger in application.PassengerList)
                {
                    if (Passenger.Status != VisaPassengerStatus.NotEligible)
                    {
                        int yearPass = (Passenger.CreatedOn.Year - Passenger.Dob.Year);
                        if (Passenger.CreatedOn.Month < Passenger.Dob.Month || (Passenger.CreatedOn.Month == Passenger.Dob.Month && Passenger.CreatedOn.Day < Passenger.Dob.Day))
                        {
                            yearPass--;
                        }
                        int year = yearPass;
                        if (year >= 12)
                        {
                            Adult++;
                        }
                        else if (year < 12 && year >= 6)
                        {
                            Child++;
                        }
                        else
                        {
                            Infant++;
                        }
                    }

                }
                if (Session["visaPassengerId"] != null)
                {
                    Session.Remove("visaPassengerId");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddAcknowledgement.aspx,Err:" + ex.Message, "");
            }

        }
        else
        {
            Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
            Response.End();
        }

    }
}
