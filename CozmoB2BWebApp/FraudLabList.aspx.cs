﻿using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using System;
using System.Data;
using System.Web.UI.WebControls;

public partial class FraudLabListGUI : CT.Core.ParentPage
{
    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            hdfParam.Value = "1";
            Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
            if (!IsPostBack)
            {
                hdfParam.Value = "0";
                InitializePageControls();
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;           
            Utility.Alert(this.Page, ex.Message);
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from FraudLab List PageLoad():" + ex.ToString(), "");
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        try
        {
            foreach (GridViewRow gvrow in gvFraudLabList.Rows)
            {
                HiddenField hdfApproveStatus = (HiddenField)gvrow.FindControl("IThdffraudlabsstatus");
                DropDownList ddlApprove = (DropDownList)gvrow.FindControl("gdddlStatus");
                if(!string.IsNullOrEmpty(hdfApproveStatus.Value))
                    {
                    ddlApprove.Items.FindByText(hdfApproveStatus.Value).Selected = true;
                }
               
                //ddlApprove.SelectedItem.Text = hdfApproveStatus.Value;
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    #endregion
    #region private Methods
    private void InitializePageControls()
    {
        try
        {
            txtFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            BindAgent();
            BindProducts();
            BindGrid();
            BindPgSources();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindPgSources()
    {
        try
        {         
            Array sources = Enum.GetValues(typeof(PaymentGatewaySource));
            foreach (PaymentGatewaySource pgateway in sources)
            {
                if(((int)pgateway).ToString()=="14")
                {
                    ChkPgSources.Items.Add(new ListItem("NEOPG", "NetworkInternational"));
                }
                else
                {
                    ChkPgSources.Items.Add(new ListItem(pgateway.ToString(), (pgateway.ToString())));
                }
                            
            }            
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }
    private void BindAgent()
    {
        try
        {
            chkAgent.DataSource = AgentMaster.GetB2CAgentList();
            chkAgent.DataTextField = "agent_name";
            chkAgent.DataValueField = "agent_id";
            chkAgent.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindProducts()
    {
        try
        {
            Array products = Enum.GetValues(typeof(ProductType));

            foreach (ProductType pd in products)
            {
                if (pd.ToString() == "Flight" || pd.ToString() == "Hotel")
                {
                    chkProduct.Items.Add(new ListItem(pd.ToString(), ((int)pd).ToString()));
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindGrid()
    {
        try
        {
            DateTime fromdate = DateTime.MinValue, todate = DateTime.MaxValue;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
            string agent = string.Empty;
            string product = string.Empty;
            string status = string.Empty;
            string Sources = string.Empty;
            if (!string.IsNullOrEmpty(txtFromDate.Text))
            {
                fromdate = Convert.ToDateTime(txtFromDate.Text, provider);
            }
            if (!string.IsNullOrEmpty(txtToDate.Text))
            {
                todate = Convert.ToDateTime(txtToDate.Text, provider);
            }
            for (int j = 0; j < ChkPgSources.Items.Count; j++)
            {
                if (ChkPgSources.Items[j].Selected)
                {
                    if (string.IsNullOrEmpty(Sources))
                    {
                        Sources = ChkPgSources.Items[j].Value;
                    }
                    else
                    {
                        Sources += "," + ChkPgSources.Items[j].Value;
                    }
                }
            }
            for (int i = 0; i < chkAgent.Items.Count; i++)
            {
                if (chkAgent.Items[i].Selected)
                {
                    if (chkAgent.Items[i].Selected)
                    {
                        if (string.IsNullOrEmpty(agent))
                        {
                            agent = chkAgent.Items[i].Value;
                        }
                        else
                        {
                            agent += "," + chkAgent.Items[i].Value;
                        }
                    }
                }
            }
            for (int i = 0; i < chkProduct.Items.Count; i++)
            {
                if (chkProduct.Items[i].Selected)
                {
                    if (string.IsNullOrEmpty(product))
                    {
                        product = chkProduct.Items[i].Value;
                    }
                    else
                    {
                        product += "," + chkProduct.Items[i].Value;
                    }
                }
            }
            for (int i = 0; i < chkStatus.Items.Count; i++)
            {
                if (chkStatus.Items[i].Selected)
                {
                    if (string.IsNullOrEmpty(status))
                    {
                        status = chkStatus.Items[i].Value;
                    }
                    else
                    {
                        status += "," + chkStatus.Items[i].Value;
                    }
                }
            }          
            DataTable dtfraudlist = FraudLabPaymentsList.GetFraudLabList(agent, product, status, fromdate, todate, Sources);          
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvFraudLabList, dtfraudlist);
           
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from FraudLab List BindGrid() :" + ex.ToString(), "");
            throw ex;
        }
    }
    private void BindHistory(DataTable dtHistory)
    {
        try
        {
            CommonGrid grid = new CommonGrid();
            grid.BindGrid(gvfraudHistory, dtHistory);
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from FraudLab List BindHistory():" + ex.ToString(), "");
            throw ex;
        }
    }
    #endregion
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    #region GridEvents
    protected void gvFraudLabList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        { 
              int  index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = gvFraudLabList.Rows[index];
                int resid = Convert.ToInt32(gvFraudLabList.DataKeys[index].Values["respId"]);
                if (e.CommandName == "Update")
                {
                    TextBox txtremarks = (TextBox)row.FindControl("ITtxtRemarks");
                    string status = (row.FindControl("gdddlStatus") as DropDownList).SelectedValue;
                    string remarks = txtremarks.Text;
                    if (!string.IsNullOrEmpty(remarks))
                    {
                        FraudLabPaymentsList.UpdateFraudLabStatus(resid, status, remarks, Convert.ToInt32(Settings.LoginInfo.UserID));
                        Utility.Alert(this.Page, "Successfully Updated");
                        BindGrid();
                    }
                    else
                    {
                    (row.FindControl("flchkSelect") as CheckBox).Checked = false;
                        Utility.Alert(this.Page, "Remarks cannot be  empty");
                    }
                }
                if (e.CommandName == "ShowHistory")
                {
                    LinkButton ITlnkHistory = (LinkButton)row.FindControl("lbtnHistory");
                   CheckBox chkselect = (CheckBox)row.FindControl("flchkSelect");
                   chkselect.Checked = false;
                    DataTable dthistory = FraudLabPaymentsList.GetFraudLabListHistory(resid);
                    BindHistory(dthistory);
                    Utility.StartupScript(this.Page, "setDocumentPosition('" + ITlnkHistory.ClientID + "',divHistory)", "setDocumentPosition");
                }
            
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void gvFraudLabList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
               
               
                //1.Enable and disable Controls
                CheckBox chk = (CheckBox)e.Row.FindControl("flchkSelect");
                DropDownList ddl = (DropDownList)e.Row.FindControl("gdddlStatus");
               
                TextBox txtcpwd = (TextBox)e.Row.FindControl("ITtxtRemarks");
                Button btnupdate = (Button)e.Row.FindControl("btnUpdate");
                chk.Attributes["onclick"] = string.Format("EnableControls('{0}','{1}','{2}', this.checked);", ddl.ClientID, txtcpwd.ClientID, btnupdate.ClientID);
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void gvFraudLabList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvFraudLabList.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void gvFraudLabList_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    #endregion

}