﻿using System;
using System.Collections;
using System.Configuration;
using System.Web.UI;
using CT.Core;
using CT.BookingEngine.Insurance;
using CT.TicketReceipt.BusinessLayer;
using System.IO;
public partial class PrintInsuranceVoucher : CT.Core.ParentPage
{
    protected string imagePath = "";
    protected InsuranceHeader header;
    protected string logoPath = string.Empty;
    protected AgentMaster agent;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                imagePath += ConfigurationManager.AppSettings["RootFolder"];
                if (Request.QueryString["InsId"] != null)
                {
                    header = new InsuranceHeader();
                    header.RetrieveConfirmedPlan(Convert.ToInt32(Request.QueryString["InsId"]));
                    if (header.AgentId > 0)
                    {
                        agent = new AgentMaster(header.AgentId);
                        logoPath = Request.Url.Scheme+"://ctb2bstage.cozmotravel.com/" + ConfigurationManager.AppSettings["AgentImage"] + agent.ImgFileName;
                        imgHeaderLogo.ImageUrl = logoPath;
                    }
                    BindVoucher(header);

                }
                else
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in Insurance Voucher :Query string header id is empty ", Request["REMOTE_ADDR"]);
                    Response.Redirect("Insurance.aspx");
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in Insurance Voucher : " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    void BindVoucher(InsuranceHeader header)
    {
        
        if (header.DepartureDateTime != header.ReturnDateTime)
        {
            lblSector.Text = header.DepartureStationCode + "-" + header.ArrivalStationCode + "-" + header.DepartureStationCode;
            lblJourneyDates.Text = Convert.ToDateTime(header.DepartureDateTime).ToString("dd-MM-yyyy") + " - " + Convert.ToDateTime(header.ReturnDateTime).ToString("dd-MM-yyyy");
        }
        else
        {
            lblSector.Text = header.DepartureStationCode + "-" + header.ArrivalStationCode;
            lblJourneyDates.Text = Convert.ToDateTime(header.DepartureDateTime).ToString("dd-MM-yyyy");
        }

    }
    protected void btnEmailVoucher_Click(object sender, EventArgs e)
    {
        try
        {
            #region Sending Email
            Hashtable table = new Hashtable();

            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();

            toArray.Add(txtEmailId.Text.Trim());
            btnEmail.Visible = false;
            btnPrint.Visible = false;

            string message = "";

            StringWriter sWriter = new StringWriter();
            HtmlTextWriter htWriter = new HtmlTextWriter(sWriter);
            printableArea.RenderControl(htWriter);


            message = sWriter.ToString();

            message = message.Replace("class=\"themecol1\"", "style=\"background: #1c498a; height: 24px; line-height: 22px; font-size: 12px;color: #fff; padding-left: 10px;\"");
            try
            {
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], txtEmailId.Text.Trim(), toArray, "Insurance Voucher", message, table);
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                throw ex;
            }
            finally
            {
                btnEmail.Visible = true;
                btnPrint.Visible = true;
            }

            #endregion

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.Normal, 0, "Failed to send Fleet Voucher to " + txtEmailId.Text + " Reason " + ex.Message, Request["REMOTE_ADDR"]);
        }
    }
}
