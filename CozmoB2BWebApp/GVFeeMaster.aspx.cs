using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using CT.GlobasVisa;
using CT.GlobalVisa;
using CT.Core;

public partial class GVFeeMasterUI : CT.Core.ParentPage
{
  
    private string GVFEE_SESSION = "_GVvisaFeeSession";

    private string GV_FEE_SEARCH_SESSION = "_GVvisaFeeSearchList";
    private string GV_FEE_DETAILS = "_GVVisaDeatilList";
    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
            }
            lblErrorMsg.Text = string.Empty;
            lblSuccessMsg.Text = string.Empty;
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    private void InitializePageControls()
    {
        try
        {
            BindCountryList();
            BindNationalityList();
            BindVisaType(ddlCountry.SelectedValue);
            BindVisaCategory();
            BindResidence();
            Clear();
        }
        catch
        {
            throw;
        }
    }
    #endregion

    #region gvLocation Events
    
    private void SetOBVisaFeeDetails(DataRow dr, GridViewRow gvRow, string mode)
    {
        try
        {
            dr["adsvc_code"] = ((TextBox)gvRow.FindControl(mode + "txtFeeCode")).Text;
            dr["adsvc_adult_amount"] = Utility.ToDecimal(((TextBox)gvRow.FindControl(mode + "txtAdults")).Text);
            dr["adsvc_child_amount"] = Utility.ToDecimal(((TextBox)gvRow.FindControl(mode + "txtChild")).Text);
            dr["adsvc_infant_amount"] = Utility.ToDecimal(((TextBox)gvRow.FindControl(mode + "txtInfants")).Text);
            dr["adsvc_created_by"] = Settings.LoginInfo.UserID;
        }
        catch { throw; }
    }

   
    private void BindCountryList()
    {
        try
        {
            ddlCountry.DataSource = Country.GetCountryList();
            ddlCountry.DataValueField = "Value";
            ddlCountry.DataTextField = "Key";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "-1"));
        }
        catch { throw; }
    }

    private void BindResidence()//Field Master
    {
        try
        {
            ddlResidence.DataSource = Country.GetCountryList();
            ddlResidence.DataTextField = "Key";
            ddlResidence.DataValueField = "Value";
            ddlResidence.DataBind();
            ddlResidence.Items.Insert(0, new ListItem("--Select Residence--", "-1"));
            ddlResidence.Items.Insert(1,new ListItem("All","0"));
        }
        catch { throw; }
    }
    private void BindNationalityList()
    {
        try
        {
            ddlNationality.DataSource = Country.GetNationalityList();
            ddlNationality.DataTextField = "Key";
            ddlNationality.DataValueField = "Value";
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("--Select Nationality--", "-1"));
            ddlNationality.Items.Insert(1, new ListItem("All", "0"));
        }
        catch { throw; }
    }
    
    private void BindVisaType(string countryCode)
    {
        try
        {
            ddlVisaType.DataSource = VisaTypeMaster.GetListBYCountryCode(ListStatus.Short, RecordStatus.Activated, countryCode);
            ddlVisaType.DataTextField = "VISA_TYPE_NAME";
            ddlVisaType.DataValueField = "VISA_TYPE_ID";
            ddlVisaType.DataBind();
            ddlVisaType.Items.Insert(0, new ListItem("--Select Visa Type--", "-1"));
            ddlVisaType.Items.Insert(1, new ListItem("All", "0"));

        }
        catch { throw; }
    }

    private void BindVisaCategory()//Field Master
    {
        try
        {
            ddlVisaCategory.DataSource = CachedMaster.GetCommonList("ob_visa_type");
            ddlVisaCategory.DataTextField = "field_text";
            ddlVisaCategory.DataValueField = "field_value";
            ddlVisaCategory.DataBind();
            ddlVisaCategory.Items.Insert(0, new ListItem("--Select Visa Category--", "-1"));
            //ddlVisaCategory.Items.Insert(1, new ListItem("All", "0"));
        }
        catch { throw; }
    }

    
    private void bindSearch()
    {
        try
        {
            LoginInfo loginfo = Settings.LoginInfo;
            DataTable dt = GVFeeMaster.GetList(ListStatus.Long, RecordStatus.Activated);
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch { throw; }
    }

    protected void btnSave_Click(object sender, EventArgs e)
      {
          try
          {
              Save();
          }
          catch (Exception ex)
          {
              Utility.WriteLog(ex, this.Title);
              Label lblMasterError = (Label)this.Master.FindControl("lblError");
              lblMasterError.Visible = true;
              lblMasterError.Text = ex.Message;
              Utility.Alert(this.Page, ex.Message);
          }
      }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            Delete();
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
      {
          try
          {             
              Clear();                           
          }
          catch (Exception ex)
          {
              Utility.WriteLog(ex, this.Title);
              Utility.Alert(this.Page, ex.Message);
              Label lblMasterError = (Label)this.Master.FindControl("lblError");
              lblMasterError.Visible = true;
              lblMasterError.Text = ex.Message;
          }
      }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            Clear();
            bindSearch();                     
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
   
    private void Clear()
    {
        try
        {
            GVFeeMaster gvVisaFee = new GVFeeMaster();
            txtAdultsVisaFee.Text = Formatter.ToCurrency(0);
            txtChildVisaFee.Text = Formatter.ToCurrency(0);
            txtInfantVisaFee.Text = Formatter.ToCurrency(0);
            txtAdultsService.Text = Formatter.ToCurrency(0);
            txtChildService.Text = Formatter.ToCurrency(0);
            txtInfantsService.Text = Formatter.ToCurrency(0);
            

            txtAdultUrgFee.Text = Formatter.ToCurrency(0);
            txtChildUrgFee.Text = Formatter.ToCurrency(0);
            txtInfantUrgFee.Text = Formatter.ToCurrency(0);

            txtAdultsAdd1Fee.Text = Formatter.ToCurrency(0);
            txtChildAdd1Fee.Text = Formatter.ToCurrency(0);
            txtInfantAdd1Fee.Text = Formatter.ToCurrency(0);

            txtAdultsAdd2Fee.Text = Formatter.ToCurrency(0);
            txtChildAdd2Fee.Text = Formatter.ToCurrency(0);
            txtInfantAdd2Fee.Text = Formatter.ToCurrency(0);

            txtAdultCourierFee.Text = Formatter.ToCurrency(0);
            txtChildCourierFee.Text = Formatter.ToCurrency(0);
            txtInfantCourierFee.Text = Formatter.ToCurrency(0);
            txtProcessing.Text = string.Empty;
            ddlCountry.SelectedIndex = 0;
            ddlNationality.SelectedIndex = 0;            
            ddlResidence.SelectedIndex = 0;

            ddlVisaType.Items.Clear();
            ddlVisaType.Items.Insert(0, new ListItem("--Select Visa Type--", "-1"));
            ddlVisaType.Items.Insert(1, new ListItem("All", "0"));
            ddlVisaType.SelectedIndex = 0;

            ddlVisaCategory.SelectedIndex = 0;
            
            CurrentObject = null;
            btnSave.Text = "Submit";
        }
        catch { throw; }      
    }

    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[GV_FEE_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["CHARGE_ID"] };
            Session[GV_FEE_SEARCH_SESSION] = value;
        }
    }

    private GVFeeMaster  CurrentObject
    {
        get
        {
            return (GVFeeMaster)Session[GVFEE_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(GVFEE_SESSION);
            }
            else
            {
                Session[GVFEE_SESSION] = value;
            }
        }
    }
    private DataTable VisaFeeDetailsList
    {
        get
        {
            DataTable dtVisa = null;

            dtVisa = ViewState[GV_FEE_DETAILS] as DataTable;
            return dtVisa;
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["adsvc_id"] };
            ViewState[GV_FEE_DETAILS] = value;
        }
    }
    private void Save()
    {
        try
        {
            GVFeeMaster  gvVisaFee;
            if (CurrentObject == null)
            {
                gvVisaFee = new GVFeeMaster();
            }
            else
            {
                gvVisaFee  = CurrentObject;
            }
            
            gvVisaFee.CountryID= Utility.ToString(ddlCountry.SelectedItem.Value);  
            gvVisaFee.NationalityId=Utility.ToString(ddlNationality.SelectedItem.Value);
            gvVisaFee.VisaTypeId= Utility.ToLong(ddlVisaType.SelectedItem.Value);           
            gvVisaFee.ResidenceId=Utility.ToString(ddlResidence.SelectedItem.Value);          
            gvVisaFee.VisaCategory=Utility.ToString(ddlVisaCategory.SelectedItem.Value);
           
            gvVisaFee.AdultVisaFee = Utility.ToDecimal(txtAdultsVisaFee.Text.Trim());
            gvVisaFee.AdultService = Utility.ToDecimal(txtAdultsService.Text.Trim());
            gvVisaFee.AdultUrgentFee=Utility.ToDecimal(txtAdultUrgFee.Text.Trim());
            gvVisaFee.AdultAdd1Fee= Utility.ToDecimal(txtAdultsAdd1Fee.Text.Trim());
            gvVisaFee.AdultAdd2Fee = Utility.ToDecimal(txtAdultsAdd2Fee.Text.Trim());
            gvVisaFee.AdultAdd3Fee = Utility.ToDecimal(txtAdultCourierFee.Text.Trim());
            
            gvVisaFee.ChildVisaFee  = Utility.ToDecimal(txtChildVisaFee.Text.Trim());
            gvVisaFee.ChildService = Utility.ToDecimal(txtChildService.Text.Trim());
            gvVisaFee.ChildUrgentFee = Utility.ToDecimal(txtChildUrgFee.Text.Trim());
            gvVisaFee.ChildAdd1Fee = Utility.ToDecimal(txtChildAdd1Fee.Text.Trim());
            gvVisaFee.ChildAdd2Fee = Utility.ToDecimal(txtChildAdd2Fee.Text.Trim());
            gvVisaFee.ChildAdd3Fee = Utility.ToDecimal(txtChildCourierFee.Text.Trim());

            gvVisaFee.InfantVisaFee = Utility.ToDecimal(txtInfantVisaFee.Text.Trim());                       
            gvVisaFee.InfantService  = Utility.ToDecimal(txtInfantsService.Text.Trim());
            gvVisaFee.InfantUrgentFee = Utility.ToDecimal(txtInfantUrgFee.Text.Trim());
            gvVisaFee.InfantAdd1Fee  = Utility.ToDecimal(txtInfantAdd1Fee.Text.Trim());
            gvVisaFee.InfantAdd2Fee = Utility.ToDecimal(txtInfantAdd2Fee.Text.Trim());
            gvVisaFee.InfantAdd3Fee = Utility.ToDecimal(txtInfantCourierFee.Text.Trim());
            
            gvVisaFee.Status = Settings.ACTIVE;
            gvVisaFee.CreatedBy = Settings.LoginInfo.UserID;
            gvVisaFee.ProcessingDays = Utility.ToInteger(txtProcessing.Text.Trim());
            gvVisaFee.Save();                

            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage(" Visa Fee Details are", "", (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
            Clear();
       }
       catch { throw; }
    }

    private void Delete()
    {
        try
        {
            GVFeeMaster obVisaFee = CurrentObject;
            
            obVisaFee.Status = Settings.DELETED;
            obVisaFee.CreatedBy  = Settings.LoginInfo.UserID;
            obVisaFee.Save();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage("Visa Fee Details are", "", (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Deleted));
            Clear();
        }
        catch
        { throw; }
    }

    private void Edit(long id)
    {
        try
        {
            GVFeeMaster gvVisaFee = new GVFeeMaster(id);
            CurrentObject = gvVisaFee;
            ddlCountry.SelectedValue=Utility.ToString(gvVisaFee.CountryID); 
            ddlNationality.SelectedValue =Utility.ToString(gvVisaFee.NationalityId);
            BindVisaType(ddlCountry.SelectedValue);
            ddlVisaType.SelectedValue = Utility.ToString(gvVisaFee.VisaTypeId);
            ddlResidence.SelectedValue = Utility.ToString(gvVisaFee.ResidenceId);
            ddlVisaCategory.SelectedValue = Utility.ToString(gvVisaFee.VisaCategory);

            txtAdultsService.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.AdultService));
            txtAdultsVisaFee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.AdultVisaFee));
            txtAdultUrgFee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.AdultUrgentFee));
            txtAdultsAdd1Fee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.AdultAdd1Fee));
            txtAdultsAdd2Fee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.AdultAdd2Fee));
            txtAdultCourierFee.Text = Formatter.ToCurrency(Utility.ToShort(gvVisaFee.AdultAdd3Fee));

            txtChildService.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.ChildService));
            txtChildVisaFee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.ChildVisaFee));
            txtChildUrgFee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.ChildUrgentFee));
            txtChildAdd1Fee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.ChildAdd1Fee));
            txtChildAdd2Fee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.ChildAdd2Fee));
            txtChildCourierFee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.ChildAdd3Fee));

            txtInfantsService.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.InfantService) );
            txtInfantVisaFee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.InfantVisaFee));
            txtInfantUrgFee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.InfantUrgentFee));
            txtInfantAdd1Fee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.InfantAdd1Fee));
            txtInfantAdd2Fee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.InfantAdd2Fee));
            txtInfantCourierFee.Text = Formatter.ToCurrency(Utility.ToString(gvVisaFee.InfantAdd3Fee));
            txtProcessing.Text = Utility.ToString(gvVisaFee.ProcessingDays);
            btnSave.Text = "Update";
        }
        catch
        {
            throw;
        }
    }

    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            long gvVisaFeeId = Utility.ToLong(gvSearch.SelectedValue);
            Edit(gvVisaFeeId);
            this.Master.HideSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

  
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{ "HTtxtVisaCategory", "charge_visa_category_name" },
            { "HTtxtCountry", "charge_country_name" },{ "HTtxtNationality", "charge_nationality_name" },{"HTxtVisaType","charge_visa_type_name"},
            {"HTtxtResidence","charge_residence_name"}};
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    #endregion


    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindVisaType(ddlCountry.SelectedValue);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }
}
