using System;
using System.Data;
using System.Configuration;
using System.Collections;
using CT.Core;
using System.IO;
using System.Xml.Serialization;
using System.Text;
using CT.BookingEngine;
using System.Threading;
using CT.Configuration;
using CT.TicketReceipt.BusinessLayer;
using CT.AccountingEngine;
using System.Collections.Specialized;
using Encryption.AES;
using SafexPay;
using System.Collections.Generic;
using System.Linq;

public partial class PaymentProcessing : System.Web.UI.Page
{
    private string debugData = "";
    protected string siteName = string.Empty;
    protected AgentMaster agency = new AgentMaster();
    protected string isDomesticReturnView = string.Empty;
    protected decimal totalAmount = 0;
    protected bool isDomestic = false;
    protected FlightItinerary itinerary = new FlightItinerary();
    protected string ipAddr;
    protected string orderId = string.Empty;
    protected string paymentGatewaySite;
    protected string trackId;
    protected string paymentGateway = string.Empty;
    protected const string ONWARD_FLIGHT_ITINERARY = "OnwardFlightItinerary";
    protected const string RETURN_FLIGHT_ITINERARY = "ReturnFlightItinerary";
    protected NameValueCollection data;
    protected Dictionary<string, object> SessionValues = new Dictionary<string, object>();
    protected string[] pageParams = null;

    private class VPCStringComparer : IComparer
    {
        public int Compare(Object a, Object b)
        {
            if (a == b) return 0;
            if (a == null) return -1;
            if (b == null) return 1;

            // Ensure we have string to compare
            string sa = a as string;
            string sb = b as string;

            // Get the CompareInfo object to use for comparing
            System.Globalization.CompareInfo myComparer = System.Globalization.CompareInfo.GetCompareInfo("en-US");
            if (sa != null && sb != null)
            {
                // Compare using an Ordinal Comparison.
                return myComparer.Compare(sa, sb, System.Globalization.CompareOptions.Ordinal);
            }
            throw new ArgumentException("a and b should be strings.");
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        try
        {
            string themeName = (string)Session["themeName"];
            if (themeName != null)
            {
                this.Page.Theme = themeName;
            }
            else
            {
                this.Page.Theme = "Default";
            }
        }
        catch { }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();
        try
        {
            SessionValues = Session["PaxPageValues"] as Dictionary<string, object>;            
            pageParams = GenericStatic.GetSetPageParams("PageParams", "", "get").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (pageParams != null && pageParams.Length > 1)
            {
                paymentGateway = pageParams[1];
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
            
            
            ipAddr = Request.ServerVariables["REMOTE_ADDR"];
            
            siteName = BookingUtility.ExtractSiteName(Request["HTTP_HOST"]);
            orderId = new Random().Next(1, 2147483647).ToString();         
                 
           if (paymentGateway == "CozmoPg")//for cozmo payment Gate Way
            {
                trackId = orderId;
                Session["trackId"] = trackId;
                CozmoPaymentGateway();
            }
            else
            {
                if (paymentGateway == "CCAvenue") //Condition Added by somasekhar on 27/06/2018 
                {
                    paymentGatewaySite = ConfigurationManager.AppSettings["CCAvenue_Url"];
                }

                
                if (Session["PaxPageValues"] != null)
                {
                    SessionValues = Session["PaxPageValues"] as Dictionary<string, object>;
                    FlightItinerary onwardItinerary = SessionValues[ONWARD_FLIGHT_ITINERARY] as FlightItinerary;
                    FlightItinerary returnItinerary = SessionValues[RETURN_FLIGHT_ITINERARY] as FlightItinerary;
                    itinerary = onwardItinerary;//for booking check 

                    if (onwardItinerary.FlightBookingSource != BookingSource.UAPI)
                    {
                        orderId = "FL-" + onwardItinerary.Passenger[0].PassportNo + "-" + orderId;
                    }
                    else
                    {
                        orderId = "FL-" + itinerary.PNR + "-" + orderId;
                    }
                    if (orderId.Length > 30)
                    {
                        orderId = orderId.Substring(0, 30);
                    }

                    foreach (FlightPassenger pax in onwardItinerary.Passenger)
                    {
                        if (onwardItinerary.FlightBookingSource != BookingSource.TBOAir)
                        {
                            totalAmount += pax.Price.PublishedFare + pax.Price.Tax + pax.Price.BaggageCharge + pax.Price.Markup + pax.Price.OutputVATAmount + pax.Price.HandlingFeeAmount - pax.Price.Discount;
                        }
                        else
                        {
                            totalAmount += Math.Ceiling(pax.Price.PublishedFare + pax.Price.Tax + pax.Price.BaggageCharge + pax.Price.Markup + pax.Price.OutputVATAmount + pax.Price.HandlingFeeAmount - pax.Price.Discount + pax.Price.OtherCharges + pax.Price.AdditionalTxnFee + pax.Price.SServiceFee + pax.Price.TransactionFee);
                        }
                    }
                    if (returnItinerary != null)
                    {
                        foreach (FlightPassenger pax in returnItinerary.Passenger)
                        {
                            if (returnItinerary.FlightBookingSource != BookingSource.TBOAir)
                            {
                                totalAmount += pax.Price.PublishedFare + pax.Price.Tax + pax.Price.BaggageCharge + pax.Price.MealCharge + pax.Price.SeatPrice + pax.Price.Markup + pax.Price.OutputVATAmount + pax.Price.HandlingFeeAmount - pax.Price.Discount;
                            }
                            else
                            {
                                totalAmount += Math.Ceiling(pax.Price.PublishedFare + pax.Price.Tax + pax.Price.BaggageCharge + pax.Price.MealCharge + pax.Price.SeatPrice + pax.Price.Markup + pax.Price.OutputVATAmount + pax.Price.HandlingFeeAmount - pax.Price.Discount + pax.Price.OtherCharges + pax.Price.AdditionalTxnFee + pax.Price.SServiceFee + pax.Price.TransactionFee);
                            }
                        }
                    }
                }
                //Generating orderNumber
                else if (Session["FlightItinerary"] != null)
                {
                    itinerary = Session["FlightItinerary"] as FlightItinerary;
                    //orderNo=FL-PNR-random num - UAPI
                    //orderNo=FL-Passport num-random num - LCC
                    if (itinerary.FlightBookingSource != BookingSource.UAPI)                    
                        orderId = "FL-" + itinerary.Passenger[0].PassportNo + "-" + orderId;                    
                    else                    
                        orderId = "FL-" + itinerary.PNR + "-" + orderId;
                    
                    if (orderId.Length > 30)                    
                        orderId = orderId.Substring(0, 30);                    

                    itinerary.Passenger.ToList().ForEach(pax =>
                    {
                        totalAmount += pax.Price.PublishedFare + pax.Price.Tax + pax.Price.BaggageCharge + pax.Price.Markup + pax.Price.OutputVATAmount + pax.Price.HandlingFeeAmount - pax.Price.Discount + pax.Price.MealCharge + pax.Price.SeatPrice;
                        totalAmount += (itinerary.FlightBookingSource == BookingSource.TBOAir) ? pax.Price.OtherCharges + pax.Price.AdditionalTxnFee + pax.Price.SServiceFee + pax.Price.TransactionFee : 0;
                    });

                    //For TBOAir ceil the amount before sending to Payment
                    if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        totalAmount = Math.Ceiling(totalAmount);
                    }
                }

                int agentId = 0; decimal charges = 0;
                try
                {
                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        agentId =Settings.LoginInfo.OnBehalfAgentID;
                    }
                    else
                    {
                        agentId = Settings.LoginInfo.AgentId;
                    }
                    agency = new AgentMaster(agentId);

                    DataTable dtChrges = AgentCreditCardCharges.GetList();
                    DataView dv = dtChrges.DefaultView;
                    if (paymentGateway == "CCAvenue")
                    {
                        dv.RowFilter = "Productid IN ('" + 1 + "') AND Agentid IN ('" + agentId + "') AND PGSource IN ('" + (int)CT.AccountingEngine.PaymentGatewaySource.CCAvenue + "')";
                    }
                    else if (paymentGateway == "SafexPay")// Added by Somasekhar on 28/06/2018
                    {
                        dv.RowFilter = "Productid IN ('" + 1 + "') AND Agentid IN ('" + agentId + "') AND PGSource IN ('" + (int)CT.AccountingEngine.PaymentGatewaySource.SafexPay + "')";
                    }
                    else if (paymentGateway == "RazorPay")// Added by Shiva on 12 Jun 2019
                    {
                        dv.RowFilter = "Productid IN ('" + 1 + "') AND Agentid IN ('" + agentId + "') AND PGSource IN ('" + (int)CT.AccountingEngine.PaymentGatewaySource.RazorPay + "')";
                    }
                    dtChrges = dv.ToTable();
                    if (dtChrges != null && dtChrges.Rows.Count > 0)
                    {
                        charges = Convert.ToDecimal(dtChrges.Rows[0]["PGCharge"]);                        
                    }


                }
                catch(Exception ex) { Audit.Add(EventType.Exception, Severity.High, 1, "Failed to filter PG Charges in Flight Payment Gateway. Reason: " + ex.ToString(), Request["REMOTE_ADDR"]); }

               //Calculate Credit Card charges for the booking amount
                decimal ccCharges = (totalAmount * charges / 100);

               //Save Credit card payment details before hitting the payment gateway
                CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                creditCard.Address = itinerary.Passenger[0].AddressLine1;
                creditCard.Amount = totalAmount;
                creditCard.Charges = ccCharges;
                creditCard.IPAddress = ipAddr;
                //Added by Somasekhar on 27/06/2018 -- For SafexPay/CCAvenue PG
                if (paymentGateway == "CCAvenue")
                {
                    creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.CCAvenue;
                }
                else if (paymentGateway == "SafexPay")
                {
                    creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.SafexPay;
                }
                else if(paymentGateway == "RazorPay")
                {
                    creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.RazorPay;
                }
                //=====================================
                creditCard.PaymentStatus = 0;
                creditCard.ReferenceType = CT.AccountingEngine.ReferenceIdType.Invoice;
                if (!itinerary.IsLCC)
                {
                    creditCard.ReferenceId = new FlightItinerary(FlightItinerary.GetFlightId(itinerary.PNR)).BookingId;
                }
                else
                {
                    creditCard.ReferenceId = 0;
                }
                if (Request.QueryString["redirect_url"] == null)
                {
                    creditCard.Remarks = "Booking In Progress";
                }
                else
                {
                    if (Request.QueryString["redirect_url"] == "CreateTicket.aspx")
                    {
                        creditCard.Remarks = "Create Ticket In Progress";
                    }
                    else if (Request.QueryString["redirect_url"] == "DisplayPNRInformation.aspx")
                    {
                        creditCard.Remarks = "Import PNR In Progress";
                    }
                }
                creditCard.TrackId = orderId;
                creditCard.TransType = "B2B";
                creditCard.PaymentId = string.Empty;
                creditCard.Save();

                Session["PaymentInformationId"] = creditCard.PaymentInformationId;

                totalAmount += ccCharges;
                totalAmount = Math.Round(totalAmount, agency.DecimalValue);
                orderId.Replace(" ", "-");
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["TestMode"]))
                {
                    //TO DO
                    totalAmount = 1;
                    if (paymentGateway == "SafexPay")
                        totalAmount = 10; // for safex Pay Purpose -- Added by somasekhar on 28/06/2018

                }


                try
                {
                    if (!string.IsNullOrEmpty(Session["sessionId"].ToString()))
                    {
                        //Serialize session into a file and reload in the confirmation page after payment failure or success
                        SessionValues = new Dictionary<string, object>();

                        if (Session["PaxPageValues"] != null)
                        {
                            Dictionary<string, object> SessionData = Session["PaxPageValues"] as Dictionary<string, object>;
                            foreach (KeyValuePair<string, object> pair in SessionData)
                            {
                                SessionValues.Add(pair.Key, pair.Value);
                            }
                        }
                        else
                        {
                            SessionValues.Add("ResultIndex", Session["ResultIndex"]);
                            SessionValues.Add("FlightItinerary", Session["FlightItinerary"]);
                            SessionValues.Add("FlightRequest", Session["FlightRequest"]);
                            SessionValues.Add("UAPIURImpresp", Session["UAPIURImpresp"]);
                            SessionValues.Add("UAPIReprice", Session["UAPIReprice"]);
                            SessionValues.Add("BookingResponse", Session["BookingResponse"]);
                        }

                        SessionValues.Add("LoginInfo", Settings.LoginInfo);
                        SessionValues.Add("PaymentInformationId", creditCard.PaymentInformationId);
                        SessionValues.Add("PageParams", Session["PageParams"]);
                        GenericStatic.Save(Session["sessionId"].ToString(), SessionValues, CT.BookingEngine.ResultCacheType.BookingDetails.ToString());
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Failed to save session before Payment Gateway: " + ex.ToString(), Request["REMOTE_ADDR"]);
                }

                if (paymentGateway == "SafexPay")
                {
                    SafexPaymentGateway();
                }
                //else if (paymentGateway == "RazorPay")
                //{
                //    Dictionary<string, object> input = new Dictionary<string, object>();
                //    input.Add("amount", totalAmount * 100); // this amount should be same as transaction amount multiplied with 100 to get in paisa
                //    input.Add("currency", "INR");
                //    input.Add("receipt", orderId);
                //    input.Add("payment_capture", 1);
                //    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                //    string key = ConfigurationSystem.RazorPayConfig["Key"];
                //    string secret = ConfigurationSystem.RazorPayConfig["Secret"];
                //    Razorpay.Api.RazorpayClient client = new Razorpay.Api.RazorpayClient(key, secret);
                //    Razorpay.Api.Order order = client.Order.Create(input);
                //    trackId = order["id"].ToString();
                //}
            }          
        }
        catch (ThreadAbortException)
        {
            //Do nothing when thread aborted
        }
        catch (Exception ex)
        {
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            Audit.Add(EventType.CCAvenue, Severity.High, (int)Settings.LoginInfo.UserID, "Error message :" + ex.Message + ex.StackTrace, ipAddr);
            Email.Send(hostPort["fromEmail"].ToString(), hostPort["ErrorNotificationMailingId"].ToString(), siteName + " Error", ex.Message + "\n StackTrace:" + ex.StackTrace + "\n InnerExcepstion: " + ex.InnerException + "\n source: " + ex.Source + "\n data: " + ex.Data);
            if (Request.QueryString["redirect_url"] == null)
            {
                Response.Redirect("PaymentConfirmation.aspx?ErrorPG=" + ex.ToString());
            }
            else
            {
                Response.Redirect(Request.QueryString["redirect_url"] + "?ErrorPG=" + ex.ToString());
            }
        }
    }

    private string GetPassengerResponseXml(FlightPassenger[] passenger)
    {
        MemoryStream stream = null;
        TextWriter writer = null;
        try
        {
            stream = new MemoryStream(); // read xml in memory

            writer = new StreamWriter(stream, Encoding.Unicode);
            // get serialise object
            XmlRootAttribute root = new XmlRootAttribute("Passenger");
            XmlSerializer serializer = new XmlSerializer(typeof(FlightPassenger), root);

            serializer.Serialize(writer, passenger); // read object
            int count = (int)stream.Length; // saves object in memory stream

            byte[] arr = new byte[count];
            stream.Seek(0, SeekOrigin.Begin);
            // copy stream contents in byte array

            stream.Read(arr, 0, count);
            UnicodeEncoding utf = new UnicodeEncoding(); // convert byte array to string

            return utf.GetString(arr).Trim();
        }
        catch
        {
            return string.Empty;
        }
        finally
        {
            if (stream != null) stream.Close();
            if (writer != null) writer.Close();
        }
    }

    //for cozmo payment gate way implementation.

    private string CreateMD5Signature(string RawData)
    {
        System.Security.Cryptography.MD5 hasher = System.Security.Cryptography.MD5CryptoServiceProvider.Create();
        byte[] HashValue = hasher.ComputeHash(Encoding.ASCII.GetBytes(RawData));

        string strHex = "";
        foreach (byte b in HashValue)
        {
            strHex += b.ToString("x2");
        }
        return strHex.ToUpper();
    }

    private void CozmoPaymentGateway()
    {
        string itineraryDetail1 = string.Empty;

        if (Convert.ToBoolean(ConfigurationManager.AppSettings["TestMode"]))
        {
            totalAmount = 100;
        }
        else
        {
            totalAmount = totalAmount * 100;
        }

        string amount = Math.Round(totalAmount).ToString();

        if (Session["FlightItinerary"] != null)
        {
            itinerary = Session["FlightItinerary"] as FlightItinerary;
        }

        //itineraryDetail1 = "Flight Booking";
        if(!string.IsNullOrEmpty(itinerary.PNR))
            itineraryDetail1 = itinerary.PNR;   
        else
            itineraryDetail1="LCC Flight Booking";

        string SECURE_SECRET = ConfigurationManager.AppSettings["CozmoPg_SECURE_SECRET"];       
        //try
        //{  
            System.Collections.SortedList transactionData = new System.Collections.SortedList(new VPCStringComparer());
            string queryString = ConfigurationManager.AppSettings["CozmoPg_queryString"];            
            transactionData.Add("vpc_Version", "1");
            transactionData.Add("vpc_Command", ConfigurationManager.AppSettings["CozmoPg_Command"]);
            transactionData.Add("vpc_AccessCode", ConfigurationManager.AppSettings["CozmoPg_AccessCode"]);
            transactionData.Add("vpc_MerchTxnRef", orderId);//user
            transactionData.Add("vpc_Merchant", ConfigurationManager.AppSettings["CozmoPg_Merchant"]);
            
            transactionData.Add("vpc_OrderInfo", itineraryDetail1);//user        
            //amount = "100";
            transactionData.Add("vpc_Amount", amount);
            //siteName = "localhost:1235/CozmoWeb";        
            //transactionData.Add("vpc_ReturnURL",Request.Url.Scheme+"://" + siteName + "/Booking.aspx");
            transactionData.Add("vpc_ReturnURL", Request.Url.Scheme+"://" + siteName + "/" + ConfigurationManager.AppSettings["RootFolder"].ToString() + "/PaymentConfirmation.aspx");

            transactionData.Add("vpc_Locale", ConfigurationManager.AppSettings["CozmoPg_Locale"]);
            //transactionData.Add("AgainLink", Request.Url.Scheme+"://" + siteName + "/FlightBooking.aspx");
            transactionData.Add("AgainLink", Request.Url.Scheme+"://" + siteName + "/" + ConfigurationManager.AppSettings["RootFolder"].ToString() + "/PaymentConfirmation.aspx");
            transactionData.Add("Title", "ASP.NET C# VPC 3-Party");
            string rawHashData = SECURE_SECRET;
            string seperator = "?";
            foreach (System.Collections.DictionaryEntry item in transactionData)
            {
                queryString += seperator + System.Web.HttpUtility.UrlEncode(item.Key.ToString()) + "=" + System.Web.HttpUtility.UrlEncode(item.Value.ToString());
                seperator = "&";
             
                if (SECURE_SECRET.Length > 0)
                {
                    rawHashData += item.Value.ToString();
                }
            }
            string signature = "";
            if (SECURE_SECRET.Length > 0)
            {
                // create the signature and add it to the query string
                signature = CreateMD5Signature(rawHashData);
                queryString += seperator + "vpc_SecureHash=" + signature;
             }

             Response.Redirect(queryString);
    }   

    

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }


    //Added by Somasekhar on 27/06/2018
    #region SafexPay

    private void SafexPaymentGateway()
    {
        SafexPayService safexPay = new SafexPayService();
        string appName = Request["HTTP_HOST"];
        string redirectUrl = Request.Url.Scheme+"://" + siteName + "/";

        try
        {

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["RootFolder"]))
            {
                redirectUrl += ConfigurationManager.AppSettings["RootFolder"] + "/";
                safexPay.success_url = redirectUrl;
                safexPay.failure_url = redirectUrl;

            }

            if (Request.QueryString["redirect_url"] == null)
            {
                redirectUrl += "ViewSwitcher.aspx?" + "&uid=" + Settings.LoginInfo.UserID + "&sid=" + Session["sessionId"].ToString() + (Session["PaxPageValues"] != null ? "&type=C" : "&type=N");
                safexPay.success_url = redirectUrl;
                safexPay.failure_url = redirectUrl;
            }
            else
            {
                redirectUrl += Request.QueryString["redirect_url"];
                safexPay.success_url = redirectUrl;
                safexPay.failure_url = redirectUrl;
            }

            paymentGatewaySite = safexPay.safexPaymentUrl;
            safexPay.order_no = orderId;

            #region 

            try
            {

                //The below transaction request fields are mandatory.
                #region mandatory transaction request fields .
                if (string.IsNullOrEmpty(safexPay.ag_id) && safexPay.ag_id.Length > 20)
                {
                    throw new Exception("Safex payment gateway:Need to specify Aggregator id with length of <= 20 for the transaction");
                }
                else if (string.IsNullOrEmpty(safexPay.me_id) && safexPay.me_id.Length > 20)
                {
                    throw new Exception("Safex payment gateway:Need to specify Merchant ID with length of <= 20 for the transaction");
                }
                else if (string.IsNullOrEmpty(safexPay.order_no) && safexPay.order_no.Length > 70)
                {
                    throw new Exception("Safex payment gateway:Need to specify Order Number with length of <= 70 for the transaction");
                }
                else if (totalAmount < 1)
                {
                    throw new Exception("Safex payment gateway:Need to specify Amount with length of <= 10 digits for the transaction");
                }
                else if (string.IsNullOrEmpty(safexPay.Country) && safexPay.Country.Length != 3)
                {
                    throw new Exception("Safex payment gateway:Need to specify Country with length of 3 for the transaction");
                }
                else if (string.IsNullOrEmpty(safexPay.Currency) && safexPay.Currency.Length != 3)
                {
                    throw new Exception("Safex payment gateway:Need to specify Currency with length of 3 for the transaction");
                }
                else if (string.IsNullOrEmpty(safexPay.txn_type) && safexPay.txn_type.Length > 10)
                {
                    throw new Exception("Safex payment gateway:Need to specify Transaction Type with length of 10 for the transaction");
                }

                else if (string.IsNullOrEmpty(safexPay.success_url) && safexPay.success_url.Length > 100)
                {
                    throw new Exception("Safex payment gateway:Need to specify success Url with length of <= 100 for the transaction");
                }
                else if (string.IsNullOrEmpty(safexPay.failure_url) && safexPay.failure_url.Length > 100)
                {
                    throw new Exception("Safex payment gateway:Need to specify failure Url with length of <= 100 for the transaction");
                }
                else if (string.IsNullOrEmpty(safexPay.Channel) && safexPay.Channel.Length > 10)
                {
                    throw new Exception("Safex payment gateway:Need to specify Channel with length of <= 10 for the transaction");
                }
                #endregion

                safexPay.txn_details = safexPay.ag_id + "|" + safexPay.me_id + "|" + safexPay.order_no + "|" + Math.Round(totalAmount, agency.DecimalValue) + "|" + safexPay.Country + "|" + safexPay.Currency + "|" + safexPay.txn_type + "|" + safexPay.success_url + "|" + safexPay.failure_url + "|" + safexPay.Channel;
                safexPay.pg_details = safexPay.pg_id + "|" + safexPay.Paymode + "|" + safexPay.Scheme + "|" + safexPay.emi_months;
                safexPay.card_details = safexPay.card_no + "|" + safexPay.exp_month + "|" + safexPay.exp_year + "|" + safexPay.cvv2 + "|" + safexPay.card_name;
                //safexPay.cust_details = itinerary.Passenger[0].FirstName + itinerary.Passenger[0].LastName + "|" + itinerary.Passenger[0].Email + "|" + itinerary.Passenger[0].CellPhone + "|" + itinerary.Passenger[0].Email + "|" + "Y";
                safexPay.cust_details = safexPay.cust_name + "|" + safexPay.email_id + "|" + safexPay.mobile_no + "|" + safexPay.unique_id + "|" + "Y";
                safexPay.bill_details = safexPay.bill_address + "|" + safexPay.bill_city + "|" + safexPay.bill_state + "|" + safexPay.bill_country + "|" + safexPay.bill_zip;
                safexPay.ship_details = safexPay.ship_address + "|" + safexPay.ship_city + "|" + safexPay.ship_state + "|" + safexPay.ship_country + "|" + safexPay.ship_zip + "|" + safexPay.ship_days + "|" + safexPay.address_count;
                safexPay.item_details = safexPay.item_count + "|" + safexPay.item_value + "|" + safexPay.item_category;
                safexPay.other_details = safexPay.udf1 + "|" + safexPay.udf2 + "|" + safexPay.udf3 + "|" + safexPay.udf4 + "|" + safexPay.udf5;

                try
                {
                    //request Log without encryption
                    safexPay.WriteRequest();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, "Error message :" + ex.Message + ex.ToString(), ipAddr);

                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, "Error message :" + ex.Message + ex.ToString(), ipAddr);
            }

            #endregion



            CryptoClass aes = new CryptoClass();
            try
            {
                aes.order_no = orderId;
                aes.enc_txn_details = aes.encrypt(safexPay.txn_details, safexPay.txnMerchantKey);
                aes.enc_pg_details = aes.encrypt(safexPay.pg_details, safexPay.txnMerchantKey);
                aes.enc_card_details = aes.encrypt(safexPay.card_details, safexPay.txnMerchantKey);
                aes.enc_cust_details = aes.encrypt(safexPay.cust_details, safexPay.txnMerchantKey);
                aes.enc_bill_details = aes.encrypt(safexPay.bill_details, safexPay.txnMerchantKey);
                aes.enc_ship_details = aes.encrypt(safexPay.ship_details, safexPay.txnMerchantKey);
                aes.enc_item_details = aes.encrypt(safexPay.item_details, safexPay.txnMerchantKey);
                aes.enc_other_details = aes.encrypt(safexPay.other_details, safexPay.txnMerchantKey);

                try
                { //request Log with encryption
                    safexPay.WriteEncryptRequest(aes);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, "Error message :" + ex.Message + ex.ToString(), ipAddr);

                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, "Error message :" + ex.Message + ex.ToString(), ipAddr);

            }
            data = new NameValueCollection();
            data.Add("me_id", safexPay.me_id);
            data.Add("txn_details", aes.enc_txn_details);
            data.Add("pg_details", aes.enc_pg_details);
            data.Add("card_details", aes.enc_card_details);
            data.Add("cust_details", aes.enc_cust_details);
            data.Add("bill_details", aes.enc_bill_details);
            data.Add("ship_details", aes.enc_ship_details);
            data.Add("item_details", aes.enc_item_details);
            data.Add("other_details", aes.enc_other_details);

            //    HttpHelper.RedirectAndPOST(this.Page, "https://test.avantgardepayments.com/agcore/payment", data);

        }
        catch (Exception ex)
        {

            Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, "Error message :" + ex.Message + ex.StackTrace, ipAddr);
        }

    }

    #endregion
}

