﻿using System;
using CT.Core;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using Visa;

public partial class AddVisaFee : CT.Core.ParentPage
{
    protected bool isAdmin = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();   //commented by vijesh 12-02-2015
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
        {
            isAdmin = true;
        }
        if (!IsPostBack)
        {
            BindAgent();

            ddlCountry.DataSource = VisaCountry.GetActiveVisaCountryList();
            ddlCountry.DataTextField = "countryName";
            ddlCountry.DataValueField = "countryCode";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Select Country","-1"));

            ddlNationality.DataSource = VisaNationality.GetNationalityList();
            ddlNationality.DataTextField = "nationalityName";
            ddlNationality.DataValueField = "nationalityCode";
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("Select Nationality", "-1"));

            if (Request.QueryString.Count > 0 && Request.QueryString[0].Trim() != "")
            {
                int feeId;
                bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out feeId);
                if (!isnumerice)
                {
                    Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                    Response.End();

                }
                try
                {
                    VisaFee fee = new VisaFee(feeId);
                    if (fee != null)
                    {
                        ///<summary>
                        /*<summary>
                          1.Get VisaType Using VisaTypeId.
                          2.Get Country Code from VisaType.
                          3.Now using CounterCode Fill Both DropDown List ddlVisaType
                            ddlVisaCity.
                          4.Now Change Select Index Of Both DropDown List using DestinationCityCode
                            VisaTypeId.
                    
                         */
                        ///</summary>
                        VisaType visaType = new VisaType(fee.VisaTypeId);
                        //BindAgent();
                        ddlAgent.SelectedValue = Convert.ToString(fee.AgentId);
                        ddlAgent.Enabled = false;
                        ddlVisaType.DataSource = VisaType.GetVisaTypeList(visaType.CountryCode, Convert.ToInt32(ddlAgent.SelectedValue));
                        ddlVisaType.DataTextField = "visaTypeName";
                        ddlVisaType.DataValueField = "visaTypeId";
                        ddlVisaType.DataBind();
                        ddlCity.DataSource = VisaCity.GetCityList(visaType.CountryCode);
                        ddlCity.DataTextField = "cityName";
                        ddlCity.DataValueField = "cityCode";
                        ddlCity.DataBind();
                        ddlCity.Items.Insert(0, new ListItem("Select", "-1"));
                        //ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(visaType.CountryCode));
                        ddlCountry.SelectedValue = visaType.CountryCode;
                        //ddlCity.SelectedIndex = ddlCity.Items.IndexOf(ddlCity.Items.FindByValue(fee.DestinationCityCode));
                        ddlCity.SelectedValue = fee.DestinationCityCode;
                        ddlNationality.SelectedValue = fee.NationalityCode;
                        ddlVisaType.SelectedIndex = ddlVisaType.Items.IndexOf(ddlVisaType.Items.FindByValue(fee.VisaTypeId.ToString()));
                        txtCost.Text = Math.Round(fee.Cost, 2).ToString();
                        txtDepositCharge.Text = Math.Round(fee.VisaDepositCharge, 2).ToString();
                        txtIsuranceCharge.Text = Math.Round(fee.InsuranceCharge, 2).ToString();
                        txtRefundable.Text = Math.Round(fee.Refundable, 2).ToString();
                        txtMarkup.Text = Math.Round(fee.Markup, 2).ToString();
                        txtMessage.Text = fee.Message;
                        btnSave.Text = "Update";
                        Page.Title = "Update Visa Fee";
                        lblStatus.Text = "Update Visa Fee";
                    }

                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaFee.aspx,Err:" + ex.Message, "");
                }
            }
            else
            {
                //ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByText("UAE"));
                if (!isAdmin)
                {
                    //ddlAgent.SelectedValue = Convert.ToString(Session["agencyId"]);
                    ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                    ddlAgent.Enabled = false;
                }
            }
        }

    }


    private void BindAgent()
    {
        try
        {
            //int agentId = 0;
            //if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
            //string agentType = (Settings.LoginInfo.AgentType.ToString() != null ? Settings.LoginInfo.AgentType.ToString() : "BASEAGENT");
            //ddlAgent.DataSource = AgentMaster.GetList(1, agentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataSource = AgentMaster.GetB2CAgentList();
            ddlAgent.DataValueField = "agent_Id";
            ddlAgent.DataTextField = "agent_Name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("Select Agent", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        int errMessage = 0;
        try
        {
            VisaFee fee = new VisaFee();
            if (Request.QueryString.Count > 0)
            {
                fee.Load(Convert.ToInt32(Request.QueryString[0]));
            }
            fee.VisaTypeId = Convert.ToInt32(ddlVisaType.SelectedValue);
            fee.Cost = Convert.ToDecimal(txtCost.Text.Trim());
            fee.Markup = Convert.ToDecimal(txtMarkup.Text.Trim());
            fee.InsuranceCharge = Convert.ToDecimal(txtIsuranceCharge.Text.Trim());
            fee.VisaDepositCharge = Convert.ToDecimal(txtDepositCharge.Text.Trim());
            fee.DestinationCityCode = ddlCity.SelectedValue != "-1" ? ddlCity.SelectedValue : string.Empty;
            fee.Refundable = Convert.ToDecimal(txtRefundable.Text.Trim());
            fee.Message = txtMessage.Text.Trim();
            fee.AgentId=Convert.ToInt32(ddlAgent.SelectedItem.Value);
            fee.NationalityCode = ddlNationality.SelectedValue != "-1" ? ddlNationality.SelectedValue : string.Empty;
            if (btnSave.Text == "Update")
            {
                fee.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                fee.Update();
            }
            else
            {
                fee.IsActive = true;
                fee.CreatedBy = (int)Settings.LoginInfo.UserID;
                errMessage = fee.Save();
                if (errMessage != 0)
                {
                    errMesage.Text = "Visa fee is already defined.";
                }
            }



        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaFee,Err:" + ex.Message, "");
        }
        if (errMessage==0)
        Response.Redirect("VisaFeeList.aspx?Country=" + ddlCountry.SelectedItem.Value +"&Agency=" +ddlAgent.SelectedItem.Value );
    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCountry.SelectedIndex != 0)
        {
            ddlVisaType.DataSource = VisaType.GetVisaTypeList(ddlCountry.SelectedValue, Convert.ToInt32(ddlAgent.SelectedValue));
            ddlVisaType.DataTextField = "visaTypeName";
            ddlVisaType.DataValueField = "visaTypeId";
            ddlVisaType.DataBind();



            ddlCity.DataSource = VisaCity.GetCityList(ddlCountry.SelectedValue);
            ddlCity.DataTextField = "cityName";
            ddlCity.DataValueField = "cityCode";
            ddlCity.DataBind();

        }
        else
        {
            ddlVisaType.Items.Clear();
            ddlCity.Items.Clear();

        }
        ddlVisaType.Items.Insert(0, "Select");
        ddlCity.Items.Insert(0, new ListItem("Select","-1"));
    }



    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }

    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ddlCountry.SelectedIndex = -1;

            ddlVisaType.DataSource = VisaType.GetVisaTypeList(ddlCountry.SelectedValue, Convert.ToInt32(ddlAgent.SelectedValue));
            ddlVisaType.DataTextField = "visaTypeName";
            ddlVisaType.DataValueField = "visaTypeId";
            ddlVisaType.DataBind();
            ddlVisaType.Items.Insert(0, new ListItem("Select", "-1"));

            ddlCity.DataSource = VisaCity.GetCityList(ddlCountry.SelectedValue);
            ddlCity.DataTextField = "cityName";
            ddlCity.DataValueField = "cityCode";
            ddlCity.DataBind();
            ddlCity.Items.Insert(0, new ListItem("Select", "-1"));
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, "exception in Adding Visa Fee page :" + ex.ToString(), "");
        }
    }
}
