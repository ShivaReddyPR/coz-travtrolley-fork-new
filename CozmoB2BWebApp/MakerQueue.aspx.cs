﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using System.Collections.Generic;
using System.Net;
using Ionic.Zip;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;


public partial class MakerQueue : CT.Core.ParentPage
{
    protected int rowsCount = 0;
    protected int rowsCount_checker = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            pnlDocuments.Visible = false;
            pnlDocuments1.Visible = false;
            if (!IsPostBack)
            {
                if (Request["Queue"].ToUpper() == "MAKER")
                {
                    txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    BindData_MakerQueue();
                    MakerQueue1.Visible = true;

                }
                else if (Request["Queue"].ToUpper() == "CHECKER")
                {
                    txtFromDate1.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtToDate1.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    BindData_CheckerQueue();
                    CheckerQueue.Visible = true;

                }
            }
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.lnlDownloadAll1);
            ScriptManager scriptManager1 = ScriptManager.GetCurrent(this.Page);
            scriptManager1.RegisterPostBackControl(this.lnlDownloadAll);
            lblErrorMessage.Text = null;
            lblErrorMessage.Visible = false;
            lblErrorMessage1.Text = null;
            lblErrorMessage1.Visible = false;
            LoadDocuments();
            LoadCheckerDocuments();
        }
        catch (Exception ex)
        {

        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        
        Utility.StartupScript(this.Page, "$('.image-link, .pdf-popup').magnificPopup({ callbacks: {elementParse: function(item) {if(item.el.context.className == 'pdf-popup') {"
     + " item.type = 'iframe';} else {item.type = 'image';}  },  afterChange: function () { var magnificPopup = $.magnificPopup.instance;  var fileSrc = $(magnificPopup.currItem).attr(" + "'src'" + ");"
     + "$('.mfp-title').append('<a href=' + fileSrc + ' download target='+ '_blank' + '>Download</a>') } }, gallery:{enabled:true}, type: 'image', }); ", "ImageViewer");

        Utility.StartupScript(this.Page, "$('.image-link, .pdf-popup').click(function () { var fileSrc = $(this).attr('href');$('.mfp-title').append('<a href=' + fileSrc + ' download target='+ '_blank' + '>Download</a>') });", "ImageViewer1");

    }

    #region Maker Queue part

    private void BindData_MakerQueue()
    {
        try
        {
            PaymentCheck PC = new PaymentCheck();
            DataSet ds = PC.GetRequestQueue("P");

            DataView dv = new DataView();
            dv.Table = ds.Tables[0];
            IFormatProvider dateformat = new System.Globalization.CultureInfo("en-GB");
            DateTime fromdate = Convert.ToDateTime(txtFromDate.Text, dateformat);//.ToString("MM/dd/yyyy");
            DateTime toDate = Convert.ToDateTime(txtToDate.Text, dateformat);//.ToString("MM/dd/yyyy");
            if (txtFromDate.Text.Trim().Length > 0 && txtToDate.Text.Trim().Length > 0)
            {
                dv.RowFilter = "(DocDate>= #" + fromdate + "# AND DocDate<=#" + toDate + "#)";
            }
            if (tbCompanyName.Text.Trim().Length > 0)
            {
                if (dv.RowFilter.Length > 0)
                {
                    dv.RowFilter += "and CompanyName LIKE '" + tbCompanyName.Text + "*'";
                }
                else
                {
                    dv.RowFilter = "CompanyName LIKE '" + tbCompanyName.Text + "*'";
                }
            }
            if (ddlCountryName.SelectedIndex > 0)
            {
                if (dv.RowFilter.Length > 0)
                {
                    dv.RowFilter += "and CountryName Like '" + ddlCountryName.SelectedItem.Value + "*'";
                }
                else
                {
                    dv.RowFilter = "CountryName Like '" + ddlCountryName.SelectedItem.Value + "*'";
                }
            }

            //adding to hidden field for drag and drop script iteration purpose;
            hdnMakerRowsCount.Value = dv.Count.ToString();

            gvMakerQueue.DataSource = dv;
            gvMakerQueue.DataBind();
            //rowsCount = dv.Count;
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnUpdate_OnClick(object sender, EventArgs e)
    {
        try
        {
            int updatedCount = 0, failureCount = 0;
            PaymentCheck PC = new PaymentCheck();
            string[] idAndStatus = Convert.ToString((hdnIds1.Value)).Split(',');
            List<string> UploadedInvoices = Session["UploadedInvoiceDetails"] as List<string>;
            if (idAndStatus.Length > 1)
            {
                for (int i = 0; i < idAndStatus.Length; i++)
                {
                    string[] idStatus = idAndStatus[i].Split('-');
                    if ((idStatus[0] != null && idStatus[0] != ""))
                    {
                        int j = PC.updateMakerQueueStatus(Convert.ToInt32(idStatus[0]), idStatus[1], Convert.ToInt32(Settings.LoginInfo.UserID));

                        if (j > 0)
                        {
                            updatedCount++;
                        }
                        else
                        {
                            failureCount++;
                        }
                    }
                }

                if (UploadedInvoices != null && UploadedInvoices.Count > 0)
                {
                    for (int k = 0; k < UploadedInvoices.Count; k++)
                    {
                        string invoicesData = UploadedInvoices[k];
                        string[] invoicesDet = invoicesData.Split('&');
                        string[] fileDet = invoicesDet[1].Split('.');
                        string fileName = fileDet[0].ToString();
                        string fileExtension = fileDet[1].ToString();
                        string filePath = invoicesDet[2].ToString();

                        int result = PC.SaveUploadInvoices(Convert.ToInt64(invoicesDet[0]), fileExtension, fileName, filePath, Convert.ToInt64(Settings.LoginInfo.UserID));
                    }
                }
                Session["UploadedInvoiceDetails"] = null;
                Session["UploadedFiles"] = null;

                lblErrorMessage.Text = "Updated Successfully";
                lblErrorMessage.Visible = true;
            }
            else {
                lblErrorMessage.Text = "Please select atleast one item";
                lblErrorMessage.Visible = true;
            }
            BindData_MakerQueue();
        }
        catch (Exception ex)
        {
            Session["UploadedInvoiceDetails"] = null;
            lblErrorMessage.Text = "Updating Failed ";
            lblErrorMessage.Visible = true;
            BindData_MakerQueue();
        }
    }

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                PaymentCheck PC = new PaymentCheck();
                DataSet statusData = PC.GetStatusDetails("Maker");
                //Find the DropDownList in the Row
                DropDownList ddlStatus = (e.Row.FindControl("ddlStatus") as DropDownList);
                ddlStatus.DataSource = statusData;
                ddlStatus.DataTextField = "Status";
                ddlStatus.DataValueField = "StatusId";
                ddlStatus.DataBind();

                string status = (e.Row.FindControl("lblStatus") as Label).Text;
                for (int i = 0; i < ddlStatus.Items.Count; i++)
                {
                    if (ddlStatus.Items[i].Text == status)
                    {
                        ddlStatus.Items[i].Selected = true;
                    }
                }
            }
            ((BoundField)gvMakerQueue.Columns[12]).ReadOnly = true;
            LinkButton btnUpload = (e.Row.FindControl("btnUploadInvoices") as LinkButton);
            btnUpload.Enabled = false;
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnSearch_OnClick(object sender, EventArgs e)
    {
        try
        {
            PaymentCheck PC = new PaymentCheck();
            DataSet ds = PC.GetRequestQueue("P");
            DataView dv = new DataView();
            dv.Table = ds.Tables[0];

            IFormatProvider dateformat = new System.Globalization.CultureInfo("en-GB");
            DateTime fromdate = Convert.ToDateTime(txtFromDate.Text, dateformat);//.ToString("MM/dd/yyyy");
            DateTime toDate = Convert.ToDateTime(txtToDate.Text, dateformat);//.ToString("MM/dd/yyyy");
            if (txtFromDate.Text.Trim().Length > 0 && txtToDate.Text.Trim().Length > 0)
            {
                dv.RowFilter = "(DocDate>= #" + fromdate + "# AND DocDate<=#" + toDate + "#)";
            }
            if (tbCompanyName.Text.Trim().Length > 0)
            {
                if (dv.RowFilter.Length > 0)
                {
                    dv.RowFilter += "and CompanyName LIKE '" + tbCompanyName.Text + "*'";
                }
                else
                {
                    dv.RowFilter = "CompanyName LIKE '" + tbCompanyName.Text + "*'";
                }
            }
            if (ddlCountryName.SelectedIndex > 0)
            {
                if (dv.RowFilter.Length > 0)
                {
                    dv.RowFilter += "and CountryName Like '" + ddlCountryName.SelectedItem.Value + "*'";
                }
                else
                {
                    dv.RowFilter = "CountryName Like '" + ddlCountryName.SelectedItem.Value + "*'";
                }
            }
            gvMakerQueue.DataSource = dv;
            gvMakerQueue.DataBind();
            hdnMakerRowsCount.Value = dv.Count.ToString();
            if (Convert.ToInt32(dv.Count) > 0)
            {
                btnUpdate.Style.Add("display", "block");
            }
            else
            {
                btnUpdate.Style.Add("display", "none");
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void lbtnBeneficiaryName_OnClick_Maker(object sender, EventArgs e)
    {
        try
        {
            ViewFiles.Controls.Clear();

            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string beneficiaryName = ((LinkButton)sender).Text;
            int rowno = grdrow.DataItemIndex;
            string Id = Convert.ToString((gvMakerQueue.Rows[rowno].Cells[4].FindControl("makerId") as HiddenField).Value);
            hdfChequeId.Value = Id;
            PaymentCheck PC = new PaymentCheck();
            DataTable dt = PC.getUploadFilesbyId(Convert.ToInt64(Id));
            if (dt.Rows.Count > 0 && dt != null)
            {
                DataRow dr = dt.Rows[0];
                hdnRowsCount.Value = Convert.ToString(dt.Rows.Count);
                string mapPath = Utility.ToString(dr["DocPath"]);
                mapPath = mapPath.Substring(0, mapPath.LastIndexOf("\\") + 1); ;
                hdfPreviewPath.Value = mapPath;
                CommonGrid grid = new CommonGrid();

                int i = 0;
                foreach (DataRow datarow in dt.Rows)
                {
                    int index = Convert.ToInt32((datarow["DocPath"].ToString()).IndexOf("Upload"));
                    string imagePath = (datarow["DocPath"].ToString()).Substring(index);


                    HtmlAnchor anchortag = new HtmlAnchor();
                    anchortag.ID = "aViewImage" + i;
                    anchortag.Attributes.Add("onclick", "saveClickDetails(" + datarow["Id"].ToString() + "," + datarow["ChequeId"].ToString() + ")");

                    string hostName = Request["HTTP_HOST"].ToString();
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["RootFolder"]))
                    {
                        anchortag.HRef = "http://" + hostName + "/" + ConfigurationManager.AppSettings["RootFolder"] + "/" + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    }
                    else
                    {
                        anchortag.HRef = "http://" + hostName + "/" + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    }

                    // anchortag.HRef = ConfigurationManager.AppSettings["RootFolder"] + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    anchortag.InnerHtml = datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();

                    if (datarow["DocType"].ToString() == ".pdf")
                    {
                        anchortag.Attributes.Add("class", "pdf-popup");
                    }
                    else
                    {
                        anchortag.Attributes.Add("class", "image-link");
                    }


                    //for having break
                    HtmlGenericControl hgc = new HtmlGenericControl("br");

                    HtmlImage image = new HtmlImage();
                    image.ID = "aViewImage1" + i;
                    image.Src = imagePath + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();


                    //adding the values to the Hidden fields

                    HiddenField hf = new HiddenField();
                    hf.ID = "hdnDocpath_Maker_" + i;
                    hf.Value = datarow["DocPath"].ToString();
                    hf.EnableViewState = true;
                    HiddenField hf1 = new HiddenField();
                    hf1.ID = "hdnDocName_Maker_" + i;
                    hf1.Value = datarow["DocName"].ToString();
                    hf1.EnableViewState = true;
                    HiddenField hf2 = new HiddenField();
                    hf2.ID = "hdnDocType_Maker_" + i;
                    hf2.Value = datarow["DocType"].ToString();
                    hf2.EnableViewState = true;
                    HiddenField hf3 = new HiddenField();
                    hf3.ID = "hdnChequeId_Maker_" + i;
                    hf3.Value = datarow["ChequeId"].ToString();
                    hf3.EnableViewState = true;
                    HiddenField hf4 = new HiddenField();
                    hf4.ID = "hdnDocId_Maker_" + i;
                    hf4.Value = datarow["Id"].ToString();
                    hf4.EnableViewState = true;


                    hdfPath.Value = imagePath;
                    hdfType.Value = "." + datarow["DocType"].ToString();
                    hdfDocName.Value = datarow["DocName"].ToString();
                    hdnBeneficiaryName_Maker.Value = beneficiaryName;
                    ViewFiles.Controls.Add(anchortag);
                    ViewFiles.Controls.Add(hgc);
                    ViewFiles.Controls.Add(hf3);
                    ViewFiles.Controls.Add(hf);
                    ViewFiles.Controls.Add(hf2);
                    ViewFiles.Controls.Add(hf1);
                    ViewFiles.Controls.Add(hf4);

                    i++;
                }
                lnlDownloadAll.Style.Add("display", "block");
                pnlDocuments.Visible = true; 
            }
            else
            {
                //lblErrorMessage.Text = "No Data Available";
                //lblErrorMessage.Visible = true;
                Label lblErrMsg = new Label();
                lblErrMsg.Text = "No Data Available";
                ViewFiles.Controls.Add(lblErrMsg);
                lnlDownloadAll.Style.Add("display", "none");
                pnlDocuments.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    protected void gvMakerQueue_PageIndexing(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvMakerQueue.PageIndex = e.NewPageIndex;
            gvMakerQueue.EditIndex = -1;
            BindData_MakerQueue();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblErrorMessage");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    //protected void btnUploadedInvoices(object sender, EventArgs e)
    //{
    //    GridViewRow gvRow = (GridViewRow)(sender as Control).Parent.Parent;
    //    int index = gvRow.RowIndex;
    //    hdnUploadingRow.Value = Convert.ToString(index);
    //}

    //protected void chkSelectAll_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        GridViewRow gvHdrRow = (GridViewRow)((CheckBox)sender).NamingContainer;
    //        CheckBox chkSelectOne = (CheckBox)gvHdrRow.FindControl("ITchkStatus");

    //    }
    //    catch (Exception ex)
    //    {

    //        throw;
    //    }
    //}

    private void LoadDocuments()
    {
        try
        {
            ViewFiles.Controls.Clear();


            string beneficiaryName = hdnBeneficiaryName_Maker.Value.Replace(" ", "_"); ;
            string Id = hdfChequeId.Value;
            PaymentCheck PC = new PaymentCheck();
            DataTable dt = PC.getUploadFilesbyId(Convert.ToInt64(Id));
            if (dt.Rows.Count > 0 && dt != null)
            {
                DataRow dr = dt.Rows[0];
                hdnRowsCount.Value = Convert.ToString(dt.Rows.Count);
                string mapPath = Utility.ToString(dr["DocPath"]);
                mapPath = mapPath.Substring(0, mapPath.LastIndexOf("\\") + 1); ;
                hdfPreviewPath.Value = mapPath;
                CommonGrid grid = new CommonGrid();

                int i = 0;
                foreach (DataRow datarow in dt.Rows)
                {
                    int index = Convert.ToInt32((datarow["DocPath"].ToString()).IndexOf("Upload"));
                    string imagePath = (datarow["DocPath"].ToString()).Substring(index);


                    HtmlAnchor anchortag = new HtmlAnchor();
                    anchortag.ID = "aViewImage" + i;
                    anchortag.Attributes.Add("onclick", "saveClickDetails(" + datarow["Id"].ToString() + "," + datarow["ChequeId"].ToString() + ")");

                    string hostName = Request["HTTP_HOST"].ToString();
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["RootFolder"]))
                    {
                        anchortag.HRef = "http://" + hostName + "/" + ConfigurationManager.AppSettings["RootFolder"] + "/" + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    }
                    else
                    {
                        anchortag.HRef = "http://" + hostName + "/" + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    }

                    // anchortag.HRef = ConfigurationManager.AppSettings["RootFolder"] + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    anchortag.InnerHtml = datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();

                    if (datarow["DocType"].ToString() == ".pdf")
                    {
                        anchortag.Attributes.Add("class", "pdf-popup");
                    }
                    else
                    {
                        anchortag.Attributes.Add("class", "image-link");
                    }


                    //for having break
                    HtmlGenericControl hgc = new HtmlGenericControl("br");

                    HtmlImage image = new HtmlImage();
                    image.ID = "aViewImage1" + i;
                    image.Src = imagePath + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();


                    //adding the values to the Hidden fields

                    HiddenField hf = new HiddenField();
                    hf.ID = "hdnDocpath_Maker_" + i;
                    hf.Value = datarow["DocPath"].ToString();
                    hf.EnableViewState = true;
                    HiddenField hf1 = new HiddenField();
                    hf1.ID = "hdnDocName_Maker_" + i;
                    hf1.Value = datarow["DocName"].ToString();
                    hf1.EnableViewState = true;
                    HiddenField hf2 = new HiddenField();
                    hf2.ID = "hdnDocType_Maker_" + i;
                    hf2.Value = datarow["DocType"].ToString();
                    hf2.EnableViewState = true;
                    HiddenField hf3 = new HiddenField();
                    hf3.ID = "hdnChequeId_Maker_" + i;
                    hf3.Value = datarow["ChequeId"].ToString();
                    hf3.EnableViewState = true;
                    HiddenField hf4 = new HiddenField();
                    hf4.ID = "hdnDocId_Maker_" + i;
                    hf4.Value = datarow["Id"].ToString();
                    hf4.EnableViewState = true;


                    hdfPath.Value = imagePath;
                    hdfType.Value = "." + datarow["DocType"].ToString();
                    hdfDocName.Value = datarow["DocName"].ToString();
                    hdnBeneficiaryName_Maker.Value = beneficiaryName;
                    ViewFiles.Controls.Add(anchortag);
                    ViewFiles.Controls.Add(hgc);
                    ViewFiles.Controls.Add(hf3);
                    ViewFiles.Controls.Add(hf);
                    ViewFiles.Controls.Add(hf2);
                    ViewFiles.Controls.Add(hf1);
                    ViewFiles.Controls.Add(hf4);

                    i++;
                }
                lnlDownloadAll.Style.Add("display", "block");
                
            }
            else
            {
                //lblErrorMessage.Text = "No Data Available";
                //lblErrorMessage.Visible = true;
                Label lblErrMsg = new Label();
                lblErrMsg.Text = "No Data Available";
                ViewFiles.Controls.Add(lblErrMsg);
                lnlDownloadAll.Style.Add("display", "none");
                
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void lnlDownloadAll_Click_Maker(object sender, EventArgs e)
    {
        try
        {
            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                int rowsCount = Convert.ToInt32(hdnRowsCount.Value);

                PaymentCheck pc = new PaymentCheck();
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST
                // Get the IP
                string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();

                for (int i = 0; i < rowsCount; i++)
                {
                    //string path = null;
                    HiddenField hdfDocPath = (ViewFiles.FindControl("hdnDocpath_Maker_" + i) as HiddenField);
                    HiddenField hdfDocType = (ViewFiles.FindControl("hdnDocType_Maker_" + i) as HiddenField);
                    HiddenField hdfDocName = (ViewFiles.FindControl("hdnDocName_Maker_" + i) as HiddenField);
                    HiddenField hdfDocId = (ViewFiles.FindControl("hdnDocId_Maker_" + i) as HiddenField);
                    string[] docPath = hdfDocPath.Value.Split(',');
                    string path = docPath[0];

                    string docName = hdfDocName.Value.Split(',')[0] + "." + hdfDocType.Value.Split(',')[0];
                    zip.AddFile(path + docName, "SupportDocs");    //renameing File name

                    int result = pc.SaveDocClicks(Convert.ToInt64(hdfChequeId.Value), Convert.ToInt64(hdfDocId.Value), myIP, Convert.ToInt64(Settings.LoginInfo.UserID));
                }
                Response.Clear();
                Response.BufferOutput = false;
                string zipName = String.Format("{0}.zip", hdnBeneficiaryName_Maker.Value.Replace(" ", "_")); //creating zip name
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                zip.Save(Response.OutputStream);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }
        catch (Exception ex)
        {

        }
        finally
        {
            Response.End();
        }
    }



    #endregion

    #region common methods

    [System.Web.Services.WebMethod]
    public static void lnkDocuments_Click(string id, string chequeId)
    {
        try
        {
            string hostName = Dns.GetHostName(); // Retrive the Name of HOST
            // Get the IP
            string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();

            PaymentCheck pc = new PaymentCheck();
            int result = pc.SaveDocClicks(Convert.ToInt64(chequeId), Convert.ToInt64(id), myIP, Convert.ToInt64(Settings.LoginInfo.UserID));

        }
        catch (Exception ex)
        {
            throw;
        }
    }

    #endregion



    #region Checker Queue Part

    private void LoadCheckerDocuments()
    {
        try
        {
            ViewCheckerFiles.Controls.Clear();

            string beneficiaryName = hdnBeneficiaryName.Value.Replace(" ", "_");

            string Id = hdfChequeId.Value;
            PaymentCheck PC = new PaymentCheck();
            DataTable dt = PC.getUploadFilesbyId(Convert.ToInt64(Id));
            if (dt.Rows.Count > 0 && dt != null)
            {
                DataRow dr = dt.Rows[0];
                hdnRowsCount.Value = Convert.ToString(dt.Rows.Count);
                string mapPath = Utility.ToString(dr["DocPath"]);
                mapPath = mapPath.Substring(0, mapPath.LastIndexOf("\\") + 1); ;
                hdfPreviewPath.Value = mapPath;
                CommonGrid grid = new CommonGrid();

                int i = 0;
                foreach (DataRow datarow in dt.Rows)
                {
                    int index = Convert.ToInt32((datarow["DocPath"].ToString()).IndexOf("Upload"));
                    string imagePath = (datarow["DocPath"].ToString()).Substring(index);


                    HtmlAnchor anchortag = new HtmlAnchor();
                    anchortag.ID = "aViewImage2_checker" + i;
                    anchortag.Attributes.Add("onclick", "saveClickDetails(" + datarow["Id"].ToString() + "," + datarow["ChequeId"].ToString() + ")");

                    string hostName = Request["HTTP_HOST"].ToString();
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["RootFolder"]))
                    {
                        anchortag.HRef = "http://" + hostName + "/" + ConfigurationManager.AppSettings["RootFolder"] + "/" + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    }
                    else
                    {
                        anchortag.HRef = "http://" + hostName + "/" + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    }

                    //anchortag.HRef = ConfigurationManager.AppSettings["RootFolder"] + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    anchortag.InnerHtml = datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();

                    if (datarow["DocType"].ToString() == "pdf")
                    {
                        anchortag.Attributes.Add("class", "pdf-popup");
                    }
                    else
                    {
                        anchortag.Attributes.Add("class", "image-link");
                    }
                    anchortag.Target = "_blank";


                    //for having break
                    HtmlGenericControl hgc = new HtmlGenericControl("br");

                    HtmlImage image = new HtmlImage();
                    image.ID = "aViewImage2" + i;
                    image.Src = imagePath + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();


                    //adding the values to the Hidden fields
                    HiddenField hf = new HiddenField();
                    hf.ID = "hdnDocpath" + i;
                    hf.Value = datarow["DocPath"].ToString();
                    hf.EnableViewState = true;
                    HiddenField hf1 = new HiddenField();
                    hf1.ID = "hdnDocName" + i;
                    hf1.Value = datarow["DocName"].ToString();
                    hf1.EnableViewState = true;
                    HiddenField hf2 = new HiddenField();
                    hf2.ID = "hdnDocType" + i;
                    hf2.Value = datarow["DocType"].ToString();
                    hf2.EnableViewState = true;
                    HiddenField hf3 = new HiddenField();
                    hf3.ID = "hdnChequeId" + i;
                    hf3.Value = datarow["ChequeId"].ToString();
                    hf3.EnableViewState = true;
                    HiddenField hf4 = new HiddenField();
                    hf4.ID = "hdnDocId" + i;
                    hf4.Value = datarow["Id"].ToString();
                    hf4.EnableViewState = true;


                    hdfPath.Value = imagePath;
                    hdfType.Value = "." + datarow["DocType"].ToString();
                    hdfDocName.Value = datarow["DocName"].ToString();
                    hdnBeneficiaryName.Value = beneficiaryName;
                    ViewCheckerFiles.Controls.Add(anchortag);
                    ViewCheckerFiles.Controls.Add(hgc);
                    ViewCheckerFiles.Controls.Add(hf3);
                    ViewCheckerFiles.Controls.Add(hf);
                    ViewCheckerFiles.Controls.Add(hf2);
                    ViewCheckerFiles.Controls.Add(hf1);
                    ViewCheckerFiles.Controls.Add(hf4);


                    i++;
                }
                lnlDownloadAll1.Style.Add("display", "block");
               
            }
            else
            {
                //lblErrorMessage1.Text = "No Data Available";
                //lblErrorMessage1.Visible = true;
                Label lblErrMsg = new Label();
                lblErrMsg.Text = "No Data Available";
                ViewCheckerFiles.Controls.Add(lblErrMsg);
                lnlDownloadAll1.Style.Add("display", "none");
                
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void BindData_CheckerQueue()
    {
        try
        {
            PaymentCheck PC = new PaymentCheck();
            DataSet ds = PC.GetRequestQueue("RC");
            DataView dv = new DataView();
            dv.Table = ds.Tables[0];
            IFormatProvider dateformat = new System.Globalization.CultureInfo("en-GB");
            DateTime fromdate = Convert.ToDateTime(txtFromDate1.Text, dateformat);//.ToString("MM/dd/yyyy");
            DateTime toDate = Convert.ToDateTime(txtToDate1.Text, dateformat);//.ToString("MM/dd/yyyy");
            if (txtFromDate1.Text.Trim().Length > 0 && txtToDate1.Text.Trim().Length > 0)
            {
                dv.RowFilter = "(DocDate>= #" + fromdate + "# AND DocDate<=#" + toDate + "#)";
            }
            if (tbCompanyName1.Text.Trim().Length > 0)
            {
                if (dv.RowFilter.Length > 0)
                {
                    dv.RowFilter += "and CompanyName LIKE '" + tbCompanyName1.Text + "*'";
                }
                else
                {
                    dv.RowFilter = "CompanyName LIKE '" + tbCompanyName1.Text + "*'";
                }
            }
            if (ddlCountryName1.SelectedIndex > 0)
            {
                if (dv.RowFilter.Length > 0)
                {
                    dv.RowFilter += "and CountryName Like '" + ddlCountryName1.SelectedItem.Value + "*'";
                }
                else
                {
                    dv.RowFilter = "CountryName Like '" + ddlCountryName1.SelectedItem.Value + "*'";
                }
            }
            gvCheckerQueue.DataSource = dv;
            gvCheckerQueue.DataBind();
            hdnCheckerRowsCount.Value = dv.Count.ToString();
            rowsCount_checker = dv.Count;
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnSearch_Checker_OnClick(object sender, EventArgs e)
    {
        try
        {
            PaymentCheck PC = new PaymentCheck();
            DataSet ds = PC.GetRequestQueue("RC");
            DataView dv = new DataView();
            dv.Table = ds.Tables[0];

            IFormatProvider dateformat = new System.Globalization.CultureInfo("en-GB");
            DateTime fromdate = Convert.ToDateTime(txtFromDate1.Text, dateformat);//.ToString("MM/dd/yyyy");
            DateTime toDate = Convert.ToDateTime(txtToDate1.Text, dateformat);//.ToString("MM/dd/yyyy");
            if (txtFromDate1.Text.Trim().Length > 0 && txtToDate1.Text.Trim().Length > 0)
            {
                dv.RowFilter = "(DocDate>= #" + fromdate + "# AND DocDate<=#" + toDate + "#)";
            }
            if (tbCompanyName1.Text.Trim().Length > 0)
            {
                if (dv.RowFilter.Length > 0)
                {
                    dv.RowFilter += "and CompanyName LIKE '" + tbCompanyName1.Text + "*'";
                }
                else
                {
                    dv.RowFilter = "CompanyName LIKE '" + tbCompanyName1.Text + "*'";
                }
            }
            if (ddlCountryName1.SelectedIndex > 0)
            {
                if (dv.RowFilter.Length > 0)
                {
                    dv.RowFilter += "and CountryName Like '" + ddlCountryName1.SelectedItem.Value + "*'";
                }
                else
                {
                    dv.RowFilter = "CountryName Like '" + ddlCountryName1.SelectedItem.Value + "*'";
                }
            }
            gvCheckerQueue.DataSource = dv;
            gvCheckerQueue.DataBind();
            hdnCheckerRowsCount.Value = dv.Count.ToString();
            if (Convert.ToInt32(dv.Count) > 0)
            {
                btnUpdate_Checker.Style.Add("display", "block");
            }
            else
            {
                btnUpdate_Checker.Style.Add("display", "none");
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    protected void OnRowDataBound_Checker(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                PaymentCheck PC = new PaymentCheck();
                DataSet statusData = PC.GetStatusDetails("Checker");
                //Find the DropDownList in the Row
                DropDownList ddlStatus1 = (e.Row.FindControl("ddlStatus1") as DropDownList);
                ddlStatus1.DataSource = statusData;
                ddlStatus1.DataTextField = "Status";
                ddlStatus1.DataValueField = "StatusId";
                ddlStatus1.DataBind();

                string status = (e.Row.FindControl("lblStatus1") as Label).Text;
                for (int i = 0; i < ddlStatus1.Items.Count; i++)
                {
                    if (ddlStatus1.Items[i].Text == status)
                    {
                        ddlStatus1.Items[i].Selected = true;
                    }
                }

            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnUpdate_OnClick_Checker(object sender, EventArgs e)
    {
        try
        {
            int updatedCount = 0, failureCount = 0;

            string[] idAndStatus = Convert.ToString((hdnIds_CheckerIds1.Value)).Split(',');
            if (idAndStatus.Length > 1)
            {
                PaymentCheck PC = new PaymentCheck();
                for (int i = 0; i < idAndStatus.Length; i++)
                {
                    string[] idStatus = idAndStatus[i].Split('-');
                    if ((idStatus[0] != null && idStatus[0] != ""))
                    {
                        int j = PC.updateMakerQueueStatus(Convert.ToInt32(idStatus[0]), idStatus[1], Convert.ToInt32(Settings.LoginInfo.UserID));
                        if (j > 0)
                        {
                            updatedCount++;
                        }
                        else
                        {
                            failureCount++;
                        }
                    }
                }
                lblErrorMessage1.Text = "Updated Successfully";
                lblErrorMessage1.Visible = true;
            }
            else 
            {
                lblErrorMessage1.Text = "Please select atleast one item";
                lblErrorMessage1.Visible = true;
            }
            BindData_CheckerQueue();
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    protected void lbtnBeneficiaryName_OnClick(object sender, EventArgs e)
    {
        try
        {
            ViewCheckerFiles.Controls.Clear();
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            string beneficiaryName = ((LinkButton)sender).Text;
            int rowno = grdrow.RowIndex;
            string docDate = grdrow.Cells[2].Text;
            DateTime dateTime = Convert.ToDateTime(docDate);
            string dtime = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd").Replace("-", "");
            hdnBeneficiaryName.Value = ((LinkButton)sender).Text + "_" + dtime;
            string Id = Convert.ToString((gvCheckerQueue.Rows[rowno].Cells[6].FindControl("checkerId1") as HiddenField).Value);
            hdfChequeId.Value = Id;
            PaymentCheck PC = new PaymentCheck();
            DataTable dt = PC.getUploadFilesbyId(Convert.ToInt64(Id));
            if (dt.Rows.Count > 0 && dt != null)
            {
                DataRow dr = dt.Rows[0];
                hdnRowsCount.Value = Convert.ToString(dt.Rows.Count);
                string mapPath = Utility.ToString(dr["DocPath"]);
                mapPath = mapPath.Substring(0, mapPath.LastIndexOf("\\") + 1); ;
                hdfPreviewPath.Value = mapPath;
                CommonGrid grid = new CommonGrid();

                int i = 0;
                foreach (DataRow datarow in dt.Rows)
                {
                    int index = Convert.ToInt32((datarow["DocPath"].ToString()).IndexOf("Upload"));
                    string imagePath = (datarow["DocPath"].ToString()).Substring(index);


                    HtmlAnchor anchortag = new HtmlAnchor();
                    anchortag.ID = "aViewImage2_checker" + i;
                    anchortag.Attributes.Add("onclick", "saveClickDetails(" + datarow["Id"].ToString() + "," + datarow["ChequeId"].ToString() + ")");

                    string hostName = Request["HTTP_HOST"].ToString();
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["RootFolder"]))
                    {
                        anchortag.HRef = "http://" + hostName + "/" + ConfigurationManager.AppSettings["RootFolder"] + "/" + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    }
                    else
                    {
                        anchortag.HRef = "http://" + hostName + "/" + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    }

                    //anchortag.HRef = ConfigurationManager.AppSettings["RootFolder"] + imagePath.Replace(@"\", "/") + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();
                    anchortag.InnerHtml = datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();

                    if (datarow["DocType"].ToString() == "pdf")
                    {
                        anchortag.Attributes.Add("class", "pdf-popup");
                    }
                    else
                    {
                        anchortag.Attributes.Add("class", "image-link");
                    }
                    anchortag.Target = "_blank";


                    //for having break
                    HtmlGenericControl hgc = new HtmlGenericControl("br");

                    HtmlImage image = new HtmlImage();
                    image.ID = "aViewImage2" + i;
                    image.Src = imagePath + datarow["DocName"].ToString() + "." + datarow["DocType"].ToString();


                    //adding the values to the Hidden fields
                    HiddenField hf = new HiddenField();
                    hf.ID = "hdnDocpath" + i;
                    hf.Value = datarow["DocPath"].ToString();
                    hf.EnableViewState = true;
                    HiddenField hf1 = new HiddenField();
                    hf1.ID = "hdnDocName" + i;
                    hf1.Value = datarow["DocName"].ToString();
                    hf1.EnableViewState = true;
                    HiddenField hf2 = new HiddenField();
                    hf2.ID = "hdnDocType" + i;
                    hf2.Value = datarow["DocType"].ToString();
                    hf2.EnableViewState = true;
                    HiddenField hf3 = new HiddenField();
                    hf3.ID = "hdnChequeId" + i;
                    hf3.Value = datarow["ChequeId"].ToString();
                    hf3.EnableViewState = true;
                    HiddenField hf4 = new HiddenField();
                    hf4.ID = "hdnDocId" + i;
                    hf4.Value = datarow["Id"].ToString();
                    hf4.EnableViewState = true;


                    hdfPath.Value = imagePath;
                    hdfType.Value = "." + datarow["DocType"].ToString();
                    hdfDocName.Value = datarow["DocName"].ToString();
                    hdnBeneficiaryName.Value = beneficiaryName;
                    ViewCheckerFiles.Controls.Add(anchortag);
                    ViewCheckerFiles.Controls.Add(hgc);
                    ViewCheckerFiles.Controls.Add(hf3);
                    ViewCheckerFiles.Controls.Add(hf);
                    ViewCheckerFiles.Controls.Add(hf2);
                    ViewCheckerFiles.Controls.Add(hf1);
                    ViewCheckerFiles.Controls.Add(hf4);


                    i++;
                }
                lnlDownloadAll1.Style.Add("display", "block");
                pnlDocuments1.Visible = true;
            }
            else
            {
                //lblErrorMessage1.Text = "No Data Available";ctl00_cphTransaction_lblErrorMessage1
                //lblErrorMessage1.Visible = true;
                Label lblErrMsg = new Label();
                lblErrMsg.Text = "No Data Available";
                ViewCheckerFiles.Controls.Add(lblErrMsg);
                lnlDownloadAll1.Style.Add("display", "none");
                pnlDocuments1.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    protected void lnlDownloadAll_Click(object sender, EventArgs e)
    {
        try
        {

            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                int rowsCount = Convert.ToInt32(hdnRowsCount.Value);

                PaymentCheck pc = new PaymentCheck();
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST
                // Get the IP
                string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();

                for (int i = 0; i < rowsCount; i++)
                {
                    //string path = null;
                    HiddenField hdfDocPath = (ViewCheckerFiles.FindControl("hdnDocpath" + i) as HiddenField);
                    HiddenField hdfDocType = (ViewCheckerFiles.FindControl("hdnDocType" + i) as HiddenField);
                    HiddenField hdfDocName = (ViewCheckerFiles.FindControl("hdnDocName" + i) as HiddenField);
                    HiddenField hdfDocId = (ViewCheckerFiles.FindControl("hdnDocId" + i) as HiddenField);
                    //string[] docPath=hdfDocPath.Value.Split(new Char[','],StringSplitOptions.RemoveEmptyEntries);
                    string[] docPath = hdfDocPath.Value.Split(',');
                    string path = docPath[0];

                    //string docName = hdfDocName.Value + "." + hdfDocType.Value;
                    string docName = hdfDocName.Value.Split(',')[0] + "." + hdfDocType.Value.Split(',')[0];
                    zip.AddFile(path + docName);//.FileName = docName + "" + hdfDocType.Value; //renameing File name
                    //zip.AddFile(path + docName);//.FileName = docName + "" + hdfDocType.Value; //renameing File name
                    int result = pc.SaveDocClicks(Convert.ToInt64(hdfChequeId.Value), Convert.ToInt64(hdfDocId.Value), myIP, Convert.ToInt64(Settings.LoginInfo.UserID));
                }
                Response.Clear();
                Response.BufferOutput = false;
                string zipName = String.Format("{0}.zip", hdnBeneficiaryName.Value.Replace(" ", "_")); //creating zip name
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                zip.Save(Response.OutputStream);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {

        }
        finally
        {
            Response.End();
        }
    }

    protected void gvCheckerQueue_PageIndexing(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvCheckerQueue.PageIndex = e.NewPageIndex;
            gvCheckerQueue.EditIndex = -1;
            BindData_CheckerQueue();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblErrorMessage1");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    #endregion




}
