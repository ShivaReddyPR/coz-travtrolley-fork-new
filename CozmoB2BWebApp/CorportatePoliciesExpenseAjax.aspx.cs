﻿using System;
using System.Data;
using CT.Corporate;
using System.IO;

public partial class CorportatePoliciesExpenseAjax : System.Web.UI.Page
{
    protected int ctrlId;
    protected string countryCode;
    protected string cityCode;
    protected string expenseType;
    protected string currency;
    protected string amount;
    protected string dailyActuals;
    protected string includeTravelDays;
    protected string rowInfo;
    protected string type;
    protected string approverName;
    protected string hierarchyOrder;


    //For expense reports tab
    protected string reportType;
    protected string reportDate;
    protected string reportEmpName;
    protected string reportCountry;
    protected string reportCity;
    protected string reportCostCenter;
    protected string reportExpType;
    protected string reportRefCode;
    protected string reportDesc;
    protected string reportCurrency;
    protected string reportAmount;
    protected string reportComment;
    protected string reportRowInfo;


    protected void Page_Load(object sender, EventArgs e)
    {
        //For ExpenseReports html
        if (Request["requestSource"] != null && Request["requestSource"] == "getExpenseReportsHtml")
        {
            if (Request["id"] != null)
            {
                this.ctrlId = Convert.ToInt32(Request["id"]);
                this.reportType = Convert.ToString(Request["rptexpenseType"]);
                this.reportDate = Convert.ToString(Request["rptDate"]);
                this.reportEmpName = Convert.ToString(Request["rptEmpName"]);
                this.reportCountry = Convert.ToString(Request["rptCountrySelectedText"]);
                this.reportCity = Convert.ToString(Request["rptCitySelectedText"]);
                this.reportCostCenter = Convert.ToString(Request["rptCostCenterSelectedText"]);
                this.reportExpType = Convert.ToString(Request["rptExpTypeSelectedText"]);
                this.reportRefCode = Convert.ToString(Request["rptRefCode"]);
                this.reportDesc = Convert.ToString(Request["rptDesc"]);
                this.reportCurrency = Convert.ToString(Request["rptCurrencySelectedText"]);
                this.reportAmount = Convert.ToString(Request["rptAmount"]);
                this.reportComment = Convert.ToString(Request["rptComment"]);
                this.reportRowInfo = Convert.ToString(Request["rowInfo"]);
            }
        }

        //The below request is to append all the expenses and allowances
        else if (Request["requestSource"] != null && Request["requestSource"] == "getExpenseAllowanceHtml")
        {
            if (Request["id"] != null)
            {
                this.ctrlId = Convert.ToInt32(Request["id"]);
                this.countryCode = Convert.ToString(Request["countryCode"]);
                this.cityCode = Convert.ToString(Request["cityCode"]);
                this.expenseType = Convert.ToString(Request["expenseType"]);
                this.currency = Convert.ToString(Request["currency"]);
                this.amount = Convert.ToString(Request["amount"]);
                this.dailyActuals = Convert.ToString(Request["dailyActuals"]);
                this.includeTravelDays = Convert.ToString(Request["includeTravelDays"]);
                this.rowInfo = Convert.ToString(Request["rowInfo"]);
                this.type = Convert.ToString(Request["type"]);

            }
        }

        else if (Request["requestSource"] != null && Request["requestSource"] == "getApproversHtml")
        {
            if (Request["id"] != null)
            {
                this.ctrlId = Convert.ToInt32(Request["id"]);
                this.approverName = Convert.ToString(Request["approverName"]);
                this.hierarchyOrder = Convert.ToString(Request["hierarchyOrder"]);
                this.type = Convert.ToString(Request["type"]);
                this.rowInfo = Convert.ToString(Request["rowInfo"]);
            }
        }
        else if (Request["requestSource"] != null && Request["requestSource"] == "getExpenseSummaryHtml")
        {
            string responseHtml = "";
            try
            {
                if (Request["bookingRecords"] != null)
                {
                    string[] bookingRecords = Convert.ToString(Request["bookingRecords"]).Split(',');
                    if (bookingRecords.Length > 0)
                    {

                        foreach (string record in bookingRecords)
                        {

                            responseHtml += "<tr>";

                            //Date of booking
                            responseHtml += "<td>";
                            responseHtml += record.Split('|')[0].Split(' ')[0];
                            responseHtml += "</td>";

                            //tripId
                            responseHtml += "<td>";
                            responseHtml += record.Split('|')[1];

                            responseHtml += "</td>";

                            //Employee ID
                            responseHtml += "<td>";
                            responseHtml += record.Split('|')[7];
                            responseHtml += "</td>";

                            //Employee Name.
                            responseHtml += "<td>";
                            responseHtml += record.Split('|')[8];
                            responseHtml += "</td>";

                            //Travel Date.
                            responseHtml += "<td>";
                            responseHtml += record.Split('|')[2].Split(' ')[0];
                            responseHtml += "</td>";

                            //Airline
                            responseHtml += "<td>";
                            responseHtml += record.Split('|')[5];
                            responseHtml += "</td>";

                            //Ticket No.
                            responseHtml += "<td>";
                            responseHtml += record.Split('|')[6];
                            responseHtml += "</td>";

                            //Route (sector details)
                            responseHtml += "<td>";
                            responseHtml += record.Split('|')[3] + "-" + record.Split('|')[4];
                            responseHtml += "</td>";

                            //Total Fare
                            responseHtml += "<td>";
                            responseHtml += record.Split('|')[9];
                            responseHtml += "</td>";

                            //Hyperlink
                            responseHtml += "<td>";
                            responseHtml += "<a href='CorporateCreateExpenseReport.aspx?type=T&tripId=" + record.Split('|')[1] + "&en=" + record.Split('|')[8] + "&cc=" + record.Split('|')[10] + "&refid=" + record.Split('|')[11] + "&depDate=" + record.Split('|')[12] + "&arrDate=" + record.Split('|')[13] + "'>" +
                            "<span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span></a>";
                            responseHtml += "</td>";
                            responseHtml += "</tr>";



                        }
                    }
                }
                Response.Write(responseHtml);
            }
            catch (Exception ex)
            {
                responseHtml = ex.Message;
            }
        }

        else if (Request["requestSource"] != null && Request["requestSource"] == "getExpenseSummaryBookings")
        {
            string response = "";
            try
            {
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                DateTime fromDate;
                DateTime toDate;
                if (Request["fromDate"] != null)
                {
                    fromDate = Convert.ToDateTime(Request["fromDate"].Replace('-', '/'), dateFormat);
                }
                else
                {
                    // fromDate = DateTime.Now.AddMonths(-1);
                    fromDate = DateTime.Now;
                }
                if (Request["toDate"] != null)
                {
                    toDate = Convert.ToDateTime(Request["toDate"].Replace('-', '/'), dateFormat);
                }
                else
                {
                    toDate = DateTime.Now;
                }
                DataTable dtBookings = CorporateProfileExpenseDetails.GetExpenseBookings(Convert.ToInt32(Request["profileid"]), fromDate, toDate);


                if (dtBookings != null && dtBookings.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtBookings.Rows)
                    {
                        if (response.Length > 0)
                        {
                            response += "," + Convert.ToString(dr["DateofBooking"]) + "|" + Convert.ToString(dr["TripID"]) + "|" + Convert.ToString(dr["TravelDate"]) + "|" + Convert.ToString(dr["Origin"]) + "|" + Convert.ToString(dr["Destination"]) + "|" + Convert.ToString(dr["Airline"]) + "|" + Convert.ToString(dr["TicketNo"]) + "|" + Convert.ToString(dr["EmployeeId"]) + "|" + Convert.ToString(dr["EmployeeName"]) + "|" + Convert.ToString(dr["TotalFare"]) + "|" + Convert.ToInt32(dr["CostCentre"]) + "|" + Convert.ToString(dr["FlightId"]) + "|" + Convert.ToString(dr["DEPDATE"]) + "|" + Convert.ToString(dr["ARRIVALDATE"]);
                        }
                        else
                        {
                            response = Convert.ToString(dr["DateofBooking"]) + "|" + Convert.ToString(dr["TripID"]) + "|" + Convert.ToString(dr["TravelDate"]) + "|" + Convert.ToString(dr["Origin"]) + "|" + Convert.ToString(dr["Destination"]) + "|" + Convert.ToString(dr["Airline"]) + "|" + Convert.ToString(dr["TicketNo"]) + "|" + Convert.ToString(dr["EmployeeId"]) + "|" + Convert.ToString(dr["EmployeeName"]) + "|" + Convert.ToString(dr["TotalFare"]) + "|" + Convert.ToInt32(dr["CostCentre"]) + "|" + Convert.ToString(dr["FlightId"]) + "|" + Convert.ToString(dr["DEPDATE"]) + "|" + Convert.ToString(dr["ARRIVALDATE"]);
                        }

                    }
                }
                Response.Write(response);
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }

        }

        else if (Request["requestSource"] != null && Request["requestSource"] == "getAmountForExpType")
        {
            int response;
            try
            {
                response = CorporateProfileExpenseDetails.GetAmountForExpType(Convert.ToInt32(Request["ui"]), Request["type"], Request["cc"], Request["ci"], Request["et"]);
                Response.Write(response);
            }
            catch
            {
                //response = ex.Message;
            }

        }

        else if (Request["requestSource"] != null && Request["requestSource"] == "getTravelDaysInclusion")
        {
            bool response;
            try
            {
                response = CorporateProfileExpenseDetails.GetTravelDaysInclusion(Convert.ToInt32(Request["ui"]), Request["type"], Request["cc"], Request["ci"], Request["et"]);
                Response.Write(response);
            }
            catch (Exception ex)
            {
                //response = ex.Message;
            }

        }

        else if (Request["requestSource"] != null && Request["requestSource"] == "getExpenseDocDetails")
        {
            string response = "";
            try
            {
                int expDetailId = Convert.ToInt32(Request["ExpDetailId"]);
                DataTable dtDocDetails = CorporateProfileExpenseDetails.GetExpenseDocDetails(expDetailId);
                if (dtDocDetails != null && dtDocDetails.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtDocDetails.Rows)
                    {



                        //response += "<td>";
                        //response += dr["ExpDetailId"];
                        //response += "</td>";

                        //response += "<td>";
                        //response += dr["FileType"];
                        //response += "</td>";

                        //response += "<td>";
                        //response += dr["FilePath"];
                        //response += "</td>";
                        string docName = Path.GetFileName(Convert.ToString(dr["FilePath"]));

                        response += "<tr>";
                        response += "<td>";
                        response += docName;
                        response += "</td>";

                        response += "<td>";
                        response += "<a href='#' onclick=\"javascript:Download('" + (Convert.ToString(dr["FilePath"])).Replace("\\", "//") + "','" + docName + "')\">Download</a>";
                        response += "</td>";
                        response += "</tr>";
                    }

                }

            }
            catch
            {

                response = "Error";
            }
            Response.Write(response);
        }

        else if (Request["requestSource"] != null && Request["requestSource"] == "getCitiesList")
        {
            string response = "";
            try
            {
                DataTable dtCities = CorporateProfileExpenseDetails.GetPolicyExpenseCityList((Request["Country"]));
                if (dtCities != null && dtCities.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtCities.Rows)
                    {
                        if (response.Length > 0)
                        {
                            response += "," + dr["cityName"] + "|" + dr["CityCode"];
                        }
                        else
                        {
                            response = Request["id"] + "#" + dr["cityName"] + "|" + dr["CityCode"];
                        }
                    }
                }
                Response.Write(response);
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }

        }

        //This will return all the document details against the profile id from the table Corp_Profile_Documents  
        else if (Request["requestSource"] != null && Request["requestSource"] == "getProfileDocDetails")
        {
            string response = "";
            try
            {
                int profileId = Convert.ToInt32(Request["ProfileId"]);
                DataTable dtDocDetails = CorporateProfile.GetProfileDocDetails(profileId);
                if (dtDocDetails != null && dtDocDetails.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtDocDetails.Rows)
                    {
                        string docName = Convert.ToString(dr["DocFileName"]);
                        response += "<tr>";
                        response += "<td>";
                        response += docName;
                        response += "</td>";

                        response += "<td>";
                        response += "<a href='#' onclick=\"javascript:Download('" + (Convert.ToString(dr["DocFilePath"]) + @"\" + Convert.ToString(dr["DocFileName"])).Replace("\\", "//") + "','" + docName + "')\">Download</a>";
                        response += "</td>";
                        response += "</tr>";
                    }

                }

            }
            catch 
            {
                response = "Error";
            }
            Response.Write(response);
        }









    }
}
