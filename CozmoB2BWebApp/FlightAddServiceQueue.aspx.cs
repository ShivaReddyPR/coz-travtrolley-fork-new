﻿using System;
using System.Data;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.Core;
using CT.BookingEngine;
using CT.Configuration;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.MetaSearchEngine;

public partial class FlightAddServiceQueueGUI : CT.Core.ParentPage// System.Web.UI.Page
{
    protected FlightItinerary[] flightItinerary = new FlightItinerary[0];
    protected BookingDetail[] bookingDetail = new BookingDetail[0];
    protected AgentMaster agency;
    protected int recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["AgentBookingQueueRecordsPerPage"]);
    protected int queueCount = 0;
    protected int noOfPages;
    protected int flag = 0;
    protected int pageNo;
    public string message = string.Empty;
    protected string Hold = string.Empty;
    protected string show = string.Empty;
    protected string pageType = string.Empty;
    protected string paxpnrFilter = string.Empty;
    protected string paxFilter = string.Empty;
    protected string pnrFilter = string.Empty;
    protected string ticketFilter = string.Empty;
    protected string vouhcerFilter = string.Empty;
    protected DateTime startDate, endDate;
    protected int locationId;
    protected bool isDomestic;
    protected string whereString = string.Empty;
    protected decimal isAgentCapable;
    protected string agencyLive = string.Empty;
    protected string agencyOnline = string.Empty;
    protected List<int> bookingModes = new List<int>();
    protected int agentFilter = 0;
    CT.Core.Queue[] relevantQueues = new CT.Core.Queue[0];


    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        hdfParam.Value = "1";
        Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");

        Page.Title = "Flight Add Service Queue";
        AuthorizationCheck();


        if (!IsPostBack)
        {
            Settings.LoginInfo.IsOnBehalfOfAgent = false;
            hdfParam.Value = "0";
            if (Request["FromDate"] != null)
            {
                txtFromDate.Text = Request["FromDate"];
            }
            else
            {
                txtFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            }

            txtToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            isAgentCapable = Settings.LoginInfo.AgentBalance;
            InitializeControls();
            if (Request["message"] != null)
            {
                message = Request["message"];
            }

            //Disable the booking status dropdown 
            //In the Queue Load Only the items with "Ticketed" Status
            ddlBookingStatus.Enabled = false;
            ddlBookingStatus.SelectedValue = "5"; //BookingStatus.Ticketed = 5
            LoadQueue();
        }

        if (PageNoString.Value != null)
        {
            pageNo = Convert.ToInt32(PageNoString.Value);
        }
        else
        {
            pageNo = 1;
        }
    }

    private void InitializeControls()
    {
        try
        {
            Array Statuses = Enum.GetValues(typeof(BookingStatus));
            foreach (BookingStatus status in Statuses)
            {
                ListItem item = new ListItem(Enum.GetName(typeof(BookingStatus), status), ((int)status).ToString());
                if ((int)status == 1 || (int)status == 2 || (int)status == 4 || (int)status == 5 || (int)status == 6 || (int)status == 6 || (int)status == 7 || (int)status == 12 || (int)status == 15 || (int)status == 16 || (int)status == 17 || (int)status == 18 || (int)status == 19)
                {
                    ddlBookingStatus.Items.Add(item);
                }
            }
            BindAgent();
            if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
            {
                ddlLocation.Enabled = true;
            }
            else
            {
                ddlLocation.Enabled = false;
            }
            ddlLocation.DataSource = LocationMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
            ddlLocation.DataTextField = "location_name";
            ddlLocation.DataValueField = "location_id";
            ddlLocation.DataBind();
            ddlLocation.SelectedIndex = -1;
            ddlLocation.SelectedValue = Convert.ToString(Settings.LoginInfo.LocationID);

            int b2bAgentId;
            int b2b2bAgentId;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgency.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgency.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgency.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }
            BindB2BAgent(Convert.ToInt32(ddlAgency.SelectedItem.Value));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);// AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            ddlAgency.DataSource = dtAgents;
            ddlAgency.DataTextField = "Agent_Name";
            ddlAgency.DataValueField = "agent_id";
            ddlAgency.DataBind();
            ddlAgency.Items.Insert(0, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string type = string.Empty;
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            if (agentId == 0)
            {
                if (Convert.ToInt32(ddlAgency.SelectedItem.Value) > 1)
                {
                    type = "AGENT";// AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                }
                else
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                }
            }
            else
            {
                if (agentId == -1)
                {
                    agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                    if (agentId == 0)
                    {
                        type = "BASE";// BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations
                    }
                }
            }
            if (Convert.ToInt32(ddlAgency.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    type = "BASEB2B";// BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations
                }
            }
            BindLocation(agentId, type);
        }
        catch (Exception ex)
        { }
    }

    protected void ddlB2B2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "B2B2B";// B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                }
            }
            else if (agentId == -1)
            {
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                }
                //if (agentId == -1)
                //{
                //    type = "BASE";
                //}
            }
            if (Convert.ToInt32(ddlAgency.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = string.Empty;
                    }
                }
            }
            if (Convert.ToInt32(ddlAgency.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                    }
                }
            }
            BindLocation(agentId, type);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void LoadQueue()
    {
        ArrayList listOfItemId = new ArrayList();
        if (txtPNR.Text.Trim().Length > 0)
        {
            pnrFilter = txtPNR.Text.Trim();
        }
        //if (txtTicketNumber.Text.Trim().Length > 0)
        //{
        //    ticketFilter = txtTicketNumber.Text.Trim();
        //}
        if (txtPaxName.Text.Trim().Length > 0)
        {
            paxFilter = txtPaxName.Text.Trim();
        }

        if (ddlLocation.SelectedIndex > 0)
        {
            locationId = Convert.ToInt32(ddlLocation.SelectedValue);
        }

        if (ddlBookingStatus.SelectedIndex > 0)
        {
            bookingModes.Add(Convert.ToInt32(ddlBookingStatus.SelectedValue));
        }
        string agentType = string.Empty;
        //if (ddlAgency.SelectedIndex > 0)
        //{
        agentFilter = Convert.ToInt32(ddlAgency.SelectedItem.Value);
        if (agentFilter == 0)
        {
            agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
            {
                agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
            }
            if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
            {
                agentType = string.Empty; // null Means binding in list all BOOKINGS
            }
        }
        if (agentFilter > 0 && ddlB2BAgent.SelectedIndex > 0)
        {
            if (Convert.ToInt32(ddlAgency.SelectedItem.Value) > 1)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                }
                else
                {
                    agentFilter = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }
            }
            else
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                }
                agentFilter = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            }
        }
        if (agentFilter > 0 && ddlB2B2BAgent.SelectedIndex > 0)
        {
            if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
            {
                agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
            }
            else
            {
                agentFilter = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
            }
        }
        if (Convert.ToInt32(ddlAgency.SelectedItem.Value) != 0)
        {
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                    agentFilter = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                }
            }
        }
        //}

        DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
        IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
        if (txtFromDate.Text != string.Empty)
        {
            try
            {
                startDate = Convert.ToDateTime(txtFromDate.Text, provider);
            }
            catch { }
        }
        else
        {
            startDate = DateTime.Now;
        }

        if (txtToDate.Text != string.Empty)
        {
            try
            {
                endDate = Convert.ToDateTime(Convert.ToDateTime(txtToDate.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
            }
            catch { }
        }
        else
        {
            endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
        }
        #region B2C purpose


        string transType = string.Empty;
        if (Settings.LoginInfo.TransType == "B2B")
        {
            ddlTransType.Visible = false;
            lblTransType.Visible = false;
            transType = "B2B";
        }
        else if (Settings.LoginInfo.TransType == "B2C")
        {
            ddlTransType.Visible = false;
            lblTransType.Visible = false;
            transType = "B2C";
        }
        else
        {
            ddlTransType.Visible = true;
            lblTransType.Visible = true;
            if (ddlTransType.SelectedItem.Value == "-1")
            {
                transType = null;
            }
            else
            {
                transType = ddlTransType.SelectedItem.Value;
            }
        }
        #endregion

        //IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
        //if (txtFromDate.Text.Trim().Length > 0 && txtToDate.Text.Trim().Length > 0)
        //{
        //    startDate = Convert.ToDateTime(txtFromDate.Text.Trim(), dateFormat);
        //    endDate = Convert.ToDateTime(txtToDate.Text, dateFormat);
        //}
        //else
        //{
        //    startDate = Convert.ToDateTime("01/01/1900");
        //    endDate = Convert.ToDateTime(DateTime.Now);
        //}

        //if (ddlTourType.SelectedIndex == 1)
        //{
        //    isDomestic = true;
        //}
        //else
        //{
        //    isDomestic = false;
        //}

        whereString = CT.Core.Queue.GetWhereStringForAgentBookingQueue(agentFilter, paxFilter, pnrFilter, bookingModes, isDomestic, ticketFilter, vouhcerFilter, locationId, startDate, endDate, agentType, transType,string.Empty,string.Empty);
        //ViewState["whereString"] = whereString;
        queueCount = CT.Core.Queue.GetCountForAgentBookingQueue(whereString);


        if ((queueCount % recordsPerPage) > 0)
        {
            noOfPages = (queueCount / recordsPerPage) + 1;
        }
        else
        {
            noOfPages = (queueCount / recordsPerPage);
        }
        if (PageNoString.Value != null)
        {
            pageNo = Convert.ToInt16(PageNoString.Value);
        }
        else
        {
            pageNo = 1;
        }

        string url = "AgentBookingQueue.aspx";
        pageType = "bookingdate";
        if (queueCount > 0)
        {
            show = MetaSearchEngine.PagingJavascript(noOfPages, url, pageNo);
        }
        if (queueCount != 0)
        {
            relevantQueues = TicketQueue.GetRelevantQueuesForAgentBookingQueues(pageNo, recordsPerPage, queueCount, whereString, ref flightItinerary, ref bookingDetail);

            if (relevantQueues.Length == 0)
            {
                relevantQueues = TicketQueue.GetRelevantQueuesForAgentBookingQueues(1, recordsPerPage, queueCount, whereString, ref flightItinerary, ref bookingDetail);
                PageNoString.Value = "1";
            }
        }
        else
        {
            flightItinerary = new FlightItinerary[0];
        }
        dlBookingQueue.DataSource = flightItinerary;
        dlBookingQueue.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LoadQueue();
    }

    // Access perm
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }

    }



    protected void dlBookingQueue_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                FlightItinerary itinerary = e.Item.DataItem as FlightItinerary;

                if (itinerary != null)
                {
                    Label lblAgencyName = e.Item.FindControl("lblAgencyName") as Label;
                    Label lblAgencyBalance = e.Item.FindControl("lblAgencyBalance") as Label;
                    Label lblAgencyPhone1 = e.Item.FindControl("lblAgencyPhone1") as Label;
                    Label lblAgencyPhone2 = e.Item.FindControl("lblAgencyPhone2") as Label;
                    Label lblAgencyEmail = e.Item.FindControl("lblAgencyEmail") as Label;
                    Label lblAgentName = e.Item.FindControl("lblAgentName") as Label;
                    Label lblAdultCount = e.Item.FindControl("lblAdultCount") as Label;

                    Label lblBookingSource = e.Item.FindControl("lblBookingSource") as Label;
                    Label lblPNR = e.Item.FindControl("lblPNR") as Label;
                    Label lblBookingId = e.Item.FindControl("lblBookingId") as Label;
                    Label lblDayString = e.Item.FindControl("lblDayString") as Label;
                    Label lblTimeString = e.Item.FindControl("lblTimeString") as Label;
                    Label lblLastTicketingDate = e.Item.FindControl("lblLastTicketingDate") as Label;
                    Label lblBookingStatus = e.Item.FindControl("lblBookingStatus") as Label;
                    Label lblSent = e.Item.FindControl("lblSent") as Label;
                    Label lblAirline = e.Item.FindControl("lblAirline") as Label;
                    Label lblFlightNumber = e.Item.FindControl("lblFlightNumber") as Label;
                    Label lblDepartureTime = e.Item.FindControl("lblDepartureTime") as Label;
                    Label lblItineraryString = e.Item.FindControl("lblItineraryString") as Label;
                    Label lblInstantPurchase = e.Item.FindControl("lblInstantPurchase") as Label;
                    Label lblLeadPaxName = e.Item.FindControl("lblLeadPaxName") as Label;
                    Label lblOtherPaxName = e.Item.FindControl("lblOtherPaxName") as Label;

                    HyperLink lblReleaseSeat = e.Item.FindControl("lblReleaseSeat") as HyperLink;
                    Label lblViewBooking = e.Item.FindControl("lblViewBooking") as Label;
                    Label lblBookingDate = e.Item.FindControl("lblBookingDate") as Label;
                    Label lblTrip = e.Item.FindControl("lblTrip") as Label;
                    Label lblTotal = e.Item.FindControl("lblTotal") as Label;
                    Label lblTicketNo = e.Item.FindControl("lblTicketNo") as Label;
                    HiddenField hdnIndex = e.Item.FindControl("hdnIndex") as HiddenField;
                    Label lblViewInvoice = e.Item.FindControl("lblViewInvoice") as Label;
                    Label lblLocation = e.Item.FindControl("lblLocation") as Label;
                    Label lblBookedBy = e.Item.FindControl("lblBookedBy") as Label;
                    Label lblSource = e.Item.FindControl("lblSource") as Label;
                    DropDownList ddlChangeRequestType = e.Item.FindControl("ddlChangeRequestType") as DropDownList;

                    Label lastTicketingDate = e.Item.FindControl("lastTicketingDate") as Label;
                    Label lastTicketingDateValue = e.Item.FindControl("lastTicketingDateValue") as Label;
                    LinkButton lnkPayment = e.Item.FindControl("lnkPayment") as LinkButton;
                    TextBox txtRemarks = e.Item.FindControl("txtRemarks") as TextBox;
                    Label lblRequestChange = e.Item.FindControl("lblRequestChange") as Label;
                    Label lblCorpBookingCode = e.Item.FindControl("lblCorpBookingCode") as Label;
                    //Label lblAcctStatus = e.Item.FindControl("lblAcctStatus") as Label;
                    //Label lblAcctStatusVal = e.Item.FindControl("lblAcctStatusVal") as Label;


                    Label lblServiceDetails = e.Item.FindControl("lblServiceDetails") as Label;
                    //if (bookingDetail[e.Item.ItemIndex].Status == BookingStatus.Ticketed || bookingDetail[e.Item.ItemIndex].Status == BookingStatus.Modified || bookingDetail[e.Item.ItemIndex].Status == BookingStatus.ModificationInProgress || bookingDetail[e.Item.ItemIndex].Status == BookingStatus.RefundInProgress || bookingDetail[e.Item.ItemIndex].Status == BookingStatus.Refunded || bookingDetail[e.Item.ItemIndex].Status == BookingStatus.VoidInProgress || bookingDetail[e.Item.ItemIndex].Status == BookingStatus.Void)
                    //{
                    //    lblViewInvoice.Text = "<input id='ViewInvoice-" + e.Item.ItemIndex + "' type='button' class='button' value='View Invoice' onclick=\"ViewInvoice(" + itinerary.FlightId + "," + Settings.LoginInfo.AgentId + ")\"  />";
                    //}
                    if (itinerary.FlightBookingSource == BookingSource.AirArabia)
                    {
                        ddlChangeRequestType.Items.RemoveAt(1);
                    }
                    else
                    {
                        if (itinerary.CreatedOn.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy"))
                        {
                            ddlChangeRequestType.Items.RemoveAt(3);
                        }
                        else
                        {
                            ddlChangeRequestType.Items.RemoveAt(1);
                        }
                    }




                    AgentMaster agent = new AgentMaster(itinerary.AgencyId);

                    lblAgencyBalance.Text = agent.CurrentBalance.ToString("N" + agent.DecimalValue);
                    lblAgencyEmail.Text = agent.Email1;
                    lblAgencyName.Text = agent.Name;
                    lblAgencyPhone1.Text = agent.Phone1;
                    lblAgencyPhone2.Text = agent.Phone2;
                    lblAgentName.Text = agent.Name;

                    hdnIndex.Value = e.Item.ItemIndex.ToString();

                    lblPNR.Text = itinerary.PNR;
                    lblPNR.Font.Bold = true;

                    if (!string.IsNullOrEmpty(itinerary.TripId))
                    {
                        lblCorpBookingCode.Text = "<strong>Corporate Booking Code : </strong>" + itinerary.TripId;
                    }

                    lblLocation.Text = itinerary.LocationName;
                    try
                    {
                        UserMaster bookedBy = new UserMaster(Convert.ToInt64(itinerary.CreatedBy));
                        lblBookedBy.Text = bookedBy.FirstName + " " + bookedBy.LastName;
                        //Show Supplier Source for Base Agent
                        if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
                        {
                            lblSource.Text = "Source : " + itinerary.FlightBookingSource.ToString();
                        }
                    }
                    catch { }

                    string daystring = Util.GetDayString(bookingDetail[e.Item.ItemIndex].CreatedOn);
                    string timestring = "";
                    if (daystring == "Today")
                    {
                        TimeSpan tspan = DateTime.UtcNow - bookingDetail[e.Item.ItemIndex].CreatedOn;
                        string time = tspan.Hours.ToString("00") + ":" + tspan.Minutes.ToString("00");
                        timestring = time + " hours Ago " + Util.GetTimeString(Util.UTCToIST(bookingDetail[e.Item.ItemIndex].CreatedOn));
                    }
                    else
                    {
                        timestring = Util.GetTimeString(Util.UTCToIST(bookingDetail[e.Item.ItemIndex].CreatedOn));
                    }

                    if (daystring == "Today" || daystring == "Yesterday")
                    {
                        lblDayString.Text = daystring + " " + Util.GetOnlyDayMon(Util.UTCToIST(bookingDetail[e.Item.ItemIndex].CreatedOn));
                        lblTimeString.Text = timestring;
                    }
                    else
                    {
                        lblDayString.Text = daystring;
                        lblTimeString.Text = timestring;
                    }
                    lblBookingDate.Text = bookingDetail[e.Item.ItemIndex].CreatedOn.ToString("dd MMM yyyy");

                    //string lastTicketingDate = string.Empty;
                    //if (itinerary.LastTicketDate != null && itinerary.LastTicketDate != DateTime.MinValue)
                    //{
                    //    lastTicketingDate = itinerary.LastTicketDate.ToString("dd MMM yy");
                    //}                    

                    //if (bookingDetail != null && bookingDetail[e.Item.ItemIndex].Status != BookingStatus.Ready)
                    //{
                    lblBookingStatus.Text = bookingDetail[e.Item.ItemIndex].Status.ToString();
                    lblBookingStatus.Font.Bold = true;
                    //}
                    //else
                    //{
                    //    lblBookingStatus.Visible = false;                    
                    //}

                    lblAirline.Text = (itinerary.Segments.Length > 0 ? itinerary.Segments[0].Airline : itinerary.AirlineCode);
                    lblFlightNumber.Text = (itinerary.Segments.Length > 0 ? itinerary.Segments[0].FlightNumber : "");
                    lblDepartureTime.Text = Util.GetDateString((itinerary.Segments.Length > 0 ? itinerary.Segments[0].DepartureTime : Convert.ToDateTime("01/01/1900")));

                    if (bookingDetail[e.Item.ItemIndex].Status != BookingStatus.Ticketed && Util.IsToday(bookingDetail[e.Item.ItemIndex].CreatedOn))
                    {
                        //lblInstantPurchase.Text = "<div class='padding-3 width-160 yellow-new'>Instant purchase required.</div>";
                    }
                    lblLeadPaxName.Text = QueueMethods.GetLeadPax(itinerary);
                    int adultCount = 0;
                    decimal total = 0;
                    if (itinerary.Passenger.Length > 0)
                    {
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {

                            if (pax.Type == PassengerType.Adult)
                            {
                                adultCount++;
                            }
                            if (pax.Price != null)
                            {
                                if (itinerary.FlightBookingSource != BookingSource.TBOAir)
                                {
                                    if (pax.Price.NetFare == 0)
                                    {
                                        total += (pax.Price.PublishedFare + pax.Price.Tax + pax.Price.Markup + pax.Price.B2CMarkup) - pax.Price.Discount; //Added only B2C markup by chandan on 13062016
                                    }
                                    else
                                    {
                                        total += (pax.Price.NetFare + pax.Price.Tax + pax.Price.Markup + pax.Price.B2CMarkup) - pax.Price.Discount;//Added only B2C markup by chandan on 13062016
                                    }
                                }
                                else
                                {
                                    total += (pax.Price.PublishedFare + pax.Price.Tax + pax.Price.Markup + pax.Price.B2CMarkup + pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee) - pax.Price.Discount; //Added only B2C markup by chandan on 13062016
                                }
                                if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.TBOAir || itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || itinerary.FlightBookingSource == BookingSource.Indigo)
                                {
                                    total += pax.Price.BaggageCharge;
                                }
                                total += pax.Price.AsvAmount;// To show total including page level markup
                            }
                        }

                        lblAdultCount.Text = adultCount.ToString();
                        if (itinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                        {
                            lblTotal.Text = agent.AgentCurrency + " " + Math.Ceiling(total).ToString("N" + agent.DecimalValue);
                        }
                        else
                        {
                            lblTotal.Text = agent.AgentCurrency + " " + total.ToString("N" + agent.DecimalValue);
                        }


                        List<string> trips = new List<string>();
                        foreach (FlightInfo segment in itinerary.Segments)
                        {
                            if (!trips.Contains(segment.Origin.CityCode))
                            {
                                trips.Add(segment.Origin.CityCode);
                            }
                            if (!trips.Contains(segment.Destination.CityCode))
                            {
                                trips.Add(segment.Destination.CityCode);
                            }
                        }

                        bool isReturn = false;

                        if (itinerary.Origin == itinerary.Segments[itinerary.Segments.Length - 1].Destination.CityCode)
                        {
                            isReturn = true;
                        }

                        foreach (string city in trips)
                        {
                            if (lblTrip.Text.Length > 0)
                            {
                                lblTrip.Text += "-" + city;
                            }
                            else
                            {
                                lblTrip.Text = city;
                            }
                        }
                        if (isReturn)
                        {
                            lblTrip.Text += "-" + trips[0];
                        }
                    }
                    if (itinerary.TransactionType == "B2C" || itinerary.PaymentMode == ModeOfPayment.CreditCard)
                    {
                        lnkPayment.Visible = true;
                        lnkPayment.OnClientClick = "return ViewPaymentInfo('" + e.Item.ItemIndex + "','" + itinerary.BookingId + "');";
                    }

                    lblServiceDetails.Text = string.Empty;
                    if (itinerary.Passenger.Length > 0)
                    {
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                           

                            string routing = lblTrip.Text;
                            string travelDate = Util.GetDateStringNum((itinerary.Segments.Length > 0 ? itinerary.Segments[0].DepartureTime : Convert.ToDateTime("01/01/1900")));
                            lblServiceDetails.Text += "<div class='col-md-4'>";
                            lblServiceDetails.Text += "<strong>Passenger Name:</strong>" + pax.FullName;
                            lblServiceDetails.Text += "<br/>";
                            lblServiceDetails.Text += "</div>";



                            lblServiceDetails.Text += "<div class='col-md-4'>";
                            lblServiceDetails.Text += "<input id='Open-" + pax.PaxId + "' type='button' class='but_b' value='Add Service' onclick=\"AddServiceDetails(" + pax.PaxId + "," + itinerary.AgencyId + ")\"  />";
                            lblServiceDetails.Text += "<br/>";
                            lblServiceDetails.Text += "</div>";

                            lblServiceDetails.Text += "<div class='col-md-4'>";
                            lblServiceDetails.Text += "<input id='View-" + pax.PaxId + "' type='button' class='but_b' value='View Service' onclick=\"ViewServiceDetails(" + pax.PaxId + ")\"  />";
                            lblServiceDetails.Text += "<br/>";
                            lblServiceDetails.Text += "</div>";



                        }
                    }


                }

            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "");
        }
    }


    protected void ddlAgency_SelectedIndexChanged(object sender, EventArgs e)
    {
        int agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
        if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
        BindB2BAgent(agentId);
        BindB2B2BAgent(agentId);
        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
        {
            ddlB2B2BAgent.Enabled = false;
        }
        string type = string.Empty;
        if (agentId == 0)
        {
            type = "BASE";
        }
        BindLocation(Utility.ToInteger(ddlAgency.SelectedItem.Value), type);
    }

    void BindLocation(int agentId, string type)
    {
        DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);

        ddlLocation.Items.Clear();
        ddlLocation.DataSource = dtLocations;
        ddlLocation.DataTextField = "location_name";
        ddlLocation.DataValueField = "location_id";
        ddlLocation.DataBind();

        ListItem item = new ListItem("All", "-1");
        ddlLocation.Items.Insert(0, item);
        hdfParam.Value = "0";
    }

   


}

