﻿using System;
using System.Data;
using System.Configuration;
using System.Web.UI;
using CT.BookingEngine;
using CT.Core;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;

public partial class CreateSightseeingInvoiceGUI : CT.Core.ParentPage
{
    protected Invoice invoice = new Invoice();
    protected AgentMaster agency;
    protected string agencyAddress;
   // protected RegCity agencyCity = new RegCity();
    protected int cityId;
    protected UserMaster loggedMember = new UserMaster();
    protected int sightseeingId;
    protected int agencyId;
    protected string remarks = string.Empty;
    protected string confNo;
    protected string routing;
    protected SightseeingItinerary itinerary = new SightseeingItinerary();
    protected string errorMessage = string.Empty;
    protected LocationMaster location;
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Sightseeing Invoice Details";
        AuthorizationCheck();
        loggedMember = new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));

        # region Sightseeing Invoice

        try
        {

            if (Request["sightseeingId"] != null && Request["agencyId"] != null && Request["confNo"] != null)
            {
                sightseeingId = Convert.ToInt32(Request["sightseeingId"]);
                agencyId = Convert.ToInt32(Request["agencyId"]);

                confNo = Request["confNo"];
            }
            else
            {
                sightseeingId = Convert.ToInt32(Request.QueryString["sightseeingId"]);
                agencyId = Convert.ToInt32(Request.QueryString["agencyId"]);

                confNo = Request.QueryString["confNo"];
            }

            //agency = new AgentMaster(Settings.LoginInfo.AgentId);
            agency = new AgentMaster(agencyId);


            string logoPath = "";
            if (agency.ID > 1)
            {
                logoPath = ConfigurationManager.AppSettings["AgentImage"] + agency.ImgFileName;
                imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + logoPath;
            }
            else
            {
                imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + "images/logo.jpg";
            }


            itinerary.Load(sightseeingId);
           // HotelPassenger hPax = new HotelPassenger();
            //hPax.Load(sightseeingId);
            List<string> paxDetails = new List<string>();
            foreach (string paxName in itinerary.PaxNames)
            {

                paxDetails.Add(paxName);
            }
            itinerary.PaxNames = paxDetails;

            if (Request["city"] != null)
            {
                routing = Request["city"];
            }

            if (Request["remarks"] != null)
            {
                remarks = Request["remarks"];
            }

            // Reading agency information.
            DataTable dtCities = CityMaster.GetList(agency.Country, ListStatus.Short, RecordStatus.Activated);
            DataRow[] cities = dtCities.Select("City_Name like '%" + agency.City + "%'");

            if (cities != null && cities.Length > 0)
            {
                cityId = Convert.ToInt32(cities[0]["city_id"]);
            }
            //agency = new AgentMaster(agencyId);
            //if (agency.City.Length > 0)
            //{
            //    agencyCity.CityName = agency.City;
            //}
            //else
            //{
            //    agencyCity = RegCity.GetCity(cityId);
            //}
            // Formatting agency address for display.
            agencyAddress = agency.Address;
            agencyAddress.Trim();
            if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
            {
                agencyAddress += ",";
            }
            if (agency.Address != null && agency.Address.Length > 0)
            {
                agencyAddress += agency.Address;
            }
            agencyAddress.Trim();
            if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
            {
                agencyAddress += ",";
            }

            location = new LocationMaster(itinerary.LocationId);

            int invoiceNumber = 0;
            try
            {
                // Generating invoice.

                if (itinerary.SightseeingId > 0)
                {
                    invoiceNumber = Invoice.isInvoiceGenerated(sightseeingId, ProductType.SightSeeing);
                }
                if (invoiceNumber > 0)
                {
                    invoice = new Invoice();
                    invoice.Load(invoiceNumber);
                }
                else
                {
                    invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(sightseeingId, string.Empty, (int)loggedMember.ID, ProductType.SightSeeing, 1);
                    invoice.Load(invoiceNumber);
                }


            }
            catch (Exception exp)
            {
                errorMessage = "Invalid Invoice Number";
                Audit.Add(EventType.Exception, Severity.High, 1, "Exception while generating Invoice.. Message:" + exp.Message, "");
                return;
            }
        }
        catch (Exception exp)
        {
            errorMessage = "Your Session is Expired!";
            Audit.Add(EventType.Exception, Severity.High, 1, "Exception while generating Invoice.. Message:" + exp.Message, "");
        }
        #endregion
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
