﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using CT.Core;
using CT.BookingEngine;
using CT.Configuration;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.MetaSearchEngine;
using CT.Corporate;
using CT.AccountingEngine;
using System.Net;
using System.Web.Script.Serialization;

public partial class HotelBookingQueue : CT.Core.ParentPage// System.Web.UI.Page
{
    protected HotelItinerary[] hotelItinerary = new HotelItinerary[0];
    protected BookingDetail[] bookingDetail = new BookingDetail[0];
    protected ServiceRequest[] serviceRequest = new ServiceRequest[0];
    private CT.Core.Queue[] relevantQueues;
    protected UserMaster loggedMember;
    protected string message = string.Empty;
    //protected int recordsPerPage = 5; // By Default it is 5
    protected int recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["BookingQueueRecordsPerPage"]);
    protected int noOfPages;
    protected int pageNo;
    protected int flag = 0;
    protected string show = string.Empty;
    protected int selfAgencyId = 1;
    protected string pageType = string.Empty; // for sorting with paging
    protected string Cancelled = string.Empty; // For filtering with paging 
    protected string Confirmed = string.Empty; // For filtering with paging 
    protected string lastCanDate = string.Empty;
    protected int agentFilter;// For restricted filtering
    protected string HotelFilter = string.Empty; // For restricted filtering
    protected string paxFilter = string.Empty;// For restricted filtering
    protected string pnrFilter = string.Empty;// For restricted filtering
    protected string reqType = "both"; // For filtering with paging
    protected List<int> bookingModes = new List<int>();
    private List<int> shownStatuses = new List<int>();
    protected List<int> sourcesList = new List<int>();
    protected List<int> selectedSources = new List<int>();
    protected string agencyOnline = string.Empty;
    protected decimal rateofExchange = 1;
    //protected string voucherStatus = "Both";
    protected bool filtered = false;
    protected string changeVisible = "visible";
    protected int locationId;
    protected Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
    protected HotelSource sourceInfo = new HotelSource();
    protected AgentMaster agent = null;
    private int loggedMemberId;

    public HotelBookingQueue()
    {
        //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
        //System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-GB");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //this.Master.PageRole= true;
           hdfParam.Value = "1";
          Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");           
            if (Settings.LoginInfo != null)
            {
                //int recordsCount = CT.Core.Queue.GetHotelSerivceRequestCount(GetWhereString(), GetOrderbyString());

                // CT.Core.Queue.GetSerivceRequests(
                Page.Title = "Hotel Booking Queue";
                //AuthorizationCheck();

                if (!IsPostBack)
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                    hdfParam.Value = "0";
                    if (Request["FromDate"] != null)
                    {
                        CheckIn.Text = Request["FromDate"];
                    }
                    else
                    {
                        CheckIn.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    }

                    CheckOut.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    sourcesList = sourceInfo.Load();
                    selectedSources = sourcesList;
                    InitializeControls();
                    Array Sources = Enum.GetValues(typeof(HotelBookingSource));
                    foreach (HotelBookingSource source in Sources)
                    {
                        if (source == HotelBookingSource.UAH || source == HotelBookingSource.DOTW || source == HotelBookingSource.RezLive || source == HotelBookingSource.LOH || source == HotelBookingSource.HotelBeds || source == HotelBookingSource.GTA || source == HotelBookingSource.Miki || source == HotelBookingSource.HotelConnect || source == HotelBookingSource.TBOHotel || source == HotelBookingSource.WST || source == HotelBookingSource.JAC || source == HotelBookingSource.EET || source == HotelBookingSource.Agoda || source == HotelBookingSource.Yatra || source == HotelBookingSource.GRN || source == HotelBookingSource.OYO || source == HotelBookingSource.GIMMONIX|| source == HotelBookingSource.HotelExtranet)    //Modified by brahmam 26.09.2014
                        {
                            ListItem item = new ListItem(Enum.GetName(typeof(HotelBookingSource), source), ((int)source).ToString());
                            ddlSource.Items.Add(item);
                        }
                    }
                    if (Settings.LoginInfo.AgentType == AgentType.B2B || Settings.LoginInfo.AgentType == AgentType.B2B2B)
                    {
                        ddlSource.Visible = false;
                        lblSource.Visible = false;
                    }

                    Array Statuses = Enum.GetValues(typeof(HotelBookingStatus));
                    foreach (HotelBookingStatus status in Statuses)
                    {
                        ListItem item = new ListItem(Enum.GetName(typeof(HotelBookingStatus), status), ((int)status).ToString());
                        if (item.Text == "Confirmed")
                        {
                            ddlBookingStatus.Items.Insert(2, new ListItem("Vouchered", "1"));
                        }
                        else
                        {
                            ddlBookingStatus.Items.Add(item);
                        }
                    }
                    //agentFilter = Utility.ToInteger(Settings.LoginInfo.AgentId);
                    //ddlAgents.SelectedIndex = -1;
                    //ddlAgents.SelectedValue = Utility.ToString(agentFilter);
                    //ddlAgents.Items.FindByText(agentFilter).Selected = true;
                    ddlLocations.SelectedIndex = -1;
                    ddlLocations.Items.FindByText(Settings.LoginInfo.LocationName).Selected = true;
                    pageNo = 1;
                    LoadBookings();
                }

                // records per page from configuration
                //StaticData staticInfo = new StaticData();
                //rateOfExList = staticInfo.CurrencyROE;


                //int agencyId = 1;

                loggedMember = new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));
                if (Request["PageNoString"] != null)
                {
                    pageNo = Convert.ToInt16(Request["PageNoString"]);
                }
                else
                {
                    pageNo = 1;
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.HotelBook, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.Message, "0");
        }
    }

    private void InitializeControls()
    {
        BindAgent();
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
        {
            ddlLocations.Enabled = true;
        }
        else
        {
            ddlSource.Visible = false;
            lblSource.Visible = false;
        }
        ddlLocations.DataSource = LocationMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
        ddlLocations.DataTextField = "location_name";
        ddlLocations.DataValueField = "location_id";
        ddlLocations.DataBind();
        int b2bAgentId;
        int b2b2bAgentId;
        if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
        {
            ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
        }
        else if (Settings.LoginInfo.AgentType == AgentType.Agent)
        {
            ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            ddlAgents.Enabled = false;
        }
        else if (Settings.LoginInfo.AgentType == AgentType.B2B)
        {
            ddlAgents.Enabled = false;
            b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
            ddlAgents.SelectedValue = Convert.ToString(b2bAgentId);
            ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            ddlB2BAgent.Enabled = false;
        }
        else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
        {
            ddlAgents.Enabled = false;
            ddlB2BAgent.Enabled = false;
            b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
            b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
            ddlAgents.SelectedValue = Convert.ToString(b2bAgentId);
            ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
            ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            ddlB2B2BAgent.Enabled = false;
        }
        BindB2BAgent(Convert.ToInt32(ddlAgents.SelectedItem.Value));
        BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
        {
            ddlB2B2BAgent.Enabled = false;
        }
    }


    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            ddlAgents.DataSource = dtAgents;
            ddlAgents.DataTextField = "Agent_Name";
            ddlAgents.DataValueField = "agent_id";
            ddlAgents.DataBind();
            ddlAgents.Items.Insert(0, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string type = string.Empty;
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            if (agentId == 0)
            {
                if (Convert.ToInt32(ddlAgents.SelectedItem.Value) > 1)
                {
                    type = "AGENT";// AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                }
                else
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                }
            }
            else
            {
                if (agentId == -1)
                {
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                    if (agentId == 0)
                    {
                        type = "BASE";// BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations
                    }
                }
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    type = "BASEB2B";// BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations
                }
            }

            BindLocation(agentId, type);
        }
        catch (Exception ex)
        { }
    }

    protected void ddlB2B2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "B2B2B";// B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                }
            }
            else if (agentId == -1)
            {
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                }
                
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = string.Empty;
                    }
                }
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                    }
                }
            }
            BindLocation(agentId, type);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Loads all the bookings
    /// </summary>
    private void LoadBookings()
    {
        try
        {
            HotelItinerary[] tempItinerary = new HotelItinerary[0];
            string orderByString = string.Empty;
            string whereString = string.Empty;
            whereString = GetWhereString();
            orderByString = GetOrderbyString();
            int numBooking = 0;
            int queueCount = 0;
            ArrayList listOfItemId = new ArrayList();
            queueCount = CT.Core.Queue.GetTotalFilteredRecordsCount(whereString, orderByString, "Hotel");
            string url = "HotelBookingQueue.aspx";
            if ((queueCount % recordsPerPage) > 0)
            {
                noOfPages = (queueCount / recordsPerPage) + 1;
            }
            else
            {
                noOfPages = (queueCount / recordsPerPage);
            }

            //Added by brahmam 
            if (noOfPages > 1)
            {
                if (PageNoString.Value != null)
                {
                    pageNo = Convert.ToInt16(PageNoString.Value);
                }
                else
                {
                    pageNo = 1;
                }
            }
            else
            {
                pageNo = 1;
            }
            if (queueCount > 0)
            {
                show = MetaSearchEngine.PagingJavascript(noOfPages, url, pageNo);
            }
            //DataTable dtQueue = new DataTable();
            DataTable dtQueue = CT.Core.Queue.GetTotalFilteredRecords(pageNo, recordsPerPage, queueCount, whereString, orderByString, "Hotel");

            //CT.Core.Audit.Add(EventType.Exception, Severity.Normal, 0, whereString, "");
            //CT.Core.Audit.Add(EventType.Exception, Severity.Normal, 0, dtQueue.Rows.Count.ToString(), "");
            //foreach (CT.Core.Queue queue in relevantQueues)
            //{
            //    BookingDetail bd = new BookingDetail(queue.ItemId);
            //    numBooking++;
            //    listOfItemId.Add(queue.ItemId);
            //}
            //bookingDetail = new BookingDetail[numBooking];
            //tempItinerary = new HotelItinerary[numBooking];
            //for (int count = 0; count < listOfItemId.Count; count++)
            //{
            //    bookingDetail[count] = new BookingDetail(Convert.ToInt32(listOfItemId[count]));
            //    Product[] products = BookingDetail.GetProductsLine(Convert.ToInt32(listOfItemId[count]));
            //    for (int i = 0; i < products.Length; i++)
            //    {
            //        string productType = Enum.Parse(typeof(ProductType), products[i].ProductTypeId.ToString()).ToString();
            //        switch (productType)
            //        {                        
            //            case "Hotel":
            //                tempItinerary[count] = new HotelItinerary();
            //                tempItinerary[count].Load(products[i].ProductId);
            //                tempItinerary[count].ProductId = products[i].ProductId;
            //                tempItinerary[count].ProductType = products[i].ProductType;
            //                tempItinerary[count].ProductTypeId = products[i].ProductTypeId;
            //                tempItinerary[count].BookingMode = products[i].BookingMode;

            //                HotelPassenger hPax = new HotelPassenger();
            //                HotelRoom hRoom = new HotelRoom();
            //                PriceAccounts priceInfo = new PriceAccounts();

            //                // Getting Passenger details
            //                hPax.Load(products[i].ProductId);
            //                tempItinerary[count].HotelPassenger = hPax;

            //                //Getting HotelRoom Details  
            //                tempItinerary[count].Roomtype = hRoom.Load(products[i].ProductId);
            //                hRoom = tempItinerary[count].Roomtype[0];
            //                priceInfo.Load(hRoom.PriceId);
            //                hRoom.Price = priceInfo;                           
            //                break;
            //            default:
            //                break;
            //        }
            //    }
            //}
            dlBookings.DataSource = dtQueue;
            dlBookings.DataBind();
            dlBookings.Style.Add("display","block");
            lblRefStatus.Style.Add("display", "none");
        }
        catch (Exception exp)
        {
            CT.Core.Audit.Add(EventType.Exception, Severity.Normal, 0, "Exception from Booking Queue:" + exp.ToString(), "");
        }
    }

    /// <summary>
    /// Method Gets Where String from the filters(Html Controls)
    /// </summary>
    /// <returns></returns>
    private string GetWhereString()
    {
        bool isOrderByLCD = false;
        bool ischeckby = false;
        string source = string.Empty;
        string voucherd = string.Empty;
        shownStatuses.Clear();
        if (Request["filter"] != null && Request["filter"] == "true")
        {
            filtered = true;
        }
        if (filtered == false)
        {
            //voucherStatus = "Both";
            voucherd = "";
            source = "";
            reqType = "both";
            Cancelled = "True";
            Confirmed = "True";
            shownStatuses.Clear();
            shownStatuses.Add((int)HotelBookingStatus.Cancelled);
            shownStatuses.Add((int)HotelBookingStatus.Confirmed);
            shownStatuses.Add((int)HotelBookingStatus.Pending);

        }
        if (ddlBookingStatus.SelectedValue == "-1")
        {
            shownStatuses.Add((int)HotelBookingStatus.Cancelled);
            shownStatuses.Add((int)HotelBookingStatus.Confirmed);
            shownStatuses.Add((int)HotelBookingStatus.Error);
            shownStatuses.Add((int)HotelBookingStatus.Failed);
            shownStatuses.Add((int)HotelBookingStatus.Pending);
        }
        else
        {
            shownStatuses.Add(Convert.ToInt32(ddlBookingStatus.SelectedValue));
        }
        if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
        {
            if (ddlSource.SelectedItem.Text == "All")
            {
                foreach (ListItem item in ddlSource.Items)
                {
                    if (item.Value != "-1")
                    {
                        selectedSources.Add(Convert.ToInt32(item.Value));
                    }
                }
            }
            else
            {
                selectedSources.Add(Convert.ToInt32(ddlSource.SelectedValue));
            }
        }

        //if (ddlVoucherStatus.SelectedItem.Text == "All")
        //{
        //    voucherStatus = "Both";
        //    voucherd = "";
        //    filtered = true;
        //}
        //else if (ddlVoucherStatus.SelectedItem.Text == "NotVouchered")
        //{
        //    voucherStatus = "True";
        //    voucherd = "True";
        //    filtered = true;
        //}
        //else if (ddlVoucherStatus.SelectedItem.Text == "Vouchered")
        //{
        //    voucherStatus = "False";
        //    voucherd = "False";
        //    filtered = true;
        //}
        //else if (ddlVoucherStatus.SelectedItem.Text == "All" && Request["filter"] == "true")
        //{
        //    selectedSources.Add(Convert.ToInt32(ddlSource.SelectedValue));
        //}

        isOrderByLCD = true;
        if (txtHotelName.Text != string.Empty)
        {
            HotelFilter = txtHotelName.Text.Trim();
        }
        if (txtPaxName.Text != string.Empty)
        {
            paxFilter = txtPaxName.Text.Trim();
        }
        if (txtConfirmNo.Text != string.Empty)
        {
            pnrFilter = txtConfirmNo.Text;
        }
        string agentType = string.Empty;
        if (!IsPostBack)
        {
            agentFilter = Utility.ToInteger(Settings.LoginInfo.AgentId);
        }
        if (Checkdates.Checked == true)
        {
            ischeckby = true;
        }
        else
        {
            agentFilter = Utility.ToInteger(ddlAgents.SelectedItem.Value);
            if (agentFilter == 0)
            {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = string.Empty;// null Means binding in list all BOOKINGS
                }
            }
            //if (ddlAgents.SelectedIndex > 0)
            //{


            if (agentFilter > 0 && ddlB2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlAgents.SelectedItem.Value) > 1)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else
                    {
                        agentFilter = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    agentFilter = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }

            }
            if (agentFilter > 0 && ddlB2B2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else
                {
                    agentFilter = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                        agentFilter = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                    }
                }
            }
            //}
        }

        DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
        IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
        if (CheckIn.Text != string.Empty)
        {
            try
            {
                startDate = Convert.ToDateTime(CheckIn.Text, provider);
            }
            catch { }
        }
        else
        {
            startDate = DateTime.Now;
        }

        if (CheckOut.Text != string.Empty)
        {
            try
            {
                endDate = Convert.ToDateTime(Convert.ToDateTime(CheckOut.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
            }
            catch { }
        }
        else
        {
            endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
        }

        if (!IsPostBack)
        {
            startDate = Convert.ToDateTime(CheckIn.Text, provider);
            //endDate = Convert.ToDateTime(DateTime.Now.AddDays(1), provider);
            //CheckOut.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
        }
        if (ddlLocations.SelectedIndex > 0)
        {
            locationId = Convert.ToInt32(ddlLocations.SelectedValue);
        }
        #region B2C purpose
        string transType = string.Empty;
        if (Settings.LoginInfo.TransType == "B2B")
        {
            ddlTransType.Visible = false;
            lblTransType.Visible = false;
            transType = "B2B";
        }
        else if (Settings.LoginInfo.TransType == "B2C")
        {
            ddlTransType.Visible = false;
            lblTransType.Visible = false;
            transType = "B2C";
        }
        else
        {
            ddlTransType.Visible = true;
            lblTransType.Visible = true;
            if (ddlTransType.SelectedItem.Value == "-1")
            {
                transType = null;
            }
            else
            {
                transType = ddlTransType.SelectedItem.Value;
            }
        }
        #endregion

        //string whereString = string.Empty;
        string whereString = CT.Core.Queue.GetWhereStringForHotelBookingQueue(shownStatuses, HotelFilter, paxFilter, pnrFilter, agentFilter, bookingModes, source, string.Empty, isOrderByLCD, selectedSources, locationId, startDate, endDate, agentType, transType,Settings.LoginInfo.UserID,Settings.LoginInfo.MemberType.ToString(), ischeckby);

        //Audit.Add(EventType.HotelBook, Severity.Normal, 1, whereString, "0");
        return whereString;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private string GetOrderbyString()
    {
        string orderString = string.Empty;
        if (Request["lcd"] != null && Request["lcd"] != string.Empty)
        {
            lastCanDate = Request["lcd"];
            orderString = " order by lastCancellationDate asc ";
        }
        return orderString;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        txtRefNum.Text = "";
        LoadBookings();
    }

    protected void btnRefSubmit_Click(object sender, EventArgs e)
    {
        LoadBookingsForRefNum();
    }

    private void LoadBookingsForRefNum()
    {
        try{           
                string whereString = "WHERE H.systemreference = '" + txtRefNum.Text + "'";
                int queueCount = CT.Core.Queue.GetTotalFilteredRecordsCount(whereString, "", "Hotel");
                DataTable dtQueue = CT.Core.Queue.GetTotalFilteredRecords(pageNo, recordsPerPage, queueCount, whereString, "", "Hotel");
                dlBookings.DataSource = dtQueue;
            if (dtQueue.Rows.Count > 0)
            {
                dlBookings.DataBind();
                dlBookings.Style.Add("display", "block");
                lblRefStatus.Style.Add("display", "none");
            }
            else {
                lblRefStatus.Text = "No Data Found";
                dlBookings.Style.Add("display", "none");
                lblRefStatus.Style.Add("display", "block");
            }
        }       
         catch (Exception exp)
        {
            CT.Core.Audit.Add(EventType.Exception, Severity.Normal, 0, "Exception from Booking Queue:" + exp.ToString(), "");
        }
    }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dlBookings_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            //Check if the bound item is DataItem only
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Get the bound item
                //CT.BookingEngine.HotelItinerary hotelItinerary = e.Item.DataItem as CT.BookingEngine.HotelItinerary;
                DataRowView hotelItinerary = e.Item.DataItem as DataRowView;
                Label lblPrice = e.Item.FindControl("lblPrice") as Label;
                Label lblBookingPrice = e.Item.FindControl("lblBookingPrice") as Label;
                Label lblGuest = e.Item.FindControl("lblGuests") as Label;
                Label lblPaxName = e.Item.FindControl("lblPaxName") as Label;
                Label lblSupplier = e.Item.FindControl("lblSupplier") as Label;
                //Label lblRequestStatus = e.Item.FindControl("lblRequestStatus") as Label;
                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                Label lblBooking = e.Item.FindControl("lblBooking") as Label;
                Button btnBooking = e.Item.FindControl("btnBooking") as Button;
                HtmlTable tblBooking = e.Item.FindControl("tblBooking") as HtmlTable;
                Label lblAgentName = e.Item.FindControl("lblAgentName") as Label;
                Label lblBookedBy = e.Item.FindControl("lblBookedBy") as Label;
                Label lblLocation = e.Item.FindControl("lblLocation") as Label;
                TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
                txtAdminFee.Attributes.Add("onblur", "Validate(" + e.Item.ItemIndex + ")");
                //Added by brahmam 
                Label lblAgencyName = e.Item.FindControl("lblAgencyName") as Label;
                Label lblAgencyBalance = e.Item.FindControl("lblAgencyBalance") as Label;
                Label lblAgencyPhone1 = e.Item.FindControl("lblAgencyPhone1") as Label;
                Label lblAgencyPhone2 = e.Item.FindControl("lblAgencyPhone2") as Label;
                Label lblAgencyEmail = e.Item.FindControl("lblAgencyEmail") as Label;
                LinkButton lnkPayment=e.Item.FindControl("lnkPayment") as LinkButton;

                Button btnInvoice = e.Item.FindControl("btnInvoice") as Button;
                Button btnOpen = e.Item.FindControl("btnOpen") as Button;
                
                //Added by somasekhar 
                Label lblCorpLabel = e.Item.FindControl("lblCorpLabel") as Label;
                
                Button btnUpdateStatus = e.Item.FindControl("btnUpdateStatus") as Button;
                DropDownList ddlApprovalStatus = e.Item.FindControl("ddlApprovalStatus") as DropDownList;
                Label lblApprovedBy = e.Item.FindControl("lblApprovedBy") as Label;
                Label lblApprovalStatus = e.Item.FindControl("lblApprovalStatus") as Label;
                LinkButton lnkPendingRemarks = e.Item.FindControl("lnkPendingRemarks") as LinkButton;
                LinkButton LnkCancelPolicy = e.Item.FindControl("LnkCancelPolicy") as LinkButton;
                Label lblViewBooking = e.Item.FindControl("lblViewBooking") as Label;
                LinkButton LnkOpen = e.Item.FindControl("LnkOpen") as LinkButton;
                Label lblAgentRef = e.Item.FindControl("lblAgentRef") as Label;

                //proceed only if the bound item (HotelItinerary object) is not null
                if (hotelItinerary != null)
                {
                    
                    //btnBooking.OnClientClick = "CancelAmendBooking('" + hotelItinerary.ConfirmationNo + "');";
                    HotelItinerary itinerary = new HotelItinerary();
                    BookingDetail bkgDetail = null;
                    //AgentMaster agent = null;
                    try
                    {
                        //---------------------------Retrieve Agent-------------------------------------------
                        int bkgId = BookingDetail.GetBookingIdByProductId(Convert.ToInt32(hotelItinerary["HotelId"]), ProductType.Hotel);
                        bkgDetail = new BookingDetail(bkgId);
                        Product[] products = BookingDetail.GetProductsLine(bkgId);

                        itinerary.Load(products[0].ProductId);
                        btnBooking.OnClientClick = "return CancelAmendBooking('" + e.Item.ItemIndex + "','" + itinerary.ConfirmationNo + "');";
                        //BookingDetail bookingDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(itinerary.HotelId, ProductType.Hotel));
                        btnInvoice.OnClientClick = "return ViewInvoice('" + itinerary.HotelId + "','" + bkgDetail.AgencyId + "','" + itinerary.ConfirmationNo + "');";
                        string approvalStatus = "";
                        if (Settings.LoginInfo.IsCorporate == "Y")
                        {
                            btnInvoice.Visible  = false;
                            approvalStatus = Convert.ToString(hotelItinerary["HotelPolicyStatus"]);
                        }
                        btnOpen.OnClientClick = "return ViewVoucher('" + itinerary.ConfirmationNo + "','" + approvalStatus + "','" + itinerary.HotelId + "');";
                        LnkOpen.OnClientClick = "return ViewBookingForHotel('" + itinerary.HotelId + "','" + approvalStatus + "');";
                        if (itinerary.TransType == "B2C" || itinerary.PaymentMode==ModeOfPayment.CreditCard)
                        {
                            lnkPayment.Visible = true;
                            lnkPayment.OnClientClick = "return ViewPaymentInfo('"+e.Item.ItemIndex+"','" + bkgId + "');";
                        }
                        
                        HotelPassenger hPax = new HotelPassenger();
                        HotelRoom hRoom = new HotelRoom();
                        PriceAccounts priceInfo = new PriceAccounts();

                        // Getting Passenger details
                        hPax.Load(products[0].ProductId);
                        itinerary.HotelPassenger = hPax;
                        if (itinerary.Source != HotelBookingSource.GIMMONIX)
                        {
                            lblSupplier.Text = (itinerary.Source.ToString());
                        }
                            
                        //Getting HotelRoom Details  
                        itinerary.Roomtype = hRoom.Load(products[0].ProductId);
                        hRoom = itinerary.Roomtype[0];
                        priceInfo.Load(hRoom.PriceId);
                        hRoom.Price = priceInfo;
                        agent = new AgentMaster(bkgDetail.AgencyId);
                        lblAgentName.Text = agent.Name;
                        //Added by brahmam 
                        lblAgencyBalance.Text = agent.CurrentBalance.ToString("0.000");
                        lblAgencyEmail.Text = agent.Email1;
                        lblAgencyName.Text = agent.Name;
                        lblAgencyPhone1.Text = agent.Phone1;
                        lblAgencyPhone2.Text = agent.Phone2;
                        LocationMaster location = new LocationMaster(itinerary.LocationId);
                        lblLocation.Text = location.Name;
                        FlightPolicy flightPolicy = new FlightPolicy();
                        flightPolicy.Flightid = itinerary.HotelId;
                        flightPolicy.ProductId = 2;
                        flightPolicy.GetPolicyByFlightId();

                        if(Settings.LoginInfo.IsCorporate!="Y")
                        { 
                        switch (Convert.ToString(hotelItinerary["HotelPolicyStatus"]))
                        {
                            case "P":
                                lblApprovalStatus.Text = "Approval Status: Pending";
                                break;
                            case "A":
                                lblApprovalStatus.Text = "Approval Status: Approved";
                                break;
                            case "R":
                                lblApprovalStatus.Text = "Approval Status: Rejected";
                                break;
                        }

                            //Show approver email id either approved or rejected
                            CorporateProfileTripDetails detail = new CorporateProfileTripDetails(itinerary.HotelId);
                            string appStatus = Convert.ToString(hotelItinerary["HotelPolicyStatus"]);
                            if (detail != null && detail.ProfileApproversList.Count > 0 && (appStatus == "A" || appStatus == "R"))
                            {
                                lblApprovedBy.Text = appStatus == "A" ? "ApprovedBy: " + detail.ProfileApproversList[0].ApproverEmail : "RejectedBy: " + detail.ProfileApproversList[0].ApproverEmail + " - Remarks: " + CorpProfileApproval.GetRejectionReason(itinerary.HotelId);
                                lblApprovedBy.CssClass = appStatus == "A" ? "text-success" : "text-danger";
                            }
                        }

                        //Added by somasekhar
                        if (Convert.ToString(hotelItinerary["HotelPolicyStatus"]) == "P" && Settings.LoginInfo.IsCorporate!="Y")
                        {
                            btnUpdateStatus.Visible = true;
                            ddlApprovalStatus.Attributes.Add("onchange", "ShowRemarks('" + ddlApprovalStatus.ClientID + "','" + e.Item.ItemIndex + "')");
                            btnUpdateStatus.OnClientClick = "return ValidateCorpStatus('" + e.Item.ItemIndex + "');";
                            lblCorpLabel.Visible=true;                            
                            ddlApprovalStatus.Visible = true;                            
                        }
                        else
                        {
                            lblCorpLabel.Visible = false;
                            btnUpdateStatus.Visible = false;
                            ddlApprovalStatus.Visible = false;
                        }
                        

                        //------------------------------------------------------------------------------------
                    }
                    catch { }
                    try
                    {
                        UserMaster bookedBy = new UserMaster(Convert.ToInt64(hotelItinerary["CreatedBy"]));
                        lblBookedBy.Text = bookedBy.FirstName + " " + bookedBy.LastName;
                    }
                    catch { }
                    int adults = 0, childs = 0;
                    decimal totalPrice = 0, outVatAmount = 0;
                    for (int k = 0; k < itinerary.Roomtype.Length; k++)
                    {
                        adults += itinerary.Roomtype[k].AdultCount;
                        childs += itinerary.Roomtype[k].ChildCount;
                        if (itinerary.Roomtype[0].Price.AccPriceType == PriceType.PublishedFare)
                        {
                            totalPrice += itinerary.Roomtype[k].Price.PublishedFare - itinerary.Roomtype[k].Price.Discount;
                        }
                        else
                        {
                            totalPrice += itinerary.Roomtype[k].Price.NetFare + itinerary.Roomtype[k].Price.Markup + itinerary.Roomtype[k].Price.B2CMarkup - itinerary.Roomtype[k].Price.Discount;
                            outVatAmount += itinerary.Roomtype[k].Price.OutputVATAmount;
                        }
                        //added by phani
                        if (itinerary.Source == HotelBookingSource.GIMMONIX)
                        {
                            if (!string.IsNullOrEmpty(itinerary.Roomtype[k].Gxsupplier))
                            {
                                if (string.IsNullOrEmpty(lblSupplier.Text))
                                    lblSupplier.Text =  itinerary.Roomtype[k].Gxsupplier;
                                else
                                    lblSupplier.Text += "," + itinerary.Roomtype[k].Gxsupplier;
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(lblSupplier.Text))
                                    lblSupplier.Text = "GX";
                            }
                           // lblSupplier.Text = "GX-" + lblSupplier.Text;
                        }
                    }
                     totalPrice = Math.Ceiling(totalPrice) + Math.Ceiling(outVatAmount);

                    //Bind the Hotel Booking price
                    lblPrice.Text = (itinerary.Roomtype[0].Price.Currency != null ? Util.GetCurrencySymbol(itinerary.Roomtype[0].Price.Currency) : "AED ") + " " + Math.Round(totalPrice, agent.DecimalValue).ToString("N" + agent.DecimalValue);
                    lblBookingPrice.Text = Math.Ceiling(totalPrice).ToString();
                    lblPaxName.Text = (itinerary.HotelPassenger.Title == null ? "" : itinerary.HotelPassenger.Title+" ") + itinerary.HotelPassenger.Firstname + " " + itinerary.HotelPassenger.Lastname;
                    lblGuest.Text = adults + childs + " Adults (" + adults + ") Childs (" + childs + ")";
                    lblAgentRef.Text = itinerary.AgentRef;

                 
					
                    DateTime checkInDate = itinerary.StartDate;
                    //if (Convert.ToInt32(Math.Ceiling(itinerary.LastCancellationDate.Subtract(DateTime.Now).TotalDays)) > 0)
                    if (Settings.LoginInfo.MemberType == MemberType.SUPER || Convert.ToInt32(Math.Ceiling(checkInDate.Subtract(DateTime.Now).TotalDays)) > 1)
                    {
                        //lblRequestStatus.Text = "<a href='#' onclick=" + "showStuff('DisplayGrid2');" + ">Request Change</a>";
                        tblBooking.Visible = true;
                        btnBooking.Visible = true;
                    }
                    else
                    {
                        string[] lastCancelPolicy = (itinerary.HotelCancelPolicy.Split('|'));
                        string[] lastCancelPolicyToDate;
                        if (lastCancelPolicy.Length > 0)
                        {
                            if (lastCancelPolicy[lastCancelPolicy.Length - 1] == "")
                            {
                                lastCancelPolicyToDate = System.Text.RegularExpressions.Regex.Split(lastCancelPolicy[lastCancelPolicy.Length - 2], "to");
                            }
                            else
                            {
                                lastCancelPolicyToDate = System.Text.RegularExpressions.Regex.Split(lastCancelPolicy[lastCancelPolicy.Length - 1], "and");
                            }

                            try
                            {
                                if (Convert.ToInt32(Math.Ceiling(Convert.ToDateTime(lastCancelPolicyToDate[lastCancelPolicyToDate.Length - 1]).Subtract(DateTime.Now).TotalDays)) > 0)
                                {
                                    tblBooking.Visible = true;
                                    btnBooking.Visible = true;
                                }
                                else
                                {
                                    tblBooking.Visible = false;
                                    btnBooking.Visible = false;
                                }
                            }
                            catch
                            {
                                tblBooking.Visible = true;
                                btnBooking.Visible = true;
                            }
                        }
                    }
                    if (itinerary.VoucherStatus && itinerary.Status == HotelBookingStatus.Confirmed)
                    {
                        lblStatus.Text = "Vouchered";

                    }
                    else
                    {
                        lblStatus.Text = itinerary.Status.ToString();
                        //Button btnInvoice = e.Item.FindControl("btnInvoice") as Button;

                        lblStatus.Style["color"] = "Red";

                        btnInvoice.Visible = false;
                    }
                    // string remarks = string.Empty;
                    //if (itinerary != null && (itinerary.Status == HotelBookingStatus.Cancelled) || itinerary.Status == HotelBookingStatus.Pending)
                    if (bkgDetail != null && bkgDetail.Status == BookingStatus.InProgress)
                    {
                        //remarks = "Cancelled";
                        //lblBooking.Text =bkgDetail.Status==BookingStatus.InProgress?  "Request For Cancellation": "";
                        lblBooking.Text = "Request For Cancellation";
                        btnBooking.Visible = false;
                        tblBooking.Visible = false;
                    }

                    //To show booking pending remarks in popup,added by Harish
                    if (itinerary.Status== HotelBookingStatus.Pending)
                    {
                        //remarks = "Pending";
                        string bookingRemarks = (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent) &&(!string.IsNullOrEmpty(itinerary.BookingRemarks)) ? itinerary.BookingRemarks : "Booking may or may not be happened please contact with Admin ";
                        lnkPendingRemarks.Visible = true;
                        bookingRemarks = System.Text.RegularExpressions.Regex.Replace(bookingRemarks, @"[^\w\d]", " ");  
                            //bookingRemarks.Replace("'", " ").Replace("_", " ").Replace("-"," ").Replace("/", " ").Replace("\", " ");                   
                        lnkPendingRemarks.OnClientClick = "return ViewPendingRemarks('" + bookingRemarks + "');";
                    }
                    if (itinerary.Source == HotelBookingSource.GIMMONIX)
                    {
                        string HotelCancelPolicy = (itinerary.HotelCancelPolicy).Replace("^Date and time is calculated based on local time of destination|", "|");
                        string HotelPolicyDetails = (itinerary.HotelPolicyDetails).Replace("\n", "").Replace("\r", "").Contains("^") ? (itinerary.HotelPolicyDetails).Replace("\n", "").Replace("\r", "").Split('^')[0] : (itinerary.HotelPolicyDetails).Replace("\n", "").Replace("\r", "");
                        string Disclaimer = (itinerary.HotelPolicyDetails).Replace("\n", "").Replace("\r", "").Contains("^") ? (itinerary.HotelPolicyDetails).Replace("\n", "").Replace("\r", "").Split('^')[1].Split('|')[0] : "";
                        LnkCancelPolicy.OnClientClick = "return ViewCancellationPolicy('" + HotelCancelPolicy.TrimStart().Replace("|", "<br>").Replace("^", "<br>") + "','" + itinerary.HotelId + "','" + HotelPolicyDetails + "','" + Disclaimer + "');";
                    }
                    else
                    {
                        string HotelCancelPolicy = (itinerary.HotelCancelPolicy).Replace("^Date and time is calculated based on local time of destination|", "|");
                        string HotelPolicyDetails = (itinerary.HotelPolicyDetails).Replace("\n", "").Replace("\r", "").Contains("^") ? (itinerary.HotelPolicyDetails).Replace("\n", "").Replace("\r", "").Split('^')[0] : (itinerary.HotelPolicyDetails).Replace("\n", "").Replace("\r", "");
                        LnkCancelPolicy.OnClientClick = "return ViewCancellationPolicyOther('" + HotelCancelPolicy.TrimStart().Replace("|", "<br>").Replace("^", "<br>") + "','" + itinerary.HotelId + "','" + HotelPolicyDetails + "');";
                    }

                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.HotelBook, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.ToString(), "0");
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlBookings_ItemCommand(object sender, DataListCommandEventArgs e)
   {

        //Label LblRefundCnfNo = e.Item.FindControl("LblRefundCnfNo") as Label;
        //if (e.CommandName == "Voucher")
        //{
        //    //string script = "window.open('printHotelVoucher.aspx?ConfNo=" + e.CommandArgument + "','','width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
        //    //Utility.StartupScript(this.Page, script, "PrintHotelVoucherScript");
        //}
        //else if (e.CommandName == "Invoice")
        //{
        //    //HotelItinerary itinerary = new HotelItinerary();
        //    //itinerary.Load(HotelItinerary.GetHotelId(e.CommandArgument.ToString()));
        //    //BookingDetail bookingDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(itinerary.HotelId, ProductType.Hotel));

        //    //string script = "window.open('CreateHotelInvoice.aspx?hotelId=" + itinerary.HotelId + "&agencyId=" + bookingDetail.AgencyId + "&ConfNo=" + e.CommandArgument + "','','width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
        //    //Utility.StartupScript(this.Page, script, "PrintHotelInvoiceScript");
        //    ////Response.Redirect("CreateHotelInvoice.aspx?hotelId=" + itinerary.HotelId + "&agencyId=" + bookingDetail.AgencyId + "&ConfNo=" + e.CommandArgument);
        //}
        //else
        if (e.CommandName == "Booking")
        {
            if (e.CommandArgument.ToString().Length > 0)
            {
                DropDownList cmbBooking = e.Item.FindControl("ddlBooking") as DropDownList;
                TextBox txtRemarks = e.Item.FindControl("txtRemarks") as TextBox;

                string bkg = cmbBooking.SelectedValue;

                string confirmationNo = e.CommandArgument.ToString();


                if (bkg.ToLower() == "cancel booking")
                {
                    BookingDetail bookingDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(HotelItinerary.GetHotelId(confirmationNo), ProductType.Hotel));
                    int bookingId = bookingDetail.BookingId;
                    string data = txtRemarks.Text;
                    int loggedMemberId = Convert.ToInt32(Settings.LoginInfo.UserID);
                    int requestTypeId = 4;
                    AgentMaster agency = null;
                    try
                    {

                        ServiceRequest serviceRequest = new ServiceRequest();
                        BookingDetail booking = new BookingDetail();
                        booking = new BookingDetail(bookingId);
                        HotelItinerary itineary = new HotelItinerary();
                        HotelRoom room = new HotelRoom();
                        Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
                        for (int i = 0; i < products.Length; i++)
                        {
                            if (products[i].ProductTypeId == (int)ProductType.Hotel)
                            {
                                itineary.Load(products[i].ProductId);
                                itineary.Roomtype = room.Load(products[i].ProductId);
                                break;
                            }
                        }

                        agency = new AgentMaster(booking.AgencyId);
                        serviceRequest.BookingId = bookingId;
                        serviceRequest.ReferenceId = itineary.Roomtype[0].RoomId;
                        serviceRequest.ProductType = ProductType.Hotel;

                        serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
                        serviceRequest.Data = data;
                        serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;

                        serviceRequest.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                        //serviceRequest.AgencyId = Settings.LoginInfo.AgentId;
                        serviceRequest.IsDomestic = itineary.IsDomestic;
                        //Audit.Add(EventType.HotelBook, Severity.High, 1, "1", "");
                        //Audit.Add(EventType.HotelBook, Severity.High, 1, "pax name:" + itineary.Roomtype[0].AdultCount.ToString(), "");
                        //Audit.Add(EventType.HotelBook, Severity.High, 1, "2", "");
                        serviceRequest.PaxName = itineary.Roomtype[0].PassenegerInfo[0].Firstname + itineary.Roomtype[0].PassenegerInfo[0].Lastname;
                        serviceRequest.Pnr = itineary.ConfirmationNo;
                        serviceRequest.Source = (int)itineary.Source;
                        serviceRequest.StartDate = itineary.StartDate;
                        serviceRequest.SupplierName = itineary.HotelName;
                        serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
                        serviceRequest.ReferenceNumber = itineary.BookingRefNo;
                        serviceRequest.PriceId = itineary.Roomtype[0].PriceId;
                        //serviceRequest.AgencyId = (int)agency.ID;
                        serviceRequest.AgencyId = booking.AgencyId;//modified by Anji based on Harish suggestions //Settings.LoginInfo.AgentId; //Modified by brahmam       //Every request must fall in Admin Queue, so assign Admin ID
                        serviceRequest.ItemTypeId = InvoiceItemTypeId.HotelBooking;
                        serviceRequest.DocName = "";
                        serviceRequest.LastModifiedBy = serviceRequest.CreatedBy;
                        serviceRequest.LastModifiedOn = DateTime.Now;
                        serviceRequest.CreatedOn = DateTime.Now;
                        //Added by shiva
                        serviceRequest.IsDomestic = true;
                        serviceRequest.AgencyTypeId = Agencytype.Cash;
                        serviceRequest.RequestSourceId = RequestSource.BookingAPI;
                        serviceRequest.Save();
                        // Setting booking details status 
                        if (requestTypeId == 4)
                        {
                            booking.SetBookingStatus(BookingStatus.InProgress, loggedMemberId);
                        }
                        else
                        {
                            booking.SetBookingStatus(BookingStatus.AmendmentInProgress, loggedMemberId);
                        }

                        BookingHistory bh = new BookingHistory();
                        bh.BookingId = bookingId;
                        bh.Remarks = "Request sent to " + serviceRequest.RequestType.ToString() + " of the hotel booking.";

                        if (requestTypeId == 4)
                        {
                            bh.EventCategory = EventCategory.HotelCancel;
                        }
                        else
                        {
                            bh.EventCategory = EventCategory.HotelAmendment;
                        }
                        bh.CreatedBy = (int)Settings.LoginInfo.UserID;
                        bh.Save();
                        //Sending email.
                        Hashtable table = new Hashtable();
                        table.Add("agentName", agency.Name);
                        table.Add("hotelName", itineary.HotelName);
                        table.Add("confirmationNo", itineary.ConfirmationNo);

                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                        UserMaster bookedBy = new UserMaster(itineary.CreatedBy);
                        AgentMaster bookedAgency = new AgentMaster(itineary.AgencyId);
                        //toArray.Add(bookedBy.Email);
                        //toArray.Add(bookedAgency.Email1);
                        string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["HOTEL_CANCEL_MAIL"]).Split(',');
                        foreach (string cnMail in cancelMails)
                        {
                            toArray.Add(cnMail);
                        }
                        //toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]); for time bing

                        //string message = "Your request for cancelling hotel <b>" + itineary.HotelName + " </b> is under process. Confirmation No:(" + itineary.ConfirmationNo + ")";
                        string message = ConfigurationManager.AppSettings["HOTEL_CANCEL_REQUEST"];
                        try
                        {
                            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Hotel)Request for cancellation. Confirmation No:(" + itineary.ConfirmationNo + ")", message, table);
                        }
                        catch { }

                        #region Send Mail For HotelInventory
                        if (itineary.Source == HotelBookingSource.HotelConnect)
                        {
                            try
                            {
                                toArray.Clear();
                                table.Clear();
                                string from = string.Empty, subject = string.Empty, eMailsTo = string.Empty;
                                message = string.Empty;
                                eMailsTo = CZInventory.InvBooking.GetSupplierEmails(itineary.ConfirmationNo);


                                //getting supplier Email Of HotelInventory
                                if (!string.IsNullOrEmpty(eMailsTo))
                                {
                                    toArray.AddRange(eMailsTo.Split(','));
                                }

                                from = ConfigurationManager.AppSettings["fromEmail"];
                                subject = ConfigurationManager.AppSettings["HIS_Supp_Cancel_Subject"];
                                message = ConfigurationManager.AppSettings["HIS_Supp_Cancel_Message"];

                                table.Add("hotelName", "<b>"+itineary.HotelName+"</b>");
                                table.Add("confNo", "<b>"+itineary.ConfirmationNo+"</b>");

                                Email.Send(from, from, toArray, subject, message, table);


                            }
                            catch (Exception ex)
                            {

                                Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Mail Sending Failed To Supplier. Error : " + ex.ToString(), "");
                            }

                            try
                            {
                                CZInventory.InvBooking.AddBookingStatusMail(itineary.HotelId, itineary.ConfirmationNo, BookingStatus.CancellationInProgress, string.Empty);
                            }
                            catch (Exception ex)
                            {

                                Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Failed to Update Booking Status. Error : " + ex.ToString(), "");
                            }
                        }
                        #endregion
                        #region Send Mail For HotelExtranet
                        if (itineary.Source == HotelBookingSource.HotelExtranet)
                        {
                            try
                            {
                                CZInventory.CZHISSearchEngine his = new CZInventory.CZHISSearchEngine();
                                message = string.Empty;
                                message = his.CancelBookingRequest(itineary.ConfirmationNo, true);
                               
                            }
                            catch (Exception ex)
                            {

                                Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Mail Sending Failed To Supplier. Error : " + ex.ToString(), "");
                            }

                            try
                            {
                                CZInventory.InvBooking.AddBookingStatusMail(itineary.HotelId, itineary.ConfirmationNo, BookingStatus.CancellationInProgress, string.Empty);
                            }
                            catch (Exception ex)
                            {

                                Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Failed to Update Booking Status. Error : " + ex.ToString(), "");
                            }
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.HotelBook, Severity.High, 1, "Exception in the Hotel Change Request. Message:" + ex.Message, "");

                    }
                }
                //else
                //{
                //    //Amend Hotel Booking: TODO shiva
                //}
            }
            Label lblRequestStatus = e.Item.FindControl("lblRequestStatus") as Label;
            Label lblBooking = e.Item.FindControl("lblBooking") as Label;

            BookingDetail bkgDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(HotelItinerary.GetHotelId(e.CommandArgument.ToString()), ProductType.Hotel));
            if (bkgDetail.Status == BookingStatus.InProgress)
            {
                lblBooking.Text = "Request For Cancellation";
            }

            LoadBookings();
        }

        else if (e.CommandName == "ApprovalStatus")
        {
            if (e.CommandArgument.ToString().Length > 0)
            {
                FlightPolicy flightPolicy = new FlightPolicy();
                
                DropDownList cmbApprovalStatus = e.Item.FindControl("ddlApprovalStatus") as DropDownList;
               // Label errApprovalStatus = e.Item.FindControl("errApprovalStatus") as Label;
                string approvalStatus = cmbApprovalStatus.SelectedValue;
                TextBox txtApprovalRemarks = e.Item.FindControl("txtCorpRemarks") as TextBox;
                // Label errApprovalStatus = e.Item.FindControl("errApprovalStatus") as Label;
                string approvalRemarks = txtApprovalRemarks.Text;
                int hotelId = Convert.ToInt32(e.CommandArgument);
                try
                {
                    flightPolicy.Flightid = hotelId;                    
                    flightPolicy.ApprovalStatus = approvalStatus;
                    flightPolicy.ApprovedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                    flightPolicy.SelectedTrip = true;
                    flightPolicy.ProductId = 2;
                    flightPolicy.ApproverComment = approvalStatus == "R" ? approvalRemarks : string.Empty;
                    flightPolicy.UpdateStatusManually();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Failed to Update Booking Approval Status. Error : " + ex.ToString(), "");
                }

                LoadBookings();
            }
        }

        else if (e.CommandName == "Refund")
        {
            loggedMemberId = (int)Settings.LoginInfo.UserID;
            TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
            TextBox txtSupplierFee = e.Item.FindControl("txtSupplierFee") as TextBox;
            Label lblRemarks = e.Item.FindControl("lblRemarks") as Label;
            Label lblBookingPrice = e.Item.FindControl("lblBookingPrice") as Label;
            string confirmationNo = ConfirmationNum.Value;
            lblMessage.Text = "";
            
            if (txtAdminFee.Text.Trim().Length > 0 && txtSupplierFee.Text.Trim().Length > 0)
            {
                decimal bookingAmount = Math.Ceiling(Convert.ToDecimal(lblBookingPrice.Text));                
                decimal feeAmount = Math.Ceiling(Convert.ToDecimal(txtAdminFee.Text) + Convert.ToDecimal(txtSupplierFee.Text));
                // for Checking Admin Fee and Supplier Fee should not be Greater Then Booking Amount
                if (feeAmount < 0 || bookingAmount < feeAmount)
                {
                    lblMessage.Text = "Admin Fee and Supplier Fee should not be Greater Then Booking Amount";
                }
                else
                {
                    BookingDetail bookingDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(HotelItinerary.GetHotelId(confirmationNo), ProductType.Hotel));
                    HotelItinerary itinerary = new HotelItinerary();                    
                    HotelRoom room = new HotelRoom();
                    HotelPassenger passInfo = new HotelPassenger();
                    AgentMaster agent = new AgentMaster(bookingDetail.AgencyId);
                    Product[] products = BookingDetail.GetProductsLine(bookingDetail.BookingId);
                    decimal bookingAmt = 0;
                    for (int i = 0; i < products.Length; i++)
                    {
                        if (products[i].ProductTypeId == (int)ProductType.Hotel)
                        {
                            itinerary.Load(products[i].ProductId);
                            passInfo.Load(products[i].ProductId);
                            itinerary.HotelPassenger = passInfo;
                            itinerary.Roomtype = room.Load(products[i].ProductId);
                            break;
                        }
                    }

                    decimal discount = 0;
                    foreach (HotelRoom hroom in itinerary.Roomtype)
                    {
                        if (hroom.Price.NetFare > 0)
                        {                        
                            bookingAmt += (hroom.Price.NetFare + hroom.Price.B2CMarkup);//Added only B2c Markup by chandan 
                        }
                       
                        discount += hroom.Price.Discount;
                    }

                    bookingAmt = Math.Ceiling(bookingAmt);
                    
                    Dictionary<string, Dictionary<string, string>> CancelDetails = new Dictionary<string, Dictionary<string, string>>();
                    Dictionary<string, string> cancellationData = new Dictionary<string, string>();
                    if (itinerary.Source == HotelBookingSource.DOTW)
                    {
                        MetaSearchEngine mse = new MetaSearchEngine();
                        cancellationData = mse.CancelHotel(itinerary, null);
                    }
                    else if (itinerary.Source == HotelBookingSource.HotelBeds)  // Added by brahmam 26.09.2014
                    {
                        CT.BookingEngine.GDS.HotelBeds hBeds = new CT.BookingEngine.GDS.HotelBeds();
                        cancellationData = hBeds.CancelBooking(itinerary);
                    }
                    else if (itinerary.Source == HotelBookingSource.RezLive)
                    {
                        RezLive.XmlHub rezAPI = new RezLive.XmlHub();
                        cancellationData = rezAPI.CancelHotelBooking(itinerary.ConfirmationNo, itinerary.BookingRefNo);
                    }
                    else if (itinerary.Source == HotelBookingSource.LOH)
                    {
                        LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine();
                        cancellationData = jxe.CancelHotel(itinerary.ConfirmationNo);
                    }
                    else if (itinerary.Source == HotelBookingSource.GTA)
                    {
                        CT.BookingEngine.GDS.GTA gtaApi = new CT.BookingEngine.GDS.GTA();
                        cancellationData = gtaApi.CancelHotelBooking(itinerary);
                    }
                    else if (itinerary.Source == HotelBookingSource.TBOHotel)
                    {
                        string remarks = lblRemarks.Text.Trim();
                        TBOHotel.HotelV10 tboHotel = new TBOHotel.HotelV10();
                        cancellationData = tboHotel.CancelHotelBooking(itinerary.BookingRefNo, remarks);
                    }
                    else if (itinerary.Source == HotelBookingSource.Miki)
                    {
                        CT.BookingEngine.GDS.MikiApi mikiApi = new CT.BookingEngine.GDS.MikiApi(Settings.LoginInfo.LocationCountryCode);
                        string[] confirmationCodes = itinerary.ConfirmationNo.Split('|');
                        foreach (string confirmationCode in confirmationCodes)
                        {
                            cancellationData = mikiApi.CancelBooking(confirmationCode);
                            CancelDetails.Add(confirmationCode, cancellationData);                           
                        }
                    }
                    else if (itinerary.Source == HotelBookingSource.HotelConnect)
                    {
                        CZInventory.SearchEngine his = new CZInventory.SearchEngine();
                        cancellationData = his.CancelBooking(itinerary.ConfirmationNo, true);
                    }
                    //WST Cancellation added by brahmam 27.06.2016
                    else if (itinerary.Source == HotelBookingSource.WST)
                    {
                        string remarks = lblRemarks.Text.Trim();
                        CT.BookingEngine.GDS.WST wstApi = new CT.BookingEngine.GDS.WST();
                        cancellationData = wstApi.CancelBooking(itinerary.ConfirmationNo, remarks);
                    }
                    //JAC Source Added by brahmam 20.09.2016
                    else if (itinerary.Source == HotelBookingSource.JAC)
                    {
                        CT.BookingEngine.GDS.JAC jacApi = new CT.BookingEngine.GDS.JAC();
                        cancellationData = jacApi.CancelHotelBooking(itinerary.ConfirmationNo);
                    }
                    //EET Source Added by brahmam 10.03.2016
                    else if (itinerary.Source == HotelBookingSource.EET)
                    {
                        CT.BookingEngine.GDS.EET eetApi = new CT.BookingEngine.GDS.EET();
                        cancellationData = eetApi.CancelHotel(itinerary.ConfirmationNo);
                    }
                    //Agoda Source Added by brahmam 08.02.2018
                    else if (itinerary.Source == HotelBookingSource.Agoda)
                    {
                        CT.BookingEngine.GDS.Agoda agodaApi = new CT.BookingEngine.GDS.Agoda();
                        cancellationData = agodaApi.CancelHotelBooking(itinerary.ConfirmationNo);
                    }
                    //Yatra Source Added by somasekhar on 14/09/2018
                    else if (itinerary.Source == HotelBookingSource.Yatra)
                    {
                        CT.BookingEngine.GDS.Yatra yatraApi = new CT.BookingEngine.GDS.Yatra();
                        yatraApi.AppUserId = loggedMemberId;
                        cancellationData = yatraApi.CancelHotelBooking(itinerary);
                    }
                    //GRN Source Added by Harish 09.19.2018
                    #region GRN
                    else if (itinerary.Source == HotelBookingSource.GRN)
                    {
                        CT.BookingEngine.GDS.GRN grnApi = new CT.BookingEngine.GDS.GRN();
                        grnApi.GrnUserId = Settings.LoginInfo.UserID;
                        cancellationData = grnApi.CancelHotelBooking(itinerary.BookingRefNo);
                    }
                    #endregion
                    //OYO Source Added by Somasekhar on 20/12/2018
                    else if (itinerary.Source == HotelBookingSource.OYO)
                    {
                        CT.BookingEngine.GDS.OYO oyoApi = new CT.BookingEngine.GDS.OYO();
                        oyoApi.AppUserId = loggedMemberId;
                        cancellationData = oyoApi.CancelHotelBooking(itinerary);
                    }
                    //Get Cancel Info for Gimmonix  source 
                    else if (itinerary.Source == HotelBookingSource.GIMMONIX || itinerary.Source == HotelBookingSource.UAH)
                    {
                        try
                        {
                            string apiUrl = string.Empty;
                            apiUrl = ConfigurationManager.AppSettings["WebApiHotelUrl"].ToString();
                            if (string.IsNullOrEmpty(apiUrl))
                            {
                                apiUrl = Request.Url.Scheme + "://" + Request.Url.Host + "/HotelWebApi";                               
                            }
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:apiURL:" + apiUrl, "");
                            object input = new
                            {
                                agentId = itinerary.AgencyId,
                                userId = itinerary.CreatedBy,
                                source = itinerary.Source == HotelBookingSource.GIMMONIX ? "GIMMONIX" : "UAH",
                                bookingRefNo = itinerary.Source == HotelBookingSource.GIMMONIX ? itinerary.BookingRefNo : itinerary.ConfirmationNo + "|" + itinerary.BookingRefNo  //"DHB190430162048432|3900673|3871174" //
                            };
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:apiURL 200 :" + apiUrl, "");
                            string inputJson = (new JavaScriptSerializer()).Serialize(input);
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:apiURL 300 :" + apiUrl, "");
                            WebClient client = new WebClient();
                            client.Headers["Content-type"] = "application/json";
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:input 400 :" + inputJson, "");
                            string CancelInfo = client.UploadString(apiUrl + "/api/HotelCancellation/GetCancelInfo", inputJson);
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:apiURL 500 :" + apiUrl, "");
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:inputJson:" + inputJson, "");
                            cancellationData = (new JavaScriptSerializer()).Deserialize<Dictionary<string, string>>(CancelInfo);
                        }
                        catch (Exception exClient)
                        {
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:exClient" + exClient.ToString(), "");
                        }
                        //cancellationData = HttpContext.Current.Session["ApiCancelInfo"] as Dictionary<string, string>;
                    }
                    //Added conditon For get cancellation details of Miki & Non-Miki product
                    if (itinerary.Source == HotelBookingSource.Miki)
                    {
                        string currency = "";
                        decimal CancelAmount = 0;
                        //string cancelId = "";
                        foreach (KeyValuePair<string, Dictionary<string, string>> cancelDetail in CancelDetails)
                        {
                            cancellationData = cancelDetail.Value;


                            if (cancellationData["Status"] == "Cancelled" || cancellationData["Status"] == "CANCELLED") //Added by chandan
                            {
                                if (itinerary.CancelId != null && itinerary.CancelId.Length > 0)
                                {
                                    itinerary.CancelId += "|" + cancellationData["ID"];
                                }
                                else
                                {
                                    itinerary.CancelId = cancellationData["ID"]; ;
                                }
                                if (cancellationData.ContainsKey("Amount") && Convert.ToDecimal(cancellationData["Amount"]) > 0)
                                {
                                    CancelAmount += Math.Ceiling(Convert.ToDecimal(cancellationData["Amount"]));
                                }
                                currency = cancellationData["Currency"];
                            }

                            else
                            {
                                lblMessage.Text = "Failed Cancellation from " + itinerary.Source.ToString() + " Confirmation No: " + itinerary.ConfirmationNo;
                                Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), lblMessage.Text, null);
                            }
                        }
                       
                        //Initialize again with update price and ID For Miki+Non Miki Item
                        cancellationData["Amount"] = Convert.ToString(CancelAmount);
                        cancellationData["Currency"] = Convert.ToString(currency);
                        cancellationData["ID"] = itinerary.CancelId;
                    }

                    if (cancellationData["Status"] == "Cancelled" || cancellationData["Status"] == "CANCELLED") //Added by brahmam
                    {

                        itinerary.Status = HotelBookingStatus.Cancelled;
                        if (itinerary.Source == HotelBookingSource.DOTW)
                        {
                            itinerary.CancelId = cancellationData["ID"];
                        }
                        else if (itinerary.Source == HotelBookingSource.HotelBeds) //Added by brahmam 26.09.2014
                        {
                            itinerary.CancelId = itinerary.ConfirmationNo;
                        }
                        else if (itinerary.Source == HotelBookingSource.RezLive)
                        {
                            //Temp code for testing
                            itinerary.CancelId = itinerary.ConfirmationNo;
                        }
                        else if (itinerary.Source == HotelBookingSource.GTA)
                        {
                            itinerary.CancelId = itinerary.ConfirmationNo;
                        }
                        else if (itinerary.Source == HotelBookingSource.WST)
                        {
                            itinerary.CancelId = itinerary.ConfirmationNo;
                        }
                        else if (itinerary.Source == HotelBookingSource.HotelConnect)
                        {
                            itinerary.CancelId = itinerary.ConfirmationNo;
                        }
                        else if (itinerary.Source == HotelBookingSource.TBOHotel)
                        {
                            itinerary.CancelId = cancellationData["ID"];
                        }
                       
                        // Condition added by somasekhar on 20/1202018 for OYO
                        else if (itinerary.Source == HotelBookingSource.JAC || itinerary.Source == HotelBookingSource.Agoda || itinerary.Source == HotelBookingSource.Yatra || itinerary.Source == HotelBookingSource.OYO)
                        {
                            itinerary.CancelId = cancellationData["ID"];
                        }
                        else if (itinerary.Source == HotelBookingSource.GRN)
                        {
                            itinerary.CancelId = cancellationData["ID"];
                        }
                        else if (itinerary.Source == HotelBookingSource.GIMMONIX)
                        {
                            itinerary.CancelId = cancellationData["ID"];
                        }
                        itinerary.Update();

                        BookingHistory bookingHistory = new BookingHistory();
                        bookingHistory.BookingId = bookingDetail.BookingId;
                        bookingHistory.EventCategory = EventCategory.HotelCancel;
                        bookingHistory.Remarks = "Hotel Booking is cancelled";
                        bookingHistory.CreatedBy = itinerary.CreatedBy;
                        bookingHistory.Save();

                        serviceRequest = Session["ServiceRequests"] as ServiceRequest[];

                        //                        loggedMemberId = (int)Settings.LoginInfo.UserID;
                        ServiceRequest sr = new ServiceRequest();

                        //TODO: Create a PaymentDetails for cancellation details and update the cancellation charges: Shiva

                        CT.BookingEngine.CancellationCharges cancellationCharge = new CT.BookingEngine.CancellationCharges();
                        cancellationCharge.AdminFee = Convert.ToDecimal(txtAdminFee.Text);
                        cancellationCharge.SupplierFee = Convert.ToDecimal(txtSupplierFee.Text);
                        cancellationCharge.PaymentDetailId = 0;//pd.PaymentDetailId;
                        cancellationCharge.ReferenceId = itinerary.Roomtype[0].RoomId;
                        decimal exchangeRate = 0;
                        if (cancellationData["Currency"] != agent.AgentCurrency)
                        {
                            StaticData staticInfo = new StaticData();
                            staticInfo.BaseCurrency = agent.AgentCurrency;

                            Dictionary<string, decimal> rateOfExList = staticInfo.CurrencyROE;
                            if (itinerary.Source == HotelBookingSource.GRN && cancellationData.ContainsKey("BookingId") && cancellationData.ContainsKey("ID") && (cancellationData["BookingId"] == cancellationData["ID"]))
                            {
                                cancellationData["Currency"] = agent.AgentCurrency;
                                string cancelAmount = cancellationData["Amount"];
                                if (cancelAmount.Contains("%"))
                                {
                                    cancelAmount = cancelAmount.Replace('%', ' ');
                                    cancellationData["Amount"] = cancelAmount;
                                    cancellationData["Amount"] = Convert.ToString(bookingAmt * Convert.ToDecimal(cancellationData["Amount"]) / 100);
                                }
                            }
                            exchangeRate = rateOfExList[cancellationData["Currency"]];
                            cancellationCharge.CancelPenalty = (Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate);
                            //Added by somasekhar on 17/09/2018  -- For Calculate Penality Charge 
                            if (itinerary.Source == HotelBookingSource.Yatra)
                            {                                
                                //Yatra supplier directly giving refund amt, then here we are calculating Cancellation charge Amt 
                                cancellationCharge.CancelPenalty = bookingAmt - Math.Ceiling((Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate));
                            }
                        }
                        else
                        {
                            cancellationCharge.CancelPenalty = Convert.ToDecimal(cancellationData["Amount"]);
                            //Added by somasekhar on 17/09/2018  -- For Calculate Penality Charge 
                            if (itinerary.Source == HotelBookingSource.Yatra)
                            {
                                //Convert.ToDecimal(cancellationData["Amount"]) this is refund amount by supplier(yatra in INR)
                                //Yatra supplier directly giving refund amt, then here we are calculating Cancellation charge Amt 
                                cancellationCharge.CancelPenalty = bookingAmt - Math.Ceiling(Convert.ToDecimal(cancellationData["Amount"]));
                            }
                        }
                        cancellationCharge.CreatedBy = loggedMemberId;
                        cancellationCharge.ProductType = ProductType.Hotel;
                        cancellationCharge.Save();
                        //(itinerary.Source == HotelBookingSource.Yatra && Convert.ToDecimal(cancellationData["Amount"]) >= 0)
                        //above condition added for yatra. If non refundable cancellation they are giving refound amount "0"
                        if (cancellationData.ContainsKey("Amount") && Convert.ToDecimal(cancellationData["Amount"]) > 0 || (itinerary.Source == HotelBookingSource.Yatra && Convert.ToDecimal(cancellationData["Amount"]) >= 0))
                        {
                            try
                            {
                                //if (itinerary.Source == HotelBookingSource.DOTW)
                                //{
                                //    bookingAmt -= Convert.ToDecimal(cancellationData["Amount"]);
                                //}
                                //else if (itinerary.Source != HotelBookingSource.DOTW)
                                //{
                                //decimal exchangeRate = 0;
                                if (cancellationData["Currency"] != agent.AgentCurrency)
                                {
                                    StaticData staticInfo = new StaticData();
                                    staticInfo.BaseCurrency = agent.AgentCurrency;
                                    Dictionary<string, decimal> rateOfExList = staticInfo.CurrencyROE;
                                    exchangeRate = rateOfExList[cancellationData["Currency"]];
                                }
                                if (exchangeRate <= 0)
                                {
                                    bookingAmt -= Math.Ceiling(Convert.ToDecimal(cancellationData["Amount"]));
                                    //Added by somasekhar on 17/09/2018  -- For Calculate Penality Charge 
                                    if (itinerary.Source == HotelBookingSource.Yatra)
                                    {
                                        //Convert.ToDecimal(cancellationData["Amount"]) this is refund amount by supplier(yatra in INR)
                                        // If non refundable cancellation they are giving refound amount "0"
                                        //the bellow we get cancellation refund amount=0, then in ledger we updeted refund amount 0;
                                        bookingAmt -= (bookingAmt - (Math.Ceiling(Convert.ToDecimal(cancellationData["Amount"]))));
                                    }
                                }
                                else if (itinerary.Source != HotelBookingSource.HotelConnect)
                                {
                                    bookingAmt -= Math.Ceiling((Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate));
                                    //Added by somasekhar on 17/09/2018  -- For Calculate Penality Charge 
                                    if (itinerary.Source == HotelBookingSource.Yatra)
                                    {
                                        //Convert.ToDecimal(cancellationData["Amount"]) this is refund amount by supplier(yatra in INR)
                                        // If non refundable cancellation they are giving refound amount "0"
                                        //the bellow we get cancellation refund amount=0, then in ledger we updeted refund amount 0;
                                        bookingAmt -= (bookingAmt - (Math.Ceiling((Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate))));
                                    }
                                }
                                //}
                            }
                            catch { }
                        }
                        bookingAmt = Math.Ceiling(bookingAmt);
                        // Admin & Supplier Fee save in Leadger
                        int invoiceNumber = 0;
                        decimal adminChar = Math.Ceiling(Convert.ToDecimal(txtSupplierFee.Text) + Convert.ToDecimal(txtAdminFee.Text));                        
                        decimal adminFee = 0;
                        if (adminChar < bookingAmt)
                        {
                            adminFee = adminChar;
                        }
                        LedgerTransaction ledgerTxn = new LedgerTransaction();
                        NarrationBuilder objNarration = new NarrationBuilder();
                        invoiceNumber = Invoice.isInvoiceGenerated(itinerary.Roomtype[0].RoomId, ProductType.Hotel);
                        //ledgerTxn = new LedgerTransaction();
                        ledgerTxn.LedgerId = bookingDetail.AgencyId;
                        ledgerTxn.Amount = -adminFee;
                        objNarration.HotelConfirmationNo = itinerary.ConfirmationNo.Replace('|', '@'); ///modified by brahmam MIKI purpose
                        objNarration.TravelDate = itinerary.StartDate.ToShortDateString();
                        if (itinerary.PaymentMode == ModeOfPayment.CreditCard) //Checking card or credit
                        {
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CardHotelCancellationCharge;
                            objNarration.Remarks = "Card Hotel Cancellation Charges";
                        }
                        else
                        {
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.HotelCancellationCharge;
                            objNarration.Remarks = "Hotel Cancellation Charges";
                        }
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.IsLCC = true;
                        ledgerTxn.ReferenceId = itinerary.Roomtype[0].RoomId;

                        ledgerTxn.Notes = "";
                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = loggedMemberId;
                        ledgerTxn.TransType = itinerary.TransType;
                        ledgerTxn.PaymentMode = (int)itinerary.PaymentMode;
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                        //save Refund amount
                        ledgerTxn = new LedgerTransaction();
                        ledgerTxn.LedgerId = bookingDetail.AgencyId;
                        ledgerTxn.Amount = bookingAmt;
                        objNarration.PaxName = string.Format("{0} {1}", itinerary.HotelPassenger.Firstname, itinerary.HotelPassenger.Lastname);
                        if (itinerary.PaymentMode == ModeOfPayment.CreditCard) //Checking card or credit  Added by brahmam
                        {
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CardHotelRefund;
                            ledgerTxn.Notes = "Card Hotel Voucher Refunded";
                            objNarration.Remarks = "Card Refunded for Voucher No -" + itinerary.ConfirmationNo.Replace('|', '@');///modified by brahmam MIKI purpose
                        }
                        else
                        {
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.HotelRefund;
                            ledgerTxn.Notes = "Hotel Voucher Refunded";
                            objNarration.Remarks = "Refunded for Voucher No -" + itinerary.ConfirmationNo.Replace('|', '@');///modified by brahmam MIKI purpose
                        }
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.IsLCC = true;
                        ledgerTxn.ReferenceId = itinerary.Roomtype[0].RoomId;

                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = loggedMemberId;
                        ledgerTxn.TransType = itinerary.TransType;
                        ledgerTxn.PaymentMode = (int)itinerary.PaymentMode;
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);
                        if (adminFee <= bookingAmt)
                        {
                            bookingAmt -= adminFee;
                        }
                       
                        if (itinerary.PaymentMode != ModeOfPayment.CreditCard)
                        {
                            if (itinerary.TransType != "B2C")
                            {                               
                                agent.CreatedBy = Settings.LoginInfo.UserID;
                                agent.UpdateBalance(bookingAmt);

                                if (bookingDetail.AgencyId == Settings.LoginInfo.AgentId)
                                {
                                    Settings.LoginInfo.AgentBalance += bookingAmt;
                                }
                            }
                        }

                        //Update Queue Status
                        CT.Core.Queue.SetStatus(QueueType.Request, bookingDetail.BookingId, QueueStatus.Completed, loggedMemberId, 0, "Completed");
                        sr.UpdateServiceRequestAssignment(bookingDetail.BookingId, (int)ServiceRequestStatus.Completed, loggedMemberId, (int)ServiceRequestStatus.Completed, null);

                        //Sending Email.
                        Hashtable table = new Hashtable();
                        table.Add("agentName", agent.Name);
                        table.Add("hotelName", itinerary.HotelName);
                        table.Add("confirmationNo", itinerary.ConfirmationNo);

                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                        UserMaster bookedBy = new UserMaster(itinerary.CreatedBy);
                        AgentMaster bookedAgency = new AgentMaster(itinerary.AgencyId);
                        toArray.Add(bookedBy.Email);
                        toArray.Add(bookedAgency.Email1);
                        //toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
                        string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["HOTEL_CANCEL_MAIL"]).Split(';');
                        foreach (string cnMail in cancelMails)
                        {
                            toArray.Add(cnMail);
                        }
                        //string message = "Your request for cancelling hotel <b>" + itineary.HotelName + " </b> has been processed. Confirmation No:(" + itineary.ConfirmationNo + ")";
                        string message = ConfigurationManager.AppSettings["HOTEL_REFUND"];
                        try
                        {
                            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Hotel)Response for cancellation. Confirmation No:(" + itinerary.ConfirmationNo + ")", message, table);
                        }
                        catch { }

                        //TODO Ziyad
                        //Generate an Invoice for the cancellation amount.
                    }
                    else
                    {
                        if (itinerary.Source == HotelBookingSource.TBOHotel)
                        {
                            lblMessage.Text = "Your request is inprocess,kindly contact supplier.";
                        }
                        else if (itinerary.Source == HotelBookingSource.GIMMONIX && cancellationData["Status"] == "ERC")
                        {
                            lblMessage.Text = "Please contact the supplier to verify the reservation status and/or cancel it directly on the supplier system.";
                        }
                        else
                        {
                            lblMessage.Text = "Failed Cancellation from " + itinerary.Source.ToString();
                        }
                    }
                    LoadBookings();
                    txtAdminFee.Text = "0";
                    txtSupplierFee.Text = "0";
                    //Response.Redirect("ChangeRequestQueue.aspx",false);
                }
            }
            else
            {
                lblMessage.Text = "Please enter Admin & Supplier Fee";
            }
        }
    }

    protected void ddlAgents_SelectedIndexChanged(object sender, EventArgs e)
    {
        int agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
        if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
        BindB2BAgent(agentId);
        BindB2B2BAgent(agentId);
        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
        {
            ddlB2B2BAgent.Enabled = false;
        }
        string type = string.Empty;
        if (agentId == 0)
        {
            type = "BASE";
        }
        BindLocation(Utility.ToInteger(ddlAgents.SelectedItem.Value), type);
    }
    void BindLocation(int agentId, string type)
    {
        DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);

        ddlLocations.Items.Clear();
        ddlLocations.DataSource = dtLocations;
        ddlLocations.DataTextField = "location_name";
        ddlLocations.DataValueField = "location_id";
        ddlLocations.DataBind();

        ListItem item = new ListItem("All", "-1");
        ddlLocations.Items.Insert(0, item);
        hdfParam.Value = "0";
    }


}
