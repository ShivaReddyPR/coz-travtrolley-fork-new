using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using CT.Roster;

public partial class ROSTranxHeaderListGUI : CT.Core.ParentPage
{
    private string ROSTER_LIST_SESSION = "_RosterDtlList";
  

    #region Page Load
    //protected override void OnPreRender(EventArgs e)
    //{
    //    base.OnPreRender(e);

    //    if (Settings.LoginInfo.LocationLanguage == LangResource.Turkish)
    //    {
    //        LoadTurkishLabels();
    //    }
    //}

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (hdfStatusVal.Value == "C" && hdfDriverStatus.Value=="S")
        {
            foreach (GridViewRow gvRow in gvRosterList.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                DropDownList ddlDriver = (DropDownList)gvRow.FindControl("ITddlDriver");
                DropDownList ddlVehicle = (DropDownList)gvRow.FindControl("ITddlVehicle");

                ddlDriver.Enabled = ddlVehicle.Enabled = chkSelect.Checked;
                ddlDriver.CssClass = ddlVehicle.CssClass = chkSelect.Checked ? "inputDdlEnabled" : "inputDdlDisabled";

            }
        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            //VisaRefund refund= new VisaRefund();
            //refund.VsDocNo = "VS/SHJ/10244";
            //refund.GetDetails();




            lblSuccessMsg.Text = string.Empty;
            hdfParam.Value = "1";
           //Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
           ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
           scriptManager.RegisterPostBackControl(this.btnExport);
           // lblError.Visible = false;
            if (!IsPostBack)
            {
                //Label lblGrandText = (Label)this.Master.FindControl("lblGrandText");
                //Label lblGrandTotal = (Label)this.Master.FindControl("lblGrandTotal");
                //lblGrandText.Visible = true;
                //lblGrandTotal.Visible = true;
                //((UpdatePanel)this.Master.FindControl("upnlGrandTotal")).Update();
               
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                //BindGrid();
                InitializePageControls();
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
       
   }
    private void  InitializePageControls()
    {
        try
        {
            //DateControl dtControl=((DateControl)gvVisaSales.HeaderRow.FindControl("HTtxtReceiptDate"));
            //dtControl.Value=DateTime.Now;
            //string date = ((TextBox)(dtControl.FindControl("Date"))).Text;
            //Filter_Click(null, null);
            BindStaffList();
            dcFromDate.Value = Utility.ToDate(DateTime.Now.Date);
            dcToDate.Value = Utility.ToDate(DateTime.Now.Date.ToString("MMM-dd-yyyy 23:59"));

            //BindNationality();                      
             
            BindGrid(true);
            btnUpdate.Visible = false;
            gvRosterList.Columns[16].Visible = false;   //TemplateField for Assigned Driver dropdown
            gvRosterList.Columns[17].Visible = false;   //TemplateField for vehicle dropdown
            gvRosterList.Columns[19].Visible = false;  //TemplateField for Edit and delete
            //gvRosterList.Columns[14].Visible = false;
            btnSearch.BackColor = System.Drawing.Color.Gray;
        }
        catch
        { throw; }

    }
    #endregion

    # region Session
    private DataTable RosterList
    {
        get
        {
            return (DataTable)Session[ROSTER_LIST_SESSION];
        }
        set
        {
            if (value == null)
                Session.Remove(ROSTER_LIST_SESSION);
            else
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["rm_id"] };
                Session[ROSTER_LIST_SESSION] = value;
            }
        }
    }
   

   # endregion

    # region Param
    private void BindStaffList()
    {
        try
        {
            ddlStaff.DataSource = ROSEmployeeMaster.GetROSEmpMasterList(RecordStatus.Activated,ListStatus.Short, "Crew");
            ddlStaff.DataValueField = "emp_Id";
            ddlStaff.DataTextField = "emp_Name";
            ddlStaff.DataBind();
            ddlStaff.Items.Insert(0, new ListItem("--Select Employee--", "0"));
            ddlStaff.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }

    private void BindDriverList(DropDownList ddlDriver)
    {
        try
        {
            ddlDriver.DataSource = ROSEmployeeMaster.GetROSEmpMasterList(RecordStatus.Activated, ListStatus.Short, "Driver");
            ddlDriver.DataValueField = "emp_Id";
            ddlDriver.DataTextField = "emp_Name";
            ddlDriver.DataBind();
            ddlDriver.Items.Insert(0, new ListItem("--Select Driver--", "-1"));
            ddlDriver.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }

    private void BindVehicleList(DropDownList ddlVehicle)
    {
        try
        {
            ddlVehicle.DataSource = ROSVehicleMaster.GetList(RecordStatus.Activated, ListStatus.Short);
            ddlVehicle.DataValueField = "ve_id";
            ddlVehicle.DataTextField = "ve_name";
            ddlVehicle.DataBind();
            ddlVehicle.Items.Insert(0, new ListItem("--Select Vehicle--", "-1"));
            ddlVehicle.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }

    private void BindLocation(DropDownList ddlLocation, string type)
    {
        try
        {
            ddlLocation.DataSource = ROSTranxHeader.GetLocationTypewiseList(type);
            ddlLocation.DataValueField = "loc_id";
            ddlLocation.DataTextField = "loc_name";
            ddlLocation.DataBind();
            //ddlLocation.Items.Insert(0, new ListItem("--Select Location--", "0"));
            ddlLocation.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    } 
    
    //protected void btnSearch_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        BindGrid(true);
    //    }
    //    catch { throw; }
    //}

    //protected void btnOk_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //         //GridViewRow gvSeletedRow = gvVisaSales.SelectedRow;
    //         long vsId = Utility.ToLong(hdfVisaId.Value);
    //            //Utility.ToLong(((HiddenField)gvSeletedRow.FindControl("IThdfVSId")).Value);

    //        TranxVisaSales visaSales = new TranxVisaSales(vsId);
    //        visaSales.CreatedBy = Settings.LoginInfo.UserID;
    //        visaSales.DeleteRemarks = txtRemarks.Text;
    //        visaSales.Status = Settings.DELETED;
    //        visaSales.Save();
    //        Settings.LoginInfo.AgentBalance = visaSales.AgentBalance;

    //        //Deleting PNR if PNR added
    //        if (visaSales.PnrStatus == "PNR")
    //        {
    //            ReceiptMaster receipt = new ReceiptMaster(visaSales.TicketId);
    //            receipt.Remarks = receipt.Remarks+"\r\n -- Cancelled Thru Visa File #'"+visaSales.DocNumber+"'";
    //            receipt.Status = Settings.DELETED;
    //            receipt.Save();
    //        }
    //        string docNo = visaSales.DocNumber;
    //        lblSuccessMsg.Text = Formatter.ToMessage("Visa Sales No", docNo, CT.TicketReceipt.Common.Action.Deleted);

    //        BindGrid();
    //        //LoginInfo userInfo = Settings.LoginInfo;
    //        //VisaDispatchStatus dispatchStatus = (ddlDispStatus.SelectedIndex > 0) ? (VisaDispatchStatus)Convert.ToInt32(Convert.ToChar(ddlDispStatus.SelectedValue)) : VisaDispatchStatus.All;
    //        //string agentType = Settings.LoginInfo.AgentType;
    //        //if (agentType == "AGENCY" && Utility.ToInteger(ddlAgent.SelectedItem.Value) > 1)
    //        //{
    //        //    agentType = "B2B";
    //        //}

    //        //int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
    //        //if (agentId > 0 && ddlB2BAgent.SelectedIndex > 0) agentId = Utility.ToInteger(ddlB2BAgent.SelectedItem.Value);
    //        //VisaSalesDtlList=TranxVisaSales.GetDailyVisaSalesDetails(dcFromDate.Value, dcToDate.Value, Utility.ToInteger(ddlVisaType.SelectedValue),
    //        //Utility.ToInteger(ddlSalesType.SelectedValue), 0, userInfo.UserID, userInfo.MemberType.ToString(), userInfo.LocationID, agentId, dispatchStatus, string.Empty,
    //        //string.Empty, string.Empty, string.Empty, string.Empty,
    //        //string.Empty, string.Empty, string.Empty, (Utility.ToLong(ddlConsultant.SelectedValue) > 0) ? Utility.ToLong(ddlConsultant.SelectedValue) : 0,
    //        //(Utility.ToLong(ddlLocation.SelectedValue) > 0) ? Utility.ToLong(ddlLocation.SelectedValue) : 0, ListStatus.Long, ddlStatus.SelectedItem.Value == "A" ? RecordStatus.Activated : RecordStatus.Deactivated, agentType,
    //        //Utility.ToInteger(ddlAgent.SelectedItem.Value) < 0 ? Settings.LoginInfo.AgentId : Utility.ToInteger(ddlAgent.SelectedItem.Value),
    //        //ddlVisaUrgentStatus.SelectedItem.Value == "A" ? string.Empty : ddlVisaUrgentStatus.SelectedItem.Value, ddlHala.SelectedItem.Value == "A" ? string.Empty : ddlHala.SelectedItem.Value,
    //        //ddlMeetAssist.SelectedItem.Value == "A" ? string.Empty : ddlMeetAssist.SelectedItem.Value, ddlQuickCheck.SelectedItem.Value == "A" ? string.Empty : ddlQuickCheck.SelectedItem.Value).Tables[0];
            
    //        Filter_Click(null, null);
    //        txtRemarks.Text = string.Empty;
    //        hdfVisaId.Value = string.Empty;
    //    }
    //    catch (Exception ex)
    //    {
    //        Label lblMasterError = (Label)this.Master.FindControl("lblError");
    //        lblMasterError.Visible = true;
    //        lblMasterError.Text = ex.Message;
    //        Utility.WriteLog(ex, this.Title);
    //        Utility.Alert(this.Page, ex.Message);
    //    }
    //}
    # endregion

    #region Bind Grid
    private void BindGrid(bool loaded)
    {
        if (loaded)
        {
            RosterList = ROSTranxHeader.GetList(dcFromDate.Value, dcToDate.Value, Utility.ToInteger(ddlStaff.SelectedItem.Value), hdfStatusVal.Value, RecordStatus.Activated,hdfDriverStatus.Value,hdfDirection.Value);
        }
        CommonGrid g = new CommonGrid();
        g.BindGrid(gvRosterList, RosterList);
        //loadExcelData(((DataTable)gvVisaSales.DataSource).Copy());
        
    }
    private void loadExcelData(DataTable dt)
    {
        try
        {
            decimal toCollect = Settings.LoginInfo.AgentId == 1 ? Utility.ToDecimal(dt.Compute("SUM( vs_base_to_collect)", "")):Utility.ToDecimal(dt.Compute("SUM(VS_TO_COLLECT)", ""));
            decimal ticketFare= Utility.ToDecimal(dt.Compute("SUM(VS_TICKET_FARE)", ""));
            decimal totalVisaCost = toCollect - ticketFare;
            //decimal totalFare = Utility.ToDecimal(dt.Compute("SUM(VISA_COST)", ""));
            decimal totalCashDep = Utility.ToDecimal(dt.Compute("SUM(VS_CHEQUE_AMOUNT)", "VS_DEPOSIT_MODE='CASH'"));
            //decimal totalCashCollection = totalFare + totalCashDep; 
            //decimal totalCashCollection = totalVisaCost; 
            Label lblGrandText = (Label)this.Master.FindControl("lblGrandText");
            Label lblGrandTotal = (Label)this.Master.FindControl("lblGrandTotal");
            //string grandTotal = Utility.ToString(((DataTable)(gvVisaSales.DataSource)).Copy().Compute("SUM(VISA_COST)", ""));
            string grandTotal = Utility.ToString(totalVisaCost);
            lblGrandTotal.Text = string.Format(" {0} {1}", (string.IsNullOrEmpty(grandTotal)) ? Formatter.ToCurrency(0) : Formatter.ToCurrency(grandTotal),Settings.LoginInfo.Currency );

            //DataTable dt = ((DataTable)gvVisaSales.DataSource).Copy();
            

            DataRow dr = dt.NewRow();
            dr["VISA_COST"] = Formatter.ToCurrency(totalVisaCost);
            dr["PAX_ID"] = -1;
            dr["vsd_pax_status"] = "GRAND TOTAL :";
            dt.Rows.Add(dr);
            Session["ExportExcelVisalist"] = dt;
            lblGrandText.Visible = true;
            lblGrandTotal.Visible = true;
            ((UpdatePanel)this.Master.FindControl("upnlGrandTotal")).Update();
        }
        catch { throw; }
    }
    #endregion

    #region gvVisaSales Events
    
    //protected void gvVisaSales_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    try
    //    {
    //        gvVisaSales.PageIndex = e.NewPageIndex;
    //        gvVisaSales.EditIndex = -1;
    //        Filter_Click(null, null);
    //    }
    //    catch (Exception ex)
    //    {
    //        Label lblMasterError = (Label)this.Master.FindControl("lblError");
    //        lblMasterError.Visible = true;
    //        lblMasterError.Text = ex.Message;
    //        Utility.WriteLog(ex, this.Title);
    //    }

    //}

    protected void Filter_Click(object sender, EventArgs e)
    {
        try
        {
            selectedItem();
            FilterControls();
           // loadExcelData(((DataTable)gvRosterList.DataSource).Copy());
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void FilterControls()
    {

        try
        {

            string[,] textboxesNColumns ={{"HTtxtLocation","FROM_LOC_NAME"},{ "HTtxtTripId", "RM_ID" },{"HTtxtStaffId", "STAFF_ID" }, { "HTtxtStaffName", "EMP_NAME" }, { "HTtxtFlightNo", "RM_FLIGHTNO" },
            { "HTtxtFromLoc", "FROM_LOC" },{ "HTDate", "RM_DATE" },{ "HTTime", "RM_TIME" },{ "HTtxtToLoc", "TO_LOC" },{ "HTtxtFromSector", "RM_FROM_SECTOR" },
            { "HTtxtToSector", "RM_TO_SECTOR" },{ "HTtxtDriverName", "DRIVER_NAME" },{ "HTtxtVehicleName", "ROS_VEHICLE_NO" },{"HTtxtTripStatus","ROS_TRIP_STATUS_NAME"},
            {"HTtxtDriverStatus","ROS_DRIVER_STATUS_NAME"}};
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvRosterList, RosterList.Copy(), textboxesNColumns);

        }
        catch { throw; }
    }
    #endregion

    # region Report
        
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            FilterControls();
            DataTable dt = ((DataTable)gvRosterList.DataSource).Copy();
            string attachment = "attachment; filename=RosterList.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dgRosterList.AllowPaging = false;
            dgRosterList.DataSource = dt;
            dgRosterList.DataBind();
            dgRosterList.RenderControl(htw);
            Response.Write(sw.ToString());
            //string script = "window.open('ExportExcelVisaList.aspx?','','width=0,height=0,toolbar=yes,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            //Utility.StartupScript(this.Page, script, "Excel");

        }
        catch(Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
        finally
        {
            Response.End();
        }
    }

    private  void StartupScript(Page page, string script, string key)
    {

        script = string.Format("{0};", script);
        if (page != null && !string.IsNullOrEmpty(script) && !page.ClientScript.IsStartupScriptRegistered(key))
        {
            if (ScriptManager.GetCurrent(page) != null && ScriptManager.GetCurrent(page).IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(page, page.GetType(), key, script, true);
            else
                page.ClientScript.RegisterStartupScript(page.GetType(), key, script, true);

        }
    }
    protected void gvRosterList_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Display the company name in italics.
            DropDownList ddlDriver = (DropDownList)e.Row.FindControl("ITddlDriver");
            DropDownList ddlVehicle = (DropDownList)e.Row.FindControl("ITddlVehicle");
            HiddenField hdfDriver = (HiddenField)e.Row.FindControl("IThdfDriver");
            HiddenField hdfVehicle = (HiddenField)e.Row.FindControl("IThdfVehicle");
            CheckBox chkSelect=(CheckBox)e.Row.FindControl("ITchkSelect");
            BindDriverList(ddlDriver);
            BindVehicleList(ddlVehicle);
            ddlDriver.SelectedValue = hdfDriver.Value;
            ddlVehicle.SelectedValue = hdfVehicle.Value;

            //gvRosterList.Columns[16].Visible = false;
            //gvRosterList.Columns[17].Visible = false;
            chkSelect.Visible = false;

            if (hdfStatusVal.Value == "P")//Pending
            {
                gvRosterList.Columns[16].Visible = true ;
                gvRosterList.Columns[17].Visible = true ;
                gvRosterList.Columns[19].Visible = true;
            }
            else if (hdfStatusVal.Value == "C" && hdfDriverStatus.Value == "N") //Confirmed
            {
                gvRosterList.Columns[16].Visible = true;
                gvRosterList.Columns[17].Visible = true;
            }
            else if (hdfStatusVal.Value == "C" &&  hdfDriverStatus.Value == "S")//Assigned
            {
                chkSelect.Visible = true;
                Utility.StartupScript(this.Page, "enableGridControls('" + chkSelect.ClientID + "')", "enableGridControls");
                gvRosterList.Columns[16].Visible = true;
                gvRosterList.Columns[17].Visible = true;
            }
            else if ( hdfDriverStatus.Value == "P" || hdfDriverStatus.Value == "X")// Picked up or Completed
            {                
                gvRosterList.Columns[16].Visible = false;
                gvRosterList.Columns[17].Visible = false;
                gvRosterList.Columns[14].Visible = true; //TemplateField for Assigned Driver Label
                gvRosterList.Columns[15].Visible = true;//TemplateField for vehicle Label
            }

        }

    }

    protected void gvRosterList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            selectedItem();
            gvRosterList.PageIndex = e.NewPageIndex;
            gvRosterList.EditIndex = -1;
            //BindGrid(false);
            Filter_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }

    protected void gvRosterList_RowEditing(object sender, GridViewEditEventArgs e)
    {

        try
        {
            gvRosterList.EditIndex = e.NewEditIndex;
            BindGrid(false);

            gvRosterList.FooterRow.Visible = false;

            long serial = Utility.ToLong(gvRosterList.DataKeys[e.NewEditIndex].Value);
            //hdfFreeAccessoriesSerial.Value = Utility.ToString(serial);
            DataRow dr = RosterList.Rows.Find(serial);

            GridViewRow gvRow = gvRosterList.Rows[e.NewEditIndex];

            HiddenField hdfFromLocType = (HiddenField)gvRow.FindControl("EIThdfFromLocType");           
            DropDownList ddlFromLocation = (DropDownList)gvRow.FindControl("EITddlFromLocation");           
            BindLocation(ddlFromLocation, hdfFromLocType.Value);
            ddlFromLocation.SelectedValue = Utility.ToString(dr["FROM_LOCID_WITH_NAME"]);

            HiddenField hdfToLocType = (HiddenField)gvRow.FindControl("EIThdfToLocType");
            DropDownList ddlToLocation = (DropDownList)gvRow.FindControl("EITddlToLocation");
            BindLocation(ddlToLocation, hdfToLocType.Value);
            ddlToLocation.SelectedValue = Utility.ToString(dr["TO_LOCID_WITH_NAME"]);

            DateControl dcDate = (DateControl)gvRow.FindControl("EITdcDate");
            if (string.IsNullOrEmpty(Utility.ToString(dr["RM_DATE"])))
            {
                dcDate.Clear();
            }
            else
                dcDate.Value = Utility.ToDate(dr["RM_DATE"]);

            DateControl dcTime = (DateControl)gvRow.FindControl("EITdcTime");
            if (string.IsNullOrEmpty(Utility.ToString(dr["RM_TIME"])))
            {
                dcTime.Clear();
            }
            else
                dcTime.Value = Utility.ToDate(dr["RM_TIME"]);

            //DateControl dcTravelDate = (DateControl)gvRow.FindControl("EITdcTravelOn");
            //if (string.IsNullOrEmpty(Utility.ToString(dr["pax_travel_on"])) || (Utility.ToDate(dr["pax_travel_on"]) == DateTime.MinValue))
            //{
            //    dcTravelDate.Clear();
            //}
            //else
            //     dcTravelDate.Value = Utility.ToDate(dr["pax_travel_on"]);
            //((AC.AutoComplete)gvRow.FindControl("EITacAccessories")).Text = Utility.ToString(dr["raa_accessory_name"]);
           // FilterSearchGrid();
            //gvVisaDetails.FooterRow.Visible = false;
            hdfVisaDetailsMode.Value = "2";
            gvRosterList.Columns[16].Visible=false ;
            gvRosterList.Columns[17].Visible=false ;
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    protected void gvRosterList_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvRosterList.Rows[e.RowIndex];

            HiddenField hdfRMId = (HiddenField)gvRow.FindControl("EIThdfRMId");
            TextBox txtFlightNo = (TextBox)gvRow.FindControl("EITtxtFlightNo");
            TextBox txtFromSector = (TextBox)gvRow.FindControl("EITtxtFromSector");
            TextBox txtToSector = (TextBox)gvRow.FindControl("EITtxtToSector");

            DropDownList ddlFromLoc = (DropDownList)gvRow.FindControl("EITddlFromLocation");
            TextBox txtfromLocDetails = (TextBox)gvRow.FindControl("EITtxtFromLocDetails");
            HiddenField hdfromLocId = (HiddenField)gvRow.FindControl("EIThdfFromLocId");

            DropDownList ddlToLoc = (DropDownList)gvRow.FindControl("EITddlToLocation");
            TextBox txtToLocDetails = (TextBox)gvRow.FindControl("EITtxtToLocDetails");
            HiddenField hdfToLocId = (HiddenField)gvRow.FindControl("EIThdfToLocId");

            DateControl rmDate = (DateControl)gvRow.FindControl("EITdcDate");
            DateControl rmTime = (DateControl)gvRow.FindControl("EITdcTime");

            TextBox txtRemarks = (TextBox)gvRow.FindControl("EITtxtRemarks");
            //long serial = Utility.ToLong(gvRosterList.DataKeys[e.RowIndex].Value);
            //DataRow dr = RosterList.Rows.Find(serial);
            long rmId = Utility.ToLong(hdfRMId.Value);
            string flightNo= Utility.ToString(txtFlightNo.Text.Trim());
            string fromSector= Utility.ToString(txtFromSector.Text.Trim());
            string toSector= Utility.ToString(txtToSector.Text.Trim());
            int fromLocId = Utility.ToInteger(hdfromLocId.Value);
            string fromLocDetails= Utility.ToString(txtfromLocDetails.Text.Trim());
             int toLocId = Utility.ToInteger(hdfToLocId.Value);
            string toLocDetails= Utility.ToString(txtToLocDetails.Text.Trim());
            string remarks = txtRemarks.Text.Trim();
            DateTime rmDateVal =Utility.ToDate(rmDate.Value.ToString("dd-MM-yyyy") + " " + rmTime.Value.ToString("HH:mm"));
            ROSTranxHeader.UpdateROSTranxStatus(rmId, 0, "P", 0, string.Empty, 0, string.Empty, Settings.LoginInfo.UserID, "Y", flightNo, fromSector, toSector,
                            fromLocId, string.Empty, string.Empty, fromLocDetails, toLocId, string.Empty, string.Empty, toLocDetails, rmDateVal, remarks);
          
            gvRosterList.EditIndex = -1;
            BindGrid(true);
            hdfVisaDetailsMode.Value = "0";
            //FilterSearchGrid();
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            // Utility.Alert(this.Page, ex.Message);
            Utility.StartupScript(this.Page, "ShowMessageDialog('gvRosterList_RowUpdating','" + ex.Message + "','Information')", "gvRosterList_RowUpdating");
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    protected void gvRosterList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        try
        {
            gvRosterList.EditIndex = -1;
            BindGrid(false);
            hdfVisaDetailsMode.Value = "0";
            // FilterSearchGrid();
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;

        }
    }

    protected void gvRosterList_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {

            GridViewRow gvRow = gvRosterList.Rows[e.RowIndex];
            long serial = Utility.ToLong(gvRosterList.DataKeys[e.RowIndex].Value);
            ROSTranxHeader.DeleteROSTranxHeader(serial, Utility.ToInteger(Settings.LoginInfo.UserID));
            BindGrid(true);
        }
        catch(Exception ex)
        {

            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    # endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
             
            hdfStatusVal.Value = string.Empty;
            hdfDriverStatus.Value = string.Empty;
            hdfDirection.Value = string.Empty;
            BindGrid(true);
            btnSearch.BackColor = System.Drawing.Color.Gray;
            btnPending.BackColor = System.Drawing.Color.Empty;
            btnConfirmed.BackColor = System.Drawing.Color.Empty;
            btnAssigned.BackColor = System.Drawing.Color.Empty;
            btnCompleted.BackColor = System.Drawing.Color.Empty;
            btnPickedUp.BackColor = System.Drawing.Color.Empty;
            btnAssignedA.BackColor = System.Drawing.Color.Empty;
            btnNotResp.BackColor = System.Drawing.Color.Empty;
            //btnPending.Enabled = false;
            btnUpdate.Visible = false;
            //gvRosterList.Columns[11].Visible = false;
            //gvRosterList.Columns[12].Visible = false;
            //gvRosterList.Columns[18].Visible = false;
            
            //chkSelect.Visible = false;


        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void btnPending_Click(object sender, EventArgs e)
    {
        try
        {
            
            hdfStatusVal.Value = "P";
            hdfDriverStatus.Value = "N";
            hdfDirection.Value = string.Empty;
            BindGrid(true);
            btnSearch.BackColor = System.Drawing.Color.Empty;
            btnPending.BackColor = System.Drawing.Color.Gray;
            btnConfirmed.BackColor = System.Drawing.Color.Empty;
            btnAssigned.BackColor = System.Drawing.Color.Empty;
            btnAssignedA.BackColor = System.Drawing.Color.Empty;
            btnCompleted.BackColor = System.Drawing.Color.Empty;
            btnPickedUp.BackColor = System.Drawing.Color.Empty;
            btnNotResp.BackColor = System.Drawing.Color.Empty;
            //btnPending.Enabled = false;
            btnUpdate.Visible=false ;
            //gvRosterList.Columns[11].Visible = false;
            //gvRosterList.Columns[12].Visible = false;
            //gvRosterList.Columns[18].Visible = true;

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void btnConfirmed_Click(object sender, EventArgs e)
    {
        try
        {

            hdfStatusVal.Value = "P"; //Passing P to get Both pending confirmed tranxs(Handled in Prcedure)
            hdfDriverStatus.Value = "N";
            hdfDirection.Value = string.Empty;
            BindGrid(true);
            btnSearch.BackColor = System.Drawing.Color.Empty;
            btnPending.BackColor = System.Drawing.Color.Empty;
            btnConfirmed.BackColor = System.Drawing.Color.Gray;
            btnAssigned.BackColor = System.Drawing.Color.Empty;
            btnAssignedA.BackColor = System.Drawing.Color.Empty;
            btnCompleted.BackColor = System.Drawing.Color.Empty;
            btnPickedUp.BackColor = System.Drawing.Color.Empty;
            btnNotResp.BackColor = System.Drawing.Color.Empty;
            //btnUpdate.Enabled = true;
            btnUpdate.Visible = true;
            //gvRosterList.Columns[11].Visible = true;
            //gvRosterList.Columns[12].Visible = true;
            //gvRosterList.Columns[18].Visible = true ;
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void btnAssigned_Click(object sender, EventArgs e)
    {
        try
        {
            hdfStatusVal.Value = "C";
            hdfDriverStatus.Value = "S";
            hdfDirection.Value = "H";
            BindGrid(true);
            btnSearch.BackColor = System.Drawing.Color.Empty;
            btnPending.BackColor = System.Drawing.Color.Empty;
            btnConfirmed.BackColor = System.Drawing.Color.Empty;
            btnAssigned.BackColor = System.Drawing.Color.Gray;
            btnAssignedA.BackColor = System.Drawing.Color.Empty;
            btnCompleted.BackColor = System.Drawing.Color.Empty;
            btnPickedUp.BackColor = System.Drawing.Color.Empty;
            btnNotResp.BackColor = System.Drawing.Color.Empty;
            //btnUpdate.Enabled = true ;
            btnUpdate.Visible = true;
            
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void btnAssignedA_Click(object sender, EventArgs e)
    {
        try
        {
            hdfStatusVal.Value = "C";
            hdfDriverStatus.Value = "S";
            hdfDirection.Value = "A";
            BindGrid(true);
            btnSearch.BackColor = System.Drawing.Color.Empty;
            btnPending.BackColor = System.Drawing.Color.Empty;
            btnConfirmed.BackColor = System.Drawing.Color.Empty;
            btnAssigned.BackColor = System.Drawing.Color.Empty;
            btnAssignedA.BackColor = System.Drawing.Color.Gray;
            btnCompleted.BackColor = System.Drawing.Color.Empty;
            btnPickedUp.BackColor = System.Drawing.Color.Empty;
            btnNotResp.BackColor = System.Drawing.Color.Empty;
            //btnUpdate.Enabled = true ;
            btnUpdate.Visible = true;

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }



    protected void btnPickedUp_Click(object sender, EventArgs e)
    {
        try
        {
            hdfStatusVal.Value = "C"; //confirmed
            hdfDriverStatus.Value = "P"; //Picked Up
            hdfDirection.Value = string.Empty;
            BindGrid(true);
            btnSearch.BackColor = System.Drawing.Color.Empty;
            btnPending.BackColor = System.Drawing.Color.Empty;
            btnConfirmed.BackColor = System.Drawing.Color.Empty;
            btnAssigned.BackColor = System.Drawing.Color.Empty;
            btnAssignedA.BackColor = System.Drawing.Color.Empty;
            btnCompleted.BackColor = System.Drawing.Color.Empty ;
            btnPickedUp.BackColor = System.Drawing.Color.Gray;
            btnNotResp.BackColor = System.Drawing.Color.Empty;            
            //btnUpdate.Enabled = false;
            btnUpdate.Visible = false;
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void btnNotResp_Click(object sender, EventArgs e)
    {
        try
        {
            
            hdfStatusVal.Value = "C"; //confirmed
            hdfDriverStatus.Value = "R"; //Customer Not Responding
            hdfDirection.Value = string.Empty;
            BindGrid(true);
            btnSearch.BackColor = System.Drawing.Color.Empty;
            btnPending.BackColor = System.Drawing.Color.Empty;
            btnConfirmed.BackColor = System.Drawing.Color.Empty;
            btnAssigned.BackColor = System.Drawing.Color.Empty;
            btnAssignedA.BackColor = System.Drawing.Color.Empty;
            btnCompleted.BackColor = System.Drawing.Color.Empty;
            btnPickedUp.BackColor = System.Drawing.Color.Empty;
            btnNotResp.BackColor = System.Drawing.Color.Gray;            
            //btnUpdate.Enabled = false;
            btnUpdate.Visible = false;
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }


    protected void btnCompleted_Click(object sender, EventArgs e)
    {
        try
        {
            hdfStatusVal.Value = "C"; //confirmed
            hdfDriverStatus.Value = "C"; //completed
            hdfDirection.Value = string.Empty;
            BindGrid(true);
            btnPending.BackColor = System.Drawing.Color.Empty;
            btnConfirmed.BackColor = System.Drawing.Color.Empty;
            btnAssigned.BackColor = System.Drawing.Color.Empty;
            btnAssignedA.BackColor = System.Drawing.Color.Empty;
            btnCompleted.BackColor = System.Drawing.Color.Gray ;
            btnPickedUp.BackColor = System.Drawing.Color.Empty;
            btnNotResp.BackColor = System.Drawing.Color.Empty;
            btnSearch.BackColor = System.Drawing.Color.Empty;
            //btnUpdate.Enabled = false;
            btnUpdate.Visible = false;
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {            
            Save();
            BindGrid(true);
            btnPending.BackColor = System.Drawing.Color.Empty;            
            btnConfirmed.BackColor = System.Drawing.Color.Empty;
            btnAssigned.BackColor = System.Drawing.Color.Empty;
            btnCompleted.BackColor = System.Drawing.Color.Empty;
            btnPickedUp.BackColor = System.Drawing.Color.Empty;

            if (hdfDriverStatus.Value == "N")
                btnConfirmed.BackColor = System.Drawing.Color.Gray;
            else if (hdfDriverStatus.Value == "S")
            {
                if(hdfDirection.Value =="H")
                    btnAssigned.BackColor = System.Drawing.Color.Gray;
                else
                    btnAssignedA.BackColor = System.Drawing.Color.Gray;
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }


    private void selectedItem()
    {
        try
        {
            foreach (GridViewRow gvRow in gvRosterList.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                HiddenField hdfRmId = (HiddenField)gvRow.FindControl("IThdfRMId");
                DropDownList ddlDriver = (DropDownList)gvRow.FindControl("ITddlDriver");
                DropDownList ddlVehicle = (DropDownList)gvRow.FindControl("ITddlVehicle");               
                
                
                foreach (DataRow dr in RosterList.Rows)
                {
                    if (dr != null)
                    {
                        dr.BeginEdit();
                        if (Utility.ToString(dr["rm_id"]) == hdfRmId.Value)
                        {
                            if (hdfStatusVal.Value == "C" || hdfStatusVal.Value == "P")//first time assign driver and vehicle
                            {
                                if (Utility.ToInteger(ddlDriver.SelectedItem.Value) > 0 && Utility.ToInteger(ddlVehicle.SelectedItem.Value) > 0)
                                {
                                    dr["ROS_DRIVER_ID"] = Utility.ToInteger(ddlDriver.SelectedItem.Value);
                                    dr["VEHICLE_ID"] = Utility.ToInteger(ddlVehicle.SelectedItem.Value);
                                    dr["ROS_VEHICLE_NO"] = ddlVehicle.SelectedItem.Text;
                                    hdfAssignedCount.Value = Utility.ToString(Utility.ToInteger(hdfAssignedCount.Value) + 1);
                                }
                            }
                            else if (hdfStatusVal.Value == "S" && chkSelect.Checked)//if edit the driver or vehicle
                            {
                                if (Utility.ToInteger(ddlDriver.SelectedItem.Value) > 0 || Utility.ToInteger(ddlVehicle.SelectedItem.Value) > 0)
                                {
                                    dr["checked_status"] = chkSelect.Checked ? "true" : "false";
                                    dr["ROS_DRIVER_ID"] = Utility.ToInteger(ddlDriver.SelectedItem.Value);
                                    dr["VEHICLE_ID"] = Utility.ToInteger(ddlVehicle.SelectedItem.Value);
                                    dr["ROS_VEHICLE_NO"] = ddlVehicle.SelectedItem.Text;
                                    hdfAssignedCount.Value = Utility.ToString(Utility.ToInteger(hdfAssignedCount.Value) + 1);
                                }
                            }
                            
                        }
                        dr.EndEdit();
                    }
                }
            }

        }
        catch { throw; }
    }

    private void Save()
    {
        try
        {
            selectedItem();
            if (Utility.ToInteger(hdfAssignedCount.Value) <= 0)
                throw new Exception("Driver and Vehicle are not seleted for any of the Trip !");

            foreach (DataRow dr in RosterList.Rows)
            {
                if (dr != null)
                {
                    if (Utility.ToInteger(dr["ROS_DRIVER_ID"]) > 0 && Utility.ToInteger(dr["VEHICLE_ID"]) > 0)
                    {
                        long rmId = Utility.ToLong(dr["rm_id"]);
                        string tranxStatus = "C";//C -still confirmed.   S- Assigned-- UPDATING ONLY DRIVER STATUS
                        int driverId = Utility.ToInteger(dr["ROS_DRIVER_ID"]);
                        int vehicleId = Utility.ToInteger(dr["VEHICLE_ID"]);
                        string vehicleNo = Utility.ToString(dr["ROS_VEHICLE_NO"]);
                        int employeeId = Utility.ToInteger(dr["RM_EMPID"]);

                        ROSTranxHeader.UpdateROSTranxStatus(rmId, employeeId, tranxStatus, driverId, vehicleNo, vehicleId, "S", Settings.LoginInfo.UserID,
                            "N",string.Empty,string.Empty,string.Empty,0,string.Empty,string.Empty,string.Empty ,0,string.Empty,string.Empty,string.Empty,DateTime.MinValue,string.Empty);

                    }                
                }
            }
           
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = "Roster Details Updated Successfully !";
            hdfAssignedCount.Value = "0";
        }
        catch { throw; }
    }

    #region Date Format
    protected string CZDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }

    protected string CZTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("hh:MM");
        }
    }
    #endregion
}
