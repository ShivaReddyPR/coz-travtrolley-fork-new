<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="VisaNationalityList" Title="Untitled Page" Codebehind="VisaNationalityList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">




        <div> <h4>Visa Nationality List</h4> </div>
   
        <div class="col-md-12 padding-bottom-10"> 
    
    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="fcol_blue pull-right" 
                    NavigateUrl="~/AddVisaNationality.aspx">Add New Nationality 
            </asp:HyperLink >
            
            
    
    
    
    
    </div>
    
    
    
       
                <div>
                    <asp:Label ID="lbl_msg" runat="server" Text=""></asp:Label>
                </div>
                <div style="width:100%; ">
             
            
                <asp:GridView ID="GridView1" CssClass="datagrid" runat="server" AutoGenerateColumns="False" CellPadding="2"
                    Width="100%" DataKeyNames="nationalityId" ForeColor="#333333" GridLines="None" 
                    OnRowDataBound="GridView1_RowDataBound"
                    OnRowCommand="GridView1_RowCommand" AllowPaging="True" HorizontalAlign="Center" 
                    OnPageIndexChanging="GridView1_PageIndexChanging" AllowSorting="True">
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="nationalityId" HeaderText="ID" ReadOnly="True">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="nationalityName" HeaderText="Name">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="nationalityCode" HeaderText="Nationality Code">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        
                        
                        <asp:BoundField DataField="isActive" HeaderText="Is Active" ReadOnly="True">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:HyperLinkField Text="Edit" DataNavigateUrlFields="nationalityId" HeaderText="Edit"
                            DataNavigateUrlFormatString="AddVisaNationality.aspx?id={0}" >
                           
                        </asp:HyperLinkField>
                        <asp:TemplateField HeaderText="Activate/Deactivate">
                            <HeaderStyle HorizontalAlign="left" />
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:LinkButton ID="linkButtonStatus" runat="server"  Text="Activate"
                                    CommandName="ChangeStatus">
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                               <RowStyle />
        <EditRowStyle />
        <SelectedRowStyle  Font-Bold="True" />
        <PagerStyle HorizontalAlign="Left" Font-Bold="True" />
        <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
         <AlternatingRowStyle CssClass="altrow" />
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
            NextPageText="Next" PreviousPageText="Previous" />
    </asp:GridView>
              
                
                </div>
          

</asp:Content>

