﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Visa;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;

public partial class VisaCountryList : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        Page.Title = "Visa Country List";
        AuthorizationCheck();
        if (!IsPostBack)
        {

            ddlRegion.DataSource = VisaRegion.GetActiveRegionList();
            ddlRegion.DataTextField = "regionName";
            ddlRegion.DataValueField = "regionId";
            ddlRegion.DataBind();
            ddlRegion.Items.Insert(0, new ListItem("Select Region", "-1"));
            if (Request.QueryString.Count > 0 && Request.QueryString[0].Trim() != "")
            {
                ddlRegion.SelectedIndex = ddlRegion.Items.IndexOf(ddlRegion.Items.FindByValue(Request.QueryString[0].Trim()));
            }
            else
            {
                ddlRegion.SelectedIndex = 1;
            }
            BindData();

        }
    }

    private void BindData()
    {

        int regionId = Convert.ToInt32(ddlRegion.SelectedValue);
        List<VisaCountry> allCountry = VisaCountry.GetCountryList(regionId);
        if (allCountry == null || allCountry.Count == 0)
        {
            lbl_msg.Text = "There are no record.";
            lbl_msg.Visible = true;
        }
        else
        {
            lbl_msg.Visible = false;
        }
        GridView1.DataSource = allCountry;
        GridView1.DataBind();

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lb = ((LinkButton)e.Row.Cells[4].Controls[1]);
            lb.CommandArgument = GridView1.DataKeys[e.Row.RowIndex].Value.ToString();
            if (e.Row.Cells[2].Text == "True")
            {
                lb.Text = "Deactivate";
                e.Row.Cells[2].Text = "Active";
            }
            else
            {
                lb.Text = "Activate";
                e.Row.Cells[2].Text = "Inactive";
            }
        }
    }


    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChangeStatus")
        {
            bool isActive;
            string status = ((LinkButton)e.CommandSource).Text;
            int rowNumber = Convert.ToInt32(e.CommandArgument);
            // int i = Convert.ToInt32(GridView1.DataKeys[rowNumber].Value);
            if (status == "Activate")
            {
                isActive = true;
            }
            else
            {
                isActive = false;
            }
            VisaCountry.ChangeStatus(rowNumber, isActive);
            BindData();


        }

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();

    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
   
}
