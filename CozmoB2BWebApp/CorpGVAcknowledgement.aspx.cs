﻿using CT.TicketReceipt.BusinessLayer;
using System;
using CT.TicketReceipt.Common;
using CT.GlobalVisa;
using CT.Core;
using System.IO;

public partial class CorpGVAcknowledgementGUI : CT.Core.ParentPage// System.Web.UI.Page
{
    protected GVVisaSale visaSale = new GVVisaSale();
    string ipAddr = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["GVSession"] = null;
        try
        {
            if (Settings.LoginInfo == null) //Checking Logininfo
            {
                Response.Redirect("AbandonSession.aspx", false);
                return;
            }
            if (Request["gvId"] != null)
            {
                long visaId = Utility.ToLong(Request["gvId"]);
                visaSale = new GVVisaSale(visaId);
              
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Error message :" + ex.Message, ipAddr);
        }
        finally
        {
            if (Settings.LoginInfo.IsCorporate != "Y")
            {
                DeleteSubFolder(Server.MapPath("~/DocumentsUpload/ "));
                deleteImages(Server.MapPath("~/Thumbnails/ "));
            }           
        }
    }
    private static void DeleteSubFolder(string rootPath)
    {
        try
        {
            DirectoryInfo rootFolder = new DirectoryInfo(rootPath);
            DirectoryInfo[] subfolders = rootFolder.GetDirectories();

            foreach (DirectoryInfo myItem in subfolders)
            {                
                    Directory.Delete(myItem.FullName,true);                
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.Message, "");
        }

    }
    public static void deleteImages(string rootpaths)
    {
        try
        {
            DirectoryInfo rootFolder = new DirectoryInfo(rootpaths);
            DirectoryInfo[] subfolders = rootFolder.GetDirectories();
            foreach (DirectoryInfo dir in subfolders)
            {
                FileInfo[] files = dir.GetFiles();
                if (files.Length > 0)
                {
                    foreach (FileInfo file in files)
                    {
                        file.Delete();
                    }
                }
            }
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.Message, "");
        }        
    }
}