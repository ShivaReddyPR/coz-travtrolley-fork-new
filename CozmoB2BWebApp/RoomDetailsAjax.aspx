﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="RoomDetailsAjax" Codebehind="RoomDetailsAjax.aspx.cs" %>
<link href="css/BookingStyle.css" rel="stylesheet" type="text/css" />
<link href="css/popup_box.css" rel="stylesheet" type="text/css" />
<%if(request != null && resultObj != null){ %>
<input type='hidden' name='hCode' value='<%=index %>' />
<input type='hidden' name='hCurrency<%=index %>' id='hCurrency<%=index %>' value='<%=resultObj.Currency %>' />
<input type='hidden' name='hROE<%=index %>' id='hROE<%=index %>' value='3.67' />


<div>
   
   
        <div class="window_title"><%=resultObj.HotelName %></div>
    
    
    

    <div id='middle-container-<%=index %>'>
     
     
     <div> 
     
     <div class="col-md-8"> 
     
     
      <div id='selectRoomDiv' runat="server">
            <div class=''>
                <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                    <tr>
                        <td valign='top'>
                            <div>
                                <table width='100%' height='100%' border='0' cellpadding='0' cellspacing='0'
                                    class='selectRoom' >
                                    <span id="tblRooms" runat="server"></span>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                </table>
            </div>
        </div>
     
     </div>
     
     <div class="col-md-4"> 
     
     <div class=''>
            <div style="text-align:center; padding: 10px 0px 10px 0px">
                <a href="javascript:HideMRPopUp('<%=index %>')"><b>Choose other Hotel</b></a></div>
            <div class='SelectRoomWidget'>
                <div class='checkInDate'>
                   <center>  <b>Check-In </b><%=request.StartDate.ToString("dd MMM yyyy") %></center>
                </div>
                <div class='checkInDate'>
                    <center> <b>Check-Out </b>
                    <%=request.EndDate.ToString("dd MMM yyyy")%></center>
                </div>
                <div class='checkInDate'>
                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                            <td width='47%' align='right'>
                                <b>Total Rate</b>
                            </td>
                            <td width='53%' align='left'>
                                <table width='80%'>
                                    <tr>
                                        <td style='width: 50%' align='right'>
                                            &nbsp<b><%=resultObj.Currency %></b>&nbsp
                                        </td>
                                        <td align='left' style='width: 50%'>
                                            <div id='TotalSelectedFare<%=index %>' style='width: 50%'>
                                                <%=Math.Ceiling(totFare ).ToString("N"+decimalValue.ToString())%></div>
                                        </td>
                                    </tr>
                                    
                                </table>
                            </td>
                        </tr>                                     
                  <%--   //After discussing with vinay and Ziyad ,commented by harish for disable the discount in Normal flow    <%if(discount > 0){ %>
                        <tr>
                            <td width='50%' align='right'>
                                <b>Discount</b>
                            </td>
                            <td width='50%' align='left'>                            
                                <table width='80%'>
                                    <tr>
                                        <td style='width: 50%' align='right'>
                                            <b><%=resultObj.Currency %></b>&nbsp
                                        </td>
                                        <td align='right' style='width: 50%'>
                                            <div id='DiscountFare<%=index %>' style='width: 50%'>
                                                <%=(discount).ToString("N" + decimalValue.ToString())%></div>
                                        </td>
                                    </tr>
                                    
                                </table>
                            </td>
                        </tr>
                        <%} %>--%>
                       
                    </table>
                </div>
                <div  class='checkInDate'>
                <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                 <tr>
                            <td width='50%' align='right'>
                                <b>Grand Total</b>
                            </td>
                            <td width='50%' align='left'>
                                <table width='80%'>
                                    <tr>
                                        <td style='width: 50%' align='right'>
                                            &nbsp<b><%=resultObj.Currency %></b>&nbsp
                                        </td>
                                        <td align='left' style='width: 50%'>
                                            <div id='TotalFare<%=index %>' style='width: 50%'>                                                              
                                              <%--  <%=Math.Ceiling(Math.Ceiling(totFare) - discount).ToString("N" + decimalValue.ToString())%>--%>
                                                <%=Math.Ceiling(Math.Ceiling(totFare)).ToString("N" + decimalValue.ToString())%>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                </table>
                </div>
                <div class='padbot10 bortopblue'>
                    <a class='buttonBookNow' id='continueButton<%=index %>' alt='continue' onclick="submitForm('<%=index %>','<%=resultObj.RoomDetails.Length%>','<%=resultObj.HotelCode%>')" />
                    <b>Book Now</b></a>
                    <br />
                   
                    </div>
                    
                  
                    
            </div>
             <!-- @@@@ -->
             <div style="margin-top:2px; text-align:center">
              <img style=" cursor: pointer " src="Images/Y.ico"  />
             <a style="color:#3A7CC6; line-height:30px; font-weight:bold; cursor:pointer" onclick="ShowHotelEmailDiv()">Email Itinerary</a>
              <%--<span id="hideDiv" style="display:none; color:#3A7CC6; font-weight:bold; cursor:pointer" onclick="HideEmailDiv()">Hide Mail</span></div>--%>
             <input type="hidden" id="productName" value='Hotel Details' />
           
           
           
           
           
             <div id="emailBlock"  style="position:absolute;  top:10px;  display:none; font-weight:bold;  z-index:9999; width: 220px;">
              
             
             
             <div style=" background:#fff!important; width:220px; border: solid 1px #ccc;"> 
              
               
                            <div style="display:none;" id="error"> </div>
                       
                        
                        
                        
                        <div class="showMsgHeading">Email Itinerary 
                        
                        
                        <a onclick="HideEmailDiv('emailBlock')" href="#" class="closex" style="position:absolute; top: 3px; right:10px">X</a>
                        
                        <%--<label style="position: absolute; top:6px; right:12px;" >
         <img style=" cursor: pointer " src="Images/close1.gif" alt="Close" onclick="HideEmailDiv()" />
       
         
         </label>--%>
                        </div>
                        
                        
                
                    
         
                    <div style=" padding:10px;">
                       
                        <div>Enter Your Email</div>
                        <div><input class="form-control" id="addressBox" type="text" name="" /></div>
                       
                        <div style="color:Red"><span id="errortext"></span></div>
                       
                             <div style=" padding-top:10px">Add Markup: <input class="form-control" type="text" id="txtMarkup" onkeypress="return isNumber(event);" name="txtMarkup" /></div>
                        <div style=" padding-top:10px"><input type="button" class="btn but_b" value="Send" onclick="SendHotelMail('<%=index %>','<%=resultObj.RoomDetails.Length%>')" /></div>
                          
                           
                    
                    </div>
                    
                
                <div class="clear"></div>
               </div>
               
             <div class='clear'></div>
            </div>
            
            
            
            
            
            
            <div class='clear'>
            </div>
             <p id="emailStatus" style="color:Green; font-weight:bold; display:none; padding-left: 10px;"> </p>
            <%--<div id="emailMsg" style="display:none;color:Green; font-weight:bold; " >Email Sent Succefully...</div>--%>
        </div>
        <div class='clear'>
        </div>
    </div>
     
     </div>
     
      <div class="clearfix"> </div>
     
     
     </div>
     
     
       
      
      
      
        
   
   
    <div class='clear'>
    </div>
</div>
<%}else if(request == null) { %>
<div class='hotel_details'>
    <div class='showMsgHeading'>
Session Expired
 <div class='clear'>
        </div>
</div>
 <div class='selectRoomL' style="color:Red;font-size:16px;top:50px;left:50px; text-align:center;">
                <b>Your session seems to be expired! Please search again.</b>
                </div>
<div class='selectRoomR'>
            <div class='padbot10'>
                <a href="javascript:HideMRPopUp('<%=index %>')"><b>Close</b></a></div>
                </div>
               
                </div>
<%}%>
  
