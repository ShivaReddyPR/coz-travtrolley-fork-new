﻿using System;
using System.Web;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using CT.Core;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Services;

public partial class FleetGuestDetails : CT.Core.ParentPage
{
    string sessionId = "";
    protected int inedxId;
    protected FleetSearchResult[] filteredResultList;
    protected FleetSearchResult searchResult;
    protected FleetRequest request = new FleetRequest();
    protected string errorMessage = string.Empty;
    protected FleetItinerary itinerary = new FleetItinerary();
    protected decimal AirportCharges = 0;
    List<FleetLocation> locationList = new List<FleetLocation>();
    protected decimal rateofExchange = 1;
    protected decimal totalPrice = 0, markup = 0;
    protected int agencyId;
    //protected bool requiredControls = true;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //session FleetRequest
            if (Session["cSessionId"] == null || Session["FleetRequest"] == null || Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", false);
            }
            else
            {
                agencyId = Settings.LoginInfo.AgentId;
                sessionId = Session["cSessionId"].ToString();
            }
            //checking query string
            if (Request.QueryString["Id"] != null)
            {
                inedxId = Convert.ToInt32(Request.QueryString["Id"]);
            }
            //Checking all sessions null or not
            if (Session["searchResult"] != null && Request.QueryString["Id"] != null && Session["LoactionList"] != null)
            {
                filteredResultList = (FleetSearchResult[])Session["searchResult"];
                searchResult = filteredResultList[inedxId];
            }
            else
            {
                Response.Redirect("AbandonSession.aspx",false);
            }
            if (!IsPostBack)
            {
                try
                {
                    if (Session["cSessionId"] == null)
                    {
                        errorMessage = "Your Session is expired !! Please" + "<a href=\"FleetSearch.aspx\"" + "\">" + " Retry" + "</a>.";
                        Response.Redirect("FleetSearch.aspx");
                    }
                    else
                    {
                        //Page refreshing time need to clear all  documents sessions
                        Session["previouslyUploadedFiles"] = null;
                        Session["documentsPath"] = null;
                        Session["files"] = null;
                        request = (FleetRequest)Session["FleetRequest"];
                        //loding itinerary details
                        LoadFleetItinerary();
                        
                        locationList = (List<FleetLocation>)Session["LoactionList"];
                        FleetLocation fromLocation = locationList.Find(delegate(FleetLocation loc) { return loc.LocationCode == request.FromLocationCode; });
                        //calucating AirportCharges
                        if (fromLocation != null)
                        {
                            AirportCharges += (fromLocation.AirportCharges);
                        }
                        if (request.ToLocationCode != null && request.ToLocationCode.Length > 0)
                        {
                            FleetLocation toLocation = locationList.Find(delegate(FleetLocation loc) { return loc.LocationCode == request.ToLocationCode; });

                            if (toLocation != null)
                            {
                                AirportCharges += (toLocation.AirportCharges);
                            }
                        }
                        //searchResult Empty navigating error page
                        if (searchResult == null && searchResult.PriceInfo == null)
                        {
                            Response.Redirect("AbandonSession.aspx");
                        }
                        //binding services
                        lblCDW.Text = Math.Round(searchResult.CDW, searchResult.PriceInfo.DecimalPoint).ToString("N" + searchResult.PriceInfo.DecimalPoint);
                        lblSCDW.Text = Math.Round(searchResult.SCDW, searchResult.PriceInfo.DecimalPoint).ToString("N" + searchResult.PriceInfo.DecimalPoint);
                        lblPAI.Text = Math.Round(searchResult.PAI, searchResult.PriceInfo.DecimalPoint).ToString("N" + searchResult.PriceInfo.DecimalPoint);
                        lblCSEAT.Text = Math.Round(searchResult.CSEAT, searchResult.PriceInfo.DecimalPoint).ToString("N" + searchResult.PriceInfo.DecimalPoint);
                        lblVMD.Text = Math.Round(searchResult.VMD, searchResult.PriceInfo.DecimalPoint).ToString("N" + searchResult.PriceInfo.DecimalPoint);
                        lblGPS.Text = Math.Round(searchResult.GPS, searchResult.PriceInfo.DecimalPoint).ToString("N" + searchResult.PriceInfo.DecimalPoint);
                        lblChauffer.Text = Math.Round(searchResult.Chauffer, searchResult.PriceInfo.DecimalPoint).ToString("N" + searchResult.PriceInfo.DecimalPoint);
                        lblLinkCharges.Text = lblLinkChargesValue.Text = Math.Round(searchResult.LinkCharges, searchResult.PriceInfo.DecimalPoint).ToString("N" + searchResult.PriceInfo.DecimalPoint);

                        decimal totalAmount = Math.Round(searchResult.PriceInfo.NetFare + searchResult.PriceInfo.Markup - searchResult.PriceInfo.Discount, searchResult.PriceInfo.DecimalPoint);
                        if (searchResult.LinkCharges > 0)
                        {
                            itinerary.Emirates_Link_Charge = searchResult.LinkCharges;
                            totalAmount += searchResult.LinkCharges;
                        }
                        if (AirportCharges > 0)
                        {
                            if (!searchResult.SourceServiceAmount.ContainsKey("AirportCharges"))
                            {
                                searchResult.SourceServiceAmount.Add("AirportCharges", AirportCharges);
                            }
                            //calucating Airportcharges based on currency
                            itinerary.AirportCharge = Math.Round(AirportCharges * itinerary.Price.RateOfExchange, itinerary.Price.DecimalPoint);
                            totalAmount += itinerary.AirportCharge;
                        }
                        lblAirPortCharges.Text = lblAirPortChargesValue.Text = Math.Round(itinerary.AirportCharge, searchResult.PriceInfo.DecimalPoint).ToString("N" + searchResult.PriceInfo.DecimalPoint);
                        //binding Total Amount
                        lblGrandTotal.Text = hdnTotalAmount.Value = Math.Round(totalAmount, searchResult.PriceInfo.DecimalPoint).ToString("N" + searchResult.PriceInfo.DecimalPoint);
                    }

                    
                    Session["fleetItinerary"] = itinerary;
                }

                catch (Exception ex)
                {
                    Audit.Add(EventType.FleetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get FleetItinerary. Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
                }
            }

            if (Session["cSessionId"] == null || Session["FleetRequest"] == null || Session["fleetItinerary"] == null || Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            else
            {
               // Audit.Add(EventType.FleetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Get FleetItinerary Start", "0");
                try
                {
                    itinerary = Session["fleetItinerary"] as FleetItinerary;
                    hdnToDate.Value = itinerary.ToDate.ToString("dd/MM/yyyy");
                    ////Temporary purpose only Hard coded Location codes  ZIYAD
                    //if (itinerary.FromLocationCode.ToUpper() == "SHJA" || itinerary.FromLocationCode.ToUpper() == "CTRCOZ" || itinerary.FromLocationCode.ToUpper() == "HAL" || itinerary.FromLocationCode.ToUpper() == "HDOCOZMO")
                    //{
                    //    requiredControls = false;
                    //}
                    //else if (itinerary.ReturnStatus == "Y" && !string.IsNullOrEmpty(itinerary.ToLocationCode))
                    //{
                    //    if (itinerary.ToLocationCode.ToUpper() == "SHJA" || itinerary.ToLocationCode.ToUpper() == "CTRCOZ" || itinerary.ToLocationCode.ToUpper() == "HAL" || itinerary.ToLocationCode.ToUpper() == "HDOCOZMO")
                    //    {
                    //        requiredControls = false;
                    //    }
                    //}

                    // Calculation Price
                    decimal total = 0;

                    if (itinerary.Price.AccPriceType == PriceType.NetFare)
                    {
                        total = Convert.ToDecimal(itinerary.Price.NetFare + itinerary.Price.Markup - itinerary.Price.Discount) * rateofExchange;

                    }
                    lblCost.Text = Convert.ToString(Math.Round(total, itinerary.Price.DecimalPoint));
                    //Add Markup Price entered by the user
                    if (txtMarkup.Text.Trim().Length > 0)
                    {
                        decimal _markup = 0, percent = 0, fix = 0, mtotal = total;
                        string asvType = "";
                        //If P is selected, calculate the percent amount from the total and add it.
                        //If F is selected, directly add the price to the total
                        if (rbtnPercent.Checked)
                        {
                            try
                            {
                                percent = Convert.ToDecimal(txtMarkup.Text);
                            }
                            catch { }
                            asvType = "P";
                            _markup = ((mtotal) * (percent / 100));
                            mtotal = total + _markup;
                        }
                        else if (rbtnFixed.Checked)
                        {
                            try
                            {
                                fix = Convert.ToDecimal(txtMarkup.Text);
                            }
                            catch { }
                            asvType = "F";
                            _markup = fix;
                            mtotal = _markup + total;
                        }

                        this.markup = _markup;
                        Session["Markup"] = _markup;

                        lblTotal.Text = Math.Round(mtotal, Settings.LoginInfo.DecimalValue).ToString("N" + searchResult.PriceInfo.DecimalPoint);


                        if (itinerary.Price.AccPriceType == PriceType.NetFare)
                        {
                            itinerary.Price.AsvAmount = markup;
                            itinerary.Price.AsvType = asvType;
                            itinerary.Price.AsvElement = txtMarkup.Text;
                            //save addl markup for first room only
                        }

                    }
                    else
                    {
                        lblTotal.Text = Math.Round(total, Settings.LoginInfo.DecimalValue).ToString("N" + searchResult.PriceInfo.DecimalPoint);
                    }
                    lblPrice.Text = lblGrandTotal.Text;
                    lblFleetAmount.Text = lblTotal.Text;
                    //Audit.Add(EventType.FleetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Get FleetItinerary End", "0");
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.FleetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get FleetItinerary. Error: " + ex.Message, "0");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.FleetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Add Guest Details. Error: " + ex.Message, "0");
        }
    }
    //loding itinerary details
    private void LoadFleetItinerary()
    {
        try
        {
            if (Session["cSessionId"] == null || Session["FleetRequest"] == null || searchResult == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }

            else
            {
                //Genarting itinerary
                itinerary.FromCity = request.FromCity;
                itinerary.FromCityCode = request.FromCityCode;
                itinerary.FromLocation = request.FromLocation;
                itinerary.FromLocationCode = request.FromLocationCode;
                itinerary.FromDate = request.FromDateTime;
                itinerary.ToCity = request.ToCity;
                itinerary.ToCityCode = request.ToCityCode;
                itinerary.ToLocation = request.ToLocation;
                itinerary.ToLocationCode = request.ToLocationCode;
                itinerary.ToDate = request.ToDateTime;
                itinerary.FleetType = searchResult.FleetSpecs[0].CarType.ToString();
                itinerary.ReturnStatus = request.ReturnStatus;
                itinerary.FleetName = searchResult.FleetName;
                itinerary.RentType = searchResult.RentType;
                itinerary.VehTariffId = searchResult.IdVehTariff;
                itinerary.VehId = searchResult.VehId;
                itinerary.Price = searchResult.PriceInfo;
                itinerary.Currency = searchResult.PriceInfo.Currency;
                if (searchResult.Source == CarBookingSource.Sayara)
                {
                    itinerary.Source = CarBookingSource.Sayara;
                }
                itinerary.CreatedBy = Settings.LoginInfo.AgentId;
                itinerary.TransType = "B2B";
                itinerary.CancelId = "";
                itinerary.Ins_Excess = searchResult.Ins_Excess;
                itinerary.AgencyId = Settings.LoginInfo.AgentId;
                itinerary.LocationId = (int)Settings.LoginInfo.LocationID;
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.FleetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get FleetItinerary. Error: " + ex.Message, "0");
        }
    }

    protected void imgContinue_Click(object sender, EventArgs e)
    {
        if (action.Value != null)
        {
            if (action.Value == "BookFleet")
            {
                if (Session["fleetItinerary"] == null && Session["cSessionId"] == null)
                {
                    errorMessage = "Your Session is expired !! Please" + "<a href=\"FleetSearch.aspx\"" + "\">" + " Retry" + "</a>.";
                    return;
                }

                else
                {
                    try
                    {
                        itinerary = (FleetItinerary)Session["fleetItinerary"];
                        string reviewBookingPageLink = "FleetBookingConfirm.aspx";
                        List<FleetPassenger> paxDetailsList = new List<FleetPassenger>();
                        FleetPassenger passenger = new FleetPassenger();
                        passenger.Title = ddlTitle.SelectedItem.Value;
                        passenger.FirstName = txtFirstName.Text.Trim();
                        passenger.LastName = txtLastName.Text.Trim();
                        passenger.Email = txtEmail.Text.Trim();
                        passenger.MobileNo = txtMobileNo.Text.Trim();
                        //if (requiredControls)
                        {
                            passenger.PickupAddress = txtPickupAddress.Text.Trim();
                            passenger.Landmark = txtLandMark.Text.Trim();
                            passenger.MobileNo2 = txtMobileNo2.Text.Trim();
                        }
                        string fileName = string.Empty;

                        try
                        {
                            #region Documents Upload Section
                            if (!string.IsNullOrEmpty(txtLicenseNumber.Text) && !string.IsNullOrEmpty(txtLicenseIssuePlace.Text))
                            {
                                IFormatProvider format = new CultureInfo("en-GB", true);
                                try
                                {
                                    DateTime date = DateTime.ParseExact(txtLicenseExpiry.Text, "dd/MM/yyyy", null);
                                    passenger.LicenseExpiryDate = Convert.ToDateTime(date, format);
                                }
                                catch { passenger.LicenseExpiryDate = DateTime.Now; }
                                int prevUploadedCount = 0; //previously uploaded files count;

                                passenger.LicenseIssuePlace = txtLicenseIssuePlace.Text.Trim();
                                passenger.LicenseNumber = txtLicenseNumber.Text.Trim();
                                string dirFullPath = HttpContext.Current.Server.MapPath("~/" + CT.Configuration.ConfigurationSystem.SAYARAConfig["UploadFleetDoc"]) + @"\";
                                passenger.DocumentsPath = dirFullPath;

                                if (Session["previouslyUploadedFiles"] != null) //Existing Files
                                {
                                    List<string> previouslyUploadedFiles = new List<string>();
                                    previouslyUploadedFiles = Session["previouslyUploadedFiles"] as List<string>;
                                    prevUploadedCount = previouslyUploadedFiles.Count;
                                    if (hdnDeletedFilesList.Value != "0") //deleted files checking
                                    {
                                        string[] deletedFilesList = hdnDeletedFilesList.Value.Split('|');
                                        if (deletedFilesList.Length > 0)
                                        {
                                            foreach (string strFileName in deletedFilesList)
                                            {
                                                if (previouslyUploadedFiles.Contains(strFileName)) //removing deleted files
                                                {
                                                    previouslyUploadedFiles.Remove(strFileName);
                                                }
                                            }
                                            for (int i = 0; i < previouslyUploadedFiles.Count; i++) //Adding Existing files to string
                                            {
                                                if (!string.IsNullOrEmpty(fileName))
                                                {
                                                    fileName += "|" + previouslyUploadedFiles[i];
                                                }
                                                else
                                                {
                                                    fileName = previouslyUploadedFiles[i];
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 0; i < previouslyUploadedFiles.Count; i++)
                                        {
                                            if (!string.IsNullOrEmpty(fileName))
                                            {
                                                fileName += "|" + previouslyUploadedFiles[i];
                                            }
                                            else
                                            {
                                                fileName = previouslyUploadedFiles[i];
                                            }
                                        }
                                    }
                                }

                                if (Session["files"] != null)
                                {
                                    List<HttpPostedFile> filesList = Session["files"] as List<HttpPostedFile>;
                                    for (int i = 0; i < filesList.Count; i++) //assign to Filenames existing and new files
                                    {
                                        string fileNameOriginal = filesList[i].FileName;
                                        if (!string.IsNullOrEmpty(fileName))
                                        {
                                            fileName = fileName + "|" + ((i + 1) + prevUploadedCount).ToString() + "_" + "Doc_" + fileNameOriginal;
                                        }
                                        else
                                        {
                                            fileName = ((i + 1) + prevUploadedCount).ToString() + "_" + "Doc_" + fileNameOriginal;
                                        }
                                    }
                                }
                            }
                            passenger.FileNames = fileName;
                            #endregion
                        }
                        catch { }

                        //new Guest should be pass clientId -1
                        if (Convert.ToInt32(hdnClient.Value) > 0)
                        {
                            passenger.ClientId = Convert.ToInt32(hdnClient.Value);
                        }
                        itinerary.PassengerInfo = new List<FleetPassenger>();
                        itinerary.PassengerInfo.Add(passenger);
                        decimal serviceCharges = 0;
                        //checked services adding to itinearay
                        if (chkCDW.Checked)
                        {
                            itinerary.CDW = searchResult.CDW;
                            serviceCharges = searchResult.CDW;
                        }
                        if (chkSCDW.Checked)
                        {
                            itinerary.SCDW = searchResult.SCDW;
                            serviceCharges += searchResult.SCDW;
                        }
                        if (chkPAI.Checked)
                        {
                            itinerary.PAI = searchResult.PAI;
                            serviceCharges += searchResult.PAI;
                        }
                        if (chkCSEAT.Checked)
                        {
                            itinerary.CSEAT = searchResult.CSEAT;
                            serviceCharges += searchResult.CSEAT;
                        }
                        if (chkVMD.Checked)
                        {
                            itinerary.VMD = searchResult.VMD;
                            serviceCharges += searchResult.VMD;
                        }
                        if (chkGPS.Checked)
                        {
                            itinerary.GPS = searchResult.GPS;
                            serviceCharges += searchResult.GPS;
                        }
                        if (chkChauffer.Checked)
                        {
                            itinerary.CHAUFFER = searchResult.Chauffer;
                            serviceCharges += searchResult.Chauffer;
                        }
                        if (itinerary.Emirates_Link_Charge > -1)
                        {
                            serviceCharges += itinerary.Emirates_Link_Charge;
                        }
                        if (itinerary.AirportCharge > 0)
                        {
                            serviceCharges += itinerary.AirportCharge;
                        }
                        //All service saving in Tax column in price table
                        itinerary.Price.Tax = Math.Round(serviceCharges, itinerary.Price.DecimalPoint);
                        itinerary.KMRATE = searchResult.KMRATE;
                        itinerary.KMREST = searchResult.KMREST;
                        itinerary.VoucherStatus = true; //Always true for Update booking queue table
                        itinerary.SourceServiceAmount = searchResult.SourceServiceAmount;
                        Session["fleetItinerary"] = itinerary;

                        Response.Redirect(reviewBookingPageLink, false);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.FleetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get FleetItinerary. Error: " + ex.Message, "0");
                        throw ex;
                    }
                }

            }
        }
    }

    [WebMethod]  //Deleting upload file
    public static void RemoveSession(string response)
    {
        List<HttpPostedFile> files = new List<HttpPostedFile>();
        if (HttpContext.Current.Session["Files"] != null)
        {
            files = HttpContext.Current.Session["Files"] as List<HttpPostedFile>;
        }
        foreach (HttpPostedFile file in files)
        {
            if (file.FileName == response)
            {
                files.Remove(file);
                break;
            }
        }
        HttpContext.Current.Session["Files"] = files;
    }
}
