﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Visa;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;

public partial class VisaFaqList : CT.Core.ParentPage
{
    protected bool isAdmin = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Faq List";
        AuthorizationCheck();
        if (Session["agencyId"] != null && (int)Session["agencyId"] == 0)
        {
            isAdmin = true;
        }
        if (!IsPostBack)
        {
            BindAgent();
            if (Request.QueryString.Count > 0 && Request.QueryString[0].Trim() != "")
            {
                ddlAgent.SelectedIndex = ddlAgent.Items.IndexOf(ddlAgent.Items.FindByValue(Request.QueryString[0].Trim()));
            
            }
            if (!isAdmin)
            {
                ddlAgent.SelectedValue = Settings.LoginInfo.AgentId.ToString();
                ddlAgent.Enabled = false;
            }
            BindData();
        }
    }
    private void BindAgent()
    {
        try
        {
            int agentId = 0;
            if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
            string agentType = (Settings.LoginInfo.AgentType.ToString() != null ? Settings.LoginInfo.AgentType.ToString() : "BASEAGENT");
            ddlAgent.DataSource = AgentMaster.GetList(1, agentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated); 
            ddlAgent.DataValueField = "agent_Id";
            ddlAgent.DataTextField = "agent_Name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("Select Agent", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindData()
    {
        List<Visafaq> allFaq = Visafaq.GetFaqList(Convert.ToInt32(ddlAgent.SelectedItem.Value) );
        if (allFaq == null || allFaq.Count == 0)
        {
            lbl_msg.Text = "There are no Record.";
            lbl_msg.Visible = true;

        }
        else
        {
            lbl_msg.Visible = false;
        }
        gvVisaFaq.DataSource = allFaq;
        gvVisaFaq.DataBind();
    }

    protected void gvVisaFaq_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lb = ((LinkButton)e.Row.Cells[5].Controls[1]);
            lb.CommandArgument = gvVisaFaq.DataKeys[e.Row.RowIndex].Value.ToString();
            if (e.Row.Cells[3].Text == "True")
            {
                lb.Text = "Deactivate";
                e.Row.Cells[3].Text = "Active";
            }
            else
            {
                lb.Text = "Activate";
                e.Row.Cells[3].Text = "Inactive";
            }
        }

    }

    protected void gvVisaFaq_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChangeStatus")
        {
            bool isActive;
            string status = ((LinkButton)e.CommandSource).Text;
            int rowNumber = Convert.ToInt32(e.CommandArgument);
            if (status == "Activate")
            {
                isActive = true;
            }
            else
            {
                isActive = false;
            }
            Visafaq.ChangeStatus(rowNumber, isActive);
            BindData();
        }
    }

    protected void gvVisaFaq_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvVisaFaq.PageIndex = e.NewPageIndex;
        BindData();

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
