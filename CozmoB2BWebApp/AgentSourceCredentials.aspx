﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="AgentSourceCredentialsGUI" Codebehind="AgentSourceCredentials.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Agent Source Credentials</title>
    <link href="styles/CTStyle.css" rel="stylesheet" type="text/css" />
    
        <link href="css/override.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <script type="text/javascript">
    function closeWindow()
    {
        window.close();
    }
    </script>
    
    
    <style> 
    input { border: solid 1px #ccc; padding:2px; }
    .pull-right { float:right }
    
    body { background:#f7f7f7; margin:10px; }
    
    </style>
    <table>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ForeColor="Green"></asp:Label>
            </td>
        </tr>
    </table>
    <div>
        
          <table width="100%" cellpadding="0" cellspacing="1">
                        <tr>
                            <td colspan="2" align="center">
                                <div>
                                    <table id="tblAgentSources" runat="server" width="100%">
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <table width="90%" cellpadding="0" cellspacing="1">
                            <tr>
                            
                            <td> 
                            
                                <asp:Button runat="server" ID="btnUpdate" Text="Update"  CssClass="button pull-right"
                                    OnClick="btnUpdate_Click" />
                                    <asp:Button runat="server" ID="btnClose" Text="Close"  CssClass="button button pull-right"
                                    OnClientClick="return closeWindow();" />
                            
                            </td>
                            </tr>
                            
                            </table>
                            
                            
                            
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2">
                                <asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                    </table>
    </div>
   
    </form>
</body>
</html>
