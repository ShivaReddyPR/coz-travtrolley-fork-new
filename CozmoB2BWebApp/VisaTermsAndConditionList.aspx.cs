﻿using System;
using System.Web.UI.WebControls;
using Visa;
using CT.Core;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;

public partial class VisaTermsAndConditionList : CT.Core.ParentPage
{
    protected bool isAdmin = false;
    protected void Page_Load(object sender, EventArgs e)
    {

        this.Master.PageRole = true;
        //Page.Title = "City List";
        AuthorizationCheck();
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
        {
            isAdmin = true;
        }
        if (!IsPostBack)
        {
            BindAgent();
            ddlCountry.DataSource = Country.GetCountryList(); // VisaCountry.GetActiveVisaCountryList();
            ddlCountry.DataTextField = "key";
            ddlCountry.DataValueField = "value";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Select Country", "-1"));
            if (Request.QueryString.Count > 0 && Request.QueryString[0].Trim() != "")
            {
                ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(Request.QueryString[0].Trim()));
                ddlAgent.SelectedIndex = ddlAgent.Items.IndexOf(ddlAgent.Items.FindByValue(Request.QueryString[1].Trim()));
            }
            else
            {
                ddlCountry.SelectedIndex = 1;
                ddlAgent.SelectedIndex = 0;
                if (!isAdmin)
                {
                    ddlAgent.SelectedValue = Settings.LoginInfo.AgentId.ToString();
                    ddlAgent.Enabled = false;
                }
            }
            BindData();
        }

    }
    private void BindAgent()
    {
        try
        {
            int agentId = 0;
            if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
            string agentType = (Settings.LoginInfo.AgentType.ToString() != null ? Settings.LoginInfo.AgentType.ToString() : "BASEAGENT");
            ddlAgent.DataSource = AgentMaster.GetList(1, agentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataValueField = "agent_Id";
            ddlAgent.DataTextField = "agent_Name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("Select Agent", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindData()
    {
        
       
        List<VisaTermsAndCondition> allTerms = VisaTermsAndCondition.GetTermsAndConditionList(ddlCountry.SelectedValue,Convert.ToInt32(ddlAgent.SelectedItem.Value) );
        if (allTerms == null || allTerms.Count == 0)
        {
            lbl_msg.Text = "There are no record.";
            lbl_msg.Visible = true;

        }
        else
        {
            lbl_msg.Visible = false;
        }
        GridView1.DataSource = allTerms;
        GridView1.DataBind();

    }


   
   

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lb = ((LinkButton)e.Row.Cells[6].Controls[1]);
            lb.CommandArgument = GridView1.DataKeys[e.Row.RowIndex].Value.ToString();
            if (e.Row.Cells[3].Text == "True")
            {
                lb.Text = "Deactivate";
                e.Row.Cells[4].Text = "Active";
            }
            else
            {
                lb.Text = "Activate";
                e.Row.Cells[4].Text = "Inactive";
            }
        }
    }


    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChangeStatus")
        {
            bool isActive;
            string status = ((LinkButton)e.CommandSource).Text;
            int rowNumber = Convert.ToInt32(e.CommandArgument);
            // int i = Convert.ToInt32(GridView1.DataKeys[rowNumber].Value);
            if (status == "Activate")
            {
                isActive = true;
            }
            else
            {
                isActive = false;
            }
            VisaTermsAndCondition.ChangeStatus(rowNumber, isActive);
            BindData();


        }

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }


    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }


}
