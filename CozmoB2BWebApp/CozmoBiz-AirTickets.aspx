﻿<%@ Page Language="C#" %>
	<!DOCTYPE html>
	<html>

	<head id="Head1" runat="server">
		<title>Air Tickets</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="images/favicon.ico">

		<link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="css/RegisterStyle.css" rel="stylesheet" type="text/css" />
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<!-- manual css -->

		<link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
		<link href="build/css/main.min.css" rel="stylesheet" type="text/css" />


	</head>

	<body>
		<div class="topHeader2">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<a href="bizzlogin.aspx"><img src="images/logoc.jpg"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="heading_bizz">
			<div class="container">
				<h2> Air Tickets</h2>
			</div>
		</div>
		<form runat="server" id="form1">
			<div class="container">
				<div class="innerPage2">
					<div class="row">
						<div class="col-12">
							<p>CCTM ticketing staff are proven experts at constructing the best possible fares for domestic and international travel. Our qualified professionals are IATA-qualified with a minimum experience of five years in handling client requirements. These personnel are well versed with GDS (Global Distribution Software) and upgrade their skills continuously to keep up with the changing demands of the industry. As a result our department is well-placed to handle the numerous complexities involved in managing multi-city, multi-country itineraries.
								<br />
								<br />At CCTM, we leverage our impeccable research skills and deep understanding of every flight sector to offer competitive ticket prices and logical itineraries to our clients. We make this task look deceptively easy! Contact us for bookings for individual or group travel, of any size, to any destination across the globe.</p>
						</div>
					</div>
				</div>
			</div>
		</form>
		<div class="footer_sml_bizz">Copyright 2019 Cozmo Travel World, India. All Rights Reserved.</div>





	</body>

	</html>
