﻿using System;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine.Insurance;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using System.Web.Services;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Data;
using System.Linq;


public partial class Insurance : CT.Core.ParentPage// System.Web.UI.Page
{
    protected string currency = string.Empty;
    protected string data = string.Empty;
    List<SourceDetails> sourceDetails;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            Culture = "en-GB";
            if (Settings.LoginInfo != null)
            {
                sourceDetails = new List<SourceDetails>();
                if (!IsPostBack)
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;

                    UserMaster user = new UserMaster(Settings.LoginInfo.UserID);
                    List<UserSpecialAccess> userSpecialAccessList =  UserSpecialAccess.GetUserSpecialAccesses(Settings.LoginInfo.UserID);
                    if (userSpecialAccessList!=null && userSpecialAccessList.Where(x => x.AccessType == "Insurance Access" && x.AccessValue == 1).ToList().Count > 0)
                    {
                        hdnInsurance.Value = "Insurance";
                        sourceDetails = AgentMaster.GetInsuranceCredentials(string.Empty);
                        data = Newtonsoft.Json.JsonConvert.SerializeObject(sourceDetails.Distinct().ToList());
                        ddlPseudo.DataSource = JsonConvert.DeserializeObject<DataTable>(data);
                        ddlPseudo.DataTextField = "HAP";
                        ddlPseudo.DataValueField = "HAP";
                        ddlPseudo.DataBind();
                        ddlPseudo.Visible = true;
                        divPsedo.Visible = true;
                        string HAP = string.Empty;
                        sourceDetails.ForEach(x => { if (x.CurrencyCode == Settings.LoginInfo.Currency) HAP = x.HAP; });
                        ddlPseudo.SelectedValue = HAP;
                        hdnCurrency.Value = Settings.LoginInfo.Currency;
                    }
                }
                currency = Settings.LoginInfo.Currency;
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed in Page_Load: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
    protected void btnGetQuote_Click(object sender, EventArgs e)
    {
        try
        {
            Session["InsPassengers"] = null; // Remove the previous passenger details 
            InsuranceRequest request = new InsuranceRequest();
            request.Adults = Convert.ToInt32(ddlAdults.SelectedValue);
            request.Childs = Convert.ToInt32(ddlChilds.SelectedValue);
            request.Infants = Convert.ToInt32(ddlInfants.SelectedValue);
            request.CountryCode = Country.GetCountryCodeFromCountryName(Origin.Text.Split(',')[1].Split('-')[0]);
            request.CultureCode = "EN";

            request.IsSeniorCitizen = hdnPolicyFor.Value == "SeniorCitizen" ? true : false;
            request.IsDomesticSearch = Convert.ToBoolean(hdnIsDomestic.Value);

            //Flight details
            request.DepartureStationCode = GetCityCode(Origin.Text);
            request.DepartureAirlineCode = txtDepAirline.Text;
            request.DepartureCountryCode = Country.GetCountryCodeFromCountryName(Origin.Text.Split(',')[1].Split('-')[0]);
            request.DepartureFlightNo = "";
            request.DepartureDateTime = Convert.ToDateTime(DepDate.Text).ToString("yyyy-MM-dd hh:mm:ss");

            request.ArrivalStationCode = GetCityCode(Destination.Text);
            request.ArrivalCountryCode = Country.GetCountryCodeFromCountryName(Destination.Text.Split(',')[1].Split('-')[0]); //Destination.Text.Split(',')[1];
            if (hdnWayType.Value == "return")
            {
                request.ReturnAirlineCode = txtArrAirline.Text;
                request.ReturnDateTime = Convert.ToDateTime(ReturnDate.Text).ToString("yyyy-MM-dd hh:mm:ss");
                request.ReturnFlightNo = "";
            }
            else
            {
                request.ReturnAirlineCode = txtDepAirline.Text;
                request.ReturnDateTime = Convert.ToDateTime(DepDate.Text).ToString("yyyy-MM-dd hh:mm:ss");
                request.ReturnFlightNo = "";
            }
            if(ddlPseudo.Visible)
                 request.PseudoCode = ddlPseudo.SelectedItem.Value;
            request.TransType = "B2B";//B2B or B2C
            request.UserName = Settings.LoginInfo.LoginName;
            
            if (!string.IsNullOrEmpty(request.PseudoCode))
            {
                request.CurrencyCode = hdnCurrency.Value;
            }
            Session["InsRequest"] = request;

            Response.Redirect("InsurancePlans.aspx", false);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to call Insurance: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }


    private string GetCityCode(string searchCity)
    {
        string cityCode = "";

        cityCode = searchCity.Split(',')[0];

        if (cityCode.Contains("(") && cityCode.Contains(")") && cityCode.Length > 3)
        {
            cityCode = cityCode.Substring(1, 3);
        }
        return cityCode;
    }

    [WebMethod]  //Checking search request is domestic or not only for India
    public static bool IsDomesticSearch(string origin, string destination)
    {
        string originCountryCode = Country.GetCountryCodeFromCountryName(origin);
        string destinationCountryCode = Country.GetCountryCodeFromCountryName(destination);
        return (originCountryCode == "IN" && destinationCountryCode == "IN") ? true : false;
    }
}
