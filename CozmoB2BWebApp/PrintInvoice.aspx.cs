using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

public partial class PrintInvoice :CT.Core.ParentPage// System.Web.UI.Page
{
    protected List<Ticket> ticketList;
    protected Invoice invoice = new Invoice();
    protected AgentMaster agency;
    protected string agencyAddress;
    protected RegCity agencyCity = new RegCity();
    protected int agencyId;
    protected int cityId;
    protected string remarks = string.Empty;
    protected string pnr;
    protected string routing;
    protected int invoiceNumber;
    protected string billAddress;
    protected DateTime traveldate = new DateTime();
    protected Dictionary<string, DataRow[]> segment = new Dictionary<string, DataRow[]>();
    protected DataTable segmentTable;

    /// <summary>
    /// Flag to indicate if the agency type is 'Service'
    /// </summary>
    protected bool isServiceAgency;
    protected void Page_Load(object sender, EventArgs e)
    {
        invoiceNumber = Convert.ToInt32(Request["invoiceNumber"]);
        invoice.Load(invoiceNumber);
        billAddress = Request["billingAddress"];
        agency = new AgentMaster(Settings.LoginInfo.AgentId);
        if (Request["isSave"].ToString() == "true")
        {
            decimal tdsCommission = Convert.ToDecimal(Request["tdsCommission"]);
            decimal commission = Convert.ToDecimal(Request["handlingCharge"]);
            decimal agentplb = Convert.ToDecimal(Request["agentplb"]);  
            for (int i = 0; i < invoice.LineItem.Count; i++)
            {
                invoice.LineItem[i].Price.TdsCommission = tdsCommission / invoice.LineItem.Count;
                invoice.LineItem[i].Price.AgentCommission = commission / invoice.LineItem.Count;
                invoice.LineItem[i].Price.AgentPLB = agentplb / invoice.LineItem.Count;
                invoice.LineItem[i].Price.Save();
            }
            invoice.SaveBillingAddress(invoiceNumber, billAddress);
        }
        else
        {
            // checking if the page is reached with proper data
            BillingAddress.BorderStyle = BorderStyle.None;
            BillingAddress.ReadOnly = true;
            BillingAddress.Text = billAddress;
            HandlingBox.BorderStyle = BorderStyle.None;
            HandlingBox.ReadOnly = true;
            double handlingCharge = 0;
            for (int count = 0;count < invoice.LineItem.Count; count++)
            {
                handlingCharge = handlingCharge + Convert.ToInt32(invoice.LineItem[count].Price.AgentCommission);
            }
            HandlingBox.Text = handlingCharge.ToString();
            if (Request["agencyId"] != null && Request["pnr"] != null)
            {
                agencyId = Convert.ToInt32(Request["agencyId"]);
                agency = new AgentMaster(agencyId);
                pnr = Request["pnr"];
                routing = Request["routing"];
                              
            }
            else
            {
                //TODO: Handle it
            }

            if (Request["remarks"] != null)
            {
                remarks = Request["remarks"];
            }
            // Reading agency information.
            DataTable dtCities = CityMaster.GetList(agency.Country, ListStatus.Short, RecordStatus.Activated);
            DataRow[] cities = dtCities.Select("City_Name like '%" + agency.City + "%'");

            if (cities != null && cities.Length > 0)
            {
                cityId = Convert.ToInt32(cities[0]["city_id"]);
            }
            agency = new AgentMaster(agencyId);
            isServiceAgency = true;//agency.AgencyTypeId == Convert.ToInt32(ConfigurationSystem.Core["ServiceAgencyTypeId"]);
            if (agency.City.Length > 0)
            {
                agencyCity.CityName = agency.City;
            }
            else
            {
                agencyCity = RegCity.GetCity(cityId);
            }
            // Formatting agency address for display.
            agencyAddress = agency.Address;
            agencyAddress.Trim();
            if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
            {
                agencyAddress += ",";
            }
            //if (agency.Address.Line2 != null && agency.Address.Line2.Length > 0)
            //{
            //    agencyAddress += agency.Address.Line2;
            //}
            agencyAddress.Trim();
            if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
            {
                agencyAddress += ",";
            }
            ticketList = new List<Ticket>();
            
            for (int count = 0; count < invoice.LineItem.Count; count++)
            {
                Ticket ticket = new Ticket();
                ticket.Load(invoice.LineItem[count].ItemReferenceNumber);
                ticketList.Add(ticket);
            }
            int flightId = Convert.ToInt32(ticketList[0].FlightId);

            FlightInfo[] allSegments = FlightInfo.GetSegments(flightId);
            segmentTable = new DataTable();
            DataColumn column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "source"; segmentTable.Columns.Add(column);
            column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "destination"; segmentTable.Columns.Add(column);
            column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "flightNumber"; segmentTable.Columns.Add(column);
            column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "status"; segmentTable.Columns.Add(column);
            column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "bookingClass"; segmentTable.Columns.Add(column);
            column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "AirLine"; segmentTable.Columns.Add(column);
            column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "conjunctionNo"; segmentTable.Columns.Add(column);

            List<string> conjunctionlist = new List<string>();
            conjunctionlist.Add("C0");

            for (int i = 0; i < allSegments.Length; i++)
            {
                if (allSegments[i].ConjunctionNo != null)
                {
                    if (!conjunctionlist.Contains(allSegments[i].ConjunctionNo.ToString()))
                    {
                        conjunctionlist.Add(allSegments[i].ConjunctionNo.ToString());
                    }
                }

                DataRow row = segmentTable.NewRow();

                row["source"] = allSegments[i].Origin.AirportCode;
                row["destination"] = allSegments[i].Destination.AirportCode;
                row["flightNumber"] = allSegments[i].FlightNumber;
                row["status"] = allSegments[i].Status;
                row["bookingClass"] = allSegments[i].BookingClass;
                row["AirLine"] = allSegments[i].Airline;
                row["conjunctionNo"] = allSegments[i].ConjunctionNo;
                segmentTable.Rows.Add(row);
            }

            for (int j = 0; j < conjunctionlist.Count; j++)
            {
                if (conjunctionlist[j] == "C0")
                {
                    segment[conjunctionlist[j]] = segmentTable.Select("conjunctionNo is null");
                }
                else
                {
                    segment.Add(conjunctionlist[j], segmentTable.Select("conjunctionNo='" + conjunctionlist[j].ToString() + "'"));
                }
            }

            traveldate = FlightItinerary.GetTravelDateByFlightId(flightId);

        }

    }
}
