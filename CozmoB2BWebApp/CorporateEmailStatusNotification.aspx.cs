﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using CT.Core;
using CT.Corporate;
using System.Collections.Generic;

public partial class CorporateEmailStatusNotification : System.Web.UI.Page
{
    protected string status = string.Empty;
    protected string _approvalStatus;
    protected string approverNames = string.Empty;
    protected CorporateProfileExpenseDetails detail;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["status"] != null)
        {

           
            if (Convert.ToString(Request.QueryString["status"]) == "A")
            {
                status = "APPROVED";
            }
            else
            {
                status = "REJECTED";
            }
        }

    }

    protected string getEmpHtmlText(CorporateProfileExpenseDetails detail, string status)
    {
        string empHtml = string.Empty;
        empHtml += @"<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'><html><head><META http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body><div style='Margin:0;background:#f3f3f3!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important'><span style='color:#f3f3f3;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;overflow:hidden'></span><table style='Margin:0;background:#f3f3f3!important;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;height:100%;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;width:100%'><tr style='padding:0;text-align:left;vertical-align:top'><td align='center' valign='top' style='Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'><center style='min-width:580px;width:100%'><table style='Margin:0 auto;background:#fefefe;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;margin-top:20px;padding:10px;text-align:center;vertical-align:top;width:580px' align='center'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><td style='Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'><table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><td height='16px' style='Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:16px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'> </td></tr></tbody></table>";

        empHtml += @"<table style='border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><th style='Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px'><table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tr style='padding:0;text-align:left;vertical-align:top'><th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'><img src='"+Request.Url.Scheme+"://www.travtrolley.com/images/logo.jpg' style='clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto'></th><th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;width:0'></th></tr></table></th></tr></tbody></table>";

        empHtml += @"<table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><td height='16px' style='Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:16px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'> </td></tr></tbody></table><table style='border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><th style='Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px'><table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tr style='padding:0;text-align:left;vertical-align:top'><th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'><h1 style='Margin:0;Margin-bottom:10px;color:inherit;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;word-wrap:normal'>";
        empHtml += @"Dear" + " " + detail.EmpName;

        empHtml += @"</h1><p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'>Below expenses are";

        empHtml += status + "by" + " " + approverNames + "</p>";

        empHtml += @"<table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><td height='30px' style='Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:30px;font-weight:400;line-height:30px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'> </td></tr></tbody></table><table style='background:#fff;border:1px solid #167f92;border-collapse:collapse;border-radius:10px;border-spacing:0;color:#024457;font-size:11px;margin:1em 0;padding:0;text-align:left;vertical-align:top;width:100%'><tr style='background-color:#eaf3f3;border:1px solid #d9e4e6;padding:0;text-align:left;vertical-align:top'><th style='Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left'>Date</th><th style='Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left'>Expense Ref</th><th style='Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left'>Expense Category</th><th style='Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left'>Expense Type</th><th style='Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left'>Amount</th><th style='Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left'>Status</th></tr>";

        empHtml += @"<tr style='border:1px solid #d9e4e6;padding:0;text-align:left;vertical-align:top'>";

        //Date      
        empHtml += @"<td style='Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word'>";
        empHtml += Convert.ToString(detail.Date).Split(' ')[0];
        empHtml += @"</td>";

        //Expense Ref
        empHtml += @"<td style='Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word'>";
        empHtml += detail.DocNo;
        empHtml += @"</td>";

        //Expense Category
        empHtml += @"<td style='Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word'>";
        empHtml += detail.ExpCategoryText;
        empHtml += @"</td>";

        //Expense Type
        empHtml += @"<td style='Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word'>";
        empHtml += detail.ExpTypeText;
        empHtml += @"</td>";

        //Amount
        empHtml += @"<td style='Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word'>";
        empHtml += detail.Currency + detail.Amount;
        empHtml += @"</td>";

        //Status --Approved or Rejected.
        empHtml += @"<td style='Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word'>";
        empHtml += status;
        empHtml += @"</td>";
        empHtml += @"</tr></tbody></table>";

        //Footer Template  

        empHtml += @"<table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><td height='30px' style='Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:30px;font-weight:400;line-height:30px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'> </td></tr></tbody></table><table style='border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><th style='Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%'><table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tr style='padding:0;text-align:left;vertical-align:top'><th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'><p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'>Regards<br><strong> ";

        empHtml += detail.AgencyName;

        empHtml += @"</strong></p></th><th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;width:0'></th></tr></table></th></tr></tbody></table></th></tr></table></th></tr></tbody></table></td></tr></tbody></table></center></td></tr></table><div style='display:none;white-space:nowrap;font:15px courier;line-height:0'></div></div></body></html>";
        return empHtml;

    }

    protected void SendEmail(string toEmpOrApp, string approverStatus, int expDetailId, int approverId)
    {
        detail = new CorporateProfileExpenseDetails(expDetailId);
        detail.ExpDetailId = expDetailId;
        try
        {
            if (approverStatus == "A")
            {
                _approvalStatus = "Approved";
            }
            else
            {
                _approvalStatus = "Rejected";
            }
            List<string> toArray = new System.Collections.Generic.List<string>();
            approverNames = detail.ProfileApproversList.Where(i => i.ApproverId == approverId).Select(i => i.ApproverName).FirstOrDefault();
            if (!string.IsNullOrEmpty(detail.EmpEmail))
            {
                toArray.Add(detail.EmpEmail);
            }
            string empHTML = getEmpHtmlText(detail, _approvalStatus);
            string subject = "Expense Status Change Notification";
            if (toArray != null && toArray.Count > 0)
            {
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, empHTML, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
            }
            approverNames = string.Empty;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, 0, "(CorporateEmailStatusNotification.aspx)Failed to Send Email For Employee: Reason - " + ex.ToString(), "");
        }

    }
    protected void btnReject_Click(object sender, EventArgs e)
    {
        try
        {
            string appStatus = string.Empty;
            int approverId = 0;
            int expDetailId = 0;
            if (Request.QueryString["status"] != null)
            {
                appStatus = HttpContext.Current.Server.UrlDecode(Request.QueryString["status"]);
            }
            if (Request.QueryString["approverId"] != null)
            {
                approverId = Convert.ToInt32(HttpContext.Current.Server.UrlDecode(Request.QueryString["approverId"]));
            }
            if (Request.QueryString["expDetailId"] != null)
            {
                expDetailId = Convert.ToInt32(HttpContext.Current.Server.UrlDecode(Request.QueryString["expDetailId"]));
            }
            if (!string.IsNullOrEmpty(appStatus) && approverId > 0 && expDetailId > 0 && txtReason.Text.Length >0)
            {
                CorporateProfileExpenseDetails.UpdateRefundStatusByMail(expDetailId, approverId, appStatus, txtReason.Text);
                lblSuccess.Visible = true;
                lblSuccess.Text = "Successfully Rejected !";
                txtReason.Text = string.Empty;
                try
                {
                    SendEmail("E", appStatus, expDetailId, approverId);
                }
                catch { }
                
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateEmailStatusNotification Page)btnReject Event Error Occurred." + ex.ToString(), "0");
        }
    }
}
