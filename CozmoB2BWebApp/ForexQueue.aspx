﻿<%@ Page Title="Forex Queue" Language="C#" MasterPageFile="~/TransactionBE.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="ForexQueue.aspx.cs" Inherits="CozmoB2BWebApp.ForexQueue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script src="scripts/paginathing.js"></script>
    <iframe id="IShimFrame" style="position: absolute; display: none;"></iframe>

    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 120px; left: 250px; display: none; z-index: 9999">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 120px; left: 500px; display: none; z-index: 9999">
        </div>
    </div>
    <div class="body_container" style="margin-top: 20px;">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <span>Select Agent</span>
                    <asp:DropDownList ID="ddlagents" runat="server" CssClass="form-control">
                        <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                    </asp:DropDownList>
                    <span id="errorddlagents" style="color: red;"></span>
                </div>
            </div>
            <div class="col-md-3">
                <span>From Date</span>
                <div class="form-group">
                    <div class="input-group date fromDate">
                        <asp:TextBox ID="txtFromDate" runat="server" placeholder="DD/MM/YYYY" ReadOnly="true" CssClass="inputEnabled form-control"></asp:TextBox>
                        <asp:Label runat="server" class="input-group-addon" onclick="showCal1()" Style="width: 25%">
                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                        </asp:Label>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <span>To Date</span>
                <div class="form-group">
                    <div class="input-group date toDate">
                        <asp:TextBox ID="txtToDate" runat="server" placeholder="DD/MM/YYYY" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        <asp:Label runat="server" class="input-group-addon" onclick="showCal2()" Style="width: 25%">
                                        <img id="dateLink2" src="images/call-cozmo.png" alt="Pick Date" /> 
                        </asp:Label>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <span>Select Product</span>
                    <asp:DropDownList ID="ddlProduct" CssClass="form-control" runat="server">
                        <asp:ListItem Value="-1">-- Select Product--</asp:ListItem>
                        <asp:ListItem Value="Multicurrency Card">Multicurrency Card</asp:ListItem>
                        <asp:ListItem Value="Cash">Cash</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <span>Select Currency</span>
                    <asp:DropDownList ID="ddlCurrency" CssClass="form-control" runat="server">
                        <asp:ListItem Value="-1">-- Select Currency--</asp:ListItem>
                        <asp:ListItem Value="US Dollar">US Dollar</asp:ListItem>
                        <asp:ListItem Value="Euro">Euro</asp:ListItem>
                        <asp:ListItem Value="Sterling Pound">Sterling Pound</asp:ListItem>
                        <asp:ListItem Value="ThaiBhat">Thai Bhat</asp:ListItem>
                        <asp:ListItem Value="Singapore Dollar">Singapore Dollar</asp:ListItem>
                        <asp:ListItem Value="Canadian Ringgit">Malaysian Ringgit</asp:ListItem>
                        <asp:ListItem Value="UAEDirham">UAE Dirham</asp:ListItem>
                        <asp:ListItem Value="HongkongDollar">Hongkong Dollar</asp:ListItem>
                        <asp:ListItem Value="AustralianDollar">Australian Dollar</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <span>Name</span>
                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control" placeholder="Name" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <span>Mobile No</span>
                    <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control" placeholder="Mobile No" onKeyPress="return isNumber(event)" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <span>Email</span>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="Email" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <span>Travelling To</span>
                    <asp:TextBox ID="txtTravelling" runat="server" CssClass="form-control" placeholder="Travelling To" />
                </div>
            </div>
              <div class="col-md-3">
                <div class="form-group">
                    <span>Reference No</span>
                    <asp:TextBox ID="txtRefNo" runat="server" CssClass="form-control" placeholder="Reference No" />
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <input type="button" id="btnSearch" class="btn btn-primary" style="float: right; margin-left: 3px;" value="Search" onclick="LoadQueueDetails();" />
            </div>

            <%-- <div class="form-group">
                <asp:Button ID="btnClear" CssClass="btn btn-primary" Style="float: right; margin-left: 3px;" OnClientClick="return window.location.reload();" runat="server" Text="Clear" />
            </div>--%>
        </div>
        <div class="row">
            <ul id="ulForexDetails" class="list-group"></ul>
        </div>
    </div>
    <div class="modal fade" id="modalHistory" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="color: white">
                    <asp:Label ID="lbltitle" Style="color: white; text-align: center; margin-top: 15px" runat="server" Text="Forex Remarks"></asp:Label>
                    <button type="button" class="close" data-dismiss="modal" style="height: 10px; width: 10px" aria-label="Close">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body" id="forexHistory">
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnclose" runat="server" data-dismiss="modal" CssClass="btn btn-info btn-primary" Text="Close" Style="border-radius: 5px" />
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            LoadQueueDetails();
        });
        var ForexQueue;
        var TotalRecords = 0;

        function showselpage(event, pageno) {
            LoadData(pageno);
            $(this).addClass('active');

        }
        function LoadData(page) {
            var frid = [];
            $('.list-group').children().remove();
            var details = "";
            var showFrom = ((Math.ceil(page) - 1) * 10);
            var showTo = Math.ceil(showFrom) + 9;
            $('#ulForexDetails').html('');
            if (ForexQueue != undefined && ForexQueue.length > 0) {

                for (var i = Math.ceil(showFrom); i <= Math.ceil(showTo); i++) {
                    if (ForexQueue[i] != undefined && ForexQueue[i].FRID != undefined && !frid.includes(ForexQueue[i].FRID)) {
                        frid.push(ForexQueue[i].FRID);
                        var status = '';
                        var remarks = "<tr><th>SL.NO</th> <th>Status</th> <th>Remarks</th> <th>log Time</th> </tr>";
                        var c = 1;
                         var forexRemarks = ForexQueue[i].forexRequestRemarks;
                        if (forexRemarks != undefined && forexRemarks.length > 0 && forexRemarks != "") {
                            for (var j = 0; j < forexRemarks.length; j++) {
                                status = forexRemarks[j].RemarksStatus;
                                remarks += "<tr><td>" + c + "</td><td>"
                                    + forexRemarks[j].RemarksStatus + "</td><td>"
                                    + forexRemarks[j].Remarks + "</td><td>"
                                    + forexRemarks[j].CreatedOn.replace('T', ' ') + "</td></tr>";
                                c++;
                            }
                        }
                        var li = '<li id="forexDetails' + ForexQueue[i].FRID + '" class="tbl"><div class="row">' +
                            '<div class="col-sm-4">Agent Name : <span style="font-weight:bold">' + ForexQueue[i].AgentName + '</span></div>' +
                            '<div class="col-sm-4">Reference No : <span style="font-weight:bold">' + ForexQueue[i].FRRefNo + '</span></div>' +
                            '<div class="col-sm-4">Status : <span style="font-weight:bold">' + status + '</span></div>'+                           
                            '<div class="col-sm-4">Product Type : <span style="font-weight:bold">' + ForexQueue[i].ProductType + '</span></div>' +
                            '<div class="col-sm-4">Currency : <span style="font-weight:bold">' + ForexQueue[i].Currency + '</span></div>' +
                            '<div class="col-sm-4">Forex : <span style="font-weight:bold">' + ForexQueue[i].ForexRequired + '</span></div>' +
                            '<div class="col-sm-4">Amount INR : <span style="font-weight:bold">' + ForexQueue[i].AmountInINR + '</span></div>' +
                            '<div class="col-sm-4">Name : <span style="font-weight:bold">' + ForexQueue[i].Name + '</span></div>' +
                            '<div class="col-sm-4">Mobile No : <span style="font-weight:bold">' + ForexQueue[i].MobileNumber + '</span></div>' +
                            '<div class="col-sm-4">Email : <span style="font-weight:bold">' + ForexQueue[i].Email + '</span></div>' +
                            '<div class="col-sm-4">Travelling To : <span style="font-weight:bold">' + ForexQueue[i].TravellingTo + '</span></div>' +
                            '<div class="col-sm-4">Created By : <span style="font-weight:bold">' + ForexQueue[i].UserName + '</span></div>' +
                             '<div class="col-sm-4">Location : <span style="font-weight:bold">' + ForexQueue[i].LocationName + '</span></div></div>' ;
                       
                        if (forexRemarks != undefined && forexRemarks.length > 0 && forexRemarks != "") {                           
                            var options = "";
                            if (status != undefined && status.toUpperCase() == "CONFIRMED" ) {
                                options += "<option selected value='2'>Confirmed</option>"; 
                                li += '<div class="row"><div class="col-sm-23"></div><div class="col-sm-3"><input type="hidden" id="hdnstatus' + ForexQueue[i].FRID + '" value="2"><select  id="ddlstatus' + ForexQueue[i].FRID + '"style="display:none" class="form-control"> ' + options + '</select ></div > ';
                            }
                             else if (status != undefined && status.toUpperCase() == "CANCELLED") {
                                options += "<option selected value='3'>Cancelled</option>"; 
                                li += '<div class="row"><div class="col-sm-2"></div><div class="col-sm-3"><input type="hidden" id="hdnstatus' + ForexQueue[i].FRID + '" value="2"><select  id="ddlstatus' + ForexQueue[i].FRID + '"style="display:none" class="form-control"> ' + options + '</select ></div > ';
                            }
                            else if (status != undefined && status.toUpperCase() == "INPROGRESS") {
                                options += "<option value='-1'>--Select Status--</option>";
                                  options += "<option value='1'>In Progress</option>";
                                options += "<option value='2'>Confirmed</option>";
                                 options += "<option value='3'>Cancelled</option>"; 
                                li += '<div class="row"><div class="col-sm-2"></div><div class="col-sm-3">   <select  id="ddlstatus' + ForexQueue[i].FRID + '" class="form-control"> ' + options + '</select ></div > ';
                            }
                            else {
                                options += "<option value='-1'>--Select Status--</option>";
                                options += "<option value='1'>In Progress</option>";
                                options += "<option value='2'>Confirmed</option>";
                                options += "<option value='3'>Cancelled</option>"; 
                                li += '<div class="row"><div class="col-sm-2"></div><div class="col-sm-3">  <select id="ddlstatus' + ForexQueue[i].FRID + '" class="form-control"> ' + options + '</select ></div > ';
                            }
                            li += '<div class="col-sm-4" ><input type="textarea" placeholder="Remarks Here" id="txtRemarks' + ForexQueue[i].FRID + '" class="form-control" placeholder="Remarks Here"></div>';
                            li += '<div class="col-sm-1"><input type="button" id="btnSave+'+ ForexQueue[i].FRID+'"  class="btn btn-primary" value="Save" onclick="saveRemarks(' + ForexQueue[i].FRID + "," + forexRemarks[0].FRRID + ')"></div>';
                            li += '<div class="col-sm-1"><input type="button"  class="btn btn-primary" value="Show Remarks" onclick="showRemarks(' + ForexQueue[i].FRID + ')"></div>';

                        }
                        else {
                            remarks += "<tr><td colspan='3'  Style='font-weight:bold;text-align:center'><td>No Remarks Found</td></tr>";
                        }
                        li += "</div><div  style='display:none' id='remarks" + ForexQueue[i].FRID + "'><table class='table table-hovered'>" + remarks + "</table></div>";
                        li += '</li>';
                        details += li;

                    }
                }
                $('#ulForexDetails').html(details);
            }
        }
        function LoadQueueDetails() {
            var startDate = $("#<%=txtFromDate.ClientID%>").val();
            var endDate = $("#<%=txtToDate.ClientID%>").val();
            var request = {
                AgentId: parseInt($("#<%=ddlagents.ClientID%>").val()),
                ProductType: $("#<%=ddlProduct.ClientID%>").val() != "-1" ? $("#<%=ddlProduct.ClientID%>").val() : null,
                Currency: $("#<%=ddlCurrency.ClientID%>").val() != "-1" ? $("#<%=ddlCurrency.ClientID%>").val() : null,
                Name: $("#<%=txtName.ClientID%>").val() != '' ? $("#<%=txtName.ClientID%>").val() : null,
                MobileNumber: $("#<%=txtMobileNo.ClientID%>").val() != '' ? $("#<%=txtMobileNo.ClientID%>").val() : null,
                Email: $("#<%=txtEmail.ClientID%>").val() != '' ? $("#<%=txtEmail.ClientID%>").val() : null,
                TravellingTo: $("#<%=txtTravelling.ClientID%>").val() != '' ? $("#<%=txtTravelling.ClientID%>").val() : null,
                FRRefNo: $("#<%=txtRefNo.ClientID%>").val() != '' ? $("#<%=txtRefNo.ClientID%>").val() : null
            };
            $.ajax({
                type: "POST",
                url: "ForexQueue.aspx/BindQueue",
                contentType: "application/json; charset=utf-8",
                data: "{'request':'" + JSON.stringify(request) + "','startDate':'" + startDate + "','endDate':'" + endDate + "'}",
                dataType: "json",
                async: true,
                success: function (data) {
                    if (data.d != undefined && data.d != '[]') {
                        ForexQueue = JSON.parse(data.d);
                        $('.panel-footer').remove();
                        $('.list-group').paginathing({
                            perPage: 10,
                            limitPagination: 9,
                            containerClass: 'panel-footer',
                            pageNumbers: true,
                            totalRecords: ForexQueue.length
                        });
                        LoadData(1);
                    }
                    else {
                        $('.panel-footer').remove();
                        $('#ulForexDetails').html('');
                        alert("No recored found in search criteria");
                    }
                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
            });
        }
        function saveRemarks(frid, frrid) {
            if (frid != undefined && frid != '' && parseInt(frid) > 0) {
                var startDate = $("#<%=txtFromDate.ClientID%>").val();
              var endDate = $("#<%=txtToDate.ClientID%>").val();
              var remarks = $('#forexDetails' + frid + ' input[id="txtRemarks' + frid + '"]').val();
              var status = $('#forexDetails' + frid + ' select[id="ddlstatus' + frid + '"]').is(":visible");
              var isvalid = true;
              if (status) {
                  var ddlCurrency = $('#forexDetails' + frid + ' select[id="ddlstatus' + frid + '"]').val();
                  if (ddlCurrency == "-1") {
                      $('#forexDetails' + frid + ' select[id="ddlstatus' + frid + '"]').focus();
                      $('#forexDetails' + frid + ' select[id="ddlstatus' + frid + '"]').css("border-color", "Red");
                      isvalid = false;
                  }
                  else {
                      $('#forexDetails' + frid + ' select[id="ddlstatus' + frid + '"]').css("border-color", "");
                  }
              }
              var txtRemarks = $('#forexDetails' + frid + ' input[id="txtRemarks' + frid + '"]').val();
              if (txtRemarks == "") {
                  $('#forexDetails' + frid + ' input[id="txtRemarks' + frid + '"]').focus();
                  $('#forexDetails' + frid + ' input[id="txtRemarks' + frid + '"]').css("border-color", "Red");
                  isvalid = false;
              } else {
                  $('#forexDetails' + frid + ' input[id="txtRemarks' + frid + '"]').css("border-color", "");
              }
                if (isvalid) {
                    $('#btnSave' + frid).attr('disabled', true);
                  var forexrequest = {
                      AgentId: parseInt($("#<%=ddlagents.ClientID%>").val()),
                        ProductType: $("#<%=ddlProduct.ClientID%>").val() != "-1" ? $("#<%=ddlProduct.ClientID%>").val() : null,
                        Currency: $("#<%=ddlCurrency.ClientID%>").val() != "-1" ? $("#<%=ddlCurrency.ClientID%>").val() : null,
                        Name: $("#<%=txtName.ClientID%>").val() != '' ? $("#<%=txtName.ClientID%>").val() : null,
                        MobileNumber: $("#<%=txtMobileNo.ClientID%>").val() != '' ? $("#<%=txtMobileNo.ClientID%>").val() : null,
                        Email: $("#<%=txtEmail.ClientID%>").val() != '' ? $("#<%=txtEmail.ClientID%>").val() : null,
                      TravellingTo: $("#<%=txtTravelling.ClientID%>").val() != '' ? $("#<%=txtTravelling.ClientID%>").val() : null,
                        FRRefNo: $("#<%=txtRefNo.ClientID%>").val() != '' ? $("#<%=txtRefNo.ClientID%>").val() : null
                  };
                    var statusVal = status ==true? $('#forexDetails' + frid + ' select[id="ddlstatus' + frid + '"]').val():0;
                    var request = {
                        FRRID:(status ==true && statusVal =="2" && statusVal =="4") ? parseInt(frrid) : 0,
                       // FRRID: $('#forexDetails' + frid + ' select[id="ddlstatus' + frid + '"]').is(":visible") ? parseInt(frrid) : 0,
                        FRID: parseInt(frid),
                        RemarksStatus: status==true ? $('#forexDetails' + frid + ' select[id="ddlstatus' + frid + '"]').val() : $('#forexDetails' + frid + ' input[id="hdnstatus' + frid + '"]').val(),
                        Remarks: $('#forexDetails' + frid + ' input[id="txtRemarks' + frid + '"]').val()
                    };
                    $.ajax({
                        type: "POST",
                        url: "ForexQueue.aspx/saveRemarks",
                        contentType: "application/json; charset=utf-8",
                        data: "{'forexrequest':'" + JSON.stringify(forexrequest) + "','request':'" + JSON.stringify(request) + "','startDate':'" + startDate + "','endDate':'" + endDate + "'}",
                        dataType: "json",
                        async: true,
                        success: function (data) {
                            if (data.d != '') {
                                alert(data.d);
                                LoadQueueDetails();
                            }
                        },
                        error: (error) => {
                            console.log(JSON.stringify(error));
                            LoadQueueDetails();
                        }
                    });
                }
            }
        }
        function showRemarks(id) {
            if (id != undefined && id != '' && parseInt(id) > 0) {
                $('#modalHistory').modal('show');
                $('#forexHistory').html($('#remarks' + id).html());
            }
        }
    </script>
    <script>
        var cal1;
        var cal2;
        function init() {
            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select From date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select To date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            init();
            cal1.show();
            cal2.hide();
        }
        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtFromDate.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());


            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear(); cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }
            var date2 = cal2.getSelectedDates()[0];
            var depDateArray = date1.split('/');
            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            var month = date2.getMonth() + 1;
            var day = date2.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        function CheckValidDate(Day, Mn, Yr) {
            var DateVal = Mn + "/" + Day + "/" + Yr;
            var dt = new Date(DateVal);

            if (dt.getDate() != Day) {
                return false;
            }
            else if (dt.getMonth() != Mn - 1) {
                //this is for the purpose JavaScript starts the month from 0
                return false;
            }
            else if (dt.getFullYear() != Yr) {
                return false;
            }
            return (true);
        }
        YAHOO.util.Event.addListener(window, "load", init);
    </script>
    <style>
          .modal-content {
            width: 150%;
            overflow-y: auto;
        }

        .modal-body {
            height: auto;
            overflow: auto;
        }
        </style>
</asp:Content>
