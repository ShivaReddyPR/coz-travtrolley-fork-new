﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System.Collections.Generic;
using CT.Corporate;
using System.IO;

public partial class CorporateTripAnalysisUI : CT.Core.ParentPage
{
    protected List<FlightItinerary> listFlightItinerary = new List<FlightItinerary>();
    
    protected FlightItinerary flightItinerary;
    
    protected BookingDetail booking;
    protected decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0;
    protected AgentMaster agency;
    protected Airline airline;
    protected string status = string.Empty;
    protected string _approvalStatus;
    protected string approverNames = string.Empty;
    protected int approverId = 0;
    protected string agentImageSource = string.Empty;

    protected string empEmail = string.Empty;
    protected string empId = string.Empty;
    protected string empName = string.Empty;

    protected int userHLevel = 0;
    protected string approverLoginName = string.Empty;
    protected bool needToApprove;
    protected CorporateProfileTripDetails detail = null;
    protected int user_corp_profile_id = 0;

    protected void Page_PreInit(object sender, EventArgs e)
    {        
        if (Request.QueryString["appStatus"] != null)
        {
            try
            {
                if (Request.QueryString["appLoginName"] != null)
                {
                    approverLoginName = GenericStatic.DecryptData(Request.QueryString["appLoginName"].Replace(" ", "+"));

                    string password = UserMaster.GetPasswordByLoginName(approverLoginName);
                    if (!UserMaster.IsAuthenticatedUser(approverLoginName, password, 1))
                    {
                        Response.Redirect("AbandonSession.aspx");
                    }

                    //if (!IsPostBack)
                    {
                        try
                        {
                            string hostName = BookingUtility.ExtractSiteName(Request["HTTP_HOST"]);

                            string[] themeDetails = AgentMaster.GetThemByDoamin(hostName);
                            if (!string.IsNullOrEmpty(themeDetails[1]))
                                Session["themeName"] = themeDetails[1];
                            else Session["themeName"] = "Default";
                            Label lblCopyRight = (Label)this.Master.FindControl("lblCopyRight");
                            if (!string.IsNullOrEmpty(themeDetails[2]))
                            {
                                lblCopyRight.Text = themeDetails[2];
                            }
                            Image imgLogo = (Image)this.Master.FindControl("imgLogo");

                            if (!string.IsNullOrEmpty(themeDetails[0]))
                            {
                                string logoPath = ConfigurationManager.AppSettings["AgentImage"] + themeDetails[0];
                                imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + logoPath;
                            }

                            string themeName = (string)Session["themeName"];
                            if (themeName != null)
                            {
                                this.Page.Theme = themeName;
                            }
                            else
                            {
                                this.Page.Theme = "Default";
                            }
                        }
                        catch (Exception ex)
                        {
                            CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Dom Error:" + ex.ToString(), "");
                        }

                    }
                }
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to authenticate thru email. Reason:  "+ex.ToString(), getExternalIp());
            }
        }
        else 
        {
            if (Session["themeName"] != null)
            {
                string themeName = (string)Session["themeName"];
                if (themeName != null)
                {
                    this.Page.Theme = themeName;
                }
                else
                {
                    this.Page.Theme = "Default";
                }
            }
            else
            {
                this.Page.Theme = "Default";
            }
        }
    }

    protected string getExternalIp()
    {
        try
        {
            string externalIP;
            externalIP = (new System.Net.WebClient()).DownloadString("http://checkip.dyndns.org/");
            externalIP = (new System.Text.RegularExpressions.Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                         .Matches(externalIP)[0].ToString();
            return externalIP;
        }
        catch { return null; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                
                    user_corp_profile_id = Settings.LoginInfo.CorporateProfileId;
                    List<int> delegateProfileId = new List<int>();
                if (Request["flightId"] != null && Settings.LoginInfo.IsCorporate == "Y")
                {

                    detail = new CorporateProfileTripDetails(Convert.ToInt32(Request["flightId"]));
                    //Get the approver hierarchy if only the status is in "P" status
                    userHLevel = Convert.ToInt32(detail.ProfileApproversList.Where(i => i.ApproverId == Settings.LoginInfo.CorporateProfileId ).Select(i => i.Hierarchy).FirstOrDefault());
                    hdnHLevel.Value = Convert.ToString(userHLevel);
                    //Avoid approval hierarchy by passing
                    if (userHLevel > 1 && detail.ProfileApproversList.Exists(x => x.Hierarchy == userHLevel - 1 && x.ApprovalStatus != "Awaiting Approval") || userHLevel == 1)
                    {
                        if (!detail.ProfileApproversList.Exists(x => x.ApproverId == user_corp_profile_id))
                        {
                            foreach (CorpProfileApproval _profileApprover in detail.ProfileApproversList)
                            {
                                CorporateProfile delegateProfile = new CorporateProfile(_profileApprover.ApproverId, Settings.LoginInfo.AgentId);
                                if (user_corp_profile_id == delegateProfile.DeligateSupervisor)
                                {
                                    user_corp_profile_id = _profileApprover.ApproverId;
                                    break;
                                }
                            }


                        }

                        if (Request.QueryString["TripId"] != null && detail.ProfileApproversList.Exists(x => x.ApproverId == user_corp_profile_id))
                        {
                            int approvers = 1; string appStatus = string.Empty;
                            GetCorporateBookings(Convert.ToString(Request["TripId"]));
                            foreach (FlightItinerary itinerary in listFlightItinerary)
                            {
                                CorporateProfileTripDetails tripDetails = new CorporateProfileTripDetails(itinerary.FlightId);
                                approvers = tripDetails.ProfileApproversList.Count;
                                if (tripDetails.ProfileApproversList.FindAll(x => x.ApprovalStatus == "Approved").Count == tripDetails.ProfileApproversList.Count)
                                    appStatus += appStatus.Length > 1 ? ",Approved" : "Approved";
                                else if (tripDetails.ProfileApproversList.FindAll(x => x.ApprovalStatus == "Rejected").Count == tripDetails.ProfileApproversList.Count)
                                    appStatus += appStatus.Length > 1 ? ",Rejected" : "Rejected";
                                else
                                    appStatus += appStatus.Length > 1 ? ",Pending" : "Pending";
                            }
                            if (!appStatus.Contains("Pending"))
                                btnApprove.Visible = btnReject.Visible = false;

                            if (appStatus.Split(',').ToList().All(x => x == "Approved"))
                                status = "Approved";
                            else if (appStatus.Split(',').ToList().All(x => x == "Rejected"))
                                status = "Rejected";
                            else
                                status = "Pending";

                        }

                        if (Request.QueryString["appStatus"] != null)
                        {
                            needToApprove = Request.QueryString["appStatus"] == "A" ? true : false;

                            if (needToApprove)
                            {
                                btnApprove.Visible = true;
                                btnReject.Visible = false;
                            }
                            else
                            {
                                btnApprove.Visible = false;
                                btnReject.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        CorpProfileApproval profileApproval = detail.ProfileApproversList.Find(x => x.Hierarchy == (userHLevel - 1));
                        if (profileApproval != null)
                            lblSuccessMsg.Text = "You are not allowed to Approve/Reject before your Hierarchy (" + profileApproval.ApproverEmail + " - " + profileApproval.ApprovalStatus + ") Level " + (userHLevel - 1);
                    }
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateTripAnalysisUI) Page Load Event Error: " + ex.ToString(), getExternalIp());
        }

    }

    private void GetCorporateBookings(string TripId)
    {
        try
        {
            //string bookingType = string.Empty;
            //if (Request["bookingType"] != null)
            //{
            //    bookingType = Request["bookingType"];
            //    if (bookingType != "Routing")
            //        bookingType = string.Empty;
            //}

            DataTable dtBookings = CorporateProfileExpenseDetails.GetCorporateBookings(TripId);
            if (dtBookings != null && dtBookings.Rows.Count > 0)
            {
                foreach (DataRow dr in dtBookings.Rows)
                {
                    //if (bookingType != "Routing")
                    {
                        if (dr["flightId"] != DBNull.Value)
                        {
                            FlightItinerary flightItinerary = new FlightItinerary(Convert.ToInt32(dr["flightId"]));

                            listFlightItinerary.Add(flightItinerary);
                        }
                    }
                    //else
                    //{
                    //    if (dr["FlightId"] != DBNull.Value)
                    //    {
                    //        CorpFlightItinerary itinerary = new CorpFlightItinerary(Convert.ToInt32(dr["FlightId"]));
                            
                    //        listOfflineItinerary.Add(itinerary);
                    //    }
                    //}
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        try
        {
            updateStatus("A");
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            //lblMasterError.Text = "Trip Approved Successfully";
            btnApprove.Visible = false;
            btnReject.Visible = false;
            Response.Redirect("CorporateTripAnalysis.aspx?" + Request.QueryString.ToString().Replace("=P", "=A").Replace("appStatus=A", "").Replace("appStatus=R", ""), false);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateTripAnalysisUI)btnApprove_Click Event .Error:" + ex.ToString(), getExternalIp());
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = "Error occured while saving:" + ex.ToString();
        }
    }


    protected void btnReject_Click(object sender, EventArgs e)
    {
        try
        {
            updateStatus("R");
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            //lblMasterError.Text = "Trip Rejected Successfully";
            btnApprove.Visible = false;
            btnReject.Visible = false;
            Response.Redirect("CorporateTripAnalysis.aspx?" + Request.QueryString.ToString().Replace("=P", "=R").Replace("appStatus=A", "").Replace("appStatus=R", ""), false);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateTripAnalysisUI)btnReject_Click Event .Error:" + ex.ToString(), "0");
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = "Error occured while saving:" + ex.ToString();
        }
    }

    private void updateStatus(string approverStatus)
    {

        try
        {
            Audit.Add(EventType.Login, Severity.High, (int)Settings.LoginInfo.UserID, "Trip Approval Request for " + Request["flightId"], getExternalIp());
            detail = new CorporateProfileTripDetails(Convert.ToInt32(Request["flightId"]));

            if (detail.ProfileApproversList.Exists(x => x.ApproverId == user_corp_profile_id && x.ApprovalStatus == "Awaiting Approval"))
            {
                string remarks = string.Empty;
                int selectedTripId = 0;
                if (txtRemarks.Text.Length > 0 && approverStatus == "R")
                {
                    remarks = txtRemarks.Text;
                }
                if (Request["flightId"] != null)
                {
                    //selectedTripId = Convert.ToInt32(hdfExpDetailId.Value);
                    hdfExpDetailId.Value = string.IsNullOrEmpty(hdfExpDetailId.Value) ? Request["flightId"] : hdfExpDetailId.Value;
                    selectedTripId = Convert.ToInt32(hdfExpDetailId.Value);
                }
                //Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "ziyad"+ selectedTripId + "," +approverStatus+","+Settings.LoginInfo.CorporateProfileId.ToString()+","+ remarks+","+ selectedTripId.ToString(), Request["REMOTE_ADDR"]);
                CorporateProfileTripDetails.UpdateTripApprovalStatus(selectedTripId, approverStatus, (int)Settings.LoginInfo.CorporateProfileId, remarks, selectedTripId);
                lblSuccessMsg.Visible = true;
                //listFlightItinerary = FlightItinerary.GetCorpItineraries(Convert.ToInt32(Request.QueryString["flightId"]));

                CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
                mse.SettingsLoginInfo = Settings.LoginInfo;
                mse.AppUserId = (int)Settings.LoginInfo.UserID;

                UserMaster loggedMember = new UserMaster(Settings.LoginInfo.UserID);
                string emailHtmlEmployee = string.Empty, emailHtmlApproval = string.Empty, emailHtmlLocation = string.Empty;
                string subjectApproval = string.Empty, subjectRejected = string.Empty;
                string header = string.Empty, footer = "</div>";
                List<string> mailAddresses = new List<string>();
                List<string> approvalMailAddresses = new List<string>();
                //lblSuccessMsg.Text = "Reference No Updated Successfully";
                string flightSegments = string.Empty, flexFields = string.Empty; 

                //Get second level approver email's
                //int hLevel = Convert.ToInt32(detail.ProfileApproversList.Where(i => i.ApproverId == Settings.LoginInfo.CorporateProfileId).Select(i => i.Hierarchy).FirstOrDefault());
                int hLevel = Convert.ToInt32(detail.ProfileApproversList.Where(i => i.ApproverId == user_corp_profile_id).Select(i => i.Hierarchy).FirstOrDefault());
                List<int> listOfAppId = detail.ProfileApproversList.Where(i => i.Hierarchy == (hLevel + 1)).Select(i => i.ApproverId).ToList();
                if (listOfAppId != null && listOfAppId.Count > 0)
                {
                    listOfAppId.ForEach(appId =>
                    {
                        approverId = appId;
                        string appEmail = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverEmail).FirstOrDefault();
                        if (!string.IsNullOrEmpty(appEmail))
                            approvalMailAddresses.Add(appEmail);
                    });
                }

           
                 //check  'L' get Email ids from  location_master and send email            
                    LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.LocationID);
                if (!string.IsNullOrEmpty(locationMaster.LocationEmail) && locationMaster.LocationEmail.Contains("@"))
                {
                    if (locationMaster.LocationEmail.Contains(","))
                    {
                        string[] emails = locationMaster.LocationEmail.Split(',');
                        for (int i = 0; i < emails.Length; i++)
                        {
                            mailAddresses.Add(emails[i]);
                        }
                    }
                    else
                        mailAddresses.Add(locationMaster.LocationEmail);
                }
                

                //Add employee email
                if (!string.IsNullOrEmpty(detail.EmpEmail))
                    mailAddresses.Add(detail.EmpEmail);

                if (approverStatus == "A")//Approved
                {
                    try
                    {
                        int counter = 0;
                        flightItinerary = listFlightItinerary.Find(x => x.FlightId == selectedTripId);
                        if (flightItinerary.Passenger[0].FlexDetailsList != null && flightItinerary.Passenger[0].FlexDetailsList.Count > 0)
                        {
                            //Add heading Reporting Fields
                            flexFields += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<td colspan=\"1\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word\">";
                            flexFields += "<table class=\"main-header\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                            flexFields += "<strong style=\"font-weight: 600\">Reporting Fields:</strong>";
                            flexFields += "</td></tr></tbody></table></td></tr></tbody></table>";
                        }
                        listFlightItinerary.ForEach(x =>
                        {                            
                            FlightPolicy policy = new FlightPolicy();
                            policy.Flightid = x.FlightId;
                            policy.GetPolicyByFlightId();
                            if (counter == 0)
                                header = "<div style='background: #388c0f;color:white;font-size:16px'><div style='-webkit-box-flex:0;flex:0 0 100%;max-width:100%'><div style='-webkit-box-flex:0;flex:0 0 41.66667%;max-width:41.66667%'><span >Employee Selected</span>&nbsp; PNR : " + x.PNR + "</div><div style='-webkit-box-flex:0;flex:0 0 58.33333%;max-width:58.33333%'>Approval Status : <strong style='font-weight: 600'>" + (x.FlightId == selectedTripId ? (listOfAppId.Count == 0 ? "Approved" : "Awaiting Approval") : "Rejected") + "</strong>&nbsp;<span>Policy Compliance : " + (policy.IsUnderPolicy ? "Inside Policy" : "Outside Policy") + "</span></div></div>";
                            else
                            {
                                header = "<br/><div style='background: #6d6b6c;color:white;font-size:16px'><div style='-webkit-box-flex:0;flex:0 0 100%;max-width:100%'><div style='-webkit-box-flex:0;flex:0 0 41.66667%;max-width:41.66667%'><span >Option " + counter + "</span>&nbsp; PNR : " + x.PNR + "</div><div style='-webkit-box-flex:0;flex:0 0 58.33333%;max-width:58.33333%'>Approval Status : <strong style='font-weight: 600'>" + (x.FlightId == selectedTripId ? (listOfAppId.Count == 0 ? "Approved" : "Awaiting Approval") : "Rejected") + "</strong>&nbsp;<span>Policy Compliance : " + (policy.IsUnderPolicy ? "Inside Policy" : "Outside Policy") + "</span></div></div>";
                                subjectApproval = subjectRejected;
                            }
                            flightItinerary = x;
                            subjectApproval = DateTime.Now.ToString("dd-MMM-yyyy") + " Trip Request Approval – " + flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName + " – " + flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName;
                            subjectRejected = DateTime.Now.ToString("dd-MMM-yyyy") + " Trip Request – " + flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName + " – " + flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName;
                            string segments = string.Empty;//Combine all flight details in flightSegments string to send for Employee or Location email, Approver's email
                            emailHtmlEmployee += SendEmail("E", (approvalMailAddresses.Count == 0 ? approverStatus : "Awaiting Approval"), x.FlightId, string.Empty, header, footer, ref flightSegments);
                            emailHtmlApproval += SendEmailForApprover("A", Settings.LoginInfo.CorporateProfileId, Settings.LoginInfo.AgentId, x.FlightId, header, footer, ref segments);//To the approver with hierarchy level one               

                            if (counter == 0)
                            {
                                //show all flex fields for first booking only
                                if (flightItinerary.Passenger[0].FlexDetailsList != null && flightItinerary.Passenger[0].FlexDetailsList.Count > 0)
                                {
                                    flexFields += "<table class=\"bottom-table-style\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\"><tbody>";
                                    flightItinerary.Passenger[0].FlexDetailsList.ForEach(flex =>
                                    {
                                        flexFields += "<tr class=\"table-cnt-inner\" style=\"padding: 0; text-align: left; vertical-align: top\">";
                                        flexFields += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">" + flex.FlexLabel + ":</td>";
                                        flexFields += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">" + flex.FlexData + "</td></tr>";
                                    });
                                    flexFields += "</tbody></table>";
                                }
                            }

                            counter++;
                        });

                        //Add the PNR of the first booking as shown in the screen
                        if (!String.IsNullOrEmpty(flightItinerary.PNR))
                        {
                            flexFields += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<td colspan=\"1\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word\">";
                            flexFields += "<table class=\"main-header\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                            flexFields += "<strong style=\"font-weight: 600\">PNR:</strong>";
                            flexFields += "</td></tr></tbody></table></td></tr></tbody></table>";
                            flexFields += "<table class=\"bottom-table-style\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<tr class=\"table-cnt-inner\" style=\"padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\"> PNR:</td>";
                            flexFields += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">"+flightItinerary.PNR+"</td></tr>";

                            if (!flightItinerary.IsLCC && flightItinerary.AirLocatorCode != null)
                            {
                                flexFields += "<tr><td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">GDS PNR:</td>";
                                flexFields += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">"+flightItinerary.AirLocatorCode+"</td></tr>";
                            }
                            flexFields += "</table>";
                        }

                        flexFields = flexFields.Replace("< ", "<").Replace(" >", ">");

                        //Replace the html place holder for flight details with flightSegments html
                        emailHtmlApproval = emailHtmlApproval.Replace("%flightSegments%", flightSegments).Replace("%flexFields%", flexFields);
                        emailHtmlEmployee = emailHtmlEmployee.Replace("%flightSegments%", flightSegments).Replace("%flexFields%", flexFields); 

                        //Since Employee and location same email content we will send email at once
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], mailAddresses.Distinct().ToList(), subjectRejected, emailHtmlEmployee, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                        if (approvalMailAddresses.Count > 0 && !string.IsNullOrEmpty(emailHtmlApproval))
                            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], approvalMailAddresses.Distinct().ToList(), subjectApproval, emailHtmlApproval, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);

                        if (hLevel == 1)
                        {
                            //Release all booked itineraries except approved one
                            var itineries = listFlightItinerary.FindAll(x => x.FlightId != selectedTripId);
                            ReleaseItineraries(itineries, mse, loggedMember);
                        }
                    }
                    catch (Exception ex) { Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to Approve Bookings. Reason:" + ex.ToString(), getExternalIp()); }


                }
                else//Rejected -- So no need to trigger any email to the next level approver
                {
                    try
                    {
                        int counter = 0;
                        flightItinerary = listFlightItinerary[0];
                        if (flightItinerary.Passenger[0].FlexDetailsList != null && flightItinerary.Passenger[0].FlexDetailsList.Count > 0)
                        {
                            //Add heading Reporting Fields
                            flexFields += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<td colspan=\"1\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word\">";
                            flexFields += "<table class=\"main-header\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                            flexFields += "<strong style=\"font-weight: 600\">Reporting Fields:</strong>";
                            flexFields += "</td></tr></tbody></table></td></tr></tbody></table>";
                        }
                        listFlightItinerary.ForEach(x =>
                        {
                            FlightPolicy policy = new FlightPolicy();
                            policy.Flightid = x.FlightId;
                            policy.GetPolicyByFlightId();
                            if (counter == 0)
                                header = "<div style='background: #388c0f;color:white;font-size:16px'><div style='-webkit-box-flex:0;flex:0 0 100%;max-width:100%'><div style='-webkit-box-flex:0;flex:0 0 41.66667%;max-width:41.66667%'><span >Employee Selected</span>&nbsp; PNR : " + x.PNR + "</div><div style='-webkit-box-flex:0;flex:0 0 58.33333%;max-width:58.33333%'>Approval Status : <strong style='font-weight: 600'>Rejected</strong>&nbsp;<span>Policy Compliance : " + (policy.IsUnderPolicy ? "Inside Policy" : "Outside Policy") + "</span></div></div>";
                            else
                            {
                                header = "<br/><div style='background: #6d6b6c;color:white;font-size:16px'><div style='-webkit-box-flex:0;flex:0 0 100%;max-width:100%'><div style='-webkit-box-flex:0;flex:0 0 41.66667%;max-width:41.66667%'><span >Option " + counter + "</span>&nbsp; PNR : " + x.PNR + "</div><div style='-webkit-box-flex:0;flex:0 0 58.33333%;max-width:58.33333%'>Approval Status : <strong style='font-weight: 600'>Rejected</strong>&nbsp;<span>Policy Compliance : " + (policy.IsUnderPolicy ? "Inside Policy" : "Outside Policy") + "</span></div></div>";                             
                            }

                            flightItinerary = x;
                            emailHtmlEmployee += SendEmail("E", approverStatus, x.FlightId, remarks, header, footer, ref flightSegments);
                            if (counter == 0)
                            {
                                //show all flex fields for first booking only
                                if (flightItinerary.Passenger[0].FlexDetailsList != null && flightItinerary.Passenger[0].FlexDetailsList.Count > 0)
                                {
                                    flexFields += "<table class=\"bottom-table-style\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\"><tbody>";
                                    flightItinerary.Passenger[0].FlexDetailsList.ForEach(flex =>
                                    {
                                        flexFields += "<tr class=\"table-cnt-inner\" style=\"padding: 0; text-align: left; vertical-align: top\">";
                                        flexFields += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">" + flex.FlexLabel + ":</td>";
                                        flexFields += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">" + flex.FlexData + "</td></tr>";
                                    });
                                    flexFields += "</tbody></table>";
                                }
                            }

                            counter++;
                        });

                        //Add the PNR of the first booking as shown in the screen
                        if (!String.IsNullOrEmpty(flightItinerary.PNR))
                        {
                            flexFields += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<td colspan=\"1\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word\">";
                            flexFields += "<table class=\"main-header\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                            flexFields += "<strong style=\"font-weight: 600\">PNR:</strong>";
                            flexFields += "</td></tr></tbody></table></td></tr></tbody></table>";
                            flexFields += "<table class=\"bottom-table-style\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<tr class=\"table-cnt-inner\" style=\"padding: 0; text-align: left; vertical-align: top\">";
                            flexFields += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\"> PNR:</td>";
                            flexFields += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">" + flightItinerary.PNR + "</td></tr>";

                            if (!flightItinerary.IsLCC && flightItinerary.AirLocatorCode != null)
                            {
                                flexFields += "<tr><td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">GDS PNR:</td>";
                                flexFields += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">" + flightItinerary.AirLocatorCode + "</td></tr>";
                            }
                            flexFields += "</table>";
                        }

                        flexFields = flexFields.Replace("< ", "<").Replace(" >", ">");

                        //Replace the html place holder for flight details with flightSegments html
                        emailHtmlEmployee = emailHtmlEmployee.Replace("%flightSegments%", flightSegments).Replace("%flexFields%", flexFields);

                        subjectRejected = DateTime.Now.ToString("dd-MMM-yyyy") + " Trip Request Rejected – " + flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName + " – " + flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName;

                        //Since Employee and location same email content we will send email at once
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], mailAddresses.Distinct().ToList(), subjectRejected, emailHtmlEmployee, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);

                        //Release all booked itineraries
                        if (hLevel == 1)
                            ReleaseItineraries(listFlightItinerary, mse, loggedMember);
                        else//Release selected booking rejected by second or third approver. example 1st Approver - Approved, 2nd Approver - Rejected
                        {
                            var itineries = listFlightItinerary.FindAll(x => x.FlightId == selectedTripId);
                            ReleaseItineraries(itineries, mse, loggedMember);
                        }
                    }
                    catch (Exception ex) { Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to Reject Bookings. Reason:" + ex.ToString(), getExternalIp()); }
                }
                btnApprove.Visible = false;
                btnReject.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    protected string SendEmailForApprover(string toEmpOrApp, int profileId, int agentId, int flightId, string header, string footer, ref string flightSegments)
    {
        string emailHtml = string.Empty;

        
        string approverStatus = string.Empty;
        //int user_corp_profile_id = profileId;        
        string serverPath = Request.Url.Scheme + "://ctb2bstage.cozmotravel.com";
        string logoPath = "";

        if (agentId > 1)
        {
            if (!string.IsNullOrEmpty(new AgentMaster(agentId).ImgFileName))
            {
                logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"] + new AgentMaster(agentId).ImgFileName;
                agentImageSource = logoPath;
            }
        }
        else
        {
            agentImageSource = serverPath + "/images/logo.jpg";
        }
        //Audit.Add(EventType.Email, Severity.High, 1, "===========3===============", "");
        if (flightId > 0)
        {
            //Audit.Add(EventType.Email, Severity.High, 1, "===========4===============", "");
            detail = new CorporateProfileTripDetails(flightId);
            //Retrieve the current trip status before sending email for employee and approver
            //If in case it is auto approved no need to send email to approver and also show the current trip status in email
            //added product Id as param to fetch the status. 
            approverStatus = FlightPolicy.GetTripApprovalStatus(flightId, (int)ProductType.Flight);
            try
            {
                if (approverStatus == "A")
                {
                    _approvalStatus = "Approved";
                }
                else if (approverStatus == "P")
                {
                    _approvalStatus = "Awaiting Approval";
                }
                else
                {
                    _approvalStatus = "Rejected";
                }

                if (toEmpOrApp == "A" && approverStatus == "P")//To the approver, Send email to approver only if the trip status is P (Awaiting Approval)
                {
                    // Audit.Add(EventType.Email, Severity.High, 1, "===========6===============", "");
                    empId = detail.EmpId;
                    empName = detail.EmpName;

                    int hLevel = Convert.ToInt32(detail.ProfileApproversList.Where(i => i.ApproverId == user_corp_profile_id).Select(i => i.Hierarchy).FirstOrDefault());
                    List<int> listOfAppId = detail.ProfileApproversList.Where(i => i.Hierarchy == (hLevel + 1)).Select(i => i.ApproverId).ToList();
                    if (listOfAppId != null && listOfAppId.Count > 0)
                    {
                        foreach (int appId in listOfAppId)
                        {
                            approverNames = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverName).FirstOrDefault();
                            approverId = appId;
                            // Audit.Add(EventType.Email, Severity.High, 1, "===========7===============", "");
                            string appEmail = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverEmail).FirstOrDefault();
                            if (!string.IsNullOrEmpty(appEmail))
                            {

                                Hashtable hashtable = new Hashtable();
                                string subject = string.Empty;

                                StreamReader reader = new StreamReader(Server.MapPath("~/CorporateEmail-2-Approver.html"));
                                emailHtml = reader.ReadToEnd();
                                reader.Close();

                                FlightPolicy flightPolicy = new FlightPolicy();
                                List<FlightPolicy> flightPolicies = new List<FlightPolicy>();

                                flightPolicy.Flightid = flightItinerary.FlightId;
                                flightPolicy.GetPolicyByFlightId();
                                flightPolicies.Add(flightPolicy);
                                subject = DateTime.Now.ToString("dd-MMM-yyyy") + " Trip Approval Request – " + flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName + " – " + flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName;

                                //string url = Request.IsSecureConnection ? "https://" : "http://" + Request["HTTP_HOST"];
                                string url = Request.Url.Scheme + "://" + Request["HTTP_HOST"];

                                //Get the login name of first approver in order approve or reject from email
                                string userLoginName = UserMaster.GetLoginNameForCorpProfile(appId);

                                hashtable.Add("requestDate", DateTime.Now.ToString("dd-MMM-yyyy"));
                                hashtable.Add("employee", flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName);
                                hashtable.Add("departureCityName", flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName);
                                hashtable.Add("tripReference", flightItinerary.TripId);
                                hashtable.Add("rootUrl", url);
                                hashtable.Add("approverId", approverId);
                                hashtable.Add("expDetailId", flightId);
                                hashtable.Add("employeeID", detail.EmpId);
                                hashtable.Add("appLoginName", GenericStatic.EncryptData(userLoginName));//Assign for approver validation
                                hashtable.Add("approvalStatus", _approvalStatus);

                                AgentAppConfig appConfig = new AgentAppConfig();
                                appConfig.AgentID = Settings.LoginInfo.AgentId;
                                List<AgentAppConfig> appConfigs = appConfig.GetConfigData();
                                double deadline = 5;//Default 5 Hours
                                if (appConfigs.Count > 0)
                                {
                                    AgentAppConfig config = appConfigs.Find(ac => ac.AppKey == "CorpApprovalDeadline");
                                    if (config != null)
                                        deadline = Convert.ToDouble(config.AppValue);
                                }
                                hashtable.Add("approvalDeadline", DateTime.Now.AddHours(Convert.ToDouble(deadline)).ToString("dd-MMM-yyyy hh:mm:ss tt"));//TODO: insert into app config table and assign

                                if (flightPolicies.Count > 0)
                                {
                                    CorporateTravelReason travelReason = new CorporateTravelReason(flightPolicies[0].TravelReasonId);
                                    CorporateTravelReason policyReason = new CorporateTravelReason();
                                    if (flightPolicies[0].PolicyReasonId > 0)
                                        policyReason = new CorporateTravelReason(flightPolicies[0].PolicyReasonId);

                                    hashtable.Add("travelReason", travelReason.Description);
                                    hashtable.Add("policyCompliance", flightPolicies[0].IsUnderPolicy ? "Inside Policy" : "Outside Policy - " + flightPolicies[0].PolicyBreakingRules);//Inside or outside policy with policy breaking rule
                                    hashtable.Add("bookingReason", !string.IsNullOrEmpty(policyReason.Description) && !flightPolicies[0].IsUnderPolicy ? "<strong style=\"font - weight: 600\">Violation Reason:</ strong > " + policyReason.Description : "");
                                }

                                string approvers = string.Empty;
                                List<CorpProfileApproval> approvals = CorpProfileApproval.RemoveDuplicatesIterative(detail.ProfileApproversList);

                                for (int i = 0; i < approvals.Count; i++)
                                {
                                    if (!string.IsNullOrEmpty(approvals[i].ApproverEmail) && approvals[i].ApproverId != detail.ProfileId)
                                    {
                                        string approverEmail = approvals[i].ApproverEmail;
                                        string approvalStatus = approvals[i].ApprovalStatus;
                                        approvers += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                        approvers += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                        approvers += "<strong style =\"font-weight: 600\">Approver" + (i + 1) + "</strong>";
                                        approvers += "</td>";
                                        approvers += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                        approvers += approverEmail + "&nbsp;&nbsp;&nbsp;&nbsp; <span style=\"color: green\">Status: <strong>" + approvalStatus + "</strong></span>";
                                        approvers += "</td>";
                                        approvers += "</tr>";
                                    }
                                }

                                hashtable.Add("approvers", approvers);
                                string fallBackApprover = string.Empty;
                                try
                                {
                                    List<CorpProfileApproval> approvalCount = CorpProfileApproval.RemoveDuplicatesIterative(detail.ProfileApproversList);
                                    List<CorpProfileApproval> fallBackApprovers = detail.ProfileApproversList.Except(approvalCount).ToList();
                                    fallBackApprovers = (from r in fallBackApprovers orderby r.Hierarchy select r).ToList();
                                    int counter = 0;
                                    foreach (CorpProfileApproval corpProfile in fallBackApprovers)
                                    {
                                        if (!string.IsNullOrEmpty(corpProfile.ApproverEmail))
                                        {
                                            fallBackApprover += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                            fallBackApprover += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                            fallBackApprover += "<strong style =\"font-weight: 600\">Fall Back Approver " + corpProfile.Hierarchy + " - " + (counter + 1) + "</strong>";
                                            fallBackApprover += "</td><td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                            fallBackApprover += corpProfile.ApproverEmail + "&nbsp;&nbsp;&nbsp;&nbsp; <span style=\"color: green\">Status: <strong>" + corpProfile.ApprovalStatus + "</strong></span>";
                                            fallBackApprover += "</td></tr>";
                                            counter++;
                                        }
                                    }

                                }
                                catch { }
                                finally { hashtable.Add("fallBackApprover", fallBackApprover); }


                                #region Flight Segments

                                int segments = flightItinerary.Segments.Length;
                                List<string> meals = new List<string>();
                                List<decimal> mealPrices = new List<decimal>();
                                List<string> baggages = new List<string>();
                                List<decimal> baggagePrices = new List<decimal>();
                                List<string> seats = new List<string>();
                                List<decimal> seatPrices = new List<decimal>();

                                flightItinerary.Passenger.ToList().ForEach(p =>
                                {
                                    string meal = string.Empty, seat = string.Empty, bag = string.Empty;
                                    for (int i = 0; i < segments; i++)
                                    {
                                        meal += meal.Length > 0 ? ", No Meal Booked" : "No Meal Booked";
                                        seat += seat.Length > 0 ? ", No Seat Booked" : "No Seat Booked";
                                        bag += bag.Length > 0 ? ", No Bag" : "No Bag";
                                    }
                                    meals.Add(string.IsNullOrEmpty(p.MealDesc) ? meal : p.MealDesc);
                                    mealPrices.Add(p.Price.MealCharge);
                                    baggages.Add(string.IsNullOrEmpty(p.BaggageCode) ? bag : p.BaggageCode);
                                    baggagePrices.Add(p.Price.BaggageCharge);
                                    seats.Add(string.IsNullOrEmpty(p.Seat.Code) ? seat : p.Seat.Code);
                                    seatPrices.Add(p.Price.SeatPrice);
                                });


                                string airlineName = string.Empty, airlineCode = string.Empty, flightNumber = string.Empty, departureCityName = string.Empty, departureAirportName = string.Empty, departureTerminal = string.Empty;
                                string departureDate = string.Empty, departureWeekDay = string.Empty, departureTime = string.Empty;
                                string arrivalCityName = string.Empty, arrivalDate = string.Empty, arrivalWeekDay = string.Empty, arrivalTime = string.Empty,
                                    arrivalAirportName = string.Empty, arrivalTerminal = string.Empty, Stops = string.Empty, Duration = string.Empty;
                                string Currency = flightItinerary.Passenger[0].Price.Currency;
                                decimal bookingAmount = 0;
                                int decimalPoint = 2;

                                bookingAmount = flightItinerary.Passenger.ToList().Sum(p => p.Price.PublishedFare + p.Price.Tax + p.Price.Markup +
                                p.Price.BaggageCharge + p.Price.OtherCharges + p.Price.HandlingFeeAmount + p.Price.OutputVATAmount +
                                p.Price.MealCharge + p.Price.SeatPrice + p.Price.AsvAmount - p.Price.Discount);

                                bookingAmount = flightItinerary.FlightBookingSource == BookingSource.TBOAir ? Math.Ceiling(bookingAmount) : bookingAmount;
                                decimalPoint = flightItinerary.Passenger[0].Price.DecimalPoint;

                                flightSegments += header;
                                //flightSegments += "< table class='spacer' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%'><tbody><tr style = 'padding: 0; text-align: left; vertical-align: top' >< td height='10px' style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word'>&#xA0;</td></tr></tbody></table>";
                                flightSegments += "<table class='flight-table' style='border-bottom: 1px solid #e4e4e4; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%'>";
                                flightSegments += "< tr style = 'padding: 0; text-align: left; vertical-align: top' >";
                                flightSegments += "< th style='Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Airline</th>";
                                flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Departure</th>";
                                flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Arrival</th>";
                                flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Stops</th>";
                                flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Duration</th>";
                                flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Price</th>";
                                flightSegments += "</tr>";


                                for (int i = 0; i < segments; i++)
                                {

                                    FlightInfo flight = flightItinerary.Segments[i];
                                    Airline airline = new Airline();
                                    airline.Load(flight.Airline);
                                    airlineName = airline.AirlineName; airlineCode = flight.Airline; flightNumber = flight.FlightNumber;
                                    departureCityName = flight.Origin.CityName; departureAirportName = flight.Origin.AirportName;
                                    departureTerminal = flight.DepTerminal; departureDate = flight.DepartureTime.ToString("dd MMM yyyy");
                                    departureWeekDay = flight.DepartureTime.DayOfWeek.ToString().Substring(0, 3);
                                    departureTime = flight.DepartureTime.ToString("hh:mm tt");
                                    arrivalCityName = flight.Destination.CityName; arrivalDate = flight.ArrivalTime.ToString("dd MMM yyyy");
                                    arrivalWeekDay = flight.ArrivalTime.DayOfWeek.ToString().Substring(0, 3);
                                    arrivalTime = flight.ArrivalTime.ToString("hh:mm tt");
                                    arrivalAirportName = flight.Destination.CityName; arrivalTerminal = flight.ArrTerminal;

                                    Stops = flight.Stops == 0 ? "Non Stop" : flight.Stops == 1 ? "One Stop" : "Two Stops";
                                    Duration = flight.Duration.ToString();


                                    flightSegments += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                    flightSegments += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">" + airlineName + "<br />" + airlineCode + " " + flightNumber + " </td>";
                                    flightSegments += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    flightSegments += "<strong>" + departureCityName + "</ strong >< br /> " + departureDate + "< br /> (" + departureWeekDay + ")," + departureTime + ",< br /> Airport:" + departureAirportName + ",< br /> Terminal: " + departureTerminal + "</ td >";
                                    flightSegments += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    flightSegments += "<strong>" + arrivalCityName + "</ strong >< br />" + arrivalDate + "< br /> (" + arrivalWeekDay + ")," + arrivalTime + ",< br /> Airport:" + arrivalAirportName + ", Terminal: " + arrivalTerminal + "</ td >";
                                    flightSegments += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\"> " + Duration + " </td>";
                                    flightSegments += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\"> " + Stops + " </td>";
                                    if (i == 0)
                                    {
                                        flightSegments += "<td rowspan =\"" + (flightItinerary.Segments.Length + 5).ToString() + "\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; border-left: 1px solid #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: center; vertical-align: middle; word-wrap: break-word\">";
                                        flightSegments += "<strong>" + Currency + " " + bookingAmount.ToString("N" + decimalPoint) + "</strong></td>";
                                    }
                                    flightSegments += "</tr>";
                                    flightSegments += "<tr>";
                                    flightSegments += "<td colspan =\"5\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    flightSegments += "<table class=\"table-main-content\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                                    flightSegments += "<tbody>";
                                    flightSegments += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                    if (mealPrices.Sum() > 0)
                                    {
                                        for (int j = 0; j < meals.Count; j++)
                                        {
                                            flightSegments += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                            flightSegments += "<strong style =\"font-weight: 600\">Meal:</strong>" + meals[j] + " [" + Currency + " " + mealPrices[j].ToString("N" + decimalPoint) + "]</td>";
                                        }
                                    }
                                    flightSegments += "</tr> ";
                                    flightSegments += "< tr style =\"padding: 0; text-align: left; vertical-align: top\">";

                                    if (seatPrices.Sum() > 0)
                                    {
                                        for (int k = 0; k < seats.Count; k++)
                                        {
                                            //Displaying seat segment wise in email.
                                            flightSegments += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                            flightSegments += "<strong style =\"font-weight: 600\">Seat:</strong>" + seats[k] + "[" + Currency + " " + seatPrices[k].ToString("N" + decimalPoint) + "]</td>";
                                        }
                                    }
                                    flightSegments += "</tr> ";
                                    flightSegments += "<tr style =\"padding: 0; text-align: left; vertical-align: top\">";
                                    if (baggagePrices.Sum() > 0)
                                    {
                                        for (int l = 0; l < baggages.Count; l++)
                                        {
                                            //Displaying baggage as segment wise in email.
                                            flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                            flightSegments += "< strong style =\"font-weight: 600\">Additional Baggage</strong>" + baggages[l] + "[" + Currency + " " + baggagePrices[l].ToString("N" + decimalPoint) + "]</td>";
                                        }
                                    }
                                    flightSegments += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                    flightSegments += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    flightSegments += "<table class=\"spacer\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%\"><tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\"><td height=\"2px\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 2px; font-weight: normal; hyphens: auto; line-height: 2px; margin: 0; mso-line-height-rule: exactly; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">&#xA0;</td></tr></tbody></table>";
                                    flightSegments += "</td></tr><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                    flightSegments += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    flightSegments += "<table class=\"spacer\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%\"><tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\"><td height=\"10px\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">&#xA0;</td></tr></tbody></table>";
                                    flightSegments += "</td></tr></tbody></table></td></tr>";
                                }

                                flightSegments += "</table>";
                                flightSegments += "<table class='spacer' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%'><tbody><tr style = 'padding: 0; text-align: left; vertical-align: top' >< td height='10px' style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word'>&#xA0;</td></tr></tbody></table>";
                                flightSegments += "<table class='table-main-content' width='100%' cellspacing='0' cellpadding='0' border='0' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top'>";
                                flightSegments += "<tbody>";
                                flightSegments += "<tr style = 'padding: 0; text-align: left; vertical-align: top' >";
                                flightSegments += "<td style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word'>";
                                flightSegments += "<table class='table-main-content' width='100%' cellspacing='0' cellpadding='0' border='0' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: bottom'>";
                                flightSegments += "<tr style = 'padding: 0; text-align: left; vertical-align: bottom' >";
                                flightSegments += "<td style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: bottom; word-wrap: break-word' valign='bottom'>";
                                flightSegments += "<strong style = 'font-weight: 600' >Total:</strong>";
                                flightSegments += "<strong style = 'font-size: 14px; font-weight: 600'>  " + Currency + " " + bookingAmount + "</ strong >";
                                flightSegments += "</td></tr></tbody></table></td></tr></tbody></table>";
                                flightSegments += footer;

                                flightSegments = flightSegments.Replace("< ", "<").Replace(" >", ">");

                                #endregion

                                hashtable.Add("Company", new AgentMaster(flightItinerary.AgencyId).Name);

                                //if (header.Contains("Employee"))
                                //{                                    
                                //    if (flightItinerary.Passenger[0].FlexDetailsList != null && flightItinerary.Passenger[0].FlexDetailsList.Count > 0)
                                //    {                                        
                                //        flexFields += "<table class=\"bottom-table-style\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\"><tbody>";
                                //        foreach (FlightFlexDetails flex in flightItinerary.Passenger[0].FlexDetailsList)
                                //        {
                                //            flexFields += "<tr class=\"table-cnt-inner\" style=\"padding: 0; text-align: left; vertical-align: top\">";
                                //            flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flex.FlexLabel}:</td>";
                                //            flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flex.FlexData}</td></tr>";
                                //        }
                                //        flexFields += "</tbody></table>";
                                //    }
                                //}

                                ////Adding PNR to Email

                                ////if (flightItinerary != null)
                                //{
                                //    flexFields += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                                //    flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                //    flexFields += "<td colspan=\"1\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                //    flexFields += "<table class=\"main-header\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                                //    flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                //    flexFields += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                //    flexFields += "<strong style=\"font-weight: 600\">PNR:</strong>";
                                //    flexFields += "</td></tr></tbody></table></td></tr></tbody></table>";
                                //    flexFields += "<table class=\"bottom-table-style\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";

                                //    flexFields += "<tr class=\"table-cnt-inner\" style=\"padding: 0; text-align: left; vertical-align: top\">";

                                //    flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\"> PNR:</td>";
                                //    flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flightItinerary.PNR}</td></tr>";

                                //    if(flightItinerary.IsLCC && flightItinerary.AirLocatorCode != null)
                                //    {
                                //         flexFields += $"<tr><td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">GDS PNR:</td>";
                                //         flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flightItinerary.AirLocatorCode}</td></tr>";

                                //    }

                                //    flexFields += "</table>";
                                //}
                                //hashtable.Add("flexFields", flexFields);
                                hashtable.Add("heading", header);
                                hashtable.Add("footer", footer);

                                // Audit.Add(EventType.Email, Severity.High, 1, "===========9===============", "");
                                //CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, emailHtml, hashtable, ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                                foreach (DictionaryEntry de in hashtable)
                                {
                                    string keyString = "%" + de.Key.ToString() + "%";
                                    if (de.Value != null)
                                    {
                                        emailHtml = emailHtml.Replace(keyString, de.Value.ToString());
                                    }
                                    else
                                    {
                                        emailHtml = emailHtml.Replace(keyString, " ");
                                    }
                                }
                                //toArray.Clear();
                                approverNames = string.Empty;
                                // Audit.Add(EventType.Email, Severity.High, 1, "===========10===============", "");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateTripAnalysisUI.aspx)Failed to Send Email For Employee and Approvers: Reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        return emailHtml;
    }

    protected string SendEmail(string toEmpOrApp, string approverStatus, int expDetailId, string remarks,  string header, string footer, ref string flightSegments)
    {
        string emailHtml = string.Empty;
        BuidItinerary(expDetailId);
        int user_corp_profile_id = Settings.LoginInfo.CorporateProfileId;
        string serverPath = Request.Url.Scheme + "://ctb2bstage.cozmotravel.com";
        string logoPath = "";
        int agentId = Settings.LoginInfo.AgentId;
        if (agentId > 1)
        {
            logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"] + new AgentMaster(agentId).ImgFileName;
            agentImageSource = logoPath;
        }
        else
        {
            agentImageSource = serverPath + "/images/logo.jpg";
        }
       List<string> toArray = new System.Collections.Generic.List<string>();
        if (Request["flightId"] != null)
        {
            CorporateProfileTripDetails detail = new CorporateProfileTripDetails(expDetailId);
            empId = detail.EmpId;
            empName = detail.EmpName;

            try
            {
                //Retrieve the actual trip status from the DB
                string tripStatus = FlightPolicy.GetTripApprovalStatus(expDetailId, (int)ProductType.Flight);

                if (tripStatus == "A")
                    _approvalStatus = "Approved";
                else if (tripStatus == "R")
                    _approvalStatus = "Rejected";
                else
                    _approvalStatus = "Awaiting Approval";

                
                if (toEmpOrApp == "E" || toEmpOrApp == "L") //To employee email or Location email
                {
                    //check  'L' get Email ids from  location_master and send email 
                    if (toEmpOrApp == "L")
                    {
                        LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.LocationID);
                        if (!string.IsNullOrEmpty(locationMaster.LocationEmail) && locationMaster.LocationEmail.Contains("@"))
                        {
                            if (locationMaster.LocationEmail.Contains(","))
                            {
                                string[] emails = locationMaster.LocationEmail.Split(',');
                                for (int i = 0; i < emails.Length; i++)
                                {
                                    toArray.Add(emails[i]);
                                }
                            }
                            else                            
                                toArray.Add(locationMaster.LocationEmail);                            
                        }
                    }
                    else if (toEmpOrApp == "E")
                    {
                        approverNames = detail.ProfileApproversList.Where(i => i.ApproverId == user_corp_profile_id).Select(i => i.ApproverName).FirstOrDefault();
                        if (!string.IsNullOrEmpty(detail.EmpEmail))                        
                            toArray.Add(detail.EmpEmail);                        
                    }

                    empId = detail.EmpId;
                    empName = detail.EmpName;
                    Hashtable hashtable = new Hashtable();
                    string subject = string.Empty;

                    //if (header.Contains("Employee"))
                    if (Convert.ToInt32(hdfExpDetailId.Value) == expDetailId)
                    {
                        StreamReader reader = new StreamReader(Server.MapPath("~/CorporateEmail-3-Traveller-approved.html"));
                        emailHtml = reader.ReadToEnd();
                        reader.Close();

                        FlightPolicy flightPolicy = new FlightPolicy();
                        List<FlightPolicy> flightPolicies = new List<FlightPolicy>();

                        flightPolicy.Flightid = flightItinerary.FlightId;
                        flightPolicy.GetPolicyByFlightId();
                        flightPolicies.Add(flightPolicy);
                        subject = DateTime.Now.ToString("dd-MMM-yyyy") + " Trip Request " + (_approvalStatus == "Awaiting Approval" ? "Pending" : _approvalStatus) + " – " + flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName + " – " + flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName;


                        AgentAppConfig appConfig = new AgentAppConfig();
                        appConfig.AgentID = Settings.LoginInfo.AgentId;
                        appConfig.ProductID = 1;
                        List<AgentAppConfig> appConfigs = appConfig.GetConfigData();


                        hashtable.Add("requestDate", DateTime.Now.ToString("dd-MMM-yyyy"));
                        hashtable.Add("employee", flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName);
                        hashtable.Add("departureCityName", flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName);
                        hashtable.Add("tripReference", flightItinerary.TripId);
                        hashtable.Add("approvalStatus", _approvalStatus);
                        hashtable.Add("employeeID", detail.EmpId);

                        tripStatus = (tripStatus == "A" ? "Approved <strong style=\"color: green; font - weight: 600\">✔</strong>" : tripStatus == "R" ? "Rejected <strong style=\"color: red; font - weight: 600\">❌</strong>" : "Awaiting Approval <strong style=\"color: gray; font - weight: 600\">&#10071;</strong>");
                        hashtable.Add("tripStatus", tripStatus);

                        double deadline = 5;//Default 5 Hours
                        if (appConfigs.Count > 0)
                        {
                            AgentAppConfig config = appConfigs.Find(ac => ac.AppKey == "CorpApprovalDeadline");
                            if (config != null)
                                deadline = Convert.ToDouble(config.AppValue);
                        }
                        hashtable.Add("approvalDeadline", DateTime.Now.AddHours(Convert.ToDouble(deadline)).ToString("dd-MMM-yyyy hh:mm:ss tt"));//TODO: insert into app config table and assign

                        if (flightPolicies.Count > 0)
                        {
                            CorporateTravelReason travelReason = new CorporateTravelReason(flightPolicies[0].TravelReasonId);
                            CorporateTravelReason policyReason = new CorporateTravelReason();
                            if (flightPolicies[0].PolicyReasonId > 0)
                                policyReason = new CorporateTravelReason(flightPolicies[0].PolicyReasonId);
                            hashtable.Add("travelReason", travelReason.Description);
                            hashtable.Add("policyCompliance", flightPolicies[0].IsUnderPolicy ? "Inside Policy" : "Outside Policy - " + flightPolicies[0].PolicyBreakingRules);//Inside or outside policy with policy breaking rule
                            hashtable.Add("bookingReason", !string.IsNullOrEmpty(policyReason.Description) && !flightPolicies[0].IsUnderPolicy ? "<strong style=\"font - weight: 600\">Violation Reason:</ strong > " + policyReason.Description : "");
                        }

                        if (_approvalStatus == "Rejected")
                        {
                            string rejectionReason = remarks;
                            string rejectedReason = "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                            rejectedReason += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                            rejectedReason += "<strong style =\"font-weight: 600\">Rejection Reason:</strong>";
                            rejectedReason += "</td>";
                            rejectedReason += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                            rejectedReason += rejectionReason;
                            rejectedReason += "</td>";
                            rejectedReason += "</tr>";

                            hashtable.Add("rejectedReason", rejectedReason);
                        }
                        else
                            hashtable.Add("rejectedReason", "");

                        string approvers = string.Empty;
                        List<CorpProfileApproval> approvals = CorpProfileApproval.RemoveDuplicatesIterative(detail.ProfileApproversList);
                        int approvalsCount = approvals.Count;
                        if (approvalsCount > 0)
                        {
                            for (int i = 0; i < approvalsCount; i++)
                            {
                                //If approver profileid and traveller profile id is same then do not show Approver label
                                if (!string.IsNullOrEmpty(approvals[i].ApproverEmail) && approvals[i].ApproverId != detail.ProfileId)
                                {
                                    string approverEmail = approvals[i].ApproverEmail;
                                    string approvalStatus = approvals[i].ApprovalStatus;
                                    approvers += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                    approvers += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    approvers += "<strong style =\"font-weight: 600\">Approver" + (i + 1) + "</strong>";
                                    approvers += "</td>";
                                    approvers += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    approvers += approverEmail + "&nbsp;&nbsp;&nbsp;&nbsp; <span style=\"color: green\">Status: <strong>" + approvalStatus + "</strong></span>";
                                    approvers += "</td>";
                                    approvers += "</tr>";
                                }
                            }
                        }
                        hashtable.Add("approvers", approvers);
                        string fallBackApprover = string.Empty;
                        try
                        {
                            List<CorpProfileApproval> approvalCount = CorpProfileApproval.RemoveDuplicatesIterative(detail.ProfileApproversList);
                            List<CorpProfileApproval> fallBackApprovers = detail.ProfileApproversList.Except(approvalCount).ToList();
                            fallBackApprovers = (from r in fallBackApprovers orderby r.Hierarchy select r).ToList(); 
                            int counter = 0;
                            foreach (CorpProfileApproval corpProfile in fallBackApprovers)
                            {
                                if (!string.IsNullOrEmpty(corpProfile.ApproverEmail))
                                {
                                    fallBackApprover += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                    fallBackApprover += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    fallBackApprover += "<strong style =\"font-weight: 600\">Fall Back Approver " + corpProfile.Hierarchy + " - " + (counter + 1) + "</strong>";
                                    fallBackApprover += "</td><td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    fallBackApprover += corpProfile.ApproverEmail + "&nbsp;&nbsp;&nbsp;&nbsp; <span style=\"color: green\">Status: <strong>" + corpProfile.ApprovalStatus + "</strong></span>";
                                    fallBackApprover += "</td></tr>";
                                    counter++;
                                }
                            }

                        }
                        catch { }
                        finally { hashtable.Add("fallBackApprover", fallBackApprover); }
                    }

                    #region Flight Segments

                    int segments = flightItinerary.Segments.Length;
                    List<string> meals = new List<string>();
                    List<decimal> mealPrices = new List<decimal>();
                    List<string> baggages = new List<string>();
                    List<decimal> baggagePrices = new List<decimal>();
                    List<string> seats = new List<string>();
                    List<decimal> seatPrices = new List<decimal>();

                    flightItinerary.Passenger.ToList().ForEach(p =>
                    {
                        string meal = string.Empty, seat = string.Empty, bag = string.Empty;
                        for (int i = 0; i < segments; i++)
                        {
                            meal += meal.Length > 0 ? ", No Meal Booked" : "No Meal";
                            seat += seat.Length > 0 ? ", No Seat Booked" : "No Seat Booked";
                            bag += bag.Length > 0 ? ", No Bag" : "No Bag";
                        }
                        meals.Add(string.IsNullOrEmpty(p.MealDesc) ? meal : p.MealDesc);
                        mealPrices.Add(p.Price.MealCharge);
                        baggages.Add(string.IsNullOrEmpty(p.BaggageCode) ? bag : p.BaggageCode);
                        baggagePrices.Add(p.Price.BaggageCharge);
                        seats.Add(string.IsNullOrEmpty(p.Seat.Code) ? seat : p.Seat.Code);
                        seatPrices.Add(p.Price.SeatPrice);
                    });


                    string airlineName = string.Empty, airlineCode = string.Empty, flightNumber = string.Empty, departureCityName = string.Empty, departureAirportName = string.Empty, departureTerminal = string.Empty;
                    string departureDate = string.Empty, departureWeekDay = string.Empty, departureTime = string.Empty;
                    string arrivalCityName = string.Empty, arrivalDate = string.Empty, arrivalWeekDay = string.Empty, arrivalTime = string.Empty,
                        arrivalAirportName = string.Empty, arrivalTerminal = string.Empty, Stops = string.Empty, Duration = string.Empty;
                    string Currency = flightItinerary.Passenger[0].Price.Currency;
                    decimal bookingAmount = 0;
                    int decimalPoint = 2;

                    bookingAmount = flightItinerary.Passenger.ToList().Sum(p => p.Price.PublishedFare + p.Price.Tax + p.Price.Markup +
                    p.Price.BaggageCharge + p.Price.OtherCharges + p.Price.HandlingFeeAmount + p.Price.OutputVATAmount +
                    p.Price.MealCharge + p.Price.SeatPrice + p.Price.AsvAmount - p.Price.Discount);

                    bookingAmount = flightItinerary.FlightBookingSource == BookingSource.TBOAir ? Math.Ceiling(bookingAmount) : bookingAmount;
                    decimalPoint = flightItinerary.Passenger[0].Price.DecimalPoint;

                    flightSegments += header;
                    flightSegments += "< table class='spacer' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%'><tbody><tr style = 'padding: 0; text-align: left; vertical-align: top' >< td height='10px' style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word'>&#xA0;</td></tr></tbody></table>";
                    flightSegments += "<table class='flight-table' style='border-bottom: 1px solid #e4e4e4; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%'>";
                    flightSegments += "< tr style = 'padding: 0; text-align: left; vertical-align: top' >";
                    flightSegments += "< th style='Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Airline</th>";
                    flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Departure</th>";
                    flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Arrival</th>";
                    flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Stops</th>";
                    flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Duration</th>";
                    flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Price</th>";
                    flightSegments += "</tr>";


                    for (int i = 0; i < segments; i++)
                    {

                        FlightInfo flight = flightItinerary.Segments[i];
                        Airline airline = new Airline();
                        airline.Load(flight.Airline);
                        airlineName = airline.AirlineName; airlineCode = flight.Airline; flightNumber = flight.FlightNumber;
                        departureCityName = flight.Origin.CityName; departureAirportName = flight.Origin.AirportName;
                        departureTerminal = flight.DepTerminal; departureDate = flight.DepartureTime.ToString("dd MMM yyyy");
                        departureWeekDay = flight.DepartureTime.DayOfWeek.ToString().Substring(0, 3);
                        departureTime = flight.DepartureTime.ToString("hh:mm tt");
                        arrivalCityName = flight.Destination.CityName; arrivalDate = flight.ArrivalTime.ToString("dd MMM yyyy");
                        arrivalWeekDay = flight.ArrivalTime.DayOfWeek.ToString().Substring(0, 3);
                        arrivalTime = flight.ArrivalTime.ToString("hh:mm tt");
                        arrivalAirportName = flight.Destination.CityName; arrivalTerminal = flight.ArrTerminal;

                        Stops = flight.Stops == 0 ? "Non Stop" : flight.Stops == 1 ? "One Stop" : "Two Stops";
                        Duration = flight.Duration.ToString();


                        flightSegments += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                        flightSegments += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">" + airlineName + "<br />" + airlineCode + " " + flightNumber + "</td>";
                        flightSegments += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                        flightSegments += "<strong>"+departureCityName + "</ strong >< br /> " + departureDate + "< br /> (" + departureWeekDay + ")," + departureTime+",< br /> Airport:" + departureAirportName + ",< br /> Terminal: " + departureTerminal+"</td>";
                        flightSegments += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                        flightSegments += "<strong>" + arrivalCityName + "</ strong >< br /> " + arrivalDate+" < br /> (" + arrivalWeekDay + ")," + arrivalTime + ",< br /> Airport:" + arrivalAirportName + ", Terminal: " + arrivalTerminal + "</ td >";
                        flightSegments += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">" + Stops + "</td>";
                        flightSegments += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">" + Duration + "</td>";
                        if (i == 0)
                        {
                            flightSegments += "<td rowspan =\"" + (flightItinerary.Segments.Length + 5).ToString() + "\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; border-left: 1px solid #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: center; vertical-align: middle; word-wrap: break-word\">";
                            flightSegments += "< strong >" + Currency + "  " + bookingAmount.ToString("N" + decimalPoint) + "</ strong ></ td >";
                        }
                        flightSegments += "</tr>";
                        flightSegments += "<tr>";
                        flightSegments += "<td colspan =\"5\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                        flightSegments += "<table class=\"table-main-content\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                        flightSegments += "<tbody>";
                        flightSegments += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                        if (mealPrices.Sum() > 0)
                        {
                            for (int j = 0; j < meals.Count; j++)
                            {
                                flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                flightSegments += "< strong style =\"font-weight: 600\">Meal:</strong>" + meals[j]+"["+Currency + " "+mealPrices[j].ToString("N" + decimalPoint)+"]</td>";
                            }
                        }
                        flightSegments += "</tr> ";
                        flightSegments += "< tr style =\"padding: 0; text-align: left; vertical-align: top\">";

                        if (seatPrices.Sum() > 0)
                        {
                            for (int k = 0; k < seats.Count; k++)
                            {
                                //Displaying seat segment wise in email.
                                flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                flightSegments += "< strong style =\"font-weight: 600\">Seat:</strong>" + seats[k] + "[" + Currency + " " + seatPrices[k].ToString("N" + decimalPoint) + "]</td>";
                            }
                        }
                        flightSegments += "</tr> ";
                        flightSegments += "<tr style =\"padding: 0; text-align: left; vertical-align: top\">";
                        if (baggagePrices.Sum() > 0)
                        {
                            for (int l = 0; l < baggages.Count; l++)
                            {
                                //Displaying baggage as segment wise in email.
                                flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                flightSegments += "< strong style =\"font-weight: 600\">Additional Baggage</strong>" + baggages[l] + "[" + Currency + " " + baggagePrices[l].ToString("N" + decimalPoint) + "]</td>";
                            }
                        }
                        flightSegments += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                        flightSegments += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                        flightSegments += "<table class=\"spacer\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%\"><tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\"><td height=\"2px\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 2px; font-weight: normal; hyphens: auto; line-height: 2px; margin: 0; mso-line-height-rule: exactly; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">&#xA0;</td></tr></tbody></table>";
                        flightSegments += "</td></tr><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                        flightSegments += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                        flightSegments += "<table class=\"spacer\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%\"><tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\"><td height=\"10px\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">&#xA0;</td></tr></tbody></table>";
                        flightSegments += "</td></tr></tbody></table></td></tr>";
                    }

                    flightSegments += "</table>";
                    flightSegments += "< table class='spacer' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%'><tbody><tr style = 'padding: 0; text-align: left; vertical-align: top' >< td height='10px' style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word'>&#xA0;</td></tr></tbody></table>";
                    flightSegments += "< table class='table-main-content' width='100%' cellspacing='0' cellpadding='0' border='0' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top'>";
                    flightSegments += "< tbody>";
                    flightSegments += "< tr style = 'padding: 0; text-align: left; vertical-align: top' >";
                    flightSegments += "< td style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word'>";
                    flightSegments += "< table class='table-main-content' width='100%' cellspacing='0' cellpadding='0' border='0' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: bottom'>";
                    flightSegments += "<tr style = 'padding: 0; text-align: left; vertical-align: bottom' >";
                    flightSegments += "< td style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: bottom; word-wrap: break-word' valign='bottom'>";
                    flightSegments += "< strong style = 'font-weight: 600' >Total:</strong>";
                    flightSegments += "<strong style = 'font-size: 14px; font-weight: 600' >  " + Currency + " " + bookingAmount + "</ strong >";
                    flightSegments += "</ td ></ tr ></ tbody ></ table ></ td ></ tr ></ tbody ></ table >";
                    flightSegments += footer;

                    flightSegments = flightSegments.Replace("< ", "<").Replace(" >", ">");

                    #endregion

                    hashtable.Add("Company", new AgentMaster(flightItinerary.AgencyId).Name);

                    string flexFields = string.Empty;

                    //if (header.Contains("Employee"))
                    //if (Convert.ToInt32(hdfExpDetailId.Value) == expDetailId)
                    //{
                    //    if (flightItinerary.Passenger[0].FlexDetailsList != null && flightItinerary.Passenger[0].FlexDetailsList.Count > 0)
                    //    {
                    //        flexFields += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                    //        flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                    //        flexFields += "<td colspan=\"1\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word\">";
                    //        flexFields += "<table class=\"main-header\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                    //        flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                    //        flexFields += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                    //        flexFields += "<strong style=\"font-weight: 600\">Reporting Fields:</strong>";
                    //        flexFields += "</td></tr></tbody></table></td></tr></tbody></table>";
                    //        flexFields += "<table class=\"bottom-table-style\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\"><tbody>";
                    //        foreach (FlightFlexDetails flex in flightItinerary.Passenger[0].FlexDetailsList)
                    //        {
                    //            flexFields += "<tr class=\"table-cnt-inner\" style=\"padding: 0; text-align: left; vertical-align: top\">";
                    //            flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flex.FlexLabel}:</td>";
                    //            flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flex.FlexData}</td></tr>";
                    //        }
                    //        flexFields += "</tbody></table>";
                    //    }
                    //}
                    //Adding PNR to Email

                    //if (flightItinerary != null)
                    //{
                    //    flexFields += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                    //    flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                    //    flexFields += "<td colspan=\"1\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word\">";
                    //    flexFields += "<table class=\"main-header\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                    //    flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                    //    flexFields += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                    //    flexFields += "<strong style=\"font-weight: 600\">PNR:</strong>";
                    //    flexFields += "</td></tr></tbody></table></td></tr></tbody></table>";
                    //    flexFields += "<table class=\"bottom-table-style\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                    //    flexFields += "<tr class=\"table-cnt-inner\" style=\"padding: 0; text-align: left; vertical-align: top\">";
                    //    flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\"> PNR:</td>";
                    //    flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flightItinerary.PNR}</td></tr>";

                    //    if (flightItinerary.IsLCC && flightItinerary.AirLocatorCode != null)
                    //    {
                    //        flexFields += $"<tr><td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">GDS PNR:</td>";
                    //        flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flightItinerary.AirLocatorCode}</td></tr>";
                    //    }

                    //    flexFields += "</table>";
                    //}
                    //flexFields = flexFields.Replace("< ", "<").Replace(" >", ">");
                    //hashtable.Add("flexFields", flexFields);
                    hashtable.Add("heading", header);
                    hashtable.Add("footer", footer);

                    if (toArray != null && toArray.Count > 0)
                    {
                        //CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, emailHtml, hashtable, ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                        foreach (DictionaryEntry de in hashtable)
                        {
                            string keyString = "%" + de.Key.ToString() + "%";
                            if (de.Value != null)                            
                                emailHtml = emailHtml.Replace(keyString, de.Value.ToString());                            
                            else                            
                                emailHtml = emailHtml.Replace(keyString, " ");                            
                        }
                    }
                    //toArray.Clear();
                    approverNames = string.Empty;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateTripAnalysisUI.aspx)Failed to Send Email For Employee and Approvers: Reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        return emailHtml;
    }

    public void BuidItinerary(int flight_id)
    {
        AirFare = 0;
        Taxes = 0;
        Baggage = 0;
        MarkUp = 0;
        Discount = 0;
        AsvAmount = 0;
        int flightId = Convert.ToInt32(flight_id);
        if (flightId > 0)
        {
            airline = new Airline();
            flightItinerary = new FlightItinerary(flightId);
            booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(flightId));
            booking.AgencyId = flightItinerary.AgencyId;
            agency = new AgentMaster(flightItinerary.AgencyId);
            for (int i = 0; i < flightItinerary.Passenger.Length; i++)
            {
                FlightPassenger pax = flightItinerary.Passenger[i];
                AirFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;
                Taxes += pax.Price.Tax + pax.Price.Markup;
                if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                {
                    Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                }
                Baggage += pax.Price.BaggageCharge;
                MarkUp += pax.Price.Markup;
                Discount += pax.Price.Discount;
                AsvAmount += pax.Price.AsvAmount;
            }
            if (flightItinerary.Passenger[0].Price.AsvElement == "BF")
            {
                AirFare += AsvAmount;
            }
            else if (flightItinerary.Passenger[0].Price.AsvElement == "TF")
            {
                Taxes += AsvAmount;
            }
        }
    }

    /// <summary>
    /// Releases/Cancels the rejected Itineraries
    /// </summary>
    /// <param name="flightItineraries">List of rejected Flight Itineraries to be released</param>
    /// <param name="mse">MetaSearchEngine object used to release</param>
    /// <param name="loggedMember">UserMaster object</param>
    private void ReleaseItineraries(List<FlightItinerary> flightItineraries, CT.MetaSearchEngine.MetaSearchEngine mse, UserMaster loggedMember)
    {
        try
        {
            flightItineraries.ForEach(x =>
            {
                string approvalStatus = FlightPolicy.GetTripApprovalStatus(x.FlightId, 1);
                if (!x.IsLCC && approvalStatus == "R")//Release only if it is in Rejected status
                {
                    string pnr = mse.CancelItinerary(x.PNR, x.FlightBookingSource);
                    if (pnr == x.PNR)//Released
                    {
                        booking = new BookingDetail(x.BookingId);
                        BookingHistory bh = new BookingHistory();
                        bh.BookingId = booking.BookingId;
                        bh.EventCategory = EventCategory.Booking;
                        bh.Remarks = "Booked seat are released pnr: " + pnr + " (IP Address: " + Request["REMOTE_ADDR"] + ")";
                        bh.CreatedBy = (int)Settings.LoginInfo.UserID;
                        bh.Save();

                        BookingDetail.SetBookingStatus(x.BookingId, BookingStatus.Released, (int)loggedMember.ID);

                        string routing = string.Empty;
                        x.Segments.ToList().ForEach(s => routing += string.IsNullOrEmpty(routing) ? s.Origin.AirportCode : "-" + s.Origin.AirportCode);

                        string subject = "Booking Cancelled - PNR(" + pnr + ")";
                        Hashtable table = new Hashtable(3);
                        table.Add("firstName", loggedMember.FirstName);
                        table.Add("lastName", loggedMember.LastName);
                        table.Add("time", DateTime.UtcNow.Add(new TimeSpan(4, 0, 0)).ToString("hh:mm:ss tt"));
                        table.Add("date", DateTime.UtcNow.Add(new TimeSpan(4, 0, 0)).ToString("dd/MMM/yyyy"));
                        table.Add("pnr", pnr);
                        table.Add("paxName", x.Passenger[0].FullName);
                        table.Add("travelDate", x.Segments[0].DepartureTime.ToString("dd/MMM/yyyy"));
                        table.Add("flight", x.Segments[0].Airline + x.Segments[0].FlightNumber);
                        table.Add("itinerary", routing);
                        table.Add("rtripId", "");
                        //preferene is getting wriiten again here                                    
                        List<string> toArray = new List<string>();
                        toArray.Add(Settings.LoginInfo.AgentEmail);

                        try
                        {
                            Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, subject, CT.Configuration.ConfigurationSystem.Email["bookingCancel"], table);
                        }
                        catch (System.Net.Mail.SmtpException)
                        {
                            Audit.Add(EventType.Email, Severity.Normal, (int)Settings.LoginInfo.UserID, "Smtp is unable to send the message", "");
                        }
                    }
                }
            });
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.CancellBooking, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to Release Auto Rejected Bookings. Reason:" + ex.ToString(), "");
        }
    }

}


