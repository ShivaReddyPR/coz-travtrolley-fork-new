﻿
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ETicket" Codebehind="ETicket.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="CT.TicketReceipt.Common" %>
<%@ Import Namespace="System.Linq" %>
<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        h1, h2, h3 {
            margin: 0px;
            padding: 0px;
        }

        body {
            margin: 20px 0% 20px 0%;
            font-family: Verdana, Geneva, sans-serif;
            font-size: 13px;
        }

        hr {
            margin: 0px;
            border: 0;
            height: 1px;
            background: #333;
            background-image: linear-gradient(to right, #ccc, #333, #ccc);
        }
    </style>
        <style>@media only screen {
  html {
    min-height: 100%;
    background: #f3f3f3;
  }
}

@media only screen and (max-width: 716px) {
  .small-float-center {
    margin: 0 auto !important;
    float: none !important;
    text-align: center !important;
  }
}

@media only screen and (max-width: 716px) {
  table.body img {
    width: auto;
    height: auto;
  }

  table.body center {
    min-width: 0 !important;
  }

  table.body .container {
    width: 95% !important;
  }

  table.body .columns {
    height: auto !important;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    padding-left: 16px !important;
    padding-right: 16px !important;
  }

  table.body .columns .columns {
    padding-left: 0 !important;
    padding-right: 0 !important;
  }

  table.body .collapse .columns {
    padding-left: 0 !important;
    padding-right: 0 !important;
  }

  th.small-6 {
    display: inline-block !important;
    width: 50% !important;
  }

  th.small-12 {
    display: inline-block !important;
    width: 100% !important;
  }

  .columns th.small-12 {
    display: block !important;
    width: 100% !important;
  }

  table.menu {
    width: 100% !important;
  }

  table.menu td,
  table.menu th {
    width: auto !important;
    display: inline-block !important;
  }

  table.menu.vertical td,
  table.menu.vertical th {
    display: block !important;
  }

  table.menu[align="center"] {
    width: auto !important;
  }
}

@media print {
  * {
    -webkit-print-color-adjust: exact !important;
    /* Chrome, Safari */
    color-adjust: exact !important;
    /*Firefox*/
  }
}</style>
    <title>E Ticket</title>
    <%--<script src="ash.js" type="text/javascript"></script>--%>

    <link href="css/main-style.css" rel="stylesheet" type="text/css" />

    <script language="JavaScript" type="text/javascript" src="Scripts/Utils.js"></script>

    <%--<script language="JavaScript" src="prototype.js" type="text/javascript"></script>--%>

    <script language="JavaScript" type="text/javascript">
        function ClosePop() {
            window.close();
        }
    </script>

    <script language="JavaScript" type="text/javascript">
<!--


        //Added by lokesh on 22-06-2018
        //When the user clicks on the print button do not display the 
        //email, print and show fare labels in the print functionality.
        function prePrint() {
            document.getElementById('ancFare').style.display = "none";
            if (document.getElementById('ancFareRouting') != null) {
                document.getElementById('ancFareRouting').style.display = "none";
            }
            document.getElementById('tblPrintEmailActions').style.display = "none";
            window.print();
            setTimeout('showFareButtons()', 1000);
            return false;
        }
        function showFareButtons() {
            document.getElementById('ancFare').style.display = "block";
            if (document.getElementById('ancFareRouting') != null) {
                document.getElementById('ancFareRouting').style.display = "block";
            }
            document.getElementById('tblPrintEmailActions').style.display = "table";
        }

    function showAllButtons() {
        document.getElementById('Print').style.display = "block";

        if (document.getElementById('SendMailButton')) {
            document.getElementById('SendMailButton').style.display = "block";
        }
    }

    function ShowEmailDivD() {
        document.getElementById('LowerEmailSpan').style.display = 'block';
        document.getElementById('addressBox').focus();
    }
    function HideEmailDiv() {
        document.getElementById('LowerEmailSpan').style.display = 'none';

    }
    var Ajax;
    if (window.XMLHttpRequest) {
        Ajax = new window.XMLHttpRequest();
    }
    else {
        Ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }
    function SendMail() {
        var addressList = document.getElementById('addressBox').value;
        if (Trim(addressList) == '') {
            document.getElementById('emailSent').style.display = 'block';
            document.getElementById('messageText').innerHTML = 'Please fill the address';
            return;
        }
        if (!ValidEmail.test(Trim(addressList))) {
            document.getElementById('emailSent').style.display = 'block';
            document.getElementById('messageText').innerHTML = 'Please fill correct email address';
            return;
        }
        var paramList = 'ticketId=' + ticketId;
        paramList += "&addressList=" + addressList;
        paramList += "&isEticket=true";
        var url = "EmailItineraryPage";

        Ajax.onreadystatechange = DisplayMessage;
        Ajax.open('POST', url);
        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        Ajax.send(paramList);
    }
    function DisplayMessage() {
        if (Ajax.readyState == 4) {
            if (Ajax.status == 200) {
                document.getElementById('emailSent').style.display = 'block';
                document.getElementById('messageText').innerHTML = 'Email Sent Successfully';
                HideEmailDiv();
            }
        }
    }

    function ShowPopUp(id) {
            //document.getElementById('<%#txtEmailId.ClientID %>').value = "";
            document.getElementById('txtEmailId').value = "";
            document.getElementById('err').style.display = 'none';
            document.getElementById('emailBlock').style.display = "block";
            //            var positions = getRelativePositions(document.getElementById(id));
            //            document.getElementById('emailBlock').style.left = (530) + 'px';
            //            document.getElementById('emailBlock').style.top = (400) + 'px';
            return false;
        }
        function HidePopUp() {
            document.getElementById('emailBlock').style.display = "none";
        }
        function Validate() {
            var isValid = true;
            var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
            document.getElementById('err').style.display = 'none';
            if (document.getElementById('<%#txtEmailId.ClientID %>').value.length <= 0) {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Email address";
                isValid = false;
            }
            else if (reg.test(document.getElementById('<%#txtEmailId.ClientID %>').value)) {
                document.getElementById('err').style.display = 'none';
            }
            else {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Correct Email";
                isValid = false;
            }
            if (isValid == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function ShowHide(div) {
            if (document.getElementById('hdfFare').value == '1') {
                document.getElementById('ancFare').innerHTML = 'Show Fare'      
                document.getElementById(div).style.display = 'none';
                document.getElementById('hdfFare').value = '0';
            }
            else {
                document.getElementById('ancFare').innerHTML = 'Hide Fare'
                document.getElementById('ancFare').value = 'Hide Fare'                
                document.getElementById(div).style.display = 'block';
                document.getElementById('hdfFare').value = '1';
            }
        }
// -->
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <center>

            <!-- mail block -->
            <div id="emailBlock" class="showmsg" style="position: absolute; display: none; left: 0;right:0;margin:auto; top: 200px; width: 300px;z-index:10001">
                <div class="showMsgHeading">Enter Your Email Address</div>

                <a style="position: absolute; right: 5px; top: 3px;" onclick="return HidePopUp();" href="#" class="closex">X</a>
                <div class="padding-5">
                    <div style="background: #fff">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                                <td height="40" align="center">
                                    <b style="display: none; color: Red" id="err"></b>
                                    <asp:TextBox Style="border: solid 1px #ccc; width: 90%; padding: 2px;" ID="txtEmailId" runat="server" CausesValidation="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>

                                <td height="40" align="center">
                                    <asp:Button CssClass="button-normal" ID="btnEmailVoucher" runat="server" Text="Send Email" OnClientClick="return Validate()" OnClick="btnEmailVoucher_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <asp:MultiView ID="MultiViewETicket" runat="server">
                <asp:View ID="TicketView" runat="server">
                    <asp:HiddenField ID="hdfFare" runat="server" Value="1" />
                    <asp:HiddenField ID="hdfTicketType" runat="server" />
                    <div id="PrintDiv" runat="server" style="width: 100%">
                        <div>

                            <div style="display: none;" id="emailSent">
                                <div style="float: left; width: 100%; margin: auto; text-align: center;">
                                    <span class="email-message-child" id="messageText">Email sent successfully</span>
                                </div>
                            </div>
                           
                            <% int bc = 0;
                                List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();
                                if (Request["bkg"] != null)
                                {
                                    ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(flightItinerary.FlightId);
                                }
                                else
                                {
                                    ptcDetails = ticketList[0].PtcDetail;
                                }
                                for (int count = 0; count < flightItinerary.Segments.Length; count++)
                                {
                                    int paxIndex = 0;
                                    if (Request["paxId"] != null)
                                    {
                                        paxIndex = Convert.ToInt32(Request["paxId"]);
                                    }
                                    List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
                                    if (Request["paxId"] == null)
                                    {
                                        ptcDetail = ptcDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.SegmentId == flightItinerary.Segments[count].SegmentId; });
                                    }
                                    else
                                    {
                                        ptcDetail = ptcDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.SegmentId == flightItinerary.Segments[count].SegmentId && ptc.PaxType == paxType; });
                                    }%>







  <div style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box; -webkit-text-size-adjust: 100%; Margin: 0; background: #f3f3f3 !important; box-sizing: border-box; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100% !important">
    <span class="preheader" style="color: #f3f3f3; display: none !important; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; mso-hide: all !important; opacity: 0; overflow: hidden; visibility: hidden"></span>
    <table class="body" style="Margin: 0; background: #f3f3f3 !important; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
      <tr style="padding: 0; text-align: left; vertical-align: top">
        <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
          <center data-parsed="" style="min-width: 700px; width: 100%">
            <table align="center" class="wrapper b2b-eticket-wrapper float-center" style="Margin: 0 auto; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><td class="wrapper-inner" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
<%if (count == 0)
    { %>
            	<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
       <% } %>     
            	<table align="center" class="container" style="Margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0; text-align: inherit; vertical-align: top; width: 700px"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
            


<%if (count == 0) { %>
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            
            
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-9 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 509px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<h2 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 22px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left; word-wrap: normal"><strong>e-Ticket</strong></h2>
            			 <span> <%if (flightItinerary != null && flightItinerary.CreatedOn != DateTime.MinValue) %>
                    <%{ %>
                                              Booking Date:  <%=flightItinerary.CreatedOn.ToString("ddd") + "," + flightItinerary.CreatedOn.ToString("dd MMM yyyy")%>
                    <%} %>	</span>
            		</th></tr></table></th>
            		<th class="small-12 large-3 columns last" right="" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 159px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">            			

                             <asp:Image ID="imgLogo" style="-ms-interpolation-mode: bicubic; border-width: 0px; clear: both; display: block; height: 51px; max-width: 100%; outline: none; text-decoration: none; width: 159px" runat="server" AlternateText="AgentLogo" ImageUrl="https://ctb2b.cozmotravel.com/images/Logo.jpg" />

            		</th></tr></table></th>
            	  </tr></tbody></table>
            	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 15px; font-weight: bold; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left; word-wrap: normal">Reservation Details</h3>
            		</th></tr></table></th>
            		<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<table class="float-right text-right" id="tblPrintEmailActions" align="right" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: right; vertical-align: top; width: 100%">
            				<tr style="padding: 0; text-align: left; vertical-align: top">
            					<td class="float-right text-right" valign="middle" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: right; vertical-align: middle; word-wrap: break-word">
            						<span class="float-right text-right" style="Margin: 0; color: #2199e8; font-family: 'Open Sans', Arial, sans-serif; font-weight: normal; height: 20px; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none">
                                        
                                             <img id="imgEmail" runat="server" src="https://ctb2b.cozmotravel.com/images/email_icon.png" alt="Email" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: inline; height: 12px; margin-right: 5px; max-width: 100%; outline: none; text-decoration: none; vertical-align: middle; width: 15px" />
                                              <asp:LinkButton ID="btnEmail" runat="server" Text="Email" OnClientClick="return ShowPopUp(this.id);" />
                                            </span>
            				<span class="float-right text-right" style="Margin: 0; color: #2199e8; font-family: 'Open Sans', Arial, sans-serif; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none">
                           
                                    <img id="imgPrint" runat="server" src="https://ctb2b.cozmotravel.com/images/print_icon.jpg" alt="Print" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: inline; height: 20px; max-width: 100%; outline: none; text-decoration: none; vertical-align: middle; width: 20px" class="" />

                                          <asp:LinkButton ID="btnPrint" runat="server" Text="Print" OnClientClick="return prePrint();" />
                                    </span>	
            					</td>
            				</tr>
            			</table>
            			
            		</th></tr></table></th>
            	  </tr></tbody></table>
            
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody>
                      <tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">PNR NO. | <strong> <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR)%></strong></p>
            	  	</th></tr></table></th>



            	  	<%--<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">Agent Name |  <strong> <%=agency.Name %></strong></p>
            	  	</th></tr></table></th>--%>


                          
                          <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo != null) %>
                          <%{
                                  var parentid1 =CT.TicketReceipt.BusinessLayer.AgentMaster.GetParentId(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId);
                                  CT.TicketReceipt.BusinessLayer.LocationMaster location1 = new CT.TicketReceipt.BusinessLayer.LocationMaster(flightItinerary.LocationId); 
                                  
                                  %>
                                  
                          <%if (parentid1 == 2125 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId == 2125) %>
                          <%{ %>
            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">Client |  <strong> <%=!string.IsNullOrEmpty(flightItinerary.TripId) && agency.AgentParantId > 0 ? new CT.TicketReceipt.BusinessLayer.AgentMaster(agency.AgentParantId).Name : agency.Name %></strong>Address | <strong> <%=location1.Address %></strong> </p>
            	  	</th></tr></table></th>
                          <%} %>
                          <%else %>
                          <%{ %>
                          <th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">Agent Name |  <strong> <%=!string.IsNullOrEmpty(flightItinerary.TripId) && agency.AgentParantId > 0 ? new CT.TicketReceipt.BusinessLayer.AgentMaster(agency.AgentParantId).Name : agency.Name %></strong></p>
            	  	</th></tr></table></th>

                          <%} %>

                          <%} %>




            	  </tr>




       


  <%var parentid =CT.TicketReceipt.BusinessLayer.AgentMaster.GetParentId(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId);
                                                                                         if (parentid == 2125 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId==2125)

                                                                                         {
                                                                                      CT.TicketReceipt.BusinessLayer.LocationMaster location = new CT.TicketReceipt.BusinessLayer.LocationMaster(flightItinerary.LocationId);       %>

                  <tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">GSTIN | <strong>  <%=location.GstNumber %></strong></p>
            	  	</th></tr></table></th>
            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"></th>
            	  </tr>
        <%} %>


            	                                                                                                                                                                              </tbody></table>
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
        <%if(!string.IsNullOrEmpty(flightItinerary.RoutingTripId)){ %>
                       	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">Routing Trip Id | <strong> <%=flightItinerary.RoutingTripId %></strong></p>
            	  	</th></tr></table></th>
 <%} %>

            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">PHONE  |  <strong><%=agency.Phone1 %></strong></p>
            	  	</th></tr></table></th>
            	  </tr></tbody></table>

    <% }%>









        <%if (count == 0)
                                                { %>
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">	
            	  		<table class="b2b-baggage-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Passenger Name</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">E-Ticket Number	</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Baggage</th>
            				
                            <%if (flightItinerary != null && flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.UAPI || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp)  %>
                                                        <%{ %>        
                                  	<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Meal</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Seats</th>
       <%} %>


            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Airline Ref.</th>
            	  			</tr>




                                 <%if (hdfTicketType.Value == "Individual")
                                                        { %>
                                                   <tr style="padding: 0; text-align: left; vertical-align: top">	
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word"><%=paxName %></td>
                                                     <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%if (Request["bkg"] == null)
                                                                { %>
                                                            <%=(ticketList[0].TicketNumber.Split('|').Length > 1 ? ticketList[0].TicketNumber.Split('|')[flightItinerary.Segments[count].Group] : ticketList[0].TicketNumber) %>
                                                            <%}
                                                                else
                                                                { %>
                                                            <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[count] : flightItinerary.PNR)%>
                                                            <%} %></td>
                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%if (flightItinerary.FlightBookingSource == BookingSource.Amadeus || flightItinerary.FlightBookingSource == BookingSource.UAPI || (flightItinerary.FlightBookingSource == BookingSource.TBOAir || flightItinerary.FlightBookingSource == BookingSource.Babylon || flightItinerary.FlightBookingSource == BookingSource.Sabre) && !flightItinerary.IsLCC)
    {%>
                             <%=GetBaggageForGDS(flightItinerary.FlightId,flightItinerary.Passenger[paxIndex].Type)%>
    <%}   

    else if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.PKFares || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp || flightItinerary.FlightBookingSource == BookingSource.Jazeera || flightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || flightItinerary.FlightBookingSource == BookingSource.Amadeus || flightItinerary.FlightBookingSource == BookingSource.Sabre || (flightItinerary.FlightBookingSource == BookingSource.TBOAir && (flightItinerary.IsLCC)))
    {
        if (flightItinerary.Passenger[paxIndex].Type != PassengerType.Infant)
        { %>
                                                            <%=(string.IsNullOrEmpty(flightItinerary.Passenger[paxIndex].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[paxIndex].BaggageCode)%>
                                                     <%}
    }    %>
                                                        </td>
                                                        <%if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.UAPI || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{
                                                                string MealDesc = string.Empty;
                                                                %>
                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (flightItinerary.Passenger[paxIndex].Type != PassengerType.Infant && !string.IsNullOrEmpty(flightItinerary.Passenger[paxIndex].MealDesc)) %>
                                                            <%{
                                                                    MealDesc = flightItinerary.Passenger[paxIndex].MealDesc;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Meals"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (flightItinerary.Passenger[paxIndex].Type != PassengerType.Infant && !string.IsNullOrEmpty(flightItinerary.Passenger[paxIndex].Seat.Code)) %>
                                                            <%{
                                                                    MealDesc = flightItinerary.Passenger[paxIndex].Seat.Code;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Seats"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <%} %>

                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word"><%=(flightItinerary.Segments[count].AirlinePNR == null || flightItinerary.Segments[count].AirlinePNR == "" ? (flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR) : flightItinerary.Segments[count].AirlinePNR) %></td>
                                                    </tr>










                                                    <%}





                                                        else if (hdfTicketType.Value == "Multiple")
                                                        {%>
                                                    <%for (int j = 0; j < flightItinerary.Passenger.Length; j++)
                                                        {
                                                    %>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">	
                                                   <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">      <%=flightItinerary.Passenger[j].Title + " " + flightItinerary.Passenger[j].FirstName + " " + flightItinerary.Passenger[j].LastName%></td>
                                                  <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%if (Request["bkg"] == null)
                                                                { %>
                                                            <%=(ticketList[j].TicketNumber.Split('|').Length > 1 ? ticketList[j].TicketNumber.Split('|')[flightItinerary.Segments[count].Group] : ticketList[j].TicketNumber) %>
                                                            <%}
                                                                else
                                                                { %>
                                                            <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[count] : flightItinerary.PNR)%>
                                                            <%} %></td>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            
                                                            
   <%if (flightItinerary.FlightBookingSource == BookingSource.Amadeus || flightItinerary.FlightBookingSource == BookingSource.UAPI || (flightItinerary.FlightBookingSource == BookingSource.TBOAir || flightItinerary.FlightBookingSource == BookingSource.Babylon || flightItinerary.FlightBookingSource == BookingSource.Sabre) && !flightItinerary.IsLCC)
       {%>
                             <%=GetBaggageForGDS(flightItinerary.FlightId, flightItinerary.Passenger[j].Type)%>
    <%}
    else if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai || 
            flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.Indigo || 
            flightItinerary.FlightBookingSource == BookingSource.PKFares || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || 
            flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || 
            flightItinerary.FlightBookingSource == BookingSource.GoAirCorp || flightItinerary.FlightBookingSource == BookingSource.Jazeera || 
            flightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || flightItinerary.FlightBookingSource == BookingSource.Amadeus ||  flightItinerary.FlightBookingSource == BookingSource.Sabre ||
            (flightItinerary.FlightBookingSource == BookingSource.TBOAir && flightItinerary.IsLCC))
    {
        if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
        { %>
                                                            <%=(string.IsNullOrEmpty(flightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[j].BaggageCode)%>
                                                     <%}
    }%>
    
     </td>


                                                        <%if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.UAPI || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{
                                                                string MealDesc = string.Empty;
                                                                %>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (flightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(flightItinerary.Passenger[j].MealDesc)) %>
                                                            <%{
                                                                    MealDesc = flightItinerary.Passenger[j].MealDesc;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Meals"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (flightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(flightItinerary.Passenger[j].Seat.Code)) %>
                                                            <%{
                                                                    MealDesc = flightItinerary.Passenger[j].Seat.Code;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Seats"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <%} %>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">  <%=(flightItinerary.Segments[count].AirlinePNR == null || flightItinerary.Segments[count].AirlinePNR == "" ? (flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR) : flightItinerary.Segments[count].AirlinePNR) %></td>
                                                    </tr>
                                                    <%}
                                                        }%>




            	  		</table>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th>

            	  	                                                                                                                                                                                                                                                                                                                                                                                                                                                    </tr></table></th>	  	
            	  </tr></tbody></table>

    <% }%>





            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">			  
            	  	  <table class="flight-list-table" style="border: 1px solid #f3f3f3; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  	    <tr class="first-row" style="padding: 0; text-align: left; vertical-align: top">
            	  	    	<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 4px; text-align: left; vertical-align: top; word-wrap: break-word;">

            	  	    		<img class="float-left" src="https://ctb2b.cozmotravel.com/images/AirlineLogo/<%=flightItinerary.Segments[count].Airline%>.gif" alt="<%=airline.AirlineName%>" style="-ms-interpolation-mode: bicubic; clear: both; display: block; float: left; max-width: 100%; outline: none; padding-right: 5px; text-align: left; text-decoration: none; width: auto">

            					<p class="float-left" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; padding-top: 5px; text-align: left"><%airline.Load(flightItinerary.Segments[count].Airline); %> <%=airline.AirlineName%> <strong><%=flightItinerary.Segments[count].Airline + "-" + flightItinerary.Segments[count].FlightNumber%></strong>
                                           <%try
                                                                                                {
                                                                                                    if ((!flightItinerary.IsLCC && flightItinerary.Segments[count].OperatingCarrier != null && flightItinerary.Segments[count].OperatingCarrier.Length > 0)||(flightItinerary.FlightBookingSource == BookingSource.Indigo && flightItinerary.Segments[count].OperatingCarrier != null && flightItinerary.Segments[count].OperatingCarrier.Length > 0 && flightItinerary.Segments[count].OperatingCarrier != "6E"))////Modified for Indigo CodeShare
                                                                                                    {
                                                                                                        string opCarrier = flightItinerary.Segments[count].OperatingCarrier;
                                                                                                        Airline opAirline = new Airline();
                                                                                                        if (opCarrier.Split('|').Length > 1)
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Split('|')[0]);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Substring(0, 2));
                                                                                                        } %>
                                             (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                                }
                                                catch { } %>
                                    
                                    </p>
            	  	    	</td>
            	  	    	<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 4px; text-align: left; vertical-align: middle; word-wrap: break-word">
            	  	    		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right"><strong> <%=flightItinerary.Segments[count].Origin.CityName%> to <%=flightItinerary.Segments[count].Destination.CityName%></strong> | <strong><%=flightItinerary.Segments[count].DepartureTime.ToString("ddd")+","+flightItinerary.Segments[count].DepartureTime.ToString("dd MMM yyyy")%></strong></p>
            	  	    	</td>
            	  	    </tr>
            			<tr style="padding: 0; text-align: left; vertical-align: top">
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word;width:30%">
            					<strong class="city-code" style="font-size: 18px"><%=flightItinerary.Segments[count].Origin.CityCode%></strong> <%=flightItinerary.Segments[count].Origin.CityName%><br>
            					<strong class="time" style="font-size: 14px"><%=flightItinerary.Segments[count].DepartureTime.ToString("HH:mm tt")%></strong><br>
            					<%=flightItinerary.Segments[count].DepartureTime.ToString("ddd")+","+flightItinerary.Segments[count].DepartureTime.ToString("dd MMM yyyy")%><br>
            					<%=flightItinerary.Segments[count].Origin.AirportName %><br>
            					Terminal <%=flightItinerary.Segments[count].DepTerminal %>
            				</td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: center; vertical-align: middle; word-wrap: break-word;width:20%">
            				 <center style="min-width: 21px; width: 100%" data-parsed="">
            						<img src="https://ctb2b.cozmotravel.com/images/aircraft_icon.png" alt="Flight Duration" style="-ms-interpolation-mode: bicubic; Margin: 0 auto; clear: both; display: block; float: none; margin: 0 auto; max-width: 100%; min-width: 21px; outline: none; text-align: center; text-decoration: none; vertical-align: middle; width: 21px" align="center" class="float-center">
            						   <%=flightItinerary.Segments[count].Duration.Hours +"hr "+flightItinerary.Segments[count].Duration.Minutes+"m"%>
            				  </center>
            			  </td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; border-right: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word;width:30%">
            					<strong class="city-code" style="font-size: 18px"><%=flightItinerary.Segments[count].Destination.CityCode%></strong> <%=flightItinerary.Segments[count].Destination.CityName%><br> 
            					<strong class="time" style="font-size: 14px"><%=flightItinerary.Segments[count].ArrivalTime.ToString("HH:mm tt")%></strong><br>
            					<%=flightItinerary.Segments[count].ArrivalTime.ToString("ddd")+","+flightItinerary.Segments[count].ArrivalTime.ToString("dd MMM yyyy")%><br>
            					<%=flightItinerary.Segments[count].Destination.AirportName %><br>
            					Terminal  <%=flightItinerary.Segments[count].ArrTerminal %>
            				</td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word;width:20%">
            					<strong>
                                     <%if (flightItinerary.Segments[count].Stops == 0) %>
                    <%{ %>
                    <strong> Non-Stop </strong>
                    <%} %>
                    <%else if (flightItinerary.Segments[count].Stops == 1) %>
                    <%{ %>
                    <strong> Single Stop </strong>
                    <%} %>
                    <%else if (flightItinerary.Segments[count].Stops == 2) %>
                    <%{ %>
                    <strong> Two Stops </strong>
                    <%} %>
                    <%else if (flightItinerary.Segments[count].Stops> 2) %>
                    <%{ %>
                    <strong> Two+ Stops </strong>
                    <%} %>

            					</strong><br>
            					Class | <%=flightItinerary.Segments[count].CabinClass%><br>
                                      <%if (flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp || flightItinerary.FlightBookingSource == BookingSource.Jazeera || flightItinerary.FlightBookingSource == BookingSource.AirArabia)
                                                                                                {%>
            					Fare Type | <%=flightItinerary.Segments[count].SegmentFareType%><br>
                                        <%} %>
            					<strong><%=booking.Status.ToString()%></strong>
            				</td>
            			</tr>










            		</table></th>

  

<%--<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th>--%>


            	  	                                                                                                                                                                                                                                                                                                                                                                                                                                                    </tr></table></th>		  	
            	  </tr></tbody></table>   <%}%>  



            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 15px; font-weight: bold; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left; word-wrap: normal"><%--Price Details--%></h3>
            	  	</th>
                          <th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: right"> <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: #5f5f5f;" id="ancFare" runat="server"
                                            onclick="return ShowHide('divFare');"  class="showhideDiv">Hide Fare</a>	</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>


              
                           
                            <div title="Param" id="divFare" runat="server" style="font-size: 12px;">
                                 
                                
                            

 <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0, outputVAT = 0, Meal = 0, SeatPrice = 0, k3Tax = 0, mgmtFeeIndia = 0;//Variable which holds the managment fee for India Location;
     //Case1:If location country code is India then do not add the markup and asv element into the tax components
     //Case2If location country code is not India then add the markup and asv element into the tax components
     //New requirement by vinay
     CT.TicketReceipt.BusinessLayer.LocationMaster locationMgmtFee = new CT.TicketReceipt.BusinessLayer.LocationMaster(flightItinerary.LocationId);
     if (Request["bkg"] == null) // For Ticketing
     {
         int paxId = -1;

         List<TaxBreakup> allPaxTaxBreakups = new List<TaxBreakup>();
         ticketList.ForEach(t =>  t.TaxBreakup.ForEach(tb => { TaxBreakup tax = new TaxBreakup(); tax.TaxCode = tb.Key; tax.TaxValue = tb.Value; allPaxTaxBreakups.Add(tax); } ));

         switch(flightItinerary.FlightBookingSource)
         {
             case BookingSource.GoAir:
             case BookingSource.GoAirCorp:
                 k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("ST") || t.TaxCode.Contains("CT") || t.TaxCode.Contains("IT")? t.TaxValue : 0);
                 break;
             case BookingSource.UAPI:
                 k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                 break;
             case BookingSource.SpiceJet:
             case BookingSource.SpiceJetCorp:
             case BookingSource.Indigo:
             case BookingSource.IndigoCorp:
                 k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("GST") ? t.TaxValue : 0);
                 break;
             default:
                 k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                 break;
         }

         k3Tax = Convert.ToDecimal(k3Tax.ToString("N" + agency.DecimalValue));
         if (Request["paxId"] != null)
         {
             paxId = Convert.ToInt32(Request["paxId"]);
         }
         if(paxId >-1 && flightItinerary.Passenger[paxId].Type == PassengerType.Infant && (flightItinerary.FlightBookingSource!=BookingSource.TBOAir && flightItinerary.FlightBookingSource!=BookingSource.UAPI ))
         {
             k3Tax = 0;
         }
         //if(paxId>=0)
         //{
         //    k3Tax = k3Tax / flightItinerary.Passenger.Length;
         //}
         
         for (int k = 0; k < ticketList.Count; k++)
         {
             AirFare += ticketList[k].Price.PublishedFare + ticketList[k].Price.HandlingFeeAmount;
             if (locationMgmtFee.CountryCode == "IN")//Case1:If location country code is India then do not add the markup and asv element into the tax components
             {
                 mgmtFeeIndia +=  ticketList[k].Price.Markup + ticketList[k].Price.B2CMarkup;
                 //mgmtFeeIndia += ticketList[k].Price.AsvAmount;
                 Taxes += ticketList[k].Price.Tax;
             }
             else//Case2If location country code is not India then add the markup and asv element into the tax components
             {
                 Taxes += ticketList[k].Price.Tax + ticketList[k].Price.Markup + ticketList[k].Price.B2CMarkup;
             }
             if(string.IsNullOrEmpty(flightItinerary.RoutingTripId))
             {
                 k3Tax = 0;
             }
             if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
             {
                 Taxes += ticketList[k].Price.OtherCharges + ticketList[k].Price.SServiceFee + ticketList[k].Price.AdditionalTxnFee + ticketList[k].Price.TransactionFee;
             }
             if (flightItinerary.FlightBookingSource == BookingSource.OffLine)
             {
                 Taxes -= (ticketList[k].Price.AdditionalTxnFee + ticketList[k].Price.TransactionFee);
             }
             Baggage += ticketList[k].Price.BaggageCharge;
             Meal += ticketList[k].Price.MealCharge;
             SeatPrice += ticketList[k].Price.SeatPrice;
             MarkUp += ticketList[k].Price.Markup + ticketList[k].Price.B2CMarkup;
             Discount += ticketList[k].Price.Discount;
             AsvAmount += ticketList[k].Price.AsvAmount;
             outputVAT += ticketList[k].Price.OutputVATAmount;
         }
         if (ticketList[0].Price.AsvElement == "BF")
         {
             AirFare += AsvAmount;
         }
         else if (ticketList[0].Price.AsvElement == "TF")
         {
             Taxes += AsvAmount;
         }
     }
     else // For Hold Itinerary View
     {
         
         int paxId = 0;

         if (Request["paxId"] != null)
         {
             paxId = Convert.ToInt32(Request["paxId"]);
         }

         for (int i = 0; i < flightItinerary.Passenger.Length; i++)
         {
             if (Request["paxId"] != null && i == paxId) // Induvidual
             {
                 FlightPassenger pax = flightItinerary.Passenger[i];
                 AirFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;

                 if (locationMgmtFee.CountryCode == "IN")//Case1:If location country code is India then do not add the markup and asv element into the tax components
                 {
                     mgmtFeeIndia += pax.Price.Markup + pax.Price.B2CMarkup;
                     //mgmtFeeIndia += pax.Price.AsvAmount;
                     Taxes += pax.Price.Tax;
                 }
                 else//Case2If location country code is not India then add the markup and asv element into the tax components
                 {
                     Taxes += pax.Price.Tax + pax.Price.Markup + pax.Price.B2CMarkup;
                 }


                 if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                 {
                     Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                 }
                 if (flightItinerary.FlightBookingSource == BookingSource.OffLine)
                 {
                     Taxes += (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                 }
                 Baggage += pax.Price.BaggageCharge;
                 Meal += pax.Price.MealCharge;
                 SeatPrice += pax.Price.SeatPrice;
                 MarkUp += pax.Price.Markup + pax.Price.B2CMarkup;
                 Discount += pax.Price.Discount;
                 AsvAmount += pax.Price.AsvAmount;
                 outputVAT += pax.Price.OutputVATAmount;
                 break;
             }
             if (Request["paxId"] == null)// For all
             {
                 FlightPassenger pax = flightItinerary.Passenger[i];
                 AirFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;

                 if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                 {
                     Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                 }
                 if (flightItinerary.FlightBookingSource == BookingSource.OffLine)
                 {
                     Taxes += (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                 }
                 else
                     Taxes += pax.Price.Tax + pax.Price.Markup+pax.Price.B2CMarkup;

                 Baggage += pax.Price.BaggageCharge;
                 Meal += pax.Price.MealCharge;
                 SeatPrice += pax.Price.SeatPrice;
                 MarkUp += pax.Price.Markup+pax.Price.B2CMarkup;
                 Discount += pax.Price.Discount;
                 AsvAmount += pax.Price.AsvAmount;
                 outputVAT += pax.Price.OutputVATAmount;//Load GST or VAT for Hold bookings also in order to match the total
             }
         }

         if (flightItinerary.Passenger[0].Price.AsvElement == "BF")
         {
             AirFare += AsvAmount;
         }
         else if (flightItinerary.Passenger[0].Price.AsvElement == "TF")
         {
             Taxes += AsvAmount;
         }
     }
                                            %>
                                            <% //if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.BaseAgent)
                                               // { %>

            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<table class="pricing-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Air Fare</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=AirFare.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right"> Taxes & Fees</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word"> <%=(Taxes-k3Tax).ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
                              <%CT.TicketReceipt.BusinessLayer.LocationMaster locationMgmtFeeIn = new CT.TicketReceipt.BusinessLayer.LocationMaster(flightItinerary.LocationId);
                                                                                             if (locationMgmtFeeIn.CountryCode == "IN")
                                                                                            {%>
                              <tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">
                                                                 
                                                                                        Management Fee
                                                                                        
                                                                                           

                                      </th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=mgmtFeeIndia.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
                                  <%} %>

                              <%if (!string.IsNullOrEmpty(flightItinerary.RoutingTripId)) { %>   
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right"> K3 Tax</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=k3Tax.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>

            	  			</tr>
                     <%} %>

                          <%if (flightItinerary.IsLCC)
                                                                                    { %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Baggage Fare </th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=Baggage.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>

                                   <%} %>


                                        <%if (flightItinerary != null && (flightItinerary.FlightBookingSource == BookingSource.UAPI || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp || flightItinerary.FlightBookingSource == BookingSource.AirArabia)) %>
                                                                                <%{ %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Meal Price</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=Meal.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Seat Price</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">    <%=SeatPrice.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>

     <%} %>


             <%if (Discount > 0)
                                                                                    { %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Discount</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  -<%=Discount.ToString("N" + agency.DecimalValue) %><%=agency.AgentCurrency%></td>
            	  			</tr>
       <%} %>


            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">
                                                                 <%CT.TicketReceipt.BusinessLayer.LocationMaster locationGST = new CT.TicketReceipt.BusinessLayer.LocationMaster(flightItinerary.LocationId);
                                                                                             if (locationGST.CountryCode == "IN")
                                                                                            {%>
                                                                                        GST
                                                                                        <%}    else
    { %>
                                                                                        VAT
                                                                                        <%} %>
                                                                                           

                                      </th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=outputVAT.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>

           


            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  
                                 <th valign="middle" width="220px" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">
            				 	     <strong style="font-size: 11px"> This is an Electronic ticket. Please carry a positive identification for Check in.</strong>
            				     </th>					
            	  				<th width="220px" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right;vertical-align:middle"><strong>Total Air Fare</strong></th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word;vertical-align:middle">

                                       <%if (flightItinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                                                                                            { %>
                                                                                        <b><%=Math.Ceiling((AirFare + Taxes + Baggage + outputVAT + mgmtFeeIndia) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%}
                                                                                         else if (flightItinerary.FlightBookingSource == BookingSource.OffLine) 
                                                                                            { %>
                                                                                        <b><%=((AirFare + Taxes + outputVAT + mgmtFeeIndia ) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%}

                                                                                            else
                                                                                            { %>
                                                                                        <b><%=((AirFare + Taxes + Baggage + Meal + SeatPrice + outputVAT + mgmtFeeIndia) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%} %>
            	  				</td>
            	  			</tr>
            	  		</table>
            	  	</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>

     <%//}
                                              %>

         </div>

            	    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
             	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">For international flights, you are suggested to arrive at the airport at least 3-4 hours prior to your departure time to check in and go through security. For connecting flights, please confirm at the counter whether not you can check your luggage through to the final destination when you check in.</p>
            			 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">Don’t forget to purchase travel insurance for your visit.Please contact us to purchase travel insurance.</p>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>
            	</td></tr>
                    <tr><td>
                       <% if (flightItinerary != null && flightItinerary.Segments.ToList().All(x => x.Destination.CountryCode == x.Origin.CountryCode) && (flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp))
               { %>
             <%if (flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp)
               { %>
                 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
             	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">
             <strong style="color:red">Important Update: Please read carefully.</strong><br/>
            Due to ongoing COVID-19 situation, for Safe and Seamless journey and as per the implementation of various safety procedure<br/>
            For Safe and Seamless travels of our guests. It is mandatory to do <span style="text-decoration:underline;font-weight:bold"><span style="color:red">“Web Check-in” along with carrying Boarding pass, affixing of baggage label</span> and following all government guidelines while travelling.</span>
            <ul style="font-weight:bold;">
            <li>Web check-in is now mandatory for each passenger for all Domestic flights.</li>
            <li>Mandatory Check-in: Web check-in to be done upto <span style="text-decoration:underline">75 minutes</span> prior from the schedule flight departure of their domestic flight.<br/>
            How to Check In :  Click on the following link which will take you to the Airline Website <a href="https://www.goindigo.in/how-to-check-in.html?linkNav=how-to-check-in_header" target="_blank">https://www.goindigo.in/how-to-check-in.html?linkNav=how-to-check-in_header</a>. Once clicking on the link, it will be redirected to Indigo Airline website Check-in page. Passengers have to mention PNR and Email/Last name in the specified field to proceed with the Check-in process.</li> 
            <li>Passenger Health declaration status and their mobile should have <span style="text-decoration:underline">“Arogya Setu”</span> app which shows Green status.  Mandatory to add <span style="text-decoration:underline">SSR “HCOK”</span> while Web Checkin.</li>
            <%if(agency.AgentCurrency != "INR"){ //Hide only for India Agents%>
            <li>Passengers travelling with Check in luggage, need to add their luggage during web check-in. Per Passenger 1 piece of Checked in Baggage upto 20 kgs will be allowed. Addition to this one small hand luggage max of 7 Kgs per passenger will be allowed.</li>   
                <%} %>
            <li>Carriage of printout of Boarding card and Baggage label which will be sent to the guest on their contact details or can download during web checkin.</li> 
            <li>Also can carry Mobile boarding pass and carry printed or written baggage label, format will be available on Indigo website.</li> 
            <li>Before reaching to the airport, Passenger need to paste the attached Baggage label on the baggage OR guest can write the mandatory fields i.e. PNR, Name of the passenger, Sector and Flight Number on plain paper & paste the same on baggage which should be prominently visible, as airport may not be having any pasting material to place the labels on the baggage.</li>
            <li>Passenger must be wearing mask properly at the airport & throughout the journey.</li> 
            <li>Passenger must sanitize at airports at various points.</li> 
            <li>Maintain Social distancing at the airport as per the markings/signage.</li> 
            <li>For Safety of all customers, Indigo reserves the rights to refuse the carriage of any passenger whose contact details are missing. Also, its mandatory to do the Web Check-in.</li>
            </ul>
            
                    </p>
            			 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left"></p>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>
            <%}
               if (flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
               { %>
                <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
             	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">
                <strong style="color:red">Important Update: Please read carefully.</strong><br/>
                Due to ongoing COVID-19 situation, for Safe and Seamless journey and as per the implementation of various safety procedure<br/>
                For Safe and Seamless travels of our guests. It is mandatory to do <span style="text-decoration:underline;font-weight:bold"><span style="color:red">“Web Check-in” along with carrying Boarding pass, affixing of baggage label</span> and following all government guidelines while travelling.</span>
                <ul style="font-weight:bold;">
                <li>Web check-in is now mandatory for each passenger for all Domestic flights.</li>
                <li>Mandatory Check-in: Web check-in to be done upto <span style="text-decoration:underline">75 minutes</span> prior from the schedule flight departure of their domestic flight.<br/>
                How to Check In :  Click on the following link which will take you to the Airline Website <a href="https://book.spicejet.com/Search.aspx?op=4" target="_blank">https://book.spicejet.com/Search.aspx?op=4 </a>. Once clicking on the link, it will be redirected to Spice Jet Airline website Check-in page. Passengers have to mention PNR and Email/Last name in the specified field to proceed with the Check-in process.</li> 
                <li>Passenger Health declaration status and their mobile should have <span style="text-decoration:underline">“Arogya Setu”</span> app which shows Green status.  Mandatory to add <span style="text-decoration:underline">SSR “HCOK”</span> while Web Checkin.</li>
                <%if(agency.AgentCurrency != "INR"){ //Hide only for India Agents%>
            <li>Passengers travelling with Check in luggage, need to add their luggage during web check-in. Per Passenger 1 piece of Checked in Baggage upto 20 kgs will be allowed. Addition to this one small hand luggage max of 7 Kgs per passenger will be allowed.</li>   
                <%} %>
                <li>Carriage of printout of Boarding card and Baggage label which will be sent to the guest on their contact details or can download during web checkin.</li> 
                <li>Also can carry Mobile boarding pass and carry printed or written baggage label, format will be available on Spice Jet website.</li> 
                <li>Before reaching to the airport, Passenger need to paste the attached Baggage label on the baggage OR guest can write the mandatory fields i.e. PNR, Name of the passenger, Sector and Flight Number on plain paper & paste the same on baggage which should be prominently visible, as airport may not be having any pasting material to place the labels on the baggage.</li>
                <li>Passenger must be wearing mask properly at the airport & throughout the journey.</li> 
                <li>Passenger must sanitize at airports at various points.</li> 
                <li>Maintain Social distancing at the airport as per the markings/signage.</li> 
                <li>For Safety of all customers, Spice Jet reserves the rights to refuse the carriage of any passenger whose contact details are missing. Also, its mandatory to do the Web Check-in.</li>
                </ul></p>
            			 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left"></p>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>
             <%}
               } %>
                        </td></tr>
                </tbody></table>
            
            </td></tr></table>
            
          </center>
        </td>
      </tr>
    </table>



            	<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
  
    <!-- prevent Gmail on iOS font size manipulation -->
   <div style="display:none; white-space:nowrap; font:15px courier; line-height:0"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
  </div>


                                






                          


                            <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.Agent)
                                { %>
                            <script type="text/javascript">ShowHide(&quot;divFare&quot;);</script> 
                            <%} %>


                            <table style="display: none;">
                                <tbody>
                                 
                                    <tr style="display: none;">
                                        <td><strong>Important Information </strong></td>
                                    </tr>
                                 

                                    <tr style="display: none;">
                                        <td>
                                            <ul>
                                                <li>Use your Trip ID for all communication with Cleartrip about this booking </li>
                                                <li>Check-in counters for International flights close 75 minutes before departure</li>
                                                <li>Your carry-on baggage shouldn't weigh more than 7kgs</li>
                                                <li>Carry photo identification, you will need it as proof of identity while checking-in</li>
                                                <li>Kindly ensure that you have the relevant visa, immigration clearance and travel with a passport, with a validity of at least 6 months.</li>
                                                <li>For hassle free refund processing, cancel/amend your tickets with Cleartrip Customer Care instead of doing so directly with Airline.</li>
                                            </ul>
                                        </td>
                                    </tr>
                                
                                    <tr style="display: none;">
                                        <td>
                                            <img src="https://ctb2b.cozmotravel.com/images/goapic.jpg" width="728" height="90" alt="#" /></td>
                                    </tr>
                                </tbody>
                            </table>

                                    <!-- Added by lokesh on 7-June-2018-->
        <!-- Need to display the below note in email for PK Fares -->
         <%if (flightItinerary != null && flightItinerary.FlightBookingSource == BookingSource.PKFares) %>
         <%{ %>
         <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                <span style="color:red">Note: This is just an itinerary. Ticket copy should not be handed over to customer unless ticket number is shown on the ticket.</span>
                    
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <%}%>



                        </div>
                   <%if(routingItinerary != null){ %>
                        <div>

                            <div style="display: none;" id="emailSent">
                                <div style="float: left; width: 100%; margin: auto; text-align: center;">
                                    <span class="email-message-child" id="messageText">Email sent successfully</span>
                                </div>
                            </div>
                        
                            <% bc = 0;
                                
                                if (Request["bkg"] != null)
                                {
                                    ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(routingItinerary.FlightId);
                                }
                                else if(routingTicketList.Count > 0)// Segment Wise Booking
                                {
                                    ptcDetails = routingTicketList[0].PtcDetail;
                                }
                                for (int count = 0; count < routingItinerary.Segments.Length; count++)
                                {
                                    int paxIndex = 0;
                                    if (Request["paxId"] != null)
                                    {
                                        paxIndex = Convert.ToInt32(Request["paxId"]);
                                    }
                                    List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
                                    if (Request["paxId"] == null)
                                    {
                                        ptcDetail = ptcDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.SegmentId == routingItinerary.Segments[count].SegmentId; });
                                    }
                                    else
                                    {
                                        ptcDetail = ptcDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.SegmentId == routingItinerary.Segments[count].SegmentId && ptc.PaxType == paxType; });
                                    }%>









      <div style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box; -webkit-text-size-adjust: 100%; Margin: 0; background: #f3f3f3 !important; box-sizing: border-box; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100% !important">
    <span class="preheader" style="color: #f3f3f3; display: none !important; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; mso-hide: all !important; opacity: 0; overflow: hidden; visibility: hidden"></span>
    <table class="body" style="Margin: 0; background: #f3f3f3 !important; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
      <tr style="padding: 0; text-align: left; vertical-align: top">
        <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
          <center data-parsed="" style="min-width: 700px; width: 100%">
            <table align="center" class="wrapper b2b-eticket-wrapper float-center" style="Margin: 0 auto; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><td class="wrapper-inner" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">

                <%if (count == 0)
                    { %>
            	<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
                    <% } %>
            	<table align="center" class="container" style="Margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0; text-align: inherit; vertical-align: top; width: 700px"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
            
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            
    <%if (count == 0)    { %>
                            
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-9 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 509px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<h2 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 22px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left; word-wrap: normal"><strong>e-Ticket</strong></h2>
            			 <span>   <%if (routingItinerary != null && routingItinerary.CreatedOn != DateTime.MinValue) %>
                    <%{ %>
                                              Booking Date:  <%=routingItinerary.CreatedOn.ToString("ddd") + "," + routingItinerary.CreatedOn.ToString("dd MMM yyyy")%>
                    <%} %>	</span>
            		</th></tr></table></th>
            		<th class="small-12 large-3 columns last" right="" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 159px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			
                              <asp:Image ID="Image1" style="-ms-interpolation-mode: bicubic; border-width: 0px; clear: both; display: block; height: 51px; max-width: 100%; outline: none; text-decoration: none; width: 159px" runat="server" AlternateText="AgentLogo" ImageUrl="https://ctb2b.cozmotravel.com/images/Logo.jpg" />
            		</th></tr></table></th>
            	  </tr></tbody></table>
            	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 15px; font-weight: bold; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left; word-wrap: normal">Reservation Details</h3>
            		</th></tr></table></th>
            		
            	  </tr></tbody></table>
            
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody>
                      
                      <tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">PNR NO. | <strong><%=(routingItinerary.PNR.Split('|').Length > 1 ? routingItinerary.PNR.Split('|')[routingItinerary.Segments[count].Group] : routingItinerary.PNR)%></strong></p>
            	  	</th></tr></table></th>
            	  	<%--<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">Agent Name |  <strong> <%=agency.Name %></strong></p>
            	  	</th></tr></table></th>--%>

                          <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo != null) %>
                          <%{
                                  var parentid1 =CT.TicketReceipt.BusinessLayer.AgentMaster.GetParentId(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId);
                                  CT.TicketReceipt.BusinessLayer.LocationMaster location1 = new CT.TicketReceipt.BusinessLayer.LocationMaster(routingItinerary.LocationId); 
                                  
                                  %>
                                  
                          <%if (parentid1 == 2125 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId == 2125) %>
                          <%{ %>
            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">Client |  <strong> <%=agency.Name %></strong>Address | <strong> <%=location1.Address %></strong> </p>
            	  	</th></tr></table></th>
                          <%} %>
                          <%else %>
                          <%{ %>
                          <th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">Agent Name |  <strong> <%=agency.Name %></strong></p>
            	  	</th></tr></table></th>

                          <%} %>

                          <%} %>



            	  </tr>




            	                                                                                                                                                                              </tbody></table>
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            
        <%if(!string.IsNullOrEmpty(routingItinerary.RoutingTripId)){ %>

            	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left"> Routing Trip Id  |  <strong><%=routingItinerary.RoutingTripId %></strong></p>
            	  	</th></tr></table></th>

                     


 <%} %>

            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">PHONE  |  <strong><%=agency.Phone1 %></strong></p>
            	  	</th></tr></table></th>
            	  </tr></tbody></table>

   <% }%>

       <%if (count == 0)    { %>
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">	
            	  		<table class="b2b-baggage-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Passenger Name</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">E-Ticket Number	</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Baggage</th>
            					   <%if (routingItinerary != null && routingItinerary.FlightBookingSource == BookingSource.AirArabia || routingItinerary.FlightBookingSource == BookingSource.UAPI || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp)  %>
                                                        <%{ %>
                                  <th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Meal</th>

            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Seats</th>
            				  <%} %>
                                  
                                  	<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Airline Ref.</th>
            	  			</tr>




 <%if (hdfTicketType.Value == "Individual")
                                                        { %>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word"> <%=paxName %></td>
                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%if (Request["bkg"] == null && routingTicketList.Count > 0)
                                                                { %>
                                                            <%=(routingTicketList[paxIndex].TicketNumber.Split('|').Length > 1 ? routingTicketList[paxIndex].TicketNumber.Split('|')[routingItinerary.Segments[count].Group] : routingTicketList[paxIndex].TicketNumber) %>
                                                            <%}
                                                                else
                                                                { %>
                                                            <%=(routingItinerary.PNR.Split('|').Length > 1 ? routingItinerary.PNR.Split('|')[count] : routingItinerary.PNR)%>
                                                            <%} %></td>
                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%if (routingItinerary.FlightBookingSource == BookingSource.Amadeus || routingItinerary.FlightBookingSource == BookingSource.UAPI || (routingItinerary.FlightBookingSource == BookingSource.TBOAir || flightItinerary.FlightBookingSource == BookingSource.Babylon  && !(routingItinerary.IsLCC)))
    {%>
                             <%=GetBaggageForGDS(routingItinerary.FlightId,routingItinerary.Passenger[paxIndex].Type)%>
    <%}   

 else if (routingItinerary.FlightBookingSource == BookingSource.AirArabia || routingItinerary.FlightBookingSource == BookingSource.FlyDubai || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.PKFares || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp || routingItinerary.FlightBookingSource == BookingSource.Jazeera || routingItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || routingItinerary.FlightBookingSource == BookingSource.Amadeus || (routingItinerary.FlightBookingSource == BookingSource.TBOAir && (routingItinerary.IsLCC)))
    {
        if (routingItinerary.Passenger[paxIndex].Type != PassengerType.Infant)
        { %>
                                                            <%=(string.IsNullOrEmpty(routingItinerary.Passenger[paxIndex].BaggageCode) ? "Airline Norms" : routingItinerary.Passenger[paxIndex].BaggageCode)%>
                                                     <%}
    }%></td>
                                                        <%if (routingItinerary.FlightBookingSource == BookingSource.AirArabia || routingItinerary.FlightBookingSource == BookingSource.UAPI || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{
                                                                string MealDesc = string.Empty;
                                                                %>
                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (routingItinerary.Passenger[paxIndex].Type != PassengerType.Infant && !string.IsNullOrEmpty(routingItinerary.Passenger[paxIndex].MealDesc)) %>
                                                            <%{
                                                                    MealDesc = routingItinerary.Passenger[paxIndex].MealDesc;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Meals"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (routingItinerary.Passenger[paxIndex].Type != PassengerType.Infant && !string.IsNullOrEmpty(routingItinerary.Passenger[paxIndex].Seat.Code)) %>
                                                            <%{
                                                                    MealDesc = routingItinerary.Passenger[paxIndex].Seat.Code;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Seats"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <%} %>

                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%=(routingItinerary.Segments[count].AirlinePNR == null || routingItinerary.Segments[count].AirlinePNR == "" ? (routingItinerary.PNR.Split('|').Length > 1 ? routingItinerary.PNR.Split('|')[routingItinerary.Segments[count].Group] : routingItinerary.PNR) : routingItinerary.Segments[count].AirlinePNR) %></td>
                                                    </tr>
                                                    <%}
                                                        else if (hdfTicketType.Value == "Multiple")
                                                        {%>
                                                    <%for (int j = 0; j < routingItinerary.Passenger.Length; j++)
                                                        {
                                                    %>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%=routingItinerary.Passenger[j].Title + " " + routingItinerary.Passenger[j].FirstName + " " + routingItinerary.Passenger[j].LastName%></td>
                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%if (Request["bkg"] == null && routingTicketList.Count > 0 && routingTicketList.Count > j)
                                                                { %>
                                                            <%=(routingTicketList[j].TicketNumber.Split('|').Length > 1 ? routingTicketList[j].TicketNumber.Split('|')[routingItinerary.Segments[count].Group] : routingTicketList[j].TicketNumber) %>
                                                            <%}
                                                                else
                                                                { %>
                                                            <%=(routingItinerary.PNR.Split('|').Length > 1 ? routingItinerary.PNR.Split('|')[count] : routingItinerary.PNR)%>
                                                            <%} %></td>
                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            
                                                            
   <%if (routingItinerary.FlightBookingSource == BookingSource.Amadeus || routingItinerary.FlightBookingSource == BookingSource.UAPI || (routingItinerary.FlightBookingSource == BookingSource.TBOAir || flightItinerary.FlightBookingSource == BookingSource.Babylon && !(routingItinerary.IsLCC)))
    {%>
                             <%=GetBaggageForGDS(routingItinerary.FlightId,routingItinerary.Passenger[j].Type)%>
    <%}                                                            
 else if (routingItinerary.FlightBookingSource == BookingSource.AirArabia || routingItinerary.FlightBookingSource == BookingSource.FlyDubai || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.PKFares || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp || routingItinerary.FlightBookingSource == BookingSource.Jazeera || routingItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || routingItinerary.FlightBookingSource == BookingSource.Amadeus || (routingItinerary.FlightBookingSource == BookingSource.TBOAir && (routingItinerary.IsLCC)))
    {
        if (routingItinerary.Passenger[j].Type != PassengerType.Infant)
        { %>
                                                            <%=(string.IsNullOrEmpty(routingItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : routingItinerary.Passenger[j].BaggageCode)%>
                                                     <%}
    } %> </td>


                                                        <%if (routingItinerary.FlightBookingSource == BookingSource.AirArabia || routingItinerary.FlightBookingSource == BookingSource.UAPI || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{
                                                                string MealDesc = string.Empty;
                                                                %>
                                                         <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (routingItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(routingItinerary.Passenger[j].MealDesc)) %>
                                                            <%{
                                                                    MealDesc = routingItinerary.Passenger[j].MealDesc;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Meals"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (routingItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(routingItinerary.Passenger[j].Seat.Code)) %>
                                                            <%{
                                                                    MealDesc = routingItinerary.Passenger[j].Seat.Code;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Seats"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <%} %>
                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%=(routingItinerary.Segments[count].AirlinePNR == null || routingItinerary.Segments[count].AirlinePNR == "" ? (routingItinerary.PNR.Split('|').Length > 1 ? routingItinerary.PNR.Split('|')[routingItinerary.Segments[count].Group] : routingItinerary.PNR) : routingItinerary.Segments[count].AirlinePNR) %></td>
                                                    </tr>

                                                    <%}
                                                        }%>



            	  		</table>



            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>	  	
            	  </tr></tbody></table>
   <% }%>



            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">			  
            	  	  <table class="flight-list-table" style="border: 1px solid #f3f3f3; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  	    <tr class="first-row" style="padding: 0; text-align: left; vertical-align: top">
            	  	    	<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 4px; text-align: left; vertical-align: top; word-wrap: break-word">

            	  	    		<img class="float-left" src="https://ctb2b.cozmotravel.com/images/AirlineLogo/<%=routingItinerary.Segments[count].Airline%>.gif" alt="<%=routingAirline.AirlineName%>" style="-ms-interpolation-mode: bicubic; clear: both; display: block; float: left; max-width: 100%; outline: none; padding-right: 5px; text-align: left; text-decoration: none; width: auto">


            					<p class="float-left" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; padding-top: 5px; text-align: left"><%routingAirline.Load(routingItinerary.Segments[count].Airline); %> <%=routingAirline.AirlineName%> <strong><%=routingItinerary.Segments[count].Airline + "-" + routingItinerary.Segments[count].FlightNumber%>
                                    
                                    
                                    </strong>
                                    
                                      <%try
                                                                                                {
                                                                                                    if ((!routingItinerary.IsLCC && routingItinerary.Segments[count].OperatingCarrier != null && routingItinerary.Segments[count].OperatingCarrier.Length > 0)||(routingItinerary.FlightBookingSource == BookingSource.Indigo && routingItinerary.Segments[count].OperatingCarrier != null && routingItinerary.Segments[count].OperatingCarrier.Length > 0 && routingItinerary.Segments[count].OperatingCarrier != "6E"))////Modified for Indigo CodeShare
                                                                                                    {
                                                                                                        string opCarrier = routingItinerary.Segments[count].OperatingCarrier;
                                                                                                        Airline opAirline = new Airline();
                                                                                                        if (opCarrier.Split('|').Length > 1)
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Split('|')[0]);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Substring(0, 2));
                                                                                                        } %>
                                             (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                                }
                                                catch { } %></strong>
                                    
                                    </p>
            	  	    	</td>
            	  	    	<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 4px; text-align: left; vertical-align: middle; word-wrap: break-word">
            	  	    		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right"><strong><%=routingItinerary.Segments[count].Origin.CityName%> to <%=routingItinerary.Segments[count].Destination.CityName%></strong> | <strong><%=routingItinerary.Segments[count].ArrivalTime.ToString("ddd")+","+routingItinerary.Segments[count].ArrivalTime.ToString("dd MMM yyyy")%></strong></p>
            	  	    	</td>
            	  	    </tr>
            			<tr style="padding: 0; text-align: left; vertical-align: top">
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word">
            					<strong class="city-code" style="font-size: 18px"><%=routingItinerary.Segments[count].Origin.CityCode%></strong> <%=routingItinerary.Segments[count].Origin.CityName%><br>
            					<strong class="time" style="font-size: 14px"><%=routingItinerary.Segments[count].DepartureTime.ToString("HH:mm tt")%></strong><br>
            					<%=routingItinerary.Segments[count].DepartureTime.ToString("ddd")+","+routingItinerary.Segments[count].DepartureTime.ToString("dd MMM yyyy")%><br>
            					<%=routingItinerary.Segments[count].Origin.AirportName %><br>
            					Terminal <%=routingItinerary.Segments[count].DepTerminal %>
            				</td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: center; vertical-align: middle; word-wrap: break-word">
            				 <center style="min-width: 21px; width: 100%" data-parsed="">
            						<img src="https://ctb2b.cozmotravel.com/images/aircraft_icon.png" alt="Flight Duration" style="-ms-interpolation-mode: bicubic; Margin: 0 auto; clear: both; display: block; float: none; margin: 0 auto; max-width: 100%; min-width: 21px; outline: none; text-align: center; text-decoration: none; vertical-align: middle; width: 21px" align="center" class="float-center">
            						 <%=routingItinerary.Segments[count].Duration.Hours +"hr "+routingItinerary.Segments[count].Duration.Minutes+"m"%>
            				  </center>
            			  </td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; border-right: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word">
            					<strong class="city-code" style="font-size: 18px"><%=routingItinerary.Segments[count].Destination.CityCode%></strong><%=routingItinerary.Segments[count].Destination.CityName%><br> 
            					<strong class="time" style="font-size: 14px"><%=routingItinerary.Segments[count].ArrivalTime.ToString("HH:mm tt")%></strong><br>
            					<%=routingItinerary.Segments[count].ArrivalTime.ToString("ddd")+","+routingItinerary.Segments[count].ArrivalTime.ToString("dd MMM yyyy")%><br>
            					<%=routingItinerary.Segments[count].Destination.AirportName %><br>
            					Terminal <%=routingItinerary.Segments[count].ArrTerminal %> 
            				</td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word">
            					 <%if (routingItinerary.Segments[count].Stops == 0) %>
                    <%{ %>
                    <strong> Non-Stop </strong>
                    <%} %>
                    <%else if (routingItinerary.Segments[count].Stops == 1) %>
                    <%{ %>
                    <strong> Single Stop </strong>
                    <%} %>
                    <%else if (routingItinerary.Segments[count].Stops == 2) %>
                    <%{ %>
                    <strong> Two Stops </strong>
                    <%} %>
                    <%else if (routingItinerary.Segments[count].Stops> 2) %>
                    <%{ %>
                    <strong> Two+ Stops </strong>
                    <%} %><br>
            					Class |  <%=routingItinerary.Segments[count].CabinClass%><br>


                                      <%if (routingItinerary.FlightBookingSource == BookingSource.FlyDubai || routingItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp || routingItinerary.FlightBookingSource == BookingSource.Jazeera || routingItinerary.FlightBookingSource == BookingSource.AirArabia)
                                                                                                {%>
                                                                                     
                                                                                        
            					Fare Type | <%=routingItinerary.Segments[count].SegmentFareType%>
                                    
                                        <%} %>
                                    
                                    <br>
            					<strong><%=routingBooking.Status.ToString()%></strong>
            				</td>
            			</tr>
            		</table></th>

  

<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>		  	
            	  </tr></tbody></table>

             <%}%>  

            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 15px; font-weight: bold; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left; word-wrap: normal"><%--Price Details--%></h3>
            	  	</th>
                         <th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: right">
                                <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: #5f5f5f;" id="ancFareRouting" runat="server"
                                onclick="return ShowHide('divFareRouting');" class="showhideDiv">Hide Fare</a>
                          </th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>





    <div title="Param" id="divFareRouting" runat="server">

            <% decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0, outputVAT = 0, Meal = 0, SeatPrice = 0, k3Tax = 0,mgmtFeeIndia = 0;//Variable which holds the managment fee for India Location;
                                                                                                                                                                         //Case1:If location country code is India then do not add the markup and asv element into the tax components
                                                                                                                                                                         //Case2If location country code is not India then add the markup and asv element into the tax components
                                                                                                                                                                         //New requirement by vinay
                CT.TicketReceipt.BusinessLayer.LocationMaster locationMgmtFee = new CT.TicketReceipt.BusinessLayer.LocationMaster(routingItinerary.LocationId);
                if (Request["bkg"] == null) // For Ticketing
                {
                    int paxId = -1;
                    if (routingTicketList.Count > 0 && Request["paxId"] == null)
                    {
                        List<TaxBreakup> allPaxTaxBreakups = new List<TaxBreakup>();
                        routingTicketList.ForEach(t =>  t.TaxBreakup.ForEach(tb => { TaxBreakup tax = new TaxBreakup(); tax.TaxCode = tb.Key; tax.TaxValue = tb.Value; allPaxTaxBreakups.Add(tax); } ));

                        switch(routingItinerary.FlightBookingSource)
                        {
                            case BookingSource.GoAir:
                            case BookingSource.GoAirCorp:
                                k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("ST") || t.TaxCode.Contains("CT") || t.TaxCode.Contains("IT") ? t.TaxValue : 0);
                                break;
                            case BookingSource.UAPI:
                                k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                break;
                            case BookingSource.SpiceJet:
                            case BookingSource.SpiceJetCorp:
                            case BookingSource.Indigo:
                            case BookingSource.IndigoCorp:
                                k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("GST") ? t.TaxValue : 0);
                                break;
                            default:
                                k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                break;
                        }
                        k3Tax = Convert.ToDecimal(k3Tax.ToString("N" + agency.DecimalValue));
                        if(string.IsNullOrEmpty(routingItinerary.RoutingTripId))
                        {
                            k3Tax = 0;
                        }
                        if (Request["paxId"] != null)
                        {
                            paxId = Convert.ToInt32(Request["paxId"]);
                        }
                        if(paxId >-1 && routingItinerary.Passenger[paxId].Type == PassengerType.Infant && (routingItinerary.FlightBookingSource!=BookingSource.TBOAir && routingItinerary.FlightBookingSource!=BookingSource.UAPI ))
                        {
                            k3Tax = 0;
                        }
                        //if(paxId>=0)
                        //{
                        //    k3Tax = k3Tax / routingItinerary.Passenger.Length;
                        //}
                        for (int k = 0; k < routingTicketList.Count; k++)
                        {
                            AirFare += routingTicketList[k].Price.PublishedFare + routingTicketList[k].Price.HandlingFeeAmount;
                            //Case1:If location country code is India then do not add the markup and asv element into the tax components
                            if (locationMgmtFee.CountryCode == "IN")
                            {
                                mgmtFeeIndia +=  routingTicketList[k].Price.Markup + routingTicketList[k].Price.B2CMarkup;
                                //mgmtFeeIndia += routingTicketList[k].Price.AsvAmount;
                                Taxes += routingTicketList[k].Price.Tax;
                            }
                            else//Case2If location country code is not India then add the markup and asv element into the tax 
                            {
                                Taxes += routingTicketList[k].Price.Tax + routingTicketList[k].Price.Markup+ routingTicketList[k].Price.B2CMarkup;
                            }
                            if (routingItinerary.FlightBookingSource == BookingSource.TBOAir)
                            {
                                Taxes += routingTicketList[k].Price.OtherCharges + routingTicketList[k].Price.SServiceFee + routingTicketList[k].Price.AdditionalTxnFee + routingTicketList[k].Price.TransactionFee;
                            }
                            if (routingItinerary.FlightBookingSource == BookingSource.OffLine)
                            {
                                Taxes -= (routingTicketList[k].Price.AdditionalTxnFee + routingTicketList[k].Price.TransactionFee);
                            }
                            Baggage += routingTicketList[k].Price.BaggageCharge;
                            Meal += routingTicketList[k].Price.MealCharge;
                            SeatPrice += routingTicketList[k].Price.SeatPrice;
                            MarkUp += routingTicketList[k].Price.Markup+ routingTicketList[k].Price.B2CMarkup;
                            Discount += routingTicketList[k].Price.Discount;
                            AsvAmount += routingTicketList[k].Price.AsvAmount;
                            outputVAT += routingTicketList[k].Price.OutputVATAmount;
                        }

                        if (routingTicketList[0].Price.AsvElement == "BF")
                        {
                            AirFare += AsvAmount;
                        }
                        else if (routingTicketList[0].Price.AsvElement == "TF")
                        {
                            Taxes += AsvAmount;
                        }
                    }
                    else
                    {


                        if (Request["paxId"] != null)
                        {
                            paxId = Convert.ToInt32(Request["paxId"]);
                        }
                        if(routingTicketList.Count>0)
                        {

                            List<TaxBreakup> allPaxTaxBreakups = new List<TaxBreakup>();
                            if (paxId == -1)
                                routingTicketList.ForEach(t => t.TaxBreakup.ForEach(tb => { TaxBreakup tax = new TaxBreakup(); tax.TaxCode = tb.Key; tax.TaxValue = tb.Value; allPaxTaxBreakups.Add(tax); }));
                            else
                                routingTicketList[paxId].TaxBreakup.ForEach(tb => { TaxBreakup tax = new TaxBreakup(); tax.TaxCode = tb.Key; tax.TaxValue = tb.Value; allPaxTaxBreakups.Add(tax); } );

                            switch(routingItinerary.FlightBookingSource)
                            {
                                case BookingSource.GoAir:
                                case BookingSource.GoAirCorp:
                                    k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("ST") || t.TaxCode.Contains("CT") || t.TaxCode.Contains("IT") ? t.TaxValue : 0);
                                    break;
                                case BookingSource.UAPI:
                                    k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                    break;
                                case BookingSource.SpiceJet:
                                case BookingSource.SpiceJetCorp:
                                case BookingSource.Indigo:
                                case BookingSource.IndigoCorp:
                                    k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("GST") ? t.TaxValue : 0);
                                    break;
                                default:
                                    k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                    break;
                            }
                            k3Tax = Convert.ToDecimal(k3Tax.ToString("N" + agency.DecimalValue));
                            if(paxId >-1 && routingItinerary.Passenger[paxId].Type == PassengerType.Infant && (routingItinerary.FlightBookingSource!=BookingSource.TBOAir && routingItinerary.FlightBookingSource!=BookingSource.UAPI))
                            {
                                k3Tax = 0;
                            }
                        }

                        if(string.IsNullOrEmpty(routingItinerary.RoutingTripId))
                        {
                            k3Tax = 0;
                        }
                        for (int i = 0; i < routingItinerary.Passenger.Length; i++)
                        {
                            if (Request["paxId"] != null && i == paxId) // Induvidual
                            {
                                FlightPassenger pax = routingItinerary.Passenger[i];
                                AirFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;

                                //Case1:If location country code is India then do not add the markup and asv element into the tax components
                                if (locationMgmtFee.CountryCode == "IN")
                                {
                                    mgmtFeeIndia += pax.Price.Markup + pax.Price.B2CMarkup;
                                    //mgmtFeeIndia += pax.Price.AsvAmount;
                                    Taxes += pax.Price.Tax;
                                }
                                else//Case2If location country code is not India then add the markup and asv element into the tax components
                                {
                                    Taxes += pax.Price.Tax + pax.Price.Markup + pax.Price.B2CMarkup;
                                }
                                if (routingItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                                }
                                if (routingItinerary.FlightBookingSource == BookingSource.OffLine)
                                {
                                    Taxes += (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                                }
                                Baggage += pax.Price.BaggageCharge;
                                Meal += pax.Price.MealCharge;
                                SeatPrice += pax.Price.SeatPrice;
                                MarkUp += pax.Price.Markup + pax.Price.B2CMarkup;
                                Discount += pax.Price.Discount;
                                AsvAmount += pax.Price.AsvAmount;
                                outputVAT += pax.Price.OutputVATAmount;
                                break;
                            }
                            if (Request["paxId"] == null)// For all
                            {
                                FlightPassenger pax = routingItinerary.Passenger[i];
                                AirFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;

                                if (routingItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                                }
                                if (routingItinerary.FlightBookingSource == BookingSource.OffLine)
                                {
                                    Taxes += (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                                }
                                else
                                    Taxes += pax.Price.Tax + pax.Price.Markup + pax.Price.B2CMarkup;

                                Baggage += pax.Price.BaggageCharge;
                                Meal += pax.Price.MealCharge;
                                SeatPrice += pax.Price.SeatPrice;
                                MarkUp += pax.Price.Markup + pax.Price.B2CMarkup;
                                Discount += pax.Price.Discount;
                                AsvAmount += pax.Price.AsvAmount;
                                outputVAT += pax.Price.OutputVATAmount;
                            }
                        }

                        if (routingItinerary.Passenger[0].Price.AsvElement == "BF")
                        {
                            AirFare += AsvAmount;
                        }
                        else if (routingItinerary.Passenger[0].Price.AsvElement == "TF")
                        {
                            Taxes += AsvAmount;
                        }
                    }
                }
                else // For Hold Itinerary View
                {
                    int paxId = 0;

                    if (Request["paxId"] != null)
                    {
                        paxId = Convert.ToInt32(Request["paxId"]);
                    }

                    for (int i = 0; i < routingItinerary.Passenger.Length; i++)
                    {
                        if (Request["paxId"] != null && i == paxId) // Induvidual
                        {
                            FlightPassenger pax = routingItinerary.Passenger[i];
                            AirFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;
                            //Case1:If location country code is India then do not add the markup and asv element into the tax components
                            if (locationMgmtFee.CountryCode == "IN")
                            {
                                mgmtFeeIndia += pax.Price.Markup + pax.Price.B2CMarkup;
                                //mgmtFeeIndia += pax.Price.AsvAmount;
                                Taxes += pax.Price.Tax;
                            }
                            else//Case2If location country code is not India then add the markup and asv element into the tax components
                            {
                                Taxes += pax.Price.Tax + pax.Price.Markup + pax.Price.B2CMarkup;
                            }
                            if (routingItinerary.FlightBookingSource == BookingSource.TBOAir)
                            {
                                Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                            }
                            if (routingItinerary.FlightBookingSource == BookingSource.OffLine)
                            {
                                Taxes += (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                            }
                            Baggage += pax.Price.BaggageCharge;
                            Meal += pax.Price.MealCharge;
                            SeatPrice += pax.Price.SeatPrice;
                            MarkUp += pax.Price.Markup + pax.Price.B2CMarkup;
                            Discount += pax.Price.Discount;
                            AsvAmount += pax.Price.AsvAmount;
                            outputVAT += pax.Price.OutputVATAmount;
                            break;
                        }
                        if (Request["paxId"] == null)// For all
                        {
                            FlightPassenger pax = routingItinerary.Passenger[i];
                            AirFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;

                            if (routingItinerary.FlightBookingSource == BookingSource.TBOAir)
                            {
                                Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                            }
                            if (routingItinerary.FlightBookingSource == BookingSource.OffLine)
                            {
                                Taxes += (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                            }
                            else
                            {
                                //Case1:If location country code is India then do not add the markup and asv element into the tax components
                                if (locationMgmtFee.CountryCode == "IN")
                                {
                                    mgmtFeeIndia += pax.Price.Markup + pax.Price.B2CMarkup;
                                    //mgmtFeeIndia += pax.Price.AsvAmount;
                                    Taxes += pax.Price.Tax;
                                }
                                else//Case2If location country code is not India then add the markup and asv element into the tax components
                                {
                                    Taxes += pax.Price.Tax + pax.Price.Markup + pax.Price.B2CMarkup;
                                }
                            }
                            Baggage += pax.Price.BaggageCharge;
                            Meal += pax.Price.MealCharge;
                            SeatPrice += pax.Price.SeatPrice;
                            MarkUp += pax.Price.Markup + pax.Price.B2CMarkup;
                            Discount += pax.Price.Discount;
                            AsvAmount += pax.Price.AsvAmount;
                            outputVAT += pax.Price.OutputVATAmount;
                        }
                    }

                    if (routingItinerary.Passenger[0].Price.AsvElement == "BF")
                    {
                        AirFare += AsvAmount;
                    }
                    else if (routingItinerary.Passenger[0].Price.AsvElement == "TF")
                    {
                        Taxes += AsvAmount;
                    }
                }
                                            %>
                                            <% //if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.BaseAgent)
                                               // { %>
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<table class="pricing-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Air Fare</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word"> <%=AirFare.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>


            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right"> Taxes & Fees</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=(Taxes-k3Tax).ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
                              <%CT.TicketReceipt.BusinessLayer.LocationMaster locationMgmtFeeIn = new CT.TicketReceipt.BusinessLayer.LocationMaster(routingItinerary.LocationId);
                                                                                             if (locationMgmtFeeIn.CountryCode == "IN")
                                                                                            {%>
                              <tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">
                                                                 
                                                                                        Management Fee
                                                                                        
                                                                                           

                                      </th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=mgmtFeeIndia.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
                                  <%} %>
                               <%if (!string.IsNullOrEmpty(routingItinerary.RoutingTripId))
                                                                                    { %>   
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right"> K3 Tax</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">    <%=(k3Tax).ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
   <%} %>


         <%if (routingItinerary.IsLCC)
                                                                                    { %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Baggage Fare </th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">   <%=Baggage.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>

                    <%} %>


                                                                                <%if (routingItinerary != null && (routingItinerary.FlightBookingSource == BookingSource.UAPI || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp|| routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp || routingItinerary.FlightBookingSource == BookingSource.AirArabia)) %>
                                                                                <%{ %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Meal Price</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=Meal.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Seat Price</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=SeatPrice.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>

    <%} %>

        
                                                                                <%if (Discount > 0)
                                                                                    { %>
        <tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Discount</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  -<%=Discount.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
    <%} %>

            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">

                                       <%CT.TicketReceipt.BusinessLayer.LocationMaster location = new CT.TicketReceipt.BusinessLayer.LocationMaster(routingItinerary.LocationId);
                                                                                             if (location.CountryCode == "IN")
                                                                                            {%>
                                                                                        GST
                                                                                        <%}    else
    { %>
                                                                                        VAT
                                                                                        <%} %>
            	  				</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">   <%=outputVAT.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">
                                  <th valign="middle" width="220px" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">
            				 	     <strong style="font-size: 11px"> This is an Electronic ticket. Please carry a positive identification for Check in.</strong>
            				     </th>		  				
            	  				<th width="220px" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right;vertical-align:middle"><strong>Total Air Fare</strong></th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word;vertical-align:middle"><strong>


                                       <%if (routingItinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                                                                                            { %>
                                                                                        <%=Math.Ceiling((AirFare + Taxes + Baggage + outputVAT + mgmtFeeIndia) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%>
                                                                                        <%}
                                                                                         else if (routingItinerary.FlightBookingSource == BookingSource.OffLine) 
                                                                                            { %>
                                                                                        <%=((AirFare + Taxes + outputVAT + mgmtFeeIndia) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%>
                                                                                        <%}

                                                                                            else
                                                                                            { %>
                                                                                        <%=((AirFare + Taxes + Baggage + Meal + SeatPrice + outputVAT + mgmtFeeIndia) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%>
                                                                                        <%} %>
            	  				                                                                                                                                                                                                                                                                                                                                                                   </strong></td>
            	  			</tr>
            	  		</table>
            	  	</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>

</div>


            	    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
             	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">For international flights, you are suggested to arrive at the airport at least 3-4 hours prior to your departure time to check in and go through security. For connecting flights, please confirm at the counter whether not you can check your luggage through to the final destination when you check in.</p>
            			 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">Don’t forget to purchase travel insurance for your visit.Please contact us to purchase travel insurance.</p>

                       <%if (routingItinerary != null && routingItinerary.Segments.ToList().All(x => x.Group == 0 && x.Destination.CountryCode == x.Origin.CountryCode) && (routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp))
                 { %>
             <%if (routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp)
    { %>
             <div><strong style="color:red">Important Update: Please read carefully.</strong><br/>
            Due to ongoing COVID-19 situation, for Safe and Seamless journey and as per the implementation of various safety procedure<br/>
            For Safe and Seamless travels of our guests. It is mandatory to do <span style="text-decoration:underline;font-weight:bold"><span style="color:red">“Web Check-in” along with carrying Boarding pass, affixing of baggage label</span> and following all government guidelines while travelling.</span>
            <ul style="font-weight:bold;">
            <li>Web check-in is now mandatory for each passenger for all Domestic flights.</li>
            <li>Mandatory Check-in: Web check-in to be done upto <span style="text-decoration:underline">75 minutes</span> prior from the schedule flight departure of their domestic flight.<br/>
            How to Check In :  Click on the following link which will take you to the Airline Website <a href="https://www.goindigo.in/how-to-check-in.html?linkNav=how-to-check-in_header" target="_blank">https://www.goindigo.in/how-to-check-in.html?linkNav=how-to-check-in_header</a>. Once clicking on the link, it will be redirected to Indigo Airline website Check-in page. Passengers have to mention PNR and Email/Last name in the specified field to proceed with the Check-in process.</li> 
            <li>Passenger Health declaration status and their mobile should have <span style="text-decoration:underline">“Arogya Setu”</span> app which shows Green status.  Mandatory to add <span style="text-decoration:underline">SSR “HCOK”</span> while Web Checkin.</li>
            <%if (agency.AgentCurrency != "INR")
    { //Hide only for India Agents%>
            <li>Passengers travelling with Check in luggage, need to add their luggage during web check-in. Per Passenger 1 piece of Checked in Baggage upto 20 kgs will be allowed. Addition to this one small hand luggage max of 7 Kgs per passenger will be allowed.</li>   
                <%} %>
            <li>Carriage of printout of Boarding card and Baggage label which will be sent to the guest on their contact details or can download during web checkin.</li> 
            <li>Also can carry Mobile boarding pass and carry printed or written baggage label, format will be available on Indigo website.</li> 
            <li>Before reaching to the airport, Passenger need to paste the attached Baggage label on the baggage OR guest can write the mandatory fields i.e. PNR, Name of the passenger, Sector and Flight Number on plain paper & paste the same on baggage which should be prominently visible, as airport may not be having any pasting material to place the labels on the baggage.</li>
            <li>Passenger must be wearing mask properly at the airport & throughout the journey.</li> 
            <li>Passenger must sanitize at airports at various points.</li> 
            <li>Maintain Social distancing at the airport as per the markings/signage.</li> 
            <li>For Safety of all customers, Indigo reserves the rights to refuse the carriage of any passenger whose contact details are missing. Also, its mandatory to do the Web Check-in.</li>
            </ul>
            </div>
            <%}
    if (routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
    { %>
                <div><strong style="color:red">Important Update: Please read carefully.</strong><br/>
                Due to ongoing COVID-19 situation, for Safe and Seamless journey and as per the implementation of various safety procedure<br/>
                For Safe and Seamless travels of our guests. It is mandatory to do <span style="text-decoration:underline;font-weight:bold"><span style="color:red">“Web Check-in” along with carrying Boarding pass, affixing of baggage label</span> and following all government guidelines while travelling.</span>
                <ul style="font-weight:bold;">
                <li>Web check-in is now mandatory for each passenger for all Domestic flights.</li>
                <li>Mandatory Check-in: Web check-in to be done upto <span style="text-decoration:underline">75 minutes</span> prior from the schedule flight departure of their domestic flight.<br/>
                How to Check In :  Click on the following link which will take you to the Airline Website <a href="https://book.spicejet.com/Search.aspx?op=4" target="_blank">https://book.spicejet.com/Search.aspx?op=4 </a>. Once clicking on the link, it will be redirected to Spice Jet Airline website Check-in page. Passengers have to mention PNR and Email/Last name in the specified field to proceed with the Check-in process.</li> 
                <li>Passenger Health declaration status and their mobile should have <span style="text-decoration:underline">“Arogya Setu”</span> app which shows Green status.  Mandatory to add <span style="text-decoration:underline">SSR “HCOK”</span> while Web Checkin.</li>
                <%if (agency.AgentCurrency != "INR")
    { //Hide only for India Agents%>
            <li>Passengers travelling with Check in luggage, need to add their luggage during web check-in. Per Passenger 1 piece of Checked in Baggage upto 20 kgs will be allowed. Addition to this one small hand luggage max of 7 Kgs per passenger will be allowed.</li>   
                <%} %>
                <li>Carriage of printout of Boarding card and Baggage label which will be sent to the guest on their contact details or can download during web checkin.</li> 
                <li>Also can carry Mobile boarding pass and carry printed or written baggage label, format will be available on Spice Jet website.</li> 
                <li>Before reaching to the airport, Passenger need to paste the attached Baggage label on the baggage OR guest can write the mandatory fields i.e. PNR, Name of the passenger, Sector and Flight Number on plain paper & paste the same on baggage which should be prominently visible, as airport may not be having any pasting material to place the labels on the baggage.</li>
                <li>Passenger must be wearing mask properly at the airport & throughout the journey.</li> 
                <li>Passenger must sanitize at airports at various points.</li> 
                <li>Maintain Social distancing at the airport as per the markings/signage.</li> 
                <li>For Safety of all customers, Spice Jet reserves the rights to refuse the carriage of any passenger whose contact details are missing. Also, its mandatory to do the Web Check-in.</li>
                </ul>
                </div>
             <%}
    } %>
            
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>
            	
            
             
            
            
            
            	</td></tr></tbody></table>
            
            </td></tr></table>
            
          </center>
        </td>
      </tr>
    </table>
    <!-- prevent Gmail on iOS font size manipulation -->
   <div style="display:none; white-space:nowrap; font:15px courier; line-height:0"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
  </div>














      


            







                            <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.Agent)
                                { %>
                            <script type="text/javascript">ShowHide(&quot;divFare&quot;);</script> 
                            <%} %>
                            <table>
                                <tbody>
                                 
                                    <tr style="display: none;">
                                        <td><strong>Important Information </strong></td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td height="10">
                                            <hr />
                                        </td>
                                    </tr>

                                    <tr style="display: none;">
                                        <td>
                                            <ul>
                                                <li>Use your Trip ID for all communication with Cleartrip about this booking </li>
                                                <li>Check-in counters for International flights close 75 minutes before departure</li>
                                                <li>Your carry-on baggage shouldn't weigh more than 7kgs</li>
                                                <li>Carry photo identification, you will need it as proof of identity while checking-in</li>
                                                <li>Kindly ensure that you have the relevant visa, immigration clearance and travel with a passport, with a validity of at least 6 months.</li>
                                                <li>For hassle free refund processing, cancel/amend your tickets with Cleartrip Customer Care instead of doing so directly with Airline.</li>
                                            </ul>
                                        </td>
                                    </tr>
                                
                                    <tr style="display: none;">
                                        <td>
                                            <img src="https://ctb2b.cozmotravel.com/images/goapic.jpg" width="728" height="90" alt="#" /></td>
                                    </tr>
                                </tbody>
                            </table>

                                    <!-- Added by lokesh on 7-June-2018-->
        <!-- Need to display the below note in email for PK Fares -->
         <%if (routingItinerary != null && routingItinerary.FlightBookingSource == BookingSource.PKFares) %>
         <%{ %>
         <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                <span style="color:red">Note: This is just an itinerary. Ticket copy should not be handed over to customer unless ticket number is shown on the ticket.</span>
                    
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <%} %>



                        </div>
                        <%} %>

                        <%if (flightItinerary.FlightBookingSource == BookingSource.FlightInventory) %>
                        <% { %>
                            
                        <%  GetConfigData("FI_Email_Alert"); %> 
                        <div style='color:red'>Note: <asp:Label ID="lblConfigData" runat="server"  ></asp:Label>
                        </div>
      
                        <%} %> 

                            
                    </div>                      
                    
                </asp:View>
            

  
                 <asp:View ID="ErrorView" runat="server">
                
                                      <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	
            			 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            			  <p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">  <asp:Label ID="ErrorMessage" runat="server"></asp:Label></p>

                       
            		</th>
</tr></table></th>
            	  </tr></tbody></table>  
                       
                </asp:View>
            </asp:MultiView>
            
            
       </center>
    </form>
</body>
</html>
