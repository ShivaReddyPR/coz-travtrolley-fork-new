﻿using System;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
public partial class ActivityDetailsGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected Activity activity = null;
    protected string activityImgFolder;
    protected void Page_Load(object sender, EventArgs e)
    {

        activityImgFolder = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"].ToString();
        if (!IsPostBack)
        {
            if (Session["Activities"] != null)
                Session.Remove("Activities");
            int id = 0;
            if (Request["id"] != null && Request["id"] != string.Empty)
            {
                id = Convert.ToInt32(Request["id"]);
            }
            LoadData(id);
        }
    }
    public void LoadData(int id)
    {
        try
        {
            Activity.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
            Activity.AgentId = Settings.LoginInfo.AgentId;
            activity = new Activity(id);
            dlRoomRates.DataSource = activity.PriceDetails;

            dlRoomRates.DataBind();
            Session["Activity"] = activity;

            decimal Total = Convert.ToDecimal(activity.StartingFrom);

            lblAmount.Text = Total.ToString("N" + Settings.LoginInfo.DecimalValue);
            lblActivity.Text = activity.Name;
            imgActivity.ImageUrl = activityImgFolder + activity.ImagePath2;
        }
        catch (Exception ex)
        {
            //Audit.Add(EventType.PakageQueries, Severity.High, 1, ex.Message, "0");
            throw ex;
        }
    }
    //protected void lbtnBook_Click(object sender, EventArgs e)
    //{
    //    if (Session["agencyId"] == null)
    //    {
    //        Response.Redirect("ActivityRegistration.aspx", false);
    //    }
    //    else
    //    {
    //        Response.Redirect("ActivityAvailability.aspx", false);
    //    }
    //}
    protected string CTCurrencyFormat(object currency)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
    }
}
