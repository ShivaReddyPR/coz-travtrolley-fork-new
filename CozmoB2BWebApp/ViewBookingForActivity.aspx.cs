﻿using System;
using System.Data;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
public partial class ViewBookingForActivity :CT.Core.ParentPage// System.Web.UI.Page
{
    protected DataTable ActHdr;
    protected long id;
    protected AgentMaster agency;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["bookingId"] != null && Request.QueryString["bookingId"] != string.Empty)
                {
                    id = Convert.ToInt64(Request.QueryString["bookingId"]);
                    Load(id);
                    hdnBookingId.Value = id.ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void Load(long id)
    {
        try
        {
            DataSet ds = Activity.GetActivityQueueDetails(id);
            ActHdr = ds.Tables[0];
            agency = new AgentMaster(Convert.ToInt32(ActHdr.Rows[0]["AgencyId"]));
            dlPaxDetails.DataSource = ds.Tables[1];
            dlPaxDetails.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void lnkActShowVoucher_Click(object sender, EventArgs e)
    {
        Response.Redirect("ActivityVoucher.aspx?ID=" + hdnBookingId.Value, false);
    }
    protected void lnkFixShowVoucher_Click(object sender, EventArgs e)
    {
        Response.Redirect("FixedDepartureVoucher.aspx?ID=" + hdnBookingId.Value, false);
    }

}
