﻿using CCA.Util;
using CT.Configuration;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace CozmoB2BWebApp
{
    public partial class ViewSwitcher : System.Web.UI.Page
    {
        protected string url = string.Empty, bookingType = "N";
        Dictionary<string, object> SessionValues = new Dictionary<string, object>();

        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {
                if (Request["uid"] != null)
                {
                    UserMaster user = new UserMaster(Convert.ToInt64(Request["uid"]));
                    UserMaster.silentLogin(user.Email);//Load LoginInfo
                    if (Request["obaid"] != null)
                    {
                        Settings.LoginInfo.IsOnBehalfOfAgent = true;
                        Settings.LoginInfo.OnBehalfAgentID = Convert.ToInt32(Request["obaid"]);
                        Settings.LoginInfo.OnBehalfAgentSourceCredentials = AgentMaster.GetAirlineCredentials(Settings.LoginInfo.OnBehalfAgentID);
                    }
                    Session["themeName"] = this.Page.Theme = Settings.LoginInfo.AgentTheme;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to restore Session for Flight PG response: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //Reload sessions we serialized in PaymentProcessing page for booking
            if (((Request.Params["txn_response"] != null || Request["razorpay_payment_id"] != null) && Request["sid"] != null) || Request.Form["encResp"] != null)
            {
                LoadSessionForBooking();
            }
            
            if ((bookingType == "N" && Session["FlightItinerary"] == null && Session["FlightRequest"] == null) || (bookingType == "C" && !SessionValues.ContainsKey("OnwardFlightItinerary")))
            {
                Response.Redirect("ErrorPage.aspx?error_message=PaymentFailed");
            }          
            
        }

        protected void LoadSessionForBooking()
        {
            try
            {
                //if (Request["razorpay_payment_id"] != null && Request["razorpay_order_id"] != null && Request["razorpay_signature"] != null)//Handle signature verification for RazorPay
                //{
                //    string rzpOrderId = string.Empty, rzpPaymentId = string.Empty, rzpSignature = string.Empty;
                //    rzpOrderId = Request["razorpay_order_id"];
                //    rzpPaymentId = Request["razorpay_payment_id"];
                //    rzpSignature = Request["razorpay_signature"];

                //    var validSignature = getHashSha256(rzpOrderId + "|" + rzpPaymentId, ConfigurationSystem.RazorPayConfig["Secret"]);

                //    if (validSignature != rzpSignature)
                //    {
                //        Audit.Add(EventType.CreditCardPaymentRequest, Severity.High, 1, "RazorPay Payment GateWay Hacked(Failure) Data : " + Request.Form.ToString(), Request["REMOTE_ADDR"]);
                //        Response.Redirect("ErrorPage.aspx?error_message=PaymentFailed");
                //    }
                //    else
                //    {
                //        Audit.Add(EventType.CreditCardPaymentRequest, Severity.High, 1, "RazorPay Payment GateWay Success Data : " + Request.Form.ToString(), Request["REMOTE_ADDR"]);
                //    }
                //}

                
                
                //CCAvenue PG response
                if (Request.Form["encResp"] != null)
                {
                    string workingKey = ConfigurationManager.AppSettings["CCA_Encrypt_Key"];
                    CCACrypto ccaCrypto = new CCACrypto();
                    string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"].Replace(" ", "+"), workingKey);
                    Audit.Add(EventType.Book, Severity.Low, 1, "(Flight)CCAvenue response data : " + encResponse, Request["REMOTE_ADDR"]);
                    NameValueCollection Params = new NameValueCollection();
                    string[] segments = encResponse.Split('&');
                    foreach (string seg in segments)
                    {
                        string[] parts = seg.Split('=');
                        if (parts.Length > 0)
                        {
                            string Key = parts[0].Trim();
                            string Value = parts[1].Trim();
                            Params.Add(Key, Value);
                        }
                    }
                    
                    UserMaster user = new UserMaster(Convert.ToInt64(Params["merchant_param1"]));
                    UserMaster.IsAuthenticatedUser(user.LoginName, UserMaster.GetPasswordByLoginName(user.LoginName), 1);//Load LoginInfo
                    SessionValues = GenericStatic.Load(Params["merchant_param2"],CT.BookingEngine.ResultCacheType.FlightQueryParams.ToString()) as Dictionary<string, object>;//Read SessionValues from file
                    Session["sessionId"] = Params["merchant_param2"];
                    bookingType = Params["merchant_param3"];                    
                }
                else//other PG response
                {
                    bookingType = Request["type"];
                    UserMaster user = new UserMaster(Convert.ToInt64(Request["uid"]));
                    UserMaster.IsAuthenticatedUser(user.LoginName, UserMaster.GetPasswordByLoginName(user.LoginName),1);//Load LoginInfo
                    SessionValues = GenericStatic.Load(Request["sid"], CT.BookingEngine.ResultCacheType.FlightQueryParams.ToString()) as Dictionary<string, object>;//Read SessionValues from file
                    Session["sessionId"] = Request["sid"];
                }

                Session["PageParams"] = SessionValues["PageParams"];
                GenericStatic.GetSetPageParams("CCParams", Request.Form.ToString(), "set");
                //Reassign session values
                if (bookingType=="C")
                {
                    Session["PaxPageValues"] = SessionValues;
                }
                else
                {
                    if (SessionValues.ContainsKey("ResultIndex") && SessionValues["ResultIndex"] != null)
                    {
                        Session["ResultIndex"] = SessionValues["ResultIndex"];
                    }

                    if (SessionValues.ContainsKey("FlightItinerary") && SessionValues["FlightItinerary"] != null)
                    {
                        Session["FlightItinerary"] = SessionValues["FlightItinerary"];
                    }

                    if (SessionValues.ContainsKey("FlightRequest") && SessionValues["FlightRequest"] != null)
                    {
                        Session["FlightRequest"] = SessionValues["FlightRequest"];
                    }

                    if (SessionValues.ContainsKey("UAPIURImpresp") && SessionValues["UAPIURImpresp"] != null)
                    {
                        Session["UAPIURImpresp"] = SessionValues["UAPIURImpresp"];
                    }

                    if (SessionValues.ContainsKey("UAPIReprice") && SessionValues["UAPIReprice"] != null)
                    {
                        Session["UAPIReprice"] = SessionValues["UAPIReprice"];
                    }

                    if (SessionValues.ContainsKey("BookingResponse") && SessionValues["BookingResponse"] != null)
                    {
                        Session["BookingResponse"] = SessionValues["BookingResponse"];
                    }

                    if (SessionValues.ContainsKey("CriteriaItineraries") && SessionValues["CriteriaItineraries"] != null)
                    {
                        Session["CriteriaItineraries"] = SessionValues["CriteriaItineraries"];
                    }
                }

                if (SessionValues.ContainsKey("OBAgentId") && SessionValues["OBAgentId"] != null)
                {
                    Settings.LoginInfo.OnBehalfAgentID = (int)SessionValues["OBAgentId"];
                    Settings.LoginInfo.IsOnBehalfOfAgent = true;
                }

                if (SessionValues.ContainsKey("OBCredentials") && SessionValues["OBCredentials"] != null)
                {
                    Settings.LoginInfo.OnBehalfAgentSourceCredentials = SessionValues["OBCredentials"] as Dictionary<string, SourceDetails>;
                }

                if (SessionValues.ContainsKey("OBExRates") && SessionValues["OBExRates"] != null)
                {
                    Settings.LoginInfo.OnBehalfAgentExchangeRates = SessionValues["OBExRates"] as Dictionary<string, decimal>;
                    
                }

                if (SessionValues.ContainsKey("OBLocation") && SessionValues["OBLocation"] != null)
                {
                    Settings.LoginInfo.OnBehalfAgentLocation = (int)SessionValues["OBLocation"];
                }

                if (SessionValues.ContainsKey("OBCurrency") && SessionValues["OBCurrency"] != null)
                {
                    Settings.LoginInfo.OnBehalfAgentCurrency = SessionValues["OBCurrency"].ToString();
                }

                if (SessionValues.ContainsKey("OBDecimalValue") && SessionValues["OBDecimalValue"] != null)
                {
                    Settings.LoginInfo.OnBehalfAgentDecimalValue = (int)SessionValues["OBDecimalValue"];
                }

                if (SessionValues.ContainsKey("PaymentInformationId") && SessionValues["PaymentInformationId"] != null)
                {
                    Session["PaymentInformationId"] = SessionValues["PaymentInformationId"];
                }

                url = bookingType == "N" ? "PaymentConfirmation.aspx" : "PaymentConfirmationBySegments.aspx";

            }
            catch (Exception ex)
            {
                string PG = string.Empty;
                if (Request.Form["encResp"] != null)
                {
                    PG = "CCAvenue Payment ";
                }
                else if (Request.Params["txn_response"] != null)
                {
                    PG = "SafexPay Payment ";
                }
                else if (Request["razorpay_payment_id"] != null)
                {
                    PG = "RazorPay Payment ";
                }

                Audit.Add(EventType.Book, Severity.High, 1, "Failed to reload session for " + PG + " booking : " + ex.ToString(), Request["REMOTE_ADDR"]);
            }

        }

        /// <summary>
        /// Razorpay signature validation
        /// </summary>
        /// <param name="signatureString"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        //private string getHashSha256(string signatureString, string secret)
        //{
        //    //string getHashSha256 = string.Empty;
        //    //byte[] bytesSignature = Encoding.UTF8.GetBytes(signatureString);
        //    //byte[] bytesSecret = Encoding.UTF8.GetBytes(secret);
        //    //HMACSHA256 mACSHA256 = new HMACSHA256(bytesSecret);
        //    //byte[] hash = mACSHA256.ComputeHash(bytesSignature);
        //    //foreach (byte x in hash)
        //    //{
        //    //    getHashSha256 += String.Format("{0:x2}", x);
        //    //}
        //    //return getHashSha256;
        //}
    }


}