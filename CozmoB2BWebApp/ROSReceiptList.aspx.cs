using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

public partial class ROSReceiptListUI : CT.Core.ParentPage
{
    public DataTable dtExport;
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            lblSuccessMsg.Text = string.Empty;
            hdfParam.Value = "1";
            StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnExport );
            // lblError.Visible = false;
            if (!IsPostBack)
            {
                //Label lblGrandText = (Label)this.Master.FindControl("lblGrandText");
                //Label lblGrandTotal = (Label)this.Master.FindControl("lblGrandTotal");
                //lblGrandText.Visible = true;
                //lblGrandTotal.Visible = true;
                //((UpdatePanel)this.Master.FindControl("upnlGrandTotal")).Update();
               
                //((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
               // BindGrid();
                InitializePageControls();
            }
        }
        catch (Exception ex)
        {
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
            //lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
       
   }
    private void InitializePageControls()
    {
        try
        {
            BindSettlementMode();
           //BindLocation();
            dcFromDate.Value = Utility.ToDate(DateTime.Now.Date);
            dcToDate.Value = Utility.ToDate(DateTime.Now.Date.ToString("MMM-dd-yyyy 23:59"));
            //DateControl dtControl=((DateControl)gvReceipt.HeaderRow.FindControl("HTtxtReceiptDate"));
            //dtControl.Value=DateTime.Now;
            //BindAgentList();
            LoginInfo logininfo = Settings.LoginInfo;
            BindAgentList();
            BindLocation(Utility.ToInteger(logininfo.AgentId));
            BindGrid();

           // string date = ((TextBox)(dtControl.FindControl("Date"))).Text;


        }
        catch
        { throw; }

    }
    #endregion


    private void BindSettlementMode()
    {
        
        try
        {
            ddlSettlementMode.DataSource = UserMaster.GetMemberTypeList("settlement_mode");
            ddlSettlementMode.DataValueField = "FIELD_VALUE";
            ddlSettlementMode.DataTextField = "FIELD_TEXT";
            ddlSettlementMode.DataBind();
            ddlSettlementMode.Items.Insert(0, new ListItem("--All--", "-1"));
        }
        catch { throw; }
    }
    private void BindLocation(int agentId)
    {
        try
        {
            ddlLocation.DataSource = LocationMaster.GetList(agentId,ListStatus.Short, RecordStatus.Activated,string.Empty);//todo25052016
            ddlLocation.DataValueField = "LOCATION_ID";
            ddlLocation.DataTextField = "LOCATION_NAME";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("--Select Location--", "-1"));

            //if (Settings.LoginInfo.MemberType == MemberType.SUPER || (Settings.LoginInfo.AgentId == 1 && (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType ==MemberType.BACKOFFICE)))
            //{
            //    ddlLocation.SelectedIndex = 0;
            //    ddlLocation.Enabled = true;
            //}
            //else
            //{
            //    ddlLocation.SelectedValue = Utility.ToString(Settings.LoginInfo.LocationID);
            //    ddlLocation.Enabled = false;
            //}
        }
        catch { throw; }
    }
    private void BindAgentList()
    {
        try
        {
            ddlAgent.DataSource = AgentMaster.GetList(1, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);// 1/*Agent and Base Agent*/);
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("-- All --", "0"));
            if (Utility.ToInteger(Settings.LoginInfo.AgentId) > 1) ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);

        }
        catch { throw; }
    }

    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);            
            BindLocation(agentId);
            hdfParam.Value = "0";
        }
        catch (Exception ex)
        { }
    }
    //private void BindAgentList()
    //{
    //    try
    //    {
    //        ddlAgent.DataSource = AgentMaster.GetList(1, Settings.LoginInfo.AgentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated, Utility.ToInteger((char)Settings.LoginInfo.AgentTypeId));
    //        ddlAgent.DataValueField = "agent_id";
    //        ddlAgent.DataTextField = "agent_name";
    //        ddlAgent.DataBind();
    //        ddlAgent.Items.Insert(0, new ListItem("-- All --", "-1"));
    //        //  ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);

    //        if (Settings.LoginInfo.MemberType == MemberType.SUPER || (Settings.LoginInfo.AgentId == 1 && Settings.LoginInfo.MemberType == MemberType.ADMIN))
    //        {
    //            ddlAgent.SelectedIndex = 0;
    //            ddlAgent.Enabled = true;
    //        }
    //        else
    //        {
    //            ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
    //            ddlAgent.Enabled = false;
    //        }


    //    }
    //    catch { throw; }
    //}



 

    
    


    
    # region Session
   
    protected void ITlnkPrintHala_Click(object sender, EventArgs e)
    {
        try
        {

            GridViewRow gvRow = (GridViewRow)(((LinkButton)sender).NamingContainer);
            HiddenField hdfReceiptId = (HiddenField)gvRow.FindControl("IThdfReceiptId");
            if (hdfReceiptId != null)
            {
                long receiptId = Utility.ToLong(hdfReceiptId.Value);
                //string script = "window.open('printReport.aspx?receiptId=" + Utility.ToString(reportId) + "','','width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=yes,scrollbars=yes,resizable=yes,left=50,top=50');";
                //StartupScript(this.Page, script, "Key");
                string reportType = "TICKET";
                string scripts = "window.open('PrintAPTDepositReport.aspx?vsId=" + Utility.ToString(receiptId) + "&reportType=" + reportType + "','','width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
                Utility.StartupScript(this.Page, scripts, "PrintAPTDepositReport");
            }

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);

        }
    }  

   # endregion
    #region Bind Grid
    private void BindGrid()
    {

        ///*
        //BaseAgent/Agent             B2B                     B2B2B 
        //-----------------------------------------------------------------------------------------------------------------------------------
        // All(0)                     ALL(0)                  All(0)                      LoginagentId = 0                   AgentTypeId= 0
        // All(0)                     Select B2b Agent(-1)    Select B2B2B Agent(-1)      LoginagentId = 0                   AgentTypeId= 2
        // All(0)                     ALL(0)                  Select B2B2B Agent(-1)      LoginagentId = 0                   AgentTypeId= 3
        // BaseAgent/Agent (+1)       Select B2b Agent(-1)    Select B2B2B Agent(-1)      LoginagentId = +1(AgentId)         AgentTypeId= 2
        // BaseAgent/Agent (+1)       ALL(0)                  ALL(0)                      LoginagentId = +1(AgentId)         AgentTypeId= 0
        // BaseAgent/Agent (+1)       B2BAgent(+1)            Select B2B2B Agent(-1)      LoginagentId = +1(B2BAgent ID)     AgentTypeId= 2
        // BaseAgent/Agent (+1)       B2BAgent(+1)            All(0)                      LoginagentId = +1(B2BAgent ID)     AgentTypeId= 3
        // BaseAgent/Agent (+1)       B2BAgent(+1)            B2B2BAgent(+1)              LoginagentId = +1(B2B2B Agent ID)  AgentTypeId= 4         
         
        //*/


        //int agentTypeId = 0;
        //int loginAgentId = 0;
        //if (Utility.ToInteger(ddlAgent.SelectedItem.Value) == 0 && Utility.ToInteger(ddlB2BAgent.SelectedItem.Value) == 0 && Utility.ToInteger(ddlB2B2BAgent.SelectedItem.Value) == 0)
        //{
        //    //0,0,0
        //    loginAgentId = 0;
        //    agentTypeId = 0;
        //}
        //else if (Utility.ToInteger(ddlAgent.SelectedItem.Value) == 0 && Utility.ToInteger(ddlB2BAgent.SelectedItem.Value) < 0 && Utility.ToInteger(ddlB2B2BAgent.SelectedItem.Value) < 0)
        //{
        //    //0,-1,-1
        //    loginAgentId = 0;
        //    agentTypeId = 2;
        //}
        //else if (Utility.ToInteger(ddlAgent.SelectedItem.Value) == 0 && Utility.ToInteger(ddlB2BAgent.SelectedItem.Value) == 0 && Utility.ToInteger(ddlB2B2BAgent.SelectedItem.Value) < 0)
        //{
        //    //0,0,-1
        //    loginAgentId = 0;
        //    agentTypeId = 3;
        //}
        //else if (Utility.ToInteger(ddlAgent.SelectedItem.Value) > 0 && Utility.ToInteger(ddlB2BAgent.SelectedItem.Value) < 0 && Utility.ToInteger(ddlB2B2BAgent.SelectedItem.Value) < 0)
        //{
        //    //+1,-1,-1
        //    loginAgentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
        //    agentTypeId = 2;
        //}
        //else if (Utility.ToInteger(ddlAgent.SelectedItem.Value) > 0 && Utility.ToInteger(ddlB2BAgent.SelectedItem.Value) == 0 && Utility.ToInteger(ddlB2B2BAgent.SelectedItem.Value) == 0)
        //{
        //    //+1,0,0
        //    loginAgentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
        //    agentTypeId = 3;
        //}
        //else if (Utility.ToInteger(ddlAgent.SelectedItem.Value) > 0 && Utility.ToInteger(ddlB2BAgent.SelectedItem.Value) > 0 && Utility.ToInteger(ddlB2B2BAgent.SelectedItem.Value) < 0)
        //{
        //    //+1,+1,-1
        //    loginAgentId = Utility.ToInteger(ddlB2BAgent.SelectedItem.Value);
        //    agentTypeId = 0;

        //}
        //else if (Utility.ToInteger(ddlAgent.SelectedItem.Value) > 0 && Utility.ToInteger(ddlB2BAgent.SelectedItem.Value) > 0 && Utility.ToInteger(ddlB2B2BAgent.SelectedItem.Value) == 0)
        //{
        //    //+1,+1,0
        //    loginAgentId = Utility.ToInteger(ddlB2BAgent.SelectedItem.Value);
        //    agentTypeId = 3;

        //}

        //else if (Utility.ToInteger(ddlAgent.SelectedItem.Value) == 0 && Utility.ToInteger(ddlB2BAgent.SelectedItem.Value) > 0 && Utility.ToInteger(ddlB2B2BAgent.SelectedItem.Value) > 0)
        //{
        //    //0,+1,+1
        //    loginAgentId = Utility.ToInteger(ddlB2B2BAgent.SelectedItem.Value);
        //    agentTypeId = 0;

        //}
        //else if (Utility.ToInteger(ddlAgent.SelectedItem.Value) > 0 && Utility.ToInteger(ddlB2BAgent.SelectedItem.Value) > 0 && Utility.ToInteger(ddlB2B2BAgent.SelectedItem.Value) > 0)
        //{
        //    //+1,+1,+1
        //    loginAgentId = Utility.ToInteger(ddlB2B2BAgent.SelectedItem.Value);
        //    agentTypeId = 0;

        //}

        int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
        if (agentId > 0 && ddlB2BAgent.SelectedIndex > 0) agentId = Utility.ToInteger(ddlB2BAgent.SelectedItem.Value);

        LoginInfo userInfo = Settings.LoginInfo;
        DataTable dt = CT.TicketReceipt.BusinessLayer.ReceiptMaster.GetList(dcFromDate.Value, dcToDate.Value, "A", userInfo.UserID, userInfo.MemberType.ToString(), userInfo.LocationID, agentId, ddlSettlementMode.SelectedItem.Value, Utility.ToLong(ddlLocation.SelectedItem.Value)).Tables[0]; ;
        gvReceipt.DataSource = dt;
        gvReceipt.DataBind();
       // CommonGrid g = new CommonGrid();
       // g.BindGrid(gvReceipt, ReceiptList);
        filterControls();
        
    }
            
    #endregion
    #region gvReceipt Events
    
    protected void gvReceipt_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvReceipt.PageIndex = e.NewPageIndex;
            gvReceipt.EditIndex = -1;
            BindGrid();
           
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    protected void filterControls()
    {
        try
        {
            Label lblGrandText = (Label)this.Master.FindControl("lblGrandText");
            Label lblGrandTotal = (Label)this.Master.FindControl("lblGrandTotal");

            string grandTotal = Utility.ToString(((DataTable)(gvReceipt.DataSource)).Copy().Compute("SUM(RECEIPT_LOCAL_TO_COLLECT)", ""));
            lblGrandTotal.Text = string.Format(" {0} AED", (string.IsNullOrEmpty(grandTotal)) ? Formatter.ToCurrency(0) : grandTotal);
            lblGrandTotal.Text = string.Format(" {0} AED", (string.IsNullOrEmpty(grandTotal)) ? Formatter.ToCurrency(0) : Formatter.ToCurrency(grandTotal));

            DataTable dtTemp = (DataTable)gvReceipt.DataSource;
            dtExport = dtTemp;
            decimal totalFare = Utility.ToDecimal(dtTemp.Compute("SUM(RECEIPT_TOTAL_FARE)", ""));

            lblGrandText.Visible = true;
            lblGrandTotal.Visible = true;
            ((UpdatePanel)this.Master.FindControl("upnlGrandTotal")).Update();

        }
        catch { throw; }
    }
    protected void Filter_Click(object sender, EventArgs e)
    {
        try
        {
            //filterControls();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    #endregion
 
    protected void btnOk_Click(object sender, EventArgs e)
    {
        try
        {            
            long receiptId = Utility.ToLong(hdfReceiptId.Value);

            CT.TicketReceipt.BusinessLayer.ReceiptMaster receipt = new  CT.TicketReceipt.BusinessLayer.ReceiptMaster(receiptId);
            //receipt.DeleteRemarks = txtRemarks.Text.Trim();
            receipt.CreatedBy = Settings.LoginInfo.UserID;
            receipt.Status = Settings.DELETED;
            //long delAgentId = receipt.AgentId;
            receipt.Save();
           
            string docNo = receipt.DocNumber;// TODO viji
            lblSuccessMsg.Text = Formatter.ToMessage("Receipt No", docNo, CT.TicketReceipt.Common.Action.Deleted);
            //LoginInfo userInfo = Settings.LoginInfo;
            //ReceiptList = ReceiptMaster.GetList(dcFromDate.Value, dcToDate.Value, VisaAccountedStatus.All, userInfo.UserID, userInfo.MemberType.ToString(), userInfo.LocationID, Utility.ToInteger(ddlAgent.SelectedItem.Value), ddlSettlementMode.SelectedItem.Value, Utility.ToLong(ddlLocation.SelectedItem.Value), ddlStatus.SelectedItem.Value == "A" ? RecordStatus.Activated : RecordStatus.Deactivated).Tables[0];
            BindGrid();
          
            txtRemarks.Text = string.Empty;
            hdfReceiptId.Value = string.Empty;
        }
        catch (Exception ex)
        {
            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    # region Report
    protected void ITlnkPrint_Click(object sender, EventArgs e)
    {
        try
        {

            GridViewRow gvRow = (GridViewRow)(((LinkButton)sender).NamingContainer);
            HiddenField hdfReceiptID = (HiddenField)gvRow.FindControl("IThdfReceiptId");
            if (hdfReceiptID != null)
            {
                long reportId=Utility.ToLong(hdfReceiptID.Value);
                string script = "window.open('printReport.aspx?receiptId=" + Utility.ToString(reportId)+ "','','width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=yes,scrollbars=yes,resizable=yes,left=50,top=50');";
                StartupScript(this.Page, script, "Key");
            }

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }


    protected void ITlnkTest_Click(object sender, EventArgs e)
    {
        try
        {


        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }


    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            //string script = "window.open('ExportExcel.aspx?','','width=0,height=0,toolbar=yes,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            //StartupScript(this.Page, script, "Excel");
            
            //filterControls();
            BindGrid();
            //DataTable dt = ReceiptList.Copy(); ;
            //if(dtExport !=null)
            DataTable dt = ((DataTable)gvReceipt.DataSource).Copy(); ;
            decimal totalFare = Utility.ToDecimal(dt.Compute("SUM(RECEIPT_TOTAL_FARE)", ""));

            DataRow dr = dt.NewRow();
            dr["RECEIPT_TOTAL_FARE"] = totalFare;
            dr["RECEIPT_ID"] = -1;
            dr["RECEIPT_PAX_NAME"] = "GRAND TOTAL OF TOTAL FARE:";
            dt.Rows.Add(dr);
            
            string attachment = "attachment; filename=ReceiptList.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dgReceiptList.AllowPaging = false;
            dgReceiptList.DataSource = dt;
            dgReceiptList.DataBind();
            dgReceiptList.RenderControl(htw);
            Response.Write(sw.ToString());
        }
        catch(Exception ex)
        {
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
            //lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
        finally
        {
            Response.End();
        }
    }

    private  void StartupScript(Page page, string script, string key)
    {

        script = string.Format("{0};", script);
        if (page != null && !string.IsNullOrEmpty(script) && !page.ClientScript.IsStartupScriptRegistered(key))
        {
            if (ScriptManager.GetCurrent(page) != null && ScriptManager.GetCurrent(page).IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(page, page.GetType(), key, script, true);
            else
                page.ClientScript.RegisterStartupScript(page.GetType(), key, script, true);

        }
    }
    
    # endregion
    #region Date Format
    protected string IDDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }
    #endregion
    #region  advance filter
    
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindGrid();
           
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }    
    # endregion
    # region GridBindMethods
    protected object StatusVisible(object value)
    {
       
            return (object)true;
        
    }
    # endregion
}
