﻿#region NameSpaceRegion
using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.Core;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
#endregion

public partial class ItineraryAddServiceDetailsGUI : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    IntialiseControls();
                    ControlsEnabling();
                }
            }
            else
            {
                Session.Clear();
                Utility.Alert(this.Page, "Session Expired please Login Again!");
                //Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(ItineraryAddServiceDetails Page)" + ex.ToString(), "0");
        }
    }
    private void IntialiseControls()
    {
        try
        {

            // BindNationalitiesList();
            BindServicesList();
            if (Request.QueryString.Count > 0 && Request.QueryString["paxId"] != null)
            {
                DataTable dtPaxDetails = ItineraryAddServiceDetails.GetPaxDetails(Convert.ToInt32(Request.QueryString["paxId"]));
                if (dtPaxDetails != null && dtPaxDetails.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtPaxDetails.Rows)
                    {
                        if (dr["firstName"] != DBNull.Value && dr["lastName"] != DBNull.Value)
                        {
                            txtPaxName.Text = Convert.ToString(dr["firstName"]) + " " + Convert.ToString(dr["lastName"]);
                        }
                        if (dr["nationality"] != DBNull.Value)
                        {
                            ddlNationality.SelectedValue = Convert.ToString(dr["nationality"]);
                        }
                        if (dr["passportNumber"] != DBNull.Value)
                        {
                            txtPassportNumber.Text = Convert.ToString(dr["passportNumber"]);
                        }
                        if (dr["cellPhone"] != DBNull.Value)
                        {
                            txtMobileNo.Text = Convert.ToString(dr["cellPhone"]);
                        }
                    }
                }

                DataTable dtFlightInfo = ItineraryAddServiceDetails.GetFlightInfo(Convert.ToInt32(Request.QueryString["paxId"]));
                if (dtFlightInfo != null && dtFlightInfo.Rows.Count > 0)
                {

                    string flightNumber = string.Empty;
                    string routing = string.Empty;
                    string depDate = string.Empty;
                    string PNR_No = string.Empty;

                    DataRow item = dtFlightInfo.Rows[0];
                    if (item["depDateTime"] != DBNull.Value)
                    {
                        depDate = Util.GetDateStringNum(Convert.ToDateTime(item["depDateTime"]));
                    }
                    if (item["pnr"] != DBNull.Value)
                    {
                        PNR_No = (item["pnr"]).ToString();
                    }
                    foreach (DataRow dr in dtFlightInfo.Rows)
                    {
                        if (dr["flightNum"] != DBNull.Value && dr["airlineCode"] != DBNull.Value)
                        {
                            if (flightNumber.Length > 0)
                            {
                                flightNumber = flightNumber + "," + Convert.ToString(dr["airlineCode"]) + "-" + Convert.ToString(dr["flightNum"]);
                            }
                            else
                            {
                                flightNumber = Convert.ToString(dr["airlineCode"]) + "-" + Convert.ToString(dr["flightNum"]);
                            }
                        }
                        if (dr["depAirport"] != DBNull.Value && dr["arrAirport"] != DBNull.Value)
                        {
                            if (routing.Length > 0)
                            {
                                routing = routing + "," + Convert.ToString(dr["depAirport"]) + "-" + Convert.ToString(dr["arrAirport"]);
                            }
                            else
                            {
                                routing = Convert.ToString(dr["depAirport"]) + "-" + Convert.ToString(dr["arrAirport"]);
                            }
                        }
                    }
                    txtFlightNumbers.Text = flightNumber;
                    txtRouting.Text = routing;
                    txtTravelDate.Text = depDate;
                    txtPNR.Text = PNR_No;
                }
            }

        }
        catch
        {
            throw;
        }
    }

    private void ControlsEnabling()
    {
        try
        {
           string Cur_Url= Request.UrlReferrer.OriginalString;
            if (Cur_Url.Contains("ItineraryAdditionalServiceDetailsIframe.aspx"))
            {

                txtPaxName.Enabled = true;
                txtFlightNumbers.Enabled = true;
                txtTravelDate.Enabled = true;
                txtRouting.Enabled = true;
                //txtAmount.Enabled = true;
                txtPNR.Enabled = true;
                anc_dateLink1.Attributes.Add("onclick", "showCal1();");
            }   
        }
        catch
        {
       
        }
    }
    private void BindNationalitiesList()
    {
        try
        {

            ddlNationality.DataSource = Country.GetNationalityList();
            ddlNationality.DataTextField = "Key";
            ddlNationality.DataValueField = "Value";
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("--Select Nationality--", "0"));
        }
        catch
        {
            throw;
        }
    }

    private void BindServicesList()
    {
        try
        {
            if (Request.QueryString.Count > 0 && Request.QueryString["aId"] != null)
            {
                string x = Request.QueryString["aId"].ToString();
                ddlService.DataSource = ItineraryAddServiceMaster.GetActiveServiceList(Convert.ToInt32(Request.QueryString["aId"]));
                ddlService.DataTextField = "service_name";
                ddlService.DataValueField = "service_id";
                ddlService.DataBind();
            }
            ddlService.Items.Insert(0, new ListItem("--Select Service--", "0"));
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "(ItineraryAddServiceDetails Page)" + ex.ToString(), "0");
        }
    }





    private void Save()
    {

        try
        {
            decimal currentAgentBalance = 0;
            AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);
            agent.CreatedBy = Settings.LoginInfo.UserID;
            currentAgentBalance = agent.UpdateBalance(0);
            if (currentAgentBalance >= Convert.ToDecimal(txtAmount.Text))
            {
                CT.BookingEngine.ItineraryAddServiceDetails det = new CT.BookingEngine.ItineraryAddServiceDetails();
                det.Service_id = Convert.ToInt32(ddlService.SelectedItem.Value);
                det.Pax_name = txtPaxName.Text;
                det.Pax_nationality_id = Convert.ToString(ddlNationality.SelectedItem.Value);
                det.Pax_passport_no = Convert.ToString(txtPassportNumber.Text);
                det.Pax_mobile_no = Convert.ToString(txtMobileNo.Text);
                det.Flightno = Convert.ToString(txtFlightNumbers.Text);
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                det.Traveldate = Convert.ToDateTime(txtTravelDate.Text, dateFormat);
                det.Routing = Convert.ToString(txtRouting.Text);
                det.Remarks = Convert.ToString(txtRemarks.Text);
                det.Amount = Convert.ToDecimal(txtAmount.Text);
                det.Pnr_No = Convert.ToString(txtPNR.Text);
                if (Request.QueryString["paxId"] != null)
                    det.Pax_id = Convert.ToInt32(Request.QueryString["paxId"]);
                else
                    det.Pax_id = 0;//There is no paxId when we came through Iframe page, thats why we are passing 0 as paxId
                det.Status = "A";
                det.Created_by = (int)Settings.LoginInfo.UserID;
                int addId = det.Save();
                if (addId > 0)
                {
                    det.Add_id = addId;
                    Response.Redirect("ItineraryPrintAddServiceDetails.aspx?addId=" + addId, false);
                }
                else
                {
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "DuplicateNotification();", "SCRIPT");
                }
                if (det.Add_id > 0)
                {
                    GenerateInvoice(det);
                }

            }
            else
            {
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "BalanceNotification();", "SCRIPT");
            }
        }
        catch
        {
            throw;
        }

    }

    protected void ddlService_Change(object sender, EventArgs e)
    {
        try
        {
            if (ddlService.SelectedItem.Value != "0")
            {
                ItineraryAddServiceMaster serviceMaster = new ItineraryAddServiceMaster();
                DataSet ds = serviceMaster.GetData(Convert.ToInt32(ddlService.SelectedItem.Value));
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if (dr["service_amount"] != DBNull.Value)
                            {
                                txtAmount.Text = Convert.ToString(dr["service_amount"]);
                            }
                            else
                            {
                                txtAmount.Text = string.Empty;
                            }
                            if (dr["service_nationality"] != DBNull.Value)
                            {
                                hdnNat.Value = Convert.ToString(dr["service_nationality"]);
                            }
                            else
                            {
                                hdnNat.Value = string.Empty;
                            }
                        }
                        if (hdnNat.Value.Length > 0)
                        {
                            BindNationalitiesList();
                            ListItemCollection items = new ListItemCollection();

                            string[] selNat = hdnNat.Value.Split(',');
                            if (selNat.Length > 0)
                            {
                                foreach (string key in selNat)
                                {
                                    items.Add(ddlNationality.Items.FindByValue(key));
                                }
                            }
                            if (items.Count > 0)
                            {
                                ddlNationality.Items.Clear();
                                ddlNationality.DataSource = items;
                                ddlNationality.DataTextField = "Text";
                                ddlNationality.DataValueField = "Value";
                                ddlNationality.DataBind();
                                ddlNationality.Items.Insert(0, new ListItem("--Select Nationality--", "0"));
                            }
                        }


                    }
                }
            }
            else
            {
                txtAmount.Text = string.Empty;
                ddlNationality.Items.Clear();
                //ddlNationality.SelectedValue = "0";
            }
            ControlsEnabling();           
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "(ItineraryAddServiceDetails Page)" + ex.ToString(), "0");

        }
    }


    private void GenerateInvoice(ItineraryAddServiceDetails det)
    {
        try
        {
            Invoice invoice = new Invoice();
            int invoiceNumber = 0;
            invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(det, "", (int)Settings.LoginInfo.UserID, 1, ProductType.ItineraryAddService, Settings.LoginInfo.AgentId);
            if (invoiceNumber > 0)
            {
                invoice.Load(invoiceNumber);
                invoice.Status = InvoiceStatus.Paid;
                invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                invoice.UpdateInvoice();
            }
            //Update the agent balance
            decimal currentAgentBalance = 0;
            AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);
            agent.CreatedBy = Settings.LoginInfo.UserID;
            currentAgentBalance = agent.UpdateBalance(-Math.Ceiling(det.Amount));


            //Utility.StartupScript(this.Page, "updateAgentBalance();", "SCRIPT");
            //if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            //{
            //    Settings.LoginInfo.AgentBalance = currentAgentBalance;
            //    Label lblAgentBalance = (Label)Master.FindControl("lblAgentBalance");
            //    lblAgentBalance.Text = CT.TicketReceipt.Common.Formatter.ToCurrency(currentAgentBalance);
            //}

        }
        catch
        {
            throw;
        }
    }
    private void Clear()
    {
        try
        {
            ddlService.SelectedValue = "0";
            txtRemarks.Text = string.Empty;
            ddlNationality.Items.Clear();
            txtAmount.Text = string.Empty;

            if (txtPNR.Enabled)
            {
                txtPaxName.Text = string.Empty;
                txtPassportNumber.Text = string.Empty;
                txtMobileNo.Text = string.Empty;
                txtFlightNumbers.Text = string.Empty;
                txtTravelDate.Text = string.Empty;
                txtRouting.Text = string.Empty;
                txtPNR.Text = string.Empty;

            }
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    protected void BtnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();          
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(ItineraryAddServiceDetails Page)" + ex.ToString(), "0");
        }
    }
}
