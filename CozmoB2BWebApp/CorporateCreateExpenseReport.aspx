<%@ Page MasterPageFile="~/TransactionVisaTitle.master" Language="C#" AutoEventWireup="true" Inherits="CorporateCreateExpenseReportUI"
    Title="Create Expense Report" Codebehind="CorporateCreateExpenseReport.aspx.cs" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <!--Start: Yahoo Date Control scripts Section -->
    <style>

    .dropzone{
    background: #fff url(images/uploadbg.png) no-repeat center !important;
    background-size: 150px !important;
}
.dropzone .dz-message{
display:none;
}
</style>
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="Scripts/jsBE/Search.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" /> 
    <script type="text/javascript">
        var call1;

        function init2() {
            var today = new Date();
            call1 = new YAHOO.widget.CalendarGroup("call1", "fcontainer1");
            var type = GetQueryStringByParameter('type');
            if (type != null && type.length > 0 && type == 'T') {

                //Travel Mode
                var depDate = GetQueryStringByParameter('depDate');
                var arrDate = GetQueryStringByParameter('arrDate');

                var depMonth = depDate.split('/')[1];
                var depDay =  parseInt(depDate.split('/')[0]) + 1;
                var depYear = depDate.split('/')[2];

                var arrMonth = arrDate.split('/')[1];
                var arrDay = parseInt(arrDate.split('/')[0]) -1;
                
                var arrYear = arrDate.split('/')[2];
                 call1.cfg.setProperty("minDate", depMonth + "/" + depDay + "/" + depYear);
                 call1.cfg.setProperty("maxDate", arrMonth + "/" + arrDay + "/" + arrYear);
                 call1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            }
            
            call1.cfg.setProperty("close", true);
            call1.selectEvent.subscribe(setFlightDate1);
            call1.render();

        }
       

        function init1() {
            var today = new Date();
            call1 = new YAHOO.widget.CalendarGroup("call1", "fcontainer1");
            var type = GetQueryStringByParameter('type');
            if (type != null && type.length > 0 && type == 'T') {
            
                //Travel Mode
                var depDate = GetQueryStringByParameter('depDate');
                var arrDate = GetQueryStringByParameter('arrDate');
                
                var depMonth = depDate.split('/')[1];
                var depDay = depDate.split('/')[0];
                var depYear = depDate.split('/')[2];
                
                var arrMonth = arrDate.split('/')[1];
                var arrDay = arrDate.split('/')[0];
                var arrYear = arrDate.split('/')[2];
                call1.cfg.setProperty("minDate", depMonth + "/" + depDay + "/" + depYear);
                call1.cfg.setProperty("maxDate", arrMonth + "/" + arrDay + "/" + arrYear);   
            }
            
            call1.cfg.setProperty("close", true);
            call1.selectEvent.subscribe(setFlightDate1);
            call1.render();

        }
        function setFlightDate1() {
            var date1 = call1.getSelectedDates()[0];  
            var month = date1.getMonth() + 1;
            var day = date1.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=dcExpRptFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
            call1.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init1);


        function showFlightCalendar1() {
           // init();
            init1();
            init2();
            var valid = false;

            var travelModeType = GetQueryStringByParameter('type');
            if (travelModeType != null && travelModeType.length > 0 && travelModeType == 'T') {


                if (document.getElementById('<%=rbtnExpType.ClientID %>').checked == false && document.getElementById('<%=rbtnPerDeim.ClientID %>').checked == false) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select Expenses Type or Per Deim .";
                }
                else if (document.getElementById('<%=ddlCountry.ClientID %>').value == "0") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select country.";
                }
                else if (document.getElementById('<%=ddlCity.ClientID %>').value == "0") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select city.";
                }
                else if (document.getElementById('<%=ddlExpenseType.ClientID %>').value == "0") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select expense type.";
                }
                else {
                    valid = true;

                    var type = '';
                    if (document.getElementById('ctl00_cphTransaction_rbtnExpType').checked) {
                        type = 'E';

                    }
                    else {
                        type = 'A';
                    }

                    var cc = document.getElementById('ctl00_cphTransaction_ddlCountry').value;
                    var ci = document.getElementById('ctl00_cphTransaction_ddlCity').value;
                    var et = document.getElementById('ctl00_cphTransaction_ddlExpenseType').value;


                    var paramList = 'requestSource=getTravelDaysInclusion&type=' + type + '&cc=' + cc + '&ci=' + ci + '&et=' + et + '&ui=' + corpProfileUserId;
                    var url = "CorportatePoliciesExpenseAjax";
                    Ajax.onreadystatechange = bindTravelDays;
                    Ajax.open('POST', url, false);
                    Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    Ajax.send(paramList);


                    document.getElementById('fcontainer1').style.display = "block";
                    document.getElementById('errMess').style.display = "none";
                    var date1 = document.getElementById('<%=dcExpRptFromDate.ClientID %>').value;
                    if (date1.length > 0 && date1 != "DD/MM/YYYY") {
                        var depDateArray = date1.split('/');
                        call1.cfg.setProperty("selected", depDateArray[1] + "/" + eval(depDateArray[0]) + "/" + depDateArray[2]);
                        call1.setMonth(eval(depDateArray[1]) - 1);
                    }
                }
            }
            else {
                valid = true;
                document.getElementById('fcontainer1').style.display = "block";
                document.getElementById('errMess').style.display = "none";
                var date1 = document.getElementById('<%=dcExpRptFromDate.ClientID %>').value;
                if (date1.length > 0 && date1 != "DD/MM/YYYY") {
                    var depDateArray = date1.split('/');
                    call1.cfg.setProperty("selected", depDateArray[1] + "/" + eval(depDateArray[0]) + "/" + depDateArray[2]);
                    call1.setMonth(eval(depDateArray[1]) - 1);

                }
            }
            
            return valid;
        }

        function bindTravelDays() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0 && Ajax.responseText == "True") {
                        init2();
                    }
                    else {
                        init1();
                    }
                }
            }
        }      
    </script>

    <asp:HiddenField runat="server" ID="hdnDuplicateExpense" />
    <!-- Refer the drpzone css and scripts -->

    <script type="text/javascript" src="DropzoneJs_scripts/dropzone.js"></script>

    <link href="DropzoneJs_scripts/dropzone.css" rel="stylesheet" />
    <!-- DropZone initialisation -->

    <script type="text/javascript">
    
    //This function will validate the user input and allows the user to enter only alpha numeric             characters.
            
            var specialKeys = new Array();
            specialKeys.push(8); //Backspace
            specialKeys.push(9); //Tab
            specialKeys.push(46); //Delete
            specialKeys.push(36); //Home
            specialKeys.push(35); //End
            specialKeys.push(37); //Left
            specialKeys.push(39); //Right
            function IsAlphaNumeric(e) {
                var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));

                return ret;
            }
    
    
    var corpProfileUserId =  <% =corpProfileUserId %>;
  
    
    var Ajax;
    var amountToBeValidated ='';
    
    if (window.XMLHttpRequest) {
        Ajax = new window.XMLHttpRequest();
    }
    else {
        Ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    function GetQueryStringByParameter(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    
    var validAmountEntered = true;
    function validateAmount() {
    
      document.getElementById('errMess').style.display = "none";  
      
      if (document.getElementById('<%=rbtnExpType.ClientID %>').checked == false && document.getElementById('<%=rbtnPerDeim.ClientID %>').checked == false) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Expenses Type or Per Deim .";
                 // document.getElementById('<%=txtAmount.ClientID %>').value = "";
            }
            else if (document.getElementById('ctl00_cphTransaction_dcExpRptFromDate').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select date.";
                //document.getElementById('<%=txtAmount.ClientID %>').value = "";
            }
            else if (document.getElementById('<%=ddlCountry.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select country.";
                // document.getElementById('<%=txtAmount.ClientID %>').value = "";
            }
            else if (document.getElementById('<%=ddlCity.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select city.";
                // document.getElementById('<%=txtAmount.ClientID %>').value = "";
            }
            else if (document.getElementById('<%=ddlCostCenter.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select cost center.";
                // document.getElementById('<%=txtAmount.ClientID %>').value = "";
            }
            else if (document.getElementById('<%=ddlExpenseType.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select expense type.";
                // document.getElementById('<%=txtAmount.ClientID %>').value = "";
            }
            else if (document.getElementById('<%=ddlCurrency.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select currency.";
               // document.getElementById('<%=txtAmount.ClientID %>').value = "";
            }
            else if (document.getElementById('<%=txtAmount.ClientID %>').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add amount.";
            }
            else{
      
        var type = '';
        if (document.getElementById('ctl00_cphTransaction_rbtnExpType').checked) {
            type = 'E';

        }
        else {
            type = 'A';
        }
        
        var cc =  document.getElementById('ctl00_cphTransaction_ddlCountry').value;
        var ci = document.getElementById('ctl00_cphTransaction_ddlCity').value;
        var et =  document.getElementById('ctl00_cphTransaction_ddlExpenseType').value;
        
    
       var paramList = 'requestSource=getAmountForExpType&type='+type +'&cc='+ cc +'&ci='+ci +'&et='+ et+'&ui='+ corpProfileUserId;   
        var url = "CorportatePoliciesExpenseAjax";
        Ajax.onreadystatechange = bindAmount;
        Ajax.open('POST', url, false);
        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        Ajax.send(paramList);
        
        
        }
        
    
    }
    
    function bindAmount() {
        if (Ajax.readyState == 4) {
            if (Ajax.status == 200) {
                if (Ajax.responseText.length > 0) {
                    amountToBeValidated = Ajax.responseText;
                    
                    amountToBeValidated = parseInt(amountToBeValidated);
                    var amountEntered = parseInt(document.getElementById('ctl00_cphTransaction_txtAmount').value);
                    if(amountEntered >amountToBeValidated){
                    
                    document.getElementById('errMess').style.display = "block"; 
                    document.getElementById('errMess').innerHTML = "Out of policy - Expense amount exceeds the specified cap on expense   (" + amountToBeValidated+")" ;
                    validAmountEntered = true;
                 //   document.getElementById('ctl00_cphTransaction_txtAmount').value = "";
                    
                    }
                    else{
                     validAmountEntered = false;
                    }
                    
                    
                }
            }
        }
    }
    
    

        function displayAddDocs() {
            if (document.getElementById('dZUpload').style.display == 'none') {
                document.getElementById('dZUpload').style.display = 'block';
            }
            else {
                document.getElementById('dZUpload').style.display = 'none';
                
            }
        }

        //drop zone method
        function loadDropZone() {
             Dropzone.autoDiscover = false;
            $("#dZUpload").dropzone({
                url: "hn_CorporateFileUploader.ashx",
                addRemoveLinks: true,
                success: function(file, response) {
                    var imgName = response;
                    file.previewElement.classList.add("dz-success");
                },
                error: function(file, response) {
                    file.previewElement.classList.add("dz-error");
                },
                removedfile: function(file) {
                    var _ref;
                    if (file.previewElement) {
                        if ((_ref = file.previewElement) != null) {
                            _ref.parentNode.removeChild(file.previewElement);
                        }
                    }

                    $.ajax({
                        type: "POST",
                        url: "CorporateCreateExpenseReport.aspx/RemoveSession",
                        data: "{response: '" + file.name + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json"
                    })
                    return this._updateMaxFilesReachedClass();

                }

            })
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {
        loadDropZone();
    });

        $(document).ready(function() {


            
            loadDropZone();

        }) //EOF : dom ready function

    </script>

    <script type="text/javascript">



        function Validate() {
            var valid = false;
            document.getElementById('errMess').style.display = "none";

            if (document.getElementById('<%=hdnExpDetails.ClientID %>').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "You have not added any records to save.";
                
            }

            else {
                valid = true;
            }
            return valid;

        }
        function Clear() {

            document.getElementById('errMess').style.display = "none";
            document.getElementById('ctl00_cphTransaction_rbtnExpType').checked = false;
            document.getElementById('ctl00_cphTransaction_rbtnPerDeim').checked = false;
            document.getElementById('ctl00_cphTransaction_dcExpRptFromDate').value = "";
            document.getElementById('ctl00_cphTransaction_ddlCountry').value = "0";
            document.getElementById('ctl00_cphTransaction_ddlCity').value = "0";
            //document.getElementById('ctl00_cphTransaction_ddlCostCenter').value ="0";
            document.getElementById('ctl00_cphTransaction_ddlExpenseType').value = "0";
            document.getElementById('ctl00_cphTransaction_txtReferenceCode').value = "";
            document.getElementById('ctl00_cphTransaction_txtDescription').value = "";
            document.getElementById('ctl00_cphTransaction_ddlCurrency').value = "0";
            document.getElementById('ctl00_cphTransaction_txtAmount').value = "";
            document.getElementById('ctl00_cphTransaction_txtComment').value = "";
            //document.getElementById('<%=hdnExpDetails.ClientID %>').value = "";

        }
        function ClearAll() {
            Clear();
            document.getElementById('<%=hdnExpDetails.ClientID %>').value = "";

        }




        function addExpReport() {

            var valid = false;
            




            document.getElementById('errMess').style.display = "none";
            var expDuplicateExpReport = '';
            var duplicateCount = 0;
            if (document.getElementById('ctl00_cphTransaction_rbtnExpType').checked == true) {
                expDuplicateExpReport += document.getElementById('ctl00_cphTransaction_dcExpRptFromDate').value;
                expDuplicateExpReport += "@" + document.getElementById('ctl00_cphTransaction_ddlCountry').value;
                expDuplicateExpReport += "@" + document.getElementById('ctl00_cphTransaction_ddlCity').value;
                expDuplicateExpReport += "@" + document.getElementById('ctl00_cphTransaction_ddlExpenseType').value;
                var expReportSaved = document.getElementById("<%=hdnDuplicateExpense.ClientID %>").value.split('|');
                for (var p = 0; p < expReportSaved.length; p++) {
                    if (expDuplicateExpReport == expReportSaved[p]) {
                        duplicateCount++;
                    }
                }
            }

            if (document.getElementById('<%=rbtnExpType.ClientID %>').checked == false && document.getElementById('<%=rbtnPerDeim.ClientID %>').checked == false) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Expenses Type or Per Deim .";
            }
            
            


            else if (document.getElementById('<%=ddlCountry.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select country.";
            }
            else if (document.getElementById('<%=ddlCity.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select city.";
            }
            else if (document.getElementById('<%=ddlCostCenter.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select cost center.";
            }
            else if (document.getElementById('<%=ddlExpenseType.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select expense type.";
            }
            else if (document.getElementById('ctl00_cphTransaction_dcExpRptFromDate').value.length == 0)  {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select date.";
            }
            
            else if (document.getElementById('<%=ddlCurrency.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select currency.";
            }
            else if (document.getElementById('<%=txtAmount.ClientID %>').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add amount.";
            }
            else if (document.getElementById('ctl00_cphTransaction_rbtnExpType').checked == true && duplicateCount > 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "You have already added this combination !";
                duplicateCount = 0;

            }
            else if (document.getElementById('ctl00_cphTransaction_txtComment').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please add comment";


            }
            else if (validAmountEntered) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = " Out of policy - Expense amount exceeds the specified cap on expense (" + amountToBeValidated+")" ;
            }


            else {


                if (document.getElementById("<%=hdnDuplicateExpense.ClientID %>").value.length == 0) {
                    document.getElementById("<%=hdnDuplicateExpense.ClientID %>").value = expDuplicateExpReport;
                }
                else {
                    document.getElementById("<%=hdnDuplicateExpense.ClientID %>").value = document.getElementById("<%=hdnDuplicateExpense.ClientID %>").value + "|" + expDuplicateExpReport;

                }

                valid = true;

                var rowInfo = "";

                rowInfo = "-1"; //Record Id;

                rowInfo += "^";
                if (document.getElementById('<%=rbtnExpType.ClientID %>').checked) {
                    rowInfo += "E"; // Expenses Type

                }
                else if (document.getElementById('<%=rbtnPerDeim.ClientID %>').checked) {
                    rowInfo += "A"; //Per Deim
                }

                //Date
                rowInfo += "@" + document.getElementById('ctl00_cphTransaction_dcExpRptFromDate').value;


                //Counrty
                rowInfo += "@" + document.getElementById('ctl00_cphTransaction_ddlCountry').value;

                //City
                rowInfo += "@" + document.getElementById('ctl00_cphTransaction_ddlCity').value;

                //Cost Center
                rowInfo += "@" + document.getElementById('ctl00_cphTransaction_ddlCostCenter').value;

                //Expense Type
                rowInfo += "@" + document.getElementById('ctl00_cphTransaction_ddlExpenseType').value;

                //Reference Code
                rowInfo += "@" + document.getElementById('ctl00_cphTransaction_txtReferenceCode').value;

                //Description
                rowInfo += "@" + document.getElementById('ctl00_cphTransaction_txtDescription').value;

                //Currency
                rowInfo += "@" + document.getElementById('ctl00_cphTransaction_ddlCurrency').value;

                //Amount
                rowInfo += "@" + document.getElementById('ctl00_cphTransaction_txtAmount').value;

                //Comment
                rowInfo += "@" + document.getElementById('ctl00_cphTransaction_txtComment').value;

                if (document.getElementById('<%=hdnExpDetails.ClientID %>').value.length == 0) {

                    document.getElementById('<%=hdnExpDetails.ClientID %>').value = rowInfo;
                }
                else {
                    document.getElementById('<%=hdnExpDetails.ClientID %>').value = document.getElementById('<%=hdnExpDetails.ClientID %>').value + "|" + rowInfo;

                }

                console.log(document.getElementById('<%=hdnExpDetails.ClientID %>').value);



                //Form the html
                //1.ExpType
                var rptexpenseType = "";
                if (document.getElementById('<%=rbtnExpType.ClientID %>').checked) {
                    rptexpenseType = "Expenses"; // Expenses Type

                }
                else if (document.getElementById('<%=rbtnPerDeim.ClientID %>').checked) {
                    rptexpenseType = "Per Deim";
                }

                //2.Date
                var rptDate = document.getElementById('ctl00_cphTransaction_dcExpRptFromDate').value;

                //3.Employee Name
                var rptEmpName = document.getElementById('ctl00_cphTransaction_txtEmployeeName').value;

                //4.Country
                var rptCountry = document.getElementById('ctl00_cphTransaction_ddlCountry');
                var rptCountrySelectedText = rptCountry.options[rptCountry.selectedIndex].text;

                //4.1 City
                var rptCity = document.getElementById('ctl00_cphTransaction_ddlCity');
                var rptCitySelectedText = rptCity.options[rptCity.selectedIndex].text;

                //5.Cost Center
                var rptCostCenter = document.getElementById('ctl00_cphTransaction_ddlCostCenter');
                var rptCostCenterSelectedText = rptCostCenter.options[rptCostCenter.selectedIndex].text;

                //6.Expense Type
                var rptExpType = document.getElementById('ctl00_cphTransaction_ddlExpenseType');
                var rptExpTypeSelectedText = rptExpType.options[rptExpType.selectedIndex].text;

                //7.Ref code

                var rptRefCode = document.getElementById('ctl00_cphTransaction_txtReferenceCode').value;
                //8.Description
                var rptDesc = document.getElementById('ctl00_cphTransaction_txtDescription').value;

                //9.Currency

                var rptCurrency = document.getElementById('ctl00_cphTransaction_ddlCurrency');
                var rptCurrencySelectedText = rptCurrency.options[rptCurrency.selectedIndex].text;

                //10 Amount
                var rptAmount = document.getElementById('ctl00_cphTransaction_txtAmount').value;
                //11.Comment

                var rptComment = document.getElementById('ctl00_cphTransaction_txtComment').value;

                AppendExpneseDiv(

rptexpenseType,
rptDate,
rptEmpName,
rptCountrySelectedText,
rptCitySelectedText,
rptCostCenterSelectedText,
rptExpTypeSelectedText,
rptRefCode,
rptDesc,
rptCurrencySelectedText,
rptAmount,
rptComment,
rowInfo

                )





            
            $('select').select2();

            }    //EOf else block

            return valid;

        } //EOf function



        function AppendExpneseDiv(

rptexpenseType,
rptDate,
rptEmpName,
rptCountrySelectedText,
rptCitySelectedText,
rptCostCenterSelectedText,
rptExpTypeSelectedText,
rptRefCode,
rptDesc,
rptCurrencySelectedText,
rptAmount,
rptComment,
rowInfo

                ) {

            var ctrlID = parseInt(document.getElementById('hdnControlsCount').value);
            ctrlID = ctrlID + 1;
            document.getElementById('hdnControlsCount').value = ctrlID;
            var paramList = 'requestSource=getExpenseReportsHtml' + '&id=' + ctrlID + '&rptexpenseType=' + rptexpenseType + '&rptDate=' + rptDate + '&rptEmpName=' + rptEmpName + '&rptCountrySelectedText=' + rptCountrySelectedText + '&rptCitySelectedText=' + rptCitySelectedText + '&rptCostCenterSelectedText=' + rptCostCenterSelectedText + '&rptExpTypeSelectedText=' + rptExpTypeSelectedText + '&rptRefCode=' + rptRefCode + '&rptDesc=' + rptDesc + '&rptCurrencySelectedText=' + rptCurrencySelectedText + '&rptAmount=' + rptAmount + '&rptComment=' + rptComment + '&rowInfo=' + rowInfo;

            var url = "CorportatePoliciesExpenseAjax";


            Ajax.onreadystatechange = getExpenseReportsHtml;


            Ajax.open('POST', url, false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);

        }

        function getExpenseReportsHtml(response) {

            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {

                        $("#onlyReportsDiv:last").append(Ajax.responseText);
                        //$("#aExpenseDocs").trigger('click');
                        $("#aonlyExpenses").trigger('click');

                        Clear();

                    }
                }
            }
        }









        //This function validates the user input which accepts only +ve integers with or without decimals.
        //This function will be invoked when the user enters the amount.
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                return false;
            }
            return true;
        }

        var Ajax; //New Ajax object.
        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var city;
        var cities = [];
        function LoadCities(id) {
            //alert(document.getElementById(id).value);
            city = 'ctl00_cphTransaction_ddlCity';
            var paramList = 'requestSource=getCitiesList' + '&Country=' + document.getElementById(id).value + '&id=' + city; ;
            var url = "CorportatePoliciesExpenseAjax";
            Ajax.onreadystatechange = ShowCitiesList;
            Ajax.open('POST', url, false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);

        }

        //Ajax call which brings the cities list for the supplied country id
        function ShowCitiesList(response) {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        city = Ajax.responseText.split('#')[0];
                        var ddl = document.getElementById(city);
                        if (ddl != null) {
                            ddl.options.length = 0;
                            var el = document.createElement("option");
                            el.textContent = "Select City";
                            el.value = "0";
                            ddl.add(el, 0);
                            var values = Ajax.responseText.split('#')[1].split(',');

                            for (var i = 0; i < values.length; i++) {
                                var opt = values[i];
                                if (opt.length > 0 && opt.indexOf('|') > 0) {
                                    var el = document.createElement("option");
                                    el.textContent = opt.split('|')[0];
                                    el.value = opt.split('|')[1];
                                    ddl.appendChild(el);
                                }
                            }

                            if (cities.length > 0 && cities[0].trim().length > 0) {
                                var id = eval(city.split('-')[1]);
                                if (id == undefined) {
                                    id = 0;
                                }
                                else {
                                    id = eval(id - 1);
                                }

                                ddl.value = cities[id];
                            }
                        }
                    }
                }
            }
        }

    </script>

    <input type="hidden" id="hdnControlsCount" value="0" />
    <asp:HiddenField runat="server" ID="hdnExpDetails" />
    <div class="body_container">
        <div>
            <div class="CorpTrvl-tabbed-panel">
                <h2>
                    Create Expense Report
                </h2>
                <div id="errMess" style="display: none; color: Red; font-weight: bold; text-align: center;">
                </div>
                <div class="clear" style="margin-left: 25px">
                    <div id="fcontainer1" style="position: absolute; top: 200px; left: 10%; display: none;">
                    </div>
                </div>
                <div class="tab-content responsive">
                    <div style="background: #fff; padding: 10px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="custom_check pull-left">
                                        Select Expense Type :<span class="fcol_red">*</span>
                                        <label for="test1">
                                            Expense Category
                                        </label>
                                    </div>
                                    <div class="pull-left">
                                        <asp:RadioButton runat="server" ID="rbtnExpType" GroupName="Type" />
                                        <label for="test0">
                                            Per Deim
                                        </label>
                                        <asp:RadioButton runat="server" ID="rbtnPerDeim" GroupName="Type" />
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        
                        

                        
                        
                            
                         
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">
                                        Employee Name <span class="fcol_red">*</span></label>
                                    <asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="txtEmployeeName"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">
                                        Cost Center <span class="fcol_red">*</span>
                                    </label>
                                    <asp:DropDownList Enabled="false" CssClass="form-control" ID="ddlCostCenter" runat="server">
                                        <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">
                                        Country <span class="fcol_red">*</span></label>
                                    <asp:DropDownList CssClass="form-control" ID="ddlCountry" onchange="LoadCities(this.id)"
                                        runat="server">
                                        <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>
                                        City <span class="fcol_red">*</span>
                                    </label>
                                    <asp:DropDownList CssClass="form-control" ID="ddlCity" runat="server">
                                        <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">
                                        Expense Type <span class="fcol_red">*</span>
                                    </label>
                                    <asp:DropDownList CssClass="form-control" ID="ddlExpenseType" runat="server">
                                        <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            <div class="col-md-2">
                            
                            <div class="form-group">
                            <label for="">
                                         Date:<span class="fcol_red">*</span>
                             <asp:TextBox MaxLength="10" placeholder="dd/mm/yyyy" ID="dcExpRptFromDate" runat="server"
                                                    class="form-control"></asp:TextBox>
                               <a href="javascript:void(null)" onclick="return showFlightCalendar1()">
                                                    <img id="Img4" src="images/call-cozmo.png" alt="Pick Date" />
                                                </a>                     
                            
                            </div>
                                    
                                </div>
                                </div>
                                
                                <div class="row">
                            
                             <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">
                                        Currency <span class="fcol_red">*</span></label>
                                    <asp:DropDownList CssClass="form-control" ID="ddlCurrency" runat="server">
                                        <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">
                                        Amount <span class="fcol_red">*</span></label>
                                    <asp:TextBox onblur="javascript:validateAmount()" onkeypress="return isNumber(event);"
                                        CssClass="form-control" runat="server" ID="txtAmount"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">
                                        Report Name</label>
                                    <asp:TextBox  CssClass="form-control" runat="server"
                                        ID="txtReferenceCode"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">
                                        Business Purpose</label>
                                    <asp:TextBox  CssClass="form-control" runat="server"
                                        ID="txtDescription"></asp:TextBox>
                                </div>
                            </div>
                            
                           
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">
                                        Comment <span class="fcol_red">*</span></label>
                                    <asp:TextBox  CssClass="form-control" runat="server"
                                        ID="txtComment"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        &nbsp;
                                    </label>
                                    <div class="font_med">
                                        <a id="AddExpenseReport" onclick="javascript:return addExpReport();"><span class="glyphicon glyphicon-plus-sign pull-right">
                                        </span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label id="addDocs" onclick="javascript:displayAddDocs()">
                                        Add Claim documents <span class="glyphicon glyphicon-plus-sign"></span>
                                    </label>
                                    <div style="display: none;" id="dZUpload" class="dropzone">
                                        <div class="dz-default dz-message">
                                            Drop Files here.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs responsive" role="tablist">
                            <li role="presentation" class="active"><a id="aonlyExpenses" href="#ExpenseReports"
                                aria-controls="onlyExpenses" role="tab" data-toggle="tab" aria-expanded="true">Created
                                Reports</a> </li>
                        <%--    <li role="presentation"><a id="aExpenseDocs" href="#expenseDocs" aria-controls="aExpenseDocs"
                                role="tab" data-toggle="tab">Expense Docs</a> </li>--%>
                        </ul>
                        <div class="tab-content responsive">
                            <div role="tabpanel" id="ExpenseReports">
                                <div >
                                
                                <div class='col-md-12'>
                                <table class="table table-bordered b2b-corp-table">
                                <tbody id="onlyReportsDiv">
                                <tr>
                            <th>
                                Type
                            </th>
                            <th>
                                Date
                            </th>
                            <th>
                                Employee Name
                            </th>
                            <th>
                                Country
                            </th>
                            <th>
                                City
                            </th>
                            <th>
                                Cost Center
                            </th>
                            <th>
                               Expense Type
                            </th>
                            <th>
                               Report Name
                            </th>
                            <th>
                                Business Purpose
                            </th>
                            <th>
                                Currency
                            </th>
                            <th>
                                Amount
                            </th>
                             <th>
                                Comment
                            </th>
                        </tr>
                                
                                </tbody>
                                
                                  </table>
                                
                                </div>
                                
                                
                                    <div class="clearfix">
                                    </div>
                                </div>
                            </div>
                            
                            <div style="display: block" class="tab-pane">
                                <div class="clearfix">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pad_10">
                    <asp:Button CssClass="btn but_d btn_xs_block cursor_point mar-5" runat="server" ID="btnCreateExpense"
                        Text="Create" OnClientClick="return Validate();" OnClick="btnCreateExpense_Click" />
                    <asp:Button Text="Clear" CssClass="btn but_d btn_xs_block cursor_point" runat="server"
                        ID="btnClear" OnClientClick="javascript:ClearAll();" />
                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </div>
   
    <!-- Email DIV-->
    <div id='EmailDivApprover' runat='server' style='width: 100%; display: none;'>
    <%if (_profileExpenseDetailsList != null && _profileExpenseDetailsList.Count > 0) %>
                                            <%{  %>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background: #f3f3f3!important">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta name="viewport" content="width=device-width">
            <title>Expense Approval Request</title>
            <style>
                @media only screen
                {
                    html
                    {
                        min-height: 100%;
                        background: #f3f3f3;
                    }
                }
                @media only screen and (max-width:596px)
                {
                    table.body img
                    {
                        width: auto;
                        height: auto;
                    }
                    table.body center
                    {
                        min-width: 0 !important;
                    }
                    table.body .container
                    {
                        width: 95% !important;
                    }
                    table.body .columns
                    {
                        height: auto !important;
                        -moz-box-sizing: border-box;
                        -webkit-box-sizing: border-box;
                        box-sizing: border-box;
                        padding-left: 16px !important;
                        padding-right: 16px !important;
                    }
                    table.body .columns .columns
                    {
                        padding-left: 0 !important;
                        padding-right: 0 !important;
                    }
                    th.small-6
                    {
                        display: inline-block !important;
                        width: 50% !important;
                    }
                    th.small-12
                    {
                        display: inline-block !important;
                        width: 100% !important;
                    }
                    .columns th.small-12
                    {
                        display: block !important;
                        width: 100% !important;
                    }
                }
            </style>
        </head>
        <body style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box;
            -webkit-text-size-adjust: 100%; margin: 0; background: #f3f3f3!important; box-sizing: border-box;
            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
            line-height: 1.3; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100%!important">
            <span class="preheader" style="color: #f3f3f3; display: none!important; font-size: 1px;
                line-height: 1px; max-height: 0; max-width: 0; mso-hide: all!important; opacity: 0;
                overflow: hidden; visibility: hidden"></span>
            <table class="body" style="margin: 0; background: #f3f3f3!important; border-collapse: collapse;
                border-spacing: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                font-weight: 400; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left;
                vertical-align: top; width: 100%">
                <tr style="padding: 0; text-align: left; vertical-align: top">
                    <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto;
                        margin: 0; border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                        font-size: 16px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                        padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                        <center data-parsed="" style="min-width: 580px; width: 100%">
                            <table style="margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0;
                                float: none; margin: 0 auto; margin-top: 20px; padding: 10px; text-align: center;
                                vertical-align: top; width: 580px" align="center" class="container float-center">
                                <tbody>
                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                                            hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top;
                                            word-wrap: break-word">
                                            <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0;
                                                text-align: left; vertical-align: top; width: 100%">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                                                            hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                            text-align: left; vertical-align: top; word-wrap: break-word">
                                                            &#xA0;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table;
                                                padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <th class="small-12 large-12 columns first last" style="margin: 0 auto; color: #0a0a0a;
                                                            font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 1.3;
                                                            margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px;
                                                            text-align: left; width: 564px">
                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                vertical-align: top; width: 100%">
                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                    <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                        <img src="<%=Request.Url.Scheme%>://www.travtrolley.com/images/logo.jpg" style="-ms-interpolation-mode: bicubic;
                                                                            clear: both; display: block; max-width: 100%; outline: 0; text-decoration: none;
                                                                            width: auto">
                                                                    </th>
                                                                    <th class="expander" style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                        font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important;
                                                                        text-align: left; visibility: hidden; width: 0">
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0;
                                                text-align: left; vertical-align: top; width: 100%">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                                                            hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                            text-align: left; vertical-align: top; word-wrap: break-word">
                                                            &#xA0;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            
                                            <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table;
                                                padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <th class="small-12 large-12 columns first last" style="margin: 0 auto; color: #0a0a0a;
                                                            font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 1.3;
                                                            margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px;
                                                            text-align: left; width: 564px">
                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                vertical-align: top; width: 100%">
                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                    <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                        <h1 style="margin: 0; margin-bottom: 10px; color: inherit; font-family: Helvetica,Arial,sans-serif;
                                                                            font-size: 20px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                            padding: 0; text-align: left; word-wrap: normal">
   Dear
       <%if (!string.IsNullOrEmpty(approverNames) && approverNames.Length > 0) %>
                                              <%{ %>
                                              
                          <%=approverNames %>
                                              <%} %>
                                                           
                                                           
                                                           </h1>
                                                                        <p style="margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                            font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                            padding: 0; text-align: left">
                                                                            Find below expense claim details for Employee ID :
                                                                            <% =_profileExpenseDetailsList[0].EmpId %>, Employee Name :
                                                                            <% =_profileExpenseDetailsList[0].EmpName %></p>
                                                                        <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0;
                                                                            text-align: left; vertical-align: top; width: 100%">
                                                                            <tbody>
                                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                    <td height="30px" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400;
                                                                                        hyphens: auto; line-height: 30px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                                                        text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        &#xA0;
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        <table class="approver-table" style="background: #FFF; border: 1px solid #167F92;
                                                                            border-collapse: collapse; border-radius: 10px; border-spacing: 0; color: #024457;
                                                                            font-size: 11px; margin: 1em 0; padding: 0; text-align: left; vertical-align: top;
                                                                            width: 100%">
                                                                            <tr style="background-color: #EAF3F3; border: 1px solid #D9E4E6; padding: 0; text-align: left;
                                                                                vertical-align: top">
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Date
                                                                                </th>
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Expense Ref
                                                                                </th>
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Expense Category
                                                                                </th>
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Expense Type
                                                                                </th>
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Amount
                                                                                </th>
                                                                                
                                                                                <th style="Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left">Action</th>
                                                                            </tr>
   <%foreach(CT.Corporate.CorporateProfileExpenseDetails ped in _profileExpenseDetailsList) %>
                                                                            <%{  %>
                                                                            
                                                                            
                                                                       
                                                                            <tr style="border: 1px solid #D9E4E6; padding: 0; text-align: left; vertical-align: top">
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                      <% = Convert.ToString(ped.Date).Split(' ')[0]%>
                                                                                </td>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                <% = ped.DocNo%>
                                                                                </td>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                            <% =ped.ExpCategoryText%>
                                                                                </td>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                           <% = ped.ExpTypeText%>
                                                                                </td>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                          <% = ped.Amount%>
                                                                              </td>
    
                                                                    
                                                                                
    <td style="Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word">
    <a href="<% =ConfigurationManager.AppSettings["CORPORATE_ROOT_URL"]%>/hn_CorporateApprovals.ashx?status=<% =HttpContext.Current.Server.UrlEncode("A")%>&approverId=<%=HttpContext.Current.Server.UrlEncode(Convert.ToString(approverId))%>&expDetailId=<%=HttpContext.Current.Server.UrlEncode(Convert.ToString(ped.ExpDetailId))%>" style="Margin:0;color:green;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:underline">Approve</a> | <a href="<% =ConfigurationManager.AppSettings["CORPORATE_ROOT_URL"]%>/hn_CorporateApprovals.ashx?status=<% =HttpContext.Current.Server.UrlEncode("R")%>&approverId=<%=HttpContext.Current.Server.UrlEncode(Convert.ToString(approverId))%>&expDetailId=<%=HttpContext.Current.Server.UrlEncode(Convert.ToString(ped.ExpDetailId))%>" style="Margin:0;color:red;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:underline">Reject</a></td>
    
                                                                             
                                                                            </tr>
                                                                            <% 
                                                                               } %>
                                                                            
                                                                        </table>
                                                                        <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0;
                                                                            text-align: left; vertical-align: top; width: 100%">
                                                                            <tbody>
                                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                    <td height="30px" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400;
                                                                                        hyphens: auto; line-height: 30px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                                                        text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        &#xA0;
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        
                                                                        <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table;
                                                                            padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%">
                                                                            <tbody>
                                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                    <th class="small-12 large-12 columns first last" style="margin: 0 auto; color: #0a0a0a;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0!important;
                                                                                        padding-right: 0!important; text-align: left; width: 100%">
                                                                                        <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                            vertical-align: top; width: 100%">
                                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                                <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                                                    font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                                                    <p style="margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                                        font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                                        padding: 0; text-align: left">
                                                                                                        Regards<br>
                                                                                                                             <strong><% =_profileExpenseDetailsList[0].AgencyName %></strong></p>
                                                                                                </th>
                                                                                                <th class="expander" style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                                    font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important;
                                                                                                    text-align: left; visibility: hidden; width: 0">
                                                                                                </th>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </th>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                       
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                           
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </center>
                    </td>
                </tr>
            </table>
            <!-- prevent Gmail on iOS font size manipulation -->
            <div style="display: none; white-space: nowrap; font: 15px courier; line-height: 0">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
        </body>
        </html>
         <%} %>
    </div>
    
     <div id='EmailDivEmployee' runat='server' style='width: 100%; display: none;'>
     <%if (_profileExpenseDetailsList != null && _profileExpenseDetailsList.Count > 0) %>
                                            <%{  %>
     
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background: #f3f3f3!important">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta name="viewport" content="width=device-width">
            <title>Expense Approval Request</title>
            <style>
                @media only screen
                {
                    html
                    {
                        min-height: 100%;
                        background: #f3f3f3;
                    }
                }
                @media only screen and (max-width:596px)
                {
                    table.body img
                    {
                        width: auto;
                        height: auto;
                    }
                    table.body center
                    {
                        min-width: 0 !important;
                    }
                    table.body .container
                    {
                        width: 95% !important;
                    }
                    table.body .columns
                    {
                        height: auto !important;
                        -moz-box-sizing: border-box;
                        -webkit-box-sizing: border-box;
                        box-sizing: border-box;
                        padding-left: 16px !important;
                        padding-right: 16px !important;
                    }
                    table.body .columns .columns
                    {
                        padding-left: 0 !important;
                        padding-right: 0 !important;
                    }
                    th.small-6
                    {
                        display: inline-block !important;
                        width: 50% !important;
                    }
                    th.small-12
                    {
                        display: inline-block !important;
                        width: 100% !important;
                    }
                    .columns th.small-12
                    {
                        display: block !important;
                        width: 100% !important;
                    }
                }
            </style>
        </head>
        <body style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box;
            -webkit-text-size-adjust: 100%; margin: 0; background: #f3f3f3!important; box-sizing: border-box;
            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
            line-height: 1.3; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100%!important">
            <span class="preheader" style="color: #f3f3f3; display: none!important; font-size: 1px;
                line-height: 1px; max-height: 0; max-width: 0; mso-hide: all!important; opacity: 0;
                overflow: hidden; visibility: hidden"></span>
            <table class="body" style="margin: 0; background: #f3f3f3!important; border-collapse: collapse;
                border-spacing: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                font-weight: 400; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left;
                vertical-align: top; width: 100%">
                <tr style="padding: 0; text-align: left; vertical-align: top">
                    <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto;
                        margin: 0; border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                        font-size: 16px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                        padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                        <center data-parsed="" style="min-width: 580px; width: 100%">
                            <table style="margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0;
                                float: none; margin: 0 auto; margin-top: 20px; padding: 10px; text-align: center;
                                vertical-align: top; width: 580px" align="center" class="container float-center">
                                <tbody>
                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                                            hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top;
                                            word-wrap: break-word">
                                            <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0;
                                                text-align: left; vertical-align: top; width: 100%">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                                                            hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                            text-align: left; vertical-align: top; word-wrap: break-word">
                                                            &#xA0;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table;
                                                padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <th class="small-12 large-12 columns first last" style="margin: 0 auto; color: #0a0a0a;
                                                            font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 1.3;
                                                            margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px;
                                                            text-align: left; width: 564px">
                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                vertical-align: top; width: 100%">
                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                    <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                        <img src="<%=Request.Url.Scheme%>://www.travtrolley.com/images/logo.jpg" style="-ms-interpolation-mode: bicubic;
                                                                            clear: both; display: block; max-width: 100%; outline: 0; text-decoration: none;
                                                                            width: auto">
                                                                    </th>
                                                                    <th class="expander" style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                        font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important;
                                                                        text-align: left; visibility: hidden; width: 0">
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0;
                                                text-align: left; vertical-align: top; width: 100%">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                                                            hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                            text-align: left; vertical-align: top; word-wrap: break-word">
                                                            &#xA0;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <%if (_profileExpenseDetailsList != null && _profileExpenseDetailsList.Count > 0) %>
                                            <%{  %>
                                            <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table;
                                                padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <th class="small-12 large-12 columns first last" style="margin: 0 auto; color: #0a0a0a;
                                                            font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 1.3;
                                                            margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px;
                                                            text-align: left; width: 564px">
                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                vertical-align: top; width: 100%">
                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                    <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                        <h1 style="margin: 0; margin-bottom: 10px; color: inherit; font-family: Helvetica,Arial,sans-serif;
                                                                            font-size: 20px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                            padding: 0; text-align: left; word-wrap: normal">
                                                                            Dear
                                                                            <% =_profileExpenseDetailsList[0].EmpName%>,</h1>
                                                                        <p style="margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                            font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                            padding: 0; text-align: left">
                                                                            Find below expense claim details submitted by you :
                                                                            </p>
                                                                        <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0;
                                                                            text-align: left; vertical-align: top; width: 100%">
                                                                            <tbody>
                                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                    <td height="30px" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400;
                                                                                        hyphens: auto; line-height: 30px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                                                        text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        &#xA0;
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <table class="approver-table" style="background: #FFF; border: 1px solid #167F92;
                                                                            border-collapse: collapse; border-radius: 10px; border-spacing: 0; color: #024457;
                                                                            font-size: 11px; margin: 1em 0; padding: 0; text-align: left; vertical-align: top;
                                                                            width: 100%">
                                                                            <tr style="background-color: #EAF3F3; border: 1px solid #D9E4E6; padding: 0; text-align: left;
                                                                                vertical-align: top">
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Date
                                                                                </th>
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Expense Ref
                                                                                </th>
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Expense Category
                                                                                </th>
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Expense Type
                                                                                </th>
                                                                                <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left">
                                                                                    Amount
                                                                                </th>
                                                                            </tr>
                                                                            <%if (_profileExpenseDetailsList != null && _profileExpenseDetailsList.Count > 0) %>
                                                                            <%{  %>
                                                                            <% foreach (CT.Corporate.CorporateProfileExpenseDetails req in _profileExpenseDetailsList) %>
                                                                            <% { %>
                                                                            <tr style="border: 1px solid #D9E4E6; padding: 0; text-align: left; vertical-align: top">
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <% = Convert.ToString(req.Date).Split(' ')[0]%>
                                                                                </td>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <% = req.DocNo%>
                                                                                </td>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <% =req.ExpCategoryText%>
                                                                                </td>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <% = req.ExpTypeText%>
                                                                                </td>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <% = req.Amount%>
                                                                                </td>
                                                                            </tr>
                                                                            <% } %>
                                                                            <% } %>
                                                                        </table>
                                                                        <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0;
                                                                            text-align: left; vertical-align: top; width: 100%">
                                                                            <tbody>
                                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                    <td height="30px" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400;
                                                                                        hyphens: auto; line-height: 30px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                                                        text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        &#xA0;
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <%if (_profileExpenseDetailsList != null && _profileExpenseDetailsList.Count > 0) %>
                                                                        <%{  %>
                                                                        <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table;
                                                                            padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%">
                                                                            <tbody>
                                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                    <th class="small-12 large-12 columns first last" style="margin: 0 auto; color: #0a0a0a;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0!important;
                                                                                        padding-right: 0!important; text-align: left; width: 100%">
                                                                                        <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                            vertical-align: top; width: 100%">
                                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                                <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                                                    font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                                                    <p style="margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                                        font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                                        padding: 0; text-align: left">
                                                                                                        Regards<br>
                                                                                                       
                                                                                                        <strong><% =_profileExpenseDetailsList[0].AgencyName%></strong></p>
                                                                                                </th>
                                                                                                <th class="expander" style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                                    font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important;
                                                                                                    text-align: left; visibility: hidden; width: 0">
                                                                                                </th>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </th>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <% } %>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <%} %>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </center>
                    </td>
                </tr>
            </table>
            <!-- prevent Gmail on iOS font size manipulation -->
            <div style="display: none; white-space: nowrap; font: 15px courier; line-height: 0">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
        </body>
        </html>
        
        <%} %>
    </div>
</asp:Content>
