﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Configuration;
using CT.AccountingEngine;
using System.Transactions;
using CT.TicketReceipt.Common;
using System.Text.RegularExpressions;
using CT.Core;
using System.Xml.Serialization;
using System.IO;

public partial class SaveFailedBooking : CT.Core.ParentPage
{
    protected FlightItinerary itinerary = new FlightItinerary();
    protected string bookingSource = string.Empty;
    protected Ticket[] ticket;
    protected string fareBasisCodeOB = string.Empty;
    protected string fareBasisCodeIB = string.Empty;
    protected List<string> ticketNumbers = new List<string>();
    protected bool fromPendingQueue = false;
    protected AgentMaster agency;
    protected bool noCredit = false;
    protected bool overrideCreditLimit = false;

    protected bool onBehalf;
    protected bool isLCC = false;
    protected decimal amountToCompare = 0;
    protected Fare[] fareBreakDown;
    protected decimal[] otherCharges;
    protected decimal totalPublished;
    protected decimal handlingCharge;
    protected decimal addHandlingCharge;
    protected decimal additionalTxnFee;
    protected decimal tdsHc;
    protected decimal tdsAddHandlingCharge;
    protected decimal reverseHandlingCharge;
    protected decimal serviceTax;
    protected decimal totalAgentprice;
    protected decimal totalTransFee;
    protected bool BookingSaved = false;
    protected string ShowMessage = string.Empty;
    protected bool saveAllowed = true;
    protected string errorMessage = string.Empty;
    protected bool isHold = false;
    protected bool savePnr = false;
    protected BookingDetail booking = new BookingDetail();
    private const int TimeLimitToComplete = 7;
    protected string ShowErrorMessage = string.Empty;
    protected bool SaveError = false;
    protected bool hasInfant = false;
    protected bool modalPopShow = false;

    Dictionary<string, string> sFeeTypeList = new Dictionary<string, string>();
    Dictionary<string, string> sFeeValueList = new Dictionary<string, string>();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
               
                if (!IsPostBack)
                {
                    if (Session["itinerary"] != null)
                    {
                        Session["itinerary"] = null;
                    }
                    if (Request["source"] != null && Request["source"] != string.Empty)
                    {
                        bookingSource = Convert.ToString(Request["source"]);
                    }

                    if (Session["FlightItinerary"] != null)
                    {
                        itinerary = Session["FlightItinerary"] as FlightItinerary;

                        FailedBooking failedBooking = FailedBooking.Load(Request["pnrNo"].Trim());
                        if (failedBooking != null && !string.IsNullOrEmpty(failedBooking.Ticketxml))
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(Ticket[]));
                            StringReader sr = new StringReader(failedBooking.Ticketxml);
                            ticket = (Ticket[])ser.Deserialize(sr);
                            Session["ticket"] = ticket;

                            itinerary = new FlightItinerary(ticket[0].FlightId);
                            Session["itinerary"] = itinerary;
                            GetFlightItinerary();
                        }
                        else if (failedBooking != null && !string.IsNullOrEmpty(failedBooking.ItineraryXML))
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(FlightItinerary));
                            StringReader sr = new StringReader(failedBooking.ItineraryXML);
                            itinerary = ser.Deserialize(sr) as FlightItinerary;
                            Session["itinerary"] = itinerary;
                            GetFlightItinerary();
                        }
                    }
                    else
                    {
                        FailedBooking failedBooking = FailedBooking.Load(Request["pnrNo"].Trim());

                        if (failedBooking != null && !string.IsNullOrEmpty(failedBooking.Ticketxml))
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(Ticket[]));
                            StringReader sr = new StringReader(failedBooking.Ticketxml);
                            ticket = (Ticket[])ser.Deserialize(sr);
                            Session["ticket"] = ticket;
                            itinerary = new FlightItinerary(ticket[0].FlightId);
                            Session["itinerary"] = itinerary;
                            GetFlightItinerary();
                        }
                        else if (failedBooking != null && !string.IsNullOrEmpty(failedBooking.ItineraryXML))
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(FlightItinerary));
                            StringReader sr = new StringReader(failedBooking.ItineraryXML);
                            itinerary = ser.Deserialize(sr) as FlightItinerary;
                            Session["itinerary"] = itinerary;
                            GetFlightItinerary();
                        }

                    }
                }
                else
                {
                    itinerary = (FlightItinerary)Session["itinerary"];
                    ticket = (Ticket[])Session["ticket"];
                    ticketNumbers = (List<string>)Session["ticketNumber"];                    
                }                
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }


        }
        catch (Exception ex)
        {
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void GetFlightItinerary()
    {
        try
        {
            int flightId = FlightItinerary.GetFlightId(Request["pnrNo"]);
            if (flightId <= 0)
            {                
                ticket = (Ticket[])Session["ticket"];
                ticketNumbers = (List<string>)Session["ticketNumber"];               

                if (Session["itinerary"] != null)
                {
                    itinerary = (FlightItinerary)Session["itinerary"];
                    agency = new AgentMaster(itinerary.AgencyId);

                    if (fromPendingQueue)
                    {
                        itinerary.BookingMode = BookingMode.Auto;
                    }
                    else
                    {
                        itinerary.BookingMode = BookingMode.Import;
                    }

                    itinerary.PaymentMode = ModeOfPayment.Credit;
                    
                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {                        
                        itinerary.Passenger[i].Price.PriceId = 0;//to insert instead of update
                    }
                }
            }
            else
            {
                //Response.Redirect("DisplayPNR.aspx?pnrNo=" + Request["pnrNo"] + "&sr=" + Request["source"] + "&error=PNR already exists");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        if (Session["itinerary"] != null)
        {
            if (itinerary.Segments == null || itinerary.Segments.Length <= 0)
            {
                Response.Redirect("DisplayPNR.aspx?pnrNo=" + Request["pnrNo"] + "&sr=" + Request["source"] + "&error=PNRcancelled");
            }
            if (itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource == BookingSource.HermesAirLine || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp) 
            {
                isLCC = true;
            }
            if (itinerary.PNR != null && itinerary.PNR.Length > 0 && FailedBooking.CheckPNR(itinerary.PNR))
            {
                fromPendingQueue = true;
            }
            if (isLCC)
            {
                // amountToCompare = AccountingEngine.Ledger.IsAgentCapable(agency.AgencyId, AccountingEngine.AccountType.LCC); //sai
            }
            else
            {
                // amountToCompare = AccountingEngine.Ledger.IsAgentCapable(agency.AgencyId, AccountingEngine.AccountType.NonLCC); //sai
            }
            decimal totalPrice = 0;
            for (int k = 0; k < itinerary.Passenger.Length; k++)
            {
                totalPrice += itinerary.Passenger[k].Price.GetAgentPrice();
                totalPrice += (itinerary.Passenger[k].Price.Markup - itinerary.Passenger[k].Price.Discount);
            }

            if (amountToCompare < totalPrice)
            {
                noCredit = true;
            }
            //overrideCreditLimit = Role.IsAllowedTask(loggedMember.RoleId, (int)CT.Core1.Task.OverrideCreditLimit); //TODO Ziyad
            itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
            // itinerary.IsDomestic = Utility.IsDomestic(itinerary.Segments, "" + ConfigurationSystem.LocaleConfig["CountryCode"] + ""); //TODO Ziyad
            //Check if more than one segment and round trip - which one is destination
            // Redirect to display PNR page and show modal pop to get input form user
            if (itinerary.Segments != null && itinerary.Segments.Length > 2 && Request["dest"] == null)
            {
                modalPopShow = true;
            }
            else if (Request["dest"] != null && Request["dest"].Length > 0)
            {
                itinerary.Destination = Convert.ToString(Request["dest"]);
            }
            else
            {
                if (itinerary.Segments.Length == 2 && (itinerary.Segments[0].Origin.CityCode == itinerary.Segments[itinerary.Segments.Length - 1].Destination.CityCode))
                {
                    itinerary.Destination = itinerary.Segments[0].Destination.CityCode;
                }
                else
                {
                    itinerary.Destination = itinerary.Segments[itinerary.Segments.Length - 1].Destination.CityCode;
                }
            }

            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                if (i == 0)
                {
                    itinerary.Passenger[i].IsLeadPax = true;
                }
                itinerary.Passenger[i].Nationality = itinerary.Passenger[i].Country;
                if (itinerary.Passenger[i].Title == null || itinerary.Passenger[i].Title.Length == 0)
                {
                    Match titleMatch = Regex.Match(itinerary.Passenger[i].FirstName, ConfigurationSystem.AllowedTitles, RegexOptions.IgnoreCase);
                    if (titleMatch.Success)
                    {
                        itinerary.Passenger[i].Title = titleMatch.Value.Replace('.', ' ').Trim();
                        itinerary.Passenger[i].FirstName = itinerary.Passenger[i].FirstName.Substring(0, titleMatch.Index).Trim();
                    }
                }
                if (itinerary.Passenger[i].Price == null)
                {
                    itinerary.Passenger[i].Price = new PriceAccounts();
                }                
            }
            //// adding created By
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                if (isLCC)
                {
                    itinerary.Segments[i].FlightStatus = FlightStatus.Confirmed;
                    //TODO: Check whether this status is needed for LCCs or not
                    itinerary.Segments[i].Status = "HK";
                }
                if (itinerary.Segments[i] != null)
                {
                    itinerary.Segments[i].CreatedBy = (int)Settings.LoginInfo.UserID;
                    itinerary.Segments[i].ETicketEligible = true;
                }                
            }

            fareBreakDown = new Fare[4];
            otherCharges = new decimal[4];
            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                totalPublished += itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.AirlineTransFee + itinerary.Passenger[i].Price.Tax + itinerary.Passenger[i].Price.OtherCharges;
                handlingCharge += itinerary.Passenger[i].Price.AgentCommission;
                addHandlingCharge += itinerary.Passenger[i].Price.AgentPLB;
                additionalTxnFee += itinerary.Passenger[i].Price.AdditionalTxnFee;
                tdsHc += itinerary.Passenger[i].Price.TdsCommission;
                tdsAddHandlingCharge += itinerary.Passenger[i].Price.TDSPLB;
                reverseHandlingCharge += itinerary.Passenger[i].Price.ReverseHandlingCharge;
                serviceTax += itinerary.Passenger[i].Price.SeviceTax;
                totalAgentprice += itinerary.Passenger[i].Price.GetAgentPrice();
                totalAgentprice += (itinerary.Passenger[i].Price.Markup - itinerary.Passenger[i].Price.Discount);
                totalAgentprice += itinerary.Passenger[i].Price.OutputVATAmount;
                totalAgentprice += itinerary.Passenger[i].Price.BaggageCharge;
                totalTransFee += itinerary.Passenger[i].Price.TransactionFee;
                int j = (int)itinerary.Passenger[i].Type - 1;
                if (fareBreakDown[j] == null)
                {
                    fareBreakDown[j] = new Fare();
                    fareBreakDown[j].PassengerType = itinerary.Passenger[i].Type;
                    fareBreakDown[j].PassengerCount = 1;
                    fareBreakDown[j].BaseFare = Convert.ToSingle(itinerary.Passenger[i].Price.PublishedFare);
                    fareBreakDown[j].AirlineTransFee = itinerary.Passenger[i].Price.AirlineTransFee;
                    fareBreakDown[j].TotalFare = Convert.ToSingle(itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.AirlineTransFee + itinerary.Passenger[i].Price.Tax + itinerary.Passenger[i].Price.Markup - itinerary.Passenger[i].Price.Discount);
                    fareBreakDown[j].SellingFare = Convert.ToSingle(itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.Tax - itinerary.Passenger[i].Price.AgentCommission - itinerary.Passenger[i].Price.AgentPLB);
                    otherCharges[j] = itinerary.Passenger[i].Price.OtherCharges;
                }
                else
                {
                    fareBreakDown[j].PassengerCount++;
                    fareBreakDown[j].BaseFare += Convert.ToDouble(itinerary.Passenger[i].Price.PublishedFare);
                    fareBreakDown[j].AirlineTransFee += itinerary.Passenger[i].Price.AirlineTransFee;
                    fareBreakDown[j].TotalFare += Convert.ToDouble(itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.AirlineTransFee + itinerary.Passenger[i].Price.Tax + itinerary.Passenger[i].Price.Markup - itinerary.Passenger[i].Price.Discount);
                    otherCharges[j] = itinerary.Passenger[i].Price.OtherCharges;
                }
            }
                        

            if (itinerary.Passenger[0].Price.PublishedFare + itinerary.Passenger[0].Price.Tax == new decimal(0))
            {
                BookingSaved = true;
                ShowMessage = "No Pricing Information <br> Please store the fare information in the pnr and then retrieve";
            }

            CheckForSaveAllowed();
        }
    }
    private void CheckForSaveAllowed()
    {
        if (ticket != null && ticket.Length > itinerary.Passenger.Length)
        {
            Audit.Add(EventType.DisplayPNR, Severity.Normal, (int)Settings.LoginInfo.UserID, "Itinerary can not be saved as there exist multiple ticket for a passenger. PNR = " + itinerary.PNR, Request.ServerVariables["REMOTE_ADDR"]);
            saveAllowed = false;
            errorMessage = "Itinerary can not be saved as there exist multiple ticket for a passenger. Please contact Help Desk.";
        }
    }
    protected void UseCredit_Click()
    {
        //System.Threading.Thread.Sleep(5000);
        BookingResponse bookingResponse = BookItinerary(BookingStatus.Ready);
        BookingSaved = true;
        ShowMessage = "PNR Information has been Saved";
        savePnr = true;
        //expire session

        if (booking.BookingId != 0)
        {
            try
            {
                SendFlightTicketEmail();
            }
            catch { }
            finally
            {
                ClearBookingSession();
            }

            Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + booking.BookingId, false);
        }
    }

    

    private void ClearBookingSession()
    {
        Session.Remove("itinerary");
        Session.Remove("ticket");
        Session.Remove("ticketNumber");
    }
    private BookingResponse BookItinerary(BookingStatus status)
    {
        itinerary = (FlightItinerary)Session["itinerary"];
        // Creating booking detail 
        bool saveQueueForPending = false;
        bool generateInvoice = false;
        bool updateFailedBookingStatus = false;
        CT.Core.Queue queue = new CT.Core.Queue();
        booking.AgencyId = itinerary.AgencyId;
        booking.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
        booking.Status = status;
        // Getting itinerary from session and setting some properties
        agency = new AgentMaster(itinerary.AgencyId);
        //agency = new AgentMaster(agency.ID);
        if (itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource == BookingSource.HermesAirLine || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp)
        {
            isLCC = true;
        }

        

        BookingHistory bh = new BookingHistory();
        bh.EventCategory = EventCategory.Booking;
        bh.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
        if (itinerary.PNR != null && itinerary.PNR.Length > 0 && FailedBooking.CheckPNR(itinerary.PNR))
        {
            fromPendingQueue = true;
            bh.Remarks = "Booking created";
        }
        else
        {
            bh.Remarks = "Booking created via Failed Booking";
        }
        ticket = (Ticket[])Session["ticket"];
        itinerary.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
        itinerary.IsOurBooking = false;
        itinerary.Origin = (string)itinerary.Segments[0].Origin.CityCode;
        if (Request["fareBasisCodeOB"] != null)
        {
            fareBasisCodeOB = Request["fareBasisCodeOB"].ToString();
        }
        if (Request["dest"] != null && Request["dest"].Length > 0)
        {
            itinerary.Destination = (string)Request["dest"];
        }
        else
        {
            itinerary.Destination = itinerary.Segments[itinerary.Segments.Length - 1].Destination.CityCode;
        }

        // Sending itinerary for booking -- TO DO: check
        BookingResponse bookingResponse = new BookingResponse();
        // TO DO: Fare Rules
        List<FareRule> fareRule = new List<FareRule>();
        if (itinerary.FlightBookingSource == BookingSource.WorldSpan)
        {
            // fareRule = Worldspan.GetFareRuleList(itinerary.FareRules);
        }
        else if (itinerary.FlightBookingSource == BookingSource.Galileo)
        {
            // fareRule = GalileoApi.GetFareRuleList(itinerary.FareRules);
        }
        else if (itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
        {
            // fareRule = SpiceJetAPI.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB);
        }
        else if (itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.IndigoCorp)
        {
            // fareRule = Navitaire.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB, "6E");
        }
        else if (itinerary.FlightBookingSource == BookingSource.Paramount)
        {
            fareRule = itinerary.FareRules;
            for (int i = 0; i < fareRule.Count; i++)
            {
                fareRule[i].Origin = itinerary.Segments[0].Origin.CityCode;
                fareRule[i].Destination = itinerary.Destination;
            }
        }
        else if (itinerary.FlightBookingSource == BookingSource.AirDeccan)
        {
            // fareRule = AirDeccanApi.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB);
        }
        else if (itinerary.FlightBookingSource == BookingSource.Amadeus)
        {
            // fareRule = itinerary.FareRules;
        }
        else if (itinerary.FlightBookingSource == BookingSource.Mdlr)
        {
            fareRule = itinerary.FareRules;
            for (int i = 0; i < fareRule.Count; i++)
            {
                fareRule[i].Origin = itinerary.Segments[0].Origin.CityCode;
                fareRule[i].Destination = itinerary.Destination;
            }
        }
        else if (itinerary.FlightBookingSource == BookingSource.GoAir)
        {
            // fareRule = GoAirApi.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB);
        }
        else if (itinerary.FlightBookingSource == BookingSource.Sama)
        {
            //  fareRule = Sama.GetFareRule(itinerary.FareRules);
        }
        else if (itinerary.FlightBookingSource == BookingSource.HermesAirLine)
        {
            fareRule = itinerary.FareRules;
        }

        itinerary.FareRules = fareRule;

        decimal totalSellingPrice = 0;
        for (int i = 0; i < itinerary.Passenger.Length; i++)
        {
            //itinerary.Passenger[i].Price = CT.AccountingEngine1.AccountingEngine.GetPrice(itinerary, Convert.ToInt32(agency.ID), i, 0);
            totalSellingPrice += (itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.Tax) - (itinerary.Passenger[i].Price.AgentPLB + itinerary.Passenger[i].Price.AgentCommission) + (itinerary.Passenger[i].Price.TdsCommission + itinerary.Passenger[i].Price.TDSPLB + itinerary.Passenger[i].Price.SeviceTax);
            itinerary.Passenger[i].Price.PriceId = 0;//for Save
            itinerary.Passenger[i].PaxId = 0;//for Save
            itinerary.Passenger[i].CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
        }
        foreach (FlightInfo fi in itinerary.Segments)
        {
            fi.SegmentId = 0;//for Save
        }
              
        decimal agentCurrentBalance = agency.UpdateBalance(0);
        try
        {
            if (agentCurrentBalance < totalSellingPrice)
            {
                throw new ArgumentException("Insufficient Credit Balance ! Please contact Admin");
            }
        }
        catch (ArgumentException excep)
        {
            BookingSaved = true;
            ShowErrorMessage = excep.Message;
            SaveError = true;
            booking.BookingId = 0;
        }
        queue.QueueTypeId = (int)QueueType.Booking;
        queue.StatusId = (int)QueueStatus.Assigned;
        queue.AssignedTo = Convert.ToInt32(Session["memberId"]);
        queue.AssignedBy = Convert.ToInt32(Session["memberId"]);
        queue.AssignedDate = DateTime.Now.ToUniversalTime();
        queue.CompletionDate = DateTime.Now.ToUniversalTime().AddDays(TimeLimitToComplete);
        queue.CreatedBy = Convert.ToInt32(Session["memberId"]);
        // Saving the booked itinerary to database
        using (TransactionScope updateTransaction = new TransactionScope())
        {
            if (booking.Status == BookingStatus.Ready)
            {
                // Agency.AlterCredit((-Convert.ToDouble(totalSellingPrice)), agency.AgencyId, !isAgent);
            }
            //We are also saving this bookingid in queue table
            //We are assigning same member to assignedTo,assignBy,Createdby
            //We take 7 as time limit of completion
            try
            {
                int flightId = FlightItinerary.GetFlightId(itinerary.PNR);
                //if (!(fromPendingQueue && (!isLCC || flightId > 0)))
                if (flightId == 0)
                {
                    saveQueueForPending = true;
                    booking.Save(itinerary, true);
                    // Utility.UpdateAirlinePNR(itinerary); // this utility is different sai
                }
                else
                {
                    saveQueueForPending = false;
                    booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(flightId));
                   
                    // Utility.UpdateAirlinePNR(itinerary); //sai
                    itinerary = new FlightItinerary(flightId);
                }
                if (itinerary.IsLCC || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.HermesAirLine || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp)
                {
                    SaveTicketInfo();
                    booking.SetBookingStatus(BookingStatus.Ticketed, Utility.ToInteger(Settings.LoginInfo.UserID));
                    generateInvoice = true;
                }
                else
                {
                    if (ticket != null && ticket.Length > 0)
                    {
                        SaveTicketInfo();
                        if (ticket.Length == itinerary.Passenger.Length)
                        {
                            booking.SetBookingStatus(BookingStatus.Ticketed, Utility.ToInteger(Settings.LoginInfo.UserID));
                        }
                        else
                        {
                            booking.Lock(BookingStatus.InProgress, Utility.ToInteger(Settings.LoginInfo.UserID));
                        }
                    }
                    else
                    {
                        bh.BookingId = booking.BookingId;
                        bh.Save();
                    }
                }
            }
            catch (ArgumentException excep)
            {
                BookingSaved = true;
                ShowErrorMessage = excep.Message;
                SaveError = true;
                booking.BookingId = 0;
                throw excep;
            }
            catch (BookingEngineException excep)
            {
                if (excep.Message.IndexOf("Ticket for this passenger is already created") >= 0)
                {
                    updateFailedBookingStatus = true;
                }
                BookingSaved = true;
                ShowErrorMessage = excep.Message;
                SaveError = true;
                booking.BookingId = 0;
            }
            catch (Exception excep)
            {
                BookingSaved = true;
                ShowErrorMessage = excep.Message;
                SaveError = true;
                booking.BookingId = 0;
                throw excep;
            }

           
            if (!SaveError)
            {
                if (saveQueueForPending)
                {
                    queue.ItemId = booking.BookingId;
                    if (string.IsNullOrEmpty(queue.Remarks))
                    {
                        queue.Remarks = string.Empty;
                    }
                    queue.Save();
                    #region Update FailedBooking Status
                    //code to check and update status if there is a record corresponding to PNR in FailedBooking table
                    if (itinerary.PNR != null && itinerary.PNR.Length > 0 && FailedBooking.CheckPNR(itinerary.PNR))
                    {
                        FailedBooking.UpdateStatus(itinerary.PNR, CT.BookingEngine.Status.Saved, "Booking is Saved");
                    }
                    #endregion
                    updateTransaction.Complete();
                }
                else if (fromPendingQueue)
                {
                    #region Update FailedBooking Status
                    //code to check and update status if there is a record corresponding to PNR in FailedBooking table
                    if (itinerary.PNR != null && itinerary.PNR.Length > 0 && FailedBooking.CheckPNR(itinerary.PNR))
                    {
                        FailedBooking.UpdateStatus(itinerary.PNR, CT.BookingEngine.Status.Saved, "Ticket is Saved");
                    }
                    #endregion
                    updateTransaction.Complete();
                }
                else
                {
                    queue.ItemId = booking.BookingId;
                    if (string.IsNullOrEmpty(queue.Remarks))
                    {
                        queue.Remarks = string.Empty;
                    }
                    queue.Save();
                    #region Update FailedBooking Status
                    //code to check and update status if there is a record corresponding to PNR in FailedBooking table
                    if (itinerary.PNR != null && itinerary.PNR.Length > 0 && FailedBooking.CheckPNR(itinerary.PNR))
                    {
                        FailedBooking.UpdateStatus(itinerary.PNR, CT.BookingEngine.Status.Saved, "Booking is Saved");
                    }
                    #endregion
                    updateTransaction.Complete();
                }
            }
        }

        try
        {
            if (booking.Status == BookingStatus.Ticketed)
            {
                Invoice invoice = new Invoice();
                int invoiceNumber = Invoice.isInvoiceGenerated(Ticket.GetTicketList(itinerary.FlightId)[0].TicketId, ProductType.Flight);

                if (invoiceNumber > 0)
                {
                    invoice.Load(invoiceNumber);
                }
                else
                {
                    invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(itinerary.FlightId, "", (int)Settings.LoginInfo.UserID, ProductType.Flight, 1);
                    if (invoiceNumber > 0)
                    {
                        invoice.Load(invoiceNumber);
                        invoice.Status = InvoiceStatus.Paid;
                        invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                        invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                        invoice.UpdateInvoice();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message, Request.ServerVariables["REMOTE_ADDR"]);
            try
            {
                string message = "Invoicing is Failed for the following PNR:(" + itinerary.PNR + ").Reason: " + ex.ToString() + "\nAgencyId is " + itinerary.AgencyId.ToString() + "\n Logged in Member is " + Settings.LoginInfo.FirstName + "(" + (int)Settings.LoginInfo.UserID + ", " + Settings.LoginInfo.LoginName + ")\n";
                Email.Send(CT.Configuration.ConfigurationSystem.Email["fromEmail"], CT.Configuration.ConfigurationSystem.Email["ErrorNotificationMailingId"], "Invoice Failed in Import PNR Page .", message);
            }
            catch { }
            throw ex;
        }
        ///This is the case when the ticket is saved in DB created and 
        ///still we are trying to save from failed booking queue
        ///In this case update the status in failed booking queue
        if (updateFailedBookingStatus)
        {
            if (FailedBooking.CheckPNR(itinerary.PNR))
            {
                FailedBooking.UpdateStatus(itinerary.PNR, CT.BookingEngine.Status.Saved, "Booking is Saved");
                int flightid = FlightItinerary.GetFlightId(itinerary.PNR);
                if (flightid > 0)
                {
                    int bookingid = BookingDetail.GetBookingIdByFlightId(flightid);
                    Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + bookingid);
                }
            }
        }
        if (!SaveError && generateInvoice)
        {
            if (saveQueueForPending || fromPendingQueue)
            {
                using (TransactionScope accountTransaction = new TransactionScope())
                {
                    //int invoiceNumber =  AccountUtility.RaiseInvoice(itinerary, string.Empty, Utility.ToInteger(Settings.LoginInfo.UserID)); //TODO Ziyad
                    if (itinerary.PaymentMode == ModeOfPayment.CreditCard && !(itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.HermesAirLine || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp))
                    {
                        // MetaSearchEngine.MakeCCPaymentEntry(booking, itinerary, loggedMember, invoiceNumber); //ziyad
                    }
                    accountTransaction.Complete();
                }
                Dictionary<int, decimal> agencyLccBal = (Dictionary<int, decimal>)CT.MemCache.Cache.Get("AgencyLccBal");
                if (agencyLccBal != null)
                {
                    agencyLccBal.Remove(Convert.ToInt32(agency.ID));
                    CT.MemCache.Cache.Replace("AgencyLccBal", (object)agencyLccBal);
                }
            }
        }
        bookingResponse.BookingId = booking.BookingId;
        return bookingResponse;
    }

    private void SaveTicketInfo()
    {
        //Utility.UpdateAirlinePNR(itinerary);
        FlightItinerary.GetMemberPrefServiceFee(Convert.ToInt32(agency.ID), ref sFeeTypeList, ref sFeeValueList);

        CT.Core.Airline air = new CT.Core.Airline();
        air.Load(itinerary.ValidatingAirlineCode);
        if (itinerary.FlightBookingSource == BookingSource.WorldSpan || itinerary.FlightBookingSource == BookingSource.Amadeus || itinerary.FlightBookingSource == BookingSource.UAPI || itinerary.FlightBookingSource == BookingSource.TBOAir || itinerary.FlightBookingSource == BookingSource.PKFares)
        {
            for (int i = 0; i < ticket.Length; i++)
            {
                if (Request["txtcreateddate"] != null)
                {
                    string issueDate = Request["txtcreateddate"] + " " + DateTime.Now.ToString("HH:mm:ss");
                    DateTime issue = DateTime.ParseExact(issueDate, "dd/MM/yyyy HH:mm:ss", null);
                    ticket[i].IssueDate = Util.ISTToUTC(issue);
                }
                if (ticket[i].PaxKey == itinerary.Passenger[i].PaxKey)
                {
                    ticket[i].SupplierRemmitance = new SupplierRemmitance();
                    ticket[i].SupplierRemmitance.CommissionType = air.CommissionType;
                    SaveTicket(i, i);
                }
                else
                {
                    for (int j = 0; j < itinerary.Passenger.Length; j++)
                    {
                        if (ticket[i].PaxKey == itinerary.Passenger[j].PaxKey)
                        {
                            ticket[j].ValidatingAriline = itinerary.ValidatingAirline;
                            ticket[j].TaxBreakup = new List<KeyValuePair<string, decimal>>();
                            ticket[j].TaxBreakup.Add(new KeyValuePair<string, decimal>("TotalTax", itinerary.Passenger[j].Price.Tax));

                            ticket[i].SupplierRemmitance = new SupplierRemmitance();
                            ticket[i].SupplierRemmitance.CommissionType = air.CommissionType;

                            SaveTicket(i, j);
                            break;
                        }
                    }
                }
            }
        }
        else if (itinerary.IsLCC || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.GoAir  || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.HermesAirLine || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp)
        {
            ticket = new Ticket[itinerary.Passenger.Length];
            for (int j = 0; j < itinerary.Passenger.Length; j++)
            {
                ticket[j] = new Ticket();
                if (itinerary.FlightBookingSource != BookingSource.HermesAirLine && itinerary.FlightBookingSource != BookingSource.Paramount && itinerary.FlightBookingSource != BookingSource.Mdlr)
                {
                    ticket[j].TicketNumber = itinerary.PNR;
                }
                if (Request["txtcreateddate"] != null)
                {
                    string issueDate = Request["txtcreateddate"] + " " + DateTime.Now.ToString("HH:mm:ss");
                    DateTime issue = DateTime.ParseExact(issueDate, "dd/MM/yyyy HH:mm:ss", null);
                    ticket[j].IssueDate = Util.ISTToUTC(issue);
                }
                else
                {
                    ticket[j].IssueDate = DateTime.UtcNow;
                }
                ticket[j].ValidatingAriline = itinerary.ValidatingAirline;
                ticket[j].TaxBreakup = new List<KeyValuePair<string, decimal>>();
                ticket[j].TaxBreakup.Add(new KeyValuePair<string, decimal>("TotalTax", itinerary.Passenger[j].Price.Tax));

                ticket[j].SupplierRemmitance = new SupplierRemmitance();
                ticket[j].SupplierRemmitance.CommissionType = air.CommissionType;
                SaveTicket(j, j);
            }
        }
    }

    private void SaveTicket(int ticketIndex, int paxIndex)
    {
        UserPreference preference = new UserPreference();

        int i = ticketIndex;
        int j = paxIndex;
        string corporateCode = string.Empty;
        string tourCode = string.Empty;
        string endorsement = string.Empty;
        string remarks = string.Empty;
        if (Request["corporateCode"] != null)
        {
            corporateCode = Request["corporateCode"].ToString();
        }
        if (Request["tourCode"] != null)
        {
            tourCode = Request["tourCode"].ToString();
        }
        if (Request["endorsement"] != null)
        {
            endorsement = Request["endorsement"].ToString();
        }
        if (Request["remarks"] != null)
        {
            remarks = Request["remarks"].ToString();
        }

        ticket[i].Status = "OK";
        ticket[i].CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
        ticket[i].PaxId = itinerary.Passenger[j].PaxId;
        ticket[i].PaxType = itinerary.Passenger[j].Type;
        string fareRule = string.Empty;
        List<string> airlineNameRemarksList = new List<string>();
        List<string> airlineNameFareRuleList = new List<string>();
        foreach (FlightInfo segment in itinerary.Segments)
        {
            string airlineCode = segment.Airline;
            Airline airline = new Airline();
            airline.Load(airlineCode);
            if (!airlineNameRemarksList.Contains(airline.AirlineName) && airline.Remarks.Length != 0)
            {
                airlineNameRemarksList.Add(airline.AirlineName);
            }
            if (!airlineNameFareRuleList.Contains(airline.AirlineName) && airline.FareRule.Length != 0)
            {
                airlineNameFareRuleList.Add(airline.AirlineName);
                fareRule += "<span style=\"font-weight:bold\">" + airline.AirlineName + "</span>" + "&nbsp;:&nbsp;" + airline.FareRule + "<br />";
            }
            if (segment.Airline == itinerary.ValidatingAirlineCode)
            {
                ticket[i].SupplierId = airline.ConsolidatorId;
            }
        }
        ticket[i].Remarks = remarks;
        ticket[i].FareRule = fareRule;


        if (itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.HermesAirLine)
        {
            ticket[i].TicketNumber = ticketNumbers[i];
        }
        ticket[i].Title = itinerary.Passenger[j].Title;
        ticket[i].PaxFirstName = itinerary.Passenger[j].FirstName;
        ticket[i].PaxLastName = itinerary.Passenger[j].LastName;

        ticket[i].FlightId = itinerary.FlightId;
        int paxIndexPricing = i;

        ticket[i].Price = AccountingEngine.GetPrice(itinerary, booking.AgencyId, paxIndexPricing, 0);

        ticket[i].Price.PriceId = itinerary.Passenger[j].Price.PriceId;
        ticket[i].ETicket = true;
        if (ticket[i].IssueInExchange == null)
        {
            ticket[i].IssueInExchange = string.Empty;
        }
        ticket[i].TourCode = tourCode;
        if (ticket[i].OriginalIssue == null)
        {
            ticket[i].OriginalIssue = string.Empty;
        }
        ticket[i].Endorsement = endorsement;
        ticket[i].CorporateCode = corporateCode;
        if (ticket[i].FareCalculation == null)
        {
            ticket[i].FareCalculation = string.Empty;
        }
        //int agencyPrimaryMemberId = Member.GetPrimaryMemberId(booking.AgencyId); //TODO Ziyad
        string sfType = string.Empty;
        decimal sfValue = 0;

        #region Old commented code
        //if (!itinerary.CheckDomestic("" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + ""))
        //if (!itinerary.IsDomestic)
        //if(true)
        //{
        //    if (sFeeTypeList.ContainsKey("INTL") && sFeeValueList.ContainsKey("INTL"))
        //    {
        //        sfType = sFeeTypeList["INTL"];
        //        sfValue = Convert.ToDecimal(sFeeValueList["INTL"]);
        //    }
        //}
        //else if (sFeeTypeList.ContainsKey(itinerary.ValidatingAirlineCode) && sFeeValueList.ContainsKey(itinerary.ValidatingAirlineCode))
        //{
        //    sfType = sFeeTypeList[itinerary.ValidatingAirlineCode];
        //    sfValue = Convert.ToDecimal(sFeeValueList[itinerary.ValidatingAirlineCode]);
        //}
        #endregion

        if (sfType == "FIXED")
        {
            ticket[i].ServiceFee = sfValue;
        }
        else if (sfType == "PERCENTAGE")
        {
            decimal totalfare = ticket[i].Price.PublishedFare + ticket[i].Price.Tax + ticket[i].Price.OtherCharges + ticket[i].Price.AdditionalTxnFee + ticket[i].Price.TransactionFee;
            ticket[i].ServiceFee = totalfare * sfValue / 100;
        }
        else
        {
            ticket[i].ServiceFee = 0;
        }

        #region Old commented code
        //preference = preference.GetPreference(agencyPrimaryMemberId, UserPreference.ServiceFee, UserPreference.ServiceFee); // this block TODO Ziyad
        //if (preference.Value != null)
        //{
        //    if (preference.Value == UserPreference.ServiceFeeInTax)
        //    {
        //        ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowInTax;
        //    }
        //    else if (preference.Value == UserPreference.ServiceFeeShow)
        //    {
        //        ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowSeparately;
        //    }
        //}
        //else
        //{
        //    ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowInTax;
        //}
        #endregion

        ticket[i].SupplierRemmitance.PublishedFare = itinerary.Passenger[j].Price.PublishedFare;
        ticket[i].SupplierRemmitance.AccpriceType = itinerary.Passenger[j].Price.AccPriceType;
        ticket[i].SupplierRemmitance.NetFare = itinerary.Passenger[j].Price.NetFare;
        ticket[i].SupplierRemmitance.CessTax = 0;
        ticket[i].SupplierRemmitance.Currency = itinerary.Passenger[j].Price.Currency;
        ticket[i].SupplierRemmitance.CurrencyCode = itinerary.Passenger[j].Price.CurrencyCode;
        ticket[i].SupplierRemmitance.Markup = itinerary.Passenger[j].Price.Markup;
        ticket[i].SupplierRemmitance.OtherCharges = itinerary.Passenger[j].Price.OtherCharges;
        ticket[i].SupplierRemmitance.OurCommission = itinerary.Passenger[j].Price.OurCommission;
        ticket[i].SupplierRemmitance.OurPlb = itinerary.Passenger[j].Price.OurPLB;
        ticket[i].SupplierRemmitance.RateOfExchange = itinerary.Passenger[j].Price.RateOfExchange;
        ticket[i].SupplierRemmitance.ServiceTax = itinerary.Passenger[j].Price.SeviceTax;
        ticket[i].SupplierRemmitance.Tax = itinerary.Passenger[j].Price.Tax;
        ticket[i].SupplierRemmitance.TdsCommission = 0;
        ticket[i].SupplierRemmitance.TdsPLB = 0;
        ticket[i].SupplierRemmitance.CreatedBy = ticket[j].CreatedBy;
        ticket[i].SupplierRemmitance.CreatedOn = DateTime.UtcNow;
        //ticket[i].SupplierRemmitance.CommissionType = air.CommissionType;
        ticket[i].ValidatingAriline = itinerary.ValidatingAirline;
        if (ticket[i].SupplierId > 0)
        {
            ticket[i].SupplierRemmitance.SupplierCode = Supplier.GetSupplierCodeById(ticket[i].SupplierId);
        }
        ticket[i].Save();
        ticket[i].SupplierRemmitance.TicketId = ticket[i].TicketId;
        ticket[i].SupplierRemmitance.Save();

        DateTime issueDate = new DateTime();
        if (Request["txtcreateddate"] != null)
        {
            issueDate = DateTime.ParseExact(Request["txtcreateddate"], "dd/MM/yyyy", null);
        }
        BookingHistory bh = new BookingHistory();
        bh.BookingId = booking.BookingId;
        bh.EventCategory = EventCategory.Ticketing;

        if (itinerary.PNR != null && itinerary.PNR.Length > 0 && FailedBooking.CheckPNR(itinerary.PNR))
        {
            if (issueDate.Date == DateTime.Now.Date)
            {
                bh.Remarks = "Ticket Created";
            }
            else
            {
                bh.Remarks = "Ticket created and the invoice date is changed";
            }
        }
        else
        {
            if (issueDate.Date == DateTime.Now.Date)
            {
                bh.Remarks = "Booking and ticket created via import pnr";
            }
            else
            {
                bh.Remarks = "Booking and ticket created via import pnr and the invoice date is changed";
            }
        }
        bh.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
        bh.Save();
        if (itinerary.FlightBookingSource == BookingSource.UAPI || itinerary.FlightBookingSource == BookingSource.TBOAir || itinerary.FlightBookingSource == BookingSource.WorldSpan || itinerary.FlightBookingSource == BookingSource.Amadeus || itinerary.FlightBookingSource == BookingSource.Galileo)
        {
            for (int f = 0; f < itinerary.Segments.Length; f++)
            {
                for (int p = 0; p < ticket[i].PtcDetail.Count; p++)
                {
                    if (ticket[i].PtcDetail[p].FlightKey == itinerary.Segments[f].FlightKey)
                    {
                        ticket[i].PtcDetail[p].SegmentId = itinerary.Segments[f].SegmentId;
                        ticket[i].PtcDetail[p].PaxType = FlightPassenger.GetPTC(ticket[i].PaxType);
                        ticket[i].PtcDetail[p].Save();
                        break;
                    }
                }
            }
        }
        #region Old commented code
        //else if (itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.HermesAirLine)
        //{
        //    ticket[i].PtcDetail = new List<SegmentPTCDetail>();
        //    for (int f = 0; f < itinerary.Segments.Length; f++)
        //    {
        //        string baggage = ConfigurationSystem.SpiceJetConfig["Baggage"];
        //        ticket[i].PtcDetail.Add(new SegmentPTCDetail());
        //        ticket[i].PtcDetail[f].SegmentId = itinerary.Segments[f].SegmentId;
        //        if (itinerary.FlightBookingSource == BookingSource.Paramount)
        //        {
        //            baggage = ConfigurationSystem.ParamountConfig["baggage"];
        //        }
        //        if (itinerary.FlightBookingSource == BookingSource.AirDeccan)
        //        {
        //            baggage = ConfigurationSystem.AirDeccanConfig["Baggage"];
        //        }
        //        if (itinerary.FlightBookingSource == BookingSource.Mdlr)
        //        {
        //            baggage = ConfigurationSystem.MdlrConfig["baggage"];
        //        }
        //        if (itinerary.FlightBookingSource == BookingSource.GoAir)
        //        {
        //            baggage = ConfigurationSystem.GoAirConfig["Baggage"];
        //        }
        //        if (itinerary.FlightBookingSource == BookingSource.Sama)
        //        {
        //            baggage = ConfigurationSystem.SamaConfig["baggage"];
        //        }
        //        if (itinerary.FlightBookingSource == BookingSource.HermesAirLine)
        //        {
        //            baggage = ConfigurationSystem.HermesConfig["Baggage"];
        //        }
        //        // Picking LCC baggage information from config
        //        ticket[i].PtcDetail[f].Baggage = baggage;
        //        ticket[i].PtcDetail[f].PaxType = FlightPassenger.GetPTC(ticket[i].PaxType);
        //        ticket[i].PtcDetail[f].FareBasis = itinerary.FareRules[0].FareBasisCode;
        //        ticket[i].PtcDetail[f].Save();
        //    }
        //}
        #endregion
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            
            UseCredit_Click();
            Session["itinerary"] = null;
        }
        catch (Exception ex)
        {
            errorMessage = ex.Message.ToString();
            Audit.Add(EventType.PNRRefresh, Severity.High, 1, "Failed to Save Booking for Import PNR. " + ex.ToString(), Request["REMOTE_ADDR"]);
        }

    }
   

    protected string SendFlightTicketEmail()
    {
        string myPageHTML = string.Empty;
        try
        {
            itinerary = Session["itinerary"] as FlightItinerary;
            string logoPath = "";
            int agentId = 0;

            if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agentId = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.OnBehalfAgentID;
            }
            else
            {
                agentId = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId;
            }

            string serverPath = "";

            if (Request.Url.Port > 0)
            {
                serverPath = Request.Url.Scheme+"://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }
            else
            {
                serverPath =Request.Url.Scheme+ "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }

            if (agentId > 1)
            {
                logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"] + new AgentMaster(agentId).ImgFileName;
                imgLogo.ImageUrl = logoPath;
            }
            else
            {
                imgLogo.ImageUrl = serverPath + "/images/logo.jpg";
            }



            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            EmailDiv.RenderControl(htw);
            myPageHTML = sw.ToString();
            //myPageHTML = myPageHTML.Replace("\"", "/\"");
            myPageHTML = myPageHTML.Replace("none", "block");

            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            toArray.Add(itinerary.Passenger[0].Email);

            string subject = "Air Reservation - " + itinerary.PNR;
            ticket = Ticket.GetTicketList(itinerary.FlightId).ToArray();
            if (ticket != null && ticket.Length > 0)
            {
                subject = "Ticket Confirmation - " + itinerary.PNR;
            }
            AgentMaster agency = new AgentMaster(itinerary.AgencyId);
            string bccEmails = string.Empty;
            if (!string.IsNullOrEmpty(agency.Email1))
            {
                bccEmails = agency.Email1;
            }
            if (!string.IsNullOrEmpty(agency.Email2))
            {
                bccEmails = bccEmails + "," + agency.Email2;
            }
            if (ViewState["MailSent"] == null)//If booking in not corporate then send email
            {
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable(), bccEmails);
                ViewState["MailSent"] = true;
            }
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to Send Flight E-ticket Email PNR " + itinerary.PNR + ": reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        return myPageHTML;
    }
}
