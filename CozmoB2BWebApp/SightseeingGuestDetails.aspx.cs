﻿using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using CT.Core;
using CT.MetaSearchEngine;
using CT.ActivityDeals;
using System.Linq;

public partial class SightseeingGuestDetails : CT.Core.ParentPage //System.Web.UI.Page
{
    protected string cancelData = "", amendmentData = "", Notes="";
    protected string[] additionalInfo;
    protected SightSeeingReguest request = new SightSeeingReguest();
    protected SightseeingSearchResult[] filteredResultList;
    protected SightseeingSearchResult searchResult;
    protected SightseeingStaticData staticInfo;
    protected SightseeingItinerary itinerary = new SightseeingItinerary();
    protected TourOperation tourOperation;
    protected string sightseeingCode;
    protected int tourId;
    protected string errorMessage = string.Empty;
    protected AgentMaster agency;
    protected decimal rateofExchange = 1;
    protected decimal totalPrice = 0, markup = 0;
    string sessionId = "";
    int agencyId = 0;
    protected bool isHotelChooseDepPoint = false;
    protected Dictionary<string, string> depPointLst = new Dictionary<string, string>();
    HotelRequest req = new HotelRequest(); //make request For get Hotels Name and theri code
    protected HotelSearchResult[] hotelResults = new HotelSearchResult[0]; // Get all hotel information
    protected Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
    protected Dictionary<string, string> CancellationData = new Dictionary<string, string>();
    protected string warningMsg = "";
    protected string userIPAddress = "10.200.44.29";
    protected LocationMaster location = new LocationMaster();
    protected CT.ActivityDeals.Activity activity=new CT.ActivityDeals.Activity();
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {           
            if (Session["cSessionId"] == null || Session["ssReq"] == null || Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", false);
            }
            else
            {
                sessionId = Session["cSessionId"].ToString();
            }
            if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                
                agency = new AgentMaster(Settings.LoginInfo.AgentId);
                location = new LocationMaster(Settings.LoginInfo.LocationID);
            }
            else
            {
                
                agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
            }

            if (Request.QueryString["tourId"] != null && Request.QueryString["itemCode"] != null)
            {
                tourId = Convert.ToInt32(Request.QueryString["tourId"]);
                sightseeingCode = Request.QueryString["itemCode"].ToString();
                
            }

            if (!IsPostBack)
            {
                try
                {
                    if (Session["searchResult"] != null && Request.QueryString["itemCode"] != null && Request.QueryString["tourId"] != null)
                    {
                        
                        
                        filteredResultList = (SightseeingSearchResult[])Session["searchResult"];
                        for (int i = 0; i < filteredResultList.Length; i++)
                        {
                            if (filteredResultList[i].ItemCode == sightseeingCode)
                            {
                                searchResult = filteredResultList[i];
                                break;
                            }
                        }
                        tourOperation = searchResult.DayWisetourOperationList[0].tourOperationList[tourId];
                       
                        
                    }
                    else
                    {
                        Response.Redirect("AbandonSession.aspx");
                    }
                    
                   
                   
                    if (Session["cSessionId"] == null)
                    {
                        errorMessage = "Your Session is expired !! Please" + "<a href=\"Sightseeing.aspx\"" + "\">" + " Retry" + "</a>.";
                        Response.Redirect("Sightseeing.aspx");
                    }
                    else
                    {
                        request = (SightSeeingReguest)Session["ssReq"];
                        LoadSightseeingItinerary();
                    }



                    //CALL MSE METHODS TO GET CANCELLATION DETAILS & SIGHTSEEING DETAILS
                   // MetaSearchEngine mse = new MetaSearchEngine(sessionId);
                    try
                    {
                        LoadCancellationDetails(ref itinerary);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SightseeingBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get SightseeingItinerary. Error: " + ex.Message, "0");
                    }
                    
                   
                    Session["SItinerary"] = itinerary;

                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SightseeingBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get SightseeingItinerary. Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
                }
            }
            imgContinue.Click += new EventHandler(imgContinue_Click);
            if (Session["searchResult"] != null)
            {
                Session["filteredResultList"] = Session["searchResult"];
            }
           

           
            if (Session["cSessionId"] == null || Session["ssReq"] == null || Session["SItinerary"] == null || Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            else
            {
                Audit.Add(EventType.SightseeingBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Get SightseeingItinerary Start", "0");
                try
                {
                   // sessionId = Request["cSessionId"];
                    itinerary = Session["SItinerary"] as SightseeingItinerary;

                    // Calculation Price
                    decimal total = 0;

                    if (itinerary.Price.AccPriceType == PriceType.NetFare)
                    {
                        total = Convert.ToDecimal(itinerary.Price.NetFare + itinerary.Price.Markup)* rateofExchange;

                    }
                    

                    //Use ceiling instead of Round, Changed on 05082016
                        lblCost.Text = Math.Ceiling(total).ToString("N"+Settings.LoginInfo.DecimalValue);
                    //Add Markup Price entered by the user
                    if (txtMarkup.Text.Trim().Length > 0)
                    {
                        decimal _markup = 0, percent = 0, fix = 0, mtotal = total;
                        string asvType = "";
                        //If P is selected, calculate the percent amount from the total and add it.
                        //If F is selected, directly add the price to the total
                        if (rbtnPercent.Checked)
                        {
                            try
                            {
                                percent = Convert.ToDecimal(txtMarkup.Text);
                            }
                            catch { }
                            asvType = "P";
                            _markup = ((mtotal) * (percent / 100));
                            mtotal = total + _markup;
                        }
                        else if (rbtnFixed.Checked)
                        {
                            try
                            {
                                fix = Convert.ToDecimal(txtMarkup.Text);
                            }
                            catch { }
                            asvType = "F";
                            _markup = fix;
                            mtotal = _markup + total;
                        }
                       
                        this.markup = _markup;
                        Session["Markup"] = _markup;


                        //Use ceiling instead of Round, Changed on 05082016
                        lblTotal.Text = Math.Ceiling(mtotal).ToString("N"+Settings.LoginInfo.DecimalValue);

                       
                            if (itinerary.Price.AccPriceType == PriceType.NetFare)
                            {
                                itinerary.Price.AsvAmount = markup;
                                itinerary.Price.AsvType = asvType;
                                itinerary.Price.AsvElement = txtMarkup.Text;
                                //save addl markup for first room only
                            }
                        
                    }
                    else
                    {
                        //Use ceiling instead of Round, Changed on 05082016
                        lblTotal.Text = Math.Ceiling(total).ToString("N" + Settings.LoginInfo.DecimalValue);
                    }
                    lblPrice.Text = lblTotal.Text;
                   
                    //Load additional Passenger Grid to fill details
                    BindHotels();
                    activity =new CT.ActivityDeals.Activity(Convert.ToInt32(sightseeingCode));
                    BindOtherPassengers();
                    
                    //Audit.Add(EventType.SightseeingBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Get SightseeingItinerary End", "0");
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SightseeingBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get SightseeingItinerary. Error: " + ex.Message, "0");
                }
            }

            
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.SightseeingBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Add Guest Details. Error: " + ex.Message, "0");

        }
    }

    void LoadSightseeingItinerary()
    {
        if (!Settings.LoginInfo.IsOnBehalfOfAgent)
        {

            agency = new AgentMaster(Settings.LoginInfo.AgentId);
            location = new LocationMaster(Settings.LoginInfo.LocationID);
        }
        else
        {

            agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
            location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
        }

        if (Session["cSessionId"] == null || Session["ssReq"] == null || searchResult ==null)
        {
            Response.Redirect("AbandonSession.aspx");
        }

        else
        {

            itinerary.AdultCount = request.NoOfAdults;
            //itinerary.CancellationPolicy = request.cancelPolicy;
            itinerary.ChildAge = request.ChildrenList;
            itinerary.ChildCount = request.ChildCount;
            itinerary.CityCode = searchResult.CityCode;
            itinerary.CityName = searchResult.CityName;
            //modified by brahmam need to assign AgentCurrency 
            itinerary.Currency = tourOperation.PriceInfo.Currency;
            itinerary.DepPointInfo = string.Empty;
            itinerary.DepTime = string.Empty;
            itinerary.Duration = searchResult.Duration;
            itinerary.ItemCode = searchResult.ItemCode;
            itinerary.IsDomestic = false;
            itinerary.ItemName = searchResult.ItemName;
            //itinerary.Language = tourOperation.LangName[0];//request.Language;
            itinerary.Price = tourOperation.PriceInfo;
           
            itinerary.Price.AccPriceType = PriceType.NetFare;
            
            if (searchResult.Source == SightseeingBookingSource.GTA)
            {
                itinerary.Source = SightseeingBookingSource.GTA;
            }
            if (searchResult.Source == SightseeingBookingSource.CZA)
            {
                itinerary.Source = SightseeingBookingSource.CZA;
            }
            //itinerary.SpecialCode = request.SpecialCode;
            itinerary.TourDate = request.TourDate;
            itinerary.CreatedBy =(int) Settings.LoginInfo.UserID; ;
            itinerary.TransType = "B2B";
            itinerary.LastCancellationDate = request.TourDate; //we can add buffer days also
            itinerary.CancelId = "";

            itinerary.AgentId = (int)agency.ID;
            itinerary.LocationId = (int)location.ID;

           // itinerary.SpecialName = tourOperation.SpecialItemName;
            itinerary.CountryName = request.CountryName;

            //if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            //{
            //    itinerary.AgentId = agencyId;
            //}
            //else
            //{
            //    itinerary.AgentId = agencyId;  // on behalf of booking passing location id as -2
                
            //    itinerary.LocationId =(int)location.ID;
            //}

            itinerary.IsDomestic = false;

            //if (Session["searchResult"] != null && Session["ssReq"] != null)
            //{
            //    filteredResultList = (SightseeingSearchResult[])Session["searchResult"];

            //    for (int i = 0; i < filteredResultList.Length; i++)
            //    {
            //        if (filteredResultList[i].ItemCode == sightseeingCode)
            //        {
            //            searchResult = filteredResultList[i];
            //            break;
            //        }
            //    }

            //searchResult = filteredResultList[0];
            if (searchResult.DayWisetourOperationList[0].departurePointList.Count > 0)
            {
                for (int i = 0; i < searchResult.DayWisetourOperationList[0].departurePointList.Count; i++)
                {
                    itinerary.DepPointInfo = itinerary.DepPointInfo +',' + searchResult.DayWisetourOperationList[0].departurePointList[i].ToString();
                    
                }


            }
            tourOperation = searchResult.DayWisetourOperationList[0].tourOperationList[tourId];
            request = (SightSeeingReguest)Session["ssReq"];
           
           
                //Language

                if (tourOperation.LangName.Count > 1)
                {

                    bool disLanguage = false;
                    for (int i = 0; i < tourOperation.LangName.Count; i++)
                    {
                        if (tourOperation.LangName[i] == request.Language)
                        {
                            itinerary.Language = request.Language + "#" + tourOperation.LanguageCode[i];
                            disLanguage = true;
                        }
                    }
                    if (!disLanguage)
                    {
                        if (tourOperation.LangName[0].Substring(0, 1) != "U")
                        {
                            itinerary.Language = tourOperation.LangName[0] + "#" + tourOperation.LanguageCode[0];
                        }
                        else
                        {
                            itinerary.Language = tourOperation.LangName[0];
                        }
                    }
                   
                }

                else if (tourOperation.LangName.Count == 1)
                {
                    if (tourOperation.LangName[0].Substring(0, 1) != "U")
                    {
                        itinerary.Language = tourOperation.LangName[0] + "#" + tourOperation.LanguageCode[0];
                    }
                    else
                    {
                        itinerary.Language = tourOperation.LangName[0];
                    }
                }
                if (itinerary.Language != null && itinerary.Language.Length > 0 && tourOperation.TourLanguageList!=null && tourOperation.TourLanguageList.Count > 0)
                {
                    itinerary.Language += "|" + tourOperation.TourLanguageList[0];
                }


                string spCode = string.Empty;
                string spName = string.Empty;
                if (tourOperation.SpecialItemList!=null && tourOperation.SpecialItemList.Count > 0)
                {
                    foreach (string code in tourOperation.SpecialItemList)
                    {
                        spCode += code + "|";
                    }
                }
                itinerary.SpecialCode = spCode;
                itinerary.SpecialName = tourOperation.SpecialItemName;
            //}
            
            //CALL MSE METHODS TO GET CANCELLATION DETAILS & Sightseeing DETAILS
        }
    }

    void LoadCancellationDetails(ref SightseeingItinerary itinerary)
    {
        /* Unnecessary code
        MetaSearchEngine mse = new MetaSearchEngine(sessionId);
        mse.SettingsLoginInfo = Settings.LoginInfo;
          */
        try
        {
            request = (SightSeeingReguest)Session["ssReq"];
            if (itinerary.Source == SightseeingBookingSource.GTA)
            {
               

                CT.BookingEngine.GDS.GTA gta = new CT.BookingEngine.GDS.GTA();
                gta.AgentCurrency = Settings.LoginInfo.Currency;
                gta.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                gta.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
                CancellationData = gta.GetSightseeingChargeCondition(request, searchResult, tourId);
                
               //For Notes
                staticInfo = gta.GetSightseeingItemInformation(itinerary.CityCode, itinerary.ItemName, itinerary.ItemCode);
                Notes = staticInfo.Notes;
                additionalInfo = searchResult.AdditionalInformationList;
                string addInfo = "";
                if (additionalInfo != null && additionalInfo.Length>0)
                {
                    foreach (string data in additionalInfo)
                    {
                        if (string.IsNullOrEmpty(addInfo))
                        {
                            addInfo = data;
                        }
                        else
                        {
                            addInfo += "|" + data;
                        }
                    }
                   
                }
                itinerary.Note =Server.HtmlDecode( Notes + "#" + addInfo);
                itinerary.TelePhno = staticInfo.TourOperationInfo[0].Telephone;
                itinerary.Address = staticInfo.TourOperationInfo[0].Address;
                foreach (KeyValuePair<string, string> pair in CancellationData)
                {
                    switch (pair.Key)
                    {

                        case "CancelPolicy":
                            cancelData = pair.Value;
                            itinerary.CancellationPolicy =Server.HtmlDecode(cancelData);
                            break;
                        case "AmendmentPolicy":
                            amendmentData = pair.Value;
                            itinerary.CancellationPolicy += Server.HtmlDecode( "@" + amendmentData);
                            break;


                    }
                }
            }
            if (itinerary.Source == SightseeingBookingSource.CZA)
            {
                CZActivity.Activity activity = new CZActivity.Activity();
                
                    activity.AgentCurrency = Settings.LoginInfo.Currency;
                    activity.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                    activity.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
                   
                
                staticInfo = new SightseeingStaticData();
                activity.GetSightseeingItemInformation(itinerary.ItemCode, userIPAddress, ref staticInfo);
                Notes = staticInfo.Notes;
                additionalInfo = searchResult.AdditionalInformationList;
                string addInfo = "";
                if (additionalInfo != null && additionalInfo.Length > 0)
                {
                    foreach (string data in additionalInfo)
                    {
                        if (string.IsNullOrEmpty(addInfo))
                        {
                            addInfo = data;
                        }
                        else
                        {
                            addInfo += "|" + data;
                        }
                    }

                }
                itinerary.Note =Server.HtmlDecode( Notes + "#" + addInfo);
                itinerary.TelePhno = staticInfo.TourOperationInfo[0].Telephone;
                itinerary.Address = staticInfo.TourOperationInfo[0].Address;
                itinerary.CancellationPolicy = Server.HtmlDecode( staticInfo.CancellationPolicy);
            }

        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.SightseeingBooking, CT.Core.Severity.High, Convert.ToInt32(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID), "Failed to get Cancellation details(SightseeingReview). Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }


    }

    void BindOtherPassengers()
    {
       // int totalPax = request.NoOfAdults + request.ChildCount;

        //for (int j = 0; j < totalPax; j++)
        //{
        for (int i = 0; i < itinerary.AdultCount; i++)
        {
            HtmlTableRow hRow = new HtmlTableRow();
            HtmlTableCell hCell = new HtmlTableCell();
            if (i == 0)
            {

                hCell.InnerHtml = "<b> Adult" + (i + 1).ToString() + " (Lead Guest) (Name As on passport)</b>";
                hCell.Attributes.Add("class", "subgray-header");
            }
            else
            {
                hCell.InnerHtml += "<b>" + " Adult " + (i + 1).ToString() + " (Name As on passport)</b>";
                hCell.Attributes.Add("class", "subgray-header");
            }
            hCell.ColSpan = 8;

            hRow.Cells.Add(hCell);

            HtmlTableRow detailRow = new HtmlTableRow();
            HtmlTableCell prefix = new HtmlTableCell();

            HtmlGenericControl divlblTitle = new HtmlGenericControl("Div");
            divlblTitle.Attributes.Add("class", "col-md-3");
            divlblTitle.InnerHtml = "<div class='col-md-3'>Title:</div>";

            HtmlGenericControl divDdlTitle = new HtmlGenericControl("Div"); // For Adult Div
            divDdlTitle.Attributes.Add("class", "col-md-3"); // For Adult Div class attribute
            DropDownList ddlTitle = new DropDownList();
            ddlTitle.Items.Add(new ListItem("Select", "Select"));
            ddlTitle.Items.Add(new ListItem("Mr.", "Mr."));
            ddlTitle.Items.Add(new ListItem("Ms.", "Ms."));
            //ddlTitle.Items.Add(new ListItem("Dr.", "Dr.")); //Commented By Chandan on 09012016
            ddlTitle.SelectedIndex = 0;
            ddlTitle.ID = "ddlATitle" + (i + 1).ToString();
            ddlTitle.CausesValidation = true;
            ddlTitle.CssClass = "form-control";
            ddlTitle.ValidationGroup = "pax";

            divDdlTitle.Controls.Add(ddlTitle);

            RequiredFieldValidator prefixvc = new RequiredFieldValidator();
            prefixvc.ControlToValidate = ddlTitle.ID;
            prefixvc.ErrorMessage = "Select&nbsp;Title";
            prefixvc.ValidationGroup = "pax";
            prefixvc.InitialValue = "Select";
            prefixvc.SetFocusOnError = true;

            divDdlTitle.Controls.Add(prefixvc);
            //prefix.Attributes.Add("style", "vertical-align:sub");
            prefix.Controls.Add(divlblTitle);
            prefix.Controls.Add(divDdlTitle);

            HtmlGenericControl divlblfName = new HtmlGenericControl("Div"); // For Adult First Name
            divlblfName.Attributes.Add("class", "col-md-3");
            divlblfName.InnerHtml = "<div  class='' >First Name:</Div>";

            HtmlGenericControl divTxtfName = new HtmlGenericControl("Div"); // For Adult First Name
            divTxtfName.Attributes.Add("class", "col-md-3");

            //HtmlTableCell fname = new HtmlTableCell();
            //fname.InnerText = "First Name:";
            TextBox txtFirstName = new TextBox();
            txtFirstName.ID = "txtAFirstName" + (i + 1).ToString();
            txtFirstName.CssClass = "form-control";
            txtFirstName.CausesValidation = true;
            txtFirstName.MaxLength = 10;
            txtFirstName.ValidationGroup = "pax";
            txtFirstName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
            txtFirstName.Attributes.Add("ondrop", "return false;");
            txtFirstName.Attributes.Add("onpaste", "return false;");
            divTxtfName.Controls.Add(txtFirstName);

            RequiredFieldValidator fnamevc = new RequiredFieldValidator();
            fnamevc.ControlToValidate = txtFirstName.ID;
            fnamevc.ErrorMessage = "Enter&nbsp;First&nbsp;Name";
            fnamevc.Display = ValidatorDisplay.Dynamic;
            fnamevc.InitialValue = "";
            fnamevc.SetFocusOnError = true;
            fnamevc.ValidationGroup = "pax";
            divTxtfName.Controls.Add(fnamevc);

            CustomValidator fnameValidator = new CustomValidator();
            fnameValidator.ControlToValidate = txtFirstName.ID;
            fnameValidator.ClientValidationFunction = "validateName";
            fnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
            fnameValidator.SetFocusOnError = true;
            fnameValidator.ValidationGroup = "pax";
            divTxtfName.Controls.Add(fnameValidator);

            prefix.Controls.Add(divlblfName);
            prefix.Controls.Add(divTxtfName);


            //Adult Last Name
            HtmlGenericControl divlbllName = new HtmlGenericControl("Div"); // For Adult Last Name
            divlbllName.Attributes.Add("class", "col-md-3");
            divlbllName.InnerHtml = "<div class='col-md-3'>Last&nbsp;Name:</Div>";

            //HtmlTableCell lname = new HtmlTableCell();
            //lname.InnerText = "Last Name:";
            HtmlGenericControl divtxtlName = new HtmlGenericControl("Div"); // For Adult Last Name
            divtxtlName.Attributes.Add("class", "col-md-3");

            TextBox txtLastName = new TextBox();
            txtLastName.ID = "txtALastName" + (i + 1).ToString();
            txtLastName.CssClass = "form-control";
            txtLastName.CausesValidation = true;
            txtLastName.MaxLength = 15;
            txtLastName.ValidationGroup = "pax";
            txtLastName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
            txtLastName.Attributes.Add("ondrop", "return false;");
            txtLastName.Attributes.Add("onpaste", "return false;");
            divtxtlName.Controls.Add(txtLastName);

            RequiredFieldValidator lnamevc = new RequiredFieldValidator();
            lnamevc.ControlToValidate = txtLastName.ID;
            lnamevc.ErrorMessage = "Enter&nbsp;Last&nbsp;Name";
            lnamevc.Display = ValidatorDisplay.Dynamic;
            lnamevc.InitialValue = "";
            lnamevc.SetFocusOnError = true;
            lnamevc.ValidationGroup = "pax";
            divtxtlName.Controls.Add(lnamevc);

            CustomValidator lnameValidator = new CustomValidator();
            lnameValidator.ControlToValidate = txtLastName.ID;
            lnameValidator.ClientValidationFunction = "validateName";
            lnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
            lnameValidator.SetFocusOnError = true;
            lnameValidator.ValidationGroup = "pax";
            divtxtlName.Controls.Add(lnameValidator);

            prefix.Controls.Add(divlbllName);
            prefix.Controls.Add(divtxtlName);
           
            detailRow.Cells.Add(prefix);
            tblAdults.Rows.Add(hRow);
            tblAdults.Rows.Add(detailRow);

            HtmlTableRow seperatorRow = new HtmlTableRow();
            if (i == 0)
            {
                HtmlTableCell seperatorCell = new HtmlTableCell();

                HtmlGenericControl divlblEmail = new HtmlGenericControl("Div");
                divlblEmail.Attributes.Add("class", "col-md-3");
                divlblEmail.InnerHtml = "<div class='col-md-3'>Email&nbsp;ID:*</div>";

                HtmlGenericControl divtxtEmail = new HtmlGenericControl("Div");
                divtxtEmail.Attributes.Add("class", "col-md-3");
                //emailCell.InnerText = "Email ID:";
                TextBox txtEmail = new TextBox();
                txtEmail.ID = "txtAEmail" + (i + 1).ToString();
                txtEmail.CssClass = "form-control";
                txtEmail.CausesValidation = true;
                txtEmail.ValidationGroup = "pax";
                txtEmail.Attributes.Add("ondrop", "return false;");
                txtEmail.Attributes.Add("onpaste", "return false;");
                divtxtEmail.Controls.Add(txtEmail);

                RequiredFieldValidator emailvc = new RequiredFieldValidator();
                emailvc.ControlToValidate = txtFirstName.ID;
                emailvc.ErrorMessage = "Enter Email";
                emailvc.InitialValue = "";
                emailvc.SetFocusOnError = true;
                emailvc.ValidationGroup = "pax";
                divtxtEmail.Controls.Add(emailvc);
                RegularExpressionValidator emailFormat = new RegularExpressionValidator();
                //CustomValidator fnameValidator = new CustomValidator();
                emailFormat.ControlToValidate = txtEmail.ID;
                //emailFormat.ClientValidationFunction = "validateName";
                emailFormat.ErrorMessage = " Invalid&nbsp;EmailId";
                emailFormat.SetFocusOnError = true;
                emailFormat.ValidationGroup = "pax";
                emailFormat.ValidationExpression = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
                divtxtEmail.Controls.Add(emailFormat);

                seperatorCell.Controls.Add(divlblEmail);
                seperatorCell.Controls.Add(divtxtEmail);

                HtmlGenericControl divlblphoneCell = new HtmlGenericControl("Div");
                divlblphoneCell.Attributes.Add("class", "col-md-3");
                divlblphoneCell.InnerHtml = "<div class='col-md-3'>Phone&nbsp;No.:*</div>";

                HtmlGenericControl divtxtPhone = new HtmlGenericControl("Div");
                divtxtPhone.Attributes.Add("class", "col-md-3"); // For Em
                //HtmlTableCell phoneCell = new HtmlTableCell();
                //phoneCell.InnerText = "Phone No.:";
                TextBox txtPhone = new TextBox();
                txtPhone.ID = "txtPhone" + (i + 1).ToString();
                txtPhone.CssClass = "form-control";
                txtPhone.CausesValidation = true;
                txtPhone.MaxLength = 10;
                txtPhone.ValidationGroup = "pax";
                txtPhone.Attributes.Add("onkeypress", "return isNumber(event);");
                txtPhone.Attributes.Add("ondrop", "return false;");
                txtPhone.Attributes.Add("onpaste", "return false;");
                divtxtPhone.Controls.Add(txtPhone);

                RequiredFieldValidator phonevc = new RequiredFieldValidator();
                phonevc.ControlToValidate = txtPhone.ID;
                phonevc.ErrorMessage = "Enter&nbsp;Phone&nbsp;Number";
                phonevc.InitialValue = "";
                phonevc.SetFocusOnError = true;
                phonevc.ValidationGroup = "pax";
                divtxtPhone.Controls.Add(phonevc);

                seperatorCell.Controls.Add(divlblphoneCell);
                seperatorCell.Controls.Add(divtxtPhone);
                if (activity.TransferInclude == "Yes")
                {
                    HtmlGenericControl divlbllPickup = new HtmlGenericControl("Div"); // For pickup Point
                    divlbllPickup.Attributes.Add("class", "col-md-3");
                    divlbllPickup.InnerHtml = "<div class='col-md-3'>PickUp&nbsp;Point:</Div>";

                    HtmlGenericControl divtxtPickup = new HtmlGenericControl("Div"); // For pickup Point
                    divtxtPickup.Attributes.Add("class", "col-md-3");

                    TextBox txtPickup = new TextBox();
                    txtPickup.ID = "txtPickup" + (i + 1).ToString();
                    txtPickup.CssClass = "form-control";
                    txtPickup.CausesValidation = true;
                    txtPickup.MaxLength = 500;
                    txtPickup.ValidationGroup = "pax";
                    txtPickup.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                    txtPickup.Attributes.Add("ondrop", "return false;");
                    txtPickup.Attributes.Add("onpaste", "return false;");
                    divtxtPickup.Controls.Add(txtPickup);

                    //RequiredFieldValidator pickup = new RequiredFieldValidator();
                    //pickup.ControlToValidate = txtPickup.ID;
                    //pickup.ErrorMessage = "Enter&nbsp;PickUp&nbsp;Point";
                    //pickup.InitialValue = "";
                    //pickup.SetFocusOnError = true;
                    //pickup.ValidationGroup = "pax";
                    //divtxtPickup.Controls.Add(pickup);

                    seperatorCell.Controls.Add(divlbllPickup);
                    seperatorCell.Controls.Add(divtxtPickup);

                }
                seperatorRow.Cells.Add(seperatorCell);
            }
           
            if (i == 0)
            {
                tblAdults.Rows.Add(seperatorRow);
            }


        }
        for (int i = 0; i < itinerary.ChildCount; i++)
        {
            HtmlTableRow hRow = new HtmlTableRow();
            HtmlTableCell hCell = new HtmlTableCell();


            hCell.InnerHtml = "<b> Child" + (i + 1).ToString() + " (Name As on passport) </b>";
            hCell.Attributes.Add("class", "subgray-header");

            hCell.ColSpan = 8;
            hCell.Attributes.Add("class", "b_bot_1");
            hRow.Cells.Add(hCell);
            tblAdults.Rows.Add(hRow);

            HtmlTableRow detailRow = new HtmlTableRow();
            HtmlTableCell prefix = new HtmlTableCell();

            HtmlGenericControl divlblTitle = new HtmlGenericControl("Div");// For Child Div
            divlblTitle.Attributes.Add("class", "col-md-3");
            divlblTitle.InnerHtml = "<div class='col-md-3'>Title:</div>";

            HtmlGenericControl divDdlTitle = new HtmlGenericControl("Div"); // For Child Div
            divDdlTitle.Attributes.Add("class", "col-md-3"); // For Child Div class attribute

            DropDownList ddlTitle = new DropDownList();
            ddlTitle.Items.Add(new ListItem("Select", "Select"));
            ddlTitle.Items.Add(new ListItem("Mr.", "Mr."));
            ddlTitle.Items.Add(new ListItem("Ms.", "Ms."));
            //ddlTitle.Items.Add(new ListItem("Dr.", "Dr.")); //Commented By Chandan on 09012016
            ddlTitle.SelectedIndex = 0;
            ddlTitle.ID = "ddlCTitle" + (i + 1).ToString();
            ddlTitle.CausesValidation = true;
            ddlTitle.CssClass = "form-control";
            ddlTitle.ValidationGroup = "pax";

            divDdlTitle.Controls.Add(ddlTitle);

            RequiredFieldValidator prefixvc = new RequiredFieldValidator();
            //prefixvc.ControlToValidate = txtPrefix.ID;
            prefixvc.ControlToValidate = ddlTitle.ID;
            //prefixvc.ErrorMessage = "Enter Prefix";
            prefixvc.ErrorMessage = "Select Title";
            prefixvc.ValidationGroup = "pax";
            prefixvc.InitialValue = "Select";
            prefixvc.SetFocusOnError = true;
            divDdlTitle.Controls.Add(prefixvc);
            //prefix.Attributes.Add("style", "vertical-align:sub");

            prefix.Controls.Add(divlblTitle);
            prefix.Controls.Add(divDdlTitle);

            //Child First Name   modified by brahmam 30.07.2016
            //HtmlTableCell fname = new HtmlTableCell();
            //fname.InnerText = "First Name:";
            HtmlGenericControl divlblfName = new HtmlGenericControl("Div"); // For Child First Name
            divlblfName.Attributes.Add("class", "col-md-3");
            divlblfName.InnerHtml = "<div  class='col-md-3' >First&nbsp;Name:</Div>";

            HtmlGenericControl divTxtfName = new HtmlGenericControl("Div"); // For Child First Name
            divTxtfName.Attributes.Add("class", "col-md-3");

            TextBox txtFirstName = new TextBox();
            txtFirstName.ID = "txtCFirstName" + (i + 1).ToString();
            txtFirstName.CssClass = "form-control";
            //txtFirstName.Width = new Unit(100, UnitType.Pixel);
            txtFirstName.CausesValidation = true;
            txtFirstName.MaxLength = 10;
            txtFirstName.ValidationGroup = "pax";
            txtFirstName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
            txtFirstName.Attributes.Add("ondrop", "return false;");
            txtFirstName.Attributes.Add("onpaste", "return false;");
            divTxtfName.Controls.Add(txtFirstName);

            RequiredFieldValidator fnamevc = new RequiredFieldValidator();
            fnamevc.ControlToValidate = txtFirstName.ID;
            fnamevc.ErrorMessage = "Enter&nbsp;First&nbsp;Name";
            fnamevc.Display = ValidatorDisplay.Dynamic;
            fnamevc.InitialValue = "";
            fnamevc.SetFocusOnError = true;
            fnamevc.ValidationGroup = "pax";
            divTxtfName.Controls.Add(fnamevc);

            CustomValidator fnameValidator = new CustomValidator();
            fnameValidator.ControlToValidate = txtFirstName.ID;
            fnameValidator.ClientValidationFunction = "validateName";
            fnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
            fnameValidator.SetFocusOnError = true;
            fnameValidator.ValidationGroup = "pax";
            divTxtfName.Controls.Add(fnameValidator);


            prefix.Controls.Add(divlblfName);
            prefix.Controls.Add(divTxtfName);

            //HtmlTableCell lname = new HtmlTableCell();
            //lname.InnerText = "Last Name:";
            //Last Name
            HtmlGenericControl divlbllName = new HtmlGenericControl("Div"); // For Child Last Name
            divlbllName.Attributes.Add("class", "col-md-3");
            divlbllName.InnerHtml = "<div  class='col-md-3' >Last&nbsp;Name:</Div>";

            HtmlGenericControl divtxtlName = new HtmlGenericControl("Div"); // For Child Last Name
            divtxtlName.Attributes.Add("class", "col-md-3");


            TextBox txtLastName = new TextBox();

            txtLastName.ID = "txtCLastName" + (i + 1).ToString();

            txtLastName.CssClass = "form-control";
            txtLastName.CausesValidation = true;
            txtLastName.MaxLength = 50;
            txtLastName.ValidationGroup = "pax";
            txtLastName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
            txtLastName.Attributes.Add("ondrop", "return false;");
            txtLastName.Attributes.Add("onpaste", "return false;");
            divtxtlName.Controls.Add(txtLastName);

            RequiredFieldValidator lnamevc = new RequiredFieldValidator();
            lnamevc.ControlToValidate = txtLastName.ID;
            lnamevc.ErrorMessage = "Enter&nbsp;Last&nbsp;Name";
            lnamevc.Display = ValidatorDisplay.Dynamic;
            lnamevc.InitialValue = "";
            lnamevc.SetFocusOnError = true;
            lnamevc.ValidationGroup = "pax";
            divtxtlName.Controls.Add(lnamevc);

            CustomValidator lnameValidator = new CustomValidator();
            lnameValidator.ControlToValidate = txtLastName.ID;
            lnameValidator.ClientValidationFunction = "validateName";
            lnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
            lnameValidator.SetFocusOnError = true;
            lnameValidator.ValidationGroup = "pax";
            divtxtlName.Controls.Add(lnameValidator);

            prefix.Controls.Add(divlbllName);
            prefix.Controls.Add(divtxtlName);


            detailRow.Cells.Add(prefix);
            //detailRow.Cells.Add(fname);
            //detailRow.Cells.Add(lname);
            tblAdults.Rows.Add(detailRow);


        }
    }


    protected void imgContinue_Click(object sender, EventArgs e)
    {
        if (action.Value != null)
        {
            if (action.Value == "BookSightseeing")
            {
                if (Session["SItinerary"] == null && Session["cSessionId"] ==null)
                {
                    errorMessage = "Your Session is expired !! Please" + "<a href=\"Sightseeing.aspx\"" + "\">" + " Retry" + "</a>.";
                    return;
                }
                else
                {
                    try
                    {
                        itinerary = (SightseeingItinerary)Session["SItinerary"];
                        string reviewBookingPageLink = "SightseeingBookingConfirm.aspx?TourId=" + tourId; //tourId used for selected tour information for Price Checking on Confirmation page
                        List<string> paxDetails = new List<string>();
                        for (int k = 0; k < itinerary.AdultCount; k++)
                        {
                            //HtmlTable tblPax = this.FindControl("ctl00_cphTransaction_tblAdults") as HtmlTable;
                            if (tblAdults.Rows.Count > 0)
                            {
                                DropDownList ddlTitle = new DropDownList();
                                TextBox txtFName = new TextBox();
                                TextBox txtLName = new TextBox();
                                TextBox txtMail = new TextBox();
                                TextBox txtPhone = new TextBox();
                                TextBox txtpickup = new TextBox();

                                ddlTitle = tblAdults.Rows[k].FindControl("ddlATitle" + (k + 1)) as DropDownList;
                                txtFName = tblAdults.Rows[k].FindControl("txtAFirstName" + (k + 1)) as TextBox;
                                txtLName = tblAdults.Rows[k].FindControl("txtALastName" + (k + 1)) as TextBox;

                                paxDetails.Add(ddlTitle.SelectedItem.Text + "." + txtFName.Text + " " + txtLName.Text + "|Adult " + (k + 1));
                                if (k == 0)
                                {
                                    txtMail = tblAdults.Rows[k].FindControl("txtAEmail" + (k + 1)) as TextBox;
                                    txtPhone = tblAdults.Rows[k].FindControl("txtPhone" + (k + 1)) as TextBox;
                                    if(activity.TransferInclude== "Yes")
                                    txtpickup = tblAdults.Rows[k].FindControl("txtPickup" + (k + 1)) as TextBox;

                                    itinerary.Email = txtMail.Text;
                                    itinerary.PhNo = txtPhone.Text;
                                    if(txtpickup.Text.Length>0)
                                    itinerary.PickupPoint = txtpickup.Text;
                                }
                            }

                        }

                        for (int k = 0; k < itinerary.ChildCount; k++)
                        {

                            if (tblAdults.Rows.Count > 0)
                            {
                                DropDownList ddlTitle = new DropDownList();
                                TextBox txtFName = new TextBox();
                                TextBox txtLName = new TextBox();

                                ddlTitle = tblAdults.Rows[k].FindControl("ddlCTitle" + (k + 1)) as DropDownList;
                                txtFName = tblAdults.Rows[k].FindControl("txtCFirstName" + (k + 1)) as TextBox;
                                txtLName = tblAdults.Rows[k].FindControl("txtCLastName" + (k + 1)) as TextBox;
                                paxDetails.Add(ddlTitle.SelectedItem.Text + "" + txtFName.Text + " " + txtLName.Text + "|Child " + (k + 1));
                            }

                        }
                        if (ddlHotels !=null && ddlHotels.SelectedIndex >= 0)
                        {
                            itinerary.DepPointInfo = ddlHotels.SelectedValue + "|" + ddlHotels.SelectedItem.Text;
                        }
                        else if (ddlTimes != null && ddlTimes.SelectedIndex >= 0)
                        {
                           
                            //foreach (TourOperations tour in staticInfo.TourOperationInfo)
                            //{
                            //    if (itinerary.TourDate >= tour.FromDate && itinerary.TourDate <= tour.ToDate)
                            //    {
                            //        depPointLst.Add(tour.DepTime, tour.DepCode + "|" + tour.DepName);

                            //    }
                            //}
                            itinerary.DepTime = ddlTimes.SelectedValue.Split('|')[1];
                           // itinerary.DepPointInfo = depPointLst[ddlTimes.SelectedValue];
                            itinerary.DepPointInfo = ddlTimes.SelectedValue.Split('|')[0] + "|" + ddlTimes.SelectedItem.Text;
                        }
                        else
                        {
                            //itinerary.DepPointInfo = "";
                        }

                        


                        itinerary.PaxNames = paxDetails;
                        itinerary.VoucherStatus = true; //Always true for Update booking queue table

                        Session["SItinerary"] = itinerary;

                        Response.Redirect(reviewBookingPageLink);
                      
                    }
                    catch (Exception ex) { throw ex; }
                }
                
            }
        }
    }
    protected void BindHotels()
    {
        

        try
        {
            if (searchResult != null)
            {

                if (searchResult.DepaturePointRequired)
                {
                    foreach (TourOperations tour in staticInfo.TourOperationInfo)
                    {
                        List<string> lng = new List<string>();
                        string[] languages = tour.Language.Split('|');
                        string lngCode;
                        foreach (string lang in languages)
                        {
                            if (!string.IsNullOrEmpty(lang))
                            {
                                lng.Add(lang.Trim());
                            }
                        }
                        if (tour.Language.ToUpper() != "UNESCORTED") //Checking Tour Language UNESCORTED
                        {
                            lngCode = tourOperation.LanguageCode[0];
                        }
                        else
                        {
                            lngCode = tourOperation.LangName[0];
                        }

                        if (request.TourDate >= tour.FromDate && request.TourDate <= tour.ToDate)
                        {
                            
                            if ((lng.Contains(lngCode)) && (tour.Frequency.ToUpper().Trim().Contains("DAILY") || tour.Frequency.ToUpper().Contains(request.TourDate.DayOfWeek.ToString().ToUpper())))
                            {
                                depPointLst.Add(tour.DepTime, tour.DepCode + "|" + tour.DepName);
                                itinerary.DepTime = tour.DepTime;
                                if (tour.DepCode == "*")
                                {
                                    isHotelChooseDepPoint = true;
                                    break;
                                }
                            }

                        }
                       
                    }
                }
                //Loading Hotels
                if (isHotelChooseDepPoint)
                {

                    try
                    {
                        if (Session["ssReq"] == null)
                        {

                            //request=(SightSeeingReguest)Session["ssReq"];
                            Response.Redirect("AbandonSession.aspx", false);

                        }

                        List<string> source = new List<string>();
                        source.Add("GTA");
                        req.CityName = request.CityName;
                        req.CityId = Convert.ToInt32(request.DestinationCode);
                        req.StartDate = request.TourDate;
                        req.EndDate = request.TourDate.AddDays(1);
                        req.CountryName = request.CountryName;
                        req.NoOfRooms = 1;
                        req.RoomGuest = new RoomGuestData[1];
                        req.RoomGuest[0] = new RoomGuestData();
                        req.RoomGuest[0].noOfAdults = 2;
                        req.RoomGuest[0].noOfChild = 0;
                        req.PassengerNationality = "6";
                        req.MinRating = 0;
                        req.MaxRating = 5;
                        req.Sources = source;
                        MetaSearchEngine mse = new MetaSearchEngine(sessionId);
                        mse.SettingsLoginInfo = Settings.LoginInfo;
                        hotelResults = mse.GetHotelResults(req, (int)Settings.LoginInfo.AgentId);
                        // ddlHotels.Items.Add(new ListItem("Select Nearest Hotel", "-1"));

                        foreach (HotelSearchResult hotelResult in hotelResults)
                        {
                            ddlHotels.Items.Add(new ListItem(hotelResult.HotelName, hotelResult.HotelCode));
                        }
                    }

                    catch (Exception ex)
                    {
                        // Response.Redirect("ErrorPage.aspx?sourcePage=Sightseeing");
                    }
                }
                //Loading Departure Time
                else if (searchResult.DepaturePointRequired)
                {

                    foreach (string depTimeDetails in depPointLst.Keys)
                    {
                        string[] tempDep = depPointLst[depTimeDetails].Split('|');
                        ddlTimes.Items.Add(new ListItem(depTimeDetails + "," + tempDep[1], tempDep[0] + "|" + depTimeDetails));
                    }

                }
            }
        }
        catch { }
    }

}
