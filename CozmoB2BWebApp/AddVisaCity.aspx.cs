﻿using System;
using System.Web.UI;
using Visa;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

public partial class AddVisaCity : CT.Core.ParentPage
{
    public int UpdateCity = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();
        if (!IsPostBack)
        {
            Page.Title = "Add City";
            ddlCountry.DataSource = VisaCountry.GetActiveVisaCountryList();
            ddlCountry.DataTextField = "countryName";
            ddlCountry.DataValueField = "countryCode";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, "Select");
            if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
            {
                int visaCityId;
                bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out visaCityId);
                if (!isnumerice)
                {
                    Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                    Response.End();

                }
                try
                {
                    VisaCity city = new VisaCity(visaCityId);
                    if (city != null)
                    {
                        UpdateCity = 1;
                        txtCityName.Text = city.CityName.Trim();
                        txtCityCode.Text = city.CityCode.Trim();
                        ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(city.CountryCode));
                        txtARCityName.Text = city.CityNameAR.Trim();
                        btnSave.Text = "Update";
                        Page.Title = "Update City";
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaCity,Err:" + ex.Message, "");
                }
            }
        }

    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        int errMsg = 0;
        try
        {
           
            VisaCity city = new VisaCity();
            city.CountryCode = ddlCountry.SelectedValue;
            city.CityCode = txtCityCode.Text.Trim();
            city.CityName = txtCityName.Text.Trim();
            city.CityNameAR = txtARCityName.Text.Trim();
            if (btnSave.Text == "Update")
            {
                if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
                {

                    city.CityId = Convert.ToInt32(Request.QueryString[0]);
                    city.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                    errMsg = city.UpdateCity();
                }
            }
            else
            {
                city.IsActive = true;
                city.CreatedBy = (int)Settings.LoginInfo.UserID;
                errMsg = city.Save();
            }
          
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaCity,Err:" + ex.Message, "");
        }
        if (errMsg == 0)
        {
            Response.Redirect("VisaCityList.aspx?Country=" + ddlCountry.SelectedValue);
        }
        else if (errMsg == 1)
        {
            lblErrorMessage.Text = "Visa city code already exists";
        }
        else if (errMsg == 2)
        {
            lblErrorMessage.Text = "Visa city name already exists";
        }
    }

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
