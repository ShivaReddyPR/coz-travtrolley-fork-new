﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.Web.UI.Controls;
using CT.TicketReceipt.Common;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.IO;
using Ionic.Zip;
using System.Web;
using CT.Configuration;

public partial class ExpReimbursementQueueUI : CT.Core.ParentPage
    {
        private string EXPREIM_SEARCH_SESSION = "_EXPReimSearchList";
        protected ExpReportData detail;
        protected void Page_Load(object sender, EventArgs e)
        {


            try
            {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
                {
                    if (!Page.IsPostBack)
                    {
                        IntialiseControls();

                    }
                }
                else//Authorisation Check -- if failed
                {
                    Response.Redirect("AbandonSession.aspx");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(ExpReimbursementQueue) Page Load Event Error: " + ex.Message, "0");
            }
        }

        //To initialise controls
        private void IntialiseControls()
        {
            try
            {
                expReimFromDate.Value = DateTime.Now;
                expReimToDate.Value = DateTime.Now;

                bindEmployeesList();
                bindExpenseTypes();
                bindCategories();
                bindSearch(DateTime.Now.AddMonths(-1), DateTime.Now, 0, 0, 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To bind employeeslist
        private void bindEmployeesList()
        {
            DataTable dt = ExpensesUtility.GetExpProfileList(Settings.LoginInfo.CorporateProfileId, (int)Settings.LoginInfo.AgentId, ListStatus.Short);
            dt.Columns.Add("FullName", typeof(string), "EmployeeId + ' - ' + SurName + ' ' + Name");
            ddlEmployee.DataSource = dt;
            ddlEmployee.DataTextField = "FullName";
            ddlEmployee.DataValueField = "ProfileId";
            ddlEmployee.DataBind();
            ddlEmployee.Items.Insert(0, new ListItem("--Select Employee--", "0"));
        }

        //To bind Expensestypes
        private void bindExpenseTypes()
        {
            ddlExpType.DataSource = ExpensesUtility.GetExpType((int)Settings.LoginInfo.AgentId);
            ddlExpType.DataTextField = "Type";
            ddlExpType.DataValueField = "ET_ID";
            ddlExpType.DataBind();
            ddlExpType.Items.Insert(0, new ListItem("--Select Type--", "0"));
        }

        //To bind categories
        private void bindCategories()
        {
            ddlExpCategory.DataSource = ExpensesUtility.GetCategory((int)Settings.LoginInfo.AgentId);
            ddlExpCategory.DataTextField = "Category";
            ddlExpCategory.DataValueField = "EC_ID";
            ddlExpCategory.DataBind();
            ddlExpCategory.Items.Insert(0, new ListItem("--Select Category--", "0"));
        }

        //To get data from database
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

                int ProfileId = 0; int EC_ID = 0; int ET_ID = 0;
                if (ddlEmployee.SelectedIndex > 0)
                {
                    ProfileId = Convert.ToInt32(ddlEmployee.SelectedValue);
                }
                if (ddlExpCategory.SelectedIndex > 0)
                {
                    EC_ID = Convert.ToInt32(ddlExpCategory.SelectedValue);
                }
                if (ddlExpType.SelectedIndex > 0)
                {
                    ET_ID = Convert.ToInt32(ddlExpType.SelectedValue);
                }
                bindSearch(Convert.ToDateTime(expReimFromDate.Value, dateFormat), Convert.ToDateTime(expReimToDate.Value, dateFormat), ProfileId, EC_ID, ET_ID);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "ExpenseReimburseQueue btnSearchApprovals Error: " + ex.Message, "0");
            }
        }

    //To bind data to gridview
    private void bindSearch(DateTime FromDate, DateTime ToDate, int ProfileId, int EC_ID, int ET_ID)
    {
        try
        {
            DataTable dt = ExpReimbursement.GetReimburseQueue(FromDate, ToDate, ProfileId, EC_ID, ET_ID);
            if (dt != null && dt.Rows.Count > 0)
            {
                SearchList = dt;
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvSearch, dt);
                gvSearch.Visible = true;
                btnPay.Visible = true;
                btnReject.Visible = true;
                hdnReimburseData.Value = JsonConvert.SerializeObject(dt);
            }
            else
            {
                gvSearch.DataBind();
                gvSearch.Visible = false;
                btnPay.Visible = false;
                btnReject.Visible = false;
                hdnReimburseData.Value = "";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

        //To store datatable in session
        private DataTable SearchList
        {
            get
            {

                return (DataTable)Session[EXPREIM_SEARCH_SESSION];
            }
            set
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["ED_Id"] };
                Session[EXPREIM_SEARCH_SESSION] = value;
            }
        }

    //To filter the girdview columns
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={
                {"HTtxtDATE", "DATE" }
                ,{"HTtxtEMPLOYEEID", "EMP_ID" }
                ,{"HTtxtEMPLOYEENAME", "EMP_NAME" }
                ,{"HTtxtEXPREF",  "ER_RefNo"}
                ,{"HTtxtEXPCAT",  "EXP_CATEGORY"}
                ,{"HTtxtEXPTYPE", "EXP_TYPE"}
                ,{"HTtxtAMOUNT", "ED_TotalAmount"}
                ,{"HTtxtTRAMOUNT", "TotalReimbursementAmount"}
                ,{"HTtxtBALANCE", "Balance"}                
                ,{"HTtxtEXPReortTitle", "ER_Title"}                
                ,{"HTtxtClaimExpense", "ED_ClaimExpense"}                
                ,{"HTtxtPaymentType", "PT_Desc"}                
                ,{"HTtxtETDesc", "ER_Purpose"}                
                ,{"HTtxtGLAccount", "ET_GLAccount"}                
                ,{"HTCostCentre", "CostCentre"}                
                ,{"HTtxtcity", "ED_City"}                
                ,{"HTtxtStatus", "ApprovalStatus"}                 
        };
        CommonGrid g = new CommonGrid();
        g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
    }

        //To enable paging in the gridview
        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSearch.PageIndex = e.NewPageIndex;
                gvSearch.EditIndex = -1;
                FilterSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Airline, Severity.High, 1, "(ExpensesReimbursementQueue)gvSearch_PageIndexChanging method .Error:" + ex.ToString(), "0");
            }
        }
     
        //To check checkbox is checked or not
        private void isTrackingEmpty()
        {
            try
            {
                bool _selected = false;
                foreach (GridViewRow gvRow in gvSearch.Rows)
                {
                    CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                    if (chkSelect.Checked)
                    {
                        _selected = true;
                        return;
                    }
                }
                string strMsg = "Please Select atleast one Item ! ";
                if (!_selected)
                Utility.Alert(this.Page, strMsg);
        }
            catch { throw; }
        }

    //Enable textbox and select entire checkbox  in gridview based on header checkbox checked
    protected void HTchkSelect_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvHdrRow = (GridViewRow)((CheckBox)sender).NamingContainer;
            CheckBox chkSelectAll = (CheckBox)gvHdrRow.FindControl("HTchkSelectAll");
            foreach (GridViewRow gvRow in gvSearch.Rows)
            {
                TextBox txt = (TextBox)gvRow.FindControl("txtReimAmount");
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                if (chkSelectAll.Checked)
                {
                    chkSelect.Checked = chkSelectAll.Checked;
                    txt.Enabled = true;
                    ((TextBox)gvRow.FindControl("txtComments")).Enabled = true;
                    txt.Text = ((Label)gvRow.FindControl("ITlblAMOUNT")).Text;
                }
                else
                {
                    chkSelect.Checked = false;
                    txt.Enabled = false;
                    txt.Text = ((Label)gvRow.FindControl("ITlblAMOUNT")).Text;
                    ((TextBox)gvRow.FindControl("txtComments")).Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    //Enable textbox in gridview based on checkbox checked
    protected void ITchkSelect_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            for (int i = 0; i < gvSearch.Rows.Count; i++)
            {
                CheckBox cb = (CheckBox)gvSearch.Rows[i].Cells[0].FindControl("ITchkSelect");
                TextBox txt = (TextBox)gvSearch.Rows[i].Cells[0].FindControl("txtReimAmount");
                if (cb.Checked)
                {
                    txt.Enabled = true;                    
                    txt.Text = ((Label)gvSearch.Rows[i].Cells[0].FindControl("ITlblAMOUNT")).Text;
                    ((TextBox)gvSearch.Rows[i].Cells[0].FindControl("txtComments")).Enabled = true;
                }
                else
                {
                    ((TextBox)gvSearch.Rows[i].Cells[0].FindControl("txtComments")).Enabled = false;
                    txt.Enabled = false;
                    txt.Text = ((Label)gvSearch.Rows[i].Cells[0].FindControl("ITlblAMOUNT")).Text;
                }
            }
        }
        catch (Exception ex)
        {

            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void gvSearch_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var ddl = e.Row.FindControl("ddlCurrency") as DropDownList;
                if (ddl != null)
                {
                    ListItem item = new ListItem();
                    item.Text = Settings.LoginInfo.Currency;
                    item.Value = Settings.LoginInfo.Currency;
                    ddl.Items.Add(item);

                }
            }
        }

        //To save data into Database
        protected void btnPay_Click(object sender, EventArgs e)
        {
            try
            {

                //isTrackingEmpty();
                foreach (GridViewRow gvRow in gvSearch.Rows)
                {
                    CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                    DropDownList ddl = (DropDownList)gvRow.FindControl("ddlCurrency");
                    TextBox txt = (TextBox)gvRow.FindControl("txtReimAmount");
                    HiddenField hdfEED_Id = (HiddenField)gvRow.FindControl("IThdfED_Id");

                HiddenField hdfClaimAmount = (HiddenField)gvRow.FindControl("hdfClaimAmount");
                if (chkSelect.Checked && txt.Text.Length > 0)
                {
                    ExpReimbursement detail = new ExpReimbursement();
                    detail.ERM_ClaimAmount = Convert.ToDecimal(hdfClaimAmount.Value);
                    detail.ERM_ED_ID = Convert.ToInt32(hdfEED_Id.Value);
                    detail.ERM_Currency = ddl.SelectedValue;
                    detail.ERM_ReimbursementAmount = Convert.ToDecimal(txt.Text);
                    detail.ERM_Status = true;
                    detail.ERM_CreatedBy = (int)Settings.LoginInfo.UserID;
                    detail.ERM_ModifiedBy = (int)Settings.LoginInfo.UserID;
                    detail.ERM_ReimRemarks = (gvRow.FindControl("txtComments") as TextBox).Text;
                    detail.Save();

                        Label lblMasterError = (Label)this.Master.FindControl("lblError");
                        lblMasterError.Visible = true;
                        lblMasterError.Text = "Details updated Successfully !";
                        //As this is reimbursement 
                        //So email will be triggered to only to the particular with reimbursement details.
                        try
                        {
                            var data = ExpReimbursement.GetReimbursedCount(Convert.ToInt32(hdfEED_Id.Value), (int)Settings.LoginInfo.UserID);
                            if (data == null || data.Rows.Count > 0)
                            {
                                if (data != null && data.Rows.Count > 0)
                                {                                    
                                    EmailParams clsParams = new EmailParams();
                                    clsParams.FromEmail = ConfigurationSystem.Email["fromEmail"];
                                    clsParams.ToEmail = Convert.ToString(data.Rows[0]["Email"]);
                                    clsParams.Subject = Convert.ToString(data.Rows[0]["RepSubject"]);
                                    clsParams.EmailBody = Convert.ToString(data.Rows[0]["RepHeader"]).Replace("@Expenses", Convert.ToString(data.Rows[0]["RepBody"]));
                                    Email.Send(clsParams);
                                }
                            }
                        }
                        catch { }


                    }
                }
                IntialiseControls();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Airline, Severity.High, 1, "(ExpensesReimbursementQueue)btnPay_Click Event .Error:" + ex.ToString(), "0");

            }
        }

        //To get expenses details
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string getReportData(int ED_Id)
        {
            try
            {
            DataTable dt = ExpReportData.GetReportData(ED_Id);
            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(dt);
                return JSONString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To get document details
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string getDocDetails(int ED_Id)
        {
            try
            {
                DataTable dt = ExpReportData.GetDocDetails(ED_Id);
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                //var Result = (from data in dt.AsEnumerable()
                //          select new
                //          {
                //              docName = Convert.ToString(data["RC_FileName"]),
                //              RC_Path = "<a href='#' onclick=\"javascript:Download('" + (Convert.ToString(data["RC_Path"])).Replace("\\", "//") + "','" + Path.GetFileName(Convert.ToString(data["RC_Path"])) + "')\">Download</a>"
                //          }).ToList();
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(dt);
                return JSONString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //To send email to employee
        protected string SendEmail(string toEmpOrApp, int ED_Id)
        {
            detail = new ExpReportData(ED_Id);
            string myPageHTML = string.Empty;
            try
            {
                List<string> toArray = new System.Collections.Generic.List<string>();
                if (!string.IsNullOrEmpty(detail.EMP_Email))
                {
                    toArray.Add(detail.EMP_Email);
                }
                System.IO.StringWriter sw = new System.IO.StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                EmailDivEmployee.RenderControl(htw);
                myPageHTML = sw.ToString();
                myPageHTML = myPageHTML.Replace("none", "block");
                string subject = "Reimbursement Notification";
                if (toArray != null && toArray.Count > 0)
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                }
            }

            catch (Exception ex)
            {
                Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateCreateExpenseReport.aspx)Failed to Send Email For Employee and Approvers: Reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
            return myPageHTML;
        }

    protected void btnReject_Click(object sender, EventArgs e)
    {
        try
        {
            List<object> liExpnses = new List<object>();
            foreach (GridViewRow gvRow in gvSearch.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                //DropDownList ddl = (DropDownList)gvRow.FindControl("ddlCurrency");
                TextBox txtComments = (TextBox)gvRow.FindControl("txtComments");
                HiddenField hdfEED_Id = (HiddenField)gvRow.FindControl("IThdfED_Id");

                //HiddenField hdfClaimAmount = (HiddenField)gvRow.FindControl("hdfClaimAmount");
                if (chkSelect.Checked && !string.IsNullOrEmpty(txtComments.Text))
                {
                    //ExpReimbursement detail = new ExpReimbursement();
                    //detail.ERM_ClaimAmount = Convert.ToDecimal(hdfClaimAmount.Value);
                    //detail.ERM_ED_ID = Convert.ToInt32(hdfEED_Id.Value);
                    //detail.ERM_Currency = ddl.SelectedValue;
                    //detail.ERM_ReimbursementAmount = Convert.ToDecimal(txt.Text);
                    //detail.ERM_Status = true;
                    //detail.ERM_CreatedBy = (int)Settings.LoginInfo.UserID;
                    //detail.ERM_ModifiedBy = (int)Settings.LoginInfo.UserID;
                    //detail.ERM_ReimRemarks = (gvRow.FindControl("txtComments") as TextBox).Text;
                    //detail.Save();

                    //Label lblMasterError = (Label)this.Master.FindControl("lblError");
                    //lblMasterError.Visible = true;
                    //lblMasterError.Text = "Details updated Successfully !";
                    ////As this is reimbursement 
                    ////So email will be triggered to only to the particular with reimbursement details.
                    //try
                    //{
                    //    //SendEmail("E", Convert.ToInt32(hdfEED_Id.Value)); Deactivated as per Vijay Req
                    //}
                    //catch { }

                    liExpnses.Add(new
                    {
                        ApprovalStatus = "RR",
                        ExpDetailId = Convert.ToInt32(hdfEED_Id.Value),
                        expApproverId = Settings.LoginInfo.CorporateProfileId,
                        ProfileID = 0,
                        CreatedBy = (int)Settings.LoginInfo.UserID,
                        ExpRepId = 0,
                        Remarks = txtComments.Text
                    });
                }
            }

            if (liExpnses != null && liExpnses.Count > 0)
                CozmoExpenseApi.ReimburseRevise(liExpnses);
            IntialiseControls();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(ExpensesReimbursementQueue)btnReject_Click Event .Error:" + ex.ToString(), "0");
        }
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = ExpReportData.GetDocDetails(Convert.ToInt32(hdnEDID.Value));
            if (dt != null && dt.Rows.Count > 0)
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    string zipName = string.Empty;
                    foreach (DataRow row in dt.Rows)
                    {
                        if (!string.IsNullOrEmpty(row["RC_Path"].ToString()) && !string.IsNullOrEmpty(row["RC_FileName"].ToString()))
                        {
                            if (row["RC_FileName"].ToString() != "NR" && File.Exists(row["RC_Path"].ToString()))
                                zip.AddFile(row["RC_Path"].ToString()).FileName = row["RC_FileName"] + "" + ""; //renameing File name
                        }
                        zipName = String.Format("{0}.zip", row["RC_Name"].ToString() + DateTime.Now.ToString("yyyyMMddHHmmss"));
                    }
                    Response.Clear();
                    Response.BufferOutput = false;
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    zip.Save(Response.OutputStream);
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(ExpensesReimbursementQueue)btnDownload_Click Event .Error:" + ex.ToString(), "0");
        }
        finally
        {
            Response.End();
        }
    }
}
