﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using CT.Core;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;

public partial class FixedDepartureConfirmationGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected Activity activity = null;
    protected string activityImgFolder;
    protected decimal total = 0;
    protected string code;
    protected decimal discount;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            activityImgFolder = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"].ToString();
            if (Session["FixedDeparture"] != null)
            {
                activity = Session["FixedDeparture"] as Activity;
                DataTable dtTransHeader = activity.TransactionHeader;
                int adults = 0, childs = 0, infants = 0;
                if (dtTransHeader != null && dtTransHeader.Rows.Count > 0)
                {
                    adults = Convert.ToInt32(dtTransHeader.Rows[0]["Adult"]);
                    childs = Convert.ToInt32(dtTransHeader.Rows[0]["Child"]);
                    infants = Convert.ToInt32(dtTransHeader.Rows[0]["Infant"]);
                    hdnTotalPaxCount.Value = Convert.ToString(adults + childs + infants);
                }
                activity.TransactionHeader.Rows[0]["QuotedStatus"] = "C";
                //lblAgentBalance.Text = Settings.LoginInfo.Currency + " " + Settings.LoginInfo.AgentBalance.ToString("N" + CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue);
                lblAmountPaid.Text = Settings.LoginInfo.Currency + " " + Convert.ToDecimal(activity.TransactionHeader.Rows[0]["TotalPrice"]).ToString("N" + CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue);
                lblTitle.Text = activity.Name;
                //lblLocation.Text = activity.City + ", " + activity.Country;
                //lblDuration.Text = activity.DurationHours.ToString();
                lblOverview.Text = activity.Overview;
                total = Convert.ToDecimal(activity.TransactionHeader.Rows[0]["TotalPrice"]);
                lblTotal.Text = Convert.ToDecimal(activity.TransactionHeader.Rows[0]["TotalPrice"]).ToString("N" + CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue);

                code = Convert.ToString(activity.DiscountCode);
                discount = Convert.ToDecimal(activity.Discount);
                hdnTotal.Value = total.ToString();
                imgActivity.ImageUrl = activityImgFolder + activity.ImagePath2;
                dlPaxList.DataSource = activity.TransactionDetail;
                dlPaxList.DataBind();
                if (!IsPostBack)
                {
                    lblTotalAmount.Text = Settings.LoginInfo.Currency + " " + lblTotal.Text;
                    lblPayable.Text = Settings.LoginInfo.Currency + " " + lblTotal.Text;
                    lblBalance.Text = Settings.LoginInfo.Currency + " " + lblTotal.Text;

                    ddlSettlementMode.DataSource = UserMaster.GetMemberTypeList("settlement_mode").Tables[0];
                    ddlSettlementMode.DataTextField = "FIELD_TEXT";
                    ddlSettlementMode.DataValueField = "FIELD_VALUE";
                    ddlSettlementMode.DataBind();

                    ddlSettlementMode.SelectedIndex = 0;

                    ddlCreditCard.DataSource = CreditCardMaster.GetList(ListStatus.Short, RecordStatus.Activated);
                    ddlCreditCard.DataTextField = "Card_Name";
                    ddlCreditCard.DataValueField = "Card_Charge";
                    ddlCreditCard.DataBind();
                    ddlCreditCard.SelectedIndex = 0;
                    lblCharge.Text = ddlCreditCard.Items[0].Value;
                    hdnCardType.Value = ddlCreditCard.SelectedItem.Text;
                }
                
            }
            else
            {
                Response.Redirect("FixedDepartureResults.aspx", false);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
        }
    }
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["FixedDeparture"] != null)
            {
                decimal total = Convert.ToDecimal(lblAmountPaid.Text.Replace(Settings.LoginInfo.Currency, "")); //Convert.ToDecimal(activity.TransactionHeader.Rows[0]["TotalPrice"]);

                AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);
                decimal currentAgentBalance = agent.UpdateBalance(0);
                //if (currentAgentBalance > Convert.ToDecimal(total))
                //{
                    if (activity.IsFixedDeparture=="Y")
                    {
                        decimal discountAmount = 0;
                        if (ddlDiscount.SelectedIndex > 0)
                        {
                            if (txtCode.Text.Trim() == activity.DiscountCode && activity.Discount > 0)
                            {
                                discountAmount = activity.Discount;
                            }
                        }

                        IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                        DataTable dtFDDetails = activity.FixedDepartureDetails;
                        dtFDDetails.Rows.Clear();
                        int mode = Convert.ToInt32(ddlSettlementMode.SelectedItem.Value);
                        if (mode == 1)
                        {
                            DataRow row = dtFDDetails.NewRow();
                            row["TranxId"] = -1;
                            row["ATHDId"] = activity.Id;
                            row["SettlementMode"] = (int)PaymentMode.Cash;
                            row["Amount"] = Convert.ToDecimal(txtCash.Text.Trim());
                            row["BalanceAmount"] = Convert.ToDecimal(lblTotal.Text.Replace(Settings.LoginInfo.Currency, "")) - Convert.ToDecimal(txtCash.Text.Trim());
                            row["Currency"] = Settings.LoginInfo.Currency;
                            row["ExchangeRate"] = Convert.ToDecimal(1.00);
                            row["CreditCardId"] = -1;
                            row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                            row["PaymentType"] = (int)PaymentMode.Cash;
                            row["CCCharge"] = 0;
                            if (hdnPaymentDate.Value.Length > 0)
                            {
                                try
                                {
                                    row["NextPaymentDate"] = Convert.ToDateTime(hdnPaymentDate.Value, dateFormat);
                                }
                                catch { }
                            }
                            else
                            {
                                row["NextPaymentDate"] = DBNull.Value;
                            }
                            row["PromotionAmount"] = Convert.ToDecimal(discountAmount);
                            dtFDDetails.Rows.Add(row);
                        }
                        else if (mode == 2)
                        {
                            DataRow row = dtFDDetails.NewRow();
                            row["TranxId"] = -1;
                            row["ATHDId"] = activity.Id;
                            row["SettlementMode"] = (int)PaymentMode.Credit;
                            row["Amount"] = Convert.ToDecimal(txtCredit.Text.Trim());
                            row["BalanceAmount"] = Convert.ToDecimal(lblTotal.Text.Replace(Settings.LoginInfo.Currency, "")) - Convert.ToDecimal(txtCredit.Text.Trim());
                            row["Currency"] = Settings.LoginInfo.Currency;
                            row["ExchangeRate"] = Convert.ToDecimal(1.00);
                            row["CreditCardId"] = -1;
                            row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                            row["PaymentType"] = (int)PaymentMode.Credit;
                            row["CCCharge"] = 0;
                            if (hdnPaymentDate.Value.Length > 0)
                            {
                                try
                                {
                                    row["NextPaymentDate"] = Convert.ToDateTime(hdnPaymentDate.Value, dateFormat);
                                }
                                catch { }
                            }
                            else
                            {
                                row["NextPaymentDate"] = DBNull.Value;
                            }
                            row["PromotionAmount"] = Convert.ToDecimal(discountAmount);
                            dtFDDetails.Rows.Add(row);
                        }
                        else if (mode == 3)
                        {
                            DataTable dtCreditCards = CreditCardMaster.GetList(ListStatus.Short, RecordStatus.Activated);
                            DataRow row = dtFDDetails.NewRow();
                            row["TranxId"] = -1;
                            row["ATHDId"] = activity.Id;
                            row["SettlementMode"] = (int)PaymentMode.Card;
                            row["Amount"] = Convert.ToDecimal(txtCard.Text.Trim());
                            row["BalanceAmount"] = (Convert.ToDecimal(lblTotal.Text.Replace(Settings.LoginInfo.Currency, ""))) - Convert.ToDecimal(txtCard.Text.Trim());
                            row["Currency"] = Settings.LoginInfo.Currency;
                            row["ExchangeRate"] = Convert.ToDecimal(1.00);
                            row["CreditCardId"] = dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Id"];
                            row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                            row["CCCharge"] = Math.Ceiling(Convert.ToDecimal(txtCard.Text.Trim()) * Convert.ToDecimal(dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Charge"]) / 100);
                            row["PaymentType"] = (int)PaymentMode.Card;
                            if (hdnPaymentDate.Value.Length > 0)
                            {
                                try
                                {
                                    row["NextPaymentDate"] = Convert.ToDateTime(hdnPaymentDate.Value, dateFormat);
                                }
                                catch { }
                            }
                            else
                            {
                                row["NextPaymentDate"] = DBNull.Value;
                            }
                            row["PromotionAmount"] = Convert.ToDecimal(discountAmount);
                            dtFDDetails.Rows.Add(row);
                        }
                        else if (mode == 4)
                        {
                            DataRow row = dtFDDetails.NewRow();
                            row["TranxId"] = -1;
                            row["ATHDId"] = activity.Id;
                            row["SettlementMode"] = (int)PaymentMode.Cash_Credit;
                            row["Amount"] = Convert.ToDecimal(txtCash.Text.Trim());
                            row["BalanceAmount"] = Convert.ToDecimal(lblTotal.Text.Replace(Settings.LoginInfo.Currency, "")) - Convert.ToDecimal(txtCash.Text.Trim());
                            row["Currency"] = Settings.LoginInfo.Currency;
                            row["ExchangeRate"] = Convert.ToDecimal(1.00);
                            row["CreditCardId"] = -1;
                            row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                            row["PaymentType"] = (int)PaymentMode.Cash;
                            row["CCCharge"] = 0;
                            if (hdnPaymentDate.Value.Length > 0)
                            {
                                try
                                {
                                    row["NextPaymentDate"] = Convert.ToDateTime(hdnPaymentDate.Value, dateFormat);
                                }
                                catch { }
                            }
                            else
                            {
                                row["NextPaymentDate"] = DBNull.Value;
                            }
                            row["PromotionAmount"] = Convert.ToDecimal(discountAmount);
                            dtFDDetails.Rows.Add(row);

                            DataRow row1 = dtFDDetails.NewRow();
                            row1["TranxId"] = -1;
                            row1["ATHDId"] = activity.Id;
                            row1["SettlementMode"] = (int)PaymentMode.Cash_Credit;
                            row1["Amount"] = Convert.ToDecimal(txtCredit.Text.Trim());
                            row1["BalanceAmount"] = Convert.ToDecimal(lblTotal.Text.Replace(Settings.LoginInfo.Currency, "")) - Convert.ToDecimal(txtCash.Text.Trim()) - Convert.ToDecimal(txtCredit.Text.Trim());
                            row1["Currency"] = Settings.LoginInfo.Currency;
                            row1["ExchangeRate"] = Convert.ToDecimal(1.00);
                            row1["CreditCardId"] = -1;
                            row1["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                            row1["PaymentType"] = (int)PaymentMode.Credit;
                            row1["CCCharge"] = 0;
                            if (hdnPaymentDate.Value.Length > 0)
                            {
                                try
                                {
                                    row1["NextPaymentDate"] = Convert.ToDateTime(hdnPaymentDate.Value, dateFormat);
                                }
                                catch { }
                            }
                            else
                            {
                                row1["NextPaymentDate"] = DBNull.Value;
                            }
                            dtFDDetails.Rows.Add(row1);
                        }
                        else if (mode== 5)
                        {
                            DataTable dtCreditCards = CreditCardMaster.GetList(ListStatus.Short, RecordStatus.Activated);
                            DataRow row = dtFDDetails.NewRow();
                            row["TranxId"] = -1;
                            row["ATHDId"] = activity.Id;
                            row["SettlementMode"] = (int)PaymentMode.Cash_Card;
                            row["Amount"] = Convert.ToDecimal(txtCash.Text.Trim());
                            row["BalanceAmount"] = (Convert.ToDecimal(lblTotal.Text.Replace(Settings.LoginInfo.Currency, ""))) - Convert.ToDecimal(txtCash.Text.Trim());
                            row["Currency"] = Settings.LoginInfo.Currency;
                            row["ExchangeRate"] = Convert.ToDecimal(1.00);
                            row["CreditCardId"] = dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Id"]; ;
                            row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                            row["CCCharge"] = 0;// Math.Ceiling(Convert.ToDecimal(txtCard.Text.Trim()) * Convert.ToDecimal(dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Charge"]) / 100);
                            row["PaymentType"] = (int)PaymentMode.Cash;
                            if (hdnPaymentDate.Value.Length > 0)
                            {
                                try
                                {
                                    row["NextPaymentDate"] = Convert.ToDateTime(hdnPaymentDate.Value, dateFormat);
                                }
                                catch { }
                            }
                            else
                            {
                                row["NextPaymentDate"] = DBNull.Value;
                            }
                            row["PromotionAmount"] = Convert.ToDecimal(discountAmount);
                            dtFDDetails.Rows.Add(row);

                            DataRow row1 = dtFDDetails.NewRow();
                            row1["TranxId"] = -1;
                            row1["ATHDId"] = activity.Id;
                            row1["SettlementMode"] = (int)PaymentMode.Cash_Card;
                            row1["Amount"] = Convert.ToDecimal(txtCard.Text.Trim());
                            row1["BalanceAmount"] = (Convert.ToDecimal(lblTotal.Text.Replace(Settings.LoginInfo.Currency, "")) ) - Convert.ToDecimal(txtCash.Text.Trim()) - Convert.ToDecimal(txtCard.Text.Trim());
                            row1["Currency"] = Settings.LoginInfo.Currency;
                            row1["ExchangeRate"] = Convert.ToDecimal(1.00);
                            row1["CreditCardId"] = dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Id"]; ;
                            row1["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                            row1["CCCharge"] = Math.Ceiling(Convert.ToDecimal(txtCard.Text.Trim()) * Convert.ToDecimal(dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Charge"]) / 100);
                            row1["PaymentType"] = (int)PaymentMode.Card;
                            if (hdnPaymentDate.Value.Length > 0)
                            {
                                try
                                {
                                    row1["NextPaymentDate"] = Convert.ToDateTime(hdnPaymentDate.Value, dateFormat);
                                }
                                catch { }
                            }
                            else
                            {
                                row1["NextPaymentDate"] = DBNull.Value;
                            }
                            dtFDDetails.Rows.Add(row1);
                        }
                        else if (mode == 6)
                        {
                            DataTable dtCreditCards = CreditCardMaster.GetList(ListStatus.Short, RecordStatus.Activated);
                            DataRow row = dtFDDetails.NewRow();
                            row["TranxId"] = -1;
                            row["ATHDId"] = activity.Id;
                            row["SettlementMode"] = (int)PaymentMode.Cash_Credit_Card;
                            row["Amount"] = Convert.ToDecimal(txtCash.Text.Trim());
                            row["BalanceAmount"] = (Convert.ToDecimal(lblTotal.Text.Replace(Settings.LoginInfo.Currency, "")) ) - Convert.ToDecimal(txtCash.Text.Trim());
                            row["Currency"] = Settings.LoginInfo.Currency;
                            row["ExchangeRate"] = Convert.ToDecimal(1.00);
                            row["CreditCardId"] = dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Id"];
                            row["CCCharge"] = 0;// Math.Ceiling(Convert.ToDecimal(txtCard.Text.Trim()) * Convert.ToDecimal(dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Charge"]) / 100);
                            row["PaymentType"] = (int)PaymentMode.Cash;
                            if (hdnPaymentDate.Value.Length > 0)
                            {
                                try
                                {
                                    row["NextPaymentDate"] = Convert.ToDateTime(hdnPaymentDate.Value, dateFormat);
                                }
                                catch { }
                            }
                            else
                            {
                                row["NextPaymentDate"] = DBNull.Value;
                            }
                            row["PromotionAmount"] = Convert.ToDecimal(discountAmount);
                            row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                            dtFDDetails.Rows.Add(row);

                            DataRow row1 = dtFDDetails.NewRow();
                            row1["TranxId"] = -1;
                            row1["ATHDId"] = activity.Id;
                            row1["SettlementMode"] = (int)PaymentMode.Cash_Credit_Card;
                            row1["Amount"] = Convert.ToDecimal(txtCredit.Text.Trim());
                            row1["BalanceAmount"] = (Convert.ToDecimal(lblTotal.Text.Replace(Settings.LoginInfo.Currency, "")) ) - Convert.ToDecimal(txtCash.Text.Trim()) - Convert.ToDecimal(txtCredit.Text.Trim());
                            row1["Currency"] = Settings.LoginInfo.Currency;
                            row1["ExchangeRate"] = Convert.ToDecimal(1.00);
                            row1["CreditCardId"] = dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Id"]; ;
                            row1["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                            row1["CCCharge"] = 0;//Math.Ceiling(Convert.ToDecimal(txtCard.Text.Trim()) * Convert.ToDecimal(dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Charge"]) / 100);
                            row1["PaymentType"] = (int)PaymentMode.Credit;
                            if (hdnPaymentDate.Value.Length > 0)
                            {
                                try
                                {
                                    row1["NextPaymentDate"] = Convert.ToDateTime(hdnPaymentDate.Value, dateFormat);
                                }
                                catch { }
                            }
                            else
                            {
                                row1["NextPaymentDate"] = DBNull.Value;
                            }
                            dtFDDetails.Rows.Add(row1);

                            DataRow row2 = dtFDDetails.NewRow();
                            row2["TranxId"] = -1;
                            row2["ATHDId"] = activity.Id;
                            row2["SettlementMode"] = (int)PaymentMode.Cash_Credit_Card;
                            row2["Amount"] = Convert.ToDecimal(txtCard.Text.Trim());
                            row2["BalanceAmount"] = (Convert.ToDecimal(lblTotal.Text.Replace(Settings.LoginInfo.Currency, "")) ) - Convert.ToDecimal(txtCash.Text.Trim()) - Convert.ToDecimal(txtCredit.Text.Trim()) - Convert.ToDecimal(txtCard.Text.Trim());
                            row2["Currency"] = Settings.LoginInfo.Currency;
                            row2["ExchangeRate"] = Convert.ToDecimal(1.00);
                            row2["CreditCardId"] = dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Id"]; ;
                            row2["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                            row2["CCCharge"] = Math.Ceiling(Convert.ToDecimal(txtCard.Text.Trim()) * Convert.ToDecimal(dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Charge"]) / 100);
                            row2["PaymentType"] = (int)PaymentMode.Card;
                            if (hdnPaymentDate.Value.Length > 0)
                            {
                                try
                                {
                                    row2["NextPaymentDate"] = Convert.ToDateTime(hdnPaymentDate.Value, dateFormat);
                                }
                                catch { }
                            }
                            else
                            {
                                row2["NextPaymentDate"] = DBNull.Value;
                            }
                            dtFDDetails.Rows.Add(row2);
                        }
                        else if (mode == 7 || mode == 8)
                        {
                            DataRow row = dtFDDetails.NewRow();
                            row["TranxId"] = -1;
                            row["ATHDId"] = activity.Id;
                            row["SettlementMode"] = (int)PaymentMode.Employee;
                            row["Amount"] = Convert.ToDecimal(txtCash.Text.Trim());
                            row["BalanceAmount"] = Convert.ToDecimal(lblTotal.Text.Replace(Settings.LoginInfo.Currency, "")) - Convert.ToDecimal(txtCash.Text.Trim());
                            row["Currency"] = Settings.LoginInfo.Currency;
                            row["ExchangeRate"] = Convert.ToDecimal(1.00);
                            row["CreditCardId"] = -1;
                            row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                            row["PaymentType"] = (int)PaymentMode.Employee;
                            row["CCCharge"] = 0;
                            if (hdnPaymentDate.Value.Length > 0)
                            {
                                try
                                {
                                    row["NextPaymentDate"] = Convert.ToDateTime(hdnPaymentDate.Value, dateFormat);
                                }
                                catch { }
                            }
                            else
                            {
                                row["NextPaymentDate"] = DBNull.Value;
                            }
                            row["PromotionAmount"] = Convert.ToDecimal(discountAmount);
                            dtFDDetails.Rows.Add(row);
                        }
                        if (hdnPaymentDate.Value.Length == 0)
                        {
                            activity.TransactionHeader.Rows[0]["PaymentStatus"] = 0;
                        }
                        else
                        {
                            activity.TransactionHeader.Rows[0]["PaymentStatus"] = 1;
                        }
                        activity.FixedDepartureDetails = dtFDDetails;
                        activity.TransactionHeader.Rows[0]["IsFixedDeparture"] = "Y";

                        //Visa
                        if (txtVisaDate.Text.Length > 0 && txtVisaDate.Text != "DD/MM/YYYY" )
                        {
                            DataTable dtFDVisaFollowup = activity.FixedVisaFollowup;
                            dtFDVisaFollowup.Rows.Clear();
                            DataRow visaRow = dtFDVisaFollowup.NewRow();
                            visaRow["ATHId"] = -1;
                            visaRow["Visa_date"] = Convert.ToDateTime(txtVisaDate.Text, dateFormat);
                            visaRow["MailRequired"] = chkvisa.Checked == true ? "Y" : "N";
                            visaRow["Visa_created_by"] = (int)Settings.LoginInfo.UserID;
                            dtFDVisaFollowup.Rows.Add(visaRow);
                            activity.FixedVisaFollowup = dtFDVisaFollowup;
                        }
                        decimal discount=0;
                        foreach (DataRow row in activity.TransactionPrice.Rows)
                        {
                            if (ddlDiscount.SelectedIndex > 0)
                            {
                                if (txtCode.Text.Trim() == activity.DiscountCode && activity.Discount > 0)
                                {
                                    discount = activity.Discount / Convert.ToInt32(hdnTotalPaxCount.Value);
                                }
                                row["DiscountType"] = ddlDiscount.SelectedItem.Text;
                                row["DiscountRemarks"] = txtPlease.Text;
                                row["PromotionCode"] = txtCode.Text;
                                row["PromotionAmount"] = discount;
                            }
                            //row["ReceiptNo"] = txtReceiptNo.Text;
                        }
                        foreach (DataRow row in activity.TransactionDetail.Rows)
                        {
                            row["ReceiptNo"] = txtReceiptNo.Text;
                        }
                        
                    }
                    else
                    {
                        activity.TransactionHeader.Rows[0]["IsFixedDeparture"] = "N";
                    }
                    activity.AgencyId = (Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentID : Settings.LoginInfo.AgentId);
                    activity.SaveActivityTransaction();
                    //Invoice 
                    try
                    {
                        Invoice invoice = new Invoice();
                        int invoiceNumber = CT.BookingEngine.Invoice.isInvoiceGenerated(Convert.ToInt32(activity.TransactionHeader.Rows[0]["ATHId"]), CT.BookingEngine.ProductType.FixedDeparture);

                        if (invoiceNumber > 0)
                        {
                            invoice.Load(invoiceNumber);
                        }
                        else
                        {
                            invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(Convert.ToInt32(activity.TransactionHeader.Rows[0]["ATHId"]), "", (int)Settings.LoginInfo.UserID, CT.BookingEngine.ProductType.FixedDeparture, 1);
                            if (invoiceNumber > 0)
                            {
                                invoice.Load(invoiceNumber);
                                invoice.Status = CT.BookingEngine.InvoiceStatus.Paid;
                                invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                                invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                                invoice.UpdateInvoice();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message, Request.ServerVariables["REMOTE_ADDR"]);
                    }
                    if (txtVisaDate.Text != "DD/MM/YYYY")
                    {
                        if (chkvisa.Checked)
                        {
                            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                            toArray.Add(activity.TransactionDetail.Rows[0]["Email"].ToString());

                            string EmailText = "<table style='width:100%;'><tr><td>Dear &nbsp;" + activity.TransactionDetail.Rows[0]["FirstName"].ToString() + "</td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td>Below are the documents required from for visa application before (Date selected in calendar in Visa Date Follow-up):</td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Application form filled and signed</td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Visa fees ( non refundable )</td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Two Photos white background (PHOTO’S NOT OLDER THAN 6 MONTHS).</td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Original passport- must be valid for six months.</td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Passport older than 10 years will not be accepted. (Issued in 2004)</td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Residence Visa should be valid for three months from return date. </td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Six months bank statement with Bank stamp/original.</td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•NOC/Salary Certificate from the Company- if partner need trade license copy.</td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td>(Please note the above documents are subject to change as per the embassy terms, please re check with our visa consultant on the current requirements)</td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td>Thank you for choosing Cozmo Travel.</td></tr>"
                                        + "<tr><td>Holidays Specialist Team</td></tr></table>";

                            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEMail"], ConfigurationManager.AppSettings["holidaySupport"], toArray, "Visa FollowUP", EmailText, new Hashtable());
                        }
                    }
                    Response.Redirect("FixedDepartureVoucher.aspx?ID=" + Convert.ToInt64(activity.TransactionHeader.Rows[0]["ATHId"]), false);
                }
                else
                {
                    lblBalanceEtrror.Text = "Insufficient Credit Balance ! please contact Admin";
                }
            //}
            //else
            //{
            //    Response.Redirect("FixedDepartureResults.aspx", false);
            //}


        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GetBooking, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed FixedDeparture Booking. Error: " + ex.Message, Request["REMOTE_ADDR"]);
        }
    }
}
