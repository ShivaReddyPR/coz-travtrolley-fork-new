﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="RoleMasterGUI" Title="Role Master" Codebehind="RoleMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="InfoDynamic" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<div class=" body_container"> 

   <asp:HiddenField id="hdfDetailRowId" runat="server"></asp:HiddenField>
   
     <div class="paramcon" title="header"> 
         
          
           <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-5"> <asp:Label CssClass=" pull-right fl_xs" ID="lblRole" Text="Role:" runat="server"></asp:Label></div>
   
    <div class="col-md-2"> <asp:TextBox ID="txtRole"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox></div>

    <div class="clearfix"></div>
    </div>
    
   
           
     </div>
     
      

<center> 

  <table class="label" cellpadding="0" cellspacing="0"  border="0" width="100%">
                <tr>
             <td valign="top" align="left">
             
          <div style="height:400px;width:550px; border: solid 1px #ccc" class="grdScrlTrans">
             
          <asp:GridView ID="gvRoleDetails" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="rd_func_id" 
            EmptyDataText="No Role Details!" AutoGenerateColumns="false" PageSize="17" GridLines="none"  CssClass="grdTable"
            CellPadding="2" CellSpacing="0"
            OnPageIndexChanging="gvRoleDetails_PageIndexChanging" >
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label style="color:Black;">Select</label>
    <asp:CheckBox runat="server" id="HTchkSelectAll" AutoPostbACK="true" OnCheckedChanged="HTchkSelectAll_CheckedChanged"  ></asp:CheckBox>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" AutoPostbACK="true"   OnCheckedChanged="ITchkSelect_CheckedChanged" CssClass="InputEnabled" Checked='<%# Eval("checked_status").ToString()=="A"%>' ></asp:CheckBox>
    <%--<asp:Label ID="ITlblRemarks" runat="server" Text='<%# Eval("vs_remarks") %>' CssClass="label grdof"  ToolTip='<%# Eval("vs_remarks") %>' Width="150px"></asp:Label>--%>
    </ItemTemplate>    
    </asp:TemplateField>   
    
    
    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <label style=" float:left; text-align:left"><cc1:Filter ID="HTtxtPageName" Width="150px" HeaderText="Page Name" CssClass="inputEnabled width150" OnClick="Filter_Click" runat="server" /></label>                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblPageName" runat="server" Text='<%# Eval("PageName") %>' CssClass="label grdof" ToolTip='<%# Eval("PageName") %>' Width="150px"></asp:Label>
    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfPaxId" runat="server" Value='<%# Bind("pax_id") %>'></asp:HiddenField>--%>
    </ItemTemplate>    
    
    </asp:TemplateField>  
    
    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <label style=" float:left; text-align:left">
    <cc1:Filter ID="HTtxtParentMenu" Width="150px" HeaderText="ParentMenu" CssClass="inputEnabled width150" OnClick="Filter_Click" runat="server" />                 
   </label>
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblParentMenu" runat="server" Text='<%# Eval("ParentMenu") %>' CssClass="label grdof" ToolTip='<%# Eval("ParentMenu") %>' Width="175px"></asp:Label>
    </ItemTemplate>    
    
    </asp:TemplateField>  

          
    </Columns>           
    </asp:GridView>
                </div>   
              </td>
            </tr>
     
         </table>

<div>                 
               <asp:Button ID="btnSave" Text="Save" runat="server" Font-Bold="true" OnClientClick="return Save();" CssClass="button" OnClick ="btnSave_Click" ></asp:Button>
                    <asp:Button ID="btnClear" Text="Clear" Font-Bold="true" runat="server" CssClass="button" OnClick="btnClear_Click"></asp:Button>
                    <asp:Button ID="btnSearch" Text="Search" Font-Bold="true" runat="server" CssClass="button"  OnClick="btnSearch_Click"></asp:Button></div>

</center>

  <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>



</div>



<script type="text/javascript">



    function Save()
     {           
         
        if(getElement('txtRole').value=='' ) addMessage('Role cannot be blank!','');
       // if(getElement('txtMapLocation').value=='' ) addMessage('Mapping Location cannot be blank!','');
        //if(getElement('txtName').value =='' ) addMessage('Name cannot be blank!','');
        //if(getElement('hdfmapingDetailsMode').value!="0") addMessage('Mapping Location Details in Edit Mode!','');
     
    if(getMessage()!=''){ 
    alert(getMessage()); clearMessage(); return false;}
  }
   
//function validateDtlAddUpdate(action)
//{
//    try
//    {   
//           
//            var msg='';
//            var rowId = getElement('hdfDetailRowId').value;
//            
//            if(action=='I')//Insert
//            {     
//                 if(document.getElementById(rowId+'_FTtxtModule').value=='') msg+='Module cannot be blank !\n';                                 
//                 if(document.getElementById(rowId+'_FTtxtAccountCode').value=='') msg+='Account code cannot be blank !\n'; 
//                 if(document.getElementById(rowId+'_FTtxtGLCode').value=='') msg+='GL code cannot be blank !\n'; 
//                 if(document.getElementById(rowId+'_FTddlVisaElement').selectedIndex<=0) msg+='Please Select Visa Element from the list !\n';                
//                                  
//                 if(document.getElementById(rowId+'_FTddlVisaElement').value=='CASH_BOOK' || document.getElementById(rowId+'_FTddlVisaElement').value=='BANK_BOOK')
//                {
//                    if(document.getElementById(rowId+'_FTtxtDocType').value=='') msg+='Doc Type cannot be blank !\n';                               
//                    if(document.getElementById(rowId+'_FTddlSettleMode').selectedIndex<=0) msg+='Please Select Settlement Mode from the list !\n';                                 
//                }
//             }
//            
//            else if(action=='U')//Update
//            {    
//            
//                if(document.getElementById(rowId+'_EITtxtModule').value=='') msg+='Module cannot be blank !\n';                                    
//                if(document.getElementById(rowId+'_EITtxtAccountCode').value=='') msg+='Account code cannot be blank !\n'; 
//                if(document.getElementById(rowId+'_EITtxtGLCode').value=='') msg+='GL code cannot be blank !\n'; 
//                if(document.getElementById(rowId+'_EITddlVisaElement').selectedIndex<=0) msg+='Please Select Visa Element from the list !\n';      
//               
//                 if(document.getElementById(rowId+'_EITddlVisaElement').value=='CASH_BOOK' || document.getElementById(rowId+'_EITddlVisaElement').value=='BANK_BOOK')     
//                 {
//                    if(document.getElementById(rowId+'_EITtxtDocType').value=='') msg+='Doc Type cannot be blank !\n';            
//                    if(document.getElementById(rowId+'_EITddlSettleMode').selectedIndex<=0) msg+='Please Select Settlement Mode from the list !\n';                
//                 }              
//             
//            }
//         
//        if(msg!='')
//        {
//            alert(msg);
//            return false;
//        }
//        return true;
//    }
//    catch(e)
//    {
//        //alert(e.description);
//        return true;
//    }
//}

//Documents
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">

<div class=""> 
<asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="ROLE_ID" 
    EmptyDataText="No Roles List!" AutoGenerateColumns="false" PageSize="20" GridLines="none"  CssClass="grdTable"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
    OnPageIndexChanging="gvSearch_PageIndexChanging" >
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
    <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
    
     <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>    
    <cc1:Filter   ID="HTtxtRole"  Width="70px" HeaderText="Role" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblRole" runat="server" Text='<%# Eval("role_name") %>' CssClass="label grdof" ToolTip='<%# Eval("role_name") %>' Width="250px"></asp:Label>
    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>--%>
    </ItemTemplate>    
    
    </asp:TemplateField>          
            
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtStatus"  Width="150px" CssClass="inputEnabled" HeaderText="Status" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("role_status") %>' CssClass="label grdof"  ToolTip='<%# Eval("role_status") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
   
   
    </Columns>           
    </asp:GridView>
 
 </div>
</asp:Content>

