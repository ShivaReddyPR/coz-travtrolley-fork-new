﻿using CT.TicketReceipt.Common;
using CT.Core;
using CT.GlobalVisa;
using CT.GlobasVisa;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GVSalesList : CT.Core.ParentPage
{

   // DataTable dtVisaSalesList = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnExport);
            
            lblSuccessMsg.Text = string.Empty;
            StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");

            if (!IsPostBack)
            {
                InitializePageControls();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa,Severity.High,0,ex.ToString(),"0");
            throw;
        }
    }

    private void StartupScript(Page page, string script, string key)
    {

        script = string.Format("{0};", script);
        if (page != null && !string.IsNullOrEmpty(script) && !page.ClientScript.IsStartupScriptRegistered(key))
        {
            if (ScriptManager.GetCurrent(page) != null && ScriptManager.GetCurrent(page).IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(page, page.GetType(), key, script, true);
            else
                page.ClientScript.RegisterStartupScript(page.GetType(), key, script, true);

        }
    }

    private void InitializePageControls()
    {
        try
        {
            
            dcFromDate.Value = Utility.ToDate(DateTime.Now.Date);
            dcToDate.Value = Utility.ToDate(DateTime.Now.Date.ToString("MMM-dd-yyyy 23:59"));
            BindCountryList();
            BindNationality();
            BindAgentList();
            BindVisaType(ddlCountry.SelectedValue);
            BindDispatchStatus();

            int b2bAgentId;
            int b2b2bAgentId;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgent.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgent.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }
            BindB2BAgent(Convert.ToInt32(ddlAgent.SelectedValue));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedValue));
            BindLocation(Settings.LoginInfo.AgentId, string.Empty);
            BindConsultant(Settings.LoginInfo.AgentId);
            BindGrid();

        }
        catch
        { throw; }

    }
    
    private void BindCountryList()
    {
        try
        {
            ddlCountry.DataSource = Country.GetCountryList();
            ddlCountry.DataValueField = "Value";
            ddlCountry.DataTextField = "Key";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "-1"));
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }

    private void BindNationality()
    {
        try
        {
            ddlNationality.DataSource = Country.GetNationalityList();
            ddlNationality.DataTextField = "Key";
            ddlNationality.DataValueField = "Value";
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("--Select Nationality--", "-1"));
            ddlNationality.Items.Insert(1, new ListItem("All", "0"));
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }

    private void BindAgentList()
    {
        try
        {
            ddlAgent.DataSource = AgentMaster.GetList(1, Settings.LoginInfo.AgentType.ToString(), Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("-- All --", "-1"));
            if (Utility.ToInteger(Settings.LoginInfo.AgentId) > 1) ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }

    private void BindVisaType(string countryCode)
    {
        try
        {
            ddlVisaType.DataSource = VisaTypeMaster.GetCountryByVisaType(countryCode);
            ddlVisaType.DataTextField = "VISA_TYPE_NAME";
            ddlVisaType.DataValueField = "VISA_TYPE_ID";
            ddlVisaType.DataBind();
            ddlVisaType.Items.Insert(0, new ListItem("--Select Visa Type--", "-1"));
            ddlVisaType.Items.Insert(1, new ListItem("All", "0"));

        }
        catch(Exception ex) {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }
    private void BindDispatchStatus()
    {
        try
        {
            ddlDispStatus.DataSource = UserMaster.GetMemberTypeList("gv_dispatch_status");
            ddlDispStatus.DataValueField = "FIELD_VALUE";
            ddlDispStatus.DataTextField = "FIELD_TEXT";
            ddlDispStatus.DataBind();
            ddlDispStatus.Items.Insert(0, new ListItem("--Select Dispatch Status--", "-1"));
        }
        catch(Exception ex) {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }

    private void BindConsultant(int agentId)
    {
        try
        {
            ddlConsultant.DataSource = UserMaster.GetList(agentId, ListStatus.Short, RecordStatus.All);
            ddlConsultant.DataValueField = "USER_ID";
            ddlConsultant.DataTextField = "USER_FULL_NAME";
            ddlConsultant.DataBind();
            ddlConsultant.Items.Insert(0, new ListItem("--Select consultant--", "-1"));
        }
        catch(Exception ex) {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw ex;
        }
    }

    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw ex;
        }
    }

    private void BindLocation(int agentId, string type)
    {
        try
        {
            DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);

            ddlLocation.Items.Clear();
            ddlLocation.DataSource = dtLocations;
            ddlLocation.DataTextField = "location_name";
            ddlLocation.DataValueField = "location_id";
            ddlLocation.DataBind();
            
            ddlLocation.Items.Insert(0, new ListItem("--Select Location--", "-1"));
            ddlLocation.Items.Insert(1, new ListItem("All", "0"));
            hdfParam.Value = "0";
        }
        catch { throw; }
    }
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }

    private void BindGrid()
    {
        try
        {
            DateTime fromDate = Utility.ToDate(dcFromDate.Value);
            DateTime toDate = Utility.ToDate(dcToDate.Value);
            int agent = Utility.ToInteger(ddlAgent.SelectedValue);
            string agentType = string.Empty;
            if (agent == 0)
            {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = string.Empty;// null Means binding in list all BOOKINGS
                }
            }
            if (agent > 0 && ddlB2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else
                    {
                        agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }

            }
            if (agent > 0 && ddlB2B2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else
                {
                    agent = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                        agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                }
            }
            string country = ddlCountry.SelectedValue;
            string nationality = ddlNationality.SelectedValue;
            int visaType = Convert.ToInt32(ddlVisaType.SelectedValue);
            string dispatchStatus = ddlDispStatus.SelectedValue;
            int location = Convert.ToInt32(ddlLocation.SelectedValue);
            int consultant = Convert.ToInt32(ddlConsultant.SelectedValue);

            dtVisaSalesList = GVVisaSale.GetVisaSaleList(fromDate, toDate, agent, agentType, country, nationality, visaType, dispatchStatus, location, consultant);
            CommonGrid cg = new CommonGrid();
            cg.BindGrid(gvVisaSales, dtVisaSalesList);
            
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }

    

    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
            BindB2BAgent(agentId);
            BindB2B2BAgent(agentId);
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "BASE";
            }
            BindLocation(Utility.ToInteger(ddlAgent.SelectedItem.Value), type);
            BindConsultant(Utility.ToInteger(ddlAgent.SelectedItem.Value));
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }

    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
        string type = string.Empty;
        if (agentId >= 0)
        {
            BindB2B2BAgent(agentId);
            ddlB2B2BAgent.Enabled = true;
        }
        else
        {
            ddlB2B2BAgent.SelectedIndex = 0;
            ddlB2B2BAgent.Enabled = false;
        }
        if (agentId == 0)
        {
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
            {
                type = "AGENT";// AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations
                agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            }
            else
            {
                type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            }
        }
        else
        {
            if (agentId == -1)
            {
                agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "BASE";// BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations
                }
            }
        }
        if (Convert.ToInt32(ddlAgent.SelectedItem.Value) == 0)
        {
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
            {
                type = "BASEB2B";// BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations
            }
        }
        BindLocation(Utility.ToInteger(ddlAgent.SelectedItem.Value), type);
        BindConsultant(Utility.ToInteger(ddlAgent.SelectedItem.Value));
        hdfParam.Value = "0";
    }

    protected void ddlB2B2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "B2B2B";// B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                }
            }
            else if (agentId == -1)
            {
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = string.Empty;
                    }
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                }
            }
            BindLocation(agentId, type);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void gvVisaSales_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvVisaSales.PageIndex = e.NewPageIndex;
            gvVisaSales.EditIndex = -1;
            Filter_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }

    protected void gvVisaSales_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow gvSeletedRow = gvVisaSales.SelectedRow;
        long obVsId = Utility.ToLong(((HiddenField)gvSeletedRow.FindControl("IThdfVSId")).Value);
        string docNo = Utility.ToString(((Label)gvSeletedRow.FindControl("ITlblDocNo")).Text);
        long tranxAgentId = Utility.ToLong(((HiddenField)gvSeletedRow.FindControl("IThdfAgentId")).Value);

        GVVisaSale.deleteVisaSaleHeader(obVsId, Settings.LoginInfo.UserID, tranxAgentId);
        //string docNo = obVisaSales.DocNumber;
        lblSuccessMsg.Text = Formatter.ToMessage("GV Sales No: ", docNo, CT.TicketReceipt.Common.Action.Deleted);
        BindGrid();
        Filter_Click(null, null);
    }

    protected void Filter_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{ "HTtxtVSDate", "fvs_doc_date" },{ "HTtxtDocNo", "fvs_doc_no" },{"HTtxtTracking", "fpax_tracking_no" },
                { "HTtxtDispatchStatus", "fpax_dispatch_status" }, { "HTtxtDispRefNo", "FPAX_DOC_NO" },{ "HTtxtVSVisaType", "fvs_visa_type_name" },
                {"HTtxtDocketNo","FVS_DOCKET_NO"}, { "HttxtCountry", "COUNTRY_NAME" },
                { "HttxtNationality", "NATIONALITY_NAME" }, { "HttxtResidence", "FVS_RESIDENCE_NAME" },  { "HTtxtVisitorName", "FPAX_NAME" },
                {"HTtxtPaxType","FPAX_PAX_TYPE"}, { "HTtxtProfession", "FPAX_PROFESSION" }, { "HTtxtPassportNo", "fpax_passport_no" },{"HTtxtPhone","fpax_Phone"},
                { "HTtxtTotalVisaFee", "fvs_total_visa_fee" }, { "HTtxtCurrency", "fvs_currency_code" }, { "HTtxtModeDesc", "fvs_settlement_mode_description" },
                { "HTtxtCash", "fvs_cash_local_amount" },{ "HTtxtCredit", "fvs_credit_local_amount" }, { "HTtxtCard", "fvs_card_local_amount" },
              { "HTtxtEmployee", "fvs_employee_local_amount" },  { "HTtxtOthers", "fvs_others_local_amount" }, { "HTtxtModeRemraks", "fvs_mode_remarks" },
              { "HTtxtUser", "fVS_CREATED_NAME" }, { "HTtxtLocation", "fvs_location_name" },{"HTtxtAgent","fvs_agent_name"},{ "HTtxtStatus", "FVS_STATUS_NAME" },
            };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvVisaSales, dtVisaSalesList.Copy(), textboxesNColumns);
            // loadExcelData(((DataTable)gvVisaSales.DataSource).Copy());

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            //string script = "window.open('ExportExcelInsuranceList.aspx?','','width=0,height=0,toolbar=yes,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            //Utility.StartupScript(this.Page, script, "Excel");
            string attachment = "attachment; filename=VisaSalesList.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dgVisaSalesList.AllowPaging = false;
            dgVisaSalesList.DataSource = dtVisaSalesList;
            dgVisaSalesList.DataBind();
            dgVisaSalesList.RenderControl(htw);
            Response.Write(sw.ToString());

            //HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);


        }
        finally
        {
            Response.End();
        }
    }

    private DataTable dtVisaSalesList
    {
        get
        {
            return (DataTable)Session["GVSalesList"];
        }
        set
        {
            if (value == null)
                Session.Remove("GVSalesList");
            else
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["FPAX_ID"] };
                Session["GVSalesList"] = value;
            }
        }
    }


    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindVisaType(ddlCountry.SelectedValue);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }
}
