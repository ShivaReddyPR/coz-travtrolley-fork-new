﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

public partial class EnquiryQueueGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    PagedDataSource pagedData = new PagedDataSource();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnExport);
            if (!IsPostBack)
            {
                InitializePageControls();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void InitializePageControls()
    {
        try
        {
            Clear();
            txtFrom.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtTo.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            BindAgent();
            BindSearch();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(1,Settings.LoginInfo.AgentType.ToString(),Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agency--", "0"));
            ddlAgent.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void Clear()
    {
        ddlAgent.SelectedIndex = 0;
        ddlProduct.SelectedIndex = 0;
    }

    protected void btnSearch_OnClick(object sender, EventArgs e)
    {
        try
        {
            BindSearch();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindSearch()
    {
        try
        {

            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            string StartFromDate = (txtFrom.Text);
            DateTime fromDate = Convert.ToDateTime(StartFromDate, dateFormat);
            string StartToDate = (txtTo.Text);
            DateTime toDate = Convert.ToDateTime(StartToDate, dateFormat);
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            string productId=string.Empty;
            if (ddlProduct.SelectedItem.Value != "0")
            {
               productId = ddlProduct.SelectedItem.Value;
            }
            DataTable dtEnquiry = Enquiry.GetList(fromDate, toDate, agentId, productId, Settings.LoginInfo.UserID, Settings.LoginInfo.MemberType.ToString());
            BindEnquiry(dtEnquiry);
        }
        catch
        {
            throw;
        }
    }

    private void BindEnquiry(DataTable dtEnquiry)
    {
        try
        {
            if (dtEnquiry.Rows.Count > 0)
            {
                dlEnquiry.Visible = true;
                lblMessage.Visible = false;
                dlEnquiry.DataSource = dtEnquiry;
                ViewState["Enquiry"] = dtEnquiry;
                dlEnquiry.DataBind();
                doPaging();
            }
            else
            {
                dlEnquiry.Visible = false;
                btnPrev.Visible = false;
                btnFirst.Visible = false;
                btnLast.Visible = false;
                btnNext.Visible = false;
                lblMessage.Visible = true;
                lblMessage.Text = "No Results Found.";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }

    void doPaging()
    {
        DataTable pdt = (DataTable)ViewState["Enquiry"];
        if (pdt != null && pdt.Rows.Count > 0)
        {
            DataTable dt = (DataTable)ViewState["Enquiry"];
            pagedData.DataSource = dt.DefaultView;
        }
        pagedData.AllowPaging = true;
        pagedData.PageSize = 10;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        dlEnquiry.DataSource = pagedData;
        dlEnquiry.DataBind();
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }


    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }

    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }

    protected void btnOk_Click(object sender,EventArgs e)
    {
        try
        {
            int actId = Utility.ToInteger(hdnEnquiryId.Value);
            string remarks = txtRemarks.Text.Trim();
            Enquiry objEnquiry = new Enquiry();
            objEnquiry.SaveRemarks(actId, remarks, Settings.LoginInfo.UserID);
            hdnEnquiryId.Value = string.Empty;
            txtRemarks.Text = string.Empty;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    //For Export into Excel
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            //string script = "window.open('ExportExcelHotelAcctList.aspx?','','width=0,height=0,toolbar=yes,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            //Utility.StartupScript(this.Page, script, "Excel");
            string attachment = "attachment; filename=EnquiryQueueReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            dgEnquiryReportList.AllowPaging = false;
            dgEnquiryReportList.DataSource = ViewState["Enquiry"];
            dgEnquiryReportList.DataBind();
            dgEnquiryReportList.RenderControl(htw);
            Response.Write(sw.ToString());
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);


        }
        finally
        {
            Response.End();
        }

    }
    
}
