﻿using CT.BookingEngine;
//using CT.Core;
//using CT.DAL;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class OffLineEntryAjax : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Settings.LoginInfo != null)
        {
            if (Request["taxType"] !=null && Request["taxType"] =="GST")
            {  //Compute GST 

                List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                decimal gstAmount = 0;
                decimal markUP =Convert.ToDecimal(Request["totMarkup"]);
                int locationId = Convert.ToInt32(Request["locationId"]);
                gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, markUP, locationId);
                Response.Write(gstAmount);
            }
            else if (Request["origin"] != null && Request["destination"] != null)
            {

                Airport airportorigin = new Airport(Request["origin"].ToUpper());
                Airport airportDest = new Airport(Request["destination"].ToUpper());
                string destinationType = "";
                if (airportorigin.CountryCode == airportDest.CountryCode)
                    destinationType = "Domestic";
                else
                    destinationType = "International";
                string supplierCountryCode = string.Empty;
                PriceTaxDetails taxDetails = PriceTaxDetails.GetVATCharges(airportorigin.CountryCode, Settings.LoginInfo.LocationCountryCode, supplierCountryCode, (int)ProductType.Flight, Module.Ticket.ToString(), destinationType);
                string vatConfig = string.Empty;
                if (taxDetails != null && taxDetails.InputVAT != null && taxDetails.InputVAT.Applied)
                {
                    vatConfig = "I" + taxDetails.InputVAT.Charge + "|" + taxDetails.InputVAT.CostIncluded;
                }
                if (taxDetails != null && taxDetails.OutputVAT != null && taxDetails.OutputVAT.Applied)
                {
                    vatConfig += "#O" + taxDetails.OutputVAT.Charge + "|" + taxDetails.OutputVAT.AppliedOn;
                }
                Response.Write(vatConfig);
            }
        }
        else
        {
            Response.Write("Login Session expired");
        }
    }

   


}