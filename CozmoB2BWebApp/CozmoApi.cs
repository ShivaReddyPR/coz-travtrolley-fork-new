﻿using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

/// <summary>
/// Common class to make web api calls 
/// </summary>
public static class CozmoApi
{
    /// <summary>
    /// To get webapi token
    /// </summary>
    /// <param name="sAPIURL"></param>
    /// <param name="sUserName"></param>
    /// <param name="sPassword"></param>
    /// <param name="sSessionKey"></param>
    /// <returns></returns>
    public static string GetWebAPIToken(string sAPIURL, string sUserName, string sPassword, string sSessionKey)
    {
        string sToken = string.Empty;
        try
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(sAPIURL);

            HttpResponseMessage response =
              client.PostAsync("token",
                new StringContent(string.Format("grant_type=password&username={0}&password={1}",
                  HttpUtility.UrlEncode(sUserName),
                  HttpUtility.UrlEncode(sPassword)), Encoding.UTF8,
                  "application/x-www-form-urlencoded")).Result;

            string resultJSON = response.Content.ReadAsStringAsync().Result;
            LoginTokenResult result = JsonConvert.DeserializeObject<LoginTokenResult>(resultJSON);

            if (result != null && !string.IsNullOrEmpty(result.ErrorDescription))
                throw new Exception(result.Error + ": " + result.ErrorDescription);

            sToken = result.AccessToken;
            HttpContext.Current.Session[sSessionKey] = sToken;
        }
        catch (WebException wex)
        {
            throw wex;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return sToken;
    }

    /// <summary>
    /// Common method to make web api request calls
    /// </summary>
    /// <param name="sUrl"></param>
    /// <param name="sMethod"></param>
    /// <param name="liHeaders"></param>
    /// <param name="sReqName"></param>
    /// <param name="sData"></param>
    /// <param name="sToken"></param>
    /// <returns></returns>
    public static string WebReqCall(string sUrl, string sMethod, List<string> liHeaders, string sReqName, string sData, string sToken)
    {
        string JSresponse = string.Empty;
        try
        {
            ServicePointManager.SecurityProtocol = !ServicePointManager.SecurityProtocol.HasFlag(SecurityProtocolType.Tls12) ?
                ServicePointManager.SecurityProtocol | SecurityProtocolType.Tls12 : ServicePointManager.SecurityProtocol;

            string sAPIURL = ConfigurationManager.AppSettings["WebApiFlightUrl"];
            sMethod = string.IsNullOrEmpty(sMethod) ? "POST" : sMethod;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sUrl);
            request.Method = sMethod;
            request.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + sToken);
            request.ContentType = request.Accept = "application/json";

            if (liHeaders != null && liHeaders.Count > 0)
                liHeaders.ForEach(x => request.Headers.Add(x.Split('|')[0], x.Split('|')[1]));

            if (!string.IsNullOrEmpty(sData))
            {
                byte[] bytes = Encoding.ASCII.GetBytes(sData);
                request.ContentLength = bytes.Length;
                Stream requestWriter = (request.GetRequestStream());
                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();
            }

            string sReq = SerializeJson(request);

            HttpWebResponse clsHttpResp = (HttpWebResponse)request.GetResponse();

            Stream dataStream = clsHttpResp.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            JSresponse = reader.ReadToEnd();

            if (!string.IsNullOrEmpty(JSresponse))
            {
                var js = new JavaScriptSerializer();
                js.MaxJsonLength = Int32.MaxValue;
                ApplicationResponse clsRsp = js.Deserialize<ApplicationResponse>(JSresponse);
                JSresponse = clsRsp != null && clsRsp.Result != null ? JsonConvert.SerializeObject(clsRsp.Result) : string.Empty;
            }
        }
        catch (WebException ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Cozmo api call web exception at(ApiCommon.cs/WebReqCall). Api call name - (" + sUrl + "). Error : " + ex.ToString(), string.Empty);
            throw ex;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Cozmo api call exception at(ApiCommon.cs/WebReqCall). Api call name - (" + sUrl + "). Error : " + ex.ToString(), string.Empty);
            throw ex;
        }
        return JSresponse;
    }

    /// <summary>
    /// To serialize given object into Json string
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string SerializeJson<T>(T obj)
    {
        return JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
    }

    /// <summary>
    /// To get agent info session login object
    /// </summary>
    /// <returns></returns>
    public static ApiAgentInfo GetAgentInfo()
    {
        if (Settings.LoginInfo == null)
            throw new Exception("Login session expired");

        ApiAgentInfo clsAgentAuth = new ApiAgentInfo();

        clsAgentAuth.AgentId = Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentID : Settings.LoginInfo.AgentId;
        clsAgentAuth.LoginUserId = (int)Settings.LoginInfo.UserID;
        clsAgentAuth.OnBelahfAgentLoc = Settings.LoginInfo.OnBehalfAgentLocation;
        clsAgentAuth.IPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        clsAgentAuth.LoginUserCorpId = Settings.LoginInfo.CorporateProfileId;

        return clsAgentAuth;
    }
}

/// <summary>
/// A wrapper class for reading both errors or the result
/// </summary>
public class ApplicationResponse
{
    /// <summary>
    /// Gets the errors.
    /// </summary>
    /// <value>
    /// The errors.
    /// </value>
    public IEnumerable<KeyValuePair<string, string>> Errors { get; set; }

    /// <summary>
    /// Gets the result.
    /// </summary>
    /// <value>
    /// The result.
    /// </value>
    public object Result { get; set; }
}

/// <summary>
/// To read api token result
/// </summary>
public class LoginTokenResult
{
    public override string ToString()
    {
        return AccessToken;
    }

    [JsonProperty(PropertyName = "access_token")]
    public string AccessToken { get; set; }

    [JsonProperty(PropertyName = "error")]
    public string Error { get; set; }

    [JsonProperty(PropertyName = "error_description")]
    public string ErrorDescription { get; set; }

}

/// <summary>
/// To pass agent info in api request 
/// </summary>
public class ApiAgentInfo
{
    public int AgentId { get; set; }

    public int LoginUserId { get; set; }

    public int OnBelahfAgentLoc { get; set; }

    public string IPAddress { get; set; }

    public int LoginUserCorpId { get; set; }
}