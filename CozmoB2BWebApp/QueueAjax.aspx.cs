using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using CT.BookingEngine;
using CT.Core;
using CT.AccountingEngine;
using System.Transactions;
using System.Collections.Generic;
using System.Data.SqlClient;
using Visa;
using CT.TicketReceipt.BusinessLayer;


public partial class QueueAjax : System.Web.UI.Page
{
    protected Ticket[] ticket;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Convert.ToBoolean(Request["sendRequest"]))
        {
            int bookingId = Convert.ToInt32(Request["bookingId"]);
            string data = Request["requestText"].ToString();
            int requestTypeId = Convert.ToInt32(Request["requestTypeId"]);
            string[] ticketlist = (Request["selectedTicketId"].Remove(Request["selectedTicketId"].Length - 1, 1)).Split(',');
            FlightItinerary itinerary = null;
            AgentMaster AgentMaster = null;
            //string supplierName = null;
            for (int i = 0; i < ticketlist.Length; i++)
            {
                Ticket tempTicket = new Ticket();
                tempTicket.Load(Convert.ToInt32(ticketlist[i]));
                if (itinerary == null)
                {
                    itinerary = new FlightItinerary(tempTicket.FlightId);
                }
                if (AgentMaster == null)
                {
                    AgentMaster = new AgentMaster(itinerary.AgencyId);
                }
                //if(supplierName == null)
                //{
                // supplierName = Supplier.GetSupplierNameById(tempTicket.SupplierId);
                //}
                if (tempTicket.Status != "Voided" && tempTicket.Status != "Refunded")
                {
                    ServiceRequest serviceRequest = new ServiceRequest();
                    serviceRequest.BookingId = bookingId;
                    serviceRequest.ReferenceId = Convert.ToInt32(ticketlist[i]);
                    serviceRequest.ProductType = ProductType.Flight;
                    serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
                    serviceRequest.Data = data;
                    serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
                    serviceRequest.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);
                    serviceRequest.ItemTypeId = InvoiceItemTypeId.Ticketed;
                    serviceRequest.AgencyId = itinerary.AgencyId;
                    serviceRequest.AgencyTypeId = (Agencytype)AgentMaster.AgentType;
                    serviceRequest.Details = ServiceRequest.GenerateFlightDetailXML(tempTicket, itinerary, AgentMaster);
                    serviceRequest.IsDomestic = itinerary.IsDomestic;
                    serviceRequest.PaxName = tempTicket.Title + " " + tempTicket.PaxFirstName + " " + tempTicket.PaxLastName;
                    serviceRequest.Pnr = itinerary.PNR;
                    serviceRequest.ReferenceNumber = tempTicket.TicketNumber;
                    serviceRequest.Source = (int)itinerary.FlightBookingSource;
                    serviceRequest.PriceId = tempTicket.Price.PriceId;
                    serviceRequest.SupplierName = itinerary.AirlineCode;
                    serviceRequest.StartDate = itinerary.TravelDate;
                    serviceRequest.RequestSourceId = RequestSource.TBO;
                    serviceRequest.TicketStatus = tempTicket.Status;
                    serviceRequest.Tax = tempTicket.Price.Tax;
                    serviceRequest.PublishedFare = tempTicket.Price.PublishedFare;
                    serviceRequest.AirlineCode = itinerary.ValidatingAirline;

                    BookingHistory bh = new BookingHistory();
                    bh.BookingId = bookingId;
                    bh.EventCategory = EventCategory.Ticketing;
                    if (requestTypeId == 1)
                    {
                        bh.Remarks = "Request sent to Void " + tempTicket.TicketNumber;
                    }
                    else if (requestTypeId == 2)
                    {
                        bh.Remarks = "Request sent to Reissuance " + tempTicket.TicketNumber;
                    }
                    else
                    {
                        bh.Remarks = "Request sent to refund " + tempTicket.TicketNumber;
                    }
                    bh.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);

                    using (TransactionScope setTransaction = new TransactionScope())
                    {
                        try
                        {
                            serviceRequest.Save();
                            bh.Save();
                        }
                        catch (ArgumentException)
                        {
                        }
                        setTransaction.Complete();
                    }
                }
            }
            Response.Write(Request["index"]);
        }

        if (Convert.ToBoolean(Request["OffBookingSendRequest"]))
        {
            int bookingId = Convert.ToInt32(Request["bookingId"]);
            CT.ReadingEngine.OfflineBooking offBooking = new CT.ReadingEngine.OfflineBooking();
            offBooking.Load(bookingId);
            if (offBooking.Status != "Voided" && offBooking.Status != "Refunded")
            {
                string data = Request["requestText"].ToString();
                int requestTypeId = Convert.ToInt32(Request["requestTypeId"]);
                ServiceRequest serviceRequest = new ServiceRequest();
                serviceRequest.BookingId = bookingId;
                serviceRequest.ReferenceId = bookingId;
                serviceRequest.ProductType = ProductType.Flight;
                serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
                serviceRequest.Data = data;
                serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
                serviceRequest.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);
                serviceRequest.ItemTypeId = InvoiceItemTypeId.Offline;
                BookingHistory bh = new BookingHistory();
                bh.BookingId = bookingId;
                bh.EventCategory = EventCategory.Ticketing;
                if (requestTypeId == 2)
                {
                    bh.Remarks = "Request sent to Reissuance " + bookingId;
                }
                else
                {
                    bh.Remarks = "Request sent to refund " + bookingId;
                }
                bh.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);
                using (TransactionScope setTransaction = new TransactionScope())
                {
                    try
                    {
                        serviceRequest.Save();
                        bh.Save();

                    }
                    catch (ArgumentException)
                    {

                    }
                    setTransaction.Complete();
                }
            }
            Response.Write(Request["index"]);
        }


        if (Convert.ToBoolean(Request["voidRequest"]))
        {
            int loggedUserMasterId = Convert.ToInt32(Session["UserMasterId"]);
            int bookingId = Convert.ToInt32(Request["bookingId"]);
            string data = Request["data"].ToString();
            bool isLcc = Convert.ToBoolean(Request["isLcc"]);
            decimal totalVoidationCharges = 0;
            decimal[] voidationCharges = FlightItinerary.GetVoidationCharge(isLcc);
            if (voidationCharges.Length == 2)
            {
                totalVoidationCharges = voidationCharges[0] + voidationCharges[1];
            }
            string[] ticketIdlist = (Request["selectedTicketId's"].Remove(Request["selectedTicketId's"].Length - 1, 1)).Split(',');
            string refunded = string.Empty;
            string notRefunded = string.Empty;
            FlightItinerary itinerary = null;
            AgentMaster AgentMaster = null;
            string supplierName = null;
            for (int i = 0; i < ticketIdlist.Length; i++)
            {
                int ticketId = Convert.ToInt32(ticketIdlist[i]);
                Ticket ticket = new Ticket();
                ticket.Load(ticketId);

                if (ticket.Status != "Voided" && ticket.Status != "Refunded")
                {
                    //if (MetaSearchEngine.CancelTicket(Convert.ToInt32(ticketIdlist[i])))
                    if (itinerary == null)
                    {
                        itinerary = new FlightItinerary(ticket.FlightId);
                    }
                    if (AgentMaster == null)
                    {
                        AgentMaster = new AgentMaster(itinerary.AgencyId);
                    }
                    if (supplierName == null)
                    {
                        supplierName = Supplier.GetSupplierNameById(ticket.SupplierId);
                    }
                    if (false)
                    {
                        ServiceRequest serviceRequest = new ServiceRequest();
                        serviceRequest.BookingId = bookingId;
                        serviceRequest.ReferenceId = ticketId;
                        serviceRequest.ProductType = ProductType.Flight;
                        serviceRequest.RequestType = RequestType.Void;
                        serviceRequest.Data = data;
                        serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Completed;
                        serviceRequest.CreatedBy = loggedUserMasterId;
                        serviceRequest.ItemTypeId = InvoiceItemTypeId.Ticketed;

                        BookingHistory bh = new BookingHistory();
                        bh.BookingId = bookingId;
                        bh.EventCategory = EventCategory.Ticketing;
                        bh.Remarks = "Voided TicketNumber:" + ticket.TicketNumber;
                        bh.CreatedBy = loggedUserMasterId;
                        //TODO:here we will put the code for refunding the amount
                        if (ticket.Status == "Voided")
                        {
                            Response.Write("Already Voided");
                            Response.End();
                        }
                        string voided = string.Empty;
                        string voidedString = string.Empty;
                        string refundRemarks = "Voided for Ticket No -" + ticket.TicketNumber;
                        int AgencyId = Ticket.GetAgencyIdForTicket(ticketId);
                        if (ticket.Price.GetAgentPrice() > totalVoidationCharges)
                        {
                            decimal money = ticket.Price.GetAgentPrice() - totalVoidationCharges;
                            bool isLCC = false;
                            #region Enter in Payment Table
                            PaymentDetails pd = new PaymentDetails();
                            pd.AgencyId = AgencyId;
                            pd.PaymentDate = DateTime.Now.ToUniversalTime();
                            pd.Remarks = "Credit Note as Ticket Void";
                            pd.IsLCC = isLCC;
                            pd.Amount = money;
                            pd.RemainingAmount = money;
                            pd.OurBankDetailId = 2;
                            pd.ModeOfPayment = CT.AccountingEngine.PaymentMode.Credit;
                            pd.Invoices = new List<InvoiceCollection>();
                            pd.Status = PaymentStatus.Accepted;
                            pd.CreatedBy = loggedUserMasterId;
                            #endregion

                            #region Entry in Cancellation Charge Table
                            CancellationCharges cancellationCharge = new CancellationCharges();
                            if (voidationCharges.Length == 2)
                            {
                                cancellationCharge.AdminFee = voidationCharges[0];
                                cancellationCharge.SupplierFee = voidationCharges[1];
                            }
                            else
                            {
                                cancellationCharge.AdminFee = 0;
                                cancellationCharge.SupplierFee = 0;
                            }
                            cancellationCharge.ReferenceId = ticketId;
                            cancellationCharge.ProductType = ProductType.Flight;
                            cancellationCharge.ItemTypeId = InvoiceItemTypeId.Ticketed;
                            #endregion

                            
                            LedgerTransaction ledgerTxn = new LedgerTransaction();
                            ledgerTxn.LedgerId = AgencyId;
                            ledgerTxn.Amount = ticket.Price.GetAgentPrice();

                            ledgerTxn.ReferenceType = ReferenceType.TicketRefund;
                            ledgerTxn.IsLCC = isLCC;
                            ledgerTxn.Notes = "Ticket Voided";
                            ledgerTxn.Date = DateTime.UtcNow;
                            ledgerTxn.CreatedBy = loggedUserMasterId;

                            LedgerTransaction ledgerTxnCancelCharge = new LedgerTransaction();
                            NarrationBuilder objNarration = new NarrationBuilder();
                            ledgerTxnCancelCharge.LedgerId = AgencyId;
                            ledgerTxnCancelCharge.Amount = -(totalVoidationCharges);
                            objNarration.TicketNo = ticket.TicketNumber;

                            ledgerTxnCancelCharge.ReferenceType = ReferenceType.TicketCancellationCharge;
                            ledgerTxnCancelCharge.IsLCC = isLCC;
                            ledgerTxnCancelCharge.Notes = "";
                            ledgerTxnCancelCharge.Date = DateTime.UtcNow;
                            ledgerTxnCancelCharge.CreatedBy = loggedUserMasterId;

                            using (TransactionScope setTransaction = new TransactionScope())
                            {
                                try
                                {
                                    serviceRequest.Save();//service request save
                                    bh.Save();            //booking history save
                                    pd.Save();            //payment detail save
                                    cancellationCharge.PaymentDetailId = pd.PaymentDetailId;
                                    cancellationCharge.Save(); //entry in the cancellation charges table

                                    //TODO:here we will put the code for refunding the amount

                                    #region Two New Entries in LedgerTransaction One for ticket refund one for charge

                                    objNarration.Remarks = refundRemarks;
                                    ledgerTxn.Narration = objNarration;
                                    ledgerTxn.ReferenceId = pd.PaymentDetailId;

                                    ledgerTxn.Save();

                                    ledgerTxnCancelCharge.ReferenceId = pd.PaymentDetailId;
                                    objNarration.Remarks = "Cancellation Charges";
                                    ledgerTxnCancelCharge.Narration = objNarration;
                                    ledgerTxnCancelCharge.Save();
                                    #endregion

                                    

                                    #region Change Tikcet status to Voided
                                    voided = "Voided";
                                    Ticket.SetTicketStatus(ticketId, voided, loggedUserMasterId);
                                    #endregion

                                    #region Update Queue Status
                                    //Update Queue Status
                                    CT.Core.Queue.SetStatus(QueueType.Request, serviceRequest.RequestId, QueueStatus.Completed, loggedUserMasterId, 0, "Completed");
                                    #endregion
                                    setTransaction.Complete();
                                    if (refunded.Length > 0)
                                    {
                                        refunded = refunded + "~" + ticketId + "," + refundRemarks;

                                    }
                                    else
                                    {
                                        refunded = ticketId + "," + refundRemarks;

                                    }

                                }
                                catch (SqlException ex)
                                {
                                    throw new ArgumentException(ex.Message);

                                }
                            }
                        }
                        else
                        {

                            string notRefundRemarks = "Ticket cannot be voided:AgentPrice is less than voidation charges";
                            if (notRefunded.Length > 0)
                            {
                                notRefunded = notRefunded + "~" + ticketId + "," + notRefundRemarks;
                            }
                            else
                            {
                                notRefunded = ticketId + "," + notRefundRemarks;

                            }

                        }

                    }
                    else
                    {
                        ServiceRequest serviceRequest = new ServiceRequest();
                        serviceRequest.BookingId = bookingId;
                        serviceRequest.ReferenceId = ticketId;
                        serviceRequest.ProductType = ProductType.Flight;
                        serviceRequest.RequestType = RequestType.Void;
                        serviceRequest.Data = data;
                        serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
                        serviceRequest.CreatedBy = loggedUserMasterId;
                        serviceRequest.ItemTypeId = InvoiceItemTypeId.Ticketed;
                        serviceRequest.AgencyId = itinerary.AgencyId;
                        serviceRequest.AgencyTypeId = (CT.Core.Agencytype)AgentMaster.AgentType;
                        serviceRequest.Details = ServiceRequest.GenerateFlightDetailXML(ticket, itinerary, AgentMaster);
                        serviceRequest.IsDomestic = itinerary.IsDomestic;
                        serviceRequest.PaxName = ticket.Title + " " + ticket.PaxFirstName + " " + ticket.PaxLastName;
                        serviceRequest.Pnr = itinerary.PNR;
                        serviceRequest.ReferenceNumber = ticket.TicketNumber;
                        serviceRequest.Source = (int)itinerary.FlightBookingSource;
                        serviceRequest.PriceId = ticket.Price.PriceId;
                        serviceRequest.SupplierName = itinerary.AirlineCode;
                        serviceRequest.StartDate = itinerary.TravelDate;
                        serviceRequest.RequestSourceId = RequestSource.TBO;
                        serviceRequest.TicketStatus = ticket.Status;
                        serviceRequest.Tax = ticket.Price.Tax;
                        serviceRequest.PublishedFare = ticket.Price.PublishedFare;
                        serviceRequest.AirlineCode = itinerary.ValidatingAirline;

                        BookingHistory bh = new BookingHistory();
                        bh.BookingId = bookingId;
                        bh.EventCategory = EventCategory.Ticketing;
                        bh.Remarks = "Request sent to administrator for voidation" + ticket.TicketNumber;
                        bh.CreatedBy = loggedUserMasterId;
                        using (TransactionScope setTransaction = new TransactionScope())
                        {
                            try
                            {
                                serviceRequest.Save();
                                bh.Save();
                                //todo send a response
                                setTransaction.Complete();
                                string remarks = "Request sent to administrator for voiding Ticket No:" + ticket.TicketNumber;
                                if (notRefunded.Length > 0)
                                {
                                    notRefunded = notRefunded + "~" + ticketId + "," + remarks;
                                }
                                else
                                {
                                    notRefunded = ticketId + "," + remarks;

                                }
                            }
                            catch (ArgumentException ex)
                            {
                                throw new ArgumentException(ex.Message);
                            }
                        }

                    }
                }
                else
                {
                    notRefunded = ticketId + ", Already Refunded";
                }
            }

            Response.Write(refunded + "^" + notRefunded);
        }

        else if (Convert.ToBoolean(Request["sendHotelRequest"]))
        {
            int bookingId = Convert.ToInt32(Request["bookingId"]);
            string data = Request["requestText"].ToString();
            int loggedUserMasterId = Convert.ToInt32(Session["UserMasterId"]);
            int requestTypeId = Convert.ToInt16(Request["requestTypeId"]);
            AgentMaster AgentMaster = null;
            try
            {
                ServiceRequest serviceRequest = new ServiceRequest();
                BookingDetail booking = new BookingDetail();
                booking = new BookingDetail(bookingId);
                HotelItinerary itineary = new HotelItinerary();
                HotelRoom room = new HotelRoom();
                Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
                for (int i = 0; i < products.Length; i++)
                {
                    if (products[i].ProductTypeId == (int)ProductType.Hotel)
                    {
                        itineary.Load(products[i].ProductId);
                        itineary.Roomtype = room.Load(products[i].ProductId);
                        break;
                    }
                }
                if (requestTypeId == 4)
                {
                    booking.SetBookingStatus(BookingStatus.InProgress, loggedUserMasterId);
                }
                else
                {
                    booking.SetBookingStatus(BookingStatus.AmendmentInProgress, loggedUserMasterId);
                }
                AgentMaster = new AgentMaster(booking.AgencyId);                
                serviceRequest.BookingId = bookingId;
                serviceRequest.ReferenceId = itineary.Roomtype[0].RoomId;
                serviceRequest.ProductType = ProductType.Hotel;
                serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
                serviceRequest.Data = data;
                serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;
                serviceRequest.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);
                serviceRequest.AgencyId = (int)AgentMaster.ID;
                serviceRequest.IsDomestic = itineary.IsDomestic;
                serviceRequest.PaxName = itineary.Roomtype[0].PassenegerInfo[0].Firstname + itineary.Roomtype[0].PassenegerInfo[0].Lastname;
                serviceRequest.Pnr = itineary.ConfirmationNo;
                serviceRequest.Source = (int)itineary.Source;
                serviceRequest.StartDate = itineary.StartDate;
                serviceRequest.SupplierName = itineary.HotelName;
                serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
                serviceRequest.ReferenceNumber = itineary.BookingRefNo;
                serviceRequest.PriceId = itineary.Roomtype[0].PriceId;
                serviceRequest.AgencyTypeId = (CT.Core.Agencytype)AgentMaster.AgentType;
                serviceRequest.ItemTypeId = InvoiceItemTypeId.HotelBooking;
                serviceRequest.Save();
                BookingHistory bh = new BookingHistory();
                bh.BookingId = bookingId;
                bh.Remarks = "Request sent to " + serviceRequest.RequestType.ToString() + " of the hotel booking.";
                if (requestTypeId == 4)
                {
                    bh.EventCategory = EventCategory.HotelCancel;
                }
                else
                {
                    bh.EventCategory = EventCategory.HotelAmendment;
                }
                bh.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);
                bh.Save();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBook, Severity.High, 1, "Exception in the Hotel Change Request. Message:" + ex.Message, "");
                Response.Write("Exception occured in the change request");
            }
            Response.Write(Request["index"]);
        }
        else if (Convert.ToBoolean(Request["sendTransferRequest"]))
        {
            int bookingId = Convert.ToInt32(Request["bookingId"]);
            string data = Request["requestText"].ToString();
            int loggedUserMasterId = Convert.ToInt32(Session["UserMasterId"]);
            int requestTypeId = 4;

            try
            {
                ServiceRequest serviceRequest = new ServiceRequest();
                BookingDetail booking = new BookingDetail();
                booking = new BookingDetail(bookingId);
                TransferItinerary itineary = new TransferItinerary();
                Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
                for (int i = 0; i < products.Length; i++)
                {
                    if (products[i].ProductTypeId == (int)ProductType.Transfers)
                    {
                        itineary.Load(products[i].ProductId);
                        break;
                    }
                }
                booking.SetBookingStatus(BookingStatus.InProgress, loggedUserMasterId);
                serviceRequest.BookingId = bookingId;
                serviceRequest.ReferenceId = itineary.TransferId;
                serviceRequest.ProductType = ProductType.Transfers;
                serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
                serviceRequest.Data = data;
                serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;
                serviceRequest.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);

                serviceRequest.Save();
                BookingHistory bh = new BookingHistory();
                bh.BookingId = bookingId;
                bh.Remarks = "Request sent to cancel the Transfer booking of " + itineary.ConfirmationNo;
                bh.EventCategory = EventCategory.HotelCancel;
                bh.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);
                bh.Save();
            }
            catch (ArgumentException)
            {
            }




            Response.Write(Request["index"]);
        }
        else if (Convert.ToBoolean(Request["sendSightseeingRequest"]))
        {
            //int bookingId = Convert.ToInt32(Request["bookingId"]);
            //string data = Request["requestText"].ToString();
            //int loggedUserMasterId = Convert.ToInt32(Session["UserMasterId"]);
            //int requestTypeId = 4;

            //try
            //{
            //    ServiceRequest serviceRequest = new ServiceRequest();
            //    BookingDetail booking = new BookingDetail();
            //    booking = new BookingDetail(bookingId);
            //    SightseeingItinerary itinearary = new SightseeingItinerary(); ;
            //    Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
            //    for (int i = 0; i < products.Length; i++)
            //    {
            //        if (products[i].ProductTypeId == (int)ProductType.SightSeeing)
            //        {
            //            itinearary.Load(products[i].ProductId);
            //            break;
            //        }
            //    }
            //    booking.SetBookingStatus(BookingStatus.InProgress, loggedUserMasterId);
            //    serviceRequest.BookingId = bookingId;
            //    serviceRequest.ReferenceId = itinearary.SightseeingId;
            //    serviceRequest.ProductType = ProductType.SightSeeing;
            //    serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
            //    serviceRequest.Data = data;
            //    serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;
            //    serviceRequest.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);

            //    serviceRequest.Save();
            //    BookingHistory bh = new BookingHistory();
            //    bh.BookingId = bookingId;
            //    bh.Remarks = "Request sent to cancel the Sightseeing booking of " + itinearary.ConfirmationNo;
            //    bh.EventCategory = EventCategory.HotelCancel;
            //    bh.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);
            //    bh.Save();
            //}
            //catch (ArgumentException)
            //{
            //}




            //Response.Write(Request["index"]);
        }

        if (Convert.ToBoolean(Request["updateQueueStatus"]))
        {
            //TODO:Send Message to Agent
            QueueStatus status;
            bool hasTranCompleted = false;
            int previousStatusId = Convert.ToInt32(Request["previousStatusId"]);
            int requestId = Convert.ToInt32(Request["requestId"]);
            string queueStatus = Request["serviceStatus"];
            string remarks = Request["remarks"];

            status = (QueueStatus)Enum.Parse(typeof(QueueStatus), queueStatus);

            int lastModifiedBy = Convert.ToInt32(Session["UserMasterId"]);
            bool isPrivatecomment = Convert.ToBoolean(Request["isPrivatecomment"]);
            int assignedTo = Convert.ToInt32(Request["assignedTo"]);

            int requestStatus = (int)(ServiceRequestStatus)Enum.Parse(typeof(ServiceRequestStatus), queueStatus);
            #region Sending Email
            string pnr = Request["pnr"];
            string messgageString = string.Empty;
            //string messageString = string.Empty;

            int AgencyId = Convert.ToInt32(Request["AgencyId"]);
            string subjectString = string.Empty;
            //FlightPassenger fp = new FlightPassenger();

            //string custEmail=FlightPassenger.GetPassengers(Convert.ToInt32(Request["flightId"]))[0].Email;

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["isSendEmail"]) && ((status == QueueStatus.Rejected) || (status == QueueStatus.Closed)))
            {
                Hashtable table = new Hashtable(4);
                UserMaster userMaster = new UserMaster(UserMaster.GetPrimaryUserId(AgencyId));                
                table.Add("firstName", userMaster.FirstName);
                table.Add("lastName", userMaster.LastName);
                table.Add("pnr", pnr);
                table.Add("remarks", remarks);
                table.Add("paxName", Request["paxName"]);
                table.Add("DOT", Request["dateOfTravel"]);
                table.Add("airline", Request["airline"]);
                table.Add("sector", Request["sector"]);
                table.Add("processedBy", new UserMaster(lastModifiedBy).FirstName);
                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                //System.Collections.Generic.List<string> toArrays = new System.Collections.Generic.List<string>();
                toArray.Add(userMaster.Email);
                //UserPreference uP =new UserPreference();

                // uP=uP.GetPreference(UserMaster.UserMasterId, "SITENAME", "SETTINGS");


                ////uP.pref


                //if (uP.Value.Contains  ("oxi"))
                //{
                //    toArrays.Add(custEmail);
                //    toArray.Add("testoxitravel@myoxigen.com");
                //}

                if (status == QueueStatus.Closed)
                {
                    subjectString = "Request Processed Successfully";
                    messgageString = "processedRequest";
                    //messageString = "processedRequests";
                }
                else
                {
                    subjectString = "Request Rejected";
                    messgageString = "rejectMessage";
                    //messageString = "rejectMessages";
                }

                try
                {
                    Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, subjectString, ConfigurationManager.AppSettings[messgageString], table);
                    //Email.Send(ConfigurationManager.AppSettings["fromEmails"], ConfigurationManager.AppSettings["replyTos"], toArrays, subjectString, ConfigurationManager.AppSettings[messageString], table);
                }
                catch (System.Net.Mail.SmtpException)
                {
                    Audit.Add(EventType.Email, Severity.Normal, 0, "Smtp is unable to send the message", "");
                }
            }
            #endregion

            Comment comment = new Comment();
            comment.CommentDescription = remarks;
            comment.ParentId = Convert.ToInt32(Request["bookingId"]);
            comment.CommentTypes = CommmentType.Booking;
            comment.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);
            comment.LastModifiedBy = Convert.ToInt32(Session["UserMasterId"]);
            comment.IsPublic = isPrivatecomment;
            comment.ParentCommentId = 0;
            comment.AgencyId = Convert.ToInt32(Session["AgencyId"]);

            ServiceRequestAssignmentLog srLog = new ServiceRequestAssignmentLog();
            srLog.RequestId = requestId;
            srLog.ServiceRequestStatusId = requestStatus;
            srLog.AssignedTo = assignedTo;
            srLog.AssignedBy = Convert.ToInt32(Session["UserMasterId"]);
            srLog.ModifiedBy = Convert.ToInt32(Session["UserMasterId"]);


            BookingHistory bookingHistory = new BookingHistory();
            bookingHistory.BookingId = Convert.ToInt32(Request["bookingId"]);
            bookingHistory.EventCategory = EventCategory.Ticketing;


            if (status == QueueStatus.Completed)
            {
                bookingHistory.Remarks = "Assigned To: " + new UserMaster(lastModifiedBy).FirstName + " Status : " + queueStatus;

            }
            else
            {
                bookingHistory.Remarks = "Assigned To: " + Request["assignedToName"].ToString() + " Status : " + queueStatus;
            }

            bookingHistory.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);
            //bookingHistory.Remarks=""

            using (TransactionScope setTransaction = new TransactionScope())
            {
                #region Saving Comment

                comment.AddComment();

                #endregion
                //Save Log of Change Request
                #region Saving Log

                srLog.Save();
                #endregion
                //Update Service Request Assignment
                #region Update Service Request
                ServiceRequest serviceRequest = new ServiceRequest();
                serviceRequest.Comments = comment.CommentDescription;
                serviceRequest.UpdateServiceRequestAssignment(requestId, (int)requestStatus, lastModifiedBy, requestStatus, null);
                # endregion

                bookingHistory.Save();
                //Update Queue Status
                CT.Core.Queue.SetStatus(QueueType.Request, requestId, status, lastModifiedBy, assignedTo, remarks);
                setTransaction.Complete();
                hasTranCompleted = true;
            }

            string serviceReq = Request["serviceRequestType"];
            if ((previousStatusId != (int)ServiceRequestStatus.Completed && status == QueueStatus.Closed && Request["serviceRequestType"].Equals("Reissuance", StringComparison.OrdinalIgnoreCase)) || (status == QueueStatus.Completed && Request["serviceRequestType"].Equals("Reissuance", StringComparison.OrdinalIgnoreCase)))
            {
                RefreshPNR();
            }
            #region Sending SMS
            if (hasTranCompleted)
            {
                ////AgentMaster AgentMaster = new AgentMaster();
                //Address address = new Address();
                //UserMaster UserMaster = new UserMaster();
                //UserMaster.Load(UserMaster.GetPrimaryUserMasterId(AgencyId));

                ////AgentMaster.Load(AgencyId);
                //address.Load(UserMaster.AddressId);

                //Hashtable hTable = new Hashtable();
                //hTable.Add("AgencyId", AgencyId);
                //hTable.Add("TicketNo", Request["ticketNumber"]);
                //hTable.Add("PNR", pnr);
                //hTable.Add("AirLine", Request["airline"]);
                //hTable.Add("PaxName", Request["paxName"]);
                //hTable.Add("MobileNo", address.Mobile);

                //hTable.Add("EventType", SMSEvent.RejectChangeRequestAir);
                //SMS.SendSMSThread(hTable);
            }
            # endregion
        }
        if (Convert.ToBoolean(Request["updateHotelQueueStatus"]))
        {
            QueueStatus status;
            int previousStatusId = Convert.ToInt32(Request["previousStatusId"]);

            int requestId = Convert.ToInt32(Request["requestId"]);
            string queueStatus = Request["serviceStatus"];
            string remarks = Request["remarks"];

            status = (QueueStatus)Enum.Parse(typeof(QueueStatus), queueStatus);

            int lastModifiedBy = Convert.ToInt32(Session["UserMasterId"]);
            bool isPrivatecomment = Convert.ToBoolean(Request["isPrivatecomment"]);
            int assignedTo = Convert.ToInt32(Request["assignedTo"]);

            int requestStatus = (int)(ServiceRequestStatus)Enum.Parse(typeof(ServiceRequestStatus), queueStatus);
            Comment comment = new Comment();
            comment.CommentDescription = remarks;
            comment.ParentId = Convert.ToInt32(Request["bookingId"]);
            comment.CommentTypes = CommmentType.Booking;
            comment.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);
            comment.LastModifiedBy = Convert.ToInt32(Session["UserMasterId"]);
            comment.IsPublic = isPrivatecomment;
            comment.ParentCommentId = 0;
            comment.AgencyId = Convert.ToInt32(Session["AgencyId"]);

            ServiceRequestAssignmentLog srLog = new ServiceRequestAssignmentLog();
            srLog.RequestId = requestId;
            srLog.ServiceRequestStatusId = requestStatus;
            srLog.AssignedTo = assignedTo;
            srLog.AssignedBy = Convert.ToInt32(Session["UserMasterId"]);
            srLog.ModifiedBy = Convert.ToInt32(Session["UserMasterId"]);


            BookingHistory bookingHistory = new BookingHistory();
            bookingHistory.BookingId = Convert.ToInt32(Request["bookingId"]);
            if (Request["requestType"] == "HotelCancel")
            {
                bookingHistory.EventCategory = EventCategory.HotelCancel;
            }
            else if (Request["requestType"] == "HotelAmendment")
            {
                bookingHistory.EventCategory = EventCategory.HotelAmendment;
            }

            if (status == QueueStatus.Completed)
            {
                bookingHistory.Remarks = "Assigned To: " + new UserMaster(lastModifiedBy) + " Status : " + queueStatus;

            }
            else
            {
                bookingHistory.Remarks = "Assigned To: " + Request["assignedToName"].ToString() + " Status : " + queueStatus;
            }

            bookingHistory.CreatedBy = Convert.ToInt32(Session["UserMasterId"]);
            //bookingHistory.Remarks=""

            using (TransactionScope setTransaction = new TransactionScope())
            {
                #region Saving Comment

                comment.AddComment();

                #endregion
                //Save Log of Change Request
                #region Saving Log

                srLog.Save();
                #endregion
                //Update Service Request Assignment
                #region Update Service Request
                ServiceRequest serviceRequest = new ServiceRequest();
                serviceRequest.UpdateServiceRequestAssignment(requestId, (int)requestStatus, lastModifiedBy, requestStatus, null);
                # endregion

                bookingHistory.Save();
                //Update Queue Status
                CT.Core.Queue.SetStatus(QueueType.Request, requestId, status, lastModifiedBy, assignedTo, remarks);
                setTransaction.Complete();
            }

        }
        if (Convert.ToBoolean(Request["updateHotelQueueStatusOffBooking"]))
        {
            //TODO:Send Message to Agent
            QueueStatus status;
            int requestId = Convert.ToInt32(Request["requestId"]);
            string queueStatus = Request["serviceStatus"];
            string remarks = Request["remarks"];

            status = (QueueStatus)Enum.Parse(typeof(QueueStatus), queueStatus);

            int lastModifiedBy = Convert.ToInt32(Session["UserMasterId"]);
            bool isPrivatecomment = Convert.ToBoolean(Request["isPrivatecomment"]);
            int assignedTo = Convert.ToInt32(Request["assignedTo"]);

            int requestStatus = (int)(ServiceRequestStatus)Enum.Parse(typeof(ServiceRequestStatus), queueStatus);



            ServiceRequestAssignmentLog srLog = new ServiceRequestAssignmentLog();
            srLog.RequestId = requestId;
            srLog.ServiceRequestStatusId = requestStatus;
            srLog.AssignedTo = assignedTo;
            srLog.AssignedBy = Convert.ToInt32(Session["UserMasterId"]);
            srLog.ModifiedBy = Convert.ToInt32(Session["UserMasterId"]);

            using (TransactionScope setTransaction = new TransactionScope())
            {

                #region Saving Log
                srLog.Save();
                #endregion
                //Update Service Request Assignment
                #region Update Service Request
                ServiceRequest serviceRequest = new ServiceRequest();
                serviceRequest.UpdateServiceRequestAssignment(requestId, (int)requestStatus, lastModifiedBy, requestStatus, null);
                # endregion

                //Update Queue Status
                CT.Core.Queue.SetStatus(QueueType.Request, requestId, status, lastModifiedBy, assignedTo, remarks);
                setTransaction.Complete();
            }
        }

        if (Convert.ToBoolean(Request["updateQueueStatusOffBooking"]))
        {
            //TODO:Send Message to Agent
            QueueStatus status;
            int requestId = Convert.ToInt32(Request["requestId"]);
            string queueStatus = Request["serviceStatus"];
            string remarks = Request["remarks"];

            status = (QueueStatus)Enum.Parse(typeof(QueueStatus), queueStatus);

            int lastModifiedBy = Convert.ToInt32(Session["UserMasterId"]);
            bool isPrivatecomment = Convert.ToBoolean(Request["isPrivatecomment"]);
            int assignedTo = Convert.ToInt32(Request["assignedTo"]);

            int requestStatus = (int)(ServiceRequestStatus)Enum.Parse(typeof(ServiceRequestStatus), queueStatus);

            #region Sending Email
            string ticketNo = Request["ticketNo"];
            string messgageString = string.Empty;
            int UserMasterId = Convert.ToInt32(Request["UserMasterId"]);
            string subjectString = string.Empty;
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["isSendEmail"]) && ((status == QueueStatus.Rejected) || (status == QueueStatus.Closed)))
            {
                Hashtable table = new Hashtable(4);
                UserMaster UserMaster = new UserMaster(UserMasterId);
                table.Add("firstName", UserMaster.FirstName);
                table.Add("lastName", UserMaster.LastName);
                table.Add("ticketNo", ticketNo);
                table.Add("remarks", remarks);
                table.Add("paxName", Request["paxName"]);
                table.Add("airlineCode", Request["airlineCode"]);
                table.Add("sector", Request["sector"]);
                table.Add("processedBy", new UserMaster(lastModifiedBy).FirstName);
                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();

                toArray.Add(UserMaster.Email);


                if (status == QueueStatus.Closed)
                {
                    subjectString = "Request Processed Successfully";
                    messgageString = "processedRequestOffBooking";

                }
                else
                {
                    subjectString = "Request Rejected";
                    messgageString = "rejectMessageOffBooking";

                }
                try
                {
                    Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, subjectString, ConfigurationManager.AppSettings[messgageString], table);




                }
                catch (System.Net.Mail.SmtpException)
                {
                    Audit.Add(EventType.Email, Severity.Normal, 0, "Smtp is unable to send the message", "");
                }
            }
            #endregion

            ServiceRequestAssignmentLog srLog = new ServiceRequestAssignmentLog();
            srLog.RequestId = requestId;
            srLog.ServiceRequestStatusId = requestStatus;
            srLog.AssignedTo = assignedTo;
            srLog.AssignedBy = Convert.ToInt32(Session["UserMasterId"]);
            srLog.ModifiedBy = Convert.ToInt32(Session["UserMasterId"]);

            using (TransactionScope setTransaction = new TransactionScope())
            {

                #region Saving Log
                srLog.Save();
                #endregion
                //Update Service Request Assignment
                #region Update Service Request
                ServiceRequest serviceRequest = new ServiceRequest();
                serviceRequest.UpdateServiceRequestAssignment(requestId, (int)requestStatus, lastModifiedBy, requestStatus, null);
                # endregion

                //Update Queue Status
                CT.Core.Queue.SetStatus(QueueType.Request, requestId, status, lastModifiedBy, assignedTo, remarks);
                setTransaction.Complete();
            }
        }

        if (Convert.ToBoolean(Request["rejectPayment"]))
        {
            int paymentDetailId = Convert.ToInt32(Request["paymentDetailId"]);
            string remarks = Request["remarks"];
            int lastModifiedBy = Convert.ToInt32(Session["UserMasterId"]);
            PaymentDetails.SetStatus(paymentDetailId, PaymentStatus.Rejected, remarks, lastModifiedBy);
            CT.Core.Queue.SetStatus(QueueType.Payment, paymentDetailId, QueueStatus.Suspended, lastModifiedBy);
            PaymentDetails pd = new PaymentDetails();
            pd.Load(paymentDetailId);
            string responseString = string.Empty;
            //If there will be more mode of payment we need to change this string as well
            if (pd.ModeOfPayment != CT.AccountingEngine.PaymentMode.Cash)
            {
                responseString = pd.ModeOfPayment.ToString() + " No - " + pd.ReferenceNo.ToString() + "|" + paymentDetailId.ToString();
            }
            else
            {
                responseString = "Cash (" + Settings.LoginInfo.Currency + "" + pd.Amount.ToString("N2") + ")" + "|" + paymentDetailId.ToString();
            }
            Response.Write(responseString);
        }
        if (Convert.ToBoolean(Request["cancelBooking"]))
        {
            //TODO:Delete itinerary from WorldSpan
            //TODO:Send Message to Agent    
            int requestId = Convert.ToInt32(Request["bookingId"]);
            string statusString = Request["status"];
            QueueStatus status = (QueueStatus)Enum.Parse(typeof(QueueStatus), statusString);
            int lastModifiedBy = Convert.ToInt32(Session["UserMasterId"]);
            CT.Core.Queue.SetStatus(QueueType.Booking, requestId, status, lastModifiedBy);
        }
        


        if (Convert.ToBoolean(Request["isReadOnlyPaxProfile"]))
        {
            VisaPassenger visaPassenger = new VisaPassenger();
            Address paxAddress = new Address();
            if (Request["paxId"] != null)
            {
                visaPassenger.Load(Convert.ToInt32(Request["paxId"]));
            }
            paxAddress = visaPassenger.Address;
            string readOnlyBlock = string.Empty;
            string nationality = string.Empty;

            if (visaPassenger.Nationality != null && visaPassenger.Nationality != "")
            {
                VisaNationality visaNationality = new VisaNationality(visaPassenger.Nationality);
                nationality = visaNationality.NationalityName;
            }
            else
            {
                nationality = visaPassenger.NationalityOther;
            }
            readOnlyBlock = "<div class=\"agency_summary\"><h1>" + visaPassenger.Firstname + " " + visaPassenger.LastName + "</h1>";
            readOnlyBlock += "<span><label>" + "<img class=\"hand margin-right-50\" alt=\"close\" src=\"Images/close.gif\" onclick=\"hideDiv('" + Request["blockId"].ToString() + "')\" />";
            readOnlyBlock += "<em>" + nationality + "</em>" + "<b> Nationality : </b>";
            readOnlyBlock += "</span></div><div class=\"agency_holder_content\">";
            //readOnlyBlock += "<span><label style='width:111px;'><b>Father Name:</b></label><em>" + visaPassenger.FatherName + "</em></span>";
            //readOnlyBlock += "<span><label style='width:111px;'><b>Mother Name :</b></label><em>" + visaPassenger.MotherName + "</em></span>";
            //readOnlyBlock += "<span><label style='width:111px;'><b>Date of Birth:</b></label><em>" + Util.UTCToIST(Convert.ToDateTime(visaPassenger.Dob)).ToString("dd/MM/yyyy") + "</em></span>";
            //readOnlyBlock += "<span><label style='width:111px;'><b>Sex:</b></label><em>" + visaPassenger.Sex + "</em></span>";

            readOnlyBlock += "<span><label style='width:111px;'><b>Mobile No:</b></label><em>" + visaPassenger.ContactNoCountryCode + "-" + visaPassenger.ContactNo + "</em></span>";
            readOnlyBlock += "<span><label style='width:111px;'><b>Email Id:</b></label><em>" + visaPassenger.EmailId + "</em></span>";
            readOnlyBlock += "</div>";
            if (!string.IsNullOrEmpty(paxAddress.Line1) && !string.IsNullOrEmpty(paxAddress.CityName))
            {
                readOnlyBlock += "<div class=\"agency_address_content\"><p>Pax Address</p>";
                readOnlyBlock += "<span><label>" + paxAddress.Line1.Trim() + "</label></span>";
                readOnlyBlock += "<span><label>" + paxAddress.Line2 + "</label></span>";
                readOnlyBlock += "<span><label>" + paxAddress.CityName + "</label></span>";
                readOnlyBlock += "<span><label>" + Country.GetCountry(paxAddress.CountryCode).CountryName + " </label></span>";
                readOnlyBlock += "<span><label>Ph:" + paxAddress.Phone + " </label></span>";
                readOnlyBlock += "</div>";
            }
            readOnlyBlock += "";
            Response.Write(readOnlyBlock);
        }
        //Code to display Guarantor info............
        if (Convert.ToBoolean(Request["isVisaGuarantor"]))
        {
            VisaPassenger visaPassenger = new VisaPassenger();
            Address paxAddress = new Address();
            if (Request["paxId"] != null)
            {
                visaPassenger.Load(Convert.ToInt32(Request["paxId"]));
            }
            paxAddress = visaPassenger.Address;
            string readOnlyBlock = string.Empty;
            string nationality = string.Empty;
            if (visaPassenger.AdditionalInfo.GuranterNationality != null && visaPassenger.AdditionalInfo.GuranterNationality != "")
            {
                VisaNationality visaNationality = new VisaNationality(visaPassenger.AdditionalInfo.GuranterNationality);
                nationality = visaNationality.NationalityName;
            }
            else
            {
                nationality = visaPassenger.AdditionalInfo.GuranterNationalityOther;
            }
            readOnlyBlock = "<div class=\"agency_summary\"><h1>Guarantor Details </h1>";
            readOnlyBlock += "<span><label><b> </b>";
            readOnlyBlock += "<img class=\"hand margin-right-50\" alt=\"close\" src=\"Images/close.gif\" onclick=\"hideDiv('" + Request["blockId"].ToString() + "')\" />";
            readOnlyBlock += "</span></div><div class=\"agency_holder_content\">";
            readOnlyBlock += "<span><label style='width:129px;'><b>Guarantor name:</b></label><em>" + visaPassenger.AdditionalInfo.GuranterName + "</em></span>";
            readOnlyBlock += "<span><label style='width:129px;'><b>Guarantor nationality :</b></label><em>" + nationality + "</em></span>";
            readOnlyBlock += "<span><label style='width:129px;'><b>Realition to applicant:</b></label><em>" + visaPassenger.AdditionalInfo.RealationShipWithGuranter + "</em></span>";
            readOnlyBlock += "<span><label style='width:129px;'><b>Visa no:</b></label><em>" + visaPassenger.AdditionalInfo.GuranterVisaNo + "</em></span>";
            readOnlyBlock += "<span><label style='width:129px;'><b>Emailid:</b></label><em>" + visaPassenger.AdditionalInfo.GuranterEmailId + "</em></span>";
            readOnlyBlock += "<span><label style='width:129px;'><b>Mobile no:</b></label><em>" + visaPassenger.AdditionalInfo.GuranterMobile + "</em></span>";
            readOnlyBlock += "</div>";
            readOnlyBlock += "<div class=\"agency_address_content\"><p></p>";
            readOnlyBlock += "<span><label><b>Guarantor Pre.Nationality:</b>" + visaPassenger.AdditionalInfo.GuranterPreNationality + "</label></span>";
            readOnlyBlock += "<span><label> <b>Office no:</b>" + visaPassenger.AdditionalInfo.GuranterOfficeNo + "</label></span>";
            //readOnlyBlock += "<span><label>" + paxAddress.CityName + " " + paxAddress.Pin + " </label></span>";
            //readOnlyBlock += "<span><label>" + visaPassenger.EmailId + " </label></span>";
            //readOnlyBlock += "<span><label>Ph:" + visaPassenger.HomePhone + " </label></span>";
            readOnlyBlock += "</div>";
            readOnlyBlock += "";
            Response.Write(readOnlyBlock);
        }
        if (Convert.ToBoolean(Request["isReadOnlyPaxDocument"]))
        {
           string  siteName = Convert.ToString(CT.Configuration.ConfigurationSystem.VisaConfig["SiteName"]);
           string readOnlyBlock="";
           DataTable visaDT =new DataTable();
            if (Request["paxId"] != null && Request["paxId"] != "0")
            {
                VisaQueue visaQueue = new VisaQueue();
                visaQueue.PaxId = Convert.ToInt32(Request["paxId"]);
                //pa = Convert.ToInt32(Request["visaId"]);
                try
                {
                    visaDT = visaQueue.getVisaUploadedDocuement();
                }
                catch (Exception ex)
                {
                }
            }
           // siteName = "localhost:1581/B2B/";
            readOnlyBlock = "<div class=\"airfare_calender width_100\"><div class=\"cal_headPopUp\"><span>Pop up</span></div><div class=\"login\"><div  class=\"pop_container\">";
            for (int i = 0; i < visaDT.Rows.Count; i++)
            {

                readOnlyBlock+= "<div class=\"pop_cont\"><p><label>Type</label> <span>" + visaDT.Rows[i]["documentType"] + "</span></p><p class=\"imgarea\"><img src=\""+Request.Url.Scheme+ "://" + siteName + visaDT.Rows[i]["documentPath"] + "  /></p></div>";
            }

            readOnlyBlock += "</div></div></div> </div>";
            Response.Write(readOnlyBlock);
        }



        if (Convert.ToBoolean(Request["isReadOnlyPaxAddress"]))
        {
            VisaPassenger visaPassenger = new VisaPassenger();
            Address paxAddress = new Address();
            if (Request["paxId"] != null)
            {
                visaPassenger.Load(Convert.ToInt32(Request["paxId"]));
            }
            paxAddress = visaPassenger.Address;
            string readOnlyBlock = string.Empty;
            string nationality = string.Empty;
            if (visaPassenger.Nationality != null && visaPassenger.Nationality != "")
            {
                VisaNationality visaNationality = new VisaNationality(visaPassenger.Nationality);
                nationality = visaNationality.NationalityName;
            }
            else
            {
                nationality = visaPassenger.NationalityOther;
            }
            readOnlyBlock = "<div class=\"agency_summary\"><h1>" + visaPassenger.Firstname + " " + visaPassenger.LastName + "</h1>";
            readOnlyBlock += "<span><label>" + "<img class=\"hand margin-right-50\" alt=\"close\" src=\"Images/close.gif\" onclick=\"hideDiv('" + Request["blockId"].ToString() + "')\" />";
            readOnlyBlock += "<em>" + nationality + "</em>" + "<b> Nationality : </b>";
           // readOnlyBlock += "<span><label><b>Nationality : </b>" + nationality;
           // readOnlyBlock += "<img class=\"hand margin-right-50\" alt=\"close\" src=\"Images/close.gif\" onclick=\"hideDiv('" + Request["blockId"].ToString() + "')\" />";
            readOnlyBlock += "</span></div><div class=\"agency_holder_content\">";
            readOnlyBlock += "<span><label><b>Phone :</b></label><em>" + paxAddress.Phone + "</em></span>";
            readOnlyBlock += "<span><label><b>Mobile :</b></label><em>" + paxAddress.Mobile + "</em></span>";
            readOnlyBlock += "<span><label><b>Fax:</b></label><em>" + paxAddress.Fax + "</em></span>";
            readOnlyBlock += "<span><label><b>Email:</b></label><em>" + paxAddress.Email + "</em></span>";
            readOnlyBlock += "</div>";
            readOnlyBlock += "<div class=\"agency_address_content\"><p>Pax Address</p>";
            readOnlyBlock += "<span><label>" + paxAddress.Line1.Trim() + "</label></span>";
            readOnlyBlock += "<span><label>" + paxAddress.Line2 + "</label></span>";
            readOnlyBlock += "<span><label>" + paxAddress.CityName + " </label></span>";
            readOnlyBlock += "<span><label>" + Country.GetCountry( paxAddress.CountryCode).CountryName +  " </label></span>";
            readOnlyBlock += "</div>";
            readOnlyBlock += "";
            Response.Write(readOnlyBlock);

        }
        if (Convert.ToBoolean(Request["isVisaUploadedDocumrnt"]))
        {
           List<VisaUploadedDocument> uploadedDocumentList=null;
           SortedList visaDocument=new SortedList();
            if (Request["paxId"] != null)
            {
                uploadedDocumentList = VisaUploadedDocument.GetUploadedDocumentList(Convert.ToInt32(Request["paxId"]));
                visaDocument = VisaDocument.GetDocumentTypeList((string)Request["countryCode"], Settings.LoginInfo.AgentId);
                
            }


            string readOnlyBlock = string.Empty;
            readOnlyBlock = "<div class='airfare_calender width_100'><div class='cal_head'><span>Uploaded Documents</span><div style='float:right;margin-right:10px'><a href=javaScript:HideUploadDocumentDiv('" + Request["DivId"] + "')><b>X</b></a></div></div><div class='login'><div class='pop_container' style='height:199px'>";
            readOnlyBlock += "<div class='pop_cont'><p><label style='width:40px;'>SNo</label><label style='width:250px;'>Type</label><label style='width:40px;'></label> <span></span></p>";
            int i = 1;
            foreach (VisaUploadedDocument document in uploadedDocumentList)
            {

                readOnlyBlock += "<p><label style='width:40px;' >" + i + "</label><label style='width:250px;'>" + visaDocument[document.DocumentId.ToString()] + "</label><label style='width:40px;'><a href=\"javascript:newwindow(\'ViewUploadedVisaDocuments.aspx?paxId=" + Request["paxId"] + "&documentId=" + document.DocumentId + "\')\" >View</a></label></p>";
                i++;
            }
            readOnlyBlock += "</div></div></div></div></div>";
            Response.Write(readOnlyBlock);

        }

        if (Convert.ToBoolean(Request["isReadOnlyPaxPassport"]))
        {
            VisaPassenger visaPassenger = new VisaPassenger();
            Address paxAddress = new Address();
            if (Request["paxId"] != null)
            {
                visaPassenger.Load(Convert.ToInt32(Request["paxId"]));
            }
            paxAddress = visaPassenger.Address;
            string readOnlyBlock = string.Empty;
            string nationality = string.Empty;
            if (visaPassenger.Nationality != null && visaPassenger.Nationality != "")
            {
                VisaNationality visaNationality = new VisaNationality(visaPassenger.Nationality);
                nationality = visaNationality.NationalityName;
            }
            else
            {
                nationality = visaPassenger.NationalityOther;
            }
            readOnlyBlock = "<div class=\"agency_summary\"><h1>" + visaPassenger.Firstname + " " + visaPassenger.LastName + "</h1>";
           // readOnlyBlock += "<span><label><b>Nationality : </b>" + nationality;
           // readOnlyBlock += "<img class=\"hand margin-right-50\" alt=\"close\" src=\"Images/close.gif\" onclick=\"hideDiv('" + Request["blockId"].ToString() + "')\" />";
            readOnlyBlock += "<span><label>" + "<img class=\"hand margin-right-50\" alt=\"close\" src=\"Images/close.gif\" onclick=\"hideDiv('" + Request["blockId"].ToString() + "')\" />";
            readOnlyBlock += "<em>" + nationality + "</em>" + "<b> Nationality : </b>";
            readOnlyBlock += "</span></div><div class=\"agency_holder_content\"><p>Passport Details</p>";
            readOnlyBlock += "<span><label style='width:115px;float:left'><b>Passport No :</b></label><em  style='width:145px;float:left'>" + visaPassenger.PassportNo + "</em></span>";
            readOnlyBlock += "<span><label  style='width:115px;float:left'><b>Issue City :</b></label><em style='width:145px;float:left'>" + visaPassenger.PassprotIssueCity + "</em></span>";
            readOnlyBlock += "<span><label style='width:115px;float:left'><b>Issue Country:</b></label><em style='width:145px;float:left'>" + Country.GetCountry( visaPassenger.PassportIssueCountry).CountryName + "</em></span>";
            //readOnlyBlock += "<span><label style='width:115px;float:left'><b>Issue Authority:</b></label><em style='width:145px;float:left'>" + visaPassenger.PassportIssuingAuthority + "</em></span>";
            readOnlyBlock += "</div>";
            readOnlyBlock += "<div class=\"agency_address_content\"><p></p>";
            readOnlyBlock += "<span><label><b>Passport Type: </b>" + visaPassenger.PassportType + "</label></span>";
            readOnlyBlock += "<span><label><b>Issue Date :</b> " + Util.UTCToIST(Convert.ToDateTime(visaPassenger.PassportIssueDate)).ToString("dd/MM/yyyy") + "</label></span>";
            readOnlyBlock += "<span><label><b>Expiry Date:</b> " + Util.UTCToIST(Convert.ToDateTime(visaPassenger.PassportExpiryDate)).ToString("dd/MM/yyyy") + "</label></span>";
            readOnlyBlock += "</div>";
            readOnlyBlock += "";
            Response.Write(readOnlyBlock);
        }




        //To Show Fare Rule Block
        if (Convert.ToBoolean(Request["isFareRuleBlock"]))
        {
            FlightItinerary itinerary = new FlightItinerary(Convert.ToInt32(Request["flightId"]));
            string fareRuleBlock = string.Empty;
            string blockName = Request["blockId"].ToString();
            fareRuleBlock = "<div  style=\"background:url(Images/FareRules_header.png) repeat-x;width:490px; float:left; padding:5px;\">";
            fareRuleBlock += "<div class=\"fleft white bold\" style=\"padding:0px; margin:0px;\"></div>";
            fareRuleBlock += "<div class=\"hand fright\" onclick=\"HideDiv('" + blockName + "')\">";
            fareRuleBlock += "<img src=\"images/close.gif\" alt=\"Close\" /></div></div>";
            fareRuleBlock += "<div class=\"fleft clear refine-result-bg-color\" style=\"width:495px;padding:0px 0 0 5px; margin:0px; height:300px; overflow:auto;\">";
            for (int p = 0; p < itinerary.FareRules.Count; p++)
            {
                fareRuleBlock += "<div class=\"fleft width-100 bold\">";
                fareRuleBlock += "<span class=\"fleft\">" + itinerary.FareRules[p].Airline + "</span>";
                fareRuleBlock += "<span class=\"fleft\">[" + itinerary.FareRules[p].Origin + "] </span>";
                fareRuleBlock += "<span class=\"fleft\">" + Util.GetCityName(itinerary.FareRules[p].Origin) + "- </span>";
                fareRuleBlock += "<span class=\"fleft\">[" + itinerary.FareRules[p].Destination + "]</span>";
                fareRuleBlock += "<span class=\"fleft\">" + Util.GetCityName(itinerary.FareRules[p].Destination) + "</span>";
                fareRuleBlock += "</div>";
                fareRuleBlock += "<div class=\"fleft width-100\"><pre class=\"fleft\" style=\"width:300px;\"><br />";
                fareRuleBlock += Regex.Replace(Regex.Replace(itinerary.FareRules[p].FareRuleDetail.Trim(), "\n", "<br />"), "-FARE BASIS", "-FARE BASIS   ");
                fareRuleBlock += "</pre></div>";
            }
            fareRuleBlock += "</div>";
            fareRuleBlock += "<div class=\"clear refine-result-bg-color\" id=\"FareRuleFoot\" style=\"float:left; width:100%;padding:5px;\">";
            fareRuleBlock += "<span class=\"fleft\" style=\" width:100%;text-align:center;\"><input type=\"button\" value=\"Close\"  onclick=\"HideDiv('" + blockName + "')\"/></span></div><img src=\"Images/spacer.gif\" alt=\"spacer\" />";
            fareRuleBlock += "</div>";
            Response.Write(fareRuleBlock);
        }

    }
    private void RefreshPNR()
    {
        FlightItinerary itinerary = GetItinerary();
        FlightItinerary.RefreshItinerarySelectedData(itinerary);
        FlightInfo.RefreshFlightInfoData(itinerary.Segments);
        Ticket.RefreshTicketData(ticket);


    }
    private FlightItinerary GetItinerary()
    {
        FlightItinerary itinerary = new FlightItinerary();
        //int flightId = Convert.ToInt32(Request["flightId"]);
        //FlightInfo[] segment = FlightInfo.GetSegments(flightId);
        //DataTable tempTable = Ticket.GetSelectedColumnsFromTicketAgainstFlightId(flightId);

        //int lastModifiedBy = Convert.ToInt32(Convert.ToInt32(Session["UserMasterId"]));
        //try
        //{
        //    //todo: check it should call only once it getting data first time. now it is calling again and again if the page is reloaded.
        //    string fareBasisCodeOB = string.Empty;
        //    string fareBasisCodeIB = string.Empty;
        //    BookingSource bookingSource = ((BookingSource)Enum.Parse(typeof(BookingSource), Request["bookingSource"]));


        //    string fareCode = string.Empty;
        //    List<string> ticketNumbers = new List<string>();

        //    if (bookingSource == BookingSource.WorldSpan)
        //    {
        //        itinerary = Worldspan.RetrieveItinerary(Request["pnr"], out ticket);
        //    }
        //    else if (bookingSource == BookingSource.SpiceJet)
        //    {
        //        // For Spice Jet
        //        SpiceJetAPI sp = new SpiceJetAPI();
        //        itinerary = sp.RetrieveItinerary(Request["pnr"], ref fareBasisCodeOB, ref fareBasisCodeIB);
        //        ticket = new Ticket[0];
        //    }
        //    else if (bookingSource == BookingSource.Indigo)
        //    {
        //        // For Spice Jet
        //        Navitaire navi = new Navitaire("6E");
        //        itinerary = navi.RetrieveItinerary(Request["pnr"], ref fareBasisCodeOB, ref fareBasisCodeIB);
        //        ticket = new Ticket[0];

        //    }
        //    else if (bookingSource == BookingSource.Paramount)
        //    {
        //        // For Paramount
        //        itinerary = ParamountApi.RetrieveItinerary(Request["pnr"], ref fareCode, ref ticketNumbers);
        //        ticket = new Ticket[0];
        //        Session["ticketNumber"] = ticketNumbers;

        //    }
        //    else if (bookingSource == BookingSource.AirDeccan)
        //    {
        //        // For Air Deccan
        //        AirDeccanApi airDeccan = new AirDeccanApi();
        //        itinerary = airDeccan.RetrieveItinerary(Request["pnr"], ref fareBasisCodeOB);
        //        ticket = new Ticket[0];

        //    }
        //    else if (bookingSource == BookingSource.Amadeus)
        //    {
        //        itinerary = Amadeus.RetrieveItinerary(Request["pnr"], out ticket);

        //    }
        //    else if (bookingSource == BookingSource.Mdlr)
        //    {
        //        // For MDLR
        //        itinerary = Mdlr.RetrieveItinerary(Request["pnr"], ref fareCode, ref ticketNumbers);
        //        ticket = new Ticket[0];
        //        Session["ticketNumber"] = ticketNumbers;

        //    }
        //    else if (bookingSource == BookingSource.GoAir)
        //    {
        //        // For Go Air
        //        GoAirApi airDeccan = new GoAirApi();
        //        itinerary = airDeccan.RetrieveItinerary(Request["pnr"], ref fareBasisCodeOB);
        //        ticket = new Ticket[0];

        //    }
        //    else if (bookingSource == BookingSource.Galileo)
        //    {
        //        itinerary = GalileoApi.RetrieveItinerary(Request["pnr"], out ticket);
        //    }
        //    else if (bookingSource == BookingSource.Sama)
        //    {
        //        // For SAMA
        //        itinerary = Sama.RetrieveItinerary(Request["pnr"]);
        //        ticket = new Ticket[0];
        //    }

        //    if (bookingSource == BookingSource.Amadeus || bookingSource == BookingSource.WorldSpan || bookingSource == BookingSource.Galileo)
        //    {
        //        if (ticket.Length == 0)
        //        {
        //            throw new ArgumentException("INVALID RECORD LOCATOR.");
        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //    Response.Write("Request processed successfully but unable to update PNR automatically.Please Edit Ticket(s) and update PNR information manually.");
        //    Response.End();
        //}
        //if (itinerary.Segments == null || itinerary.Segments.Length <= 0 || itinerary.Segments.Length != segment.Length)
        //{
        //    Response.Write("Request processed successfully but unable to update PNR automatically. Please Edit Ticket(s) and update PNR information manually.");
        //    Response.End();
        //}

        //itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
        //itinerary.IsDomestic = Utility.IsDomestic(itinerary.Segments, "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
        //itinerary.Origin = itinerary.Segments[0].Origin.CityCode;

        //if (itinerary.Segments.Length == 2 && (itinerary.Segments[0].Origin.CityCode == itinerary.Segments[itinerary.Segments.Length - 1].Destination.CityCode))
        //{
        //    itinerary.Destination = itinerary.Segments[0].Destination.CityCode;
        //}
        //else
        //{
        //    itinerary.Destination = itinerary.Segments[itinerary.Segments.Length - 1].Destination.CityCode;
        //}

        //if (itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.Sama)
        //{
        //    ticket = new Ticket[itinerary.Passenger.Length];
        //    for (int j = 0; j < itinerary.Passenger.Length; j++)
        //    {
        //        ticket[j] = new Ticket();
        //        if (itinerary.FlightBookingSource != BookingSource.Paramount && itinerary.FlightBookingSource != BookingSource.Mdlr)
        //        {
        //            ticket[j].TicketNumber = itinerary.PNR;
        //        }
        //        else
        //        {
        //            ticket[j].TicketNumber = Session["ticketNumber"].ToString();
        //        }
        //        ticket[j].IssueDate = DateTime.UtcNow;
        //        ticket[j].ValidatingAriline = itinerary.ValidatingAirline;
        //        ticket[j].FlightId = flightId;
        //    }
        //}
        //else
        //{
        //    for (int i = 0; i < ticket.Length; i++)
        //    {
        //        ticket[i].FlightId = flightId;
        //        ticket[i].LastModifiedBy = lastModifiedBy;
        //    }
        //}
        //for (int i = 0; i < itinerary.Segments.Length; i++)
        //{
        //    itinerary.Segments[i].LastModifiedBy = lastModifiedBy;
        //    itinerary.Segments[i].FlightId = flightId;
        //}

        //itinerary.FlightId = flightId;
        //if (ticket.Length != tempTable.Rows.Count)
        //{
        //    Response.Write("Request processed successfully but unable to update PNR automatically. Please Edit Ticket(s) and update PNR information manually.");
        //    Response.End();
        //}
        return itinerary;
    }


}
