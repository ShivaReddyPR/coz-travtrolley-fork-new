﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using CT.Core;
using CT.Configuration;
using Visa;
using System.IO;
using System.Globalization;
using CT.TicketReceipt.BusinessLayer;
using System.Web.UI.WebControls;

public partial class ViewBookingForVisa : CT.Core.ParentPage
{
    protected string message = string.Empty;
    protected bool isAdmin = false;
    protected int visaId = 0;
    protected int TotalPassenger = 0;
    private string imageServerPath;
    private string attachmentPath;
    private decimal netAmount = 0;
    protected List<DataRow[]> listOfVisaBookings = new List<DataRow[]>();
    protected DataTable visaDT;
    protected int refundableFeeFor = 1;
   
    protected void Page_Init(object sender, EventArgs e)
    {
        Page.Form.Enctype = "multipart/form-data";
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        Page.Title = "View Booking for Visa";
        AuthorizationCheck();
        Page.Form.Attributes.Add("enctype", "multipart/form-data");
        if (!IsPostBack)
        {
            Session["CheckRefresh"] = Server.UrlDecode(System.DateTime.Now.ToString());

        }

        //if (Settings.LoginInfo != null)
        //{
        //    ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        //    scriptManager.RegisterPostBackControl(this.btnExport);
        //}

        if (Request["message"] != null)
        {
            message = Request["message"];
        }

        if (Session["agencyId"] != null && (int)Session["agencyId"] == 0)
        {
            isAdmin = true;
        }
        else
        {
            isAdmin = false;
        }
        if (Request["message"] != null)
        {
            message = Request["message"];
        }   // else nothing to do. optional parameter, displayed in page if given.


        if (Request.QueryString.Count > 0)
        {
            int visaId;
            bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out visaId);
            if (!isnumerice)
            {
                Response.Redirect("AdminVisaQueue.aspx");
            }

            Session["visaId"] = visaId;
            if (!IsPostBack)
            {
                GetVisaDetails();
            }
            else
            {
                if (Session["refundableFeeFor"] != null)
                    refundableFeeFor = (int)Session["refundableFeeFor"];
            }
        }
        else
        {
            Response.Redirect("AdminVisaQueue.aspx");
        }
        if (Session["visaId"] != null)
        {
            visaId = Convert.ToInt32(Session["visaId"]);

        }
        if (Session["VisaDT"] != null)
        {

            visaDT = (DataTable)Session["VisaDT"];
            TotalPassenger = visaDT.Rows.Count;
        }

    }

    protected void GetVisaDetails()
    {
        VisaQueue visaQueue = new VisaQueue();
        if (Convert.ToInt32(Request["visaId"]) > 0)
        {
            visaId = Convert.ToInt32(Session["visaId"]);
            visaQueue.VisaId = visaId;
        }
        else
        {
            visaId = Convert.ToInt32(Session["visaId"]);
            visaQueue.VisaId = visaId;
        }

        try
        {
            visaDT = visaQueue.SearchByVisaNumber();
            MultiViewBooking.SetActiveView(BookingView);
            TotalPassenger = visaDT.Rows.Count;
            refundableFeeFor = 1;
            for (int i = 1; i < TotalPassenger; i++)
            {
                //if (visaDT.Rows[i]["paxLastName"].ToString().ToLower() != visaDT.Rows[0]["paxLastName"].ToString().ToLower())Commented by shiva to calculate refund fee for all pax
                {
                    refundableFeeFor++;
                }

            }
            Session["refundableFeeFor"] = refundableFeeFor;
            Session["VisaDT"] = visaDT;
            if (visaDT.Rows.Count > 0)
            {

                if (Convert.ToInt32(visaDT.Rows[0]["visaStatus"]) >= (int)VisaStatus.InProcess)
                {
                    btnFee.Visible = false;
                }
                else
                {
                    btnFee.Visible = true;
                }
                lblVisaFee.Text = String.Format("{0:0.00}", visaDT.Rows[0]["visaCostFee"]);
                lblMarkup.Text = String.Format("{0:0.00}", visaDT.Rows[0]["visaMarkupFee"]);
                lblInsuranceFee.Text = String.Format("{0:0.00}", visaDT.Rows[0]["visaInsuranceFee"]);
                lblTransaction.Text = String.Format("{0:0.00}", visaDT.Rows[0]["visaDepositFee"]);
                lblRefund.Text = String.Format("{0:0.00}", (decimal)visaDT.Rows[0]["visaRefundableFee"]);
                netAmount = Convert.ToDecimal(visaDT.Rows[0]["visaFee"]);
                lblVisaFee.Visible = true;
                lblMarkup.Visible = true;
                lblInsuranceFee.Visible = true;
                lblTransaction.Visible = true;
                lblRefund.Visible = true;
                lblVAT.Text = String.Format("{0:0.00}", visaDT.Rows[0]["VISAVAT"]);
                GetPaymentSource();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, "Page:viewBookingForVisa.Aspx,Exception in GetVisaDetails() ,message: " + ex.Message, "");
        }

    }

    protected void btnFee_Click(object sender, EventArgs e)
    {
        if (Session["CheckRefresh"].ToString() == ViewState["CheckRefresh"].ToString())
        {
            Session["CheckRefresh"] = Server.UrlDecode(System.DateTime.Now.ToString());

            visaDT = (DataTable)Session["VisaDT"];
            if (btnFee.Text == "Edit fee")
            {

                lblTransaction.Visible = false;
                lblVisaFee.Visible = false;
                lblMarkup.Visible = false;
                lblInsuranceFee.Visible = false;
                lblRefund.Visible = false;
                btnSubmit.Visible = true;
                btnFee.Text = "Cancel";
                txtTransaction.Visible = true;
                txtVisaFee.Visible = true;
                txtMarkup.Visible = true;
                txtInsuranceFee.Visible = true;
                txtRefund.Visible = true;
                txtTransaction.Text = lblTransaction.Text;
                txtVisaFee.Text = lblVisaFee.Text;
                txtRefund.Text = lblRefund.Text;
                txtMarkup.Text = lblMarkup.Text;
                txtInsuranceFee.Text = lblInsuranceFee.Text;
            }
            else
            {
                lblTransaction.Visible = true;
                lblVisaFee.Visible = true;
                lblMarkup.Visible = true;
                lblInsuranceFee.Visible = true;
                lblRefund.Visible = true;
                btnSubmit.Visible = false;
                btnFee.Text = "Edit fee";
                txtTransaction.Visible = false;
                txtVisaFee.Visible = false;
                txtRefund.Visible = false;
                txtMarkup.Visible = false;
                txtInsuranceFee.Visible = false;
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Session["CheckRefresh"].ToString() == ViewState["CheckRefresh"].ToString())
        {
            Session["CheckRefresh"] = Server.UrlDecode(System.DateTime.Now.ToString());
            Session["memberId"] = 1;
            if (!String.IsNullOrEmpty(txtTransaction.Text) && !String.IsNullOrEmpty(txtRefund.Text))
            {
                visaDT = (DataTable)Session["VisaDT"];
                int visaId = Convert.ToInt32(Session["visaId"]);
                VisaQueue visaQueue = new VisaQueue();
                decimal PublishedFee = Convert.ToDecimal(txtVisaFee.Text.Trim());
                decimal Refundable = Convert.ToDecimal(txtRefund.Text.Trim());
                decimal VisaDepositCharge = Convert.ToDecimal(txtTransaction.Text.Trim());
                decimal Markup = Convert.ToDecimal(txtMarkup.Text.Trim());    
                decimal InsuranceFee = Convert.ToDecimal(txtInsuranceFee.Text.Trim());
                //netAmount = VisaDepositCharge + Convert.ToDecimal(visaDT.Rows[0]["visaCostFee"]) + Convert.ToDecimal(txtVisaFee.Text.Trim()) + Convert.ToDecimal(txtMarkup.Text.Trim()) + Convert.ToDecimal(txtInsuranceFee.Text.Trim());
                netAmount = VisaDepositCharge + Convert.ToDecimal(txtVisaFee.Text.Trim()) + Convert.ToDecimal(txtMarkup.Text.Trim()) + Convert.ToDecimal(txtInsuranceFee.Text.Trim());

                decimal vatCharge = 0, vatAmount=0,outvatAmount=0;
                DataRow row = visaDT.Rows[0];
                if (row["INVAT_APPLIED"] != DBNull.Value && row["INVAT_COST_INCLUDED"] != DBNull.Value && row["INVAT_VALUE"] != DBNull.Value)
                {
                    bool invatApplied = Convert.ToBoolean(row["INVAT_APPLIED"].ToString());
                    bool invatCostIncluded = Convert.ToBoolean(row["INVAT_COST_INCLUDED"].ToString());
                    if (invatApplied && !invatCostIncluded)
                    {
                        vatCharge = Convert.ToDecimal(visaDT.Rows[0]["INVAT_VALUE"]);
                        vatAmount = (VisaDepositCharge + Convert.ToDecimal(txtVisaFee.Text.Trim())) * vatCharge / 100;
                        
                        lblVAT.Text = String.Format("{0:0.00}", vatAmount);
                    }
                }

                if (row["OUTVAT_APPLIED"] != DBNull.Value && row["OUTVAT_ON"] != DBNull.Value && row["OUTVAT_VALUE"] != DBNull.Value)
                {
                    bool outvatApplied =Convert.ToBoolean(row["OUTVAT_APPLIED"].ToString());
                    string ouvatON = row["OUTVAT_ON"].ToString();
                    if (outvatApplied)
                    {
                        vatCharge = Convert.ToDecimal(visaDT.Rows[0]["OUTVAT_VALUE"]);
                        if (ouvatON == "TC")
                        {                            
                            outvatAmount = netAmount * vatCharge / 100;
                            
                            lblVAT.Text = String.Format("{0:0.00}", (vatAmount + outvatAmount));
                        }
                        else if(ouvatON == "SF")
                        {
                            outvatAmount = Markup * vatCharge / 100;
                            
                            lblVAT.Text = String.Format("{0:0.00}", (vatAmount + outvatAmount));
                        }
                    }
                }
                netAmount += Math.Ceiling(vatAmount + outvatAmount);
                try
                {
                    if (visaId > 0)
                    {
                        if (VisaFee.UpdateVisaFee(visaId, Refundable, VisaDepositCharge, PublishedFee, Markup, InsuranceFee, netAmount, vatAmount, outvatAmount))
                        {
                            visaQueue.VisaId = visaId;
                            visaQueue.VisaStatusId = Convert.ToInt32(visaDT.Rows[0]["visaStatus"].ToString());
                            visaQueue.VisaDepositFee = VisaDepositCharge;
                            visaQueue.VisaFee = PublishedFee;
                            visaQueue.VisaRefundableFee = Refundable;
                            visaQueue.VisaMarkup = Markup;
                            visaQueue.VisaInsuranceFee = InsuranceFee;
                            visaQueue.VisaRemarks = "Price Edited Previous Deposite Fee:" + lblTransaction.Text + " New Deposite Fee:" + txtTransaction.Text.Trim() + "Previous Refundable Fee:" + lblRefund.Text + " New Refundable Fee:" + txtRefund.Text.Trim() + "Previous Visa Fee:" + lblVisaFee.Text + " New Visa Fee:" + txtVisaFee.Text.Trim() + "Previous Mark Fee:" + lblMarkup.Text + " New Visa Fee:" + txtMarkup.Text.Trim() + "Previous Insurance Fee:" + lblInsuranceFee.Text + " New Insurance Fee:" + txtInsuranceFee.Text.Trim();
                            visaQueue.UpdatedBy = (int)Session["memberId"];
                            visaQueue.UpdateVisaQueueHistory();
                            txtRefund.Visible = false;
                            txtTransaction.Visible = false;
                            txtVisaFee.Visible = false;
                            txtMarkup.Visible = false;
                            txtInsuranceFee.Visible = false;
                            // lblMarkup.Text = txtMarkup.Text;
                            //  lblInsuranceFee.Text = txtInsuranceFee.Text;
                            btnSubmit.Visible = false;
                            btnFee.Text = "Edit fee";
                            GetVisaDetails();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Visa, Severity.High, (int)Session["memberId"], "Page:ViewBookingForVisa.aspx,Err in updating fee:" + ex.Message, "");
                }
            }
        }
    }


    protected void btnAddComment_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["CheckRefresh"].ToString() == ViewState["CheckRefresh"].ToString())
            {
                Session["CheckRefresh"] = Server.UrlDecode(System.DateTime.Now.ToString());
                VisaQueue visaQueue = new VisaQueue();
                visaQueue.VisaId = Convert.ToInt32(Session["visaId"]);
                visaQueue.VisaStatusId = Convert.ToInt32(visaDT.Rows[0]["visaStatus"].ToString());
                visaQueue.VisaRemarks = txtComment.Text;
                visaQueue.UpdatedBy = (int)Session["memberId"];
                visaQueue.UpdateVisaQueueHistory();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Session["memberId"], "Page:ViewBookingForVisa.aspx,Err in Updating Comment:" + ex.Message, "");
        }

    }


    private void createEmail(string strEmail, VisaStatus strVisaStatus, string countryName, string visaTripId,List<string> attachments)
    {
        ConfigurationSystem con = new ConfigurationSystem();
        //string fromEmail = Convert.ToString(ConfigurationSystem.VisaConfig["fromEmail"]);
        string fromEmail = Convert.ToString(ConfigurationManager.AppSettings["fromEmail"]);
        string toEmail = strEmail.ToString();
        string messageText = string.Empty;

        if (strVisaStatus == VisaStatus.Approved)
        {
            messageText = "<p>Dear Customer</p>"
                + "<p>Greetings from Cozmo Travels LLC.</p>"
                + "<p>Thank you for applying <b> " + countryName + " </b> tourist visa with Cozmo Travel.</p>"
                + "<p>Your visa application has been accepted and approved by the <b> " + countryName + " </b> Immigration Department.</p>"
                + "<p>We request you to log on to <a  class='fcol_blue' target='_blank' href='www.gocozmo.com'>www.gocozmo.com</a> to book your flight ticket or contact our customer support for issuing your flight ticket. You need to provide the flight ticket details purchased from us for issuing your visa.</p>"
                + "<p>If you wish to arrange your flight ticket for your travel from your side, an additional charge of AED 100 per person will be applicable to issue the visa.</p>"
                + "<p>Kindly note the security deposit, if any, will be refunded to you after collecting our service charges of AED 30 per person upon producing valid documents stating the visitor exit the country.</p><br/>"
                + "<p>Best Regards,<br/><br/>Customer Support Team</p>"
                + "<p>Contact Details:<br/>"
                + "Phone :  +971 600 52 4444<br/>"
                + "Fax     : +971 657 58 578<br/>"
                + "Email  : helpdesk@cozmotravel.com<br/>"
                + "Skype : helpdesk@cozmotravel.com<br/>"
                + "Web    : www.gocozmo.com</p>";                
        }

        if (strVisaStatus == VisaStatus.Eligible)
        {
            messageText = "<p>Dear Customer</p>" + "<p>Greetings from Cozmo Travels LLC.</p>"
                + "<p>Thank you for your inquiry for <b> " + countryName + " </b>tourist visa application and your Trip Id is " + visaTripId + "</p>"
                + "<p>After evaluation of your application, we would like to inform you that you are eligible to apply for <b> " + countryName + " </b> tourist visa.</p>"
                //+ "<p>We will contact you shortly to get the documents collected from you to process your " + countryName + " tourist visa application. In the meantime, please arrange the following documents for the application process: </p>"
                + "<p>Please arrange the following documents for the application process:</p>"
                + "<li>Copy of Valid <b> " + countryName + " </b> residence Visa of Guarantor</li>"
                + "<li>Copy of Passport of Guarantor (both first and last pages)</li>"
                + "<li>Front and back copy of Guarantor’s Emirates ID</li>"
                + "<li>Contact details of Guarantor, mobile number and email id</li>"
                + "<li>Clear copy of passport of Visitor/s (both first and last pages)</li>"
                + "<li>Passport size photograph in white background (PDF)</li>"
                + "<p>Alternately,you can send us the above mentioned documents at mailto:helpdesk@cozmotravel.com mentioning your Trip Id on the subject line for us to complete your visa application form and submit to the immigration department.</p>"
                + "<p>We request you to login to <a  class='fcol_blue' target='_blank' href='www.gocozmo.com'>www.gocozmo.com</a> using your username and password sent to your email id to view your application status under 'My Trips' and make the payment for your visa application.</p>"
                + "<p>Kindly note visa fee paid for visa application is non-refundable once the application is submitted in the <b> " + countryName + " </b> Immigration Department under any circumstances. The security deposit, if any, will be refunded to you upon producing valid documents stating that the visitor exit the country or the if the visa application gets rejected by the <b> " + countryName + " </b> Immigration Department after collecting our service charges of AED 30 per person.</p><br/>"
                + "<p>if your application request needs to be processed on fast track mode, please call us at +971 600 52 4444 or email at mailto:helpdesk@cozmotravel.com. An additional fees of AED 200 per person will be applicable for each visa processed in FastTrack mode..</p>"
                //+ "<p>If you wish to arrange the air ticket for your travel from your side, an additional charge of AED 100 per person will be applicable to release the visa. </p>"
                + "<p>We also provide OK TO BOARD (OTB) message service with all airlines which is mandatory to be done to board the flight from the home country at an additional cost of AED 25 per person. To avail this service, please send your request to mailto:helpdesk@cozmotravel.com,prior to the departure date (not less than 24 hrs).</p>"
                + "<p>Best Regards,<br/><br/>Customer Support Team</p>"
                + "<p>Contact Details:<br/>"
                + "Phone :  +971 600 52 4444<br/>"
                + "Fax     : +971 657 58 578<br/>"
                + "Email  : helpdesk@cozmotravel.com<br/>"
                + "Skype : helpdesk@cozmotravel.com<br/>"
                + "Web    : www.gocozmo.com</p>";                
        }

        if (strVisaStatus == VisaStatus.NotEligible)
        {
            messageText = "<p>Dear Customer</p>"
                + "<p>Greetings from Cozmo Travels LLC.</p>"
                + "<p>Thank you for your inquiry for <b> " + countryName + " </b> tourist visa application.</p>"
                + "<p>After detailed evaluation of your application, we regret to inform you that you are not eligible to apply for <b> " + countryName + " </b> tourist visa and we will not be processing your visa application.</p>"
                + "<p>Kindly note payments made, if any, for visa application will be refunded to you after collecting our service charges of AED 30 per person.</p>"
                + "<p>Please visit our website <a  class='fcol_blue' target='_blank' href='www.gocozmo.com'>www.gocozmo.com</a> for your travel needs.</p><br/>"
                + "<p>Best Regards,<br/></p>"
                + "<p>Customer Support Team</p>"
                + "<p>Contact Details:<br/>"
                + "Phone :  +971 600 52 4444<br/>"
                + "Fax     : +971 657 58 578<br/>"
                + "Email  : helpdesk@cozmotravel.com<br/>"
                + "Skype : helpdesk@cozmotravel.com<br/>"
                + "Web    : www.gocozmo.com</p>";                
        }

        if (strVisaStatus == VisaStatus.Rejected)
        {
            messageText = "<p>Dear Customer</p>"
                + "<p>Greetings from Cozmo Travels LLC.</p>"
                + "<p>Thank you for applying <b> " + countryName + " </b> tourist visa with Cozmo Travel. </p>"
                + "<p>We regret to inform you that your visa application has been rejected by <b> " + countryName + " </b> Immigration department.</p>"
                + "<p>Kindly note, visa fees paid for visa application is non-refundable once the application is submitted in the Immigration under any circumstances. The security deposit, if any, will be refunded to you after collecting our service charges of AED 30 per person.</p>"
                + "<p>Please visit our website <a class='fcol_blue' target='_blank' href='www.gocozmo.com'>www.gocozmo.com</a> for your travel needs.</p><br/>"
                + "<p>Best Regards,<br/></p>"
                + "<p>Customer Support Team</p>"
                + "<p>Contact Details:<br/>"
                + "Phone :  +971 600 52 4444<br/>"
                + "Fax     : +971 657 58 578<br/>"
                + "Email  : helpdesk@cozmotravel.com<br/>"
                + "Skype : helpdesk@cozmotravel.com<br/>"
                + "Web    : www.gocozmo.com</p>";              
        }

        if (strVisaStatus == VisaStatus.Cancelled)
        {
            messageText = "<p>Dear Customer</p>"
                + "<p>Greetings from Cozmo Travels LLC.</p>"
                + "<p>Thank you for your inquiry for <b> " + countryName + " </b> tourist visa application. </p>"
                + "<p>After the detailed evaluation of your application, we would like to inform you that your application has been cancelled as per your request for <b> " + countryName + " </b> tourist visa and we will not be processing your visa application.</p>"
                + "<p>Kindly note payments made, if any, for visa application will be refunded to you after collecting our service charges of AED 30 per person.</p>"
                + "<p>Please visit our website <a class='fcol_blue' target='_blank' href='www.gocozmo.com'>www.gocozmo.com</a> for your travel needs.</p><br/>"
                + "<p>Best Regards,<br/></p>"
                + "<p>Customer Support Team</p>"
                + "<p>Contact Details:<br/>"
                + "Phone :  +971 600 52 4444<br/>"
                + "Fax     : +971 657 58 578<br/>"
                + "Email  : helpdesk@cozmotravel.com<br/>"
                + "Skype : helpdesk@cozmotravel.com<br/>"
                + "Web    : www.gocozmo.com</p>";
        }

        if (strVisaStatus == VisaStatus.Duplicate)
        {
            messageText = "<p>Dear Customer</p>"
                + "<p>Greetings from Cozmo Travels LLC.</p>"
                + "<p>Thank you for your inquiry for <b> " + countryName + " </b> tourist visa application. </p>"
                + "<p>After the detailed evaluation of your application, we would like to inform you that your travel Trip id " + visaTripId + " is found as duplicate for <b> " + countryName + " </b> tourist visa and we will not be processing your visa application. </p>"

                + "<p>Kindly note payments made, if any, for visa application will be refunded to you after collecting our service charges of AED 30 per person.</p>"

                + "<p>Please visit our website <a class='fcol_blue' target='_blank' href='www.gocozmo.com'>www.gocozmo.com</a> for your travel needs.</p><br/>"
                + "<p>Best Regards,<br/></p>"
                + "<p>Customer Support Team</p>"
                + "<p>Contact Details:<br/>"
                + "Phone :  +971 600 52 4444<br/>"
                + "Fax     : +971 657 58 578<br/>"
                + "Email  : helpdesk@cozmotravel.com<br/>"
                + "Skype : helpdesk@cozmotravel.com<br/>"
                + "Web    : www.gocozmo.com</p>";
        }
        if (strVisaStatus == VisaStatus.InProcess_M)
        {
            messageText = "<p>Dear Customer</p>"
               + "<p>Greetings from Cozmo Travels LLC.</p>"
               + "<p>We have received your tourist visa application and payment. Your application reference number (Trip ID) is:<b>" + visaTripId + "</b></p>"
               + "<p>Please email the required documents given below to helpdesk@cozmotravel.com clearly stating the Trip Id in the subject line to complete your Visa application process.</p>"
               + "<p>Documents Required:"
               + "<ul>Copy of Valid UAE residence Visa of Guarantor</ul>"
               + "<ul>Copy of Passport of Guarantor (both first and last pages)</ul>"
               + "<ul>Front and back copy of Guarantor’s Emirates ID / QID</ul>"
               + "<ul>Contact details of Guarantor, mobile number and email id</ul>"
               + "<ul>Clear copy of passport of Visitor/s (both first and last pages)</ul>"
               + "<ul>Passport size photograph in white background (PDF)</ul>"
               + "<p>We request you to login to www.gocozmo.com using your username & password sent to your email id to view your application status under 'My Trips'.</p>"
               + "<p>If your application request needs to be processed on fast track mode, please call us at +971 600 52 4444 or email at helpdesk@cozmotravel.com.</p>"
               + "<p>Kindly note visa fee paid for visa application is non-refundable once the application is submitted in the UAE Immigration Department under any circumstances. The security deposit, if any, will be refunded to you upon producing valid documents stating that the visitor exit the country or the if the visa application gets rejected by the UAE Immigration Department after collecting our service charges of AED 30 per person.</p>"
               + "<p></p>"
               + "<p>Best Regards,<br/></p>"
               + "<p>Customer Support Team</p>"
               + "<p>Contact Details:<br/>"
               + "Phone : +971 600 52 4444<br/>"
               + "Fax   : +971 657 58 578<br/>"
               + "Email : helpdesk@cozmotravel.com<br/>"
               + "Skype : helpdesk@cozmotravel.com<br/>"
               + "Web   : www.gocozmo.com</p>";
        }
        try
        {
            if (strVisaStatus != VisaStatus.PartiallyApproved && strVisaStatus != VisaStatus.PartiallyEligible)
            {
                if (attachments.Count > 0)
                {
                    List<string> toArray = new List<string>();
                    toArray.AddRange(toEmail.Split(','));
                    Email.Send(fromEmail, fromEmail,toArray, "Cozmo Travel – Your visa status update - " + strVisaStatus + " ", messageText,new Hashtable(),attachments.ToArray());
                }
                else
                {
                    Email.Send(fromEmail, toEmail, "Cozmo Travel – Your visa status update - " + strVisaStatus + " ", messageText);
                }
                
            }
        }
        catch (System.Net.Mail.SmtpException ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Session["memberId"], "Page:ViewBookingForVisa.aspx,SMTP Exception returned Error Message:" + ex.Message + " | " + DateTime.Now, "");
        }
    }


    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }        
    }


    protected void UpdatePassenger_Click(object sender, EventArgs e)
    {
        if (Session["CheckRefresh"].ToString() == ViewState["CheckRefresh"].ToString())
        {
            Session["CheckRefresh"] = Server.UrlDecode(System.DateTime.Now.ToString());
            try
            {
                visaDT = (DataTable)Session["VisaDT"];
                attachmentPath = ConfigurationManager.AppSettings["RootFolder"];
                imageServerPath = Server.MapPath(Convert.ToString(ConfigurationSystem.VisaConfig["visaImagePath"]));
                attachmentPath += (Convert.ToString(ConfigurationSystem.VisaConfig["visaImagePath"]));
                Visa.VisaQueue visaQueue = new Visa.VisaQueue();
                VisaPassenger visaPassenger = new VisaPassenger();
                DataTable visaDT1;
                visaDT1 = (DataTable)Session["VisaDT"];
                visaPassenger = new VisaPassenger(Convert.ToInt32(visaDT1.Rows[0]["paxId"]));
                #region //ADDED by Hari Save the Inprocess_M details 
                for(int i=0;i< visaDT.Rows.Count;i++)
                {                                   
                    if (Convert.ToInt32(Request["Status" + i]) == (int)VisaPassengerStatus.InProcess_M) // Inprocess Manual
                    {
                        try
                        {
                            VisaPaymentInfo paymentInfo = new VisaPaymentInfo();
                            decimal Visafee = visaDT.Rows.Count * Convert.ToDecimal(visaDT1.Rows[0]["visaFee"]) + Convert.ToDecimal(visaDT1.Rows[0]["visaRefundableFee"]) * refundableFeeFor;
                            paymentInfo.PaymentId = txtpaymentid.Text;
                            paymentInfo.OrderId = txtorderid.Text;
                            paymentInfo.VisaId = visaPassenger.VisaId;
                            paymentInfo.Amount = Visafee;
                            paymentInfo.PGSourceId = (CT.BookingEngine.PGSource)Convert.ToInt32(ddlEnumValue.SelectedValue);
                            paymentInfo.CreatedBy = visaPassenger.CreatedBy;
                            paymentInfo.LastModifiedBy = visaPassenger.LastModifiedBy;
                            paymentInfo.VisaStatus = Convert.ToInt32(Request["Status" + i]); 
                            paymentInfo.Remarks = Request["PaxRemarks" + i];
                            int res = paymentInfo.UpdatePaymentInfo(); 
                        }
                        catch
                        { }
                    }
                }
                    clear();
                #endregion
                imageServerPath += "/Member_" + visaPassenger.CreatedBy + "/";
                attachmentPath += "Member_" + visaPassenger.CreatedBy + "/";
                HttpFileCollection uploadFilCol = Request.Files;

                #region Update Passenger Status only
                visaPassenger.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                int visaStatuscheck = Convert.ToInt32(visaDT1.Rows[0]["visaStatus"]);
                bool selectedStatus = true;
                if ( visaStatuscheck == (int)VisaStatus.InProcess_M || visaStatuscheck == (int)VisaStatus.Submitted ||  visaStatuscheck == (int)VisaStatus.Eligible || visaStatuscheck == (int)VisaStatus.NotEligible || visaStatuscheck == (int)VisaStatus.PartiallyEligible)//if visa status is submited.
                {                    
                    bool isDuplicate = false;
                    int canceled = 0, countEligible = 0, countInprocess = 0,countSubmit=0,countNE=0 ; 
                    for (int i = 0; i < visaDT1.Rows.Count; i++)
                    {
                        visaQueue.PaxId = Convert.ToInt32(visaDT1.Rows[i]["paxId"]);
                        if (Convert.ToInt32(Request["Status" + i]) == (int)VisaPassengerStatus.InProcess_M)
                        {
                            countInprocess++;
                        }
                        else  if (Convert.ToInt32(Request["Status" + i]) == (int)VisaPassengerStatus.Submitted)
                        {
                            countSubmit++;
                        }
                        else if (Convert.ToInt32(Request["Status" + i]) == (int)VisaPassengerStatus.NotEligible)
                        {
                            countNE++;
                        }
                         
                        if (Convert.ToInt32(Request["Status" + i]) != 0) //Visa status not selected No need to update Passenger status
                        {
                            selectedStatus = true;
                            visaPassenger.PaxId = Convert.ToInt32(visaDT1.Rows[i]["paxId"]);
                            visaPassenger.Status = (VisaPassengerStatus)Convert.ToInt32(Request["Status" + i]);
                            if (visaPassenger.Status == VisaPassengerStatus.Duplicate)
                            {
                                isDuplicate = true;
                            }
                            else if (visaPassenger.Status == VisaPassengerStatus.Cancelled)
                            {
                                canceled++;
                            }
                            else
                            {
                                if (Convert.ToInt32(Request["Status" + i]) == (int)VisaPassengerStatus.Eligible)
                                {
                                    countEligible++;
                                }
                            }
                            visaPassenger.Remarks = Request["PaxRemarks" + i];
                            visaPassenger.VisaId = Convert.ToInt32(visaDT1.Rows[0]["visaId"]);
                            visaPassenger.UpdatePassengerStatus();
                        }
                        else
                        {
                            if (Request["Status" + i] != null)
                            {
                                visaQueue.VisaId = Convert.ToInt32(visaDT1.Rows[0]["visaId"]);
                                visaQueue.VisaStatusId = Convert.ToInt32(visaDT1.Rows[i]["visaStatus"]);
                                visaQueue.VisaRemarks = Request["PaxRemarks" + i];
                                visaQueue.VisaDepositFee = Convert.ToDecimal(visaDT1.Rows[i]["visaDepositFee"]);
                                visaQueue.VisaRefundableFee = Convert.ToDecimal(visaDT1.Rows[i]["visaRefundableFee"]);
                                visaQueue.UpdatedBy = (int)Settings.LoginInfo.UserID;
                                visaQueue.UpdateVisaQueueHistory();
                                selectedStatus = false;
                            }
                            else if (Request["Status" + i] == null)
                            {
                                if (Convert.ToInt32(visaDT.Rows[i]["paxStatus"]) == (int)VisaPassengerStatus.Cancelled)
                                {
                                    canceled++;
                                }
                            }
                        }
                    }
                    visaQueue.VisaId = Convert.ToInt32(visaDT1.Rows[0]["visaId"]);
                    if (selectedStatus)
                    {   //updating the visa status 
                        int visaStatus = 0;
                        if (isDuplicate) //Visa status should be change Duplicate
                        {
                            visaStatus = (int)VisaStatus.Duplicate;
                        }
                        else if (canceled == visaDT1.Rows.Count) //Visa status should be change Canceled
                        {
                            visaStatus = (int)VisaStatus.Cancelled;
                        }
                        else if (countInprocess == visaDT1.Rows.Count)
                        {
                            visaStatus = (int)(VisaStatus.InProcess_M);
                        }
                        else if (countEligible == visaDT1.Rows.Count)
                        {
                            visaStatus = (int)VisaStatus.Eligible;
                        }
                        else if (countSubmit == visaDT1.Rows.Count)
                        {
                            visaStatus = (int)VisaStatus.Submitted;
                        }
                        else if (countNE == visaDT1.Rows.Count)
                        {
                            visaStatus = (int)VisaStatus.NotEligible;
                        }
                        else if (countInprocess > 0 )
                        {
                            visaStatus = (int)(VisaStatus.InProcessPartial_M);
                        }
                        else if (countEligible > 0)
                        {
                            visaStatus = (int)VisaStatus.PartiallyEligible;
                        }                     
                        

                        List<VisaCountry> countries = VisaCountry.GetActiveVisaCountryList();
                        VisaApplication application = new VisaApplication(visaId);
                        VisaCountry country = countries.Find(delegate(VisaCountry vc) { return vc.CountryCode == application.CountryCode; });
                        visaQueue.VisaStatusId = visaStatus;
                        
                        for (int i = 0; i < visaDT1.Rows.Count; i++)
                        {
                            visaQueue.VisaRemarks = "Passenger Status Changed" + "-" + Request["PaxRemarks" + i];
                            visaQueue.UpdatedBy = (int)Settings.LoginInfo.UserID;
                            visaQueue.UpdateVisaQueueHistory();                            
                        }
                        selectedStatus = false;
                        int Status = visaQueue.UpdateVisaStatus();
                        if (Status > 0)
                        {
                            string visaTripId = "VISA " + Convert.ToDateTime(visaDT.Rows[0]["visaBookingDate"]).ToString("yy") + Convert.ToDateTime(visaDT.Rows[0]["visaBookingDate"]).ToString("MM") + Convert.ToInt32(Request["visaId"]);
                            message = "Status Updated successfully for Transaction Id:" + visaTripId + ".";
                            string emails = visaDT.Rows[0]["UserEmail"].ToString() + "," + visaDT.Rows[0]["PaxEmail"].ToString();//Send mail to Visa Pax also apart from B2C Customter
                            createEmail(emails, (VisaStatus)visaStatus, country.CountryName, visaTripId,new List<string>());
                        }
                    }
                }
                #endregion
                #region Update Passenger VisaDocument Ans status 
                //visaStatuscheck == (int)VisaStatus.InProcess_M || visaStatuscheck == (int)VisaStatus.InProcess ||
                if (visaStatuscheck == (int)VisaStatus.InProcess || visaStatuscheck == (int)VisaStatus.Approved || visaStatuscheck == (int)VisaStatus.PartiallyApproved || visaStatuscheck == (int)VisaStatus.Rejected)//if visa status is Inprocess.
                {
                    CultureInfo ci = new CultureInfo("en-GB");
                    string appPath = imageServerPath + "ApplicationId_" + visaDT1.Rows[0]["visaId"];
                    attachmentPath += "ApplicationId_" + visaDT1.Rows[0]["visaId"];
                    string documentPath = string.Empty;
                    List<string> attatched = new List<string>();
                    int uploadFile = 0;
                    if (!Directory.Exists(appPath))
                    {
                        Directory.CreateDirectory(appPath);
                    }
                    int countApproved = 0;
                    int countCancelled = 0;
                    for (int i = 0; i < visaDT1.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(Request["Status" + i]) != 0)
                        {
                            HttpPostedFile file = uploadFilCol[i];
                            uploadFile++;
                            visaPassenger.PaxId = Convert.ToInt32(visaDT1.Rows[i]["paxId"]);
                            visaPassenger.Status = (VisaPassengerStatus)Convert.ToInt32(Request["Status" + i]);
                            visaPassenger.Remarks = "Passenger Status Changed";
                            visaPassenger.VisaId = Convert.ToInt32(visaDT1.Rows[0]["visaId"]);
                            if (visaPassenger.Status == VisaPassengerStatus.Cancelled)
                            {
                                countCancelled++;
                            }
                            else
                            {
                                if (Request["Status" + i] == null || Convert.ToInt32(Request["Status" + i]) == 0)
                                {
                                    if (Convert.ToInt32(visaDT1.Rows[i]["paxStatus"]) == (int)VisaPassengerStatus.Approved)
                                    {
                                        countApproved++;
                                    }
                                    continue;
                                }
                            }

                            if (Convert.ToInt32(Request["Status" + i]) != (int)VisaPassengerStatus.Approved)//If rejected Update Status Only...
                            {
                            
                                visaPassenger.UpdatePassengerStatus();
                            }
                            else if (Convert.ToInt32(Request["Status" + i]) == (int)VisaPassengerStatus.Approved)//If Approved Update Status Upload File
                            {
                                string fileExt = Path.GetExtension(file.FileName).ToLower();
                                string fileName = Path.GetFileName(file.FileName);
                                if (fileName != string.Empty)
                                {
                                    try
                                    {
                                        string docPath = "ApplicationId_" + visaDT1.Rows[0]["visaId"] + "/" + "PassengerId_" + visaDT1.Rows[i]["paxId"].ToString() + "_" + fileName;
                                        attatched.Add(appPath + "/PassengerId_" + visaDT1.Rows[i]["paxId"].ToString() + "_" + fileName);
                                        file.SaveAs(appPath + "/PassengerId_" + visaDT1.Rows[i]["paxId"].ToString() + "_" + fileName);
                                        if (Convert.ToInt32(Request["Status" + i]) == (int)VisaPassengerStatus.Approved)
                                        {
                                            countApproved++;
                                        }
                                        visaPassenger.VisaDocumentPath = docPath;
                                        visaPassenger.VisaNumber = Request["txtvisaNumber" + i];
                                        visaPassenger.VisaIssue = Convert.ToDateTime(Request["txtIssueDate" + i], ci);
                                        visaPassenger.VisaExpiry = Convert.ToDateTime(Request["txtExpiryDate" + i], ci);                                     
                                        int Status = visaPassenger.UpdatePaxStatusAndPath();
                                        if (Status > 0)
                                        {
                                            message = "Status Updated successfully for Transaction Id " + Convert.ToInt32(Request["visaId"]) + ".";
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:ViewBookingForVisa.aspx, Updating Pax Status Error Message:" + ex.Message + " | " + DateTime.Now, "");
                                    }
                                }
                            }
                            selectedStatus = true;
                        }
                        else if (Request["Status" + i] != null)
                        {
                            visaQueue.VisaId = Convert.ToInt32(visaDT1.Rows[0]["visaId"]);
                            visaQueue.VisaStatusId = Convert.ToInt32(visaDT1.Rows[i]["visaStatus"]);
                            visaQueue.VisaRemarks = Request["PaxRemarks" + i];
                            visaQueue.VisaDepositFee = Convert.ToDecimal(visaDT1.Rows[i]["visaDepositFee"]);
                            visaQueue.VisaRefundableFee = Convert.ToDecimal(visaDT1.Rows[i]["visaRefundableFee"]);
                            visaQueue.UpdatedBy = (int)Settings.LoginInfo.UserID;
                            visaQueue.UpdateVisaQueueHistory();
                            selectedStatus = false;
                        }
                        else if (Request["Status" + i] == null)
                        {
                            if (Convert.ToInt32(visaDT.Rows[i]["paxStatus"]) == (int)VisaPassengerStatus.Approved)
                            {
                                countApproved++;
                            }
                        }
                    }
                    if (selectedStatus)
                    {
                        int visaStatus = 0;
                        if (countCancelled == visaDT1.Rows.Count)
                        {
                            visaStatus = (int)VisaStatus.Cancelled;
                        }
                        else
                        {

                            if (countApproved == visaDT1.Rows.Count)
                            {
                                visaStatus = (int)VisaStatus.Approved;
                            }
                            else if (countApproved == 0)
                            {
                                visaStatus = (int)VisaStatus.Rejected;
                            }
                            else
                            {
                                visaStatus = (int)VisaStatus.PartiallyApproved;
                            }
                        }
                        visaQueue.VisaId = Convert.ToInt32(visaDT1.Rows[0]["visaId"]);
                        List<VisaCountry> countries = VisaCountry.GetActiveVisaCountryList();
                        VisaApplication application = new VisaApplication(visaId);
                        VisaCountry country = countries.Find(delegate(VisaCountry vc) { return vc.CountryCode == application.CountryCode; });
                        visaQueue.VisaStatusId = visaStatus;
                        
                        for (int i = 0; i < visaDT1.Rows.Count; i++)
                        {
                            if (Request["PaxRemarks" + i] != null && Request["Status" + i] != null && !string.IsNullOrEmpty(Request["PaxRemarks" + i]))
                            {
                                visaQueue.VisaRemarks += "Passenger Status Changed-" + Request["PaxRemarks" + i]+" ";
                                visaQueue.UpdatedBy = (int)Settings.LoginInfo.UserID;
                                visaQueue.UpdateVisaQueueHistory();
                            }
                        }
                        selectedStatus = false;
                        int Status1 = visaQueue.UpdateVisaStatus();
                        if (Status1 > 0)
                        {
                            string visaTripId = "VISA " + Convert.ToDateTime(visaDT.Rows[0]["visaBookingDate"]).ToString("yy") + Convert.ToDateTime(visaDT.Rows[0]["visaBookingDate"]).ToString("MM") + Convert.ToInt32(Request["visaId"]);
                            message = "Status Updated successfully for Transaction Id:" + visaTripId + ".";
                            //message = "Status Updated successfully for Transaction Id: VISA " + Convert.ToDateTime(visaDT.Rows[0]["visaBookingDate"]).ToString("yy") + Convert.ToDateTime(visaDT.Rows[0]["visaBookingDate"]).ToString("MM") + Convert.ToInt32(Request["visaId"]) + ".";
                            string emails = visaDT.Rows[0]["UserEmail"].ToString() + "," + visaDT.Rows[0]["PaxEmail"].ToString();//Send mail to Visa Pax also apart from B2C Customter
                            createEmail(emails, (VisaStatus)visaStatus, country.CountryName, visaTripId,attatched);
                        }
                    }
                }

                #endregion
                else if (visaStatuscheck == (int)VisaStatus.RequestChange)
                {
                    int countCancelled = 0;
                    for (int i = 0; i < visaDT1.Rows.Count; i++)
                    {
                        visaQueue.PaxId = Convert.ToInt32(visaDT1.Rows[i]["paxId"]);
                        visaPassenger.PaxId = Convert.ToInt32(visaDT1.Rows[i]["paxId"]);
                        visaPassenger.Status = VisaPassengerStatus.Cancelled;
                        if (Convert.ToInt32(Request["Status" + i]) == (int)VisaPassengerStatus.Cancelled)
                        {
                            countCancelled++;
                        }
                        visaPassenger.Remarks = Request["PaxRemarks" + i];
                        visaPassenger.VisaId = Convert.ToInt32(visaDT1.Rows[0]["visaId"]);
                        
                        visaPassenger.UpdatePassengerStatus();
                    }
                    visaQueue.VisaId = Convert.ToInt32(visaDT1.Rows[0]["visaId"]);
                    int visaStatus = 0;
                    if (countCancelled == visaDT1.Rows.Count)
                    {
                        visaStatus = (int)VisaStatus.Cancelled;
                    }
                    List<VisaCountry> countries = VisaCountry.GetActiveVisaCountryList();
                    VisaApplication application = new VisaApplication(visaId);
                    VisaCountry country = countries.Find(delegate(VisaCountry vc) { return vc.CountryCode == application.CountryCode; });
                    visaQueue.VisaStatusId = visaStatus;
                    for (int i = 0; i < visaDT1.Rows.Count; i++)
                    {
                        visaQueue.VisaRemarks = "Passenger Status Changed" +"-"+ Request["PaxRemarks" + i];
                    }
                    int Status = visaQueue.UpdateVisaStatus();
                    if (Status > 0)
                    {
                        string visaTripId = "VISA " + Convert.ToDateTime(visaDT.Rows[0]["visaBookingDate"]).ToString("yy") + Convert.ToDateTime(visaDT.Rows[0]["visaBookingDate"]).ToString("MM") + Convert.ToInt32(Request["visaId"]);
                        message = "Status Updated successfully for Transaction Id:" + visaTripId + ".";
                        //message = "Status Updated successfully for Transaction Id: VISA " + Convert.ToDateTime(visaDT.Rows[0]["visaBookingDate"]).ToString("yy") + Convert.ToDateTime(visaDT.Rows[0]["visaBookingDate"]).ToString("MM") + Convert.ToInt32(Request["visaId"]) + ".";
                        string emails = visaDT.Rows[0]["UserEmail"].ToString() + "," + visaDT.Rows[0]["PaxEmail"].ToString();//Send mail to Visa Pax also apart from B2C Customter
                        createEmail(emails, (VisaStatus)visaStatus, country.CountryName, visaTripId,new List<string>());
                    }
                }
                if (selectedStatus)
                {
                    visaQueue.VisaFee = Convert.ToDecimal(visaDT1.Rows[0]["visaCostFee"]);
                    visaQueue.VisaMarkup = Convert.ToDecimal(visaDT1.Rows[0]["visaMarkupFee"]);
                    visaQueue.VisaInsuranceFee = Convert.ToDecimal(visaDT1.Rows[0]["visaInsuranceFee"]); visaQueue.VisaDepositFee = Convert.ToDecimal(visaDT1.Rows[0]["visaDepositFee"]);
                    visaQueue.VisaRefundableFee = Convert.ToDecimal(visaDT1.Rows[0]["visaRefundableFee"]);
                    visaQueue.UpdatedBy = (int)Settings.LoginInfo.UserID;
                    visaQueue.UpdateVisaQueueHistory();
                }
                GetVisaDetails();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:ViewBookingForVisa.aspx,Updating Pax status Error Message:" + ex.Message + " | " + DateTime.Now, "");
            }
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        //    try
        //    {   
        //        string attachment = "attachment; filename=VisaDocumentList.xls";
        //        Response.ClearContent();
        //        Response.AddHeader("content-disposition", attachment);
        //        Response.ContentType = "application/vnd.ms-excel";
        //        System.IO.StringWriter sw = new System.IO.StringWriter();
        //        HtmlTextWriter htw = new HtmlTextWriter(sw);
        //        dgVisaDocumentList.AllowPaging = false;
        //        dgVisaDocumentList.DataSource = visaDT;
        //        dgVisaDocumentList.DataBind();
        //        dgVisaDocumentList.RenderControl(htw);
        //        Response.Write(sw.ToString());
        //    }
        //    catch (Exception ex)
        //    {
        //        Label lblMasterError = (Label)this.Master.FindControl("lblError");
        //        lblMasterError.Visible = true;
        //        lblMasterError.Text = ex.Message;
        //    }
        //    finally
        //    {
        //        Response.End();
        //    }
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        ViewState["CheckRefresh"] = Session["CheckRefresh"];
    }
    #region Added By hari
    private void GetPaymentSource()
    {
        Array Statuses = Enum.GetValues(typeof(CT.AccountingEngine.PaymentGatewaySource));
        foreach (CT.AccountingEngine.PaymentGatewaySource status in Statuses)
        {
            ListItem item = new ListItem(Enum.GetName(typeof(CT.AccountingEngine.PaymentGatewaySource), status), ((int)status).ToString());
            ddlEnumValue.Items.Add(item);
        }
        ddlEnumValue.Items.Insert(0, new ListItem("-- select --", "-1"));
        ddlEnumValue.SelectedValue = "-1";
    }
    private void clear()
    {
        txtorderid.Text = "";
        txtpaymentid.Text = "";      
        ddlEnumValue.ClearSelection();
        
    }
    #endregion
}
