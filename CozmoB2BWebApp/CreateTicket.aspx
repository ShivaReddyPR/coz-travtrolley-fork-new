<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="CreateTicket" CodeBehind="CreateTicket.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.TicketReceipt.Common" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="System.Collections.Generic" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">

 <%--   <script src="Scripts/jsBE/ModalPop.js" type="text/javascript"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <script src="yui/build/animation/animation-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <script type="text/javascript">
        toolTip = new YAHOO.widget.Tooltip("Tooltip", {context:"GetETicketLink", text:"Gets E-Ticket detail from source, if generated.", showDelay:500 } );  
        toolTip1 = new YAHOO.widget.Tooltip("Tooltip1", {context:"ETicketButton", text:"Make E-Ticket", showDelay:500 } );
        toolTip2 = new YAHOO.widget.Tooltip("Tooltip2", {context:"CreateManualTicketLink", text:"Creates Ticket Manually", showDelay:500 } );
    </script>
    <script type="text/javascript">
        function StartTicket()
        {        
             var isPassed=true;        
             if(document.getElementById('corporateCodeBox'))
             {
                 var corpCode = document.getElementById('corporateCodeBox').value;
                 var tourCode = document.getElementById('tourCodeBox').value;
                 var endorsement = document.getElementById('endorsementBox').value;
                 var remarks = document.getElementById('remarksBox').value;
                 corpCodePattern = /^[a-zA-Z0-9]*$/;
                 if(!corpCodePattern.test(corpCode))
                 {
                    document.getElementById('ETicketMessage').style.color="Red";
                    document.getElementById('ETicketMessage').innerHTML = "Invalid Corporate code"
                    isPassed=false;        
                 }
                 if(!corpCodePattern.test(tourCode))
                 {
                    document.getElementById('ETicketMessage').style.color="Red";
                    document.getElementById('ETicketMessage').innerHTML = "Invalid Tour code"
                    isPassed=false;        
                 }
                 if(!corpCodePattern.test(endorsement))
                 {
                    document.getElementById('ETicketMessage').style.color="Red";
                    document.getElementById('ETicketMessage').innerHTML = "Invalid Endorsement code"
                    isPassed=false;        
                 }
             }
             if(isPassed)
             {
                document.getElementById('ETicketMessage').style.color="black";      
//                ModalPop.AcceptButton.disabled = true;
//                ModalPop.DenyButton.disabled = true;
                document.getElementById('ETicketMessage').innerHTML = "Generating E-Ticket ... ";
//                //document.getElementById('corporateCode').value=document.getElementById('corpCode').value;
//                document.getElementById('corporateCode').value=document.getElementById('corporateCodeBox').value;
//                document.getElementById('tourCode').value=document.getElementById('tourCodeBox').value;
//                document.getElementById('endorsement').value=document.getElementById('endorsementBox').value;
//                document.getElementById('remarks').value=document.getElementById('remarksBox').value;
//                //$('ticket').submit();
                var url = 'AutoTicket.aspx?flightId=' + <%=itinerary.FlightId %>;
//                url = url + '&corporateCode=' + document.getElementById('corporateCode').value;
//                url = url + '&tourCode=' + document.getElementById('tourCode').value ;
//                url = url + '&endorsement=' + document.getElementById('endorsement').value;
//                url = url + '&remarks=' + document.getElementById('remarks').value;
                window.location.href = url;
             }  
        }
//        function GenerateTicket()
//        {
//        document.getElementById('popupbox').style.display='block';
//            ModalPop.AcceptButton.onclick = StartTicket;
//            var modalMessage="";

//            modalMessage += "<div id=\"ETicketMessage\" style=\"margin-top:15px\"></div>";
//            ModalPop.Show({x: 300, y: 300}, modalMessage)
//        }
                     
    </script>--%>
    <asp:HiddenField ID="hdfFlightId" runat="server" />
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="PrimaryView" runat="server">
  <table width="98%" align="right" border="0" cellspacing="3" cellpadding="0">
  <tr height="50px">
  <td width="50%">
  </td>
  <td width="50%">
  </td>
  </tr>
  
 <tr>
 <td>
  <div class="fleft font-size-16 fleft"> Agency Name: 
                <asp:Label ID="lblAgeny" Font-Bold="true" runat="server" Text=""></asp:Label>
                <asp:Label ID="lblAgentName" Font-Bold="true" runat="server" Text=""></asp:Label>
                   
                </div>
 </td>
 <td>
  <div class="fright">
               <%-- <asp:Image ID="imgAlert" runat="server" ImageUrl="~/images/alert.gif" Visible="false" />--%>
                    PNR:
                            <asp:Label ID="lblPNRNo" runat="server" Font-Bold="true" Text=""></asp:Label>
                        </span>
                    <asp:Label ID="lblAlertmsg" CssClass="fleft width-100" runat="server"></asp:Label>
                </div>
 </td>
 </tr>
 <tr>
 <td>
  <div class="fleft">
                    Booking Date: <span class="bold">
                    <asp:Label ID="lblBkgDate" runat="server" Text=""></asp:Label> &nbsp;&nbsp;
&nbsp;                        
                    </span>
                </div>
                <div class="margin-left-50 fleft">
                    Travel Date: <span class="bold">
                    <asp:Label ID="lblTravelDate" runat="server" Text=""></asp:Label>
                        
                    </span>
                </div>
 </td>
 </tr>
 <tr>
<td> &nbsp;</td>
 </tr>
 <tr>
 <td>
 <table width="50%"  style="border:solid 1px #ccc;" >
 <tr>
 <td>
  <div class="ns-h3" > Passenger Details </div>         
 <div>
 <%  
                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        if (IsTicketed(itinerary.Passenger[i].PaxId, ticketList))
                        {
                            continue;
                        }
                %>                
                <div class="clear">
                    <div class="width-250 fleft margin-top-10">
                    <%string title = string.Empty;
                      if (itinerary.Passenger[i].Title != null)
                      {
                          title = itinerary.Passenger[i].Title + " ";
                      } %>
                        <% = title + itinerary.Passenger[i].FirstName%>
                        <% = itinerary.Passenger[i].LastName %>
                    </div>
                    <div class="fleft">
                        <form method="post" name="paxForm<% = i %>" action="ManualTicket.aspx">
                            <input name="bookingId" type="hidden" value="<% = bookingId %>" />
                            <input name="paxId" type="hidden" value="<% = itinerary.Passenger[i].PaxId %>" />
                            <%--<a id="CreateManualTicketLink" href="javascript:document.paxForm<% = i %>.submit();">Create Manual Ticket</a>--%>
                        </form>
                    </div>
                    <% if (eTicketEligible == true) { %>
                    <div class="fleft" style="margin-left:10px;">
                        <form method="post" name="paxForm1<% = i %>" action="ManualTicket.aspx?eticketMode=true">
                            <input name="bookingId" type="hidden" value="<% = bookingId %>" />
                            <input name="pnrNo" type="hidden" value="<% = itinerary.PNR %>" />
                            <input name="paxId" type="hidden" value="<% = itinerary.Passenger[i].PaxId %>" />
                            <%  %>
                            <%--<a id="GetETicketLink" href="javascript:document.paxForm1<% = i %>.submit();" >Get E-Ticket detail</a>--%>
                        </form>
                    </div>
                    <% } %>
                </div>
                <%  }   %>
        
        </div>
 </td>
 </tr>
 </table>

 </td>
 </tr>
 <tr>
 <td> &nbsp;</td>
 </tr>
 <tr>
 <td align="center">
   <div class="fleft padding-3 creating-ticket-child margin-top-20 border-bottom-black padding-left-10">
               <% if (eTicketEligible && !ticketed && airlineEligible && ticketList.Count == 0) 
               {
                  // if ((isDomestic && Settings.LoginInfo.MemberType == MemberType.ADMIN) || (isDomestic == false && Settings.LoginInfo.MemberType == MemberType.ADMIN))
                  //{ %> 
                <div class="center">
                    <%--<form id="ticket" method="post" action="AutoTicket.aspx">--%>
                        <input name="flightId" type="hidden" value="<% = itinerary.FlightId %>" />
                        <input name="tourCode" type="hidden" id="tourCode"/>
                        <input name="endorsement" type="hidden" id="endorsement"/>
                        <input name="remarks" type="hidden" id="remarks"/>
                        <input name="corporateCode" type="hidden" id="corporateCode"/>                
                        <%--<input id="ETicketButton" onclick="StartTicket()" type="button" value=" Generate E-Ticket " />--%>
                        <asp:Button ID="btnGenerateTicket" runat="server" Text="Generate E-Ticket" 
                        onclick="btnGenerateTicket_Click" />
                    <%--</form>--%>
                <%--<a class="black-links" href="AutoTicket.aspx?pnr=<% = itinerary.PNR %>">Generate E-Ticket</a>--%>
                </div>
              <% //} 
                  } 
                  %>
               
            </div>
 </td>
 </tr>
 
 <tr>
 <td>
  <%--<div id="ETicketMessage" style="margin-top:15px"></div>--%>
  <asp:Label ID="lblETicketMsg" runat="server" Text=""></asp:Label>
  
 </td>
 </tr>
  </table>
  </asp:View>
  <asp:View ID="SecondaryView" runat="server">
    <%if (errorMessage.Length >0)
      { %>
    <div class="width-720 margin-left-30" style="text-align: center">
        <br />
        <br />
        <br />
        <span>
            <% = errorMessage%>
        </span>
        <br />
        <br />
       <%-- <%if (response.Status > 0 && response.Status != CT.BookingEngine.TicketingResponseStatus.NotSaved)
          { %>--%>
        <span>Go to <a href="AGENTBOOKINGQUEUE.ASPX">Booking Queue</a> or <a href="ViewBookingForTicket.aspx?bookingId=<% = booking.BookingId %>">
            View Booking</a>
            <% //TODO: Navigation links here. %>
        </span>
      <%--  <%}
          else if (response.Status > 0)
          { %>--%>
       <%-- <span>Go to <a href="FailedBookingQueue.aspx">Pending Queue</a>--%>
            <% //TODO: Navigation links here. %>
       <%-- </span>--%>
       <%-- <%} %>--%>
        <br />
        <br />
        <span>For further assistance contact
            <%= CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"]%>. </span>
        <br />
        <br />
    </div>
    <%} %>
    </asp:View>
        </asp:MultiView>
        <div class="fleft margin-top-10 form-parent-width" style="margin-left:200px; margin-top:150px;">
            <div class="rules-parent-width margin-left-40">
                <span class="font-red">
                <asp:Label ID="lblErrMsg" runat="server" Text=""></asp:Label>
                </span>
            </div>
        
            <div class="rules-parent-width margin-left-40">
                <span>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/alert.gif" Visible="false" AlternateText="Alert" /></span> 
                    <span class="margin-left-15">
                    <asp:Label ID="lblAltMsg" runat="server" Text= ""></asp:Label>
                    </span>
            </div>
       
          
        </div>
        <%  if (ticketList.Count > 0)
            {   %>
        <div class="fleft margin-top-10 locked-pnr-parent-width dotted-border padding" style="text-align:center">
            <%  //TODO: add Contract title %>
            
            <div class="fleft bold">
                <span class="fleft width-100px">Ticket No</span>
                <span class="fleft width-130 margin-left-10">Passenger</span>
                <span class="fleft width-120 margin-left-10">Routing</span>
                <span class="fleft width-70 margin-left-10">Fare Type</span>
                <span class="fleft width-70 margin-left-10">Airline</span>
                <span class="fleft width-70 margin-left-10">Status</span>
            </div>
            <%  int counter = 0;
                foreach (Ticket ticket in ticketList)
                {   %>
            <div class="fleft margin-top-15">
                <span class="fleft width-100px"><%=ticket.ValidatingAriline%><% = ticket.TicketNumber%></span>
                <%string title = string.Empty;
                  if (ticket.Title != null)
                  {
                      title = ticket.Title + " ";
                  } %>
                <span class="fleft width-130 margin-left-10"><% = title + ticket.PaxFirstName%> <% = ticket.PaxLastName%></span> 
                <span class="fleft width-120 margin-left-10"><% = routing%></span>
                <span class="fleft width-70 margin-left-10"><% = itinerary.FareType%></span>
                <span class="fleft width-70 margin-left-10"><% = ticket.ValidatingAriline%></span>
                <span class="fleft width-70 margin-left-10"><% = ticket.Status%></span>
                <div class="fleft width-70 margin-left-10">
                    <form id="Edit<% = ticket.TicketId %>" action="ManualTicket.aspx" method="post">                       
                        <input name="eticketMode" type="hidden" value="<%=ticket.ETicket.ToString().ToLower() %>" />                                                 
                        <input name="pnrNo" type="hidden" value="<% = itinerary.PNR %>" />                          
                        <input name="bookingId" type="hidden" value="<% = bookingId %>" />
                        <input name="paxId" type="hidden" value="<% = ticket.PaxId %>" />
                        <input name="ticketId" type="hidden" value="<% = ticket.TicketId %>" />
                        <input name="ticketStatus" type="hidden" value="<%=ticket.Status %>" />
                        <input name="editMode" type="hidden" value="true" />
                        <a href="javascript:document.getElementById('Edit<% = ticket.TicketId %>').submit();">Edit</a>
                        <a href="ETicket.aspx?ticketId=<%=ticket.TicketId%>" target="_blank">View</a>
                    </form>
                </div>
            </div>
            <%      counter++;
                } %>
        </div>        
        <%  }
            if (ticketed)
            { %>
        <div class="fleft margin-top-15 width-315">
        
            <form action="CreateInvoice.aspx" method="post">
                <div>
                    Remarks</div>
                <div class="margin-top-5 width-300 padding-left-10">
                    <textarea name="remarks" cols="30" rows="3" style="width: 300px; height: 50px;"></textarea>
                </div>
                
                <div class="fright margin-top-10">
                    <div class="margin-left-10">
                        <input name="flightId" type="hidden" value="<% = itinerary.FlightId %>" />
                        <input name="agencyId" type="hidden" value="<% = booking.AgencyId %>" />
                        <input name="pnr" type="hidden" value="<% = itinerary.PNR %>" />
                        <input name="routing" type="hidden" value="<% = routing %>" />
                        <%if (Invoice.isInvoiceGenerated(ticketList[0].TicketId) > 0)
                          { %>
                            <input type="submit" value=" View Invoice " />
                          <%}
                          else
                          { %>
                            <input type="submit" value=" Generate Invoice " />
                        <%} %>
                    </div>
                
                </div>
            </form>
        </div>
        <%  } %>
   
   







    <!-- Modified by Lokesh on 25Sep2019-->
    <!-- New Design for E-ticket in Email-->

        <div id='EmailDiv' runat='server' style='width: 600px; display: none;'>
       <!--Start:__________________Email Div Start_____________-->
        <%if (isBookingSuccess) %>
        <%{ %>
         <div>

                            <%  int bc = 0;
                                List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();
                                  if (ticketList != null && ticketList.Count > 0)
                                  {
                                      ptcDetails = ticketList[0].PtcDetail;
                                  }
                                  else// For Hold Bookings
                                  {
                                      ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(flightItinerary.FlightId);
                                  }
                                for (int count = 0; count < flightItinerary.Segments.Length; count++)
                                {
                                    CT.Core.Airline airline = new CT.Core.Airline();
                                    airline.Load(flightItinerary.Segments[count].Airline);

                                    int paxIndex = 0;
                                    if (Request["paxId"] != null)
                                    {
                                        paxIndex = Convert.ToInt32(Request["paxId"]);
                                    }
                                    List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();

               ptcDetail = ptcDetails.FindAll(delegate(SegmentPTCDetail ptc) { return ptc.SegmentId == flightItinerary.Segments[count].SegmentId; });
                                    %>

  <div style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box; -webkit-text-size-adjust: 100%; Margin: 0; background: #f3f3f3 !important; box-sizing: border-box; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100% !important">
    <span class="preheader" style="color: #f3f3f3; display: none !important; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; mso-hide: all !important; opacity: 0; overflow: hidden; visibility: hidden"></span>
    <table class="body" style="Margin: 0; background: #f3f3f3 !important; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
      <tr style="padding: 0; text-align: left; vertical-align: top">
        <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
          <center data-parsed="" style="min-width: 700px; width: 100%">
            <table align="center" class="wrapper b2b-eticket-wrapper float-center" style="Margin: 0 auto; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><td class="wrapper-inner" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
<%if (count == 0)
    { %>
            	<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
       <% } %>     
            	<table align="center" class="container" style="Margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0; text-align: inherit; vertical-align: top; width: 700px"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
            


<%if (count == 0) { %>
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            
            
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-9 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 509px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<h2 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 22px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left; word-wrap: normal"><strong>e-Ticket</strong></h2>
            			 <span> <%if (flightItinerary != null && flightItinerary.CreatedOn != DateTime.MinValue) %>
                    <%{ %>
                                              Booking Date:  <%=flightItinerary.CreatedOn.ToString("ddd") + "," + flightItinerary.CreatedOn.ToString("dd MMM yyyy")%>
                    <%} %>	</span>
            		</th></tr></table></th>
            		<th class="small-12 large-3 columns last" right="" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 159px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">            			

                             <asp:Image ID="imgLogo" style="-ms-interpolation-mode: bicubic; border-width: 0px; clear: both; display: block; height: 51px; max-width: 100%; outline: none; text-decoration: none; width: 159px" runat="server" AlternateText="AgentLogo"/>

            		</th></tr></table></th>
            	  </tr></tbody></table>
            	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 15px; font-weight: bold; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left; word-wrap: normal">Reservation Details</h3>
            		</th></tr></table></th>
            		<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<table class="float-right text-right" id="tblPrintEmailActions" align="right" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: right; vertical-align: top; width: 100%">
            				<tr style="padding: 0; text-align: left; vertical-align: top">
            					<td class="float-right text-right" valign="middle" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: right; vertical-align: middle; word-wrap: break-word">
            						<span class="float-right text-right" style="Margin: 0; color: #2199e8; font-family: 'Open Sans', Arial, sans-serif; font-weight: normal; height: 20px; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none">
                                        
<%--                                             <img id="imgEmail" runat="server" src="https://ctb2b.cozmotravel.com/images/email_icon.png" alt="Email" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: inline; height: 12px; margin-right: 5px; max-width: 100%; outline: none; text-decoration: none; vertical-align: middle; width: 15px" />
                                              <asp:LinkButton ID="btnEmail" runat="server" Text="Email" OnClientClick="return ShowPopUp(this.id);" />--%>
                                            </span>
            				<span class="float-right text-right" style="Margin: 0; color: #2199e8; font-family: 'Open Sans', Arial, sans-serif; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none">
                           
<%--                                    <img id="imgPrint" runat="server" src="https://ctb2b.cozmotravel.com/images/print_icon.jpg" alt="Print" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: inline; height: 20px; max-width: 100%; outline: none; text-decoration: none; vertical-align: middle; width: 20px" class="" />

                                          <asp:LinkButton ID="btnPrint" runat="server" Text="Print" OnClientClick="return prePrint();" />--%>
                                    </span>	
            					</td>
            				</tr>
            			</table>
            			
            		</th></tr></table></th>
            	  </tr></tbody></table>
            
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody>
                      <tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">PNR NO. | <strong> <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR)%></strong></p>
            	  	</th></tr></table></th>

                          <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo != null) %>
                          <%{
                                  var parentid1 =CT.TicketReceipt.BusinessLayer.AgentMaster.GetParentId(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId);
                                  CT.TicketReceipt.BusinessLayer.LocationMaster location1 = new CT.TicketReceipt.BusinessLayer.LocationMaster(flightItinerary.LocationId); 
                                  
                                  %>
                                  
                          <%if (parentid1 == 2125 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId == 2125) %>
                          <%{ %>
            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">Client |  <strong> <%=!string.IsNullOrEmpty(flightItinerary.TripId) ? new CT.TicketReceipt.BusinessLayer.AgentMaster(parentid1).Name : agency.Name %></strong>Address | <strong> <%=location1.Address %></strong> </p>
            	  	</th></tr></table></th>
                          <%} %>
                          <%else %>
                          <%{ %>
                          <th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">Agent Name |  <strong> <%=!string.IsNullOrEmpty(flightItinerary.TripId) && agency.AgentParantId > 0 ? new CT.TicketReceipt.BusinessLayer.AgentMaster(agency.AgentParantId).Name : agency.Name %></strong></p>
            	  	</th></tr></table></th>

                          <%} %>

                          <%} %>


            	  </tr>




       


  <%var parentid =CT.TicketReceipt.BusinessLayer.AgentMaster.GetParentId(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId);
                                                                                         if (parentid == 2125 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId==2125)

                                                                                         {
                                                                                      CT.TicketReceipt.BusinessLayer.LocationMaster location = new CT.TicketReceipt.BusinessLayer.LocationMaster(flightItinerary.LocationId);       %>

                  <tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">GSTIN | <strong>  <%=location.GstNumber %></strong></p>
            	  	</th></tr></table></th>
            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"></th>
            	  </tr>
        <%} %>


            	                                                                                                                                                                              </tbody></table>
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
        <%if(!string.IsNullOrEmpty(flightItinerary.RoutingTripId) && Settings.LoginInfo.IsCorporate!="Y") { %>
                       	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left"><%=!string.IsNullOrEmpty(Settings.LoginInfo.IsCorporate) && Settings.LoginInfo.IsCorporate == "Y"?"Corporate Booking Code":"Routing Trip Id"%>| <strong> <%=flightItinerary.RoutingTripId %></strong></p>
            	  	</th></tr></table></th>
 <%} %>

            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">PHONE  |  <strong><%=agency.Phone1 %></strong></p>
            	  	</th></tr></table></th>
            	  </tr></tbody></table>

    <% }%>









        <%if (count == 0)
                                                { %>
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">	
            	  		<table class="b2b-baggage-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">

                              <%--passanger header row  starts --%>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Passenger Name</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">E-Ticket Number	</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Baggage</th>
            				
                            <%if (flightItinerary != null && flightItinerary.FlightBookingSource == BookingSource.UAPI || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp)  %>
                                                        <%{ %>        
                                  	<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Meal</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Seats</th>
       <%} %>


            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Airline Ref.</th>
            	  			</tr>
                              <%--passanger header row  ends --%>



                                 <% if (flightItinerary != null)
                                                        {%>
                                                    <%for (int j = 0; j < flightItinerary.Passenger.Length; j++)
                                                        {
                                                    %>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">	
                                                   <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">      <%=flightItinerary.Passenger[j].Title + " " + flightItinerary.Passenger[j].FirstName + " " + flightItinerary.Passenger[j].LastName%></td>
                                                  <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">

                                                             <%if (ticketList != null && ticketList.Count > 0) { %>
                                           <%=(ticketList[j].TicketNumber.Split('|').Length > 1 ? ticketList[j].TicketNumber.Split('|')[flightItinerary.Segments[count].Group] : ticketList[j].TicketNumber) %>
                                            <%} else { //for Corporate HOLD Booking%>
                                        <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR)%>
                                            <%} %>


                                                  </td>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            
                                                            
   <%if (flightItinerary.FlightBookingSource == BookingSource.Amadeus || flightItinerary.FlightBookingSource == BookingSource.UAPI || (flightItinerary.FlightBookingSource == BookingSource.TBOAir && !(flightItinerary.IsLCC)))
    {%>
                             <%=GetBaggageForGDS(flightItinerary.FlightId,flightItinerary.Passenger[j].Type)%>
    <%}                                                            
 else if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.PKFares || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp || flightItinerary.FlightBookingSource == BookingSource.Jazeera)
    {
        if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
        { %>
                                                            <%=(string.IsNullOrEmpty(flightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[j].BaggageCode)%>
                                                     <%}
    }
    else if (flightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl && flightItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
                                                       
                                                            <%=(string.IsNullOrEmpty(flightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[j].BaggageCode)%>
                                                        
                                                    <%}
    //Added by lokesh on 22-06-2018
    //For Amadeus Air Source
    //Display baggage information based on the pax type.
    else if (flightItinerary.FlightBookingSource == BookingSource.Amadeus && flightItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
              <%=(string.IsNullOrEmpty(flightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[j].BaggageCode)%>
    <%}

    else if (flightItinerary.FlightBookingSource == BookingSource.TBOAir && (flightItinerary.IsLCC))
    {
        if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
        {
            string strBaggage = string.Empty;
            
            if (!string.IsNullOrEmpty(flightItinerary.Passenger[j].BaggageCode))
            {
                strBaggage = flightItinerary.Passenger[j].BaggageCode;
            }
            else
            {
                strBaggage = "Airline Norms";
            }
                                                           %>
                                                      <%=strBaggage%> 
                                                    <%}
    } %> </td>


                                                        <%if (flightItinerary.FlightBookingSource == BookingSource.UAPI || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{
                                                                string MealDesc = string.Empty;
                                                                %>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (flightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(flightItinerary.Passenger[j].MealDesc)) %>
                                                            <%{
                                                                    MealDesc = flightItinerary.Passenger[j].MealDesc;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Meals"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (flightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(flightItinerary.Passenger[j].Seat.Code)) %>
                                                            <%{
                                                                    MealDesc = flightItinerary.Passenger[j].Seat.Code;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "NoSeat"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <%} %>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">  <%=(flightItinerary.Segments[count].AirlinePNR == null || flightItinerary.Segments[count].AirlinePNR == "" ? (flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR) : flightItinerary.Segments[count].AirlinePNR) %></td>
                                                    </tr>
                                                    <%}
                                                        }%>

                              <%--passanger row ends--%>




            	  		</table>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th>

            	  	                                                                                                                                                                                                                                                                                                                                                                                                                                                    </tr></table></th>	  	
            	  </tr></tbody></table>

    <% }%>





            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">			  
            	  	  <table class="flight-list-table" style="border: 1px solid #f3f3f3; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  	    <tr class="first-row" style="padding: 0; text-align: left; vertical-align: top">
            	  	    	<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 4px; text-align: left; vertical-align: top; word-wrap: break-word;">

            	  	    		<img class="float-left" src="https://ctb2b.cozmotravel.com/images/AirlineLogo/<%=airline.AirlineCode%>.gif" alt="<%=airline.AirlineName%>" style="-ms-interpolation-mode: bicubic; clear: both; display: block; float: left; max-width: 100%; outline: none; padding-right: 5px; text-align: left; text-decoration: none; width: auto">

            					<p class="float-left" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; padding-top: 5px; text-align: left"><%airline.Load(flightItinerary.Segments[count].Airline); %> <%=airline.AirlineName%> <strong><%=flightItinerary.Segments[count].Airline + "-" + flightItinerary.Segments[count].FlightNumber%></strong>
                                           <%try
                                                                                                {
                                                                                                    if ((!flightItinerary.IsLCC && flightItinerary.Segments[count].OperatingCarrier != null && flightItinerary.Segments[count].OperatingCarrier.Length > 0)||(flightItinerary.FlightBookingSource == BookingSource.Indigo && flightItinerary.Segments[count].OperatingCarrier != null && flightItinerary.Segments[count].OperatingCarrier.Length > 0 && flightItinerary.Segments[count].OperatingCarrier != "6E"))////Modified for Indigo CodeShare
                                                                                                    {
                                                                                                        string opCarrier = flightItinerary.Segments[count].OperatingCarrier;
                                                                                                        CT.Core.Airline opAirline = new CT.Core.Airline();
                                                                                                        if (opCarrier.Split('|').Length > 1)
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Split('|')[0]);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Substring(0, 2));
                                                                                                        } %>
                                             (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                                }
                                                catch { } %>
                                    
                                    </p>
            	  	    	</td>
            	  	    	<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 4px; text-align: left; vertical-align: middle; word-wrap: break-word">
            	  	    		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right"><strong> <%=flightItinerary.Segments[count].Origin.CityName%> to <%=flightItinerary.Segments[count].Destination.CityName%></strong> | <strong><%=flightItinerary.Segments[count].DepartureTime.ToString("ddd")+","+flightItinerary.Segments[count].DepartureTime.ToString("dd MMM yyyy")%></strong></p>
            	  	    	</td>
            	  	    </tr>
            			<tr style="padding: 0; text-align: left; vertical-align: top">
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word;width:30%">
            					<strong class="city-code" style="font-size: 18px"><%=flightItinerary.Segments[count].Origin.CityCode%></strong> <%=flightItinerary.Segments[count].Origin.CityName%><br>
            					<strong class="time" style="font-size: 14px"><%=flightItinerary.Segments[count].DepartureTime.ToString("HH:mm tt")%></strong><br>
            					<%=flightItinerary.Segments[count].DepartureTime.ToString("ddd")+","+flightItinerary.Segments[count].DepartureTime.ToString("dd MMM yyyy")%><br>
            					<%=flightItinerary.Segments[count].Origin.AirportName %><br>
            					Terminal <%=flightItinerary.Segments[count].DepTerminal %>
            				</td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: center; vertical-align: middle; word-wrap: break-word;width:20%">
            				 <center style="min-width: 21px; width: 100%" data-parsed="">
            						<img src="https://ctb2b.cozmotravel.com/images/aircraft_icon.png" alt="Flight Duration" style="-ms-interpolation-mode: bicubic; Margin: 0 auto; clear: both; display: block; float: none; margin: 0 auto; max-width: 100%; min-width: 21px; outline: none; text-align: center; text-decoration: none; vertical-align: middle; width: 21px" align="center" class="float-center">
            						   <%=flightItinerary.Segments[count].Duration.Hours +"hr "+flightItinerary.Segments[count].Duration.Minutes+"m"%>
            				  </center>
            			  </td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; border-right: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word;width:30%">
            					<strong class="city-code" style="font-size: 18px"><%=flightItinerary.Segments[count].Destination.CityCode%></strong> <%=flightItinerary.Segments[count].Destination.CityName%><br> 
            					<strong class="time" style="font-size: 14px"><%=flightItinerary.Segments[count].ArrivalTime.ToString("HH:mm tt")%></strong><br>
            					<%=flightItinerary.Segments[count].ArrivalTime.ToString("ddd")+","+flightItinerary.Segments[count].ArrivalTime.ToString("dd MMM yyyy")%><br>
            					<%=flightItinerary.Segments[count].Destination.AirportName %><br>
            					Terminal  <%=flightItinerary.Segments[count].ArrTerminal %>
            				</td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word;width:20%">
            					<strong>
                                     <%if (flightItinerary.Segments[count].Stops == 0) %>
                    <%{ %>
                    <strong> Non-Stop </strong>
                    <%} %>
                    <%else if (flightItinerary.Segments[count].Stops == 1) %>
                    <%{ %>
                    <strong> Single Stop </strong>
                    <%} %>
                    <%else if (flightItinerary.Segments[count].Stops == 2) %>
                    <%{ %>
                    <strong> Two Stops </strong>
                    <%} %>
                    <%else if (flightItinerary.Segments[count].Stops> 2) %>
                    <%{ %>
                    <strong> Two+ Stops </strong>
                    <%} %>

            					</strong><br>
            					Class | <%=flightItinerary.Segments[count].CabinClass%><br>
                                      <%if (flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp || flightItinerary.FlightBookingSource == BookingSource.Jazeera)
                                                                                                {%>
            					Fare Type | <%=flightItinerary.Segments[count].SegmentFareType%><br>
                                        <%} %>
            					<strong><%=booking.Status.ToString()%></strong>
            				</td>
            			</tr>










            		</table></th>

  

<%--<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th>--%>


            	  	                                                                                                                                                                                                                                                                                                                                                                                                                                                    </tr></table></th>		  	
            	  </tr></tbody></table>   <%}%>  



            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 15px; font-weight: bold; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left; word-wrap: normal"><%--Price Details--%></h3>
            	  	</th>
                          <th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: right"> <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: #5f5f5f;" id="ancFare" runat="server"
                                             class="showhideDiv"></a>	</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>


              
                           
                            <div title="Param" id="divFare" runat="server" style="font-size: 12px;">
                                 
                                
                            
                                 <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0, outputVAT = 0,Meal=0,SeatPrice=0;

                        if (ticketList != null && ticketList.Count > 0)
                        {
                            for (int k = 0; k < ticketList.Count; k++)
                            {
                                AirFare += ticketList[k].Price.PublishedFare + ticketList[k].Price.HandlingFeeAmount;
                                Taxes += ticketList[k].Price.Tax + ticketList[k].Price.Markup;
                                if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += ticketList[k].Price.AdditionalTxnFee + ticketList[k].Price.OtherCharges + ticketList[k].Price.SServiceFee + ticketList[k].Price.TransactionFee;
                                }
                                Baggage += ticketList[k].Price.BaggageCharge;
                                Meal += ticketList[k].Price.MealCharge;
                                SeatPrice += ticketList[k].Price.SeatPrice;
                                MarkUp += ticketList[k].Price.Markup;
                                Discount += ticketList[k].Price.Discount;
                                AsvAmount += ticketList[k].Price.AsvAmount;
                                outputVAT += ticketList[k].Price.OutputVATAmount;
                            }
                            if (ticketList[0].Price.AsvElement == "BF")
                            {
                                AirFare += AsvAmount;
                            }
                            else if (ticketList[0].Price.AsvElement == "TF")
                            {
                                Taxes += AsvAmount;
                            }
                        }
                        else
                        {
                            for (int k = 0; k < flightItinerary.Passenger.Length; k++)
                            {
                                AirFare += flightItinerary.Passenger[k].Price.PublishedFare + flightItinerary.Passenger[k].Price.HandlingFeeAmount;
                                Taxes += flightItinerary.Passenger[k].Price.Tax + flightItinerary.Passenger[k].Price.Markup;
                                if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += flightItinerary.Passenger[k].Price.AdditionalTxnFee + flightItinerary.Passenger[k].Price.OtherCharges + flightItinerary.Passenger[k].Price.SServiceFee + flightItinerary.Passenger[k].Price.TransactionFee;
                                }
                                Baggage += flightItinerary.Passenger[k].Price.BaggageCharge;
                                Meal += flightItinerary.Passenger[k].Price.MealCharge;
                                SeatPrice += flightItinerary.Passenger[k].Price.SeatPrice;
                                MarkUp += flightItinerary.Passenger[k].Price.Markup;
                                Discount += flightItinerary.Passenger[k].Price.Discount;
                                AsvAmount += flightItinerary.Passenger[k].Price.AsvAmount;
                                outputVAT += flightItinerary.Passenger[k].Price.OutputVATAmount;
                                if (flightItinerary.Passenger[k].Price.AsvElement == "BF")
                                {
                                    AirFare += AsvAmount;
                                }
                                else if (flightItinerary.Passenger[k].Price.AsvElement == "TF")
                                {
                                    Taxes += AsvAmount;
                                }
                            }
                        }
                    %>
                                            <% if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.BaseAgent)
                                                { %>

            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<table class="pricing-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Air Fare</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=AirFare.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right"> Taxes & Fees</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word"> <%=Taxes.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>


            	  				</td>
            	  			</tr>

                          <%if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.TBOAir && Baggage > 0 || flightItinerary.IsLCC || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp)
                                                                                    { %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Baggage Fare </th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=Baggage.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>

                                   <%} %>


                                        <%if (flightItinerary != null && (flightItinerary.FlightBookingSource == BookingSource.UAPI || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp)) %>
                                                                                <%{ %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Meal Price</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=Meal.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Seat Price</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">    <%=SeatPrice.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>

     <%} %>


             <%if (Discount > 0)
                                                                                    { %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Discount</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  -<%=Discount.ToString("N" + agency.DecimalValue) %><%=agency.AgentCurrency%></td>
            	  			</tr>
       <%} %>


            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">
                                                                 <%CT.TicketReceipt.BusinessLayer.LocationMaster locationGST = new CT.TicketReceipt.BusinessLayer.LocationMaster(flightItinerary.LocationId);
                                                                                             if (locationGST.CountryCode == "IN")
                                                                                            {%>
                                                                                        GST
                                                                                        <%}    else
    { %>
                                                                                        VAT
                                                                                        <%} %>
                                                                                           

                                      </th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=outputVAT.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  
                                 <th valign="middle" width="220px" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">
            				 	     <strong style="font-size: 11px"> This is an Electronic ticket. Please carry a positive identification for Check in.</strong>
            				     </th>					
            	  				<th width="220px" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right;vertical-align:middle"><strong>Total Air Fare</strong></th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word;vertical-align:middle">

                                       <%if (flightItinerary.FlightBookingSource == BookingSource.TBOAir) 
                                                                                            { %>
                                                                                        <b>
                                                                                            <%=Math.Ceiling((AirFare + Taxes + Baggage + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%>

                                                                                        </b>
                                                                                        <%}
                                                                                            else
                                                                                            { %>
                                                                                        <b><%=((AirFare + Taxes + Baggage + Meal + SeatPrice + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%} %>
            	  				</td>
            	  			</tr>
            	  		</table>
            	  	</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>

     <%
         }
                                              %>

         </div>

            	    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
             	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">For international flights, you are suggested to arrive at the airport at least 3-4 hours prior to your departure time to check in and go through security. For connecting flights, please confirm at the counter whether not you can check your luggage through to the final destination when you check in.</p>
            			 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">Don�t forget to purchase travel insurance for your visit.Please contact us to purchase travel insurance.</p>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>
            	
            
            
            
            
            
            
            	</td></tr></tbody></table>
            
            </td></tr></table>
            
          </center>
        </td>
      </tr>
    </table>



            	<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
  
    <!-- prevent Gmail on iOS font size manipulation -->
   <div style="display:none; white-space:nowrap; font:15px courier; line-height:0"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
  </div>


        <!--Start:__________________NOTIFICATION FOR PK FARES_____________-->
        <!-- Added by lokesh on 7-June-2018-->
        <!-- Need to display the below note in email for PK Fares -->
         <%if (flightItinerary != null && flightItinerary.FlightBookingSource == BookingSource.PKFares) %>
         <%{ %>
         <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                <span style="color:red">Note: This is just an itinerary and Unless ticket number is shown on the ticket copy.Ticket copy should not be handed over to customer.</span>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <%} %>
        <!--End:__________________NOTIFICATION FOR PK FARES_____________-->

        
             </div>
        <%} %>
             
        <!--End:__________________Email Div End _____________-->

   
   
    </div>
  
  
    <!--Email sending code for Corporate Profile -->




</asp:Content>
