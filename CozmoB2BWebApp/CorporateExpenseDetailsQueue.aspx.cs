﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.Web.UI.Controls;

public partial class CorporateExpenseDetailsQueueUI : CT.Core.ParentPage
{


    private string REIM_SEARCH_SESSION_APPROVED = "_ReimSearchListApproved";
    private string REIM_SEARCH_SESSION_PENDING = "_ReimSearchListPending";
    private string REIM_SEARCH_SESSION_REJECTED = "_ReimSearchListRejected";
    private string REIM_SEARCH_SESSION_REIMBURSED = "_ReimSearchListReimbursed";
    protected int userCorpProfileId;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {
                if (!Page.IsPostBack)
                {
                    IntialiseControls();

                }
            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateExpenseDetailsQueue) Page Load Event Error: " + ex.Message, "0");
        }
    }
    private void bindExpenseTypes()
    {
        try
        {


            ddlExpType.DataSource = PolicyExpenseDetails.GetAllExpenseTypes('A', "ET");
            ddlExpType.DataValueField = "SetupId";
            ddlExpType.DataTextField = "Name";
            ddlExpType.AppendDataBoundItems = true;
            ddlExpType.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            int employeeProfileId = 0;
            if (ddlEmployee.SelectedIndex > 0)
            {
                employeeProfileId = Convert.ToInt32(ddlEmployee.SelectedValue);
            }
            string expCategory = string.Empty;
            if (ddlExpCategory.SelectedIndex > 0)
            {
                expCategory = ddlExpCategory.SelectedValue;
            }
            string expenseType = string.Empty;
            if (ddlExpType.SelectedIndex > 0)
            {
                expenseType = ddlExpType.SelectedValue;
            }



            bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), employeeProfileId, expCategory, expenseType, "A");//Approved
            bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), employeeProfileId, expCategory, expenseType, "P");//Pending
            bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), employeeProfileId, expCategory, expenseType, "R");//Rejected
            bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), employeeProfileId, expCategory, expenseType, "I");//Reimbursed


        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateExpenseApprovals btnSearchApprovals Error: " + ex.Message, "0");
        }
    }



    private void IntialiseControls()
    {
        try
        {
            dcReimFromDate.Value = DateTime.Now;
            dcReimToDate.Value = DateTime.Now;

            int user_corp_profile_id = CorporateProfile.GetCorpProfileId((int)Settings.LoginInfo.UserID);
            if (user_corp_profile_id > 0)
            {
                this.userCorpProfileId = user_corp_profile_id;
            }
            if (user_corp_profile_id < 0)
            {
                user_corp_profile_id = 0;
            }


            bindEmployeesList();
            bindExpenseTypes();

            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

            bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), user_corp_profile_id, string.Empty, string.Empty, "A");//Approved
            bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), user_corp_profile_id, string.Empty, string.Empty, "P");//Pending
            bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), user_corp_profile_id, string.Empty, string.Empty, "R");//Rejected
            bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), user_corp_profile_id, string.Empty, string.Empty, "I");//Reimbursed

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void bindEmployeesList()
    {
        ddlEmployee.DataSource = CorporateProfile.GetCorpProfilesList((int)Settings.LoginInfo.AgentId);
        ddlEmployee.DataTextField = "Name";
        ddlEmployee.DataValueField = "ProfileId";
        ddlEmployee.DataBind();
        ddlEmployee.Items.Insert(0, new ListItem("--Select Employee--", "0"));
        ddlEmployee.Enabled = false;
        if (Settings.LoginInfo.MemberType == MemberType.TRAVELCORDINATOR || Settings.LoginInfo.MemberType == MemberType.SUPER || Settings.LoginInfo.MemberType == MemberType.ADMIN)
        {
            ddlEmployee.Enabled = true;
            ddlEmployee.SelectedIndex = 0;
        }
        else
        {
            try
            {
                //ddlEmployee.SelectedItem.Value = Convert.ToString(Settings.LoginInfo.CorporateProfileId);
                ddlEmployee.SelectedIndex = ddlEmployee.Items.IndexOf(ddlEmployee.Items.FindByValue(Convert.ToString(Settings.LoginInfo.CorporateProfileId)));

            }
            catch { }
        }
        //if (this.userCorpProfileId > 0)
        //{
        //    ddlEmployee.Enabled = false;
        //    ddlEmployee.SelectedItem.Value = Convert.ToString(userCorpProfileId);
        //}
    }


    protected void gvApproved_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvApproved.PageIndex = e.NewPageIndex;
            gvApproved.EditIndex = -1;
            FilterSearch2_Click(null, null);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateExpenseDetailsQueue)gvApproved__PageIndexChanging method .Error:" + ex.ToString(), "0");

        }
    }

    protected void gvPending_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvPending.PageIndex = e.NewPageIndex;
            gvPending.EditIndex = -1;
            FilterSearch1_Click(null, null);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateExpenseDetailsQueue)gvPending_PageIndexChanging method .Error:" + ex.ToString(), "0");

        }

    }

    protected void gvRejected_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvRejected.PageIndex = e.NewPageIndex;
            gvRejected.EditIndex = -1;
            FilterSearch3_Click(null, null);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateExpenseDetailsQueue)gvRejected_PageIndexChanging method .Error:" + ex.ToString(), "0");

        }

    }

    protected void gvReimbursed_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvReimbursed.PageIndex = e.NewPageIndex;
            gvReimbursed.EditIndex = -1;
            FilterSearch4_Click(null, null);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateExpenseDetailsQueue)gvReimbursed_PageIndexChanging method .Error:" + ex.ToString(), "0");

        }

    }


    protected void FilterSearch1_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={

                                         {"HTtxtDATE", "DATE" }
                                        ,{"HTtxtEMPLOYEEID", "EMP_ID" } 
                                        ,{"HTtxtEMPLOYEENAME", "EMP_NAME" }
                                         ,{"HTtxtEXPREF",  "EXP_REF"}
                                         ,{"HTtxtEXPCAT",  "EXP_CATEGORY"}
                                         ,{"HTtxtAMOUNT", "AMOUNT"}
                                         

                                        ,
                                         };
        CommonGrid g = new CommonGrid();
        g.FilterGridView(gvPending, SearchListPending.Copy(), textboxesNColumns);

    }

    protected void FilterSearch2_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={

                                         {"HTtxtDATE", "DATE" }
                                        ,{"HTtxtEMPLOYEEID", "EMP_ID" } 
                                        ,{"HTtxtEMPLOYEENAME", "EMP_NAME" }
                                         ,{"HTtxtEXPREF",  "EXP_REF"}
                                         ,{"HTtxtEXPCAT",  "EXP_CATEGORY"}
                                         ,{"HTtxtAMOUNT", "AMOUNT"}
                                         

                                        ,
                                         };
        CommonGrid g = new CommonGrid();

        g.FilterGridView(gvApproved, SearchListApproved.Copy(), textboxesNColumns);


    }


    protected void FilterSearch3_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={

                                         {"HTtxtDATE", "DATE" }
                                        ,{"HTtxtEMPLOYEEID", "EMP_ID" } 
                                        ,{"HTtxtEMPLOYEENAME", "EMP_NAME" }
                                         ,{"HTtxtEXPREF",  "EXP_REF"}
                                         ,{"HTtxtEXPCAT",  "EXP_CATEGORY"}
                                         ,{"HTtxtAMOUNT", "AMOUNT"}
                                         

                                        ,
                                         };
        CommonGrid g = new CommonGrid();

        g.FilterGridView(gvRejected, SearchListRejected.Copy(), textboxesNColumns);


    }

    protected void FilterSearch4_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={

                                         {"HTtxtDATE", "DATE" }
                                        ,{"HTtxtEMPLOYEEID", "EMP_ID" } 
                                        ,{"HTtxtEMPLOYEENAME", "EMP_NAME" }
                                         ,{"HTtxtEXPREF",  "EXP_REF"}
                                         ,{"HTtxtEXPCAT",  "EXP_CATEGORY"}
                                         ,{"HTtxtAMOUNT", "AMOUNT"}
                                         

                                        ,
                                         };
        CommonGrid g = new CommonGrid();
        g.FilterGridView(gvReimbursed, SearchListReimbursed.Copy(), textboxesNColumns);


    }
    private void bindSearch(DateTime fromDate, DateTime toDate, int profileId, string expCategory, string expType, string approvalStatus)
    {
        try
        {
            if (approvalStatus == "A") //Approved
            {
                DataTable dtApproved = CorporateProfileExpenseDetails.GetExpenseDetailsQueueList(profileId, fromDate, toDate, expCategory, expType, approvalStatus);
                if (dtApproved != null)// && dtApproved.Rows.Count > 0)
                {
                    SearchListApproved = dtApproved;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvApproved, dtApproved);
                }
            }
            if (approvalStatus == "P") //Pending
            {
                DataTable dtPending = CorporateProfileExpenseDetails.GetExpenseDetailsQueueList(profileId, fromDate, toDate, expCategory, expType, approvalStatus);
                if (dtPending != null )//&& dtPending.Rows.Count > 0)
                {
                    SearchListPending = dtPending;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvPending, dtPending);
                }
            }
            if (approvalStatus == "R") //Rejected
            {
                DataTable dtRejected = CorporateProfileExpenseDetails.GetExpenseDetailsQueueList(profileId, fromDate, toDate, expCategory, expType, approvalStatus);
                if (dtRejected != null)// && dtRejected.Rows.Count > 0)
                {
                    SearchListRejected = dtRejected;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvRejected, dtRejected);
                }
            }

            if (approvalStatus == "I") //Complete Reimbursement
            {
                DataTable dtReimbursement = CorporateProfileExpenseDetails.GetExpenseDetailsQueueList(profileId, fromDate, toDate, expCategory, expType, approvalStatus);
                if (dtReimbursement != null)// && dtReimbursement.Rows.Count > 0)
                {
                    SearchListReimbursed = dtReimbursement;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvReimbursed, dtReimbursement);
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private DataTable SearchListApproved
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_APPROVED];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["EXP_DETAIL_ID"] };
            Session[REIM_SEARCH_SESSION_APPROVED] = value;
        }
    }

    private DataTable SearchListPending
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_PENDING];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["EXP_DETAIL_ID"] };
            Session[REIM_SEARCH_SESSION_PENDING] = value;
        }
    }


    private DataTable SearchListRejected
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_REJECTED];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["EXP_DETAIL_ID"] };
            Session[REIM_SEARCH_SESSION_REJECTED] = value;
        }
    }

    private DataTable SearchListReimbursed
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_REIMBURSED];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["EXP_DETAIL_ID"] };
            Session[REIM_SEARCH_SESSION_REIMBURSED] = value;
        }
    }


}
