﻿ <%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="InsurancePlans" Title="Insurance Plans" Codebehind="InsurancePlans.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
   <%-- <link href="css/main-style.css" rel="stylesheet" type="text/css" />--%>
        <link href="css/steps.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function validateTerms() {
       
            var isValied = false;
            if (document.getElementById('hdnOTPPlansCount') != null) {
                var plans = eval(document.getElementById('hdnOTPPlansCount').value);
                document.getElementById('hdnOTPPlan').value = "";
                for (i = 0; i < plans; i++) {
                    if (document.getElementById('rbPlan' + i).checked) {
                        document.getElementById('hdnOTPPlan').value = eval(i);
                        isValied = true;
                        break;
                    }
                }
            }
            if (document.getElementById('hdnUpsellPlansCount') != null) {
                if (document.getElementById('hdnUPSellPlan')!=null)
                   document.getElementById('hdnUPSellPlan').value = "";
                var selectedPlans = null;
                var upSellPlans = eval(document.getElementById('hdnUpsellPlansCount').value);
                for (j = 0; j < upSellPlans; j++) {
                    if (document.getElementById('chkPlan' + j).checked) {
                        if (selectedPlans == null) {
                            selectedPlans = eval(j);
                        }
                        else {
                            selectedPlans = selectedPlans + "," + eval(j);
                        }
                        isValied = true;
                    }
                }
                document.getElementById('hdnUPSellPlan').value = selectedPlans;
            }
            var chk = document.getElementById('chkTerms');
            if (!isValied) {
                alert("please select atleast one plan");
                return false;
            }
            else if (chk.checked == true) {
                document.getElementById('termsError').style.color = 'gray';
                return true;
            }
            else {
                document.getElementById('termsError').style.color = 'red';
                return false;
            }
        }
        //unsellecting radio button
        function SetRadio(i) {
            var plans = eval(document.getElementById('hdnOTPPlansCount').value);
            for (k = 0; k < plans; k++) {
                if (i != k) {
                    document.getElementById('rbPlan' + k).checked = false;
                    document.getElementById('hdnPlan' + k).value = false;
                }
            }
            if (document.getElementById('hdnPlan' + i).value == "true") {
            //Loading Plans count and unselecting all radio buttons
                document.getElementById('rbPlan' + i).checked = false;
                document.getElementById('hdnPlan' + i).value = false;
            }
            else {
                if (document.getElementById('hdnChargeType' + i) != null) {
                  //Per Booking (Family) time Popup showing
                    if (document.getElementById('hdnChargeType' + i).value == "PerBooking") {
                        document.getElementById('hdnPlan' + i).value = true;
                        SetRadio(i);
                        document.getElementById('divFamilyPopup').style.display = "block";
                        document.getElementById('hdnPlanType').value = "OTP";
                        document.getElementById('hdnSelectIndex').value = i;
                        return false;
                    }
                }
                document.getElementById('rbPlan' + i).checked = true;
                document.getElementById('hdnPlan' + i).value = true;
            }
        }

        function SetCheckBox(i) {
            if (document.getElementById('hdnUpsellChargeType' + i) != null) {
                if (document.getElementById('hdnUpsellChargeType' + i).value == "PerBooking") {
                    if (document.getElementById('chkPlan' + i).checked) {
                        document.getElementById('divFamilyPopup').style.display = "block";
                    }
                    document.getElementById('hdnPlanType').value = "UPSell";
                    document.getElementById('hdnSelectIndex').value = i;
                    document.getElementById('chkPlan' + i).checked = false;
                }
            }
        }
        
    </script>

    <div>
        <div id="travelInsurancePlan">
        <div class="wizard-steps">
            <div class="active-step">
                <a href="#step-one"><span>1</span>Select Policy</a></div>
            <!-- <div class="completed-step"><a href="#step-one"><span>1</span>Select Policy</a></div>-->
            <div>
                <a href="#step-two"><span>2</span> PAX Details</a></div>
            <div>
                <a href="#"><span>3</span> Secure Payments</a></div>
            <div>
                <a href="#"><span>4</span> Confirmation</a></div>
            <div class="clear">
            </div>
        </div>
        <br />
        <div class="step1-insurance">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td style="width: 50%; text-align: right; height: 30px" class="clbg">
                            <h4>
                                <strong>Duration</strong>
                            </h4>
                        </td>
                        <td class="clbg">
                            <h4>
                                &nbsp; <strong>
                                    <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                                    Days </strong>
                            </h4>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <input type="hidden" id="hdnOTPPlan" name="hdnOTPPlan" />
        <input type="hidden" id="hdnUPSellPlan" name="hdnUPSellPlan" />
        
        <%if (InsPlansResponse != null && InsPlansResponse.OTAPlans != null && InsPlansResponse.OTAPlans.Count > 0)
          { %>
        <div>
            <h3>
                Core Plans</h3>
        </div>
        
        <input type="hidden" id="hdnOTPPlansCount" name="hdnOTPPlansCount" value="<%=InsPlansResponse.OTAPlans.Count %>" />
        
            <%for (int i = 0; i < InsPlansResponse.OTAPlans.Count; i++)
              {%>
            <div class="planContainer">
                <div class="planName">
                    <span id="lblTitle">
                        <%=InsPlansResponse.OTAPlans[i].Title%></span>
                </div>
                <div class="PlanWraper">
                    <div>
                        <input type="radio" id="rbPlan<%=i %>" name="ZEUS" onclick="SetRadio(<%=i %>)" />
                        <input type="hidden" id="hdnPlan<%=i%>" />
                        <input type="hidden" id="hdnChargeType<%=i%>" value='<%=InsPlansResponse.OTAPlans[i].PremiumChargeType %>' />
                        <span class="planPrice">
                            <%=InsPlansResponse.OTAPlans[i].CurrencyCode%>
                            <%=Math.Round(InsPlansResponse.OTAPlans[i].TotalAmount,InsPlansResponse.OTAPlans[i].DecimalValue)%></span>
                    </div>
                    <div class="planDescription" style="overflow: auto">
                        <span id="lblDescOne">
                            <%=InsPlansResponse.OTAPlans[i].Content%></span></div>
                    <div class="clear">
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="clear">
            </div>
            <%}
      }
            %>
            <%if (InsPlansResponse != null && InsPlansResponse.UpsellPlans != null && InsPlansResponse.UpsellPlans.Count > 0)
              {%>
            <div>
                <h3>
                    Upsell Plans</h3>
            </div>
            <input type="hidden" id="hdnUpsellPlansCount" name="hdnUpsellPlansCount" value="<%=InsPlansResponse.UpsellPlans.Count %>" />
            <%for (int k = 0; k < InsPlansResponse.UpsellPlans.Count; k++)
              {%>
            <div class="planContainer">
                <div class="planName">
                    <span id="Span1">
                        <%=InsPlansResponse.UpsellPlans[k].Title%></span>
                </div>
                <div class="PlanWraper">
                    <div>
                        <input type="checkbox" id="chkPlan<%=k %>" name="ZEUS" onchange="SetCheckBox(<%=k %>)" />
                        <input type="hidden" id="hdnUpsellPlan<%=k %>" />
                           <input type="hidden" id="hdnUpsellChargeType<%=k%>" value='<%=InsPlansResponse.UpsellPlans[k].UPsellPremiumChargeType %>' />
                        <span class="planPrice">
                            <%=InsPlansResponse.UpsellPlans[k].CurrencyCode%>
                            <%=Math.Round(InsPlansResponse.UpsellPlans[k].TotalAmount,InsPlansResponse.UpsellPlans[k].DecimalValue)%></span>
                    </div>
                    <div class="planDescription" style="overflow: auto">
                        <span id="lblDescOne">
                            <%=InsPlansResponse.UpsellPlans[k].Content%></span></div>
                    <div class="clear">
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="clear">
            </div>
            <%}
              } %>
        
        <%if (InsPlansResponse == null || (InsPlansResponse.OTAPlans == null && InsPlansResponse.UpsellPlans == null) || (InsPlansResponse.OTAPlans.Count == 0 && InsPlansResponse.UpsellPlans.Count==0))
          { %>
        <div class="planTerms">
            <div class="planTermsHeading">
                No Plans available for the Searched Itinerary <u><a href="Insurance.aspx" style="font-size: medium;">
                    please search again</a></u>
            </div>
        </div>
        <%}
          else
          { %>
        <br />
        <div class="col-md-12">
            <span id="termsError" class="gray_h">Please check the terms checkbox to continue</span><br />
            <input type="checkbox" id="chkTerms" />&nbsp;I agree to the Terms & Conditions of
            the Insurance.
        </div>
        <div class="col-md-12">
            <asp:Button ID="btnContinue" CssClass="btn but_b pull-right" runat="server" Text="Continue"
                OnClientClick="return validateTerms();" OnClick="btnContinue_Click" /></div>
        <%} %>
            </div>
    </div>


                    
<style type="text/css">

#divFamilyPopup { position:fixed; top:0; left:0; width:100%; height:100%; display:none; background:rgba(31,78,146,0.7); }



</style>


<div style="display:none" id="divFamilyPopup"> 

    <div class="familypack">
     <div class="ns-h3" >Important Declaration  <a style="color:#fff; position:absolute; right:10px; cursor:pointer" onclick="hide('divFamilyPopup')"> X </a> </div>  
        



<script>

    //Popup Accepting time selected Plan
    function show(id) {
        document.getElementById(id).style.display = 'none';
        if (document.getElementById('hdnSelectIndex') != null) {
            if (document.getElementById('hdnPlanType') != null && document.getElementById('hdnPlanType').value == "OTP") {
                document.getElementById('rbPlan' + document.getElementById('hdnSelectIndex').value).checked = true;
                document.getElementById('hdnPlan' + document.getElementById('hdnSelectIndex').value).value = true;
            }
            else {
                document.getElementById('chkPlan' + document.getElementById('hdnSelectIndex').value).checked = true;
            }
        }
    }

    //Popup Reject time selected no need to select
    function hide(boxid) {
        document.getElementById(boxid).style.display = "none";
    }

</script>




        <div id="dialog-familyplan" style=" padding:0px 10px 10px 10px; border: solid 1px #ccc; border-top:none">
           
        <input type="hidden" id="hdnPlanType" name="hdnPlanType" />
        <input type="hidden" id="hdnSelectIndex" name="hdnSelectIndex" />
           
            <div>


            <h4> Family Plan has been selected!</h4>

            <div> <strong> General Information</strong> </div>



              
                <p>
                    <b>Family Plan: </b>2 adults aged 75 and below and unlimited number of Children
                    (including legally adopted Children) provided the appropriate premiums must have
                    been paid. All persons must reside at the same address and must be travelling together.
                    All ages referred to are at the date of first departure.</p>
                <p>
                    <b>Children: </b>means the Insured Person’s dependent children who are not in full-time
                    employment and who are between the ages of 2 months and 18 years (<span style="background-color: #FFFF00">or
                        under the age of 23 years provided that they are fulltime students</span>),
                    unmarried, not pregnant, without children and primarily dependent on the Insured
                    Person for support.</p>
                <span style="color: red; font-weight: bold">Important Declaration:-</span>
                <br>
                <p>
                    I, as the travel agent <span style="background-color: #FFFF00">booking on behalf of
                        the policyholder</span>, hereby declare that I understand the definition of
                    Family Plan and have informed the customer accordingly. Tune Protect and the underwriter
                    shall not be held responsible for any misrepresentation <span style="background-color: #FFFF00">
                        and/or inappropriate choosing of the plan</span>.</p>
                <p>
                    <b>Note for Family Plan: Means the Insured Person’s legal spouse and child/children
                        insured under the same certificate of insurance.</b></p>



                        <div>
                        
                        <input style=" margin-left:10px; margin-right:10px;" class="btn but_b pull-right" onclick="show('divFamilyPopup')" type="button" value="Accept" />  
                        
                        
                         <input class="btn but_b pull-right" onclick="hide('divFamilyPopup')" type="button" value="Reject" />   
                         
                         <div class="clear"></div>
                         
                         
                          </div>


            </div>
        </div>
        
        
        
        
        



    </div>

</div>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
