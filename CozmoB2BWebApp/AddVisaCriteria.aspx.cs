using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Visa;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

public partial class AddVisaCriteria : CT.Core.ParentPage
{
    protected bool isAdmin = false;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck(); 
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
        {
            isAdmin = true;
        }
        if (!IsPostBack)
        {
            BindAgent();
            Page.Title = "Add Criteria";
            ddlCountry.DataSource = Country.GetCountryList(); // VisaCountry.GetActiveVisaCountryList();
            ddlCountry.DataTextField = "key";
            ddlCountry.DataValueField = "value";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, "Select");
            if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
            {
                int criteriaId;
                bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out criteriaId);
                if (!isnumerice)
                {
                    Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                    Response.End();

                }
                try
                {
                    VisaCriteria criteria = new VisaCriteria(Convert.ToInt32(Request.QueryString[0]));
                    if (criteria != null)
                    {
                        txtName.Text = criteria.CriteriaName;
                       // editor.Value = Server.HtmlDecode(criteria.Description);
                        editor.Text = criteria.Description;
                        ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(criteria.CountryCode));
                        ddlAgent.SelectedValue = Convert.ToString(criteria.AgentId);
                        ddlAgent.Enabled = false;
                        btnSave.Text = "Update";
                        Page.Title = "Update Visa Criteria";
                        lblStatus.Text = "Update Visa Criteria";
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaCriteria,Err:" + ex.Message,"");
                }

            }
            else
            {
                ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue("AE"));
                if (!isAdmin)
                {
                    ddlAgent.SelectedValue = Settings.LoginInfo.AgentId.ToString();
                    ddlAgent.Enabled = false;
                }
            }
        }
        

    }

    private void BindAgent()
    {
        try
        {
            int agentId = 0;
            if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
            string agentType = (Settings.LoginInfo.AgentType.ToString() != null ? Settings.LoginInfo.AgentType.ToString() : "BASEAGENT");
            ddlAgent.DataSource = AgentMaster.GetList(1, agentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataValueField = "agent_Id";
            ddlAgent.DataTextField = "agent_Name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("Select Agent", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            // string html = Request.Form["editor"];
            VisaCriteria criteria = new VisaCriteria();
            criteria.CountryCode = ddlCountry.SelectedValue;
            criteria.AgentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            //criteria.Description = editor.Value;//Server.HtmlEncode(html);
            criteria.Description = editor.Text;
            criteria.CriteriaName = txtName.Text.Trim();
            if (btnSave.Text == "Update")
            {
                if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
                {

                    criteria.CriteriaId = Convert.ToInt32(Request.QueryString[0]);
                    criteria.LastModifiedBy = (int)Settings.LoginInfo.UserID;

                }
            }
            else
            {
                criteria.IsActive = true;
                criteria.CreatedBy = (int)Settings.LoginInfo.UserID;
            }
            criteria.Save();
            
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaCriteria,Err:" + ex.Message, "");
        }
        Response.Redirect("VisaCriteriaList.aspx?Country=" + ddlCountry.SelectedValue + "&Agency=" + ddlAgent.SelectedItem.Value);
    }


    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
