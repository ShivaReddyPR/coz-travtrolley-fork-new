﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.BookingEngine;
using System.Collections;
using CT.TicketReceipt.Web.UI.Controls;

public partial class OutputVatConfigGUI :CT.Core.ParentPage
{
    private string OUTPUT_VAT_SEARCH_SESSION = "_OutputVatSearchSession";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                IntialiseControls();
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    #region Private Methods
    private void IntialiseControls()
    {
        try
        {
            LoadCountry();
            InvatProduct();
            InvatModule();
        }
        catch
        {
            throw;
        }

    }
    private void LoadCountry()
    {
        try
        {
            SortedList Countries = Country.GetCountryList();
            ddlCountry.DataSource = Countries;
            ddlCountry.DataTextField = "key";
            ddlCountry.DataValueField = "value";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
        }
        catch
        {
            throw;
        }
    }
    private void InvatProduct()
    {
        try
        {
            Array products = Enum.GetValues(typeof(ProductType));
            ddlOutProduct.Items.Insert(0, new ListItem("--Select--", "-1"));
            foreach (ProductType pd in products)
            {
                ddlOutProduct.Items.Add(new ListItem(pd.ToString(), ((int)pd).ToString()));
            }
        }
        catch
        {
            throw;
        }
    }
    private void InvatModule()
    {
        try
        {
            Array modules = Enum.GetValues(typeof(Module));
            ddlOutModule.Items.Insert(0, new ListItem("--Select--", "-1"));
            foreach (Module md in modules)
            {
                ddlOutModule.Items.Add(new ListItem(md.ToString(), ((int)md).ToString()));
            }
        }
        catch
        {
            throw;
        }
    }
    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[OUTPUT_VAT_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["outvat_id"] };
            Session[OUTPUT_VAT_SEARCH_SESSION] = value;
        }
    }
    private void Edit(int id)
    {
        try
        {
            OutputVATDetail outvatdetails = new OutputVATDetail(id);
            hdfOutvatId.Value = Utility.ToString(outvatdetails.Id);
            ddlCountry.SelectedValue = outvatdetails.CountryCode;
            if (outvatdetails.DestinationType == DestinationType.International)
            {
                rbInternational.Checked = true;
            }
            else
            {
                rbDomestic.Checked = true;
            }
            ddlOutProduct.SelectedValue = Utility.ToString(outvatdetails.ProductId);
            if (outvatdetails.Module == Module.Ticket)
            {
                ddlOutModule.SelectedValue = ((int)Module.Ticket).ToString();
            }
            else if (outvatdetails.Module == Module.Hotel)
            {
                ddlOutModule.SelectedValue = ((int)Module.Hotel).ToString();
            }
            else if (outvatdetails.Module == Module.Visa)
            {
                ddlOutModule.SelectedValue = ((int)Module.Visa).ToString();
            }
            else if (outvatdetails.Module == Module.Package)
            {
                ddlOutModule.SelectedValue = ((int)Module.Package).ToString();
            }
            else if (outvatdetails.Module == Module.Meal)
            {
                ddlOutModule.SelectedValue = ((int)Module.Meal).ToString();
            }
            else if (outvatdetails.Module == Module.Seat)
            {
                ddlOutModule.SelectedValue = ((int)Module.Seat).ToString();
            }
            else
            {
                ddlOutModule.SelectedValue = ((int)Module.HALA).ToString();
            }
            if (outvatdetails.Applied == true)
            {
                rbOutvatApplied.Checked = true;
            }
            else
            {
                rbOutvatNotApplied.Checked = true;
            }
            txtOutvatvalue.Text = Utility.ToString(outvatdetails.Charge);
            txtRemarks.Text = outvatdetails.Remarks;
            if (outvatdetails.AppliedOn == VATAppliedOn.SF)
            {
                ddlOutVatOn.SelectedValue = (VATAppliedOn.SF).ToString();
            }
            else
            {
                ddlOutVatOn.SelectedValue = (VATAppliedOn.TC).ToString();
            }
            if (outvatdetails.CCChargeApplied == true)
            {
                rbOutVatCCAplied.Checked = true;
            }
            else
            {
                rbOutVatCCNotAplied.Checked = true;
            }
            if (outvatdetails.Calculate == true)
            {
                rbOutvatCalculate.Checked = true;
            }
            else
            {
                rbOutvatNotCalculate.Checked = true;
            }
            btnSave.Text = "Update";
            btnClear.Text = "Cancel";
        }
        catch
        {
            throw;
        }
    }
    private void Clear()
    {
        try
        {
            ddlCountry.SelectedValue = "0";
            rbInternational.Checked = false;
            rbDomestic.Checked = false;
            ddlOutProduct.SelectedValue = "-1";
            ddlOutModule.SelectedValue = "-1";
            rbOutvatApplied.Checked = false;
            rbOutvatNotApplied.Checked = false;
            txtOutvatvalue.Text = string.Empty;
            ddlOutVatOn.SelectedValue = "-1";
            rbOutVatCCAplied.Checked = false;
            rbOutVatCCNotAplied.Checked = false;
            rbOutvatCalculate.Checked = false;
            rbOutvatNotCalculate.Checked = false;
            txtRemarks.Text = string.Empty;
            hdfOutvatId.Value = string.Empty;
            btnSave.Text = "Save";
            btnClear.Text = "Clear";
        }
        catch
        {
            throw;
        }
    }
    private void bindSearch()
    {
        try
        {
            DataTable dt = OutputVATDetail.GetList();
            if (dt != null && dt.Rows.Count > 0)
            {
                SearchList = dt;
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvSearch, dt);
            }
        }
        catch
        {
            throw;
        }
    }
    private void Save()
    {
        try
        {
            OutputVATDetail otdetail = new OutputVATDetail();
            if (Utility.ToInteger(hdfOutvatId.Value) > 0)
            {
                otdetail.Id = Utility.ToInteger(hdfOutvatId.Value);
            }
            else
            {
                otdetail.Id = -1;
            }
            otdetail.CountryCode = ddlCountry.SelectedValue;
            if (rbInternational.Checked == true)
            {
                otdetail.DestinationType = DestinationType.International;
            }
            else
            {
                otdetail.DestinationType = DestinationType.Domestic;
            }
            otdetail.ProductId = Utility.ToInteger(ddlOutProduct.SelectedValue);
            switch (ddlOutModule.SelectedItem.Text)
            {
                case "Ticket":
                    otdetail.Module = Module.Ticket;
                    break;
                case "Hotel":
                    otdetail.Module = Module.Hotel;
                    break;
                case "Visa":
                    otdetail.Module = Module.Visa;
                    break;
                case "Package":
                    otdetail.Module = Module.Package;
                    break;
                case "Meal":
                    otdetail.Module = Module.Meal;
                    break;
                case "Seat":
                    otdetail.Module = Module.Seat;
                    break;
                case "HALA":
                    otdetail.Module = Module.HALA;
                    break;
                case "Insurance":
                    otdetail.Module = Module.Insurance;
                    break;
            }
            if (rbOutvatApplied.Checked == true)
            {
                otdetail.Applied = true;
            }
            else
            {
                otdetail.Applied = false;
            }
            otdetail.Charge = Utility.ToDecimal(txtOutvatvalue.Text);
            switch (ddlOutVatOn.SelectedItem.Text)
            {
                case "Total Collection":
                    otdetail.AppliedOn = VATAppliedOn.TC;
                    break;
                case "Service Fee":
                    otdetail.AppliedOn = VATAppliedOn.SF;
                    break;
            }
            if (rbOutVatCCAplied.Checked == true)
            {
                otdetail.CCChargeApplied = true;
            }
            else
            {
                otdetail.CCChargeApplied = false;
            }
            if (rbOutvatCalculate.Checked==true)
            {
                otdetail.Calculate = true;
            }
            else
            {
                otdetail.Calculate = false;
            }
            otdetail.Remarks = txtRemarks.Text;
            otdetail.Status = "A";
            otdetail.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
            otdetail.Save();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage("Successfully", "", (Utility.ToInteger(hdfOutvatId.Value) == 0 ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
            Clear();         
        }
        catch
        {
            throw;
        }
    }
    #endregion
    #region Button Events
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Audit.Add(EventType.Exception, Severity.High, 1, "OutputVatConfig page " + ex.Message, "0");
        }
    }
    #endregion
    #region GridEvents
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={

                                       
                                         {"HTtxtCountry", "outvatcountry"},
                                         {"HTtxtDestinationType", "outvat_destination_type"},
                                         {"HTtxtProductType", "outvat_product"},
                                         {"HTtxtModule", "outvat_module"},
                                         };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            Int32 outvatid = Utility.ToInteger(gvSearch.SelectedValue);
            Edit(outvatid);
            this.Master.HideSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    #endregion
    protected void ddlOutProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlOutModule.SelectedValue = "-1";
    }
}
