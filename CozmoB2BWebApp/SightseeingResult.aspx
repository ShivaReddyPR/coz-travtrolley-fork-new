﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="SightseeingResult" Title="Sightseeing Search Results" Codebehind="SightseeingResult.aspx.cs" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

    <link rel="stylesheet" href="css/jquery-ui.css" />
   <%-- <link href="css/select2.css" rel="stylesheet" />--%>

 <script src="Scripts/SliderJquery/jquery-ui-1.12.js" type="text/javascript"></script>
 <script type="text/javascript" src="Scripts/jsBE/Utils.js" language="javascript"></script>
 <script type="text/javascript" src="Scripts/Jquery/popup_window.js"></script>
 <script type="text/javascript" src="Scripts/jsBE/ModalPop.js"></script>
  <script src="scripts/bootstrap.min.js"></script>   
    
    
 
    <link href="css/ModalPop.css" rel="stylesheet" type="text/css" />
    
    <link href="css/popup_box.css" rel="stylesheet" type="text/css" />
   
    <link rel="stylesheet" href="css/ajax_tab_css.css">
    
     <%-- <link href="css/bootstrap-slider.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/jquery.horizontal.scroll.css" />--%>
   <%-- <link rel="stylesheet" type="text/css" href="css/bs_leftnavi.css">--%>
   <%-- <link href="css/bootstrap.min.css" rel="stylesheet" />--%>
 
  <!-- Here Calling only JavaScript -->
     <script type="text/javascript">
        $(function() {

            $("#slider-range").slider({
               range: "max",
                min: parseFloat(document.getElementById('<%=hdnMinValue.ClientID %>').value),
                max: parseFloat(document.getElementById('<%=hdnMaxValue.ClientID %>').value),
                step: 1,
                values: [parseFloat(document.getElementById('<%=hdnPriceMin.ClientID %>').value), parseFloat(document.getElementById('<%=hdnPriceMax.ClientID %>').value)],
                slide: function(event, ui) {

                    if (ui.values[0] <= ui.values[1]) {
                        $("#amount").val(ui.values[0] + "-" + ui.values[1]);
                        document.getElementById('<%=hdnPriceMin.ClientID %>').value = ui.values[0];
                        document.getElementById('<%=hdnPriceMax.ClientID %>').value = ui.values[1];
                    }
                    else {
                        $("#amount").val(parseFloat(document.getElementById('<%=hdnMinValue.ClientID %>').value) + "-" + parseFloat(document.getElementById('<%=hdnMaxValue.ClientID %>').value));
                        document.getElementById('<%=hdnPriceMin.ClientID %>').value = parseFloat(document.getElementById('<%=hdnMinValue.ClientID %>').value);
                        document.getElementById('<%=hdnPriceMax.ClientID %>').value = parseFloat(document.getElementById('<%=hdnMaxValue.ClientID %>').value);
                    }
                }
            });
            $("#amount").val($("#slider-range").slider("values", 0) + "-" + $("#slider-range").slider("values", 1));

        });
   
        $(function() {
            $("#slider-range").slider({
                change: function() {
                    var duration = document.getElementById('amount').value;
                    var min = duration.split('-')[0];
                    var max = duration.split('-')[1];                   

                    document.getElementById('<%=hdnPriceMin.ClientID %>').value = min;
                    document.getElementById('<%=hdnPriceMax.ClientID %>').value = max;
                  document.getElementById('<%=Change.ClientID %>').value = "Price";
                    document.getElementById('label').InnerHTML = max;
                    

                    document.forms[0].submit();
                    
                }
            });
        });
    </script>
<script type="text/javascript">
    var Ajax;
    var hotelIndex = "";
    var CONTEXT_HOTELDETAIL = ''

    if (window.XMLHttpRequest) {
        Ajax = new window.XMLHttpRequest();
    }
    else {
        Ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }

    function showStuff(id) {
        document.getElementById(id).style.display = 'block';
    }
    function hidestuff(boxid) {
        document.getElementById(boxid).style.display = "none";
    }
    var CONTEXT_HOTELDETAIL = ''
    function GetSightSeeingDetail(source, cityCode, itemCode, itemName, sourceLink) {
        var paramList = 'Action=getSightSeeingDetail' + '&source=' + source + '&cityCode=' + cityCode + '&itemCode=' + itemCode + '&itemName=' + itemName;
        var url = "SightSeeingAjax";
        CONTEXT_HOTELDETAIL = sourceLink;
        ModalPop.ControlBox.style.display = "none";
        var modalMessage = "<div><center><img alt=\"Loading...\" height=\"100\" width=\"100\" src=\"Images/ajax-loader.gif\" style=\"margin: 100px 0 0 0;\" /><h3 class=\"primary-color\">Loading SightSeeing Details...</h3></center></div>";
        ModalPop.Show({ x: 0 }, modalMessage);
        //new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: ShowSightSeeingDetail } );
        Ajax.onreadystatechange = ShowSightSeeingDetail;
        Ajax.open('POST', url);
        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        Ajax.send(paramList);
    }

    function ShowSightSeeingDetail(response) {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {

                        var text = Ajax.responseText;
                        ModalPop.ControlBox.style.display = "none";
                        var modalMessage = text;
                        modalMessage += "<br/><div class=\"close_fcozmo\" onclick=\"javascript:HideWindowPopUp(0)\"><a class=\"xclo fcol_fff\">X</a> </div>";
                        ModalPop.Show({ x: 530, y: 425 }, modalMessage);
                        if (CONTEXT_HOTELDETAIL == 'overview') {
                            ShowDiv('sig_overview', 'overview');
                        }
                        else if (CONTEXT_HOTELDETAIL == 'inclusions') {
                            ShowDiv('sig_inclusions', 'inclusions');
                        }
                        else if (CONTEXT_HOTELDETAIL == 'operation') {
                            ShowDiv('sig_operation', 'price');
                        }
                        else if (CONTEXT_HOTELDETAIL == 'note') {
                            ShowDiv('sig_note', 'note');
                        }
                        else if (CONTEXT_HOTELDETAIL == 'terms') {
                            ShowDiv('sig_terms', 'terms');
                        }
                    }
                }
            }
        }
    function ShowDiv(showDivId, hideDivId) {


        document.getElementById('sig_overview').style.display = 'none';
        document.getElementById('lisig_overview').className = '';


        document.getElementById('sig_inclusions').style.display = 'none';
        document.getElementById('lisig_inclusions').className = '';

        document.getElementById('sig_operation').style.display = 'none';
        document.getElementById('lisig_operation').className = '';

        document.getElementById('sig_note').style.display = 'none';
        document.getElementById('lisig_note').className = '';

        document.getElementById('sig_terms').style.display = 'none';
        document.getElementById('lisig_terms').className = '';
       document.getElementById(showDivId).style.display = 'block';

       document.getElementById("li" + showDivId).className = 'active';

    }
    function HideWindowPopUp(check) {
        flag = check;
        ModalPop.Hide();
    }

    function GetCancelInfo(source, itemCode, choseTour) {
        var paramList = 'Action=getCancellationDetail' + '&Source=' + source + '&itemCode=' + itemCode + '&choseTour=' + choseTour;
        var url = "SightSeeingAjax";
        ModalPop.ControlBox.style.display = "none";
        var modalMessage = "<div><center><img alt=\"Loading...\" height=\"100\" width=\"100\" src=\"Images/ajax-loader.gif\" style=\"margin: 130px 0 0 0;\" /><h3 class=\"primary-color\">Loading Hotel Details...</h3></center></div>";
        ModalPop.Show({ x: 0 }, modalMessage);
        //new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: SetCancelInfo } );
        Ajax.onreadystatechange = SetCancelInfo;
        Ajax.open('POST', url);
        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        Ajax.send(paramList);
    }

    function SetCancelInfo(response) {
        if (Ajax.readyState == 4) {
            if (Ajax.status == 200) {
                if (Ajax.responseText.length > 0) {
                    var text = Ajax.responseText;
                    ModalPop.ControlBox.style.display = "none";
                    var modalMessage = text;
                    modalMessage += "<br/><div class=\"close_fcozmo\" onclick=\"javascript:HideWindowPopUp(0)\"><a class=\"xclo fcol_fff\">X</a> </div>";
                    ModalPop.Show({ x: 0, y: 425 }, modalMessage);
                }
            }
        }
    }

    function EnableDisableControls(id) {
        var chkAll = document.getElementById(id);
        var chkControlId = document.getElementById('<%=chkList.ClientID%>');
        var options = chkControlId.getElementsByTagName('input');
        for (i = 0; i < options.length; i++) {
            var opt = options[i];
            if (opt.type == "checkbox") {
                if (chkAll.checked && !opt.disabled) {
                    opt.checked = true;
                }
                else {
                    opt.checked = false;
                }

            }
        }
    }
    function EnableDisableAll(id) {
        var chkControlId = document.getElementById(id);
        var chkAll = document.getElementById('<%=chkAll.ClientID%>');
        var options = chkControlId.getElementsByTagName('input');
        var checkedLenght = 0;
        var enableLenght = 0;
        for (i = 0; i < options.length; i++) {
            var opt = options[i];
            if (opt.checked) {
                checkedLenght++;
            }
            if (!opt.disabled) {
                enableLenght++;
            }
        }
        if (enableLenght == checkedLenght) {
            chkAll.checked = true;
        }
        else {
            chkAll.checked = false;
        }
    }

    function ShowPage(pageNum) {
        document.getElementById('<%=PageNoString.ClientID %>').value = pageNum;
         document.getElementById('<%=Change.ClientID %>').value = "true";
          $(document.forms[0]).submit();
    }
    
</script>
  <!--End JavaScript Here -->
  
  <!--Page Level Style -->
  <style>
   .PagerStyle
   {
   	 border: solid 1px #ccc;
   	 background:#FFFFFF;
   	 font-size:11px; 
   	 padding:2px 5px 2px 5px; 
   	 margin-left:5px;
   	}
   </style>
  
  <!--End Page Level Style -->
  <asp:HiddenField ID="resultCount" Value="0" runat="server" />
    <asp:HiddenField ID="hdnPriceMax" runat="server" Value="0" />
    <asp:HiddenField ID="hdnPriceMin" runat="server" Value="0" />
    <asp:HiddenField ID="hdnMinValue" runat="server" Value="0" />
    <asp:HiddenField ID="hdnMaxValue" runat="server" Value="0" />
    <asp:HiddenField ID="Change" runat="server" />
    <input runat="server" type="hidden" id="PageNoString" name="PageNoString" />
    <asp:HiddenField ID="hdnFilter" runat="server" />
    
   <!-- Here Calling Sightseeing Search Functionality -->
<div>
   <div class="search_rpt1 hidden-xs">
   
               
            <div class="L31">
                <span class="fnt16"><b>
                    <%if (chkAll.Checked && txtSight.Text.Length <= 0)
                      { %>
                    <%=(resultCount.Value != null ? resultCount.Value : "0")%></b></span>
                Sightseeing Found in :<br />
                <%}
                      else if (!chkAll.Checked || txtSight.Text.Length > 0)
                      {%>
                <%=(resultCount.Value != null ? resultCount.Value : "0")%></b></span> Sightseeing Found in :<br />
                <%} %>
                <span class="spnred ">
                <%=request.CityName %>
                </span>
            </div>
            <div class="L32">
                <span class="spnred">Check-in</span><br />
                <span class="fnt11">
                    <%=request.TourDate.ToString("dd, MMM yyyy") %></span> <span class="fnt11_gray">(<%=request.TourDate.ToString("ddd") %>)</span> 
            </div>
            <div class="L31">
                <span class="spnred">No. of People</span>
                <br />
                <span class="fnt11">
                 <%=(request.NoOfAdults + request.ChildCount) %> (<%=request.NoOfAdults %> Adult(s)
                    <% =request.ChildCount %> Child(s))
                    </span>
            </div>
           
      <asp:LinkButton ID="lnkModifySearch" CssClass="but but_b pull-right margin-right-5" PostBackUrl="~/Sightseeing.aspx" runat="server">Modify Search
      </asp:LinkButton>  
      
      <div class="clearfix"> </div>
            
   </div>
 <div id="errMess" class="error_module" style="display: none;">
                <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                </div>
            </div>

<%--<div id="main-container">--%>
           <%-- <div id="sightseeing_main_inner">
                <div class="sightseeing_container">--%>
                <div>
                     <div class="col-md-2 padding-0">
            <div><asp:LinkButton CssClass="form-control" ID="lnkClear" runat="server" Text="Clear Filter" OnClientClick="ClearFilter();"></asp:LinkButton></div>
                <div class="bg_white">
                
                    <div class="ns-h3">
                        Filter Results                        
                        </div>                        
                    <div class="pad-5">
                        <asp:TextBox ID="txtSight" runat="server" placeholder="Search By Sightseeing Name"  CssClass="form-control" Visible="false"></asp:TextBox>
                    </div>
                    <div class="ns-h3" style="display:none">
                        Sightseeing Type</div>
                    <div class="pad-5">

                    <label> 
                         <asp:CheckBox CssClass="chkChoice custom-checkbox-table chk-Suppliers-table" ID="chkAll" Visible="false" runat="server" Checked="true" Text="All" onclick="EnableDisableControls(this.id)"/>
                          </label>          
                    </div>


                      <div class="mt-5 mb-2 ml-4">


                      <asp:CheckBoxList CssClass="chkList" ID="chkList" runat="server" RepeatDirection="Vertical" Visible="false"  onclick="EnableDisableAll(this.id)">
                                        <asp:ListItem Text="Air Sightseeing" Value="AR" Enabled="false"></asp:ListItem>
                                        <asp:ListItem Text="Attractions" Value="AT" Enabled="false"></asp:ListItem>
                                        <asp:ListItem Text="Hop On, Hop Off" Value="HO" Enabled="false"></asp:ListItem>
                                        <asp:ListItem Text="Show/Concerts" Value="SM" Enabled="false"></asp:ListItem>
                                        <asp:ListItem Text="Shuttle" Value="ST" Enabled="false"></asp:ListItem>
                                        <asp:ListItem Text="Sightseeing Cruises" Value="CR" Enabled="false"></asp:ListItem>
                                        <asp:ListItem Text="Sightseeing Tours" Value="RS" Enabled="false"></asp:ListItem>
                                        <asp:ListItem Text="Tourists Pass" Value="TP" Enabled="false"></asp:ListItem>
                                    </asp:CheckBoxList>
<div class="clearfix"> </div>
                      </div>



                    <div class="ns-h3" style="display:none">
                        Categories</div>
                    <div class="pad-5">
                    <asp:DropDownList ID="ddlCategory" runat="server" class="form-control" name="wrewr6" Visible="false">
                                        <asp:ListItem Value="All" Text="All" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="FW" Text="4WD Tours"></asp:ListItem>
                                        <asp:ListItem Value="AV" Text="Adventure"></asp:ListItem>
                                        <asp:ListItem Value="AL" Text="Airport Lounges"></asp:ListItem>
                                        <asp:ListItem Value="CL" Text="Arts & Culture"></asp:ListItem>
                                        <asp:ListItem Value="BE" Text="Beauty/Spa/Massage"></asp:ListItem>
                                        <asp:ListItem Value="CS" Text="City Sightseeing"></asp:ListItem>
                                        <asp:ListItem Value="CM" Text="Cruise & Meal"></asp:ListItem>
                                        <asp:ListItem Value="CU" Text="Culinary"></asp:ListItem>
                                        <asp:ListItem Value="CY" Text="Cycling"></asp:ListItem>
                                        <asp:ListItem Value="EC" Text="Eco-Tours"></asp:ListItem>
                                        <asp:ListItem Value="EV" Text="Evening Tour"></asp:ListItem>
                                        <asp:ListItem Value="EX" Text="Excursion"></asp:ListItem>
                                        <asp:ListItem Value="FM" Text="Family Attraction"></asp:ListItem>
                                        <asp:ListItem Value="FA" Text="Fashion"></asp:ListItem>
                                        <asp:ListItem Value="GI" Text="General Interest"></asp:ListItem>
                                        <asp:ListItem Value="HE" Text="Helicopter Tour"></asp:ListItem>
                                        <asp:ListItem Value="HI" Text="Historical"></asp:ListItem>
                                        <asp:ListItem Value="HT" Text="Horticulture"></asp:ListItem>
                                        <asp:ListItem Value="HB" Text="Hot Air Ballooning"></asp:ListItem>
                                        <asp:ListItem Value="KA" Text="Kayaking/Canoeing"></asp:ListItem>
                                        <asp:ListItem Value="MI" Text="Military"></asp:ListItem>
                                        <asp:ListItem Value="MU" Text="Music"></asp:ListItem>
                                        <asp:ListItem Value="NA" Text="Nature"></asp:ListItem>
                                        <asp:ListItem Value="PT" Text="Premium Tours"></asp:ListItem>
                                        <asp:ListItem Value="RT" Text="Recommended Tours"></asp:ListItem>
                                        <asp:ListItem Value="RE" Text="Religious"></asp:ListItem>
                                        <asp:ListItem Value="RB" Text="Restaurants & Bars"></asp:ListItem>
                                        <asp:ListItem Value="3D" Text="RSSC_3D"></asp:ListItem>
                                        <asp:ListItem Value="AU" Text="RSSC_AU"></asp:ListItem>
                                        <asp:ListItem Value="BA" Text="RSSC_BA"></asp:ListItem>
                                        <asp:ListItem Value="CC" Text="RSSC_CC"></asp:ListItem>
                                        <asp:ListItem Value="GP" Text="RSSC_GP"></asp:ListItem>
                                        <asp:ListItem Value="QU" Text="RSSC_QU"></asp:ListItem>
                                        <asp:ListItem Value="SL" Text="Sailing"></asp:ListItem>
                                        <asp:ListItem Value="SF" Text="Scenic Flights"></asp:ListItem>
                                        <asp:ListItem Value="TS" Text="Scenic Train"></asp:ListItem>
                                        <asp:ListItem Value="SC" Text="Science"></asp:ListItem>
                                        <asp:ListItem Value="SH" Text="Shopping"></asp:ListItem>
                                        <asp:ListItem Value="DS" Text="Show/Concert & Meal"></asp:ListItem>
                                        <asp:ListItem Value="ST" Text="Shuttle"></asp:ListItem>
                                        <asp:ListItem Value="SP" Text="Sport"></asp:ListItem>
                                        <asp:ListItem Value="SA" Text="Submarine"></asp:ListItem>
                                        <asp:ListItem Value="SS" Text="Sunrise/Sunset Tour"></asp:ListItem>
                                        <asp:ListItem Value="TH" Text="Theme Park"></asp:ListItem>
                                        <asp:ListItem Value="TR" Text="Transport"></asp:ListItem>
                                        <asp:ListItem Value="WA" Text="Walking/Trekking"></asp:ListItem>
                                        <asp:ListItem Value="WR" Text="Water Recreation"></asp:ListItem>
                                        <asp:ListItem Value="WS" Text="Water Sports"></asp:ListItem>
                                        <asp:ListItem Value="WD" Text="Weddings"></asp:ListItem>
                                        <asp:ListItem Value="WT" Text="Wine Tasting"></asp:ListItem>
                                    </asp:DropDownList>
                    </div>
                     <%--<div class="filter-item price-slider themes_filter pb-3">--%>
                              <%--<div class="ns-h4">PRICE</div> --%> 
                               <%-- <div class="row row mr-1 mb-3">--%>
                                    <%--<div class="col-12 text-right text_sml">
                                        <span class="float-left">
                                            <%=Settings.LoginInfo.Currency %> <%=minPrice%>
                                        </span><span class="float-right">
                                            <%=Settings.LoginInfo.Currency %> <%=maxPrice %>
                                               </span>
                                    </div>--%>
                                    <%--<div class="col-12 pl-3 pr-3">
                                       <input id="RangePrice4" data-slider-id="ExcSlider" type="text" data-slider-min="<%=Session["min"] %>"
                                         data-slider-max="<%=Session["max"] %>" data-slider-step="1" data-slider-value="[<%=minPrice %>,<%=maxPrice %>]"/>
                                        <input style="border: none; width: 100%; font-size: 11px; display: none;" type="text"
                                                id="packageamount" />
                                    </div>--%>
                                    <div class="pad-5">
                                <p>
                                    <label>
                                        Price range:</label><label id="label" onkeypress="return false;"></label>
                                    <input type="text" id="amount" onkeypress="return false;" style="text-align: center;
                                        border: 0; font-weight: bold;" />
                                </p>
                            </div>
                            <div class="pad-5">
                                <div style="width: 90%; margin: auto" id="slider-range" class="flight-slider-range">
                                </div>
                            </div>
                               <%-- </div>--%>
                            <%--</div>--%>
                   
                    
                   
                </div>
                
                <div class="col-md-12 pad_10">
                                           
               
                            
                             <asp:Button CssClass=" btn but_b" runat="server" ID="lnkSearch" Text="Go"  
                                            OnClick="btnSearch_Click" Visible="false" />
                                            </div>
                
            </div>
   
   
    <div class="col-md-10 pad_right0">
     <%if ((filteredResultList != null && filteredResultList.Length > 0))
       { %>
        <div class="listing_search">
             <div style="float: right; margin-left: 15px; width: auto">
                            <%--<asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click"  CssClass="PagerStyle">First</asp:LinkButton>
                       
                            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" CssClass="PagerStyle"  />
                            <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
                        
                            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" CssClass="PagerStyle" />
                        
                            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click" CssClass="PagerStyle">Last</asp:LinkButton>--%>
                 <div class="pagination tt-paging pull-right mt-2">  <%=show%> </div> 
                    </div>
                    <div class="clear">
                    </div>
             </div>
             <%}
       else
       {%>
              <div class=" bg_white bor_gray pad_10">
                    <div class=" fcol_red">
                        No Result Found
                    </div>
                    <div style="text-align: center; font-weight: bold">
                        <%=errorMessage%>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                
      <%} %>

       <div>
            <asp:DataList ID="dlSightSeeing" runat="server" class="hotel_mblock"
                  Width="100%" onitemdatabound="dlSightSeeing_ItemDataBound">
                <ItemTemplate>
                  
                    <div class="bg_white bor_gray marbot_20 pad20">
                         
                         
                           <div class="col-md-2 pad_left0">
                           <asp:Image ID="Image1" runat="server" ImageUrl='<%# Bind("Image") %>' style="width:100%; height:120px" />
                           
                           </div>
                            <div class="col-md-8">
                              <div style=" font-size:14px;"> <strong> <%# Eval("ItemName")%></strong> </div>  
                              <div  class="margin-top-5"><strong>Location :</strong>
                                   <asp:Label ID="lblCity" runat="server" Text='<%# Eval("CityName") %>' ></asp:Label><span>,</span>
                                    <asp:Label ID="lblCountry" runat="server" ></asp:Label></div>
                              <div>  <strong>Duration :</strong><asp:Label ID="lblDuration" runat="server" Text='<%# Eval("Duration") %>'></asp:Label></div> 
                                  
             
             <div><asp:Label ID="Label2" runat="server" Text='<%# System.Web.HttpUtility.HtmlDecode(Convert.ToString(restrictLength(Eval("Description")))) %>'></asp:Label> </div>                 
                                   
                                   
                                   
                                
                               <div class=" marbot_10"> Category: <asp:Label ID="lblCategory" runat="server" Font-Bold="true" ></asp:Label> </div> 
                           
                           <div> 
                  
                                           <a class="linktheme margin-right-5" href="javascript:GetSightSeeingDetail('<%#Eval("Source") %>','<%#Eval("CityCode")%>','<%#Eval("ItemCode")%>','<%#Eval("ItemName")%>','overview')">
                                                OverView</a>
                                                <a class="linktheme margin-right-5" href="javascript:GetSightSeeingDetail('<%#Eval("Source") %>','<%#Eval("CityCode")%>','<%#Eval("ItemCode")%>','<%#Eval("ItemName")%>','inclusions')">
                                                Inclusions</a>
                                                 <a class="linktheme margin-right-5" href="javascript:GetSightSeeingDetail('<%#Eval("Source") %>','<%#Eval("CityCode")%>','<%#Eval("ItemCode")%>','<%#Eval("ItemName")%>','operation')">
                                                Tour Operation</a></li>
                                                <a class="linktheme margin-right-5" href="javascript:GetSightSeeingDetail('<%#Eval("Source") %>','<%#Eval("CityCode")%>','<%#Eval("ItemCode")%>','<%#Eval("ItemName")%>','note')">
                                                Please note</a>
                                                 <a class="linktheme margin-right-5" href="javascript:GetSightSeeingDetail('<%#Eval("Source") %>','<%#Eval("CityCode")%>','<%#Eval("ItemCode")%>','<%#Eval("ItemName")%>','terms')">
                                                Terms & Conditions </a>
                                        
                           
                           </div>
                                        
                                  
                            </div>
                               
                               
                               
                                <div class="col-md-2">
                                  
                                   
                                   <table width="100%"> 
                                   <tr> 
                                   
                                   <td> 
                                   
                                        <p class="lowest_price">Starting From</p>
                                   <p class="best_price">
                                    <%=agency.AgentCurrency %>
                                    <asp:Label ID="lblAmount" runat="server"></asp:Label></p>
                                     
                                     
                                     <div> <a class="but but_b" onclick="showStuff('<%#Eval("ItemCode")+"-"+(Container.ItemIndex+1)%>')" style="cursor: pointer" >
                                           View Details
                                             </a></div>
                                   
                                   </td>
                                   </tr>
                                   
                                   
                                   </table>
                                   
                               
                                     
                                     
                                             
                                 </div>
                                  <div class="clear">
                            </div>
                                </div>
                      
                        
                     <div style="display:none;" id="<%#Eval("ItemCode")+"-"+(Container.ItemIndex+1)%>">
                     <div class="cnt091">

<h4>Tour Info  <a style=" position:absolute; right:10px; z-index:1; color:#fff; cursor:pointer;" onclick="hidestuff('<%#Eval("ItemCode")+"-"+(Container.ItemIndex+1)%>');" href="#">X </a>    </h4>

<table id="tblTourList" class="hotels_grid" width="100%" border="1" cellspacing="0" cellpadding="5" runat="server">
<tr>
    <th width="15%" style="font-weight:bold" height="25" bgcolor="#f1f2f3"><strong>Language</strong></th>
    <th width="17%" bgcolor="#f1f2f3"><strong>Option</strong></th>
    <th width="25%" bgcolor="#f1f2f3"><strong>Duration</strong></th>
    <th width="15%" bgcolor="#f1f2f3"><strong>Amount</strong></th>
    <th width="12%" bgcolor="#f1f2f3"><strong>Condition</strong></th>
    <th width="16%" bgcolor="#f1f2f3"></th>
  </tr>

 
  <%--<table id="tblTourList" runat="server" width="100%" border="1" cellpadding="0" cellspacing="0"></table>--%>

</table>

</div>
</div>

                         
                     
                </ItemTemplate>
            </asp:DataList>
            </div>
       
     
    </div>
  </div>
    <div class="clear">
    </div>
   
    <div class="clear">
    </div>
  
<%--</form>--%>
</div>
   
    <!-- End Sightseeing search Functionality -->
<script type="text/javascript">
    function ClearFilter() {
        document.getElementById("ctl00_cphTransaction_hdnFilter").value = "ClearFilter";   
        $("#slider-range").slider({
            change: function () {
                var duration = document.getElementById('amount').value;
                var min = duration.split('-')[0];
                var max = duration.split('-')[1];
                document.getElementById('<%=hdnPriceMin.ClientID %>').value = min;
                document.getElementById('<%=hdnPriceMax.ClientID %>').value = max;
                document.getElementById('label').InnerHTML = max;
                document.forms[0].submit();
            }
        });

        document.forms[0].submit();
    }
</script>

</asp:Content>


