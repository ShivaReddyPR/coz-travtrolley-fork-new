﻿<%@ page MaintainScrollPositionOnPostback="true" language="C#" masterpagefile="~/TransactionVisaTitle.master" validaterequest="false" autoeventwireup="true" inherits="PackageMasterGUI" title="Package Master" enableeventvalidation="false" Codebehind="PackageMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
   

 <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
     <script type="text/javascript" src="Scripts/Common/Common.js"></script>
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
 <%-- <script type="text/javascript" src="js2/Common.js"></script>--%>
  <script src="Scripts/jsBE/Utils.js" type="text/javascript"></script>
   <script type="text/javascript" src="ash.js"></script>
  <script language="javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js" type="text/javascript"></script>
  
  <style> 
  
  .label { font-size:13px; }
  .custom-checkbox-table input[type="checkbox"] + label,
  .custom-radio-table input[type="radio"] + label{
        position: relative;
        padding-left: 25px;
        margin-right: 20px;
   }
  .custom-checkbox-table input[type="checkbox"] + label:before,
  .custom-radio-table input[type="radio"] + label:before{
        position: absolute;
        left: 0;
        top: 0;
        border: 1px solid #7690b5;
  }
  @media (max-width:1400px){      
   .custom-checkbox-table td{
        width:20%;
        display:block;
        float:left;
    }
  }
  @media (max-width:991px){      
   .custom-checkbox-table td{
        width:33.33%;
    }
  }
  @media (max-width:767px){      
   .custom-checkbox-table td{
        width:50%;
    }
  }
  @media (max-width:480px){      
   .custom-checkbox-table td{
        width:100%;
    }
  }
.rb-custom-checkbox label { margin-right:6px;  }
  </style>

    <script type="text/javascript">


       
        


        //Do not allow special characters in the textbox
        //This function will validate the user input and allows the user to enter only alpha numeric             characters.
        //This function will be invoked when the user enters the PNR's.
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
       // specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        function IsAlphaNumeric(e) {
            //var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            //var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));

            //return ret;

            var regex = new RegExp("^[0-9a-zA-Z \b]+$");
            var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        }

        //Validate email
        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }




        //Added by lokesh on 12 july2017 to display the successfully saved message to the user
        function ShowAlertMessage(msg) {
            alert(msg);
            window.location.href = "PackageMaster.aspx";
        }

        function clearTabs() {
            window.location.href = "PackageMaster.aspx";
        }

        //Added by lokesh on 12 july2017

        //The below functions are used for tabs navigation.
        //Initally only submit button is used to navigate through the tabs
        //Only at the last tab the user is able to see the price and the finally save button.
        $(document).ready(function () {
            $('ul.nav-tabs li').click(function (e) {  
                navigatePackageTabs();
            });
        });

        function navigateTabs() {
            navigatePackageTabs();         
        }

        function navigatePackageTabs() {
            if ($('#tabTourDetails').hasClass('active')) {
                $('#tabTourDetails').removeClass('active');
                $('#tabPriceDetails').addClass('active');
                $("#packageTab1").removeClass('active');
                $("#packageTab2").addClass('active');
                $('#tblControls').hide();
                $('#tabNavControls').show();
            }
            else if ($('#tabPriceDetails').hasClass('active')) {
                $('#tabPriceDetails').removeClass('active');
                $('#tabGallery').addClass('active');
                $("#packageTab2").removeClass('active');
                $("#packageTab3").addClass('active');
                $('#tblControls').hide();
                $('#tabNavControls').show();
            }

            else if ($('#tabGallery').hasClass('active')) {
                $('#tabGallery').removeClass('active');
                $('#tabAvailableDates').addClass('active');
                $("#packageTab3").removeClass('active');
                $("#packageTab4").addClass('active');
                $('#tblControls').hide();
                $('#tabNavControls').show();
            }

            else if ($('#tabAvailableDates').hasClass('active')) {
                $('#tabAvailableDates').removeClass('active');
                $('#tabInclusions').addClass('active');
                $("#packageTab4").removeClass('active');
                $("#packageTab5").addClass('active');
                $('#tblControls').show();
                $('#tabNavControls').hide();
            }
        }

    </script>

    <script type="text/javascript" language="javascript">

    


    tinyMCE.init({
        mode: "textareas",
        theme: "advanced",
        width: '640px',
        theme_advanced_buttons1_add: "bullist,numlist,outdent,indent,undo,redo,fontselect,fontsizeselect",
        theme_advanced_buttons2: "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_disable: "styleselect,anchor,formatselect,justifyfull,help,cleanup",
        convert_fonts_to_spans: true,
        content_css: "/style.css",
        //onchange_callback: "CheckSize",
        //handle_event_callback: "CheckSize"
    });

    



    var Ajax;
    if (window.XMLHttpRequest) {
        Ajax = new window.XMLHttpRequest();
    }
    else {
        Ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }
       //var region;
        var country;
        var city;

        //var regions=[];
        var countries=[];
        var cities=[];
        var rc;
        
        function ShowCountriesList(response) {           
            country = response.responseText.split('#')[0];            
            var ddl = document.getElementById(country);
            if (ddl != null) {
                ddl.options.length = 0;
//                for (var i = 0; i <= ddl.length; i++) {
//                    ddl.remove(i);
//                }                 
            var values = response.responseText.split('#')[1].split(',');
            var el = document.createElement("option");
            el.textContent = "Select Country";
            el.value = "0";
            ddl.add(el,0);
            var values = response.responseText.split('#')[1].split(',');
            
            for (var i = 0; i < values.length; i++) {
                var opt = values[i];
                if (opt.length > 0 && opt.indexOf('|') > 0) {
                    var el = document.createElement("option");
                    el.textContent = opt.split('|')[0];
                    el.value = opt.split('|')[1];
                    ddl.appendChild(el);
                }
            }

                //Set the Country and load its cities
                if (countries.length > 0 && countries[0].trim().length > 0) {
                    var id = eval(country.split('-')[1]);
                    if (id == undefined) {
                        id = 0;
                    }
                    else {
                        id = eval(id - 1);
                    }
                    ddl.value = countries[id];
                    LoadCities(country);
                }
                else {
                    ddl.value = ddl.options[0].value;
                }
            }
            //LoadCities(country);
        }

        var Origincountry;
        var Origincity;

        //var regions=[];
        var Origincountries = [];
        var Origincities = [];
        var Originrc;
        function ShowOriginCountriesList(response) {
            Origincountry = response.responseText.split('#')[0];
            var ddl = document.getElementById(Origincountry);
            if (ddl != null) {
                ddl.options.length = 0;
                //                for (var i = 0; i <= ddl.length; i++) {
                //                    ddl.remove(i);
                //                }                 
                var values = response.responseText.split('#')[1].split(',');
                var el = document.createElement("option");
                el.textContent = "Select Country";
                el.value = "0";
                ddl.add(el, 0);
                var values = response.responseText.split('#')[1].split(',');

                for (var i = 0; i < values.length; i++) {
                    var opt = values[i];
                    if (opt.length > 0 && opt.indexOf('|') > 0) {
                        var el = document.createElement("option");
                        el.textContent = opt.split('|')[0];
                        el.value = opt.split('|')[1];
                        ddl.appendChild(el);
                    }
                }

                //Set the Country and load its cities
                if (Origincountries.length > 0 && Origincountries[0].trim().length > 0) {
                    var id = eval(country.split('-')[1]);
                    if (id == undefined) {
                        id = 0;
                    }
                    else {
                        id = eval(id - 1);
                    }
                    ddl.value = Origincountries[id];
                    LoadOriginCities(Origincountry);
                }
                else {
                    ddl.value = ddl.options[0].value;
                }
            }
            //LoadCities(country);
        }
        
        function LoadCities(id) {
            var countryId = "";
            if (id > 1) {
                city = 'CityText' + id;
                countryId = 'CountryText' + id;
            }
            else{
                city = 'ctl00_cphTransaction_CityText1';
                countryId = 'ctl00_cphTransaction_CountryText1';
            }
            var sel = document.getElementById('<%=hdfCountries.ClientID %>');

           
            sel.value = document.getElementById(countryId).value;
            var paramList = 'requestSource=packageGetCountryIdByCities' + '&Country=' + document.getElementById(countryId).value + '&id=' + city + '&CountryId=' + countryId + '&selId=' + id;
            var url = "CityAjax";
            Ajax.onreadystatechange = ShowCitiesList;
            Ajax.open('POST', url, false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
            //new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: ShowCitiesList });
        }

        function LoadOriginCities(id) {
            
            city = 'ctl00_cphTransaction_originCityText';
            var sel = document.getElementById('<%=hdfOriginCountries.ClientID %>');
            sel.value = document.getElementById(id).value;
            var paramList = 'requestSource=getCountryIdByCities' + '&Country=' + document.getElementById(id).value + '&id=' + city;
            var url = "CityAjax";
            Ajax.onreadystatechange = ShowOriginCitiesList;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
            //new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: ShowCitiesList });
        }
        function ShowCitiesList(response) {
            
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        city = Ajax.responseText.split('#')[0];

                        //Added by venkatesh
                        var countryId = Ajax.responseText.split('#')[1];
                        var selCountryId = Ajax.responseText.split('#')[2];
                        var ddl = document.getElementById(city);
                        var noofCountries = document.getElementById('<%=hdfNoofCountries.ClientID %>').value;
                        if (noofCountries == "" || noofCountries == 0)
                        {
                            noofCountries = 1;
                        }
        
                        var dynId = 2;//Dynamic dropdowns of the country id start from "2";
                        document.getElementById('<%=hdfSelCountry.ClientID %>').value = "";
                        for (var i = 0; i < noofCountries; i++)
                        {
                            if (i == 0) {
                                if (document.getElementById('ctl00_cphTransaction_CountryText1').value != "0") {
                                    //assiging selcted values to hidden filds to retain the value
                                    if (document.getElementById('<%=hdfSelCountry.ClientID %>').value == "") {
                                        document.getElementById('<%=hdfSelCountry.ClientID %>').value = '1-' + document.getElementById('ctl00_cphTransaction_CountryText1').value;
                                    }
                                    else {
                                        document.getElementById('<%=hdfSelCountry.ClientID %>').value += ',1-' + document.getElementById('ctl00_cphTransaction_CountryText1').value;
                                    }
                                    // alert(document.getElementById('<%=hdfSelCountry.ClientID %>').value);
                                }
                            }
                            else {
                                if (document.getElementById('CountryText' + dynId) != null) {
                                    if (document.getElementById('CountryText' + dynId).value != "-1") {
                                        //assiging selcted values to hidden filds to retain the value
                                        if (document.getElementById('<%=hdfSelCountry.ClientID %>').value == "") {
                                            document.getElementById('<%=hdfSelCountry.ClientID %>').value = dynId + '-' + document.getElementById('CountryText' + dynId).value;
                                        }
                                        else {
                                            document.getElementById('<%=hdfSelCountry.ClientID %>').value += ',' + dynId + '-' + document.getElementById('CountryText' + dynId).value;
                                        }
                                    }
                                }


                                dynId = parseInt(dynId + 1);
                            }
                        }
                        <%-- if (countryId != null && countryId != "" && countryId == "ctl00_cphTransaction_CountryText1") {
                            if (document.getElementById('ctl00_cphTransaction_CountryText1').value != "0") {
                                //assiging selcted values to hidden filds to retain the value
                                if (document.getElementById('<%=hdfSelCountry.ClientID %>').value == "") {
                                    document.getElementById('<%=hdfSelCountry.ClientID %>').value = '1-' + document.getElementById('ctl00_cphTransaction_CountryText1').value;
                                }
                                else {
                                    document.getElementById('<%=hdfSelCountry.ClientID %>').value += ',1-' + document.getElementById('ctl00_cphTransaction_CountryText1').value;
                                }
                                // alert(document.getElementById('<%=hdfSelCountry.ClientID %>').value);
                            }
                        }
                        else if(countryId != null ) {
                            if (document.getElementById(countryId).value != "-1") {
                                var cid = countryId.replace()
                                //assiging selcted values to hidden filds to retain the value
                                if (document.getElementById('<%=hdfSelCountry.ClientID %>').value == "") {
                                    document.getElementById('<%=hdfSelCountry.ClientID %>').value = selCountryId + '-' + document.getElementById(countryId).value;
                                }
                                else {
                                    document.getElementById('<%=hdfSelCountry.ClientID %>').value += ',' + selCountryId + '-' + document.getElementById(countryId).value;
                                }
                                // alert(document.getElementById('<%=hdfSelCountry.ClientID %>').value);
                            }
                        }--%>


                        <%--if (document.getElementById('ctl00_cphTransaction_CityText1').value != "0") {
                            document.getElementById('<%=hdfSelCity.ClientID %>').value = document.getElementById('ctl00_cphTransaction_CityText1').value;
                        }--%>

                        

                        if (ddl != null) {

                            if (ddl.id == "ctl00_cphTransaction_CityText1") {
                                if (document.getElementById('select2-chosen-9') != null) {
                                    document.getElementById('select2-chosen-9').innerText = "Select City";
                                    ddl.value = 0;
                                }
                            }
                            else {
                                ddl.value = "Select City";
                            }
                                ddl.options.length = 0;
                                //                for (var i = 0; i <= ddl.length; i++) {
                                //                    ddl.remove(i);
                                //                }

                                var el = document.createElement("option");
                                el.textContent = "Select City";
                                el.value = "0";
                                ddl.add(el, 0);
                                var values = Ajax.responseText.split('#')[3].split(',');

                                for (var i = 0; i < values.length; i++) {
                                    var opt = values[i];
                                    if (opt.length > 0 && opt.indexOf('|') > 0) {
                                        var el = document.createElement("option");
                                        el.textContent = opt.split('|')[0];
                                        el.value = opt.split('|')[1];
                                        ddl.appendChild(el);
                                    }
                                }

                                if (cities.length > 0 && cities[0].trim().length > 0) {
                                    var id = eval(city.split('-')[1]);
                                    if (id == undefined) {
                                        id = 0;
                                    }
                                    else {
                                        id = eval(id - 1);
                                    }

                                    ddl.value = cities[id];
                                }
                            }
                    }
                }
            }
        }

    

        function ShowOriginCitiesList(response) {
        
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        Origincity = Ajax.responseText.split('#')[0];
                        var ddl = document.getElementById(Origincity);
                        if (document.getElementById('ctl00_cphTransaction_OriginCountryText').value != "0") {
                            //assiging selcted values to hidden filds to retain the value
                            document.getElementById('<%=hdfOriginSelCountry.ClientID %>').value = document.getElementById('ctl00_cphTransaction_OriginCountryText').value;
                            // alert(document.getElementById('<%=hdfSelCountry.ClientID %>').value);
                        }
                        if (document.getElementById('ctl00_cphTransaction_originCityText').value != "0") {
                            document.getElementById('<%=hdfOriginSelCity.ClientID %>').value = document.getElementById('ctl00_cphTransaction_originCityText').value;
                        }
                        if (ddl != null) {
                            ddl.options.length = 0;
                            //                for (var i = 0; i <= ddl.length; i++) {
                            //                    ddl.remove(i);
                            //                }

                            var el = document.createElement("option");
                            el.textContent = "Select City";
                            el.value = "0";
                            ddl.add(el, 0);
                            var values = Ajax.responseText.split('#')[1].split(',');

                            for (var i = 0; i < values.length; i++) {
                                var opt = values[i];
                                if (opt.length > 0 && opt.indexOf('|') > 0) {
                                    var el = document.createElement("option");
                                    el.textContent = opt.split('|')[0];
                                    el.value = opt.split('|')[1];
                                    ddl.appendChild(el);
                                }
                            }
                            document.getElementById('select2-chosen-7').innerText = "Select City";
                            ddl.value = 0;
                            if (Origincities.length > 0 && Origincities[0].trim().length > 0) {
                                var id = eval(city.split('-')[1]);
                                if (id == undefined) {
                                    id = 0;
                                }
                                else {
                                    id = eval(id - 1);
                                }

                                ddl.value = Origincities[id];
                            }
                        }
                    }
                }
            }
        }

        function SetCity(id, cityNo) {
            //assiging selcted values to hidden filds to retain the value
            //document.getElementById('<%=hdfSelCity.ClientID %>').value = document.getElementById('ctl00_ContentPlaceHolder1_CityText1').value;
            //alert(document.getElementById('<%=hdfSelCity.ClientID %>').value);
            <%--if (id != "ctl00_cphTransaction_CityText1") {
                var sel = document.getElementById('<%=hdfCities.ClientID %>');
               
                
                if (sel.value.indexOf(document.getElementById(id).value) < 0 && sel.value != "0") {
                    if (sel.value.trim().length > 0) {
                        sel.value += "," + document.getElementById(id).value;
                    }
                    else {
                        sel.value = document.getElementById(id).value;
                    }
                    //alert(sel.value);
                    if (document.getElementById('<%=hdfSelCity.ClientID %>').value != null && document.getElementById('<%=hdfSelCity.ClientID %>').value != "") {
                        document.getElementById('<%=hdfSelCity.ClientID %>').value = document.getElementById('<%=hdfSelCity.ClientID %>').value + ',' + cityNo + '-' + document.getElementById(id).value;
                    }
                    else {
                        document.getElementById('<%=hdfSelCity.ClientID %>').value = cityNo + '-' + document.getElementById(id).value;
                    }
                }
            } else {
                //assiging selcted values to hidden filds to retain the value
                if (document.getElementById('ctl00_cphTransaction_CityText1').value != "0") {
                    if (document.getElementById('<%=hdfSelCity.ClientID %>').value != "" && document.getElementById('<%=hdfSelCity.ClientID %>').value != null) {
                        document.getElementById('<%=hdfSelCity.ClientID %>').value = document.getElementById('<%=hdfSelCity.ClientID %>').value +',1-' + document.getElementById('ctl00_cphTransaction_CityText1').value;
                    }
                    else {
                        document.getElementById('<%=hdfSelCity.ClientID %>').value = '1-' + document.getElementById('ctl00_cphTransaction_CityText1').value;
                    }
                }
                //alert(document.getElementById('<%=hdfSelCity.ClientID %>').value);
            }--%>


            var noofCountries = document.getElementById('<%=hdfNoofCountries.ClientID %>').value;
                        if (noofCountries == "" || noofCountries == 0)
                        {
                            noofCountries = 1;
                        }
        
                        var dynId = 2;//Dynamic dropdowns of the country id start from "2";
                        document.getElementById('<%=hdfSelCity.ClientID %>').value = "";
                        for (var i = 0; i < noofCountries; i++)
                        {
                            if (i == 0) {
                                if (document.getElementById('ctl00_cphTransaction_CityText1').value != "0") {
                                    //assiging selcted values to hidden filds to retain the value
                                    if (document.getElementById('<%=hdfSelCity.ClientID %>').value == "") {
                                        document.getElementById('<%=hdfSelCity.ClientID %>').value = '1-' + document.getElementById('ctl00_cphTransaction_CityText1').value;
                                    }
                                    else {
                                        document.getElementById('<%=hdfSelCity.ClientID %>').value += ',1-' + document.getElementById('ctl00_cphTransaction_CityText1').value;
                                    }
                                    // alert(document.getElementById('<%=hdfSelCountry.ClientID %>').value);
                                }
                            }
                            else {
                                if (document.getElementById('CityText' + dynId) != null) {
                                    if (document.getElementById('CityText' + dynId).value != "-1") {
                                        //assiging selcted values to hidden filds to retain the value
                                        if (document.getElementById('<%=hdfSelCity.ClientID %>').value == "") {
                                            document.getElementById('<%=hdfSelCity.ClientID %>').value = dynId + '-' + document.getElementById('CityText' + dynId).value;
                                        }
                                        else {
                                            document.getElementById('<%=hdfSelCity.ClientID %>').value += ',' + dynId + '-' + document.getElementById('CityText' + dynId).value;
                                        }
                                    }
                                }
                                dynId = parseInt(dynId + 1);
                            }
                        }


        }

        function SetOriginCity(id) {
            //assiging selcted values to hidden filds to retain the value
            //document.getElementById('<%=hdfSelCity.ClientID %>').value = document.getElementById('ctl00_ContentPlaceHolder1_CityText1').value;
            //alert(document.getElementById('<%=hdfSelCity.ClientID %>').value);
            if (id != "ctl00_cphTransaction_originCityText") {
                var sel = document.getElementById('<%=hdfOriginCities.ClientID %>');


                if (sel.value.indexOf(document.getElementById(id).value) < 0 && sel.value != "0") {
                    if (sel.value.trim().length > 0) {
                        sel.value += "," + document.getElementById(id).value;
                    }
                    else {
                        sel.value = document.getElementById(id).value;
                    }
                    //alert(sel.value);
                }
            } else {
                //assiging selcted values to hidden filds to retain the value
            if (document.getElementById('ctl00_cphTransaction_originCityText').value != "0") {
                document.getElementById('<%=hdfOriginSelCity.ClientID %>').value = document.getElementById('ctl00_cphTransaction_originCityText').value;
                }
                //alert(document.getElementById('<%=hdfSelCity.ClientID %>').value);
            }
        }
        
    function init1() {

        //    showReturn();
        var today = new Date();
        // For making dual Calendar use CalendarGroup  for single Month use Calendar     
        cal3 = new YAHOO.widget.Calendar("cal3", "container3");
        cal3.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
        cal3.cfg.setProperty("title", "Select From Date");
        cal3.cfg.setProperty("close", true);
        cal3.selectEvent.subscribe(setDate3);
        cal3.render();

        cal4 = new YAHOO.widget.Calendar("cal4", "container4");
        cal4.cfg.setProperty("title", "Select To Date");
        cal4.selectEvent.subscribe(setDate4);
        cal4.cfg.setProperty("close", true);
        cal4.render();
    }
    function init2() {

        //    showReturn();
        var today = new Date();
        // For making dual Calendar use CalendarGroup  for single Month use Calendar     
        cal5 = new YAHOO.widget.Calendar("cal5", "container5");
        cal5.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
        cal5.cfg.setProperty("title", "Select From Date");
        cal5.cfg.setProperty("close", true);
        cal5.selectEvent.subscribe(setDate5);
        cal5.render();

        cal6 = new YAHOO.widget.Calendar("cal6", "container6");
        cal6.cfg.setProperty("title", "Select To Date");
        cal6.selectEvent.subscribe(setDate6);
        cal6.cfg.setProperty("close", true);
        cal6.render();
    }
    function init3() {

        //    showReturn();
        
        var today = new Date();
        // For making dual Calendar use CalendarGroup  for single Month use Calendar
        cal7 = new YAHOO.widget.Calendar("cal7", "container7");
        cal7.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
        cal7.cfg.setProperty("title", "Select From Date");
        cal7.cfg.setProperty("close", true);
        cal7.selectEvent.subscribe(setDate7);
        cal7.render();

        cal8 = new YAHOO.widget.Calendar("cal8", "container8");
        cal8.cfg.setProperty("title", "Select To Date");
        cal8.selectEvent.subscribe(setDate8);
        cal8.cfg.setProperty("close", true);
        cal8.render();
    }
       function init4()
 {
   cal1 = new YAHOO.widget.Calendar("cal1","callContainer");   
   cal1.selectEvent.subscribe(setDate9);   
   cal1.cfg.setProperty("close",true);
   cal1.render(); 
 }

 
 function setDate9() {
			var date1 = cal1.getSelectedDates()[0];		
			var month = date1.getMonth()+1;
			var day = date1.getDate();
			
			if (month.toString().length == 1)
			{
			  month = "0"+month;
			}	
			
			if (day.toString().length == 1)
			{
			  day = "0"+day;
			}				
						
		   var availDateFrom= new Date() ;
		   var availDateTo= new Date();
		   var unAvDate= new Date();
		   availDateFrom=document.getElementById('<% = txtFrom.ClientID %>').value;
		   availDateTo = document.getElementById('<% = txtTo.ClientID %>').value;
		  
		   var d1 = availDateFrom.split("/");
		   var d2 = availDateTo.split("/");
		   var c = unAvDate.split("/");

		   var from = new Date(d1[2], d1[1] - 1, d1[0]);  // -1 because months are from 0 to 11
		   var to = new Date(d2[2], d2[1] - 1, d2[0]);
		   var check = new Date(c[2], c[1] - 1, c[0]);
		   if (check < from || check > to)
		    {
		         document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "UnAvail Dates should be in between from Date and To Date ";
                return false;
		    }
		    else 
		    {
		        document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
		    }
		    
		    			

		  
		    
		    var counter = document.getElementById("<%=hdfUnavailDateCounter.ClientID%>").value;		    
		    counter= counter - 1;	
		    
	        
	        
			document.getElementById('callContainer').style.display="none";
		}
		
    function ShowCalender(container)
    {
        var containerId=container;   
        if(document.getElementById(containerId).style.display=="none")
        {        
            document.getElementById(containerId).style.display="block"
        }
    }
//    function showCalendar1() {
//        document.getElementById('container2').style.display = "none";
//        document.getElementById('container1').style.display = "block";
//        document.getElementById('container2').style.display = "none";
//        document.getElementById('container1').style.display = "block"

//    }
    function showCalendar3() {
//        document.getElementById('container4').style.display = "none";
//        document.getElementById('container3').style.display = "block";
hideAvailableDatesCal("cal3");
       document.getElementById('container4').style.display = "none";
       document.getElementById('container3').style.display = "block";
    }
    function showCalendar5() {
        document.getElementById('container6').style.display = "none";
        document.getElementById('container5').style.display = "block";
//        document.getElementById('container6').style.display = "none";
//        document.getElementById('container5').style.display = "block";

    }
    function showCalendar7() {
//        document.getElementById('container8').style.display = "none";
//        document.getElementById('container7').style.display = "block";
            hideAvailableDatesCal("cal7");
            var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var fromDateArray = date1.split('/');
                if(fromDateArray.length > 2){
                       var arrMinDate = new Date(fromDateArray[2], fromDateArray[1], fromDateArray[0]);
                       cal7.cfg.setProperty("minDate", (arrMinDate.getMonth()) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                       cal7.cfg.setProperty("pageDate", fromDateArray[1] + "/" + fromDateArray[2]);
                       cal7.render();
                }
            }
            
            var date2 = document.getElementById("<%=txtTo.ClientID%>").value;

            if (date2.length != 0 && date2 != "DD/MM/YYYY") {
                var toDateArray = date2.split('/');
                if(toDateArray.length > 2){
                       var arrMinDate = new Date(toDateArray[2], (toDateArray[1]), toDateArray[0]);
                       cal7.cfg.setProperty("maxDate", (arrMinDate.getMonth()) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                       cal7.cfg.setProperty("pageDate", toDateArray[1] + "/" + toDateArray[2]);
                       cal7.render();
                }
            }


       document.getElementById('container8').style.display = "none";
       document.getElementById('container7').style.display = "block";
    }
    var departureDate = new Date();
    function showCalendar2() {
        
        //vij document.getElementById('container1').style.display = "none";
        document.getElementById('container1').style.display = "none";
        cal1.hide();
        // setting Calender2 min date acoording to calendar1 selected date
        var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
        //var date1=new Date(tempDate.getDate()+1);

        if (date1.length != 0 && date1 != "DD/MM/YYYY") {
            var depDateArray = date1.split('/');

            var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

            cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
            cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
            cal2.render();
        }
        document.getElementById('container2').style.display = "block";
    }
    function showCalendar4() {
        hideAvailableDatesCal("cal4");
       //vij document.getElementById('container3').style.display = "none";
       document.getElementById('container3').style.display = "none";
        cal3.hide();
        // setting Calender2 min date acoording to calendar1 selected date
        var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
        //var date1=new Date(tempDate.getDate()+1);

        if (date1.length != 0 && date1 != "DD/MM/YYYY") {
            var depDateArray = date1.split('/');

            var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

            cal4.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
            cal4.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
            cal4.render();
        }
        document.getElementById('container4').style.display = "block";
    }

    function showCalendar8() {
    hideAvailableDatesCal("cal8");
        //document.getElementById('container7').style.display = "none";
        document.getElementById('container7').style.display = "none";
        cal5.hide();
        // setting Calender2 min date acoording to calendar1 selected date
//        var date1 = document.getElementById("<%=txtPickUpHr.ClientID%>").value;
        var date1 = document.getElementById("<%=txtUnavailFrom.ClientID%>").value;
        //var date1=new Date(tempDate.getDate()+1);

        if (date1.length != 0 && date1 != "DD/MM/YYYY") {
            var depDateArray = date1.split('/');

            var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

            cal8.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
            cal8.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
            cal8.render();
        }
        
         var date2 = document.getElementById("<%=txtTo.ClientID%>").value;

            if (date2.length != 0 && date2 != "DD/MM/YYYY") {
                var toDateArray = date2.split('/');
                if(toDateArray.length > 2){
                       var arrMinDate = new Date(toDateArray[2], (toDateArray[1]), toDateArray[0]);
                       cal8.cfg.setProperty("maxDate", (arrMinDate.getMonth()) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                       cal8.cfg.setProperty("pageDate", toDateArray[1] + "/" + toDateArray[2]);
                       cal8.render();
                }
            }
        document.getElementById('container8').style.display = "block";
    }

    //YAHOO.util.Event.addListener(window, "load", init);

    function setDate3() {
    
        var date1 = cal3.getSelectedDates()[0];
        //alert(date1);
        document.getElementById('IShimFrame').style.display = "none";
        this.today = new Date();
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var thisTime = this.today.getHours();
        var thisMinutes = this.today.getMinutes();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());

        if (difference < 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
            return false;
        }
        departureDate = cal3.getSelectedDates()[0];
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";
        //			
        var month = date1.getMonth() + 1;
        var day = date1.getDate();
        var time = "";
        var timePM = "";

        if (month.toString().length == 1) {
            month = "0" + month;
        }

        if (day.toString().length == 1) {
            day = "0" + day;
        }
        if (thisMinutes < 10) {
            thisMinutes = "0" + thisMinutes
        }
        if (thisTime > 11) {
            timePM = ("PM")
        } else {
            timePM = ("AM")
        }
        time = (thisTime + ":" + thisMinutes + timePM + " ");
        document.getElementById("<%=txtFrom.ClientID%>").value = day + "/" + (month) + "/" + date1.getFullYear();
        document.getElementById("<%=txtFromTime.ClientID%>").value = time;

        //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
        //cal2.render();

        cal3.hide();

    }
    function setDate4() {
    hideAvailableDatesCal("cal4")
        var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
        if (date1.length == 0 || date1 == "DD/MM/YYYY") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "First select from date.";
            return false;
        }

        var date2 = cal4.getSelectedDates()[0];

        var depDateArray = date1.split('/');

        // checking if date1 is valid		    
        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = " Invalid To Date";
            return false;
        }
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";

        // Note: Date()	for javascript take months from 0 to 11
        var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
        var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
        var difference = returndate.getTime() - depdate.getTime();
        this.today = depdate; 
        var thisTime = this.today.getHours();
        var thisMinutes = this.today.getMinutes();
        if (difference < 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "To Date should be greater than  or equal to date of From (" + date1 + ")";
            return false;
        }

        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";

        var month = date2.getMonth() + 1;
        var day = date2.getDate();

        if (month.toString().length == 1) {
            month = "0" + month;
        }

        if (day.toString().length == 1) {
            day = "0" + day;
        }
        if (thisMinutes < 10) {
            thisMinutes = "0" + thisMinutes
        }
        if (thisTime > 11) {
            timePM = ("PM")
        } else {
            timePM = ("AM")
        }
        time = (thisTime + ":" + thisMinutes + timePM + " ");

        document.getElementById("<%=txtTo.ClientID%>").value = day + "/" + month + "/" + date2.getFullYear();
        document.getElementById("<%=txtToTime.ClientID%>").value = time;

        cal4.hide();
    }
    YAHOO.util.Event.addListener(window, "load", init1);

    function setDate5() {


    }
    function setDate6() {
    }
    YAHOO.util.Event.addListener(window, "load", init2);

    function setDate7() {
    hideAvailableDatesCal("cal7");
        var fromDate = document.getElementById("<%=txtFrom.ClientID%>").value;
        if (fromDate.length == 0 || fromDate == "DD/MM/YYYY") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "First select from date.";
            return false;
        }
        
        var fromDate = document.getElementById("<%=txtTo.ClientID%>").value;
        if (fromDate.length == 0 || fromDate == "DD/MM/YYYY") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "First select to date.";
            return false;
        }
    
    
        var date1 = cal7.getSelectedDates()[0];

        document.getElementById('IShimFrame').style.display = "none";
        this.today = new Date();
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var thisTime = this.today.getHours();
        var thisMinutes = this.today.getMinutes();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());

        if (difference < 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
            return false;
        }
        departureDate = cal7.getSelectedDates()[0];
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";
        //			
        var month = date1.getMonth() + 1;
        var day = date1.getDate();
        var time = "";
        var timePM = "";

        if (month.toString().length == 1) {
            month = "0" + month;
        }

        if (day.toString().length == 1) {
            day = "0" + day;
        }
        if (thisMinutes < 10) {
            thisMinutes = "0" + thisMinutes
        }
        if (thisTime > 11) {
            timePM = ("PM")
        } else {
            timePM = ("AM")
        }
       //time = (thisTime + ":" + thisMinutes + timePM + " ");
        document.getElementById("<%=txtUnavailFrom.ClientID%>").value = day + "/" + (month) + "/" + date1.getFullYear();
        //document.getElementById("<%=txtUnavailFrom.ClientID%>").value = time;
        //getElement('txtUnavailTo').value = document.getElementById("<%=txtUnavailFrom.ClientID%>").value;

        //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
        //cal2.render();

        cal7.hide();

    }
    function setDate8() {
 hideAvailableDatesCal("cal8");
        var date1 = document.getElementById("<%=txtUnavailFrom.ClientID%>").value;
        
        if (date1.length == 0 || date1 == "DD/MM/YYYY") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "First select from date.";
            return false;
        }

        var date2 = cal8.getSelectedDates()[0];

        var depDateArray = date1.split('/');

        // checking if date1 is valid		    
        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
        
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = " Invalid To Date";
            return false;
        }
        
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";

        // Note: Date()	for javascript take months from 0 to 11
        var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
        var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
        var difference = returndate.getTime() - depdate.getTime();
        var thisTime = this.today.getHours();
        var thisMinutes = this.today.getMinutes();
        if (difference < 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "To Date should be greater than  or equal to date of From (" + date1 + ")";
            return false;
        }

        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";

        var month = date2.getMonth() + 1;
        var day = date2.getDate();

        if (month.toString().length == 1) {
            month = "0" + month;
        }

        if (day.toString().length == 1) {
            day = "0" + day;
        }
        if (thisMinutes < 10) {
            thisMinutes = "0" + thisMinutes
        }
        if (thisTime > 11) {
            timePM = ("PM")
        } else {
            timePM = ("AM")
        }
        time = (thisTime + ":" + thisMinutes + timePM + " ");

        document.getElementById("<%=txtUnavailTo.ClientID%>").value = day + "/" + month + "/" + date2.getFullYear();
        //document.getElementById("<%=txtUnavailTo.ClientID%>").value = time;

        cal8.hide();
    }
    
    function hideAvailableDatesCal(calendar){
    if("cal3" != calendar){
    cal3.hide();
    }
    if("cal4" != calendar){
    cal4.hide();
    }
    if("cal7" != calendar){
    cal7.hide();
    }
    if("cal8" != calendar){
    cal8.hide();
    }
    }
    YAHOO.util.Event.addListener(window, "load", init3);
    
     YAHOO.util.Event.addListener(window, "load", init4);
		
    function validateDtlAddUpdate(action, id) {
        try {

            var msg = '';
            var rowId = id.substring(0, id.lastIndexOf('_'));
            if (action == 'I') {
               
                if (document.getElementById(rowId + '_FTtxtLabel').value == '') msg += '\n Label cannot be blank!';
                if (document.getElementById(rowId + '_FTddlControl').selectedIndex <= 0) msg += '\n Please select a Control! ';
                if (document.getElementById(rowId + '_FTddlControl').value == 'L') 
                {
                     if (document.getElementById(rowId + '_FTtxtSqlQuery').value == '') msg += 'SqlQuery cannot be blank!';
                }
                if (document.getElementById(rowId + '_FTddlDatatype').selectedIndex <= 0) msg += '\n Please select a DataType! ';
                if (document.getElementById(rowId + '_FTddlMandatory').selectedIndex <= 0) msg += '\n Please select a MandatoryStatus!';
                if (document.getElementById(rowId + '_FTtxtOrder').value == '') msg += '\n Order cannot be blank!';
                
            }
            else if (action == 'U')//Update
            {    
                if (document.getElementById(rowId + '_EITtxtLabel').value == '') msg += 'Label cannot be blank!';         
                //if (document.getElementById(rowId + '_EITddlControl').selectedIndex <= 0) msg += '\n Please select a Control! ';  
                if (document.getElementById(rowId + '_EITddlControl').value == 'L') 
                {
                     if (document.getElementById(rowId + '_EITtxtSqlQuery').value == '') msg += 'SqlQuery cannot be blank!';
                }               
                //if (document.getElementById(rowId + '_EITddlDatatype').selectedIndex <= 0) msg += '\n Please select a DataType! ';
                //if (document.getElementById(rowId + '_EITddlMandatory').selectedIndex <= 0) msg += '\n Please select a MandatoryStatus!';
                if (document.getElementById(rowId + '_EITtxtOrder').value == '') msg += '\n Order cannot be blank!';
            }
            if (msg != '') {
                alert(msg);
                return false;
            }
            return true;
        }
        catch (e) {
            return true;
        }
    }
    function validateDtlAddUpdate1(action, id) {
        try {

            var msg = '';
            var rowId = id.substring(0, id.lastIndexOf('_'));
            if (action == 'I') {
                if (document.getElementById(rowId + '_FTtxtLabel').value =='') msg += '\n Label cannot be Blank!';
                if (document.getElementById(rowId + '_FTtxtSupplierCost').value =='') msg += '\n Supplier Cost cannot be Blank!';
                if (document.getElementById(rowId + '_FTtxtTax').value =='') msg += '\n Tax cannot be Blank!';
                if (document.getElementById(rowId + '_FTtxtMarkup').value == '') msg += '\n Markup cannot be Blank!';
                if (document.getElementById(rowId + '_FTtxtMinPax').value == '') {
                    msg += '\n MinPax cannot be Blank!';
                }
                else if (parseInt(document.getElementById(rowId + '_FTtxtMinPax').value) <= 0) {
                    msg += '\n At least 1 min pax is Required!';
                }
            }
            else if (action == 'U')//Update
            {
                if (document.getElementById(rowId + '_EITtxtLabel').value =='') msg += '\n Label cannot be Blank!';
                if (document.getElementById(rowId + '_EITtxtSupplierCost').value =='') msg += '\n Supplier Cost cannot be Blank!';
                if (document.getElementById(rowId + '_EITtxtTax').value =='') msg += '\n Tax cannot be Blank!';
                if (document.getElementById(rowId + '_EITtxtMarkup').value == '') msg += '\n Markup cannot be Blank!';
                if (document.getElementById(rowId + '_EITtxtMinPax').value == '') {
                    msg += '\n MinPax cannot be Blank!';
                }
                else if (parseInt(document.getElementById(rowId + '_EITtxtMinPax').value) <= 0) {
                    msg += '\n At least 1 min pax is Required!';
                }
            }
            if (msg != '') {
                alert(msg);
                return false;
            }
            return true;
        }
        catch (e) {
            return true;
        }
    }

    function validateSupplierAddUpdate(action, id) {
        try {

            var msg = '';
            var rowId = id.substring(0, id.lastIndexOf('_'));
            var validProduct='Y';
            if (action == 'I') {
                if (document.getElementById(rowId + '_FTtxtSuppName').value == '') msg += '\n Supplier Name cannot be Blank!';
                if (document.getElementById(rowId + '_FTtxtSuppEmail').value == '') msg += '\n Supplier Mail cannot be Blank!';
                if (document.getElementById(rowId + '_FTddlSuppProduct').selectedIndex <= 0) msg += '\n Please select Product from the List!';

                if (document.getElementById(rowId + '_FTtxtSuppEmail').value.length >0 && !validateEmail(document.getElementById(rowId + '_FTtxtSuppEmail').value)) msg += '\n Not a valid email';
                

                var ddl = document.getElementById(rowId + '_FTddlSuppProduct');
                var selValue =  ddl.options[ddl.selectedIndex].value;
                
                if(selValue=='A' && !getElement('chkAir').checked)
                            validProduct='N'                    
                else if(selValue=='H' && !getElement('chkHotel').checked)
                           validProduct='N'
                else if(selValue=='T' && !getElement('chkTours').checked)
                           validProduct='N'
                else if(selValue=='R' && !getElement('chkTransfer').checked)
                           validProduct='N'
                else if (selValue == 'M' && !getElement('chkMeals').checked)
                          validProduct = 'N'

                if(validProduct=='N') msg += '\n Selected product is not included in the Package.Pleaase select valid product!';
            }
            else if (action == 'U')//Update
            {
                if (document.getElementById(rowId + '_EITtxtSuppName').value == '') msg += '\n Supplier Name cannot be Blank!';
                if (document.getElementById(rowId + '_EITtxtSuppEmail').value == '') msg += '\n Supplier Mail cannot be Blank!';
                if (document.getElementById(rowId + '_EITddlSuppProduct').selectedIndex <= 0) msg += '\n Please select Product from the List!';
                if (document.getElementById(rowId + '_EITtxtSuppEmail').value.length > 0 && !validateEmail(document.getElementById(rowId + '_EITtxtSuppEmail').value)) msg += '\n Not a valid email';
                var ddl = document.getElementById(rowId + '_EITddlSuppProduct');
                var selValue = ddl.options[ddl.selectedIndex].value;

                if (selValue == 'A' && !getElement('chkAir').checked)
                    validProduct = 'N'
                else if (selValue == 'H' && !getElement('chkHotel').checked)
                    validProduct = 'N'
                else if (selValue == 'T' && !getElement('chkTours').checked)
                    validProduct = 'N'
                else if (selValue == 'R' && !getElement('chkTransfer').checked)
                    validProduct = 'N'
                else if (selValue == 'M' && !getElement('chkMeals').checked)
                    validProduct = 'N'

                if (validProduct == 'N') msg += '\n Selected product is not included in the Package.Pleaase select valid product!';
            }
            if (msg != '') {
                alert(msg);
                return false;
            }
            return true;
        }
        catch (e) {
            return true;
        }
    }
   
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function Save() {

    document.getElementById('errMess').style.display = "none";

    if (document.getElementById("<%=ddlAgent.ClientID%>").value <= 0) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Please select agent from the list!";
        ShowTab('packageTab1');
        return false;
    }
    if (document.getElementById("<%=txtPackageName.ClientID%>").value == "") {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "PackageName cannot be blank!"
        ShowTab('packageTab1');
        return false;
    }


    if (document.getElementById("<%=txtStockInHand.ClientID%>").value == "") {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Stock in Hand cannot be Blank !";
        ShowTab('packageTab1');
        return false;
    }

    if (document.getElementById("<%=txtStartingFrom.ClientID%>").value == "") {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "StartingFrom cannot be blank!";
        ShowTab('packageTab1');
        return false;
    }

    if (Trim(document.getElementById('<%=OriginCountryText.ClientID %>').value) == "0" || Trim(document.getElementById('<%=OriginCountryText.ClientID %>').value) == "") {

        // document.getElementById('errorMessage').innerHTML = "Please select Country for the Fixed Departure.";
        //document.getElementById('errMess').style.display = "block";
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Please select Origin Country from the list .";
        ShowTab('packageTab1');
        return false;
    }
    else if (Trim(document.getElementById('<%=originCityText.ClientID %>').value) == "0" || Trim(document.getElementById('<%=originCityText.ClientID %>').value) == "") {

        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Please select Origin City from the list .";
        ShowTab('packageTab1');
        return false;
    }


    


    /***************************Start:Tiny MCE Editor Validation ****************/
    var daysCount = document.getElementById("<%=ddlDays.ClientID%>").value;
    if (parseInt(daysCount) > 0) {
        for (var i = 1; i <= parseInt(daysCount) ; i++) {
            /*While dealing with mce editors do not target the textarea element*/
            /*Every tinymce editor will generate a respective iframe and 
             stores the entered content inside the body tag of the iframe element*/

            //Step-1: Get Iframe Id <iframe id="mce_editor_1"
            //Step-2: Target body element in the iframe
            //Step-3: Check the characters count.


            var editorId = i + 1;
            if (document.getElementById('mce_editor_' + editorId) != null
                &&
                document.getElementById('mce_editor_' + editorId).contentDocument.body.innerHTML.length == 0
                &&
                document.getElementById('mce_editor_' + editorId).contentDocument.body.innerHTML == "") {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter the details of Itinerary : " + i;
                ShowTab('packageTab1');
                return false;
            }
        }
    }
    /***************************End:Tiny MCE Editor Validation ****************/






    
    if (document.getElementById("<%=hdfUploadYNImage1.ClientID%>").value == "0") {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Image 1 should be uploaded !";
        ShowTab('packageTab3');
        return false;
    }

    if (document.getElementById("<%=hdfUploadYNImage2.ClientID%>").value == "0") {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Image 2 should be uploaded !";
        ShowTab('packageTab3');
        return false;
    }


    if (document.getElementById("<%=hdfUploadYNImage3.ClientID%>").value == "0") {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Image 3 should be uploaded !";
        ShowTab('packageTab3');
        return false;
    }


    var chklist = document.getElementById('<%= chkTheme.ClientID %>');
    var chkListinputs = chklist.getElementsByTagName("input");
    var count = 0;
    for (var i = 0; i < chkListinputs.length; i++) {
        if (chkListinputs[i].checked) {
            count = count + 1;
        }
    }
    if (count == 0) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Atleast one theme should be selected !";
        ShowTab('packageTab1');
        return false;
    }


    var regexStr = /<\S[^><]*>/g;
    var regexStr1 = /&nbsp;/g;
    var regexStr2 = /<BR>/g;
    var changed = window.frames.mce_editor_0.document.body.innerHTML.replace(regexStr, '');
    var changed = changed.replace(regexStr1, '');
    var changed = changed.replace(regexStr2, '');
    if (trim(changed) == "") {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Please enter Intoduction.";
        ShowTab('packageTab1');
        return false;
    }

    changed = window.frames.mce_editor_1.document.body.innerHTML.replace(regexStr, '');
    var changed = changed.replace(regexStr1, '');
    var changed = changed.replace(regexStr2, '');

    if (trim(changed) == "") {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Please enter OverView.";
        ShowTab('packageTab1');
        return false;
    }


    //          if (document.getElementById("<%=hdfUploadYNImage2.ClientID%>").value =="0") {
    //            document.getElementById('errMess').style.display = "block";
    //            document.getElementById('errorMessage').innerHTML = "Image 2 should be uploaded !";
    //            return false;
    //        }
    //        
    //        
    //          if (document.getElementById("<%=hdfUploadYNImage2.ClientID%>").value =="0") {
    //            document.getElementById('errMess').style.display = "block";
    //            document.getElementById('errorMessage').innerHTML = "Image 2 should be uploaded !";
    //            return false;
    //        }

    if (document.getElementById("<%=txtStartFromHr.ClientID%>").value == "" && false) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Start from Hour cannot be Blank !";
        ShowTab('packageTab1');
        return false;
    }

    if (document.getElementById("<%=txtStartTimeMin.ClientID%>").value == "" && false) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Start from Minute cannot be Blank !";
        ShowTab('packageTab1');
        return false;
    }

    if (document.getElementById("<%=txtEndToHr.ClientID%>").value == "" && false) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "End to Hour cannot be Blank !";
        ShowTab('packageTab1');
        return false;
    }

    if (document.getElementById("<%=txtEndTimeMin.ClientID%>").value == "" && false) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "End to Minute cannot be Blank !";
        ShowTab('packageTab1');
        return false;
    }


    var startHr = parseInt(document.getElementById("<%=txtStartFromHr.ClientID%>").value);
    //alert(startHr);     
    var startMin = parseInt(document.getElementById("<%=txtStartTimeMin.ClientID%>").value);
    var endHr = parseInt(document.getElementById("<%=txtEndToHr.ClientID%>").value);
    var endMin = parseInt(document.getElementById("<%=txtEndTimeMin.ClientID%>").value);


    if (startHr > 23 || endHr > 23) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Invalid Time!";
        document.getElementById("<%=txtDuration.ClientID%>").value = "Invalid Time!";
        ShowTab('packageTab1');
        return false;
    }
    else if (startMin > 59 || endMin > 59) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Invalid Time!";
        document.getElementById("<%=txtDuration.ClientID%>").value = "Invalid Time!";
        ShowTab('packageTab1');
        return false;
    }
    else if (startHr > endHr) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Start Time can not be later than End Time!";
        document.getElementById("<%=txtDuration.ClientID%>").value = "Start Time can not be later than End Time!";
        ShowTab('packageTab1');
        return false;
    }
    else if (startHr == endHr && startMin > endMin) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Start Time can not be later than End Time!";
        document.getElementById("<%=txtDuration.ClientID%>").value = "Start Time can not be later than End Time!";
        ShowTab('packageTab1');
        return false;
    }

    if (document.getElementById("<%=hdfSupplier.ClientID%>").value != 0) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = " Supplier Details in Edit Mode!";
        ShowTab('packageTab1');
        return false;
    }



    if (!ValidDate(document.getElementById('<%= txtFrom.ClientID %>').value)) {

        document.getElementById('errorMessage').innerHTML = "Please fill valid Available Start To date";
        document.getElementById('errMess').style.display = "block";
        ShowTab('packageTab4');
        return false;
    }


    if (document.getElementById("<%=txtTo.ClientID%>").value == "" || document.getElementById("<%=txtFromTime.ClientID%>").value == "") {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Invalid Available Start From !";
        ShowTab('packageTab4');
        return false;
    }

    changed = window.frames.mce_editor_2.document.body.innerHTML.replace(regexStr, '');
    var changed = changed.replace(regexStr1, '');
    var changed = changed.replace(regexStr2, '');

    if (trim(changed) == "") {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Please enter Itinerary details.";
        ShowTab('packageTab1');
        return false;
    }

    var exclValue = eval(document.getElementById('ExclusionsCurrent').value);// its value is +1 the current


    for (var i = 1; i < exclValue; i++) {
        //alert(i);
        if (Trim(document.getElementById('ExclusionsText-' + i).value) == "") {
            if (i == 1) {
                document.getElementById('errorMessage').innerHTML = "Please enter Exclusions for the Package.";
                document.getElementById('errMess').style.display = "block";
                ShowTab('packageTab5');
                return false;
            }
            else {
                document.getElementById('errorMessage').innerHTML = "Please fill all Exclusions for the Package.";
                document.getElementById('errMess').style.display = "block";
                ShowTab('packageTab5');
                return false;
            }
        }
    }


    var incValue = eval(document.getElementById('InclusionsCurrent').value);// its value is +1 the current

    for (var i = 1; i < incValue; i++) {

        if (Trim(document.getElementById('InclusionsText-' + i).value) == "") {
            if (i == 1) {
                document.getElementById('errorMessage').innerHTML = "Please enter Inclusions for Package.";
                document.getElementById('errMess').style.display = "block";
                ShowTab('packageTab5');
                return false;
            }
            else {
                document.getElementById('errorMessage').innerHTML = "Please fill all Inclusions for Package.";
                document.getElementById('errMess').style.display = "block";
                ShowTab('packageTab5');
                return false;
            }
        }
    }

    var incValue = eval(document.getElementById('CancellPolicyCurrent').value);// its value is +1 the current

    for (var i = 1; i < incValue; i++) {
        if (Trim(document.getElementById('CancellPolicyText-' + i).value) == "") {
            if (i == 1) {
                document.getElementById('errorMessage').innerHTML = "Please enter Cancellation Policy for Package.";
                document.getElementById('errMess').style.display = "block";
                ShowTab('packageTab5');
                return false;
            }
            else {
                document.getElementById('errorMessage').innerHTML = "Please fill all Cancellation Policies for Package.";
                document.getElementById('errMess').style.display = "block";
                ShowTab('packageTab5');
                return false;
            }
        }
    }

    if (document.getElementById("<%=hdfPackFlexDetails.ClientID%>").value != 0) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = " Package Flex Details in Edit Mode!";
        ShowTab('packageTab1');
        return false;
    }

    if (document.getElementById("<%=hdfPriceCount.ClientID%>").value == 0) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = " Price Details cannot be blank!";
        ShowTab('packageTab2');
        return false;
    }

    if (document.getElementById("<%=hdfPrice.ClientID%>").value != 0) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = " Price Details in Edit Mode!";

        ShowTab('packageTab2');
        return false;
    }
    if (document.getElementById("<%=chkCopy.ClientID%>").checked) {
        if (document.getElementById("<%=ddlPackages.ClientID%>").selectedIndex == 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Please select the Package for the package!";
            ShowTab('packageTab1');
            return false;
        }
        if (document.getElementById("<%=ddlAgentCopy.ClientID%>").selectedIndex == 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Please select the Copy to Agent for the package!";
            ShowTab('packageTab1');
            return false;
        }
    }

    if (!document.getElementById('ctl00_cphTransaction_chkAir').checked && !document.getElementById('ctl00_cphTransaction_chkHotel').checked && !document.getElementById('ctl00_cphTransaction_chkTours').checked && !document.getElementById('ctl00_cphTransaction_chkTransfer').checked && !document.getElementById('ctl00_cphTransaction_chkMeals').checked) {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Please select atleast one product from the list";
        ShowTab('packageTab1');
        return false;
    }


    if (Trim(document.getElementById('<%=CountryText1.ClientID %>').value) == "0" || Trim(document.getElementById('<%=CountryText1.ClientID %>').value) == "") {

        // document.getElementById('errorMessage').innerHTML = "Please select Country for the Fixed Departure.";
        //document.getElementById('errMess').style.display = "block";
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Please select Destination Country from the list .";
        ShowTab('packageTab1');
        return false;
    }
    else {
        document.getElementById('<%=hdfSelCountry.ClientID %>').value = document.getElementById('<%=CountryText1.ClientID %>').value;
    }

    var city = "";
    if (Trim(document.getElementById('<%=CityText1.ClientID %>').value) == "0" || Trim(document.getElementById('<%=CityText1.ClientID %>').value) == "") {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Please select Destination City from the list .";
        ShowTab('packageTab1');
        return false;
    }
    else {
        document.getElementById('<%=hdfSelCity.ClientID %>').value = document.getElementById('<%=CityText1.ClientID %>').value;
        city = document.getElementById('<%=CityText1.ClientID %>').value;
    }

    //Added by Venkatesh
    //We are checking the rest of the destination cities and countries here because they are created dynamically and their "Id's" cannot be checked on above
    if (parseInt(document.getElementById('<%=hdfNoofCountries.ClientID %>').value) > 1) {
        for (var c = 2; c <= parseInt(document.getElementById('<%=hdfNoofCountries.ClientID %>').value) ; c++) {
            if (document.getElementById('CountryText' + c) != null) {
                if (document.getElementById('CountryText' + c).value == "-1") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select all Destination Countries from the list.";
                    ShowTab('packageTab1');
                    return false;
                }
            }

            if (document.getElementById('CityText' + c) != null) {
                if (Trim(document.getElementById('CityText' + c).value) == "0" || Trim(document.getElementById('CityText' + c).value) == "") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select all Destination Cities from the list.";
                    ShowTab('packageTab1');
                    return false;
                }
            }

            var cityList = city.split(',');
            if (cityList != "" && cityList.length > 0) {
                //for (var c = 0; c < cityList.length; c++) {
                //}
                if (document.getElementById('CityText' + c) != null) {
                    if (cityList.indexOf(document.getElementById('CityText' + c).value) > -1) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Select Unique Destination Cities.";
                        ShowTab('packageTab1');
                        return false;
                    }
                }
            }
            if (document.getElementById('CityText' + c) != null) {
                city += ',' + document.getElementById('CityText' + c).value;
            }

        }
        for (var c = 2; c <= parseInt(document.getElementById('<%=hdfNoofCountries.ClientID %>').value) ; c++) {

            if (document.getElementById('CountryText' + c) != null && document.getElementById('CityText' + c) != null) {
                document.getElementById('<%=hdfSelCountry.ClientID %>').value += ',' + document.getElementById('CountryText' + c).value;
                document.getElementById('<%=hdfSelCity.ClientID %>').value += ',' + document.getElementById('CityText' + c).value;
            }
        }
    }

    //var availableFrom = document.getElementById('ctl00_cphTransaction_txtFrom').value.split('/');
    //var availableTo = document.getElementById('ctl00_cphTransaction_txtTo').value.split('/');
    //if (availableFrom != null && availableFrom.length > 0 && availableTo != null && availableTo.length > 0) {
    //    if (!IsValidDate(availableFrom[0], availableFrom[1], availableFrom[2]) && !IsValidDate(availableTo[0], availableTo[1], availableTo[2]))
    //    {
    //        var dateFrom = new Date(availableFrom[2], availableFrom[1], availableFrom[0]);
    //        var dateTo = new Date(availableTo[2], availableTo[1], availableTo[0]);
    //        if (dateFrom > dateTo)
    //        {
    //            document.getElementById('errMess').style.display = "block";
    //            document.getElementById('errorMessage').innerHTML = "Available To Date should be greater than or equal to Available From Date";
    //            ShowTab('packageTab4');
    //            return false;
    //        }

    //        var todayDate = new Date();
    //        if(todayDate )
    //    }
    //}
    document.getElementById('savePage').value = "true";
    document.forms[0].submit();

}

//    function Save() {
//        if (getElement('txtPackageName').value == '') addMessage('PackageName cannot be blank!', '');
//        if (getElement('txtStartingFrom').value == '') addMessage('StartingFrom cannot be blank!', '');

//        if (getElement('hdfPackFlexDetails').value != "0") addMessage('ACF Details in Edit Mode!', '');
//        if (getElement('hdfPrice').value != "0") addMessage('ACF Price in Edit Mode!', '');

//        if (getMessage() != '') {
//            alert(getMessage());
//            clearMessage(); return false;
//        }
//    }

//    function OnTimeExit(dcId) {
//        var rowId = dcId.substring(0, dcId.lastIndexOf('_'));
//        var svcRowId = rowId.substring(0, rowId.lastIndexOf('_'));
//        var id = rowId.split("_");
//        var inDate = "";
//        var outDate = "";
//        var unit = "";

//        if (id[4] == '_dcEndTo') {
//            inDate = GetDateTimeObject(svcRowId + '_dcStartFrom');
//            outDate = GetDateTimeObject(rowId);
//            unit = outDate - inDate;
//            var noOfDays = ((outDate.getTime() - inDate.getTime()) / (1000 * 60 * 60 * 24));
//            unit = Math.floor(noOfDays / 24);
//            document.getElementById(svcRowId + '_txtDuration').value = unit;
//        }

//    }
// 
 
  function AddTextBox(divName)
  {
  //alert(divName);
    var idCount = eval(document.getElementById(divName+'Counter').value);
    var idCurrent = eval(document.getElementById(divName+'Current').value);
    //alert(idCount);
    if(document.getElementById(divName+"-"+idCurrent))
    {
        document.getElementById(divName+"-"+idCurrent).style.display="block";
        document.getElementById(divName+"Img-"+idCurrent).style.display="block";
        Remove(divName+"Img-"+eval(idCurrent-1),'img');
        idCurrent++;
        document.getElementById(divName+'Current').value = idCurrent;
    }
    else
    {
    
        var stringHTML = "";
        var newdiv = document.createElement('div');
        if(divName == "Inclusions")
        {
            stringHTML += "<p class=\"fleft width-100 padding-top-5\" style=\"\" id=\""+divName+"-" + idCount + "\">";
        }
        if(divName == "CancellPolicy" || divName == "ThingsToBring" || divName == "Exclusions")
        {
            stringHTML += "<p class=\"fleft width-100 padding-top-5\" style=\"\" id=\""+divName+"-" + idCount + "\">";
        }
        stringHTML += "<span class=\"fleft width-300\"><input maxlength=\"4000\" onkeypress=\"return IsAlphaNumeric(event)\" type=\"text\" id=\""+divName+"Text-"+idCount+"\" name=\""+divName+"Text-"+idCount+"\" class=\"form-control\" /></span>";
        stringHTML += "<i class=\"fleft margin-left-10\" id=\"" + divName + "Img-" + idCount + "\"><img src=\"Images/delete.gif\" alt=\"Remove\" onclick=\"javascript:Remove('"+divName+"-"+idCount+"','text')\"/></i>";
        stringHTML += "</p>";
        newdiv.innerHTML = stringHTML;
        document.getElementById(divName+"Div").appendChild(newdiv)   
        //document.getElementById(divName+"Div").innerHTML += stringHTML;       
        Remove(divName+"Img-"+eval(idCount-1),'img');
        
        idCount++;
        document.getElementById(divName+'Counter').value = idCount;
        idCurrent++;
        document.getElementById(divName+'Current').value = idCurrent;
        
          if(divName == "Inclusions")
          {          
            document.getElementById("<%=hdfinclCounter.ClientID%>").value=idCount;           
          } 
          else if(divName == "Exclusions")
          {          
            document.getElementById("<%=hdfExclCounter.ClientID%>").value=idCount;           
          } 
           else if(divName == "CancellPolicy")
          {          
            document.getElementById("<%=hdfCanPolCounter.ClientID%>").value=idCount;           
          } 
          else if(divName == "ThingsToBring") 
          {
            document.getElementById("<%=hdfThingsCounter.ClientID%>").value=idCount;                  
          }
       
//        alert(document.getElementById('InclusionsCounter').value);
    }
  }
  
   function AddDateMore(divName)
  {  
    var idCount = eval(document.getElementById(divName+'Counter').value);
    var idCurrent = eval(document.getElementById(divName+'Current').value);   
    if(document.getElementById(divName+"-"+idCurrent))
    {
        document.getElementById(divName+"-"+idCurrent).style.display="block";
        document.getElementById(divName+"Img-"+idCurrent).style.display="block";
        Remove(divName+"Img-"+eval(idCurrent-1),'img');
        idCurrent++;
        document.getElementById(divName+'Current').value = idCurrent;
    }
    else
    {
    
        var stringHTML = "";
        var newdiv = document.createElement('div');
        if(divName == "UnAvailDates")
        {
            stringHTML += "<p class=\"fleft width-100 padding-top-5\" style=\"\" id=\""+divName+"-" + idCount + "\">";
        }







//        stringHTML += "<div class=\"input-group fleft\"><div class=\"input-group-addon\"><span class=\"glyphicon glyphicon-calendar\"></span></div><input type=\"text\" id=\"txt" + divName + "Text-" + idCount + "\" name=\"txt" + divName + "Text-" + idCount + "\" class=\"fleft form-control undatewidth\" /></div>";





        stringHTML += "<span class=\"fleft width-300\"><input type=\"text\" id=\"txt" + divName + "Text-" + idCount + "\" name=\"txt" + divName + "Text-" + idCount + "\" class=\"width-300 fleft form-control\" /></span>";





        stringHTML += "<i class=\"fleft margin-left-10\" id=\"" + divName + "Img-" + idCount + "\"><img src=\"Images/delete.gif\" alt=\"Remove\" onclick=\"javascript:Remove('" + divName + "-" + idCount + "','text')\"/></i>";


        stringHTML += "</p>";
        newdiv.innerHTML = stringHTML;        
        document.getElementById(divName+"Div").appendChild(newdiv)   
        //document.getElementById(divName+"Div").innerHTML += stringHTML;       
        Remove(divName+"Img-"+eval(idCount-1),'img');
        
        idCount++;
        document.getElementById(divName+'Counter').value = idCount;
        idCurrent++;
        document.getElementById(divName+'Current').value = idCurrent;
          if(divName == "UnAvailDates")
          {          
            document.getElementById("<%=hdfUnavailDateCounter.ClientID%>").value=idCount;           
          }          

    }
  }
  
//   function RemoveAll(id, count)
//  {
//  alert('hi');
// alert(id);
// alert(count);
////    for(var i=1; i<=count;i++)
////    {
////         document.getElementById(id+i).style.display="block";   
////    }
//        
//  }
   function Remove(id, val)
   {

 
    if(id.split('-')[1]!=1)
    {
        //Added by Lokesh on 7-Feb-2018 in order to clear the content inside the dynamic controls
        //While performing delete operation on the below controls
        //Exclusions,Inclusions,Cancellation Policy,Things to Bring.

        var controlId = id.split('-')[0] + 'Text' + '-' + id.split('-')[1];

        if (document.getElementById(controlId) != null) {
            document.getElementById(controlId).value = "";
        }
        document.getElementById(id).style.display = "none";
        //alert(id);
        if(val == "text")
        {
        
            var count = eval(id.split('-')[1]);
            var idSplit =  id.split('-')[0];
            //alert(id);
           
            if(eval(count-1) != 1)
            {
                document.getElementById(idSplit+"Img-"+eval(count-1)).style.display="block";
               
            }
            var idCurrent = eval(document.getElementById(id.split('-')[0]+'Current').value);
            idCurrent--;
            document.getElementById(id.split('-')[0]+'Current').value = idCurrent;
            
            if(idSplit == "Inclusions")
            {  
                document.getElementById("<%=hdfinclCounter.ClientID%>").value=idCurrent;           
            }  
            else if(idSplit == "Exclusions")    
            {
               document.getElementById("<%=hdfExclCounter.ClientID%>").value=idCurrent;                        
            } 
            else  if(idSplit == "CancellPolicy")    
            {             
                document.getElementById("<%=hdfCanPolCounter.ClientID%>").value=idCurrent;                        
            }
            else  if(idSplit == "ThingsToBring")    
            {             
                document.getElementById("<%=hdfThingsCounter.ClientID%>").value=idCurrent;                        
            }
            else if(idSplit == "UnAvailDates")    
            {            
                document.getElementById("<%=hdfUnavailDateCounter.ClientID%>").value=idCurrent;                        
            }              
           
        }
    }
  }
  
  function CalcDuration(hrOrMin)
  {
  
    var startHr= parseInt(document.getElementById("<%=txtStartFromHr.ClientID%>").value);
    //alert(startHr);     
    var startMin= parseInt(document.getElementById("<%=txtStartTimeMin.ClientID%>").value); 
    var endHr= parseInt(document.getElementById("<%=txtEndToHr.ClientID%>").value); 
    var endMin= parseInt(document.getElementById("<%=txtEndTimeMin.ClientID%>").value);
    

     if(startHr >23 || endHr >23)
     {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Invalid Time!";
        document.getElementById("<%=txtDuration.ClientID%>").value = "Invalid Time!";
        return false;
     }
     else if(startMin >59 || endMin >59)
     {
        document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Invalid Time!";
        document.getElementById("<%=txtDuration.ClientID%>").value = "Invalid Time!";
        return false;
     }
     else if(startHr > endHr)
     {    
         document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Start Time can not be later than End Time!";
        document.getElementById("<%=txtDuration.ClientID%>").value = "Start Time can not be later than End Time!";
        return false;
     }
     else if(startHr==endHr && startMin > endMin)
     {    
         document.getElementById('errMess').style.display = "block";
        document.getElementById('errorMessage').innerHTML = "Start Time can not be later than End Time!";
        document.getElementById("<%=txtDuration.ClientID%>").value = "Start Time can not be later than End Time!";
        return false;
     }
     else
     {
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";     
     }
       
    // alert(parseInt(endHr)+parseInt(startHr));
     var durationHr =  endHr-startHr  ;
     var durationMin = endMin - startMin;
     if(durationMin<0)
     {
        durationHr = durationHr -1
        durationMin =60+durationMin;
     }
     var duration = durationHr + 'Hrs ' + durationMin + 'Min';
    // alert(duration);   
    document.getElementById("<%=txtDuration.ClientID%>").value=duration;
    //document.getElementById("<%=txtDuration.ClientID%>").disabled=true;
  }
  
  function ValidateTime(hrOrMin,value,id)
  {        
        if(hrOrMin=='H' && value >23)
        {        
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Invalid Time!";
            document.getElementById(id).value="";
            return false;
        }
       else if(hrOrMin=='M' && value >59)
        {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Invalid Time!";
            document.getElementById(id).value="";
            return false;
        }
        else
        {
         document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
        }
  }
  
  function getTotalAmount(rowId, action) {
            var id = rowId.substring(0, rowId.lastIndexOf('_') + 1);
            //var quantity = parseFloat(isNaN(document.getElementById(id+'FTtxtQty').value));// document.getElementById(id+'FTtxtQty').value;
            var supplierCost = 0;
            var tax = 0;
            var markup = 0;
            var totalAmount=0;
            if (action == "I") {            
                supplierCost = parseFloat((isNaN(document.getElementById(id + 'FTtxtSupplierCost').value)|| document.getElementById(id + 'FTtxtSupplierCost').value=='')?'0' :document.getElementById(id + 'FTtxtSupplierCost').value);    
                tax = parseFloat((isNaN(document.getElementById(id + 'FTtxtTax').value)|| document.getElementById(id + 'FTtxtTax').value=='')?'0' :document.getElementById(id + 'FTtxtTax').value);                    
                markup = parseFloat((isNaN(document.getElementById(id + 'FTtxtMarkup').value)|| document.getElementById(id + 'FTtxtMarkup').value=='')?'0' :document.getElementById(id + 'FTtxtMarkup').value);                                   
               
                totalAmount =  supplierCost + tax + markup;
                document.getElementById(id + 'FTtxtAmount').value = totalAmount;
                document.getElementById(id + 'FTtxtAmount').value = totalAmount.toFixed(2);
                //alert(document.getElementById(id+'FTtxtRate').value);
            }
            else if (action == "U") {

               supplierCost = parseFloat((isNaN(document.getElementById(id + 'EITtxtSupplierCost').value)|| document.getElementById(id + 'EITtxtSupplierCost').value=='')?'0' :document.getElementById(id + 'EITtxtSupplierCost').value);    
                tax = parseFloat((isNaN(document.getElementById(id + 'EITtxtTax').value)|| document.getElementById(id + 'EITtxtTax').value=='')?'0' :document.getElementById(id + 'EITtxtTax').value);                    
                markup = parseFloat((isNaN(document.getElementById(id + 'EITtxtMarkup').value)|| document.getElementById(id + 'EITtxtMarkup').value=='')?'0' :document.getElementById(id + 'EITtxtMarkup').value);                              
                
                totalAmount = supplierCost + tax + markup;
                document.getElementById(id + 'EITtxtAmount').value = totalAmount;
                document.getElementById(id + 'EITtxtAmount').value = totalAmount.toFixed(2);


            }


        }
       
function restrictNumeric(fieldId,kind){
try{
return(maskNumeric(fieldId,(kind=='3'||kind=='4'?'false':'true'),(kind=='1'||kind=='3'?'true':'false')))}
catch(err){
showError(err,'Validation','restrictNumeric');return(false)}}
function maskNumeric(fieldId,ignoreNegative,IgnoreDecimal){
var key;var keychar
if(ignoreNegative==null)ignoreNegative='true'
if(IgnoreDecimal==null)IgnoreDecimal='true'
if(window.event){
if(navigator.appName.substring(0,1)=='M') key=window.event.keyCode
else key=window.event.charCode }
else if(event) key=event.which
else return true
keychar=String.fromCharCode(key)
if((key==null)||(key==0)||(key==8)||(key==9)||(key==13)||(key==27))return true
var strSet="0123456789"+(ignoreNegative=='true'?'':'-')+(IgnoreDecimal=='true'?'':'.')
if((strSet.indexOf(keychar)>-1)){
var inputbox=document.getElementById(fieldId)
if(ignoreNegative=='false'&&key==45){
if(inputbox.value.indexOf('-')==-1)inputbox.value='-'+inputbox.value
return(false)}
if(IgnoreDecimal=='false'&&inputbox.value.indexOf('.')>-1&&key==46)return(false)
return true}
return(false)}


function ValidDate(text)
{
    var textArray=text.split('/')
    if(textArray.length!=3 || textArray.length!=3)
    {
        return false;
    }
    if (!CheckValidDate(textArray[0], textArray[1], textArray[2]))
    {
        return false;
    }
    return true;
}
function showimag1(id,imgid,hdfid,hideid) {
//    var div = document.getElementById(id);
//    if (div.style.display == "block") {
//        div.style.display = "none";
//    }
//    else {
//        div.style.display = "block";;
//    }
    //    document.getElementById(imgid).src = document.getElementById(hdfid).value;
    window.open(document.getElementById(hdfid).value);
    return false;
}
 function showImage(stat,img)
        {       
         
            if(stat=="S")
            {   
                         
                //alert(document.getElementById('<%=pnlImages.ClientID %>'));
                document.getElementById('<%=pnlImages.ClientID %>').style.display="block";
                
                if(img=="1")  
                {                
                document.getElementById('<%=img1PreView.ClientID %>').style.display="block";
                document.getElementById('<%=img2PreView.ClientID %>').style.display="none";
                document.getElementById('<%=img3PreView.ClientID %>').style.display="none";
                }
                else if(img=="2")
                {
                document.getElementById('<%=img1PreView.ClientID %>').style.display="none";
                document.getElementById('<%=img2PreView.ClientID %>').style.display="block";
                document.getElementById('<%=img3PreView.ClientID %>').style.display="none";
                }
                else 
                {
                document.getElementById('<%=img1PreView.ClientID %>').style.display="none";
                document.getElementById('<%=img2PreView.ClientID %>').style.display="none";
                document.getElementById('<%=img3PreView.ClientID %>').style.display="block";
                }
            }
            else
            {
                document.getElementById('<%=pnlImages.ClientID %>').style.display="none";
                document.getElementById('<%=img1PreView.ClientID %>').style.display="none";
                document.getElementById('<%=img2PreView.ClientID %>').style.display="none";
                document.getElementById('<%=img3PreView.ClientID %>').style.display="none";
                return false;
            }
            
        }


        


       

        function ShowTab(val) {
            //alert(val);
            $('a[href="#'+val+'"]').tab('show')

        }
//adding unavail dates
        function AddDetails(type) {
           if (validateDuplicate(type,-5)) {
            var UnAvailFrom = "";
            var displayValue = "";
            var UnAvailTo = "";
            var detailId = 0;
            var opt = document.createElement("option");
            //alert(getElement('txtUnavailFrom'));
            //if (type == "UA") {
            if(getElement('txtFrom').value != '' && getElement('txtTo').value != ''){
                if (getElement('txtUnavailFrom').value != '' && getElement('txtUnavailTo').value != '') {
                    //var ddlHtl = getElement("ddlHotel");
                    var fromDate = getElement('txtFrom').value.split('/');
                    var toDate = getElement('txtTo').value.split('/');
                     UnavailFrom = getElement('txtUnavailFrom').value.split('/');              //ddlHtl.options[ddlHtl.selectedIndex].innerHTML;
                        //displayValue = getElement("txtCCcardNo").value;
                        UnAvailTo = getElement('txtUnavailTo').value.split('/');
                        
                        
                        var availableFrom = new Date();
                        availableFrom.setFullYear(fromDate[2], (fromDate[1] - 1), fromDate[0]);
                        var availableTo = new Date();
                        availableTo.setFullYear(toDate[2], (toDate[1]-1), toDate[0]);
                        var unavailableFrom = new Date();
                        unavailableFrom.setFullYear(UnavailFrom[2], (UnavailFrom[1]-1), UnavailFrom[0]);
                        var unavailableTo = new Date();
                        unavailableTo.setFullYear(UnAvailTo[2], (UnAvailTo[1]-1), UnAvailTo[0]);
                        
                        if(unavailableFrom <= unavailableTo){
                            if(availableFrom <= unavailableFrom && availableTo >= unavailableTo ){
                               UnavailFrom = UnavailFrom[0] +'/'+UnavailFrom[1]+'/'+UnavailFrom[2];
                               UnAvailTo = UnAvailTo[0]+'/'+UnAvailTo[1]+'/'+UnAvailTo[2];
                                detailId = getElement('hdfUAId').value;
                                //alert(detailId);
                                opt.text = UnavailFrom + '--' + UnAvailTo;
                                opt.value = detailId + '||' + UnavailFrom + '||' + UnAvailTo;
                               
                                getElement("lstUA").options.add(opt);

                                getElement("txtUnavailFrom").value = '';
                                getElement('txtUnavailTo').value = '';
                                var unavailableDays = 0;
                                
                                unavailableDays = parseInt(getElement('hdnUnavailableDays').value);
                                getElement('hdnUnavailableDays').value = unavailableDays + 1;
                                getElement('txtUnavailableDays').value = unavailableDays + 1;
                            }
                            else{
                                alert('Unavailable From / Unavailable To should be in between the AvailableFrom and AvailableTo');
                            }
                      }
                      else{
                            alert('Unavailable From should be less than or equal to Unavailable To');
                      }
                }
                else {
                    alert('Unavailable From / Unavailable To cannot be blank');
                }
            }
            else{
                alert('AvailableFrom / AvailableTo Dates cannot be blank');
            }
                BindDetails(type);
                return false;
           }
           return false;
        }

        function BindDetails(type) {
            var listBox = ""
            var texts = "";
            var values = "";
            //if (type == "UA") {
                var listBox = getElement("lstUA");
                for (var i = 0; i < listBox.options.length; i++) {
                    texts += listBox.options[i].innerHTML + "#&#";
                    values += listBox.options[i].value + "#&#";
                }
                getElement('hdfUADisplayText').value = texts;
                getElement('hdfUADisplayValue').value = values;
               // alert(getElement('hdfUADisplayText').value);
            //}
        }

        function removeItem(type) {
            var listControl = "";
            var deletedItems = ""

            if (type == "UA") {
                listControl = getElement("lstUA");
                var optionsList = '';
                if (listControl.value.length > 0) {
                    var itemIndex = listControl.selectedIndex;
                    if (itemIndex >= 0) {
                        deletedItems += listControl.options[itemIndex].value + "&&";
                        listControl.remove(itemIndex);
                       getElement('txtUnavailFrom').value = "";
                    getElement('txtUnavailTo').value = "";
                    }
                    getElement('hdfUADeleted').value = deletedItems;
                    
                    var unavailableDays = 0;
                    unavailableDays = parseInt(getElement('hdnUnavailableDays').value);
                    getElement('hdnUnavailableDays').value = unavailableDays - 1;
                    getElement('txtUnavailableDays').value = unavailableDays - 1;
                }
                else {
                    alert('Please select an item');
                    listControl.focus();
                    //listFF.select();
                }
            }
            BindDetails(type);
            return false;
        }

        function EditItems(type) {
            var listControl = "";
            var values = "";
            var itemIndex = "";
            if (type == "UA") {
                listControl = getElement("lstUA");
                itemIndex = listControl.selectedIndex;
                if (itemIndex >= 0) {
                    values = listControl.options[itemIndex].value;
                    var detailsVal = values.split('||');
                    getElement('hdfUAId').value = detailsVal[0];
                    //alert(getElement('hdfUAId').value);
                    getElement('txtUnavailFrom').value = detailsVal[1];
                    getElement('txtUnavailTo').value = detailsVal[2];
                    getElement('hdfDetailsIndexUA').value = itemIndex;
                   //alert(getElement('hdfDetailsIndexUA').value);
                }
                else {
                    alert('Please select an item');
                    listControl.focus();
                }
            }

            return false;
        }

        function UpdateItems(type) {
            var UnAvailFrom = "";
            //var displayValue = "";
            var UnAvailTo = "";
            var detailId = 0;

            var opt = document.createElement("option");
            var updateIndex = "-1";
            updateIndex = getElement('hdfDetailsIndexUA').value;

            if (updateIndex >= 0) {
                if (validateDuplicate(type, updateIndex)) {
                    if (type == "UA") {
                        // var opt = document.createElement("option");
                        UnAvailFrom = getElement("txtUnavailFrom").value;
                        UnAvailTo = getElement('txtUnavailTo').value;
                        detailId = getElement('hdfUAId').value;
                        opt.text = UnAvailFrom + '--' + UnAvailTo; ;
                        opt.value = detailId + '||' + UnAvailFrom + '||' + UnAvailTo;
                        //alert(opt.value); 
                        getElement("lstUA").options[updateIndex] = new Option(opt.text, opt.value, false, false);
                        //ddlHtl.selectedIndex = 0;
                        getElement('txtUnavailFrom').value = '';
                        getElement('txtUnavailTo').value = '';
                        getElement('hdfDetailsIndexUA').value = "-1";
                    }
                }
                //return false;
            }
            else {
                alert('No item selected to update');
                //listControl.focus();
                return false;
            }
            BindDetails(type);
            return false;

        }
        
        function validateDuplicate(type, index) {

            var UnavailFrom = "";
            var displayValue = "";
            var UnavailTo = "";
            var detailId = 0;
            var listBox = "";
            var listBoxText = "";
            var opt = document.createElement("option");
            if (type == "UA") {
                if (getElement('txtUnavailFrom').value != '' && getElement('txtUnavailTo').value != '') {
                    //var ddlHtl = getElement("ddlHotel");

                    UnavailFrom = getElement("txtUnavailFrom").value;              //ddlHtl.options[ddlHtl.selectedIndex].innerHTML;
                    //displayValue = getElement("txtCCcardNo").value;
                    UnavailTo = getElement('txtUnavailTo').value;
                    detailId = getElement('hdfUAId').value;
                    opt.text = UnavailFrom + '--' + UnavailTo;;
                    listBox = getElement("lstUA");
                    for (var i = 0; i < listBox.options.length; i++) {

                        listBoxText = listBox.options[i].innerHTML;

                        if (opt.text == listBoxText) {
                            alert('Unavail From and Unavail To combinations already exists');
                            return false;
                        }
                    }

                }

            }
            return true;
        }

        function chkvalidate() {

            if (document.getElementById("<%=chkCopy.ClientID%>").checked) {
                document.getElementById('errMess').style.display = "none";
                if (document.getElementById("<%= ddlAgent.ClientID %>").selectedIndex <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select a Agent for hotel deal!";
                    return false;
                }
                document.getElementById("<%=divCopyToAgent.ClientID %>").style.display="block";
                document.getElementById("<%= lblPackage.ClientID %>").style.display = "block";
                document.getElementById("<%= ddlPackages.ClientID %>").style.display = "block";
                document.getElementById("<%= lblcopyAgent.ClientID %>").style.display = "block";
                document.getElementById("<%= ddlAgentCopy.ClientID %>").style.display = "block";
            }
            else {
            document.getElementById("<%=divCopyToAgent.ClientID %>").style.display="none";
                document.getElementById("<%= lblPackage.ClientID %>").style.display = "none";
                document.getElementById("<%= ddlPackages.ClientID %>").style.display = "none";
                document.getElementById("<%= lblcopyAgent.ClientID %>").style.display = "none";
                document.getElementById("<%= ddlAgentCopy.ClientID %>").style.display = "none";
            }

        }

        //var countryId = 0;
        function addCountry(id) {
            //countryId = id;
            BindCountriesDet(id);
            //PageMethods.getCountryNames(onComplete);

        }
        function onComplete(result)
        {
            BindCountriesDet(countryId);
        }

        function BindCountriesDet(id){
            id = parseInt(parseInt(id) + 1);
            var result = document.getElementById('<%=hdfCountryList.ClientID%>').value;
            if (result != null && result != "" && id > 0) {
                var countryList = result.split('|');

                var div = document.getElementById('divCountries');
                
                //Main Div
                var mainDiv = document.createElement('div');
                mainDiv.setAttribute('id', 'divCountry' + id);
                mainDiv.setAttribute('name', 'divCountry' + id);

                //Country DropDown
                var divCountry = document.createElement('div');
                divCountry.setAttribute('class', 'col-md-3');

                var subDiv = document.createElement('div');
                subDiv.setAttribute('class', 'form-group');

                var lblCountry = document.createElement('label');
                lblCountry.setAttribute('id', 'lblCountry' + id);
                lblCountry.innerHTML = "Destination Country:<span style='color:red'>*</span>";

                var ddlCountry = document.createElement('select');
                ddlCountry.className = "form-control";
                ddlCountry.setAttribute('id', 'CountryText' + id);
                ddlCountry.setAttribute('onchange', 'LoadCities(' + id + ')');
                ddlCountry.name = "CountryText" + id;
                
                for (var i = 0; i < countryList.length; i++)
                {
                    var opt = document.createElement('option');
                    var keyValue = countryList[i].split(',');
                    if (i == 0) {
                        opt.value = -1;
                        opt.innerHTML = "Select Country";
                        opt.setAttribute("selected", "selected");
                        ddlCountry.appendChild(opt);
                    }
                    else {
                        opt.value = keyValue[1];
                        opt.innerHTML = keyValue[0];
                        ddlCountry.appendChild(opt);
                    }
                }

                //City DropDown
                var divCity = document.createElement('div');
                divCity.setAttribute('class', 'col-md-3');

                var subDivCity = document.createElement('div');
                subDivCity.setAttribute('class', 'form-group');

                var lblCity = document.createElement('label');
                lblCity.setAttribute('id', 'lblCity' + id);
                lblCity.setAttribute('name', 'lblCity' + id);
                lblCity.innerHTML = "Destination City:<span style='color:red'>*</span>";

                var ddlCity = document.createElement('select');
                ddlCity.className = "form-control";
                ddlCity.setAttribute('id', 'CityText' + id);
                ddlCity.setAttribute('name', 'CityText' + id);
                ddlCity.setAttribute('onchange', 'SetCity(this.id,'+id+')');
                //ddlCity.name = "ddlCity" + id;

                var opt = document.createElement('option');
                opt.value = -1;
                opt.innerHTML = "Select City";
                opt.setAttribute("selected", "selected");
                ddlCity.appendChild(opt);

                //Adding Button 
                var divBtn = document.createElement('div');
                divBtn.setAttribute('class', 'col-md-3 marbot_20');

                var btn = document.createElement('a');
                btn.setAttribute('href', 'javascript:void(0)');
                btn.setAttribute('id', 'btnAddCountry'+id);
                btn.setAttribute('name', 'btnAddCountry'+id);
                btn.setAttribute('onclick', 'javascript:addCountry(' + id + ')');
                btn.innerHTML = "Add More";

                var btnSpan = document.createElement('span');
                btnSpan.setAttribute('class', ' glyphicon glyphicon-plus-sign');
                
                btn.appendChild(btnSpan);
                divBtn.appendChild(btn);

                //Removing Button 
                var divRemoveBtn = document.createElement('div');
                divRemoveBtn.setAttribute('class', 'col-md-3 marbot_20');

                var removeBtn = document.createElement('i');
                removeBtn.setAttribute('class', 'fleft margin-left-10');
                removeBtn.setAttribute('id', 'btnRemove' + id);
                removeBtn.setAttribute('name', 'btnRemove' + id);
                removeBtn.setAttribute('onclick', 'javascript:RemoveCountry(' + id + ')');
                removeBtn.style.cursor = "pointer";
                removeBtn.innerHTML = "Remove";

                //var btnRemoveImg = document.createElement('img');
                //btnRemoveImg.setAttribute('src', 'Images/delete.gif');
                //btnRemoveImg.setAttribute('alt', 'Remove');
                //btnRemoveImg.setAttribute('onclick','javascript:RemoveCountry('+id+')');

                var divClear = document.createElement('div');
                divClear.setAttribute('class', 'clearfix');

                //removeBtn.appendChild(btnRemoveImg);
                divRemoveBtn.appendChild(removeBtn);

                subDiv.appendChild(lblCountry);
                subDiv.appendChild(ddlCountry);
                divCountry.appendChild(subDiv);

                subDivCity.appendChild(lblCity);
                subDivCity.appendChild(ddlCity);
                divCity.appendChild(subDivCity);

                mainDiv.appendChild(divCountry);
                mainDiv.appendChild(divCity);
                mainDiv.appendChild(divBtn);
                mainDiv.appendChild(divRemoveBtn);
                mainDiv.appendChild(divClear);

                div.appendChild(mainDiv);
                
                
                document.getElementById('<%=hdfNoofCountries.ClientID %>').value = id;

                //Here we are reducing the 'id' value by 1 because we are adding 1 value at the top.
                id = parseInt(id - 1);
                if (id > 0) {
                    document.getElementById('btnAddCountry' + id).style.display = "none";
                }
            }
        }


        //used for startup script method and BindingpostBack controls
        function BindCountries()
        {
            var noofCountries = document.getElementById('<%=hdfNoofCountries.ClientID %>').value;
            var selCountries = "";
            if (document.getElementById('<%=hdfSelCountry.ClientID %>').value != null && document.getElementById('<%=hdfSelCountry.ClientID %>').value != "") {
                selCountries = document.getElementById('<%=hdfSelCountry.ClientID %>').value.split(',');
            }
            var selCities = "";
            if(document.getElementById('<%=hdfSelCity.ClientID %>').value!=null && document.getElementById('<%=hdfSelCity.ClientID %>').value != "")
            {
                selCities = document.getElementById('<%=hdfSelCity.ClientID %>').value.split(',');
            }

            if (noofCountries > 1)
            {
                document.getElementById('divCountries').value = "";
                //Here we are starting with 1 because we are incrementing the value in the "BindCountriesDet()"
                for (var i = 1; i < noofCountries; i++)
                {
                    BindCountriesDet(i);
                }
            }

            if (selCountries != null && selCountries != "" && selCountries.length > 0)
            {
                for (var cou = 0; cou < selCountries.length; cou++)
                {
                    var country = selCountries[cou].split('-');
                    if (country != null && country.length > 1) {
                        if (country[0] == "1") {
                            document.getElementById('<%=CountryText1.ClientID %>').value = country[1];
                            LoadCities(1);
                        }
                        else {
                            document.getElementById('CountryText' + country[0]).value = country[1];
                            LoadCities(country[0]);
                        }
                        
                    }
                }
            }

            if (selCities != null && selCities != "" && selCities.length > 0)
            {
                for (var city = 0; city < selCities.length; city++)
                {
                    if (selCities[city] != "") {
                        var cityDet = selCities[city].split('-');
                        if (cityDet != null && cityDet.length > 1) {
                            if (cityDet[0] == "1") {
                                document.getElementById('<%=CityText1.ClientID %>').value = cityDet[1];
                            }
                            else {
                                if (document.getElementById('CityText' + cityDet[0]) != null) {
                                    document.getElementById('CityText' + cityDet[0]).value = cityDet[1];
                                }
                            }
                        }
                    }
                }
            }
        }

        function RemoveCountry(id)
        {
            var noofCountries = document.getElementById('<%=hdfNoofCountries.ClientID %>').value;
            var countryList = document.getElementById('divCountries');
            //if (id <= parseInt(noofCountries))
            {
                var dynId = 2;//Dynamic dropdowns of the country id start from "2";
                document.getElementById('<%=hdfSelCountry.ClientID %>').value = "";
                document.getElementById('<%=hdfSelCity.ClientID %>').value = "";
                for (var i = 1; i <= parseInt(noofCountries) ; i++) {
                    if (id != i) {
                        if (i == 1) {
                            if (document.getElementById('ctl00_cphTransaction_CountryText1') != null && document.getElementById('ctl00_cphTransaction_CountryText1').value != "0") {
                                //assiging selcted values to hidden filds to retain the value
                                if (document.getElementById('<%=hdfSelCountry.ClientID %>').value == "" || document.getElementById('<%=hdfSelCountry.ClientID %>').value.length==0) {
                                    document.getElementById('<%=hdfSelCountry.ClientID %>').value = '1-' + document.getElementById('ctl00_cphTransaction_CountryText1').value;
                                    document.getElementById('<%=hdfSelCity.ClientID %>').value = '1-' + document.getElementById('ctl00_cphTransaction_CityText1').value;
                                }
                                else {
                                    document.getElementById('<%=hdfSelCountry.ClientID %>').value += ',1-' + document.getElementById('ctl00_cphTransaction_CountryText1').value;
                                    document.getElementById('<%=hdfSelCity.ClientID %>').value += ',1-' + document.getElementById('ctl00_cphTransaction_CityText1').value;
                                }
                                // alert(document.getElementById('<%=hdfSelCountry.ClientID %>').value);
                            }
                        }
                        else {
                            if (document.getElementById('CountryText' + i) != null) {
                                if (document.getElementById('CountryText' + i).value != "-1") {
                                    //assiging selcted values to hidden filds to retain the value
                                    if (document.getElementById('<%=hdfSelCountry.ClientID %>').value == "") {
                                        document.getElementById('<%=hdfSelCountry.ClientID %>').value = dynId + '-' + document.getElementById('CountryText' + i).value;
                                        document.getElementById('<%=hdfSelCity.ClientID %>').value = dynId + '-' + document.getElementById('CityText' + i).value;
                                    }
                                    else {
                                        document.getElementById('<%=hdfSelCountry.ClientID %>').value += ',' + dynId + '-' + document.getElementById('CountryText' + i).value;
                                        document.getElementById('<%=hdfSelCity.ClientID %>').value += ',' + dynId + '-' + document.getElementById('CityText' + i).value;
                                    }
                                    dynId = parseInt(dynId + 1);
                                }
                            }

                        }
                    }
                }

                countryList.removeChild(document.getElementById('divCountry' + id));
                if (countryList.children.length == 0) {
                    document.getElementById('btnAddCountry1').style.display = 'block';
                    document.getElementById('<%=hdfNoofCountries.ClientID %>').value = "";

                }
                else {
                    document.getElementById('btnAddCountry1').style.display = 'none';
                    document.getElementById('<%=hdfNoofCountries.ClientID %>').value = countryList.children.length;


                    var ctrlId = document.getElementById("divCountries").lastElementChild.getAttribute('id').replace('divCountry','');
                    //alert(ctrlId);
                    if (document.getElementById('btnAddCountry' + ctrlId) != null) {
                        document.getElementById('btnAddCountry' + ctrlId).style.display = 'block';
                    }

                }
                //if (id < parseInt(noofCountries)) {
                //    //reAssigningIds(id, noofCountries, countryList);
                //}
                //else {
                //    document.getElementById('btnAddCountry' + parseInt(id-1)).style.display = "block";
                //}
               // document.getElementById('<%=hdfNoofCountries.ClientID %>').value = parseInt(noofCountries - 1);
            }
        }

        function reAssigningIds(id, noofCountries, countryList)
        {
            if (id > 1)//Always the default dropdown 
            {
                alert(id);
                alert(noofCountries);
                alert(countryList);

                for (var i = 2; i <= noofCountries; i++) {

                    if (i > parseInt(id) && i != parseInt(id)) {
                        var countryDet = document.getElementById('divCountry' + i);
                        console.log(countryDet);

                        countryDet.setAttribute('id', 'divCountry' + parseInt(i - 1));
                        countryDet.setAttribute('name', 'divCountry' + parseInt(i - 1));
                        if (countryDet.childNodes.length > 0) {

                            console.log(countryDet.childNodes.length);
                            for (var i = 0; i < countryDet.childNodes.length; i++)
                            {
                                console.log(countryDet.childNodes[i]);
                            }

                            var divCountry = countryDet.childNodes[0]; 
                            var selcountry = null;
                            var divCity = countryDet.childNodes[1];
                            var selCity = null;
                            var divAddBtn = countryDet.childNodes[2];
                            var divRemoveBtn = countryDet.childNodes[3];

                            //Changing the id's of Country DropDown
                            if (divCountry.childNodes.length > 0) {

                                console.log(divCountry.childNodes.length);
                                for (var i = 0; i < divCountry.childNodes.length; i++) {
                                    console.log(divCountry.childNodes[i]);
                                }


                                var ctrlCountry = divCountry.childNodes[0];
                                if (ctrlCountry.childNodes.length > 0) {

                                    console.log(ctrlCountry.childNodes.length);
                                    for (var i = 0; i < ctrlCountry.childNodes.length; i++) {
                                        console.log(ctrlCountry.childNodes[i]);
                                    }

                                    var lblCountry = ctrlCountry.childNodes[0];
                                    lblCountry.setAttribute('id', 'lblCountry' + parseInt(i - 1));
                                    lblCountry.setAttribute('name', 'lblCountry' + parseInt(i - 1));

                                    var ddlCountry = ctrlCountry.childNodes[1];
                                    ddlCountry.setAttribute('id', 'CountryText' + parseInt(i - 1));
                                    ddlCountry.setAttribute('name', 'CountryText' + parseInt(i - 1));
                                    ddlCountry.setAttribute('onchange', 'Load(' + parseInt(i - 1) + ')');
                                    ddlCountry.value = selcountry;

                                    //var destCountry = ctrlCountry.childNodes[2];
                                    //destCountry.setAttribute('id', 'CountryText' + parseInt(i - 1));
                                    //destCountry.setAttribute('name', 'CountryText' + parseInt(i - 1));
                                    //destCountry.setAttribute('onchange', 'Load(' + parseInt(i - 1) + ')');
                                    //destCountry.value = selcountry;
                                }
                            }

                            //Changing the id's of City DropDown
                            if (divCity.childNodes.length > 0) {
                                var ctrlCity = divCity.childNodes[0];
                                if (ctrlCity.childNodes.length > 0) {
                                    var lblCity = ctrlCity.childNodes[0];
                                    lblCity.setAttribute('id', 'lblCity' + parseInt(i - 1));
                                    lblCity.setAttribute('name', 'lblCity' + parseInt(i - 1));

                                    var ddlCity = ctrlCity.childNodes[1];
                                    ddlCity.setAttribute('id', 'CityText' + parseInt(i - 1));
                                    ddlCity.setAttribute('name', 'CityText' + parseInt(i - 1));
                                    ddlCity.setAttribute('onchange', 'SetCity(this.id,' + parseInt(i - 1) + ')');
                                    ddlCity.value = selCity;
                                    //ddlCity.setAttribute('onchange', 'SetCity(this.id,' + parseInt(i - 1) + ')');

                                    //var destCity = ctrlCity.childNodes[2];
                                    //destCity.setAttribute('id', 'CityText' + parseInt(i - 1));
                                    //destCity.setAttribute('name', 'CityText' + parseInt(i - 1));
                                    //destCity.setAttribute('onchange', 'SetCity(this.id,' + parseInt(i - 1) + ')');
                                    //destCity.value = selCity;
                                }
                            }

                            //Changing the id's of Add Button
                            if (divAddBtn.childNodes.length > 0) {
                                var ctrlAddBtn = divAddBtn.childNodes[0];
                                ctrlAddBtn.setAttribute('id', 'btnAddCountry' + parseInt(i - 1));
                                ctrlAddBtn.setAttribute('name', 'btnAddCountry' + parseInt(i - 1));
                            }

                            //Changing the id's of Remove Button
                            if (divRemoveBtn.childNodes.length > 0) {
                                var ctrlRemoveBtn = divRemoveBtn.childNodes[0];
                                ctrlRemoveBtn.setAttribute('id', 'btnRemove' + parseInt(i - 1));
                                ctrlRemoveBtn.setAttribute('name', 'btnRemove' + parseInt(i - 1));
                                ctrlRemoveBtn.setAttribute('onclick', 'RemoveCountry(' + parseInt(i - 1)+')');
                            }

                        }
                    }
                }
            }
        }
        
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
</script>

   
   <%-- <script type="text/javascript">

        function CheckSize() {
         
            var editor_id = tinyMCE.selectedInstance.formTargetElementId;
            console.log("editor_id" + editor_id);
           
            var Limit = 10;
            console.log("Limit" + Limit);
          
            var x = tinyMCE.getContent();
            console.log(x);
            console.log(x.length);

            if (x.length > Limit) {
                console.log("Inside if block");

                alert('You have reached the maximum size for this field');           
            };
         
        }



        
    </script>--%>


    <asp:HiddenField runat="server" ID="hdfImage1" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage2" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage3" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage1Extn" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage2Extn" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage3Extn" Value="0" />
    <asp:HiddenField runat="server" ID="hdfUploadYNImage1" Value="0" />
    <asp:HiddenField runat="server" ID="hdfUploadYNImage2" Value="0" />
    <asp:HiddenField runat="server" ID="hdfUploadYNImage3" Value="0" />
    <asp:HiddenField runat="server" ID="hdfCountries" Value="" />
    <asp:HiddenField runat="server" ID="hdfCities" Value="" />
    <asp:HiddenField runat="server" ID="hdfSelCountry" Value="" />
    <asp:HiddenField runat="server" ID="hdfSelCity" Value="" />
    
    <asp:HiddenField runat="server" ID="hdfOriginCountries" Value="" />
    <asp:HiddenField runat="server" ID="hdfOriginCities" Value="" />
    <asp:HiddenField runat="server" ID="hdfOriginSelCountry" Value="" />
    <asp:HiddenField runat="server" ID="hdfOriginSelCity" Value="" />
    <asp:HiddenField runat="server" ID="hdfPackId" Value="0" />
    
    <asp:HiddenField  id="hdfUAId" runat="server" Value="0"/>
    <asp:HiddenField  id="hdfDetailsIndexUA" runat="server" Value="-1"/>
    <asp:HiddenField  id="hdfUADisplayText" runat="server" Value=""/>
    <asp:HiddenField  id="hdfUADisplayValue" runat="server" Value=""/>
    <asp:HiddenField  id="hdfUADeleted" runat="server" Value=""/>
    <asp:HiddenField runat="server" ID="hdfAgencyImagePath" Value="" />
    <asp:HiddenField runat="server" ID="hdnUnavailableDays" Value="0" />
    <%--//Added by venkatesh--%>
    <asp:HiddenField ID="hdfCountryList" runat="server" Value="" />
    <asp:HiddenField ID="hdfNoofCountries" runat="server" Value="" />


    <input type="hidden" id="savePage" name="savePage" value="false" />
    <asp:Label runat="server" Style="color: Red" ID="lblError" Visible="false" Text=""></asp:Label>
    <%--<div id="errMess" class="error_module" style="display:none;"> <div id="errorMessage" style="float:left; color:Red;" class="padding-5 yellow-back width-100 center margin-top-5"> </div></div>--%>
   
    <div class="clear" style="margin-left: 30px">
        <div id="container1" style="position: absolute; top: 990px; left: 10%; z-index:9999; display: none;">
        </div>
    </div>
    
    
    <div class="clear" style="margin-left: 30px">
        <div id="container3" style="position: absolute; top: 240px; left: 10%; z-index:9999; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container5" style="position: absolute; top: 220px; left: 215px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container7" style="position: absolute; top: 220px; left: 215px; display: none;z-index:111">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 252px; left: 215px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container4" style="position: absolute; top: 240px; left: 23%; z-index:9999; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container6" style="position: absolute; top: 252px; left: 215px; display: none;">
        </div>
    </div>
   
   
    <div class="clear" style="margin-left: 30px">
        <div id="container8" style="position: absolute; top: 252px; left: 215px; display: none;z-index:111">
        </div>
        
        
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="callContainer" style="position: absolute; top: 300px;  left: 10%; z-index:9999; display: none;">
        </div>
    </div>
    
    
    <div class="body_container">

          <h2>Create Package</h2>
          
          <div class="tabbed-panel">

              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li id="tabTourDetails" role="presentation" class="active"><a href="#packageTab1" aria-controls="home" role="tab" data-toggle="tab">Tour Details</a></li>
                <li id="tabPriceDetails" role="presentation"><a href="#packageTab2" aria-controls="settings" role="tab" data-toggle="tab">Price Details</a></li>
                <li id ="tabGallery" role="presentation"><a href="#packageTab3" aria-controls="profile" role="tab" data-toggle="tab">Gallery</a></li>
                <li id="tabAvailableDates" role="presentation"><a href="#packageTab4" aria-controls="messages" role="tab" data-toggle="tab">Available Dates</a></li>
                <li id="tabInclusions" role="presentation"><a href="#packageTab5" aria-controls="settings" role="tab" data-toggle="tab">Inclusions / Exclusions</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content rb-form">

              <asp:Label ID="lblMessage" runat="server" Font-Bold="true" Width="250"></asp:Label>
                <div role="tabpanel" class="tab-pane active rb-container-wrap" id="packageTab1">
                    <div class="form">
                          <div class="row">
                              
                        <div class="col-md-4">
                              <div class="form-group">
                                <label><asp:Label ID="lblCulture" runat="server" Text="Culture:"> </asp:Label><span class="req-star" style="color:red">*</span></label>
                                  <asp:UpdatePanel ID="up_culture" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlCulture" runat="server" CssClass=" form-control">
                                            <asp:ListItem Text="ENGLISH" Value="EN"></asp:ListItem>
                                            <asp:ListItem Text="ARABIC" Value="AR"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="ddlCulture" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                               </div>
                           </div>
                              <div class="clearfix"></div>
                          </div>
                       <div class="row">

                        <div class="col-md-4">
                              <div class="form-group">
                                <label><asp:Label ID="lblAgent" runat="server" Text="Agent:"> </asp:Label><span class="req-star" style="color:red">*</span></label>
                                  <asp:UpdatePanel ID="UP_ddlAgent" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlAgent" runat="server" CssClass=" form-control" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" AutoPostBack="true">  </asp:DropDownList>
                                    </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="ddlAgent" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                               </div>
                           </div>

                           <div class="col-md-8">
                               <div class="col-md-2 custom-checkbox-table"> 
                                   <label id="lblCheckBox"></label><br />
                                   <asp:CheckBox ID="chkCopy" Font-Bold="true" runat="server" Text="Copy" onclick="return chkvalidate();" />
                                   <div class="clearfix"></div>
                               </div>
                               <br />
                           </div>
                          <div class="clearfix"></div>
                           <div class="col-md-12 padding-0 marbot_10" id="divCopyToAgent" runat="server" style="display:none">                   
    
    
        
    <div class="col-md-4"> 
        <asp:Label ID="lblPackage" runat="server" Text="Package" style="display:none;"></asp:Label>
    <asp:UpdatePanel ID="UP_ddlPackages" runat="server">
  <ContentTemplate>
    <asp:DropDownList ID="ddlPackages" runat="server" CssClass="form-control" style="display:none;" OnSelectedIndexChanged="ddlPackages_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
     </ContentTemplate>
  <Triggers>
  <asp:PostBackTrigger ControlID="ddlPackages" />
  </Triggers>
  </asp:UpdatePanel>
    
    </div>
    
    
    


  <div class="col-md-4"> <asp:Label ID="lblcopyAgent" runat="server" Text="Copy to Agent:" style="display:none;"></asp:Label>
      <asp:UpdatePanel ID="UP_ddlAgentCopy" runat="server">
    <ContentTemplate>
    <asp:DropDownList ID="ddlAgentCopy" runat="server" CssClass="form-control" style="display:none;" OnSelectedIndexChanged="ddlAgentCopy_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
    </ContentTemplate>
    <Triggers>
    <asp:PostBackTrigger ControlID="ddlAgentCopy" />
    </Triggers>
    </asp:UpdatePanel></div>
  
  
  

    <div class="clearfix"></div>
    </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                <label><asp:Label ID="lblPackageName" runat="server" Text="Package Name:"> </asp:Label><span class="req-star" style="color:red">*</span></label>
                              

                                <asp:TextBox MaxLength="50" onkeypress="return IsAlphaNumeric(event);" ID="txtPackageName" placeholder="Package" runat="server" CssClass=" form-control"></asp:TextBox>
                               </div>
                           </div>


                            <div class="col-md-1">
                              <div class="form-group">
                                <label><asp:Label ID="lblDays" runat="server" Text="Days:"> </asp:Label><span class="req-star" style="color:red">*</span></label>
                               <asp:UpdatePanel ID="UpdatePanel1_ddlDays" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlDays" CssClass="form-control" runat="server"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlDays_SelectedIndexChanged">
                                            <%--<asp:ListItem Text="1" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="5" Value="5"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="ddlDays" />
                                    </Triggers>
                                </asp:UpdatePanel>

                                
                               </div>
                           </div>


                           <div class="col-md-2">
                              <div class="form-group">
                                <label><asp:Label ID="lblStockInHand" runat="server" Text="StockInHand:"> </asp:Label><span class="req-star" style="color:red">*</span></label>
                               


                                <asp:TextBox MaxLength="8" ID="txtStockInHand" placeholder="StockInHand:" oncopy="return false" onpaste="return false" oncut="return false" runat="server" CssClass=" form-control" 
                        onkeypress="return restrictNumeric(this.id,'0');"></asp:TextBox>


                               </div>
                           </div>
                           <div class="col-md-3">
                           <div class="form-group custom-radio-table">
                           <label> Package Type: </label>

                          <div>  <asp:RadioButton GroupName="NationType" value=1 Checked="true" ID="rbtnInternational" runat="server" Text="IsInterNational" />
                           <asp:RadioButton GroupName="NationType" value=0 ID="rbtnDomestic" runat="server" Text="IsDomestic" /> </div>

                           </div>
                           
                           </div>
                           <div class="col-md-4" style="display:none">
                           <div class="form-group">
                           <label><asp:label ID="lblorderby" Text="Orderby:" runat="server"></asp:label></label>
                           <asp:textbox ID="txtorderby" runat="server" width="100 px"  cssclass="form-control"></asp:textbox>
                           </div>
                           
                           </div>


                           <div class="col-md-1">
                            <div class="form-group">
                            <label><asp:Label ID="lblStartingFrom" runat="server" Text="Currency:"> </asp:Label> <span class="req-star" style="color:red">*</span> </label>

                            <asp:DropDownList ID="ddlCurrency" runat="server" style="width:70px"  Enabled="false">
                
                                    </asp:DropDownList>
                            </div>

                           </div>


                           <div class="col-md-2">
                            <div class="form-group"> 
                            <label>Starting Cost From: </label>
                            
                            
                            <asp:TextBox MaxLength="10" ID="txtStartingFrom" runat="server"  oncopy="return false" onpaste="return false" oncut="return false" oncontextmenu="return false" placeholder="Starting From" CssClass=" form-control" 
                        onkeypress="return restrictNumeric(this.id,'2');" ></asp:TextBox>
                            
                             </div>

                            



                           </div>

                           


                           


                          
                          
                          
                      <%--     <div class="col-md-4">
                              <div class="form-group">
                                <label>Package Origin:<span class="req-star">*</span></label>
                                <input type="text" class="form-control" placeholder="Package Origin">
                               </div>
                           </div>
--%>




  <%-- <asp:DropDownList ID="ddlCountry" Style="width: 50%;" 
             runat="server" AutoPostBack="True"  CssClass="inputDdlEnabled"
             onselectedindexchanged="ddlCountry_SelectedIndexChanged"  >
                            </asp:DropDownList> --%>

                        <div class="col-md-3">
                              <div >
                                <label><asp:Label ID="lblOriginCountry" runat="server" Text="Origin Country:"></asp:Label><span class="req-star" style="color:red">*</span></label>
                                
                                <asp:DropDownList ID="OriginCountryText" runat="server" CssClass=" form-control"  
                        AppendDataBoundItems="true" onchange="LoadOriginCities(this.id)">
                        <asp:ListItem Selected="True" Value="0" Text="Select Country"></asp:ListItem>
                    </asp:DropDownList>


                                
                                
                                
                              </div>
                           </div>



                           <div class="col-md-3">
                              <div class="form-group">
                                <label><asp:Label ID="lblOriginCity" runat="server" Text="Origin City:"></asp:Label><span class="req-star" style="color:red">*</span></label>
                                
                                
                          <asp:DropDownList ID="originCityText" CssClass=" form-control" runat="server"
                        onchange="SetOriginCity(this.id)">
                        <asp:ListItem Selected="True" Value="0" Text="Select City"></asp:ListItem>
                    </asp:DropDownList>
                    
                          
                                
                                
                                
                                
                               </div>
                           </div>  

                           <div class="row">
                           <div class="col-md-12">
                           <div class="col-md-3">
                              <div class="form-group">
                                <label>
                                    <asp:Label ID="lblCountry1" runat="server" Text="Destination Country:"></asp:Label>
                                    <span class="req-star" style="color:red">*</span>
                                </label>
                                <asp:DropDownList ID="CountryText1" runat="server" CssClass=" form-control"  AppendDataBoundItems="true" onchange="LoadCities(this.id)">
                                    <asp:ListItem Selected="True" Value="0" Text="Select Country"></asp:ListItem>
                                </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                <label>
                                    <asp:Label ID="lblCity1" runat="server" Text="Destination City:"></asp:Label>
                                    <span class="req-star" style="color:red">*</span>
                                </label>
                                  <asp:DropDownList ID="CityText1" CssClass=" form-control" runat="server" onchange="SetCity(this.id,1)">
                                      <asp:ListItem Selected="True" Value="0" Text="Select City"></asp:ListItem>
                                  </asp:DropDownList>
                              </div>
                           </div>
                           <%--<div class="col-md-6">
                                    <a id="btnAddCountry1" name="btnAddCountry1" onclick="addCountry(1)">Add</a>
                               </div>--%>
                           <div class="col-md-6 marbot_20">
                               <a href="javascript:void(0)" id="btnAddCountry1" name="btnAddCountry1" onclick="javascript:addCountry(1)"><span class=" glyphicon glyphicon-plus-sign"></span> Add More</a>
                           </div>
                           </div>
                           
                         </div>
                           <div id="divCountries">

                           </div>
                          

                           <div class="col-md-12 marbot_10">
                              <div class="form-group">
                                <label><asp:Label ID="lblTheme" runat="server" Text="Select Theme:"></asp:Label><span class="req-star" style="color:red">*</span></label>
                               <div class="clearfix"></div>
                                <div class="rb-custom-checkbox pull-left">                                     
                                  
                                  
                                  
                                  
                                  <asp:CheckBoxList ID="chkTheme" runat="server" CssClass="custom-checkbox-table" RepeatDirection="Horizontal">
                    </asp:CheckBoxList>

                     
                                  
                                   <%-- <input type="checkbox">
                                    <label>Cruise</label>--%>
                                </div>
                                 
                                  
                                  
                               </div>
                               
                           </div>
                           <div class="clearfix"></div>
                            <div class="col-md-12 ">
                               
                              <div class="form-group">
                                <label><asp:Label ID="Label1" runat="server" Text="Package Includes:"></asp:Label><span class="req-star" style="color:red">*</span></label>
                                <div class="clearfix"></div>
                                <div class="rb-custom-checkbox pull-left custom-checkbox-table">                                     
                                  
                                  <asp:CheckBox CssClass="" runat="server" ID="chkAir" Text="Air" />
                                  <asp:CheckBox CssClass="" runat="server" ID="chkHotel" Text="Hotel" />
                                  <asp:CheckBox CssClass="" runat="server" ID="chkTours" Text="Tours" />
                                  <asp:CheckBox CssClass="" runat="server" ID="chkTransfer" Text="Transfer" />
                                  <asp:CheckBox CssClass="" runat="server" ID="chkMeals" Text="Meals" />
                                  
                                  
                                 <%-- <asp:CheckBoxList ID="ChkPackageIncludes" runat="server" RepeatDirection="Horizontal" onchange="SetSupplierProducts();" >
                                    <asp:ListItem Text="Air" Value="A" ></asp:ListItem>
                                    <asp:ListItem Text="Hotel" Value="H" ></asp:ListItem>
                                    <asp:ListItem Text="Tours" Value="T" ></asp:ListItem>
                                    <asp:ListItem Text="Transfer" Value="R" ></asp:ListItem>
                                    
                                    </asp:CheckBoxList>--%>

                     
                                  
                                   <%-- <input type="checkbox">
                                    <label>Cruise</label>--%>
                                </div>
                                 
                                  
                                  
                               </div>
                               
                           </div>


                          
                        </div>

                      
                        <div class="row">
                         <br />  
                             <div class="col-md-8">
                              <div class="form-group">
                                <label><asp:Label ID="lblIntroduction" runat="server" Text="Introduction:"></asp:Label><span class="req-star" style="color:red">*</span></label>
                                


                                <asp:TextBox MaxLength="10" CssClass="form-control" ID="txtIntroduction" runat="server" 
                        TextMode="MultiLine" Height="100"></asp:TextBox>


                              </div>
                           </div>
                        </div>


                        <div class="row">
                           <div class="col-md-8">
                              <div class="form-group">
                                <label><asp:Label ID="lblOverView" runat="server" Text="OverView:"></asp:Label><span class="req-star" style="color:red">*</span></label>
                               


                                <asp:TextBox ID="txtOverView" CssClass="form-control" runat="server" TextMode="MultiLine"
                        Height="100"></asp:TextBox>


                              </div>
                           </div>

                        </div>


                      <div class="row" style="display:none">

                           <div class="col-md-2">
                              <div class="form-group">
                                <label>Start From :<span class="req-star" style="color:red">*</span></label>
                                <div class="row no-gutter">
                                    <div class="col-xs-6">                                         
                                        <div class="input-group">
                                           <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                                       
                                           
                                           
<asp:TextBox ID="txtStartFromHr" runat="server" CssClass="form-control"  MaxLength="2" onkeypress="return restrictNumeric(this.id,'0');" placeholder="Hr" 
                                            onchange=" CalcDuration()"> </asp:TextBox>
                                            
                                                                                   
                                        </div>
                                    </div>
                                    <div class="col-xs-6">                                       
                                        <div class="input-group">
                                           <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                                        
                                           
                                           
<asp:TextBox ID="txtStartTimeMin" placeholder="Min" runat="server" CssClass="form-control" Width="60" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0');"
                                            onchange="CalcDuration()"></asp:TextBox>                                    
                                           
                                           
                                           
                                                        
                                        </div>
                                    </div>
                                </div>
                              </div>
                           </div>
                           <div class="col-md-2 col-md-2">
                              <div class="form-group">
                                <label>End To :<span class="req-star" style="color:red">*</span></label>
                                <div class="row no-gutter">
                                    <div class="col-xs-6">                                                                      
                                        <div class="input-group">
                                           <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                                         
                                            
                                            
<asp:TextBox ID="txtEndToHr" placeholder="Hr" runat="server" CssClass="form-control" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0');" 
                                            onchange="CalcDuration()"></asp:TextBox>                               
                                            
                                            
                                            
                                            
                                                                 
                                        </div>
                                    </div>
                                    <div class="col-xs-6">                                                                                                  
                                        <div class="input-group">
                                           <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                                         
          <asp:TextBox ID="txtEndTimeMin" runat="server" CssClass="form-control" placeholder="Min" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0');"
                                            onchange="CalcDuration()"></asp:TextBox>                               
                                         
                                         
                                               
                                        </div>
                                    </div>
                                </div>
                              </div>
                           </div>
                          <div class="col-md-2">
                              <div class="form-group">
                                <label><asp:Label ID="Label2" runat="server" Text="Package Duration:"></asp:Label></label>                                                                                                           
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                                    
                                   
                                     <asp:TextBox CssClass="form-control" ID="txtDuration"  EnableViewState="true" runat="server"></asp:TextBox>
                                                            
                                </div>                         
                              </div>
                           </div>

                       </div>


                      <div class="row">
                          
                          <div class="col-md-8">
                              <div class="form-group">
                              <table id="tblItinerary" runat="server">
                                <tr id="trLBL1" runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary1" runat="server" Text="Itinerary 1:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT1" runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary1" name="txtItinerary1" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr id="trLBL2" runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary2" runat="server" Text="Itinerary 2:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT2" runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary2" name="txtItinerary2" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr> 
                                
                                 <tr id="trLBL3" runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary3" runat="server" Text="Itinerary 3:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT3" runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary3" name="txtItinerary3" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                
                                  
                                <tr id=trLBL4 runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary4" runat="server" Text="Itinerary 4:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT4" runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary4" name="txtItinerary4" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr id="trLBL5"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary5" runat="server" Text="Itinerary 5:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT5"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary5" name="txtItinerary5"  CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr id="trLBL6"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary6" runat="server" Text="Itinerary 6:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT6"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary6" name="txtItinerary6"  CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr id="trLBL7"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary7" runat="server" Text="Itinerary 7:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT7"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary7"  name="txtItinerary7" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr id="trLBL8"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary8" runat="server" Text="Itinerary 8:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT8"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary8" name="txtItinerary8" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr id="trLBL9"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary9" runat="server" Text="Itinerary 9:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT9"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary9" name="txtItinerary9"  CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr id="trLBL10"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary10" runat="server" Text="Itinerary 10:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT10"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary10" name="txtItinerary10"  CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                     <tr id="trLBL11"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary11" runat="server" Text="Itinerary 11:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT11"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary11" name="txtItinerary11"  CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr id="trLBL12"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary12" runat="server" Text="Itinerary 12:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT12"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary12" name="txtItinerary12"  CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr> 
                                
                                 <tr id="trLBL13"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary13" runat="server" Text="Itinerary 13:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT13"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary13" name="txtItinerary13" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                
                                  
                                <tr id="trLBL14"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary14" runat="server" Text="Itinerary 14:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT14"   runat="server"> 
                                   <td>
                                        <asp:TextBox ID="txtItinerary14" name="txtItinerary14" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr id="trLBL15"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary15" runat="server" Text="Itinerary 15:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT15"   runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary15" name="txtItinerary15" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr id="trLBL16"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary16" runat="server" Text="Itinerary 16:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT16"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary16" name="txtItinerary16" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr id="trLBL17"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary17" runat="server" Text="Itinerary 17:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT17"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary17" name="txtItinerary17" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr id="trLBL18"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary18" runat="server" Text="Itinerary 18:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT18"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary18" name="txtItinerary18" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr  id="trLBL19"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary19" runat="server" Text="Itinerary 19:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT19"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary19" name="txtItinerary19" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>   
                                <tr id="trLBL20"  runat="server">
                                    <td>
                                        <label><asp:Label ID="lblItinerary20" runat="server" Text="Itinerary 20:"></asp:Label></label>
                                    </td>
                                </tr>
                                <tr id="trTXT20"  runat="server">
                                   <td>
                                        <asp:TextBox ID="txtItinerary20" name="txtItinerary20" CssClass="form-control" runat="server" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                    </td>
                                </tr>                                                     
                             </table>                                 
                              </div>
                           </div>
                     </div>
                    
                    
                     <div class="row">
                          <div class="col-md-8">
                              <div class="form-group">
                                <label><asp:Label ID="lblDetails" runat="server" Text="Details:"></asp:Label></label>
                               
                                
                <asp:TextBox ID="txtDetails" CssClass="form-control" runat="server" TextMode="MultiLine" Height="100"></asp:TextBox>
                                
                                
                                                                                  
                              </div>
                           </div>

                      </div>


         <div class="row"> 
                     
            <div class="col-md-12"> 

                    <div>  <strong>  Supplier Details :<span class="req-star" style="color:red">*</span></strong></div>
      
                        <div class="well"> 
                            <div id="div2" class="">
                        <asp:HiddenField ID="hdfSupplier" runat="server"></asp:HiddenField>
                         <asp:HiddenField runat="server" ID="hdfSupplierCount" Value="0" />


<div class="table-responsive">
                       
                        <asp:GridView CssClass="gridviewbornone borderless table" ID="gvSuppliers" runat="server" AutoGenerateColumns="False" AllowPaging="true"
                            PageSize="10" DataKeyNames="packSuppId" CellPadding="0" ShowFooter="true" CellSpacing="0" BorderWidth="0" 
                            OnRowCommand="gvSuppliers_RowCommand" Width="100%" OnRowEditing="gvSuppliers_RowEditing"
                            OnRowDeleting="gvSuppliers_RowDeleting" OnRowUpdating="gvSuppliers_RowUpdating"
                            OnRowCancelingEdit="gvSuppliers_RowCancelingEdit"
                            >
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <label style="color: Black; font-weight:bold">
                                            Supplier</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITLblSuppName" runat="server" Text='<%#Eval("packSuppName") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("packSuppName") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EITtxtSuppName" runat="server" Enabled="true" Text='<%#Eval("packSuppName") %>'
                                            Width="100px" CssClass="form-control"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox onkeypress="return IsAlphaNumeric(event);" MaxLength="75" ID="FTtxtSuppName" runat="server" Enabled="true" CssClass="form-control"
                                            Width="300px"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                
                                 <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <label style="color: Black; font-weight:bold">
                                            Supplier Email</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITLblSuppEmail" runat="server" Text='<%#Eval("packSuppEmail") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("packSuppEmail") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EITtxtSuppEmail" runat="server" Enabled="true" Text='<%#Eval("packSuppEmail") %>'
                                            Width="300px" CssClass="form-control"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox MaxLength="75" ID="FTtxtSuppEmail" runat="server" Enabled="true" CssClass="form-control"
                                            Width="300px"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="left" />
                                    <HeaderTemplate>
                                      <label style="color: Black; font-weight:bold">
                                            <span class="mdt"></span>Supplier Product</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%-- <asp:Label ID="ITlblControl" runat="server" Text='<%# Eval("flexControlDESC") %>' CssClass="labelDtl grdof"
                                                                    ToolTip='<%# Eval("flexControlDESC") %>' Width="110px"></asp:Label>--%>
                                        <asp:Label ID="ITlblSuppProduct" runat="server" Text='<%# Eval("packProductType_name") %>'
                                            CssClass="labelDtl grdof" ToolTip='<%# Eval("packProductType_name") %>' Width="110px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList   ID="EITddlSuppProduct"  CssClass="form-control" runat="server" Enabled="true" Width="300px">
                                            <asp:ListItem Text="--Select Product--" Value="-1"></asp:ListItem> 
                                                <asp:ListItem Text="Air" Value="A" ></asp:ListItem>
                                                <asp:ListItem Text="Hotel" Value="H" ></asp:ListItem>
                                                <asp:ListItem Text="Tours" Value="T" ></asp:ListItem>
                                                <asp:ListItem Text="Transfer" Value="R" ></asp:ListItem>                                           
                                                <asp:ListItem Text="Meals" Value="M" ></asp:ListItem> 
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="EIThdfSuppProduct" runat="server" Value='<%# Eval("packProductType") %>'>
                                        </asp:HiddenField>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:DropDownList CssClass="form-control" ID="FTddlSuppProduct" runat="server" Enabled="true" Maxlength="50" Width="300px"
                                           >
                                            <asp:ListItem Text="-Select Product-" Value="-1"></asp:ListItem>                                          
                                                <asp:ListItem Text="Air" Value="A" ></asp:ListItem>
                                                <asp:ListItem Text="Hotel" Value="H" ></asp:ListItem>
                                                <asp:ListItem Text="Tours" Value="T" ></asp:ListItem>
                                                <asp:ListItem Text="Transfer" Value="R" ></asp:ListItem> 
                                                <asp:ListItem Text="Meals" Value="M" ></asp:ListItem>    
                                        </asp:DropDownList>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label runat="server" ID="id" Width="100px"></asp:Label></HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel_lnkSuppEdit" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkSuppEdit" runat="server" Text="Edit" CommandName="Edit" Width="50px">
                                                </asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkSuppEdit" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="UpdatePanel_ibtnSuppDelete" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="ibtnSuppDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif"
                                                    CausesValidation="False"  CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the Supplier?');">
                                                </asp:ImageButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="ibtnSuppDelete" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel_lnkSuppUpdate" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkSuppUpdate" runat="server" Text="Update" Width="50px" CommandName="Update"
                                                    OnClientClick="return validateSupplierAddUpdate('U',this.id)"></asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkSuppUpdate" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="UpdatePanel_lnkFCancel" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkSuppCancel" runat="server" Text="Cancel" Width="50px" CommandName="Cancel"></asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkSuppCancel" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel_lnkSuppAdd" runat="server">
                                            <ContentTemplate>
                                              
                                                 <asp:LinkButton ID="lnkSuppAdd" CommandName="Add" runat="server" Text="Add" CssClass="marright_10"  
                                                    OnClientClick="return validateSupplierAddUpdate('I',this.id)"> </asp:LinkButton>

                                          </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkSuppAdd" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="" />
                            <RowStyle HorizontalAlign="left" />
                            <AlternatingRowStyle CssClass="" />
                        </asp:GridView>
    
</div>


                    </div>

                        </div>          
            
            </div>


        </div>

                   <div class="well" data-clone-content="supplierDetails">
                                 
                                 <div class="row">
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <label><asp:Label ID="lblMealsInclude" runat="server" Text="Meals Include:"> </asp:Label>	</label>
                                          <asp:TextBox MaxLength="100" onkeypress="return IsAlphaNumeric(event);" ID="txtMealsInclude" runat="server" CssClass="form-control"></asp:TextBox>
                                          
                                                                                   
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <label><asp:Label ID="lblTrasferInclude" runat="server" Text="Transfer Include:"> </asp:Label>	</label>
                                          <asp:TextBox MaxLength="100" onkeypress="return IsAlphaNumeric(event);"  ID="txtTrasferInclude" runat="server" CssClass="form-control"></asp:TextBox>                                         
                                       </div>
                                    </div>
                                </div>
                              
                              









                              
                                <div class="row">
                                    <div class="col-md-3">
                                       <div class="form-group">
                                          <label><asp:Label ID="lblPickLocation" runat="server" Text="Pick Location:"> </asp:Label>	</label>
                                           <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></div>
                                                
                                                 
                                                 
                                                 <asp:TextBox MaxLength="100" onkeypress="return IsAlphaNumeric(event);" ID="txtPickLocation" runat="server" CssClass="form-control"></asp:TextBox>
                                                 
                                                                           
                                            </div>                                      
                                       </div>
                                    </div>
                                    
                                
                                   
                                    <div class="col-md-3">
                                       <div class="form-group">
                                          <label>Pick Up Date/Time	</label>
                                           <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                                               <asp:TextBox ID="txtPickUpHr" runat="server" CssClass="form-control" Width="60px"      MaxLength="2" onkeypress="return restrictNumeric(this.id,'0');"
                                            onchange="return ValidateTime('H',this.value,this.id)"></asp:TextBox>  
                                            
<asp:TextBox ID="txtPickUpMin" runat="server" CssClass="form-control" MaxLength="2" Width="60px" onkeypress="return restrictNumeric(this.id,'0');"
                                            onchange="return ValidateTime('M',this.value,this.id)"></asp:TextBox>
                                            
                                            
                                                      
                                                                      
                                            </div>                                        
                                       </div>
                                    </div>
                                   
                                    <div class="col-md-3">
                                       <div class="form-group">
                                          <label><asp:Label ID="lblDropOffLocation" runat="server" Text="Drop off Location:"> </asp:Label>	</label>
                                           <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></div>
                                                
                                                 
                                              <asp:TextBox MaxLength="100" onkeypress="return IsAlphaNumeric(event);" ID="txtDropOffLocation" runat="server" CssClass="form-control"></asp:TextBox>   
                                                                            
                                            </div>                                        
                                       </div>
                                    </div>


                                    <div class="col-md-3">
                                       <div class="form-group">
                                          <label><asp:Label ID="lblDropOffDate" runat="server" Text="Drop off Date/Time:"></asp:Label></label> 
                                           <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                                                 
                                                 
                                                 
                            
                                        <asp:TextBox ID="txtDropUpHr" runat="server" CssClass="form-control pull-left" Width="60px" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0');"
                                            onchange="return ValidateTime('H',this.value,this.id)"></asp:TextBox>
                                        <asp:TextBox ID="txtDropUpMin" runat="server" CssClass="form-control pull-left" Width="60px" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0');"
                                            onchange="return ValidateTime('M',this.value,this.id)"></asp:TextBox>
                                        <%--<input  name="FromDate" type="text" value="DD/MM/YYYY" id="FromDate" />--%>            
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                                       
                                            </div>                                        
                                       </div>
                                    </div>

                                 </div>





                              </div>



<%--
<div>

 <a class="pull-left" href="javascript:void(0)" data-clone-button="supplierDetails"><span class=" glyphicon glyphicon-plus-sign"></span> Add More  <div class="clearfix"></div></a>
 
 
 

  <a class="pull-right" href="javascript:void(0)"><span class=" glyphicon glyphicon-minus-sign"></span> Delete  <div class="clearfix"></div></a>



  <div class="clearfix"></div>
 </div>--%>  



                              

                           </div>
                    




                      <div class="row"> 
                     
                      <div class="col-md-12"> 
                     
            
            
        

        <div>  <strong>  Additional Details </strong></div>
            <div class="well"> 
                       <div id="div1" class="">
                        <asp:HiddenField ID="hdfPackFlexDetails" runat="server"></asp:HiddenField>
                         <asp:HiddenField runat="server" ID="hdfPackFlexCount" Value="0" />
             <div class="table-responsive">          
                  <asp:GridView CssClass="gridviewbornone table borderless" ID="gvPackFlexDetails" runat="server" AutoGenerateColumns="False" AllowPaging="true"
                            PageSize="10" DataKeyNames="flexId" CellPadding="0" ShowFooter="true" CellSpacing="0" BorderWidth="0" 
                            OnRowCommand="gvPackFlexDetails_RowCommand" Width="100%" OnRowEditing="gvPackFlexDetails_RowEditing"
                            OnRowDeleting="gvPackFlexDetails_RowDeleting" OnRowUpdating="gvPackFlexDetails_RowUpdating"
                            OnRowCancelingEdit="gvPackFlexDetails_RowCancelingEdit">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <label style="color: Black; font-weight:bold">
                                            Label</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITLabel" runat="server" Text='<%#Eval("flexLabel") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("flexLabel") %>' Width="100px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox MaxLength="50" onkeypress="return IsAlphaNumeric(event);" ID="EITtxtLabel" runat="server" Enabled="true" Text='<%#Eval("flexLabel") %>'
                                            Width="100px" CssClass="inputDdlEnabled"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox MaxLength="50" onkeypress="return IsAlphaNumeric(event);" ID="FTtxtLabel" runat="server" Enabled="true" CssClass="form-control"
                                            Width="100px"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="left" />
                                    <HeaderTemplate>
                                      <label style="color: Black; font-weight:bold">
                                            <span class="mdt"></span>Control</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%-- <asp:Label ID="ITlblControl" runat="server" Text='<%# Eval("flexControlDESC") %>' CssClass="labelDtl grdof"
                                                                    ToolTip='<%# Eval("flexControlDESC") %>' Width="110px"></asp:Label>--%>
                                        <asp:Label ID="ITlblControl" runat="server" Text='<%# Eval("flexControlDESC") %>'
                                            CssClass="labelDtl grdof" ToolTip='<%# Eval("flexControlDESC") %>' Width="110px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList CssClass="form-control" ID="EITddlControl" runat="server" Enabled="true" Width="120px">
                                            <asp:ListItem Text="TextBox" Value="T"></asp:ListItem>
                                            <asp:ListItem Text="DateControl" Value="D"></asp:ListItem>
                                            <asp:ListItem Text="DropDownList" Value="L"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="EIThdfControl" runat="server" Value='<%# Eval("flexControl") %>'>
                                        </asp:HiddenField>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:DropDownList CssClass="form-control" ID="FTddlControl" runat="server" Enabled="true" Maxlength="50"
                                           >
                                            <asp:ListItem Text="-SelectControl-" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="TextBox" Value="T"></asp:ListItem>
                                            <asp:ListItem Text="DateControl" Value="D"></asp:ListItem>
                                            <asp:ListItem Text="DropDownList" Value="L"></asp:ListItem>
                                        </asp:DropDownList>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <label style="color: Black; font-weight:bold">
                                            Sql Query</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITSqlQuery" runat="server" Text='<%#Eval("flexSqlQuery") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("flexSqlQuery") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox onkeypress="return IsAlphaNumeric(event);" ID="EITtxtSqlQuery" runat="server" Enabled="true" Text='<%#Eval("flexSqlQuery") %>'
                                            CssClass="form-control"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox onkeypress="return IsAlphaNumeric(event);"  ID="FTtxtSqlQuery" runat="server" Enabled="true" CssClass="form-control"
                                            Width="100px"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="left" />
                                    <HeaderTemplate>
                                        <label style="color: Black; font-weight:bold">
                                            <span class="mdt"></span>DataType</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%--<asp:Label ID="ITlblDataType" runat="server" Text='<%# Eval("flexDataTypeDESC") %>' CssClass="labelDtl grdof"
                                                                    ToolTip='<%# Eval("flexDataTypeDESC") %>' Width="110px"></asp:Label>--%>
                                        <asp:Label ID="ITlblDataType" runat="server" Text='<%# Eval("flexDataTypeDESC") %>'
                                            CssClass="labelDtl grdof" ToolTip='<%# Eval("flexDataTypeDESC") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList CssClass="form-control" ID="EITddlDataType" runat="server" Enabled="true">
                                            <asp:ListItem Text="AlphaNumeric" Value="A"></asp:ListItem>
                                            <asp:ListItem Text="Numeric" Value="N"></asp:ListItem>
                                            <asp:ListItem Text="TextOnly" Value="T"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="EIThdfDataType" runat="server" Value='<%# Eval("flexDataType") %>'>
                                        </asp:HiddenField>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:DropDownList CssClass="form-control" ID="FTddlDatatype" runat="server" Enabled="true">
                                            <asp:ListItem Text="-SelectDataType-" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="AlphaNumeric" Value="A"></asp:ListItem>
                                            <asp:ListItem Text="Numeric" Value="N"></asp:ListItem>
                                            <asp:ListItem Text="TextOnly" Value="T"></asp:ListItem>
                                        </asp:DropDownList>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="left" />
                                    <HeaderTemplate>
                                        <label style="color: Black; font-weight:bold">
                                            <span class="mdt"></span>Mandatory</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%-- <asp:Label ID="ITlblMandatory" runat="server" Text='<%# Eval("flexStatusDESC") %>' CssClass="labelDtl grdof"
                                                                    ToolTip='<%# Eval("flexStatusDESC") %>' Width="110px"></asp:Label>--%>
                                        <asp:Label ID="ITlblMandatory" runat="server" Text='<%# Eval("flexStatusDESC") %>'
                                            CssClass="labelDtl grdof" ToolTip='<%# Eval("flexStatusDESC") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList CssClass="form-control" ID="EITddlMandatory" runat="server" Enabled="true">
                                            <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                            <asp:ListItem Text="NO" Value="N"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="EIThdfStatus" runat="server" Value='<%# Eval("flexMandatoryStatus") %>'>
                                        </asp:HiddenField>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:DropDownList CssClass="form-control" ID="FTddlMandatory" runat="server" Enabled="true">
                                            <asp:ListItem Text="-SelectStatus-" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                            <asp:ListItem Text="NO" Value="N"></asp:ListItem>
                                        </asp:DropDownList>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                       <label style="color: Black; font-weight:bold">
                                            Order</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITOrder" runat="server" Text='<%#Eval("flexOrder") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("flexOrder") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox MaxLength="3" ID="EITtxtOrder" runat="server" Enabled="true" Text='<%#Eval("flexOrder") %>'
                                            Width="100px" CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2');"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox MaxLength="3" ID="FTtxtOrder" runat="server" Enabled="true" CssClass="form-control"
                                            onkeypress="return restrictNumeric(this.id,'2');"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label runat="server" ID="id" Width="100px"></asp:Label></HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel_lnkFEdit" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkFEdit" runat="server" Text="Edit" CommandName="Edit" Width="50px">
                                                </asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkFEdit" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="UpdatePanel_ibtnFDelete" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="ibtnFDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif" 
                                                    CausesValidation="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the user?');">
                                                </asp:ImageButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="ibtnFDelete" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel_lnkFUpdate" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkFUpdate" runat="server" Text="Update" Width="50px" CommandName="Update"
                                                    OnClientClick="return validateDtlAddUpdate('U',this.id)"></asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkFUpdate" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="UpdatePanel_lnkFCancel" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkFCancel" runat="server" Text="Cancel" Width="50px" CommandName="Cancel"></asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkFCancel" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel_lnkFAdd" runat="server">
                                            <ContentTemplate>
                                              
                                                 <asp:LinkButton ID="lnkFAdd" CommandName="Add" runat="server" Text="Add" CssClass="marright_10"  
                                                    OnClientClick="return validateDtlAddUpdate('I',this.id)"> </asp:LinkButton>

                                          </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkFAdd" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="" />
                            <RowStyle HorizontalAlign="left" />
                            <AlternatingRowStyle CssClass="" />
                        </asp:GridView>
             </div>

                    </div>
              


                </div>
               
                      </div>


                       </div>

                 
                    

                </div>
            
                <div role="tabpanel" class="tab-pane" id="packageTab2">
                        

<style> 

.gridviewbornone td,th { border:none }

.undatewidth { width:101px!important; float:left}


</style>
                   

                     <div id="divPODetails" class="well" data-clone-content="PriceDetail">
                        
                        <asp:HiddenField ID="hdfPrice" runat="server"></asp:HiddenField>
                        <asp:HiddenField runat="server" ID="hdfPriceCount" Value="0" />
                       
        <div class="table-responsive table">            
                        <asp:GridView CssClass="gridviewbornone borderless table" ID="GvPrice" runat="server" AutoGenerateColumns="False" AllowPaging="false" BorderStyle="None" BorderWidth="0" 
                             DataKeyNames="priceId" CellPadding="0" ShowFooter="true" CellSpacing="0"
                            Width="100%" GridLines="Both" OnRowCommand="GvPrice_RowCommand" OnRowDeleting="GvPrice_RowDeleting"
                            OnRowEditing="GvPrice_RowEditing" OnRowUpdating="GvPrice_RowUpdating" OnRowCancelingEdit="GvPrice_RowCancelingEdit">
                           
                           
                            <Columns>
                               




                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                          <div class="col-md-12"><label style="color: Black; font-weight:bold">
                                            Label / RoomType</label> </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                       <div class="col-md-12">  <asp:Label ID="ITLabel" runat="server" Text='<%#Eval("Label") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("Label") %>'></asp:Label>


                                            </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        
                                        <div class="col-md-12"> 
                                        <asp:TextBox MaxLength="50" ID="EITtxtLabel" onkeypress="return IsAlphaNumeric(event);"  runat="server" Enabled="true" Text='<%#Eval("Label") %>'
                                            CssClass="form-control"></asp:TextBox>


                                            </div>

                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                    
                                    
                                    <div class="col-md-12"> <asp:TextBox MaxLength="50" onkeypress="return IsAlphaNumeric(event);" ID="FTtxtLabel" runat="server" Enabled="true" CssClass="form-control"></asp:TextBox></div>    


                                    </FooterTemplate>
                                </asp:TemplateField>
                                 
                                  <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                          <div class="col-md-12"><label style="color: Black; font-weight:bold">
                                            Pax Type</label> </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                       <div class="col-md-12">  <asp:Label ID="ITlblPaxType" runat="server" Text='<%#Eval("PaxType") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("PaxType") %>'></asp:Label>


                                            </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        
                                        <div class="col-md-12"> 
                                        <asp:DropDownList CssClass="form-control" ID="EITddlPaxType" runat="server" Enabled="true">
                                             <asp:ListItem Text="-Select PaxType-" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Adult" Value="Adult"></asp:ListItem>
                                            <asp:ListItem Text="Child" Value="Child"></asp:ListItem>
                                            <asp:ListItem Text="Infant" Value="Infant"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="EIThdfPaxType" runat="server" Value='<%# Eval("PaxType") %>'>
                                        </asp:HiddenField>
                                            </div>

                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                    
                                    
                                    <div class="col-md-12"> 
                                    <asp:DropDownList CssClass="form-control" ID="FTddlPaxType" runat="server" Enabled="true">
                                            <asp:ListItem Text="-Select PaxType-" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Adult" Value="Adult"></asp:ListItem>
                                            <asp:ListItem Text="Child" Value="Child"></asp:ListItem>
                                            <asp:ListItem Text="Infant" Value="Infant"></asp:ListItem>
                                        </asp:DropDownList>
</div>    


                                    </FooterTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                       <label style="color: Black; font-weight:bold">
                                            Supplier Cost</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITSupplierCost" runat="server" Text='<%#Eval("SupplierCost") %>'  CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("SupplierCost") %>' Width="80px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EITtxtSupplierCost" runat="server" Enabled="true" Text='<%#Eval("SupplierCost") %>' oncopy="return false" onpaste="return false" oncut="return false" oncontextmenu="return false"
                                            Width="80px" CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2');"
                                            onChange="getTotalAmount(this.id,'U')"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTtxtSupplierCost" runat="server" Enabled="true" CssClass=" form-control" oncopy="return false" onpaste="return false" oncut="return false" oncontextmenu="return false"
                                            onChange="getTotalAmount(this.id,'I')" onkeypress="return restrictNumeric(this.id,'2');"
                                            Width="80px"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                       <label style="color: Black; font-weight:bold">
                                            Tax</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITTax" runat="server" Text='<%#Eval("tax") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("tax") %>' Width="80px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EITtxtTax" runat="server" Enabled="true" Text='<%#Eval("tax") %>' oncopy="return false" onpaste="return false" oncut="return false" oncontextmenu="return false"
                                            Width="80px" CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2');"
                                            onChange="getTotalAmount(this.id,'U')"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTtxtTax" runat="server" Enabled="true" CssClass="form-control" oncopy="return false" onpaste="return false" oncut="return false" oncontextmenu="return false"
                                            onkeypress="return restrictNumeric(this.id,'2');" onChange="getTotalAmount(this.id,'I')"
                                            Width="80px"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                       <label style="color: Black; font-weight:bold">
                                            Markup</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITMarkup" runat="server" Text='<%#Eval("markup") %>' CssClass="labelDtl grdof" 
                                            ToolTip='<%#Eval("markup") %>' Width="80px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EITtxtMarkup" runat="server" Enabled="true" Text='<%#Eval("markup") %>'  oncopy="return false" onpaste="return false" oncut="return false" oncontextmenu="return false"
                                            Width="80px" CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2');"
                                            onChange="getTotalAmount(this.id,'U')"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTtxtMarkup" runat="server" Enabled="true" CssClass="form-control" oncopy="return false" onpaste="return false" oncut="return false" oncontextmenu="return false"
                                            onkeypress="return restrictNumeric(this.id,'2');" onChange="getTotalAmount(this.id,'I')"
                                            Width="80px"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <label style="color: Black; font-weight:bold">
                                            Amount(Per Pax)</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITAmount" runat="server" Text='<%#Eval("amount") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("amount") %>' Width="70px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EITtxtAmount" Enabled="false" runat="server" Text='<%#Eval("amount") %>'  oncopy="return false" onpaste="return false" oncut="return false" oncontextmenu="return false"
                                            Width="70px" CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2');"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTtxtAmount" Enabled="false" runat="server" CssClass="form-control"  oncopy="return false" onpaste="return false" oncut="return false" oncontextmenu="return false"
                                           onkeypress="return restrictNumeric(this.id,'2');" Width="70px"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <label style="color: Black; font-weight:bold">
                                            Min Pax</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITMinPax" runat="server" Text='<%#Eval("MinPax") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("MinPax") %>' Width="70px"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EITtxtMinPax" Enabled="true" runat="server" Text='<%#Eval("MinPax") %>'  oncopy="return false" onpaste="return false" oncut="return false" oncontextmenu="return false"
                                            Width="70px" CssClass="form-control" onkeypress="return restrictNumeric(this.id,'2');"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTtxtMinPax" Enabled="true" runat="server" CssClass="form-control"  oncopy="return false" onpaste="return false" oncut="return false" oncontextmenu="return false"
                                            onkeypress="return restrictNumeric(this.id,'2');" Width="70px"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                              
                              
                                <%--<asp:TemplateField Visible="false" >
                                                            <HeaderStyle HorizontalAlign="left" />
                                                            <HeaderTemplate>
                                                                <label style="color: Black;">
                                                                    <span class="mdt"></span>Type</label>
                                                            </HeaderTemplate>
                                                        <ItemTemplate>
                                                           <asp:Label ID="ITlblType" runat="server" Text='<%# Eval("flexTypeDESC") %>' CssClass="labelDtl grdof"
                                                                    ToolTip='<%# Eval("flexTypeDESC") %>' Width="10px"></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                              <asp:DropDownList ID="EITddlType" runat="server" Enabled="true" Maxlength="200" Width="10px">
                                                          
                                                            <asp:ListItem Text="SupplierCost" Value="S"></asp:ListItem>
                                                            <asp:ListItem Text="Tax" Value="T"></asp:ListItem>
                                                            <asp:ListItem Text="Mark Up" Value="M"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            
                                                             <asp:HiddenField id="EIThdfType" runat="server" value='<%# Eval("type") %>'></asp:HiddenField>
                                                         </EditItemTemplate>
                                                           <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                            <FooterTemplate>
                                                                 <asp:DropDownList ID="FTddltype" runat="server" Enabled="true" Maxlength="200" Width="10px">
                                                              <asp:ListItem Text="-SelectType-" Value="0"></asp:ListItem>
                                                             <asp:ListItem Text="SupplierCost" Value="S"></asp:ListItem>
                                                            <asp:ListItem Text="Tax" Value="T"></asp:ListItem>
                                                            <asp:ListItem Text="Mark Up" Value="M"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>--%>
                                    <%--<asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                          <div class="col-md-12"><label style="color: Black; font-weight:bold">
                                            Room Type</label> </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                       <div class="col-md-12">  <asp:Label ID="ITlblRoomType" runat="server" Text='<%#Eval("roomType") %>' CssClass="labelDtl grdof"
                                            ToolTip='<%#Eval("roomType") %>'></asp:Label>


                                            </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        
                                        <div class="col-md-12"> 
                                        <asp:DropDownList CssClass="form-control" ID="EITddlRoomType" runat="server" Enabled="true">
                                             <asp:ListItem Text="-Select RoomType-" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="SINGLE" Value="SINGLE"></asp:ListItem>
                                            <asp:ListItem Text="DOUBLE" Value="DOUBLE"></asp:ListItem>
                                            <asp:ListItem Text="TRIPLE" Value="TRIPLE"></asp:ListItem>
                                            <asp:ListItem Text="SUIT" Value="SUIT"></asp:ListItem>                                            
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="EIThdfRoomType" runat="server" Value='<%# Eval("roomType") %>'>
                                        </asp:HiddenField>
                                            </div>

                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                    
                                    
                                    <div class="col-md-12"> 
                                    <asp:DropDownList CssClass="form-control" ID="FTddlRoomType" runat="server" Enabled="true">
                                             <asp:ListItem Text="-Select RoomType-" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="SINGLE" Value="SINGLE"></asp:ListItem>
                                            <asp:ListItem Text="DOUBLE" Value="DOUBLE"></asp:ListItem>
                                            <asp:ListItem Text="TRIPLE" Value="TRIPLE"></asp:ListItem>
                                            <asp:ListItem Text="SUIT" Value="SUIT"></asp:ListItem>
                                        </asp:DropDownList>
</div>    


                                    </FooterTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label runat="server" ID="id" Width="200px"></asp:Label></HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel1_lnkEdit" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit" Width="50px">
                                                </asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkEdit" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="UpdatePanel1_ibtnDelete" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif"
                                                    CausesValidation="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the user?');">
                                                </asp:ImageButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="ibtnDelete" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel1_lnkUpdate" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" Width="50px" CommandName="Update"
                                                    OnClientClick="return validateDtlAddUpdate1('U',this.id)"></asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkUpdate" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="UpdatePanel1_lnkCancel" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" Width="50px" CommandName="Cancel"></asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkCancel" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                    <FooterTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel1_lnkAdd" runat="server">
                                            <ContentTemplate>
                                               
                                                <asp:LinkButton ID="lnkAdd" CommandName="Add" runat="server" Text="Add"
                                                    OnClientClick="return validateDtlAddUpdate1('I',this.id)"></asp:LinkButton>
                                                    
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkAdd" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="" />
                            <RowStyle CssClass="" HorizontalAlign="left" />
                            <AlternatingRowStyle CssClass="" />
                        </asp:GridView>
            </div>   


                    </div>
                    
                  <%--      <a href="javascript:void(0)" data-clone-button="PriceDetail" class="pull-left"><span class=" glyphicon glyphicon-plus-sign"></span> Add More</a>
                        <div class="clear"></div>--%>
                    
                    
                                   
                </div>


                <div role="tabpanel" class="tab-pane" id="packageTab3">




                   <table>
                        <tr>
                            <td valign="top">
                                <table border="0">
                                   
                                   <tr> 
                                   <td>   <label>Image1 -Home Thumbnail(250x150)</label>  </td>
                                   
                                   </tr>
                                    <tr>
                                        
                                        
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel1_fuImage1" runat="server">
                                                            <ContentTemplate>
                                                                <asp:FileUpload ID="fuImage1" runat="server" Width="200px" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnUpload1" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Button style="margin-top:0px;" class="btn but_b rbpackage-btn" ID="btnUpload1" runat="server" Text="Upload" OnClick="btnUpload1_Click" />
                                                   </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel1_lbUploadMsg" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="lbUploadMsg" runat="server" OnClick="lbUploadMsg_Click"></asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lbUploadMsg" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <asp:Label ID="lblUpload1" runat="server" Visible="false"></asp:Label>
                                                        <div id="dUpload1Msg" style="display: none;">
                                                            <img id="imgupload1" height="50px" width="50px" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                   
                                   


                                   <tr> 
                                   
                                   <td height="20"> </td>
                                   
                                   </tr>


                                      <tr> 
                                   <td>   <label>Image2-Listing (580x350)</label>  </td>
                                   
                                   </tr>

                                   
                                    <tr>
                                        
                                        
                                        <td align="left" style="width: 500px">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel2_fuImage2" runat="server">
                                                            <ContentTemplate>
                                                                <asp:FileUpload ID="fuImage2" runat="server" Width="200px" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnUpload2" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td>
                                                        <asp:Button style="margin-top:0px;" class="btn but_b rbpackage-btn marxs_top5" ID="btnUpload2" runat="server" Text="Upload" OnClick="btnUpload2_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel2_lbUpload2Msg" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="lbUpload2Msg" runat="server" OnClick="lbUpload2Msg_Click"></asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lbUpload2Msg" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        <asp:Label ID="lblUplaod2" runat="server" Visible="false"></asp:Label>
                                                        <div id="dUpload2Msg" style="display: none;">
                                                            <img id="imgupload2" height="50px" width="50px" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                   
                                      <tr> 
                                   
                                   <td height="20"> </td>
                                   
                                   </tr>


                                   <tr> 
                                   <td>   <label> Image3-Gallery (580x350</label>  </td>
                                   
                                   </tr>


                                    <tr>
                                        
                                        
                                        <td align="left" style="width: 500px">
                                            <table border="0">
                                                <tr>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel_fuImage3" runat="server">
                                                            <ContentTemplate>
                                                                <asp:FileUpload ID="fuImage3" runat="server" Width="200px" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnUplaod3" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td>



                                                


                                                        <asp:Button style="margin-top:0px;" class="btn but_b rbpackage-btn" ID="btnUplaod3" runat="server" Text="Upload" OnClick="btnUpload3_Click" />
                                                    </td>
                                                    <td>
                                                    <asp:UpdatePanel ID="UpdatePanel_lbUpload3Msg" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="lbUpload3Msg" runat="server" OnClick="lbUpload3Msg_Click"></asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lbUpload3Msg" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                        
                                                        <asp:Label ID="lblUpload3" runat="server" Visible="false"></asp:Label>
                                                        <div id="dUpload3Msg" style="display: none;">
                                                            <img id="imgupload3" height="50px" width="50px" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>



                           <td valign="top">
                            
                            <div id="pnlImages" runat="server" style="display: none;  z-index:9999">
                                    <%--<asp:Panel id="pnlImages" runat="server"  style="position:fixed;right:250px;display:none" >--%>
                                    <div st class="thepet pdac bg_white bor_gray">
                                        <table border="0">
                                          
                                            <tr>
                                                <td>
                                                   <div class="ns-h3"> Preview</div> 
                                                   
                                                   
                                                    
                                                    
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                    <asp:Image runat="server" Style="height: 100px; width:150px; display: block" ID="img1PreView" />
                                                    <asp:Image runat="server" Style="height: 100px; width:150px;  display: block" ID="img2PreView" />
                                                    <asp:Image runat="server" Style="height: 100px; width:150px;  display: block" ID="img3PreView" />
                                                </td>
                                            </tr>
                                           
                                           
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="UpdatePanel_btnClose" runat="server">
                                                        <ContentTemplate>
   <asp:Button OnClientClick="return showImage('H','')" runat="server" ID="btnClose" Text="Close" CssClass="button" />
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnClose" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            
                                            
                                        </table>
                                        
                                        
                                    </div>
                                    <%--</asp:Panel>--%>
                                </div>
                            
                            </td>
                            
                        </tr>
                    </table>


                    
                    


                </div>


                <div role="tabpanel" class="tab-pane rb-container-wrap" id="packageTab4">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label>Available Dates<span class="req-star" style="color:red">*</span></label>
                                <div class="row">                                                
                                    <div class="col-xs-6 col-md-3">       
                                        <label>From </label>                                                                        
                                        <div class="input-group">
                                            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
                                          
                                      <%--   <a href="javascript:void()" onclick="showCalendar3()">
                                        <img src="images/call-cozmo.png" alt="Pick Date" /></a>   --%>
                                            
                                             <asp:TextBox Width="100px" ID="txtFrom" runat="server" onclick="showCalendar3()" class="form-control"></asp:TextBox>
                                             
                                                                  
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">   
                                         <label>&nbsp;</label>                                                                                                     
                                        <div class="input-group">
                                            <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                                           
                                           <asp:TextBox  ID="txtFromTime" runat="server" CssClass="form-control"></asp:TextBox>
                                                      
                                        </div>
                                    </div>
                                   




                                   
                                    <div class="col-xs-6 col-md-3"> 
                                         <label>To </label>                                                                                                  
                                        <div class="input-group">
                                            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
                                           
                                            
                                            
                                            <asp:TextBox Width="100px" onclick="showCalendar4()" ID="txtTo" runat="server" CssClass="form-control"></asp:TextBox>
                                            
                                                                
                                        </div>
                                    </div>
                                 
                                 
                                    <div class="col-xs-6 col-md-3">      
                                         <label>&nbsp;</label>                                                                                                
                                        <div class="input-group">
                                            <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                                           
                                           
                                           <asp:TextBox ID="txtToTime" runat="server" CssClass="form-control"></asp:TextBox>
                                                           
                                        </div>
                                    </div>



                                </div>
                            </div>
                       </div>
                   </div>
                    <label>UnAvailable Dates<span class="req-star" style="color:red">*</span></label>
                    <div class="row freqflyer-wrapper">                       
                      <div class="col-xs-12 col-sm-4  col-lg-3">
                        <div class="form-group">
                          <label for="">From</label>
                         <%-- <input type="text" class="form-control" id="" placeholder="Card Number">--%>
                           <div class="input-group">
                                            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
                                          
                                      <%--   <a href="javascript:void()" onclick="showCalendar3()">
                                        <img src="images/call-cozmo.png" alt="Pick Date" /></a>   --%>
                                            
                                             <asp:TextBox ID="txtUnavailFrom" Width="100px"  runat="server" onclick="showCalendar7()" class="form-control"></asp:TextBox>
                                             
                                                                  
                                        </div>
                        </div>
                      </div> 
                      <div class="col-xs-10 col-sm-4">
                        <div class="form-group">
                          <label for="">To</label>
                         <div class="input-group">
                                            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
                                          
                                      <%--   <a href="javascript:void()" onclick="showCalendar3()">
                                        <img src="images/call-cozmo.png" alt="Pick Date" /></a>   --%>
                                            
                                             <asp:TextBox ID="txtUnavailTo"  Width="100px"  runat="server" onclick="showCalendar8()" class="form-control"></asp:TextBox>
                                             
                                                                  
                                        </div>
                        </div>
                      </div> 
                      <div class="col-md-2">                        
                         <%-- <a href="javascript:void(0);" id="Add-More" class="add-more"><span class=" glyphicon glyphicon-plus-sign"></span></a>--%>
                          <asp:Button CssClass="btn-link" runat="server" Text="Add" ID="btnAddUA" OnClientClick="return AddDetails('UA');" />                          
                          <asp:Button CssClass="btn-link" ID="btnUpdateUA" Text="Update" OnClientClick="return UpdateItems('UA')" runat="server" />
                           <asp:ListBox runat="server" ID="lstUA" Width="300px" EnableViewState="true" onchange="return EditItems('UA')"></asp:ListBox>
                           <asp:Button CssClass="btn-link" ID="btnRemoveUA" Text="Remove"  OnClientClick="return removeItem('UA')" runat="server" />
</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <asp:Label Font-Bold="true" ID="lblUnavailableDays" runat="server" Text="No.of Unavailable Days :"> </asp:Label></div>
                            <div class="marbot_10">
                                <asp:TextBox MaxLength="4000" ID="txtUnavailableDays" value="0" onkeypress="return restrictNumeric(this.id,'0');"
                                    CssClass="form-control" Width="210px" runat="server" disabled="disabled" ReadOnly="true" Text="0"></asp:TextBox></div>
                        </div>     
                 </div>              
                 
                   <%-- <div class="row">




                        <div class="col-md-2">
                            
                            <label>UnAvailable Dates<span class="req-star">*</span></label>
                            <div class="form-group"  data-clone-content="UnAvailableDates">
                                <div class="row">                                                
                                    <div class="col-xs-6 col-md-10">                                                                           
                                        <div class="input-group">
                                            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
                                          <asp:TextBox onclick="ShowCalender('callContainer')" ID="Date" runat="server" CssClass="form-control"></asp:TextBox>                      
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                                  
                  <div class="clear"></div>

                       </div>
                   </div>--%>
                


                  <%--<div> 
                   
                  
                  
                 <table width="100%">
                        
                        <tr>
                            <td height="23px" align="right">
                                <div style="display: none;">
                                    <input type="hidden" id="UnAvailDatesCounter" value="<%=PackageVariables["UnAvailDatesCount"] %>" />
                                    <input type="hidden" id="UnAvailDatesCurrent" name="UnAvailDatesCurrent" value="<%=PackageVariables["UnAvailDatesCount"] %>" />
                                </div>
                                <div >
                                    <div id="UnAvailDatesDiv">
                                       
                                        <%for (int i = 1; i < Convert.ToInt32(PackageVariables["UnAvailDatesCount"]); i++)
                                          {
                                              if (i == 1)
                                              { %>
                                        
                      
                                                




  <div class="row">




                        <div class="col-md-2">
                            
                          
                            <div class="form-group">
                                <div class="row">                                                
                                    <div class="col-xs-6 col-md-10">                                                                           
                                        <div class="input-group">
                                            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
                                          <input type="text" id="Text5" name="txtUnAvailDatesText-<%=i %>"
                                                    class="form-control" value="<%=PackageVariablesArray["UnAvailDatesValue"][i - 1] %>" />                      
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                                  
                  <div class="clear"></div>

                       </div>
                   </div>







                                      
                                        <%}
                            else
                            { %>
                                        <%if (i < Convert.ToInt32(PackageVariables["UnAvailDatesCount"]) - 1)
                                          { %>
                                       
                                       
                                       
                                    
                                    
                       <div class="row" id="UnAvailDates-<%=i %>">




                        <div class="col-md-2">
                            
                          
                            <div class="form-group">
                                <div class="row">                                                
                                    <div class="col-xs-6 col-md-10">                                                                           
                                        <div class="input-group">
                                            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
                                          
                                          
                                      
                                <input id="Text7" name="txtUnAvailDatesText-<%=i %>" class="form-control"
                                                    type="text" value="<%=PackageVariablesArray["UnAvailDatesValue"][i - 1] %>" />                    
                                                    
                                                    
                                                    
                                                    
                                                                      
                                        </div>
                                    </div>


                                   <div class="col-xs-6 col-md-2">
                                    
                                    
                                    <i style="display: none;" class="fleft margin-left-10" id="I1">
                                           <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('UnAvailDates-<%=i %>','text')" /></i>
                                    
                                    </div>




                                    
                                </div>

                            </div>
                                  
                  <div class="clear"></div>

                       </div>
                   </div>
                   
                   
                                
                                    
                                      
                                      
                                      
                                        







                                        <%}
                                          else
                                          { %>
                                       
                                       


                                       
                                        <div  id="UnAvailDates-<%=i %>">
                                            <span class="fleft width-300">
                                                <input id="txtUnAvailDatesText-<%=i %>" name="txtUnAvailDatesText-<%=i %>"  class="form-control" 
                                                    type="text" value="<%=PackageVariablesArray["UnAvailDatesValue"][i - 1] %>" /></span>
                                           
                                            <i class="fleft margin-left-10" id="UnAvailDatesImg-<%=i %>">
                                             <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('UnAvailDates-<%=i %>','text')" /></i>
                                        </div>


                                        <%} %>
                                        <%} %>
                                        <%} %>
                                    </div>
                                  
                                       
                                       
                                       
                                  <a href="javascript:void(0)"  onclick="javascript:AddDateMore('UnAvailDates')"  class="pull-left"><span class=" glyphicon glyphicon-plus-sign"></span> Add More</a>     
                                       
                                       
                                       
                                       
                                  
                                </div>
                            </td>
                        </tr>
                    </table>
                   
                   
                   </div>--%>


                </div>
                <div role="tabpanel" class="tab-pane rb-container-wrap" id="packageTab5">



                <div class="row"> 
                
                <table> 

            
            
            <tr>
                <td>
                </td>
                <td colspan="5" style="height: 21px">
                    <div style="display: none;">
                        <input type="hidden" id="ExclusionsCounter" value="<%=PackageVariables["exclusionsCount"] %>" />
                        <input type="hidden" id="ExclusionsCurrent" name="ExclusionsCurrent" value="<%=PackageVariables["exclusionsCount"] %>" />
                    </div>
                    <div class="col-md-12">

                    <div>  <b>Exclusions:<sup><span class="req-star" style="color:red">*</span></sup></b></div>
                        <div id="ExclusionsDiv">
                           
                            <%for (int i = 1; i < Convert.ToInt32(PackageVariables["exclusionsCount"]); i++)
                              {
                                  if (i == 1)
                                  { %>
                            <p class="fleft">
                                <%-- <span style=" width:470px" class="fleft"><input style="width:460px" type="text" id="ExclusionsText-<%=i %>" value='<%=Page.Request["ExclusionsText-"+i] %>' name="ExclusionsText-<%=i %>" class="width-300 fleft" value="<%=PackageVariablesArray["exclusionsValue"][i - 1] %>" /></span>--%>
                                <span style="width: 470px" class="fleft">
                                    <input maxlength="4000" onkeypress="return IsAlphaNumeric(event);" type="text" id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>"
                                        class="width-300 fleft form-control" value="<%=PackageVariablesArray["exclusionsValue"][i - 1] %>" /></span>
                            </p>
                            <%}
                            else
                            { %>
                            <%if (i < Convert.ToInt32(PackageVariables["exclusionsCount"]) - 1)
                              { %>
                            <p class="fleft width-100 padding-top-5" id="Exclusions-<%=i %>">
                                <span class="fleft">
                                    <input maxlength="4000" onkeypress="return IsAlphaNumeric(event);" id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>" class="width-300 fleft form-control"
                                        type="text" value="<%=PackageVariablesArray["exclusionsValue"][i - 1] %>" /></span>
                                <%--<span class="fleft width-300""><input id="Text1" name="ExclusionsText-<%=i %>" value='<%=Page.Request["ExclusionsText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                <i style="display: none;" class="fleft margin-left-10" id="ExclusionsImg-<%=i %>">
                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Exclusions-<%=i %>','text')" /></i>
                            </p>
                            <%}
                              else
                              { %>
                            <p class="fleft width-100 padding-top-5" id="Exclusions-<%=i %>">
                                <span class="fleft width-300">
                                    <input maxlength="4000" onkeypress="return IsAlphaNumeric(event);" id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>" class="width-300 fleft form-control"
                                        type="text" value="<%=PackageVariablesArray["exclusionsValue"][i - 1] %>" /></span>
                                <%--<span class="fleft width-300"><input id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>"  value='<%=Page.Request["ExclusionsText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                <i class="fleft margin-left-10" id="ExclusionsImg-<%=i %>">
                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Exclusions-<%=i %>','text')" /></i>
                            </p>
                            <%} %>
                            <%} %>
                            <%} %>
                        </div>
                       
                       
<div class="clearfix"> </div>
                        <div class="marbot_20">
                       
                       <a href="javascript:void(0)" onclick="javascript:AddTextBox('Exclusions')"><span class=" glyphicon glyphicon-plus-sign"></span> Add More</a>
                       
                       
                     

                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="5">
                    <asp:HiddenField runat="server" ID="hdfinclCounter" Value="2" />
                    <asp:HiddenField runat="server" ID="hdfExclCounter" Value="2" />
                    <asp:HiddenField runat="server" ID="hdfCanPolCounter" Value="2" />
                    <asp:HiddenField runat="server" ID="hdfThingsCounter" Value="2" />
                    <asp:HiddenField runat="server" ID="hdfUnavailDateCounter" Value="2" />
                    <asp:HiddenField runat="server" ID="hdfinclusion" Value="" />
                    <asp:HiddenField runat="server" ID="hdfExclusions" Value="" />
                    <asp:HiddenField runat="server" ID="hdfCanPolicy" Value="" />
                    <asp:HiddenField runat="server" ID="hdfThings" Value="" />
                    <asp:HiddenField runat="server" ID="hdfUnavailDates" Value="" />
                    <%--<td align="left"><asp:TextBox ID="txtInclusion" runat="server"></asp:TextBox></td>
    <td><asp:Button ID="btnInclusion" Text ="Add" runat="server" Width="68"
            onclick="btnInclusion_Click" /></td>--%>
                    <%-- <input name="tst" type="text" value='<%=Page.Request["tst"] %>' />--%>
                    <div style="display: none;">
                        <input type="hidden" id="InclusionsCounter" name="hdInclusionsCounter" value="<%=PackageVariables["inclusionsCount"] %>" />
                        <input type="hidden" id="InclusionsCurrent" name="InclusionsCurrent" value="<%=PackageVariables["inclusionsCount"] %>" />
                    </div>
                    <div class="col-md-12">

                    <div>  <b>Inclusions :<sup><span class="req-star" style="color:red">*</span></sup></b></div>
                        <div id="InclusionsDiv">
                           
                            <%for (int i = 1; i < Convert.ToInt32(PackageVariables["inclusionsCount"]); i++)
                              {
                                  if (i == 1)
                                  { %>
                            <p class="fleft">
                                <span style="width: 470px" class="fleft">
                                    <input maxlength="4000" onkeypress="return IsAlphaNumeric(event);" type="text" id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>"
                                        class="width-300 fleft form-control" value="<%=PackageVariablesArray["inclusionsValue"][i - 1] %>" /></span>
                            </p>
                            <%}
                            else
                            { %>
                            <%if (i < Convert.ToInt32(PackageVariables["inclusionsCount"]) - 1)
                              { %>
                            <p class="fleft width-100 padding-top-5" id="Inclusions-<%=i %>">
                                <%--<span class="fleft"><input id="Text1" name="InclusionsText-<%=i %>" value='<%=Page.Request["InclusionsText-"+i] %>' class="width-300 fleft" type="text" value="<%=PackageVariablesArray["inclusionsValue"][i - 1] %>" /></span>--%>
                                <span class="fleft width-300"">
                                    <input maxlength="4000" onkeypress="return IsAlphaNumeric(event);" id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" value="<%=PackageVariablesArray["inclusionsValue"][i - 1] %>"
                                        class="width-300 fleft form-control" type="text" /></span> <i style="display: none;" class="fleft margin-left-10"
                                            id="InclusionsImg-<%=i %>">
                                            <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Inclusions-<%=i %>','text')" /></i>
                            </p>
                            <%}
                              else
                              { %>
                            <p class="fleft width-100 padding-top-5" id="Inclusions-<%=i %>">
                                <%--<span class="fleft width-300"><input id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" value='<%=Page.Request["InclusionsText-"+i] %>' class="width-300 fleft" type="text" value="<%=PackageVariablesArray["inclusionsValue"][i - 1] %>" /></span>--%>
                                <span class="fleft width-300">
                                    <input maxlength="4000" onkeypress="return IsAlphaNumeric(event);" id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" value="<%=PackageVariablesArray["inclusionsValue"][i - 1] %>"
                                        class="width-300 fleft form-control" type="text" /></span> <i class="fleft margin-left-10" id="InclusionsImg-<%=i %>">
                                            <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Inclusions-<%=i %>','text')" /></i>
                            </p>
                            <%} %>
                            <%} %>
                            <%} %>
                        </div>



                        <div class="clearfix"></div>
                        <div class="marbot_20">
                            



                            <a href="javascript:void(0)" onclick="javascript:AddTextBox('Inclusions')" ><span class=" glyphicon glyphicon-plus-sign"></span> Add More</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="5" style="height: 21px">
                    <div style="display: none;">
                        <input type="hidden" id="CancellPolicyCounter" value="<%=Convert.ToInt32(PackageVariables["CancellPolicyCount"])%>" />
                        <input type="hidden" id="CancellPolicyCurrent" name="CancellPolicyCurrent" value="<%=Convert.ToInt32(PackageVariables["CancellPolicyCount"])%>" />
                    </div>
                    <div class="col-md-12">

                        <div>  <b>Cancellation Policy :</b><span class="req-star" style="color:red">*</span></div>

                        <div id="CancellPolicyDiv">
                           
                            <%for (int i = 1; i < Convert.ToInt32(PackageVariables["CancellPolicyCount"]); i++)
                              {
                                  if (i == 1)
                                  { %>
                            <p class="fleft">
                                <%-- <span style=" width:470px;" class="fleft"><input style=" width:460px" type="text" id="CancellPolicyText-<%=i %>" value='<%=Page.Request["CancellPolicyText-"+i] %>' name="CancellPolicyText-<%=i %>"  class="width-300 fleft" value="<%=PackageVariablesArray["CancellPolicyValue"][i - 1] %>" /></span>--%>
                                <span style="width: 470px;" class="fleft">
                                    <input maxlength="4000" onkeypress="return IsAlphaNumeric(event);"  type="text" id="CancellPolicyText-<%=i %>" name="CancellPolicyText-<%=i %>"
                                        class="width-300 fleft form-control" value="<%=PackageVariablesArray["CancellPolicyValue"][i - 1] %>" /></span>
                            </p>
                            <%}
                            else
                            { %>
                            <%if (i < Convert.ToInt32(PackageVariables["CancellPolicyCount"]) - 1)
                              { %>
                            <p class="fleft width-100 padding-top-5" id="CancellPolicy-<%=i %>">
                                <span class="fleft width-300">
                                    <input maxlength="4000" id="CancellPolicyText-<%=i %>" name="CancellPolicyText-<%=i %>" class="width-300 fleft form-control" type="text"
                                        value="<%=PackageVariablesArray["CancellPolicyValue"][i - 1] %>" /></span>
                                <%-- <span class="fleft width-300"><input id="Text8" name="CancellPolicyText-<%=i %>" value='<%=Page.Request["CancellPolicyText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                <i style="display: none;" class="fleft margin-left-10" id="CancellPolicyImg-<%=i %>">
                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('CancellPolicy-<%=i %>','text')" /></i>
                            </p>
                            <%}
                              else
                              { %>
                            <p class="fleft width-100 padding-top-5" id="CancellPolicy-<%=i %>">
                                <span class="fleft width-300">
                                    <input maxlength="4000" id="CancellPolicyText-<%=i %>" name="CancellPolicyText-<%=i %>" class="width-300 fleft form-control"
                                        type="text" value="<%=PackageVariablesArray["CancellPolicyValue"][i - 1] %>" /></span>
                                <%--<span class="fleft width-300"><input id="Text7" name="CancellPolicyText-<%=i %>" value='<%=Page.Request["CancellPolicyText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                <i class="fleft margin-left-10" id="CancellPolicyImg-<%=i %>">
                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('CancellPolicy-<%=i %>','text')" /></i>
                            </p>
                            <%} %>
                            <%} %>
                            <%} %>
                        </div>

                       

                     <div class="marbot_20">


                        <a href="javascript:void(0)" onclick="javascript:AddTextBox('CancellPolicy')"  class="pull-left"><span class=" glyphicon glyphicon-plus-sign"></span>Add More</a>

 <div class="clearfix"> </div>
                          
                        </div>
                    </div>
                </td>
            </tr>
              <tr>
               <%-- <td align="right">
                    <asp:Label ID="lblItinerary2" runat="server" Text="Itinerary 2:" Visible="false"></asp:Label>
                </td>
                <td align="left" style="width: 521px">
                    <asp:TextBox ID="txtItinerary2"  runat="server" Width="210px"
                        TextMode="MultiLine" Height="100px" Visible="false"></asp:TextBox>
                </td>--%>
            </tr>
           </table>
                
                </div>



        


                   <div class="row"> 

                   
                   
                   <table> 
            
            <tr>
                <td>
                </td>
                <td colspan="5" style="height: 21px">
                    <div style="display: none;">
                        <input type="hidden" id="ThingsToBringCounter" value="<%=Convert.ToInt32(PackageVariables["ThingsToBringCount"])%>" />
                        <input type="hidden" id="ThingsToBringCurrent" name="ThingsToBringCurrent" value="<%=Convert.ToInt32(PackageVariables["ThingsToBringCount"])%>" />
                    </div>
                    <div class="col-md-12">

                    <div>  <b>Things to Bring :</b></div>
                        <div id="ThingsToBringDiv">
                           
                            <%for (int i = 1; i < Convert.ToInt32(PackageVariables["ThingsToBringCount"]); i++)
                              {
                                  if (i == 1)
                                  { %>
                            <p class="fleft">
                                <%-- <span style=" width:470px;" class="fleft"><input style=" width:460px" type="text" id="Text3" name="ThingsToBringText-<%=i %>" value='<%=Page.Request["ThingsToBringText-"+i] %>' class="width-300 fleft" value="<%=PackageVariablesArray["ThingsToBringValue"][i - 1] %>" /></span>--%>
                                <span style="width: 470px;" class="fleft">
                                    <input maxlength="4000"  type="text" onkeypress="return IsAlphaNumeric(event);" id="Text3" name="ThingsToBringText-<%=i %>"
                                        class="width-300 fleft form-control" value="<%=PackageVariablesArray["ThingsToBringValue"][i - 1] %>" /></span>
                            </p>
                            <%}
                            else
                            { %>
                            <%if (i < Convert.ToInt32(PackageVariables["ThingsToBringCount"]) - 1)
                              { %>
                            <p id="ThingsToBring-<%=i %>">
                                <span class="fleft width-300">
                                    <input maxlength="4000" onkeypress="return IsAlphaNumeric(event);" id="Text2" name="ThingsToBringText-<%=i %>" class="width-300 fleft form-control" type="text"
                                        value="<%=PackageVariablesArray["ThingsToBringValue"][i - 1] %>" /></span>
                                <%--<span class="fleft width-300"><input id="Text2" name="ThingsToBringText-<%=i %>" value='<%=Page.Request["ThingsToBringText-"+i] %>' class="width-300 fleft" type="text" /></span>--%>
                                <i style="display: none;" class="fleft margin-left-10" id="ThingsToBringImg-<%=i %>">
                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('ThingsToBring-<%=i %>','text')" /></i>
                            </p>
                            <%}
                              else
                              { %>
                            <p class="fleft width-100 padding-top-5"  id="ThingsToBring-<%=i %>">
                                <span class="fleft width-300">
                                    <input maxlength="4000" onkeypress="return IsAlphaNumeric(event);" id="ThingsToBringText-<%=i %>" name="ThingsToBringText-<%=i %>" class="width-300 fleft  form-control"
                                        type="text" value="<%=PackageVariablesArray["ThingsToBringValue"][i - 1] %>" /></span>
                                <%--<span class="fleft width-300"><input id="ThingsToBringText-<%=i %>" name="ThingsToBringText-<%=i %>"value='<%=Page.Request["ThingsToBringText-"+i] %>'  class="width-300 fleft" type="text"  /></span>--%>
                                <i class="fleft margin-left-10" id="ThingsToBringImg-<%=i %>">
                                    <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('ThingsToBring-<%=i %>','text')" /></i>
                            </p>
                            <%} %>
                            <%} %>
                            <%} %>
                        </div>
                        <div class="clearfix"></div>
                        <div class="marbot_20">
                           
                           
                          


                            <a href="javascript:void(0)" onclick="javascript:AddTextBox('ThingsToBring')"><span class="glyphicon glyphicon-plus-sign"></span> Add More</a>
                        </div>
                    </div>
                </td>
            </tr>
           
           </table>
                   
                   
                   
                   </div>





                   <div class="row"> 



                   <div class="col-md-3"> 
                   
                   <div> <asp:Label Font-Bold="true" ID="lblBookingCutOff" runat="server" Text="Booking CutOff:"> </asp:Label></div>

                   <div> <asp:DropDownList ID="ddlBookingCutOff" runat="server" CssClass="form-control" Width="210px">
                <asp:ListItem Text="1" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                </asp:DropDownList></div>
                   
                   
                   </div>

                   </div>
                </div>



              </div>
                   
         </div>




    
        <table cellpadding="0" width="100%" cellspacing="0"  border="0">
            
            
            
            
            <tr>
                <td>
                    <div id="errMess" class="error_module" style="display: none;">
                        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding-top: 5px" align="right">
                
                
                
                <table id="tblControls" border="0" cellspacing="0" cellpadding="0" style="display:none">
  <tr>
    <td>  <asp:Label ID="lblTotal" Text="Total:" runat="server" CssClass="label"></asp:Label></td>
 <td> <asp:TextBox ID="txtTotal" runat="server" Enabled="false" CssClass="inputDisabled form-control"
                        Width="100px"></asp:TextBox> </td>
                        
    <td>  <input class="fright button" type="button" id="SaveButton" value="Save" onclick="javascript:Save()" />
    
    
    
    </td>
    
    <td> <asp:UpdatePanel ID="UpdatePanel1_btnClear" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnClear" Text="Clear" runat="server" Font-Bold="true" CssClass="button" OnClick="btnClear_Click">
                            </asp:Button>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnClear" />
                        </Triggers>
                    </asp:UpdatePanel></td>

  </tr>
</table>
        
                    
                </td>
            </tr>
        </table>

        <div id="tabNavControls">
            <div class="row">
                <div class="col-md-8">

                </div>
                <div class="col-md-2">
                    <input id="btnSubmit" type="button" value="Submit" onclick="navigateTabs()" class="but but_b pull-right btn-xs-block margin-top-10" />

                </div>
                <div class="col-md-2">
                    <input id="btnClearTabs" type="button" value="Clear" onclick="clearTabs()" class="but but_b pull-left btn-xs-block margin-top-10" />
                </div>

            </div>
            
            

            

            

        </div>

  </div>


<script>
    $(function () {
        var dataCloneBtn = $('[data-clone-button]'),
            dataCloneid = 0;
        dataCloneBtn.each(function () {
            var $this = $(this),
                btnID = $this.attr('data-clone-button'),
                dataCloneCnt = $('[data-clone-content="' + btnID + '"]');
            dataCloneCnt.attr('id', btnID + '-' + ++dataCloneid);
            $this.click(function () {
                dataCloneCnt.clone(true, true).insertBefore($this);
                dataCloneCnt.attr('id', btnID + '-' + ++dataCloneid);
            })
        })
    })
</script>

    
    
    


        
        
  
        
        
        
    
  
    
    
    
<iframe id="IShimFrame" style="position:absolute; display:none;" frameborder="0"></iframe>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

	
