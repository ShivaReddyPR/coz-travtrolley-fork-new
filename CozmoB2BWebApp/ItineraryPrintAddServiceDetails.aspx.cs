﻿using System;
using System.Data;
using System.Web.UI;
using CT.Core;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

public partial class ItineraryPrintAddServiceDetails : System.Web.UI.Page
{
    protected DataTable dtRecords;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString.Count > 0 && Request.QueryString["addId"] != null)
                    {
                        GetDetails(Convert.ToInt32(Request.QueryString["addId"]));
                        //Update the agent balance

                        AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);
                        agent.CreatedBy = Settings.LoginInfo.UserID;
                        decimal currentAgentBalance = agent.UpdateBalance(0);
                        Settings.LoginInfo.AgentBalance = currentAgentBalance;
                        hdfCurBal.Value = Settings.LoginInfo.AgentBalance.ToString("N" + Settings.LoginInfo.DecimalValue);
                        Utility.StartupScript(this.Page, "updateAgentBalance();", "SCRIPT");
                    }
                }
            }
            else
            {
                Response.Redirect("SessionExpired.aspx");
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "(ItineraryPrintAddServiceDetails Page)" + ex.ToString(), "0");

        }

    }
    private void GetDetails(int addId)
    {
        try
        {
            dtRecords = ItineraryAddServiceDetails.GetServiceDetailsByServiceId(addId);

            if (dtRecords != null && dtRecords.Rows.Count > 0)
            {
                foreach (DataRow dr in dtRecords.Rows)
                {
                    if (dr["pax_name"] != DBNull.Value)
                    {
                        lblPaxName.Text = Convert.ToString(dr["pax_name"]);
                    }
                    if (dr["service_name"] != DBNull.Value)
                    {
                        lblServiceName.Text = Convert.ToString(dr["service_name"]);
                    }
                    if (dr["traveldate"] != DBNull.Value)
                    {
                        lblTravelDate.Text = Convert.ToString(CZDateFormat(dr["traveldate"]));
                    }
                    if (dr["countryName"] != DBNull.Value)
                    {
                        lblNationality.Text = Convert.ToString(dr["countryName"]);
                    }
                    
                    if (dr["pax_passport_no"] != DBNull.Value)
                    {
                        lblPassportNumber.Text = Convert.ToString(dr["pax_passport_no"]);
                    }
                    if (dr["routing"] != DBNull.Value)
                    {
                        lblRouting.Text = Convert.ToString(dr["routing"]);
                    }
                    if (dr["remarks"] != DBNull.Value)
                    {
                        lblRemarks.Text = Convert.ToString(dr["remarks"]);
                    }
                    if (dr["DocNo"] != DBNull.Value)
                    {
                        lblRefNo.Text = Convert.ToString(dr["DocNo"]);
                    }
                    if (dr["PNR"] != DBNull.Value)
                    {
                        lblPNR.Text = Convert.ToString(dr["PNR"]);
                    }
                }
            }
        }
        catch
        {
            throw;
        }
    }
    protected string CZDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string CZDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }
}
