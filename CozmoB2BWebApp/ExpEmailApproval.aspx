﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExpEmailApproval.aspx.cs" Inherits="CozmoB2BWebApp.ExpEmailApproval" %>

<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <link href="build/css/main.min.css?V1" rel="stylesheet" type="text/css" />
    <link href="css/override.css" rel="stylesheet" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />    
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="#" rel="shortcut icon" />
    <style>
        @media print{.receipt-wrapper{color:#000!important}}
        @media print{.receipt-visitor-info th{color:#000!important}}
        @media print{.receipt-wrapper{font-family:Arial,Helvetica,sans-serif;width:100%}.receipt-visitor-info th{color:#fff;text-shadow:0 0 0 #cfffcc}}
        @media print and (-webkit-min-device-pixel-ratio:0){.receipt-visitor-info th{color:#fff;-webkit-print-color-adjust:exact}}
    </style>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/Common/Common.js" ></script>
    <script type="text/javascript" src="Scripts/Menu/query2.js"></script>
    <script src="scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="Scripts/bootstrap.min.js"></script>
    <script type="text/javascript">

        var mt = ''; var apiHost = mt; var bearer = mt; var cntrllerPath = 'api/expTransactions';         
        var apiAgentInfo = {}; var selExpRepId = 0; var selExpDtlId = 0; var authError = '';
        var repAction = mt; var approverId = 0; var webAppHost = mt;

        $(document).ready(function() {

            authError = '<%=authError%>';
            approverId = '<%=Request.QueryString["DIRPPA"] == null ? "0" : Request.QueryString["DIRPPA"]%>';

            /* Check login user authentication error */
            if (!IsEmpty(authError)) {

                alert(authError);
                return;
            }
            
            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();
            
            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo) || approverId == 0) {

                alert('User authentication failed.');
                return;
            }
            
            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            /* Check query string to see to approve/reject expenses */
            selExpRepId = '<%=Request.QueryString["DIPERPXE"] == null ? "0" : Request.QueryString["DIPERPXE"]%>';
            selExpDtlId = '<%=Request.QueryString["DILTDPXE"] == null ? "0" : Request.QueryString["DILTDPXE"]%>';
            repAction = '<%=Request.QueryString["TATS"] == null ? string.Empty : Request.QueryString["TATS"]%>';            
            webAppHost = '<%=Request.Url.Scheme + "://" + Request["HTTP_HOST"]%>';

            /* Check the expense ref no */
            if (selExpRepId == 0 && selExpDtlId == 0) {

                alert('Invalid expense reference no.');
                return;
            }

            /* To show comments section for reject/revise status */
            if (repAction != 'A') {

                $('#lnkSubmit').html(repAction == 'RJ' ? 'Reject' : 'Revise');
                $('#lnkSubmit').addClass(repAction == 'RJ' ? 'btn-danger' : 'btn-primary');
                $('#divComments').show();
                return false;
            }                

            /* Show approval confirmation popup */
            $('#divApproveConfirm').modal('show');
        });

        /* To Update approval/reject status */
        function UpdateReportStatus() {

            $('#divApproveConfirm').modal('hide');
                        
            if (repAction != 'A' && IsEmpty($('#txtComments').val())) {

                alert('Please enter comments.');
                return;
            }

            var ExpReps = [];

            ExpReps.push({
                ApprovalStatus: repAction,
                ExpDetailId: parseInt(selExpDtlId),
                expApproverId: approverId,
                ProfileID: 0,
                CreatedBy: apiAgentInfo.LoginUserId,
                ExpRepId: parseInt(selExpRepId),
                Remarks: $('#txtComments').val()
            });
                        
            var reqData = { AgentInfo: apiAgentInfo, ApproverReports: ExpReps, ApproverQueue: null, type: 'AQ', Host: webAppHost, ActionFrom: 'Mail' };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveExpApprovalReports';
            WebApiReq(apiUrl, 'POST', reqData, '', ApproverSuccess, null, null);                
        }

        /* To bind expense info */
        function ApproverSuccess(resp) {

            if (!IsEmpty(resp) && resp.length > 0) {

                alert('Failed to update status, as expense is already ' + (resp[0].approvalStatus == 'A' ? 'APPROVED' : 'REJECTED')
                    + ' by ' + resp[0].updatedUser + ' on ' + resp[0].updatedDate + '.');
            } else {

                $('#txtComments').html('');
                $('#divComments').hide();
                alert('Expense(s) ' + (repAction == 'A' ? 'approved' : repAction == 'RJ' ? 'rejected' : repAction == 'RV' ? 'revised' : 'status updated') + ' successfully.');
            }
        }

        /* To get agent and login info */
        function GetAgentInfo() {

            try {

                var loginInfo = '<%=Settings.LoginInfo == null%>';

                if (loginInfo == true)
                    return apiAgentInfo;

                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {

            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';

            if (IsEmpty(apiHost))                                 
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";

            return apiHost;
        }

    </script>
</head>
<body>
    <form id="form1">
        <div id="divComments" style="display:none;">
            <label>Comments</label>
            <textarea class="form-control" style="width:500px;margin-bottom:20px;" id="txtComments" placeholder="Enter Comments"></textarea>
            <a id="lnkSubmit" class="btn" onclick="return UpdateReportStatus();"></a>
        </div>
        <div id="ctl00_upProgress" style="display:none">                        
            <div class="lds-loader-wrap">
                <div class="lds-loader-inner"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
            </div>
        </div>
        <!-- Expense approve confirm popup -->
        <div class="modal fade" id="divApproveConfirm" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background: #388c0f;color: white">
                        <span id="title" style="color: white; text-align: center; margin-top: 15px">Approver Electronic Agreement</span>
                        <button type="button" class="close" data-dismiss="modal" style="height: 10px; width: 10px" aria-label="Close"><span aria-hidden="true">X</span></button>
                    </div>
                     <div class="modal-body" id="modalReports"><ul><li>By clicking "Accept" I hereby certify that the expenses in this report are directly linked to AAME business.</li></ul></div>
                    <div class="modal-footer">
                        <a class="btn btn-danger mr-2" data-dismiss="modal" >Cancel</a>
                        <a class="btn btn-info mr-2" onclick="return UpdateReportStatus()">Accept & Approve</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
