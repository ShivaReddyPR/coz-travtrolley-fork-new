using System;
using System.Collections.Generic;
using System.Configuration;
using CT.Core;
using CT.BookingEngine;

public partial class EmailAjax :System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["submitActivityEnquiry"] != null && Convert.ToBoolean(Request["submitActivityEnquiry"]))
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            PackageQueries query = new PackageQueries();
            query.DealId = Convert.ToInt32(Request["dealId"]);
            query.AgencyId = Convert.ToInt32(Request["agencyId"]);
            query.DealName = Request["activityName"].Trim();
            query.CityOfResidence = Request["City"];
            query.CountryOfResidence = Request["Country"];
            query.PaxName = Request["paxName"].Trim();
            query.Email = Request["EmailId"].Trim();
            query.Phone = Request["phoneNo"].Trim();
            query.PaxMessage = Request["description"].Replace("\r\n", " ").Trim();
            query.Adults = Convert.ToInt32(Request["Noofpax"]);
            query.ProductType = Request["productType"].Trim();
            query.DepartureDate = DateTime.Now;
            query.CreatedBy = (int)CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
            query.RoomCount = 0;
            query.TransType = "B2B";
            int rowsAffected = query.Save();
            string responseMessage = string.Empty;
            string EmailText = "<table style='width:50%;'><tr><td>Name:</td><td>" + Request["paxName"].Trim() + "</td></tr>"
                                + "<tr><td>Email ID</td><td><a href='mailto:" + Request["EmailId"].Trim() + "'>" + Request["EmailId"].Trim() + "</a></td></tr>"
                                + "<tr><td>Country of Residence</td><td>" + Request["Country"] + "</td></tr>"
                                + "<tr><td>City of Residence</td><td>" + Request["City"] + "</td></tr>"
                                + "<tr><td>No of Passengers</td><td>No of Passengers: " + Request["NoofPax"] + "</td></tr>"
                                + "<tr><td>Phone Number</td><td>" + Request["phoneNo"] + "</td></tr>"
                                + "<tr><td colspan='2'>" + Request["description"] + "</td></tr><tr><td>Greetings!</br>Cozmo Travel</td></tr></table>";
            if (rowsAffected > 0)
            {
                responseMessage = "true";

               // Email.Send(ConfigurationManager.AppSettings["fromEMail"], ConfigurationManager.AppSettings["holidaySupport"], new List<string>(), Request["packageName"], EmailText, new Hashtable());
            }
            else
            {
                responseMessage = "false";
            }
            Response.Write(responseMessage);
        }
        else if (Request["submitEnquiry"] != null && Convert.ToBoolean(Request["submitEnquiry"]))
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            PackageQueries query = new PackageQueries();
            query.DealId = Convert.ToInt32(Request["packageId"]);
            query.AgencyId = Convert.ToInt32(Request["agencyId"]);
            query.DealName = Request["packageName"].Trim();
            query.CityOfResidence = Request["city"];
            query.CountryOfResidence = Request["country"];
            query.PaxName = Request["paxName"].Trim();
            query.Email = Request["EmailId"].Trim();
            query.Phone = Request["phoneNo"].Trim();
            query.PaxMessage = Request["remarks"].Replace("\r\n", " ").Trim();
            query.DepartureDate = Convert.ToDateTime(Request["depDate"], dateFormat);
            query.Adults = Convert.ToInt32(Request["adults"]);
            query.Childs = Convert.ToInt32(Request["childs"]);
            query.Infants = Convert.ToInt32(Request["infants"]);
            query.ProductType = Request["productType"].Trim();
            query.CreatedBy = (int)CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
            query.RoomCount = 0;
            query.TransType = "B2B";
            int rowsAffected = query.Save();
            string responseMessage = string.Empty;
            string EmailText = "<table style='width:50%;'><tr><td>Name:</td><td>" + Request["paxName"].Trim() + "</td></tr>"
                                + "<tr><td>Email ID</td><td><a href='mailto:" + Request["EmailId"].Trim() + "'>" + Request["EmailId"].Trim() + "</a></td></tr>"
                                + "<tr><td>Country of Residence</td><td>" + Request["country"] + "</td></tr>"
                                + "<tr><td>City of Residence</td><td>" + Request["city"] + "</td></tr>"
                                + "<tr><td>Departure Date</td><td>" + Request["depDate"] + "</td></tr>"
                                + "<tr><td>No of Passengers</td><td>Adult: " + Request["adults"] + " Child: " + Request["childs"] + " Infant: " + Request["infants"] + "</td></tr>"
                                + "<tr><td>Phone Number</td><td>" + Request["phoneNo"] + "</td></tr>"
                                + "<tr><td colspan='2'>" + Request["remarks"] + "</td></tr><tr><td>Greetings!</br>Cozmo Travel</td></tr></table>";
            if (rowsAffected > 0)
            {
                responseMessage = "true";
                List<string> toArray =new List<string>();

                if(!string.IsNullOrEmpty(ConfigurationManager.AppSettings["holidaySupport"]))
                {
                    toArray.Add(ConfigurationManager.AppSettings["holidaySupport"]);

                }
                Email.Send(ConfigurationManager.AppSettings["fromEMail"], ConfigurationManager.AppSettings["replyTo"], toArray, Request["packageName"], EmailText, new System.Collections.Hashtable());
            }
            else
            {
                responseMessage = "false";
            }
            Response.Write(responseMessage);
        }
        else if (Request["FDNoStockEnquiry"] != null && Convert.ToBoolean(Request["FDNoStockEnquiry"]))
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            PackageQueries query = new PackageQueries();
            query.DealId = Convert.ToInt32(Request["dealId"]);
            query.AgencyId = Convert.ToInt32(Request["agencyId"]);
            query.DealName = Request["packageName"].Trim();
            query.CityOfResidence = string.Empty;
            query.CountryOfResidence = string.Empty;
            query.PaxName = Request["paxName"].Trim();
            query.Email = Request["EmailId"].Trim();
            query.Phone = Request["phoneNo"].Trim();
            query.PaxMessage = "Stcok Not Available";
            query.DepartureDate = Convert.ToDateTime(Request["depDate"], dateFormat);
            query.Adults = Convert.ToInt32(Request["adults"]);
            query.Childs = Convert.ToInt32(Request["childs"]);
            query.Infants = Convert.ToInt32(Request["infants"]);
            query.ProductType = Request["productType"].Trim();
            query.CreatedBy = (int)CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
            query.RoomCount= Convert.ToInt32(Request["roomCount"]);
            query.TransType = "B2B";
            int rowsAffected = query.Save();
            string responseMessage = string.Empty;
            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            toArray.Add(query.Email);
            string EmailText = "<table style='width:50%;'><tr><td>Dear Admin</td></tr>"
                                + "<tr><td></td></tr>"
                                + "<tr><td></td></tr>"
                                + "<tr><td colspan='2'>Please find below Enquiry for Package Name where the required stock is not available.</td><tr>"
                                + "<tr><td></td></tr>"
                                + "<tr><td></td></tr>"
                                +"<tr><td>Name:</td><td>" + Request["paxName"].Trim() + "</td></tr>"
                                + "<tr><td>Email ID</td><td><a href='mailto:" + Request["EmailId"].Trim() + "'>" + Request["EmailId"].Trim() + "</a></td></tr>"
                                + "<tr><td>Departure Date</td><td>" + Request["depDate"] + "</td></tr>"
                                + "<tr><td>No of Passengers</td><td>Adult: " + Request["adults"] + " Child: " + Request["childs"] + " Infant: " + Request["infants"] + "</td></tr>"
                                + "<tr><td>Package Name</td><td>" + Request["packageName"] + "</td></tr>"
                                + "<tr><td>Phone Number</td><td>" + Request["phoneNo"] + "</td></tr>"
                                + "<tr><td colspan='2'>" + Request["remarks"] + "</td></tr>"
                                +"<tr><td>Greetings! </br>Cozmo Travel</td></tr></table>";
            if (rowsAffected > 0)
            {
               responseMessage = "true";
               CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEMail"], ConfigurationManager.AppSettings["holidaySupport"], new List<string>(), "Stock Not Available", EmailText, new System.Collections.Hashtable(),string.Empty);
               string message = ConfigurationManager.AppSettings["StockNotAvailable"];
               CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEMail"], ConfigurationManager.AppSettings["holidaySupport"], toArray, "Stock Not Available", message, new System.Collections.Hashtable(), string.Empty);
            }
            else
            {
                responseMessage = "false";
            }
            Response.Write(responseMessage);
        }
    }
}
