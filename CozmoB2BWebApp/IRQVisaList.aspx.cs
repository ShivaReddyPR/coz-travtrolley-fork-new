﻿using CT.Configuration;
using CT.Core;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class IRQVisaList : CT.Core.ParentPage
{
    protected DataTable visaList = new DataTable();
    protected int recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["BookingQueueRecordsPerPage"]);
    protected int noOfPages;
    protected int pageNo;
    protected string show = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            if (!IsPostBack)
            {
                InitializePageControls();
                
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            //throw new Exception;
        }

    }

    private void InitializePageControls()
    {
        txtFromDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
        txtToDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
        BindVisaTypes();
        BindB2BAgent(Settings.LoginInfo.AgentId);

        getVisaList();
    }

    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            if (dtAgents.Rows != null && dtAgents.Rows.Count > 0)
            {
                ddlB2BAgent.DataSource = dtAgents;
                ddlB2BAgent.DataTextField = "Agent_Name";
                ddlB2BAgent.DataValueField = "agent_id";
                ddlB2BAgent.DataBind();
                ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
                ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
                divAgentNames.Style.Add("display","block");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindVisaTypes()
    {
        DataTable visaMaster = IRQApplicantDetails.GetVisaTypes();
        if (visaMaster != null && visaMaster.Rows != null && visaMaster.Rows.Count > 0)
        {
            ddlVisaType.DataSource = visaMaster;
            ddlVisaType.DataValueField = "VisaTypeID";
            ddlVisaType.DataTextField = "VisaType";
            ddlVisaType.DataBind();
            ddlVisaType.Items.Insert(0, new ListItem("Select", "-1"));

        }
    }

    private void getVisaList()
    {
        IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");

        string docNumber = txtDocNumber.Value;
        int agentId = Settings.LoginInfo.AgentId;
        if (ddlB2BAgent != null && ddlB2BAgent.SelectedValue != "")
        {
            agentId = Convert.ToInt32(ddlB2BAgent.SelectedValue);
        }
        string visaStatus = Convert.ToString(ddlVisaStatus.SelectedValue);
        int visaType = Convert.ToInt32(ddlVisaType.SelectedValue);
        DateTime fromDate = Convert.ToDateTime(txtFromDate.Value, provider);
        DateTime toDate = Convert.ToDateTime(txtToDate.Value, provider);
        IRQHeader header = new IRQHeader();
        
        visaList = header.GetVisaList(fromDate, toDate, docNumber, visaStatus, agentId, visaType);
        string url = "VisaList.aspx";
        int queueCount = visaList.Rows.Count;

        if ((queueCount % recordsPerPage) > 0)
        {
            noOfPages = (queueCount / recordsPerPage) + 1;
        }
        else
        {
            noOfPages = (queueCount / recordsPerPage);
        }

        if (noOfPages > 1)
        {
            if (PageNoString.Value != null)
            {
                pageNo = Convert.ToInt16(PageNoString.Value);
            }
            else
            {
                pageNo = 1;
            }
        }
        else
        {
            pageNo = 1;
        }
        if (queueCount > 0 && pageNo > 1)
        {
            show = MetaSearchEngine.PagingJavascript(noOfPages, url, pageNo);
        }


        dlVisaBookings.DataSource = visaList;
        dlVisaBookings.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            getVisaList();    
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "");
            throw;
        }
    }

    protected void dlVisaBookings_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
                DataRowView visaList = e.Item.DataItem as DataRowView;
                Label lblVisaType = e.Item.FindControl("lblVisaType") as Label;
                Label lblVisaPrice = e.Item.FindControl("lblVisaPrice") as Label;
                Label lblVisaCurrency = e.Item.FindControl("lblVisaCurrency") as Label;
                Label lblVisaStatus = e.Item.FindControl("lblVisaStatus") as Label;
                Label lblLocationName = e.Item.FindControl("lblLocationName") as Label;
                Label lblDocNumber = e.Item.FindControl("lblDocNumber") as Label;
                Label lblPaxName = e.Item.FindControl("lblPaxName") as Label;
                Button btnUpdate = e.Item.FindControl("btnUpdate") as Button;
                HtmlGenericControl divUpdateStatus = e.Item.FindControl("divUpdateStatus") as HtmlGenericControl;
                
                if (visaList["VisaType"] != DBNull.Value)
                {
                    lblVisaType.Text = visaList["VisaType"].ToString();
                }
                if (visaList["VisaPrice"] != DBNull.Value) {
                    lblVisaPrice.Text = visaList["VisaPrice"].ToString();
                }
                if (visaList["Currency"] != DBNull.Value) {
                    lblVisaCurrency.Text = visaList["Currency"].ToString();
                }
                if (visaList["VisaStatusName"] != DBNull.Value)
                {
                    lblVisaStatus.Text = visaList["VisaStatusName"].ToString();
                }
                if (visaList["location_name"] != DBNull.Value)
                {
                    lblLocationName.Text = visaList["location_name"].ToString();
                }
                if (visaList["DocNumber"] != DBNull.Value)
                {
                    lblDocNumber.Text = visaList["DocNumber"].ToString();
                }
                if (visaList["ApplicantName"] != DBNull.Value)
                {
                    lblPaxName.Text = visaList["ApplicantName"].ToString();
                }
                divUpdateStatus.Visible = false;
                if (visaList["VisaStatus"] != DBNull.Value && visaList["VisaStatus"].ToString() == "I")
                {
                    if (visaList["ApplicantId"] != DBNull.Value)
                    {
                        btnUpdate.CommandArgument = visaList["ApplicantId"].ToString();
                        btnUpdate.Visible = true;

                        divUpdateStatus.Visible = true;
                    }
                }
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }

    protected void dlVisaBookings_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Update")
            {
                if (e.CommandArgument.ToString().Length > 0)
                {
                    DropDownList ddlApplicantStatus = e.Item.FindControl("ddlApplicantStatus") as DropDownList;
                    string updatedStatus = ddlApplicantStatus.SelectedValue;
                    long applicantId = Convert.ToInt32(e.CommandArgument);

                    IRQApplicantDetails.UpdateVisaStatus(applicantId, updatedStatus, Settings.LoginInfo.AgentId);

                    getVisaList();
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }
}
