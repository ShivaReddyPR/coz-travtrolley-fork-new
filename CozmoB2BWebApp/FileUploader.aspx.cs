using System;
using System.Web.UI;
using CT.TicketReceipt.Common;
public partial class FileUploader : Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (DocumentName == null)
        {
            lblError.Text="ERROR: Document Name is not passed !";
        }
    
    }
    //protected void Page1_Init(object sender, EventArgs e) 
    //{ 
    //    ScriptManager.GetCurrent(this).RegisterPostBackControl(fuDocument); 
    //} 

    private string DocumentName
    {
        get{ return Request.QueryString["docName"];}
    }

    protected void butPreview_Click(object sender, EventArgs e)
    {
        try
        {
            hdfUploaded.Value = "0";
            
            if (DocumentName == null)
            {
                lblError.Text = "ERROR: Document Name is not passed !";
            }
            else if (fuDocument.FileName == string.Empty)
            {
                lblError.Text = "Please select file to Upload !";
            }
            else
            {
                 long  RestrictSize = Utility.ToLong(System.Configuration.ConfigurationManager.AppSettings["UploadFileSize"]);
                if (RestrictSize != 0)
                {
                    long fileSize = Utility.ToLong(fuDocument.PostedFile.ContentLength) / 1024; //converting to KB
                    if (fileSize > RestrictSize)throw new Exception("File size should be less than or equal to " + RestrictSize + " KB !");
                        
                }                     
                                
                byte[] bytes = fuDocument.FileBytes;
                UploadDocument uploadInfo = new UploadDocument();
               // UploadDocument.saveFile(fuDocument);
                string url = UploadDocument.CreateImage(bytes, fuDocument.FileName); 
                if (url != null)
                {
                    uploadInfo.FileName = fuDocument.FileName;
                    uploadInfo.Content = bytes;
                    uploadInfo.ServerFileName = url;
                    uploadInfo.FileUploder = fuDocument;
                    DocumentInfo = uploadInfo;
                    hdfUploaded.Value = "1";
                    hdfFileName.Value = fuDocument.FileName;
                }
                else
                {
                    uploadInfo.FileName = string.Empty;
                    uploadInfo.Content = null;
                    uploadInfo.ServerFileName = "~/Images/Common/NoPreview.png";
                    DocumentInfo = uploadInfo;
                }
                imgPreview.ImageUrl = uploadInfo.ServerFileName;
                lblError.Text = string.Empty;
            }
        }
        catch (Exception ex) 
        {
            Utility.Alert(this.Page, ex.Message);
            lblError.Text = ex.Message;
        }
    }
    public UploadDocument DocumentInfo
    {
        get { return (UploadDocument)Session[DocumentName]; }
        set { Session[DocumentName] = value; }
    }
   
}
