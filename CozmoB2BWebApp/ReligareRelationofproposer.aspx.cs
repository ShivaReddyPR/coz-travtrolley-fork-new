﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using System.IO;
using System.Collections.Generic;
using ReligareInsurance;
using CT.Core;

namespace CozmoB2BWebApp
{
    public partial class ReligareRelationofproposer :CT.Core.ParentPage
    {
         
         

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
                

            }
           
        }
        #region Methods

        void Save()
        {
            try
            {
                Relationofproposer Rproposer=new Relationofproposer();
                if (Utility.ToInteger(hdfMid.Value) > 0)
                {
                    Rproposer.ID = Utility.ToLong(hdfMid.Value);
                }
                else
                {
                    Rproposer.ID = -1;
                }
                string relationcode = string.Empty;
                Rproposer.CODE = txtRelationCode.Text;
                Rproposer.RELATION = txtRelation.Text;
                Rproposer.CREATED_BY = Settings.LoginInfo.UserID;
                Rproposer.Save();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage("Details Successfully", "", hdfMid.Value == "0" ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);
                btnSave.Text = "Save";
                Clear();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         void Clear()
        {
            txtRelationCode.Text = string.Empty;
            txtRelation.Text = string.Empty;
        }

         void bindSearch()
        {

            try
            {

                DataTable dt = Relationofproposer.GetAdditionalServiceMasterDetails();
                if (dt.Rows.Count > 0)
                {
                    gvSearch.DataSource = dt;
                    gvSearch.DataBind();
                }
            }

            catch(Exception ex)
            {
                throw ex;
            }

        }

         void Edit(long id)
        {
            try
            {
                Relationofproposer Rproposer = new Relationofproposer(id);
                hdfMid.Value = Utility.ToString(Rproposer.ID);
                txtRelationCode.Text = Rproposer.CODE;
                txtRelation.Text = Rproposer.RELATION;
                btnSave.Text = "Update";
                btnClear.Text = "Cancel";

            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Button Events
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareRelationofproposer.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Master.ShowSearch("Search");
                bindSearch();
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareRelationofproposer.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareRelationofproposer.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        #endregion
        #region Gridview Events
        protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                long relationid = Utility.ToInteger(gvSearch.SelectedValue);
                Edit(relationid);
                this.Master.HideSearch();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareRelationofproposer.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }

        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSearch.PageIndex = e.NewPageIndex;
                gvSearch.EditIndex = -1;
                bindSearch();
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareRelationofproposer.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        #endregion

    }
}
