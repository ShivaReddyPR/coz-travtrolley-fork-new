﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.Web.UI.Controls;
using CT.TicketReceipt.Common;
using System.Collections.Generic;

public partial class CorporateExpenseApprovalsUI : CT.Core.ParentPage
{

    private string EXPENSEAPPROVALS_SEARCH_SESSION = "_ExpenseApprovalsSearchList";
    protected int userCorpProfileId;
    protected string _approvalStatus;
    protected string approverNames = string.Empty;
    protected CorporateProfileExpenseDetails detail;
    protected int approverId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {
                int user_corp_profile_id = CorporateProfile.GetCorpProfileId((int)Settings.LoginInfo.UserID);
                if (user_corp_profile_id > 0)
                {
                    this.userCorpProfileId = user_corp_profile_id;
                }
                if (!Page.IsPostBack)
                {
                    IntialiseControls();

                }
            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateExpenseApprovals page Load Error: " + ex.Message, "0");
        }
    }
    private void IntialiseControls()
    {
        try
        {
            dcApprovalFromDate.Value = DateTime.Now;
            dcApprovalToDate.Value = DateTime.Now;
            int user_corp_profile_id = -1;
            //ddlEmployee.Enabled = false;
            //int user_corp_profile_id = CorporateProfile.GetCorpProfileId((int)Settings.LoginInfo.UserID);
            //if (Settings.LoginInfo.MemberType == MemberType.TRAVELCORDINATOR || Settings.LoginInfo.MemberType == MemberType.SUPER || Settings.LoginInfo.MemberType == MemberType.ADMIN)
            //{
            //    user_corp_profile_id = 0;
            //    ddlEmployee.Enabled = true;
            //}
                //if (user_corp_profile_id > 0)
                //{
                //    this.userCorpProfileId = user_corp_profile_id;
                //}
                //if (user_corp_profile_id < 0)
                //{
                //    user_corp_profile_id = 0;
                //}

                // bindSearch(DateTime.Now.AddMonths(-1), DateTime.Now, userCorpProfileId);
                bindSearch(DateTime.Now.AddYears(-10), DateTime.Now.AddYears(10), user_corp_profile_id);// default loading all pending data
            bindEmployeesList();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void bindEmployeesListNEW()
    {
        if (Settings.LoginInfo != null && Settings.LoginInfo.CorporateProfileId > 0)
        {
            DataTable dtEmployees = null;
            CorporateProfile cp = new CorporateProfile(Settings.LoginInfo.CorporateProfileId, (int)Settings.LoginInfo.AgentId);

            dtEmployees = CorporateProfile.GetCorpProfilesListByApproverType((int)Settings.LoginInfo.CorporateProfileId, "E");//E--Expense Approver

            //if (dtEmployees != null && dtEmployees.Rows != null && dtEmployees.Rows.Count > 0)
            ddlEmployee.DataSource = dtEmployees;
            ddlEmployee.DataTextField = "Name";
            ddlEmployee.DataValueField = "ProfileId";
            ddlEmployee.DataBind();
            ddlEmployee.Items.Insert(0, new ListItem("--Select Employee--", "0"));
            ddlEmployee.SelectedValue = "0";

        }
        //if (this.userCorpProfileId > 0)
        //{


        //    ddlEmployee.Enabled = false;
        //    ddlEmployee.SelectedItem.Value = Convert.ToString(userCorpProfileId);
        //}
    }

    private void bindEmployeesList()
    {
        ddlEmployee.DataSource = CorporateProfile.GetCorpProfilesList((int)Settings.LoginInfo.AgentId);
        ddlEmployee.DataTextField = "Name";
        ddlEmployee.DataValueField = "ProfileId";
        ddlEmployee.DataBind();
        ddlEmployee.Items.Insert(0, new ListItem("--Select Employee--", "0"));

        //if (this.userCorpProfileId > 0)
        //{


        //    ddlEmployee.Enabled = false;
        //    ddlEmployee.SelectedItem.Value = Convert.ToString(userCorpProfileId);
        //}
    }

    protected void btnSearchApprovals_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlEmployee.SelectedValue == "0")
            {
                bindSearch(Convert.ToDateTime(dcApprovalFromDate.Value), Convert.ToDateTime(dcApprovalToDate.Value), 0);
            }
            else
            {
                bindSearch(Convert.ToDateTime(dcApprovalFromDate.Value), Convert.ToDateTime(dcApprovalToDate.Value), Convert.ToInt32(ddlEmployee.SelectedValue));
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateExpenseApprovals btnSearchApprovals Error: " + ex.Message, "0");
        }
    }

    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateExpenseApprovals)gvSearch_PageIndexChanging method .Error:" + ex.ToString(), "0");

        }

    }

    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={

                                         {"HTtxtDATE", "DATE" }
                                        ,{"HTtxtEMPLOYEEID", "EMP_ID" } 
                                        ,{"HTtxtEMPLOYEENAME", "EMP_NAME" }
                                        ,{"HTtxtAMOUNT", "AMOUNT"}
                                        ,{"HTtxtEXPREF",  "EXPREF"}
                                        ,{"HTtxtEXPCAT", "EXPMODE"}

                                        ,{"HTtxtEXPTYPE",  "EXPTYPE"}
                                         };
        CommonGrid g = new CommonGrid();
        g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);

    }



    protected void ITchkSelect_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvHdrRow = (GridViewRow)((CheckBox)sender).NamingContainer;
            CheckBox chkSelectAll = (CheckBox)gvHdrRow.FindControl("HTchkSelectAll");
            foreach (GridViewRow gvRow in gvSearch.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                chkSelect.Checked = chkSelectAll.Checked;
            }
        }
        catch (Exception ex)
        {

            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void isTrackingEmpty()
    {
        try
        {
            bool _selected = false;
            foreach (GridViewRow gvRow in gvSearch.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");


                if (chkSelect.Checked)
                {
                    _selected = true;
                    return;
                }

            }
            string strMsg = "Please Select atleast one Item ! ";
            if (!_selected)
                Utility.Alert(this.Page, strMsg);
        }
        catch { throw; }
    }

    private void bindSearch(DateTime fromDate, DateTime toDate, int profileId)
    {
        try
        {
            // show data only for booker
            //if (Settings.LoginInfo.MemberType == MemberType.TRAVELCORDINATOR || Settings.LoginInfo.MemberType == MemberType.SUPER || Settings.LoginInfo.MemberType == MemberType.ADMIN)
            //{
                DataTable dt = CorporateProfileExpenseDetails.GetCorpExpenseApprovalsList(profileId, fromDate, toDate);
                if (dt != null && dt.Rows.Count > 0)
                {
                    SearchList = dt;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvSearch, dt);
                    gvSearch.Visible = true;
                    //lblSuccessMsg.Visible = false;

                }
                else
                {
                    gvSearch.Visible = false;
                    //lblSuccessMsg.Visible = true;
                    //lblSuccessMsg.Text = "NO RECORDS FOUND !";

                }
            //}

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private DataTable SearchList
    {
        get
        {

            return (DataTable)Session[EXPENSEAPPROVALS_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["Id"] };
            Session[EXPENSEAPPROVALS_SEARCH_SESSION] = value;
        }
    }


    private void updateStatus(string approverStatus)
    {
        isTrackingEmpty();
        try
        {
            foreach (GridViewRow gvRow in gvSearch.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                HiddenField hdfExpId = (HiddenField)gvRow.FindControl("IThdfEXPDETAIL_ID");
                HiddenField hdfExpDetailId = (HiddenField)gvRow.FindControl("hdfExpDetailId");

                if (chkSelect.Checked)
                {
                    CorporateProfileExpenseDetails.UpdateRefundStatus(Convert.ToInt32(hdfExpId.Value), approverStatus, (int)Settings.LoginInfo.AgentId);
                    lblSuccessMsg.Visible = true;
                    lblSuccessMsg.Text = "Updated Successfully";


                    if (approverStatus == "A")//Approved
                    {
                        try
                        {
                            SendEmail("E", approverStatus, Convert.ToInt32(hdfExpDetailId.Value));
                            SendEmail("A", approverStatus, Convert.ToInt32(hdfExpDetailId.Value));
                        }
                        catch { }
                    }
                    else//Rejected -- So no need to trigger any email to the next level approver
                    {
                        try
                        {
                            SendEmail("E", approverStatus, Convert.ToInt32(hdfExpDetailId.Value));
                        }
                        catch { }

                    }
                }
            }
        }

        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        try
        {
            updateStatus("A");
            IntialiseControls();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateExpenseApprovalsPage)btnApprove_Click Event .Error:" + ex.ToString(), "0");
        }
    }


    protected void btnReject_Click(object sender, EventArgs e)
    {
        try
        {
            updateStatus("R");
            IntialiseControls();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateExpenseApprovalsPage)btnReject_Click Event .Error:" + ex.ToString(), "0");

        }
    }

   
    protected void SendEmail(string toEmpOrApp, string approverStatus,int expDetailId)
    {
        
        detail = new CorporateProfileExpenseDetails(expDetailId);
        detail.ExpDetailId = expDetailId; 
        int user_corp_profile_id = CorporateProfile.GetCorpProfileId((int)Settings.LoginInfo.UserID);
               
        try
        {
            if (approverStatus == "A")
            {
                _approvalStatus = "Approved";
            }
            else
            {
                _approvalStatus = "Rejected";
            }
            List<string> toArray = new System.Collections.Generic.List<string>();
            if (toEmpOrApp == "A" && approverStatus == "A")//To the approver
            {
                int hLevel = Convert.ToInt32(detail.ApprovalHierarachy);
                List<int> listOfAppId = detail.ProfileApproversList.Where(i => i.Hierarchy == (hLevel + 1)).Select(i => i.ApproverId).ToList();
                if (listOfAppId != null && listOfAppId.Count > 0)
                {
                    foreach (int appId in listOfAppId)
                    {
                        approverNames = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverName).FirstOrDefault();
                        approverId = appId;
                        string appEmail = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverEmail).FirstOrDefault();
                        if (!string.IsNullOrEmpty(appEmail))
                        {
                            toArray.Add(appEmail);
                            System.IO.StringWriter sw = new System.IO.StringWriter();
                            HtmlTextWriter htw = new HtmlTextWriter(sw);
                            EmailDivApprover.RenderControl(htw);
                            string myPageHTML = string.Empty;
                            myPageHTML = sw.ToString();
                            myPageHTML = myPageHTML.Replace("none", "block");                        
                            string subject = "Expense Approval Request";
                            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                            toArray.Clear();
                            approverNames = string.Empty;
                        }
                    }
                }
            }
            else //To the employee
            {
                approverNames = detail.ProfileApproversList.Where(i => i.ApproverId == user_corp_profile_id).Select(i => i.ApproverName).FirstOrDefault();
                if (!string.IsNullOrEmpty(detail.EmpEmail))
                {
                    toArray.Add(detail.EmpEmail);
                }
                System.IO.StringWriter sw = new System.IO.StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                EmailDivEmployee.RenderControl(htw);
                string myPageHTML = string.Empty;
                myPageHTML = sw.ToString();
                myPageHTML = myPageHTML.Replace("none", "block");
                string subject = "Expense Status Change Notification";
                if (toArray != null && toArray.Count > 0)
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                }
                toArray.Clear();
                approverNames = string.Empty;
            }
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateCreateExpenseReport.aspx)Failed to Send Email For Employee and Approvers: Reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        
    }



}
