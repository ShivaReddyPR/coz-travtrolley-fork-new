﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;

public partial class ItineraryViewServiceDetailsGUI : System.Web.UI.Page
{
    protected DataTable dtRecords;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString.Count > 0 && Request.QueryString["paxId"] != null)
                {
                    BindGrid(Convert.ToInt32(Request.QueryString["paxId"]));
                }
            }
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "(ItineraryViewServiceDetails Page)" + ex.ToString(), "0");
    
        }

    }
    private void BindGrid(int paxId)
    {
        try
        {
            dtRecords = ItineraryAddServiceDetails.GetServiceDetails(paxId);
            gvSearch.DataSource = dtRecords;
            gvSearch.DataBind();
        }
        catch
        {
            throw;
        }
    }
    #region Date Format
    protected string CZDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string CZDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }


    protected void gvSearch_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string add_id = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "add_id"));
            Button lnkbtnresult = (Button)e.Row.FindControl("btnPrint");
            if (lnkbtnresult != null)
            {
                lnkbtnresult.Attributes.Add("onclick", "javascript:print('" + add_id + "')");
            }
        }
    }
    #endregion
}
