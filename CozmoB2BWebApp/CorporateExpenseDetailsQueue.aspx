﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CorporateExpenseDetailsQueueUI"
    Title="Expense Details Queue" Codebehind="CorporateExpenseDetailsQueue.aspx.cs" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">


<script type="text/javascript">

    function Validate() {

        var valid = false;
        document.getElementById('errMess').style.display = "none";

        if (document.getElementById('ctl00_cphTransaction_dcReimFromDate_Date').value.length == 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please select from date .";
        }
        else if (document.getElementById('ctl00_cphTransaction_dcReimToDate_Date').value.length == 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please select to date.";
        }

        else {
            valid = true;
        }

        return valid;
    }
</script>
    <div class="body_container">
        <div class="col-md-12 CorpTrvl-tabbed-panel">
            <h2>
                Expense Details Queue</h2>
                 <div id="errMess" style="display: none; color: Red; font-weight: bold; text-align: center;">
                    </div>
                
            <div class="tab-content responsive">
                <div style="background: #fff; padding: 10px;">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    From Date <span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcReimFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                    </uc1:DateControl>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    To Date <span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcReimToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Select Employee</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlEmployee" runat="server">
                                    
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Expense Category</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlExpCategory" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                    <asp:ListItem Value="T" Text="Travel Mode"></asp:ListItem>
                                    <asp:ListItem Value="N" Text="Non Travel Mode"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Expense Type</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlExpType" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="center-block">
                                &nbsp;</label>
                            <asp:Button OnClientClick="return Validate();" runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn but_d btn_xs_block cursor_point mar-5"
                                Text="Search" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="CorpTrvl-tabbed-panel">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#Pending" aria-controls="Pending"
                                        role="tab" data-toggle="tab" aria-expanded="true">Pending</a></li>
                                    <li role="presentation" class=""><a href="#Approved" aria-controls="Approved" role="tab"
                                        data-toggle="tab" aria-expanded="false">Approved</a></li>
                                    <li role="presentation" class=""><a href="#Rejected" aria-controls="Rejected" role="tab"
                                        data-toggle="tab" aria-expanded="false">Rejected</a></li>
                                    <li role="presentation" class=""><a href="#Reimbursed" aria-controls="Reimbursed"
                                        role="tab" data-toggle="tab" aria-expanded="false">Reimbursed</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="Pending">
                                     <div class="table table-responsive  mb-0">      
                            <asp:GridView ID="gvPending" Width="100%" runat="server" AllowPaging="true" DataKeyNames="EXP_DETAIL_ID"
                                EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvPending_PageIndexChanging">
                                <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="left" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="IThdfEXPDETAIL_ID" runat="server" Value='<%# Bind("EXP_DETAIL_ID") %>'>
                                            </asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtDATE" Width="100px" HeaderText="DATE" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDATE" runat="server" Text='<%# Eval("DATE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("DATE") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMPLOYEEID" Width="70px" HeaderText="EMPLOYEEID" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMPLOYEEID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_ID") %>' Width="70px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMPLOYEENAME" Width="100px" HeaderText="EMPLOYEENAME" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMPLOYEENAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_NAME") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPREF" Width="100px" HeaderText="EXPENSE REF" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEXPREF" runat="server" Text='<%# Eval("EXP_REF") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EXP_REF") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPCAT" Width="100px" HeaderText="EXPENSE CATEGORY" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEXPCAT" runat="server" Text='<%# Eval("EXP_CATEGORY") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EXP_CATEGORY") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtAMOUNT" Width="100px" HeaderText="AMOUNT" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAMOUNT" runat="server" Text='<%# Eval("AMOUNT") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("AMOUNT") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="APPROVAL STATUS">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblStatus" Text='<%# Eval("APPROVALSTATUS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Details">
                                        <ItemTemplate>
                                            <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" NavigateUrl='<%# Eval("EXP_DETAIL_ID", "~/CorporateViewExpenseDetails.aspx?ExpId={0}") %>'
                                                Text='View Details' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="Approved">
                                          <div class="table table-responsive mb-0">
                                         <asp:GridView ID="gvApproved" Width="100%" runat="server" AllowPaging="true" DataKeyNames="EXP_DETAIL_ID"
                                EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvApproved_PageIndexChanging">
                                <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="left" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="IThdfEXPDETAIL_ID" runat="server" Value='<%# Bind("EXP_DETAIL_ID") %>'>
                                            </asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtDATE" Width="100px" HeaderText="DATE" CssClass="form-control"
                                                OnClick="FilterSearch2_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDATE" runat="server" Text='<%# Eval("DATE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("DATE") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMPLOYEEID" Width="70px" HeaderText="EMPLOYEEID" CssClass="form-control"
                                                OnClick="FilterSearch2_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMPLOYEEID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_ID") %>' Width="70px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMPLOYEENAME" Width="100px" HeaderText="EMPLOYEENAME" CssClass="form-control"
                                                OnClick="FilterSearch2_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMPLOYEENAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_NAME") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPREF" Width="100px" HeaderText="EXPENSE REF" CssClass="form-control"
                                                OnClick="FilterSearch2_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEXPREF" runat="server" Text='<%# Eval("EXP_REF") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EXP_REF") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPCAT" Width="100px" HeaderText="EXPENSE CATEGORY" CssClass="form-control"
                                                OnClick="FilterSearch2_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEXPCAT" runat="server" Text='<%# Eval("EXP_CATEGORY") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EXP_CATEGORY") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtAMOUNT" Width="100px" HeaderText="AMOUNT" CssClass="form-control"
                                                OnClick="FilterSearch2_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAMOUNT" runat="server" Text='<%# Eval("AMOUNT") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("AMOUNT") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="APPROVAL STATUS">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblStatus" Text='<%# Eval("APPROVALSTATUS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Details">
                                        <ItemTemplate>
                                            <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" NavigateUrl='<%# Eval("EXP_DETAIL_ID", "~/CorporateViewExpenseDetails.aspx?ExpId={0}") %>'
                                                Text='View Details' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                        </div>
                                        
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="Rejected">
                                          <div class="table table-responsive mb-0">
                                         <asp:GridView ID="gvRejected" Width="100%" runat="server" AllowPaging="true" DataKeyNames="EXP_DETAIL_ID"
                                EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvRejected_PageIndexChanging">
                                <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="left" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="IThdfEXPDETAIL_ID" runat="server" Value='<%# Bind("EXP_DETAIL_ID") %>'>
                                            </asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtDATE" Width="100px" HeaderText="DATE" CssClass="form-control"
                                                OnClick="FilterSearch3_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDATE" runat="server" Text='<%# Eval("DATE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("DATE") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMPLOYEEID" Width="70px" HeaderText="EMPLOYEEID" CssClass="form-control"
                                                OnClick="FilterSearch3_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMPLOYEEID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_ID") %>' Width="70px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMPLOYEENAME" Width="100px" HeaderText="EMPLOYEENAME" CssClass="form-control"
                                                OnClick="FilterSearch3_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMPLOYEENAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_NAME") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPREF" Width="100px" HeaderText="EXPENSE REF" CssClass="form-control"
                                                OnClick="FilterSearch3_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEXPREF" runat="server" Text='<%# Eval("EXP_REF") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EXP_REF") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPCAT" Width="100px" HeaderText="EXPENSE CATEGORY" CssClass="form-control"
                                                OnClick="FilterSearch3_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEXPCAT" runat="server" Text='<%# Eval("EXP_CATEGORY") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EXP_CATEGORY") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtAMOUNT" Width="100px" HeaderText="AMOUNT" CssClass="form-control"
                                                OnClick="FilterSearch3_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAMOUNT" runat="server" Text='<%# Eval("AMOUNT") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("AMOUNT") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="APPROVAL STATUS">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblStatus" Text='<%# Eval("APPROVALSTATUS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Details">
                                        <ItemTemplate>
                                            <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" NavigateUrl='<%# Eval("EXP_DETAIL_ID", "~/CorporateViewExpenseDetails.aspx?ExpId={0}") %>'
                                                Text='View Details' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                              </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="Reimbursed">
                                          <div class="table table-responsive mb-0">
                                         <asp:GridView ID="gvReimbursed" Width="100%" runat="server" AllowPaging="true" DataKeyNames="EXP_DETAIL_ID"
                                EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvReimbursed_PageIndexChanging">
                                <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="left" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="IThdfEXPDETAIL_ID" runat="server" Value='<%# Bind("EXP_DETAIL_ID") %>'>
                                            </asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtDATE" Width="100px" HeaderText="DATE" CssClass="form-control"
                                                OnClick="FilterSearch4_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDATE" runat="server" Text='<%# Eval("DATE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("DATE") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMPLOYEEID" Width="70px" HeaderText="EMPLOYEEID" CssClass="form-control"
                                                OnClick="FilterSearch4_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMPLOYEEID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_ID") %>' Width="70px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMPLOYEENAME" Width="100px" HeaderText="EMPLOYEENAME" CssClass="form-control"
                                                OnClick="FilterSearch4_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMPLOYEENAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_NAME") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPREF" Width="100px" HeaderText="EXPENSE REF" CssClass="form-control"
                                                OnClick="FilterSearch4_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEXPREF" runat="server" Text='<%# Eval("EXP_REF") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EXP_REF") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPCAT" Width="100px" HeaderText="EXPENSE CATEGORY" CssClass="form-control"
                                                OnClick="FilterSearch4_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEXPCAT" runat="server" Text='<%# Eval("EXP_CATEGORY") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EXP_CATEGORY") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtAMOUNT" Width="100px" HeaderText="AMOUNT" CssClass="form-control"
                                                OnClick="FilterSearch4_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAMOUNT" runat="server" Text='<%# Eval("AMOUNT") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("AMOUNT") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="APPROVAL STATUS">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblStatus" Text='<%# Eval("APPROVALSTATUS") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Details">
                                        <ItemTemplate>
                                            <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" NavigateUrl='<%# Eval("EXP_DETAIL_ID", "~/CorporateViewExpenseDetails.aspx?ExpId={0}") %>'
                                                Text='View Details' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                              </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</asp:Content>
