﻿using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Web.UI.Controls;

public partial class BaggageMasterGUI : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblSuccessMsg.Text = string.Empty;
        this.Master.PageRole = true;
        try
        {
            if(Settings.LoginInfo!=null)
            {
                if(!IsPostBack)
                {
                    InitialisePageControls();
                }
            }
        }
        catch(Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;

        }
    }
    #region PrivateMethods
    private void InitialisePageControls()
    {
        try
        {
            BindSources();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindSources()
    {
        try
        {
            Array sources = Enum.GetValues(typeof(BookingSource));
            ddlSource.Items.Insert(0, (new ListItem("--Select--", "-1")));
            foreach (BookingSource bs in sources)
            {
                ddlSource.Items.Add(new ListItem(bs.ToString(), ((int)bs).ToString()));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void Clear()
    {
        try
        {
            hdfBaggageId.Value = string.Empty;
            ddlSource.SelectedValue = "-1";
            txtCode.Text = string.Empty;
            txtPrice.Text = string.Empty;
            txtCurrency.Text = string.Empty;
            rbActive.Checked = true;
            rbInActive.Checked = false;
            rbDomestic.Checked = true;
            rbInternational.Checked = false;
            btnSave.Text = "Save";
            btnClear.Text = "Clear";         
        }
        catch
        {
            throw;
        }
    }
    private void Save()
    {
        try
        {
            BaggageMaster bgMaster = new BaggageMaster();
            if (Utility.ToInteger(hdfBaggageId.Value) > 0)
            {
                bgMaster.BaggageId = Utility.ToInteger(hdfBaggageId.Value);
            }
            else
            {
                bgMaster.BaggageId = -1;
            }
            bgMaster.Code = txtCode.Text;
            bgMaster.Price =Utility.ToDecimal(txtPrice.Text);
            bgMaster.Currency = txtCurrency.Text;
            if (rbActive.Checked == true)
            {
                bgMaster.IsActive = true ;
            }
            else
            {
                bgMaster.IsActive = false;
            }
            if (rbDomestic.Checked == true)
            {
                bgMaster.IsDomestic = true;
            }
            else
            {
                bgMaster.IsDomestic = false;
            }
            bgMaster.SourceId = (BookingSource)Utility.ToInteger(ddlSource.SelectedItem.Value);          
            bgMaster.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
            bgMaster.Save();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage("Successfully", "", (Utility.ToInteger(hdfBaggageId.Value) == 0 ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
            Clear();
        }
        catch
        {
            throw;
        }
    }
    private void Edit(int Id)
    {
        BaggageMaster bgMaster = new BaggageMaster(Id);
        hdfBaggageId.Value = Utility.ToString(bgMaster.BaggageId);
        ddlSource.SelectedValue =Utility.ToString((int)bgMaster.SourceId);
        if (bgMaster.IsDomestic == true)
        {
            rbDomestic.Checked = true;
        }
        else
        {
            rbInternational.Checked = true;
        }
        if (bgMaster.IsActive == true)
        {
            rbActive.Checked = true;
        }
        else
        {
            rbInActive.Checked = true;
        }
        txtCode.Text = bgMaster.Code;
        txtCurrency.Text = bgMaster.Currency;
        txtPrice.Text =Utility.ToString(bgMaster.Price);            
        btnSave.Text = "Update";
        btnClear.Text = "Cancel";
    }
    #endregion
    #region Button Events
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            Utility.Alert(this.Page, ex.Message);
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Audit.Add(EventType.Exception, Severity.High, 1, "BaggageMaster page " + ex.Message, "0");
        }
    }
    private void bindSearch()
    {
        try
        {
            DataTable dt = BaggageMaster.GetList(ListStatus.Long,RecordStatus.All);
            if (dt != null && dt.Rows.Count > 0)
            {             
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvSearch, dt);
            }
        }
        catch
        {
            throw;
        }
    }
    #endregion
    #region GridEvents
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            bindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            Int32 baggageId = Utility.ToInteger(gvSearch.SelectedValue);
            Edit(baggageId);
            this.Master.HideSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    #endregion

}
