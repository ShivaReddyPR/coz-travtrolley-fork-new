using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DocumentManager : System.Web.UI.UserControl
{
    #region Member Variables  
    private string _documentName;  
    private string _defaultImageUrl=string.Empty;
    private string _documentImageUrl = string.Empty;
    private string _savePath = string.Empty;
    private string _defaultToolTip;
    private bool _enabled;
    private bool _showActualImage = true;
    
  //  private System.Web.UI.WebControls.FileUpload _fileUploder;
    #endregion
    #region Properties
    public string DocumentName
    {
        get { return _documentName; }
        set { _documentName = value; }
    }
    
    public string DialogStyle
    {        
        set
        {
            string[] keyVals = value.Split(new char[1] { ';' });
            foreach (string keyvalpair in keyVals)
            {                
                string[] keyval = keyvalpair.Split(new char[] { ':' });
                if (keyval.Length == 2)
                {
                    pnlUploadImage.Style[keyval[0].Trim()] = keyval[1].Trim();
                }
            }           

        }
    }
    //public System.Web.UI.WebControls.FileUpload FileUploder
    //{
    //    get { return _fileUploder; }
    //    set { _fileUploder = value; }
    //}
    public string SavePath
    {
        get
        {
             return (string)ViewState["_savePath"];
        }
        set
        {
            ViewState["_savePath"]=value;
            
        }
    }
    public string ContentType
    {
        get
        {
            return (string)ViewState["_ContentType"];
        }
        set
        {
            ViewState["_ContentType"] = value;

        }
    }
    
    public string SessionName
    {
        get { return hdfSessionName.Value; }
        set { hdfSessionName.Value = value; }  
    }
    public bool IsDocumentChanged
    {
        get { return (hdfIsDocumentChanged.Value == "1"); }
        set { hdfIsDocumentChanged.Value = value ? "1" : "0"; }
    }
    public string FileName
    {
        get
        {
            return (string)ViewState["_fileName"];
        }
        set
        {
            ViewState["_fileName"] = value;
        }
    }
    public string FileExtension
    {
        get
        {
            return (string)ViewState["_fileExtension"];
        }
        set
        {
            ViewState["_fileExtension"] = value;
        }
    }
    public string FullPath
    {
        get
        {
            return (string)ViewState["_fullPath"];
        }
        set
        {
            ViewState["_fullPath"] = value;
        }
    }
    public string CommandName
    {
        get
        {
            return btnDocApply.CommandName;
        }
        set
        {
            btnDocApply.CommandName = value;
        }
    }
    //public string FileName
    //{
    //    get
    //    {
    //        if (DocumentInfo != null) return DocumentInfo.FileName;
    //        else return string.Empty;
    //    }
    //    set
    //    {
    //        //if (DocumentInfo == null)
    //        //{
    //        //    UploadDocument document = new UploadDocument();
    //        //    Session[SessionName] = document;
    //        //}
    //        //DocumentInfo.FileName = value;
    //        //if (string.IsNullOrEmpty(DocumentInfo.ServerFileName) && DocumentInfo.Content != null && (!string.IsNullOrEmpty(value)))
    //        //{
    //        //    DocumentInfo.ServerFileName = UploadDocument.CreateImage(DocumentInfo.Content, value);
    //        //    if (ShowActualImage) imgDocument.ImageUrl = DocumentInfo.ServerFileName;
    //        //    else imgDocument.ImageUrl = DocumentImageUrl;
    //        //    imgDocument.ToolTip = DocumentInfo.FileName;
    //        //} TODO ziya
    //    }
    //}
    public bool Enabled
    {
        get { return _enabled; }
        set
        {
            lnkUpload.Enabled = value;
            
        }
    }
    public byte[] Image
    {
        get
        {
            if (DocumentInfo != null) return DocumentInfo.Content;
            else return null;
        }
        set
        {
            if (DocumentInfo == null)
            {
                UploadDocument document = new UploadDocument();
                Session[SessionName] = document;
            }
            //DocumentInfo.Content = value;
            //if (string.IsNullOrEmpty(DocumentInfo.ServerFileName) && value != null && (!string.IsNullOrEmpty(DocumentInfo.FileName)))
            //{
            //    DocumentInfo.ServerFileName = UploadDocument.CreateImage(value, DocumentInfo.FileName);
            //    if (ShowActualImage) imgDocument.ImageUrl = DocumentInfo.ServerFileName;
            //    else imgDocument.ImageUrl = DocumentImageUrl;
            //    imgDocument.ToolTip = DocumentInfo.FileName;
            //}
        }
    }
    private UploadDocument DocumentInfo
    {
        get { return (UploadDocument)Session[SessionName]; }
        set
        {
            if (value == null)
                Session.Remove(SessionName);
            else
                Session[SessionName] = value;
        }
    }
    public string DefaultImageUrl
    {
        get { return _defaultImageUrl; }
        set 
        {
            if (value != null) imgDocument.ImageUrl = value;
            _defaultImageUrl = value; 
        }
    }
    public string DocumentImageUrl
    {
        get { return _documentImageUrl; }
        set
        {            
            _documentImageUrl = value;
        }
    }
    public Unit Width
    {        
        set { imgDocument.Width = value; }
    }
    public Unit Height
    {        
        set { imgDocument.Height = value; }
    }
    public string DefaultToolTip
    {
        get { return _defaultToolTip; }
        set 
        {
            if (value != null) imgDocument.ToolTip = value;
            _defaultToolTip = value; 
        }
    }
    public string AlternateText
    {        
        set { imgDocument.AlternateText = value; }
    }
    public bool ShowActualImage
    {
        get { return _showActualImage; }
        set { _showActualImage = value; }
    }    
    #endregion
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
            if (!this.Page.ClientScript.IsClientScriptIncludeRegistered("FileUploadScript"))
            {
                Page.ClientScript.RegisterClientScriptInclude("FileUploadScript", "Scripts/DocumentManager/DocumentManager.js");
            }
        //}
    }
    protected void btnDocApply_Click(object sender, EventArgs e)
    {
        try
        {
            
            if (string.IsNullOrEmpty(SavePath)) throw new Exception("Save Path not Defined");
            UploadDocument document = DocumentInfo;
            if (document != null)
            {
                IsDocumentChanged = true;
                if (document.Content == null)
                    imgDocument.ImageUrl = DefaultImageUrl;
                else
                {
                    if (ShowActualImage) imgDocument.ImageUrl = document.ServerFileName;
                    else imgDocument.ImageUrl = DocumentImageUrl;
                }
                imgDocument.AlternateText = DocumentName;
                imgDocument.ToolTip = FileName;
                FileExtension = System.IO.Path.GetExtension(document.FileUploder.FileName);
                if (!System.IO.Directory.Exists(Server.MapPath("") + "\\" + SavePath)) System.IO.Directory.CreateDirectory(Server.MapPath("") + "\\" + SavePath);

                FullPath= string.Format("{0}{1}{2}",SavePath, FileName, FileExtension);
                ContentType = document.FileUploder.PostedFile.ContentType;
                string imgPath = string.Format("{0}\\{1}{2}{3}", Server.MapPath(""), SavePath, FileName, FileExtension);
                document.FileUploder.SaveAs(imgPath);

                //UploadDocument.ClearCache();
            }
        }
        catch { throw; }

    }
    protected void butDocCancel_Click(object sender, EventArgs e)
    {
        try
        {
            //UploadDocument.ClearCache();
            //Clear();
        }
        catch
        {
            throw;
        }
    }
    public void Clear()
    {
        DocumentInfo = null;
        IsDocumentChanged = false;
        
        imgDocument.ImageUrl = DefaultImageUrl;
        imgDocument.AlternateText = DocumentName;
        imgDocument.ToolTip = DefaultToolTip;
        UploadDocument.ClearCache();
        FullPath = string.Empty;

        
    }    
    
}
