﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FleetPaymentVoucher" Title="Payment Voucher" Codebehind="FleetPaymentVoucher.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<link href="css/menuDefault.css" rel="stylesheet" type="text/css">
    

                <link href="css/accordion_ui.css" type="text/css" rel="stylesheet" />
                <script type="text/javascript">
                    function viewVoucher() {
                        window.open('PrintFleetVoucher.aspx?ConfNo=<%= itinerary.BookingRefNo %>', 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes');
                        return false;
                    }
                </script>
                <div class="ns-h3">

                    <label>     <strong>  Fleet Confirmation</strong> </label>

                    <label class=" pull-right"> Confirmation No:
                <%=itinerary.BookingRefNo%>
                </label>
                    <div class="clearfix"></div>
                </div>
                <div class="bg_white bor_gray padding-5">


                    <div>
                        <div class="col-md-6">
                            <asp:Image ID="imgHeaderLogo" runat="server" Visible="false" />Vehicle Name: <b><%=itinerary.FleetName %></b> </div>

                        <div class="col-md-6">Booked on: <span class="spnred"><%=itinerary.CreatedOn.ToString("dd MMM yyyy hh:mm") %></span> </div>
                        <div class="col-md-6 margin-top-10">Pickup Location:<b><%=itinerary.FromLocation%></b></div>
                        <%if (!string.IsNullOrEmpty(itinerary.ToLocation))
  {%>
                            <div class="col-md-6 margin-top-10">Drop Location:<b><%=itinerary.ToLocation%></b></div>
                            <%} %>
                                <div class="col-md-6 margin-top-10">Pickup Date:<b><%=itinerary.FromDate.ToString("dd MMM yyyy hh:mm")%></b></div>
                                <div class="col-md-6 margin-top-10">Drop Date:<b><%=itinerary.ToDate.ToString("dd MMM yyyy hh:mm")%></b></div>
                                <div class="clearfix"></div>
                    </div>




                </div>
                <div class="padding-5 col-md-6" style="margin-top: 30px;">

                    <h4> Pax Details</h4>

                    <table class="table table-bordered">
                        <tbody>
                            <tr>

                                <th>
                                   <strong> Title</strong>
                                </th>
                                <td>
                                    <%=itinerary.PassengerInfo[0].Title%>
                                </td>


                            </tr>
                            <tr>

                                <th>
                                    <strong>First Name</strong>
                                </th>
                                <td>
                                    <%=itinerary.PassengerInfo[0].FirstName%>
                                </td>


                            </tr>

                            <tr>

                                <th>
                                   <strong> Last Name</strong>
                                </th>
                                <td>
                                    <%=itinerary.PassengerInfo[0].LastName%>
                                </td>


                            </tr>
                            <tr>

                                <th>
                                   <strong> Email</strong>
                                </th>
                                <td>
                                    <%=itinerary.PassengerInfo[0].Email%>
                                </td>


                            </tr>
                            <%if(!string.IsNullOrEmpty(itinerary.PassengerInfo[0].PickupAddress)){ %>
                             <tr>

                                <th>
                                   <strong> Pckup Address</strong>
                                </th>
                                <td>
                                    <%=itinerary.PassengerInfo[0].PickupAddress%>
                                </td>


                            </tr>
                            
                            <%} %>
                             <%if(!string.IsNullOrEmpty(itinerary.PassengerInfo[0].Landmark)){ %>
                             <tr>

                                <th>
                                   <strong>Landmark</strong>
                                </th>
                                <td>
                                    <%=itinerary.PassengerInfo[0].Landmark%>
                                </td>


                            </tr>
                            
                            <%} %>
                            <tr>

                                <th>
                                   <strong> Mobile No1</strong>
                                </th>
                                <td>
                                    <%=itinerary.PassengerInfo[0].MobileNo%>
                                </td>


                            </tr>
                             <%if (!string.IsNullOrEmpty(itinerary.PassengerInfo[0].MobileNo2))
   { %>
                             <tr>

                                <th>
                                   <strong>Mobile No2</strong>
                                </th>
                                <td>
                                    <%=itinerary.PassengerInfo[0].MobileNo2%>
                                </td>


                            </tr>
                            
                            <%} %>
                        </tbody>
                    </table>

                </div>
                <div class="padding-5 col-md-6" style="margin-top: 30px;">

                    <h4> Payment Details</h4>



                    <table class="table table-bordered">
                        <%--<tr>

                            <th>
                                <strong>Name</strong>
                            </th>

                            <th>
                                <strong>Total</strong>
                            </th>
                        </tr>--%>
                        <% decimal gtotal = 0,pageMarkup=0;%>
                            <tr>

                                <th>
                                   Car Rental Charges
                                </th>
                                <td>

                                    <%pageMarkup = itinerary.Price.AsvAmount; %>

                                        <%=Math.Round(itinerary.Price.NetFare + itinerary.Price.AsvAmount - itinerary.Price.Discount, itinerary.Price.DecimalPoint).ToString("N" + itinerary.Price.DecimalPoint)%>

                                </td>


                            </tr>
                            <tr>

                                <th>
                                    VRF
                                </th>

                                <td>

                                    <%=Math.Round(Convert.ToDecimal(0)).ToString("N"+Settings.LoginInfo.DecimalValue) %>
                                </td>
                            </tr>
                            <%if (itinerary.CDW > -1)
                                  { %>
                                <tr>

                                    <th>
                                        CDW
                                    </th>
                                    <td>

                                        <%=Math.Round(itinerary.CDW,Settings.LoginInfo.DecimalValue).ToString("N"+ Settings.LoginInfo.DecimalValue) %>

                                    </td>


                                </tr>

                                <%} %>
                                    <%if (itinerary.SCDW > -1)
                                  { %>
                                        <tr>
                                            <th>
                                                SCDW
                                            </th>
                                            <td>

                                                <%=Math.Round(itinerary.SCDW,Settings.LoginInfo.DecimalValue).ToString("N"+ Settings.LoginInfo.DecimalValue) %>

                                            </td>
                                        </tr>
                                        <%} %>
                                            <%if (itinerary.PAI > -1)
                                  { %>
                                                <tr>
                                                    <th>
                                                        PAI
                                                    </th>
                                                    <td>

                                                        <%=Math.Round(itinerary.PAI,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                    </td>
                                                </tr>
                                                <%} %>
                                                    <%if (itinerary.CSEAT > -1)
                                  { %>
                                                        <tr>
                                                            <th>
                                                                CSEAT
                                                            </th>
                                                            <td>

                                                                <%=Math.Round(itinerary.CSEAT,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                            <%if (itinerary.VMD > -1)
                                  { %>
                                                                <tr>
                                                                    <th>
                                                                        VMD
                                                                    </th>
                                                                    <td>


                                                                        <%=Math.Round(itinerary.VMD,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                                    </td>
                                                                </tr>
                                                                <%} %>
                                                                    <%if (itinerary.Emirates_Link_Charge > -1)
                                  { %>
                                                                        <tr>
                                                                            <th>
                                                                                Link Charges :
                                                                            </th>
                                                                            <td>


                                                                                <%=Math.Round(itinerary.Emirates_Link_Charge,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                                            </td>
                                                                        </tr>
                                                                        <%} %>
                                                                            <%if (itinerary.AirportCharge > -1)
                                  { %>
                                                                                <tr>
                                                                                    <th>
                                                                                        Airport Charge
                                                                                    </th>
                                                                                    <td>



                                                                                        <%=Math.Round(itinerary.AirportCharge,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>

                                                                                <%if (itinerary.GPS > -1)
                                  { %>
                                                                                <tr>
                                                                                    <th>
                                                                                        GPS
                                                                                    </th>
                                                                                    <td>



                                                                                        <%=Math.Round(itinerary.GPS,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>
                                                                                 <%if (itinerary.CHAUFFER > -1)
                                  { %>
                                                                                <tr>
                                                                                    <th>
                                                                                        CHAUFFER
                                                                                    </th>
                                                                                    <td>



                                                                                        <%=Math.Round(itinerary.CHAUFFER,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>
                                                                                    <% gtotal += Convert.ToDecimal(itinerary.Price.NetFare + itinerary.Price.AsvAmount - itinerary.Price.Discount + itinerary.Price.Tax); %>
 

                                                                                       <!--  <%if(pageMarkup > 0){ %>

                                                                                            <tr>
                                                                                                <th>
                                                                                                    <b>Addl Markup </b>
                                                                                                </th>
                                                                                                <td>
                                                                                                    <b class="spnred">
                                        <%=Math.Round(pageMarkup,Settings.LoginInfo.DecimalValue).ToString("N"+Settings.LoginInfo.DecimalValue)%></b>
                                                                                                </td>
                                                                                            </tr> 
                                                                                            <%} %>  -->
                                                                                                <tr>
                                                                                                    <th>
                                                                                                        <b>Grand Total </b>
                                                                                                    </th>
                                                                                                    <td>
                                                                                                        <b class="spnred"><%=itinerary.Price.Currency %>
                                        <%=(Math.Ceiling(gtotal)).ToString("N" + Settings.LoginInfo.DecimalValue)%></b>
                                                                                                    </td>
                                                                                                </tr>
                    </table>

                </div>
                
                <div style=" float:right; padding-top:20px; padding-bottom:10px; text-align:right">


                    <asp:Button ID="ImageButton1" OnClientClick="return viewVoucher();" Text="View Voucher" CssClass="btn but_b" runat="server" />
                </div>
                <div class="col-md-12">
                <table>
                 <tr>
                                <td>
                                  <strong>Cancellation Policy:</strong> 
                                </td>
                            </tr>
                             <tr>
                               <td style='padding: 5px;'>
                        <div style='padding: 0px 10px 0px 10px'>
                                   <li>No charges for cancellation of booking within 24
                                       hours</li>
                                   <li>10% of total bill will be charged for booking cancellation
                                       fee if the cancellation is done less than 4 hours from the pickup time/delivery
                                       of vehicle.</li>
                                   <li>2 hours grace time given to pick the vehicle after
                                       the booking time. Exceptions are acceptable on a case to case basis and upon approval
                                       of Management.</li><br />
                              </div> </td>
                           </tr>
                </table>
                </div>

                <div id="printableArea" runat="server" style="border-style: solid;
        border-width: 1px;" visible="false">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td>
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td width="21%">
                                                    <div style='margin-top: 5px;'>
                                                     <img src="<%=logoPath %>" width='180px' height='60px' />
                                                        </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                            border: solid 1px; border-collapse: collapse">
                                        <tbody>
                                            <tr>
                                                <td style="background: #1c498a; height: 24px; line-height: 22px; font-size: 13px;
                                        color: #fff; padding-left: 10px; font-weight: bold;" colspan="2">
                                                    <label>
                                            Voucher Details</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Fleet Name: </strong>
                                                    <asp:Label ID="lblFleetName" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                    <strong>Date of Issue:</strong>
                                                    <asp:Label ID="lblBookingDate" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="25">
                                                    <strong>Booking Ref:</strong>
                                                    <asp:Label ID="lblBookingRef" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="background: #1c498a; height: 24px; line-height: 22px; font-size: 13px;
                        color: #fff; padding-left: 10px; font-weight: bold;">
                                    <label>
                            Guest Details</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                            border: solid 1px; border-collapse: collapse" class="pd15 padd10 ">
                                        <tbody>
                                            <tr>
                                                <td width="50%" height="20" valign="top">
                                                    <strong>Title :</strong>
                                                    <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td width="50%">
                                                    <strong>First Name :</strong>
                                                    <asp:Label ID="lblFirstName" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" valign="top">
                                                    <strong>Last Name :</strong>
                                                    <asp:Label ID="lblLastName" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                    <strong>Email :</strong>
                                                    <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            
                                           
                                            <tr>
                                                <td height="20" valign="top">
                                                    <strong>Pickup Date </strong>:
                                                    <asp:Label ID="lblFromDate" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                    <strong>Drop Date :</strong>
                                                    <asp:Label ID="lblToDate" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" valign="top">
                                                    <strong>Pickup Location  </strong>:
                                                    <asp:Label ID="lblPickupLocation" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                    <strong> <asp:Label ID="lblDropupLocation" runat="server" Text="Drop Location:" Visible="false"></asp:Label></strong>
                                                    <asp:Label ID="lblDropupLocationValue" runat="server" Text="" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td height="20" valign="top">
                                                    <strong>Mobile No1 :</strong>
                                                    <asp:Label ID="lblMobileNo" runat="server" Text=""></asp:Label>
                                                </td>
                                                 <td>
                                                  <strong> <asp:Label ID="lblMobileNo2" runat="server" Text="Mobile No2:" Visible="false"></asp:Label></strong>
                                                    <asp:Label ID="lblMobileNo2Value" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
<tr>
                                                <td height="20" valign="top">
                                                 <strong> <asp:Label ID="lblPickupAddress" runat="server" Text="Pickup Address:" Visible="false"></asp:Label></strong>
                                                    <asp:Label ID="lblPickupAddressValue" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                   <strong><asp:Label ID="lblLandmark" runat="server" Text="Landmark:" Visible="false"></asp:Label></strong>
                                                    <asp:Label ID="lblLandmarkValue" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                                 <tr>
                                <td style="background: #1c498a; height: 24px; line-height: 22px; font-size: 13px;
                        color: #fff; padding-left: 10px; font-weight: bold;">
                                    <label>
                            Payment Details</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="1" cellspacing="0" cellpadding="0">
                                        <%--<tr>

                                            <td>
                                                <b>Name</b>
                                            </td>

                                            <td>
                                                <b>Total</b>
                                            </td>
                                        </tr>--%>
                                        <% decimal gtotal = 0,pageMarkup=0;%>
                                            <tr>

                                                <td>
                                                    Car Rental Charges
                                                </td>
                                                <td>

                                                    <%pageMarkup = itinerary.Price.AsvAmount; %>


                                                        <%=Math.Round(itinerary.Price.NetFare - +itinerary.Price.Discount, Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                </td>


                                            </tr>
                                            <tr>

                                                <td>
                                                    VRF
                                                </td>

                                                <td>

                                                    <b><%=Math.Round(Convert.ToDecimal(0)).ToString("N"+Settings.LoginInfo.DecimalValue) %></b>
                                                </td>
                                            </tr>
                                            <%if (itinerary.CDW > -1)
                                  { %>
                                                <tr>

                                                    <td>
                                                        CDW
                                                    </td>
                                                    <td>


                                                        <%=Math.Round(itinerary.CDW,Settings.LoginInfo.DecimalValue).ToString("N"+ Settings.LoginInfo.DecimalValue) %>

                                                    </td>


                                                </tr>

                                                <%} %>
                                                    <%if (itinerary.SCDW > -1)
                                  { %>
                                                        <tr>
                                                            <td>
                                                                SCDW
                                                            </td>
                                                            <td>

                                                                <%=Math.Round(itinerary.SCDW,Settings.LoginInfo.DecimalValue).ToString("N"+ Settings.LoginInfo.DecimalValue) %>

                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                            <%if (itinerary.PAI > -1)
                                  { %>
                                                                <tr>
                                                                    <td>
                                                                        PAI
                                                                    </td>
                                                                    <td>

                                                                        <%=Math.Round(itinerary.PAI,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                                    </td>
                                                                </tr>
                                                                <%} %>
                                                                    <%if (itinerary.CSEAT > -1)
                                  { %>
                                                                        <tr>
                                                                            <td>
                                                                                CSEAT
                                                                            </td>
                                                                            <td>

                                                                                <%=Math.Round(itinerary.CSEAT,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                                            </td>
                                                                        </tr>
                                                                        <%} %>
                                                                            <%if (itinerary.VMD > -1)
                                  { %>
                                                                                <tr>
                                                                                    <td>
                                                                                        VMD
                                                                                    </td>
                                                                                    <td>


                                                                                        <%=Math.Round(itinerary.VMD,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>
                                                                                                                        <%if (itinerary.GPS > -1)
                                  { %>
                                                                                <tr>
                                                                                    <td>
                                                                                        GPS
                                                                                    </td>
                                                                                    <td>


                                                                                        <%=Math.Round(itinerary.GPS,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>
                                                                                 <%if (itinerary.CHAUFFER > -1)
                                  { %>
                                                                                <tr>
                                                                                    <td>
                                                                                        CHAUFFER
                                                                                    </td>
                                                                                    <td>


                                                                                        <%=Math.Round(itinerary.CHAUFFER, Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>
                                                                                    <%if (itinerary.Emirates_Link_Charge > -1)
                                  { %>
                                                                                        <tr>
                                                                                            <td>
                                                                                                Link Charges :
                                                                                            </td>
                                                                                            <td>


                                                                                                <%=Math.Round(itinerary.Emirates_Link_Charge,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                                                            </td>
                                                                                        </tr>
                                                                                        <%} %>
                                                                                            <%if (itinerary.AirportCharge > -1)
                                  { %>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        Airport Charge
                                                                                                    </td>
                                                                                                    <td>

                                                                                                        <%=Math.Round(itinerary.AirportCharge,Settings.LoginInfo.DecimalValue).ToString("N" + Settings.LoginInfo.DecimalValue)%>

                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%} %>
                                                                                                    <% gtotal += Convert.ToDecimal(itinerary.Price.NetFare - itinerary.Price.Discount + itinerary.Price.Tax); %>

                                                                                                    <!--    <%if(pageMarkup > 0){ %>

                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <b>Addl Markup </b>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <b class="spnred"> 
                                        <%=Math.Round(pageMarkup,Settings.LoginInfo.DecimalValue).ToString("N"+Settings.LoginInfo.DecimalValue)%></b>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <%} %>  -->
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <b>Grand Total </b>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <b class="spnred"><%=itinerary.Price.Currency %>
                                        <%=(Math.Ceiling(gtotal)).ToString("N"+Settings.LoginInfo.DecimalValue)%></b>
                                                                                                                    </td>
                                                                                                                </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                             <tr>
                                <td>
                                  <strong>Cancellation Policy:</strong> 
                                </td>
                            </tr>
                             <tr>
                               <td style='padding: 5px;'>
                        <div style='padding: 0px 10px 0px 10px'>
                                   <li>No charges for cancellation of booking within 24
                                       hours</li>
                                   <li>10% of total bill will be charged for booking cancellation
                                       fee if the cancellation is done less than 4 hours from the pickup time/delivery
                                       of vehicle.</li>
                                   <li>2 hours grace time given to pick the vehicle after
                                       the booking time. Exceptions are acceptable on a case to case basis and upon approval
                                       of Management.</li><br />
                              </div> </td>
                           </tr>
                            <tr>
                                <tr>
                                    <td height="40" align="center">
                                        <div style="border: solid 1px; padding: 10px; text-align: center">
                                             Tel. No. :+971 600522200
                                            <br /> Email : <a style="color: #ff7800; text-decoration: none;" href="mailto:info@sayararental.com">
                                info@sayararental.com</a>
                                        </div>
                                    </td>
                                </tr>
                            </tr>
                            <tr>
                                <td>
                                    <center>
                                        <div>
                                            Booked and payable by Sayara services.</div>
                                    </center>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </asp:Content>
