using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
//using CrystalDecisions.CrystalReports.Engine;
//using CrystalDecisions.Shared;
using CT.Roster;

public partial class ROSReceiptMasterUI : CT.Core.ParentPage
{
    int decimalNos = 2;

    private enum PageMode
    {
        Add=1,
        Update=2,
        Redirect=3
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            hdfCurrentDate.Value = DateTime.Now.ToString("MM-dd-yyyy");
           // hdfDecimalNo.Value = Utility.ToString(Settings.LoginInfo.LocationDecimals);
            //decimalNos = Settings.LoginInfo.LocationDecimals;
            if (!IsPostBack)
            {
               // ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;

               
                    hdfVMSDocNo.Value = Utility.ToString(Request.QueryString["visaDocNo"]);
                    hdfVisaNo.Value = Utility.ToString(Request.QueryString["visaNo"]);
                    hdfVisaRefId.Value = Utility.ToString(Request.QueryString["vsId"]);
                    hdfNrmHandActivity.Value = Utility.ToString(Request.QueryString["RefType"]);
                    hdfPaxName.Value = Utility.ToString(Request.QueryString["paxName"]);
                    hdfGrntEmail.Value = Utility.ToString(Request.QueryString["grntEmail"]);

                    //setDecimals();
                    //decimalNos = Utility.ToInteger(hdfDecimalNo.Value);
                    InitializeControls();
                lblSuccessMsg.Text = string.Empty;
             if(!string.IsNullOrEmpty(Request.QueryString["Mode"]))
                {
                 if(Utility.ToString(Request.QueryString["Mode"])=="R")
                 {
                    Mode = PageMode.Redirect;
                    txtPaxName.Text = Utility.ToString(Request.QueryString["paxName"]);
                    int adult = Utility.ToInteger(Request.QueryString["adult"]);
                    int child = Utility.ToInteger(Request.QueryString["child"]);
                    int infant = Utility.ToInteger(Request.QueryString["infant"]);
                    ddlAdults.SelectedValue = Utility.ToString(adult);
                    ddlChildren.SelectedValue = Utility.ToString(child);
                    ddlInfants.SelectedValue = Utility.ToString(infant);
                   
                 }
                 else
                 {
                     Mode = PageMode.Redirect;
                 }
                 
                }
                else Mode=PageMode.Add;
            }
            //decimalNos = Utility.ToInteger(hdfDecimalNo.Value);
            
        }
        catch (Exception ex)
        {

            Utility.WriteLog(ex, this.Title);
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
           // lblMasterError.Text = ex.Message;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        try
        {
            //Utility.StartupScript(this.Page, " alert('hi');", " showHidMode1aaa");
            Utility.StartupScript(this.Page, " showHidMode(" + ddlSettlementMode.SelectedItem.Value + ")", " showHidMode");
            //StartupScript(this.Page, " showHidMode(" + ddlSettlementMode.SelectedItem.Value + ")", " showHidMode");
        }
        catch { }
    }
    # region Page Members

    private PageMode Mode
    {
        get { return (PageMode)ViewState["_Mode"]; }
        set { ViewState["_Mode"] = value; }
    }
  
    # endregion

    # region Private Methods
    private void InitializeControls()
    {
        try
        {            
            for (int i = 0; i < 20; i++)
            {
                ddlAdults.Items.Add(new ListItem(i.ToString(),i.ToString()));
                ddlChildren.Items.Add(new ListItem(i.ToString(),i.ToString()));
                ddlInfants.Items.Add(new ListItem(i.ToString(),i.ToString()));
            }
            BindSettlementMode();
            BindCurrency();
            BindCardType();
            BindCompany();
            BindVehicleList();
            BindStaffList();
            //BindTKTAirline();
            //ListItem item = ddlCurrency.Items.FindByText(Settings.LoginInfo.Currency);
            //ddlCurrency.ClearSelection();
            //item.Selected = true;
            //ddlCurrency.Enabled = false;
            clearControls();
            txtExchRate.Text = Formatter.ToCurrency(Utility.ToDecimal(Settings.LoginInfo.ExchangeRate));//,decimalNos );
           // txtHandlingFee.Text = Formatter.ToCurrency(Utility.ToDecimal(hdfHandlingFee.Value) * Utility.ToDecimal(txtExchRate.Text));
            //rbnSales.Checked = true;//TODO brahmam
           // txtHandlingFee.Text = Formatter.ToCurrency(Utility.ToDecimal(hdfHandlingFee.Value));
        }
        
        catch { throw; }

    }
    private void clearControls()
    {
        try
        {

            // txtDocumentNumber.Text = string.Empty;
            //dcDocDate.Clear();

            ReceiptMaster tempObject = new ReceiptMaster();
            //BindMobileProviders(tempObject.MobProviders);
            //Settings.LoginInfo.AgentBalance = tempObject.AgentBalance;
            //((Label)(this.Master.FindControl("lblAgentBalance"))).Text = Utility.ToString(tempObject.AgentBalance);
            txtDocType.Text = tempObject.DocType;
            dcDocDate.Value = tempObject.DocDate;
            txtPaxName.Text = string.Empty;
            //ddlPaxMobCode.SelectedIndex = 0;
            txtPaxMobNo.Text = string.Empty;
            //txtPaxEmail.Text = string.Empty;
            ddlStaff.SelectedIndex = 0;
            ddlAdults.SelectedIndex = 1;
            ddlChildren.SelectedIndex = 0;
            ddlInfants.SelectedIndex = 0;
            txtFare.Text = Formatter.ToCurrency(0);//,decimalNos);
            txtTotalFare.Text = Formatter.ToCurrency(0);//,decimalNos);
            txtCashBaseAmount.Text = Formatter.ToCurrency(0);//, decimalNos);            
            hdfCardCharge.Value =Utility.ToString(0);
            txtTotCardCharge.Text = Formatter.ToCurrency(0);//, decimalNos);    
               
            txtRemarks.Text = string.Empty;
            txtCreatedBy.Text = string.Format("{0} {1}", Settings.LoginInfo.FirstName, Settings.LoginInfo.LastName);
            //txtHandlingFee.Text = Formatter.ToCurrency(Utility.ToDecimal(hdfHandlingFee.Value) * Utility.ToDecimal(txtExchRate.Text));//,decimalNos); 
            Utility.StartupScript(this.Page, "TotalFare()", "TotalFare");
            ReceiptMasterObject=null;
            //if (Mode == PageMode.Update) Response.Redirect("ChequeReturnAdvice.aspx");

            ddlSettlementMode.SelectedIndex = 1;
            //ddlCurrency.SelectedIndex = 1;
            txtCashBaseAmount.Text = txtCashLocalAmount.Text = Formatter.ToCurrency("0");//,decimalNos);
            txtSettlementReceived.Text = Formatter.ToCurrency("0");//,decimalNos);
            txtModeRemarks.Text = string.Empty;

            ListItem item = ddlCurrency.Items.FindByText(Settings.LoginInfo.Currency);
            ddlCurrency.ClearSelection();
            item.Selected = true;
            ddlCurrency.Enabled = false;
            
            string[] exchRate = ddlCurrency.SelectedItem.Value.Split('~');
            Utility.StartupScript(this.Page, "setExchRate(" + exchRate[1] + ");", "setExchRate;");
            
            if (Settings.LoginInfo.AgentId > 1) ddlSettlementMode.SelectedIndex = 2;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ");", "setExchRate;");
            //Utility.StartupScript(this.Page, "setExchRate('" + ddlCurrency.SelectedItem.Text + "');", "setExchRate;");
            ddlCardType.Enabled = true;
            ddlCardType.SelectedIndex = 0;
            dcFromDate.Clear();
            dcToDate.Clear();
            hdfServiceType.Value = string.Empty;                
            txtNotes.Text = string.Empty;
            ddlCompany.SelectedIndex = 0;
            ddlStaff_SelectedIndexChanged(null,null);
        }
        catch { throw; }
    }
    private void setDecimals()
    {
        try
        {
            long locationId = Settings.LoginInfo.LocationID;
            string[] locIds = hdfDecimalLocations.Value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] locAndDecimals = hdfDecimals.Value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (locIds.Length > 0)
            {
                foreach (string locId in locIds)
                {
                    if ( locationId== Utility.ToLong(locId))
                    {
                        foreach (string locDecimals in locAndDecimals)
                        {
                            string[] decimalValues = locDecimals.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                            if (locationId == Utility.ToLong(decimalValues[0])) hdfDecimalNo.Value = Utility.ToString(decimalValues[1]);
                        }                       
                        break;
                    }
                }

            }
        }
        catch
        { throw; }
    
    }
    //TODO brahmam
    protected void rbnSales_OnCheckedChaged(object sender, EventArgs e)
    {
        try
        {
           // BindHandlingFee("S");
        }
        catch(Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    //TODO brahmam
    protected void rbnPayments_OnCheckedChaged(object sender, EventArgs e)
    {
        try
        {
            //BindHandlingFee("P");
        }
        catch(Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }


    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
        try
        {
            BindStaffList();
            
            Utility.StartupScript(this.Page, " ShowTariffGridValues(); ", "ShowTariffGridValues");
        
        }
        catch (Exception ex)
        { }
    }
    protected void ddlStaff_SelectedIndexChanged(object sender, EventArgs e)
        {
        try
        {
            lblTrfCodeValue.Text = string.Empty;
            txtTrfAmount.Text = txtFare.Text =Formatter.ToCurrency(0); 
           DataSet ds = ReceiptMaster.PendingReciptList(Utility.ToInteger(ddlStaff.SelectedItem.Value));
           DataTable dtTarifDtls = ds.Tables[0];
           if (dtTarifDtls.Rows.Count > 0)
           {
               lblTrfCodeValue.Text = dtTarifDtls.Rows[0]["trf_Code"].ToString();
               txtTrfAmount.Text = Formatter.ToCurrency(dtTarifDtls.Rows[0]["emp_trf_amount"].ToString());
               if (dcFromDate.Value  == DateTime.MinValue || dcToDate.Value == DateTime.MinValue)
               {
                   txtFare.Text = Formatter.ToCurrency(txtTrfAmount.Text.Trim());
               }
               else
               {
                   DateTime fromDate = dcFromDate.Value;
                   DateTime toDate = dcToDate.Value;
                   double diff = (toDate - fromDate).TotalDays + 1;
                   decimal noOfDays = 30;
                   decimal actualTrfAmount = 0;
                   if (diff > 0)
                   {
                       actualTrfAmount = (Utility.ToDecimal(txtTrfAmount.Text.Trim()) / noOfDays) * Utility.ToDecimal(diff);
                       txtFare.Text = Formatter.ToCurrency(actualTrfAmount);
                   }
               }

           }

            DataTable dtPendings = new DataTable();
           if (ds != null)
           {               
               dtPendings = ds.Tables[1];
               gvPendingRcpts.DataSource = dtPendings;
               gvPendingRcpts.DataBind();

               //CommonGrid grid = new CommonGrid();
               //grid.BindGrid(gvPendingRcpts, dtPendings);
           }

           Utility.StartupScript(this.Page, "TotalFare()", "tariff");
        }
        catch (Exception ex)
        { }
    }


    //private void BindHandlingFee(string handlingType)
    //{ 
    //    try
    //    {
    //        HandlingFeeList = HandlingFeeMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Long, RecordStatus.Activated, handlingType);
    //        rdblHandlingFee.DataSource = HandlingFeeList;
    //        rdblHandlingFee.DataValueField = "handling_id";
    //        rdblHandlingFee.DataTextField = "handling_element";
    //        rdblHandlingFee.DataBind();
    //        DataTable dt = HandlingFeeList;
    //        DataRow[] dr = dt.Select("handling_fee_status='D'");

    //        if (Utility.ToLong(hdfVisaRefId.Value) > 0)
    //        {
    //            dr = dt.Select("handling_fee_activity=" + "'" + hdfNrmHandActivity.Value + "'");
    //        }
    //        if (dr[0] != null)
    //        {
    //            rdblHandlingFee.SelectedValue = Utility.ToString(dr[0]["handling_id"]);
    //            txtHandlingFee.Text = Formatter.ToCurrency(dr[0]["handling_fee"]);//, decimalNos);
    //            hdfHandlingFee.Value = Formatter.ToCurrency(dr[0]["handling_fee"]);//, decimalNos);
    //            string handleActivity = Utility.ToString(dr[0]["handling_fee_activity"]);
    //            hdfNrmHandActivity.Value = handleActivity;
    //            if (handleActivity == "TPO" || handleActivity == "TPI" || handleActivity == "HALA" || handleActivity == "MEET" || handleActivity == "PINK" || handleActivity == "OK2B" || handleActivity == "OK2BREQ")
    //            {
    //                //Utility.StartupScript(this.Page, "document.getElementById('trHandling').style.display='block';", "display1");
    //                Utility.StartupScript(this.Page, "document.getElementById('trVisaDocNo').style.display='block';", "display1");
    //                Utility.StartupScript(this.Page, "document.getElementById('trVisaNumber').style.display='block';", "display2");
    //                if (Utility.ToString(dr[0]["handling_fee_activity"]) == "OK2B" || Utility.ToString(dr[0]["handling_fee_activity"]) == "OK2BREQ")
    //                {
    //                    Utility.StartupScript(this.Page, "document.getElementById('tdAirline').style.display='block';", "display3");
    //                    Utility.StartupScript(this.Page, "document.getElementById('tdLBLtktAirline').style.display='none';", "display4");
    //                    Utility.StartupScript(this.Page, "document.getElementById('tdDDLtktAirline').style.display='none';", "display5");
                           
    //                }

    //            }

    //            if (Utility.ToString(dr[0]["handling_edit_status"]) == "Y" || Settings.LoginInfo.AgentId==1)
    //            {
    //                Utility.StartupScript(this.Page, "enableControls(false);", "editStatus");
    //                //txtHandlingFee.Enabled = true;
    //            }
    //            else
    //            {
    //                //Utility.StartupScript(this.Page, "document.getElementById('<%= txtHandlingFee.ClientID %>').disabled;", "editStatus1");
    //                Utility.StartupScript(this.Page, "enableControls(true);", "editStatus1");
    //                //txtHandlingFee.Enabled =false;
    //            }
    //        }
    //        //clearControls();
    //        txtExchRate.Text = Formatter.ToCurrency(Utility.ToDecimal(Settings.LoginInfo.ExchangeRate));//, decimalNos);
    //        txtHandlingFee.Text = Formatter.ToCurrency(Utility.ToDecimal(hdfHandlingFee.Value) * Utility.ToDecimal(txtExchRate.Text));//, decimalNos);
    //    }
    //    catch(Exception ex)
    //    {
    //        Utility.WriteLog(ex, this.Title);
    //        Label lblMasterError = (Label)this.Master.FindControl("lblError");
    //        lblMasterError.Visible = true;
    //        lblMasterError.Text = ex.Message;
    //    }
    //}
    
    private void Save()
    {
        try
        {
            decimal totalInvAmount = 0;
            string pendingInvIds = string.Empty;
            if (ddlStaff.SelectedIndex > 0)
            {
                foreach (GridViewRow gvRow in gvPendingRcpts.Rows)
                {
                    CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                    HiddenField hdfRcptId = (HiddenField)gvRow.FindControl("IThdfRcptId");
                    Label ITlblInvAmount = (Label)gvRow.FindControl("ITlblAmount");

                    if (chkSelect.Checked)
                    {
                        if (string.IsNullOrEmpty(pendingInvIds))
                            pendingInvIds = Utility.ToString(hdfRcptId.Value);
                        else
                            pendingInvIds = pendingInvIds + "," + Utility.ToString(hdfRcptId.Value);

                        totalInvAmount = totalInvAmount + Utility.ToDecimal(ITlblInvAmount.Text.Trim());
                    }

                }
            }
            if(!string.IsNullOrEmpty(pendingInvIds)  && totalInvAmount!=Utility.ToDecimal(txtToCollect.Text.Trim())) throw new Exception("Selected Pending invoices total amount and Tocollect Amount should be Same !");
            if (!string.IsNullOrEmpty(pendingInvIds) && ddlSettlementMode.SelectedItem.Value != "1") throw new Exception("If payment against pending invoices then settlement mode should be Cash !");
            ReceiptMaster receipt;
            if (Mode != PageMode.Update) receipt = new ReceiptMaster();
            else receipt = ReceiptMasterObject;

            receipt.DocDate = dcDocDate.Value;
            receipt.DocType = txtDocType.Text.Trim();
            receipt.PaxName = txtPaxName.Text.Trim();
            receipt.PaxMobileNo = txtPaxMobNo.Text.Trim();                       
            receipt.Adults = Utility.ToInteger(ddlAdults.SelectedValue);
            receipt.Children = Utility.ToInteger(ddlChildren.SelectedValue);
            receipt.Infants = Utility.ToInteger(ddlInfants.SelectedValue);
            receipt.Fare = Utility.ToDecimal(txtFare.Text.Trim());
            receipt.TotalFare = Utility.ToDecimal(txtTotalFare.Text.Trim());
            receipt.Remarks = Utility.ToString(txtRemarks.Text.Trim());

            receipt.LocationId = Settings.LoginInfo.LocationID;
            //receipt.AgentId = Settings.LoginInfo.AgentId;
            receipt.Status = Settings.ACTIVE;
            receipt.CreatedBy = Settings.LoginInfo.UserID;
                        
            //receipt.HandlingActivity = hdfServiceType.Value;
            //Settlement Details
            PaymentMode mode = PaymentMode.Cash;
            switch (ddlSettlementMode.SelectedItem.Value)
            {
                case "1":
                    mode = PaymentMode.Cash;
                    break;
                case "2":
                    mode = PaymentMode.Credit;
                    break;
                case "3":
                    mode = PaymentMode.Card;
                    receipt.CardName  = ddlCardType.SelectedItem.Text;
                    receipt.CardRate = Utility.ToDecimal(ddlCardType.SelectedItem.Value);
                    receipt.CardTotal = Utility.ToDecimal(txtTotCardCharge.Text);
                    break;
                case "4":
                    mode = PaymentMode.Cash_Credit;
                    break;
                case "5":
                    mode = PaymentMode.Cash_Card;
                    receipt.CardName = ddlCardType.SelectedItem.Text;
                    receipt.CardRate = Utility.ToDecimal(ddlCardType.SelectedItem.Value);
                    receipt.CardTotal = Utility.ToDecimal(txtTotCardCharge.Text);
                    break;
                case "6":
                    mode = PaymentMode.Cash_Credit_Card;
                    receipt.CardName = ddlCardType.SelectedItem.Text;
                    receipt.CardRate = Utility.ToDecimal(ddlCardType.SelectedItem.Value);
                    receipt.CardTotal = Utility.ToDecimal(txtTotCardCharge.Text);
                    break;
                case "7":
                    mode = PaymentMode.Employee;
                    break;
                case "8":
                    mode = PaymentMode.Others;
                    break;
            }
            //PaymentMode mode = (PaymentMode)ddlSettlementMode.SelectedItem.Value;
            receipt.SettlementMode = Utility.ToString(Convert.ToInt32(mode));
            receipt.CurrencyCode = ddlCurrency.SelectedItem.Text; ;
            receipt.ExchangeRate = Utility.ToDecimal(txtExchRate.Text.Trim());

            decimal handlingFeetype = 1;

            if (mode == PaymentMode.Cash || mode == PaymentMode.Cash_Credit || mode == PaymentMode.Cash_Card || mode == PaymentMode.Cash_Credit_Card) receipt.CashMode.Set("CASH", handlingFeetype * Utility.ToDecimal(txtCashBaseAmount.Text.Trim()), handlingFeetype * Utility.ToDecimal(txtCashLocalAmount.Text.Trim()));
            if (mode == PaymentMode.Credit || mode == PaymentMode.Cash_Credit || mode == PaymentMode.Cash_Credit_Card) receipt.CreditMode.Set("CREDIT", handlingFeetype * Utility.ToDecimal(txtCreditBaseAmount.Text.Trim()), handlingFeetype * Utility.ToDecimal(txtCreditLocalAmount.Text.Trim()));
            if (mode == PaymentMode.Card || mode == PaymentMode.Cash_Card || mode == PaymentMode.Cash_Credit_Card) receipt.CardMode.Set("CARD", handlingFeetype * Utility.ToDecimal(txtCardBaseAmount.Text.Trim()), handlingFeetype * Utility.ToDecimal(txtCardLocalAmount.Text.Trim()));
            if (mode == PaymentMode.Employee) receipt.EmployeeMode.Set("Employee", handlingFeetype * Utility.ToDecimal(txtCashBaseAmount.Text.Trim()), handlingFeetype * Utility.ToDecimal(txtCashLocalAmount.Text.Trim()));
            if (mode == PaymentMode.Others) receipt.OthersMode.Set("Others", handlingFeetype * Utility.ToDecimal(txtCashBaseAmount.Text.Trim()), handlingFeetype * Utility.ToDecimal(txtCashLocalAmount.Text.Trim()));

            receipt.LocalToCollect = handlingFeetype * Utility.ToDecimal(txtToCollect.Text);
            receipt.BaseToCollect = handlingFeetype * Utility.ToDecimal(txtToCollect.Text);
            //receipt.HandlingFee = handlingFeetype * Utility.ToDecimal(txtHandlingFee.Text.Trim());
            receipt.TotalFare = handlingFeetype * Utility.ToDecimal(txtTotalFare.Text.Trim());
            receipt.ModeRemarks = txtModeRemarks.Text.Trim();
            receipt.CompanyId = Utility.ToInteger(ddlCompany.SelectedItem.Value);
            receipt.StaffIdName = ddlStaff.SelectedItem.Text;
            if (string.IsNullOrEmpty(receipt.PaxName)) receipt.PaxName = receipt.StaffIdName;
            receipt.EmployeeId = Utility.ToInteger(ddlStaff.SelectedItem.Value);
            receipt.VehicleId = Utility.ToInteger(ddlVehicle.SelectedItem.Value);
            receipt.FromDate = dcFromDate.Value;
            receipt.ToDate = dcToDate.Value;
            receipt.Notes = txtNotes.Text.Trim();
            receipt.EmpTariffAmount=Utility.ToDecimal(txtTrfAmount.Text.Trim());
            receipt.PendingCreditRcpts = pendingInvIds;
            receipt.Save();
            //string message = string.Format("Cheque Return Entry Saved !", (Mode == PageMode.Add) ? "Created" : "Updated", txtDefaultingCustomer.Text.Trim());
            //this.Master.ShowStatus("Cheque Return Entry", message);
            txtDocumentNumber.Text = receipt.DocNumber;
            //Settings.LoginInfo.AgentBalance = receipt.AgentBalance;
            long reportId = receipt.TransactionId;
            //if (Mode == PageMode.Redirect)
            //{
            //    Session["RCPT_PNR_NO"] = txtStaffId.Text.Trim();
            //    Session["RCPT_TICKET_ID"] = reportId;
            //    Session["RCPT_TICKET_FARE"] = txtTotalFare.Text.Trim();
            //    string script = "parentUpdate();windowClose();";
            //    Utility.StartupScript(this.Page, script, "windowClose");
            //}
            //else
            //{
                string script = "window.open('printReport.aspx?receiptId=" + Utility.ToString(reportId) + "','','width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');window.open('ROSReceiptMaster.aspx','_self',false);";
                Utility.StartupScript(this.Page, script, "printReport");
                lblSuccessMsg.Text = Formatter.ToMessage("Receipt No", receipt.DocNumber, Mode == PageMode.Add ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);

                clearControls();
            //}
            //if(hdfNrmHandActivity.Value == "OK2BREQ") --Commented thwe mailing because mail will be sent after uploading all doc in upload page.
            //{
            //    DataSet dsReport = ReceiptMaster.GetOK2BReportDetails(reportId,string.Empty);
            //    if (dsReport.Tables[0].Rows.Count > 0)
            //    {
            //        string gsaAgentName = Utility.ToString(dsReport.Tables[0].Rows[0]["ok2b_gsa_agent_name"]);
            //        string gsaEmail1 = Utility.ToString(dsReport.Tables[0].Rows[0]["ok2b_gsa_email_1"]);
            //        string gsaEmail2 = Utility.ToString(dsReport.Tables[0].Rows[0]["ok2b_gsa_email_2"]);
            //        string createdUser = Utility.ToString(dsReport.Tables[0].Rows[0]["createdUser"]);
            //        string docNo = Utility.ToString(dsReport.Tables[0].Rows[0]["receipt_number"]);
            //        string locationName = Utility.ToString(dsReport.Tables[0].Rows[0]["location_name"]);  //mail Sending by
            //        string locationMail = Utility.ToString(dsReport.Tables[0].Rows[0]["location_email"]);

            //        GenerateReport(dsReport, gsaAgentName, gsaEmail1, gsaEmail2, locationMail, createdUser, locationName, docNo);
            //    }
            //}
            //25052016 if (!string.IsNullOrEmpty(receipt.RetMessage)) Utility.Alert(this.Page, receipt.RetMessage);
            
        }
        catch
        {   throw;     }

    }

    #region REPORT GENERATION

    protected void GenerateReport(DataSet dsReport, string gsaAgentName, string gsaEmail1, string gsaEmail2,string locationMail, string createdUser, string locationName, string docNo)
    {

        DiskFileDestinationOptions diskOpts;
        string targetFileName = null;
        ReportDocument rptDocument = null;
        try
        {
            string FType = "PDF";//Request.QueryString["FType"] == null ? "" : Request.QueryString["FType"];

          
            //dsReport.WriteXmlSchema(@"C:\XSD\TPOReport.xsd");
            // dsReport.WriteXmlSchema(@"C:\Developments\DotNet\2005\AirArabia\XSD\AA\consultant_productivity.xsd");
            ReportDocument rptdocument = new ReportDocument();
            rptdocument.Load(Server.MapPath("Reports/RptOK2BDetailsReport.rpt"));
            DataTable OK2BDetails = dsReport.Tables[0];
            rptdocument.SetDataSource(OK2BDetails);

            decimal Amount = Utility.ToDecimal(dsReport.Tables[0].Rows[0]["receipt_local_to_collect"]);
            //string docNo = Utility.ToString(dsReport.Tables[0].Rows[0]["receipt_number"]);         
           

            string amountInWord = NumberToEnglish.changeCurrencyToWords(Utility.ToDouble(Amount));
            rptdocument.SetParameterValue("AmountInWords", amountInWord);
            docNo = docNo.Replace("/", "-");
            //if (mailStatus == "U")
            //{
            //    targetFileName = Request.PhysicalApplicationPath + "Reports\\TempReports\\" + tpoDocNo + "Updated.pdf";
            //}
            //else
                targetFileName = Request.PhysicalApplicationPath + "Reports\\TempReports\\" + docNo + ".pdf";


            // Export configuration.    
            diskOpts = new DiskFileDestinationOptions();
            rptdocument.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
            rptdocument.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

            diskOpts.DiskFileName = targetFileName;
            rptdocument.ExportOptions.DestinationOptions = diskOpts;

            // Export report ... Server-Side.
            rptdocument.Export();


           // long reportId = 0;
           // string targetFile = Path.GetFileName(targetFileName);
           // string script = "window.open('RptOK2BDetailsReport.aspx?receiptId=" + Utility.ToString(reportId) + "&targetFile=" + targetFile + "','','width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
           // Utility.StartupScript(this.Page, script, "printTPOReport");

        }

        catch (Exception ex)
        {
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Write("An error occured while generating report.<br /> Error Details: <br />" + ex.Message);
            // ID_Exception.WriteExceptionLogEntry(ex);
        }
        finally
        {
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            if (rptDocument != null)
            {
                rptDocument.Close();
                rptDocument.Dispose();
                GC.Collect();
            }
            //if (!string.IsNullOrEmpty(mailStatus) && (mailStatus == "S" || mailStatus == "U"))
            //{
            //sendEmail("pdf", targetFileName, gsaAgentName, gsaEmail1, gsaEmail2, locationMail,createdUser, locationName, docNo);
            //}


        }
    }

  
    #endregion

    private void StartupScript(Page page, string script, string key)
    {

        script = string.Format("{0};", script);
        if (page != null && !string.IsNullOrEmpty(script) && !page.ClientScript.IsStartupScriptRegistered(key))
        {
            if (ScriptManager.GetCurrent(page) != null && ScriptManager.GetCurrent(page).IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(page, page.GetType(), key, script, true);
            else
                page.ClientScript.RegisterStartupScript(page.GetType(), key, script, true);

        }
    }
    //private void Edit(long receiptId)
    //{
    //    try
    //    {
    //        ReceiptMaster receipt = new ReceiptMaster(receiptId);
    //        dcDocDate.Value = receipt.DocDate;
    //        txtPaxName.Text = receipt.PaxName;
    //        txtStaffId.Text = receipt.PNR;
    //        ddlAdults.SelectedIndex = ddlAdults.Items.IndexOf(ddlAdults.Items.FindByValue(Utility.ToString(receipt.Adults)));
    //        ddlChildren.SelectedIndex = ddlChildren.Items.IndexOf(ddlChildren.Items.FindByValue(Utility.ToString(receipt.Children)));
    //        ddlInfants.SelectedIndex = ddlInfants.Items.IndexOf(ddlInfants.Items.FindByValue(Utility.ToString(receipt.Infants)));
    //        txtFare.Text = Utility.ToString(receipt.Fare);
    //        //rdblHandlingFee.SelectedValue = Utility.ToString(receipt.HandlingId);
    //        //txtHandlingFee.Text = Utility.ToString(receipt.HandlingFee);
    //        txtTotalFare.Text = Utility.ToString(receipt.TotalFare);
    //        txtRemarks.Text = receipt.Remarks;
    //        receipt.Status = Settings.ACTIVE;
    //        receipt.CreatedBy = Settings.LoginInfo.UserID;
    //        //dcDateOfTravel.Value = receipt.DateOfTravel;
    //        //if (Settings.LoginInfo.UserLevel == UserLevel.SuperUser || (Settings.LoginInfo.UserLevel == UserLevel.Administrator && companyKey == Settings.LoginInfo.CompanyInfo.Value))
    //        //{
    //        //    btnSave.Enabled = true;
    //        //}
    //        //else
    //        //{
    //        //    btnSave.Enabled = false;
    //        //} TODO viji




    //    }
    //    catch { throw; }
    //}
    # endregion

    # region Session Objects

    private ReceiptMaster ReceiptMasterObject
    {
        get
        {
            return (ReceiptMaster)Session["_ReceiptMasterObject"];
        }
        set
        {
            if (value == null) Session.Remove("_ReceiptMasterObject");
            else Session["_ReceiptMasterObject"] = value;
        }
    }
    private static  DataTable HandlingFeeList
    {
        get
        {
            return (DataTable)HttpContext.Current.Session["_ReceiptHandlingFeeList"];
        }
        set
        {
            if (value == null) HttpContext.Current.Session.Remove("_ReceiptHandlingFeeList");
            else
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["handling_id"] };
                HttpContext.Current.Session["_ReceiptHandlingFeeList"] = value;
            }
        }
    }
    # endregion

    # region Control Events
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            clearControls();
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
           
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();            
            hdfNrmHandActivity.Value = string.Empty ;
            hdfVisaRefId.Value =  string.Empty ;
            hdfVisaNo.Value =  string.Empty ;
            hdfVMSDocNo.Value = string.Empty ;
            hdfPaxName.Value = string.Empty ;
            hdfGrntEmail.Value= string.Empty ;
          // Response.Redirect("ReceiptMaster.aspx",false);
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            string script = "getElement('divPlsWait').style.display = 'none';";
            Utility.StartupScript(this.Page, script, "HidePlsWait");
            //StartupScript(this.Page, "alert('" + ex.Message + "');", "Err");
            Utility.Alert(this.Page, ex.Message);
        }
    }

    # endregion

    # region Web Methods
    [WebMethod]
    public static string[] GetSelectedFee(long handlingId,int deciVal)
    {
        try
        {
            //decimal fee = 0;
            DataRow dr = HandlingFeeList.Rows.Find(handlingId);
            if (dr != null)
            {  // if(Utility.ToString(dr["handling_fee_activity"])=="OK2B" )
                if(Settings.LoginInfo.AgentId>1)
                    return new string[] { Formatter.ToCurrency(dr["handling_fee"]), Utility.ToString(dr["handling_fee_activity"]), Utility.ToString(dr["HANDLING_EDIT_STATUS"]) };
                else
                    return new string[] { Formatter.ToCurrency(dr["handling_fee"]), Utility.ToString(dr["handling_fee_activity"]), "Y" };
                //return new string[] { Formatter.ToCurrency(dr["handling_fee"]), Utility.ToString(dr["handling_fee_activity"])};
            }
            else return new string[] { "0.00", "",""};
        }
        catch { throw; }
    }
    # endregion

    # region Settlement Details
    private void BindSettlementMode()
    {
        try
        {

            DataTable dtMode = UserMaster.GetMemberTypeList("settlement_mode").Tables[0];
            DataView dv = dtMode.DefaultView;
            dv.RowFilter = "FIELD_VALUE NOT IN ('6','8')";
            ddlSettlementMode.DataSource = dv.ToTable();
            ddlSettlementMode.DataValueField = "FIELD_VALUE";
            ddlSettlementMode.DataTextField = "FIELD_TEXT";
            ddlSettlementMode.DataBind();
            ddlSettlementMode.Items.Insert(0, new ListItem("--Select Mode--", "-1"));
            ddlSettlementMode.SelectedIndex = 1;
        }
        catch { throw; }
    }
    private void BindCurrency()
    {
        try
        {
            ddlCurrency.DataSource = CurrencyMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            ddlCurrency.DataValueField = "CURRENCY_ID";
            ddlCurrency.DataTextField = "CURRENCY_CODE";
            ddlCurrency.DataBind();
            ddlCurrency.Items.Insert(0, new ListItem("--Select Currency--", "0"));
            ddlCurrency.SelectedIndex = 1;
            string[] exchRate = ddlCurrency.SelectedItem.Value.Split('~');
           Utility.StartupScript(this.Page, "setExchRate(" + exchRate[1] + ")", "setExchRate");
            
            
        }
        catch { throw; }
    }
    private void BindCardType()
    {
        try
        {
            ddlCardType.DataSource = CreditCardMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            ddlCardType.DataValueField = "CARD_CHARGE";
            ddlCardType.DataTextField = "CARD_NAME";
            ddlCardType.DataBind();
            ddlCardType.Items.Insert(0, new ListItem("--Card Type--", "0"));
            ddlCardType.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }
    private void BindCompany()//Company
    {
        try
        {
            ddlCompany.DataSource = ReceiptMaster.GetCompanyList(ListStatus.Short, RecordStatus.Activated,"R");
            ddlCompany.DataValueField = "COMPANY_ID";
            ddlCompany.DataTextField = "COMPANY_NAME";
            ddlCompany.DataBind();
            ddlCompany.Items.Insert(0, new ListItem("-- Company --", "0"));
            ddlCompany.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }


    private void BindVehicleList()
    {
        try
        {
            ddlVehicle.DataSource = ROSVehicleMaster.GetList(RecordStatus.Activated, ListStatus.Short);
            ddlVehicle.DataValueField = "ve_id";
            ddlVehicle.DataTextField = "ve_name";
            ddlVehicle.DataBind();
            ddlVehicle.Items.Insert(0, new ListItem("--Select Vehicle--", "-1"));
            ddlVehicle.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }

    private void BindStaffList()
    {
        try
        {
            ddlStaff.DataSource = ReceiptMaster.GetReceiptStaffList("Crew", Utility.ToInteger(ddlCompany.SelectedItem.Value),ListStatus.Short );
            ddlStaff.DataValueField = "emp_Id";
            ddlStaff.DataTextField = "emp_Name";
            ddlStaff.DataBind();
            ddlStaff.Items.Insert(0, new ListItem("--Select Employee--", "0"));
            ddlStaff.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }
   
     # endregion
}
