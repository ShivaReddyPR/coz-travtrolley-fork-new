﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using System.Data;
using CT.BookingEngine;
using CT.AccountingEngine;
using System.Configuration;
using System.Collections;

public partial class PackageChangeRequestQueueGUI : CT.Core.ParentPage// System.Web.UI.Page
{
    PagedDataSource adsource;
    int pos;
    private int loggedMemberId;
    private AgentMaster agent;


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");

            AuthorizationCheck();
           
            if (!IsPostBack)
            {
                hdfParam.Value = "0";
                BindAgent();
                txtFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                //LoadQueue();
                this.ViewState["vs"] = 0;

                pos = (int)this.ViewState["vs"];
                databind();
            }
            

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from (PackageQueue Page):" + ex.ToString(), "");
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            //LoadQueue();
            this.ViewState["vs"] = 0;
            databind();
            //Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from (PackageQueue Page):" + ex.ToString(), "");
        }

    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }

    }

    protected void dlBookingQueue_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            loggedMemberId = (int)Settings.LoginInfo.UserID;
            if (e.CommandName == "Refund")
            {
                if (e.CommandArgument.ToString().Length > 0)
                {
                    Label leadPaxName = e.Item.FindControl("lblLeadPaxName") as Label;
                    Label leadPaxEmail = e.Item.FindControl("lblLeadPaxEmail") as Label;
                    TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
                    TextBox txtSupplierFee = e.Item.FindControl("txtSupplierFee") as TextBox;
                    Label lblError = e.Item.FindControl("lblError") as Label;
                    if (txtAdminFee.Text.Trim().Length > 0 && txtSupplierFee.Text.Trim().Length > 0)
                    {   
                        //Step-1:Get  Package_Transaction_Header details from tbl_Package_Transaction_Header by tranx id
                        DataTable dtDetails = CT.Core.Queue.GetPackageHeaderDetailsByTranxId(Convert.ToInt32(e.CommandArgument));

                        agent = new AgentMaster(Convert.ToInt32(dtDetails.Rows[0]["AgencyId"]));
                        if (dtDetails != null && dtDetails.Rows.Count > 0)
                        {

                            decimal bookingAmt = 0;
                            bookingAmt = Convert.ToDecimal(dtDetails.Rows[0]["TotalPrice"]);

                            if ((Convert.ToDecimal(txtAdminFee.Text) + Convert.ToDecimal(txtSupplierFee.Text)) <= Convert.ToDecimal(bookingAmt))
                            {

                                //Step-2: Update tbl_Package_Transaction_Header bookingStatus Column to 'X'
                                //X ---- Cancelled
                                CT.Core.Queue.UpdatePkgTranxBookingStatus(Convert.ToInt32(e.CommandArgument), string.Empty, "X");


                                //Step-3: Update Cancellation Charges.
                                CT.BookingEngine.CancellationCharges cancellationCharge = new CT.BookingEngine.CancellationCharges();
                                cancellationCharge.AdminFee = Convert.ToDecimal(txtAdminFee.Text);
                                cancellationCharge.SupplierFee = Convert.ToDecimal(txtSupplierFee.Text);
                                cancellationCharge.PaymentDetailId = 0;
                                cancellationCharge.ReferenceId = Convert.ToInt32(e.CommandArgument);
                                decimal exchangeRate = 0;
                                //decimal baseRateofExchange = 0;

                            decimal amount=0;
                            if (Convert.ToString(dtDetails.Rows[0]["Currency"]) != agent.AgentCurrency)
                            {
                                StaticData staticInfo = new StaticData();
                                staticInfo.BaseCurrency = agent.AgentCurrency;
                                Dictionary<string, decimal> rateOfExList = staticInfo.CurrencyROE;
                                if (rateOfExList.ContainsKey(Convert.ToString(dtDetails.Rows[0]["Currency"])))
                                {
                                    exchangeRate = rateOfExList[Convert.ToString(dtDetails.Rows[0]["Currency"])];
                                }
                                else {
                                    exchangeRate = 1;
                                }
                                amount = (Convert.ToDecimal(cancellationCharge.AdminFee + cancellationCharge.SupplierFee) * exchangeRate);
                                //cancellationCharge.AgentBaseAmount = (Convert.ToDecimal(cancellationCharge.AdminFee + cancellationCharge.SupplierFee) * exchangeRate);
                            }
                            else
                            {
                                amount = (Convert.ToDecimal(cancellationCharge.AdminFee + cancellationCharge.SupplierFee));
                                //cancellationCharge.AgentBaseAmount = Convert.ToDecimal(cancellationCharge.AdminFee + cancellationCharge.SupplierFee);
                            }

                            //StaticData baseStaticInfo = new StaticData();
                            //baseStaticInfo.BaseCurrency = Convert.ToString(dtDetails.Rows[0]["Currency"]);
                            //Dictionary<string, decimal> baseRateOfExList = baseStaticInfo.CurrencyROE;
                            //if (baseRateOfExList.ContainsKey(agent.AgentCurrency))
                            //{
                            //    baseRateofExchange = baseRateOfExList[agent.AgentCurrency];
                            //}
                            //else
                            //{
                            //    baseRateofExchange = 1;
                            //}
                            cancellationCharge.CancelPenalty = (amount);
                            cancellationCharge.CreatedBy = loggedMemberId;
                            cancellationCharge.ProductType = ProductType.Packages;
                            cancellationCharge.Save();

                            //Step-4: Admin & Supplier Fee save in Ledger
                            bookingAmt = Math.Ceiling(bookingAmt);
                            int invoiceNumber = 0;
                            decimal adminChar = Math.Ceiling(Convert.ToDecimal(txtSupplierFee.Text) + Convert.ToDecimal(txtAdminFee.Text));
                            decimal adminFee = 0;
                            if (adminChar < bookingAmt)
                            {
                                adminFee = adminChar;
                            }

                            LedgerTransaction ledgerTxn = new LedgerTransaction();
                            NarrationBuilder objNarration = new NarrationBuilder();
                            invoiceNumber = Invoice.isInvoiceGenerated(Convert.ToInt32(e.CommandArgument), ProductType.Packages);
                            ledgerTxn.LedgerId = Convert.ToInt32(dtDetails.Rows[0]["AgencyId"]);
                          //ledgerTxn.ProductId = (int)ProductType.Packages;
                            ledgerTxn.Amount = -adminFee;
                            objNarration.HotelConfirmationNo = Convert.ToString(dtDetails.Rows[0]["TripId"]);
                            objNarration.TravelDate = Convert.ToString(dtDetails.Rows[0]["Booking"]);
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.PackageCancellationCharges;
                            objNarration.Remarks = "Packages Cancellation Charges";
                            ledgerTxn.Narration = objNarration;
                            ledgerTxn.IsLCC = true;
                            ledgerTxn.ReferenceId = Convert.ToInt32(e.CommandArgument);
                            ledgerTxn.Notes = "";
                            ledgerTxn.Date = DateTime.UtcNow;
                            ledgerTxn.CreatedBy = loggedMemberId;
                            ledgerTxn.TransType = Convert.ToString(dtDetails.Rows[0]["TransactionType"]);
                            ledgerTxn.Save();

                            LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                            //save Refund amount
                            ledgerTxn = new LedgerTransaction();
                            ledgerTxn.LedgerId = Convert.ToInt32(dtDetails.Rows[0]["AgencyId"]);
                             //if (adminFee <= bookingAmt)
                                //{
                                //    bookingAmt -= adminFee;
                                //}
                            ledgerTxn.Amount = bookingAmt;
                            objNarration.PaxName = Convert.ToString(dtDetails.Rows[0]["PassengerName"]);
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.PackageRefund;
                            ledgerTxn.Notes = "Package Voucher Refunded";
                            objNarration.Remarks = "Refunded for Voucher No -" + dtDetails.Rows[0]["BookingRefNo"];
                           // ledgerTxn.ProductId = (int)ProductType.Packages;
                            ledgerTxn.Narration = objNarration;
                            ledgerTxn.IsLCC = true;
                            ledgerTxn.ReferenceId = Convert.ToInt32(e.CommandArgument);

                            ledgerTxn.Date = DateTime.UtcNow;
                            ledgerTxn.CreatedBy = loggedMemberId;
                            ledgerTxn.TransType = Convert.ToString(dtDetails.Rows[0]["TransactionType"]);
                            ledgerTxn.Save();
                            LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                            
                            if (Convert.ToString(dtDetails.Rows[0]["TransactionType"]) != "B2C")
                            {
                                agent.CreatedBy = Settings.LoginInfo.UserID;
                                agent.UpdateBalance(bookingAmt);
                                if (Convert.ToInt32(dtDetails.Rows[0]["AgencyId"]) == Settings.LoginInfo.AgentId)
                                {
                                    Settings.LoginInfo.AgentBalance += bookingAmt;
                                }
                            }
                            //Send Email -- Pending 
                            try

                            {
                                //Sending Email.
                                Hashtable table = new Hashtable();
                            table.Add("leadPaxName", !string.IsNullOrEmpty(leadPaxName.Text) ? leadPaxName.Text : "Customer");
                               // table.Add("agentName", Convert.ToString(dtDetails.Rows[0]["Supplier"]));
                               table.Add("pkgName", Convert.ToString(dtDetails.Rows[0]["packageName"]));
                                table.Add("confirmationNo", Convert.ToString(dtDetails.Rows[0]["TripId"]));

                                    System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                                    UserMaster bookedBy = new UserMaster(Convert.ToInt32(dtDetails.Rows[0]["CreatedBy"]));
                                    AgentMaster bookedAgency = new AgentMaster(Convert.ToInt32(dtDetails.Rows[0]["AgencyId"]));
                                    toArray.Add(bookedBy.Email);
                                    toArray.Add(bookedAgency.Email1);
                                    string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["CANCEL_REQUEST_MAIL"]).Split(';');
                                    foreach (string cnMail in cancelMails)
                                    {
                                        toArray.Add(cnMail);
                                    }
                                    if (!string.IsNullOrEmpty(leadPaxEmail.Text))
                                    {
                                        toArray.Add(leadPaxEmail.Text);
                                    }
                                    string message = ConfigurationManager.AppSettings["PKG_REFUND"];
                                    try
                                    {
                                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Package)Response for cancellation. Confirmation No:(" + Convert.ToString(dtDetails.Rows[0]["TripId"]) + ")", message, table, ConfigurationManager.AppSettings["BCCEmail"]);
                                    }
                                    catch { }
                                }
                                catch { }
                                //LoadQueue();
                                databind();
                            }
                            else
                            {
                                lblError.Text = "Sum of Admin & Supplier Fee should not be Greater than Total Price";
                            }
                        }
                    }
                    else
                    {
                        lblError.Text = "Please enter Admin & Supplier Fee";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from (PackageChangeRequestQueue Page):" + ex.ToString(), "");
        }
    }
    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = CT.TicketReceipt.BusinessLayer.AgentMaster.GetList(1, "ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            DataView view = dtAgents.DefaultView;
            view.Sort = "agent_Name ASC";
            dtAgents = view.ToTable();
            ddlAgents.DataSource = dtAgents;
            ddlAgents.DataTextField = "agent_Name";
            ddlAgents.DataValueField = "agent_Id";
            ddlAgents.AppendDataBoundItems = true;
            ddlAgents.DataBind();
            ddlAgents.Items.Insert(0, new ListItem("--All--", "0"));
            ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);

            if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
            {
                ddlAgents.Enabled = true;
            }
            else
            {
                ddlAgents.Enabled = false;
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private DataTable GetPackagesQueueData(DateTime fromDate, DateTime toDate, string agencyId)
    {
        DataTable dtItems = null;
        try
        {
            dtItems = CT.Core.Queue.GetChangeRequestPackageQueueRecords(fromDate, toDate, agencyId);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return dtItems;
    }

    protected void dlBookingQueue_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                {
                    DataRowView drv = (DataRowView)(e.Item.DataItem);
                    Label lblSupplierRef = e.Item.FindControl("lblSupplierRefTxt") as Label;
                    Button btnRefund =  e.Item.FindControl("btnRefund") as Button;
                    TextBox adminFee = e.Item.FindControl("txtAdminFee") as TextBox;
                    TextBox supplierFee = e.Item.FindControl("txtSupplierFee") as TextBox;
                    Label lblAdminFee = e.Item.FindControl("Label1") as Label;
                    Label lblSupplierFee = e.Item.FindControl("Label2") as Label;

                    Label lblPromoCode = e.Item.FindControl("lblPromoCode") as Label;
                    Label lblPromoCodeValue = e.Item.FindControl("lblPromoCodeValue") as Label;
                    Label lblTotalPrice = e.Item.FindControl("lblTotalPrice") as Label;

                    if (drv.Row["Promo_Code"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["Promo_Code"])))
                    {
                        lblPromoCode.Visible = true;
                        lblPromoCodeValue.Visible = true;
                    }
                    else
                    {
                        lblPromoCode.Visible = false;
                        lblPromoCodeValue.Visible = false;
                    }
                    if (drv.Row["Promo_Discount_Value"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["Promo_Discount_Value"])))
                    {
                        decimal promodiscountValue = Math.Ceiling(Convert.ToDecimal(drv.Row["Promo_Discount_Value"]));
                        decimal totalPriceWithoutDiscount = 0;//Convert.ToDecimal(drv.Row["Sale_Price"]);
                        string[] priceDet = drv.Row["Sale_Price"].ToString().Split(' ');
                        if (priceDet.Length > 1)
                        {
                            totalPriceWithoutDiscount = Convert.ToDecimal(priceDet[1]);
                            decimal totalPriceAfterPromo = (totalPriceWithoutDiscount - promodiscountValue);
                            lblTotalPrice.Text = Convert.ToString(priceDet[0] + ' ' + totalPriceAfterPromo);
                        }
                        else {
                            totalPriceWithoutDiscount = Convert.ToDecimal(priceDet[0]);
                            decimal totalPriceAfterPromo = (totalPriceWithoutDiscount - promodiscountValue);
                            lblTotalPrice.Text = Convert.ToString(totalPriceAfterPromo);
                        }
                        
                        
                    }
                    else
                    {
                        lblTotalPrice.Text = Convert.ToString(drv.Row["Total_Price"]);
                    }
                   
                    if (drv.Row["Supplier_Ref"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["Supplier_Ref"])))
                    {
                        lblSupplierRef.Visible = true;
                    }
                    else
                    {
                        lblSupplierRef.Visible = false;
                    }
                    if (drv["TranxBookingStatus"] != DBNull.Value && Convert.ToString(drv["TranxBookingStatus"]).ToLower() == "r")
                    {
                        btnRefund.Visible = true;
                        adminFee.Visible = true;
                        supplierFee.Visible = true;
                        lblAdminFee.Visible = true;
                        lblSupplierFee.Visible = true;
                    }
                    else
                    {
                        btnRefund.Visible = false;
                        adminFee.Visible = false;
                        supplierFee.Visible = false;
                        lblAdminFee.Visible = false;
                        lblSupplierFee.Visible = false;

                    }


                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from (PackageChangeRequestQueue Page):" + ex.ToString(), "");
        }
    }

    protected void btnfirst_Click(object sender, EventArgs e)
    {
        pos = 0;
        databind();
    }

    protected void btnprevious_Click(object sender, EventArgs e)
    {
        pos = (int)this.ViewState["vs"];
        pos -= 1;
        this.ViewState["vs"] = pos;
        databind();
    }

    protected void btnnext_Click(object sender, EventArgs e)
    {
        pos = (int)this.ViewState["vs"];
        pos += 1;
        this.ViewState["vs"] = pos;
        databind();
    }

    protected void btnlast_Click(object sender, EventArgs e)
    {
        pos = adsource.PageCount - 1;
        databind();
    }

    public void databind()
    {

       
        DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
        IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
        string agencyId = string.Empty;
      //  string pnrNo = string.Empty;

        if (ddlAgents.SelectedItem.Value != "0")
        {
            agencyId = Convert.ToString(ddlAgents.SelectedItem.Value);
        }
        //if (txtPNR.Text.Length > 0)
        //{
        //    pnrNo = txtPNR.Text;
        //}

        if (txtFromDate.Text != string.Empty)
        {
            startDate = Convert.ToDateTime(txtFromDate.Text, provider);
        }
        else
        {
            startDate = DateTime.Now;
        }
        if (txtToDate.Text != string.Empty)
        {
            endDate = Convert.ToDateTime(Convert.ToDateTime(txtToDate.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
        }
        else
        {
            endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
        }

        DataTable dt = GetPackagesQueueData(startDate, endDate, agencyId);
        DataView dv = dt.DefaultView;
        dv.Sort = "TranxHeaderId desc";
        DataTable sortedDT = dv.ToTable();

        adsource = new PagedDataSource();
        DataSet dsPkgQueues = new DataSet("PackagesQueues");
        dsPkgQueues.Tables.Add(sortedDT);
        if (dt.Rows.Count > 10)
        {
            Paging.Visible = true;
        }
        else
        {
            Paging.Visible = false;
        }
        adsource.DataSource = dsPkgQueues.Tables[0].DefaultView;
        adsource.PageSize = 10;
        adsource.AllowPaging = true;
        adsource.CurrentPageIndex = pos;
        btnfirst.Enabled = !adsource.IsFirstPage;
        btnprevious.Enabled = !adsource.IsFirstPage;
        btnlast.Enabled = !adsource.IsLastPage;
        btnnext.Enabled = !adsource.IsLastPage;
        dlBookingQueue.DataSource = adsource;
        dlBookingQueue.DataBind();
        hdfParam.Value = "1";
    }

}
