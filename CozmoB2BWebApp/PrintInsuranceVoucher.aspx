﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PrintInsuranceVoucher" Codebehind="PrintInsuranceVoucher.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head runat="server">
<link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />
    
    <link href="css/bootstrap.min.css" rel="stylesheet" />

  <!-- manual css -->
    <link href="css/override.css" rel="stylesheet" />


    
</head>
<body>
    <form id="form1" runat="server">
    <script>
        function printPage() {
            document.getElementById('btnPrint').style.display = "none";
            document.getElementById('btnEmail').style.display = "none";

            for (var i = 0; i < document.getElementById('hdnPaxCount').value; i++) {
                if (document.getElementById('tdPolicy'+i) != null) {
                    document.getElementById('tdPolicy'+i).style.display = "none";
                }
                for (var k = 0; k < document.getElementById('hdnPlansCount').value; k++) {
                    if (document.getElementById('tdViewLink' + i + k) != null) {
                        document.getElementById('tdViewLink' + i + k).style.display = "none";
                    }
                }
            }
            
            window.print();
            setTimeout('showButtons()', 1000);
            return false;
        }
        function showButtons() {
            document.getElementById('btnPrint').style.display = "block";
            document.getElementById('btnEmail').style.display = "block";

            for (var i = 0; i < document.getElementById('hdnPaxCount').value; i++) {
                if (document.getElementById('tdPolicy'+i) != null) {
                    document.getElementById('tdPolicy'+i).style.display = "block";
                }
                for (var k = 0; k < document.getElementById('hdnPlansCount').value; k++) {
                    if (document.getElementById('tdViewLink' + i + k) != null) {
                        document.getElementById('tdViewLink' + i + k).style.display = "block";
                    }
                }
            }
        }
        function Validate() {
            var isValid = true;
            var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
            document.getElementById('err').style.display = 'none';
            if (document.getElementById('<%=txtEmailId.ClientID %>').value.length <= 0) {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Email address";
                isValid = false;
            }
            else if (reg.test(document.getElementById('<%=txtEmailId.ClientID %>').value)) {
                document.getElementById('err').style.display = 'none';
            }
            else {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Correct Email";
                isValid = false;
            }
            if (isValid == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function ShowPopUp(id) {
            document.getElementById('txtEmailId').value = "";
            document.getElementById('err').style.display = 'none';
            document.getElementById('emailBlock').style.display = "block";
            return false;
        }
        function HidePopUp() {
            document.getElementById('emailBlock').style.display = "none";
        }
        
    </script>
    <div class="col-md-12">
   <div id="emailBlock" class="showmsg" style="position:absolute;display:none; left:400px;top:200px;width:300px;">
          <div class="showMsgHeading"> Enter Your Email Address</div>
          
          <a style=" position:absolute; right:5px; top:3px;" onclick="return HidePopUp();" href="#" class="closex"> X</a>
           <div class="padding-5"> 
           <div style=" background:#fff">
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       
                           <tr>
                           <td height="40" align="center">
                            <b style="display: none; color:Red" id="err"></b>
                           <asp:TextBox style=" border: solid 1px #ccc; width:90%; padding:2px;"  ID="txtEmailId" runat="server" CausesValidation="True" ></asp:TextBox>
                           </td>
                           </tr>
                           <tr>
                           
                           <td height="40" align="center">
                           <asp:Button CssClass="button-normal" ID="btnEmailVoucher" runat="server" Text="Send Email" OnClientClick="return Validate()" OnClick="btnEmailVoucher_Click" />
                           </td>
                       </tr>
                   </table>
            </div>
           
           
           
             </div>
           
           
               
           </div>
  
    <div id="printableArea" runat="server">
    <div class="font_med marbot_20">  <center>  <strong> Insurance Voucher</strong></center></div>
        <div id="insurance_container">
          <div class="col-md-4"> <asp:Image ID="imgHeaderLogo" runat="server" Width="162"  Height="51px" AlternateText="AgentLogo" ImageUrl="" /></div>
               <label class="pull-right mar10">  <asp:Button ID="btnPrint" runat="server" Text="Print Voucher" OnClientClick="return printPage();" /></label>
    
      <label class="pull-right mar10">  <asp:Button ID="btnEmail" runat="server" Text="Email Voucher" OnClientClick="return ShowPopUp(this.id);"/></label>
            <div class="marbot_20">
                <div class="col-md-6 col-xs-12">
                    Your Journey :
                    <asp:Label ID="lblSector" runat="server" Text="" Font-Bold="true"></asp:Label></div>
                <div class="col-md-6 col-xs-12">
                    Departure Date :
                    <asp:Label ID="lblJourneyDates" runat="server" Text="" Font-Bold="true"></asp:Label></div>
                <div class="clearfix">
                </div>
            </div>
                    <% if (header != null && header.InsPassenger != null && header.InsPassenger.Count > 0)
                     {
                         for (int i = 0; i < header.InsPassenger.Count; i++)
                         {%>
                          <input id="hdnPaxCount" type="hidden" value='<%=header.InsPassenger.Count %>' />
             
              <div class="panel-group" id="accordion">
  <div class="panel panel-default">
   
   
     
          <%=header.InsPassenger[i].FirstName + " " + header.InsPassenger[i].LastName%>
         
   
   
    <div >
      
      
       <table width="100%" border="1" cellpadding="0" cellspacing="0">
       <tr class="font_bold">
       <td>Plan Title</td>
       
       <td>Plan Code</td>
       
       <td>Policy No</td>
       
        <td id="tdPolicy<%=i %>" style="display:block;">Policy Link</td>
        
       </tr>
       
       
       <%if (header.InsPlans != null && header.InsPlans.Count > 0)
         {%>
          <input id="hdnPlansCount" type="hidden" value='<%=header.InsPlans.Count %>' />
             
              <%for (int k = 0; k < header.InsPlans.Count; k++)
             {%>
               
       <tr>
      
       <td><%=header.InsPlans[k].PlanTitle %></td>
       
       <td><%=header.InsPlans[k].InsPlanCode %></td>
       
       <% if (header.InsPassenger[i].PolicyNo.Split('|').Length >= k)
          { %>
       <td><%=header.InsPassenger[i].PolicyNo.Split('|')[k]%></td>
       
       
       <%} %>
       
        <% if (header.InsPassenger[i].PolicyUrlLink.Split('|').Length > k)
          { %>
            
       <td id="tdViewLink<%=i %><%=k %>" style="display:block;"><a target='_blank' href='<%=header.InsPassenger[i].PolicyUrlLink.Split('|')[k]%>'>View Certificate</a></td>
        
       
       <%} %>
       </tr>
      
         
       <%}
         } %>
          </table>
    </div>
  </div>


   </div>
            <%}
                     }%>
          <%--  <div class="col-md-12 padding-0 marbot_10">
                <div id="divPlan" runat="server">
                </div>
            </div>--%>
            <div class="col-md-12 marbot_10">
                <label style="color: Red">
                    Note:</label>This voucher is only for cozmo internal reference
            </div>
        </div>
    </div>
    
    
    </div>
    </form>
</body>
</html>
