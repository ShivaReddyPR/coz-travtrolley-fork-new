﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="HotelBookingQueue" Title="Hotel Booking Queue" Codebehind="HotelBookingQueue.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.BookingEngine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <style>
        .Ruleshdr {
            float: left;
            font-style: normal;
            background: #dadada;
            color: black;
            padding: 1px 2px 0 2px;
            margin-top: -1px;
            margin-left: 0;
            margin-right: 5px;
            border-radius: 3px;
        }

        .Rulesdtl {
            border: 1px solid #18407B;
            margin-right: 10px;
            padding: 10px;
        }
    </style>
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script type="text/javascript" src="js/Search.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    
     <link href="css/style.css" rel="stylesheet" type="text/css" /> <!--Added by chandan on  13062016 -->

  <%--  <script src="Scripts/Queue.js" type="text/javascript"></script>--%>


    <script type="text/javascript">

        //function ViewDocs(id) {
        //    var wnd = window.open('HotelInvoiceDocuments.aspx?id=' + id, '_blank', 'width=800,height=500,scroll=yes,resizable=no,menubar=no,status=no');
        //}

        function ViewCancellationPolicy(CPolicy, HotelId, HPolicy,Disclaimer) {           
            document.getElementById('HotelCancelPolicy').innerHTML = CPolicy;
            if (HPolicy != "") {
                document.getElementById('HotelPolicyDetails').innerHTML = HPolicy;                
                $("#CancelPolicy").modal("show");
                $("#ImpInfo").show();
                 $("#DisclaimerCond").show();
                document.getElementById('HotelPolicyDetails').style.display = "block";
            }           
            else {
                $("#CancelPolicy").modal("show");
                $("#ImpInfo").hide();
                $("#DisclaimerCond").show();
                document.getElementById('HotelPolicyDetails').style.display = "none";
            }
             if (Disclaimer != "") {
                document.getElementById('HotelDisclaimer').innerHTML = Disclaimer;
                $("#CancelPolicy").modal("show");               
                document.getElementById('HotelDisclaimer').style.display = "block";
            }
            else {
                document.getElementById('HotelDisclaimer').style.display = "none";
            }
        }

         function ViewCancellationPolicyOther(CPolicy, HotelId, HPolicy) {           
            document.getElementById('HotelCancelPolicy').innerHTML = CPolicy;
            if (HPolicy != "") {
                document.getElementById('HotelPolicyDetails').innerHTML = HPolicy;                
                $("#CancelPolicy").modal("show");
                $("#DisclaimerCond").hide();
                $("#ImpInfo").show();
                document.getElementById('HotelPolicyDetails').style.display = "block";
            }           
            else {
                $("#CancelPolicy").modal("show");
                $("#ImpInfo").hide();
                 $("#DisclaimerCond").hide();
                document.getElementById('HotelPolicyDetails').style.display = "none";
            }           
        }


        var Ajax;

        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }

        function ViewPaymentInfo(id, bookingId) {
            pblockId = id;
            var url = "PaymentInfoAjax";
            var paramList = 'isPaymentInfo=true';
            paramList += '&bookingId=' + bookingId;
            paramList += '&blockId=' + pblockId;
            document.getElementById('PaymentInfo-' + pblockId).style.display = "block";
            document.getElementById('PaymentInfo-' + pblockId).innerHTML = "Loading...";

            Ajax.onreadystatechange = ShowPaymentInfoPopUp;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }

        function ShowPaymentInfoPopUp() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        var numberOfRecord = eval('<%=recordsPerPage%>');
                        for (var i = 0; i < numberOfRecord; i++) {
                            if (document.getElementById('PaymentInfo-' + i) != null)
                                document.getElementById('PaymentInfo-' + i).style.display = "none";

                        }
                        document.getElementById('PaymentInfo-' + pblockId).style.display = "block";
                        document.getElementById('PaymentInfo-' + pblockId).innerHTML = Ajax.responseText;

                    }
                }
            }
        }



        function ShowPage(pageNo) {
            document.getElementById('<%=PageNoString.ClientID %>').value = pageNo;
            document.getElementById('<%=btnSubmit.ClientID %>').click();
        }
        function show(id) {
            document.getElementById(id).style.visibility = "visible";
        }
        function hide(id) {
            document.getElementById(id).style.visibility = "hidden";
        }


        function showStuff(id) {
            document.getElementById('DisplayAgent' + id).style.display = 'block';
        }


        function hidestuff(boxid) {
            document.getElementById(boxid).style.display = "none";
        }


//        function CancelAmendBooking(cnf) {

//            //document.getElementById('<%=hdnBooking.ClientID %>').value = document.getElementById('booking' + cnf).value + '|' + document.getElementById('txtRemarks').value;
        //            hidestuff('DisplayGrid2');
        //        }

 
        function CancelAmendBooking(cnf,confNo) {
            document.getElementById('<%=ConfirmationNum.ClientID%>').value = confNo;
            var val = document.getElementById('ctl00_cphTransaction_dlBookings_ctl0' + cnf + '_txtRemarks').value;
            if (document.getElementById('ctl00_cphTransaction_dlBookings_ctl0' + cnf + '_ddlBooking').value == "Select") {
                document.getElementById('ctl00_cphTransaction_dlBookings_ctl0' + cnf + '_errRemarks').innerHTML = "Please Select Request";
                return false;
            }

            else if (val.length <= 0 || val == "Enter Remarks here") {
                //document.getElementById('errMess1').style.display = "block";
                document.getElementById('ctl00_cphTransaction_dlBookings_ctl0' + cnf + '_errRemarks').innerHTML = "Enter remarks";
                return false;
            }
            //else if (document.getElementById('ctl00_cphTransaction_dlBookings_ctl0' + cnf + '_ddlBooking').value == "Cancel Booking") {                
            //    if (confirm("Are you sure want to cancel your Booking?")) { 
            //        $("#SubmitRequest").modal("show");   
            //        return false;                   
            //    }                           
            //}
            else {
                //hidestuff('DisplayGrid2');
                // document.getElementById('errMess1').style.display = "none";
                document.getElementById('ctl00_cphTransaction_dlBookings_ctl0' + cnf + '_errRemarks').innerHTML = "";
                return true;
            }
             $("#SubmitRequest").modal("hide");  
        }
        //To show the failure Remarks PopUp
        function ViewPendingRemarks(pendingRemarks) {
            if (pendingRemarks.length > 0) {
                document.getElementById('FailureRemarks').style.display = "block";
                $('#FailureRemarks').html(pendingRemarks);
                $("#FailureRemarksModal").modal("show");
            }
            else {
                 $("#FailureRemarksModal").modal("hide");
            }
        }
        //--------------------------Calender control start-------------------------------
        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) /" + today.getDate() /" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }

        function showCal1() {
            init();
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            cal1.show();
            cal2.hide();
            document.getElementById('container1').style.display = "block";

        }


        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= CheckIn.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            //    if (difference < 0) {
            //        document.getElementById('errMess').style.display = "block";
            //        document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
            //        return false;
            //    }
            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= CheckIn.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=CheckIn.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            //    if (difference < 1) {
            //        document.getElementById('errMess').style.display = "block";
            //        document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 )";
            //        return false;
            //    }
            //            if (difference == 0) {
            //                document.getElementById('errMess').style.display = "block";
            //                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
            //                return false;
            //            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=CheckOut.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);

        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
                  
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
                  
            }
        }
       
        function ViewInvoice(hotelId, agentId, confNo) {
            //window.open("CreateHotelInvoice.aspx?hotelId=" + hotelId + "&agencyId=" + agentId + "&ConfNo=" + confNo + "", '', 'width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
            AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'CreateHotelInvoice', 'sessionData':'" + (hotelId + '|' + agentId+ '|' + confNo) + "', 'action':'set'}");
            window.open("CreateHotelInvoice.aspx", "HotelInvoice", "width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
            return false;
        }

        function ViewVoucher(confNo,approvalStatus,hotelid) {
            if (approvalStatus!=null &&  approvalStatus!='') {
                //window.location.href = "CorpHotelSummary.aspx?ConfNo=" + confNo;
                //window.open("CorpHotelSummary.aspx?ConfNo=" + confNo +"&HotelId=" + hotelid +"", '', 'width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
                AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'CorpHotelSummary', 'sessionData':'" + (confNo + '|' + hotelid) + "', 'action':'set'}");
                window.open("CorpHotelSummary.aspx", "Summary", "width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
            } else {
                //window.open("printHotelVoucher.aspx?ConfNo=" + confNo + "&HotelId=" + hotelid + "", '', 'width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
                AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'PrintHotelVoucher', 'sessionData':'" + (confNo + '|' + hotelid) + "', 'action':'set'}");
                window.open("printHotelVoucher.aspx", "Voucher", "width=900,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
                }
                return false;
        }
        //added by somasekhar on 10-10-2019
        function ValidateCorpStatus(index) {
            debugger;
            var valid = true;
            var prefix = 'ctl00_cphTransaction_dlBookings_ctl0';
            var ddl = '_ddlApprovalStatus';
            var lbl = '_lblCorpError';
            var txt = '_txtCorpRemarks';
            if ($('#' + prefix + index + ddl).val() == '-1') {
                $('#' + prefix + index + lbl).html('Please select any status to Update!');
                valid = false;
            }
            else if ($('#' + prefix + index + ddl).val() == 'R' && $('#' + prefix + index + txt).val().length == 0) {
                $('#' + prefix + index + lbl).html('Please enter rejection remarks!');
                valid = false;
            }

            return valid;
        }
        //--------------------------------
        function ShowRemarks(id, index) {

            var ddl = id;
            var txtid = 'ctl00_cphTransaction_dlBookings_ctl0' + index + '_txtCorpRemarks';
            if ($('#' + ddl).val() == 'R') {
                $('#' + txtid).show();
            }
            else {
                $('#' + txtid).hide();
            }
        }
    </script>

    <Script>                    
        function Validate(index) {
            var adminFee = document.getElementById('ctl00_cphTransaction_dlChangeRequests_ctl0' + index + '_txtAdminFee').value;
            var supplierFee = document.getElementById('ctl00_cphTransaction_dlChangeRequests_ctl0' + index + '_txtSupplierFee').value;
            var total = document.getElementById('ctl00_cphTransaction_dlChangeRequests_ctl0' + index + '_lblPrice').innerText;
            var totalAmount = total.split(" ");
            if (parseFloat(adminFee)+parseFloat(supplierFee) <0 || (parseFloat(adminFee)+parseFloat(supplierFee)) > parseInt(totalAmount[1])) {
                alert("Admin Fee and Supplier Fee should not be Greater Then Booking Amount");
                return false;
            }
         }
    </script>

    <Script> 
      function RefNum() { 
         var Validation = $("#<%= txtRefNum.ClientID %>").val();
            if (Validation.length <= 0 || Validation == "Enter Ref Number") {
                $("#<%= txtRefNum.ClientID %>").css('border-color', 'red');
                    return false;
          }
          else {
                $("#<%= txtRefNum.ClientID %>").css('border-color', '');               
              }
            }         
    </script>

      <Script> 
            function ConfirmNo() {
                $("#SubmitRequest").modal("hide");              
            }
            //function ViewBookingForHotel(HotelId) {
            //    var url = "ViewBookingForHotel.aspx?HotelId=" + HotelId + "&fromAgent=true";
            //    window.location = url;
            //}
  function ViewBookingForHotel(HotelId,approvalStatus) {
                //var url = "ViewBookingForHotel?HotelId=" + HotelId + "&pStatus=" + approvalStatus + "&fromAgent=true";
      AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'ViewBookingForHotel', 'sessionData':'" + JSON.stringify({ HotelId: HotelId, pStatus: approvalStatus, fromAgent: true }) + "', 'action':'set'}");
                var url = "ViewBookingForHotel";
                window.location = url;
            }
    </script>

    <asp:HiddenField ID="hdnBooking" runat="server"/>
    <asp:HiddenField ID="PageNoString" runat="server" Value="1"/>
     <asp:HiddenField ID="ConfirmationNum" runat="server" Value="1"/>
    <asp:HiddenField runat="server" ID="hdfIsCorporate" Value=""></asp:HiddenField>
    
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    
    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0"></iframe>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; z-index:9999; top: 120px; left:8%; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute;  z-index:9999; top: 120px; left:40%; display: none;">
        </div>
    </div>
    <div>
 <asp:HiddenField runat="server" id="hdfParam" value="1"></asp:HiddenField>
      <table cellpadding="0" cellspacing="0" class="label">
          <tr>
              <td style="width: 700px" align="left">
                  <a style="cursor:pointer; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
                      onclick="return ShowHide('divParam');">Hide Parameter</a>
              </td>
          </tr>
      </table>
        <%--<div>                                                 
            <label id="SysRefNo" style="cursor:pointer;"><a>Ref Num</a></label>                                                                                        
       </div>--%>
      <div title="Param" id="divParam">
       
        <asp:Panel runat="server" ID="pnlParam" Visible="true">
     
    <div class="paramcon"> 
       
  <div class="margin-top-5"> 


<div class="col-md-2"> From Date:  </div> 
<div class="col-md-2"> 

<table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="CheckIn" runat="server" CssClass="form-control" Width="120px" ></asp:TextBox>
                                            </td>
                                            <td>
                                                <a href="javascript:void(null)" onclick="showCal1()">
                                                    <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date"/>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>

   </div> 

<div class="col-md-2">  To Date: </div> 
<div class="col-md-2"> 

<table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="CheckOut" CssClass="form-control" runat="server"  Width="120px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <a href="javascript:void(null)" onclick="showCal2()">
                                                    <img id="Img1" src="images/call-cozmo.png" alt="Pick Date"/>
                                                </a>
                                            </td>
                                            <td><asp:CheckBox ID="Checkdates" runat="server" Text=""  />
  <label style="font-size:xx-small; color:Green" for="check">CheckIn Dates</label></td>
                                        </tr>
                                    </table>

 </div> 

<div class="col-md-2"> Booking Status:</div> 
<div class="col-md-2"> 
   <asp:DropDownList ID="ddlBookingStatus" CssClass="form-control" runat="server" AppendDataBoundItems="true">
                                        <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                    </asp:DropDownList>
 </div> 


<div class="clearfix"> </div>

 </div>
 
  <div class="margin-top-5"> 


<div class="col-md-2"> Confirmation No. </div> 
<div class="col-md-2"> <asp:TextBox ID="txtConfirmNo"  runat="server" CssClass="inputEnabled form-control"></asp:TextBox> </div> 


<div class="col-md-2">  Agent: </div> 
<div class="col-md-2"> <asp:DropDownList ID="ddlAgents" CssClass="form-control" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlAgents_SelectedIndexChanged">
                                        <%--<asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>--%>
                                    </asp:DropDownList> </div> 

<div class="col-md-2"> <asp:Label ID="lblB2BAgent" Text="B2BAgent:" runat="server"></asp:Label> </div> 
<div class="col-md-2"> <asp:DropDownList ID="ddlB2BAgent"  runat="server" CssClass="inputDdlEnabled form-control"
                                        OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList> </div> 
<div class="clearfix"> </div>
 </div>  
 
 
 <div class="margin-top-5"> 



<div class="col-md-2"> <asp:Label ID="lblB2B2BAgent" Text="B2B2BAgent:" runat="server"></asp:Label>  </div> 
<div class="col-md-2"> <asp:DropDownList ID="ddlB2B2BAgent"  runat="server" CssClass="inputDdlEnabled form-control" OnSelectedIndexChanged="ddlB2B2BAgent_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>   </div> 

<div class="col-md-2">  Location: </div> 
<div class="col-md-2"> <asp:DropDownList ID="ddlLocations" CssClass="form-control" runat="server" AppendDataBoundItems="true" Enabled="false" >
                                        <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                    </asp:DropDownList> </div> 

<div class="col-md-2"> Hotel: </div> 
<div class="col-md-2"> <asp:TextBox ID="txtHotelName" runat="server" CssClass="inputEnabled form-control"></asp:TextBox></div> 
<div class="clearfix"> </div>
 </div>
 
 
 <div class="margin-top-5"> 


<div class="col-md-2">  Pax Name: </div> 
<div class="col-md-2"> <asp:TextBox ID="txtPaxName" runat="server" CssClass="inputEnabled form-control"></asp:TextBox>   </div> 
<div class="col-md-2"> <asp:Label ID="lblSource" runat="server" Text="Source:"></asp:Label>  </div> 
<div class="col-md-2"> <asp:DropDownList ID="ddlSource" CssClass="form-control" runat="server" AppendDataBoundItems="true">
                                        <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                    </asp:DropDownList>   </div> 
<div class="col-md-2"> <asp:Label ID="lblTransType" runat="server" Text="TransType:" Visible="false"></asp:Label> </div> 
<div class="col-md-2"> <asp:DropDownList ID="ddlTransType" CssClass="form-control" runat="server" Visible="false">
                                    <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                                    <asp:ListItem Value="B2B" Text="B2B"></asp:ListItem>
                                    <asp:ListItem Value="B2C" Text="B2C"></asp:ListItem>
                                </asp:DropDownList> </div> 
<div class="clearfix"> </div>
     
 </div>

        <div class="margin-top-5">
 <div class="col-md-12"><asp:Button CssClass="btn btn-primary pull-right" ID="btnSubmit" runat="server" Text="Search"
                                            OnClick="btnSubmit_Click"/> </div>     
     <div class="clearfix"> </div> 
 </div>
   <h1 class="m-0 p-0 text-center"><span class="badge badge-secondary">OR</span></h1>
 <div class="margin-top-5" id="ReferenceNum"> 
 <div class="col-md-2"> Ref. Number  
 <asp:TextBox ID="txtRefNum" runat="server" CssClass="form-control" placeholder="Enter Ref. Number"></asp:TextBox> </div>
    <%-- <div class="col-md-2" style="margin-top:3px;"> <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" placeholder="Enter Ref Number"></asp:TextBox></div>--%>
 <div class="col-md-2" style="margin-top:29px;">
     <asp:Button class="btn btn-primary" ID="btnRefSubmit" runat="server" Text="Search"
                                            OnClick="btnRefSubmit_Click" OnClientClick="return RefNum()"/>
 </div>     
     <div class="clearfix"> </div> 
 </div>
      
      <div class="clearfix"> </div>        
    </div>         
        </asp:Panel>
        </div>
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0">     
             <tr>
        <td>
        <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
        </td></tr>
            <tr>
                <td align="right">
                    <%=show %>
                </td>
            </tr>
            <tr>
                <td valign="top">
           <div> 
             <asp:Label ID="lblRefStatus" runat="server" Text="" style="color:Red;background:#fff; border: solid 1px #ccc; padding: 10px 0px 10px 0px; margin-bottom:10px;text-align:center;font-weight:600;"></asp:Label>
            </div>
                    <div style=" border: solid 0px red">                       
                        <asp:DataList ID="dlBookings" runat="server" Width="100%" OnItemDataBound="dlBookings_ItemDataBound"
                            OnItemCommand="dlBookings_ItemCommand">
                            <ItemStyle VerticalAlign="Top" />
                            <ItemTemplate>
                                <div style="display: none" id="DisplayAgent<%#Container.ItemIndex %>" class="div-TravelAgent">
                                    <span style="position: absolute; right: 0px; top: 0px; cursor: pointer"><a href="#"
                                        onclick="hidestuff('DisplayAgent<%#Container.ItemIndex %>');">
                                        <img src="images/close1.png" /></a></span>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="25">
                                                <b>
                                                    <asp:Label ID="lblAgencyName" runat="server" Text=""></asp:Label></b>
                                            </td>
                                            <td>
                                                Credit Balance: <strong>
                                                    <asp:Label ID="lblAgencyBalance" runat="server" Text=""></asp:Label></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                Mob : <strong>
                                                    <asp:Label ID="lblAgencyPhone1" runat="server" Text=""></asp:Label></strong>
                                            </td>
                                            <td>
                                                Phone : <strong>
                                                    <asp:Label ID="lblAgencyPhone2" runat="server" Text=""></asp:Label></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                Email : <strong>
                                                    <asp:Label ID="lblAgencyEmail" runat="server" Text=""></asp:Label></strong>
                                            </td>
                                            <td>
                                                <a href="#">View Full Profile</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                
                         
                     <div style=" background:#fff; border: solid 1px #ccc; padding: 10px 0px 10px 0px; margin-bottom:10px;"> 
                                
                                
                   <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4">  Agent : <a href="#" onclick="showStuff(<%#Container.ItemIndex %>);"><strong>
                                                        <asp:Label ID="lblAgentName" runat="server" Text=""></asp:Label></strong></a></div>
                                <div class="col-md-4"> Status : <b style="color: #009933">
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                                </b></div>
                              <%--   <div class="col-md-4">Confirmation No :<b>
                                                    <%#Eval("ConfirmationNo") %></b> </div>--%>
                                 
                          <div class="col-md-4">  Confirmation No : <b><%#Eval("ConfirmationNo") %></b></div>
                                <div class="clearfix"> </div>
                                </div>
                                
                                     <%--<asp:HiddenField ID="ConfirmationNo-<%#Container.ItemIndex %>" runat="server"/>  --%>        
                   <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4">
                                     <% if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                                         { %>
                                Supplier :<b style="color: #999999">
                                                    <asp:Label ID="lblSupplier" runat="server"></asp:Label></b>
                                                    
                                            <%}
                                                else
                                                { %>        
                                                    
                                                    <%} %>
                                                     </div>
                                                     
                                <div class="col-md-4" style="display:none"> Last Cancellation Date : <b>
                                                    <%# Convert.ToDateTime(Eval("LastCancellationDate")).ToString("dd-MMM-yyyy HH:mm:ss")%></b> </div>
                                 <div class="col-md-4"> Supplier Ref :<b>
                                                    <%#Eval("BookingRefNo") %>
                                                </b> </div>
                           <%--<div class="col-md-4"> City Name : <b>
                                                    <%# Eval("cityref") %></b> </div>--%>
                                 
                         
                                <div class="clearfix"> </div>
                                </div>
                               
                                
                  <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4">Hotel :<b>
                                                    <%#Eval("HotelName") %></b> </div>
                                <div class="col-md-4"> No. Of Rooms : <b>
                                                    <%#Eval("NoOfRooms") %></b></div>
                                 <div class="col-md-4"> Total Price : <b>
                                                    <asp:Label ID="lblPrice" runat="server" Text='<%#Eval("TotalPrice") %>'></asp:Label></b></div>
                                  <div class="col-md-4" style="display:none"> <asp:Label ID="lblBookingPrice" runat="server" Text=""></asp:Label></div>
                         
                                <div class="clearfix"> </div>
                                </div>
                                
                                
                    <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4"> Booked On :<b>
                                                    <%# Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MMM-yyyy HH:mm:ss")%></b> </div>
                                <div class="col-md-4"> No. Of Guests : <b>
                                                    <asp:Label ID="lblGuests" runat="server" Text='<%#Eval("Guests") %>'></asp:Label></b></div>
                                 <div class="col-md-4"> Pax Name : <b>
                                                    <asp:Label ID="lblPaxName" runat="server" Text='<%#Eval("PaxName") %>'></asp:Label></b></div>
                                 
                         
                                <div class="clearfix"> </div>
                                </div>
                                
                     <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4">Check In :<b>
                                                    <%#Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy HH:mm:ss")%></b> </div>
                                <div class="col-md-4"> Check Out : <b>
                                                    <%#Convert.ToDateTime(Eval("EndDate")).ToString("dd-MMM-yyyy HH:mm:ss")%></b></div>
                                 <div class="col-md-4"> Booked By : <asp:Label ID="lblBookedBy" runat="server" Text=""></asp:Label></div>
                                 
                         
                                <div class="clearfix"> </div>
                                </div>
                                         
                       <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4">  Location : <asp:Label ID="lblLocation" runat="server" Text=""></asp:Label></div>
                           
                                <div class="col-md-3"> 
                                    Agent Ref : <asp:Label ID="lblAgentRef" runat="server" Text='<%#Eval("Agent_Ref") %>'></asp:Label>                                                     
                                                        </div>
                            <div class="col-md-1"> <label class=" pull-left"><asp:LinkButton ID="LnkOpen" class="btn btn-primary" runat="server" Text="Open" href="#"></asp:LinkButton></label></div>
                                 <div class="col-md-4">
                                 
                             
                                  <%--<label class=" pull-left"><asp:Button Class="btn btn-primary margin-right-5" ID="btnOpenHist" CommandName="BookHistory" CommandArgument='<%#Eval("ConfirmationNo") %>'
                                                        runat="server" Text="Open" Visible="true" /> </label>--%>

                                 <label class=" pull-left"><asp:Button Class="btn btn-primary margin-right-5" ID="btnInvoice" CommandName="Invoice" CommandArgument='<%#Eval("ConfirmationNo") %>'
                                                        runat="server" Text="View Invoice" Visible="true" /> </label>
                                 
                                  <label class=" pull-left"><asp:Button ID="btnOpen" Class="btn btn-primary" CommandName="Voucher" CommandArgument='<%#Eval("ConfirmationNo") %>'
                                                                runat="server" Text="View Voucher" Visible="true" /> </label>
                                      
                                 <%if (agent != null && agent.RequiredItinerary)
                             { %>                                                        
                             <%--<label class=" pull-left"><a class="btn btn-primary" href="#" onclick='ViewDocs(<%#Eval("HotelId") %>)'>View Documents</a></label>--%>                                                                            
                              <%} %> 
                                 <div class="clearfix"></div>
                                 
                                  </div>
                                 
                         
                                <div class="clearfix"> </div>
                                </div>
<%--                         <%if (agent != null && agent.RequiredItinerary)
                             { %>
                         <div class="col-md-12">
                             <a href="#" onclick='ViewDocs(<%#Eval("HotelId") %>)'>View Documents</a>                             
                         </div>
                         
                              <%} %>  --%>
       <div class="modal fade in farerule-modal-style" data-backdrop="static" id="CancelPolicy" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel" style="display:none;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" id="btnModelC">&times;</button>
                        <h4 class="modal-title">Cancellation Policy Details</h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="Rulesdtl" id="HotelCancelPolicy" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;"></div>
                        <p class="Ruleshdr" id="ImpInfo">Important Information</p>
                        <div class="Rulesdtl" id="HotelPolicyDetails" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;"></div>                       
                       <div id="DisclaimerCond">
                        <p class="Ruleshdr">Disclaimer</p>
                        <div class="Rulesdtl" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;">
                            <div id="HotelDisclaimer"></div><br />
                            <label class="bold"> Bed type availabilities depends @ Check-In time </label>
                        </div>
                       </div>
                    </div>             
                </div>
            </div>
        </div>
                        <div class="col-md-12 margin-top-10"> 
                            <div class="col-md-2" style="margin-top:22px;"><asp:LinkButton ID="LnkCancelPolicy" class="btn btn-primary" runat="server" Text="Cancellation Policy" href="#"></asp:LinkButton></div>
                               <asp:Label ID="lblBooking" BackColor="#9AF1A2" ForeColor="Black" runat="server" Font-Italic="True"
                                                    Font-Names="Arial" Font-Size="10pt"></asp:Label>
                                        <table id="tblBooking" width="100%" runat="server">
                                                    <tr>
                                                       <td>
                                                   <div> 
  
             <%-- <div class="col-md-2" style="margin-top:22px;"><asp:LinkButton ID="LnkCancelPolicy" class="btn btn-primary" runat="server" Text="Cancellation Policy" href="#"></asp:LinkButton></div>--%>
      

      <div class="modal fade in farerule-modal-style" data-backdrop="static" id="SubmitRequest" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel" style="display:none;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Cancellation Request Details</h4>
                    </div>
                    <div class="modal-body">     
                           <% if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                                { %>                                 
                                    <div class="col-md-2" style="margin-top:7px;">                                                                                      
                                            <asp:Label ID="Label1" runat="server" Text="Admin Fee"></asp:Label>                                                                             
                                            <asp:Label ID="lblAdminCurrency" runat="server" Text=""></asp:Label>                                       
                                            <asp:TextBox ID="txtAdminFee" runat="server" Text="0" onkeypress="return restrictNumeric(this.id,'0');" CssClass="form-control"></asp:TextBox>                                                                                                                                         
                                 </div>                                                                  
                                <div class="col-md-2" style="margin-top:7px;">
                                    <asp:Label ID="Label2" runat="server" Text="Supplier Fee"></asp:Label>
                                    <asp:Label ID="lblSupplierCurrency" runat="server" Text=""></asp:Label>                                       
                                    <asp:TextBox ID="txtSupplierFee" runat="server" Text="0" CssClass="form-control"></asp:TextBox>                              
                                 </div>
                          <%} %>
                                    <div class="col-md-1 marbot_10" style="margin-top:27px;">                                   
                                 <label><asp:Button ID="btnRefund" class="btn btn-primary" runat="server" Text="Refund" CommandName="Refund" CommandArgument='<%#Eval("ConfirmationNo") %>' OnClientClick="return ConfirmNo();" /></label>
                                    </div>
                     </div>              
                </div>
            </div>
        </div>

                                <div class="col-md-2">Request Change
                                <asp:DropDownList CssClass="form-control" ID="ddlBooking" runat="server">
                                                            <asp:ListItem Selected ="True" Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="Cancel Booking" Value="Cancel Booking"></asp:ListItem>
                                                                <asp:ListItem Text="Amend Booking" Value="Amend Booking"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <br>
                                                            </br>
                                                            <b id="errRemarks" style="color:Red" runat="server"></b>
                                                           </div> 
                                                     
                                 <div class="col-md-3 marbot_10"> Remarks
                                 
                                 <asp:TextBox CssClass="form-control" ID="txtRemarks" TextMode="MultiLine" Rows="1" Text="Enter Remarks here"
                                                                runat="server" onfocus="this.style.color='#000000'; if( this.value=='Enter Remarks here' ) { this.value=''; }"></asp:TextBox>                                                                                                            
                                      </div>
                                                                                                     
                                   <%-- <div class="col-md-1 marbot_10" style="margin-top:20px;">  <asp:Button Class="btn btn-secondary" ID="btnBooking" runat="server" CommandName="Booking" CommandArgument='<%#Eval("ConfirmationNo") %>'
                                                                Text="Submit" Visible="false" /> </div>--%>
                                                       <div class="col-md-1 marbot_10" style="margin-top:20px;"><asp:Button Class="btn btn-secondary" ID="btnBooking" runat="server" CommandName="Booking" CommandArgument='<%#Eval("ConfirmationNo")%>'
                                                                Text="Submit" Visible="false"/> </div>

                                                        
                                <div class="clearfix"> </div>
                                </div>           
                                                       </td>
                                                    </tr>
                                                     
                                                </table>
                                                <table>
                                 <tr>
                                    <td align="left" colspan="2" style="padding-left: 20px">
                                        <div style="position: relative">
                                            <asp:LinkButton ID="lnkPayment" runat="server" Visible="false" Text="Payment Information"
                                                href="#"></asp:LinkButton>
                                            <div id="PaymentInfo-<%#Container.ItemIndex %>" class="visa_PaymentInfo_pop" style="position: absolute;
                                                display: none; bottom: -70px; left: 234px; height: auto!important; z-index: 9999;">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                </table>
                                 <div class="clearfix"> </div>
                                

                                </div>

                         
                         <%--Added by somasekhar for Approval Status--%>
                         <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4"> <asp:Label ID="lblCorpLabel" runat="server" Text="Select Approval Status:"></asp:Label> 
                                   <asp:DropDownList ID="ddlApprovalStatus" runat="server" CssClass="form-contrl" Width="200px">
                                       <asp:ListItem Text="Select Status" Value="-1" Selected="True"></asp:ListItem>
                                       <asp:ListItem Text="Approve" Value="A" ></asp:ListItem>
                                       <asp:ListItem Text="Reject" Value="R"></asp:ListItem>
                                   </asp:DropDownList> 
                                 
                                     <br> 
                                 
                                     </br>
                                     <asp:TextBox ID="txtCorpRemarks" runat="server" TextMode="MultiLine" Width="200px" Height="50px" style="display:none;"></asp:TextBox> 
                                    <br> </br>
                                    <asp:Label ID="lblCorpError" runat="server" ForeColor="Red"></asp:Label>
                                </div>

                         <div class="col-md-4"> <label class=" pull-left">
                             <asp:Button ID="btnUpdateStatus" CssClass="button" runat="server" Text="Update"  Visible="true" CommandName="ApprovalStatus" CommandArgument='<%#Eval("HotelId") %>'/> </label> </div>
                                
                             <div class="clearfix"> </div>
                                </div>
                         <div class="col-md-12 margin-top-10"> 
                                
                                <div class="col-md-4"> 
                                    <b> <asp:Label ID="lblApprovalStatus" runat="server" Text=""></asp:Label></b>
                                    <asp:Label ID="lblApprovedBy" runat="server" Text=""></asp:Label> 
                                </div>
                                
                             <div class="clearfix"> </div>
                                </div>
                        <%--End --  for Approval Status--%>                              
                                <div class="col-md-2"> 
                                            <asp:LinkButton ID="lnkPendingRemarks" style="font-weight:600;color:red;" runat="server" Visible="false" Text="Failure Remarks"
                                                href="#"></asp:LinkButton>
                                </div>
                              <div class="modal fade in farerule-modal-style" data-backdrop="static" id="FailureRemarksModal" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" id="btnModelClose">&times;</button>
                        <h4 class="modal-title">Failure Remarks</h4>
                    </div>
                    <div class="modal-body">                                          
                        <div class="bg_white" id="FailureRemarks" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;color:red"></div>
                    </div>
                </div>
            </div>
        </div>
                             <div class="clearfix"> </div>
                            
                         <div class="clearfix"></div>
                                                        
                             </div>       
                                
                                
                                
                               
                                  
                                
                                
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                </td>
            </tr>
            <tr>
                 <td align="right">
                    <%=show %>
                </td>
            </tr>
         
        </table>
        
        
    </div>

    
    
    <input type="hidden" name="filter" value="true" />

   
</asp:Content>






<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>


                
   
