<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ROSReceiptMasterUI" Title="ROS Receipt Master"  ValidateRequest="false" EnableEventValidation="false" Codebehind="ROSReceiptMaster.aspx.cs" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%--<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="uc1.Web.UI.Controls.AutoComplete" Namespace="uc1.Web.UI.Controls" TagPrefix="cc1" %> --%>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">



<%--<style>


body:nth-of-type(1) .flt {                     /* mozilla + Chrome... */
  float:right; font-size:14px;  }
}





</style>--%>

<script src="Scripts/Autocomplete/autoComplete.js" type="text/javascript"></script>
<script  type="text/javascript">

 
function MaxLength(value)
 {
 
   var len = value.split("~");
   getElement('txtPaxMobNo').maxLength = len[1];
   getElement('hdfMaxLenGrntrMobNo').value = len[1];

}
function validateRadioButtonList(radioButtonListId) {
    var listItemArray = document.getElementsByName(radioButtonListId);
    var isItemChecked = false;

    for (var i = 0; i < listItemArray.length; i++) {
        var listItem = listItemArray[i];

        if (listItem.checked) {
            //alert(listItem.value);
            isItemChecked = true;
        }
    }

    if (isItemChecked == false) {
       //' alert('Nothing is checked!');

        return false;
    }

    return true;
}

function Save() {


    var currentDate = new Date(getElement('hdfCurrentDate').value);
    var modeValue = getElement('ddlSettlementMode').value;
    var docDate = GetDateObject('ctl00_cphTransaction_dcDocDate');
    if (docDate == null) addMessage('Please select Date !', '');
    if (docDate > currentDate) addMessage('Date should not be later than Current Date!', '');


    var fromDate = GetDateObject('ctl00_cphTransaction_dcFromDate');
    var toDate = GetDateObject('ctl00_cphTransaction_dcToDate');
    if (fromDate == null) addMessage('Please select From Date !', '');
    if (toDate == null) addMessage('Please select To Date !', '');

    if (fromDate != null && toDate != null && (fromDate > toDate)) addMessage('To-Date should be later than or equal to From-Date !', '');



    if (getElement('txtPaxName').value == '') addMessage('Pax Name cannot be blank!', '');    
    if (getElement('ddlAdults').selectedIndex <= 0) addMessage('Please select Adults from the List!', '');


    if (getElement('ddlSettlementMode').selectedIndex <= 0) addMessage('Please Select Mode from the List!', '');

    if (parseFloat(getElement('txtSettlementDue').value) != 0) addMessage('Due should be zero!', '')
    if (getElement('txtModeRemarks').value == '' && (getElement('ddlSettlementMode').value == '7' || getElement('ddlSettlementMode').value == '8')) addMessage('settlement Remarks cannot be Blank!', '')

    if (getElement('txtPaxMobNo').value == '') addMessage('Pax Mobile # cannot be blank!', '');
    //    else if(getElement('txtPaxMobNo').value.length!=getElement('hdfMaxLenGrntrMobNo').value) addMessage('Pax Mobile # is incorrect !','');


    switch (modeValue) {
        case '1':
            if (getElement('txtCashBaseAmount').value == '' || parseFloat(getElement('txtCashBaseAmount').value) <= 0) addMessage('Cash Base Amount cannot be blank/zero!', '');
            break;
        case '2':
            if (getElement('txtCreditBaseAmount').value == '' || parseFloat(getElement('txtCreditBaseAmount').value) <= 0) addMessage('Credit Base Amount cannot be blank/zero!', '');
            break;
        case '3':
            if (getElement('txtCardBaseAmount').value == '' || parseFloat(getElement('txtCardBaseAmount').value) <= 0) addMessage('Card Base Amount cannot be blank/zero!', '');
            if (getElement('ddlCardType').selectedIndex <= 0) addMessage('Please Select Card Type from the List!', '');
            break;
        case '4':
            if (getElement('txtCashBaseAmount').value == '' || parseFloat(getElement('txtCashBaseAmount').value) <= 0) addMessage('Cash Base Amount cannot be blank/zero!', '');
            if (getElement('txtCreditBaseAmount').value == '' || parseFloat(getElement('txtCreditBaseAmount').value) <= 0) addMessage('Credit Base Amount cannot be blank/zero!', '');
            break;
        case '5':
            if (getElement('txtCashBaseAmount').value == '' || parseFloat(getElement('txtCashBaseAmount').value) <= 0) addMessage('Cash Base Amount cannot be blank/zero!', '');
            if (getElement('txtCardBaseAmount').value == '' || parseFloat(getElement('txtCardBaseAmount').value) <= 0) addMessage('Card Base Amount cannot be blank/zero!', '');
            if (getElement('ddlCardType').selectedIndex <= 0) addMessage('Please Select Card Type from the List!', '');
            break;
        case '6':
            if (getElement('txtCashBaseAmount').value == '' || parseFloat(getElement('txtCashBaseAmount').value) <= 0) addMessage('Cash Base Amount cannot be blank/zero!', '');
            if (getElement('txtCreditBaseAmount').value == '' || parseFloat(getElement('txtCreditBaseAmount').value) <= 0) addMessage('Credit Base Amount cannot be blank/zero!', '');
            if (getElement('txtCardBaseAmount').value == '' || parseFloat(getElement('txtCardBaseAmount').value) <= 0) addMessage('Card Base Amount cannot be blank/zero!', '');
            if (getElement('ddlCardType').selectedIndex <= 0) addMessage('Please Select Card Type from the List!', '');
            break;
        case '7':
            if (getElement('txtCashBaseAmount').value == '' || parseFloat(getElement('txtCashBaseAmount').value) <= 0) addMessage('Bank Transfer Base Amount cannot be blank/zero!', '');
            break;
        case '8':
            if (getElement('txtCashBaseAmount').value == '' || parseFloat(getElement('txtCashBaseAmount').value) <= 0) addMessage('Others Base Amount cannot be blank/zero!', '');
            break;

        default: document.getElementById('trCash').style.display = 'block';
    }


    if (getMessage() != '') {
        alert(getMessage()); clearMessage(); return false;
    }
    getElement('divPlsWait').style.display = 'block';
}
//    
//    var selectedValue;
//    function getSelectedFee(id)
//    {        
//        id=id.replace(/_/g,"$");
//        var items=document.getElementsByName(id);
//        for(i=0;i<items.length;i++)
//        {
//            if(items[i].checked && items[i].value!=selectedValue)
//            {                
//                selectedValue=items[i].value;
//                return items[i].value;
//            }
//        }
//    }
   
   
 function setLocalAmount(baseId,localId)
   {
        //var exchRate=parseFloat(getElement('txtExchRate').value);
        var exchRate=1;
        var baseAmount=parseFloat(document.getElementById(baseId).value);
        var localAmount=exchRate*baseAmount;
        document.getElementById(localId).value = localAmount.toFixed(PAGE_DECIMALVAL);
        calcAmount();
   }
  function setExchRate(value)
   {
            getElement('txtExchRate').value=value;
            setToFixedThis(getElement('txtExchRate').id);
            
            setLocalAmount(getElement('txtCashBaseAmount').id,getElement('txtCashLocalAmount').id);           
            setLocalAmount(getElement('txtCreditBaseAmount').id,getElement('txtCreditLocalAmount').id);
            setLocalAmount(getElement('txtCardBaseAmount').id,getElement('txtCardLocalAmount').id);
   }
   
    function calcAmount()
   {
        
         if(getElement('txtFare').value=='' || !isNaN(getElement('txtFare').value))
        {
           
            var fare =parseFloat(getElement('txtFare').value);                   
            var cardRate=parseFloat((isNaN(getElement('hdfCardCharge').value) ||getElement('hdfCardCharge').value=='' )?'0':getElement('hdfCardCharge').value);//toFixed(PAGE_DECIMALVAL);                    
           
            var totalFare = parseFloat(fare).toFixed(PAGE_DECIMALVAL)
//            var totalCardAmount =parseFloat((isNaN(getElement('txtCardBaseAmount').value) ||getElement('txtCardBaseAmount').value=='' )?'0':getElement('txtCardBaseAmount').value);
            
            var cashAmount =parseFloat(getElement('txtCashLocalAmount').value);
            var creditAmount=parseFloat((isNaN(getElement('txtCreditLocalAmount').value) ||getElement('txtCreditBaseAmount').value=='' )?'0':getElement('txtCreditBaseAmount').value);

            var totalCardAmount = parseFloat(fare - creditAmount - cashAmount).toFixed(PAGE_DECIMALVAL)
            
            var totalCardRate = totalCardAmount*cardRate/100;
            var exchRate= getElement('txtExchRate').value;
            var modeValue=getElement('ddlSettlementMode').value
            getElement('txtTotalFare').value = totalFare;
            
             if(modeValue=='3' || modeValue=='5' || modeValue=='6' )
                {
                    getElement('txtTotCardCharge').value = parseFloat(totalCardRate).toFixed(PAGE_DECIMALVAL);
                }            
//
                getElement('txtToCollect').value = parseFloat(fare + totalCardRate).toFixed(PAGE_DECIMALVAL);
                
        }
        var cashLocalAmnt=parseFloat((isNaN(getElement('txtCashLocalAmount').value) ||getElement('txtCashLocalAmount').value=='' )?'0':getElement('txtCashLocalAmount').value);
             
        var creditLocalAmnt=parseFloat((isNaN(getElement('txtCreditLocalAmount').value) ||getElement('txtCreditLocalAmount').value=='' )?'0':getElement('txtCreditLocalAmount').value);
        var cardLocalAmnt=parseFloat((isNaN(getElement('txtCardLocalAmount').value) ||getElement('txtCardLocalAmount').value=='' )?'0':getElement('txtCardLocalAmount').value);

        getElement('txtSettlementReceived').value = (cashLocalAmnt + creditLocalAmnt + cardLocalAmnt).toFixed(PAGE_DECIMALVAL);
        DueAmount();
        
   }
  function DueAmount()
  {
        var toCollect=parseFloat((isNaN(getElement('txtToCollect').value) ||getElement('txtToCollect').value=='' )?'0':getElement('txtToCollect').value);
        var received=parseFloat((isNaN(getElement('txtSettlementReceived').value) ||getElement('txtSettlementReceived').value=='' )?'0':getElement('txtSettlementReceived').value);
        var dueAmount=toCollect-received;
        getElement('txtSettlementDue').value=dueAmount.toFixed(PAGE_DECIMALVAL);
  }
  function ShowBalance()
  {
        var modeValue=getElement('ddlSettlementMode').value
        var toCollect= getElement('txtToCollect').value;
        if(modeValue=='1' || modeValue=='7' || modeValue=='8' )
                {
                    //getElement('txtCashBaseAmount').value=getElement('txtSettlementDue').value+getElement('txtCashBaseAmount').value;
                    //alert(settlementDue+' :'+cashBaseAmnt);
                    getElement('txtCashBaseAmount').value=toCollect;//.toFixed(PAGE_DECIMALVAL);
                    setLocalAmount(getElement('txtCashBaseAmount').id,getElement('txtCashLocalAmount').id)
                    setToFixed('txtCashBaseAmount');
                }
                
                if(modeValue=='2' )
                {
                    //getElement('txtCreditBaseAmount').value=getElement('txtSettlementDue').value;
                    getElement('txtCreditBaseAmount').value=toCollect
                    setLocalAmount(getElement('txtCreditBaseAmount').id,getElement('txtCreditLocalAmount').id);
                    setToFixed('txtCreditBaseAmount');
                }
                if(modeValue=='3' )
                {
                    //getElement('txtCreditBaseAmount').value=getElement('txtSettlementDue').value;
                    getElement('txtCardBaseAmount').value=toCollect
                    setLocalAmount(getElement('txtCardBaseAmount').id,getElement('txtCardLocalAmount').id);
                    setToFixed('txtCardBaseAmount');
                }

  }
    function TotalFare()
    {
    
        if(getElement('txtFare').value=='' || !isNaN(getElement('txtFare').value))
        {
           
            var fare =parseFloat(getElement('txtFare').value);                   
            var cardRate=parseFloat((isNaN(getElement('hdfCardCharge').value) ||getElement('hdfCardCharge').value=='' )?'0':getElement('hdfCardCharge').value);//toFixed(PAGE_DECIMALVAL);                    
           // var handlingFee =parseFloat(getElement('txtHandlingFee').value);             
            var totalFare =parseFloat(fare).toFixed(PAGE_DECIMALVAL)
//            var totalCardAmount =parseFloat((isNaN(getElement('txtCardBaseAmount').value) ||getElement('txtCardBaseAmount').value=='' )?'0':getElement('txtCardBaseAmount').value);
            
            var cashAmount =parseFloat(getElement('txtCashLocalAmount').value);
            var creditAmount=parseFloat((isNaN(getElement('txtCreditLocalAmount').value) ||getElement('txtCreditBaseAmount').value=='' )?'0':getElement('txtCreditBaseAmount').value);
            
            var totalCardAmount =parseFloat(fare-creditAmount-cashAmount).toFixed(PAGE_DECIMALVAL)
            
            var totalCardRate = totalCardAmount*cardRate/100;
            var exchRate= getElement('txtExchRate').value;
            var modeValue=getElement('ddlSettlementMode').value
            getElement('txtTotalFare').value = totalFare;
            
             if(modeValue=='3' || modeValue=='5' || modeValue=='6' )
                {
                   getElement('txtTotCardCharge').value = parseFloat(totalCardRate).toFixed(PAGE_DECIMALVAL);
                }            
//        
            getElement('txtToCollect').value= parseFloat(fare+totalCardRate).toFixed(PAGE_DECIMALVAL);
                
        }
        else getElement('txtToCollect').value=getElement('txtTotalFare').value=getElement('txtTotCardCharge').value= '0.00';
        
       ShowBalance();
       DueAmount();
       calcAmount()
    }
    
//    function TotalHandlingFee()
//    {
//        var adults =parseFloat(getElement('ddlAdults').value);
//        var children =parseFloat(getElement('ddlChildren').value);
//        var exchRate= getElement('txtExchRate').value;
//        
//        //var handlingFee =parseFloat(getElement('hdfHandlingFee').value);
//        //alert(handlingFee);
//        getElement('txtHandlingFee').value= parseFloat((handlingFee)*(adults+children)*exchRate).toFixed(PAGE_DECIMALVAL);
//        TotalFare();
//    }
    function windowClose()
    {    
            window.opener=self;//To skip exit confirmation message
            window.close();//TODO: redirect to login page 
        
    }
    function parentUpdate()
    {
        window.opener.document.getElementById('ctl00_cphTransaction_txtCollectTicketCost').value=getElement('txtTotalFare').value;
        window.opener.ToCollect()
    }
    
      function setCardRate(value)
   {
            getElement('hdfCardCharge').value=value;          
            //alert(getElement('hdfCardCharge').value);
            TotalFare();
            //setToFixedThis(getElement('txtExchRate').id);
            
   }
     function showHidMode(value)
   {
       value=value.toString();
       document.getElementById('trCash').style.display='none';
       document.getElementById('trCredit').style.display='none';
       document.getElementById('trCard').style.display='none';
       getElement('lblCashBaseAmount').innerHTML='Cash Base Amnt:'
       getElement('lblCashLocalAmount').innerHTML='Cash Local Amnt:'
       document.getElementById('tblCardType').style.display='none';
       getElement('hdfCardCharge').value='0';
      
        switch(value)
        {
            case '1':
                //alert(value);
                document.getElementById('trCash').style.display='block';
                getElement('txtCreditBaseAmount').value=parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCreditLocalAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCardBaseAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCardLocalAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtTotCardCharge').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('ddlCardType').selectedIndex='0';
                break;
            case '2':
                document.getElementById('trCredit').style.display='block';
                getElement('txtCashBaseAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCashLocalAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCardBaseAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCardLocalAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtTotCardCharge').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('ddlCardType').selectedIndex='0';
                break;
            case '3':
                 document.getElementById('trCard').style.display='block';
                 getElement('txtCashBaseAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                 getElement('txtCashLocalAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                 getElement('txtCreditBaseAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                 getElement('txtCreditLocalAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                 document.getElementById('tblCardType').style.display='block';
                 setCardRate(getElement('ddlCardType').value)
                break;
            case '4':
                document.getElementById('trCash').style.display='block';
                document.getElementById('trCredit').style.display='block';
                getElement('txtCardBaseAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCardLocalAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtTotCardCharge').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('ddlCardType').selectedIndex='0';
                break;
            case '5':
                document.getElementById('trCash').style.display='block';
                document.getElementById('trCard').style.display='block';
                getElement('txtCreditBaseAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCreditLocalAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                document.getElementById('tblCardType').style.display='block';
                setCardRate(getElement('ddlCardType').value)
                break;
            case '6':
                document.getElementById('trCash').style.display='block';
                document.getElementById('trCredit').style.display='block';
                document.getElementById('trCard').style.display='block';
                document.getElementById('tblCardType').style.display='block';
                setCardRate(getElement('ddlCardType').value)
                break;
            case '7':
                document.getElementById('trCash').style.display='block';
                getElement('lblCashBaseAmount').innerHTML='Bank Base Amnt'
                getElement('lblCashLocalAmount').innerHTML = 'Bank Local Amnt'
                getElement('txtCreditBaseAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCreditLocalAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCardBaseAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCardLocalAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtTotCardCharge').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('ddlCardType').selectedIndex='0';
                
                break;
            case '8':
                document.getElementById('trCash').style.display='block';
                getElement('lblCashBaseAmount').innerHTML='Oth Base Amnt'
                getElement('lblCashLocalAmount').innerHTML='Oth Local Amnt'
                getElement('txtCreditBaseAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCreditLocalAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCardBaseAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtCardLocalAmount').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('txtTotCardCharge').value = parseFloat('0.00').toFixed(PAGE_DECIMALVAL);
                getElement('ddlCardType').selectedIndex='0';
                break;
                
            default: document.getElementById('trCash').style.display='block';
        }
         calcAmount();
         ShowBalance();
        //alert(getElement('hdfCardCharge').value);
       // TotCardCharge();
       
        
        
   return false;
   }
   function SetPaxName(id) {
      
       var ddl = document.getElementById(id);
       getElement('txtPaxName').value = ddl.options[ddl.selectedIndex].text;              
   
   }
   function calcTariffAmount() {

       var fromDate = GetDateTimeObject('ctl00_cphTransaction_gvROSDetails_ctl_dcFromDate');
       var toDate = GetDateTimeObject('ctl00_cphTransaction_gvROSDetails_ctl_dcToDate');
       var diff = fromDate - toDate;
      // alert(diff);
   
   }

   function CalcPendings() {

//alert('hi');
      var counts = getElement("gvPendingRcpts").rows.length - 1;
      
       var rows;
       var rowNo;
       var totalAmount=0;
       var amount=0;

       for (var i = 0; i < counts; i++) {

           if (i + 2 < 10) {

               rows = "0" + (i + 2);
           }
           else {

               rows = i + 2;
           }
           rowNo = i + 1;
          // alert(document.getElementById('ctl00_cphTransaction_gvPendingRcpts_ctl' + rows + '_ITchkSelect'));
           if (document.getElementById('ctl00_cphTransaction_gvPendingRcpts_ctl' + rows + '_ITchkSelect') != null) {
               if (document.getElementById('ctl00_cphTransaction_gvPendingRcpts_ctl' + rows + '_ITchkSelect').checked) {            
                   amount = parseFloat(document.getElementById('ctl00_cphTransaction_gvPendingRcpts_ctl' + rows + '_ITlblAmount').innerHTML);
                   //alert(amount);
                   totalAmount = parseFloat(totalAmount) + amount;
               }
           }
           getElement('txtFare').value = parseFloat(totalAmount).toFixed(2);
           TotalFare();
       }
       return true;

   }

   function OnDateExit(dcId) {
       ShowTariffGridValues();
   }
   function ShowTariffGridValues()
   {
   
    getElement('ddlStaff').selectedIndex=0;

    //TO CLEAR THE CHECK BOX
   
    if (getElement("gvPendingRcpts") != null) {
        var counts = getElement("gvPendingRcpts").rows.length - 1;

        var rows;
        var rowNo;
        var totalAmount = 0;
        var amount = 0;

        for (var i = 0; i < counts; i++) {

            if (i + 2 < 10) {

                rows = "0" + (i + 2);
            }
            else {

                rows = i + 2;
            }
            rowNo = i + 1;
            // alert(document.getElementById('ctl00_cphTransaction_gvPendingRcpts_ctl' + rows + '_ITchkSelect'));
            if (document.getElementById('ctl00_cphTransaction_gvPendingRcpts_ctl' + rows + '_ITchkSelect') != null) {
                if (document.getElementById('ctl00_cphTransaction_gvPendingRcpts_ctl' + rows + '_ITchkSelect').checked) {
                    document.getElementById('ctl00_cphTransaction_gvPendingRcpts_ctl' + rows + '_ITchkSelect').checked = false;
                }
            }
        }
        //END --TO CLEAR THE CHECK BOX

        getElement('gvPendingRcpts').style.display = 'none';
    }
        getElement('lblTrfCodeValue').innerHTML = '';
        getElement('txtTrfAmount').value = "";
        getElement('txtFare').value = "0.00";
        TotalFare();
    
   }
</script>
<asp:HiddenField id = "hdfNrmHandActivity" runat ="server" value ="0"></asp:HiddenField>
<asp:HiddenField id = "hdfCurrentDate" runat ="server" value ="0"></asp:HiddenField>
<asp:HiddenField id = "hdfVMSDocNo" runat ="server" value ="0"></asp:HiddenField>
<asp:HiddenField id = "hdfVisaNo" runat ="server" value ="0"></asp:HiddenField>
<asp:HiddenField id = "hdfVisaRefId" runat ="server" value ="0"></asp:HiddenField>
<asp:HiddenField id = "hdfPaxName" runat ="server" value ="0"></asp:HiddenField>
<asp:HiddenField id = "hdfGrntEmail" runat ="server" value ="0"></asp:HiddenField>
<asp:HiddenField runat="server" id="hdfMaxLenGrntrMobNo" ></asp:HiddenField>
<asp:HiddenField runat="server" id="hdfServiceType" ></asp:HiddenField>
<asp:HiddenField runat="server" id="hdfOk2bId" ></asp:HiddenField>
<asp:HiddenField runat="server" id="hdfDecimalLocations" Value="3,4" ></asp:HiddenField><%--LocationIds--%>
<asp:HiddenField runat="server" id="hdfDecimals" Value="3-2,4-3" ></asp:HiddenField><%--locationId-DecimalNo--%>
<asp:HiddenField runat="server" id="hdfDecimalNo" Value="2" ></asp:HiddenField><%--Decimal No--%>
<script  type="text/javascript">
    PAGE_DECIMALVAL = parseInt(getElement('hdfDecimalNo').value);
    GLOBAL_DECIVAL=PAGE_DECIMALVAL
        //alert(getElement('hdfDecimalNo').value);
        //alert(PAGE_DECIMALVAL);
 </script>
<%--<asp:UpdatePanel ID="upnl" runat="server" UpdateMode="conditional">
    <ContentTemplate>--%>
       
       <div class="paramcon"> 
        
         <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2 col-xs-12"><asp:Label ID="lblDocType" Text="Document Type:" runat="server"></asp:Label> </div>
 
    <div class="col-md-2 col-xs-12"> <asp:TextBox ID="txtDocType"  runat="server" Enabled="false" CssClass="inputDisabled form-control" ></asp:TextBox></div>
    
    
    <div class="col-md-2 col-xs-12"> <asp:Label ID="lblDocNumber" Text="Receipt Number:" runat="server" ></asp:Label></div>
    
    
     <div class="col-md-2 col-xs-12"> <asp:TextBox ID="txtDocumentNumber"  runat="server" Enabled="false" CssClass="inputDisabled form-control"></asp:TextBox></div>
    
     <div class="col-md-2 col-xs-3"> <asp:Label ID="lblDocDate" Text="Date:" runat="server"></asp:Label></div>
     
     
         <div class="col-md-2 col-xs-9"> <uc1:DateControl ID="dcDocDate" runat="server" Enabled="false" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></div>


<div class="clearfix"></div>
    </div>
    
      <div class="col-md-12 padding-0 marbot_10">                                      
   
   
        <div class="col-md-2 col-xs-6"> <asp:Label ID="lblFromDate" Text="From Date:" runat="server" ></asp:Label></div>
        
        
         <div class="col-md-2 col-xs-6"> <uc1:DateControl ID="dcFromDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></div>
         
         
 <div class="col-md-2 col-xs-6"> <asp:Label ID="lblToDate" Text="To Date:" runat="server" ></asp:Label></div>
 
 
    <div class="col-md-2 col-xs-6"><uc1:DateControl ID="dcToDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl> </div>
    
  
<div class="clearfix"></div>
    </div>




     <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblCompanyName" Text="Company Name:" runat="server"></asp:Label></div>
 
 
    <div class="col-md-2"><asp:DropDownList ID="ddlCompany"  AutoPostBack="true"  OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" runat="server" CssClass="inputEnabled form-control" ></asp:DropDownList> </div>
    
    <div class="col-md-2"> <asp:Label ID="lblStaff" Text="Staff ID:" runat="server" ></asp:Label></div>
 
 
    <div class="col-md-2">
    
    <asp:DropDownList ID="ddlStaff"  runat="server" CssClass="inputDdlEnabled form-control" AutoPostBack="true"  OnSelectedIndexChanged="ddlStaff_SelectedIndexChanged" ></asp:DropDownList>
    
   </div>
    
    
    <div class="col-md-2"> <asp:Label ID="lblPaxName" Text="Pax Name:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"><asp:TextBox ID="txtPaxName"  runat="server" CssClass="inputEnabled form-control" ></asp:TextBox> </div>
     


  


<div class="clearfix"></div>
    </div>
    
    
    
    
     <div class="col-md-12 padding-0 marbot_10">   
    

    
    
      <div class="col-md-2"> <asp:Label ID="lblTrfAmount"  runat="server" Text=" Tariff Amount:"></asp:Label> </div>
      
        <div class="col-md-2">  <asp:TextBox ID="txtTrfAmount" onkeypress="return restrictNumeric(this.id,'2');" OnChange="setToFixed('txtTrfAmount');TotalFare();" runat="server" CssClass="inputEnabled form-control" ></asp:TextBox></div>
        
        
            
       <div class="col-md-2"> <asp:Label ID="lblTrfCode"  runat="server" Text="Tariff Code :">
    
    
    </asp:Label> </div>
       
<div class="col-md-2"> <asp:Label ID="lblTrfCodeValue" style="font-weight:bold;color:Red" runat="server" Text=""></asp:Label> </div>
     
    <div class="clearfix"></div>
    
    </div>
    
    
    
    
    
     <div class="col-md-12 padding-0 marbot_10">  
    
    
    <asp:GridView ID="gvPendingRcpts" CssClass="" runat="server"  AutoGenerateColumns="False" Width="100%"
                                    DataKeyNames="receipt_id" CellPadding="4" 
                                    CellSpacing="0"  GridLines="none" >
                                    <Columns>
                                       <asp:TemplateField>
                                            <HeaderTemplate>
                                                <%--<asp:CheckBox runat="server" id="HTchkSelectAll" Text="Select All" AutoPostbACK="true" OnCheckedChanged="ITchkSelect_CheckedChanged"></asp:CheckBox>                                            --%>
                                                <label style="color:Black;font-weight:bold">Select</label>
                                                
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="left" />
                                            <ItemTemplate>                                         
                                             <asp:CheckBox runat="server" id="ITchkSelect" Checked="false" onClick="CalcPendings();" ></asp:CheckBox>
                                             <asp:HiddenField runat="server" id="IThdfRcptId" value='<%# Eval("receipt_id") %>' ></asp:HiddenField>                                                                                                           
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                           <HeaderTemplate>
                                          <label style="color:Black;font-weight:bold; float:left;">Receipt.No</label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblDocName" style="text-transform:uppercase" runat="server" Text='<%# Eval("receipt_number") %>' CssClass="grdof" ToolTip='<%# Eval("receipt_number") %>'   Width="200px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField>
                                           <HeaderTemplate>
                                          <label style="color:Black;font-weight:bold; float:left;">Amount</label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblAmount" style="text-transform:uppercase" runat="server" Text='<%# Eval("receipt_base_to_collect") %>' CssClass="grdof" ToolTip='<%# Eval("receipt_base_to_collect") %>'   Width="200px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                     
                                        
                                         
                                    </Columns>
                                    <HeaderStyle></HeaderStyle>
                                    <RowStyle CssClass="" HorizontalAlign="left" />
                                    <AlternatingRowStyle CssClass="" />                                    
                                </asp:GridView>
    
    </div>



     <div class="col-md-12 padding-0 marbot_10">                                      
   
      
     <div class="col-md-2"> <asp:Label ID="lblPaxMobNo" Text="Mobile #:" runat="server"></asp:Label></div>
     
     
         <div class="col-md-2"><asp:TextBox ID="txtPaxMobNo"  runat="server" CssClass="form-control"  onkeypress="return restrictNumeric(this.id,'1');" ></asp:TextBox> </div>
    <div class="col-md-2"> <asp:Label ID="lblVehicle" Text="Vehicle:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2 marbot_10"> <asp:DropDownList ID="ddlVehicle"  runat="server" style="text-transform:lowercase"  CssClass="inputEnabled form-control"></asp:DropDownList></div>
     <div class="col-md-4">
    
    
     <table width="300"  cellpadding="0" cellspacing="0">
            <tr>
                <td><asp:Label ID="lblAdults" Text="Adults" runat="server" ></asp:Label></td>
                         <td><asp:DropDownList ID="ddlAdults" onchange="TotalHandlingFee();" runat="server" CssClass="inputDdlEnabled form-control"></asp:DropDownList></td>
                      
              
                <td><asp:Label ID="lblChild" Text="Child" runat="server"></asp:Label></td>
                
                          <td><asp:DropDownList ID="ddlChildren" onchange="TotalHandlingFee();" runat="server" CssClass="inputDdlEnabled form-control"></asp:DropDownList></td>
              
                <td><asp:Label ID="lblInfants" Text="Infants" runat="server"></asp:Label></td>
           
            
       
      
                <td><asp:DropDownList ID="ddlInfants" runat="server" CssClass="inputDdlEnabled form-control"></asp:DropDownList></td>
            </tr>
            </table>
     
     
      </div>
      

<div class="clearfix"></div>
    </div>
    
    
    
         <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"><asp:Label ID="lblFare" Text="Amount:" runat="server"></asp:Label> </div>
 
    <div class="col-md-2"> <asp:TextBox ID="txtFare" onkeypress="return restrictNumeric(this.id,'2');" OnChange="setToFixed('txtFare');TotalFare();" runat="server" CssClass="inputEnabled form-control" ></asp:TextBox></div>
    
    
    <div class="col-md-2"> <asp:Label ID="lblTotalFare" Text="Total Amount:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"><asp:TextBox ID="txtTotalFare"  runat="server" Enabled="false" CssClass="inputDisabled form-control"></asp:TextBox> </div>
   

         
         

<div class="clearfix"></div>
    </div>
       
    
      <div class="col-md-12 padding-0 marbot_10">  
    
        <div class="col-md-2"><asp:Label ID="lblRemarks" Text="Remarks:" runat="server"></asp:Label> </div>
        
        
     <div class="col-md-6"><asp:TextBox ID="txtRemarks"  runat="server" TextMode="multiline" Height="45" CssClass="inputEnabled form-control" Width="100%" ></asp:TextBox> </div>
     <div class="clearfix"></div>
    </div>
    
       <div class="col-md-12 padding-0 marbot_10">  
     
     <div class="col-md-2"><asp:Label ID="lblNotes" Text="Notes:" runat="server" ></asp:Label> </div>
     
         <div class="col-md-6"><asp:TextBox ID="txtNotes"  TextMode="MultiLine" Height="40px" runat="server" CssClass="inputEnabled"  Width="100%" ></asp:TextBox> </div>
         
    
   <div class="clearfix"></div>
    </div>
    
    
    
         <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblCreatedBy" Text="Created By:" runat="server" ></asp:Label></div>
    <div class="col-md-2"> <asp:TextBox ID="txtCreatedBy"  runat="server" Enabled="false" CssClass="inputDisabled form-control"></asp:TextBox></div>


<div class="clearfix"></div>
    </div>
    
      <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
    
    
    
    <div class="clearfix"></div>
    
    </div>
    
    <div>
<div class="content12" style="padding-top:10px;">



<div class="ns-h3"> Settlement Mode</div>

<div class="settlement_mode paramcon bg_white bor_gray">
<table cellpadding="0" cellspacing="0" border="0" >
    <tr>    
        <td valign="top">
        
      
        
      <div style=" padding-top:10px">
      
      <div style=" margin-bottom:3px;">
    <div style="width:100px; float:left; text-align:right"><asp:Label runat="server" id="lblSettlementMode" Text="Mode:"></asp:Label></div>
    
    <div style="width:100px; float:left"><asp:DropDownList Id="ddlSettlementMode"  onchange="return showHidMode(this.value);" CssClass="inputDdlEnabled form-control"  runat="server" ></asp:DropDownList></div>
    <div style="width:125px; float:left; text-align:right"><asp:Label runat="server" id="lblSettlementCurrenecy"  Text="Currency:"></asp:Label></div>
	<div style="width:60px; float:left"><asp:DropDownList Id="ddlCurrency" Width="60px" CssClass="form-control" OnChange="setExchRate(this.value);"  runat="server" ></asp:DropDownList></div>
	
	<div style="width:60px; float:left"><asp:TextBox ID="txtExchRate" Text="0.00" Enabled="false" runat="server"   CssClass="inputDisabled number"  Width="40px" ></asp:TextBox></div>
	
	<div id="tblCardType" style=" display:none" >
    <div style="width:80px; float:left; text-align:right"><asp:Label ID="lblCardType" Text="Card Type:" runat="server"></asp:Label>
                   <asp:HiddenField id="hdfCardCharge" runat="server"></asp:HiddenField> </div>
                    
                   
                   
                    <div style="width:110px; float:left"><asp:DropDownList Id="ddlCardType" Width="100px" CssClass="inputDdlEnabled form-control" OnChange="setCardRate(this.value);"  runat="server" ></asp:DropDownList> </div>
                    
                    </div>
                    
                    
                    
   
    
    <div class="clear"></div>
    </div>
      
      
      <table width="100%" cellpadding="0" cellspacing="0" border="0" >
          
      
            <tr id="trCash">
            <td>
            
            <div style="margin-bottom:3px;">
    <div style="width:100px; float:left; text-align:right"><asp:Label ID="lblCashBaseAmount" Text="Base Amount:" runat="server" ></asp:Label></div>
    <div style="width:100px; float:left"><asp:TextBox ID="txtCashBaseAmount" onkeypress="return restrictNumeric(this.id,'2');" onchange="setLocalAmount(this.id,getElement('txtCashLocalAmount').id);setToFixedThis(this.id);" runat="server" CssClass="inputEnabled" Width="120px" ></asp:TextBox></div>
    <div style="width:125px; float:left; text-align:right"><asp:Label ID="lblCashLocalAmount" Text="Local Amount:" runat="server"></asp:Label></div>
	<div style="width:130px; float:left; "><asp:TextBox ID="txtCashLocalAmount" align="Right" runat="server" Enabled="false" CssClass="inputDisabled number" Width="100px" ></asp:TextBox></div>
    <div style="width:180px; float:left"> </div>
   
    
    <div class="clear"></div>
    </div>
            
            
            
            
            
            
       </td>
            
            
            
            
             
               
            </tr>
           
            <tr id="trCredit">
            
          <td>
            
            <div style="margin-bottom:3px;">
    <div style="width:100px; float:left; text-align:right"><asp:Label ID="lblCreditBaseAmount" Text="Credit Amount:" runat="server"></asp:Label></div>
    <div style="width:100px; float:left"><asp:TextBox ID="txtCreditBaseAmount" runat="server" onchange="setLocalAmount(this.id,getElement('txtCreditLocalAmount').id);setToFixedThis(this.id);" CssClass="inputEnabled" Width="120px" ></asp:TextBox></div>
    <div style="width:125px; float:left; text-align:right"><asp:Label ID="lblCreditLocalAmount" Text="Local Amount:" runat="server"></asp:Label></div>
	<div style="width:130px; float:left"><asp:TextBox ID="txtCreditLocalAmount" align="Right" runat="server" Enabled="false" CssClass="inputDisabled number" Width="100px" ></asp:TextBox></div>
    <div style="width:180px; float:left"> </div>
   
    
    <div class="clear"></div>
    </div>
            
            </td>
        
            </tr>
             <tr id="trCard">
             
             
             <td>
             
             <div style="margin-bottom:3px;">
    <div style="width:100px; float:left; text-align:right"><asp:Label ID="lblCardBaseAmount" Text="Card Amount:" runat="server" ></asp:Label></div>
    <div style="width:100px; float:left"><asp:TextBox ID="txtCardBaseAmount" runat="server" onchange="setLocalAmount(this.id,getElement('txtCardLocalAmount').id);setToFixedThis(this.id);" CssClass="inputEnabled" Width="120px" ></asp:TextBox></div>
    <div style="width:125px; float:left; text-align:right"><asp:Label ID="lblCardLocalAmount" Text="Local Amount:" runat="server"></asp:Label></div>
	<div style="width:120px; float:left"><asp:TextBox ID="txtCardLocalAmount" align="Right" runat="server" Enabled="false" CssClass="inputDisabled number" Width="100px" ></asp:TextBox></div>
    <div style="width:80px; float:left; text-align:right"> <asp:Label ID="lblTotCardCharge" Text="Card Charge:" runat="server"></asp:Label></div>
   
   
   
   <div style="width:110px; float:left"><asp:TextBox ID="txtTotCardCharge" Text="0.00"  Enabled="false"   runat="server"   CssClass="inputDisabled" Width="100px" ></asp:TextBox></div>
    
    <div class="clear"></div>
    </div>
             
             
          </td>   
             
             
             
             
               
            </tr>
            <tr>
            
            
            <td>
            
            
            <div style="margin-bottom:3px;">
    <div style="width:100px; float:left; text-align:right"><asp:Label ID="lblModeRemarks" Text="Remarks:" runat="server"></asp:Label></div>
    <div style="width:400px; float:left"><asp:TextBox ID="txtModeRemarks" runat="server" CssClass="inputEnabled" Width="324px" ></asp:TextBox></div>

   
    
    <div class="clear"></div>
    </div>
            
            
            </td>
               
            </tr>
            
            </table>
          <div class="clear"></div>
      </div>
      
      
       
      
     
           
     
        </td>
        
        
        <td valign="top" style="padding-left:20px;padding-right:10px; padding-top:5px;">
       
        <table align="right" cellpadding="0" cellspacing="0" border="0" >
           
            <tr>
                <td align="right"><asp:Label ID="lblToCollect" Text="To&nbsp;Collect:" runat="server"></asp:Label></td>
                <td ><asp:TextBox ID="txtToCollect"  runat="server" Enabled="false" CssClass="inputDisabled" Width="100px" ></asp:TextBox></td>
            </tr>
            <tr>
                 <td align="right"><asp:Label ID="lblSettlementReceived" Text="Received:" runat="server" ></asp:Label></td>
                <td ><asp:TextBox ID="txtSettlementReceived" Text="0.00"  runat="server" Enabled="false" CssClass="inputDisabled" Width="100px" ></asp:TextBox></td>
            </tr>
            
           
            <tr>
                <td align="right"><asp:Label ID="lblSettlementDue" Text="Due:"  runat="server"></asp:Label></td>
                <td ><asp:TextBox ID="txtSettlementDue"  runat="server" Text="0.00" Enabled="false" CssClass="inputDisabled" Width="100px" ></asp:TextBox></td>
            </tr>
            <tr><td style="height:15px"></td></tr>
            <tr style="padding-top:15px">
                <td  align="right" colspan="4"><table><tr><td>
                <asp:Button ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();" Width="50px" CssClass="button" OnClick ="btnSave_Click" ></asp:Button></td>
                <td><asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="button" Width="50px"  OnClick="btnClear_Click"></asp:Button>
                    <div id="divPlsWait" runat="server" style="top:500px;right:250px"  align="center" class="divPleaseWait">
                        <asp:Label ID="lblPlsWait" style="font-size:16px;font-weight:bold" runat="server" Text="Please wait........."></asp:Label>
                    </div></td>
                    </tr>
                    </table>
                </td>
            </tr>
             
            </table>
        
        
        </td>
      
      </tr>
      </table>
 
 
  </div>

<asp:Label style="COLOR: #dd1f10"  id="lblError" runat="server"></asp:Label>    
</div>
<div id="showMessage"  style="display:none" >
<a id="lnkClose" class="modalCloseImage" >
</a>

<div class="modalheader" id="divDragableHeader" >
<img id="imgClose" alt="" src="Images/Common/Message/exclamation.ico" style="margin-top:2px"  />
<span id="spanMsgHeader" style="color:#FFFFFF;padding-top:-5px;font-weight:bold"></span>
</div>

<table style="width:100%;" cellpadding="3" cellspacing="3">
    <tr>
        <td  style="width:100%;text-align:center" colspan="2">
          <span id="lblErrorMessage" class="modalError"></span>
          <a onclick="ShowHideDetailsMessage(this.id)" id="lnkViewDetails" style="display:none;cursor:hand">View Details</a>
          <div  id="divDetailMsg" style="display:none;">
            <span id="spnErrorMessage"  style="color:Navy;font-weight:bold"></span>
          </div>
         </td>
    </tr>
    <tr>
        <td align="right" style="width:50%" id="btnRow">
            <input type="button" id="buttonOk"  value="OK"  class="modalButton" style="width:60px" />
        </td>
        <td align="left">
            <input type="button" id="buttonCancel" class="button" style="display:none" value="Cancel" />
        </td>
    </tr>
</table>

</div>
 </div>
<%--</ContentTemplate>
</asp:UpdatePanel>--%>

</asp:Content>
