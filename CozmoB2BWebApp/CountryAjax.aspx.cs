﻿using System;
using System.Collections;
using CT.Core;


public partial class CountryAjax : System.Web.UI.Page
{
    string response = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            SortedList CountryList = Country.GetCountryListByRegion(Request["Region"]);
           // SortedList CountryList = CT.Core.Country.GetCountryList();

            foreach (DictionaryEntry item in CountryList)
            {
                if (response.Length > 0)
                {
                    response += "," + item.Key + "|" + item.Value;
                }
                else
                {
                    response = Request["id"] + "#" + item.Key + "|" + item.Value;
                }

            }

            Response.Write(response);
            CountryList.Clear();
        }
        catch (Exception ex)
        {
            response = ex.Message;
        }
    }
}
