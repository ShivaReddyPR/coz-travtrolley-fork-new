﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine.GDS;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using System.Collections.Generic;
using System.Linq;

public partial class FlightInvMaster : CT.Core.ParentPage
{
    private string FLIGHTINV_SEARCH_SESSION = "_FlightInvSearchList";

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {
                if (!IsPostBack)
                {
                    Clear();
                    InitializeControls();
                }


            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(FlightInvMaster.aspx)Page Load method error.Reason:" + ex.ToString(), "0");
        }

    }

    private void InitializeControls()
    {
        try
        {
            BindCountry(ddlInExCountry);
            BindCountry(ddlInToCountry);
            BindCountry(ddlOutExCountry);
            BindCountry(ddlOutToCountry);
            BindDates();
            BindTimeZones(ddlOutTimeZone);
            BindTimeZones(ddlInTimeZone);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindCountry(DropDownList ddlCountry)
    {
        try
        {
            SortedList Countries = Country.GetCountryList();
            ddlCountry.DataSource = Countries;
            ddlCountry.DataTextField = "key";
            ddlCountry.DataValueField = "value";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Select", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    private void LoadCities(string countryCode, DropDownList ddl)
    {
        List<RegCity> cities = CT.Core.RegCity.GetCityList(countryCode);
        ddl.DataSource = cities;
        ddl.DataValueField = "CityCode";
        ddl.DataTextField = "CityName";
        ddl.DataBind();
        ddl.Items.Insert(0, new ListItem("Select", "-1"));
    }

    private void BindTimeZones(DropDownList ddlTimeZones)
    {
        try
        {
            System.Collections.ObjectModel.ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            ddlTimeZones.DataSource = timeZones;
            ddlTimeZones.DataBind();
            ddlTimeZones.Items.Insert(0, new ListItem("Select", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindDates()
    {
        try
        {
            txtInDepDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtInArrivalDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtOutDepDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtOutArrivalDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtBookFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtBookToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtTravelFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtTravelToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            //Saves the record into CZ_Flight_Inventory,CZ_Flight_Inventory_Details,CZ_Flight_Inventory_FareDetails,CZ_Flight_Inventory_Quota
            Save();
            lblSuccess.Text = "Successfully saved !";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(FlightInvMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
        }
        finally
        {
            Clear();
            Utility.StartupScript(this.Page, "Clear();", "SCRIPT");

        }
    }

    private void Save()
    {
        try
        {
            //*********************Start :Flight_Inventory Header table details*********************************************/

            FlightInventoryHeader fliInvHeader;
            if (hdfEMId.Value == "0")//Insert Mode.
            {
                fliInvHeader = new FlightInventoryHeader();
                fliInvHeader.InvStatus = "A";
            }
            else //Update mode.
            {
                fliInvHeader = new FlightInventoryHeader(Convert.ToInt32(hdfEMId.Value));
            }
            fliInvHeader.Destination = hdnOutToCity.Value;
            fliInvHeader.Origin = hdnOutExCity.Value;

            if (oneway.Checked)
            {

                fliInvHeader.SearchType = "OneWay";
            }
            else
            {

                fliInvHeader.SearchType = "Return";
            }
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            fliInvHeader.DepartureDate = Convert.ToDateTime(txtOutDepDate.Text, dateFormat);
            if (roundtrip.Checked)
            {
                fliInvHeader.ReturnDate = Convert.ToDateTime(txtInDepDate.Text, dateFormat);
            }
            fliInvHeader.Stops = 0;
            fliInvHeader.BookingPeriodFrom = Convert.ToDateTime(txtBookFromDate.Text, dateFormat);
            fliInvHeader.BookingPeriodTo = Convert.ToDateTime(txtBookToDate.Text, dateFormat);

            //*********************End :Flight_Inventory Header table details****************************************/

            //*********************Start : Inventory Details*********************************************/

            if (hdfEMId.Value == "0")//New Record Insertion Inventory Details
            {
                #region New Record Insert Mode Inventory Details

                if (!string.IsNullOrEmpty(hdnAirlineCabinFareBooking.Value))
                {
                    string[] selAirlines = hdnAirlineCabinFareBooking.Value.Split('-');
                    int tripCount = 1;
                    if (roundtrip.Checked)
                    {
                        tripCount++;
                    }

                    if (selAirlines.Length > 0)
                    {
                        for (int i = 0; i < tripCount; i++)
                        {
                            foreach (string selValue in selAirlines)
                            {

                                DataRow drAirline = fliInvHeader.DtInventoryDetails.NewRow();

                                if (i == 0)//OneWay
                                {

                                    drAirline["Origin"] = hdnOutExCity.Value;
                                    drAirline["Destination"] = hdnOutToCity.Value;
                                    drAirline["DepartureTime"] = Convert.ToDateTime(txtOutDepDate.Text + " " + txtOutDepTime.Text, dateFormat);
                                    drAirline["ArrivalTime"] = Convert.ToDateTime(txtOutArrivalDate.Text + " " + txtOutArrTime.Text, dateFormat);
                                    drAirline["FlightNo"] = txtOutFlightNo.Text;
                                    if (ddlOutTimeZone.SelectedIndex > 0)
                                    {
                                        drAirline["TimeZone"] = ddlOutTimeZone.SelectedValue.Split(')')[0].Replace("(UTC", "");
                                    }
                                    if (!string.IsNullOrEmpty(txtOutTerminal.Text))
                                    {
                                        drAirline["Terminal"] = txtOutTerminal.Text;
                                    }

                                    var start = TimeSpan.Parse(txtOutDepTime.Text);
                                    var end = TimeSpan.Parse(txtOutArrTime.Text);

                                    TimeSpan duration = new TimeSpan();
                                    duration = new TimeSpan(Math.Abs((end - start).Ticks));


                                    //TimeSpan result = end - start;

                                    drAirline["Duration"] = Convert.ToString(duration);

                                }
                                else//RoundTrip
                                {
                                    drAirline["Origin"] = hdnInExCity.Value;
                                    drAirline["Destination"] = hdnInToCity.Value;
                                    drAirline["DepartureTime"] = Convert.ToDateTime(txtInDepDate.Text + " " + txtInDepTime.Text, dateFormat);
                                    drAirline["ArrivalTime"] = Convert.ToDateTime(txtInArrivalDate.Text + " " + txtInArrTime.Text, dateFormat);
                                    drAirline["FlightNo"] = txtInFlightNo.Text;
                                    if (ddlInTimeZone.SelectedIndex > 0)
                                    {
                                        drAirline["TimeZone"] = ddlInTimeZone.SelectedValue.Split(')')[0].Replace("(UTC", "");
                                    }
                                    if (!string.IsNullOrEmpty(txtInTerminal.Text))
                                    {
                                        drAirline["Terminal"] = txtInTerminal.Text;
                                    }
                                    var start = TimeSpan.Parse(txtInDepTime.Text);
                                    var end = TimeSpan.Parse(txtInArrTime.Text);

                                    TimeSpan duration = new TimeSpan();
                                    duration = new TimeSpan(Math.Abs((end - start).Ticks));
                                    //TimeSpan result = end - start;
                                    drAirline["Duration"] = Convert.ToString(duration);

                                }
                                drAirline["CabinClass"] = selValue.Split(',')[2];
                                drAirline["AirlineCode"] = selValue.Split(',')[0];
                                if (selValue.Split(',')[1] == "Yes")
                                {
                                    fliInvHeader.IsLCC = true;
                                }
                                else
                                {
                                    fliInvHeader.IsLCC = false;
                                }
                                drAirline["BookingClass"] = selValue.Split(',')[3];
                                fliInvHeader.DtInventoryDetails.Rows.Add(drAirline);
                            }
                        }
                    }

                }
                #endregion
            }
            else//Update Inventory Details
            {
                #region Update Record Inventory Details

                if (!string.IsNullOrEmpty(hdnAirlineCabinFareBooking.Value))
                {
                    string[] selAirlines = hdnAirlineCabinFareBooking.Value.Split('-');

                    int tripCount = 1;
                    if (roundtrip.Checked)
                    {
                        tripCount++;
                    }
                    for (int i = 0; i < tripCount; i++)
                    {

                        DataRow drAirline;
                        if (i == 0)
                        {
                            drAirline = fliInvHeader.DtInventoryDetails.Rows[0];//Oneway
                        }
                        else
                        {
                            drAirline = fliInvHeader.DtInventoryDetails.Rows[1];//Return
                        }
                        foreach (string selValue in selAirlines)
                        {
                            if (i == 0)//OneWay
                            {
                                #region Oneway

                                drAirline["Origin"] = hdnOutExCity.Value;
                                drAirline["Destination"] = hdnOutToCity.Value;
                                drAirline["DepartureTime"] = Convert.ToDateTime(txtOutDepDate.Text + " " + txtOutDepTime.Text, dateFormat);
                                drAirline["ArrivalTime"] = Convert.ToDateTime(txtOutArrivalDate.Text + " " + txtOutArrTime.Text, dateFormat);
                                drAirline["FlightNo"] = txtOutFlightNo.Text;
                                if (ddlOutTimeZone.SelectedIndex > 0)
                                {
                                    drAirline["TimeZone"] = ddlOutTimeZone.SelectedValue.Split(')')[0].Replace("(UTC", "");
                                }
                                if (!string.IsNullOrEmpty(txtOutTerminal.Text))
                                {
                                    drAirline["Terminal"] = txtOutTerminal.Text;
                                }
                                else
                                {
                                    drAirline["Terminal"] = string.Empty;
                                }

                                var start = TimeSpan.Parse(txtOutDepTime.Text);
                                var end = TimeSpan.Parse(txtOutArrTime.Text);

                                TimeSpan duration = new TimeSpan();
                                duration = new TimeSpan(Math.Abs((end - start).Ticks));
                                //TimeSpan result = end - start;

                                drAirline["Duration"] = Convert.ToString(duration);
                                #endregion
                            }
                            else//RoundTrip
                            {
                                #region RoundTrip
                                drAirline["Origin"] = hdnInExCity.Value;
                                drAirline["Destination"] = hdnInToCity.Value;
                                drAirline["DepartureTime"] = Convert.ToDateTime(txtInDepDate.Text + " " + txtInDepTime.Text, dateFormat);
                                drAirline["ArrivalTime"] = Convert.ToDateTime(txtInArrivalDate.Text + " " + txtInArrTime.Text, dateFormat);
                                drAirline["FlightNo"] = txtInFlightNo.Text;
                                if (ddlInTimeZone.SelectedIndex > 0)
                                {
                                    drAirline["TimeZone"] = ddlInTimeZone.SelectedValue.Split(')')[0].Replace("(UTC", "");
                                }
                                if (!string.IsNullOrEmpty(txtInTerminal.Text))
                                {
                                    drAirline["Terminal"] = txtInTerminal.Text;
                                }
                                else
                                {
                                    drAirline["Terminal"] = string.Empty;
                                }
                                var start = TimeSpan.Parse(txtInDepTime.Text);
                                var end = TimeSpan.Parse(txtInArrTime.Text);

                                TimeSpan duration = new TimeSpan();
                                duration = new TimeSpan(Math.Abs((end - start).Ticks));

                                //TimeSpan result = end - start;
                                drAirline["Duration"] = Convert.ToString(duration);
                                #endregion

                            }
                            drAirline["CabinClass"] = selValue.Split(',')[2];
                            drAirline["AirlineCode"] = selValue.Split(',')[0];
                            if (selValue.Split(',')[1] == "Yes")
                            {
                                fliInvHeader.IsLCC = true;
                            }
                            else
                            {
                                fliInvHeader.IsLCC = false;
                            }
                            drAirline["BookingClass"] = selValue.Split(',')[3];

                        }
                    }

                }
                #endregion
            }

            //*********************End : Inventory Details*********************************************/

            //*********************Start : Fare Details*********************************************/

            if (hdfEMId.Value == "0")
            {
                #region Fare Details Insertion Mode
                //Adult Fare
                DataRow drAdultFare = fliInvHeader.DtFareDetails.NewRow();
                drAdultFare["PaxType"] = "ADT";//Adult
                drAdultFare["BaseFare"] = Convert.ToDecimal(txtAdultFare.Text);
                drAdultFare["Tax"] = Convert.ToDecimal(txtAdultTax.Text);
                drAdultFare["PublishedFare"] = Convert.ToDecimal(txtAdultFare.Text) + Convert.ToDecimal(txtAdultTax.Text);
                drAdultFare["Currency"] = "AED";
                drAdultFare["PriceType"] = (int)CT.BookingEngine.PriceType.PublishedFare;
                fliInvHeader.DtFareDetails.Rows.Add(drAdultFare);

                //Child Fare
                DataRow drChildFare = fliInvHeader.DtFareDetails.NewRow();
                drChildFare["PaxType"] = "CNN";//Child
                drChildFare["BaseFare"] = Convert.ToDecimal(txtChildFare.Text);
                drChildFare["Tax"] = Convert.ToDecimal(txtChildTax.Text);
                drChildFare["PublishedFare"] = Convert.ToDecimal(txtChildFare.Text) + Convert.ToDecimal(txtChildTax.Text);
                drChildFare["Currency"] = "AED";
                drChildFare["PriceType"] = (int)CT.BookingEngine.PriceType.PublishedFare;
                fliInvHeader.DtFareDetails.Rows.Add(drChildFare);

                //Infant Fare
                //It is not necessary to include the Infant fare details.
                //so we are checking the infant fare and tax 
                //If the infant published fare(infant fare + tax) >0
                //Then we will save the infant price details into the table.
                if (txtInfantFare.Text.Length > 0 && txtInfanttax.Text.Length > 0)
                {
                    if (Convert.ToDecimal(Convert.ToDecimal(txtInfantFare.Text) + Convert.ToDecimal(txtInfanttax.Text)) > 0)
                    {
                        DataRow drInfantFare = fliInvHeader.DtFareDetails.NewRow();
                        drInfantFare["PaxType"] = "INF"; //Infant
                        drInfantFare["BaseFare"] = Convert.ToDecimal(txtInfantFare.Text);
                        drInfantFare["Tax"] = Convert.ToDecimal(txtInfanttax.Text);
                        drInfantFare["PublishedFare"] = Convert.ToDecimal(txtInfantFare.Text) + Convert.ToDecimal(txtInfanttax.Text);
                        drInfantFare["Currency"] = "AED";
                        drInfantFare["PriceType"] = (int)CT.BookingEngine.PriceType.PublishedFare;
                        fliInvHeader.DtFareDetails.Rows.Add(drInfantFare);
                    }
                }
                #endregion
            }
            else//Update Mode
            {
                #region Fare Details Update  Mode

                int infantCount = 0;
                foreach (DataRow drFareDetails in fliInvHeader.DtFareDetails.Rows)
                {

                    //Adult Fare
                    if (Convert.ToString(drFareDetails["PaxType"]) == "ADT")
                    {
                        drFareDetails["PaxType"] = "ADT";//Adult
                        drFareDetails["BaseFare"] = Convert.ToDecimal(txtAdultFare.Text);
                        drFareDetails["Tax"] = Convert.ToDecimal(txtAdultTax.Text);
                        drFareDetails["PublishedFare"] = Convert.ToDecimal(txtAdultFare.Text) + Convert.ToDecimal(txtAdultTax.Text);
                        drFareDetails["Currency"] = "AED";
                        drFareDetails["PriceType"] = (int)CT.BookingEngine.PriceType.PublishedFare;
                    }
                    //Child Fare
                    else if (Convert.ToString(drFareDetails["PaxType"]) == "CNN")
                    {
                        drFareDetails["PaxType"] = "CNN";//Child
                        drFareDetails["BaseFare"] = Convert.ToDecimal(txtChildFare.Text);
                        drFareDetails["Tax"] = Convert.ToDecimal(txtChildTax.Text);
                        drFareDetails["PublishedFare"] = Convert.ToDecimal(txtChildFare.Text) + Convert.ToDecimal(txtChildTax.Text);
                        drFareDetails["Currency"] = "AED";
                        drFareDetails["PriceType"] = (int)CT.BookingEngine.PriceType.PublishedFare;
                    }
                    //InfantFare
                    else if (Convert.ToString(drFareDetails["PaxType"]) == "INF")
                    {
                        infantCount++;
                        if (txtInfantFare.Text.Length > 0 && txtInfanttax.Text.Length > 0)
                        {
                            drFareDetails["PaxType"] = "INF"; //Infant
                            drFareDetails["BaseFare"] = Convert.ToDecimal(txtInfantFare.Text);
                            drFareDetails["Tax"] = Convert.ToDecimal(txtInfanttax.Text);
                            drFareDetails["PublishedFare"] = Convert.ToDecimal(txtInfantFare.Text) + Convert.ToDecimal(txtInfanttax.Text);
                            drFareDetails["Currency"] = "AED";
                            drFareDetails["PriceType"] = (int)CT.BookingEngine.PriceType.PublishedFare;
                        }
                        else
                        {
                            drFareDetails["PaxType"] = "INF"; //Infant
                            drFareDetails["BaseFare"] = 0;
                            drFareDetails["Tax"] = 0;
                            drFareDetails["PublishedFare"] = 0;
                            drFareDetails["Currency"] = "AED";
                            drFareDetails["PriceType"] = (int)CT.BookingEngine.PriceType.PublishedFare;

                        }
                    }


                }

                //Infant Fare
                //It is not necessary to include the Infant fare details.
                //so we are checking the infant fare and tax 
                //If the infant published fare(infant fare + tax) >0
                //Then we will save the infant price details into the table.
                if (infantCount == 0 && fliInvHeader.DtFareDetails.Rows.Count <= 2)
                {
                    if (txtInfantFare.Text.Length > 0 && txtInfanttax.Text.Length > 0)
                    {
                        if (Convert.ToDecimal(Convert.ToDecimal(txtInfantFare.Text) + Convert.ToDecimal(txtInfanttax.Text)) > 0)
                        {
                            DataRow drInfantFare = fliInvHeader.DtFareDetails.NewRow();
                            drInfantFare["PaxType"] = "INF"; //Infant
                            drInfantFare["BaseFare"] = Convert.ToDecimal(txtInfantFare.Text);
                            drInfantFare["Tax"] = Convert.ToDecimal(txtInfanttax.Text);
                            drInfantFare["PublishedFare"] = Convert.ToDecimal(txtInfantFare.Text) + Convert.ToDecimal(txtInfanttax.Text);
                            drInfantFare["Currency"] = "AED";
                            drInfantFare["PriceType"] = (int)CT.BookingEngine.PriceType.PublishedFare;
                            fliInvHeader.DtFareDetails.Rows.Add(drInfantFare);
                        }
                    }
                }
                #endregion
            }


            //*********************End : Fare Details*********************************************/

            //*********************Start : PNR's and Seats *******************************************/

            if (hdfEMId.Value == "0")
            {
                #region PNR's and Seats Insertion mode
                if (!string.IsNullOrEmpty(hdnPNRSeats.Value))
                {
                    string[] selSeatsPNR = hdnPNRSeats.Value.Split('-');
                    if (selSeatsPNR.Length > 0)
                    {
                        foreach (string selValue in selSeatsPNR)
                        {
                            DataRow drSeatsPNR = fliInvHeader.DtInventoryQuota.NewRow();
                            drSeatsPNR["PNR"] = selValue.Split(',')[0];
                            drSeatsPNR["AvailQuota"] = selValue.Split(',')[1];
                            drSeatsPNR["UsedQuota"] = 0;//As it is a new entry always the used quota will be Zero.
                            fliInvHeader.DtInventoryQuota.Rows.Add(drSeatsPNR);
                        }
                    }
                }
                #endregion
            }
            else
            {
                #region PNR's and Seats Update mode
                if (!string.IsNullOrEmpty(hdnPNRSeats.Value))
                {
                    string[] selSeatsPNR = hdnPNRSeats.Value.Split('-');
                    if (selSeatsPNR.Length > 0)
                    {
                        int index = 0;
                        for (int i = 0; i < fliInvHeader.DtInventoryQuota.Rows.Count; i++)
                        {
                            DataRow drSeatsPNR = fliInvHeader.DtInventoryQuota.Rows[i];
                            drSeatsPNR["PNR"] = selSeatsPNR[i].Split(',')[0];
                            drSeatsPNR["AvailQuota"] = selSeatsPNR[i].Split(',')[1];
                            drSeatsPNR["UsedQuota"] = selSeatsPNR[i].Split(',')[2];
                            index++;
                        }
                        if (selSeatsPNR.Length > fliInvHeader.DtInventoryQuota.Rows.Count)
                        {
                            int newPNRLength = selSeatsPNR.Length - fliInvHeader.DtInventoryQuota.Rows.Count;
                            for (int q = 0; q < newPNRLength; q++)
                            {
                                DataRow drSeatsPNR = fliInvHeader.DtInventoryQuota.NewRow();
                                drSeatsPNR["PNR"] = selSeatsPNR[index].Split(',')[0];
                                drSeatsPNR["AvailQuota"] = selSeatsPNR[index].Split(',')[1];
                                drSeatsPNR["UsedQuota"] = 0;//As it is a new entry always the used quota will be Zero.
                                fliInvHeader.DtInventoryQuota.Rows.Add(drSeatsPNR);
                                index++;
                            }

                        }
                    }
                }
                #endregion
            }
            //*********************End : PNR's and Seats ********************************************/  

            fliInvHeader.CreatedBy = (int)Settings.LoginInfo.UserID;
            fliInvHeader.Save();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void Clear()
    {
        try
        {
            ddlOutExCountry.SelectedIndex = -1;
            ddlOutExCity.SelectedIndex = 0;

            ddlOutToCountry.SelectedIndex = -1;
            ddlOutToCity.SelectedIndex = 0;

            ddlInExCountry.SelectedIndex = -1;
            ddlInExCity.SelectedIndex = 0;

            ddlInToCountry.SelectedIndex = -1;
            ddlInToCity.SelectedIndex = 0;

            txtOutDepTime.Text = string.Empty;
            txtOutArrTime.Text = string.Empty;

            txtOutTerminal.Text = string.Empty;
            txtInTerminal.Text = string.Empty;

            txtOutFlightNo.Text = string.Empty;
            txtInFlightNo.Text = string.Empty;

            txtAdultFare.Text = string.Empty;
            txtAdultTax.Text = string.Empty;

            txtChildFare.Text = string.Empty;
            txtChildTax.Text = string.Empty;

            txtInfantFare.Text = string.Empty;
            txtInfanttax.Text = string.Empty;

            ddlInTimeZone.SelectedIndex = -1;
            ddlOutTimeZone.SelectedIndex = -1;

            txtInDepTime.Text = string.Empty;
            txtInArrTime.Text = string.Empty;

            hdfMode.Value = "0";
            hdfEMId.Value = "0";
            btnSubmit.Text = "Submit";
            BindDates();


        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #region GridEvents
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            lblSuccess.Text = string.Empty;
            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "(FlightInvMaster page)btnSearch_Click method " + ex.ToString(), "0");
        }
    }

    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.Master.HideSearch();
            Guid vsId = (Guid)gvSearch.SelectedValue;
            Edit(vsId);

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Audit.Add(EventType.Airline, Severity.High, 1, "(FlightInvMaster page)gvSearch_SelectedIndexChanged method .Error:" + ex.Message, "0");
        }
    }

    private void Edit(Guid id)
    {
        try
        {
            Clear();//Initially Clear all the control values on the page.

            //From the SearchList DataTable get the Main Inventory Id
            if (SearchList != null && SearchList.Rows.Count > 0)
            {
                DataRow dataRow = SearchList.AsEnumerable()
                 .SingleOrDefault(r => r.Field<Guid>("ID") == id);

                //Save the inventory id in a hidden field
                //Later used to update the records based on the inventory id.

                if (dataRow != null && !string.IsNullOrEmpty(Convert.ToString(dataRow["INVID"])))
                {
                    hdfEMId.Value = Convert.ToString(dataRow["INVID"]);
                }

                if (dataRow != null && Convert.ToString(dataRow["INVSTATUS"]).ToUpper() == "D")
                {
                    btnSubmit.Text = "Update";
                    btnSubmit.Visible = true;
                }
                else//If the row status is 'A' we will not allow the user to update the information.
                {
                    btnSubmit.Visible = false;
                }

               
                FlightInventoryHeader fih = new FlightInventoryHeader(Convert.ToInt32(hdfEMId.Value));
                /******************Start : Bind Inventory -- CZ_Flight_Inventory ********************/
                txtBookFromDate.Text = fih.BookingPeriodFrom.ToString("dd/MM/yyyy");
                txtBookToDate.Text = fih.BookingPeriodTo.ToString("dd/MM/yyyy");

                //As of now we are not saving travel date anywhere so assigned current date to travel dates textbox controls.
                txtTravelFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                txtTravelToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

                ddlOutExCountry.SelectedValue = City.GetCountryCodeFromCityCode(fih.Origin);
                ddlOutToCountry.SelectedValue = City.GetCountryCodeFromCityCode(fih.Destination);

                LoadCities(ddlOutExCountry.SelectedValue, ddlOutExCity);
                ddlOutExCity.SelectedValue = fih.Origin;

                LoadCities(ddlOutToCountry.SelectedValue, ddlOutToCity);
                ddlOutToCity.SelectedValue = fih.Destination;

                if (fih.SearchType.ToLower() == "oneway")
                {
                    oneway.Checked = true;
                }
                else //return trip
                {
                    roundtrip.Checked = true;
                    ddlInExCountry.SelectedValue = City.GetCountryCodeFromCityCode(fih.Destination);
                    ddlInToCountry.SelectedValue = City.GetCountryCodeFromCityCode(fih.Origin);

                    LoadCities(ddlInExCountry.SelectedValue, ddlInExCity);
                    ddlInExCity.SelectedValue = fih.Destination;

                    LoadCities(ddlInToCountry.SelectedValue, ddlInToCity);
                    ddlInToCity.SelectedValue = fih.Origin;

                }



                /***************** End:Bind Inventory **********************/

                /******************Start : Bind Inventory_Details -- CZ_Flight_Inventory_Details ********************/
                int tripCount = 1;
                if (fih.SearchType.ToLower() == "return")
                {
                    tripCount++;
                }

                if (fih.DtInventoryDetails != null && fih.DtInventoryDetails.Rows.Count > 0)
                {
                    for (int i = 0; i < tripCount; i++)
                    {
                        DataRow dr;
                        if (i == 0)
                        {
                            dr = fih.DtInventoryDetails.Rows[0];//Onward Trip Object
                        }
                        else
                        {
                            dr = fih.DtInventoryDetails.Rows[1];//Return Trip Object
                        }
                        if (i == 0) //OneWay
                        {
                            #region OneWay
                            if (dr["DepartureTime"] != DBNull.Value)
                            {
                                txtOutDepDate.Text = Convert.ToDateTime(dr["DepartureTime"]).ToString("dd/MM/yyyy");
                                txtOutDepTime.Text = Convert.ToDateTime(dr["DepartureTime"]).ToString("HH:mm");
                            }
                            if (dr["ArrivalTime"] != DBNull.Value)
                            {
                                txtOutArrivalDate.Text = Convert.ToDateTime(dr["ArrivalTime"]).ToString("dd/MM/yyyy");
                                txtOutArrTime.Text = Convert.ToDateTime(dr["ArrivalTime"]).ToString("HH:mm");
                            }
                            if (dr["FlightNo"] != DBNull.Value)
                            {
                                txtOutFlightNo.Text = Convert.ToString(dr["FlightNo"]);
                            }

                            if (dr["Terminal"] != DBNull.Value)
                            {
                                txtOutTerminal.Text = Convert.ToString(dr["Terminal"]);
                            }
                            #endregion

                        }
                        else
                        {
                            #region Return
                            if (dr["DepartureTime"] != DBNull.Value)
                            {
                                txtInDepDate.Text = Convert.ToDateTime(dr["DepartureTime"]).ToString("dd/MM/yyyy");
                                txtInDepTime.Text = Convert.ToDateTime(dr["DepartureTime"]).ToString("HH:mm");
                            }
                            if (dr["ArrivalTime"] != DBNull.Value)
                            {
                                txtInArrivalDate.Text = Convert.ToDateTime(dr["ArrivalTime"]).ToString("dd/MM/yyyy");
                                txtInArrTime.Text = Convert.ToDateTime(dr["ArrivalTime"]).ToString("HH:mm");
                            }
                            if (dr["FlightNo"] != DBNull.Value)
                            {
                                txtInFlightNo.Text = Convert.ToString(dr["FlightNo"]);
                            }

                            if (dr["Terminal"] != DBNull.Value)
                            {
                                txtInTerminal.Text = Convert.ToString(dr["Terminal"]);
                            }
                            #endregion
                        }

                    }
                }
                //For Airline,CabinClass ,ISLcc,and booking class.
                //Save these values in a hidden field.

                if (fih.DtInventoryDetails != null && fih.DtInventoryDetails.Rows.Count > 0 && fih.DtInventoryDetails.Rows[0] != null)
                {
                    DataRow dr2 = fih.DtInventoryDetails.Rows[0];
                    if (dr2 != null)
                    {
                        if (dr2["AirlineCode"] != DBNull.Value)
                        {
                            hdfEModeAirline.Value = Convert.ToString(dr2["AirlineCode"]);
                        }
                        hdfEModeAirline.Value += "," + fih.IsLCC;

                        if (dr2["CabinClass"] != DBNull.Value)
                        {
                            hdfEModeAirline.Value += "," + Convert.ToString(dr2["CabinClass"]);
                        }
                        if (dr2["BookingClass"] != DBNull.Value)
                        {
                            hdfEModeAirline.Value += "," + Convert.ToString(dr2["BookingClass"]);
                        }
                        else
                        {
                            hdfEModeAirline.Value += ",";
                        }
                    }
                }
                /***************** End:Bind Inventory_Details **********************/

                /******************Start : Bind FareDetails -- CZ_Flight_Inventory_FareDetails ********************/
                if (fih.DtFareDetails != null && fih.DtFareDetails.Rows.Count > 0)
                {
                    foreach (DataRow dr in fih.DtFareDetails.Rows)
                    {
                        if (dr["PaxType"] != DBNull.Value)
                        {
                            switch (Convert.ToString(dr["PaxType"]))
                            {
                                case "ADT": //Adult PaxType
                                    txtAdultFare.Text = Convert.ToString(dr["BaseFare"]);
                                    txtAdultTax.Text = Convert.ToString(dr["Tax"]);
                                    break;
                                case "CNN": //Child Pax Type
                                    txtChildFare.Text = Convert.ToString(dr["BaseFare"]);
                                    txtChildTax.Text = Convert.ToString(dr["Tax"]);
                                    break;
                                case "INF": //Infant Pax Type
                                    txtInfantFare.Text = Convert.ToString(dr["BaseFare"]);
                                    txtInfanttax.Text = Convert.ToString(dr["Tax"]);
                                    break;

                            }
                        }
                    }
                }

                /***************** End:Bind FareDetails **********************/

                /******************Start : Bind Inventory_Quota  -- CZ_Flight_Inventory_Quota ***/

                if (fih.DtInventoryQuota != null && fih.DtInventoryQuota.Rows.Count > 0)
                {
                    //save all the pnr's in a hiiden field and then iterate through the loop on the client side.
                    hdfEModePNR.Value = string.Empty;
                    foreach (DataRow dr in fih.DtInventoryQuota.Rows)
                    {
                        if (dr["PNR"] != DBNull.Value && dr["AvailQuota"] != DBNull.Value)
                        {
                            if (hdfEModePNR.Value.Length > 0)
                            {
                                hdfEModePNR.Value += "-" + Convert.ToString(dr["PNR"]) + "," + Convert.ToString(dr["AvailQuota"]) + "," + Convert.ToString(dr["UsedQuota"]);
                            }
                            else
                            {
                                hdfEModePNR.Value = Convert.ToString(dr["PNR"]) + "," + Convert.ToString(dr["AvailQuota"]) + "," + Convert.ToString(dr["UsedQuota"]);
                            }
                        }
                    }
                }
                /***************** End:Bind Inventory_Quota **********************/

                hdfMode.Value = "1";
                Utility.StartupScript(this.Page, "bindParams();", "SCRIPT");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void bindSearch()
    {
        try
        {
            DataTable dt = FlightInventoryHeader.GetFlightInventoryDetails("A");
            if (dt != null && dt.Rows.Count > 0)
            {

                SearchList = dt;
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvSearch, dt);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(FlightInvMasterPage)gvSearch_PageIndexChanging method .Error:" + ex.ToString(), "0");

        }

    }

    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string[,] textboxesNColumns ={

                                         {"HTtxtORIGIN", "ORIGIN" }
                                        ,{"HTtxtDESTINATION", "DESTINATION" } 
                                        ,{"HTtxtSEARCHTYPE", "SEARCHTYPE" }
                                        ,{"HTtxtDEPDATE", "DEPDATE"}

                                        ,{"HTtxtRETDATE", "RETDATE" }
                                        ,{"HTtxtBKEPERIODFROM", "BKEPERIODFROM" } 
                                        ,{"HTtxtBKEPERIODTO", "BKEPERIODTO" }
                                        ,{"HTtxtISLCC", "ISLCC"}

                                        
                                        ,{"HTtxtAIRLINE", "AIRLINE" }
                                        ,{"HTtxtARRIVALTIME", "ARRIVALTIME" } 
                                        ,{"HTtxtDEPTTIME", "DEPTTIME" }
                                        ,{"HTtxtCABINCLASS", "CABINCLASS"}
                                         ,{"HTtxtFLIGHTNO", "FLIGHTNO"}

                                        //,{"HTtxtPAXTYPE", "PAXTYPE" }
                                        //,{"HTtxtBASEFARE", "BASEFARE" } 
                                        //,{"HTtxtTAX", "TAX" }
                                        //,{"HTtxtPUBLISHEDFARE", "PUBLISHEDFARE"}

                                        ,{"HTtxtPNR", "PNR" }
                                        ,{"HTtxtAVAILQUOTA", "AVAILQUOTA" } 
                                        ,{"HTtxtUSEDQUOTA", "USEDQUOTA" }
                                        
                                         };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[FLIGHTINV_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ID"] };
            Session[FLIGHTINV_SEARCH_SESSION] = value;
        }
    }

    protected void ITlnkStatus_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvRow = (GridViewRow)(((LinkButton)sender).NamingContainer);
            HiddenField hdfStatus = (HiddenField)gvRow.FindControl("IThdfStatus");
            HiddenField hdfInvId = (HiddenField)gvRow.FindControl("IThdfInvId");
            if (hdfStatus != null && hdfInvId != null && hdfStatus.Value.Length > 0 && hdfInvId.Value.Length > 0)
            {  
                if (hdfStatus.Value.ToUpper() == "A")
                {
                    FlightInventoryHeader.UpdateFlightInventoryRowStatus(Convert.ToInt32(hdfInvId.Value), (int)Settings.LoginInfo.UserID, "D");
                }
                else
                {
                    FlightInventoryHeader.UpdateFlightInventoryRowStatus(Convert.ToInt32(hdfInvId.Value), (int)Settings.LoginInfo.UserID, "A");
                }
                bindSearch();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(FlightInvMaster page)ITlnkStatus_Click method " + ex.ToString(), "0");
        }
    }

    protected void gvSearch_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                LinkButton ITlnkStatus = (System.Web.UI.WebControls.LinkButton)e.Row.FindControl("ITlnkStatus");
                if (drv.Row["INVSTATUS"] != DBNull.Value && Convert.ToString(drv.Row["INVSTATUS"]).ToUpper() == "A")
                {
                    ITlnkStatus.Text = "DEACTIVATE";
                    ITlnkStatus.Attributes.Add("onclick", "javascript:return confirm('Do you want to Deactivate?');");
                }
                else
                {
                    ITlnkStatus.Text = "ACTIVATE";
                    ITlnkStatus.Attributes.Add("onclick", "javascript:return confirm('Do you want to Activate?');");
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(FlightInvMaster page)gvSearch_RowDataBound event error raised " + ex.ToString(), "0");
        }
    }



    #endregion



}
