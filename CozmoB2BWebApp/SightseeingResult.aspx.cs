﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections.Generic;
//Required UD Namespace
using CT.Core;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using System.Linq;
using CT.TicketReceipt.Common;

public partial class SightseeingResult : CT.Core.ParentPage //System.Web.UI.Page
{
    PagedDataSource pagedData = new PagedDataSource();
    protected SightSeeingReguest request = new SightSeeingReguest();
    List<SightSeeingReguest> sortResult = new List<SightSeeingReguest>();
    protected List<string> sightSeeingType;
    protected string filterBySightSeeingName = string.Empty;
    protected string category = string.Empty;
    protected SightseeingSearchResult[] totalResultList = new SightseeingSearchResult[0];
    protected int decimalValue = 2;
    protected SightseeingSearchResult[] filteredResultList;
    protected SightseeingSearchResult[] filterTotalResults;
    protected string errorMessage = string.Empty;
    protected bool ErrorFlag = false;
    protected int noOfPages;
    protected AgentMaster agency;
    protected int recordsPerPage = 10;
    protected int filteredCount;
    //protected decimal minPrice = 0, maxPrice = 0;
    protected string show = string.Empty;
    protected string resultIds = string.Empty;
    int currentPageNo = 1;
    protected int maxResult, minResult;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["ssReq"] == null || Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            else
            {
                
                Page.Title = "Sightseeing Search Results";
                request = (SightSeeingReguest)Session["ssReq"];
                if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                }
                else
                {
                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                }
                if (!IsPostBack)
                {

                    hdnPriceMin.Value = "";
                    hdnPriceMax.Value = "";
                    SearchSightseeing();
                    

                    
                }
                else
                {
                    filteredResultList = (SightseeingSearchResult[])Session["filteredResultList"];
                    totalResultList = Session["searchResult"] as SightseeingSearchResult[];
                    if (Change.Value == "Price")
                    {
                        SortByPrice();
                        Change.Value = "";
                    }
                    if (hdnFilter.Value.Length > 0)
                    {
                        if (hdnFilter.Value == "ClearFilter")//If Clear Filter is clicked
                        {
                            ClearFilter();
                            hdnFilter.Value = "";
                        }
                       
                    }
                    if (Change.Value == "true")
                    {
                        if (PageNoString.Value.Length > 0)
                        {
                            currentPageNo = Convert.ToInt16(PageNoString.Value);
                            if (Session["FilteredAirResults"] != null)
                            {
                                totalResultList = Session["FilteredAirResults"] as SightseeingSearchResult[];
                            }
                            else
                            {
                                totalResultList = Session["searchResult"] as SightseeingSearchResult[];
                            }
                            filteredResultList = DoPaging(totalResultList);
                            dlSightSeeing.DataSource = filteredResultList;
                            dlSightSeeing.DataBind();
                            Change.Value = "";
                        }
                    }
                    


                }
               

            }

        
            
        }
        catch (BookingEngineException ex)
        {
            string url = Request.ServerVariables["HTTP_REFERER"].ToString();
            errorMessage = "Error: There is some error. Please " + "<a href=\"" + url + "\">" + "Retry" + "</a>.";
            ErrorFlag = true;
            throw ex;
        }
        catch (Exception excep)
        {
            errorMessage = "Error: " + excep.Message;
            ErrorFlag = true;
            Audit.Add(EventType.HotelSearch, Severity.High, (int)Settings.LoginInfo.UserID, excep.ToString(), "0");
        }
    }
    protected void SearchSightseeing()
    {
        try
        {
            if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agency = new AgentMaster(Settings.LoginInfo.AgentId);
            }
            else
            {
                agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
            }
            if (Session["searchResult"] == null)
            {
                CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
                //Vat Implementation
                request.LoginCountryCode = Settings.LoginInfo.LocationCountryCode;
                try
                {
                    request.CountryCode = Country.GetCountryCodeFromCountryName(request.CountryName);
                }
                catch { }
                
                mse.SettingsLoginInfo = Settings.LoginInfo;
                if (mse.SettingsLoginInfo.IsOnBehalfOfAgent)
                    totalResultList = mse.GetSightseeingResults(request, Convert.ToInt64(Settings.LoginInfo.OnBehalfAgentID));
                else
                    totalResultList = mse.GetSightseeingResults(request, Convert.ToInt64(Settings.LoginInfo.AgentId));


                Session["cSessionID"] = mse.SessionId;
            }
            else
            {
                totalResultList = Session["searchResult"] as SightseeingSearchResult[];
            }

            Session["searchResult"] = totalResultList;
            Session["totalResultList"] = totalResultList;
            
            if (totalResultList!=null && totalResultList.Length > 0)
            {
                hdnMinValue.Value=Math.Ceiling(totalResultList[0].DayWisetourOperationList[0].tourOperationList[0].PriceInfo.NetFare + totalResultList[0].DayWisetourOperationList[0].tourOperationList[0].PriceInfo.Markup).ToString("N" + agency.DecimalValue).Replace(",", "");
                hdnMaxValue.Value = Math.Ceiling(totalResultList[totalResultList.Length - 1].DayWisetourOperationList[0].tourOperationList[0].PriceInfo.NetFare + totalResultList[totalResultList.Length - 1].DayWisetourOperationList[0].tourOperationList[0].PriceInfo.Markup).ToString("N" + agency.DecimalValue).Replace(",", "");
            }
           
            hdnPriceMin.Value = hdnMinValue.Value;
            hdnPriceMax.Value = hdnMaxValue.Value;
           
            filteredResultList = DoPaging(totalResultList);
            Session["filteredResultList"] = filteredResultList;
            dlSightSeeing.DataSource = filteredResultList;
            dlSightSeeing.DataBind();
            if (totalResultList != null)
            {
                resultCount.Value = totalResultList.Length.ToString();
               
            }
            else
            {
                resultCount.Value = "0";
            }

            
            LoadSightSeeingType(filteredResultList);
            

        }
        catch { }
    }

    private void LoadSightSeeingType(SightseeingSearchResult[] totalResultList)
    {
        try
        {
           
            for (int i = 0; i < totalResultList.Length; i++)
            {
                if (totalResultList[i].SightseeingTypeList != null)
                {
                    for (int j = 0; j < totalResultList[i].SightseeingTypeList.Count; j++)
                    {
                        foreach (ListItem item in chkList.Items)
                        {
                            if (!item.Selected)
                            {
                                if (totalResultList[i].SightseeingTypeList.ContainsKey(item.Value))
                                {
                                    item.Selected = true;
                                    item.Enabled = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void dlSightSeeing_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            SightseeingSearchResult resultObj = e.Item.DataItem as SightseeingSearchResult;
            Image img = e.Item.FindControl("Image1") as Image;
            Label lblCountry = e.Item.FindControl("lblCountry") as Label;
            Label lblCategory = e.Item.FindControl("lblCategory") as Label;
            Label lblAmount = e.Item.FindControl("lblAmount") as Label;
            HtmlTable tblTourList = e.Item.FindControl("tblTourList") as HtmlTable;
            if (resultObj != null)
            {
                
                img.ImageUrl = img.ImageUrl.Split('|')[0];
                lblCountry.Text = request.CountryName;
                string category = string.Empty;
                Dictionary<string, string> sightseeingList=resultObj.SightseeingCategoryList;
                foreach(KeyValuePair<string,string> categoryList in sightseeingList)
                {
                
                    if (!string.IsNullOrEmpty(category))
                    {
                        category += "," + categoryList.Value;
                    }
                    else
                    {
                        category = categoryList.Value;
                    }
                }
                lblCategory.Text = category;

                for (int j = 0; j < resultObj.DayWisetourOperationList[0].tourOperationList.Count; j++)
                {
                    //Use ceiling instead of Round, Changed on 05082016
                    decimal totalAmount = Math.Ceiling(resultObj.DayWisetourOperationList[0].tourOperationList[j].PriceInfo.NetFare + resultObj.DayWisetourOperationList[0].tourOperationList[j].PriceInfo.Markup);
                    lblAmount.Text = totalAmount.ToString("N"+ agency.DecimalValue);
                    break;
                }

                #region LanguageList
                for (int k = 0; k < resultObj.DayWisetourOperationList[0].tourOperationList.Count; k++)
                {
                    //LanguageName
                    HtmlTableRow tr = new HtmlTableRow();
                    HtmlTableCell tc = new HtmlTableCell();
                    if (resultObj.DayWisetourOperationList[0].tourOperationList[k].LangName != null && resultObj.DayWisetourOperationList[0].tourOperationList[k].LangName.Count > 0)
                   
                    {
                        bool disLanguage = false;
                        for (int i = 0; i < resultObj.DayWisetourOperationList[0].tourOperationList[k].LangName.Count; i++)
                        {
                            if (resultObj.DayWisetourOperationList[0].tourOperationList[k].LangName[i] == request.Language)
                            {
                                tc.InnerText = request.Language;
                                disLanguage = true;
                            }
                        }
                        if (!disLanguage)
                        {
                            tc.InnerText = resultObj.DayWisetourOperationList[0].tourOperationList[k].LangName[0];
                        }
                    }
                    else if (resultObj.DayWisetourOperationList[0].tourOperationList[k].LangName != null && resultObj.DayWisetourOperationList[0].tourOperationList[k].LangName.Count == 1)
                    {
                        tc.InnerText = resultObj.DayWisetourOperationList[0].tourOperationList[k].LangName[0];
                    }
                    tc.Width = "15%";

                    //SpecialName
                    HtmlTableCell tc1 = new HtmlTableCell();

                    if (!string.IsNullOrEmpty(resultObj.DayWisetourOperationList[0].tourOperationList[k].SpecialItemName))
                    {
                        tc1.InnerText = resultObj.DayWisetourOperationList[0].tourOperationList[k].SpecialItemName;
                    }
                    else
                    {
                        tc1.InnerText = "N/A";
                    }                    
                    tc1.Width = "17%";
                    tc1.Height = "25";

                    //Duration
                    HtmlTableCell tc2 = new HtmlTableCell();
                    //string data = "";
                    //foreach (KeyValuePair<string, string> item in resultObj.TourOperationList[k].ConfirmationCodeList)
                    //{
                    //    data = item.Value;
                    //}
                    //tc2.InnerText = resultObj.Duration + " " + data;
                    if (!string.IsNullOrEmpty(resultObj.Duration))
                    {
                        tc2.InnerText = resultObj.Duration; 
                    }
                    else
                    {
                        tc2.InnerText = "N/A";
                    }
                    tc2.Width = "25%";

                    //TotalAmount
                    HtmlTableCell tc3 = new HtmlTableCell();
                    //Use ceiling instead of Round, Changed on 05082016
                    decimal totalAmount = Math.Ceiling(resultObj.DayWisetourOperationList[0].tourOperationList[k].PriceInfo.NetFare + resultObj.DayWisetourOperationList[0].tourOperationList[k].PriceInfo.Markup);
                    tc3.InnerText = agency.AgentCurrency + " " + totalAmount.ToString("N"+ agency.DecimalValue);
                    tc3.Width = "15%";

                    //CancelLink
                    HtmlTableCell tc4 = new HtmlTableCell();
                    LinkButton lnkCancel = new LinkButton();
                    lnkCancel.ID = "Can" + resultObj.ItemCode + "" + k;
                    lnkCancel.Text = "CancelPolicy";
                    tc4.Width = "12%";
                    lnkCancel.Attributes.Add("OnClick", "GetCancelInfo('" + resultObj.Source + "','" + resultObj.ItemCode + "','" + k + "');return false;"); //@@@@  resultObj.ItemCode using instead of index 
                    tc4.Controls.Add(lnkCancel);

                    //Book Now
                    HtmlTableCell tc5 = new HtmlTableCell();
                    LinkButton lnkBookNow = new LinkButton();
                    lnkBookNow.ID = resultObj.ItemCode + "-" + k;
                    lnkBookNow.Text = "<span>Book Now</span>";
                   

                    lnkBookNow.PostBackUrl = "SightseeingGuestDetails.aspx?itemCode=" + resultObj.ItemCode + "&tourId=" + k; //@@@@  resultObj.ItemCode using instead of index 
                    lnkBookNow.CssClass = "book-now-button";
                    tc5.Width = "16%";
                    tc5.Controls.Add(lnkBookNow);

                    tr.Cells.Add(tc);
                    tr.Cells.Add(tc1);
                    tr.Cells.Add(tc2);
                    tr.Cells.Add(tc3);
                    tr.Cells.Add(tc4);
                    tr.Cells.Add(tc5);
                    tblTourList.Rows.Add(tr);
                }
                #endregion
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.SightseeingSearch, Severity.High, 0, "SightSeeing Search Result Page Error message :" + ex.ToString() + ex.StackTrace, "");
        }
    }
    private void SortByPrice()
    {
        try
        {
           
            List<SightseeingSearchResult> results = new List<SightseeingSearchResult>(0);
            for (int i = 0; i < totalResultList.Length; i++)
            {
                if (Math.Ceiling(totalResultList[i].DayWisetourOperationList[0].tourOperationList[0].PriceInfo.NetFare) >= Convert.ToDecimal(hdnPriceMin.Value) && Math.Ceiling(totalResultList[i].DayWisetourOperationList[0].tourOperationList[0].PriceInfo.NetFare) <= Convert.ToDecimal(hdnPriceMax.Value))
                {
                    results.Add(totalResultList[i]);
                }
            }
            filterTotalResults = results.ToArray();
            Session["FilteredAirResults"] = filterTotalResults;
            filteredResultList = DoPaging(filterTotalResults);
            dlSightSeeing.DataSource = filteredResultList;
            dlSightSeeing.DataBind();
            }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.ToString(), "0");
        }
    }
    

   

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        
        try
        {
            List<SightseeingSearchResult> filteredResults = new List<SightseeingSearchResult>();
            totalResultList = (SightseeingSearchResult[])Session["totalResultList"];
            setFliter();
            if (totalResultList.Length > 0)
            {
                for (int i = 0; i < totalResultList.Length; i++)
                {
                    SightseeingSearchResult result = totalResultList[i];
                   // Dictionary<string, string> sightseeingCatgList = totalResultList[i].SightseeingCategoryList;
                  
                    if (sightSeeingType.Count > 0 && category.Length > 0 && filterBySightSeeingName.Length > 0 )
                    {
                        bool flag = false;
                        for (int k = 0; k < totalResultList[i].SightseeingCategoryList.Count; k++ )
                        {
                            if (totalResultList[i].SightseeingCategoryList.ContainsKey(category) && totalResultList[i].ItemName.ToLower().Contains(filterBySightSeeingName.Trim().ToLower()))
                            {

                                flag = true;
                                break;
                            }
                        }
                        if (flag)
                        {
                            for (int k = 0; k < totalResultList[i].SightseeingTypeList.Count; k++)
                            {
                                foreach (KeyValuePair<string, string> typelist in totalResultList[i].SightseeingTypeList)
                                {
                                    if (sightSeeingType.Contains(typelist.Key))
                                    {
                                        filteredResults.Add(totalResultList[i]);
                                        break;
                                    }
                                }
                            }
                           
                        }
                    }

                    else if (filterBySightSeeingName.Length >0 && sightSeeingType.Count > 0)
                    {
                        bool flag=false;
                        for (int k = 0; k < totalResultList[i].SightseeingTypeList.Count; k++)
                        {
                           
                            foreach (KeyValuePair<string, string> typelist in totalResultList[i].SightseeingTypeList)
                            {
                                if (sightSeeingType.Contains(typelist.Key) && totalResultList[i].ItemName.ToLower().Contains(filterBySightSeeingName.Trim().ToLower()))
                                {
                                    flag = true;
                                    break;
                                }
                               
                            }
                            if (flag)
                            {

                                filteredResults.Add(totalResultList[i]);
                                break;
                            }
                        }

                    }
                    else if (filterBySightSeeingName.Length > 0 && filterBySightSeeingName != null && category.Length > 0)
                    {
                        for (int k = 0; k < totalResultList[i].SightseeingCategoryList.Count; k++)
                        {
                            if (totalResultList[i].SightseeingCategoryList.ContainsKey(category) && totalResultList[i].ItemName.ToLower().Contains(filterBySightSeeingName.Trim().ToLower()))
                            {

                                filteredResults.Add(totalResultList[i]);
                                break;
                            }
                        }
                    }
                    else if (category.Length > 0 && sightSeeingType.Count > 0)
                    {
                        bool flag = false;
                        for (int k = 0; k < totalResultList[i].SightseeingCategoryList.Count; k++)
                        {
                            if (totalResultList[i].SightseeingCategoryList.ContainsKey(category))
                            {

                                flag = true;
                                break;
                            }
                        }
                        if (flag)
                        {
                            for (int k = 0; k < totalResultList[i].SightseeingTypeList.Count; k++)
                            {
                                foreach (KeyValuePair<string, string> typelist in totalResultList[i].SightseeingTypeList)
                                {
                                    if (sightSeeingType.Contains(typelist.Key))
                                    {
                                        filteredResults.Add(totalResultList[i]);
                                        break;
                                    }
                                }
                            }
                        }

                    }


                    else if (sightSeeingType.Count > 0)
                    {
                        foreach (string type in sightSeeingType)
                        {
                            if (totalResultList[i].SightseeingTypeList.ContainsKey(type))
                            {

                                filteredResults.Add(totalResultList[i]);
                                break;
                            }
                        }

                    }
                    else if (category.Length > 0)
                    {
                        
                        if (totalResultList[i].SightseeingCategoryList.ContainsKey(category))
                        {
                            filteredResults.Add(totalResultList[i]);
                            break;
                        }
                    }
                    else if (filterBySightSeeingName.Length > 0)
                    {

                        if (totalResultList[i].ItemName.ToLower().Contains(filterBySightSeeingName.Trim().ToLower()))
                        {
                            filteredResults.Add(totalResultList[i]);
                            break;
                        }

                    }

                    else
                    {
                        filteredResults.Add(totalResultList[i]);
                    }

                }
            }
            filteredResultList = filteredResults.ToArray();
            Session["filteredResultList"] = filteredResultList;
            filteredResults.Clear();
            CurrentPage = 0;
            DoPaging(filteredResultList);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.SightseeingSearch, Severity.High, 0, "search event method in SightSeeingResult Page Error message :" + ex.ToString() + ex.StackTrace, "");
        }
    }
    void setFliter()
    {
        try
        {
            filterBySightSeeingName = txtSight.Text.Trim();
            //Selected SightSeeingType Loading
            string starRating = string.Empty;
            sightSeeingType = new List<string>();
            for (int i = 0; i < chkList.Items.Count; i++)
            {
                if (chkList.Items[i].Selected)
                {
                    sightSeeingType.Add(chkList.Items[i].Value);
                }
            }
            if (ddlCategory.SelectedIndex > 0)
            {
                category = ddlCategory.SelectedItem.Value;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    

    protected void ClearFilter()
    {
        try
        {
            CurrentPage = 0;
            hdnPriceMin.Value = hdnMinValue.Value;
            hdnPriceMax.Value = hdnMaxValue.Value;
            SightseeingSearchResult[] tempResult = null;
            if (Session["searchResult"] != null)
            {
                tempResult = Session["searchResult"] as SightseeingSearchResult[];
            }
            else
            {
                Response.Redirect("Sightseeing.aspx", true);
            }
            totalResultList = (SightseeingSearchResult[])Session["totalResultList"];
            filteredResultList = totalResultList;
            Session["filteredResultList"] = filteredResultList;
            Session["FilteredAirResults"] = tempResult;
            LoadSightSeeingType(tempResult);
            ddlCategory.SelectedIndex = 0;
            txtSight.Text = string.Empty;
            filteredResultList= DoPaging(tempResult);
            dlSightSeeing.DataSource = filteredResultList;
            dlSightSeeing.DataBind();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.SightseeingSearch, Severity.High, 0, "Clear method SightSeeingResult Page Error message :" + ex.ToString() + ex.StackTrace, "");
        }

    }

    #region paging
    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }

   private  SightseeingSearchResult[] DoPaging (SightseeingSearchResult[] results)
    {

        try
        {
            if (results != null)
            {
                filteredCount = results.Length;
                resultIds = string.Empty;
                int numberOfPages = 0;
                int resultsPerPage = 50;
                if (results.Length % resultsPerPage > 0)
                {
                    numberOfPages = (results.Length / resultsPerPage) + 1;
                }
                else
                {
                    numberOfPages = results.Length / resultsPerPage;
                }
                //Showing first page by default (while filtering)
                if (currentPageNo == 0 || currentPageNo > numberOfPages)
                {
                    currentPageNo = 1;
                }
                string urlForPaging = string.Empty;
                show = BookingUtility.PagingJavascript(numberOfPages, urlForPaging, currentPageNo);
                List<SightseeingSearchResult> searchResults = new List<SightseeingSearchResult>();
                if (results.Length > 0)
                {
                    if ((currentPageNo * resultsPerPage) <= results.Length)
                    {
                        for (int i = (currentPageNo * resultsPerPage) - resultsPerPage; i < (currentPageNo * resultsPerPage); i++)
                        {
                            if (i < results.Length)
                            {
                                searchResults.Add(results[i]);
                            }
                        }
                    }
                    else
                    {
                        for (int i = (currentPageNo * resultsPerPage) - resultsPerPage; i < (currentPageNo * resultsPerPage); i++)
                        {
                            if (i < results.Length)
                            {
                                searchResults.Add(results[i]);
                            }
                        }
                    }
                }
                filteredResultList = new SightseeingSearchResult[searchResults.Count];

                for (int i = 0; i < searchResults.Count; i++)
                {
                    filteredResultList[i] = searchResults[i];
                }

                maxResult = filteredResultList.Length * currentPageNo;
                if (currentPageNo * resultsPerPage < maxResult)
                {
                    maxResult = currentPageNo * resultsPerPage;//filteredResults.Length;
                }

                minResult = ((currentPageNo * resultsPerPage) - resultsPerPage);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return filteredResultList;
    }
    //protected void btnPrev_Click(object sender, EventArgs e)
    //{
    //    //CurrentPage--;
    //    //doPaging();
    //}

    //protected void btnNext_Click(object sender, EventArgs e)
    //{
    //    //CurrentPage++;
    //    ////BookingUtility.PagingJavascript(2, "", CurrentPage);
    //    //doPaging();
        
    //}


    //protected void btnFirst_Click(object sender, EventArgs e)
    //{
    //    //CurrentPage = 0;
    //    //doPaging();
    //}

    //protected void btnLast_Click(object sender, EventArgs e)
    //{
    //    //CurrentPage = (int)Session["count"];
    //    //doPaging();
    //}
    #endregion

    # region GridBindMethods
    protected object restrictLength(object value)
    {
        string description = string.Empty;
        if (value != null)
        {
            string txt = value.ToString();
            if (txt.Length > 125)
            {
                txt = txt.Substring(0, 125) + "....";
            }
            description = txt;
        }
        return description;
    }

    # endregion


}
