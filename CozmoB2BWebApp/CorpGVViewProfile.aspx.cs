﻿using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Web.UI.WebControls;
using CT.GlobalVisa;
using CT.TicketReceipt.Common;

public partial class CorpGVViewProfileGUI : CT.Core.ParentPage
{
    protected GlobalVisaSession visaSession = new GlobalVisaSession();
    protected CorporateProfile cropProfileDetails;
    protected string rootFolder = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["ProfileImage"]);
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (Settings.LoginInfo == null) //Checking Logininfo
            {
                Response.Redirect("AbandonSession.aspx", false);
                return;
            }
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                if (Session["GVSession"] != null) //Checking Session
                {
                    visaSession = Session["GVSession"] as GlobalVisaSession;
                    if (visaSession != null && visaSession.CorpProfileDetails != null)
                    {
                        cropProfileDetails = visaSession.CorpProfileDetails as CorporateProfile;
                    }
                    else
                    {
                        Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Gv CorpProfile Details is Null", Request["REMOTE_ADDR"]);
                        Response.Redirect("AbandonSession.aspx", false);
                    }
                }
                else
                {
                    Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Global Visa Session is NUll", Request["REMOTE_ADDR"]);
                    Response.Redirect("AbandonSession.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGVRequest page load exception reson: " + ex.ToString(), Request["REMOTE_ADDR"]);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
}