﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.Core;

public partial class CorporateExpenseDashboard : CT.Core.ParentPage
{
    public new TransactionVisaTitle Master
    {
        get
        {
            return ((TransactionVisaTitle)(base.Master));
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
    }
}
