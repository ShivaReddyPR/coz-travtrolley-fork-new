﻿using CT.AccountingEngine;
using CT.BookingEngine;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace CozmoB2BWebApp
{
    public partial class ApiGuestDetails : CT.Core.ParentPage//System.Web.UI.Page
    {
        protected  HotelRequest request = null;
        protected   HotelItinerary itinerary = new HotelItinerary();
        protected  HotelSearchResult result = new HotelSearchResult();
        protected  string roomTypeCode = string.Empty;
        protected  HotelRoomsDetails[] RoomDetails = new HotelRoomsDetails[0];
        protected  decimal outPutVat = 0m;
        protected  decimal  discount = 0;
        protected  int agencyId = 0;
        protected string warningMsg = "";
        protected int decimalValue = 2;
        protected  decimal rateOfExchange = 1;
        protected CorporateProfile corpProfile;
        protected string trid = "";

        protected void Page_Load(object sender, EventArgs e)
            {
            hdnTokenId.Value = JsonConvert.SerializeObject(Session["ApiTokenId"]);

            var pageParams = GenericStatic.GetSetPageParams("ApiGuestDetails", "", "get").Replace("||","~~").Split('|');
            pageParams = pageParams.Select(x => x.Replace("~~", "||")).ToArray();
            trid = pageParams[2];

            if (!IsPostBack)
            {
                if (Session["req"] == null || Settings.LoginInfo == null || Session["ApiHotelResult"] == null)
                {
                    Response.Redirect("AbandonSession.aspx", true);
                }
                else
                {
                    int agencyId = 0;
                    Dictionary<string, string> diLoginInfo = new Dictionary<string, string>();
                    if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        agencyId = Settings.LoginInfo.AgentId;
                        diLoginInfo.Add("Decimal", Convert.ToString(Settings.LoginInfo.DecimalValue));
                        diLoginInfo.Add("OnBehalfAgentLocation", "0");
                        diLoginInfo.Add("phoneno", Settings.LoginInfo.AgentPhone);
                        diLoginInfo.Add("email", Settings.LoginInfo.AgentEmail);
                    }
                    else
                    {
                        agencyId = Settings.LoginInfo.OnBehalfAgentID; //Selected Agency
                        diLoginInfo.Add("Decimal", Convert.ToString(Settings.LoginInfo.OnBehalfAgentDecimalValue));
                        diLoginInfo.Add("OnBehalfAgentLocation", Convert.ToString(Settings.LoginInfo.OnBehalfAgentLocation)); 
                        AgentMaster  behalfAgency = new AgentMaster(agencyId);
                        diLoginInfo.Add("phoneno", behalfAgency.Phone1);
                        diLoginInfo.Add("email", behalfAgency.Email1);
                    }
                    diLoginInfo.Add("AgentId", Convert.ToString(agencyId));
                    diLoginInfo.Add("MemberType", Settings.LoginInfo.MemberType.ToString());
                    //diLoginInfo.Add("SessionId", Request.QueryString["sessionId"]);
                    diLoginInfo.Add("SessionId", Convert.ToString(pageParams[1]));
                    diLoginInfo.Add("UserId", Convert.ToString(Settings.LoginInfo.UserID));
                    diLoginInfo.Add("AgentType", Convert.ToString(Settings.LoginInfo.AgentType));


                    request = Session["req"] as HotelRequest;
                    request.Corptravelreason = string.IsNullOrEmpty(request.Corptravelreason) ? string.Empty : request.Corptravelreason.Split('~')[0];
                    request.Corptraveler = string.IsNullOrEmpty(request.Corptraveler) ? string.Empty : request.Corptraveler.Split('~')[0];
                    
                    result = Session["ApiHotelResult"] as HotelSearchResult;
                    //roomTypeCode = Request.QueryString["RoomType"];
                    roomTypeCode = Convert.ToString(pageParams[0]);
                    
                        List<HotelRoomsDetails> rmdetails = new List<HotelRoomsDetails>();
                    if (result.RoomDetails[0].SupplierId != "23") { 
                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            for (int j = 0; j < result.RoomDetails.Length; j++)
                            {
                                if (result.RoomDetails[j].IsIndividualSelection)
                                {
                                    if (roomTypeCode.Split('^').Length>=i+1 && result.RoomDetails[j].RoomTypeCode == (roomTypeCode.Split('^')[i]))
                                    { 
                                        rmdetails.Add(result.RoomDetails[j]);
                                        break;
                                    }
                                 }          
                                else
                                {
                                    if (result.RoomDetails[j].CommonRoomKey == roomTypeCode && result.RoomDetails[j].SequenceNo==Convert.ToString(i+1))
                                    { 
                                        rmdetails.Add(result.RoomDetails[j]);
                                        break;
                                    }
                                }
                            }
                        }
                    RoomDetails =rmdetails.ToArray();                                  
                    }
                    else
                    {
                        RoomDetails = result.RoomDetails.Where(r => r.RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[1] == roomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[1]).ToArray();
                    }
                    /* To reset the no of room objects for UAPI hotel */
                    //int Rooms = Request.QueryString["NOR"] != null ? Convert.ToInt32(Request.QueryString["NOR"]) : 0;    
                    int Rooms = pageParams[3] != "" ? Convert.ToInt32(pageParams[3]) : 0;    
                    if (Rooms > 0 && RoomDetails.Length != Rooms)
                    {
                        request.NoOfRooms = Rooms;
                        int TotalPax = request.RoomGuest[0].noOfAdults;
                        request.RoomGuest[0].noOfAdults = 1;
                        request.RoomGuest = Enumerable.Repeat(request.RoomGuest[0], Rooms).ToArray();
                        RoomDetails = Rooms > 0 ? Enumerable.Repeat(RoomDetails[0], Rooms).ToArray() : RoomDetails;
                        request.RoomGuest[Rooms - 1].noOfAdults = request.RoomGuest[Rooms - 1].noOfAdults + (TotalPax - Rooms);
                        result.RoomGuest = request.RoomGuest;
                    }
                    
                    Session["RoomDetails"] = RoomDetails;
                    hdnObjRequest.Value = JsonConvert.SerializeObject(request);
                    hdnHotelDetails.Value = JsonConvert.SerializeObject(result);                    
                    hdnSelectedRoomTypes.Value = JsonConvert.SerializeObject(RoomDetails);
                    hdnLoginInfo.Value = JsonConvert.SerializeObject(diLoginInfo);

                    AgentMaster clsAgentMaster = new AgentMaster(agencyId);

                    if (clsAgentMaster.RequiredFlexFields || Settings.LoginInfo.IsCorporate == "Y")
                    {
                        corpProfile = string.IsNullOrEmpty(request.Corptraveler) ? null :
                            new CorporateProfile(Convert.ToInt32(request.Corptraveler), agencyId); 
                        
                        DataTable dtFlex = corpProfile != null && corpProfile.DtProfileFlex != null && corpProfile.DtProfileFlex.Select("FlexProductId = '1'").Length > 0 ?
                            corpProfile.DtProfileFlex.Select("FlexProductId = '1'").CopyToDataTable() : CorporateProfile.GetAgentFlexDetailsByProduct(agencyId, 1);//Loading Flight Flex details
                        hdnFlexInfo.Value = dtFlex != null && dtFlex.Rows.Count > 0 ? JsonConvert.SerializeObject(dtFlex) : string.Empty;                        
                        hdnCorpProfInfo.Value = corpProfile != null ? JsonConvert.SerializeObject(corpProfile) : string.Empty;
                        hdnFlexConfig.Value = Settings.LoginInfo.IsCorporate == "Y" ? Convert.ToString(ConfigurationManager.AppSettings["TRAVEL_REASON_FLEX_PAIR"]) : string.Empty;
                    }

                    LoadHotelItinerary(request,RoomDetails, result);
		// If corporate , then assigning policy details
                      if(Settings.LoginInfo.IsCorporate=="Y")
                    {  //Assign the Hotel Policy created
                        itinerary.HotelPolicy= GenerateHotelPolicy(RoomDetails,result.HotelName);
                    }
                    
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        void LoadHotelItinerary(HotelRequest request,HotelRoomsDetails[] hotelRoomsDetails,HotelSearchResult result)
        {
             decimal markup = 0, total = 0 ;
            if (Session["req"] == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            else
            {
                SimilarHotelSourceInfo sourceInfo = result.SimilarHotels.Where(x => x.Source.ToString() == hotelRoomsDetails[0].SupplierName).FirstOrDefault();
                rateOfExchange = (Settings.LoginInfo.ExchangeRate > 0 ? Settings.LoginInfo.ExchangeRate : 1);
                Dictionary<string, string> Countries = null;
                DOTWCountry dotw = new DOTWCountry();
                Countries = dotw.GetAllCountries(); 
                itinerary.CityCode = request.CityCode;
                string nationality= (!string.IsNullOrEmpty(request.PassengerNationality)&& Countries.ContainsKey(request.PassengerNationality)) ? Countries[request.PassengerNationality]:string.Empty;
                itinerary.PassengerNationality =(hotelRoomsDetails.Any(x=>x.SupplierName==HotelBookingSource.RezLive.ToString()))?Country.GetCountryCodeFromCountryName(nationality) :nationality;
                itinerary.PassengerCountryOfResidence = nationality;                   
                itinerary.AgencyId = agencyId;
                itinerary.EndDate = result.EndDate;
                itinerary.StartDate = result.StartDate;
                itinerary.HotelCode = sourceInfo.HotelCode; ;
                itinerary.HotelAddress1 = result.HotelAddress;
                itinerary.HotelAddress2 = result.HotelContactNo;
                itinerary.HotelName = result.HotelName;
                itinerary.NoOfRooms = request.NoOfRooms;
                itinerary.Rating = result.Rating;
                itinerary.CityRef = request.CityName;
                itinerary.Map = result.HotelMap;
                itinerary.CityCode = sourceInfo.CityCode;
                if(result.HotelPolicyDetails != "" && result.HotelPolicyDetails != null)
                {
                   itinerary.HotelPolicyDetails = result.HotelPolicyDetails;
                }
                else { 
                itinerary.HotelPolicyDetails = string.Empty;
                    }
                itinerary.Remarks = string.Empty;
                itinerary.SpecialRequest = string.Empty;
                itinerary.VoucherStatus = true; //always set to true, otherwise View Invoice button on booking queue will not be displayed
                                                /////////////////// PropertyType Used For TBO Hotel to send while booking ahotel
                                                //added on 02032016
                                                //Agoda Also We are Using This properity 
                itinerary.PropertyType = result.PropertyType; //PropertyType store value of Hotel alert message for miki
                                                              //update request city code for the selected booking source
                request.CityCode = result.CityCode;
                itinerary.Source = (HotelBookingSource)Enum.Parse(typeof(HotelBookingSource), hotelRoomsDetails[0].SupplierName, true);           //result.BookingSource;
                itinerary.LastCancellationDate = request.StartDate;
                //////////////////////////////////////////////////////////////////
                //    Used for LOH only to send while booking a Hotel.
                itinerary.SequenceNumber = result.SequenceNumber;
                //////////////////////////////////////////////////////////////////
                itinerary.LocationId = Convert.ToInt32(Settings.LoginInfo.LocationID);
                //if (Settings.LoginInfo.AgentId == (int)Session["BookingAgencyID"])
                if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    itinerary.AgencyId = Settings.LoginInfo.AgentId;
                }
                else
                {
                    itinerary.AgencyId = Settings.LoginInfo.OnBehalfAgentID;  // on behalf of booking passing location id as -2
                    itinerary.LocationId = Settings.LoginInfo.OnBehalfAgentLocation;
                }

                itinerary.IsDomestic = false;
                itinerary.DestinationCountryCode =!string.IsNullOrEmpty(request.CountryName)? Country.GetCountryCodeFromCountryName(request.CountryName):"";
                itinerary.LoginCountryCode = request.LoginCountryCode;
                //setting room info into itinerary
                HotelRoom[] roomInfo = new HotelRoom[itinerary.NoOfRooms];
                
                    roomInfo = LoadRoomDetails(itinerary, hotelRoomsDetails,request,result);               
                itinerary.Roomtype = roomInfo;
                if (itinerary.Source == HotelBookingSource.HotelConnect || itinerary.Source == HotelBookingSource.HotelExtranet)
                {
                    List<string> Rooms = new List<string>();

                    foreach (HotelRoom room in itinerary.Roomtype)
                    {
                        Rooms.Add(room.RoomTypeCode);
                    }
                    CZInventory.SearchEngine se = null;
                    CZInventory.CZHISSearchEngine czse = null;
                    Dictionary<string, string> CancellationData = new Dictionary<string, string>();
                    if (itinerary.Source == HotelBookingSource.HotelConnect)
                    {
                        se = new CZInventory.SearchEngine();
                        CancellationData = se.GetCancellationPolicy(ref result, request, Rooms);
                    }
                    else
                    {
                        czse = new CZInventory.CZHISSearchEngine();                    
                        CancellationData = czse.GetCancellationPolicy(ref result, request, Rooms);
                    }
                    foreach (KeyValuePair<string, string> pair in CancellationData)
                    {
                        switch (pair.Key)
                        {
                            case "lastCancellationDate":
                                itinerary.LastCancellationDate = Convert.ToDateTime(pair.Value);
                                break;
                            case "CancelPolicy":
                                itinerary.HotelCancelPolicy = pair.Value;
                                string[] policies = itinerary.HotelCancelPolicy.Split('|');
                                if (policies.Length == RoomDetails.Length)
                                {
                                    for (int i = 0; i < policies.Length; i++)
                                    {
                                        RoomDetails[i].CancellationPolicy = policies[i];
                                    }
                                    hdnSelectedRoomTypes.Value = JsonConvert.SerializeObject(RoomDetails);
                                }
                                break;
                            case "HotelPolicy":
                                itinerary.HotelPolicyDetails = pair.Value;
                                break;
                        }
                    }
                }
                // itinerary.TotalPrice = Math.Round(result.SellingFare, 2, MidpointRounding.AwayFromZero);

                itinerary.TotalPrice = 0;
                //foreach (HotelRoom room in itinerary.Roomtype)
                //{
                //    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                //    {
                //        room.Price.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                //    }
                //    else
                //    {
                //        room.Price.DecimalPoint = Settings.LoginInfo.DecimalValue;
                //    }
                //}
                itinerary.Currency = result.Currency;
                //CALL MSE METHODS TO GET CANCELLATION DETAILS & HOTEL DETAILS

                try
                {
                   
                    if (itinerary.Source != HotelBookingSource.HotelConnect && itinerary.Source != HotelBookingSource.HotelExtranet)
                    {
                        foreach (HotelRoom room in itinerary.Roomtype)
                        {
                            if (room.NonRefundable || room.PaymentMode == "CC" || room.RatePlanCode == "")
                            {
                                warningMsg = "This Hotel cannot be booked. Please search again to book another Hotel.";
                                //hdnWarning.Value = "This Hotel cannot be booked. Please search again to book another Hotel.";
                                //imgContinue.Text = "Search";
                            }
                        }
                    }

                    {
                        itinerary.HotelAddress1 = result.HotelAddress;
                    }
                    if (!string.IsNullOrEmpty(result.HotelContactNo))
                    {
                        itinerary.HotelAddress2 = result.HotelContactNo;
                    }



                        decimal totMarkup = 0, totalHtAmount = 0, discount = 0;
                        foreach (HotelRoom room in itinerary.Roomtype)
                        {
                            itinerary.TotalPrice += room.Price.NetFare;
                            total += Math.Round((room.Price.NetFare + room.Price.Markup), decimalValue);
                            totMarkup += room.Price.Markup;
                            discount += room.Price.Discount;
                            if (Settings.LoginInfo.IsOnBehalfOfAgent)
                            {
                                room.Price.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                            }
                            else
                            {
                                room.Price.DecimalPoint = Settings.LoginInfo.DecimalValue;
                            }

                        }
                        markup += Math.Ceiling(total);
                        //foreach (HotelRoom room in itinerary.Roomtype)
                        //{

                        //}
                        LocationMaster locationMaster = null;
                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            locationMaster = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                        }
                        if (Settings.LoginInfo.IsOnBehalfOfAgent && locationMaster.CountryCode == "IN")
                        {
                            foreach (HotelRoom room in itinerary.Roomtype) //Room Wsie Gst Calculations
                            {
                                List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                                decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, (room.Price.Markup), Settings.LoginInfo.OnBehalfAgentLocation);
                                room.Price.GSTDetailList = new List<GSTTaxDetail>();
                                room.Price.GSTDetailList = gstTaxList;
                                outPutVat += Math.Round(gstAmount, Settings.LoginInfo.OnBehalfAgentDecimalValue);
                                room.Price.OutputVATAmount = Math.Round(gstAmount, Settings.LoginInfo.OnBehalfAgentDecimalValue);
                            }
                        }
                        else if (!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.LocationCountryCode == "IN")
                        {
                            foreach (HotelRoom room in itinerary.Roomtype) //Room Wsie Gst Calculations
                            {
                                List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                                decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, (room.Price.Markup), Settings.LoginInfo.LocationID);
                                room.Price.GSTDetailList = new List<GSTTaxDetail>();
                                room.Price.GSTDetailList = gstTaxList;
                                outPutVat += Math.Round(gstAmount, Settings.LoginInfo.DecimalValue);
                                room.Price.OutputVATAmount = Math.Round(gstAmount, Settings.LoginInfo.DecimalValue);
                            }
                        }
                        else
                        {
                            //Out put Vat Calucation
                            PriceTaxDetails taxDetails = itinerary.Roomtype[0].Price.TaxDetails;
                            if (Settings.LoginInfo.AgentType == AgentType.Agent)
                            {
                                totalHtAmount = Math.Ceiling(markup); //here markup means total with Pagelevel markup
                                totMarkup = Math.Round((totMarkup), Settings.LoginInfo.DecimalValue);
                            }
                            else
                            {
                                totalHtAmount = Math.Round(Math.Ceiling(total), Settings.LoginInfo.DecimalValue);
                                totMarkup = Math.Round(totMarkup, Settings.LoginInfo.DecimalValue);
                            }
                            if (taxDetails != null && taxDetails.OutputVAT != null)
                            {
                                outPutVat = taxDetails.OutputVAT.CalculateVatAmount(totalHtAmount - discount, totMarkup, Settings.LoginInfo.DecimalValue);
                                foreach (HotelRoom room in itinerary.Roomtype)
                                {
                                    if (room.Price.AccPriceType == PriceType.NetFare)
                                    {
                                        room.Price.OutputVATAmount = Math.Round(outPutVat / itinerary.Roomtype.Length, Settings.LoginInfo.DecimalValue);
                                    }
                                }
                            }
                        }
                        //itinerary.TotalPrice = itinerary.TotalPrice + outPutVat;
                        itinerary.Currency = result.Currency;

                    if (!string.IsNullOrEmpty(request.Corptraveler) && Convert.ToInt32(request.Corptraveler) != 0)
                    {
                        itinerary.HotelPolicy = new FlightPolicy();
                        itinerary.HotelPolicy.TravelReasonId = Convert.ToInt32(request.Corptravelreason);
                        itinerary.HotelPolicy.ProfileId = Convert.ToInt32(request.Corptraveler);
                        itinerary.HotelPolicy.TravelDate = request.StartDate;
                        itinerary.HotelPolicy.ProfileGrade = corpProfile.Grade;
                    }
                }
                catch (Exception ex)
                {
                    //errorMessage = ex.Message;
                }
                // for hotel check-in check-out information - End

                itinerary.URLocator = request.URLocator;
                itinerary.BookUserIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];
                Session["hItinerary"] = itinerary;
                //for GetPaymentPreference Method Params

                if (itinerary.Source != HotelBookingSource.HotelConnect && itinerary.Source != HotelBookingSource.RezLive && itinerary.Source != HotelBookingSource.Illusions&& itinerary.Source != HotelBookingSource.HotelExtranet)
                    hdnPaymentPrefParams.Value = JsonConvert.SerializeObject(itinerary.HotelCode+ "," + itinerary.Roomtype[0].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[1]  + "," + itinerary.PropertyType);
                hdnItinerary.Value = JsonConvert.SerializeObject(itinerary);
            }
        }








        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string Createitinerary(string data,string addlMarkup,string remarks,string specialRequest,string voucherStatus)
        {

            HotelItinerary itineraryStatic = new HotelItinerary();
            itineraryStatic = (HotelItinerary)HttpContext.Current.Session["hItinerary"];
            HotelRequest request=(HotelRequest)HttpContext.Current.Session["req"];
            try
            {
                Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();

                string JsonString = string.Empty;
                int agencyId = 0;
                AgentMaster agency = new AgentMaster();
                if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    agencyId = Settings.LoginInfo.AgentId;
                }
                else
                {
                    agencyId = Settings.LoginInfo.OnBehalfAgentID; //Selected Agency
                }

                agency = new AgentMaster(agencyId);
                
                agency.UpdateBalance(0);
                if (agencyId == Settings.LoginInfo.AgentId)
                {
                    Settings.LoginInfo.AgentBalance = agency.CurrentBalance;
                }
             decimal amountToCompare = agency.CurrentBalance;

                decimal hotelRate = 0;
                decimal outputVat = 0; //Here Vat Also We are doing Celling
                for (int k = 0; k < itineraryStatic.Roomtype.Length; k++)
                {
                    hotelRate += itineraryStatic.Roomtype[k].Price.GetAgentPrice();
                    outputVat += itineraryStatic.Roomtype[k].Price.OutputVATAmount;
                }
                hotelRate = Math.Ceiling(hotelRate) + Math.Ceiling(outputVat);
                //Credit should be checking Agentbalance (no need to check if agent is corporate)
                if (hotelRate > amountToCompare && (Settings.LoginInfo.IsCorporate != "Y"))
                {
                   //   "Dear Agent you do not have sufficient balance to book a room for this hotel.";
                    JsonString = JsonConvert.SerializeObject("In Sufficient Balance");
                   
                }

                else
                {





                //int decimalValue = 2;
                HotelRoom[] hotelRooms = JsonConvert.DeserializeObject<HotelRoom[]>(data);
                    Dictionary<string, string> Countries = null;
                    DOTWCountry dotw = new DOTWCountry();
                    Countries = dotw.GetAllCountries();
                    itineraryStatic.HotelPassenger = hotelRooms[0].PassenegerInfo.FirstOrDefault();
                    for (int i=0;i< itineraryStatic.NoOfRooms;i++)
                {

                        itineraryStatic.Roomtype[i].PassenegerInfo = hotelRooms[i].PassenegerInfo;
                        if(itineraryStatic.Roomtype[i].PassenegerInfo.Count()>0)
                        {
                            for (int j=0;j< itineraryStatic.Roomtype[i].PassenegerInfo.Count();j++)
                            {
                                itineraryStatic.Roomtype[i].PassenegerInfo[j].Nationality = itineraryStatic.PassengerNationality;
                                itineraryStatic.Roomtype[i].PassenegerInfo[j].Country = itineraryStatic.PassengerCountryOfResidence;
                                itineraryStatic.Roomtype[i].PassenegerInfo[j].NationalityCode= Countries.ContainsKey(request.PassengerNationality) ? Country.GetCountryCodeFromCountryName(Countries[request.PassengerNationality]) : string.Empty;
                            }
                           
                        }                        
                    }
                    itineraryStatic.Roomtype[0].Price.AsvAmount = Convert.ToDecimal(addlMarkup);
                    itineraryStatic.SpecialRequest = specialRequest;
                    itineraryStatic.Remarks = remarks;

                    itineraryStatic.ProductType = ProductType.Hotel;
                    itineraryStatic.VoucherStatus = voucherStatus =="true";
                Product prod = itineraryStatic;
                //Product prod = new Product();
                prod.ProductId = itineraryStatic.ProductId;
                prod.ProductType = itineraryStatic.ProductType;
                prod.ProductTypeId = itineraryStatic.ProductTypeId;

                if (itineraryStatic.ProductType != ProductType.Hotel)
                {
                        itineraryStatic.ProductType = ProductType.Hotel;
                }
                    itineraryStatic.HotelCategory = "";
                    itineraryStatic.VatDescription = "";
                    itineraryStatic.AgencyReference = Convert.ToString(Settings.LoginInfo.UserID);
                    itineraryStatic.CancelId = "";
                    //itinerary.HotelAddress2 = "";
                    itineraryStatic.CreatedOn = DateTime.Now;
                    itineraryStatic.CreatedBy = (int)Settings.LoginInfo.UserID;

                    itineraryStatic.PaymentMode = agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Credit_Limit ? ModeOfPayment.CreditLimit : ModeOfPayment.Credit;

                    //itinerary.Roomtype = hotelRooms;                            
                    JsonString = JsonConvert.SerializeObject(prod);
                }
                return JsonString;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        [WebMethod]
        [ScriptMethod]
        public static string CreateInvoice(string bookingResponse, string hotelItinerary)
            {
            string JsonString = string.Empty;
           // agency = new AgentMaster(Settings.LoginInfo.AgentId);
            //int agencyId = Settings.LoginInfo.AgentId;
            //agency.UpdateBalance(0);
            
               // Settings.LoginInfo.AgentBalance = agency.CurrentBalance;

           //decimal amountToCompare = agency.CurrentBalance;
            BookingResponse bookRes = JsonConvert.DeserializeObject<BookingResponse>(bookingResponse);
            HotelItinerary itinerary = JsonConvert.DeserializeObject<HotelItinerary>(hotelItinerary);
            if (bookRes.Status != BookingResponseStatus.Successful)
            {
                string bookingMsg = "";
                //Load hotel booking messages
                try
                {
                    string filePath = ConfigurationManager.AppSettings["configFiles"] + "HotelBookingMsg.txt";
                    System.IO.StreamReader sr = new StreamReader(filePath);
                    bookingMsg = sr.ReadToEnd();
                    bookRes.Error = bookingMsg;
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to read Hotel Booking Messages: " + ex.ToString(), string.Empty);
                }
                string[] msgs = bookingMsg.Split('|');
                //PaymentMultiView.ActiveViewIndex = 1;
                if (bookRes.Error != null && bookRes.Error.Length > 0)
                {
                    foreach (string msg in msgs)
                    {
                        if (bookRes.Error.Contains("Block") && msg.Split('=')[0] == "Block")
                        {
                            JsonString = "error||"+msg.Split('=')[1];
                            break;
                        }
                        else if (bookRes.Error.Contains("Failed") && msg.Split('=')[0] == "Failed")
                        {
                            JsonString = "error||" + msg.Split('=')[1];
                            break;
                        }
                        else if (bookRes.Error.Contains("Error") && msg.Split('=')[0] == "Error")
                        {
                            JsonString = "error||" + msg.Split('=')[1];
                            break;
                        }
                        else
                        {
                            JsonString = "error||" + bookRes.Error;
                        }
                    }

                }
                else
                {
                    //Show failed message
                   // lblError.Text = msgs[1].Split('=')[1];
                }
               // Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloader");
                //PaymentMultiView.ActiveViewIndex = 1;
            }
            else
            {
                HttpContext.Current.Session["hItinerary"] = itinerary;
                HttpContext.Current.Session["BookingResponse"] = bookRes;

                //SendHotelVoucherEmail();
                AgentMaster agency = new AgentMaster();
                Dictionary<int, HotelItinerary> UserBookings = new Dictionary<int, HotelItinerary>();
                UserBookings.Remove((int)Settings.LoginInfo.UserID);

                //if (itinerary.Source != HotelBookingSource.HotelConnect)
                {
                    //itinerary = prod as HotelItinerary;
                    //Load or save the invoice
                    int invoiceNumber = Invoice.isInvoiceGenerated(itinerary.Roomtype[0].RoomId, ProductType.Hotel);
                    Invoice invoice = new Invoice();
                    if (invoiceNumber > 0)
                    {
                        invoice.Load(invoiceNumber);
                    }
                    else
                    {
                        decimal rateOfExchange =  (Settings.LoginInfo.ExchangeRate > 0 ? Settings.LoginInfo.ExchangeRate : 1);
                        invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(itinerary.HotelId, string.Empty, (int)Settings.LoginInfo.UserID, ProductType.Hotel, rateOfExchange);
                        invoice.Load(invoiceNumber);
                        invoice.Status = InvoiceStatus.Paid;
                        invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                        invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                        invoice.UpdateInvoice();

                        //SendHotelInvoiceEmail(invoiceNumber);// commented invoice mail by ziya
                    }
                    try
                    {

                        //Reduce Agent Balance
                        decimal total = 0, discount = 0, outVatAmount = 0;
                        foreach (HotelRoom room in itinerary.Roomtype)
                        {
                            ///room.Price.pri
                            if (room.Price.AccPriceType == PriceType.NetFare)
                            {
                                total += (room.Price.NetFare + room.Price.Markup);
                                outVatAmount += room.Price.OutputVATAmount;
                            }
                            else
                            {
                                total += (room.Price.PublishedFare);
                            }
                            total += Settings.LoginInfo.IsOnBehalfOfAgent ? room.Price.AsvAmount : 0;
                            discount = room.Price.Discount;
                        }
                        //No need to deduct from agent balance as it is addl markup only. We are not adding markup to total
                        //so no need to deduct addl markup from total now. 
                        //total = Math.Ceiling(total) - Convert.ToDecimal(Session["Markup"]);
                        total -= discount;
                        total = Math.Ceiling(total) + Math.Ceiling(outVatAmount); //VAT And Total Amount Both are Celling

                        if (bookRes.Status == BookingResponseStatus.Successful && (itinerary.PaymentMode== ModeOfPayment.Credit || itinerary.PaymentMode == ModeOfPayment.CreditLimit)) //Credit mode checking and deducting the agent balance
                        {
                            //Update agent balance, reduce the hotel booking amount from the balance.
                            if (Settings.LoginInfo.IsOnBehalfOfAgent)
                            {
                                agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                                agency.CreatedBy = Settings.LoginInfo.UserID;
                                
                            }
                            
                            else {
                                int agencyId = 0;
                                if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                                {
                                    agencyId = Settings.LoginInfo.AgentId;
                                }
                                else
                                {
                                    agencyId = Settings.LoginInfo.OnBehalfAgentID; //Selected Agency
                                }
                                agency = new AgentMaster(agencyId);
                                agency.CreatedBy = Settings.LoginInfo.UserID;                               

                                if (agencyId == Settings.LoginInfo.AgentId)
                                {
                                    Settings.LoginInfo.AgentBalance -= total;
                                }
                            }
                            agency.UpdateBalance(-total);

                        }
                        else if (itinerary.PaymentMode == ModeOfPayment.CreditCard)  //card mode checking and updateing Card info
                        {
                            CreditCardPaymentInformation payment = new CreditCardPaymentInformation();
                            payment.PaymentStatus = 1; //here 1 means Suceess
                            payment.ReferenceId = bookRes.BookingId;
                            //payment.PaymentId = paymentid;
                           // payment.PaymentInformationId = (int)Session["PaymentInformationId"];
                            payment.Remarks = "Successful";
                            payment.UpdatePaymentDetails();
                            //Session["PaymentInformationId"] = null;
                        }
                       
                            if (bookRes.ConfirmationNo != null)
                        {
                            JsonString = JsonConvert.SerializeObject(bookRes.ConfirmationNo.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries)[0]);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.GetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to update Agent Balance. Error: " + ex.ToString(), string.Empty);
                    }
                }
                //Response.Redirect("HotelPaymentVoucher.aspx?ConfNo=" + bookRes.ConfirmationNo, false);
            }


            return JsonString;
        }





        public static HotelRoom[] LoadRoomDetails(HotelItinerary itinerary,HotelRoomsDetails[] RoomDetails,HotelRequest request,HotelSearchResult result)
        {
           // itinerary.HotelCancelPolicy = string.Empty;
            //HotelRoomsDetails[] RoomDetails=  result.RoomDetails.Where(r => r.RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[1] == roomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[1]).ToArray();
            List<HotelRoom> rooms = new List<HotelRoom>();
            for (int i=0;i< RoomDetails.Length;i++)
            {
                HotelRoom hotelRoom = new HotelRoom();
                hotelRoom.RoomName = RoomDetails[i].RoomTypeName;
                hotelRoom.MealPlanDesc = RoomDetails[i].mealPlanDesc;
                hotelRoom.RatePlanCode= RoomDetails[i].RatePlanCode;
                hotelRoom.Ameneties = RoomDetails[i].Amenities;
                hotelRoom.RoomTypeCode = RoomDetails[i].RoomTypeCode;
                //Cancel Policy Updates
                hotelRoom.CancelPolicy = RoomDetails[i].CancelPolicyList;
                hotelRoom.AdultCount = request.RoomGuest[i].noOfAdults;
                hotelRoom.ChildCount = request.RoomGuest[i].noOfChild;
                hotelRoom.ChildAge = request.RoomGuest[i].childAge;
                if (string.IsNullOrEmpty(itinerary.HotelCancelPolicy))
                {
                    itinerary.HotelCancelPolicy = RoomDetails[i].CancellationPolicy;
                }
                else
                {
                    itinerary.HotelCancelPolicy += "|" + RoomDetails[i].CancellationPolicy;
                }   
                if (!string.IsNullOrEmpty(RoomDetails[i].EssentialInformation))
                { 
                if(string.IsNullOrEmpty(itinerary.HotelPolicyDetails))
                {
                    itinerary.HotelPolicyDetails = RoomDetails[i].EssentialInformation;
                }
                else
                {
                    itinerary.HotelPolicyDetails += "|"+RoomDetails[i].EssentialInformation;
                }
                 }
                itinerary.HotelEssentialBytes = GenericStatic.GetByteArrayWithObject(itinerary.HotelPolicyDetails);
                itinerary.HotelPolicyDetails = string.Empty;


                hotelRoom.NoOfUnits = "1";//for each room there will be only one unit
                hotelRoom.RateType = RoomRateType.DOTW;// for failed serialization



                hotelRoom.Price = new PriceAccounts();
                decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
               
                    HotelRoomFareBreakDown[] fareInfo;
                fareInfo = new HotelRoomFareBreakDown[RoomDetails[i].Rates.Length];
                for (int k = 0; k < RoomDetails[i].Rates.Length; k++)
                {
                    HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                    fare.Date = result.RoomDetails[i].Rates[k].Days;
                    //if (itinerary.Source != HotelBookingSource.RezLive)
                    if (itinerary.Source != HotelBookingSource.RezLive && itinerary.Source != HotelBookingSource.Illusions)
                    {
                        fare.RoomPrice = result.RoomDetails[i].Rates[k].SellingFare;
                    }

                    fareInfo[k] = fare;
                }
                hotelRoom.RoomFareBreakDown = fareInfo;
                grossFare += RoomDetails[i].SellingFare;
                        totalExtraGuestCharge += RoomDetails[i].SellExtraGuestCharges;
                    hotelRoom.Price.Discount = RoomDetails[i].Discount;
                    hotelRoom.Price.Markup = RoomDetails[i].Markup;
                    hotelRoom.Price.MarkupValue = RoomDetails[i].MarkupValue;
                    hotelRoom.Price.MarkupType = RoomDetails[i].MarkupType;
                    hotelRoom.Price.DiscountValue = RoomDetails[i].DiscountValue;
                    hotelRoom.Price.DiscountType = RoomDetails[i].DiscountType;
                    hotelRoom.Price.InputVATAmount = RoomDetails[i].InputVATAmount;
                    hotelRoom.Price.TaxDetails = RoomDetails[i].TaxDetail;
                        if (RoomDetails[i].supplierPrice > 0)
                        {
                        hotelRoom.Price.SupplierPrice = RoomDetails[i].supplierPrice;
                        }
                        else
                        {

                    hotelRoom.Price.SupplierPrice = (itinerary.Source!=HotelBookingSource.HotelConnect)? RoomDetails[i].TotalPrice / result.Price.RateOfExchange: (RoomDetails[i].TotalPrice-RoomDetails[i].Markup) / result.Price.RateOfExchange;
                        }
                    hotelRoom.Price.SupplierCurrency = result.Price.SupplierCurrency;
                    hotelRoom.Price.RateOfExchange = result.Price.RateOfExchange;
                   
                    totalChildCharge = RoomDetails[i].ChildCharges;
                    totalTax = RoomDetails[i].TotalTax;


                hotelRoom.Price.NetFare = grossFare + totalExtraGuestCharge + totalChildCharge;
                hotelRoom.Price.Tax = totalTax;
                PriceType priceType = PriceType.NetFare;
                hotelRoom.Price.AccPriceType = priceType;

                hotelRoom.Price.CurrencyCode = result.Currency;

                hotelRoom.Price.CurrencyCode = result.Currency;
                hotelRoom.Price.Currency = result.Currency;

                hotelRoom.Price.PublishedFare = RoomDetails[i].PublishedFare;
                hotelRoom.Price.MarketingFee = RoomDetails[i].MarketingFee;
                hotelRoom.Price.MFDiscountPercent = RoomDetails[i].MFDiscountPercent;
                hotelRoom.Price.MFDiscountAmount = RoomDetails[i].MFDiscountAmount;

                if (RoomDetails[i].SupplierName == HotelBookingSource.GIMMONIX.ToString())
                {
                    hotelRoom.Gxsupplier = RoomDetails[i].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[3];
                    if (string.IsNullOrEmpty(itinerary.Gxsupplier))
                    {
                        itinerary.Gxsupplier = "GX-" + RoomDetails[i].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[3];
                    }
                    else
                    {
                        itinerary.Gxsupplier += "|" + RoomDetails[i].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[3];
                    }
                }
                hotelRoom.GuaranteeTypeCode = RoomDetails[i].GuaranteeTypeCode;// For UAH
                hotelRoom.RatePlanCode = RoomDetails[i].RatePlanCode;
                hotelRoom.TaxBreakUps = RoomDetails[i].TaxBreakups;
                hotelRoom.RatePlanId = RoomDetails[i].RatePlanId;
                hotelRoom.AllocType = RoomDetails[i].AllocationType;
                hotelRoom.SellType = RoomDetails[i].SellType;
                hotelRoom.RqId = RoomDetails[i].RqId;
                rooms.Add(hotelRoom);
                //itinerary.Roomtype[i].RoomName = RoomDetails[i].RoomTypeName;
                //itinerary.Roomtype[i].MealPlanDesc = RoomDetails[i].mealPlanDesc;
                //itinerary.Roomtype[i].RatePlanCode = RoomDetails[i].RatePlanCode;
                //itinerary.Roomtype[i].Ameneties = RoomDetails[i].Amenities;
                //itinerary.Roomtype[i].RoomTypeCode = RoomDetails[i].RoomTypeCode;
                
            }
            return rooms.ToArray();
        }

        /// <summary>
        /// Generate THREE Hotel policies for saving the policy criteria.
        /// </summary>
        protected FlightPolicy GenerateHotelPolicy( HotelRoomsDetails[] RoomDetails,string hotelname)
        {
            FlightPolicy policy = null;
            int policyReasonId = 0;
            //if (Request["trid"] != null)//Policy Reason Id entered by the user
            //{
            //    policyReasonId = Convert.ToInt32(Request["trid"]);
            //}
            if (trid != null && trid != "")//Policy Reason Id entered by the user
            {
                policyReasonId = Convert.ToInt32(trid);
            }
            try
            {
                string policyBreakingRules = string.Empty;
                if (RoomDetails[0].TravelPolicyResult != null) { 
                foreach (KeyValuePair<string, List<string>> pair in RoomDetails[0].TravelPolicyResult.PolicyBreakingRules)
                {
                    foreach (string rule in pair.Value)
                    {
                        if (policyBreakingRules.Length > 0)
                        {
                            policyBreakingRules += "," + rule;
                        }
                        else
                        {
                            policyBreakingRules = rule;
                        }
                    }
                }
                }
                string flightNumbers = string.Empty;

               string RoomNames= string.Join("|", RoomDetails.Select(x => x.RoomTypeName));
               
                policy = new FlightPolicy();
                policy.ProductId =(int) ProductType.Hotel;
                policy.IsUnderPolicy = RoomDetails[0].TravelPolicyResult!=null? RoomDetails[0].TravelPolicyResult.IsUnderPolicy:true;
                policy.PolicyBreakingRules =!string.IsNullOrEmpty (policyBreakingRules) ? policyBreakingRules : "Inside Policy" ;
                policy.PolicyReasonId = policyReasonId;
                policy.ProfileGrade = Settings.LoginInfo.CorporateProfileGrade;
                //policy.ProfileId = Settings.LoginInfo.CorporateProfileId;
                if(request!=null)
                {
                    policy.ProfileId = Convert.ToInt32(request.Corptraveler.Split('~')[0]);
                }
                policy.Status = "A";
                policy.TravelReasonId =Convert.ToInt32( request.Corptravelreason.Split('~')[0]);
                policy.TravelDate = request.StartDate;
                policy.FlightNumbers = hotelname +"-"+ RoomNames;
                policy.SelectedTrip = true;
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High,Convert.ToInt32(Settings.LoginInfo.UserID), "exception at GenerateHotelPolicy(),reason is:" + ex.ToString(), "");
            }

            return policy;
        }

        /// <summary>
        /// To log exception to audit and send email
        /// </summary>
        /// <param name="sException"></param>
        /// <param name="sEvent"></param>
        [WebMethod]        
        public static void LogError(string sException, string sEvent)
        {            
            try
            {
                sEvent += " For Agent" + Settings.LoginInfo.AgentName;
                GenericStatic.LogAuditSendMail(sException, sEvent, Settings.LoginInfo.UserID);                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Exception at LogError,reason is:" + ex.ToString(), "");                
            }
        }
    }
}
