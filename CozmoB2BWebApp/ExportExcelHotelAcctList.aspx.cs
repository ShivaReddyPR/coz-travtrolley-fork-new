﻿using System;
using System.Data;
using System.Web.UI;
using System.IO;

public partial class ExportExcelHotelAcctListGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            dgHotelAcctReportList.DataSource = Session["HotelAcctReport"] as DataTable;
            dgHotelAcctReportList.DataBind();
            string attachment = "attachment; filename=HotelAcctReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            dgHotelAcctReportList.AllowPaging = false;
            dgHotelAcctReportList.DataSource = Session["HotelAcctReport"] as DataTable;
            dgHotelAcctReportList.DataBind();
            Form.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
