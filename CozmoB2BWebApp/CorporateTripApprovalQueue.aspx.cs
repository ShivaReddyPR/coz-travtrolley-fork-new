﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.Web.UI.Controls;
using System.Collections.Generic;
using CT.TicketReceipt.Common;

public partial class CorporateTripApprovalQueue : CT.Core.ParentPage
{
    protected int userId;
    protected int userCorpProfileId;
    private string REIM_SEARCH_SESSION_PENDING = "_ReimSearchListPending";
    private string REIM_HOTEL_SEARCH_SESSION_PENDING = "_ReimHotelSearchListPending"; //Added for Hotel Tab
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {
                if (!Page.IsPostBack)
                {
                    userId = (int)Settings.LoginInfo.UserID;
                    IntialiseControls();
                }
                if (hdnBindData.Value == "true") //Only Tab click event only this will be true and calls the BindCorporateQueueData()
                {
                    BindQueueData();
                }
            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateTripDetailsQueue) Page Load Event Error: " + ex.Message, "0");
        }

    }

    private void IntialiseControls()
    {
        try
        {
            dcReimFromDate.Value = DateTime.Now;
            dcReimToDate.Value = DateTime.Now;
            dcHotelFromDate.Value = DateTime.Now; //Initializing current date for hotel Tab
            dcHotelToDate.Value = DateTime.Now; //Initializing current date for Hotel Tab

            int user_corp_profile_id = CorporateProfile.GetCorpProfileId((int)Settings.LoginInfo.UserID);
            if (user_corp_profile_id > 0)
            {
                this.userCorpProfileId = user_corp_profile_id;
            }
            if (user_corp_profile_id < 0)
            {
                user_corp_profile_id = 0;
            }
            bindEmployeesList();
            //bindSearch(DateTime.Now.AddMonths(-1), DateTime.Now, user_corp_profile_id, Convert.ToInt32(ddlEmployee.SelectedItem.Value), Convert.ToInt32(hdnProductType.Value));
            bindSearch(DateTime.Now.AddYears(-10), DateTime.Now.AddYears(10), user_corp_profile_id, Convert.ToInt32(ddlEmployee.SelectedItem.Value), Convert.ToInt32(hdnProductType.Value));

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }



    private void bindEmployeesList()
    {
        //MODIFIED BY LOKESH ON 19AUG2019
        //NOTE1:IF THE LOGGED IN CORPORATE USER FALLS 
        //UNDER "TVLCO" THEN DISPLAY ALL THE PROFILES WHICH ARE WAITING FOR APPROVAL UNDER THAT AGENCY
        //NOT UNDER "TVLCO" THEN DISPLAY THE PROFILES WHICH ARE WAITING FOR APPROVAL UNDER THAT AGENCY ID AND ALSO FOR THE LOGGED IN USER ONLY

        //APPROVERS TYPE : T---TICKET APPROVER
        //APPROVERS TYPE : V---VISA APPROVER
        if (Settings.LoginInfo != null && Settings.LoginInfo.CorporateProfileId > 0)
        {
            DataTable dtEmployees = null;
            CorporateProfile cp = new CorporateProfile(Settings.LoginInfo.CorporateProfileId, (int)Settings.LoginInfo.AgentId);

            dtEmployees = CorporateProfile.GetCorpProfilesListByApproverType((int)Settings.LoginInfo.CorporateProfileId, "T");//T--TICKET APPROVER

            //if (dtEmployees != null && dtEmployees.Rows != null && dtEmployees.Rows.Count > 0)
            {
                ddlEmployee.DataSource = dtEmployees;
                ddlEmployee.DataTextField = "Name";
                ddlEmployee.DataValueField = "ProfileId";
                ddlEmployee.DataBind();
                ddlEmployee.Items.Insert(0, new ListItem("--Select Employee--", "0"));

                ddlEmployee.SelectedValue = "0";

                //Added for binding employees for Hotel Tab
                ddlHtlEmployees.DataSource = dtEmployees;
                ddlHtlEmployees.DataTextField = "Name";
                ddlHtlEmployees.DataValueField = "ProfileId";
                ddlHtlEmployees.DataBind();
                ddlHtlEmployees.Items.Insert(0, new ListItem("--Select Employee--", "0"));

                ddlHtlEmployees.SelectedValue = "0";
            }

        }
    }

    private void bindSearch(DateTime fromDate, DateTime toDate, int approverId, int empId, int productId)
    {
        try
        {
            //string bookingType = ddlBookingType.SelectedValue;
            DataTable dtPending = CorporateProfileExpenseDetails.GetCorpTicketApprovalsList(approverId, fromDate, toDate, empId, productId);
            if (Convert.ToInt32(hdnProductType.Value) == 1) //based on the product type binding the data
            {
                SearchListPending = dtPending;
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvPending, dtPending);
            }
            else
            {
                HotelSearchListPending = dtPending;
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvHTPending, dtPending);

            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private DataTable SearchListPending
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_PENDING];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ROWID"] };
            Session[REIM_SEARCH_SESSION_PENDING] = value;
        }
    }

    //added for Hotel Tab to bind the data to Grid
    private DataTable HotelSearchListPending
    {
        get
        {

            return (DataTable)Session[REIM_HOTEL_SEARCH_SESSION_PENDING];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ROWID"] };
            Session[REIM_HOTEL_SEARCH_SESSION_PENDING] = value;
        }
    }

    protected void FilterSearch1_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={

                                         {"HTtxtEMP_ID", "EMP_ID"}
,{"HTtxtEMP_NAME", "EMP_NAME"}
,{"HTtxtTRIP_ID", "UNIQUE_TRIP_ID"}
,{"HTtxtBOOKING_DATE", "BOOKING_DATE"}
,{"HTtxtTRAVEL_DATE", "TRAVEL_DATE"}
            ,{"HTtxtRETURN_DATE", "RETURN_DATE"}
,{"HTtxtAIRLINE_CODE", "AIRLINE_CODE"}
,{"HTtxtROUTE", "ROUTE"}
,{"HTtxtREASON_OF_TRAVEL", "REASON_OF_TRAVEL"}
,{"HTtxtTOTAL_FARE", "TOTAL_FARE" }


,




                                         };
        CommonGrid g = new CommonGrid();
        g.FilterGridView(gvPending, SearchListPending.Copy(), textboxesNColumns);

    }

    //Added for Filtering event for Hotel Tab
    protected void HotelFilterSearch1_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={
         {"HTtxtEMP_ID", "EmpId"}
        ,{"HTtxtEMP_NAME", "EmpName"}
        ,{"HTtxtRefenceNo", "Reference_No"}
        ,{"HTtxtBookingDate", "BookingDate"}
        ,{"HTtxtCheckinDate", "CheckinDate"}
        ,{"HTtxtCheckoutDate", "CheckoutDate"}
        ,{"HTtxtHotelName", "HotelName"}
        ,{"HTtxtCity", "City"}
        ,{"HTtxtPrice", "Price" }
        ,{"HTtxtReasonOfTravel", "ReasonOfTravel"}
        ,};
        CommonGrid g = new CommonGrid();
        g.FilterGridView(gvHTPending, HotelSearchListPending.Copy(), textboxesNColumns);

    }

    protected void gvPending_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvPending.PageIndex = e.NewPageIndex;
            gvPending.EditIndex = -1;
            FilterSearch1_Click(null, null);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateTripDetailsQueue)gvPending_PageIndexChanging method .Error:" + ex.ToString(), "0");

        }

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (ddlEmployee.SelectedValue == "0")
            {
                bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), (int)Settings.LoginInfo.CorporateProfileId, 0, Convert.ToInt32(hdnProductType.Value));
            }
            else
            {
                bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), (int)Settings.LoginInfo.CorporateProfileId, Convert.ToInt32(ddlEmployee.SelectedValue), Convert.ToInt32(hdnProductType.Value));
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateTripApprovalQueue) btnSearchApprovals Error: " + ex.ToString(), "0");
        }



    }

    /// <summary>
    /// Return the ceiled value for TBOAir otherwise rounded value
    /// </summary>
    /// <param name="price"></param>
    /// <param name="source"></param>
    /// <returns></returns>
    protected Decimal GetRoundedAmount(decimal price, string source)
    {
        decimal total = 0m;
        if (source.Contains(","))
        {
            if (source == "21,21")
                total = Math.Ceiling(price);
            else
                total = Math.Round(price, Settings.LoginInfo.DecimalValue);
        }
        else if (source == "21")
        {
            total = Math.Ceiling(price);
        }
        else
        {
            total = Math.Round(price, Settings.LoginInfo.DecimalValue);
        }

        return total;
    }



    protected void gvPending_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink hlink = e.Row.FindControl("hlinkDetails") as HyperLink;
            hlink.NavigateUrl = string.Format("~/CorporateTripAnalysis.aspx?flightId={0}&empId={1}&empName={2}&status=P&TripId={3}&bookingType={4}", DataBinder.Eval(e.Row.DataItem, "TRIP_ID"), DataBinder.Eval(e.Row.DataItem, "EMP_ID"), DataBinder.Eval(e.Row.DataItem, "EMP_NAME"), DataBinder.Eval(e.Row.DataItem, "UNIQUE_TRIP_ID"), DataBinder.Eval(e.Row.DataItem, "BOOKING_TYPE"));
            if (DataBinder.Eval(e.Row.DataItem, "BOOKING_TYPE").ToString() == "Normal")
            {
                hlink.NavigateUrl = string.Format("~/CorporateTripAnalysis.aspx?flightId={0}&empId={1}&empName={2}&status=P&TripId={3}", DataBinder.Eval(e.Row.DataItem, "TRIP_ID"), DataBinder.Eval(e.Row.DataItem, "EMP_ID"), DataBinder.Eval(e.Row.DataItem, "EMP_NAME"), DataBinder.Eval(e.Row.DataItem, "UNIQUE_TRIP_ID"));
            }
        }
    }
    //added for Search button for Hotel Tab
    protected void btnHotelSearch_Click(object sender, EventArgs e)
    {
        IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
        if (ddlHtlEmployees.SelectedValue == "0")
        {
            bindSearch(Convert.ToDateTime(dcHotelFromDate.Value, dateFormat), Convert.ToDateTime(dcHotelToDate.Value, dateFormat), (int)Settings.LoginInfo.CorporateProfileId, 0, Convert.ToInt32(hdnProductType.Value));
        }
        else
        {
            bindSearch(Convert.ToDateTime(dcHotelFromDate.Value, dateFormat), Convert.ToDateTime(dcHotelToDate.Value, dateFormat), (int)Settings.LoginInfo.CorporateProfileId, Convert.ToInt32(ddlHtlEmployees.SelectedValue), Convert.ToInt32(hdnProductType.Value));
        }
    }

    protected void Page_PreRender(object sender, EventArgs e) //before rendering page setting Tab selection.
    {
        Utility.StartupScript(this.Page, "SetActiveTab();", "SetActiveTab");
    }

    private void BindQueueData() //Added for bind the data only in Tab click event
    {
        dcHotelFromDate.Value = DateTime.Now;
        dcHotelToDate.Value = DateTime.Now;
        dcReimFromDate.Value = DateTime.Now;
        dcReimToDate.Value = DateTime.Now;

        if (Convert.ToInt32(hdnProductType.Value) == 1)
            bindSearch(DateTime.Now.AddMonths(-1), DateTime.Now, this.userCorpProfileId, Convert.ToInt32(ddlEmployee.SelectedItem.Value), Convert.ToInt32(hdnProductType.Value));
        else
            bindSearch(DateTime.Now.AddMonths(-1), DateTime.Now, this.userCorpProfileId, Convert.ToInt32(ddlHtlEmployees.SelectedItem.Value), Convert.ToInt32(hdnProductType.Value));
        hdnBindData.Value = "false";
    }

    protected void gvHTPending_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvHTPending.PageIndex = e.NewPageIndex;
        gvHTPending.EditIndex = -1;
        HotelFilterSearch1_Click(null, null);
    }

    //Rounding price value to agent decimal value
    protected Decimal GetPriceRoundOff(object price)
    {
        decimal total = 0m;
        if (price != DBNull.Value)
        {
            total = Math.Round(Convert.ToDecimal(price), Settings.LoginInfo.DecimalValue);
        }
        else
        {
            total = 0;
        }
        return total;
    }
}
