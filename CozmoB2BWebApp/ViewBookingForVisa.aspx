﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ViewBookingForVisa" ValidateRequest="false" CodeBehind="ViewBookingForVisa.aspx.cs" %>

<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.BookingEngine.GDS" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Visa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script src="Scripts/jsBE/ModalPop.js" type="text/javascript"></script>

    <script src="Scripts/jsBE/Utils.js" type="text/javascript"></script>

    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="yui/build/calendar/assets/calendar.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script src="yui/build/animation/animation-min.js" type="text/javascript"></script>

    <script src="Scripts/ViewBookingForVisa.js" type="text/javascript"></script>
    <style type="text/css">
        .inp_lists 
        {
            height:0px;
             width:0px;
             border:none;
             background:white;
        }
    </style>
    <script type="text/javascript">


        function newwindow(url) {
            window.open(url, '', 'toolbar=no,width=900,height=500,resizable=yes,scrollbars=yes', true);

        }


        function Edit(Id) {
            if (document.getElementById('ViewInfoDiv' + Id) != null) {

                document.getElementById('ViewInfoDiv' + Id).style.display = 'none';


            }
            document.getElementById('edit' + Id).style.display = 'none';
            document.getElementById('cancel' + Id).style.display = 'block';
            if (document.getElementById('UpdateInfoDiv' + Id) != null) {
                document.getElementById('UpdateInfoDiv' + Id).style.display = 'block';
            }
            var select = document.getElementById('Status' + Id);
            if (select != null) {
                document.getElementById('Status' + Id).style.display = 'block';
                document.getElementById('PaxRemarks' + Id).style.display = 'block';
                select.options.length = 0;
                select.options.add(new Option("Select", "0"));
                select.options.add(new Option("Approved", "5"));
                select.options.add(new Option("Rejected", "6"));
            }

        }
        function Cancel(Id) {
            if (document.getElementById('ViewInfoDiv' + Id) != null) {

                document.getElementById('ViewInfoDiv' + Id).style.display = 'block';

            }
            document.getElementById('cancel' + Id).style.display = 'none';
            document.getElementById('edit' + Id).style.display = 'block';
            if (document.getElementById('UpdateInfoDiv' + Id) != null) {
                document.getElementById('UpdateInfoDiv' + Id).style.display = 'none';
            }
            var select = document.getElementById('Status' + Id);
            if (select != null) {
                document.getElementById('Status' + Id).style.display = 'none';
                document.getElementById('PaxRemarks' + Id).style.display = 'none';
                select.options.length = 0;
                select.options.add(new Option("Select", "0"));

            }
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                return false;
            }
            return true;
        }



        function ValidateFields() {
            var isValid = true;
            $("#<%=txtorderid.ClientID%>").css("border-color", "");
            $("#<%=txtpaymentid.ClientID%>").css("border-color", "");
            $("#<%=ddlEnumValue.ClientID%>").css("border-color", "");
<%--            $("#<%=txtremarks.ClientID%>").css("border-color", "");--%>
            //Added by hari checking the visa status inprocess and inprocess_M
            var visastatus =$(".lblvisastatus").text().trim().toUpperCase(); 
            for (var i = 0; i <<% = visaDT.Rows.Count %>; i++) {
                var status = $('#Status' + i).val().trim();
                if (!visastatus.startsWith("INPROCESS")) {  
                    if (status == "0") {
                        $('#s2id_Status' + i).focus();
                        $('#s2id_Status' + i).css("border-color", "Red");
                        isValid = false;
                    }
                }
                var PaxRemarks = $("#PaxRemarks" + i).val();
                if (PaxRemarks == "") {
                    $("#PaxRemarks" + i).focus();
                    $("#PaxRemarks" + i).css("border-color", "Red");

                    isValid = false;
                }

                if (status == 9) {
                    var txtorderid = $("#<%=txtorderid.ClientID%>").val();
                    if (txtorderid == "") {
                        $("#<%=txtorderid.ClientID%>").focus();
                        $("#ctl00_cphTransaction_txtorderid").css("border-color", "Red");
                        isValid = false;
                    }
                    var txtpaymentid = $("#<%=txtpaymentid.ClientID%>").val();
                    if (txtpaymentid == "") {
                        $("#<%=txtpaymentid.ClientID%>").focus();
                        $("#ctl00_cphTransaction_txtpaymentid").css("border-color", "Red");
                        isValid = false;
                    }

                    var ddlEnumValue = $("#<%=ddlEnumValue.ClientID%>").val();
                    if (ddlEnumValue == "-1") {
                        $("#s2id_ctl00_cphTransaction_ddlEnumValue").focus();
                        $("#s2id_ctl00_cphTransaction_ddlEnumValue").css("border-color", "Red");
                        alert('Select The Payment Source...!');
                        isValid = false;
                    }
                }

            }


            return isValid;
        }

    </script>

    <asp:MultiView ID="MultiViewBooking" runat="server">
        <asp:View ID="BookingView" runat="server">
            <div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="30" colspan="3">
                            <label class="heading_h3">
                                Review Visa Details</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%DataRow dataRow = visaDT.Rows[0]; %>
                            <a href="AdminVisaQueue.aspx"><< Back To Visa Queue</a>
                        </td>
                        <td>Booking Date:
                            <% 
                                string daystring = string.Empty;
                                daystring = Util.GetDayString(Util.UTCToIST(Convert.ToDateTime(dataRow["visaBookingDate"])));
                                if (daystring == "Today")
                                { %>
                            Today
                            <% } %>
                            <% = Util.UTCToIST(Convert.ToDateTime(dataRow["visaBookingDate"])).ToString("dd/MM/yyyy")%>
                        </td>
                        <td align="right">Transaction Id: <span style="color: Red">VISA<% =Convert.ToDateTime(dataRow["visaBookingDate"]).ToString("yy")%><% =Convert.ToDateTime(dataRow["visaBookingDate"]).ToString("MM")%><% = dataRow["VisaId"]%>
                        </span>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:Label ID="lblMessage" Text="" runat="server"></asp:Label>
            <div>
                <div class="margin-left-15 fleft font-red">
                    <% = message %>
                </div>
                <%--<div class="fright font-16 bold"><% = booking.Status.ToString() %></div>--%>
            </div>
            <div class="width-100 fleft margin-top-10">
                <div class="fleft bold font-12">
                    <%if (visaDT.Rows[0]["guranterName"] != DBNull.Value)
                        { %>
                    <a href="javascript:showGuarantorDiv('<% =visaDT.Rows[0]["paxId"] %>')"><span>Guarantor
                        Details</span></a>
                    <%} %>
                </div>
                <div style="height: auto !important; display: none;" class="agency_module" id="GurantorDiv">
                </div>
            </div>

            <div id="calDiv" class="" style="position: relative;">
                <div id="callContainer" style="position: absolute; display: none; top: 28px; left: 79px;">
                </div>
                <div id="callContainer2" style="position: absolute; display: none; top: 28px; left: 79px;">
                </div>
                <div id="errMess" class="error_module" style="display: none;">
                    <div id="divErrMess" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                    </div>
                </div>
                <div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <label style="margin-right: 100px; padding-left: 10px; font-weight: bold">
                                    Passenger Details
                                </label>
                            </td>
                            <td>

                                <a class="normallink_li" href="javascript:newwindow('VisaAcknowledgement.aspx?key=<%=dataRow["VisaId"]%>')">Acknowledgement Receipt</a> <a style="display: none;" class="normallink_li" href="javascript:newwindow('VisaApplicationFormDetails.aspx?key=<%=dataRow["VisaId"]%>')">Visa Application Form</a>
                                <%  if (Convert.ToInt32(dataRow["visaStatus"]) != (int)VisaStatus.Submitted && Convert.ToInt32(dataRow["visaStatus"]) != (int)VisaStatus.NotEligible && Convert.ToInt32(dataRow["visaStatus"]) != (int)VisaStatus.Eligible && Convert.ToInt32(dataRow["visaStatus"]) != (int)VisaStatus.PartiallyEligible)
                                    { %>
                                <a class="normallink_li" href="javascript:newwindow('VisaPaymentSlip.aspx?key=<%=dataRow["VisaId"]%>')">Visa Payment Receipt</a>
                                <%} %>
                            </td>
                            <td align="right">
                                <label>
                                    Visa Status:</label>
                                <label class="lblvisastatus" style="color: Red">
                                    <% = (Visa.VisaStatus)Enum.Parse(typeof(Visa.VisaStatus), dataRow["visaStatus"].ToString()) %></label>
                            </td>
                        </tr>
                    </table>
                </div>


                <div style="float: left; margin-top: 5px; width: 124px; display: none;">
                    <asp:Button OnClick="btnExport_Click" runat="server" ID="btnExport" Text="Export To Excel"
                        CssClass="button" />
                </div>

                <div class="PassengerDetails_form">
                    <div id="Div1" class="">
                        <table class="fnt12" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="20px" valign="top" width="20%">
                                    <label style="font-weight: bold;">
                                        Passenger Name</label>
                                </td>
                                <td width="12%" align="left">
                                    <%--<label style="font-weight: bold;">
                                        Passport No</label>--%>
                                </td>
                                <td width="20%" align="left">
                                    <%--<label style="font-weight: bold;">
                                        Documents</label>--%>
                                </td>
                                <td width="20%" align="left">
                                    <label style="font-weight: bold;">
                                        Select Status</label>
                                </td>
                                <td width="20%" align="left">
                                    <label style="font-weight: bold;">
                                        Remarks</label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <%--my work--%>
                <%  for (int i = 0; i < visaDT.Rows.Count; i++)
                    {
                        List<VisaUploadedDocument> uploadedDocumentList = uploadedDocumentList = VisaUploadedDocument.GetUploadedDocumentList(Convert.ToInt32(visaDT.Rows[i]["paxId"]));

                        string linkClass = string.Empty;
                        string linkOnclick = "void(0)";
                        string linkOnAddress = "void(0)";
                        string linkOnPassport = "void(0)";
                        linkClass = " hand underline";
                        linkOnclick = "Javascript:ShowPaxPopUp('PaxProfile-" + i + "','Result-" + i + "'," + visaDT.Rows[i]["paxId"].ToString() + ")";
                        linkOnAddress = "Javascript:ShowPaxAddressPopUp('PaxAddress-" + i + "','Result-" + i + "'," + visaDT.Rows[i]["paxId"].ToString() + ")";
                        linkOnPassport = "Javascript:ShowPaxPassportPopUp('PaxPassport-" + i + "','Result-" + i + "'," + visaDT.Rows[i]["paxId"].ToString() + ")";
                %>
                <div class="PassengerDetails_form">
                    <div id="Col1" class="bg_white bor_gray pad_10 marbot_10">
                        <table class="fnt12" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="20px" valign="top" width="24%">
                                    <label class="lblbold12">
                                        Passenger
                                        <%=i+1 %>
                                        Details <span id="passenger_status" style="color: Red">
                                            <%if (Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) != 0)
                                                { %>
                                            (<%=(VisaPassengerStatus)Convert.ToInt32(visaDT.Rows[i]["PaxStatus"])%>)
                                            <%} %>
                                        </span>
                                    </label>
                                    <span style="color: Red">
                                        <label id="errMsg<%=i %>">
                                        </label></td>
                                <td width="16%">&nbsp;
                                </td>
                                <td width="20%" align="right">&nbsp;
                                </td>
                                <td width="20%" align="right">&nbsp;
                                </td>
                                <td width="20%" align="right">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="24%">
                                    <%--<label>
                                            Passenger Name</label><br />--%>
                                    <label>
                                        <span class="<% = linkClass %> color-underline" title="Click to see pax profile"
                                            onclick="<% = linkOnclick %>">
                                            <%=visaDT.Rows[i]["paxFirstName"]%>
                                            <%=visaDT.Rows[i]["paxLastName"]%></span></label>
                                </td>
                                <td valign="top">

                                    <%--<label>
                                        <span class="<% = linkClass %> color-underline" title="Click to see pax profile"
                                            onclick="<% = linkOnPassport %>">
                                            <% =visaDT.Rows[i]["passportNo"]%></span></label>--%>
                                </td>

                                <td valign="top">

                                    <label>
                                        <%if (uploadedDocumentList.Count > 0)
                                            { %>
                                        <a href="javascript:DisplayUploadedDocument('<%= visaDT.Rows[i]["paxId"]%>','UploadedDocument<%=i%>','<% = dataRow["Countrycode"]%>')">View Documents</a>
                                        <%} %>
                                    </label>
                                </td>

                                <td valign="top" style="margin-left: 100px;">
                                    <%--<label>
                                            Select Status</label><br />--%>
                                    <%if ((Convert.ToInt32(dataRow["visaStatus"]) == (int)VisaStatus.InProcess || Convert.ToInt32(dataRow["visaStatus"]) == (int)VisaStatus.Approved || Convert.ToInt32(dataRow["visaStatus"]) == (int)VisaStatus.PartiallyApproved || Convert.ToInt32(dataRow["visaStatus"]) == (int)VisaStatus.Rejected) && (Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) == (int)VisaPassengerStatus.NotEligible || Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) == (int)VisaPassengerStatus.Cancelled))
                                        { %>
                                    <%if (Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) == (int)VisaPassengerStatus.NotEligible)
                                        { %>
                                    <label>
                                        Not Eligible</label>
                                    <%}
                                        else
                                        { %>
                                    <label>
                                        Cancelled</label>
                                    <%} %>
                                    <%}
                                        else if (Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) == (int)VisaPassengerStatus.Cancelled)
                                        {%>
                                    <label>
                                        Cancelled</label>
                                    <%}
                                        else
                                        { %>
                                    <%if (Convert.ToInt32(dataRow["visaStatus"]) != (int)VisaStatus.Approved)
                                        { %>
                                    <select class="inp_lists" style="width: 120px" id="Status<% =i%>" onchange="passengerStatusChange('<%=i %>')"
                                        name="Status<% =i%>">
                                        <%}
                                            else
                                            { %>
                                        <select class="inp_lists" style="width: 120px; display: none;" id="Status<% =i%>" onchange="passengerStatusChange('<%=i %>')"
                                            name="Status<% =i%>">
                                            <%} %>
                                            <%if (Convert.ToInt32(dataRow["visaStatus"]) != (int)VisaStatus.RequestChange && Convert.ToInt32(dataRow["visaStatus"]) != (int)VisaStatus.Cancelled)
                                                {
                                                    if (Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) == (int)VisaPassengerStatus.InProcess ||Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) == (int)VisaPassengerStatus.InProcess_M )
                                                    { %>
                                            <option value="0">Select</option>
                                            <option value="5">Approved</option>
                                            <option value="6">Rejected</option>
                                            <option value="7">Cancelled</option>
                                            <%}

                                                else if (Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) == (int)VisaPassengerStatus.Submitted || Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) == (int)VisaPassengerStatus.Eligible)
                                                { %>
                                            <option value="0">Select</option>
                                            <option value="2">Eligible</option>
                                            <option value="3">NotEligible</option>
                                            <option value="7">Cancelled</option>
                                            <option value="8">Duplicate</option>
                                            <option value="9">InProcess_M</option>

                                            <%}

                                                else if (Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) != (int)VisaPassengerStatus.Approved && Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) != (int)VisaPassengerStatus.Rejected && Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) != (int)VisaPassengerStatus.Duplicate)
                                                { %>
                                            <option value="0">Select</option>
                                            <option value="2">Eligible</option>
                                            <option value="3">NotEligible</option>
                                            <option value="7">Cancelled</option>
                                            <option value="8">Duplicate</option>
                                            <%}
                                                }
                                                else if (Convert.ToInt32(dataRow["visaStatus"]) == (int)VisaStatus.RequestChange)
                                                {%>
                                            <option value="0">Select</option>
                                            <option value="7">Cancelled</option>
                                            <%}
                                                else
                                                { %>
                                            <option value="0">Select</option>
                                            <%} %>
                                        </select>
                                        <%} %>
                                </td>
                                <%--<td valign="top">
                                    <asp:TextBox ID="txtremarks" CssClass="form-control" runat="server" TextMode="MultiLine" />
                                </td>--%>
                                <td valign="top">
                                 <%--   <label>
                                        Remarks
                                    </label> --%>
                                    <br />
                                    <%if (Convert.ToInt32(dataRow["visaStatus"]) != (int)VisaStatus.Approved)
                                        { %>

                                    <textarea style="width: 170px" class="remarks0" id="PaxRemarks<%=i %>" name="PaxRemarks<%=i %>"></textarea>
                                    <%}
                                        else
                                        { %>
                                    <textarea style="width: 170px; display: none;margin-right:20px;" class="remarks1" id="PaxRemarks<%=i %>" name="PaxRemarks<%=i %>"></textarea>
                                    <%} %>      
                                </td>
                            </tr>
                        </table>
                        <%if (Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) == (int)VisaPassengerStatus.Approved || Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) == (int)VisaPassengerStatus.Rejected)
                            { %>
                        <span class="passenger_Edit"><a id="edit<%=i %>" href="javascript:Edit('<%=i %>')" style="display: block;">Edit</a>
                            <a id="cancel<%=i %>" href="javascript:Cancel('<%=i %>')" style="display: none;">Cancel</a></span>
                        <%} %>
                        <div class="TopCol">
                            <div style="height: auto !important; display: none;" class="agency_module" id="PaxProfile-<%=i %>">
                            </div>
                            <div class="agency_module" id="PaxPassport-<%=i %>" style="height: auto !important; display: none;">
                            </div>
                            <div id="UploadedDocument<%=i%>" class="cz_popup1" style="display: none;">
                            </div>
                        </div>
                        <div id="UpdateInfoDiv<%=i %>" style="display: none; position: relative;" class="TopCol">
                            <p class="pdCol">
                                <label>
                                    Visa Issue Date
                                </label>
                                <input type="text" id="txtIssueDate<%=i %>" name="txtIssueDate<%=i %>" class="imgwd" />
                                <em class="cal">
                                    <img id="ImgIss<%=i %>" src="images/call-cozmo.png" alt="" onclick="ShowCalender('callContainer','ImgIss<%=i %>','txtIssueDate<%=i %>','txtExpiryDate<%=i %>')" /></em>
                            </p>
                            <p class="pdCol">
                                <label>
                                    Visa Expiry Date</label>
                                <input type="text" id="txtExpiryDate<%=i %>" name="txtExpiryDate<%=i %>" class="imgwd" />
                                <em class="cal">
                                    <img id="ImgExp<%=i %>" src="images/call-cozmo.png" alt="" onclick="ShowCalender2('callContainer2','ImgExp<%=i %>','txtExpiryDate<%=i %>','txtIssueDate<%=i %>')" /></em>
                            </p>
                            <p class="pdCol">
                                <label>
                                    Visa Number</label>
                                <input type="text" id="txtvisaNumber<%=i %>" name="txtvisaNumber<%=i %>" />
                            </p>

                            <p class="pdCol">
                                <label>
                                    Upload Visa</label>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:FileUpload ID="File" runat="server" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="UpdatePassenger" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </p>

                            <p class="pdCol" style="display: none">
                                <label>
                                    Visa Issue Date
                                </label>
                                <input type="text" />
                            </p>
                        </div>
                        <%if (Convert.ToInt32(visaDT.Rows[i]["PaxStatus"]) == (int)VisaPassengerStatus.Approved)
                            {
                                string imagePath = Convert.ToString(CT.Configuration.ConfigurationSystem.VisaConfig["visaImagePath"]);
                                imagePath += "Member_" + new VisaPassenger(Convert.ToInt32(visaDT.Rows[i]["PaxId"])).CreatedBy + "/";
                        %><%-- "<%=Request.Url.Scheme %>://cozmotravel.tektravels.com/Other/"; --%>
                        <div id="ViewInfoDiv<%=i %>" class="TopCol">
                            <p class="pdCol">
                                <label>
                                    Visa Issue Date
                                </label>
                                <label>
                                    <%=String.Format("{0:dd-MM-yyyy}",visaDT.Rows[i]["paxVisaIssue"])%>
                                </label>
                            </p>
                            <p class="pdCol">
                                <label>
                                    Visa Expiry Date</label>
                                <label>
                                    <%=String.Format("{0:dd-MM-yyyy}",visaDT.Rows[i]["paxVisaExpiry"])%></label>
                            </p>
                            <p class="pdCol">
                                <label>
                                    Visa Number</label>
                                <label>
                                    <%=visaDT.Rows[i]["paxVisaNumber"].ToString()%></label>
                            </p>
                            <p class="pdCol">
                                <label>
                                    Upload Visa</label>
                                <label id="lblVisaImg<% =i %>">
                                    <%if (System.IO.Path.GetExtension(Convert.ToString(visaDT.Rows[i]["paxVisaPath"])).ToUpper() != ".PDF")
                                        {%>
                                    <a href="javascript:ShowVisa('visaIcon<%=i %>','Images')">
                                        <img src="<%= imagePath + Convert.ToString(visaDT.Rows[i]["paxVisaPath"]) %>" alt="View"
                                            height="100px" width="100px" />
                                    </a>
                                    <%}
                                        else
                                        { %>
                                    <input id="hdfPath" type="hidden" value="<%=imagePath+ Convert.ToString(visaDT.Rows[i]["paxVisaPath"]) %>" />
                                    <input id="hdfType" type="hidden" value="<%=System.IO.Path.GetExtension(Convert.ToString(visaDT.Rows[i]["paxVisaPath"])) %>" />
                                    <input id="hdfDocName" type="hidden" value="<%=System.IO.Path.GetFileName(Convert.ToString(visaDT.Rows[i]["paxVisaPath"])) %>" />
                                    <asp:ImageButton ID="imgPreview" runat="server" OnClientClick="return download();" Height="80px" Width="80px" CausesValidation="false" ImageUrl="~/images/Common/pdfFileImage.png" />
                                    <%} %>
                                </label>
                            </p>
                            <div style="height: auto !important; display: none;" class="agency_module" id="visaIcon<%=i %>">
                                <span style="float: right"><a href="javascript:Close('visaIcon<%=i %>')"><b>X</b></a></span>
                                <img src="<%=imagePath+ Convert.ToString(visaDT.Rows[i]["paxVisaPath"]) %>" alt="Img"
                                    height="300px" width="300px" />
                            </div>
                            <p class="pdCol" style="display: none">
                                <label>
                                    Visa Issue Date
                                </label>
                                <label>
                                    Visa Number</label>
                            </p>
                        </div>
                    
                       <%-- Review Visa Details--%>
                        <%} %>
                        <div class="clear">
                        </div>
                    </div>
                  
                    <div>
                        <%} %>
                        <%if (Convert.ToInt32(dataRow["visaStatus"]) != (int)VisaStatus.Cancelled && Convert.ToInt32(dataRow["visaStatus"]) != (int)VisaStatus.Duplicate)
                            { %>
                        <label id="lblErrorMsg" style="color: Red;">
                        </label>
                        <div style="float: right; margin-top: 5px; width: 55px;">
                            <asp:Button ID="UpdatePassenger" runat="server" Text="Submit" CssClass="button fright"
                                OnClientClick="javascript:return ValidateFields();"
                                EnableViewState="False" OnClick="UpdatePassenger_Click" />
                        </div>
                        <%} %>
                    </div>
                        <%--  Added By Hari Inprocess Mode--%>
                    <div class="box_16mtop" id="visaDetails">
                        <table>
                            <thead>
                                <th>Order Id</th>
                                <th style="width: 50px"></th>
                                <th>Payment Id</th>
                                <th style="width: 50px"></th>
                                <th>Payment Sources</th>
                            </thead>
                            <tbody>
                                <tr ><strong style="font-size:13px;">Payment Details </strong></tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtorderid" CssClass="form-control" runat="server" /></td>
                                    <td>
                                        <input id="txtstatus" cssclass="form-control" runat="server" style="display: none;" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtpaymentid" CssClass="form-control" runat="server" />

                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:DropDownList ID="ddlEnumValue" runat="server" Style="width: 120px;"></asp:DropDownList>
                                </tr>
                            </tbody>
                        </table>



                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td valign="top" width="75%">
                                <div>
                                    <label class="heading_h3">
                                        Review Visa Details</label>
                                </div>
                                <div style="width: 98%" class="box_16mtop">
                                    <div id="itineraryDiv">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="20%">
                                                    <label class="lblbold12">
                                                        Type Of Visa:</label><br />
                                                    <% = dataRow["visaType"] %>
                                                </td>
                                                <td width="19%">
                                                    <label class="lblbold12">
                                                        Country:</label><br />
                                                    <% = dataRow["visaCountry"]%>
                                                </td>
                                                <%--    <td width="18%">
                                                    <label class="lblbold12">
                                                        Destination City:
                                                    </label>
                                                    <br />
                                                    <% = dataRow["visaCity"]%>
                                                </td>--%>
                                                <td width="20%">
                                                    <label class="lblbold12">
                                                        Nationality:
                                                    </label>
                                                    <br />
                                                    <% VisaNationality nationality = new VisaNationality(); %>
                                                    <% =dataRow["nationality"]%>
                                                </td>
                                                <%-- <td width="23%">
                                                    <label class="lblbold12">
                                                        Country of residence:
                                                    </label>
                                                    <br />
                                                    <% = dataRow["residence"]%>
                                                </td>--%>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <%--------------------- Start Queue History  ----------------------%>
                                <%if (visaId != 0)
                                    {%>
                                <div class="hand underline color-underline" id="BHContainer" onclick="DisplayBookingHistory(<% = visaId %>)">
                                    Queue History
                                </div>
                                <div class="fleft margin-top-20" id="QueueHistory" style="width: 100%; background: #fff; display: none; position: static; left: 300px; top: 450px; border: 0px outset #ccc;">
                                </div>
                                <%} %>
                                <%--------------------- End Queue History ----------------------%>
                                <div>
                                    <div class="fleft width-600 margin-top-10">
                                        <span class="fleft full-width bold">Enter Remarks</span>
                                        <div class="fleft full-width">
                                            <span class="fleft margin-right-5">
                                                <asp:TextBox ID="txtComment" runat="server" Columns="43" Rows="4" TextMode="MultiLine"></asp:TextBox>
                                            </span>
                                        </div>
                                        <br />
                                        <div style="float: left; margin-top: 5px;">
                                            <asp:Button ID="btnAddComment" runat="server" Text="Submit" CssClass="button fright"
                                                OnClick="btnAddComment_Click" />
                                        </div>

                                        <script type="text/javascript">
                                            showsubmitBtn();
                                        </script>

                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </td>
                            <td valign="top" width="25%">
                                <div style="float: right">
                                    <div>
                                        <label class="heading_h3">
                                            Price overview</label>
                                    </div>
                                    <div style="width: 200px" class="box_visaquee">
                                        <div class="fnt12">
                                            <div id="hidesplit" style="display: block; margin-left: 0px; _margin-left: 8px; float: left;">
                                                <div class="fleft width-100">
                                                    <div class="fleft width-45">
                                                        <img src="Images/spacer.gif" alt="Spacer" />
                                                    </div>
                                                    <div class="fleft width-160">
                                                        <%--<a href="#">Show Fee Details</a>--%>
                                                        <%--<a href="#" onclick="Show()" >Show Fee Details</a>--%>
                                                    </div>
                                                </div>
                                                <div class="fleft margin-bottom-20">
                                                    <div class="width-100 fleft">
                                                        <div class="fleft width67">
                                                            Visa Fee :
                                                        </div>
                                                        <div class="fleft text-right" style="width: 96px;">
                                                            <%-- <% = String.Format("{0:0.00}", dataRow["visaCostFee"])%>--%>
                                                            <asp:Label ID="lblVisaFee" runat="server"></asp:Label>
                                                            <asp:TextBox ID="txtVisaFee" runat="server" Visible="false" MaxLength="7" Width="50px" onkeypress="return isNumber(event);"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <div class="width-100 fleft">
                                                        <div class="fleft width67">
                                                            Markup Fee
                                                        </div>
                                                        <div class="fleft text-right" style="width: 96px;">
                                                            <%--<% = String.Format("{0:0.00}", dataRow["visaMarkupFee"])%>--%>
                                                            <asp:Label ID="lblMarkup" runat="server"></asp:Label>
                                                            <asp:TextBox ID="txtMarkup" runat="server" Visible="false" MaxLength="7" Width="50px" onkeypress="return isNumber(event);"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="width-100 fleft">
                                                        <div class="fleft width67">
                                                            Insurace Fee
                                                        </div>
                                                        <div class="fleft text-right" style="width: 96px;">
                                                            <%-- <% = String.Format("{0:0.00}", dataRow["visaInsuranceFee"])%>--%>
                                                            <asp:Label ID="lblInsuranceFee" runat="server"></asp:Label>
                                                            <asp:TextBox ID="txtInsuranceFee" runat="server" Visible="false" MaxLength="7" Width="50px" onkeypress="return isNumber(event);"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="width-100 fleft">
                                                        <div class="fleft width67">
                                                            Deposit Fee
                                                        </div>
                                                        <div class="fleft text-right" style="width: 96px;">
                                                            <asp:Label ID="lblTransaction" runat="server"></asp:Label>
                                                            <asp:TextBox ID="txtTransaction" runat="server" Visible="false" MaxLength="7" Width="50px" onkeypress="return isNumber(event);">
                                                            </asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <!-- VAT Label-->
                                                    <div class="width-100 fleft">
                                                        <div class="fleft width67">
                                                            VAT
                                                        </div>
                                                        <div class="fleft text-right" style="width: 96px;">
                                                            <asp:Label ID="lblVAT" runat="server"></asp:Label>

                                                        </div>
                                                    </div>

                                                    <div class="width-100 fleft total-top">
                                                        <div class="fleft width67">
                                                            Total
                                                        </div>
                                                        <div class="fleft text-right" style="width: 96px;">

                                                            <% = String.Format("{0:0.00}", dataRow["visaFee"])%>

                                                            <!--  <% = String.Format("{0:0.00}", (Convert.ToDouble(dataRow["visaFee"])) + (Convert.ToDouble(dataRow["visaDepositFee"])) + (Convert.ToDouble(dataRow["visaInsuranceFee"])) + (Convert.ToDouble(dataRow["visaMarkupFee"])))%> -->
                                                        </div>
                                                    </div>
                                                    <div class="width-100 fleft">
                                                        <br />
                                                    </div>
                                                    <div class="width-100 fleft">
                                                        <div>
                                                            <div class="fleft width67">
                                                                Refundable Fee
                                                            </div>
                                                            <div class="fleft text-right" style="width: 96px;">
                                                                <asp:Label ID="lblRefund" runat="server"></asp:Label>
                                                                <asp:TextBox ID="txtRefund" runat="server" Visible="false" Width="50px" onkeypress="return isNumber(event);"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="width-100 fleft">
                                                        <asp:Button ID="btnFee" runat="server" Text="Edit fee" OnClick="btnFee_Click" />
                                                        <asp:Button ID="btnSubmit" runat="server" Visible="false" Text="Save" OnClick="btnSubmit_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="margin-left-5 fleft width-185">
                                                <div class="fleft margin-top-10 dotted-border light-gray-back  padding-block"
                                                    style="margin-left: 1px;">
                                                    <div class="contract-heading fleft">
                                                        <div class="fleft" style="width: 96px;">
                                                            Passenger x
                                                            <% = visaDT.Rows.Count %>
                                                        </div>
                                                        <div class="fright">
                                                            <% = String.Format("{0:0.00}",visaDT.Rows.Count * Convert.ToDouble(dataRow["visaFee"]))%>
                                                        </div>
                                                    </div>
                                                    <div class="contract-heading fleft">
                                                        <div class="fleft" style="width: 96px;">
                                                            Refundable Fee
                                                        </div>
                                                        <div class="fright">
                                                            <% = String.Format("{0:0.00}", ((decimal)dataRow["visaRefundableFee"])*refundableFeeFor)%>
                                                        </div>
                                                    </div>
                                                    <div class="contract-heading bold margin-top-10 fleft">
                                                        <div class="fleft width-100">
                                                            <div class="fleft ticket-left-block">
                                                                Total
                                                            </div>
                                                            <div class="fright" id="visafee">
                                                                <% = String.Format("{0:0.00}", (visaDT.Rows.Count * Convert.ToDouble(dataRow["visaFee"])) + (Convert.ToDouble(dataRow["visaRefundableFee"]) * refundableFeeFor))%>
                                                            </div>
                                                        </div>
                                                        <div>
                                                        </div>
                                                        <div id="breakup" style="display: none">
                                                            <div class="fleft width-100">
                                                                <div class="fleft ticket-left-block">
                                                                    Total Payable:
                                                                </div>
                                                                <div class="fright">
                                                                    <% = String.Format("{0:0.00}", visaDT.Rows.Count * Convert.ToDouble(dataRow["visaFee"]))%>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                  
                <%--//my work--%>
                <div class="ticket-middle-parent padding-left-5" style="width: 360px; margin-top: 101px;">
                    <div class="view_booking_visa">
                    </div>
                </div>
            </div>
            <input type="hidden" name="hdVisaId" value='<%=dataRow["VisaId"] %>' />
            <input type="hidden" name="paxCount" id="paxCount" value='<%=TotalPassenger %>' />
        </asp:View>
        <asp:View ID="ErrorView" runat="server">
            <div class="width-720 margin-left-30" style="text-align: center">
                <br />
                <br />
                <br />
                <span>
                    <asp:Label ID="ErrorMessage" runat="server"></asp:Label>
                </span>
                <br />
                <br />
                <span>Go to <a href="">Booking Queue</a>.</span>
                <br />
                <br />
            </div>
            <div class="width-720 margin-left-30">
                <a href="javascript:history.go(-1);"><< Back</a>
            </div>
        </asp:View>
    </asp:MultiView>

    <div>
        <asp:DataGrid ID="dgVisaDocumentList" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundColumn HeaderText="FirstName" DataField="paxFirstName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="LastName" DataField="paxLastName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Email" DataField="paxEmail" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Remarks" DataField="paxRemarks" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="BookingDate" DataField="visaBookingDate" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Agent" DataField="agencyName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Nationality" DataField="nationality" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
            </Columns>
        </asp:DataGrid>
    </div>
    <script type="text/javascript" language="javascript">

        function download(path, type, docName) {
            var path = document.getElementById('hdfPath').value;
            var type = document.getElementById('hdfType').value;
            var docName = document.getElementById('hdfDocName').value;
            var open = window.open('DownloadeDoc.aspx?path=' + path + '&type=' + type + '&docName=' + docName);
            return false;
        }
        //Added By Hari Inprocess Mode
        $(document).ready(function () {
            $('#visaDetails').hide();
            var buttonId;
            var dropdown = document.getElementsByClassName('inp_lists');
            for (var i = 0; i < count; i++) {
                dropdown[i].addEventListener('change', function () {
                    buttonId = this.id;
                });
            }
            $('.inp_lists').change(function () {

                if (this.value== 9) {
                    $('#visaDetails').show();
                    $('#ctl00_cphTransaction_txtstatus').val(this.value);
                }
                var valid = false;
                for (var i = 0; i <<% = visaDT.Rows.Count %>; i++) {
                   var status= $('#Status' + i).val();
                    if (status == '9')
                        valid = true;
                   
                }
                if (valid == false) {
                     $('#ctl00_cphTransaction_txtorderid').val('');
                    $('#ctl00_cphTransaction_txtpaymentid').val('');
                    $('#ctl00_cphTransaction_txtstatus').val(this.value);
                    $('#' + getElement('ddlEnumValue').id).select2('val', '-1');
                    $('#visaDetails').hide();
                }
            });
            //$('#Status0').change(function () {
            //    get_Payment_Details(0);
            //});
            function get_Payment_Details(i) {
                if ($('#Status' + i + '').val() == 9) {
                    $('#visaDetails').show();
                    $('#ctl00_cphTransaction_txtstatus').val($('#Status' + i + '').val());
                }
                else {
                    $('#ctl00_cphTransaction_txtorderid').val('');
                    $('#ctl00_cphTransaction_txtpaymentid').val('');
                    $('#ctl00_cphTransaction_txtstatus').val($('#Status' + i + '').val());
                    $('#' + getElement('ddlEnumValue').id).select2('val', '-1');
                    $('#visaDetails').hide();
                }
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
