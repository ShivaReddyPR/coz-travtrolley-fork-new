﻿using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class ForexQueue : CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindAgent();
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
        }
        /// <summary>
        /// Binding the Agent Details.
        /// </summary>
        private void BindAgent()
        {
            try
            {
                ddlagents.DataSource = AgentMaster.GetList(1, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//TODO
                ddlagents.DataValueField = "agent_id";
                ddlagents.DataTextField = "agent_name";
                ddlagents.DataBind();
                ddlagents.Items.Insert(0, new ListItem("-- Select Agent --", "-1"));
                ddlagents.SelectedValue = Settings.LoginInfo.AgentId.ToString();
            }
            catch { throw; }
        }
        /// <summary>
        /// Getting the Forex request details based on selected start date and end date.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [WebMethod]
        public static string BindQueue(string request, string startDate, string endDate)
        {
            string details = string.Empty;
            List<ForexRequestDetails> forexDetails = new List<ForexRequestDetails>();
            try
            {
                ForexRequestDetails forexRequest = JsonConvert.DeserializeObject<ForexRequestDetails>(request);
                if (forexRequest != null)
                {
                    IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
                    forexRequest.CreatedOn = Convert.ToDateTime(Convert.ToDateTime(startDate, provider).ToString("dd/MM/yyyy 00:00"), provider);
                    forexRequest.ModifiedOn = Convert.ToDateTime(Convert.ToDateTime(endDate, provider).ToString("dd/MM/yyyy 23:59"), provider);
                    forexDetails = forexRequest.Load();
                }
                details = JsonConvert.SerializeObject(forexDetails);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "failed to Binding the Forex Queue Details" + ex.ToString(), "");
            }
            return details;
        }
        /// <summary>
        /// Saving the remarks based on FRID and sending the mail.
        /// </summary>
        /// <param name="forexrequest"></param>
        /// <param name="request"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [WebMethod]
        public static string saveRemarks(string forexrequest, string request, string startDate, string endDate)
        {
            string status = string.Empty;
            try
            {
                ForexRequestRemarks forexRequestRemarks = JsonConvert.DeserializeObject<ForexRequestRemarks>(request);
                if (forexRequestRemarks != null)
                {
                    string rstatus = ForexRequestRemarks.LoadRemarks(forexRequestRemarks.FRID);
                    forexRequestRemarks.RemarksStatus = ((ForexStatus)Convert.ToInt32(forexRequestRemarks.RemarksStatus)).ToString();
                    forexRequestRemarks.CreatedOn = DateTime.Now;
                    forexRequestRemarks.CreatedBy = (int)Settings.LoginInfo.UserID;
                    int res = forexRequestRemarks.save();
                    status = res > 0 ? "Success" : "Fail";
                    if (res > 0)
                    {
                        ForexRequestDetails forexRequest = JsonConvert.DeserializeObject<ForexRequestDetails>(forexrequest);
                        if (forexRequest != null)
                        {
                            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
                            forexRequest.CreatedOn = Convert.ToDateTime(Convert.ToDateTime(startDate, provider).ToString("dd/MM/yyyy 00:00"), provider);
                            forexRequest.ModifiedOn = Convert.ToDateTime(Convert.ToDateTime(endDate, provider).ToString("dd/MM/yyyy 23:59"), provider);
                            List<ForexRequestDetails> ForexRequestDetails = forexRequest.Load();
                            if (ForexRequestDetails != null && ForexRequestDetails.Count > 0)
                            {
                                bool isExist = ForexRequestDetails.Exists(x => x.FRID == forexRequestRemarks.FRID && x.forexRequestRemarks != null && x.forexRequestRemarks.Exists(y => y.RemarksStatus != rstatus));
                                forexRequest = ForexRequestDetails.Where(x => x.FRID == forexRequestRemarks.FRID).FirstOrDefault();
                                AgentAppConfig clsCnfig = new AgentAppConfig();
                                clsCnfig.AgentID = 1;
                                var liconfigs = clsCnfig.GetConfigData();
                                var config = liconfigs.Where(x => x.ProductID == (int)ProductType.Forex && x.AppKey.ToUpper() == "FOREXAGENTEMAIL").FirstOrDefault();
                                if (config != null && !string.IsNullOrWhiteSpace(config.AppValue) && isExist)
                                {
                                    Hashtable ht = new Hashtable();
                                    ht["AgentName"] = forexRequest.AgentName;
                                    ht["FRRefNo"] = forexRequest.FRRefNo;
                                    ht["ProductType"] = forexRequest.ProductType;
                                    ht["Currency"] = forexRequest.Currency;
                                    ht["ForexRequired"] = forexRequest.ForexRequired;
                                    ht["AmountInINR"] = forexRequest.AmountInINR;
                                    ht["Name"] = forexRequest.Name;
                                    ht["MobileNumber"] = forexRequest.MobileNumber;
                                    ht["Email"] = forexRequest.Email;
                                    ht["TravellingTo"] = forexRequest.TravellingTo;
                                    ht["CreatedOn"] = forexRequestRemarks.CreatedOn;
                                    ht["RemarksStatus"] = forexRequestRemarks.RemarksStatus;
                                    ht["Remarks"] = forexRequestRemarks.Remarks;

                                    List<string> toArray = new List<string>();
                                    toArray.Add(config.AppValue);
                                    toArray.Add(forexRequest.Email);
                                    string EmailBody = ConfigurationManager.AppSettings["FOREXREQUEST"];
                                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Forex Request Details("+forexRequestRemarks.RemarksStatus+")", EmailBody, ht);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "failed to Binding the Forex Queue Details" + ex.ToString(), "");
            }
            return status;
        }
    }
}