﻿using System;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.Core;
using CT.BookingEngine;

public partial class UpdateAgentMarkupGUI : CT.Core.ParentPage// System.Web.UI.Page
{
    DataTable dtProducts;
    DataTable dtMarkupList;
    PagedDataSource pagedData = new PagedDataSource();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            AuthorizationCheck();
            if (!IsPostBack)
            {
                InitializePageControls();
            }
            else
            {
                if (ddlAgent.SelectedIndex > 0)
                {
                    if (Utility.ToInteger(hdfAgentId.Value) != Utility.ToInteger(ddlAgent.SelectedItem.Value))
                    {
                        BindProcduct();
                        hdfAgentId.Value = ddlAgent.SelectedItem.Value;
                    }
                    BindControls();
                }
                else
                {
                    chkProduct.Items.Clear();
                }
            }
            
            BindMarkupList();
            lblSuccessMsg.Text = string.Empty;
            errMess.Style.Add("display", "none");
            errorMessage.InnerHtml = string.Empty;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
           // throw ex;
        }
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx", true);
        }
    }

    #region SelectedIndexChanged Events
    protected void ddlAgent_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (ListItem item in chkProduct.Items)
            {
                item.Selected = false;
            }
            tblMarkup.Rows.Clear();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    protected void chkProduct_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            if (ddlAgent.SelectedIndex != 0)
            {
                LoadControls();
            }
            else
            {
                errMess.Style.Add("display", "block");
                errorMessage.InnerHtml = "Please select Agent!";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void ddlSource_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddlSource = sender as DropDownList;
            int agent = 0;
            int rowIndex = 0;
            string find = string.Empty;
            string findB2BMarkup = string.Empty;
            rowIndex = Convert.ToInt32(ddlSource.ID.Split('_')[1]);
            if (ddlAgent.SelectedItem.Value != "0")
            {
                agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            }
            string source = ddlSource.SelectedItem.Text;
            if (source == "GDS")
            {
                source = "SourceId='UA'";
            }
            else if (source == "LCC")
            {
                source = "(SourceId= 'G9' OR SourceId='FZ')";
            }
            else if (source == "O.Supplier")
            {
                source = "(SourceId='DOTW' OR SourceId='GTA' OR SourceId='HotelBeds' OR SourceId='LOH' OR SourceId='WST')";
            }
            else if (source == "C.Supplier")
            {
                source = "SourceId='HotelConnect'";
                //source = "HotelConnect";
            }
            TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + rowIndex.ToString());
            DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + rowIndex.ToString());
            TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + rowIndex.ToString());
            DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + rowIndex.ToString());
            ddlMarkupType.Enabled = true;
            dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty);
            if (agent == 0)
            {
                find = "SourceId='" + source + "' AND AgentId IS NULL";
            }
            else
            {
                find = source + " AND AgentId = '" + agent + "' AND transType='B2B'";
            }
            DataRow[] foundRows = dtMarkupList.Select(find);

            
            if (foundRows.Length > 0)
            {
                txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
                ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
                txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
                ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
            }
            else
            {
                txtAgentMarkup.Text = "0.00";
                txtDiscount.Text = "0.00";
            }
            int agentType = 0;
            int parentAgentID = 0;
            if (Settings.LoginInfo.AgentType == AgentType.Agent || Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                agentType = AgentMaster.GetAgentType(agent);
            }
            if (agentType == (int)AgentType.B2B2B)
            {
                parentAgentID = AgentMaster.GetParentId(agent);
            }
            else if (agentType == (int)AgentType.B2B || agentType == (int)AgentType.Agent)
            {
                parentAgentID = agent;
            }
            else
            {
                parentAgentID = Settings.LoginInfo.AgentId;
            }
            if (Settings.LoginInfo.AgentType == AgentType.B2B || parentAgentID != Settings.LoginInfo.AgentId)
            {
                findB2BMarkup = source + " AND AgentId='" + parentAgentID + "' AND transType='B2B'";
                DataRow[] foundB2BMarkupRows = dtMarkupList.Select(findB2BMarkup);
                if (foundB2BMarkupRows.Length > 0)
                {
                    ddlMarkupType.SelectedValue = Utility.ToString(foundB2BMarkupRows[0]["MarkupType"]);
                    ddlDiscountType.SelectedValue = Utility.ToString(foundB2BMarkupRows[0]["DiscountType"]);
                    ddlDiscountType.Enabled = false;
                    ddlMarkupType.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region ClickEvents
    protected void btnUpdate_OnClick(object sender, EventArgs e)
    {
        try
        {
            dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty);
            hdnSourceName.Value = "0";
            for (int i = 0; i < chkProduct.Items.Count; i++)
            {
                int productId = Convert.ToInt32(chkProduct.Items[i].Value);
                if (chkProduct.Items[i].Selected)
                {
                    int agent = 0;
                    string find = string.Empty;
                    string source = string.Empty;
                    string agentSource = string.Empty;
                    string findB2BMarkup = string.Empty;
                    if (productId < 3 || productId == 5)
                    {
                        DropDownList ddlSource = tblMarkup.Rows[0].Cells[i].FindControl("ddlSource_" + i.ToString()) as DropDownList;
                        agentSource = ddlSource.SelectedItem.Text;
                    }
                    if (ddlAgent.SelectedItem.Value != "0")
                    {
                        agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                    TextBox txtAgentMarkup = tblMarkup.Rows[0].Cells[i].FindControl("txtAgentMarkup_" + i.ToString()) as TextBox;
                    DropDownList ddlMarkUp = tblMarkup.Rows[0].Cells[i].FindControl("ddlMarkupType_" + i.ToString()) as DropDownList;
                    TextBox txtDiscount = tblMarkup.Rows[0].Cells[i].FindControl("txtDiscount_" + i.ToString()) as TextBox;
                    DropDownList ddlDiscount = tblMarkup.Rows[0].Cells[i].FindControl("ddlDiscountType_" + i.ToString()) as DropDownList;
                    DataTable dtSources = AgentMaster.GetAgentSources(agent, productId);
                    DataView dv = dtSources.DefaultView;
                    if (productId < 3 || productId == 5)
                    {
                        if (agentSource == "LCC")
                        {
                            dv.RowFilter = "Name NOT IN('UA')";
                        }
                        else if (agentSource == "GDS")
                        {
                            dv.RowFilter = "Name IN('UA')";
                        }
                        else if (agentSource == "O.Supplier")
                        {
                            dv.RowFilter = "Name NOt IN('HotelConnect')";
                        }
                        else if (agentSource == "C.Supplier")
                        {
                            dv.RowFilter = "Name IN('HotelConnect')";
                        }
                        else
                        {
                            dv.RowFilter = null;
                        }
                        dtSources = dv.ToTable();
                        if (dtSources.Rows.Count > 0)
                        {

                            for (int j = 0; j < dtSources.Rows.Count; j++)
                            {

                                if (productId < 3 || productId == 5)
                                {
                                    source = Convert.ToString(dtSources.Rows[j]["Name"]);
                                }
                                if (agent == 0)
                                {
                                    find = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId IS NULL";
                                }
                                else
                                {
                                    find = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId='" + agent + "' AND transType='B2B'";
                                }

                                DataRow[] foundRows = dtMarkupList.Select(find);
                                UpdateMarkup objUpdateMarkup = new UpdateMarkup();
                                if (foundRows.Length > 0)
                                {
                                    objUpdateMarkup.Id = Utility.ToInteger(foundRows[0]["MRId"]);
                                    objUpdateMarkup.MrDId = Utility.ToInteger(foundRows[0]["MRDId"]);
                                }
                                int agentType = 0;
                                int parentAgentID = 0;
                                if (Settings.LoginInfo.AgentType == AgentType.Agent || Settings.LoginInfo.AgentType == AgentType.BaseAgent)
                                {
                                    agentType = AgentMaster.GetAgentType(agent);
                                }
                                if (agentType == (int)AgentType.B2B2B)
                                {
                                    parentAgentID = AgentMaster.GetParentId(agent);
                                }
                                else
                                {
                                    parentAgentID = Settings.LoginInfo.AgentId;
                                }

                                if (Settings.LoginInfo.AgentType == AgentType.B2B || parentAgentID != Settings.LoginInfo.AgentId)
                                {
                                    findB2BMarkup = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId='" + parentAgentID + "' AND transType='B2B'";
                                    DataRow[] foundB2BMarkupRows = dtMarkupList.Select(findB2BMarkup);
                                    if (foundB2BMarkupRows.Length > 0)
                                    {
                                        objUpdateMarkup.OurCommission = Utility.ToDecimal(foundB2BMarkupRows[0]["Markup"]);
                                    }
                                    else
                                    {
                                        objUpdateMarkup.OurCommission = Utility.ToDecimal("0.00");
                                    }
                                }
                                else
                                {
                                    objUpdateMarkup.OurCommission = Utility.ToDecimal("0.00");
                                }
                                objUpdateMarkup.ProductId = productId;
                                objUpdateMarkup.SourceId = source;

                                objUpdateMarkup.AgentId = agent;
                                objUpdateMarkup.AgentMarkup = Utility.ToDecimal(txtAgentMarkup.Text);

                                objUpdateMarkup.Markup = (objUpdateMarkup.AgentMarkup + objUpdateMarkup.OurCommission);
                                objUpdateMarkup.MarkupType = ddlMarkUp.SelectedItem.Value;
                                objUpdateMarkup.Discount = Utility.ToDecimal(txtDiscount.Text);
                                objUpdateMarkup.DiscountType = ddlDiscount.SelectedItem.Value;
                                objUpdateMarkup.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
                                objUpdateMarkup.Save();
                            }
                        }
                        else
                        {
                            string productName = string.Empty;
                            if (productId == 1)
                            {
                                productName = "Flight";
                            }
                            else if (productId == 2)
                            {
                                productName = "Hotel";
                            }
                            else if (productId == 3)
                            {
                                productName = "Insurance";
                            }
                            if (hdnSourceName.Value == "0")
                            {
                                hdnSourceName.Value = productName;
                            }
                            else
                            {
                                hdnSourceName.Value += "," + productName;
                            }
                        }
                    }
                    else
                    {
                        if (agent == 0)
                        {
                            find = "ProductId='" + productId + "' AND SourceId = '" + agentSource + "' AND AgentId IS NULL";
                        }
                        else
                        {
                            find = "ProductId='" + productId + "' AND SourceId = '" + agentSource + "' AND AgentId='" + agent + "' AND transType='B2B'";
                        }

                        DataRow[] foundRows = dtMarkupList.Select(find);
                        UpdateMarkup objUpdateMarkup = new UpdateMarkup();
                        if (foundRows.Length > 0)
                        {
                            objUpdateMarkup.Id = Utility.ToInteger(foundRows[0]["MRId"]);
                            objUpdateMarkup.MrDId = Utility.ToInteger(foundRows[0]["MRDId"]);
                        }
                        int agentType = 0;
                        int parentAgentID = 0;
                        if (Settings.LoginInfo.AgentType == AgentType.Agent || Settings.LoginInfo.AgentType == AgentType.BaseAgent)
                        {
                            agentType = AgentMaster.GetAgentType(agent);
                        }
                        if (agentType == (int)AgentType.B2B2B)
                        {
                            parentAgentID = AgentMaster.GetParentId(agent);
                        }
                        else
                        {
                            parentAgentID = Settings.LoginInfo.AgentId;
                        }

                        if (Settings.LoginInfo.AgentType == AgentType.B2B || parentAgentID != Settings.LoginInfo.AgentId)
                        {
                            findB2BMarkup = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId='" + parentAgentID + "' AND transType='B2B'";
                            DataRow[] foundB2BMarkupRows = dtMarkupList.Select(findB2BMarkup);
                            if (foundB2BMarkupRows.Length > 0)
                            {
                                objUpdateMarkup.OurCommission = Utility.ToDecimal(foundB2BMarkupRows[0]["Markup"]);
                            }
                            else
                            {
                                objUpdateMarkup.OurCommission = Utility.ToDecimal("0.00");
                            }
                        }
                        else
                        {
                            objUpdateMarkup.OurCommission = Utility.ToDecimal("0.00");
                        }

                        objUpdateMarkup.ProductId = productId;
                        objUpdateMarkup.SourceId = source;

                        objUpdateMarkup.AgentId = agent;
                        objUpdateMarkup.AgentMarkup = Utility.ToDecimal(txtAgentMarkup.Text);

                        objUpdateMarkup.Markup = (objUpdateMarkup.AgentMarkup + objUpdateMarkup.OurCommission);
                        objUpdateMarkup.MarkupType = ddlMarkUp.SelectedItem.Value;
                        objUpdateMarkup.Discount = Utility.ToDecimal(txtDiscount.Text);
                        objUpdateMarkup.DiscountType = ddlDiscount.SelectedItem.Value;
                        objUpdateMarkup.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
                        objUpdateMarkup.Save();

                    }
                    chkProduct.Items[i].Selected = false;
                }
            }


            ddlAgent.SelectedIndex = 0;
            //ddlAgent.Style.Add("display", "none");
            tblMarkup.Rows.Clear();
            chkProduct.Items.Clear();
            hdfAgentId.Value = "0";
            BindMarkupList();
            if (hdnSourceName.Value == "0")
            {
                lblSuccessMsg.Text = "Updated successfully";
            }
            else
            {
                Utility.Alert(this.Page, "Selected "+hdnSourceName.Value +" source is not assigned to the agent");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
        }
    }
    #endregion
    #region Private Methods
    private void InitializePageControls()
    {
        try
        {
            BindAgent();
            //BindProcduct();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = null;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                dtAgents = AgentMaster.GetList(1, "ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
                DataView dv = dtAgents.DefaultView;
                if (Settings.LoginInfo.MemberType != MemberType.SUPER)
                    dv.RowFilter = "AGENT_ID NOT IN('" + Settings.LoginInfo.AgentId + "')";
                dtAgents = dv.ToTable();
            }
            else
            {
                dtAgents = AgentMaster.GetList(1, "B2B-B2B2B", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            }
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agency--", "0"));
            ddlAgent.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindProcduct()
    {
        try
        {
            Session["dtProducts"] = null;
            dtProducts = UpdateMarkup.ProductGetList(ListStatus.Long, RecordStatus.Activated,Utility.ToInteger(ddlAgent.SelectedItem.Value));
            chkProduct.DataSource = dtProducts;
            chkProduct.DataTextField = "productType";
            chkProduct.DataValueField = "productTypeId";
            chkProduct.DataBind();
            //BindControls();
            //BindMarkupList();
            Session["dtProducts"] = dtProducts;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindControls()
    {
        if (Session["dtProducts"] != null)
        {
            dtProducts = (DataTable)Session["dtProducts"];
        }
        HtmlTableRow hr = new HtmlTableRow();
        for (int i = 0; i < dtProducts.Rows.Count; i++)
        {
            Label lblAgentMarkup = new Label();
            lblAgentMarkup.ID = "lblAgentMarkup_" + i.ToString();
            lblAgentMarkup.Text = "Agent Markup";
            lblAgentMarkup.Width = new Unit(90, UnitType.Pixel);
            lblAgentMarkup.Style.Add("display", "none");
            TextBox txtAgentMarkup = new TextBox();
            txtAgentMarkup.ID = "txtAgentMarkup_" + i.ToString();
            txtAgentMarkup.Text = "0.00";
            txtAgentMarkup.Width = new Unit(80, UnitType.Pixel);
            txtAgentMarkup.Style.Add("display", "none");
            txtAgentMarkup.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'1');");
            txtAgentMarkup.Attributes.Add("onchange", "SetValue();");
            txtAgentMarkup.Attributes.Add("onfocus", "Check(this.id);");
            txtAgentMarkup.Attributes.Add("onBlur", "Set(this.id);");
           
            Label lblmarkUpType = new Label();
            lblmarkUpType.ID = "lblmarkUpType_" + i.ToString();
            lblmarkUpType.Text = "MarkupType";
            lblmarkUpType.Width = new Unit(80, UnitType.Pixel);
            lblmarkUpType.Style.Add("display", "none");
            DropDownList ddlMarkupType = new DropDownList();
            ddlMarkupType.ID = "ddlMarkupType_" + i.ToString();
            ddlMarkupType.Items.Insert(0, new ListItem("Fixed", "F"));
            ddlMarkupType.Items.Insert(0, new ListItem("Percentage", "P"));
            ddlMarkupType.Width = new Unit(80, UnitType.Pixel);
            ddlMarkupType.Style.Add("display", "none");
            HtmlTableCell tc = new HtmlTableCell();
            TextBox txtDiscount = new TextBox();
            txtDiscount.Width = new Unit(80, UnitType.Pixel);
            txtDiscount.ID = "txtDiscount_" + i.ToString();
            txtDiscount.Text = "0.00";
            txtDiscount.Style.Add("display", "none");
            txtDiscount.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'1');");
            txtDiscount.Attributes.Add("onchange", "setToFixedThis(this.id);");
            txtDiscount.Attributes.Add("onfocus", "Check(this.id);");
            txtDiscount.Attributes.Add("onBlur", "Set(this.id);");
            Label lblDiscountType = new Label();
            lblDiscountType.ID = "lblDiscountType_" + i.ToString();
            lblDiscountType.Text = "Disc Type";
            lblDiscountType.Style.Add("display", "none");
            lblDiscountType.Width = new Unit(80, UnitType.Pixel);
            DropDownList ddlDiscountType = new DropDownList();
            ddlDiscountType.ID = "ddlDiscountType_" + i.ToString();
            ddlDiscountType.Style.Add("display", "none");
            ddlDiscountType.Items.Insert(0, new ListItem("Fixed", "F"));
            ddlDiscountType.Items.Insert(1, new ListItem("Percentage", "P"));
            ddlDiscountType.Width = new Unit(80, UnitType.Pixel);

            Label lblDiscount = new Label();
            lblDiscount.ID = "lblDiscount_" + i.ToString();
            lblDiscount.Text = "Discount";
            lblDiscount.Style.Add("display", "none");
            lblDiscount.Width = new Unit(80, UnitType.Pixel);

            if (Convert.ToInt32(dtProducts.Rows[i]["productTypeId"]) != 5)
            {
                txtDiscount.Visible = false;
                lblDiscountType.Visible = false;
                ddlDiscountType.Visible = false;
                lblDiscount.Visible = false;
            }

            if (Convert.ToInt32(dtProducts.Rows[i]["productTypeId"]) == 1)
            {
                Label lblSource = new Label();
                lblSource.ID = "lblSource_" + i.ToString();
                lblSource.Text = "Sources";
                lblSource.Style.Add("display", "none");
                lblSource.Width = new Unit(80, UnitType.Pixel);
                tc.Controls.Add(lblSource);
                DropDownList ddlSource = new DropDownList();
                ddlSource.ID = "ddlSource_" + i.ToString();
                ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
                ddlSource.Items.Insert(1, new ListItem("GDS", "GDS"));
                ddlSource.Items.Insert(2, new ListItem("LCC", "LCC"));
                ddlSource.Width = new Unit(80, UnitType.Pixel);
                ddlSource.Style.Add("display", "none");
                ddlSource.AutoPostBack = true;
                ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
                tc.Controls.Add(ddlSource);
            }
            else if (Convert.ToInt32(dtProducts.Rows[i]["productTypeId"]) == 2)
            {
                Label lblSource = new Label();
                lblSource.ID = "lblSource_" + i.ToString();
                lblSource.Text = "Sources";
                lblSource.Style.Add("display", "none");
                lblSource.Width = new Unit(80, UnitType.Pixel);
                tc.Controls.Add(lblSource);
                DropDownList ddlSource = new DropDownList();
                ddlSource.ID = "ddlSource_" + i.ToString();
                ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
                ddlSource.Items.Insert(1, new ListItem("O.Supplier", "O"));
                ddlSource.Items.Insert(2, new ListItem("C.Supplier", "C"));
                ddlSource.Width = new Unit(80, UnitType.Pixel);
                ddlSource.Style.Add("display", "none");
                ddlSource.AutoPostBack = true;
                ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
                tc.Controls.Add(ddlSource);
            }
            else if (Convert.ToInt32(dtProducts.Rows[i]["productTypeId"]) == 5)
            {
                Label lblSource = new Label();
                lblSource.ID = "lblSource_" + i.ToString();
                lblSource.Text = "Sources";
                lblSource.Style.Add("display", "none");
                lblSource.Width = new Unit(80, UnitType.Pixel);
                tc.Controls.Add(lblSource);
                DropDownList ddlSource = new DropDownList();
                ddlSource.ID = "ddlSource_" + i.ToString();
                ddlSource.DataSource = AgentMaster.GetActiveSources(5);
                ddlSource.DataValueField = "Id";
                ddlSource.DataTextField = "Name";
                ddlSource.DataBind();
                ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
                ddlSource.Width = new Unit(80, UnitType.Pixel);
                ddlSource.Style.Add("display", "none");
                ddlSource.AutoPostBack = true;
                ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
                tc.Controls.Add(ddlSource);
            }
            else
            {
            }
            tc.Controls.Add(lblAgentMarkup);
            tc.Controls.Add(txtAgentMarkup);
            tc.Controls.Add(lblmarkUpType);
            tc.Controls.Add(ddlMarkupType);
            tc.Controls.Add(lblDiscount);
            tc.Controls.Add(txtDiscount);
            tc.Controls.Add(lblDiscountType);
            tc.Controls.Add(ddlDiscountType);
            tc.Width = "75px";
            tc.VAlign = "top";
            hr.Cells.Add(tc);
        }
        tblMarkup.Rows.Add(hr);
    }

    private void LoadControls()
    {
        dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty);
        for (int i = 0; i < chkProduct.Items.Count; i++)
        {
            Label lblAgentMarkup = (Label)tblMarkup.FindControl("lblAgentMarkup_" + i.ToString());
            TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + i.ToString());
            Label lblMarkupType = (Label)tblMarkup.FindControl("lblmarkUpType_" + i.ToString());
            DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + i.ToString());
            Label lblDiscount = (Label)tblMarkup.FindControl("lblDiscount_" + i.ToString());
            TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + i.ToString());
            Label lblDiscountType = (Label)tblMarkup.FindControl("lblDiscountType_" + i.ToString());
            DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + i.ToString());
            Label lblSource = (Label)tblMarkup.FindControl("lblSource_" + i.ToString());
            DropDownList ddlSource = (DropDownList)tblMarkup.FindControl("ddlSource_" + i.ToString());
            int productId = Convert.ToInt32(chkProduct.Items[i].Value);
            if (chkProduct.Items[i].Selected)
            {
                lblAgentMarkup.Style.Add("display", "block");
                txtAgentMarkup.Style.Add("display", "block");
                lblMarkupType.Style.Add("display", "block");
                ddlMarkupType.Style.Add("display", "block");
                lblDiscount.Style.Add("display", "block");
                txtDiscount.Style.Add("display", "block");
                lblDiscountType.Style.Add("display", "block");
                ddlDiscountType.Style.Add("display", "block");
                if (productId < 3 || productId == 5)
                {
                    lblSource.Style.Add("display", "block");
                    ddlSource.Style.Add("display", "block");
                }
                if (productId > 2 && productId != 5)
                {
                    int agent = 0;
                    string find = string.Empty;
                    
                    if (ddlAgent.SelectedItem.Value != "0")
                    {
                        agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                    if (agent == 0)
                    {
                        find = "ProductId='" + productId + "' AND AgentId IS NULL";
                    }
                    else
                    {
                        find = "ProductId='" + productId + "' AND AgentId = '" + agent + "' AND transType='B2B'";
                    }
                    DataRow[] foundRows = dtMarkupList.Select(find);
                    
                    if (foundRows.Length > 0)
                    {
                        txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
                        ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
                        txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
                        ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
                    }
                    else
                    {
                        txtAgentMarkup.Text = "0.00";
                        txtDiscount.Text = "0.00";
                    }
                }
            }
            else
            {
                lblAgentMarkup.Style.Add("display", "none");
                txtAgentMarkup.Style.Add("display", "none");
                lblMarkupType.Style.Add("display", "none");
                ddlMarkupType.Style.Add("display", "none");
                lblDiscount.Style.Add("display", "none");
                txtDiscount.Style.Add("display", "none");
                lblDiscountType.Style.Add("display", "none");
                ddlDiscountType.Style.Add("display", "none");
                if (productId < 3 || productId == 5)
                {
                    lblSource.Style.Add("display", "none");
                    ddlSource.Style.Add("display", "none");
                }
            }
        }
    }
    private void BindMarkupList()
    {
        try
        {
            string agentType = string.Empty;
            int agentId = -1;

            if (ddlAgent.SelectedIndex > 0)
            {
                agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            }
            else
            {
                if (Settings.LoginInfo.AgentType != AgentType.BaseAgent)
                {
                    agentType = "AGENT";
                    agentId = Settings.LoginInfo.AgentId;
                }
            }

            dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, agentId, agentType);
            for (int i = 0; i < dtMarkupList.Rows.Count; i++)
            {
                if (dtMarkupList.Rows[i]["SourceId"].ToString() == "UA")
                {
                    dtMarkupList.Rows[i]["SourceId"] = "GDS";
                }
                else if(dtMarkupList.Rows[i]["productType"].ToString() == "Flight")
                {
                    dtMarkupList.Rows[i]["SourceId"] = "LCC";
                }
                else if (dtMarkupList.Rows[i]["SourceId"].ToString() == "HotelConnect")
                {
                    dtMarkupList.Rows[i]["SourceId"] = "C.Supplier";
                }
                else if (dtMarkupList.Rows[i]["productType"].ToString() == "Hotel")
                {
                    dtMarkupList.Rows[i]["SourceId"] = "O.Supplier";
                }
            }


           #region  Distinct
            dtMarkupList = dtMarkupList.DefaultView.ToTable(true, "productType", "agent_name", "SourceId", "AgentMarkup", "MarkupTypeName");
           
            DataTable dtMarkup = dtMarkupList.Clone();
            int counter = 0;
            for (int i = 0; i < dtMarkupList.Rows.Count; i++)
            {
                if (dtMarkup.Rows.Count > 0)
                {
                    for (int j = 0; j < dtMarkup.Rows.Count; j++)
                    {
                        if (dtMarkupList.Rows[i]["productType"].ToString() == dtMarkup.Rows[j]["productType"].ToString() && dtMarkupList.Rows[i]["agent_name"].ToString() == dtMarkup.Rows[j]["agent_name"].ToString() && dtMarkupList.Rows[i]["SourceId"].ToString() == dtMarkup.Rows[j]["SourceId"].ToString())
                        {
                            counter++;
                            break;
                        }
                    }
                    if (counter == 0)
                    {
                        dtMarkup.Rows.Add(dtMarkupList.Rows[i].ItemArray);
                    }
                    else
                    {
                        counter = 0;
                    }
                }
                else
                {
                    dtMarkup.Rows.Add(dtMarkupList.Rows[i].ItemArray);
                }
            }
            dtMarkupList = dtMarkup;
            #endregion


            if (dtMarkupList != null && dtMarkupList.Rows.Count > 0)
            {
                dlMarkup.Visible = true;
                lblMessage.Visible = false;
                dlMarkup.DataSource = dtMarkupList;
                ViewState["Markup"] = dtMarkupList;
                dlMarkup.DataBind();
                doPaging();
            }
            else
            {
                dlMarkup.Visible = false;
                btnPrev.Visible = false;
                btnFirst.Visible = false;
                btnLast.Visible = false;
                btnNext.Visible = false;
                lblMessage.Visible = true;
                lblMessage.Text = "No Results Found.";
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    void doPaging()
    {
        DataTable pdt = (DataTable)ViewState["Markup"];
        if (pdt != null && pdt.Rows.Count > 0)
        {
            DataTable dt = (DataTable)ViewState["Markup"];
            pagedData.DataSource = dt.DefaultView;
        }
        pagedData.AllowPaging = true;
        pagedData.PageSize = 10;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        dlMarkup.DataSource = pagedData;
        dlMarkup.DataBind();
    }

    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }
    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }
    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();

    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }
    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }
    protected void btnClear_Click(object sender, EventArgs e)//Added for clearing the enter data
    {
        try
        {
            ddlAgent.SelectedIndex = 0;
            ddlAgent.Style.Add("display", "block");
            tblMarkup.Rows.Clear();
            chkProduct.ClearSelection();
           // BindMarkupList();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.Low, 0, ex.Message, "0");
        }
    }
    
    #endregion
}
