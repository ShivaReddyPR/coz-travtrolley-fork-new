<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ViewBookingForTicket" Codebehind="ViewBookingForTicket.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="System.Collections.Generic" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">
    <link href="css/TicketQuee.css" rel="stylesheet" type="text/css" />
  <%--  <link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />--%>
      <%----------------------------------the files for multiple uploads-------------------------------------------------%>
        <link href="DropzoneJs_scripts/dropzone.css" rel="stylesheet" />
    <script src="DropzoneJs_scripts/dropzone.js"></script>
    <%----------------------------------------------------------------------------------------------------------------%>    
       
    <script>

        function Showseatalert(msg) {

            document.getElementById('divSeatsalertText').innerHTML = msg.replace(/@/g, '</br>');
            $('#Seatsalert').modal('show');
        }

        var dropZoneObj;
            var dropZone;

            function InitDropZone() {
                $(".closepnldocs").on('click', function() {
                    $(".pnldocs").hide();
                    $(".pnldrop").hide();
                    if (document.getElementById('lnkRemove') != null) {
                        document.getElementById('lnkRemove').click();
                    }
                });
            
                

                Dropzone.autoDiscover = false;

                dropZoneObj = {
                    url: "hn_GSTFileUploader.ashx",
                    maxFiles: 10,
                    addRemoveLinks: true,
                    success: function(file, response) {
                        var imgName = response;
                        file.previewElement.classList.add("dz-success");
                        console.log("Successfully uploaded :" + imgName);
                    },
                    error: function(file, response) {
                        file.previewElement.classList.add("dz-error");
                    }
                }
                dropZone = new Dropzone('#ctl00_cphTransaction_dZUpload', dropZoneObj);
            }

           
        $(document).ready(function() {
            $(".ssr-info").click(function() {
                $(".ssr-infodiv").toggle();
                change();
            });


            $(".booking-info").click(function() {
                $(".booking-infodiv").toggle();
            });
            InitDropZone();
        });
       
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            InitDropZone();
        });
       
        function change() {
//            if (document.getElementById('ssrDiv').style.display == 'none') {
//                document.getElementById('ssr').innerHTML = "Show SSR Information";
//            }
//            else {
//                document.getElementById('ssr').innerHTML = "Hide SSR Information";
            //            }
            if (document.getElementById('ssrDiv').style.display == "none") {
                document.getElementById('ssrDiv').style.display = "block";
                document.getElementById('ssr').innerHTML = "Hide SSR Information";
            }
            else {
                document.getElementById('ssrDiv').style.display = "none";
                document.getElementById('ssr').innerHTML = "Show SSR Information";
            }
        }


    </script>

    <script type="text/javascript">
        var Ajax;
        function show(id) {
            document.getElementById(id).style.visibility = "visible";
        }
        function hide(id) {
            document.getElementById(id).style.visibility = "hidden";
        }


        function showStuff(id) {
            document.getElementById(id).style.display = 'block';
        }


        function hidestuff(boxid) {
            document.getElementById(boxid).style.display = "none";
        }

        function UpdateStatus(pnr, bookingId) {
            if (window.XMLHttpRequest) {
                Ajax = new XMLHttpRequest();
            }
            else {
                Ajax = new ActiveXObject('Microsoft.XMLHTTP');
            }
            var url = "AjaxUpdateFlightSegmentStatus?pnr=" + pnr + "&bookingId=" + bookingId;
            var id = "UpdateStatMsg";
            var paramList = '';

            //new Ajax.Request(url, {method: 'post', parameters: paramList, onComplete: DisplayMessage});    
            Ajax.onreadystatechange = DisplayMessage;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
            document.getElementById(id).style.display = 'block';
            document.getElementById(id).innerHTML = 'Updating Status ...';
            return false;
        }



        function DisplayMessage() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    var responseVal = Ajax.responseText;
                    var splittedVal = responseVal.split('@');

                    if (splittedVal[0] == 'TRUE') {
                        document.getElementById('UpdateStatMsg').style.display = 'block';
                        document.getElementById('UpdateStatMsg').innerHTML = 'Status Updated';
                        document.getElementById('UpdateStatusBox').style.display = 'none';

                        for (var i = 0; i < document.getElementById('itineraryLength').value; i++) {
                            var splitString = splittedVal[i + 1].split('=');
                            if (document.getElementById('ctl00_cphTransaction_dlTicketDetails_ctl00_FlightKey' + (i + 1)).value == splitString[0]) {
                                if (i <= 1) {
                                    document.getElementById('ctl00_cphTransaction_dlTicketDetails_ctl00_lblOnwardFlightStatus' + (i + 1)).innerHTML = "Status: " + splitString[1];
                                }
                                else if (i <= 3) {
                                    document.getElementById('ctl00_cphTransaction_dlTicketDetails_ctl00_lblReturnFlightStatus' + (i - 1)).innerHTML = "Status: " + splitString[1];
                                }

                                // change background color for few seconds
                                document.getElementById('ctl00_cphTransaction_dlTicketDetails_ctl00_lblReturnFlightStatus' + (i + 1)).style.backgroundColor = "yellow";
                                setTimeout("setBgCol(" + i + ", 1)", 500);
                                setTimeout("setBgCol(" + i + ", 2)", 800);
                                setTimeout("setBgCol(" + i + ", 3)", 1100);
                                setTimeout("setBgCol(" + i + ", 4)", 1400);
                                setTimeout("setBgCol(" + i + ", 5)", 1700);
                            }
                        }
                    }
                    else if (splittedVal[0] == 'FALSE') {
                        document.getElementById('UpdateStatMsg').style.display = 'block';
                        document.getElementById('UpdateStatMsg').innerHTML = splittedVal[1];
                        document.getElementById('UpdateStatusBox').style.display = 'block';
                    }
                }
            }
        }

        function setBgCol(k, timeout) {
            if (timeout == 1) {
                document.getElementById('ctl00_cphTransaction_dlTicketDetails_ctl00_lblOnwardFlightStatus' + k).style.backgroundColor = "#FFFF33";
            }
            if (timeout == 2) {
                document.getElementById('ctl00_cphTransaction_dlTicketDetails_ctl00_lblOnwardFlightStatus' + k).style.backgroundColor = "#FFFF66";
            }
            if (timeout == 3) {
                document.getElementById('ctl00_cphTransaction_dlTicketDetails_ctl00_lblReturnFlightStatus' + (k - 2)).style.backgroundColor = "#FFFF99";
            }
            if (timeout == 4) {
                document.getElementById('ctl00_cphTransaction_dlTicketDetails_ctl00_lblReturnFlightStatus' + (k - 2)).style.backgroundColor = "#FFFFcc";
            }
            //            if (timeout == 5) {
            //                document.getElementById('ctl00_cphTransaction_dlTicketDetails_ctl0' + (k - 1) + '_lblReturnFlightStatus' + k).style.backgroundColor = "#f5f5f5";
            //            }
        }
        function ShowNoCreditNote() {
            document.getElementById('noCreditLeft').style.display = 'block';
            document.getElementById('noCreditLeft').innerHTML = 'Insufficient balance left( <%=CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"]  %> <%=amountToCompare %> ) to make Ticket of this Booking';
            return true;
        }
        function quickReplyOpen(obj, replytextbox, tablename, repliedCommentID, repliedBy) {
            var box = document.getElementById(tablename);
            //<% // shows the quick reply box %>
            //box.style.display = 'block';
            // set focus on the textarea
            var text = document.getElementById(replytextbox);
            text.value = "";
            text.focus();
            document.getElementById('<%= txtComments.ClientID %>').value = "";
            document.getElementById('replyCommentID').value = repliedCommentID;
            document.getElementById('CommentDetail').value = document.getElementById(replytextbox).value;
            document.getElementById('editedCommentID').value = "";
            document.getElementById('CommentBlock').style.display = "block";

        }
        function replyCloseAll(tableName) {
            var text = document.getElementById(tableName);
            text.style.display = "none";
            return false;
        }

        function SaveComment(replytextbox) {
            if (document.getElementById(replytextbox).value == "") {
                alert("Please Enter Some Comment");
                document.getElementById(replytextbox).focus();
                return false;
            }
            else {
                document.getElementById('CommentDetail').value = document.getElementById(replytextbox).value;
                var pars = "action=ReplyComment&reply=" + document.getElementById('CommentDetail').value + "&replyCommentID=" + document.getElementById('replyCommentID').value + "&bookingId=" + document.getElementById('<%=hdnBookingId.ClientID %>').value;
                var url = "AjaxHandler";
                new Ajax.Request(url, { method: 'post', parameters: pars, onComplete: ReplyCommentComplete });
            }
        }

        function ReplyCommentComplete(response) {
            document.getElementById('CommentBlock').style.display = "block";
            document.getElementById('ShowHideComment').title = "Click To Hide Comment";
            document.getElementById('ShowHideComment').innerHTML = "Click To Hide Comment";
            location.href = "ViewBookingForTicket.aspx?bookingId=" + document.getElementById('<%=hdnBookingId.ClientID %>').value + "&CommentSave=True";
        }

        function editComment(obj, editBox, tablename, commentID, index) {
            var box = document.getElementById(tablename);
            //shows the quick reply box
            //box.style.display = 'block';
            var commentDescription = document.getElementById('commentDescription-' + index).innerHTML;
            // print comment on reply text box and focus on that box    
            document.getElementById(editBox).value = commentDescription;
            document.getElementById(editBox).focus();
            document.getElementById('editedCommentID').value = commentID;
            document.getElementById('replyCommentID').value = "";
            document.getElementById('CommentBlock').style.display = "block";
        }
        function ShowComment(commentCount) {

            if (commentCount > 0) {
                if (document.getElementById('CommentBlock').style.display == "none") {
                    document.getElementById('CommentBlock').style.display = "block";
                    document.getElementById('ShowHideComment').title = "Click To Hide Comment";
                    document.getElementById('ShowHideComment').innerHTML = "Hide Comments";
                }
                else {
                    document.getElementById('CommentBlock').style.display = "none";
                    document.getElementById('ShowHideComment').title = "Click To Show Comment";
                    document.getElementById('ShowHideComment').innerHTML = "Show Comments";
                }
            }
            else {
                document.getElementById('CommentBlock').style.display = "none";
            }
        }
        function CancelTicket(ticketId, price) {
            document.getElementById('TicketPrice').innerHTML = price;
            document.getElementById('TotalRefundedAmount').innerHTML = price;
            document.getElementById('surchargePop').style.display = 'block';
            document.getElementById('RefundTicket').style.display = 'block';
            document.getElementById('Remarks').style.display = 'block';
            document.getElementById('Remarks').value = '';
            document.getElementById('RemarkText').style.display = 'block';
            document.getElementById('AdminFee').disabled = '';
            document.getElementById('SupplierFee').disabled = '';
            //document.getElementById('AdminFee').value = '0';
            document.getElementById('SupplierFee').value = '0';
            document.getElementById('TicketStatus').style.display = 'block';
            document.getElementById('StatusString').innerHTML = '';
            cancelledTicketId = ticketId;
        }
        function RefundedTicket(ticketId, price, status) {
            document.getElementById('TicketPrice').innerHTML = price;
            document.getElementById('TotalRefundedAmount').innerHTML = price;
            document.getElementById('surchargePop').style.display = 'block';
            document.getElementById('AdminFee').disabled = 'disabled';
            document.getElementById('SupplierFee').disabled = 'disabled';
            document.getElementById('RefundTicket').style.display = 'none';
            document.getElementById('Remarks').style.display = 'none';
            document.getElementById('RemarkText').style.display = 'none';
            document.getElementById('RefundedAmountText').style.display = 'none';
            document.getElementById('TicketStatus').style.display = 'none';
            var url = "BookingAjax";
            var paramList = 'RefundedAmount=true';
            paramList += '&ticketId=' + ticketId;
            paramList += '&bookingId=' + document.getElementById('bookingId').value;

            new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: DisplayRefundpop });
        }
        function DisplayRefundpop(response) {
            var chargeAmount = response.responseText.split(",");
            document.getElementById('AdminFee').value = Math.round(eval(chargeAmount[0]));
            document.getElementById('SupplierFee').value = Math.round(eval(chargeAmount[1]));
            document.getElementById('StatusString').innerHTML = 'Ticket is ' + chargeAmount[2];
            document.getElementById('RefundedAmountText').style.display = 'block';
            document.getElementById('TotalRefundedAmount').innerHTML = Math.round((eval(document.getElementById('TicketPrice').innerHTML) - (eval(document.getElementById('AdminFee').value) + eval(document.getElementById('SupplierFee').value))) * 100) / 100;

        }
        function HideDiv() {
            document.getElementById('surchargePop').style.display = 'none';
        }
        function SetRefundedAmount(box) {
            if (isNaN(box.value)) {
                box.style.border = "solid 1px #FF0000";
                box.value = 0;
                box.focus();
                return;
            }
            if (box.value == '') {
                box.value = "0";
            }
            box.value = Math.round(box.value);
            document.getElementById('TotalRefundedAmount').innerHTML = Math.round((eval(document.getElementById('TicketPrice').innerHTML) - (eval(document.getElementById('AdminFee').value) + eval(document.getElementById('SupplierFee').value))) * 100) / 100;
            if (eval(document.getElementById('TotalRefundedAmount').innerHTML) < 0) {
                document.getElementById('ErrorLabel').innerHTML = "Total refunded amount can not be negative";
                box.style.border = "solid 1px #FF0000";
                box.value = 0;
                box.focus();
                return;
            }
            else {
                box.style.border = "solid 1px #A4B97F";
                document.getElementById('ErrorLabel').innerHTML = "";
            }
        }
        function ViewFare() {
            document.getElementById('divFareRules').style.display = 'block';
        }

        function FareRuleHide() {
            document.getElementById('divFareRules').style.display = 'none';
        }
        var bkgId;
        function Loading(bookingId) {
            bkgId = bookingId;
            //document.getElementById('TicketDiv').style.display = "block";
            $('#TicketDiv').modal('show')
            var html = document.getElementById('TicketDiv').innerHTML;
//            if (confirm("Do you want to proceed creating ticket.!")) {
//                var finalurl = "CreateTicket.aspx?bookingId=" + bookingId + "&fromAgent=true";
//                document.getElementById('PreLoader').style.display = "block";
//                document.getElementById('MainDiv').style.display = "none";
//                document.getElementById('divPrg').style.display = "none";
//                document.getElementById('<%=btnCreateTicket.ClientID %>').enabled = false;
//                //Detecting only IE Browser..
//                if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
//                    document.getElementById('loadingDivision').innerHTML = document.getElementById('loadingDivision').innerHTML;
//                }
//                window.location = finalurl;
//            }
            return false;
        }

        /* To reprice and proceed for ticketing */
        function ContinueTicket() {

            $('#TicketDiv').modal('hide');
            document.getElementById('ctl00_upProgress').style.display = 'block';
            setTimeout(function () { if (Reprice()) { CreateTicket(); } }, 100);                                  
        }
        
        /* This method is used in Create Ticket Popup for sending request to Create Ticket page */
        function CreateTicket() {

            $('#divRepriceAlert').modal('hide');
            var finalurl = "CreateTicket?bookingId=" + bkgId + "&fromAgent=true";
            //For Credit Card we are passing the parameter 'mode'=Card
            if (document.getElementById('<%=ddlPaymentType.ClientID %>').value == "Card") {
                finalurl += "&mode=Card&pg=" + document.getElementById('<%=hdnSelectedPG.ClientID%>').value;
            }
            document.getElementById('PreLoader').style.display = "block";
            document.getElementById('MainDiv').style.display = "none";
            document.getElementById('divPrg').style.display = "none";
            document.getElementById('<%=btnCreateTicket.ClientID %>').enabled = false;
            //Detecting only IE Browser..
            if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
                document.getElementById('loadingDivision').innerHTML = document.getElementById('loadingDivision').innerHTML;
            }
            //Hide the existing div and show awaiting confirmation div
            document.getElementById('MainDiv').style.display = "none";
            document.getElementById('PreLoader').style.display = "block";
            window.location = finalurl;
        }
        //This method is used to close the Create Ticket popup
        function CancelTicket() {
            //document.getElementById('TicketDiv').style.display = "none";  
            $('#TicketDiv').modal('hide')
        }
        function CardChargeEnable() {
            FillCharges();//==================================
            if (document.getElementById('divAgentPG') != null && document.getElementById('divCardCharge') != null) {
                if (document.getElementById('ctl00_cphTransaction_ddlPaymentType').value == "Card") {

                    document.getElementById('divCardCharge').style.display = "block";
                    document.getElementById('lblCardCharges').innerHTML = parseFloat(eval('<%=charges %>')).toFixed(eval('<%=agency.DecimalValue %>'));

                    //Added by somasekhar on 02/07/2018                
                    document.getElementById('divAgentPG').style.display = "block";
                }
                else {
                    document.getElementById('divCardCharge').style.display = "none";
                    //document.getElementById('lblCardCharges').innerHTML = "";

                    //Added by somasekhar on 02/07/2018                
                    document.getElementById('divAgentPG').style.display = "none";
                }
            }
        }
        function FillCharges() {  // Added by Suresh
 
            var bookingAmount = document.getElementById("<%=hdnBookingAmount.ClientID%>").defaultValue;
            var radioButtons = document.getElementById("<%=rblAgentPG.ClientID%>");
            if (radioButtons != null) {
                var inputs = radioButtons.getElementsByTagName("input");
                var labels = radioButtons.getElementsByTagName("label");
                var selected;
                for (var x = 0; x < inputs.length; x++) {
                    var totalamount = bookingAmount.replace(/\,/g, '');
                    if (inputs[x].checked) {
                        var charge = inputs[x].value;
                        document.getElementById('<%=hdnSelectedPG.ClientID%>').value = labels[x].innerHTML;
                        document.getElementById('lblCardCharges').innerHTML = parseFloat(charge).toFixed(eval('<%=agency.DecimalValue %>'));
                        var currentCode = document.getElementById("ctl00_cphTransaction_dlSaleSummaryDetails_ctl00_lblTotal").innerHTML.split(' ')[1];
                        if (charge != 0) {
                            var amount = parseFloat(totalamount) + (parseFloat(totalamount) * (parseFloat(charge) / 100));
                            document.getElementById("ctl00_cphTransaction_dlSaleSummaryDetails_ctl00_lblTotal").innerHTML = amount.toLocaleString('en') + " " + currentCode;
                        }
                    }
                }

                if (selected) {
                    alert(selected.value);
                }
            }
        }//==============================
        function  ShowReportingFields(){

             if (document.getElementById('ReportingFieldsDiv').style.display == "none") {
                document.getElementById('ReportingFieldsDiv').style.display = "block";
                document.getElementById('ShowReportingFields').innerHTML = "Hide Reporting Fields";
            }
            else {
                document.getElementById('ReportingFieldsDiv').style.display = "none";
                document.getElementById('ShowReportingFields').innerHTML = "Show Reporting Fields";
            }
             
        }        

        function Reprice() {

            var repriceresp = true;

            <%if (itinerary.FlightBookingSource == BookingSource.UAPI) {%>

                var PNR = '<%=itinerary.PNR%>';
                var FlightId = '<%=itinerary.FlightId%>';

                $.ajax({
                    type: "POST",
                    url: "ViewBookingForTicket.aspx/RepriceBeforeTicketing",
                    contentType: "application/json; charset=utf-8",
                    data: "{'sPNR':'" + PNR + "', 'sFlightId':'" + FlightId + "'}",
                    dataType: "json",
                    async: false,
                    success: function (data) {

                        if (data != null && data != 'undefined' && data.d.Message != '' && data.d.Message != null) {

                            if (data.d.Status == '8') {// If Price Changed
                                document.getElementById('divRepricefooter').style.display = 'block';
                                document.getElementById('divPriceAlert').style.display = 'block';
                                document.getElementById('lblOrigPrice').innerHTML = data.d.Message.split('-')[0];
                                document.getElementById('lblNewPrice').innerHTML = data.d.Message.split('-')[1];
                                document.getElementById('divAlertMessage').innerHTML = '';
                            }
                            else {
                                document.getElementById('divRepricefooter').style.display = 'none';
                                document.getElementById('divPriceAlert').style.display = 'none';
                                document.getElementById('lblOrigPrice').innerHTML = '';
                                document.getElementById('lblNewPrice').innerHTML = '';
                                document.getElementById('divAlertMessage').innerHTML = data.d.Message;
                            }
                            
                            $('#divRepriceAlert').modal('show');
                            repriceresp = false;
                        }
                    },
                    error: (error) => {
                        console.log(JSON.stringify(error));
                    }
                });
            
            <%}%>

            document.getElementById('ctl00_upProgress').style.display ='none';
            return repriceresp;
        }

        function CancelPNR() {
            $('#divRepriceAlert').modal('hide');
        }
    </script>
    <div id="MainDiv">
        <asp:HiddenField ID="hdnBookingAmount" runat="server" />
        <asp:HiddenField ID="hdnSelectedPG" runat="server" />
  <div class="modal fade pymt-modal" data-backdrop="static" id="TicketDiv" tabindex="-1" role="dialog" aria-labelledby="TicketDivLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Create Ticket</h4>
              </div>
              <div class="modal-body">
<table width="100%">
            <tr>
                <td>
                    Payment mode:
                </td>
                <td>
                    <asp:DropDownList ID="ddlPaymentType" runat="server" Width="100px" onchange="CardChargeEnable()">
                        <asp:ListItem Selected="True" Text="On Account" Value="Credit"></asp:ListItem>
                        <asp:ListItem Text="Credit Card" Value="Card"></asp:ListItem>
                    </asp:DropDownList>
                   
                </td>
            </tr>
            <tr>
                <td colspan="2">
                     <%if (itinerary.PaymentMode == ModeOfPayment.CreditCard)
                        { %>
                                                                    <div id="divAgentPG" margin-bottom: 10px">
                                                                        Payment Gateways
                                                                <asp:RadioButtonList ID="rblAgentPG" runat="server" onchange="return FillCharges()">
                                                                    <%--  OnSelectedIndexChanged="rblAgentPG_SelectedIndexChanged" AutoPostBack="true" onchange=" return CardChargeEnable()">--%>
                                                                </asp:RadioButtonList>
                                                                        <div id="pgValidation" style="color: red; display: none; width: 100%;">
                                                                            Please select one of the payment gateways.
                                                                        </div>
                                                                    </div>
                                                                    

                                                                    <div style="display: none; color: Red; font-weight: bold;" id="divCardCharge">
                                                                        Credit Card Charges
                                                                <label id="lblCardCharges" style="color: Red; font-weight: bold;">
                                                                </label>
                                                                % applicable on Total Amount
                                                                        <%} %>
                </td>
            </tr>
            <tr>
            <td colspan="2">
            &nbsp;
            </td>
            </tr>
            <tr>
                <td align="right">
                    <input type="button" id="btnContinue" value="Continue" class="button-normal" onclick="ContinueTicket()" />&nbsp;
                </td>
                <td align="left">                    
                   &nbsp;<input type="button" id="btnCancel" value="Cancel" class="button-normal" onclick="CancelTicket()" />
                </td>
            </tr>
        </table>
    </div>
                           </div>
          </div>
        </div>
<div> 

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="30">
      <a href="AgentBookingQueue.aspx">&lt;&lt; Back To Booking Queue</a></td>

     
    <td align="right">Booking Date:  </td>
    <td> <b><asp:Label ID="lblBookingDate" runat="server" Text=""></asp:Label> </b></td>
    <td> <b><asp:Label ID="lblBookingStatus" runat="server" Text=""></asp:Label></b></td>
  </tr>
  <tr>
      <%-- Newly Added Content for Corporate Module --%> 
      <td></td>
      <%-- END: Newly Added Content for Corporate Module --%>


    <td > <b>
                        <asp:Label ID="lblBookingAgent" runat="server" Text=""></asp:Label></b> (Booked
                    by:
                    <asp:Label ID="lblBookedBy" runat="server" Text=""></asp:Label>) </td>
    <td align="right">Travel Date: </td>
    <td><b><asp:Label ID="lblTravelDate" runat="server" Text=""></asp:Label></b></td>
    <td> PNR: <span style="color: Red">
                        <asp:Label ID="lblPNR" runat="server" Text=""></asp:Label>
                    </span></td>
  </tr>
  <tr style="display:none">
  <td>
  <b><asp:Label ID="lastTicketingDate" ForeColor="Red" runat="server" Text="Last Ticketing Date:" Visible="false"></asp:Label> </b>
  <asp:Label ID="lastTicketingDateValue" ForeColor="Red" runat="server" Visible="false"></asp:Label>
  </td>
  </tr>
</table>







</div>

    <div class="view-book-window" title="FareRules" id="divFareRules" style="position:absolute; display:none;left:200px;top:200px;  
        
         width:500px " >
        
        <table style="background-color:White" border="0">
        
         <asp:DataList ID="ddlFareRules" runat="server" OnItemDataBound="ddlFareRules_ItemDataBound">
          <ItemTemplate>
          <tbody>
          <tr>
          <td valign="top">
          <label class="fright"><a style="color:white; font-family:Verdana; font-weight:normal; font-size:18px; text-decoration:none" href="#" onclick="FareRuleHide()">X</a></label>
                           
                           
                           
                            <div class="box_b2bdesign" style="background:#fff;height:200px; width: 480px;
                                overflow:auto; padding:10px; " >
                                
                                <div style=" position:absolute; left:10px; top:0px; color:White;">  <label class="head_bg"><strong>Fare Rules</strong></label></div>
                                 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                           <asp:Label ID="lblFareType" runat="server" Text=""></asp:Label> 
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblFareRuleKey1" runat="server" Text=""></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblFareRuleDetail1" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblFareRuleKey2" runat="server" Text=""></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblFareRuleDetail2" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblFareRuleKey3" runat="server" Text=""></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblFareRuleDetail3" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                <asp:Label ID="lblFareRuleKey4" runat="server" Text=""></asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblFareRuleDetail4" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
          </tr>
          </tbody>
          </ItemTemplate>
         </asp:DataList>
        </table>
    </div>




<div>


<asp:HiddenField ID="hdnStop" runat="server" Value="0" />
 <td>
                                                                <a href="#" onclick="javascript:ViewFare(this.id);" style="display:none">View FareRules </a>
                    
                    <!--Start: New Data List items -->                                        </td>
               <div class="col-md-12">
                 <!--Start: Flight Details -->
                 <div class="col-md-10 pad_left0">
       <div class="ns-h3"> <strong> Flight Details</strong> </div> 
       
                                               
      
      
           <asp:DataList ID="dlTicketDetails1" runat="server" Width="100%" OnItemDataBound="dlTicketDetails1_ItemDataBound">
            <ItemTemplate>
                               <div class="bg_white pad_10"> 
                 
                                <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Image ID="imgFlightCarrierCode" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFlightOriginCode" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFlightDestinationCode" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFlightDuration" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblOnFlightWayType" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            
                                                                                                                  
                                                        </tr>
                                                           <tr>
                                                            <td>
                                                                <asp:Label ID="lblFlightCode" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFlightDepartureDate" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblFlightArrivalDate" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td height="14px" colspan="4">
                                                                <hr class="b_bot_1" />
                                                            </td>
                                                        </tr>
                                                      
                                                    </table>
                               </div>    
                                 
                           
               
               
               
               
               
                    
                    
               
            </ItemTemplate>
        </asp:DataList>
                                
                                                                          
                      </div> 
                       <!--End: Flight Details -->   
                   
                   
                   <div class="col-md-2 padding-0">
       <div class="ns-h3"> <strong> Sale Summary</strong> </div> 
       
       
       
        <asp:DataList ID="dlSaleSummaryDetails" runat="server" Width="100%" OnItemDataBound="dlSaleSummaryDetails_ItemDataBound">
            <ItemTemplate>
                       
              
               <div class="bg_white marbot_20 pad_10"> 
                               
                                
                                
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                
                                
                                    <tr>
                                        <td>
                                            <%--<a onclick="Show()" href="#">Show Fare Details</a>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <span class="fleft">Base Fare </span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblBaseFare" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="fleft width-50">Tax</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblTax" runat="server" Text=""></asp:Label></span>
                                                    </td>
                                                </tr>
                                               <%if(isOneToOneSearch){%>
                                                <tr>
                                                    <td>
                                                        <span class="fleft width-50">K3 Tax</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblK3Tax" runat="server" Text=""></asp:Label></span>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <tr style="display:none;">
                                                    <td>
                                                        <span class="fleft">Service Fee</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblServiceFee" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                     <tr>
                                                    <td>
                                                        <span class="fleft width-50">Baggage</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblBaggage" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="fleft width-50">Meal</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblMeal" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="fleft width-50">Seat</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblSeat" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <%if (location.CountryCode == "IN")
                                                            { %>
                                                        <span class="fleft width-50">TotalGST</span>
                                                        <%}else { %>
                                                        <span class="fleft width-50">VAT</span>
                                                        <%} %>
                                                        
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblVAT" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <%if(discount > 0){ %>
                                                 <tr>
                                                    <td>
                                                        <span class="fleft width-50">Discount(-)</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <%=discount.ToString("N"+agency.DecimalValue) %> <%=agency.AgentCurrency %>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <%} %>
                                              <%--  <tr>
                                                    <td>
                                                        <span class="fleft width-50">Total</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft" style="width: 123px;">Adult x
                                                            <asp:Label ID="lblAdultCount" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                  <%--  <tr>
                                                        <td>
                                                            <div>
                                                                Adult x
                                                                <asp:Label ID="lblAdultCount1" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </td>
                                                    </tr>--%>
                                                    <tr>
                                                        <td align="right">
                                                            <div>
                                                                <asp:Label ID="lblTotalForAdult" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    
                                                                    <tr> 
                                                                    
                                                                    <td colspan="2">
                                                                  <hr class="b_bot_1 ">
                                                                    
                                                                    </td>
                                                                    
                                                                    </tr>
                                                                    
                                                                    <tr height="30px">
                                                                        <td align="left">
                                                                            <b>Total</b>
                                                                        </td>
                                                                        <td align="right">
                                                                            <b>
                                                                            <span class="text-right">
                                                                                <asp:Label ID="lblTotal"  runat="server" Text=""></asp:Label>
                                                                                </span>
                                                                            </b>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                           
                                        </td>
                                    </tr>
                                </table>
                                
                                
                                
                                 </div>
               
               
                    
                    
               
            </ItemTemplate>
        </asp:DataList>
       
       
       
       </div>
                 
           
              
                   
                                                              
              </div>   
            <!--End: New Data List items -->   
                                                         
                                                            
     <!--Start: Old Data List items -->      
    <div class="col-md-12" style="display:none;">
                                                            
<table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <asp:DataList ID="dlTicketDetails" runat="server" Width="100%" OnItemDataBound="dlTicketDetails_ItemDataBound">
            <ItemTemplate>
               
               
               
               <div>
               
               
               <div class="col-md-10 pad_left0">
                <div class="ns-h3"> <strong> Flight Details</strong> </div> 
                <div class="bg_white marbot_20 pad_10"> 
                 
                                <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td >
                                                                <asp:Image ID="imgOnCarrier1" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnOrigin" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnDestination" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnDuration" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td rowspan="1">
                                                                <asp:Label ID="lblOnAircraft" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblOnRefundable" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                           
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblOnCarrier" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnDepAirport" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblOnDepTerminal" runat="server" Font-Bold="True"></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblOnArrAirport" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblOnArrTerminal" runat="server" Font-Bold="True"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnWayType" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblOnFlightCode" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnDepartureDate" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblOnArrivalDate" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr id="OnStopDep1" runat="server" visible="false">
                                                            <td >
                                                                &nbsp;<asp:Image ID="imgOnCarrier2" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnOrigin1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnDestination1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnDuration1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td rowspan="1">
                                                                <asp:Label ID="lblOnAircraft1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblOnRefundable1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr id="OnStopArr1" runat="server" visible="false">
                                                            <td>
                                                                <asp:Label ID="lblOnCarrier1" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnDepAirport1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblOnDepTerminal1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblOnArrAirport1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblOnArrTerminal1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnWaytype1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr id="OnStopTime1" runat="server" visible="false">
                                                            <td>
                                                                <asp:Label ID="lblOnFlightCode1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnDepartureDate1" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblOnArrivalDate1" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr id="OnStopDep2" runat="server" visible="false">
                                                            <td >
                                                                <asp:Image ID="imgOnCarrier3" runat="server" />
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnOrigin2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnDestination2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnDuration2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td rowspan="1">
                                                                <asp:Label ID="lblOnAircraft2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblOnRefundable2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr id="OnStopArr2" runat="server" visible="false">
                                                            <td>
                                                                <asp:Label ID="lblOnCarrier2" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnDepAirport2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblOnDepTerminal2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblOnArrAirport2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblOnArrTerminal2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnWaytype2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr id="OnStopTime2" runat="server" visible="false">
                                                            <td>
                                                                <asp:Label ID="lblOnFlightCode2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnDepartureDate2" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblOnArrivalDate2" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td height="14px" colspan="4">
                                                                <hr class="b_bot_1 " />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td >
                                                                <asp:Image ID="imgRetCarrier1" runat="server" />
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetOrigin1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetDestination1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetDuration1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td rowspan="1">
                                                                <asp:Label ID="lblRetAircraft1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblRetRefundable1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblRetCarrier1" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetDepAirport1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblRetDepTerminal1" runat="server" Font-Bold="True"></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblRetArrAirport1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblRetArrTerminal1" runat="server" Font-Bold="True"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetWayType1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblRetFlightCode1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetDepartureDate1" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblRetArrivalDate1" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr id="RetStopArr1" runat="server" visible="false">
                                                            <td >
                                                                <asp:Image ID="imgRetCarrier2" runat="server" />
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetOrigin2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetDestination2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetDuration2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td rowspan="1">
                                                                <asp:Label ID="lblRetAircraft2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblRetRefundable2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr id="RetStopDep1" runat="server" visible="false">
                                                            <td>
                                                                <asp:Label ID="lblRetCarrier2" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetDepAirport2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblRetDepTerminal2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblRetArrAirport2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblRetArrTerminal2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetWaytype2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr id="RetStopTime1" runat="server" visible="false">
                                                            <td>
                                                                <asp:Label ID="lblRetFlightCode2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetDepartureDate2" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblRetArrivalDate2" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr id="RetStopArr2" runat="server" visible="false">
                                                            <td >
                                                                <asp:Image ID="imgRetCarrier3" runat="server" />
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetOrigin3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetDestination3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetDuration3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td rowspan="1">
                                                                <asp:Label ID="lblRetAircraft3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblRetRefundable3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr id="RetStopDep2" runat="server" visible="false">
                                                            <td>
                                                                <asp:Label ID="lblRetCarrier3" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetDepAirport3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblRetDepTerminal3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblRetArrAirport3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblRetArrTerminal3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetWaytype3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr id="RetStopTime2" runat="server" visible="false">
                                                            <td >
                                                                <asp:Label ID="lblRetFlightCode3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRetDepartureDate3" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblRetArrivalDate3" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                       
                                                    </table>
                               </div>    
                                 
                           
                </div>
               
               
               
               
                    
                    <div class="col-md-2 padding-0">
                    
                   
                                
                                 <div  class="ns-h3">
                                                Sale Summary</div>
                                
                                
                                  <div class="bg_white marbot_20 pad_10"> 
                               
                                
                                
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                
                                
                                    <tr>
                                        <td>
                                            <%--<a onclick="Show()" href="#">Show Fare Details</a>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <span class="fleft">Base Fare </span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblBaseFare" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="fleft width-50">Tax</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblTax" runat="server" Text=""></asp:Label></span>
                                                    </td>
                                                </tr>
                                                <tr style="display:none;">
                                                    <td>
                                                        <span class="fleft">Service Fee</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblServiceFee" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                     <tr>
                                                    <td>
                                                        <span class="fleft width-50">Baggage</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblBaggage" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="fleft width-50">Meal</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblMeal" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="fleft width-50">VAT</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblVAT" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                  <%if(isOneToOneSearch){%>
                                                <tr>
                                                    <td>
                                                        <span class="fleft width-50">K3 Tax</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft text-right" style="width: 123px;">
                                                            <asp:Label ID="lblK3Tax" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <%}%>
                                              <%--  <tr>
                                                    <td>
                                                        <span class="fleft width-50">Total</span>
                                                    </td>
                                                    <td align="right">
                                                        <span class="fleft" style="width: 123px;">Adult x
                                                            <asp:Label ID="lblAdultCount" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                  <%--  <tr>
                                                        <td>
                                                            <div>
                                                                Adult x
                                                                <asp:Label ID="lblAdultCount1" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </td>
                                                    </tr>--%>
                                                    <tr>
                                                        <td align="right">
                                                            <div>
                                                                <asp:Label ID="lblTotalForAdult" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    
                                                                    <tr> 
                                                                    
                                                                    <td colspan="2">
                                                                  <hr class="b_bot_1 ">
                                                                    
                                                                    </td>
                                                                    
                                                                    </tr>
                                                                    
                                                                    <tr height="30px">
                                                                        <td align="left">
                                                                            <b>Total</b>
                                                                        </td>
                                                                        <td align="right">
                                                                            <b>
                                                                            <span class="text-right">
                                                                                <asp:Label ID="lblTotal"  runat="server" Text=""></asp:Label>
                                                                                </span>
                                                                            </b>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                           
                                        </td>
                                    </tr>
                                </table>
                                
                                
                                
                                 </div>
                            
                     
                     
                     </div>
                     
                     
                     
                     <div class="clearfix"></div>
                     
                     </div>
               
            </ItemTemplate>
        </asp:DataList>
                                
                                </table>
</div>

 <!--Start: Old Data List items -->      

</div>


<div> 


 
 </div>
 
   <div> 
                                                           <div class="col-md-6 pad_left0 pad_xs0"> 
                                                            
                                                                
                                                                
                                                                <div class="ns-h3">  <strong>Passenger Details</strong>
                                                                
                                                                  
                                                                  <label class="pull-right"> 
                                                                  
                                                                    <%if (booking.Status == BookingStatus.Ticketed)
                                                                               {%>
                                                       <a style="color:#fff; padding-right:10px;" href="ETicket.aspx?FlightId=<%=itinerary.FlightId%>" target="_blank">View All Tickets</a>  
                                                                          <%}
                                                                               else if (booking.Status == BookingStatus.Hold || booking.Status == BookingStatus.Ready)
                                                                               { %>
                                                                          <a href="ETicket.aspx?FlightId=<%=itinerary.FlightId%>&bkg=H" style="color:#fff; padding-right:10px;" target="_blank">View All Itinerary</a>  
                                                                          <%} %>
                                                                  
                                                                  </label>
                                                                  
                                                                  
                                                                  </div>
                                                                  
                                                                  <div class="bg_white bor_gray pad_10">
                                                                  
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                <td height="30"><B>Passenger Name</B></td>
                                <td><B>Type</B></td>
                                <td><B>DOB</B></td>
                                <td></td>
                                </tr>
                                            <%  for (int i = 0; i < itinerary.Passenger.Length; i++)
                        {   %>
                              
                                <tr>
                                <td height="30"> <% = (i + 1)%>.
                            <% = itinerary.Passenger[i].FullName%></td>
                                <td> <% = itinerary.Passenger[i].Type.ToString()%></td>
                                <td><% if (itinerary.Passenger[i].DateOfBirth > DateTime.MinValue)
                               { %>
                            <% = itinerary.Passenger[i].DateOfBirth.ToString("dd/MM/yyyy")%>
                            <% }
                               else
                               {%>
                                   --
                               <% }
                            %></td>
                              
                                
             
                           
                    
                  <td>
                   
                  
                        
                        
                        
                    
                        <%
                            if (booking.Status == BookingStatus.Ticketed || booking.Status == BookingStatus.ModificationInProgress ||booking.Status == BookingStatus.Refunded || booking.Status == BookingStatus.RefundInProgress)
                          {
                                
                              int ticketId = Ticket.GetTicketIdForPax(itinerary.Passenger[i].PaxId);
                              if (ticketId > 0)
                              {
                                  Ticket ticket = new Ticket();
                                  ticket.Load(ticketId);
                                  decimal ticketPrice = Math.Round(ticket.Price.GetAgentPrice(), 2);                                    
                         %>
                         <div class="">
                            <a href="ETicket.aspx?ticketId=<%=ticketId%>&paxId=<%=i%>" target="_blank">View Ticket</a>
                        <%          if (ticket.Status == "OK" )
                                    {
                                  %>
                            <a style="display:none" id="CancelLink-<%=ticketId%>" href="javascript:CancelTicket('<%=ticketId%>','<%=ticketPrice%>')">Void/Cancel Ticket</a>                           
                             
                        <%      }       %>
                        <% if (Settings.LoginInfo.MemberType == MemberType.ADMIN && 1==2) %>      
                        <% 
                           { %>                        
                            <a id="RefundedLink-<%= ticketId %>" href="javascript:RefundedTicket('<%=ticketId%>','<%=ticketPrice%>','<%= ticket.Status %>')" style="display:none;">Refunded Ticket</a>                        
                            <% if (ticket.Status != "OK") %>
                            <% { %>
                            <script type="text/javascript" language="javascript">
                                document.getElementById('RefundedLink-'+<%= ticketId %>).style.display='block';
                            </script>
                        
                        <% } %>
                        
                        
                        <% } %>
                         </div>
                        
                        <%      }
                                else if (pendingId > 0 &&( itinerary.FlightBookingSource!=BookingSource.Amadeus && itinerary.FlightBookingSource!=BookingSource.Galileo && itinerary.FlightBookingSource!=BookingSource.WorldSpan))
                                {
                                  %>
                
                                <div class="">
                            </div>
                            <a class="" href="ETicket.aspx?pendingId=<%=pendingId%>&paxId=<%=i%>" target="_blank">View Ticket</a>
                                    
                              
                               <%}
                                 else
                                 {%>
                               <a  href="javascript:ViewTicket('<%= itinerary.FlightBookingSource %>','<%=itinerary.PNR%>','<%=itinerary.Passenger[i].LastName%>');"> View Ticket</a>
                         <%}
                          }
                            else if (booking.Status == BookingStatus.Hold)
                            {%>
                       <a class="" href="ETicket.aspx?flightId=<%=itinerary.FlightId%>&paxId=<%=i%>&bkg=H" target="_blank">View Itinerary</a>
                       <%}
                   }%>
                   </td>
                     </tr>
                    
                    </table>
                                                                  
                                                                  
                                                                  </div>
                                                                  
                                                                  
                                                                  
                                                                  
                                                                  
                                                                  <div class="clearfix"></div>
                                                                
                                                                </div>
                                                    
                                                    
                                                    
                                                    
                                                    <div class="col-md-6 padding-0">
                                                    
                                                    
                                                     <div class="ns-h3">   <strong>Booking History</strong>  </div> 
                                                    
                                                    
                                                     <div class="bg_white bor_gray pad_10 table-responsive">
                                                    <asp:DataList ID="dlBookingHistory" runat="server" Width="100%" OnItemDataBound="dlBookingHistory_ItemDataBound">
                        <ItemTemplate>
                            <table class="qty-tble" width="100%" border="1" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblModifiedDate" runat="server" Text="" ></asp:Label>
                                    </td width="20%">
                                    
                                    <td width="15%">
                                        <asp:Label ID="lblModifiedTime" runat="server" Text=""></asp:Label>
                                    </td>
                                    
                                    <td width="15%">
                                        Booking
                                    </td>
                                   
                                    <td width="30%">
                                        <asp:Label ID="lblRemarks" runat="server" Text="" ></asp:Label>
                                    </td>
                                    
                                    <td width="20%">
                                        <asp:Label ID="lblModifiedBy" runat="server" Text="Label" ></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:DataList>
                                                     </div>
                                                     </div>
                                                    
                                                    
                                                    
                                                    
                                                    
                                                     <div class="clearfix"></div>
                                               
                                                    
                                                    
                                                    </div>

        
 
 
 <div class="col-md-6"> 
<%if(itinerary.Passenger[0].FlexDetailsList != null && itinerary.Passenger[0].FlexDetailsList.Count > 0){ %>
<%-- Newly Added Content for Corporate Module --%> 
<div class="" onclick="ShowReportingFields()"><span id="ShowReportingFields" class="hand primary-color" title="Click To Reporting Fields">Show Reporting Fields</span></div>

<div class="mt-3" id="ReportingFieldsDiv" style="display:none">
<div class="ns-h3"> <strong>Reporting Fields</strong>

<a class="advPax-collapse-btn-new" style=" color: #fff;" role="button" data-toggle="collapse" href="#FlexDataDetails" aria-expanded="false" aria-controls="FlexDataDetails"> 
</a>
    <div class="bg_white bor_gray pad_10 collapse" style="color:gray;" id="FlexDataDetails">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
<%foreach (FlightFlexDetails flex in itinerary.Passenger[0].FlexDetailsList){ %>
<tr>
<td height="30"><b><%=flex.FlexLabel %></b>
</td>
<td><%=flex.FlexData %></td>
</tr>
<%} %>
</tbody>
</table>
</div>
</div>

<div class="clearfix"></div>
</div>
<%--END: Newly Added Content for Corporate Module --%> 
<%} %>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    
    
     <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <a class="ssr-info" id="ssr" href="#" style="display:none" onclick = "change();">Show SSR Information</a>
                        </td>
                        <td>
                                                       
                            <div>
                                <span><a style="display:none" href="UpdateSSR.aspx?flightId=<% = itinerary.FlightId %>&redirectUrl=ViewBookingForTicket.aspx?bookingId=<% = booking.BookingId %>">
                                    Update SSR</a></span>
                                
                            </div>
                           
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="49%">
            <div class="font-14" style="display:none">
                 
                   <%if (booking.Status == BookingStatus.Ticketed && itinerary.Passenger.Length > 1)
                     {
                        System.Collections.Generic.List<Ticket> tkt=Ticket.GetTicketList(itinerary.FlightId);
                        if (Request["pendingId"] == null && tkt.Count>0)
                         {    %>
                         <span><a href="ViewAllETickets.aspx?flightId=<%=itinerary.FlightId%>" target="_blank">( View All Tickets )</a></span>
                        <%}
                  } %>
                    </div>
            
            
            
                <div class="ssr-infodiv" id="ssrDiv" style="background:#fff; border: solid 1px #ccc; padding:10px; display:none">
                <%--SSR information--%>
                    <asp:DataList ID="dlSSR" runat="server" Width="100%" OnItemDataBound="dlSSRInfo_ItemDataBound">
                        <ItemTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="20">
                                        <asp:Label ID="lblLeadPaxName" runat="server" Text="" ></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSSRCode" runat="server" Text="" ></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSSRDetail" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                       <asp:Label ID="lblStatus" runat="server" Text="" ></asp:Label>
                                        
                                    </td>
                                    
                                    <td><asp:Label ID="lblSSRStatus" runat="server" Text="" ></asp:Label> </td>
                                   
                                   
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </td>
            
            
        </tr>
    </table>
    
    
    
    <div style=" padding:10px; text-align:right "><div class="fleft" id="UpdateStatusBox">
                <%if (booking.Status != BookingStatus.Inactive && booking.Status != BookingStatus.Released && booking.Status != BookingStatus.Cancelled && (itinerary.FlightBookingSource != BookingSource.SpiceJet ||itinerary.FlightBookingSource != BookingSource.SpiceJetCorp) && (itinerary.FlightBookingSource != BookingSource.Indigo || itinerary.FlightBookingSource != BookingSource.IndigoCorp) && (itinerary.FlightBookingSource != BookingSource.Paramount) && (itinerary.FlightBookingSource != BookingSource.AirDeccan) && (itinerary.FlightBookingSource != BookingSource.Mdlr) && (itinerary.FlightBookingSource != BookingSource.GoAir || itinerary.FlightBookingSource != BookingSource.GoAirCorp) && (itinerary.FlightBookingSource != BookingSource.Sama) && (itinerary.FlightBookingSource != BookingSource.HermesAirLine))
                  { %>
                    <asp:Button ID="btnUpdate" runat="server" Text="Update Status" Visible="false" OnClientClick="return UpdateStatus('<%=itinerary.PNR %>',<%=bookingId %>)"/>
                    <%} %>
                </div></div>
    </td>
   
   
    
  </tr>
  
  <tr> 
  
  <td> <div style=" padding:10px"> 
 <%-- <%if (Settings.LoginInfo.IsCorporate != "Y" && itinerary != null)//If all the approvers approved then only we will show the CreateTicket Button
    { %>
       <%if (string.IsNullOrEmpty(itinerary.TripId) ||corpTripApprovalStatus == "A") %>
      <%{ %>--%>
   <asp:Button CssClass="btn but_b pull-right" ID="btnCreateTicket" runat="server" Text="Create Ticket Now" />
    <%--  <%} %>
   <%} %>--%>
</div></td>
  
  </tr>
</table>
 

 
 
 
 </div>
        <asp:UpdatePanel ID="upFiles" runat="server">
            <ContentTemplate>            
        <div class="col-md-6 padding-0">
            <asp:Panel class="pnldrop" ID="pnlDragandDrop" runat="server" Style="display: block; width: 100%; height: 100px;  background-color: White;
                border-color: blue">
               
                <div id="dZUpload" class="dropzone" runat="server">
                    <div class="dz-default dz-message">
                        Upload or Drag Drop documents here.
                    </div>
                </div>
                    <asp:Button ID="btnUploadFiles" runat="server" Text="Upload Files" OnClick="btnUploadFiles_Click" CssClass="button-normal" />
                    <asp:Panel ID="pnlUploadedFiles" runat="server" Width="400px" Height="200px" class="pnldrop" BorderStyle="Solid" BorderWidth="1px" BorderColor="Gray">
                        <div class="col-md-12 ns-h3">
                            
                                <div class="col-md-6">
                                    <span>View Files</span>
                                </div>
                                <div class="col-md-6" style="align-content:flex-end">
                                    <asp:LinkButton ID="lbDownloadAll" runat="server" Text="Download All" OnClick="lbDownloadAll_Click1"></asp:LinkButton>
                                </div>
                            
                        </div>
                        <asp:Panel ID="pnlFiles" runat="server" Width="100%" Height="90%" class="pnldrop" style="overflow:auto;">
                            </asp:Panel>
                    </asp:Panel>
            </asp:Panel>
        </div>          
    </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnUploadFiles" EventName="Click" />
                <asp:PostBackTrigger ControlID="lbDownloadAll" />
            </Triggers>
        </asp:UpdatePanel>

 <div> 
 

 <table width="100%" border="0" cellspacing="0" cellpadding="0">
        
        
        
       <%-- <tr>
            <td height="30px">
              <b>  Booking History</b>
            </td>
        </tr>--%>
       
        <tr>
            <td>
                <div class="col-md-6">
      <div class="" onclick="ShowComment(<%= CommentData.Count%>)"><span id="ShowHideComment" class="hand primary-color" title="Click To Show Comment"><%= spantext %></span></div>
        <div id="CommentBlock" class="" style="display: <%= displayComment%>;" "border:2px outset #ccc;">
      <% if(CommentData.Count > 0) %>
      <%{
            UserMaster member = new UserMaster(); %>      
            <%  for (int i = 0; i < CommentData.Count; i++)
                {
                    member = new UserMaster(Convert.ToInt32(CommentData[i].CreatedBy)); %>
         <div class="">
            <div class="">
            <span class=""><%= Util.UTCToIST((CommentData[i].LastModifiedOn)).ToString("dd/MM/yyyy") %> </span>      
            <span class=""><%= Util.UTCToIST((CommentData[i].LastModifiedOn)).ToString("HHmm")%>hrs</span>
            <%if (member != null && member.FirstName != null && member.LastName != null)
              { %>
            <span class=""><%= member.FirstName.ToString()%>&nbsp;
                <% if (member.LastName != null) %>
                <%{ %>
                    <%= member.LastName.ToString()%>
                <% }
              } %>
             <b>  says:</b></span>
            <span class="" id="commentDescription-<%=i%>">
                <%= CommentData[i].CommentDescription.ToString().Trim() %> 
            </span>
            </div>        
         <div class="">
         <%if(Settings.LoginInfo.MemberType == MemberType.ADMIN)
            { %>
                <span class="fright"><a href="javascript:quickReplyOpen(this, '<%=txtComments.ClientID %>', 'tbl_<%= i %>','<%= CommentData[i].CommentId.ToString() %>','<%= Session["memberId"] %>');", id = "reply_<%= i %>" style="display:none" >Reply</a></span>
          <%} %>
          
          <% if (Convert.ToInt32(Settings.LoginInfo.UserID) == CommentData[i].CreatedBy)  %>
          <%{ %>
            <span class="fright"><a href="javascript:editComment(this, '<%=txtComments.ClientID %>','tbl_<%= i %>','<%= CommentData[i].CommentId.ToString() %>',<%=i %>)", id="edit_<%= i %>" class="margin-right-5" style="display:none;">Edit</a> &nbsp;</span>
            <%} %>
          <%else if(Settings.LoginInfo.MemberType == MemberType.ADMIN)
            { %>
                <span class="fright"><a href="javascript:editComment(this, '<%=txtComments.ClientID %>','tbl_<%= i %>','<%= CommentData[i].CommentId.ToString() %>',<%=i %>)" ,id="edit_<%= i %>" class="margin-right-5" style="display:none;">Edit</a> &nbsp;</span>
          <%} %>
         </div> 
         </div>
         <div class=""><%--<img src="Images/spacer.gif" alt="Spacer" />--%></div>    
         <div class="" id="tbl_<%= i %>" style="display: <%= displayReplyBox %>;">
           <span class="">
             <textarea rows="3" cols="95" id="replyText_<%= i %>" name="replyText_<%= i %>"></textarea>
           </span>
           <span class="">
             <span class="fleft"><input type="button" id="submit<% = i %>" value="Submit" onclick="javascript:SaveComment('<%=txtComments.ClientID %>');" /></span>
             <span class=""><input type="button" id="cancel<%= i %>" value="Cancel"  onclick="javascript:replyCloseAll('tbl_<%= i %>')"/></span>
           </span>
         </div>      
        
                <%  
                }  %>
                
      <% } %>
      
      </div>
       </div>
            </td>
        </tr>
       <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            
            <div class="col-md-6"> 
              Enter Comments
            <br />
            
                <label>
                    <asp:TextBox ID="txtComments"  CssClass="form-control" runat="server" TextMode="MultiLine" Rows="5" Columns="45"></asp:TextBox>
                    <asp:Label ID="lblError" runat="server" ForeColor="Red" Text=""></asp:Label>
                </label>
                
                
                </div>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <%--<asp:RadioButton ID="rbtnPublic" runat="server" Text="Public" GroupName="publicOrPrivate" Checked="true" />
                <asp:RadioButton ID="rbtnPrivate" runat="server" Text="Private" GroupName="publicOrPrivate" />--%>
                <label>
                    &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnSubmit" CssClass="button-normal" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                </label>
                <br />
            </td>
        </tr>
    </table>
 
 </div>
 
 
 
    <div id="UpdateStatMsg" style="width: 400px; display: none;" class="font-red padding-5 yellow-new center auto">
    </div>
    <div id="noCreditLeft" style="display: none;" class="font-red bold">
        manu</div>
    <input type="hidden" id="itineraryLength" value="<%=itinerary.Segments.Length %>" />
    <asp:HiddenField ID="hdnBookingId" runat="server" />     
        <input type="hidden" name="CommentID" id="CommentID" />
        <input type="hidden" id="replyCommentID" name="replyCommentID" value=""  />
        <input type="hidden" id="editedCommentID" name="editedCommentID"  value=""/>
        <input type="hidden" id="CommentDetail" name="CommentDetail" />
    <div>
        
    </div>
    <br />
    <div>
       
    </div>
    
    
    
    <div id="surchargePop" class="light-grey-bg border-y padding-bottom-10 search-popup-parent"
                    style="display: none; width: 280px; position: absolute; left: 620px; top: 210px;">
                    <span class="fleft width-100 margin-left-10 margin-top-10 bold" id="StatusString"></span>
                    <span id="TicketStatus" class="fleft width-100 margin-top-10 margin-left-10">
                    <%string display =string.Empty;
                      string voidButton = string.Empty;
                      string cancelButton = string.Empty;
                      decimal adminFee = 0;
                      //AgencyCancellationCharge canCharge = AgencyCancellationCharge.GetAgencyCanChargeByAirline(itinerary.AgencyId, itinerary.ValidatingAirlineCode);
                      //if (canCharge != null)
                      //{
                      //    adminFee = canCharge.CancellationCharge;
                      //}
                      //else
                      //{
                          //AgencyCategoryCollection categoryList = new AgencyCategoryCollection();
                          //categoryList.LoadAgencyCategory();
                          //AgencyCategory category;
                          //category = categoryList.GetAgencyCategoryByAgencyIdAirlineCode(itinerary.AgencyId, itinerary.ValidatingAirlineCode);
                          //System.Collections.Generic.Dictionary<int, AirlineCategoryProperties> categoryProperties = AirlineCategoryProperties.Load(itinerary.ValidatingAirlineCode);
                          //if (category != null && categoryProperties.ContainsKey(category.CategoryId))
                          //{
                          //    adminFee = categoryProperties[category.CategoryId].CancellationCharge;
                          //}
                          //else
                          //{
                          //    adminFee = 0;
                          //}
                      //}
                      if (Util.IsToday(itinerary.CreatedOn))
                      {
                        display = "display:block";
                        voidButton = "checked=\"checked\"";
                      }
                      else
                      {
                        display = "display:none";
                        cancelButton = "checked=\"checked\"";
                      }
                        %>
                        <span style="width:50px;float:left;<%=display %>" ><input id="Void" <% = voidButton%> name="CancelVoid" type="radio" value="void" />Void</span>
                       
                        <span style="width:60px;float:left;"><input id="Cancel" <%=cancelButton %> name="CancelVoid" type="radio" value="cancel" />Refund </span>
                        </span>
                    <span class="fleft margin-left-10 bold font-14 margin-top-10 width-100">Total Amount
                        <span id="TicketPrice"></span> </span><span class="fleft margin-left-10 bold font-14 margin-top-10 width-100">
                            Admin Fees
                            <input id="AdminFee" class="width-50" onblur="SetRefundedAmount(this)" style="margin-left: 30px;"
                                type="text" value="<%=adminFee %>" />
                        </span><span class="fleft margin-left-10 bold font-14 margin-top-10 width-100">Supplier
                            Fees
                            <input id="SupplierFee" class="width-50" onblur="SetRefundedAmount(this)" style="margin-left: 15px;"
                                type="text" value="0" />
                        </span><span id= "RefundedAmountText" class="fleft margin-left-5 bold font-14 margin-top-10 width-100">Total
                            Refunded Amount : <span id="TotalRefundedAmount"></span></span><span id="RemarkText" class="fleft width-100 margin-left-10 margin-top-10">
                                Remarks</span> <span class="fleft margin-left-10 width-100">
                                    <textarea id="Remarks" cols="35" rows="3"></textarea>
                                </span><span id="ErrorLabel" class="fleft width-100 margin-left-10 margin-top-10 font-red">
                                </span><span class="width-100 fright">
                                    <input class="fright margin-right-10" onclick="RefundTicket()" type="button" value="Refund" id="RefundTicket" />
                                    <a class="fright margin-right-20" href="javascript:HideDiv()">Cancel</a> </span>
                    <span></span>
                </div>
    
     <div id='EmailDiv' runat='server' style='width: 100%; display: none;'>
        <%try
          {
              if (isBookingSuccess)
              {
                  List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();

                  if (ticketList != null && ticketList.Count > 0)
                  {
                      ptcDetails = ticketList[0].PtcDetail;
                  }
                  else// For Hold Bookings
                  {
                      ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(flightItinerary.FlightId);
                  }

              %>
              <%//Show the Agent details for First Email only. In case of Corporate booking second & third 
            //emails need not show Agent details in the email body
            if (emailCounter <= 1)
            { %>
        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                  <asp:Image ID='imgLogo' runat='server'  />
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                    <td style="width:70%;"></td>
                     <td align="right"><label style="padding-right: 20%"><strong>Agent Name:</strong></label></td>
                     <td align="left"><label style="padding-right: 10%"><strong><%=agency.Name%></strong></label></td>
                     </tr>
                        <tr> <td style="width:70%;"></td>
                         <td align="right"><label style="padding-right: 20%"><strong>Phone No:</strong></label></td>
                        <td align="left"><label style="padding-right: 10%"><strong><%=agency.Phone1%></strong></label></td>
                        </tr>
                    </table>
        <% }
            for (int count = 0; count < flightItinerary.Segments.Length; count++)
            {
                int paxIndex = 0;
              

                List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
                ptcDetail = ptcDetails.FindAll(delegate(SegmentPTCDetail ptc) { return ptc.SegmentId == flightItinerary.Segments[count].SegmentId; });
               %>
        <table style='font-size: 12px;' width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td>
                    <div style='border: solid 1px #000'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td width='25%' style='background: #1C498A; height: 34px; line-height: 34px; padding-left: 20px;'>
                                    <font color='white'>
                                        <strong>Flight :
                                            <%=flightItinerary.Segments[count].Origin.CityName%>
                                            to
                                            <%=flightItinerary.Segments[count].Destination.CityName%></strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px;
                                    padding-left: 20px;'>
                                    <font color='white'>
                                        <strong>RESERVATION</strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px;
                                    padding-left: 20px;'>
                                    <font color='white'>
                                        <strong>
                                            <%=flightItinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%></strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px;
                                            padding-left: 20px;'>
                                            <font color='white'><strong>Airline Ref : 
                                                <%=(flightItinerary.Segments[count].AirlinePNR == null || flightItinerary.Segments[count].AirlinePNR == "" ? (flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR) : (flightItinerary.Segments[count].AirlinePNR.Split('|').Length > 1 ? flightItinerary.Segments[count].AirlinePNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.Segments[count].AirlinePNR))%>
                                                </strong></font>
                                        </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='width: 100%;'>
                        <tr>
                            <td style='width: 57%;'>
                                <table style='background: #f3f6f8; width: 100%; float: left; border: solid 1px #cdcdcd;
                                    margin-top: 2px;'>
                                    <tr>
                                        <td style='width: 50%; float: left; height: 20px; line-height: 20px;'>
                                            <label style='padding-left: 20px'>
                                                <strong>Passenger Name's :</strong></label>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px;'>
                                            <%if (ticketList != null && ticketList.Count > 0)
                                              { %>
                                            <strong>Ticket No </strong>
                                            <%}
                                              else
                                              { // For Hold Bookings%>
                                            <strong>PNR </strong>
                                            <%} %>

                                        </td>
                                        <td style='width: 25%; float: left; height: 20px; line-height: 20px;'>
                                            <strong>Baggage </strong>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 43%;'>
                                <table style='background: #f3f6f8; width: 100%; float: right; border: solid 1px #cdcdcd;
                                    margin-top: 2px; height: 20px; line-height: 20px;'>
                                    <tr>
                                        <td>
                                            <label style='padding-left: 20px'>                                                
                                                <%if (ticketList != null && ticketList.Count > 0)
                                                  { %>
                                                <strong>Ticket Date:</strong>
                                                <%=ticketList[0].IssueDate.ToString("dd/MM/yyyy")%>
                                                <%}
                                                  else
                                                  {//for Corporate HOLD Booking %>
                                                <strong>Booking Date:</strong>
                                                <%=flightItinerary.CreatedOn.ToString("dd/MM/yyyy")%>
                                                <%} %>
                                                </label>
                                        </td>
                                        <td>
                                            <label style='padding-left: 20px'>
                                                <strong>Trip ID :</strong>
                                                <%=booking.BookingId%></label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%for (int j = 0; j < flightItinerary.Passenger.Length; j++)
              { %>
            <tr>
                <td>
                    <table style='width: 100%;'>
                        <tr>
                            <td style='width: 57%;'>
                                <table style='background: #f3f6f8; width: 100%; float: left; border: solid 1px #cdcdcd;
                                    margin-top: 2px;'>
                                    <tr>
                                        <td style='width: 50%; float: left; height: 20px; line-height: 20px;'>
                                            <label style='margin-left: 20px; padding: 2px; background: #EBBAD9;'>
                                                <strong>
                                                    <%=flightItinerary.Passenger[j].Title + " " + flightItinerary.Passenger[j].FirstName + " " + flightItinerary.Passenger[j].LastName%>
                                                    </strong></label>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px;'>
                                        <%if (ticketList != null && ticketList.Count > 0)
                                          { %>
                                            <%=(ticketList[j].TicketNumber.Split('|').Length > 1 ? ticketList[j].TicketNumber.Split('|')[flightItinerary.Segments[count].Group] : ticketList[j].TicketNumber)%>
                                            <%}
                                          else
                                          { //for Corporate HOLD Booking%>
                                             <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR)%>
                                            <%} %>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px;text-align:right;'>
                                                
                                                 <%if (flightItinerary.FlightBookingSource == BookingSource.UAPI || (flightItinerary.FlightBookingSource == BookingSource.TBOAir && !(flightItinerary.IsLCC)))
                                                   {

                                                       if (ptcDetail.Count > 0)
                                                       {
                                                           string baggage = "";
                                                           foreach (SegmentPTCDetail ptc in ptcDetail)
                                                           {
                                                               switch (flightItinerary.Passenger[j].Type)
                                                               {
                                                                   case PassengerType.Adult:
                                                                       if (ptc.PaxType == "ADT")
                                                                       {
                                                                           baggage = ptc.Baggage;
                                                                       }
                                                                       break;
                                                                   case PassengerType.Child:
                                                                       if (ptc.PaxType == "CNN")
                                                                       {
                                                                           baggage = ptc.Baggage;
                                                                       }
                                                                       break;
                                                                   case PassengerType.Infant:
                                                                       if (ptc.PaxType == "INF")
                                                                       {
                                                                           baggage = ptc.Baggage;
                                                                       }
                                                                       break;
                                                               }
                                                           }

                                                           if (baggage.Length > 0 && !baggage.Contains("Bag") && !baggage.ToLower().Contains("piece") && !baggage.ToLower().Contains("unit") && !baggage.ToLower().Contains("units"))
                                                           {%>
                                                            <%=(baggage.ToLower().Contains("kg") ? baggage : baggage + " Kg")%> 
                                                         
                                                          <%}
                                                           else
                                                           {%>
                                                            <%=baggage%>
                                                          <%}
                                                       }//End PtcDetail.Count

                                                   }
                                                   else if (flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
                                                   {
                                                       if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
                                                       { %>
                                                            <%=flightItinerary.Passenger[j].BaggageCode.Split(',')[flightItinerary.Segments[count].Group]%> 
                                                     <%}
                                                   }
                                                   else if (flightItinerary.FlightBookingSource == BookingSource.AirArabia)
                                                   {
                                                       if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
                                                       {%>
                                                            <%=flightItinerary.Passenger[j].BaggageCode.Split(',')[flightItinerary.Segments[count].Group]%>
                                                     <%}
                                                   }
                                                   else if (flightItinerary.FlightBookingSource == BookingSource.TBOAir && (flightItinerary.IsLCC))
                                                   {
                                                       if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
                                                       {
                                                           string strBaggage = string.Empty;
                                                           if (flightItinerary.Passenger[j].BaggageCode != string.Empty && flightItinerary.Passenger[j].BaggageCode.Split(',').Length > 1)
                                                           {
                                                               strBaggage = flightItinerary.Passenger[j].BaggageCode.Split(',')[flightItinerary.Segments[count].Group];
                                                           }
                                                           else if (flightItinerary.Passenger[j].BaggageCode.Split(',').Length <= 1)
                                                           {
                                                               strBaggage = flightItinerary.Passenger[j].BaggageCode;
                                                           }
                                                           else
                                                           {
                                                               strBaggage = "Airline Norms";
                                                           }
                                                           %>
                                                      <%=strBaggage%> 
                                                    <%}
                                                   } %>
                                            
                                                </td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 43%;'>
                                <table style='background: #f3f6f8; width: 100%; float: right; border: solid 1px #cdcdcd;
                                    margin-top: 2px; height: 20px; line-height: 20px'>
                                    <tr>
                                        <td style='width:50%'>
                                            <label style='padding-left: 20px'>
                                                <strong>PNR No : </strong>
                                                <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR)%></label>
                                        </td>
                                        <%if (!string.IsNullOrEmpty(flightItinerary.TripId))
                                          { %>
                                        <td style='width:50%;text-align:right'>                                        
                                        <strong>Corporate Booking Code</strong>
                                        <%=flightItinerary.TripId%>
                                        </td>
                                        <%} %>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%} %>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left'>
                                <label style='padding-left: 20px'>
                                    Date:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Status:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 40%; float: left;'>
                                <label style='padding-left: 20px; color: #08bd48;'>
                                    <strong>
                                        <%=booking.Status.ToString()%></strong></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Departs:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].DepartureTime.ToString("hh:mm tt")%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Arrives:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].ArrivalTime.ToString("hh:mm tt")%>
                                </label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airline:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%try
                                      {
                                          CT.Core.Airline airline = new CT.Core.Airline();
                                          airline.Load(flightItinerary.Segments[count].Airline);%>
                                    <%=airline.AirlineName%>
                                    <%}
                                      catch { } %>
                                </label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Flight:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Airline + " " + flightItinerary.Segments[count].FlightNumber%>
                                     <%try
                                       {  //Loading Operating Airline and showing
                                           if ((!flightItinerary.IsLCC && flightItinerary.Segments[count].OperatingCarrier != null && flightItinerary.Segments[count].OperatingCarrier.Length > 0)||(flightItinerary.FlightBookingSource == BookingSource.Indigo && flightItinerary.Segments[count].OperatingCarrier != null && flightItinerary.Segments[count].OperatingCarrier.Length > 0 && flightItinerary.Segments[count].OperatingCarrier != "6E"))////Modified for Indigo CodeShare
                                           {
                                               string opCarrier = flightItinerary.Segments[count].OperatingCarrier;
                                               CT.Core.Airline opAirline = new CT.Core.Airline();
                                               if (opCarrier.Split('|').Length > 1)
                                               {
                                                   opAirline.Load(opCarrier.Split('|')[0]);
                                               }
                                               else
                                               {
                                                   opAirline.Load(opCarrier.Substring(0, 2));
                                               } %>
                                                        (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                       }
                                       catch { } %>
                                </label>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    From:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Origin.CityName + ", " + flightItinerary.Segments[count].Origin.CountryName%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airport:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Origin.AirportName + ", " + flightItinerary.Segments[count].DepTerminal%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    To:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Destination.CityName + ", " + flightItinerary.Segments[count].Destination.CountryName%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airport:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Destination.AirportName + ", " + flightItinerary.Segments[count].ArrTerminal%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Class:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].CabinClass%></label>
                                    <%if (flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
                                      {%>                                    
                                      <label style='padding-left: 20px'><label style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                        width: 25%; '>Fare Type : </label> <%=flightItinerary.Segments[count].SegmentFareType%></label>                                                                             
                                      <%} %>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Duration:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Duration%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <%} %>
        </table>
        <table style='font-size: 12px;' width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0;

                      if (ticketList != null && ticketList.Count > 0)
                      {
                          for (int k = 0; k < ticketList.Count; k++)
                          {
                              AirFare += ticketList[k].Price.PublishedFare;
                              Taxes += ticketList[k].Price.Tax + ticketList[k].Price.Markup;
                              if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                              {
                                  Taxes += ticketList[k].Price.AdditionalTxnFee + ticketList[k].Price.OtherCharges + ticketList[k].Price.SServiceFee + ticketList[k].Price.TransactionFee;
                              }
                              Baggage += ticketList[k].Price.BaggageCharge;
                              MarkUp += ticketList[k].Price.Markup;
                              Discount += ticketList[k].Price.Discount;
                              AsvAmount += ticketList[k].Price.AsvAmount;
                          }
                          if (ticketList[0].Price.AsvElement == "BF")
                          {
                              AirFare += AsvAmount;
                          }
                          else if (ticketList[0].Price.AsvElement == "TF")
                          {
                              Taxes += AsvAmount;
                          }
                      }
                      else
                      {
                          for (int k = 0; k < flightItinerary.Passenger.Length; k++)
                          {
                              AirFare += flightItinerary.Passenger[k].Price.PublishedFare;
                              Taxes += flightItinerary.Passenger[k].Price.Tax + flightItinerary.Passenger[k].Price.Markup;
                              if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                              {
                                  Taxes += flightItinerary.Passenger[k].Price.AdditionalTxnFee + flightItinerary.Passenger[k].Price.OtherCharges + flightItinerary.Passenger[k].Price.SServiceFee + flightItinerary.Passenger[k].Price.TransactionFee;
                              }
                              Baggage += flightItinerary.Passenger[k].Price.BaggageCharge;
                              MarkUp += flightItinerary.Passenger[k].Price.Markup;
                              Discount += flightItinerary.Passenger[k].Price.Discount;
                              AsvAmount += flightItinerary.Passenger[k].Price.AsvAmount;
                              if (flightItinerary.Passenger[k].Price.AsvElement == "BF")
                              {
                                  AirFare += AsvAmount;
                              }
                              else if (flightItinerary.Passenger[k].Price.AsvElement == "TF")
                              {
                                  Taxes += AsvAmount;
                              }
                          }
                      }
                    %>
                     <% if (agencyId <= 1)
                        {
                                  
                                  %>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                            <td>
                                <table width='100%' cellspacing='0' cellpadding='0' border='0'>
                                    <tbody>
                                        <tr>
                                            <td width='41%'>
                                            </td>
                                            <td>
                                                <table width='100%' class=''>
                                                    <tbody>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Air Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=AirFare.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Taxes & Fees</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Taxes.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency%>
                                                            </td>
                                                        </tr>
                                                        <%if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.TBOAir && Baggage > 0 || flightItinerary.IsLCC || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp)
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Baggage Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Baggage.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency%>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <%if (Discount > 0)
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Discount</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Discount.ToString("N" + agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency%>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <tr>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: left!important;'>
                                                                <b>Total Air Fare</b>
                                                            </td>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: right!important;'>
                                                                <b>
                                                                   <%if (flightItinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                                                                     { %>
                                                                    <%=Math.Ceiling((AirFare + Taxes + Baggage) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                    <%}
                                                                     else
                                                                     { %>
                                                                     <%=((AirFare + Taxes + Baggage) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                     <%} %>
                                                                    <%=agency.AgentCurrency%></b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
                      <%} %>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        <%}
          }
          catch { } %>
    </div>
     </div>
     <div id="PreLoader" style="display:none">
        <div class="loadingDiv" style="top:230px;">
            <div id="loadingDivision">
                <img src="images/preloader11.gif" /></div>
            <div style="font-size: 16px; color: #999999">
            
                <strong>Awaiting confirmation</strong>
               
                </div>
            <div style="font-size: 21px; color: #3060a0">
                       
                 <strong style="font-size: 14px; color: black" >do not click the refresh or back button as the transaction may be interrupted or terminated.</strong>
            
                    </div>
        </div>
    </div>

    <!-- Seat failure alert -->
    <div class="modal fade pymt-modal" data-backdrop="static" id="Seatsalert" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
        <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content" style="width:500px;">
            <div class="modal-header">
                <h4 class="modal-title" id="SeatsalertLabel">Seat assignment alert</h4>
            </div>
            <div class="modal-body">
            <div id="divSeatsalertText" style="color:Red; font-size:14px; font-weight:bold; padding:10px;"></div>                  
            <div class="padtop_4">
                <a id="SeatsalertContinue" class="btn but_b" onclick="$('#Seatsalert').modal('hide');" >OK</a>
            </div>
            </div>
        </div>
        </div>
    </div>
    <!--Seat failure alert End-->

    <!-- Reprice Modal -->
    <div id="divRepriceAlert" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width:500px">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="padding:5px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Alert</h4>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                        <div id="divPriceAlert" style="display:none"> 
                            <div style="text-align: CENTER; MARGIN-BOTTOM: 20PX;"> 
                                <span style="color: Red; font-size: 14px; font-weight: bold;"> Price has been changed for this Itinerary </span> 
                            </div>
                            <div style="text-align: CENTER;">
                                <div class="col-md-6"> <b>Original Fare :</b> <label id="lblOrigPrice"></label> </div>
                                <div class="col-md-6"> <b>Changed Fare :</b> <label id="lblNewPrice"style="color: Red"></label> </div>
                            </div>
                        </div>
                        <div id="divAlertMessage" class="row custom-gutter"></div>
                    </div>
                </div>
                <div id="divRepricefooter" style="display:none" class="modal-footer"> 
                    <button type="button" class="button" onclick="CreateTicket();">Continue booking</button> 
                    <%--<button type="button" class="button" onclick="CancelPNR(); return false">Cancel PNR</button>--%>
                    <button type="button" class="button" onclick="window.location.href = 'HotelSearch.aspx?source=Flight';">Go to search page</button>
                </div>
            </div>
        </div>
    </div>
    <!--Reprice Modal End-->
    <script>
        <%if (booking.Status == BookingStatus.InProgress || booking.Status == BookingStatus.Hold || booking.Status == BookingStatus.Ready)
        {%>
            CardChargeEnable();
        <%}%>
    </script>
</asp:Content>
