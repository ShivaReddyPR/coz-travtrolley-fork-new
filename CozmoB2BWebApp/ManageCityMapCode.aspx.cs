﻿using System;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;

public partial class ManageCityMapCodeGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected string cityName = "Enter city name";
    protected DataTable dtEnquiry;
    protected DataTable dtCityCodes;
    protected string[,] cityCodes;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            if (!IsPostBack)
            {
                InitializePageControls();
            }
            else
            {
                if (hdnSearchClickCount.Value == "1")
                {
                    BindSearch();
                }
                else
                {
                    BindAdd();
                }
            }
            lblErrorMsg.Text = string.Empty;
            lblSuccessMsg.Text = string.Empty;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
        }
    }

    public string LoadCityCodes()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        try
        {
            sb.Append("[");
            dtCityCodes = ManageCityMapCode.LoadCityCodes();
            cityCodes = new string[dtCityCodes.Rows.Count, dtCityCodes.Columns.Count];
            for (int i = 0; i < dtCityCodes.Rows.Count; i++)
            {
                sb.Append("{");
                for (int j = 0; j < dtCityCodes.Columns.Count; j++)
                {
                    //cityCodes[i, j] = dtCityCodes.Rows[i][j].ToString();
                    if (j > 0)
                    {
                        sb.Append(",\"" + dtCityCodes.Columns[j].ColumnName + "\":\"" + dtCityCodes.Rows[i][j].ToString().Replace("'", "") + "\"");
                    }
                    else
                    {
                        sb.Append("\"" + dtCityCodes.Columns[j].ColumnName + "\":\"" + dtCityCodes.Rows[i][j].ToString().Replace("'", "") + "\"");
                    }
                }
                if (i == dtCityCodes.Rows.Count - 1)
                {
                    sb.Append("}");
                }
                else
                {
                    sb.Append("},");
                }
            }
            if (sb.ToString().EndsWith(","))
            {
                sb.ToString(sb.Length - 1, 1).Replace(",", "");
            }
            sb.Append("]");
            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void InitializePageControls()
    {
        txtCityName.Text = string.Empty;
        btnUpdate.Visible = false;
        btnSave.Visible = false;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            tblManage.Controls.Clear();
            BindSearch();
            btnUpdate.Visible = true;
            txtCityName.Visible = true;
            lblCityName.Visible = true;
            btnAdd.Visible = false;
            btnSearch.Visible = true;
            btnClear.Visible = true;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
        }
    }
    private void BindSearch()
    {
        try
        {
            Session["SearchData"] = null;
            string sources = "";
            System.Collections.Generic.Dictionary<string, string> sourcesConfig = CT.Configuration.ConfigurationSystem.ActiveSources;
            foreach (System.Collections.Generic.KeyValuePair<string, string> pair in sourcesConfig)
            {
                if (pair.Value.Split(',')[0].ToLower() == "hotel")
                {
                    if (sources.Length > 0)
                    {
                        sources += "," + pair.Key;
                    }
                    else
                    {
                        sources = pair.Key;
                    }
                }
            }
            
            dtEnquiry = ManageCityMapCode.GetList(txtCityName.Text.Trim(),sources);
            Session["SearchData"] = dtEnquiry;
            
            HtmlTableRow hr = new HtmlTableRow();
             
                for (int i = 0; i < dtEnquiry.Columns.Count; i++)
                {
                    HtmlTableCell tc = new HtmlTableCell();
                    tc.InnerText = dtEnquiry.Columns[i].ColumnName;
                    tc.Width = "50px";
                    tc.Attributes["style"] = "color:black; font-weight:bold; border: none;background-color: #ccc;  height: 1px;";
                    hr.Cells.Add(tc);
                }
                tblManage.Rows.Add(hr);

                for (int j = 0; j < dtEnquiry.Rows.Count; j++)
                {
                    HtmlTableRow tr = new HtmlTableRow();
                    for (int k = 0; k < dtEnquiry.Columns.Count; k++)
                    {
                        if (k > 2)
                        {
                            TextBox txt = new TextBox();
                            txt.ID = "txtSource_" + j.ToString() + "_" + k.ToString();
                            txt.Text = dtEnquiry.Rows[j][k].ToString();
                            txt.Width = new Unit(60, UnitType.Pixel);
                            txt.MaxLength = 30;
                            HtmlTableCell td = new HtmlTableCell();
                            td.Controls.Add(txt);
                            td.Width = "30px";
                            tr.Cells.Add(td);
                        }
                        else
                        {
                            Label lbl = new Label();
                            lbl.Text = dtEnquiry.Rows[j][k].ToString();
                            HtmlTableCell td = new HtmlTableCell();
                            td.Controls.Add(lbl);
                            td.Width = "30px";
                            tr.Cells.Add(td);
                            tblManage.Rows.Add(tr);
                        }
                    }
                }
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
        }
    }


    void BindAdd()
    {
        string sources = "";
        System.Collections.Generic.Dictionary<string, string> sourcesConfig = CT.Configuration.ConfigurationSystem.ActiveSources;
        foreach (System.Collections.Generic.KeyValuePair<string, string> pair in sourcesConfig)
        {
            if (pair.Value.Split(',')[0].ToLower() == "hotel")
            {
                if (sources.Length > 0)
                {
                    sources += "," + pair.Key;
                }
                else
                {
                    sources = pair.Key;
                }
            }
        }
        sources = "Destination,CountryCode,stateprovince," + sources;
        HtmlTableRow hr = new HtmlTableRow();

        for (int i = 0; i < sources.Split(',').Length; i++)
        {
            HtmlTableCell tc = new HtmlTableCell();
            tc.InnerText = sources.Split(',')[i];
            tc.Width = "50px";
            tc.Attributes["style"] = "color:black; font-weight:bold; border: none;background-color: #ccc;  height: 1px;";
            hr.Cells.Add(tc);
        }
        if (Convert.ToInt32(hdnAddCount.Value) > 0)
        {
            tblSave.Rows.Add(hr);
        }
        string[] textValues = new string[0];

        if (hdnValue.Value != "0")
        {
            textValues = hdnValue.Value.Split('|');
        }

        for (int j = 0; j < Convert.ToInt32(hdnAddCount.Value); j++)
        {
            string[] values = new string[0];
            if (textValues.Length > 0 && textValues.Length >j)
            {
                if (textValues[j] != "")
                {
                    values = textValues[j].Split(',');
                }
            }
            HtmlTableRow tr = new HtmlTableRow();
            tr.ID = "tr" + j.ToString();
            for (int k = 0; k < sources.Split(',').Length; k++)
            {
                TextBox txt = new TextBox();
                txt.ID = "txt_" + j.ToString() + "_" + k.ToString();
                txt.Width = new Unit(60, UnitType.Pixel);
                txt.MaxLength = 30;
                if (values.Length > 0 && values.Length > k)
                {
                    if (values[k] != "")
                    {
                        txt.Text = values[k];
                    }
                    
                }
                txt.Attributes.Add("onchange", "ReadValues()");
                Label lbl = new Label();
                lbl.ID = "lbl_" + j.ToString() + "_" + k.ToString();
                HtmlTableCell td = new HtmlTableCell();
                td.Controls.Add(txt);
                td.Controls.Add(lbl);
                td.Width = "30px";
                tr.Cells.Add(td);
            }
            HtmlTableCell Remove = new HtmlTableCell();
            Button btnRemove = new Button();
            btnRemove.ID = "btnRemove" + j.ToString();
            btnRemove.Text = "Remove";
            btnRemove.Width = new Unit(60, UnitType.Pixel);
            btnRemove.Click += new EventHandler(btnRemove_Click);
            if (j != 0)
            {
                Remove.Width = "30px";
                Remove.Controls.Add(btnRemove);
                tr.Cells.Add(Remove);
            }
            tblSave.Rows.Add(tr);
        }
        btnSearch.Visible = false;
        btnUpdate.Visible = false;
        txtCityName.Visible = false;
        lblCityName.Visible = false;
    }

    void btnRemove_Click(object sender, EventArgs e)
    {
        try
        {
            string rowIndex = ((System.Web.UI.Control)(sender)).ID.Split('e')[2];
            tblSave.Controls.Clear();
            hdnAddCount.Value = Convert.ToString(Convert.ToInt32(hdnAddCount.Value) - 1);
            string[] textValues = new string[0];
            string values = string.Empty;
            if (hdnValue.Value != "0")
            {
                textValues = hdnValue.Value.Split('|');
                for (int i = 0; i < textValues.Length; i++)
                {
                    if (i != Utility.ToInteger(rowIndex))
                    {
                        if (values == string.Empty)
                        {
                            values = textValues[i];
                        }
                        else
                        {
                            values = values + "|" + textValues[i];
                        }
                    }
                }
            }
            hdnValue.Value = values;
            BindAdd();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {

            if (Session["SearchData"] != null)
            {
                dtEnquiry = Session["SearchData"] as DataTable;
            }
            int count = 0;
            for (int i = 0; i < dtEnquiry.Rows.Count; i++)
            {
                System.Collections.Generic.Dictionary<string, string> sourceValues = new System.Collections.Generic.Dictionary<string, string>();
                for (int j = 0; j < dtEnquiry.Columns.Count; j++)
                {
                    if (j > 2)
                    {

                        TextBox source = tblManage.Rows[i].Cells[j].FindControl("txtSource_" + i.ToString() + "_" + j.ToString()) as TextBox;
                        if (source.Text != dtEnquiry.Rows[i][j].ToString())
                        {
                            sourceValues.Add(dtEnquiry.Columns[j].ColumnName, source.Text);
                        }
                    }

                }
                if (sourceValues.Count > 0 )
                {
                    string query = "Update HTL_MAP_CITYCODE SET ";
                    string columns = "";
                    foreach (System.Collections.Generic.KeyValuePair<string, string> pair in sourceValues)
                    {
                        if (columns.Length > 0)
                        {
                            columns += "," + pair.Key + "='" + pair.Value + "'";
                        }
                        else
                        {
                            columns = pair.Key + "='" + pair.Value + "'";
                        }
                    }
                    query += columns + " WHERE CityId=" + dtEnquiry.Rows[i]["CityId"].ToString();
                    ManageCityMapCode objManageCity = new ManageCityMapCode();
                    objManageCity.UpdateCityCode(query);
                    if (count == 0)
                    {
                        count = 1;
                    }
                    else
                    {
                        count++;
                    }
                }

            }
            lblSuccessMsg.Text = count+" Records Updated";
            tblManage.Controls.Clear();
            BindSearch();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            tblSave.Controls.Clear();
            hdnAddCount.Value = Convert.ToString(Convert.ToInt32(hdnAddCount.Value) + 1);
            BindAdd();
            btnSave.Visible = true;
            btnClear.Visible = true;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            string sources = "";
            System.Collections.Generic.Dictionary<string, string> sourcesConfig = CT.Configuration.ConfigurationSystem.ActiveSources;
            foreach (System.Collections.Generic.KeyValuePair<string, string> pair in sourcesConfig)
            {
                if (pair.Value.Split(',')[0].ToLower() == "hotel")
                {
                    if (sources.Length > 0)
                    {
                        sources += "," + pair.Key;
                    }
                    else
                    {
                        sources = pair.Key;
                    }
                }
            }
            sources = "Destination,CountryCode,stateprovince," + sources;

            int count = 0;
            string duplicateCodes = string.Empty;
            for (int i = 0; i < Convert.ToInt32(hdnAddCount.Value); i++)
            {
                System.Collections.Generic.Dictionary<string, string> sourceValues = new System.Collections.Generic.Dictionary<string, string>();
                for (int j = 0; j < sources.Split(',').Length; j++)
                {
                    TextBox source = tblSave.Rows[i].Cells[j].FindControl("txt_" + i.ToString() + "_" + j.ToString()) as TextBox;
                    sourceValues.Add(sources.Split(',')[j], source.Text);
                }
                
                if (sourceValues.Count > 0)
                {
                    //string query = "Insert into HTL_MAP_CITYCODE(" + sources + ") values(";
                    string values = "";
                    string destination="";
                    string countryCode = "";
                    int k=0;
                    //string validateSource = string.Empty;
                   
					
                    foreach (System.Collections.Generic.KeyValuePair<string, string> pair in sourceValues)
                    {
                        if (values.Length > 0)
                        {
                            values += ",'" + pair.Value + "'";
                            //if (k > 2 && pair.Value != string.Empty)
                            //{
                            //    if (validateSource.Length > 0)
                            //    {
                            //        validateSource += "," + pair.Key + "=" + "'" + pair.Value + "'";
                            //    }
                            //    else
                            //    {
                            //        validateSource = "SELECT  1 FROM [HTL_MAP_CITYCODE] WHERE " + pair.Key + "=" + "'" + pair.Value + "'";
                            //    }
                            //}
                        }
                        else
                        {
                            values = "'" + pair.Value + "'";
                           
                        }
                        if(k==0)
                        {
                            destination=pair.Value;
                        }
                        else if(k==1)
                        {
                            countryCode = pair.Value;
                        }
                        k++;
                    }
                    //query += columns + ")";
                    ManageCityMapCode objManageCity = new ManageCityMapCode();
                    string message= objManageCity.SaveCityCode(sources,values, destination, countryCode);
                    if (message == "S")
                    {
                        if (count == 0)
                        {
                            count = 1;
                        }
                        else
                        {
                            count++;
                        }
                    }
                    else
                    {
                        if (duplicateCodes == string.Empty)
                        {
                            duplicateCodes = countryCode;
                        }
                        else
                        {
                            duplicateCodes +=","+ countryCode;
                        }
                    }
                }
            }
            tblSave.Controls.Clear();
            hdnAddCount.Value = "0";
            btnSave.Visible = false;
            txtCityName.Visible = true; ;
            lblCityName.Visible = true; ;
            btnSearch.Visible = true;
            btnClear.Visible = false;
            if (duplicateCodes == string.Empty)
            {
                lblSuccessMsg.Text = count + " Records Saved";
            }
            else
            {
                Utility.Alert(this.Page, duplicateCodes + " Alerday Exists");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Response.Redirect("ManageCityMapCode.aspx", true);
    }
}
