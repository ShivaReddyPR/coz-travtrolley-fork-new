﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using CT.TicketReceipt.BusinessLayer;

public partial class ExcelUploadFile : System.Web.UI.Page
{
    private String strConnection = System.Configuration.ConfigurationManager.AppSettings["dbSetting"];
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {

        }
    }

    private void Save()
    {
        try
        {
            string savingPath = System.Configuration.ConfigurationManager.AppSettings["ChequeDocPath"];
            string path = string.Empty;
            string fileName = string.Empty;
            OleDbDataReader dReader = null;
            OleDbConnection excelConnection = null;
            OleDbCommand cmd = null;
            OleDbDataAdapter oleDAdapter = null;
            try
            {
                if (fileuploadExcel1.HasFile == true)
                {
                    //file upload path
                    path = fileuploadExcel1.PostedFile.FileName;
                }
                fileName = path.Substring(path.LastIndexOf('\\') + 1);
                //savingPath = Server.MapPath(@"Upload\PaymentCheck\") + fileName;
                string[] file = fileName.Split('.');
                string dateTime = Convert.ToDateTime(DateTime.Now).ToString("yyyyMMdd HH:mm:ss").Replace(":", "").Replace(" ", "");
                path = Server.MapPath(savingPath) + file[0] + "_" + dateTime +"."+ file[1];
                if (System.IO.File.Exists(path)) System.IO.File.Delete(path);
                fileuploadExcel1.PostedFile.SaveAs(path);


                string excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;Persist Security Info=False";
                excelConnection = new OleDbConnection(excelConnectionString);
                //SqlBulkCopy sqlBulk = new SqlBulkCopy(strConnection);

                excelConnection.Open();

                cmd = new OleDbCommand("Select [Doc Date],[Country],[Company Name],[Bank Name],[Debit Account No],[Beneficiary Name],[Currency],[Amount],[Transfer Type],[Remarks],[Voucher No],'P'," + Convert.ToInt32(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID) + ",'" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss") + "' from [Sheet1$]", excelConnection);
                oleDAdapter = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();

                oleDAdapter.Fill(dt);
                
                dt.Columns.Add("UploadDocRef", typeof(System.String));
                foreach (DataRow row in dt.Rows)
                {
                    row["UploadDocRef"] = Convert.ToString("CHQ/" + dateTime);   // or set it to some other value
                }


                //Create connection string to Excel work book
                bool duplicateData = false;
                int i = 0;
                for (int j = 0; j < dt.Rows.Count; j++)
                {

                    for (int k = j + 1; k < dt.Rows.Count; k++)
                    {
			if (!string.IsNullOrEmpty(dt.Rows[j]["Voucher No"].ToString()))
	                        {
                        if (dt.Rows[j]["Voucher No"].ToString() == dt.Rows[k]["Voucher No"].ToString() && dt.Rows[j]["Company Name"].ToString() == dt.Rows[k]["Company Name"].ToString())
                        {
                            duplicateData = true;
                            i = j;
                        }
		   }
                    }
                }
                if (duplicateData != true)
                {
                    //foreach (DataRow dr in dt.Rows)
                    //{
                    //    dr["Doc Date"] = Convert.ToDateTime(dr["Doc Date"]).ToString("yyyy-MM-dd");
                    //}
                    dReader = cmd.ExecuteReader();



                    using (SqlConnection con = new SqlConnection(strConnection))
                    {
                        con.Open();
                        using (SqlTransaction sqlTransaction = con.BeginTransaction())
                        {
                            PaymentCheck PC = new PaymentCheck();
                            int result = PC.saveExcelFileDetails(Convert.ToString(file[0]), Convert.ToString("CHQ/" + dateTime), Convert.ToString(file[0] + "_" + dateTime + "." + Convert.ToString(file[1])), "." + Convert.ToString(file[1]), "A", "", Convert.ToInt64(Settings.LoginInfo.UserID));

                            if (result == 1)
                            {
                                using (SqlBulkCopy sqlBulk = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, sqlTransaction))
                                {
                                    //Set the database table name
                                    sqlBulk.DestinationTableName = "cheque_approval_queue";

                                    //[OPTIONAL]: Map the DataTable columns with that of the database table
                                    sqlBulk.ColumnMappings.Add(0, "DocDate");
                                    sqlBulk.ColumnMappings.Add(1, "CountryName");
                                    sqlBulk.ColumnMappings.Add(2, "CompanyName");
                                    sqlBulk.ColumnMappings.Add(3, "BankName");
                                    sqlBulk.ColumnMappings.Add(4, "DebitAcNo");
                                    sqlBulk.ColumnMappings.Add(5, "BeneficiaryName");
                                    sqlBulk.ColumnMappings.Add(6, "Currency");
                                    sqlBulk.ColumnMappings.Add(7, "Amount");
                                    sqlBulk.ColumnMappings.Add(8, "TransferType");
                                    sqlBulk.ColumnMappings.Add(9, "Remarks");
                                    sqlBulk.ColumnMappings.Add(10, "VoucherNo");
                                    sqlBulk.ColumnMappings.Add(11, "Status");
                                    sqlBulk.ColumnMappings.Add(12, "CreatedBy");
                                    sqlBulk.ColumnMappings.Add(13, "CreatedOn");
                                    sqlBulk.ColumnMappings.Add(14, "UploadDocRef");
                                    
                                    try
                                    {
                                        sqlBulk.WriteToServer(dt);
                                        sqlTransaction.Commit();
                                        lblErrorMessage.Text = "Uploading Successful";
                                        lblErrorMessage.Visible = true;
                                    }
                                    catch
                                    {
                                        sqlTransaction.Rollback();
                                        lblErrorMessage.Text = "Uploading Failed";
                                        lblErrorMessage.Visible = true;
                                    }
                                }
                            }
                        }
                        con.Close();
                        excelConnection.Close();
                        
                    }
                }
                else
                {
                    lblErrorMessage.Text = "Your Excel sheet is having the Duplicate data, at line number: " + ++i;
                    lblErrorMessage.Visible = true;
                    excelConnection.Close();
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = "Uploading Failed";
                lblErrorMessage.Visible = true;
                CT.TicketReceipt.Common.Utility.WriteLog(ex, "reconcile Uploader");
                CT.TicketReceipt.Common.Utility.Alert(this.Page, ex.Message);
                excelConnection.Close();
                excelConnection.Dispose();
                cmd.Dispose();
            }
            finally
            {
                //CT.TicketReceipt.Common.Utility.WriteLog("finally start", "reconcile Uploader");
                ////if (System.IO.File.Exists(path)) System.IO.File.Delete(path); //TOREMOVE

                //CT.TicketReceipt.Common.Utility.WriteLog("finally end", "reconcile Uploader");
                excelConnection.Close();
            }
        }
        catch(Exception ex) { throw; }
    }
}
