﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PrintPackageUI"
    Codebehind="PrintPackage.aspx.cs" %>
    <%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<head id="Head1" runat="server">


</head>

    <form id="form1">
        
    
    
   <script type="text/javascript" language="javascript">
        function printpage() {
            window.print()
        }
</script>
    
   


    
<div><input type="button" value="Print this page" onclick="printpage()"></div>
  
    
  
    
    
    
   
   <table style="line-height:17px" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    
    <table class="themecol1" style="border:solid 1px #000;" width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td  width="50%" height="30" ><label style=" color:#fff; font-size:18px; padding-left:10px">
        
        
        
        
        
        
 <%=deal.DealName %>
                    (<%=deal.Nights %>
                        Nights &amp;
                        <%=deal.Nights + 1 %>
                        Days) 
        
        
        
        
        
        
        
        </label></td>
        <td width="50%" align="right">
        
        
        <label style="padding-right:15px; color:#FFFFFF; font-size:16px">Starting From- <%=Settings.LoginInfo.Currency %> <%=minPrice.ToString("N"+Settings.LoginInfo.DecimalValue) %></label>
        
        <label style="padding-right:15px; color:#FFFFFF">per person</label>        </td>
      </tr>
      
    </table>
    
    </td>
  </tr>
  
  <tr>
    <td style="padding-top:5px">
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top" width="30%">
        
        
        
        <img width="366px" height="247px" src="<%=imageServerPath + deal.MainImagePath%>" alt="<%=deal.DealName %>" title="<%=deal.DealName %>" />
        
        
        </td>
        <td width="70%" valign="top"><table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
          <tr>
            <td height="0"><label style="color:#cc0000; font-size:16px"><strong>Overview</strong></label></td>
          </tr>
          <tr>
            <td style=" vertical-align:top;font-size:14px!important" valign="top">
            
            
            <label>
             <%=deal.Overview %>
            
            
            </label>
              
              
              
               </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  
  <tr>
    <td><table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
      <tr>
        <td height="0"><label style="color:#cc0000; font-size:16px"><strong>Inclusions </strong></label></td>
      </tr>
   
      <tr>
        <td>
        
       <div style=" margin-top:-10px"><label style=" font-size:14px"></label>
                           
                              
                              <ul style="padding:0px 20px 0px 15px;">
                               
                                <%for (int i = 0; i < inclusionsLimit; i++)
                              { %>
                          
                               <li style="list-style-type:circle; padding-bottom:10px; padding-top:10px; ">
                                          <%=inclusions[i]%>
                                       
                                
                               
                                </li>
                                
                                
                                <%if ((i + inclusionsLimit) < (inclusions.Length))
                                  { %>
                                <li style="list-style-type:circle; padding-bottom:10px ">
                                    
                                            <%=inclusions[i + inclusionsLimit]%>
                                       
                                       
                                       </li>
                                  <%} %>
                           
                            <%} %>
                           
                                </ul>  
                        </div>
          
          
          </td>
      </tr>
      <tr>
      <td>
      <div>
     <label style="color:#cc0000; font-size:16px"><strong>Exclusions </strong></label><br />
                                  <ul style="padding:0px 20px 0px 15px;">
                            
                            <%string[] notes = deal.Notes.Split('&'); %>
                                <%for (int i = 0; i < notes.Length; i++)
                                  { %>
                                  <%if (notes[i].Replace("#", "").Trim().Length > 0) %>
                                  <%{ %>
                                <li style="list-style-type:circle; padding-bottom:10px">
                                     
                                            <%=notes[i].Replace("#", "")%>
                                        
                                </li>
                                <%}
                                  } %>
                            </ul>
                              </div>
      </td>
      </tr>
    </table></td>
  </tr>
  
  <tr>
    <td><table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
      <tr>
        <td height="0"><label style="color:#cc0000; font-size:16px"><strong>Itinerary</strong></label></td>
      </tr>
      <tr>
        <td valign="top">
        <label style=" font-size:14px">
        
  
  
  
  
<span>
 <%for (int i = 0; i < (itinaryList.Count / 2 + itinaryList.Count % 2); i++)
                                  {
                                      dayItinerary = itinaryList[i].Split(splitter, StringSplitOptions.RemoveEmptyEntries);%>
                               
                               
                               
                             
                                    
                                    
                                    
                                    <div style=" padding-bottom:7px"><b>Day
                                            <%=i+1 %>
                                            :
                                            <%=dayItinerary[0] %>
                                        </b></div>
                                           
                                           
                                           
                                           
                                            <%=dayItinerary[1] %>
                                            <br /> 
                                           
                                           
                                             <div style=" padding-top:7px; padding-bottom:7px">
                                            <b>Meals: </b>
                                            
                                            <%=dayItinerary[2] %>
                                           
                                           </div>
                                            
                                           
                                           
                                            
                                            <%if (dayItinerary[3] != "N.A" && dayItinerary[3] != "N.A.")
                                              { %>
                                            <b>Optional: </b>
                                            <%=dayItinerary[3]%>
                                            <br />
                                            <%} %>
                                       
                                       <br />
                                       
                               
                                
                                
                                
                                <%} %>
                            </span>
  
  
  
  <span class="package_subpart">
                                <%for (int i = (itinaryList.Count / 2 + itinaryList.Count % 2); i < itinaryList.Count; i++)
                                  {
                                      dayItinerary = itinaryList[i].Split(splitter, StringSplitOptions.RemoveEmptyEntries);%>
                              
                                    <%--<i><img src="Images/blue_bullet.gif" alt="Bullet" /></i> --%>
                                
                                 <div style=" padding-bottom:7px">  <b>Day
                                            <%=i+1 %>
                                            :
                                            <%=dayItinerary[0] %>
                                        </b>
                                        
                                          </div>
                                            
                                            
                                            <%=dayItinerary[1] %>
                                            <br />
                                            
                                            <div style=" padding-top:7px; padding-bottom:7px"> 
                                            <b>Meals: </b>
                                            
                                            <%=dayItinerary[2] %>
                                            </div>
                                            
                                            <%if (dayItinerary[3] != "N.A")
                                              { %>
                                            
                                            <b>Optional: </b>
                                            
                                            <%=dayItinerary[3]%>
                                            <br />
                                            <%} %>
                                       
                                        
                               <br />
                                
                                
                                <%} %>
                            </span>
  
  
          
          
          
          
          </label>
          
          
          </td>
      </tr>
    </table></td>
  </tr>
  
  <tr>
    <td><table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
      <tr>
        <td height="0"><label style="color:#cc0000; font-size:16px"><strong>Room Rates</strong></label></td>
      </tr>
      <tr>
        <td>
        <label style=" font-size:14px">
        
        <div id="RoomRates">
<div>
                       
                                             
                       <%=deal.RoomRates %>
                       
                       
                       
                        
                        
                        
                        
                    

                      
                        
                    </div>
    
    
    
    </div>
        
        </label>
        
        
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
      <tr>
        <td height="0"><label style="color:#cc0000; font-size:16px"><strong>Terms & Conditions</strong></label></td>
      </tr>
      
      <tr>
        <td>
        <label style=" font-size:14px"></label>
       <div>
        <%if (terms.Length > 0)
          { %>
                            <div style=" margin-top:-10px">
                              
                               
                                
                                
                               <ul style="padding:0px 20px 0px 15px;">
                                    
                                    
                                     <%for (int i = 0; i < terms.Length; i++)
                                  { %>
                               
                                    <li style="list-style-type:circle; padding-bottom:10px; padding-top:10px;">
                                            <%=terms[i]%>
                                        
                               </li> <%} %>
                               
                               </ul>
                               </div>
                            <%} %>
                            </div>
                           <%-- <label style="color:#cc0000; font-size:16px"><strong>Prices does not include </strong></label><br />
       
                            <%if (price.Length > 0)
                              { %>
                            <div class="">
                               <ul>
                                <%for (int i = 0; i < price.Length; i++)
                                  { %>
                                <label>
                                     <li style="list-style-type:circle">
                                            <%=price[i] %>
                                        </li>
                                </label>
                                <%} %>
                                </ul>
                            </div>
                            <%} %>
                            <br />
                           
                               
                               
                               
                            
                        </div>
          
          </label>--%>
          
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
   
   
   
   
   
    <%-- <input type="hidden" id="agencyId" name="agencyId" value="<%=holidaySettings.AgencyId %>" />
    <input type="hidden" id="packageId" name="packageId" value="<%=deal.DealId %>" />
    <input type="hidden" id="dealName" name="dealName" value="<%=deal.DealName %>" />
    <input type="hidden" id="nights" name="nights" value="<%=deal.Nights %>" />
    <input type="hidden" id="isInternational" name="isInternational" value="<%=deal.IsInternational %>" />
   <span id="bookNow" style="display:none"><a class="clik" href="<%=redirectToPage %><%=deal.DealId %>" >Book Now</a></span>--%>
   
   
   
    <%if (itinaryList.Count > 0)
                      { %>
                    <%--<div class="packages_fulldescription_content width_100">
                        <tt>Itinerary : </tt>
                        <div class="package_subcontent">
                            
                        </div>
                    </div>--%>
                    <%} %>
                    
   
    
    
    </form>
