﻿#region Referenced Assemblies

using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System.Collections.Generic;
using CT.TicketReceipt.Web.UI.Controls;
using CT.BookingEngine;
using CT.TicketReceipt.Common;

#endregion


public partial class APISSourceMasterGUI : CT.Core.ParentPage
{
    private string APIS_SEARCH_SESSION = "_APISSearchList";

    private int MandateId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {

                if (!Page.IsPostBack)
                {
                    Clear(); //Clears the user input.
                    IntialiseControls();//Loads the airlines,countries and page fields.
                }
            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, ex.Message, "0");
        }

    }
    /// <summary>
    /// Intialises the AirSources,Country,AirLines,Fields List
    /// </summary>
    private void IntialiseControls()
    {
        try
        {
            BindAirSourcesList();
            BindCountryList();
            BindAirLinesList();
            BindFieldsList();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Clears all the controls on the page
    /// </summary>
    private void Clear()
    {
        try
        {
            ddlAirSources.SelectedIndex = 0;
            hdfMode.Value = "0";
            hdfEMId.Value = "0";
            btnCancel.Text = "Cancel";
            btnSave.Text = "Save";
            rbtnCountry.Checked = false;
            rbtnAirline.Checked = false;
            rbtnCommon.Checked = false;
            chkAirLines.ClearSelection();
            chkCountries.ClearSelection();
            chkFields.ClearSelection();
            lblSuccess.Text = string.Empty;
            ddlTransType.Enabled = true;
            ddlTransType.SelectedIndex = 0;
            ddlFlightType.SelectedIndex = 0;
            lblError.Text = "";
            lblSuccess.Text = "";
           


        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Binds the AirSourcesList
    /// </summary>
    private void BindAirSourcesList()
    {
        try
        {
            DataTable dtAirSources = AirlineMinimalFields.GetAllAPISAirSources(1);
            for (int i = 0; i < dtAirSources.Rows.Count; i++)
            {
                if (dtAirSources.Rows[i]["Name"].ToString() == "UA")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.UAPI;
                }
                else if (dtAirSources.Rows[i]["Name"].ToString() == "G9")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.AirArabia;
                }
                else if (dtAirSources.Rows[i]["Name"].ToString() == "FZ")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.FlyDubai;
                }
                else if (dtAirSources.Rows[i]["Name"].ToString() == "SG")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.SpiceJet;
                }
                else if (dtAirSources.Rows[i]["Name"].ToString() == "TA")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.TBOAir;
                }
                else if (dtAirSources.Rows[i]["Name"].ToString() == "6E")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.Indigo;
                }
                else if (dtAirSources.Rows[i]["Name"].ToString() == "IX")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.AirIndiaExpressIntl;
                }
                else if (dtAirSources.Rows[i]["Name"].ToString() == "G8")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.GoAir;
                }
                //Added by lokesh on 12-March-2018 Regarding PK Fares.
                //In table BKE_ACTIVE_SOURCE we have the source name as PK.
                //But while retrieving the mandateFields we will pass bookingsource enum.

                else if (dtAirSources.Rows[i]["Name"].ToString() == "PK")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.PKFares;
                }
                //Added by lokesh on 20-June-2018
                //Regarding Amadeus Air Source
                else if (dtAirSources.Rows[i]["Name"].ToString() == "1A")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.Amadeus;
                }

                else if (dtAirSources.Rows[i]["Name"].ToString() == "SGCORP")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.SpiceJetCorp;
                }

                else if (dtAirSources.Rows[i]["Name"].ToString() == "6ECORP")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.IndigoCorp;
                }
                else if (dtAirSources.Rows[i]["Name"].ToString() == "J9")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.Jazeera;
                }
                else if (dtAirSources.Rows[i]["Name"].ToString() == "G8CORP")
                {
                    dtAirSources.Rows[i]["Name"] = BookingSource.GoAirCorp;
                }
            }
            ddlAirSources.DataSource = dtAirSources;
            ddlAirSources.DataValueField = "Code";
            ddlAirSources.DataTextField = "Name";
            ddlAirSources.AppendDataBoundItems = true;
            ddlAirSources.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Binds the Countries List
    /// </summary>
    private void BindCountryList()
    {
        try
        {

            chkCountries.DataSource = CountryMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            chkCountries.DataValueField = "COUNTRY_CODE";
            chkCountries.DataTextField = "COUNTRY_NAME";
            chkCountries.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    /// <summary>
    /// Binds the airlines List
    /// </summary>
    private void BindAirLinesList()
    {
        try
        {
            DataTable dtAirlines = Airline.GetAllAirlinesList();
            DataView view = dtAirlines.DefaultView;
            view.Sort = "airlineName ASC";
            dtAirlines = view.ToTable();
            chkAirLines.DataSource = dtAirlines;
            chkAirLines.DataTextField = "airlineName";
            chkAirLines.DataValueField = "airlineCode";
            chkAirLines.DataBind();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    /// <summary>
    /// Binds the ManadateFields
    /// </summary>
    private void BindFieldsList()
    {
        try
        {
            DataTable dtPageFields = AirlineMinimalFields.GetAllPageFields();
            chkFields.DataSource = dtPageFields;
            chkFields.DataTextField = "Field";
            chkFields.DataValueField = "FieldId";
            chkFields.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //saves or updates the user input into the tables BKE_APIS_Mandate_Sources and BKE_APIS_Mandate_Source_Fields 
            //Irrespective of country ,airline or common(incluedes both airline and country)
            Save();
            Clear();
            if (MandateId > 0)
            {

                lblSuccess.Text = "Details saved successfully !";
            }
            else
            {

                lblError.Text = "Row already exist for same criteria !";
            }


        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(APISSourceMaster Page. Save method Error:)" + ex.Message, "0");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();//Clears the user inputs on the page
            this.Master.HideSearch();
            Utility.StartupScript(this.Page, "clearControls();", "SCRIPT");

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(APISSourceMaster Page. Cancel method Error:)" + ex.Message, "0");
        }
    }

    /// <summary>
    /// Gets the Selected Mandatory Fields that needs to be saved or updated against the country or airline or common(includes both airline and country)
    /// </summary>
    /// <returns></returns>
    private List<AirlineMandateSourceFields> GetSelectedPageFieldsList()
    {
        List<AirlineMandateSourceFields> mandateSourceFields = null;
        try
        {
            int selectedPageFieldsCount = chkFields.Items.Cast<ListItem>().Count(li => li.Selected);
            if (selectedPageFieldsCount > 0)
            {
                mandateSourceFields = new List<AirlineMandateSourceFields>();
                AirlineMinimalFields minimalFields = new AirlineMinimalFields(Convert.ToInt32(hdfEMId.Value));
                mandateSourceFields.AddRange(minimalFields.MandateSourcesFieldsList);
                foreach (ListItem item in chkFields.Items)
                {
                    if (item.Selected)
                    {
                        if (hdfMode.Value == "1")//During edit mode.
                        {
                            AirlineMandateSourceFields mandateSourceFieldObj = minimalFields.MandateSourcesFieldsList.Where(i => i.Field.ToString().ToUpper() == item.Value.ToUpper()).FirstOrDefault();
                            mandateSourceFieldObj.Status = "A";
                        }
                    }
                    else
                    {
                        AirlineMandateSourceFields mandateSourceFieldObj = minimalFields.MandateSourcesFieldsList.Where(i => i.Field.ToString().ToUpper() == item.Value.ToUpper()).FirstOrDefault();
                        mandateSourceFieldObj.Status = "D";
                    }

                }

            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
        return mandateSourceFields;
    }

    /// <summary>
    /// Gets the  Mandatory Fields List irrespective of country ,airline or common (includes both country and airline)
    /// </summary>
    /// <returns></returns>
    private List<AirlineMandateSourceFields> GetFieldsList()
    {
        List<AirlineMandateSourceFields> mandateSourceFields = null;
        try
        {
            int selectedPageFieldsCount = chkFields.Items.Cast<ListItem>().Count(li => li.Selected);
            if (selectedPageFieldsCount > 0)
            {
                mandateSourceFields = new List<AirlineMandateSourceFields>();
                foreach (ListItem item in chkFields.Items)
                {

                    AirlineMandateSourceFields mandateSourceFieldObj = new AirlineMandateSourceFields();
                    mandateSourceFieldObj.Field = item.Value;
                    if (item.Selected)
                    {
                        mandateSourceFieldObj.Status = "A"; //field selected
                    }
                    else
                    {
                        mandateSourceFieldObj.Status = "D"; //field not selected
                    }
                    //if (chkIsDomestic.Checked)
                    //{
                    //    mandateSourceFieldObj.IsDomestic = true;
                    //}
                    //if (ddlTransType.SelectedIndex > 0)
                    //{
                    //    mandateSourceFieldObj.TransType = ddlTransType.SelectedItem.Value;
                    //}
                    mandateSourceFieldObj.MFId = -1;
                    mandateSourceFields.Add(mandateSourceFieldObj);

                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return mandateSourceFields;
    }

    /// <summary>
    /// Save the Header Details as well as fields for the selected country
    /// </summary>
    private void SaveCountryFields()
    {
        try
        {
            string flightType = ddlFlightType.SelectedItem.Text;
            DataRow[] foundRows = null;
            DataRow[] domesticTypeRows = null;
            DataRow[] internationalTypeRows = null;
            string find = "";
            if (hdfMode.Value == "0") //Insert
            {

                int selectedCountriesCount = chkCountries.Items.Cast<ListItem>().Count(li => li.Selected);
                int selectedPageFieldsCount = chkFields.Items.Cast<ListItem>().Count(li => li.Selected);
                if (selectedCountriesCount > 0 && selectedPageFieldsCount > 0)
                {
                    foreach (ListItem item in chkCountries.Items)
                    {
                        if (item.Selected)
                        {
                            AirlineMinimalFields minimalFieldObj = new AirlineMinimalFields();
                            minimalFieldObj.ProductId = 1;
                            minimalFieldObj.Source = ddlAirSources.SelectedItem.Text;
                            minimalFieldObj.Country = item.Value;
                            minimalFieldObj.Airline = string.Empty;
                            minimalFieldObj.Status = "A";
                            minimalFieldObj.MandateSourcesFieldsList = GetFieldsList();
                            minimalFieldObj.TransType = ddlTransType.SelectedItem.Text;
                            minimalFieldObj.FlightType = ddlFlightType.SelectedItem.Text;
                            DataTable dt = AirlineMinimalFields.GetApiMandateSources(1, ddlAirSources.SelectedItem.Text);
                            if (flightType == "ALL")
                            {
                                find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Country='" + minimalFieldObj.Country + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='ALL' ";
                                domesticTypeRows = dt.Select(find);
                                if (domesticTypeRows.Length == 0)
                                {
                                    find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Country='" + minimalFieldObj.Country + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='DOMESTIC' ";
                                    domesticTypeRows = dt.Select(find);
                                    find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Country='" + minimalFieldObj.Country + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='INTERNATIONAL' ";
                                    internationalTypeRows = dt.Select(find);
                                }

                            }
                            else if (flightType == "DOMESTIC")
                            {
                                find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Country='" + minimalFieldObj.Country + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='ALL' ";
                                foundRows = dt.Select(find);
                                if (foundRows.Length == 0)
                                {
                                    find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Country='" + minimalFieldObj.Country + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='DOMESTIC' ";
                                    foundRows = dt.Select(find);
                                }
                            }
                            else if (flightType == "INTERNATIONAL")
                            {
                                find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Country='" + minimalFieldObj.Country + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='ALL' ";
                                foundRows = dt.Select(find);
                                if (foundRows.Length == 0)
                                {
                                    find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Country='" + minimalFieldObj.Country + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='INTERNATIONAL' ";
                                    foundRows = dt.Select(find);
                                }
                            }
                            
                            if (foundRows != null && foundRows.Length == 0)
                            {
                                minimalFieldObj.Save();
                            }
                            else if (flightType == "ALL" && (domesticTypeRows.Length == 0 && internationalTypeRows.Length == 0))
                            {
                                minimalFieldObj.Save();
                            }

                            if (minimalFieldObj.IsSavedSuccessfully)
                            {
                                MandateId = 1;
                            }
                        }
                    }
                }
            }
            else if (hdfMode.Value == "1") //Update
            {
                AirlineMinimalFields minimalFieldObj = new AirlineMinimalFields((Convert.ToInt32(hdfEMId.Value)));
                if (flightType != minimalFieldObj.FlightType)
                {

                    DataTable dt = AirlineMinimalFields.GetApiMandateSources(1, ddlAirSources.SelectedItem.Text);
                    if (flightType == "ALL")
                    {
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Country='" + minimalFieldObj.Country + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='DOMESTIC' ";
                        domesticTypeRows = dt.Select(find);
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Country='" + minimalFieldObj.Country + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='INTERNATIONAL' ";
                        internationalTypeRows = dt.Select(find);

                    }
                    else
                    {
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Country='" + minimalFieldObj.Country + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='" + flightType + "' ";
                        foundRows = dt.Select(find);
                    }
                }

                minimalFieldObj.MandateSourcesFieldsList = GetSelectedPageFieldsList();

                if (chkStatus.Checked)
                {
                    minimalFieldObj.Status = "A";
                }
                else
                {
                    minimalFieldObj.Status = "D";
                }
                //minimalFieldObj.TransType = ddlTransType.SelectedItem.Text;
                if ((foundRows != null && foundRows.Length == 0) || (flightType == minimalFieldObj.FlightType))
                {
                    minimalFieldObj.FlightType = ddlFlightType.SelectedItem.Text;
                    minimalFieldObj.Save();
                }
                else if (flightType == "ALL" && (domesticTypeRows.Length == 0 || internationalTypeRows.Length == 0))
                {
                    minimalFieldObj.FlightType = ddlFlightType.SelectedItem.Text;
                    minimalFieldObj.Save();
                }
                if (minimalFieldObj.IsSavedSuccessfully)
                {
                    MandateId = 1;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Save the Header Details as well as fields for the selected airline
    /// </summary>
    private void SaveAirlineFields()
    {
        try
        {
            string flightType = ddlFlightType.SelectedItem.Text;
            string find = "";
            DataRow[] foundRows = null;
            DataRow[] domesticTypeRows = null;
            DataRow[] internationalTypeRows = null;
            if (hdfMode.Value == "0")//Insert
            {

                int selectedAirlinesCount = chkAirLines.Items.Cast<ListItem>().Count(li => li.Selected);
                int selectedPageFieldsCount = chkFields.Items.Cast<ListItem>().Count(li => li.Selected);
                if (ddlAirSources.SelectedItem.Value == "UA" || ddlAirSources.SelectedItem.Value == "TA" || ddlAirSources.SelectedItem.Value == "PK" || ddlAirSources.SelectedItem.Value == "1A")
                {
                    if (selectedAirlinesCount > 0 && selectedPageFieldsCount > 0)
                    {
                        foreach (ListItem item in chkAirLines.Items)
                        {
                            if (item.Selected)
                            {
                                AirlineMinimalFields minimalFieldObj = new AirlineMinimalFields();
                                minimalFieldObj.ProductId = 1;
                                minimalFieldObj.Source = ddlAirSources.SelectedItem.Text;
                                minimalFieldObj.Country = string.Empty;
                                minimalFieldObj.Airline = item.Value;
                                minimalFieldObj.Status = "A";
                                minimalFieldObj.MandateSourcesFieldsList = GetFieldsList();
                                minimalFieldObj.TransType = ddlTransType.SelectedItem.Text;
                                minimalFieldObj.FlightType = ddlFlightType.SelectedItem.Text;

                                DataTable dt = AirlineMinimalFields.GetApiMandateSources(1, ddlAirSources.SelectedItem.Text);

                                if (flightType == "ALL")
                                {
                                    find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='ALL' ";
                                    domesticTypeRows = dt.Select(find);
                                    if (domesticTypeRows.Length == 0)
                                    {
                                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='DOMESTIC' ";
                                        domesticTypeRows = dt.Select(find);
                                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='INTERNATIONAL' ";
                                        internationalTypeRows = dt.Select(find);
                                    }

                                }
                                else if (flightType == "DOMESTIC")
                                {
                                    find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='ALL' ";
                                    foundRows = dt.Select(find);
                                    if (foundRows.Length == 0)
                                    {
                                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='DOMESTIC' ";
                                        foundRows = dt.Select(find);
                                    }
                                }
                                else if (flightType == "INTERNATIONAL")
                                {
                                    find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='ALL' ";
                                    foundRows = dt.Select(find);
                                    if (foundRows.Length == 0)
                                    {
                                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='INTERNATIONAL' ";
                                        foundRows = dt.Select(find);
                                    }
                                }
                                if (foundRows != null && foundRows.Length == 0)
                                {

                                    minimalFieldObj.Save();
                                }
                                else if (flightType == "ALL" && (domesticTypeRows.Length == 0 && internationalTypeRows.Length == 0))
                                {

                                    minimalFieldObj.Save();
                                }
                                if (minimalFieldObj.IsSavedSuccessfully)
                                {
                                    MandateId = 1;
                                }
                            }
                        }
                    }
                }
                else if (ddlAirSources.SelectedItem.Value == "G9" || ddlAirSources.SelectedItem.Value == "FZ" || ddlAirSources.SelectedItem.Value == "SG" || ddlAirSources.SelectedItem.Value == "6E" || ddlAirSources.SelectedItem.Value == "IX" || ddlAirSources.SelectedItem.Value == "G8" || ddlAirSources.SelectedItem.Value == "SGCORP" || ddlAirSources.SelectedItem.Value == "6ECORP" || ddlAirSources.SelectedItem.Value == "J9" || ddlAirSources.SelectedItem.Value == "G8CORP")
                {
                    AirlineMinimalFields minimalFieldObj = new AirlineMinimalFields();
                    minimalFieldObj.ProductId = 1;
                    minimalFieldObj.Source = ddlAirSources.SelectedItem.Text;
                    minimalFieldObj.Country = string.Empty;
                    switch (ddlAirSources.SelectedItem.Value)
                    {
                        case "G9":
                            minimalFieldObj.Airline = "G9";
                            break;
                        case "FZ":
                            minimalFieldObj.Airline = "FZ";
                            break;
                        case "SG":
                        case "SGCORP":
                            minimalFieldObj.Airline = "SG";
                            break;
                        case "6E":
                        case "6ECORP":
                            minimalFieldObj.Airline = "6E";
                            break;
                        case "IX":
                            minimalFieldObj.Airline = "IX";
                            break;
                        case "G8":
                            minimalFieldObj.Airline = "G8";
                            break;
                        case "J9":
                            minimalFieldObj.Airline = "J9";
                            break;
                    }
                    minimalFieldObj.Status = "A";
                    minimalFieldObj.MandateSourcesFieldsList = GetFieldsList();
                    minimalFieldObj.TransType = ddlTransType.SelectedItem.Text;
                    minimalFieldObj.FlightType = ddlFlightType.SelectedItem.Text;
                    DataTable dt = AirlineMinimalFields.GetApiMandateSources(1, ddlAirSources.SelectedItem.Text);

                    if (flightType == "ALL")
                    {
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='ALL' ";
                        domesticTypeRows = dt.Select(find);
                        if (domesticTypeRows.Length == 0)
                        {
                            find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='DOMESTIC' ";
                            domesticTypeRows = dt.Select(find);
                            find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='INTERNATIONAL' ";
                            internationalTypeRows = dt.Select(find);
                        }

                    }
                    else if(flightType == "DOMESTIC")
                    {
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='ALL' ";
                        foundRows = dt.Select(find);
                        if(foundRows.Length==0)
                        {
                            find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='DOMESTIC' ";
                            foundRows = dt.Select(find);
                        }
                    }
                    else if (flightType == "INTERNATIONAL")
                    {
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='ALL' ";
                        foundRows = dt.Select(find);
                        if (foundRows.Length == 0)
                        {
                            find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='INTERNATIONAL' ";
                            foundRows = dt.Select(find);
                        }
                    }
                    
                    if (foundRows != null && foundRows.Length == 0)
                    {
                        minimalFieldObj.Save();
                    }
                    else if (flightType == "ALL" && (domesticTypeRows.Length == 0 && internationalTypeRows.Length == 0))
                    {

                        minimalFieldObj.Save();
                    }

                    if (minimalFieldObj.IsSavedSuccessfully)
                    {
                        MandateId = 1;
                    }
                }
            }
            else if (hdfMode.Value == "1") //Update//
            {
                AirlineMinimalFields minimalFieldObj = new AirlineMinimalFields((Convert.ToInt32(hdfEMId.Value)));

                if (flightType != minimalFieldObj.FlightType)
                {
                    DataTable dt = AirlineMinimalFields.GetApiMandateSources(1, ddlAirSources.SelectedItem.Text);
                    if (flightType == "ALL")
                    {
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='DOMESTIC' ";
                        domesticTypeRows = dt.Select(find);
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='INTERNATIONAL' ";
                        internationalTypeRows = dt.Select(find);

                    }
                    else
                    {
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline='" + minimalFieldObj.Airline + "' AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='" + flightType + "' ";
                        foundRows = dt.Select(find);
                    }
                }

                minimalFieldObj.MandateSourcesFieldsList = GetSelectedPageFieldsList();
                if (chkStatus.Checked)
                {
                    minimalFieldObj.Status = "A";
                }
                else
                {
                    minimalFieldObj.Status = "D";
                }
                if ((foundRows != null && foundRows.Length == 0) || (flightType == minimalFieldObj.FlightType))
                {
                    minimalFieldObj.TransType = ddlTransType.SelectedItem.Text;
                    minimalFieldObj.FlightType = ddlFlightType.SelectedItem.Text;
                    minimalFieldObj.Save();
                }
                else if (flightType == "ALL" && (domesticTypeRows.Length == 0 || internationalTypeRows.Length == 0))
                {
                    minimalFieldObj.TransType = ddlTransType.SelectedItem.Text;
                    minimalFieldObj.FlightType = ddlFlightType.SelectedItem.Text;
                    minimalFieldObj.Save();
                }
                if (minimalFieldObj.IsSavedSuccessfully)
                {
                    MandateId = 1;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Save the Header Details as well as fields common to both airline and country
    /// </summary>
    private void SaveCommonFields()
    {
        try
        {
            string flightType = ddlFlightType.SelectedItem.Text;
            string find = "";
            DataRow[] foundRows = null;
            DataRow[] domesticTypeRows = null;
            DataRow[] internationalTypeRows = null;
            if (hdfMode.Value == "0")//Insert
            {

                int selectedPageFieldsCount = chkFields.Items.Cast<ListItem>().Count(li => li.Selected);
                if (selectedPageFieldsCount > 0)
                {

                    AirlineMinimalFields minimalFieldObj = new AirlineMinimalFields();
                    minimalFieldObj.ProductId = 1;
                    minimalFieldObj.Source = ddlAirSources.SelectedItem.Text;
                    minimalFieldObj.Country = string.Empty;
                    minimalFieldObj.Airline = string.Empty;
                    minimalFieldObj.Status = "A";
                    minimalFieldObj.MandateSourcesFieldsList = GetFieldsList();
                    minimalFieldObj.TransType = ddlTransType.SelectedItem.Text;
                    minimalFieldObj.FlightType = ddlFlightType.SelectedItem.Text;
                    DataTable dt = AirlineMinimalFields.GetApiMandateSources(1, ddlAirSources.SelectedItem.Text);


                    if (flightType == "ALL")
                    {
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline IS NULL AND Country IS NULL AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='ALL' ";
                        domesticTypeRows = dt.Select(find);
                        if (domesticTypeRows.Length == 0)
                        {
                            find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline IS NULL AND Country IS NULL AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='DOMESTIC' ";
                            domesticTypeRows = dt.Select(find);
                            find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline IS NULL AND Country IS NULL AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='INTERNATIONAL' ";
                            internationalTypeRows = dt.Select(find);
                        }

                    }
                    else if (flightType == "DOMESTIC")
                    {
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline IS NULL AND Country IS NULL  AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='ALL' ";
                        foundRows = dt.Select(find);
                        if (foundRows.Length == 0)
                        {
                            find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline IS NULL AND Country IS NULL  AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='DOMESTIC' ";
                            foundRows = dt.Select(find);
                        }
                    }
                    else if (flightType == "INTERNATIONAL")
                    {
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline IS NULL AND Country IS NULL  AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='ALL' ";
                        foundRows = dt.Select(find);
                        if (foundRows.Length == 0)
                        {
                            find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline IS NULL AND Country IS NULL  AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='INTERNATIONAL' ";
                            foundRows = dt.Select(find);
                        }
                    }

                    if (foundRows != null && foundRows.Length == 0)
                    {

                        minimalFieldObj.Save();
                    }
                    else if (flightType == "ALL" && (domesticTypeRows.Length == 0 && internationalTypeRows.Length == 0))
                    {

                        minimalFieldObj.Save();
                    }
                    if (minimalFieldObj.IsSavedSuccessfully)
                    {
                        MandateId = 1;
                    }
                }
            }
            else if (hdfMode.Value == "1")//Update
            {

                AirlineMinimalFields minimalFieldObj = new AirlineMinimalFields((Convert.ToInt32(hdfEMId.Value)));

                if (flightType != minimalFieldObj.FlightType)
                {

                    DataTable dt = AirlineMinimalFields.GetApiMandateSources(1, ddlAirSources.SelectedItem.Text);
                    if (flightType == "ALL")
                    {
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline IS NULL AND Country IS NULL AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='DOMESTIC' ";
                        domesticTypeRows = dt.Select(find);
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline IS NULL AND Country IS NULL AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='INTERNATIONAL' ";
                        internationalTypeRows = dt.Select(find);
                    }
                    else
                    {
                        find = "ProductId='" + minimalFieldObj.ProductId + "' AND Source = '" + minimalFieldObj.Source + "' AND Airline IS NULL AND Country IS NULL AND TransType='" + minimalFieldObj.TransType + "' AND FlightType='" + flightType + "' ";
                        foundRows = dt.Select(find);
                    }
                }

                minimalFieldObj.MandateSourcesFieldsList = GetSelectedPageFieldsList();
                //minimalFieldObj.FlightType = ddlFlightType.SelectedItem.Text;
                //minimalFieldObj.TransType = ddlTransType.SelectedItem.Text;
                if ((foundRows != null && foundRows.Length == 0) || (flightType == minimalFieldObj.FlightType))
                {
                    minimalFieldObj.TransType = ddlTransType.SelectedItem.Text;
                    minimalFieldObj.FlightType = ddlFlightType.SelectedItem.Text;
                    minimalFieldObj.Save();
                }
                else if (flightType == "ALL" && (domesticTypeRows.Length == 0 || internationalTypeRows.Length == 0))
                {
                    minimalFieldObj.TransType = ddlTransType.SelectedItem.Text;
                    minimalFieldObj.FlightType = ddlFlightType.SelectedItem.Text;
                    minimalFieldObj.Save();
                }
                if (minimalFieldObj.IsSavedSuccessfully)
                {
                    MandateId = 1;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    /// <summary>
    /// Save the Header Details as well as fields.
    /// </summary>
    private void Save()
    {
        try
        {

            if (rbtnCountry.Checked)
            {
                SaveCountryFields();//Save mandatory fields against to country
            }
            else if (rbtnAirline.Checked)
            {
                SaveAirlineFields();//Save mandatory fields against to airline
            }
            else if (rbtnCommon.Checked)
            {
                SaveCommonFields();//Save mandatory fields common to both airline and counrty
            }
            else
            {
            }
            Clear();//Once saving or updation is done clear the user input.

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            long vsId = Utility.ToLong(gvSearch.SelectedValue);
            Edit(vsId);
            this.Master.HideSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Audit.Add(EventType.Airline, Severity.High, 1, "(APISSourceMasterPage)gvSearch_SelectedIndexChanged method .Error:" + ex.Message, "0");
        }
    }

    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(APISSourceMasterPage)gvSearch_PageIndexChanging method .Error:" + ex.Message, "0");

        }

    }

    /// <summary>
    /// Edit the respective Manadate Record
    /// </summary>
    /// <param name="id"></param>
    private void Edit(long id)
    {
        try
        {

            Clear();
            hdfEMId.Value = Utility.ToString(id);

            AirlineMinimalFields minimalFields = new AirlineMinimalFields(id);
            if (minimalFields.Source == "UAPI")
            {
                ddlAirSources.SelectedValue = "UA";
            }
            else if (minimalFields.Source == "AirArabia")
            {
                ddlAirSources.SelectedValue = "G9";
            }
            else if (minimalFields.Source == "FlyDubai")
            {
                ddlAirSources.SelectedValue = "FZ";
            }
            else if (minimalFields.Source == "SpiceJet")
            {
                ddlAirSources.SelectedValue = "SG";
            }
            else if (minimalFields.Source == "TBOAir")
            {
                ddlAirSources.SelectedValue = "TA";
            }
            else if (minimalFields.Source == "Indigo")
            {
                ddlAirSources.SelectedValue = "6E";
            }
            else if (minimalFields.Source == "AirIndiaExpressIntl")
            {
                ddlAirSources.SelectedValue = "IX";
            }
            else if (minimalFields.Source == "GoAir")
            {
                ddlAirSources.SelectedValue = "G8";
            }
            else if (minimalFields.Source == "PKFares")
            {
                ddlAirSources.SelectedValue = "PK";
            }
            else if (minimalFields.Source == "Amadeus")//Added by lokesh on 20-06-18 for Amadeus Purpose
            {
                ddlAirSources.SelectedValue = "1A";
            }
            else if (minimalFields.Source == "IndigoCorp")
            {
                ddlAirSources.SelectedValue = "6ECORP";
            }
            else if (minimalFields.Source == "SpiceJetCorp")
            {
                ddlAirSources.SelectedValue = "SGCORP";
            }
            else if (minimalFields.Source == "Jazeera")
            {
                ddlAirSources.SelectedValue = "J9";
            }
            else if (minimalFields.Source == "GoAirCorp")
            {
                ddlAirSources.SelectedValue = "G8CORP";
            }
            else
            {
                ddlAirSources.SelectedValue = Convert.ToString(minimalFields.Source);
            }
            if (minimalFields.Status == "A")
            {
                chkStatus.Checked = true;
            }
            else
            {
                chkStatus.Checked = false;
            }
            if (!string.IsNullOrEmpty(minimalFields.Country))
            {
                rbtnCountry.Checked = true;
            }
            else if (!string.IsNullOrEmpty(minimalFields.Airline))
            {

                rbtnAirline.Checked = true;
            }
            else
            {
                rbtnCommon.Checked = true;
            }

            if (rbtnCountry.Checked)
            {

                foreach (ListItem item in chkCountries.Items)
                {


                    if (item.Value == minimalFields.Country)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            else if (rbtnAirline.Checked)
            {

                foreach (ListItem item in chkAirLines.Items)
                {
                    if (minimalFields.Source == "UAPI" || minimalFields.Source == "TBOAir" || minimalFields.Source == "PKFares" || minimalFields.Source == "Amadeus")
                    {
                        if (item.Value == minimalFields.Airline)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                    else
                    {
                        switch (item.Value)
                        {

                            case "G9":
                                chkG9.Checked = true;
                                break;
                            case "FZ":
                                chkFZ.Checked = true;
                                break;
                            case "SG":
                                chkSG.Checked = true;
                                break;
                            case "6E":
                                chk6E.Checked = true;
                                break;
                            case "IX":
                                chkIX.Checked = true;
                                break;
                            case "G8":
                                chkG8.Checked = true;
                                break;
                            case "J9":
                                chkJ9.Checked = true;
                                break;

                        }
                    }

                }
            }

            if (minimalFields.MandateSourcesFieldsList != null && minimalFields.MandateSourcesFieldsList.Count > 0)
            {

                foreach (AirlineMandateSourceFields field in minimalFields.MandateSourcesFieldsList)
                {
                    foreach (ListItem item in chkFields.Items)
                    {
                        if (item.Value == field.Field && field.Status == "A")
                        {
                            item.Selected = true;
                        }
                        ddlFlightType.Enabled = true;
                        ddlFlightType.SelectedValue = minimalFields.FlightType;
                        ddlTransType.Enabled = false;
                        ddlTransType.SelectedValue = minimalFields.TransType;
                    }
                }
            }



            btnCancel.Text = "Cancel";
            btnSave.Text = "Update";
            hdfMode.Value = "1";
            Utility.StartupScript(this.Page, "showSourceList();", "SCRIPT");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// DataTable which holds the SearchItems
    /// </summary>
    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[APIS_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["MandateId"] };
            Session[APIS_SEARCH_SESSION] = value;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "(APISSourceMaster page)btnSearch_Click method " + ex.Message, "0");
        }
    }

    private void bindSearch()
    {
        try
        {


            DataTable dt = AirlineMinimalFields.GetFlightAPISMandateSources(ProductType.Flight, "A");
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);

        }
        catch { throw; }
    }

    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string[,] textboxesNColumns ={{ "HTtxtSource", "Source" }
                                             ,{"HTtxtCountry", "Country" }
                                             ,{"HTtxtAirline", "Airline" }
                                         ,{"HTtxtStatus", "HTtxtStatus" }
                                         ,{"HTtxtFlightType", "FlightType" }

                                         };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }


}
