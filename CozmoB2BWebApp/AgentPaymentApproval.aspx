<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="AgentPaymentApprovalUI" Title="Agent Payment Approval" Codebehind="AgentPaymentApproval.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphtransaction" runat="Server">
    
    <asp:HiddenField runat="server" ID="hdfParam" Value="1"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfSelectCount" Value="0"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfDepSlipPath" />
    <asp:HiddenField runat="server" ID="hdfDepSlipType" />
    
    <div class="body_container">
        <%--Added by venkatesh 31-01-2018--%>
        <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
        <script type="text/javascript" src="yui/build/event/event-min.js"></script>
        <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
        <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
        <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
        <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
        <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
        <script type="text/javascript" src="js/Search.js"></script>
        <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
        <script src="yui/build/container/container-min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="ash.js"></script>
        <%--Added by venkatesh 01-02-2018--%>
        <script type="text/javascript">
            var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) /" + today.getDate() /" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            init();
              $('container2').context.styleSheets[0].display = "none";
              $('container1').context.styleSheets[0].display = "block";
              cal1.show();
              cal2.hide();
            document.getElementById('container1').style.display = "block";
             
        }


        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtFromDate.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            //    if (difference < 0) {
            //        document.getElementById('errMess').style.display = "block";
            //        document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
            //        return false;
            //    }
            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            //    if (difference < 1) {
            //        document.getElementById('errMess').style.display = "block";
            //        document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 )";
            //        return false;
            //    }
//            if (difference == 0) {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
//                return false;
//            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);
        </script>
        <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
        </div>
        <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
        </iframe>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; z-index:9999; top: 120px; left:8%; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute;  z-index:9999; top: 120px; left:40%; display: none;">
        </div>
    </div>
        <table cellpadding="0" cellspacing="0" class="label">
          <tr>
              <td style="width: 700px" align="left">
                  <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
                      onclick="return ShowHide('divParam');">Hide Parameter</a>
              </td>
          </tr>
      </table>
        <div class="paramcon" title="Param" id="divParam">
            <asp:Panel runat="server" ID="pnlParam" Visible="true">

                <div class="col-md-12 padding-0 marbot_10">

                    <div class="col-md-2">From Date:  </div>
                    <div class="col-md-2">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control" Width="120px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal1()">
                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">To Date: </div>
                    <div class="col-md-2">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtToDate" CssClass="form-control" runat="server" Width="120px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal2()">
                                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">Agent: </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlAgent" CssClass="form-control" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlAgents_SelectedIndexChanged">
                            <%--<asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>
                    </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        <asp:Label ID="lblB2BAgent" Text="B2BAgent:" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="inputDdlEnabled form-control"
                            OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:Label ID="lblB2B2BAgent" Text="B2B2BAgent:" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control" OnSelectedIndexChanged="ddlB2B2BAgent_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:Label ID="lblStatus" Text="Status:" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="inputDdlEnabled form-control" >
                            <asp:ListItem Text="--Select Status--" Value="-1" Selected="True" />
                            <asp:ListItem Text="Approved" Value="Y" />
                            <asp:ListItem Text="Not Approved" Value="N" />
                              <asp:ListItem Text="Rejected" Value="R" />
                        </asp:DropDownList>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2"> 
                         <asp:Label ID="lblPayMode" Text="Pay Mode:" runat="server"></asp:Label>
                     </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlPayMode"  runat="server" Enabled="true" CssClass="inputddlEnabled form-control">
                        </asp:DropDownList> 
                    </div>
                    <div class="col-md-2">
                        <asp:Label ID="lblTransType" Text="Transaction Type:" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlTransType"  runat="server" Enabled="true" CssClass="inputddlEnabled form-control" onchange="transType()">
                            <asp:ListItem Text="All" Value="-1" Selected="True" />
                            <asp:ListItem Text="Credit" Value="PAY" />
                            <asp:ListItem Text="Debit" Value="DEB"  />
                            <asp:ListItem Text="Transfer" Value="TRA"  />
                        </asp:DropDownList> 
                    </div>
                    <div id="divCredit" style="display:none">
                        <div class="col-md-2">
                            <asp:Label ID="lblCredit" Text="Credit:" runat="server"></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <asp:DropDownList ID="ddlCreditItems" CssClass="form-control" runat="server" >
                                <asp:ListItem Text="TOP UP" Value= "ADD" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="CR-TRANSFER" Value= "CR-TRA" ></asp:ListItem>
                                <asp:ListItem Text="CREDIT NOTE" Value= "CR-NOTE" ></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="divDebit" style="display:none">
                        <div class="col-md-2">
                            <asp:Label ID="lblDebit" Text="Debit:" runat="server"></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <asp:DropDownList ID="ddlDebitItems" CssClass="form-control" runat="server" >
                                <asp:ListItem Text="OFFLINE BOOKING OTHERS" Value= "DEB-OTH" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="OFFLINE BOOKING AIR" Value= "DEB-AIR" ></asp:ListItem>
                                <asp:ListItem Text="OFFLINE BOOKING HOTEL" Value= "DEB-HTL" ></asp:ListItem>
                                <asp:ListItem Text="CREDIT REVERSAL" Value= "DEB-REV" ></asp:ListItem>
                                <asp:ListItem Text="DEB-TRANSFER" Value= "DEB-TRA" ></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                 <%--<div class="col-md-2 col-xs-4"> <asp:Label ID="lblAgent" runat="server" Text="Agent:"></asp:Label></div>
    <div class="col-md-2 col-xs-8"> <asp:DropDownList ID="ddlAgent"  CssClass="inputddlEnabled form-control" runat="server"></asp:DropDownList></div>--%>

                <div class="col-md-7 col-xs-12 mar_xs_10">

                        <asp:Button runat="server" ID="btnSearch" Text="Search" OnClientClick="return Validate();" CssClass="btn but_b pull-left marright_10" OnClick="btnSearch_Click" />

                        <%--<asp:Button runat="server" ID="btnClear" Text="Clear" CssClass="btn but_b pull-left" OnClick="btnClear_Click" />--%>
                    </div>

            </asp:Panel>
        </div>

        <div class="grdScrlTrans" style="margin-top: -2px; height: 525px; border: solid 0px">

            <asp:UpdatePanel ID="uplGrid" runat="server" UpdateMode="conditional">
                <ContentTemplate>
                    <table id="tabSearch" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:GridView ID="gvAgentPayDetails" Width="100%" runat="server" AllowPaging="true" DataKeyNames="AP_ID"
                                    EmptyDataText="No Agent Payment List!" AutoGenerateColumns="false" PageSize="20" GridLines="none" CssClass="grdTable"
                                    OnSelectedIndexChanged="gvAgentPayDetails_SelectedIndexChanged" CellPadding="4" CellSpacing="0" PagerSettings-Mode="NumericFirstLast"
                                    OnPageIndexChanging="gvAgentPayDetails_PageIndexChanging">

                                    <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left"></HeaderStyle>
                                    <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                    <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                    <Columns>

                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="center" />

                                            <HeaderTemplate>
                                                <%--<label style="color:Black">Select</label>--%>
                                                <%--  <asp:CheckBox runat="server" id="HTchkSelectAll" AutoPostbACK="true" OnCheckedChanged="ITchkSelect_CheckedChanged"    ></asp:CheckBox>--%>
                                            </HeaderTemplate>
                                            <ItemStyle />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" CssClass="label" Checked='<%# Eval("ap_approval_status").ToString()!="N"%>' Enabled='<%# Eval("ap_approval_status").ToString()=="N" %>'></asp:CheckBox>

                                                <asp:HiddenField ID="IThdfAPId" runat="server" Value='<%# Bind("ap_id") %>'></asp:HiddenField>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <ItemStyle HorizontalAlign="left" />
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>' CssClass="label" ToolTip='<%# Container.DataItemIndex+1 %>' Width="20px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <HeaderStyle VerticalAlign="top" />
                                            <HeaderTemplate>
                                                <table>
                                                    <tr>
                                                       <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <uc1:DateControl ID="HTtxtReceiptDate" runat="server" DateOnly="true" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="HTbtnReceiptDate" runat="server" ImageUrl="~/Images/wg_filter.GIF" ImageAlign="AbsMiddle" OnClick="Filter_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <label style="width: 140px" class="filterHeaderText">&nbsp;Date</label>
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblReceiptDate" Width="130px" runat="server" Text='<%# IDDateTimeFormat(Eval("ap_receipt_date")) %>' CssClass="label grdof" ToolTip='<%# Eval("ap_receipt_date") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField>
                                            <ItemStyle HorizontalAlign="left" />
                                            <HeaderTemplate>
                                                <cc2:Filter ID="HTtxtDocNo" Width="150px" HeaderText="Receipt #" CssClass="inputEnabled" OnClick="Filter_Click" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblDocNo" runat="server" Text='<%# Eval("ap_reciept_no") %>' CssClass="label grdof" ToolTip='<%# Eval("ap_reciept_no") %>' Width="150px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle Width="100px" />
                                            <HeaderTemplate>
                                                <label style="width: 140px" class="filterHeaderText">Rcpt Print</label>

                                                <%--<cc2:Filter ID="HTtxtStatus" TextBoxWidth="60px" OnClick="Filter_Click" runat="server" FilterDataType=number />                 --%>
                                            </HeaderTemplate>
                                            <ItemStyle />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="ITlnkPrint" runat="Server" OnClick="ITlnkPrint_Click" Text="Rcpt Print" Width="70px"></asp:LinkButton>
                                                <%--<asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("receipt_status_name") %>' ToolTip='<%# Eval("receipt_status_name") %>' Width="60px"></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle />
                                            <HeaderTemplate>

                                                <cc2:Filter ID="HTtxtAmount" Width="70px" HeaderText="Amount" CssClass="inputEnabled" OnClick="Filter_Click" runat="server" />
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="right" />
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblAmount" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("ap_amount")) %>' ToolTip='<%# Eval("ap_amount") %>' Width="70px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%--Commented by venkatesh on 05-02-2018 said by Vinay--%>
                                        <%--<asp:TemplateField>
                                            <HeaderStyle />
                                            <HeaderTemplate>
                                                <asp:Label ID="HTtxtDownLoad" runat="server" Text="DownLoad" CssClass="label grdof"></asp:Label>
                                                
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="right" />
                                            <ItemTemplate>
                                                <asp:LinkButton Visible="true" ID="IThlnkSlipDownload" OnClientClick='<%# "return download(\""+ DataBinder.Eval(Container.DataItem,"ap_deposit_slip_path") + "\",\""+DataBinder.Eval(Container.DataItem,"ap_deposit_slip_type")+ "\");" %>' ForeColor="RoyalBlue" Font-Bold="true" runat="server" Text="Download" Width="70px"></asp:LinkButton>
                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <cc2:Filter ID="HTtxtPaymentMode" HeaderText="Payment Mode" CssClass="inputEnabled" Width="100px" OnClick="Filter_Click" runat="server" />
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblPaymentMode" runat="server" Text='<%# Eval("ap_payment_mode_name") %>' CssClass="label grdof" ToolTip='<%# Eval("ap_payment_mode_name") %>' Width="80px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <cc2:Filter ID="HTtxtChequeNo" HeaderText="Cheque #" CssClass="inputEnabled" Width="70px" OnClick="Filter_Click" runat="server" />
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblDepAmt" runat="server" Text='<%# Eval("ap_cheque_no") %>' CssClass="label grdof" ToolTip='<%# Eval("ap_cheque_no") %>' Width="70px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField><HeaderTemplate>
    <cc2:Filter ID="HTtxtChequeDate" HeaderText="Cheque Date" CssClass="inputEnabled" Width="120px" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblChequeDate" runat="server" Text='<%# Eval("ap_cheque_date") %>' CssClass="label grdof"  ToolTip='<%# Eval("ap_cheque_date") %>' Width="125px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>--%>

                                        <asp:TemplateField>
                                            <HeaderStyle VerticalAlign="top" />
                                            <HeaderTemplate>
                                                <table>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <uc1:DateControl ID="HTtxtChequeDate" runat="server" DateOnly="true" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="HTbtnChequeDate" runat="server" ImageUrl="~/Images/wg_filter.GIF" ImageAlign="AbsMiddle" OnClick="Filter_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <label style="width: 140px" class="filterHeaderText">Cheque Date</label>
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblChequeDate" Width="130px" runat="server" Text='<%# IDDateTimeFormat(Eval("ap_cheque_date")) %>' CssClass="label grdof" ToolTip='<%# Eval("ap_cheque_date") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>



                                        <asp:TemplateField>
                                            <HeaderStyle />
                                            <HeaderTemplate>
                                                <cc2:Filter ID="HTtxtBankName" HeaderText="Bank" Width="120px" CssClass="inputEnabled" OnClick="Filter_Click" runat="server" />
                                            </HeaderTemplate>
                                            <ItemStyle />
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblBankName" runat="server" Text='<%# Eval("ap_bank_name") %>' CssClass="label grdof" ToolTip='<%# Eval("ap_bank_name") %>' Width="120px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <HeaderStyle />
                                            <HeaderTemplate>
                                                <cc2:Filter ID="HTtxtBankBranch" HeaderText="Branch" CssClass="inputEnabled" Width="80px" OnClick="Filter_Click" runat="server" />
                                            </HeaderTemplate>
                                            <ItemStyle />
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblBankBranch" runat="server" Text='<%# Eval("ap_bank_branch") %>' CssClass="label grdof" ToolTip='<%# Eval("ap_bank_branch") %>' Width="80px"></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle />
                                            <HeaderTemplate>
                                                <cc2:Filter ID="HTtxtBankac" HeaderText="Account" CssClass="inputEnabled" Width="80px" OnClick="Filter_Click" runat="server" />
                                            </HeaderTemplate>
                                            <ItemStyle />
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblBankac" runat="server" Text='<%# Eval("ap_bank_account") %>' CssClass="label grdof" ToolTip='<%# Eval("ap_bank_account") %>' Width="80px"></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>



                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="center" />
                                            <HeaderTemplate>

                                                <cc2:Filter ID="HTtxtRemarks" HeaderText="Remarks" CssClass="inputEnabled" Width="150px" OnClick="Filter_Click" runat="server" FilterDataType="number" />
                                            </HeaderTemplate>
                                            <ItemStyle />
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblRemarks" runat="server" Text='<%# Eval("ap_remarks") %>' CssClass="label grdof" ToolTip='<%# Eval("ap_remarks") %>' Width="150px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="center" />
                                            <HeaderTemplate>

                                                <cc2:Filter ID="HTtxtStatus" HeaderText="Status" CssClass="inputEnabled" Width="150px" OnClick="Filter_Click" runat="server" />
                                            </HeaderTemplate>
                                            <ItemStyle />
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("approval_status") %>' CssClass="label grdof" ToolTip='<%# Eval("approval_status") %>' Width="150px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="center" />
                                            <HeaderTemplate>

                                                <cc2:Filter ID="HTtxtTransType" HeaderText="TransType" CssClass="inputEnabled" Width="150px" OnClick="Filter_Click" runat="server" />
                                            </HeaderTemplate>
                                            <ItemStyle />
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblTransType" runat="server" Text='<%# Eval("AP_TRANX_TYPE") %>' CssClass="label grdof" ToolTip='<%# Eval("AP_TRANX_TYPE") %>' Width="150px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="center" />
                                            <HeaderTemplate>

                                                <cc2:Filter ID="HTtxtTransProvision" HeaderText="TransProvision" CssClass="inputEnabled" Width="150px" OnClick="Filter_Click" runat="server" />
                                            </HeaderTemplate>
                                            <ItemStyle />
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblTransProvision" runat="server" Text='<%# Eval("ap_tranx_provision") %>' CssClass="label grdof" ToolTip='<%# Eval("ap_tranx_provision") %>' Width="150px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table>
                                    <tr>
                                        <td width="80px">
                                            <asp:Button OnClick="btnApproval_Click" runat="server" ID="btnApproval" Text="Approve" CssClass="button" />
                                        </td>

                                        <td width="80"><asp:Button runat="server" ID="btnReject" Text="Reject" CssClass="btn-danger btn-lg" OnClick="btnReject_Click"/></td>
                                        <td width="80px">
                                            <asp:Button OnClick="btnExport_Click" runat="server" ID="btnExport" Text="Export To Excel" CssClass="button"  />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <%--<asp:Label Cssclass="grandTotal" id="lblGrandText" runat="server" text="Grand Total Of Total Fare:">
    </asp:Label><asp:Label Cssclass="grandTotal" id="lblGrandTotal" runat="server" text=""></asp:Label>--%>

        <%--  </div>--%>



        <div class="clearfix"></div>

    </div><div>
    <asp:DataGrid ID="dgAgentPaymentApprovalList" runat="server" AutoGenerateColumns="false" >
    <Columns>
        <asp:BoundColumn HeaderText="Date" DataField="ap_receipt_date" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Receipt" DataField="ap_reciept_no" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Amount" DataField="ap_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Payment Mode" DataField="ap_payment_mode" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Cheque" DataField="ap_cheque_no" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Cheque Date" DataField="ap_cheque_date" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Bank" DataField="ap_bank_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Branch" DataField="ap_bank_branch" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Account" DataField="ap_bank_account" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Remarks" DataField="ap_remarks" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Status" DataField="approval_status" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Transaction Type" DataField="AP_TRANX_TYPE" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Transaction Provision" DataField="ap_tranx_provision" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    </Columns>
        </asp:DataGrid>
        </div>


    <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"></asp:Label>
    <asp:Label Style="color: #dd1f10" ID="lblError" runat="server"></asp:Label>

    <script type="text/javascript">

        function Validate() {
            if (document.getElementById('ctl00_cphTransaction_txtFromDate').value == "" || document.getElementById('ctl00_cphTransaction_txtFromDate').value == null) addMessage('Please select FromDate');
            if (document.getElementById('ctl00_cphTransaction_txtToDate').value == "" || document.getElementById('ctl00_cphTransaction_txtToDate').value == null) addMessage('Please select ToDate');
            if (getElement('ddlAgent').selectedIndex <= '') addMessage('Please select Agent from the list!', '');


            if (getMessage() != '') {
                //alert(getMessage()); 
                alert(getMessage()); clearMessage(); return false;
            }


        }

        function download(path, type) {
            //    var path =document.getElementById('hdfDocPath').value;
            //    var type =document.getElementById('hdfDocType').value;
            //alert('path:'+path+', type:'+type);
            var open = window.open('DownloadVisa.aspx?path=' + path + '&extension=' + type);
            return false;
        }

        //Added by venkatesh on 31-01-2018
        
        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }
        function transType()
        {
            document.getElementById('divCredit').style.display = "none";
            document.getElementById('divDebit').style.display = "none";
            if (document.getElementById('<%=ddlTransType.ClientID %>').value == "PAY") {
                document.getElementById('divCredit').style.display = "block";
            }
            else if(document.getElementById('<%=ddlTransType.ClientID %>').value == "DEB"){
                document.getElementById('divDebit').style.display = "block";
            }
            
        }
    </script>
</asp:Content>
