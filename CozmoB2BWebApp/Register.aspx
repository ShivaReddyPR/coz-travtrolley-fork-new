﻿<%@ Page Language="C#" MasterPageFile="~/Transaction.master" AutoEventWireup="true" Inherits="Register" Title="Register" Codebehind="Register.aspx.cs" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="System.Security.AccessControl" %>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

    <link href="css/toastr.css" rel="stylesheet" type="text/css" />
       
    <script src="css/toastr.min.js"></script>

 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Register</title>
    <link href="App_Themes/Ibyta/Default.css" rel="stylesheet" type="text/css" />
     <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>
    <script src="scripts/Captcha.js" type="text/javascript"></script>

    <style type="text/css">
        body{
            background-color: #ec0b43;
        }
.required:after 
{
    content: "*";
    font-weight: bold;
    color: red; 
}

.container.header_top{
    display:none !important;
}
.ibyta--top-header {
    max-width: 170px;
    margin: 20px auto 0 auto;
}
</style>
 <script type="text/javascript">
     var widgetId1;     
    
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function checkEmail(inputvalue) {
            var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
            if (pattern.test(inputvalue)) {
                return true;
            }
            else {
                return false;
            }
        }
        
     function Save() {
         var valid = false;
         var ValidationMessage = "";
         //var CheckTerms = false;
         var Agency = document.getElementById('<%= txtAgency.ClientID %>').value
         var Address = document.getElementById('<%= txtAddress.ClientID %>').value
         var PhoneNo = document.getElementById('<%= txtTelephone.ClientID %>').value;
         var Email = document.getElementById('<%= txtEmail.ClientID %>').value
         var ValidEmail = checkEmail(document.getElementById('<%= txtEmail.ClientID %>').value)
         var Country = document.getElementById('<%=ddlCountry.ClientID%>').value;
         var Currency = document.getElementById('<%=ddlCurrency.ClientID%>').value;
         var vatfiles = $('#vatContainer .dz-filename');
         var tradefiles = $('#tradeContainer .dz-filename');
         //var CheckTerms = document.getElementById('chkAgrement').checked;

         if (Agency.length == 0) {
             ValidationMessage += " Agency Cannot be blank  \n";
         }
         if (Address.length == 0) {
             ValidationMessage += " Address Cannot be blank \n";
         }
         if (PhoneNo.length == 0) {
             ValidationMessage += " Phone No. Cannot be Blank \n";
         }
         if (PhoneNo.length < 10) {
             ValidationMessage += " Please enter valid Phone number \n";
         }
         if (Email.length == 0) {
             ValidationMessage += " Please Enter Email  \n";
         }
         if (Email.length > 0) {
             if (!ValidEmail) {
                 ValidationMessage += " Please Enter Valid E-mail  \n";
             }
         }
         if (Country == -1) { ValidationMessage += " Please select country from the list! \n"; }
         if (Currency == -1) { ValidationMessage += " Please select currency from the list! \n"; }
         if (vatfiles.length < 1) { ValidationMessage += " Please upload The vat Receipts! \n"; }
         if (tradefiles.length < 1) { ValidationMessage += " Please upload The trade Receipts! \n"; }
         if (Agency.length == 0 || PhoneNo.length == 0 || Email == 0 || Address.length == 0 || (PhoneNo.length < 10) || Country == -1 || vatfiles.length == 0 || tradefiles.length == 0) {
             alert(ValidationMessage);
             return valid;
         }
         else {
             var RegUploadedFiles = [];
             var vatdzUploadedfiles = $('#vatContainer .dz-filename');
             var tradedzUploadedfiles = $('#tradeContainer .dz-filename');

             $.each(vatdzUploadedfiles, function (key, col) {

                 RegUploadedFiles.push({ doc_Id:0, doc_code: 'VAT', doc_name: col.innerText, doc_path: fileUploadPath, doc_type: col.innerText.split('.').pop() });
             });

             $.each(tradedzUploadedfiles, function (key, col) {

                 RegUploadedFiles.push({ doc_Id:0, doc_code: 'TLC', doc_name: col.innerText, doc_path: fileUploadPath, doc_type: col.innerText.split('.').pop() });
            });
             
             $('#ctl00_cphTransaction_hdnfileUploads').val(JSON.stringify(RegUploadedFiles));

             var value = $('#ctl00_cphTransaction_hdnfileUploads').val(); //retrieve array
                value = JSON.parse(value);
             
             var resp = grecaptcha.getResponse(widgetId1);
             if (resp != "" && resp.length > 0) {
                 $('#ctl00_cphTransaction_hdnreCaptchaResponse').val(resp);
                 grecaptcha.reset(widgetId1);
                 return true;
             }
             else {
                 alert('Please verify that you are not robot by checking the checkbox!');                 
                 return false;
             }
         }
     }
    </script>
        <!--Dropzone-->
    <link href="DropzoneJs_scripts/dropzone.css" rel="stylesheet" />
    <script src="DropzoneJs_scripts/dropzone.js"></script> 

    <script type="text/javascript">

        var dropZonePIR = null;
        var dropZonePIRObj;
        var files = [];
        var TLCdropZonePIR = null;
        var TLCdropZonePIRObj;

        function InitVATDropZone() {

            $(".closepnldocs").on('click', function () {

                $(".pnldocs").hide();
                $(".pnldrop").hide();

                if (document.getElementById('lnkRemove') != null) 
                    document.getElementById('lnkRemove').click();
                
            });

            // Dropzone.autoDiscover = false;
            Dropzone.prototype.defaultOptions.acceptedFiles = ".JPG,.JPEG,.PNG,.PDF,.DOC,.DOCX,.XLS,.XLSX";
            Dropzone.prototype.defaultOptions.maxFiles = "1";
            var filesize = 1024 * 5000;
            Dropzone.maxFilesize = 5;

            
            // for Receipt files Upload          
            dropZonePIRObj = {

                url: "hn_RegisterFileUploader.ashx",
                maxFiles: 4,
                addRemoveLinks: true,
                previewsContainer: '.dz-custom-preview.vat',
                thumbnailHeight: 120,
                thumbnailWidth: 120,
                init: function () {

                    this.on("addedfile", function (file) {

                        if (this.files.length) {

                            var _i, _len;

                            //Reoving the File when exceed the File Limit.
                            if (file.size > filesize) {

                                this.removeFile(file);

                                if (!files.includes(file.name))
                                    files.push(file.name);

                                var msg = '';

                                for (var i = 0; i < files.length; i++)
                                    msg += files[i] + (i != files.length - 1 ? ',' : '');

                                $('#errorfiles').html("<b>" + msg + "</b> Failed to Upload the files exceeded file Limits.");
                            }

                            //Removing the duplicate file having same size ,name and modification date.
                            for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) // -1 to exclude current file
                            {
                                if (this.files[_i].name === file.name && this.files[_i].size === file.size && this.files[_i].lastModifiedDate.toString() === file.lastModifiedDate.toString()) {
                                    this.removeFile(file);
                                }
                            }
                        }
                    });
                },
                success: function (file, response) {

                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                }
            }

            TLCdropZonePIRObj = {

                url: "hn_RegisterFileUploader.ashx",
                maxFiles: 4,
                addRemoveLinks: true,
                previewsContainer: '.dz-custom-preview.trade',
                thumbnailHeight: 120,
                thumbnailWidth: 120,
                init: function () {

                    this.on("addedfile", function (file) {

                        if (this.files.length) {

                            var _i, _len;

                            //Reoving the File when exceed the File Limit.
                            if (file.size > filesize) {

                                this.removeFile(file);

                                if (!files.includes(file.name))
                                    files.push(file.name);

                                var msg = '';

                                for (var i = 0; i < files.length; i++)
                                    msg += files[i] + (i != files.length - 1 ? ',' : '');

                                $('#errorfiles').html("<b>" + msg + "</b> Failed to Upload the files exceeded file Limits.");
                            }

                            //Removing the duplicate file having same size ,name and modification date.
                            for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) // -1 to exclude current file
                            {
                                if (this.files[_i].name === file.name && this.files[_i].size === file.size && this.files[_i].lastModifiedDate.toString() === file.lastModifiedDate.toString()) {
                                    this.removeFile(file);
                                }
                            }
                        }
                    });
                },
                success: function (file, response) {

                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                }
            }

            $('.dropzone').each(function () {
                let dropzoneControl = $(this)[0].dropzone;
                if (dropzoneControl) {
                    dropzoneControl.destroy();
                }
            });

            dropZonePIR = new Dropzone('#dropzoneVatDoc', dropZonePIRObj);
            TLCdropZonePIR = new Dropzone('#dropzoneTradeDoc', TLCdropZonePIRObj);
            
            $('.dz-message').append('<div class="use-camera bg-primary"  onclick="showCamera()">USE CAMERA<i class="icon-camera pl-2"></i></div><div class="selectfromGallery "><i class="icon icon-image pr-2 "></i>Select from Gallery</div>');
        }


    </script>

    <script type="text/javascript">

        /* Global variables */
        var fileUploadPath = '<%=ConfigurationManager.AppSettings["RegisterReceiptsFilePath"]%>';
        
        /* Page Load */
        $(document).ready(function () {

            var pathError = '';

            if (IsEmpty(fileUploadPath)) 
                pathError = 'Please configure path to upload files.';

            
            if (IsEmpty(pathError) && CompFields('<%= Directory.Exists(ConfigurationManager.AppSettings["RegisterReceiptsFilePath"])%>', 'False'))
                pathError = 'File upload path not found to upload files.';

            if (IsEmpty(pathError) && CompFields('<%= GenericStatic.DirectoryHasPermission(ConfigurationManager.AppSettings["RegisterReceiptsFilePath"], FileSystemRights.Write)%>', 'False'))
                pathError = 'Access denied to path to upload files.';

            if (!IsEmpty(pathError)) {

                ShowError(pathError);
                $('#divReceiptDetails').hide();
                $('#btnSave').hide();
                $('#btnClear').hide();
                return false;
            }

            /* Initialize VAT drop zone control */
            InitVATDropZone();

            $("#ctl00_upProgress").hide();
        });

        /* Reloading the Page */
        function pageReload(){

            window.location.href = 'Register.aspx';
        }
       
    </script>

      <div class="ibyta--top-header">
			<div class="container">
				<div class="row">
					<div class="col-xs-6">
						<img src="https://travtrolley.com/images/ibyta-logo.jpg" class="img-responsive" style="max-width: 130px;">
					</div>
				</div>
			</div>
		</div>  
 <div class="register-container">


    
   
     <asp:HiddenField ID="hdnreCaptchaResponse" runat="server" />
     <asp:HiddenField ID="hdnfileUploads" runat="server" />
     

   
         <h4 class="text-center py-4">
            Register
         </h4>
         <hr class="mt-0">
      
      <div class="row px-5">
          <div class="col-12 col-md-6">
              <div class="form-group">
                <label>
                    Name of Agency:<span class='required'> </span>
                </label>        
                <asp:TextBox ID="txtAgency" runat="server" class=" form-control"> </asp:TextBox>
            </div>
         </div>
          <div class="col-12 col-md-6">              
              <div class="form-group">
                <label>Post Box No:</label>
                <asp:TextBox ID="txtPostBoxNo" runat="server" onkeypress = "return isNumber(event);"    class=" form-control"></asp:TextBox>
         </div>
              </div>
          <div class="col-12 col-md-6">              
              <div class="form-group">
                <label> Nature of Business:
                </label>
                   <asp:TextBox ID="txtNatureOfBussiness" runat="server"   class=" form-control"></asp:TextBox>
         </div>
          </div>

          <div class="col-12 col-md-6">              
              <div class="form-group">
                <label> Name of Owners:
                </label>
                   <asp:TextBox ID="txtNameOfOwners" runat="server"  class=" form-control"></asp:TextBox>
         </div></div>
          <div class="col-12 col-md-6">              
              <div class="form-group">
                <label> Phone Number:<span class='required'></span>
                </label>
                   <asp:TextBox ID="txtTelephone" runat="server"  onkeypress = "return isNumber(event);" class=" form-control"></asp:TextBox>
         </div></div>
          <div class="col-12 col-md-6">              
              <div class="form-group">
                <label>  Email:<span class='required'></span>
                </label>
                   <asp:TextBox ID="txtEmail" runat="server" class=" form-control"></asp:TextBox>
         </div></div>
          <div class="col-12 col-md-6">              
              <div class="form-group">
                <label> Website:
                </label>
                   <asp:TextBox ID="txtWebsite" runat="server" class=" form-control"></asp:TextBox>
         </div></div>
          <div class="col-12 col-md-6">              
              <div class="form-group">
                <label> Trade License No:
                </label>
                   <asp:TextBox ID="txtTradeLicenseNo" runat="server"   class=" form-control"></asp:TextBox>
         </div></div>

          <div class="col-12 col-md-6">              
              <div class="form-group">
                <label> Exp. Date:
                </label>
                  <uc1:DateControl ID="dcLicExpDate" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY"
               DateOnly="true" DropDownYears="10" Enabled="true" HorizontalAlignment="Left"
               OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down"
               WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday">
            </uc1:DateControl>
          </div>
          </div>
          
        
          <div class="col-12 col-md-6">              
              <div class="form-group">
                <label>
                     Country :<span class='required'></span>
                </label>
                   <asp:DropDownList runat="server" ID="ddlCountry" CssClass="inputDdlEnabled form-control"></asp:DropDownList>
         </div></div>
           <div class="col-12 col-md-6">
               <div class="form-group">
                    <label>Currecy:<span class='required'></span></label>
                    <asp:DropDownList runat="server" ID="ddlCurrency" CssClass="inputDdlEnabled form-control"></asp:DropDownList>
             </div>
              
          </div>
          
            <div class="col-12 col-md-6">              
              <div class="form-group">
                    <label>Address:<span class='required'></span></label>
                    <asp:TextBox ID="txtAddress"  Rows="4" Columns="40" class=" form-control" TextMode="MultiLine"   runat="server"></asp:TextBox>
             </div>
          </div>
    
          <div class="col-12 col-md-6">              
              <div class="form-group">
                    <label> Upload Documents<span class='required'></span></label>
                    <div class="clearfix"></div>
                    <button type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#ModalUploadDocuments">
                        Upload
                    </button>
                 
             </div>
          </div>
          <div class="col-12 col-md-6">              
              <div id="captchaDiv"></div>
          </div>
         <div class="col-12 mt-2 px-4">
            
            <div class="d-flex justify-content-center my-3">
               <asp:Button ID="btnRegister" CssClass="btn btn-primary mr-2" runat="server" Text="Register" OnClientClick="return Save();"
                  OnClick="btnRegister_Click" />
               <asp:Button ID="btnClear" CssClass="btn btn-secondary" runat="server" Text="Cancel" OnClick="btnClear_Click" />
               <div class="clearfix">
               </div>
            </div>
            <asp:Label ID="lblSuccessMsg" runat="server" Text=""></asp:Label>
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            <div class="clearfix">
            </div>
         </div>
        <span id="errorfiles" style="color: red" />
   </div>

   
 </div>


<!-- Open ModalUploadDocuments -->
<div class="modal fade" id="ModalUploadDocuments" tabindex="-1" aria-labelledby="ModalUploadDocumentsLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalUploadDocumentsLabel">Upload Documents</h5>
        <button type="button" class="close position-relative" data-dismiss="modal" aria-label="Close" style="top: -20px;
    right: -8px;font-size: 20px;">
         <span aria-hidden="true" class="icon icon-close"></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-12 col-md-6">              
              <div class="form-group">
                    <label> Upload VAT Docs<span class='required'></span></label>
                    <div class="dropzone-file-upload-wrap minimal flex-column">
                        <div action="/file-upload" class="dropzone dropzone-custom" id="dropzoneVatDoc"></div>
                        <div class="dz-custom-preview dropzone-previews vat" id="vatContainer">
                        </div>
                    </div>
                 
             </div>

          </div>          
          <div class="col-12 col-md-6">              
              <div class="form-group">
                    <label>  Upload Trade License<span class='required'></span></label>
                    <div class="dropzone-file-upload-wrap minimal flex-column">
                        <div action="/file-upload" class="dropzone dropzone-custom" id="dropzoneTradeDoc"></div>
                        <div class="dz-custom-preview dropzone-previews trade" id="tradeContainer">

                        </div>
                    </div>                 
             </div>

          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <%--<button type="button" class="btn btn-primary">Save changes</button>--%>
      </div>
    </div>
  </div>
</div>
</asp:Content>

