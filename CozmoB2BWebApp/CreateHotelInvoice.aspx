<%@ Page Language="C#" AutoEventWireup="true" Inherits="CreateHotelInvoiceGUI" Codebehind="CreateHotelInvoice.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>

<html xmlns="https://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Hotel Invoice </title>
        <%-- To enable processing style while sending email --%>          
        <link href="css/override.css" rel="stylesheet"/>
        <script type="text/javascript">
        
            /* To print the page */
            function printPage() {

                document.getElementById('btnEmail').style.display = document.getElementById('btnPrint').style.display = "none";
                window.print();
                setTimeout('showButtons()', 1000);
            }
            
            /* To hide email and print buttons */
            function showButtons() {

                document.getElementById('btnEmail').style.display = document.getElementById('btnPrint').style.display = "block";
            }
            
            /* To validate email */  
            function HideEmailDiv() {

                document.getElementById('emailBlock').style.display = 'none';
            }     

            /* To validate email */   
            function Validate()
            {
                ErrorMsg('');

                var sEmail = document.getElementById('txtEmail').value;

                if (sEmail == '')        
                {
                    ErrorMsg('Enter Email address');
                    return false;
                }

                if (!ValidateEmail('txtEmail', 'lblError'))        
                    return false;

                document.getElementById('btnEmail').style.display = document.getElementById('btnPrint').style.display =
                    document.getElementById('emailBlock').style.display = "none";
                document.getElementById('divProcess').style.display = "block";
                return true;
            }

            /* To open email pop up */
            function ShowPopUp() {

                document.getElementById('txtEmail').value = "";
                ErrorMsg('');
                document.getElementById('emailBlock').style.display = "block";
                return false;
            }

            /* To prepare and show the alert pop up box */
            function Showalert(message) {

                alert(message);
                document.getElementById('divProcess').style.display = "none";
            }

            /* To validate email and display error message */
            function ValidateEmail(id, errorid) {

                var sEmail = document.getElementById(id).value;
                if (sEmail != '') {

                    var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
                
                    if (pattern.test(sEmail)) 
                        ErrorMsg('');
                    else {

                        ErrorMsg('Invalid email format');
                        return false;
                    }
                }
                else
                    ErrorMsg('');
                return true;
            }

            /* To display and clear error message */
            function ErrorMsg(val) {
                                
                document.getElementById('lblError').innerHTML = val;
                document.getElementById('lblError').style.display = val == '' ? 'none' : 'block';            
            }
        </script>
    </head>
    <body>
        <form id="form1" runat="server">
            <input type="hidden" id="invoiceNo" value="<%=invoice.InvoiceNumber%>" /> 
            <div id="divProcess" style="display:none" class="lds-loader-wrap"><div class="lds-loader-inner"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div></div>
            <div style="position:absolute; right:700px; top:485px; display:none"  id="emailBlock">
                <div style="border: solid 4px #ccc;width:400px;height: 130px;"> 
                    <div style="font-size:16px;font-family:Arial;background-color:#60c231;padding: 5px 0px 5px 0px;color:#fff;font-weight: bold;"> 
                        Enter Email Address <a style="float:right;padding-right:10px;color: #fff;" href="javascript:HideEmailDiv()">X</a>
                    </div>                    
                    <div style=" padding:10px">
                        <asp:TextBox style="width:100%;border: solid 1px #7F9DB9;height: 20px;" ID="txtEmail" runat="server" onchange="ValidateEmail(this.id, 'lblError')"></asp:TextBox>
                        <label id="lblError" style="display:none;color:red;font-family:Arial;"></label>
                    </div>
                    <div style=" padding-left:10px; padding-bottom:10px">
                        <asp:Button ID="btnEmailInvoice" runat="server" Text="Send Email" OnClientClick="return Validate()" OnClick="btnEmailInvoice_Click"
                            style="font-size: 13px;font-family:Arial;background-color: #60c231;padding: 5px 5px 5px 5px;color: #fff;font-weight: bold;"  />
                    </div>          
                </div>
            </div>
            <div id="divInvoice" runat="server" style=" padding:10px;">
                <table id="tblInvoice"  width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                        <td> 
                            <div> <center> <asp:Image ID="imgLogo"  runat="server" AlternateText="AgentLogo" ImageUrl="" style="max-width: 159px;max-height: 51px;" /> </center> </div>
                            <div> 
                                <table width="100%" style=" font-family:Arial;"> 
                                    <tr> 
                                        <td> 
                                            <center>  
                                                <h1 style=" font-size:26px">Invoice</h1>                                                
                                                <%if (location.CountryCode == "IN") { %>  GSTIN:100019554300003 <%} else if(agency.AgentParantId>0){%> TRN: <% = parentAgency.Telex %><%} else {%> TRN: <% = agency.Telex %><%}%>                                                
                                            </center>
                                            <input runat="server" style=" float:right; margin-top:-26px;" id="btnEmail" onclick="return ShowPopUp();" type="button" value="Email Invoice" />
                                            <input runat="server" style=" float:right; margin-top:-26px;margin-right: 150px;" id="btnPrint" onclick="return printPage();" type="button" value="Print Invoice" />
                                        </td>  
                                    </tr>  
                                </table>  
                            </div>  
                            <table width="100%"> 
                                <tr> 
                                    <td width="50%" valign="top"> 
                                        <h3 style="font-size:16px; font-family:Arial; background-color:#60c231; padding: 7px 10px 7px 10px; color:#fff;">Client Details</h3>
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;"> 
                                            <tbody>                                        
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Agency Name: </b></td>
                                                    <td style="vertical-align: top;"> <% = agency.Name %></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Agency Code: </b></td>
                                                    <td style="vertical-align: top;"> <% = agency.Code %></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Agency Address: </b></td>
                                                    <td style="vertical-align: top;"><% = agencyAddress %></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>TelePhone No: </b></td>
                                                    <td style="vertical-align: top;">
                                                            <%  if (agency.Phone1 != null && agency.Phone1.Length != 0)
                                                                {%>
                                                            Phone:
                                                            <% = agency.Phone1%>
                                                            <%  }
                                                                else if (agency.Phone2 != null && agency.Phone2.Length != 0)
                                                                { %>
                                                            Phone:
                                                            <% = agency.Phone2%>
                                                            <%  } %>
                                                            <%  if (agency.Fax != null && agency.Fax.Length > 0)
                                                                { %>
                                                            Fax:
                                                            <% = agency.Fax %>
                                                            <%  } %>                                                
                                                    </td>
                                                </tr>
                                                <%if (location.CountryCode == "IN") { %>
                                                <tr>
                                                    <td style="vertical-align: middle; width: 120px;"><b>GST Number: </b></td>
                                                    <td style="vertical-align: middle;"><% = location.GstNumber %></td>
                                                </tr>
                                                <%}
                                                else if (agency.Telex != null && agency.Telex.Length > 0) {%>
                                                <tr>
                                                    <td style="vertical-align: middle; width: 120px;"><b>TRN Number: </b></td>
                                                    <td style="vertical-align: middle;"><% = agency.Telex %></td>
                                                </tr>
                                                <%} %>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="50%" valign="top"> 
                                        <h3 style="font-size:16px; font-family:Arial; background-color:#60c231; padding: 7px 10px 7px 10px; color:#fff;">Invoice Details</h3>
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;"> 
                                            <tbody>                                        
                                                <tr>
                                                    <td style="vertical-align: top; width: 120px;"><b>Invoice No: </b></td>
                                                    <td style="vertical-align: top;"><%=invoice.CompleteInvoiceNumber%></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Invoice Date: </b></td>
                                                    <td style="vertical-align: top;"><% = invoice.CreatedOn.ToString("dd MMMM yyyy") %></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Confirmation No: </b></td>
                                                    <td style="vertical-align: top;"><% = confNo %></td>
                                                </tr>
                                                <tr>
                                                     <%  
                                                         int bookingID = 0;
                                                         bookingID = BookingDetail.GetBookingIdByProductId(hotelId, ProductType.Hotel); 
                                                    %>
                                                    <td style="vertical-align: top;width: 240px;"><b><%= agency.AgentParantId>0?parentAgency.Name:agency.Name%> Vocher No: </b></td>
                                                    <td style="vertical-align: top;">HTL-CT-<%=bookingID%></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr> 
                                    <td width="50%" valign="top"> 
                                        <h3 style="font-size:16px; font-family:Arial; background-color:#60c231; padding: 7px 10px 7px 10px; color:#fff;"> Booking Details</h3>
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;"> 
                                            <tbody>                                
                                                <tr>
                                                    <td style="vertical-align: top;"><b>Booking Date: </b></td>
                                                    <td style="vertical-align: top;"> <%=itinerary.CreatedOn.ToString("dd MMM yy") %></td>
                                                </tr>                             
                                                <tr>
                                                    <td style="vertical-align: top;"><b>Check In: </b></td>
                                                    <td style="vertical-align: top;"><%=itinerary.StartDate.ToString("dd MMM yy") %></td>                                 
                                                    <td style="vertical-align: top;"><b>Check Out: </b></td>
                                                    <td style="vertical-align: top;"><%=itinerary.EndDate.ToString("dd MMM yy") %></td>
                                                </tr>                                
                                                <tr>
                                                    <%  
                                                                    System.TimeSpan diffResult = itinerary.EndDate.Subtract(itinerary.StartDate);
                                                            %>
                                                    <td style="vertical-align: top;"><b>No. of Night(s): </b></td>
                                                    <td style="vertical-align: top;"><%= diffResult.Days%></td>
                                                    <td style="vertical-align: top;"><b>No. of Room(s): </b></td>
                                                    <td style="vertical-align: top;"><% = itinerary.NoOfRooms%></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="50%" valign="top"> 
                                        <h3 style="font-size:16px; font-family:Arial; background-color:#60c231; padding: 7px 10px 7px 10px; color:#fff;"> Hotel Details </h3>
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;"> 
                                            <tbody>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Hotel Name: </b></td>
                                                    <td style="vertical-align: top;"> <%=itinerary.HotelName%></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Address1: </b></td>
                                                    <td style="vertical-align: top;"> <%=itinerary.HotelAddress1%></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>City: </b></td>
                                                    <td style="vertical-align: top;">  <%=itinerary.CityRef %></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr> 
                                    <td width="50%" valign="top"> 
                                        <h3 style="font-size:16px; font-family:Arial; background-color:#60c231; padding: 7px 10px 7px 10px; color:#fff;"> Lead Passenger Details </h3>
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;"> 
                                            <tbody>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Pax Name: </b></td>
                                                    <td style="vertical-align: top;"> <% = itinerary.HotelPassenger.Firstname %> <%= itinerary.HotelPassenger.Lastname %></td>
                                                </tr>
                                                <%if(agency.AddnlPaxDetails=="Y" ) {%>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Department: </b></td>
                                                    <td style="vertical-align: top;"> <% = itinerary.HotelPassenger.Department%></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Division: </b></td>
                                                    <td style="vertical-align: top;"> <% = itinerary.HotelPassenger.Division%></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Employee ID: </b></td>
                                                    <td style="vertical-align: top;"> <% = itinerary.HotelPassenger.EmployeeID%></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Employee Name: </b></td>
                                                    <td style="vertical-align: top;"> <% = itinerary.HotelPassenger.Employee%></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Purpose of Travel: </b></td>
                                                    <td style="vertical-align: top;"> <% = itinerary.HotelPassenger.Purpose%></td>
                                                </tr>
                                                <%} %>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="50%" valign="top"> 
                                        <h3 style="font-size:16px; font-family:Arial; background-color:#60c231; padding: 7px 10px 7px 10px; color:#fff;">Rate Details </h3>
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;"> 
                                            <tbody>
                                              <% decimal totalPrice = 0;
                                                  decimal handlingCharge = 0;
                                                  decimal serviceTax = 0;
                                                  decimal tdsCommission = 0;
                                                  decimal tdsPLB = 0;
                                                  decimal plb = 0;
                                                  decimal transactionFee = 0;
                                                  PriceAccounts price = new PriceAccounts();
                                                  price = hRoomList[0].Price;
                                                  decimal rateofExchange = price.RateOfExchange;
                                                  decimal markup = 0, addlMarkup = 0, discount = 0, b2cMarkup = 0;
                                                  decimal outPutVat = 0, inPutVat = 0;
                                                  string symbol = Util.GetCurrencySymbol(price.Currency);
                                                  decimal CGST = 0, SGST = 0, IGST = 0;
                                                  int CGSTPer = 0, IGSTPer = 0, SGSTPer = 0;
                                                  decimal agentInputVat = 0;
                                                  if (hRoomList[0].Price.AccPriceType == PriceType.PublishedFare)
                                                  {
                                                      for (int k = 0; k < hRoomList.Length; k++)
                                                      {
                                                          totalPrice = totalPrice + (hRoomList[k].Price.PublishedFare + hRoomList[k].Price.OtherCharges);
                                                          markup += hRoomList[k].Price.Markup;
                                                          b2cMarkup += hRoomList[k].Price.B2CMarkup;
                                                      }
                                                  }
                                                  else if (hRoomList[0].Price.AccPriceType == PriceType.NetFare)
                                                  {
                                                      for (int k = 0; k < hRoomList.Length; k++)
                                                      {
                                                          totalPrice = totalPrice + (hRoomList[k].Price.NetFare + hRoomList[k].Price.OtherCharges );
                                                          discount += hRoomList[k].Price.Discount;
                                                          markup += hRoomList[k].Price.Markup;
                                                          addlMarkup += hRoomList[k].Price.AsvAmount;
                                                          b2cMarkup += hRoomList[k].Price.B2CMarkup;
                                                          if (location.CountryCode == "IN") //If Booking Login Country IN is there Need to load GST
                                                          {
                                                              List<GSTTaxDetail> taxDetList = GSTTaxDetail.LoadGSTDetailsByPriceId(hRoomList[k].Price.PriceId);
                                                              if (taxDetList != null && taxDetList.Count > 0)
                                                              {
                                                                  CGST += taxDetList.Where(p => p.TaxCode == "CGST").Sum(p => p.TaxAmount);
                                                                  SGST += taxDetList.Where(p => p.TaxCode == "SGST").Sum(p => p.TaxAmount);
                                                                  IGST += taxDetList.Where(p => p.TaxCode == "IGST").Sum(p => p.TaxAmount);
                                                                  GSTTaxDetail objTaxDet = taxDetList.Where(p => p.TaxCode == "CGST").FirstOrDefault();
                                                                  if (objTaxDet != null)
                                                                  {
                                                                      CGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                                                                  }
                                                                  objTaxDet = taxDetList.Where(p => p.TaxCode == "SGST").FirstOrDefault();
                                                                  if (objTaxDet != null)
                                                                  {
                                                                      SGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                                                                  }
                                                                  objTaxDet = taxDetList.Where(p => p.TaxCode == "IGST").FirstOrDefault();
                                                                  if (objTaxDet != null)
                                                                  {
                                                                      IGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                                                                  }
                                                              }
                                                          }
                                                          else
                                                          {
                                                              outPutVat += hRoomList[k].Price.OutputVATAmount;
                                                              inPutVat += hRoomList[k].Price.InputVATAmount;
                                                          }
                                                      }

                                                      if(agencyType==AgentType.BaseAgent || agencyType==AgentType.Agent)
                                                      {
                                                          agentInputVat = inPutVat;
                                                      }
                                                      totalPrice -= inPutVat;
                                                      // else
                                                      //  inPutVat = 0;
                                                  }
                                                  %>                                                                                    
                                                <tr>
                                                    <td style="vertical-align: top; width: 120px;"><b>Net Amount:</b></td>
                                                    <td style="vertical-align: top;">
                                                        <%=price.Currency  %>
                                                        <% if (agencyId <= 1)
                                                        {%>
                                                            <%=((totalPrice)).ToString("N" + agency.DecimalValue)%>
                                                        <%}
                                                        else {%>
                                                            <%=(Math.Ceiling(totalPrice + markup)).ToString("N" + agency.DecimalValue)%>
                                                        <%} %>
                                        
                                                    </td>
                                                </tr>    
                                                <tr>
                                                    <%if (agencyId == 1) { %>
                                                    <td style="vertical-align: top; width: 120px;"><b>Markup: </b></td>
                                                    <td style="vertical-align: top;">
                                                        <%=price.Currency %>
                                                        <%=((markup) ).ToString("N"+agency.DecimalValue)%>
                                                    </td>
                                                    <%} %>
                                                </tr>
                                                <%if (discount > 0) { %>
                                                <tr>
                                                    <td style="vertical-align: top; width: 120px;"><b>Discount:</b></td>
                                                    <td style="vertical-align: top;">
                                                        <%=price.Currency%>
                                                        <%=((discount)).ToString("N"+agency.DecimalValue)%>
                                                    </td>
                                                </tr>
                                                <%} %>
                                                <%if (addlMarkup > 0)
                                                {%>
                                                <tr>
                                                    <td style="vertical-align: top; width: 120px;"><b>Addl Markup: </b></td>
                                                    <td style="vertical-align: top;">
                                                        <%=price.Currency  %>
                                                        <%=(addlMarkup).ToString("N"+agency.DecimalValue)%>
                                                    </td>
                                                </tr>
                                                <%} %>
                                                <%if (location.CountryCode !="IN" && (agencyType==AgentType.BaseAgent || agencyType==AgentType.Agent) ){%>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>In Vat: </b></td>  
                                                    <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(inPutVat).ToString("N"+agency.DecimalValue)%></td>                                 
                                                </tr>                               
                                                <% }%>                             
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Gross Amount:  </b></td>
                                                    <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(Math.Ceiling((totalPrice + markup+agentInputVat)) - handlingCharge - plb + serviceTax + tdsCommission + tdsPLB + transactionFee - discount + addlMarkup).ToString("N" + agency.DecimalValue)%></td>
                                                </tr>                            
                                                <%if (location.CountryCode != "IN") {%>  
                                               <%  if (agencyType == AgentType.BaseAgent || agencyType == AgentType.Agent)
                                                   {%>
                                               <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Out Vat: </b></td>
                                                    <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(outPutVat).ToString("N" + agency.DecimalValue)%></td>
                                                </tr>     
                                                <%} %>
                                                <%else
                                                 { %>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Vat: </b></td>
                                                    <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(inPutVat + outPutVat).ToString("N" + agency.DecimalValue)%></td>
                                                </tr> 
                                                <%} %>
                                                <%}
                                                else { %> 
                                                    <%if(IGST > 0) { %>    
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>IGST(<%=IGSTPer %>) %: </b></td>
                                                    <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(IGST).ToString("N" + agency.DecimalValue)%></td>
                                                </tr>
                                                    <%} %>
                                                    <%if(CGST > 0) { %>    
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>CGST(<%=CGSTPer %>) %: </b></td>
                                                    <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(CGST).ToString("N" + agency.DecimalValue)%></td>
                                                </tr>
                                                    <%} %>
                                                    <%if(SGST > 0) { %>    
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>SGST(<%=SGSTPer %>) %: </b></td>
                                                    <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(SGST).ToString("N" + agency.DecimalValue)%></td>
                                                </tr>
                                                    <%} %>
                                                <%} %>                     
                                              <tr>
                                                <td style="vertical-align: top;width: 120px;"><b>Total Amount: </b></td>
                                                <td style="vertical-align: top;"><%=price.Currency%> <%=Math.Ceiling(Math.Ceiling(totalPrice + markup+inPutVat) + serviceTax + tdsCommission + transactionFee - discount + addlMarkup + Math.Ceiling(outPutVat)+Math.Ceiling(IGST+CGST+SGST)).ToString("N"+agency.DecimalValue)%></td>
                                              </tr>                      
                                            </tbody>
                                          </table>
                                    </td>
                                </tr>
                                <tr> 
                                    <td width="50%" valign="top"> 
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;"> 
                                            <tr>
                                                <% UserMaster member = new UserMaster(invoice.CreatedBy); %>
                                                <td style="vertical-align: top; width: 120px;"><b>invoiced By: </b></td>
                                                 <td style="vertical-align: top;"  align="left"><%= agency.AgentParantId>0?parentAgency.Name:agency.Name%></td>
                                            </tr>   
                                            <tr>
                                                <td style="vertical-align: top; width: 120px;"><b>Created By: </b></td>
                                                <td style="vertical-align: top;" align="left"><%= member.FirstName + " " + member.LastName%></td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top; width: 120px;"><b>Location: </b></td>
                                                <td style="vertical-align: top;" align="left"><%=location.Name %></td>
                                            </tr>
                                            <tr>
                                                <td style="width:120px">
                                                    <div style="display:none"><img src="images/Email1.gif"  />
                                                        <a href="javascript:ShowEmailDivD()">Email Invoice</a>
                                                    </div>
                                                </td>
                                                <td style="width:160px"><span id="LowerEmailSpan" style="margin-left: 15px;"></span> </td>
                                                <td>
                                                    <div style="display:none;" id="emailSent">
                                                        <span  style="float:left;width:100%;margin:auto;text-align:center;">
                                                            <span id="messageText" style="background-color:Yellow";>Email sent successfully</span>
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="50%" valign="top"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </body>
</html>
