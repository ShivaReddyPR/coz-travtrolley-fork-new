﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ExpReimbursementQueueUI"
    Title="Reimbursement Queue" Codebehind="ExpReimbursementQueue.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <script type="text/javascript">

        //Validate to enter only number
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                return false;
            }
            return true;
        }

        //Validate amount entered should not be greater tahn balance amount
        function getValue(rowId, action) {

            var id = rowId.substring(0, rowId.lastIndexOf('_') + 1);
            console.log(id);
            if (action == "U") {

                var amount = document.getElementById(id + 'txtReimAmount').value;
                var balance = document.getElementById(id + 'ITlblBALANCE').innerHTML;

                amount = parseInt(amount);
                balance = parseInt(balance);
                if (amount > balance) {
                    document.getElementById(id + 'txtReimAmount').value = '';
                    alert("Entered amount is greater than balance amount !");
                }
            }
        }

        //Validating Dates
        function Validate() {

            if ($("#ctl00_cphTransaction_expReimFromDate_Date").val() == "" || $("#ctl00_cphTransaction_expReimToDate_Date").val() == "") {
                if ($("#ctl00_cphTransaction_expReimFromDate_Date").val() == "") {
                    toastr.error('Please Select From Date');
                    $('#ctl00_cphTransaction_expReimFromDate_Date').parent().addClass('form-text-error');
                }
                if ($("#ctl00_cphTransaction_expReimToDate_Date").val() == "") {
                    toastr.error('Please Select To Date');
                    $('#ctl00_cphTransaction_expReimToDate_Date').parent().addClass('form-text-error');
                }
                return false;
            }
            else {
                var fromDate = $("#ctl00_cphTransaction_expReimFromDate_Date").val();
                var toDate =   $("#ctl00_cphTransaction_expReimToDate_Date").val();
                if (fromDate > toDate) {
                    toastr.error('Please Select From Date should be less than todate.');
                    return false;
                }
            }
        }

        //To open pop window and get Expenses details and documents to download 
        function openWindow(ED_Id) {

            var ED_Id = parseInt(ED_Id)
            getReportData(ED_Id);
            getExpenseReceipts(ED_Id);
            $('#ctl00_cphTransaction_hdnEDID').val(ED_Id);
            $("#ReportData").modal("show");
        }

        //To bind Expenses details
        function getReportData(ED_id) {

            var data = AjaxCall('ExpReimbursementQueue.aspx/getReportData', "{'ED_Id':'" + ED_id + "'}");
            $("#ReportDetails").html('');

            if (!IsEmpty(data)) {

                var expDtls = JSON.parse(data);
                
                var items = '<table class="table table-bordered b2b-corp-table">';

                items += '<thead><tr><th>DATE</th><th>Employee_Name</th><th>Destination</th><th>CostCenter</th><th>EXPENSE_TYPE</th><th>REFERENCE_CODE</th><th>CURRENCY</th><th>AMOUNT</th><th>COMMENT</th></tr></thead><tbody>'

                $.each(expDtls, function (i, val) {

                    items += '<tr><td>' + val.Date + '</td><td>' + val.EMP_NAME + '</td><td>' + val.DESTINATION + '</td><td>' + val.COSTCENTER + '</td><td>' + val.Exp_Type + '</td><td>' + val.ER_RefNo + '</td><td>' + '<%=Settings.LoginInfo.Currency%>' + '</td><td>' + val.AMOUNT + '</td><td>' + val.COMMENT + '</td></tr>'                            
                });

                items += '</tbody></table>';                 
                $("#ReportDetails").html(items);           
            }

            //$.ajax({
            //    type: 'POST',
            //    url: "ExpReimbursementQueue.aspx/getReportData",
            //     dataType: "json",
            //    contentType: "application/json",  
            //    data: "{'ED_Id':'" + ED_id + "'}",                              
            //    success: function (data) {
            //        var d = JSON.parse(data.d);
            //        $("#ReportDetails").html('');
            //        var items = '';
            //        items = '<table class="table table-bordered b2b-corp-table">'
            //        items += '<thead><tr><th>DATE</th><th>Employee_Name</th><th>Destination</th><th>CostCenter</th><th>EXPENSE_TYPE</th><th>REFERENCE_CODE</th><th>CURRENCY</th><th>AMOUNT</th><th>COMMENT</th></tr></thead><tbody>'
            //        $.each(d, function (i, val) {
            //            items += '<tr><td>' + val.Date + '</td><td>' + val.EMP_NAME + '</td><td>' + val.DESTINATION + '</td><td>' + val.COSTCENTER + '</td><td>' + val.Exp_Type + '</td><td>' + val.ER_RefNo + '</td><td>' + val.CURRENCY + '</td><td>' + val.AMOUNT + '</td><td>' + val.COMMENT + '</td></tr>'                            
            //        });
            //        items += '</tbody></table>';                 
            //        $("#ReportDetails").html(items);           
            //    }
            //})              
        }

        //To bind documents to download
        function getExpenseReceipts(ED_Id) {

           
            var data = AjaxCall('ExpReimbursementQueue.aspx/getDocDetails', "{'ED_Id':'" + ED_Id + "'}");

            $("#DocDetails").html('');

            if (!IsEmpty(data)) {

                var docs = JSON.parse(data);
                var items = '<table class="table table-bordered b2b-corp-table"><thead><tr><th>DOC ID</th><th>Preview</th><th>DOWNLOAD</th></tr></thead><tbody>';

                $.each(docs, function (i, val) {

                    var fileName = val.RC_FileName == 'NR' ? 'Affidavit Generated' : val.RC_FileName;
                    var filePath = val.RC_FileName == 'NR' ? "ShowAffidavit('" + val.RC_ED_Id + "')" : "Download('" + val.RC_Path + "', '" + fileName + "')";
                    var preview = '<td><a onclick="' + filePath + '" class="btn btn-outline btn-default"><img src="build/img/pdf-icon.svg" alt="view PDF" style="width: 35px;"></a></td>';

                    if (val.RC_FileName != 'NR') {

                        var imagePath = val.RC_Path.replaceAll('/', '\\');
                        imagePath = imagePath.substring(imagePath.indexOf('\\Upload\\'), imagePath.Length);
                        var ext = fileName.split('.').reverse()[0].toLowerCase();

                       if (ext === "pdf") {
                            preview = '<td><a href="#viewPdfModal" data-toggle="modal" data-target="#viewPdfModal" class="btn btn-outline btn-default viewPdfModalBtn"><img src="build/img/pdf-icon.svg" data-obj-url="' + imagePath + '" alt="view PDF" style="width: 35px;"></a></td>';
                            //$('#prevImageDiv').html('<object data="' + imagePath + '" type="application/pdf" height="400" width="100%"></object>');
                       }
                        else if (ext === "jpg" || "jpeg" || "png" ) {
                            preview = '<td><a href="#viewPdfModal" data-toggle="modal" data-target="#viewPdfModal" class="btn btn-outline btn-default viewPdfModalBtn"><img src="' + imagePath + '" class="thumbnail mb-0" style="width:70px;" ></a></td>';
                            //$('#prevImageDiv').html('<a href="' + imagePath + '" download="' + imagePath + '" target="_blank"><img src="' + imagePath + '" class="thumbnail" style="margin: 0 auto;max-width:100%" ></a>');
                        }
                        else {
                            preview = '<td><a href="' + imagePath + '" download="' + fileName + '" class="btn btn-outline btn-default"><span class="icon icon-files" style="font-size: 24px;line-height: 1;"></span></a></td>';
                        }
                    }

                    items += '<tr><td>' + fileName + '</td>' + preview + '<td><a href="#" onclick="' + filePath + '">Download</a></td></tr>'                            
                });

                items += '</tbody></table>';
                $("#DocDetails").html(items);
            }

            //$.ajax({
            //    type: 'POST',
            //    url: "ExpReimbursementQueue.aspx/getDocDetails",
            //     dataType: "json",
            //    contentType: "application/json",  
            //    data: "{'ED_Id':'" + ED_Id + "'}",                              
            //    success: function (data) {
            //        var d = JSON.parse(data.d);
            //        $("#DocDetails").html('');
            //        var items = '';
            //        items = '<table class="table table-bordered b2b-corp-table">'
            //        items += '<thead><tr><th>DOC ID</th><th>Preview</th><th>DOWNLOAD</th></tr></thead><tbody>'
            //        $.each(d, function (i, val) {

            //            var fileName = val.RC_FileName == 'NR' ? 'Affidavit Generated' : val.RC_FileName;
            //            var filePath = val.RC_FileName == 'NR' ? "ShowAffidavit('" + val.RC_ED_Id + "')" :
            //                "Download('" + val.RC_Path + "', '" + fileName + "')";
            //            items += '<tr><td>' + fileName + '</td><td><a href="#" onclick="' + filePath + '">Download</a></td></tr>'                            
            //        });
            //        items += '</tbody></table>';
            //        $("#DocDetails").html(items);
            //       // d.length > 0 ? $("#ctl00_cphTransaction_btnDownload").show() : $("#ctl00_cphTransaction_btnDownload").hide();
            //    }
            //})
            
        }

        /* To show affidavit for selected expense type */
        function ShowAffidavit(eDID) {

            AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpAffidavit', 'sessionData':'" + JSON.stringify({ ExpDtlId: eDID }) + "', 'action':'set'}");
            window.open('ExpAffidavit.aspx', 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes');
        }

        //To download the documents
        function Download(path, docName) {
            var open = window.open("DownloadeDoc.aspx?path=" + path + "&docName=" + docName);
            return false;
        }

        function ValidateCheckbox(type) {
            var chkboxrowcount = $("#<%=gvSearch.ClientID%> input[type=checkbox]:checked").size();
            if (chkboxrowcount == 0) {
                alert("Please Select atleast one Item ! ");
                return false;
            }
            else if (type == "reject") {
                var selectedRecords = $("#<%=gvSearch.ClientID%> input[type=checkbox]:checked");
                for (var i = 0; i < selectedRecords.length; i++) {
                    var cid = selectedRecords[i].id;
                    cid = cid.replace("ITchkSelect", "txtComments");
                    if ($('#' + cid).val() == '') {
                        $("#" + cid).focus();
                        $("#" + cid).css("border-color", "Red");
                        return false;
                    }
                }
            }
        }
        //To bind Expenses details
        function GenerateExcel() {
            if (!IsEmpty($('#ctl00_cphTransaction_hdnReimburseData').val())) {
                var reimbursementQueue = JSON.parse($('#ctl00_cphTransaction_hdnReimburseData').val());
                var bgcolor = $('.themecol1').css('background-color');
                var tab_text = "<table  border='1'>";
                tab_text += '<tr style="font-weight:bold;text-align:center">'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">DATE </span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">EMPLOYEE ID</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">EMPLOYEE NAME</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">EXPENSE TITLE</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">EXPENSE REF</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">EXPENSE CATEGORY</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">EXPENSE TYPE</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">CLAIM EXPENSE</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">CLAIM AMOUNT</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">PAYMENT TYPE</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">BUSINESS PURPOSE</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">COST CENTER</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">GL ACCOUNT</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">CITY</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">CURRENCY</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">PAY AMOUNT</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">TOTAL REIM. AMOUNT</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">BALANCE</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">STATUS</span></td>'
                    + '<td style="width:150px;background-color:Black;"><span style="color: White;">EXPENSE DATE</span></td>'
                    + '</tr>';
                var rowcount = reimbursementQueue.length;

                $.each(reimbursementQueue, function (key, col) {
                    tab_text += '<tr>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.DATE + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.EMP_ID + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.EMP_NAME + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.ER_Title + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.ER_RefNo + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.EXP_CATEGORY + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.ET_Desc + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.ED_ClaimExpense + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.ED_TotalAmount + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.PT_Desc + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.ER_Purpose + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.CostCentre + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.ET_GLAccount + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.ED_City + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.ED_Currency + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.ED_TotalAmount + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.TotalReimbursementAmount + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.Balance + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.ApprovalStatus + ' </span></td>'
                        + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.ED_TransDate + ' </span></td>'
                        + '</tr>';

                });
                tab_text = tab_text + "</table>";
                var date = new Date();//    1048576 
                var link = document.createElement("A");
                link.href = 'data:text/application/vnd.ms-excel,' + encodeURIComponent(tab_text);
                link.download = "ExpReimbursementQueue_" + date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() + '-' + date.getTime() + ".xls";
                link.click();
            }
        }
          
        /* To open exp report details */
        function ShowReferenceDetails(expDtlId) {

            if (parseInt(expDtlId) > 0) {

                AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpReportPrint', 'sessionData':'" + JSON.stringify({ expDetailId: expDtlId }) + "', 'action':'set'}");
                window.open("ExpReportPrint.aspx", "Expense Report", "width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
            }
        }
          
        /* Open expense report screen */
        function OpenExpReport(expRepId, expDetailId) {

            if (parseInt(expRepId) > 0 && parseInt(expDetailId) > 0) {

                AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpReport', 'sessionData':'" + JSON.stringify({ ExpRepId: expRepId, ExpDtlId: expDetailId }) + "', 'action':'set'}");
                window.location.href = "ExpReport.aspx";
            }
        }
          
        /* Open expense create screen */
        function OpenExpCreate(expRepId, expDetailId) {

            if (parseInt(expRepId) > 0 && parseInt(expDetailId) > 0) {

                AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpCreate', 'sessionData':'" + JSON.stringify({ ExpRepId: expRepId, ExpDtlId: expDetailId }) + "', 'action':'set'}");
                window.location.href = "ExpCreate.aspx";
            }
        }
        //$('.viewPdfModal').on('show.bs.modal', function () {
        //    $('#ReportData').css('z-index', 1030);
        //})

        //$('.viewPdfModal').on('hidden.bs.modal', function () {
        //    $('#ReportData').css('z-index', 1040);
        //})
        //$('.viewPdfModal').each(function () {
        //    $(this).addClass('body')
        //})

        //$('#ReportData').on('show.bs.modal', function () {
        //    $('.viewPdfModal').each(function () {
        //        $(this).appendTo('body')
        //    })
        //})

        //$('#ReportData').on('show.bs.modal', function () {
        //    $('.viewPdfModal').each(function () {
        //        $(this).appendTo('body')
        //    })
        //}) 
        $(function () {
            $('body').on('click', '.viewPdfModalBtn', function () {
               
                if ($(this).find('img').attr('data-obj-url')) {
                    var imagePath = $(this).find('img').attr('data-obj-url');
                    var imgContent = '<object data="' + imagePath + '" type="application/pdf" height="400" width="100%"></object>';
                    $('#prevImageDiv').html(imgContent);
                } else if ($(this).find('img').attr('src')) {
                    var imagePath = $(this).find('img').attr('src');
                    var imgContent = '<a href="' + imagePath + '" download="' + imagePath + '" target="_blank"><img src="' + imagePath + '" class="thumbnail" style="margin: 0 auto;max-width:100%" ></a>';
                    $('#prevImageDiv').html(imgContent);
                }
            })
        })
    </script>
       <div class="body_container">
        <div class="col-md-12 CorpTrvl-tabbed-panel">
            <h2>
                Reimbursement Queue</h2>
            <div id="errMess" style="display: none; color: Red; font-weight: bold; text-align: center;">
            </div>
            <div class="tab-content responsive">
                <div style="background: #fff; padding: 10px;">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    From Date <span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="expReimFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                    </uc1:DateControl>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    To Date<span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="expReimToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Select Employee</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlEmployee" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Expense Category</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlExpCategory" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>                                   
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Expense Type</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlExpType" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="center-block">
                                &nbsp;</label>
                            <asp:Button OnClientClick="return Validate();" OnClick="btnSearch_Click" runat="server"
                                ID="btnSearch" CssClass="btn but_d btn_xs_block cursor_point mar-5" Text="Search" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table table-responsive  mb-0">
                                 <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="ED_Id"
                                EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging"
                                OnRowDataBound="gvSearch_RowDataBound">
                                <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="left" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="IThdfED_Id" runat="server" Value='<%# Bind("ED_Id") %>'>
                                            </asp:HiddenField>
                                            <asp:HiddenField ID="hdfClaimAmount" runat="server" Value='<%# Bind("ED_TotalAmount") %>'>
                                            </asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="center" />
                                        <HeaderTemplate>
                                            <label style="color: Black">
                                                Select</label>
                                            <asp:CheckBox OnCheckedChanged="HTchkSelect_CheckedChanged" runat="server" AutoPostBack="true"
                                                ID="HTchkSelectAll"></asp:CheckBox>
                                        </HeaderTemplate>
                                        <ItemStyle />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" CssClass="label" OnCheckedChanged="ITchkSelect_CheckedChanged" AutoPostBack="true"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtDATE" Width="100px" HeaderText="DATE" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDATE" runat="server" Text='<%# Eval("DATE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("DATE") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMPLOYEEID" Width="70px" HeaderText="EMPLOYEE ID" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMPLOYEEID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_ID") %>' Width="70px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMPLOYEENAME" Width="100px" HeaderText="EMPLOYEE NAME" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMPLOYEENAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_NAME") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPReortTitle" Width="100px" HeaderText="EXPENSE TITLE" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <a href="#" onclick="OpenExpReport('<%# Eval("ER_Id") %>', '<%# Eval("ED_Id") %>');" ><%# Eval("ER_Title") %></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPREF" Width="100px" HeaderText="EXPENSE REF" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                             <a href="#" onclick="ShowReferenceDetails('<%# Eval("ER_Id") %>');" ><%# Eval("ER_RefNo") %></a>
                                           </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPCAT" Width="100px" HeaderText="EXPENSE CATEGORY" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEXPCAT" runat="server" Text='<%# Eval("EXP_CATEGORY") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EXP_CATEGORY") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPTYPE" Width="100px" HeaderText="EXPENSE TYPE" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                             <a href="#" onclick="OpenExpCreate('<%# Eval("ER_Id") %>', '<%# Eval("ED_Id") %>');" ><%# Eval("EXP_TYPE") %></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtClaimExpense" Width="100px" HeaderText="CLAIM EXPENSE" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblClaimExpense" runat="server" Text='<%# Eval("ED_ClaimExpense") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("ED_ClaimExpense") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtAMOUNT" Width="100px" HeaderText="CLAIM AMOUNT" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAMOUNT" runat="server" Text='<%# Eval("ED_TotalAmount") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("ED_TotalAmount") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtPaymentType" Width="100px" HeaderText="PAYMENT TYPE" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPaymentType" runat="server" Text='<%# Eval("PT_Desc") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("PT_Desc") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtETDesc" Width="100px" HeaderText="BUSINESS PURPOSE" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblETDesc" runat="server" Text='<%# Eval("ER_Purpose") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("ER_Purpose") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTCostCentre" Width="100px" HeaderText="COST CENTER" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCostCentre" runat="server" Text='<%# Eval("CostCentre") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("CostCentre") %>'  ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtGLAccount" Width="100px" HeaderText="GL ACCOUNT" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGLAccount" runat="server" Text='<%# Eval("ET_GLAccount") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("ET_GLAccount") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtcity" Width="100px" HeaderText="CITY" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblcity" runat="server" Text='<%# Eval("ED_City") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("ED_City") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="CURRENCY">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlCurrency" runat="server">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PAY AMOUNT">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" onkeypress="return isNumber(event);" onChange="getValue(this.id,'U')"
                                              CssClass="form-control"   ID="txtReimAmount" Enabled="false" Width="100px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Comments">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server"  ID="txtComments"  CssClass="form-control" Enabled="false" Width="100px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtTRAMOUNT" Width="100px" HeaderText="TOTAL REIM. AMOUNT" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblTRAMOUNT" runat="server" Text='<%# Eval("TotalReimbursementAmount") %>'
                                                CssClass="label grdof" ToolTip='<%# Eval("TotalReimbursementAmount") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtBALANCE" Width="100px" HeaderText="BALANCE" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblBALANCE" runat="server" Text='<%# Eval("Balance") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("Balance") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STATUS">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtStatus" Width="100px" HeaderText="STATUS" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblStatus"  Text='<%# Eval("ApprovalStatus") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("ApprovalStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DETAILS">
                                        <ItemTemplate>
                                            <a href="#" onclick='openWindow("<%# Eval("ED_Id") %>");'>View Details</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pad_10">
                <asp:HiddenField ID="hdnReimburseData" runat="server" />
                <asp:HiddenField ID="hdnEDID" runat="server" Value="" />
                <asp:Button OnClick="btnPay_Click" runat="server" ID="btnPay" Visible="false" CssClass="btn but_d btn_xs_block cursor_point mar-5" Text="PAY NOW" OnClientClick="return ValidateCheckbox('pay');" />
                <asp:Button OnClick="btnReject_Click" runat="server" ID="btnReject" Visible="false" CssClass="btn but_d btn_xs_block cursor_point mar-5" Text="Revise" OnClientClick="return ValidateCheckbox('reject');" />
                <a class="btn btn-primary " id="excelReport" onclick="GenerateExcel()">Export To Excel</a>
                <div class="clearfix">
                </div>
            </div>
            
        </div>
     </div>
    <!-- Modal PopUp DIV-->
    <div class="modal fade in farerule-modal-style" data-backdrop="static" id="ReportData" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel" style="display: none;">
        <div class="modal-dialog modal-lg" role="document" style="width: 900px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="btnModelC">&times;</button>
                    <h4 class="modal-title">Expenses Details</h4>
                </div>
                <div class="clearfix"></div>
                <div class="table-responsive bg_white">
                    <p id="ReportDetails"></p>
                </div>
                <div class="clearfix"></div>
                <div class="table-responsive bg_white">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>                    
                            <asp:Button ID="btnDownload" class="btn btn-primary" style="float:right;display:none" runat="server" OnClick="btnDownload_Click" Text="Download All" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnDownload" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <p id="DocDetails"></p>
                </div>
            </div>
        </div>
    </div>


    <!-- Email DIV-->
    <div id='EmailDivEmployee' runat='server' style='width: 100%; display: none;'>
    <%if (detail != null) %>                                        
                          <%{  %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        </head>
        <body>
            <div style="margin: 0; background: #f3f3f3!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; min-width: 100%;
                padding: 0; text-align: left; width: 100%!important">
                <span style="color: #f3f3f3; display: none!important; font-size: 1px; line-height: 1px;
                    max-height: 0; max-width: 0; overflow: hidden"></span>
                <table style="margin: 0; background: #f3f3f3!important; border-collapse: collapse;
                    border-spacing: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                    font-weight: 400; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left;
                    vertical-align: top; width: 100%">
                    <tr style="padding: 0; text-align: left; vertical-align: top">
                        <td align="center" valign="top" style="margin: 0; border-collapse: collapse!important;
                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                            line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top;
                            word-wrap: break-word">
                            <center style="min-width: 580px; width: 100%">
                                <table style="margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0;
                                    float: none; margin: 0 auto; margin-top: 20px; padding: 10px; text-align: center;
                                    vertical-align: top; width: 580px" align="center">
                                    <tbody>
                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                            <td style="margin: 0; border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left;
                                                vertical-align: top; word-wrap: break-word">
                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                    vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <td height="16px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 16px;
                                                                margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0;
                                                    position: relative; text-align: left; vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <th style="margin: 0 auto; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0 auto; padding: 0;
                                                                padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left;
                                                                width: 564px">
                                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                    vertical-align: top; width: 100%">
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                            font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                            <img src="<%=Request.Url.Scheme%>://www.travtrolley.com/images/logo.jpg" style="clear: both; display: block;
                                                                                max-width: 100%; outline: 0; text-decoration: none; width: auto">
                                                                        </th>
                                                                        <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                            font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important; text-align: left;
                                                                            width: 0">
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                    vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <td height="16px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 16px;
                                                                margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                
                                                
                                                <table style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0;
                                                    position: relative; text-align: left; vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <th style="margin: 0 auto; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0 auto; padding: 0;
                                                                padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left;
                                                                width: 564px">
                                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                    vertical-align: top; width: 100%">
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                            font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                            <h1 style="margin: 0; margin-bottom: 10px; color: inherit; font-family: Helvetica,Arial,sans-serif;
                                                                                font-size: 20px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                padding: 0; text-align: left; word-wrap: normal">
                                                                                Dear  <%=detail.EMP_Name%>,</h1>
                                                                            <p style="margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                padding: 0; text-align: left">
                                                                                Below expense are reimbursed.</p>
                                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                vertical-align: top; width: 100%">
                                                                                <tbody>
                                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                        <td height="30px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                            font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400; line-height: 30px;
                                                                                            margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            
                                                                            <table style="background: #fff; border: 1px solid #167f92; border-collapse: collapse;
                                                                                border-radius: 10px; border-spacing: 0; color: #024457; font-size: 11px; margin: 1em 0;
                                                                                padding: 0; text-align: left; vertical-align: top; width: 100%">
                                                                                <tr style="background-color: #eaf3f3; border: 1px solid #d9e4e6; padding: 0; text-align: left;
                                                                                    vertical-align: top">
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Date
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Expense Ref
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Expense Category
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Expense Type
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                               Claim  Amount
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                           Reimbursed Amount
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                           Balance Amount
                                                                                    </th>
                                                                                    
                                                                                </tr>
                                                                                
                                                                  
                          
                                                                                <tr style="border: 1px solid #d9e4e6; padding: 0; text-align: left; vertical-align: top">
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                   <% = Convert.ToString(detail.Date).Split(' ')[0]%>
                                                                                    </td>
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                           <% = detail.DocNo%>
                                                                                    </td>
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                     <% =detail.Exp_CategoryText%>
                                                                                    </td>
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                    <% =detail.Exp_TypeText%>
                                                                                    </td>
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                                        <% =detail.ReimCurrency%> <% = detail.ClaimAmount%> 
                                                                                    </td>
                                                                                    
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                                        <% =detail.ReimCurrency%> <% = detail.ReimAmount%> 
                                                                                    </td>
                                                                                    
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                        <% =detail.ReimCurrency%> <% = detail.BalanceAmount%> 
                                                                                    </td>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                </tr>
                                                                                
                                                                                
                                                                                 
                              
                                                                                
                                                                            </table>
                                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                vertical-align: top; width: 100%">
                                                                                <tbody>
                                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                        <td height="30px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                            font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400; line-height: 30px;
                                                                                            margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <table style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0;
                                                                                position: relative; text-align: left; vertical-align: top; width: 100%">
                                                                                <tbody>
                                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                        <th style="margin: 0 auto; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                            font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0 auto; padding: 0;
                                                                                            padding-bottom: 16px; padding-left: 0!important; padding-right: 0!important;
                                                                                            text-align: left; width: 100%">
                                                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                                vertical-align: top; width: 100%">
                                                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                                    <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                                                        <p style="margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                                            font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                                            padding: 0; text-align: left">
                                                                                                            Regards<br>
                                                                                                           
                                 
                                                                                                            <strong><% = detail.AgencyName%></strong></p>
                                                      
                                                                                                    </th>
                                                                                                    <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important; text-align: left;
                                                                                                        width: 0">
                                                                                                    </th>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </th>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                        </td>
                    </tr>
                </table>
                <div style="display: none; white-space: nowrap; font: 15px courier; line-height: 0">
                </div>
            </div>

         

        </body>
        </html>
        <%} %>
    </div>
       <div class="modal fade viewPdfModal" id="viewPdfModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg"> <div class="modal-content"> 
                <div class="modal-body"><button type="button" class="close text-secondary position-relative" data-dismiss="modal" aria-label="Close" style="top: -10px;right: -8px;"> 
                    <span aria-hidden="true" class="icon icon-close"></span> </button>
                    <div id="prevImageDiv"></div>
                    </div></div></div></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
