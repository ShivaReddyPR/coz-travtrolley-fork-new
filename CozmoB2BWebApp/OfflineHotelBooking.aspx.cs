﻿using CT.BookingEngine;
using CT.Core;
using CT.Corporate;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using System.Web.UI;
namespace CozmoB2BWebApp
{
    public partial class OfflineHotelBooking : ParentPage
    {
        Dictionary<string, string> NationalityList = new Dictionary<string, string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;

            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            else
            {
                if (!IsPostBack)
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                }
                int agencyId = 0;
                Dictionary<string, string> diLoginInfo = new Dictionary<string, string>();
                agencyId = Settings.LoginInfo.AgentId;
                diLoginInfo.Add("Decimal", Convert.ToString(Settings.LoginInfo.DecimalValue));
                diLoginInfo.Add("OnBehalfAgentLocation", Settings.LoginInfo.LocationCountryCode);
                diLoginInfo.Add("AgentId", Convert.ToString(agencyId));
                diLoginInfo.Add("MemberType", Settings.LoginInfo.MemberType.ToString());
                diLoginInfo.Add("UserId", Convert.ToString(Settings.LoginInfo.UserID));
                diLoginInfo.Add("LocationID", Convert.ToString(Settings.LoginInfo.LocationID));
                diLoginInfo.Add("Currency", Convert.ToString(Settings.LoginInfo.Currency));
                hdnLoginInfo.Value = JsonConvert.SerializeObject(diLoginInfo);
                lblAgentBalance.Text = Convert.ToString(Settings.LoginInfo.Currency) + " " + Convert.ToString(Settings.LoginInfo.AgentBalance);
                lblAgentBalance.Style.Add("display", "block");
                DataTable dtLocations = CT.TicketReceipt.BusinessLayer.LocationMaster.GetList(Convert.ToInt32(agencyId), CT.TicketReceipt.BusinessLayer.ListStatus.Short, CT.TicketReceipt.BusinessLayer.RecordStatus.Activated, string.Empty);
                hdnloginAgentLocations.Value = JsonConvert.SerializeObject(dtLocations);//assigning login agent locations and binding if selected agent currency is not INR then binding these locations
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string LoadRequiredData()
        {
            try
            {
                DOTWCountry dotwC = new DOTWCountry();
                Dictionary<string, string> NationalityList = dotwC.GetAllCountries();//Nationality
                Dictionary<string, string> CitiesList = LoadCity();//Cities
                DataTable dtAgents = CT.TicketReceipt.BusinessLayer.AgentMaster.GetList(1, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//Clients
                DataTable dtOnlineSources = AgentMaster.GetAgentSources(Settings.LoginInfo.AgentId, 2, true);//AgentSources==>online Suppliers
                HotelSource htSource = new HotelSource();
                DataTable dtOfflineSources = htSource.LoadOfflineSuppliers(false, false);//here first false means Non Gimmonix and second false means not online sources
                DataTable dtCurrency = CurrencyMaster.GetList(ListStatus.Short, RecordStatus.Activated);
                DataRow[] drCurrency = dtCurrency.Select("CURRENCY_CODE IS NULL");
                foreach (DataRow drdata in drCurrency)
                {
                    dtCurrency.Rows.Remove(drdata);
                }
                DataSet dtRoomData = HotelRoom.getRoomData();
                DataTable dtFlexFields = CorporateProfile.GetAgentFlexDetailsByProduct(Settings.LoginInfo.AgentId, 1);
                Dictionary<string, Object> Data = new Dictionary<string, object>();
                Data.Add("Nationality", NationalityList);
                Data.Add("Cities", CitiesList);
                Data.Add("Agents", dtAgents);
                Data.Add("OnlineSources", dtOnlineSources);
                Data.Add("OfflineSources", dtOfflineSources);
                Data.Add("Currency", dtCurrency);
                Data.Add("RoomTypes", dtRoomData.Tables[0]);
                Data.Add("MealPlans", dtRoomData.Tables[1]);
                Data.Add("flexFields", dtFlexFields);
                string JsonData = JsonConvert.SerializeObject(Data);

                return JsonData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "exception at LoadRequiredData() due to:" + ex.ToString(), "");
                return null;
            }
        }
        private static DataTable getAgentMarkup(int agentId)
        {
            DataTable dtmarkup = new DataTable();
            try
            {
                dtmarkup = UpdateMarkup.Load(agentId, HotelBookingSource.Offline.ToString(), (int)ProductType.Hotel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtmarkup;
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string locationsByAgentId(string agentId)
        {
            try
            {
                DataTable dtLocations = CT.TicketReceipt.BusinessLayer.LocationMaster.GetList(Convert.ToInt32(agentId), CT.TicketReceipt.BusinessLayer.ListStatus.Short, CT.TicketReceipt.BusinessLayer.RecordStatus.Activated, string.Empty);
                Dictionary<string, Object> Data = new Dictionary<string, object>();
                Data.Add("Locations", dtLocations);
                AgentMaster agency = new AgentMaster();
                agency = new AgentMaster(Convert.ToInt64(agentId));
                Settings.LoginInfo.IsOnBehalfOfAgent = true;
                Settings.LoginInfo.OnBehalfAgentID = Convert.ToInt32(agentId);
                Data.Add("Agency", agency);
                Data.Add("flexFields", CorporateProfile.GetAgentFlexDetailsByProduct(Convert.ToInt32(agentId), 1));
                CT.Core.StaticData sd = new CT.Core.StaticData();
                sd.BaseCurrency = agency.AgentCurrency;
                Settings.LoginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                string JsonData = JsonConvert.SerializeObject(Data);

                return JsonData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "exception at locationsByAgentId() due to:" + ex.ToString(), "");
                return null;
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static List<HotelStaticData> LoadHotelNames(string city, string country)
        {
            try
            {
                HotelStaticData staticInfo = new HotelStaticData();
                List<HotelStaticData> hotelStaticData = staticInfo.StaticDataLoad(city.Split(',')[0], Country.GetCountryCodeFromCountryName(country), null,(int)HotelBookingSource.GIMMONIX);
                //  string JsonData = JsonConvert.SerializeObject(hotelStaticData);

                return hotelStaticData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "exception at LoadHotelNames() due to:" + ex.ToString(), "");
                return null;
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string getExchangeRates(string currency, string agentId,string locationId)
        {
            try
            {
                string JsonData = string.Empty;
                decimal rateOfExchange = 0;
                DataTable dtMarkup = new DataTable();
                if (!string.IsNullOrEmpty(currency))
                {
                    LocationMaster locationMaster = null;
                    if (string.IsNullOrEmpty(agentId))
                    {
                        rateOfExchange = Settings.LoginInfo.AgentExchangeRates.ContainsKey(currency)&& Settings.LoginInfo.AgentExchangeRates[currency]>0 ? Settings.LoginInfo.AgentExchangeRates[currency] : 0;
                        dtMarkup = getAgentMarkup(Convert.ToInt32(Settings.LoginInfo.AgentId));                       
                    }
                    else
                    {
                        if (Convert.ToInt32(agentId) > 0)
                        {
                            rateOfExchange = Settings.LoginInfo.OnBehalfAgentExchangeRates.ContainsKey(currency)&& Settings.LoginInfo.OnBehalfAgentExchangeRates[currency]>0 ? Settings.LoginInfo.OnBehalfAgentExchangeRates[currency] : 0;
                            dtMarkup = getAgentMarkup(Convert.ToInt32(agentId));                            
                            if (Settings.LoginInfo.IsOnBehalfOfAgent && !string.IsNullOrEmpty(locationId))
                            {
                                locationMaster = new LocationMaster(Convert.ToInt64(locationId));
                                Settings.LoginInfo.OnBehalfAgentLocation = Convert.ToInt32(locationId);
                            }
                        }
                    }
                    decimal markup = 0; string markupType = "F";
                    if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                    {
                        markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                        markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                    }
                    Dictionary<string, Object> Data = new Dictionary<string, object>();
                    Data.Add("exchangeRates", rateOfExchange);
                    Data.Add("markup", markup);
                    Data.Add("markupType", markupType);
                    Data.Add("Location", locationMaster!=null && !string.IsNullOrEmpty(locationMaster.CountryCode)? locationMaster.CountryCode:Settings.LoginInfo.LocationCountryCode);
                    JsonData = JsonConvert.SerializeObject(Data);
                }
                return JsonData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "exception at getExchangeRates() due to:" + ex.ToString(), "");
                return null;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string getVatDetails(string countryName)
        {
            try
            {
                string JsonData = string.Empty;
                if (!string.IsNullOrEmpty(countryName))
                {
                    PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(Country.GetCountryCodeFromCountryName(countryName), Settings.LoginInfo.LocationCountryCode, null, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                    JsonData = JsonConvert.SerializeObject(priceTaxDet);
                }
                return JsonData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "exception at getVatDetails() due to:" + ex.ToString(), "");
                return null;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string calculateInputVat(string roomPrice, string decimalpoint, string vatDetails)
        {
            try
            {
                decimal hotelTotalPrice = 0m;
                decimal vatAmount = 0m;
                string JsonData = string.Empty;
                PriceTaxDetails priceTaxDet = JsonConvert.DeserializeObject<PriceTaxDetails>(vatDetails);
                hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(Convert.ToDecimal(roomPrice), ref vatAmount, Convert.ToInt32(decimalpoint));
                Dictionary<string, Object> Data = new Dictionary<string, object>();
                Data.Add("RoomPrice", hotelTotalPrice);
                Data.Add("VatAmount", vatAmount);
                JsonData = JsonConvert.SerializeObject(Data);
                return JsonData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Exception at calculateInputVat(),reason is:" + ex.ToString(), "");
                return null;
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string LoadGSTValues(string roomMarkUp)
        {
            try
            {
                string JsonData = string.Empty;
                List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                decimal gstAmount = 0;
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, Convert.ToDecimal(roomMarkUp), Settings.LoginInfo.OnBehalfAgentLocation);
                }
                else
                {
                    gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, Convert.ToDecimal(roomMarkUp), Settings.LoginInfo.LocationID);
                }
                 
                JsonData = JsonConvert.SerializeObject(gstAmount);
                return JsonData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Exception at LoadGSTValues(),reason is:" + ex.ToString(), "");
                return null;
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string calculateOutputVat(string TotalAmount, string TotalMarkup, string vatDetails)
        {
            try
            {
                string JsonData = string.Empty;
                PriceTaxDetails priceTaxDet = JsonConvert.DeserializeObject<PriceTaxDetails>(vatDetails);
                decimal outPutVat = priceTaxDet.OutputVAT.CalculateVatAmount(Convert.ToDecimal(TotalAmount), Convert.ToDecimal(TotalMarkup), Settings.LoginInfo.DecimalValue);
                JsonData = JsonConvert.SerializeObject(outPutVat);
                return JsonData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Exception at calculateOutputVat(),reason is:" + ex.ToString(), "");
                return null;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string BindSuppliers()
        {
            try
            {
                DataTable dtHotelSources = AgentMaster.GetAgentSources(Settings.LoginInfo.AgentId, 2, true);
                //if sources having Gimmonix then only binding Gimmonix sub suppliers other than bind other suppliers
                DataRow[] drGimmonix = dtHotelSources.Select("Name='GIMMONIX'");
                if (drGimmonix != null && drGimmonix.Count() > 0)
                {
                    DataRow[] drUAH = dtHotelSources.Select("Name='UAH'");
                    HotelSource hotelSource = new HotelSource();
                    DataTable dtGxSuppliers = new DataTable();
                    dtGxSuppliers = hotelSource.LoadGimmonixSuppliers();
                    dtGxSuppliers.Columns[0].ColumnName = "SourceId";
                    dtGxSuppliers.Columns[1].ColumnName = "Name";
                    dtHotelSources.Rows.Remove(drGimmonix[0]);
                    dtHotelSources.Rows.Remove(drUAH[0]);
                    dtHotelSources.Merge(dtGxSuppliers);
                }
                string JsonString = JsonConvert.SerializeObject(dtHotelSources);
                return JsonString;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Exception at BindSuppliers(),reason is:" + ex.ToString(), "");
                return null;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string createBooking(string hotelItinerary, string hotelrequest,string Settlement)
        {
            try
            {

                string JsonData = string.Empty;
                HotelItinerary itinerary = JsonConvert.DeserializeObject<HotelItinerary>(hotelItinerary);
                HotelRequest request = JsonConvert.DeserializeObject<HotelRequest>(hotelrequest);
                SettlementData settlement = JsonConvert.DeserializeObject<SettlementData>(Settlement);

                DOTWCountry dotw = new DOTWCountry();
                Dictionary<string, string> Countries = null;
                Countries = dotw.GetAllCountries();
                string nationality = Countries.ContainsKey(request.PassengerNationality) ? Countries[request.PassengerNationality] : string.Empty;
                itinerary.PassengerNationality = nationality;
                itinerary.PassengerCountryOfResidence = nationality;
                itinerary.Source = HotelBookingSource.Offline;                
                MetaSearchEngine mse = new MetaSearchEngine();
                itinerary.HotelCategory = "";
                itinerary.VatDescription = "";
                itinerary.AgencyReference = "";
                itinerary.CancelId = "";
                //itinerary.HotelAddress2 = "";
                itinerary.CreatedOn = DateTime.Now;
                itinerary.CreatedBy = (int)Settings.LoginInfo.UserID;
                itinerary.PaymentMode = ModeOfPayment.Credit;
                itinerary.SpecialRequest = string.Empty;
                for (int i = 0; i < itinerary.NoOfRooms; i++)
                {
                    if (itinerary.Roomtype[i].PassenegerInfo.Count() > 0)
                    {
                        for (int j = 0; j < itinerary.Roomtype[i].PassenegerInfo.Count(); j++)
                        {
                            itinerary.Roomtype[i].PassenegerInfo[j].Nationality = itinerary.PassengerNationality;
                            itinerary.Roomtype[i].PassenegerInfo[j].Country = itinerary.PassengerCountryOfResidence;
                            itinerary.Roomtype[i].PassenegerInfo[j].NationalityCode = Countries.ContainsKey(request.PassengerNationality) ? Country.GetCountryCodeFromCountryName(Countries[request.PassengerNationality]) : string.Empty;
                        }
                    }

                }
                Product prod = itinerary;
                //Product prod = new Product();
                prod.ProductId = 2;
                prod.ProductType = ProductType.Hotel;
                prod.ProductTypeId = 2;
                AgentMaster agency = new AgentMaster();
                int agencyId = 0;
                agencyId = itinerary.AgencyId; //Selected Agency                       
                agency = new AgentMaster(agencyId);
                agency.CreatedBy = Settings.LoginInfo.UserID;
                agency.UpdateBalance(0);
                if (agencyId == Settings.LoginInfo.AgentId)
                {
                    Settings.LoginInfo.AgentBalance = agency.CurrentBalance;
                }
                decimal amountToCompare = agency.CurrentBalance;

                decimal total = 0, discount = 0, outVatAmount = 0;
                foreach (HotelRoom room in itinerary.Roomtype)
                {
                    ///room.Price.pri
                    if (room.Price.AccPriceType == PriceType.NetFare)
                    {
                        total += (room.Price.NetFare + room.Price.Markup);
                        outVatAmount += room.Price.OutputVATAmount;
                    }
                    else
                    {
                        total += (room.Price.PublishedFare);
                    }

                    discount = room.Price.Discount;
                }
                total = Math.Ceiling(total) + Math.Ceiling(outVatAmount);
                BookingResponse bookRes = new BookingResponse();
                if (total > amountToCompare)
                {
                    bookRes.Error = "Dear " + agency.Name + " you do not have sufficient balance to book a room for " + itinerary.HotelName;
                }
                else
                {
                    mse.SettingsLoginInfo = Settings.LoginInfo;
                    bookRes = mse.Book(ref prod, agencyId, BookingStatus.Ready, Settings.LoginInfo.UserID);
                    HttpContext.Current.Session["BookingResponse"] = bookRes;
                    HttpContext.Current.Session["req"] = request;

                    if (settlement != null && itinerary.Roomtype[0].Price.PriceId > 0)
                    {
                        foreach (SettlementDetailes settlementitem in settlement.SettlementObject)
                        {
                            settlementitem.CreatedBy = (int)Settings.LoginInfo.UserID;
                            settlementitem.UpdatedBy = (int)Settings.LoginInfo.UserID;
                            settlementitem.Status = 1;
                            settlementitem.ServiceReferenceId = itinerary.HotelId; //itinerary.Roomtype[0].Price.PriceId;
                            settlementitem.Save();
                        }

                    }
                    if ( 1==2 && bookRes.Status == BookingResponseStatus.Successful)
                    {
                        int invoiceNumber = Invoice.isInvoiceGenerated(itinerary.Roomtype[0].RoomId, ProductType.Hotel);
                        Invoice invoice = new Invoice();
                        if (invoiceNumber > 0)
                        {
                            invoice.Load(invoiceNumber);
                        }
                        else
                        {
                            decimal rateOfExchange = (Settings.LoginInfo.ExchangeRate > 0 ? Settings.LoginInfo.ExchangeRate : 1);
                            invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(itinerary.HotelId, string.Empty, (int)Settings.LoginInfo.UserID, ProductType.Hotel, rateOfExchange);
                            invoice.Load(invoiceNumber);
                            invoice.Status = InvoiceStatus.Paid;
                            invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                            invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                            invoice.UpdateInvoice();
                        }
                        //Reduce Agent Balance
                        //Update agent balance, reduce the hotel booking amount from the balance.             
                        if (agencyId == Settings.LoginInfo.AgentId)
                        {
                            Settings.LoginInfo.AgentBalance -= total;
                        }
                        else
                        {
                            HttpContext.Current.Session["BookingAgencyID"] = agencyId;
                        }

                        agency.UpdateBalance(-total);

                        HttpContext.Current.Session["BookingResponse"] = bookRes;
                        HttpContext.Current.Session["req"] = request;
                    }
                }
                JsonData = JsonConvert.SerializeObject(bookRes);
                return JsonData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Exception at createBooking(),reason is:" + ex.ToString(), "");
                return null;
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string auditSaving(string exception)
        {
            string JsonData = "";
            try
            {
                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                Audit.Add(EventType.Exception, Severity.High, 0, "exception reason is" + exception, "");
                string[] errorNotificationMails = Convert.ToString(CT.Configuration.ConfigurationSystem.Email["ErrorNotificationMailingId"]).Split(',');
                foreach (string mails in errorNotificationMails)
                {
                    toArray.Add(mails);
                }
                //string message = ConfigurationManager.AppSettings["HOTEL_CANCEL_REQUEST"];
                try
                {
                    Hashtable table = new Hashtable();

                   // CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "Error Notification in Offline booking entry", exception, table);
                }
                catch { }
                string response = "Success";
                JsonData = JsonConvert.SerializeObject(response);
                return JsonData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Exception at auditSaving(),reason is:" + ex.ToString(), "");
                return null;
            }
        }

        public static Dictionary<string, string> LoadCity()
        {
            try
            {

                SqlParameter[] paramlist = new SqlParameter[0];
                Dictionary<string, string> CityList = new Dictionary<string, string>();
                using (DataTable dtCity = DBGateway.FillDataTableSP("BKE_HTL_HOTEL_CITY_LIST", paramlist))
                {
                    if (dtCity != null)
                    {
                        foreach (DataRow data in dtCity.Rows)
                        {
                            if (data["CityId"] != DBNull.Value && data["CityName"] != DBNull.Value)
                            {
                                CityList.Add(Convert.ToString(data["CityId"]), Convert.ToString(data["CityName"]));
                            }
                        }
                    }
                }
                return CityList;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Exception at LoadCity(),reason is:" + ex.ToString(), "");
                return null;
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static object LoadCreditCard()
        {
            DataTable dtAgents = new DataTable();
            try
            {
                dtAgents = CreditCardMaster.GetList(ListStatus.Short, RecordStatus.Activated); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
                return JsonConvert.SerializeObject(dtAgents);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "LoadCreditCard web service" + Convert.ToString(ex.GetBaseException()), "");
                return JsonConvert.SerializeObject(dtAgents);
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static object LoadTransferType()
        {
            DataTable dtTransferType = new DataTable();
            DataSet dtTransfer = new DataSet();
            try
            {
                dtTransfer = UserMaster.GetMemberTypeList("bank_transfer_type"); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
                dtTransferType = dtTransfer.Tables[0];
                return JsonConvert.SerializeObject(dtTransferType);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "LoadTransferType web service" + Convert.ToString(ex.GetBaseException()), "");
                return JsonConvert.SerializeObject(dtTransferType);
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static object LoadBankData()
        {
            DataTable dtBank = new DataTable();
            try
            {
                dtBank = UserMaster.GetBankDetails("A"); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
                return JsonConvert.SerializeObject(dtBank);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "LoadBankData web service" + Convert.ToString(ex.GetBaseException()), "");
                return JsonConvert.SerializeObject(dtBank);
            }

        }
    }
}
