﻿using System;
using System.Data;
using System.Collections.Generic;
using CT.Core;
using Visa;
using CT.TicketReceipt.BusinessLayer;

public partial class ViewUploadedVisaDocuments : System.Web.UI.Page
{

    protected string message = string.Empty;
    protected bool isAdmin = false;
    protected int visaId = 0;
    protected string siteName;

    protected List<DataRow[]> listOfVisaBookings = new List<DataRow[]>();
    protected DataTable visaDT;
    protected string B2CSiteUrl;
    protected void Page_Load(object sender, EventArgs e)
    {
        //Audit.Add(EventType.Visa, Severity.High, 1, "0000", "");
        int paxId=0;
        int documentId=0;
        AuthorizationCheck();       
        B2CSiteUrl = "";//ConfigurationManager.AppSettings["B2CSiteUrl"];
        siteName = Convert.ToString(CT.Configuration.ConfigurationSystem.VisaConfig["SiteName"]);
        if (Request.QueryString.Count < 2)
        {
            Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
            Response.End();
        }
        else
        {
            bool isnumericePaxId = int.TryParse(Request.QueryString["paxId"].Trim(), out paxId);
            bool isnumericeDocId = int.TryParse(Request.QueryString["documentId"].Trim(), out documentId);

            if (!isnumericePaxId || !isnumericeDocId)
            {
                Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                Response.End();
            }

            if (Request["paxId"] != null && Request["paxId"] != "0")
            {
                try
                {
                    VisaPassenger visaPax = new VisaPassenger(paxId);
                    string rootFolder = string.Empty;
                    if (!string.IsNullOrEmpty(CT.Configuration.ConfigurationSystem.VisaConfig["RootFolder"]))
                    {
                        rootFolder = CT.Configuration.ConfigurationSystem.VisaConfig["RootFolder"].ToString();
                    }


                    //  B2CSiteUrl = "http://" + UserPreference.GetPreferenceList(visaPax.CreatedBy, ItemType.Flight)["SITENAME"] + "/CozmoWeb/";
                    B2CSiteUrl = Request.Url.Scheme+"://" + UserPreference.GetPreferenceList(visaPax.CreatedBy, ItemType.ALL)["SITENAME"] + "/" + rootFolder + "/";
                    visaDT = VisaQueue.GetUploadedDocuement(Convert.ToInt32(Request["paxId"]), Convert.ToInt32(Request["documentId"]));

                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Visa, Severity.High, 1, "Error: "+ex.ToString(), "");
                }
            }
        }
    }



    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
        //if (Session["roleId"] == null)
        //{
        //    String values = "?errMessage=Login Required to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
        //else if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.IssueFlightTickets) && !Role.IsAllowedTask((int)Session["roleId"], (int)Task.ViewBookingQueue))
        //{
        //    String values = "?errMessage=You are not authorised to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}

    }
}
