﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CT.Core;
using System.Data.SqlClient;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using System.Web.UI.WebControls;
using System.Data;
using System.Transactions;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Configuration;

namespace CozmoB2BWebApp
{
    public partial class BaggageInsuranceQueueGUI : CT.Core.ParentPage
    {
        PagedDataSource pagedData = new PagedDataSource();

        protected string pagingEnable = "style='display:block'";
        protected AgentMaster agency;
        protected int agentFilter = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    InitializeControls();
                    GetBaggageInsuranceQueue();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        private void InitializeControls()
        {
            try
            {
                BindAgency();
                int b2bAgentId;
                int b2b2bAgentId;
                if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
                {
                    ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                }
                else if (Settings.LoginInfo.AgentType == AgentType.Agent)
                {
                    ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                    ddlAgency.Enabled = false;
                }
                else if (Settings.LoginInfo.AgentType == AgentType.B2B)
                {
                    ddlAgency.Enabled = false;
                    b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                    ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);
                    ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                    ddlB2BAgent.Enabled = false;
                }
                else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
                {
                    ddlAgency.Enabled = false;
                    ddlB2BAgent.Enabled = false;
                    b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                    b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                    ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);
                    ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                    ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                    ddlB2B2BAgent.Enabled = false;
                }
                BindB2BAgent(Convert.ToInt32(ddlAgency.SelectedItem.Value));
                BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
                {
                    ddlB2B2BAgent.Enabled = false;
                }
                txtCreatedDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                

            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "Baggage INsurance :Clear() ", "0");
            }
        }
        private void BindAgency()
        {
            try
            {
                DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);// AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
                ddlAgency.DataSource = dtAgents;
                ddlAgency.DataTextField = "Agent_Name";
                ddlAgency.DataValueField = "agent_id";
                ddlAgency.DataBind();
                ddlAgency.Items.Insert(0, new ListItem("--All--", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindB2BAgent(int agentId)
        {
            try
            {
                DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
                ddlB2BAgent.DataSource = dtAgents;
                ddlB2BAgent.DataTextField = "Agent_Name";
                ddlB2BAgent.DataValueField = "agent_id";
                ddlB2BAgent.DataBind();
                ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
                ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindB2B2BAgent(int agentId)
        {
            try
            {
                DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
                ddlB2B2BAgent.DataSource = dtAgents;
                ddlB2B2BAgent.DataTextField = "Agent_Name";
                ddlB2B2BAgent.DataValueField = "agent_id";
                ddlB2B2BAgent.DataBind();
                ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
                ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GetBaggageInsuranceQueue();
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "Baggage INsurance :btnSearch_Click() ", "0");
            }

        }
        public void GetBaggageInsuranceQueue()
        {
            try
            {
                int agencyId = -1;//Convert.ToInt32(ddlAgency.SelectedItem.Value);
                string agentType = "";
                DateTime createdDate;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                if (txtCreatedDate.Text.Trim().Length > 0)
                {
                    createdDate = Convert.ToDateTime(txtCreatedDate.Text.Trim(), dateFormat);
                }
                else
                {
                    createdDate = Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"));
                }


                //agentFilter = Convert.ToInt32(ddlAgency.SelectedItem.Value);

                if (ddlB2B2BAgent.SelectedIndex > 0)
                {
                    agentType = "B2B2B";
                    agencyId = (ddlB2B2BAgent.SelectedIndex > 1 ? Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) : Convert.ToInt32(ddlAgency.SelectedItem.Value));
                }
                else if (ddlB2BAgent.SelectedIndex > 0)
                {
                    if (ddlB2BAgent.SelectedIndex == 1)
                    {
                        agentType = "AGENT";
                    }
                    else
                    {
                        agentType = "";
                    }
                    //if (ddlB2BAgent.SelectedIndex > 1 && ddlAgency.SelectedIndex > 1)
                    //{
                    //    agentType = "B2B";
                    //}
                    agencyId = (ddlB2BAgent.SelectedIndex > 1 ? Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) : Convert.ToInt32(ddlAgency.SelectedItem.Value));
                }
                
                else if(ddlAgency.SelectedIndex>1)
                {
                    if(Settings.LoginInfo.AgentType== AgentType.BaseAgent)
                    {
                        agentType = "BASE";
                    }
                    else
                    {
                        
                            agency = new AgentMaster(Convert.ToInt32(ddlAgency.SelectedItem.Value));
                            if (agency.AgentType == 2)
                            {
                                agentType = "";

                            }
                            agencyId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                        
                    }
                }



                //else if(ddlB2B2BAgent.SelectedIndex==1)
                //{
                //    //agentType = "";
                //    agencyId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                //}


                //if (agentFilter == 0)
                //{
                //    agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                //    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                //    {
                //        agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                //    }
                //    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                //    {
                //        agentType = string.Empty; // null Means binding in list all BOOKINGS
                //    }
                //}
                
                //if (agentFilter > 0 && ddlB2BAgent.SelectedIndex > 0)
                //{
                //    if (Convert.ToInt32(ddlAgency.SelectedItem.Value) > 1)
                //    {
                //        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                //        {
                //            agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                //        }
                //        else
                //        {
                //            agentFilter = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                //        }
                //    }
                //    else
                //    {
                //        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                //        {
                //            agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                //        }
                //        agentFilter = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                //    }
                //}
                //if (agentFilter > 0 && ddlB2B2BAgent.SelectedIndex > 0)
                //{
                //    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                //    {
                //        agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                //    }
                //    else
                //    {
                //        agentFilter = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                //    }
                //}
                //if (Convert.ToInt32(ddlAgency.SelectedItem.Value) != 0)
                //{
                //    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                //    {
                //        if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                //        {
                //            agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                //            agentFilter = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                //        }
                //    }
                //}

                string transType = string.Empty;
                if (Settings.LoginInfo.TransType == "B2B")
                {
                    ddlTransType.Visible = false;
                    lblTransType.Visible = false;
                    transType = "B2B";
                }
                else if (Settings.LoginInfo.TransType == "B2C")
                {
                    ddlTransType.Visible = false;
                    lblTransType.Visible = false;
                    transType = "B2C";
                }
                else
                {
                    ddlTransType.Visible = true;
                    lblTransType.Visible = true;
                    if (ddlTransType.SelectedItem.Value == "-1")
                    {
                        transType = null;
                    }
                    else
                    {
                        transType = ddlTransType.SelectedItem.Value;
                    }
                }
                
                BaggageInsuranceQueue obj = new BaggageInsuranceQueue();
                DataTable dtQueueList = obj.GetBaggageInsuranceQueueList(agencyId, createdDate, txtPNRno.Text, agentType, transType);
                Session["BaggageInsuranceQueueList"] = dtQueueList;
                dlBaggageInsQueue.DataSource = dtQueueList;
                dlBaggageInsQueue.DataBind();
                CurrentPage = 0;
                doPaging();
            }
            catch(Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "Baggage INsurance :GetBaggageInsuranceQueue() ", "0");
            }
        }

        #region Paging
        public int CurrentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    return 0;
                }
                else
                {
                    return (int)ViewState["_currentPage"];
                }
            }

            set
            {
                ViewState["_currentPage"] = value;
            }
        }
        void doPaging()
        {
            DataTable dt = (DataTable)Session["BaggageInsuranceQueueList"];
            pagedData.DataSource = dt.DefaultView;
            pagedData.AllowPaging = true;
            pagedData.PageSize = 5;
            Session["count"] = pagedData.PageCount - 1;
            pagedData.CurrentPageIndex = CurrentPage;
            btnPrev.Visible = (!pagedData.IsFirstPage);
            btnFirst.Visible = (!pagedData.IsFirstPage);
            btnNext.Visible = (!pagedData.IsLastPage);
            btnLast.Visible = (!pagedData.IsLastPage);
            lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
            DataView dView = (DataView)pagedData.DataSource;
            DataTable dTable;
            dTable = (DataTable)dView.Table;
            dlBaggageInsQueue.DataSource = pagedData;
            dlBaggageInsQueue.DataBind();
            if (dt.Rows.Count <= 0)
            {
                pagingEnable = "style='display:none'";
            }
        }
        protected void btnPrev_Click(object sender, EventArgs e)
        {
            CurrentPage--;
            doPaging();
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            CurrentPage++;
            doPaging();
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 0;
            doPaging();
        }
        protected void btnLast_Click(object sender, EventArgs e)
        {
            CurrentPage = (int)Session["count"];
            doPaging();
        }
        #endregion
        private void Clear()
        {
            try
            {
                txtCreatedDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                txtPNRno.Text = string.Empty;
                ddlB2B2BAgent.SelectedIndex = -1;
                ddlB2BAgent.SelectedIndex = -1;
                ddlTransType.SelectedIndex = 0;
                ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                Session["BaggageInsuranceQueueList"] = null;
                GetBaggageInsuranceQueue();
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "Baggage INsurance :Clear() ", "0");
            }
        }
        protected void dlBaggageInsQueue_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                {
                    Label lblViewBooking = e.Item.FindControl("lblViewBooking") as Label;
                    Label lblViewInvoice = e.Item.FindControl("lblViewInvoice") as Label;
                    HtmlTable tblChangeRequest = e.Item.FindControl("tblChangeRequest") as HtmlTable;
                    LinkButton lnkPayment = e.Item.FindControl("lnkPayment") as LinkButton;
                    Button btnRequest = e.Item.FindControl("btnRequest") as Button;
                    Label lblTotal = e.Item.FindControl("lblTotal") as Label;
                    Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                    DataRowView view = e.Item.DataItem as DataRowView;
                    int id = Convert.ToInt32(view["BI_ID"]);
                    int agentId = Convert.ToInt32(view["BI_AGENT_ID"]);
                    decimal totalAmount = Convert.ToDecimal(view["BI_TOTAL_PRICE"]);
                    //decimal serviceFee = Convert.ToDecimal(view["PLAN_SERVICE_FEE"]);
                    decimal markup = Convert.ToDecimal(view["PAX_MarkUp"]);
                    decimal discount = Convert.ToDecimal(view["PAX_Discount"]);
                    decimal gst = Convert.ToDecimal(view["PAX_GST"]);
                    decimal inputVAT = Convert.ToDecimal(view["PAX_INPUTVAT"]);
                    decimal outputVAT = Convert.ToDecimal(view["PAX_OUTPUTVAT"]);
                    bool status = Convert.ToBoolean(view["BI_STATUS"]);
                    agency = new AgentMaster(agentId);
                    
                    //totalAmount = totalAmount +  markup + gst + inputVAT + outputVAT - discount;

                    lblStatus.Text = view["ProposalState"].ToString();
                    lblStatus.ForeColor = System.Drawing.Color.LimeGreen;
                    AgentMaster agent = new AgentMaster(agentId);
                    lblTotal.Text = agent.AgentCurrency + " " + totalAmount.ToString("N" + agent.DecimalValue);
                    BaggageInsuranceQueue obj = new BaggageInsuranceQueue();
                    DataTable dtPlans = obj.GetPlanDetailsById(id);
                    HtmlTable divPlans = e.Item.FindControl("divPlans") as HtmlTable;
                   
                    try
                    {
                       
                        if (dtPlans != null && dtPlans.Rows.Count > 0)
                        {
                            HtmlTableRow row = new HtmlTableRow();

                            //binding Plans Showing purpose
                            HtmlTableCell cellPlanTitle = new HtmlTableCell();
                            //divPlanTitle.Attributes.Add("class", "col-md-2 col-xs-6");
                            cellPlanTitle.InnerHtml = "<b>Plan Title</b>";
                            row.Controls.Add(cellPlanTitle);

                            HtmlTableCell cellPlanDescription = new HtmlTableCell();
                            cellPlanDescription.InnerHtml = "<b>Plan Description</b>";
                            row.Controls.Add(cellPlanDescription);

                            HtmlTableCell cellPlanPolicy = new HtmlTableCell();
                            cellPlanPolicy.InnerHtml = "<b>Policy No<b>";
                            row.Controls.Add(cellPlanPolicy);

                            HtmlTableCell cellPlanStatus = new HtmlTableCell();
                            cellPlanStatus.InnerHtml = "<b>Plan Status</b>";
                            row.Controls.Add(cellPlanStatus);

                            HtmlTableCell cellEmail = new HtmlTableCell();
                            cellEmail.InnerHtml = "<b>Email</b>";
                            row.Controls.Add(cellEmail);

                            HtmlTableCell Phone = new HtmlTableCell();
                            Phone.InnerHtml = "<b>Phone</b>";
                            row.Controls.Add(Phone);

                            divPlans.Controls.Add(row);

                            foreach (DataRow dr in dtPlans.Rows)
                            {
                                HtmlTableRow tr = new HtmlTableRow();
                                HtmlTableCell cellPlanTitleValue = new HtmlTableCell();
                                Label lblPlanTitle = new Label();
                                lblPlanTitle.Text = dr["PLAN_TITLE"].ToString();
                                cellPlanTitleValue.Controls.Add(lblPlanTitle);
                                tr.Controls.Add(cellPlanTitleValue);

                                //Plan Name
                                HtmlTableCell cellPlanDescValue = new HtmlTableCell();
                                //divPlanDescValue.Attributes.Add("class", "col-md-2 col-xs-6");
                                Label lblPlanDesc = new Label();
                                lblPlanDesc.Text = dr["PLAN_DESCRIPTION"].ToString();
                                cellPlanDescValue.Controls.Add(lblPlanDesc);
                                tr.Controls.Add(cellPlanDescValue);


                                //Plan Name
                                HtmlTableCell cellPlanNameValue = new HtmlTableCell();
                                Label lblPlanName = new Label();
                                lblPlanName.Text = dr["PolicyNo"].ToString();
                                cellPlanNameValue.Controls.Add(lblPlanName);
                                tr.Controls.Add(cellPlanNameValue);

                                //Plan Status
                                HtmlTableCell cellPlanStatusValue = new HtmlTableCell();
                                Label lblPlanStatus = new Label();
                                lblPlanStatus.Text = lblStatus.Text;
                                cellPlanStatusValue.Controls.Add(lblPlanStatus);
                                tr.Controls.Add(cellPlanStatusValue);

                                // Email
                                HtmlTableCell cellEmailValue = new HtmlTableCell();
                                
                                Label lblEmail = new Label();
                                lblEmail.Text = dr["Email"].ToString();
                                cellEmailValue.Controls.Add(lblEmail);
                                tr.Controls.Add(cellEmailValue);

                                //Plan Status
                                HtmlTableCell cellPhoneValue = new HtmlTableCell();
                               
                                Label lblphone = new Label();
                                lblphone.Text = dr["PhoneNo"].ToString();
                                cellPhoneValue.Controls.Add(lblphone);
                                tr.Controls.Add(cellPhoneValue);

                                //TableCell cellClear = new TableCell("Div");
                                //cellClear.Attributes.Add("class", "clearfix");

                                divPlans.Controls.Add(tr);

                            }

                        }
                    }
                    catch(Exception ex) { }
                    IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                     string state = view["ProposalState"].ToString();
                    
                    lblViewBooking.Text = "<input id='Open-" + e.Item.ItemIndex + "' type='button' class='button' value='Open' onclick=\"ViewBooking(" + id + ")\"  />";
                        lblStatus.ForeColor = System.Drawing.Color.LimeGreen;
                    
                    if (state != "CANCELLED" )
                    {
                        //if (travelDate.CompareTo(Convert.ToDateTime(DateTime.Now, dateFormat).Date) > 0 && state != "CANCELLED" && ChangeRequest)
                        //{
                            tblChangeRequest.Visible = true;
                            btnRequest.OnClientClick = "return CancelInsPlan('" + e.Item.ItemIndex + "');";
                        //}
                    }
                    
                    lblStatus.Text = state;
                    if (state == "CANCELREQUEST")
                    {
                        lblStatus.Text = "REQUESTED FOR CANCEL";
                        lblStatus.ForeColor = System.Drawing.Color.Gray;
                        tblChangeRequest.Visible = false;
                        //lblViewBooking.Text = "<input id='Open-" + e.Item.ItemIndex + "' type='button' class='button' value='Open' onclick=\"ViewBooking(" + id + ")\"  />";
                    }
                    if (lblStatus.Text == "CANCELLED")
                    {
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                    }
                    lblViewInvoice.Text = "<input id='ViewInvoice-" + e.Item.ItemIndex + "' type='button' class='button' value='View Invoice' onclick=\"ViewInvoice(" + id + ")\"  />";
                    //string TransType = view["TransType"].ToString();

                    //if (TransType == "B2C")
                    //{
                    //    lnkPayment.Visible = true;
                    //    lnkPayment.OnClientClick = "return ViewPaymentInfo('" + e.Item.ItemIndex + "','" + Convert.ToInt32(view["Ins_Id"]) + "');";
                    //}
                    //CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, "ziyad", "0");
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "dlInsQueue_ItemDataBound ", "0");
            }

        }
        protected void dlBaggageInsQueue_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "InsChangeRequest")
            {
                if (e.CommandArgument.ToString().Length > 0)
                {
                    int IPHId = Convert.ToInt32(e.CommandArgument);
                    IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                    BaggageInsuranceHeader header = new BaggageInsuranceHeader();
                    header.LoadBaggageHeader(IPHId);
                    DropDownList ddlChangeRequestType = e.Item.FindControl("ddlChangeRequestType") as DropDownList;
                    TextBox txtRemarks = e.Item.FindControl("txtRemarks") as TextBox;
                    CheckBoxList chkPlans = e.Item.FindControl("chkPlans") as CheckBoxList;
                    ServiceRequest serviceRequest = new ServiceRequest();
                    int requestTypeId = 3;
                    AgentMaster Agency = new AgentMaster(header.AgentID);
                    try
                    {
                        int selectedCount = 0;
                        string planTitle = string.Empty;
                        string policyNo = string.Empty;
                        bool updateHeaderStatus = true;
                        if (header != null )
                        {
                            
                                if (IPHId>0)
                                {
                                    
                                            selectedCount++;
                                            serviceRequest.BookingId = 0;
                                            serviceRequest.ReferenceId = Convert.ToInt32(header.Bid);
                                            serviceRequest.ProductType = ProductType.BaggageInsurance;
                                            serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
                                            serviceRequest.Data = txtRemarks.Text;
                                            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;
                                            serviceRequest.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                                            serviceRequest.AgencyId = Settings.LoginInfo.AgentId;
                                            //serviceRequest.IsDomestic = false;
                                            serviceRequest.PaxName = header.FirstName + header.LastName;
                                            serviceRequest.Pnr = header.PNR;
                                            
                                            serviceRequest.Source = (int)BookingSource.Baggage;
                                            serviceRequest.StartDate = Convert.ToDateTime(txtCreatedDate.Text.Trim(), dateFormat); //Convert.ToDateTime(DateTime.Now.AddDays(-Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["INS_CANCEL_DATE"])));
                                            serviceRequest.SupplierName = "Tune Protect";
                                            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
                                            serviceRequest.ReferenceNumber =header.Bid.ToString();
                                            serviceRequest.AgencyId = Settings.LoginInfo.AgentId; 
                                            serviceRequest.ItemTypeId = InvoiceItemTypeId.BaggageInsuranceBooking;
                                            serviceRequest.DocName = "";
                                            serviceRequest.LastModifiedBy = serviceRequest.CreatedBy;
                                            serviceRequest.LastModifiedOn = DateTime.Now;
                                            serviceRequest.CreatedOn = DateTime.Now;
                            
                                            serviceRequest.AgencyTypeId = CT.Core.Agencytype.Cash;
                                            serviceRequest.RequestSourceId = RequestSource.BookingAPI;

                                            serviceRequest.Save();

                                            if (!string.IsNullOrEmpty(planTitle))
                                            {
                                                planTitle = planTitle + "," + header.PlanTitle;
                                            }
                                            else
                                            {
                                                planTitle = header.PlanTitle;
                                            }
                                            if (!string.IsNullOrEmpty(policyNo))
                                            {
                                                policyNo = policyNo + "," + header.Policy;
                                            }
                                            else
                                            {
                                                policyNo = header.Policy;
                                            }
                                            //Updating status
                                            BaggageInsuranceHeader.Update(header.Bid, (int)BaggageInsuranceBookingStatus.CancelRequest, Convert.ToInt32(Settings.LoginInfo.UserID));
                                      
                                }
                                //else if (chkPlan.Selected)
                                //{
                                //    updateHeaderStatus = false;
                                //    selectedCount++;
                                //}
                            
                            if (updateHeaderStatus) //First time only changing status
                            {
                               // BaggageInsuranceHeader.UpdateInsuranceHeaderStatus(header.Id, (int)BaggageInsuranceBookingStatus.CancelRequest);
                            }
                            //else
                            //{
                            //    InsuranceHeader.UpdateInsuranceHeaderStatus(header.Id, (int)InsuranceBookingStatus.PartialCancelled);
                            //}
                            //Sending email.
                            Hashtable table = new Hashtable(3);
                            table.Add("agentName", Agency.Name);
                            table.Add("policyName", planTitle);
                            table.Add("policyNo", policyNo);

                            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                            UserMaster bookedUser = new UserMaster(header.CreatedBy);
                            AgentMaster bookedAgency = new AgentMaster(header.AgentID);
                            toArray.Add(bookedUser.Email);
                            toArray.Add(bookedAgency.Email1);
                            toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["INS_CANCEL_MAIL"]))
                            {
                                string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["INS_CANCEL_MAIL"]).Split(';');
                                foreach (string cnMail in cancelMails)
                                {
                                    toArray.Add(cnMail);
                                }
                            }

                            string message = ConfigurationManager.AppSettings["INSURANCE_CHANGE_REQUEST"]; //"Your request for cancelling Insurance plan <b>" + plan.Title + " </b> is under process. Policy No:(" + plan.PolicyNo + ")";
                            try
                            {
                                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Insurance) Request for cancellation. Policy No:(" + policyNo + ")", message, table);
                            }
                            catch
                            {
                            }
                            GetBaggageInsuranceQueue();  //Response.Redirect("BaggageInsuranceQueue.aspx", false);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, 1, "Exception in the Baggage Insurance Request. Message:" + ex.Message, "");
                    }
                }
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "Baggage Request Queue calling clear event", "0");
            }
        }

        protected void ddlAgency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
                BindB2BAgent(agentId);
                BindB2B2BAgent(agentId);
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "Agency selected Event", "0");
            }
        }

        protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId >= 0)
                {
                    BindB2B2BAgent(agentId);
                    ddlB2B2BAgent.Enabled = true;
                }
                else
                {
                    ddlB2B2BAgent.SelectedIndex = 0;
                    ddlB2B2BAgent.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "ddlB2BAgent selected Event", "0");

            }
        }
    }
}
