﻿<%@ Application Language="C#" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="CozmoB2BWebApp" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Diagnostics" %>
<script runat="server">
    public Trie<string> trieForAgencyCustomers = new Trie<string>();
    public Trie<HotelCity> trieForHotelCity = new Trie<HotelCity>();
    //public Trie<IRCTCStation> trieForTrainStation = new Trie<IRCTCStation>();
    public Trie<GTACity> trie = new Trie<GTACity>();
  //  public Trie<airport> trie1 = new Trie<airport>();// Air port List
    public Trie<GTACity> trie2 = new Trie<GTACity>();
    public Trie<airport> trie3 = new Trie<airport>(); // not using
    public Trie<City> trie4 = new Trie<City>();// Hotel City
    public Trie<City> trie5 = new Trie<City>();//Gimmonix Hotel City
    public System.Collections.Generic.List<string> topInterDestCityList = new System.Collections.Generic.List<string>();
    public System.Collections.Generic.Dictionary<string, string> transferLocList = new System.Collections.Generic.Dictionary<string, string>();
    public System.Collections.Generic.Dictionary<string,System.Collections.Generic.List<HotelCity>> hotelCityList = new System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<HotelCity>>();
    public System.Collections.Generic.Dictionary<string, System.Collections.Generic.Dictionary<string, string>> MobileOperatorList = new System.Collections.Generic.Dictionary<string, System.Collections.Generic.Dictionary<string, string>>();
    //static string configPath = "C:\\configFiles\\";
    static string configPath = ConfigurationManager.AppSettings["configFiles"];//"C:\\configFiles\\";
    void Application_Start(object sender, EventArgs e)
    {
        // for url rewriting demo
        //RegisterRoutes(RouteTable.Routes); 
        // Code that runs on application startup
        CT.BookingEngine.GTACity gta = new CT.BookingEngine.GTACity();
        Application["GTATopDestinations"] = gta.GetGTATopDestination();

        Application["SabreAuthenticateToken"] = "";
        // added bangar for Getting sectorlist

        //try
        //{
        //    Application["HOTEL_STATIC_DATA"] = HotelStaticData.GetStaticDataCityDates();
        //}
        //catch (Exception ex)
        //{
        //    Audit.Add(EventType.Exception, Severity.High, 0, "Exception in Application_Start() Stacktrace " + ex.StackTrace + " Message " + ex.Message, "");
        //}

        try
        {
            //System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
            GTACity cityInfo = new GTACity();
            topInterDestCityList = cityInfo.GetGTATopDestination();
            //TransferRequest transferReq = new TransferRequest();
            //transferLocList = transferReq.GetTransferLocation();
            #region Code for Airport Trie
            //Trie<int>.
            int index = 0;
            string searchKey;
            //static Trie<int> trie = new Trie<int>();telseHOTELSE
            //XmlTextReader reader = new XmlTextReader("airportList.xml");
            System.Xml.XmlDocument docAirport= new System.Xml.XmlDocument();
            System.Xml.XmlDocument document1 = new System.Xml.XmlDocument();
            System.Xml.XmlDocument GTACodes = new System.Xml.XmlDocument();
            System.Xml.XmlDocument GTAAirportCodes = new System.Xml.XmlDocument();

            docAirport.Load(configPath + "airportList.xml");// For airlport - origin ,destination
            document1.Load(configPath + "DomesticHotelCode.xml");
            GTAAirportCodes.Load(configPath + "GTAAirportCode.xml");
            GTACodes.Load(configPath + "GTACodes.xml"); // not using
            

            System.Xml.XmlNodeList airport = docAirport.GetElementsByTagName("airport");
            System.Xml.XmlNodeList domesticHotel = document1.GetElementsByTagName("city");
            System.Xml.XmlNodeList hotelCode = GTACodes.GetElementsByTagName("city");
            System.Xml.XmlNodeList gtaAirport = GTAAirportCodes.GetElementsByTagName("row");
            string cName = string.Empty;
            string cCode = string.Empty;
            string emptyString = string.Empty;
            string addKey = string.Empty;

            foreach (System.Xml.XmlNode node in hotelCode)
            {
                GTACity city = new GTACity();
                foreach (System.Xml.XmlNode chNode in node.ChildNodes)
                {

                    if (chNode.Name == "index")
                    {
                        city.Index = Convert.ToInt32(chNode.InnerText);
                    }

                    if (chNode.Name == "cityname")
                    {
                        city.CityName = chNode.InnerText;
                    }
                    if (chNode.Name == "citycode")
                    {
                        city.CityCode = chNode.InnerText;
                    }
                    if (chNode.Name == "countryname")
                    {
                        city.CountryName = chNode.InnerText;
                    }
                }
                foreach (System.Xml.XmlNode childNode in node.ChildNodes)
                {
                    if (childNode.Name != "index" && childNode.Name != "countryname")
                    {
                        searchKey = childNode.InnerText.ToLower().Trim();
                        searchKey = searchKey.Replace(" ", "");
                        searchKey = searchKey.Replace(".", "");
                        searchKey = searchKey.Replace("'", "");
                        searchKey = searchKey.Replace("’", "");
                        searchKey = searchKey.Replace("÷", "");
                        searchKey = searchKey.Replace("-", "");
                        searchKey = searchKey.Replace("(", "");
                        searchKey = searchKey.Replace(")", "");
                        searchKey = searchKey.Replace("/", "");
                        searchKey = searchKey.Replace("±", "");
                        searchKey = searchKey.Replace("é", "e");
                        searchKey = searchKey.Replace(",", "");
                        searchKey = searchKey.Replace("_", "");
                        addKey = searchKey;

                        if (childNode.Name == "citycode")
                        {
                            cCode = addKey;
                            trie2.Add(addKey, city);

                        }
                        if (childNode.Name == "cityname")
                        {
                            if (addKey != cCode)
                            {
                                cName = addKey;
                                try
                                {
                                    trie2.Add(addKey, city);
                                }
                                catch (Exception ex)
                                {
                                    Response.Write(ex.Message);
                                }

                            }
                            cCode = emptyString;
                            cName = emptyString;
                        }


                    }
                }
            }


            foreach (System.Xml.XmlNode node in domesticHotel)
            {
                GTACity domestic = new GTACity();
                foreach (System.Xml.XmlNode chNode in node.ChildNodes)
                {

                    if (chNode.Name == "index")
                    {
                        domestic.Index = Convert.ToInt32(chNode.InnerText);
                    }

                    if (chNode.Name == "cityname")
                    {
                        domestic.CityName = chNode.InnerText;
                    }
                    if (chNode.Name == "citycode")
                    {
                        domestic.CityCode = chNode.InnerText;
                    }
                    if (chNode.Name == "countryname")
                    {
                        domestic.CountryName = chNode.InnerText;
                    }
                }

                foreach (System.Xml.XmlNode childNode in node.ChildNodes)
                {
                    if (childNode.Name == "index")
                    {
                        index = Convert.ToInt32(childNode.InnerText);
                        if (index == 782)
                        {
                            //int p;
                        }
                    }
                    if (childNode.Name != "index" && childNode.Name != "countryname")
                    {
                        searchKey = childNode.InnerText.ToLower().Trim();
                        searchKey = searchKey.Replace(" ", "");
                        searchKey = searchKey.Replace(".", "");
                        searchKey = searchKey.Replace("'", "");
                        searchKey = searchKey.Replace("’", "");
                        searchKey = searchKey.Replace("÷", "");
                        searchKey = searchKey.Replace("-", "");
                        searchKey = searchKey.Replace("(", "");
                        searchKey = searchKey.Replace(")", "");
                        searchKey = searchKey.Replace("/", "");
                        searchKey = searchKey.Replace("±", "");
                        searchKey = searchKey.Replace("é", "e");
                        addKey = searchKey;
                        if (childNode.Name == "citycode")
                        {
                            cCode = addKey;
                            trie.Add(addKey, domestic);

                        }
                        if (childNode.Name == "cityname")
                        {
                            if (addKey != cCode)
                            {
                                cName = addKey;
                                trie.Add(addKey, domestic);
                            }
                            cCode = emptyString;
                            cName = emptyString;
                        }


                    }
                }
            }


            System.Collections.Generic.List<string> countryList = new System.Collections.Generic.List<string>();
            HotelCity cityData = new HotelCity();
            countryList = cityData.GetAllHotelCountries();
            System.Collections.Generic.List<HotelCity> cityList = new System.Collections.Generic.List<HotelCity>();
            try
            {
                foreach (string country in countryList)
                {
                    cityList = cityData.GetHotelCities(country);
                    hotelCityList.Add(country, cityList);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            City domestic1 = new City();
            HotelCity hotelcity = new HotelCity();
            System.Collections.Generic.List<string> HotelCityList = hotelcity.GetAllHotelCities();
            System.Text.RegularExpressions.Regex regexBus = new System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]");
            for (int i = 0; i < HotelCityList.Count; i++)
            {
                domestic1 = new City();
                string city = HotelCityList[i];
                string cityNameKey = HotelCityList[i].Split('|')[1];

                domestic1.Index = Convert.ToInt32(HotelCityList[i].Split('|')[0]);
                domestic1.CityCode = HotelCityList[i].Split('|')[0];
                domestic1.CityName = HotelCityList[i].Split('|')[1];
                domestic1.StateProvince = HotelCityList[i].Split('|')[2];
                domestic1.CountryName = HotelCityList[i].Split('|')[3];



                cityNameKey = cityNameKey.Replace(" ", "");
                if (regexBus.IsMatch(cityNameKey))
                {
                    continue;
                }
                cityNameKey = cityNameKey.Replace(".", "");
                cityNameKey = cityNameKey.Replace("'", "");
                cityNameKey = cityNameKey.Replace("’", "");
                cityNameKey = cityNameKey.Replace("÷", "");
                cityNameKey = cityNameKey.Replace("-", "");
                cityNameKey = cityNameKey.Replace("(", "");
                cityNameKey = cityNameKey.Replace(")", "");
                cityNameKey = cityNameKey.Replace("/", "");
                cityNameKey = cityNameKey.Replace("±", "");
                cityNameKey = cityNameKey.Replace("é", "e");

                trie4.Add(cityNameKey.ToLower(), domestic1);
            }

            #region for Gimmonix HotelCities
            System.Collections.Generic.List<string> GimmonixHotelCityList = hotelcity.GetAllHotelCitiesForGimmonix();
            for (int i = 0; i < GimmonixHotelCityList.Count; i++)
            {
                domestic1 = new City();
                string city = GimmonixHotelCityList[i];
                string cityNameKey = GimmonixHotelCityList[i].Split('|')[1].Split(',')[0];

                domestic1.Index = Convert.ToInt32(GimmonixHotelCityList[i].Split('|')[0]);
                //for checking cityname having multiple names
                if(GimmonixHotelCityList[i].Split('|')[1].Split(',').Length>2)
                {
                    string[] multiplecities = GimmonixHotelCityList[i].Split('|')[1].Split(',');
                    domestic1.CountryName = GimmonixHotelCityList[i].Split('|')[1].Split(',')[multiplecities.Length-1];
                    domestic1.CityName = string.Join(",", multiplecities.ToList().GetRange(0, multiplecities.Length - 1).ToArray());
                }
                else {
                    domestic1.CountryName = GimmonixHotelCityList[i].Split('|')[1].Split(',')[1];
                     domestic1.CityName = GimmonixHotelCityList[i].Split('|')[1].Split(',')[0];
                }
                cityNameKey = cityNameKey.Replace(" ", "");
                /// if (regexBus.IsMatch(cityNameKey))
                //  {
                //       continue;
                //   }
                cityNameKey = cityNameKey.Replace(".", "");
                cityNameKey = cityNameKey.Replace("'", "");
                cityNameKey = cityNameKey.Replace("’", "");
                cityNameKey = cityNameKey.Replace("÷", "");
                cityNameKey = cityNameKey.Replace("-", "");
                cityNameKey = cityNameKey.Replace("(", "");
                cityNameKey = cityNameKey.Replace(")", "");
                cityNameKey = cityNameKey.Replace("/", "");
                cityNameKey = cityNameKey.Replace("±", "");
                cityNameKey = cityNameKey.Replace("é", "e");
                if (regexBus.IsMatch(cityNameKey))
                {
                    continue;
                }
                trie5.Add(cityNameKey.ToLower(), domestic1);
            }
            #endregion
            this.Application.Add("tree4", trie4);
            this.Application.Add("HotelCityList", hotelCityList);
            this.Application.Add("HotelCountryList", countryList);

            this.Application.Add("tree", trie);

            this.Application.Add("tree2", trie2);
            this.Application.Add("GTATopDestinations", topInterDestCityList);
            this.Application.Add("TransferLocations", transferLocList);
            this.Application.Add("GimmonixCityList", trie5);

            // FOR FLIGHT AUTO COMPLETE

            //foreach (System.Xml.XmlNode node in airport)
            //{
            //    airport airPort = new airport();
            //    foreach (System.Xml.XmlNode chNode in node.ChildNodes)
            //    {
            //        if (chNode.Name == "airportname")
            //        {
            //            airPort.AirportName = chNode.InnerText;
            //            airPort.AirportName = airPort.AirportName.Replace("/", " ");
            //        }
            //        if (chNode.Name == "index")
            //        {
            //            airPort.Index = Convert.ToInt32(chNode.InnerText);
            //        }
            //        if (chNode.Name == "airportcode")
            //        {
            //            airPort.AirportCode = chNode.InnerText;
            //        }
            //        if (chNode.Name == "cityname")
            //        {
            //            airPort.CityName = chNode.InnerText;
            //            airPort.CityName = airPort.CityName.Replace("/", " ");
            //        }
            //        if (chNode.Name == "citycode")
            //        {
            //            airPort.CityCode = chNode.InnerText;
            //        }
            //        if (chNode.Name == "countryname")
            //        {
            //            airPort.CountryName = chNode.InnerText;
            //            airPort.CountryName = airPort.CountryName.Replace("/", " ");
            //        }
            //    }


            //    string aName = string.Empty;
            //    string aCode = string.Empty;
            //    //string cName = string.Empty;
            //    //string cCode = string.Empty;
            //    //string emptyString = string.Empty;
            //    //string addKey = string.Empty;
            //    foreach (System.Xml.XmlNode childNode in node.ChildNodes)
            //    {
            //        if (childNode.Name == "index")
            //        {
            //            index = Convert.ToInt32(childNode.InnerText);
            //            if (index == 782)
            //            {
            //                //int p;
            //            }
            //        }
            //        if (childNode.Name != "index" && childNode.Name != "countryname")
            //        {
            //            searchKey = childNode.InnerText.ToLower().Trim();
            //            searchKey = searchKey.Replace(" ", "");
            //            searchKey = searchKey.Replace(".", "");
            //            searchKey = searchKey.Replace("'", "");
            //            searchKey = searchKey.Replace("’", "");
            //            searchKey = searchKey.Replace("÷", "");
            //            searchKey = searchKey.Replace("-", "");
            //            searchKey = searchKey.Replace("(", "");
            //            searchKey = searchKey.Replace(")", "");
            //            searchKey = searchKey.Replace("/", "");
            //            searchKey = searchKey.Replace("±", "");
            //            searchKey = searchKey.Replace("é", "e");
            //            //addKey = searchKey.Substring(0, 3);
            //            addKey = searchKey;
            //            if (childNode.Name == "airportcode")
            //            {
            //                cCode = addKey;

            //                trie1.Add(addKey, airPort);
            //            }

            //            if (childNode.Name == "cityname")
            //            {
            //                if (addKey != aCode && addKey != aName && addKey != cCode)
            //                {
            //                    cName = addKey;
            //                    trie1.Add(addKey, airPort);
            //                }
            //                aCode = emptyString;
            //                aName = emptyString;
            //                cCode = emptyString;
            //                cName = emptyString;
            //            }


            //        }
            //    }
            //}
            this.Application.Add("tree1", Airport.GetAirportCodesList());

            //foreach (System.Xml.XmlNode node in airport)
            //{
            //    airport airPort = new airport();
            //    foreach (System.Xml.XmlNode chNode in node.ChildNodes)
            //    {
            //        if (chNode.Name == "index")
            //        {
            //            airPort.Index = Convert.ToInt32(chNode.InnerText);
            //        }

            //        if (chNode.Name == "cityname")
            //        {
            //            airPort.CityName = chNode.InnerText;
            //        }
            //        if (chNode.Name == "citycode")
            //        {
            //            airPort.CityCode = chNode.InnerText;
            //        }
            //        if (chNode.Name == "countryname")
            //        {
            //            airPort.CountryName = chNode.InnerText;
            //        }
            //    }

            //    foreach (System.Xml.XmlNode childNode in node.ChildNodes)
            //    {
            //        if (childNode.Name == "index")
            //        {
            //            index = Convert.ToInt32(childNode.InnerText);
            //            if (index == 782)
            //            {
            //                int p;
            //            }
            //        }
            //        if (childNode.Name != "index" && childNode.Name != "countryname")
            //        {
            //            searchKey = childNode.InnerText.ToLower().Trim();
            //            searchKey = searchKey.Replace(" ", "");
            //            searchKey = searchKey.Replace(".", "");
            //            searchKey = searchKey.Replace("'", "");
            //            searchKey = searchKey.Replace("’", "");
            //            searchKey = searchKey.Replace("÷", "");
            //            searchKey = searchKey.Replace("-", "");
            //            searchKey = searchKey.Replace("(", "");
            //            searchKey = searchKey.Replace(")", "");
            //            searchKey = searchKey.Replace("/", "");
            //            searchKey = searchKey.Replace("±", "");
            //            searchKey = searchKey.Replace("é", "e");
            //            addKey = searchKey;

            //            if (childNode.Name == "citycode")
            //            {
            //                cCode = addKey;
            //                trie1.Add(addKey, airPort);

            //            }
            //            if (childNode.Name == "cityname")
            //            {
            //                if (addKey != cCode)
            //                {
            //                    cName = addKey;
            //                    trie1.Add(addKey, airPort);
            //                }
            //                cCode = emptyString;
            //                cName = emptyString;
            //            }
            //        }
            //    }
            //}
            //this.Application.Add("tree1", trie1);

            foreach (System.Xml.XmlNode node in gtaAirport)// not using
            {
                airport airPort = new airport();
                foreach (System.Xml.XmlNode chNode in node.ChildNodes)
                {
                    if (chNode.Name == "index")
                    {
                        airPort.Index = Convert.ToInt32(chNode.InnerText);
                    }
                    if (chNode.Name == "airportcode")
                    {
                        airPort.AirportCode = chNode.InnerText;
                    }
                    if (chNode.Name == "airportname")
                    {
                        airPort.AirportName = chNode.InnerText;
                    }
                    if (chNode.Name == "cityname")
                    {
                        airPort.CityName = chNode.InnerText;
                    }
                    if (chNode.Name == "citycode")
                    {
                        airPort.CityCode = chNode.InnerText;
                    }
                    if (chNode.Name == "countryname")
                    {
                        airPort.CountryName = chNode.InnerText;
                    }
                }

                foreach (System.Xml.XmlNode childNode in node.ChildNodes)
                {
                    if (childNode.Name == "index")
                    {
                        index = Convert.ToInt32(childNode.InnerText);
                        if (index == 782)
                        {
                            //int p;
                        }
                    }
                    if (childNode.Name != "index" && childNode.Name != "countryname")
                    {
                        searchKey = childNode.InnerText.ToLower().Trim();
                        searchKey = searchKey.Replace(" ", "");
                        searchKey = searchKey.Replace(".", "");
                        searchKey = searchKey.Replace("'", "");
                        searchKey = searchKey.Replace("’", "");
                        searchKey = searchKey.Replace("÷", "");
                        searchKey = searchKey.Replace("-", "");
                        searchKey = searchKey.Replace("(", "");
                        searchKey = searchKey.Replace(")", "");
                        searchKey = searchKey.Replace("/", "");
                        searchKey = searchKey.Replace("±", "");
                        searchKey = searchKey.Replace("é", "e");
                        addKey = searchKey;

                        if (childNode.Name == "airportcode")
                        {
                            cCode = addKey;
                            trie3.Add(addKey, airPort);

                        }
                        if (childNode.Name == "cityname")
                        {
                            if (addKey != cCode)
                            {
                                cName = addKey;
                                trie3.Add(addKey, airPort);
                            }
                            cCode = emptyString;
                            cName = emptyString;
                        }


                    }
                }
            }
            this.Application.Add("tree3", trie3);// not using
            //System.Text.RegularExpressions.Regex regexBus = new System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]");
            System.Collections.Generic.List<string> Airlines = AirlineInfo.GetAllAirlines(); // Preferred Air line
            Trie<AirlineInfo> trieAir = new Trie<AirlineInfo>();// // Preferred Air line

            foreach (string airline in Airlines)
            {
                AirlineInfo ali = new AirlineInfo();
                string airlineCode = airline.Split('|')[0];
                string airlineName = airline.Split('|')[1];

                airlineName = airlineName.Replace(" ", "");
                if (regexBus.IsMatch(airlineName))
                {
                    continue;
                }
                else
                {
                    airlineName = airlineName.Replace(".", "");
                    airlineName = airlineName.Replace("'", "");
                    airlineName = airlineName.Replace("’", "");
                    airlineName = airlineName.Replace("÷", "");
                    airlineName = airlineName.Replace("-", "");
                    airlineName = airlineName.Replace("(", "");
                    airlineName = airlineName.Replace(")", "");
                    airlineName = airlineName.Replace("/", "");
                    airlineName = airlineName.Replace("±", "");
                    airlineName = airlineName.Replace("é", "e");
                    ali.AirlineCode = airlineCode;
                    ali.AirlineName = airlineName;

                    trieAir.Add(airlineName.ToLower(), ali);
                }
            }
            this.Application.Add("tree5", trieAir);

            //// COde for Trie object for train station        
            //DataTable table = IRCTCStation.GetIRCTCStationList();
            //foreach (DataRow row in table.Rows)
            //{
            //    IRCTCStation station = new IRCTCStation();
            //    station.CityName = row["stationCity"].ToString().Trim();
            //    station.StationCode = row["stationCode"].ToString().Trim();
            //    station.StationName = row["stationName"].ToString().Trim();

            //    string cityNameKey = station.CityName.Replace(" ", "");
            //    cityNameKey = cityNameKey.Replace(".", "");
            //    cityNameKey = cityNameKey.Replace("'", "");
            //    cityNameKey = cityNameKey.Replace("’", "");
            //    cityNameKey = cityNameKey.Replace("÷", "");
            //    cityNameKey = cityNameKey.Replace("-", "");
            //    cityNameKey = cityNameKey.Replace("(", "");
            //    cityNameKey = cityNameKey.Replace(")", "");
            //    cityNameKey = cityNameKey.Replace("/", "");
            //    cityNameKey = cityNameKey.Replace("±", "");
            //    cityNameKey = cityNameKey.Replace("é", "e");

            //    string stationCodeKey = station.StationCode.Replace(" ", "");
            //    stationCodeKey = stationCodeKey.Replace(".", "");
            //    stationCodeKey = stationCodeKey.Replace("'", "");
            //    stationCodeKey = stationCodeKey.Replace("’", "");
            //    stationCodeKey = stationCodeKey.Replace("÷", "");
            //    stationCodeKey = stationCodeKey.Replace("-", "");
            //    stationCodeKey = stationCodeKey.Replace("(", "");
            //    stationCodeKey = stationCodeKey.Replace(")", "");
            //    stationCodeKey = stationCodeKey.Replace("/", "");
            //    stationCodeKey = stationCodeKey.Replace("±", "");
            //    stationCodeKey = stationCodeKey.Replace("é", "e");

            //    string stationNameKey = station.StationName.Replace(" ", "");
            //    stationNameKey = stationNameKey.Replace(".", "");
            //    stationNameKey = stationNameKey.Replace("'", "");
            //    stationNameKey = stationNameKey.Replace("’", "");
            //    stationNameKey = stationNameKey.Replace("÷", "");
            //    stationNameKey = stationNameKey.Replace("-", "");
            //    stationNameKey = stationNameKey.Replace("(", "");
            //    stationNameKey = stationNameKey.Replace(")", "");
            //    stationNameKey = stationNameKey.Replace("/", "");
            //    stationNameKey = stationNameKey.Replace("±", "");
            //    stationNameKey = stationNameKey.Replace("é", "e");

            //    trieForTrainStation.Add(cityNameKey.ToLower(), station);
            //    trieForTrainStation.Add(stationCodeKey.ToLower(), station);
            //    trieForTrainStation.Add(stationNameKey.ToLower(), station);
            //}
            //this.Application.Add("trieForTrainStation", trieForTrainStation);
            //COde for Trie object for train station endsde


            // COde for Trie object for Agency Customers AutoComplete       
            //DataTable agencyCustomers = AgencyCustomer.GetAllCustomersNameAndPhoneNo();
            //foreach (DataRow row in agencyCustomers.Rows)
            //{
            //    string agencyId = row["agencyId"].ToString();
            //    string firstName = row["firstName"].ToString().Trim();
            //    string lastName = row["lastName"].ToString().Trim();
            //    string phoneNo = row["phoneNo"].ToString().Trim();
            //    string customerId = row["customerId"].ToString().Trim();

            //    string firstNameKey = firstName.Replace(" ", "");
            //    firstNameKey = firstNameKey.Replace(".", "");
            //    firstNameKey = firstNameKey.Replace("'", "");
            //    firstNameKey = firstNameKey.Replace("’", "");
            //    firstNameKey = firstNameKey.Replace("÷", "");
            //    firstNameKey = firstNameKey.Replace("-", "");
            //    firstNameKey = firstNameKey.Replace("(", "");
            //    firstNameKey = firstNameKey.Replace(")", "");
            //    firstNameKey = firstNameKey.Replace("/", "");
            //    firstNameKey = firstNameKey.Replace("±", "");
            //    firstNameKey = firstNameKey.Replace("é", "e");
            //    firstNameKey = firstNameKey.Replace("_", "");

            //    string lastNameKey = lastName.Replace(" ", "");
            //    trieForAgencyCustomers.Add(agencyId + "A" + firstNameKey.ToLower(), firstName + " " + lastName + " (" + phoneNo + ") " + ";*;" + customerId);
            //}
            //this.Application.Add("trieForAgencyCustomers", trieForAgencyCustomers);
            //COde for Trie object for train station endsde

            #endregion

            HotelCity hotelCity = new HotelCity();
            System.Collections.Generic.List<HotelCity> listOfCities = hotelCity.GetHotelCities("india");
            for (int i = 0; i < listOfCities.Count; i++)
            {
                HotelCity hc = new HotelCity();
                hc.CityName = listOfCities[i].CityName;

                string cityNameKey = hc.CityName.Replace(" ", "");
                cityNameKey = cityNameKey.Replace(".", "");
                cityNameKey = cityNameKey.Replace("'", "");
                cityNameKey = cityNameKey.Replace("’", "");
                cityNameKey = cityNameKey.Replace("÷", "");
                cityNameKey = cityNameKey.Replace("-", "");
                cityNameKey = cityNameKey.Replace("(", "");
                cityNameKey = cityNameKey.Replace(")", "");
                cityNameKey = cityNameKey.Replace("/", "");
                cityNameKey = cityNameKey.Replace("±", "");
                cityNameKey = cityNameKey.Replace("é", "e");

                trieForHotelCity.Add(cityNameKey.ToLower(), hc);
            }
            this.Application.Add("trieForHotelCity", trieForHotelCity);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception in Application_Start() Stacktrace " + ex.StackTrace + " Message " + ex.Message, "");
        }
        finally
        {
            //CoreLogic.Role.InitializeTaskRole();
        }


    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    protected void Application_Error(object sender, EventArgs e)
    {
        // Get the error details
        HttpException lastErrorWrapper =
            Server.GetLastError() as HttpException;

        Exception lastError = lastErrorWrapper;
        if (lastError != null && lastErrorWrapper.InnerException != null)
            lastError = lastErrorWrapper.InnerException;

        string ces = "B2BErrorLog";
        try
        {
            EventLog.CreateEventSource(ces, "Application");

            if (!EventLog.SourceExists(ces))
                EventLog.CreateEventSource(ces, "Application");
        }
        catch { }


        EventLog.WriteEntry(ces, lastError.ToString(), EventLogEntryType.Error, 20, 10);
    }

    void Session_Start(object sender, EventArgs e)
    {// Code that runs when a new session is started
        //string sessionId = Session.SessionID;

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

    protected void Application_EndRequest(object sender, EventArgs e)
    {

    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {
        // Code that runs when a Application ends. 
        // Note: The Application_BeginRequest event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

        //Systems.Process.Model model = new Systems.Process.Model(Systems.Process.ProcessName.MS2);

        // For handling https since Our Server Fire Wall is passing as HTTP
        //var request = Context.Request;
        //    var response = Context.Response;
        // if (!request.IsSecureConnection )//&& !request.IsLocal)
        //    {
        //        var builder = new UriBuilder(request.Url)
        //        {
        //            Scheme = Uri.UriSchemeHttps,
        //            //Port = 443
        //        };

        //        response.Redirect(builder.Uri.ToString());
        //    }

    }
    /*protected void Application_PostMapRequestHandler(object sender, EventArgs e)
    {
        Page activePage = HttpContext.Current.Handler as Page;
        if (activePage == null)
        {
            return;
        }
        activePage.PreInit
            += (s, ea) =>
            {

                //string selectedTheme = HttpContext.Current.Session["SelectedTheme"] as string;
                //activePage.Theme = selectedTheme;

                string themeName = (string)HttpContext.Current.Session["themeName"];
                if (themeName != null)
                {
                    activePage.Theme = themeName;
                }
                else
                {
                    activePage.Theme = "Sata";
                }
                //if (Request.Form["ctl00$ddlTema"] != null)
                //{
                //    HttpContext.Current.Session["SelectedTheme"]
                //        = activePage.Theme = Request.Form["ctl00$ddlTema"];
                //}
                //else if (selectedTheme != null)
                //{
                //    activePage.Theme = selectedTheme;
                //}

            };
    }*/
    //private void SetRouting(RouteCollection routeCollection)
    //{
    //    routeCollection.Add("SalesRoute", new Route
    //(
    //     "SalesReport/{locale}/{year}/{*queryvalues}"
    //     , new SalesRouteHandler()
    //)
    //    {
    //        Constraints = new RouteValueDictionary { { "locale", "[a-z]{2}-[a-z]{2}" }, { "year", @"\d{4}" } },
    //        Defaults = new RouteValueDictionary { { "locale", "en-US" }, { "year", DateTime.Now.Year.ToString() } }
    //    });
    //    routeCollection.Add("ExpensesRoute", new Route
    //    (
    //         "ExpensesReport/{locale}/{year}/{*queryvalues}"
    //         , new ExpensesRouteHandler()
    //    )
    //    {
    //        Constraints = new RouteValueDictionary { { "locale", "[a-z]{2}-[a-z]{2}" }, { "year", @"\d{4}" } },
    //        Defaults = new RouteValueDictionary { { "locale", "en-US" }, { "year", DateTime.Now.Year.ToString() } }
    //    });
    //}

    void RegisterRoutes(RouteCollection routes)
    {
        routes.Add(new Route("language2", new UserRouteHandler("~/login.aspx")));
    }
</script>
