<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="AgentPaymentTransferUI" Title="Agent Payment Transfer" Codebehind="AgentPaymentTransfer.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<%--<div class="grdScrlTrans" style="margin-top:-2px;height:520px;border:solid0px;text-align:center" >--%>

<div class="body_container"> 


    <asp:HiddenField id="hdfDetailRowId" runat="server"></asp:HiddenField>
   
     <div class="paramcon" title="header">
       
       
       <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"><asp:Label ID="lblReceiptNo" Text="Receipt No:" runat="server"></asp:Label> </div>
    <div class="col-md-2"><asp:TextBox  ID="txtReceiptNo" runat="server" Enabled="false" CssClass="inputDisabled form-control"></asp:TextBox> </div>
    
    
    <div class="col-md-2 col-xs-3 martop_xs10"><asp:Label ID="lblDate" Text="Date:" runat="server"></asp:Label> </div>
    
     <div class="col-md-2 col-xs-8 martop_xs10"> <uc1:DateControl ID="dcDocDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true" DropDownYears="10" Enabled="false"  HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></div>
    



    <div class="clearfix"></div>
    </div>
       
       
       
                  <div class="col-md-12 padding-0 marbot_10">                               

       
    <div class="col-md-2"> <asp:Label ID="lblDocType" Text="Doc Type:" runat="server"></asp:Label></div>
    <div class="col-md-2"> <asp:DropDownList ID="ddlDocType"  runat="server" Enabled="true" CssClass="inputDdlEnabled form-control" >
                 <asp:ListItem Text ="TFC" Selected="True"  Value="TFC"></asp:ListItem>                
                </asp:DropDownList></div>
                
                
    <div class="col-md-2"> <asp:Label ID="lblDocBase" Text="Doc Base:" runat="server"></asp:Label></div>
     <div class="col-md-2"> <asp:TextBox  ID="txtDocBase" runat="server"  Text="TRA" Enabled="false" CssClass="inputDisabled form-control" ></asp:TextBox></div>
     
    <div class="col-md-2"> <asp:Label ID="lblPayMode" Text="Mode:" runat="server"></asp:Label></div>
     <div class="col-md-2"><asp:TextBox  ID="txtPayMode" runat="server"  Text="TRANSFER" Enabled="false" CssClass="inputDisabled form-control"></asp:TextBox> </div>


    <div class="clearfix"></div>
    </div>
    
    
             <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label ID="lblAgent" Text="Agent:" runat="server"></asp:Label></div>
    
    <div class="col-md-2"> <asp:DropDownList ID="ddlAgent"  runat="server" Enabled="true" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" AutoPostBack="true" CssClass="inputDdlEnabled form-control"></asp:DropDownList></div>
    
    <div class="col-md-2"> <asp:Label ID="lblAgentBalance" Text="Agent Balance:" runat="server"></asp:Label></div>
    
     <div class="col-md-2"> <asp:TextBox  ID="txtAgentBalance" runat="server" Enabled="false" CssClass="inputDisabled form-control"></asp:TextBox></div>
     
    <div class="col-md-2"> <asp:Label ID="lblAgentCurrency" Text="Currency:" runat="server"></asp:Label></div>
    
     <div class="col-md-2">
       <label class="pull-left"> 
     <asp:DropDownList Id="ddlAgentCurrency" Width="60px" Enabled="false" CssClass="inputDdlEnabled form-control" OnChange="setExchRate(this.value);"  runat="server" ></asp:DropDownList> </label>
     
     <label class="pull-left">   <asp:TextBox ID="txtAgentExchrate" Text="0.00"  Enabled="false" runat="server"   CssClass="inputDisabled form-control" Width="40px" ></asp:TextBox>
     </label>
     
     </div>


    <div class="clearfix"></div>
    </div>
    
    
       
       
                <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"><asp:Label ID="lblSubAgent" Text="SubAgent:" runat="server"></asp:Label> </div>
    
    <div class="col-md-2"><asp:DropDownList ID="ddlSubAgent"  runat="server" Enabled="true"  OnSelectedIndexChanged="ddlSubAgent_SelectedIndexChanged" AutoPostBack="true"  CssClass="inputDdlEnabled form-control"></asp:DropDownList> </div>
    
    
    <div class="col-md-2"><asp:Label ID="Label2" Text="SubAgent Balance:" runat="server"></asp:Label> </div>
    
     <div class="col-md-2"> <asp:TextBox  ID="txtSubAgentBalance" runat="server" Enabled="false" CssClass="inputDisabled form-control"></asp:TextBox></div>
    <div class="col-md-2"> <asp:Label ID="lblSubAgentCurrency" Text="Currency:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"> 
     <label class="pull-left"> 
     
     <asp:DropDownList Id="ddlSubAgentCurrency" Enabled="false" CssClass="inputDdlEnabled form-control" OnChange="setExchRate(this.value);" Width="60px"  runat="server" ></asp:DropDownList></label>
     
     <label class="pull-left">  <asp:TextBox ID="txtSubAgentExchRate" Text="0.00"  runat="server" Enabled="false" CssClass="inputDisabled form-control" Width="40px" ></asp:TextBox></label>
     
     
     </div>


    <div class="clearfix"></div>
    </div>
    
    
    
    
             <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label ID="lblTranxType" Text="Provision:" runat="server"></asp:Label></div>
    
    <div class="col-md-2"> <asp:RadioButtonList runat="server" id="rdblProvision" RepeatDirection="Horizontal" onchange="CalculateAmount('ctl00_cphTransaction_txtAmount')" >
                <asp:ListItem Text ="Add" Selected="True"  Value="ADD"></asp:ListItem>
                <asp:ListItem Text ="Deduct"  Value="DEDUCT" ></asp:ListItem> </asp:RadioButtonList></div>
                
    <div class="col-md-2"><asp:Label ID="lblAmount" Text="Amount:" runat="server"></asp:Label> </div>
    
     <div class="col-md-2"> <asp:TextBox ID="txtAmount"  runat="server" Enabled="true" onkeypress="return restrictNumeric(this.id,'2');" onchange="setToFixedThis(this.id);CalculateAmount(this.id);"  CssClass="inputEnabled form-control"></asp:TextBox></div>
     
    <div class="col-md-2"><span id="lblExAmount">Amount In Currency:</span> </div>
     <div class="col-md-2"> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
    
             <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label ID="lblRemarks" Text="Remarks:" runat="server"></asp:Label></div>
    <div class="col-md-10"> <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Height="80px" Enabled="true" CssClass="inputEnabled"></asp:TextBox></div>
    


    <div class="clearfix"></div>
    </div>
    
    
    
        <div class="col-md-12 marbot_10">  
       
       <label class="pull-right marleft_10"> 
       
       <asp:Button ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();" CssClass="btn but_b" OnClick ="btnSave_Click" ></asp:Button>
       
       </label>
      <label class="pull-right marleft_10"> 
       <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="btn but_b" OnClick="btnClear_Click"></asp:Button>
       
         </label>
        
        
         <label class="pull-right marleft_10"> 
        <asp:Button ID="btnSearch" Text="Cancel" runat="server" CssClass="btn but_b" OnClick="btnCancel_Click"></asp:Button>
        
        
         </label>
       
       
        
         
    </div>
   
       
         
        </div>
     
    <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
<script type="text/javascript">

    function Save()
     {           
     
         //alert(getElement('rdblProvision').id)
         //alert(getSelectedProvision(getElement('rdblProvision').id))
        if(getElement('ddlAgent').selectedIndex<=0 ) addMessage('Please select Agent from the list!','');
        if(getElement('ddlSubAgent').selectedIndex<=0 ) addMessage('Please select SubAgent from the list!','');
       
        var agentBalance =parseFloat((isNaN(getElement('txtAgentBalance').value) ||getElement('txtAgentBalance').value=='' )?'0':getElement('txtAgentBalance').value);//toFixed(7);
        var transAmount = parseFloat((isNaN(getElement('txtAmount').value) ||getElement('txtAmount').value=='' )?'0':getElement('txtAmount').value);//toFixed(7);
        var subAgentBalance =parseFloat((isNaN(getElement('txtSubAgentBalance').value) ||getElement('txtSubAgentBalance').value=='' )?'0':getElement('txtSubAgentBalance').value);//toFixed(7);
          
         // alert(agentBalance);
         // alert(transAmount);
        //  alert(subAgentBalance);
          //alert(getSelectedProvision(getElement('rdblProvision').id);
        if(getSelectedProvision(getElement('rdblProvision').id)=='ADD')
        {          
            if( agentBalance<transAmount)addMessage('Amount should be less than Agent Current Balance!','');
        }
        else
        {
            if( subAgentBalance<transAmount)addMessage('Amount should be less than SubAgent Current Balance!','');
        }
//        if(getElement('ddlSubAgent').selectedIndex<='' ) addMessage('Please select SubAgent from the list!','');        
//        if(getElement('ddlPayMode').selectedIndex<='' ) addMessage('Please select Payment mode from the list!','');
//        // if(getElement('txtMapLocation').value=='' ) addMessage('Mapping Location cannot be blank!','');
        if(getElement('txtAmount').value=="0.00" || getElement('txtAmount').value=='') addMessage('Amount cannot be zero/blank!','');
        
//        if((getElement('ddlAgent')
        
        
    if(getMessage()!=''){ 
    alert(getMessage()); 
    //ShowMessageDialog(document.title,getMessage(),'Information');
    clearMessage(); return false;}
  }

  function setExchRate(value) {

      getElement('txtExchRate').value = value;
      setToFixedThis(getElement('txtExchRate').id);

  }
  function setToFixedThis(id) {
      //alert(document.getElementById(id).value)
      var point = 2;
      if (!isNaN(document.getElementById(id).value) && document.getElementById(id).value != '') {
          var value = parseFloat(document.getElementById(id).value);
          document.getElementById(id).value = value.toFixed(point);
      }
      else {
          var defValue = parseFloat('0');
          document.getElementById(id).value = defValue.toFixed(point);
      }


  }

  function CalculateAmount(id) {
      if (!isNaN(document.getElementById(id).value) && document.getElementById(id).value != '') {
          var amount = eval(document.getElementById(id).value);
          var exrate = eval(getElement('txtSubAgentExchRate').value);
          var currency = document.getElementById('<%=ddlSubAgentCurrency.ClientID %>');
          
          //DEDUCT checked
          if (document.getElementById('ctl00_cphTransaction_rdblProvision_1').checked == true) {
              currency = document.getElementById('<%=ddlAgentCurrency.ClientID %>');
              exrate = parseFloat(exrate);
              
              document.getElementById('lblExAmount').innerHTML = "Amount In Currency: " + currency.options[currency.selectedIndex].text + " " + parseFloat( amount/ exrate).toFixed(2);
          }
          else {//ADD checked              
              document.getElementById('lblExAmount').innerHTML = "Amount In Currency: " + currency.options[currency.selectedIndex].text + " " + parseFloat(exrate * amount).toFixed(2);
          }

          if (document.getElementById('ctl00_cphTransaction_rdblProvision_1').checked == true) {
              currency = document.getElementById('<%=ddlSubAgentCurrency.ClientID %>');
              getElement('lblAmount').innerHTML = "Amount (in " + currency.options[currency.selectedIndex].text + ")";
          }
          else {
              currency = document.getElementById('<%=ddlAgentCurrency.ClientID %>');
              getElement('lblAmount').innerHTML = "Amount (in " + currency.options[currency.selectedIndex].text + ")";
          }
      }
  }
     
     var rdblSelectedValue;
     function getSelectedProvision(id) {
         id = id.replace(/_/g, "$");

         var items = document.getElementsByName(id);

         for (i = 0; i < items.length; i++) {
             if (items[i].checked) {
                 rdblSelectedValue = items[i].value;
                 return items[i].value;
             }
         }

     }
   
   
//   function InterfaceYN()
//   {
//       alert(getElement('ctl00_cphTransaction_rdbInterface').value);
//       alert(getElement('rdbInterface').id);
//        //if(getElement('rdbInterface').selectedValue=="N" 
//        //{
//             alert('Hi');
//       // document.getElementById('divInterface').style.display='block';
//        //}
//       // else
//        //{
//        //document.getElementById('divInterface').style.display='none';
//       // }
//     
//   }

//Documents
</script>


<div class="clearfix"> </div>
</div>
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
<asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="AP_ID" 
    EmptyDataText="No Location List!" AutoGenerateColumns="false" PageSize="10" GridLines="none"  CssClass="grdTable"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
    OnPageIndexChanging="gvSearch_PageIndexChanging" >
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
    <%--<asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />--%>
    
           
      <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtName"  Width="100px" CssClass="inputEnabled" HeaderText="Agent" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblName" runat="server" Text='<%# Eval("agent_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("agent_name") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc1:Filter   ID="HTtxtPayMode" Width="100px" HeaderText="Payment Mode" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblPayMode" runat="server" Text='<%# Eval("ap_payment_mode_name") %>' CssClass="label grdof" ToolTip='<%# Eval("ap_payment_mode_name") %>' Width="150px"></asp:Label>
    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>--%>
    </ItemTemplate>    
    
    </asp:TemplateField>     
    
     <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtTo"  Width="100px" CssClass="inputEnabled" HeaderText="To" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblTo" runat="server" Text='<%# Eval("ref_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("ref_name") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>

    
    
     <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtProvision"  Width="100px" CssClass="inputEnabled" HeaderText="Provision" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblProvision" runat="server" Text='<%# Eval("ap_tranx_provision") %>' CssClass="label grdof"  ToolTip='<%# Eval("ap_tranx_provision") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtReceiptNo"  Width="150px" CssClass="inputEnabled" HeaderText="Receipt #" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="ITlblReceiptNo" runat="server" Text='<%# Eval("ap_reciept_no") %>' CssClass="label grdof"  ToolTip='<%# Eval("ap_reciept_no") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>   
    
    <asp:TemplateField>
    <HeaderStyle VerticalAlign="top" />
    <HeaderTemplate>
    <table>    
    <tr>&nbsp;<td >
    </td></tr>
    <tr><td >
    </td>
    <td>
    <uc1:DateControl ID="HTtxtDocDate" runat="server" DateOnly="true" />
    </td>
    <td>
    <asp:ImageButton ID="HTbtnDocDate" runat="server" ImageUrl="~/Images/wg_filter.GIF" ImageAlign="AbsMiddle" OnClick="FilterSearch_Click" />
    </td>
    <%--<td>
    <asp:ImageButton ID="HTbtnChequeReturnDateAdvance"  runat="server" ImageUrl="~/Images/wg_filterAdvance.GIF"  ImageAlign="AbsMiddle"  OnClientClick="return validateAdvanceFilter(this.id,'A');" Alt="Advance Filter" /></td>--%>
    </tr></table>
    <label  class="filterHeaderText">&nbsp;Date</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblReceiptDate"  runat="server" Text='<%# IDDateTimeFormat(Eval("ap_receipt_date")) %>' CssClass="label grdof width120"  ToolTip='<%# Eval("ap_receipt_date") %>' ></asp:Label>                
    </ItemTemplate>                
    </asp:TemplateField>     
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtAmount"  Width="150px" CssClass="inputEnabled width140" HeaderText="Amount" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="ITlblAmount" runat="server" Text='<%# Eval("ap_amount") %>' CssClass="label grdof"  ToolTip='<%# Eval("ap_amount") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>        
    
         
    
      <asp:TemplateField >
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    
    <cc1:Filter ID="HTtxtStatus" Width="100px" HeaderText="Status" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" FilterDataType="number" />                 
    </HeaderTemplate>
    <ItemStyle />
    <ItemTemplate>
    <%--<asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("VS_STATUS_NAME") %>' ToolTip='<%# Eval("VS_STATUS_NAME") %>' Width="60px"></asp:Label>--%>
    <%--<asp:LinkButton ID="ITlnkCancel"  Text='<%# Eval("VS_STATUS_NAME") %>' CssClass="label" Visible='<%# (bool)StatusVisible()%>'  Enabled='<%# (bool)Eval("VS_STATUS").Equals("A") && Eval("VS_ACCOUNTED_STATUS").ToString()=="N"?true:false %>' OnClientClick="return confirm('Are you sure to cancel?')"  runat="server" Width="150px" CommandName="Select" CausesValidation="True"></asp:LinkButton>--%>
    <asp:LinkButton ID="ITlnkCancel"  Text="Cancel" CssClass="label"  OnClientClick="return confirm('Are you sure to cancel?')"  runat="server" Width="150px" CommandName="Select" CausesValidation="True"></asp:LinkButton>
    </ItemTemplate>    
    </asp:TemplateField> 
    
    </Columns>           
    </asp:GridView>

</asp:Content>





