function filterImageReplace() {

    var imgFilterBnt = $('.filter-wrapper input[type=image]');

    if (imgFilterBnt.length) {
        imgFilterBnt.each(function() {
            if (!$(this).parent().hasClass('filter-wrap')) {
                $(this).wrapAll('<div class="filter-wrap" />').addClass('img-input');
                //$(this).removeAttr('src', '');
                $('.filter-wrapper input[type=image][alt="Advance Filter"]').parent().addClass('adv-filter');
                $(this).attr('src', 'images/filter-icon.png');
            }
        })
    }
}
//SetPagenameasClass
function setPageTitle() {
    var CURRENT_URL = location.pathname.split("/").slice(-1)
    var pageTitle = CURRENT_URL;
    pageTitle = pageTitle.toString().replace(/\s+/g, '').toLowerCase();
    pageTitle = pageTitle.split('.', 1)[0]
    $('body').addClass('page-' + pageTitle);
}


 //Detect Safari - for BS3 broken row/col issue 
function checkSafari(){   
    var uString = window.navigator.userAgent;     
    var safariAgent = uString.indexOf("Safari") > -1,
        chromeAgent = uString.indexOf("Chrome") > -1;
    if (!(chromeAgent) && (safariAgent)) {    
        $('body').data('browser','check-safari') 
    }
}//End:Safari Fix    


//Control Buttons : wrapping div and adding icons with data attr.
function controlButtons() {
    $('.btn-control').each(function() {

            //var isVisible = $(this).is(':visible');
            //if((!$(this).parent().hasClass('btn-wrap')) && (isVisible === true)){
            if ((!$(this).parent().hasClass('btn-wrap'))) {
                $(this).wrapAll('<div class="btn-wrap" />');

                if ($(this).attr('data-icon')) {
                    var dataIcon = $(this).data("icon");
                    $(this).closest('.btn-wrap').append('<i class="icon ' + dataIcon + '">');
                    if ($(this).next('.icon').hasClass('icon-right')) {
                        $(this).closest('.btn-wrap').addClass("btn-icon-right");
                    }
                }
            }
            var hiddenbtnCntrl = $(this).attr('style');
            if (hiddenbtnCntrl === 'display:none') {
                $(this).next('.icon').hide();
            }
        })
        //Only for asp command field Button
    $('.cmmField-btn-edit').each(function() {
        $(this).closest('.btn-wrap').append('<i class="icon fa fa-edit">');
    })
}

$(function() {

    //	$('.main-navigation li').on('mouseover',function(){
    //		$(this).find('>ul').show();	
    //		$(this).find('>ul').addClass('bouncceIn ancimated')
    //	}).on('mouseout',function(){
    //		$(this).find('>ul').hide();				
    //	})
    //	

    var screenWidth = window.innerWidth,
        bodyHeight = $(window).height();

    $('.cz-container').css('min-height', '100vh')

    ///Menu Options
    $('ul.dropdown_menu>li>ul').addClass('first-level');//Setting first level menu identifier
    $('ul.dropdown_menu>li>ul.first-level ul').addClass('second-level');//Setting second level menu identifier
    if (screenWidth < 992) {
        var menuBtn = $('ul.dropdown_menu>li a');       
        //Handling Menu in smallscreens < 992
        menuBtn.on('click', function() {
            if ($(this).hasClass('hover')) {
                $(this).next('.first-level').hide().css('visibility', 'hidden').removeClass('activeMenu');
                $(this).next('.second-level').hide().css('visibility', 'hidden').removeClass('activeMenu');
                $(this).removeClass('hover');
            } else {
                $(this).next('.first-level').show().css('visibility', 'visible').addClass('activeMenu');
                $(this).next('.second-level').show().css('visibility', 'visible').addClass('activeMenu');
                $(this).addClass('hover');
            }
        });       
    } else {
       //hover menu for rest of the screens
        $("ul.dropdown_menu li").hover(function() {
            $(this).addClass("hover");
            $('ul:first', this).css('visibility', 'visible').addClass('activeMenu');
        }, function() {
            $(this).removeClass("hover");
            $('ul:first', this).css('visibility', 'hidden').removeClass('activeMenu');
        });
    }
    //Check Height Dropdown - Adjusting Menu Height based on Screen
    var highestDropdown = -Infinity;
    $('ul.dropdown_menu>li .first-level').each(function() {
        highestDropdown = Math.max(highestDropdown, parseFloat($(this).innerHeight()));
    })
    if (bodyHeight < (highestDropdown + 80)) {
        $('.main-navigation').addClass('adjust-menuheight');     
    }

    //Adding 'hasdropdown' class to submenu parent link
    $("ul.dropdown_menu li ul li:has(ul)").find("a:first").addClass('hasdropdown');
    //removing href from menu item which is having submenu
    $('.main-navigation #nav a.hasdropdown').attr('href','javascript:void(0)');

    //END:///Menu Options




    if (screenWidth < 768) {
        $('select').select2('destroy');
    }

    $('.swap-airports').on('click', function() {
        var org = $('.origin-airport').val(),
            dest = $('.dest-airport').val();
        $('.origin-airport').val(dest);
        $('.dest-airport').val(org);
    })



    //BS4  - converting col-xs to col
    //$('[class^="col-xs"]').each(function(){
    //console.log('asdas : ' + $(this).attr('class'))
    //var currentClass = $(this).attr('class');
    //console.log(currentClass);
    //var replacedClass = currentClass.addClass('col');
    //	$(this).addClass('col');
    //console.log(replacedClass);
    //})

    //Custom DropDown   
    $('[data-dropdown]').on('click', function(event) {
        event.preventDefault();
        var ddContent = $(this).data('dropdown');
        $(ddContent).addClass('fadeIn animated faster');
        $(ddContent).removeClass('d-none');
    })
    $('.dd-done-btn').click(function(e) {
        e.preventDefault();
        var ddContent = $(this).closest('.dropdown-content');
        ddContent.addClass('d-none');
        ddContent.removeClass('fadeIn animated faster');
    })

    var mainTitle = $('.title-style span').text().trim().length
    if (!mainTitle) {
        $('.title-style').hide();
    }

    //  $('#ctl00_cphTransaction_chkSuppliers input').change(function(){	 
    //	
    //	  $('#ctl00_cphTransaction_chkSuppliers input:checked').each(function(){
    //		var content =    $(this).next('label').text();
    //		  $('#selectedSuppliers').append(content);
    //		 console.log('item' + $(this).next('label').text());  
    //	  })
    //  })

    //    $('#ctl00_cphTransaction_chkSuppliers input').each(function(){
    //		$('')
    //	})

    //For Adding Filter Icons - Image Button
    //if($('.filter-wrapper').length){


    //}
    checkSafari();
    filterImageReplace();
    controlButtons();
    setPageTitle()

    //Rebinding all functions after update
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function() {
        checkSafari();
        filterImageReplace();
        controlButtons();
        setPageTitle()
    });  





})

$(document).mouseup(function(e) {
    var ddContent = $('.dropdown-content');

    if (!ddContent.is(e.target) && ddContent.has(e.target).length === 0) {
        ddContent.addClass('d-none');
        ddContent.removeClass('fadeIn animated faster');
    }

});

//Remove on red border from input fields
$(document).on('blur', '.form-text-error', function() {
    $(this).removeClass('form-text-error');
})
$(document).on('blur', '.form-border-color', function() {
    $(this).removeClass('form-border-color');
})