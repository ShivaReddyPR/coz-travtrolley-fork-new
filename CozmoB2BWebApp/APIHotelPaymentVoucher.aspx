﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="CozmoB2BWebApp.APIHotelPaymentVoucher" Title="Hotel Booking Confirmation" CodeBehind="APIHotelPaymentVoucher.aspx.cs" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
  
    <!--menu-->
    <link href="css/menuDefault.css" rel="stylesheet" type="text/css">
    

    <link href="css/accordion_ui.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript">
        function viewVoucher() {
            
            <%--window.open('printHotelVoucher.aspx?ConfNo=<%= bookingResponse.ConfirmationNo %>&HotelId=<%= itinerary.HotelId %>', 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes');--%>
            AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'PrintHotelVoucher', 'sessionData':'" + ('<%= bookingResponse.ConfirmationNo %>' + '|' + <%= itinerary.HotelId %>) + "', 'action':'set'}");
            window.open("printHotelVoucher.aspx", "Voucher", "width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes");
            return false;
        }
     
    </script>

          <input id="hdfConfNo" type="hidden" value='<%= bookingResponse.ConfirmationNo %>' />
        <div style="" class="ns-h3 margin-top-10">
            Hotel Reservation Confirmation
            <label class=" hidden-xs" style="float: right; font-size: 11px; padding-right: 10px;
                padding-top: 0px;">
                <%if (itinerary.Source != HotelBookingSource.HotelConnect && itinerary.Source != HotelBookingSource.HotelExtranet)
                  {%>
                Confirmation No:
                <%=itinerary.ConfirmationNo%>
                (Reference No:
                <%=itinerary.BookingRefNo.Replace("|", "")%>|)
                <%}
                  else
                  { %>
                Confirmation No:
                <%=bookingResponse.ConfirmationNo%>
                <%} %>
            </label>
        </div>
      
      
      <div class="bg_white pad_10 bor_gray"> 
      
     
     <div class="col-md-2"> <asp:Image Width="87" Height="64" ID="imgHeaderLogo" runat="server" /></div>
          
     <div class="col-md-5"> 
     <div> <b>
                                                                <%=itinerary.HotelName %></b></div>
     
     <div>   Add :-<%=itinerary.HotelAddress1 %></div>
     
     
      <div>  Tel :- <b><%=itinerary.HotelAddress2 %></b></div>
       
     
     
     </div>
     <div class="col-md-5">
     
     <div>  <b class="spnred">No. of Rooms </b>:
                                    <%=itinerary.NoOfRooms %></div>
        <div> 
         <% int adults = 0, childs = 0;
                                       foreach (HotelRoom room in itinerary.Roomtype)
                                       {
                                           adults += room.AdultCount;
                                           childs += room.ChildCount;
                                       } %>
                                   
                                      
                                            <b class="spnred">No. of People </b>: <%=adults%> 
                                            (Adult(s) <b class="spnred"> </b>, <%=childs%> 
                                            (Child(s)         
        
        
        </div>
         <div><b><span class="spnred"> Booked on:</span>   </b>  <%=itinerary.CreatedOn.ToString("dd MMM yyyy hh:mm tt") %>
        
                                                                           </div>
         <div>
              <%if ((parentAgent!=null))
            { %><b><span class="spnred"> Booked by:</span>   </b>  <%=parentAgent.Name %>
             <%} %>
         </div>
     
     
     
      </div>
     
      
      <div class="clearfix"> </div>
      </div>
      
      
       
<br />       <br />      

<h4>  Payment Details</h4>

<div class="table-responsive bg_white"> 
<table width="100%" class="table">
                            <tr>
                                <td>
                                <b>Room no.</b>
                            </td>
                            <td>
                                <b>Room Type</b>
                            </td>
                            <td>
                                <b>No. of Guests</b>
                            </td>
                                <td>
                                    <b>Total Room Price </b>
                                </td>
                                <%--<td>
                                    <b>Total Room Tax </b>
                                </td>--%>
                                <td>
                                    <b>Total</b>
                                </td>
                            </tr>
                             <% decimal gtotal = 0,disc=0,pageMarkup=0,vatAmount = 0; int rc = 1;
                                 foreach (CT.BookingEngine.HotelRoom room in itinerary.Roomtype)
                                 { %>
                            <tr>
                            <td>
                            <%=rc %>
                            </td>
                                <td>
                                    <%=room.RoomName.Split('|')[0] %> - <%= (!string.IsNullOrEmpty(room.MealPlanDesc) ? room.MealPlanDesc : "Room Only")%>
                                </td>
                                <td>
                                 <%=room.AdultCount%> Adult(s) <%=room.ChildCount%> 
                                Child(s)  
                                     <%
                                    string childAges=string.Empty;
                                    for (int j = 0; j < room.ChildCount; j++)
                                    {
                                        if (room.ChildAge.Count > j)
                                        {
                                            if (childAges == string.Empty)
                                            {
                                                childAges = room.ChildAge[j].ToString();
                                            }
                                            else
                                            {
                                                childAges += "," + room.ChildAge[j].ToString();
                                            }
                                        }
                                    } %>
                                     <%if (childAges != "")
                                         { %>
                                     Child Ages(<%=childAges%>)
                                     <%}%>
                                </td>
                                <td>
                                <%disc += room.Price.Discount; %>
                                    <%vatAmount +=room.Price.OutputVATAmount; %>
                                <%--<%pageMarkup += itinerary.Roomtype[0].Price.AsvAmount; %> commented by Anji--%> 
                                    <%pageMarkup += room.Price.AsvAmount; %>
                                    <%=itinerary.Roomtype[0].Price.Currency%>
                                    <%--<%if (itinerary.Source != HotelBookingSource.LOH)
                                      { %>--%>
                                    <%=Math.Round((priceType == CT.BookingEngine.PriceType.PublishedFare ? (room.Price.PublishedFare) : (room.Price.NetFare + room.Price.Markup)), agent.DecimalValue).ToString("N" + agent.DecimalValue.ToString())%>
                                    <%--<%}
                                      else
                                      { %>
                                      <%=((priceType == CT.BookingEngine.PriceType.PublishedFare ? (room.Price.PublishedFare + room.Price.Tax) : (room.Price.NetFare+ room.Price.Markup + room.Price.Tax ))).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                    <%} %>--%>
                                </td>
                                <%--<td>
                                    AED
                                    <%=room.Price.Tax.ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                </td>--%>
                                <td>
                                
                                    <%=itinerary.Roomtype[0].Price.Currency%>
                                    <% //if (itinerary.Source != HotelBookingSource.LOH)
                                       {
                                           gtotal += Math.Round((priceType == CT.BookingEngine.PriceType.PublishedFare ? ((room.Price.PublishedFare)) * (rateOfExchange) : ((room.Price.NetFare + room.Price.Markup)) * (rateOfExchange)), agent.DecimalValue);
                                       }
                                       //else
                                       //{
                                       //    gtotal += (priceType == CT.BookingEngine.PriceType.PublishedFare ? ((room.Price.PublishedFare + room.Price.Tax)) * (rateOfExchange) : ((room.Price.NetFare + room.Price.Markup + room.Price.Tax)) * (rateOfExchange));
                                       //}
                                    %>
                                    <%--<%if (itinerary.Source != HotelBookingSource.LOH)
                                      { %>--%>
                                    <%=Math.Round((priceType == CT.BookingEngine.PriceType.PublishedFare ? (room.Price.PublishedFare) : (room.Price.NetFare + room.Price.Markup)), agent.DecimalValue).ToString("N" + agent.DecimalValue.ToString())%>
                                    <%--<%}
                                      else
                                      { %>
                                      <%=((priceType == CT.BookingEngine.PriceType.PublishedFare ? (room.Price.PublishedFare + room.Price.Tax) : (room.Price.NetFare + room.Price.Markup + room.Price.Tax))).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                    <%} %>--%>
                                </td>
                            </tr>
                           <%rc++; %>
                             <%} %>
                             
      <tr>
                                <td>
                                    <b>Total </b>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <%--<td>
                                    &nbsp;
                                </td>--%>
                                <td>
                                    <b class="spnred"><%=itinerary.Roomtype[0].Price.Currency%>
                                        <%=(gtotal).ToString("N" + agent.DecimalValue.ToString())%></b>
                                </td>
                            </tr>
    <%if(disc > 0){ %>
                              <tr>
                                <td>
                                    <b>Discount </b>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <%--<td>
                                    &nbsp;
                                </td>--%>
                                <td>
                                    <b class="spnred"><%=itinerary.Roomtype[0].Price.Currency%>
                                        <%=(disc).ToString("N" + agent.DecimalValue.ToString())%></b>
                                </td>
                            </tr>
                            <%} %>
                            <%if(pageMarkup > 0){ %>
                            
                            <tr>
                                <td>
                                    <b>Addl Markup </b>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <%--<td>
                                    &nbsp;
                                </td>--%>
                                <td>
                                    <b class="spnred"><%=itinerary.Roomtype[0].Price.Currency%>
                                        <%=(pageMarkup).ToString("N" + agent.DecimalValue.ToString())%></b>
                                </td>
                            </tr>
                            <%} %>
    
     <tr>
                                <td>
                                    <%if ((Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN") || (!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.LocationCountryCode == "IN"))
                                        { %>
                                    <b>Total GST </b>
                                    <%}
    else
    { %>
                                     <b>Vat </b>
                                    <%} %>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <%--<td>
                                    &nbsp;
                                </td>--%>
                                <td>
                                    <b class="spnred"><%=itinerary.Roomtype[0].Price.Currency%>
                                        <%=(vatAmount).ToString("N" + agent.DecimalValue.ToString())%></b>
                                </td>
                            </tr>
    
                              <tr>
                                <td>
                                    <b>Grand Total </b>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <%--<td>
                                    &nbsp;
                                </td>--%>
                                <td>
                                    <b class="spnred"><%=itinerary.Roomtype[0].Price.Currency %>
                                        <%=Math.Ceiling((gtotal)+vatAmount - disc + pageMarkup).ToString("N" + agent.DecimalValue.ToString()) %></b>
                                </td>
                            </tr>
    <% if (itinerary.Roomtype[0].TaxInfo != null && itinerary.Roomtype[0].TaxInfo.Count > 0)
        {
            List<HotelTaxBreakup> hotelTaxList = itinerary.Roomtype[0].TaxInfo;
            //var distinctList=hotelTaxList.Distinct();
            var distinctList = hotelTaxList.GroupBy(x => x.TaxName).Select(cl => new HotelTaxBreakup
            {
                TaxName= cl.Select(c => c.TaxName).FirstOrDefault(),
                IsIncluded= cl.Select(c => c.IsIncluded).FirstOrDefault(),
                UnitType= cl.Select(c => c.UnitType).FirstOrDefault(),
                IsMandatory= cl.Select(c => c.IsMandatory).FirstOrDefault(),
                FrequencyType= cl.Select(c => c.FrequencyType).FirstOrDefault(),
                TaxValue = cl.Sum(c => c.TaxValue)
            }).ToList();
            %>
     <tr>
                                <td>
                                    <b>Tax Details </b>
                                </td>
                                <td></td>
                                <td></td>
                                <td colspan="3">
                                    <table style="width:100%">
                                        <% 
                                           
                                            foreach (var Breakups in distinctList)
                                            {

                                                if (Breakups.IsIncluded) {                                            
                                            %>                                      
                                        <tr>
                                            <td> <% =Breakups.TaxName.Replace("_"," ") %>-<%= Breakups.UnitType %> -Incl :  <%=itinerary.Roomtype[0].Price.Currency %> <%= Breakups.TaxValue.ToString("N" + Settings.LoginInfo.DecimalValue) %> </td>
                                            
                                        </tr>
                                        
                                        <%}} %>
                                      </table>                                  
                                </td>
                                <td colspan="3">
                                    <table style="width:100%">
                                        <% 
                                            decimal totExl = 0;
                                            foreach (var Breakups in distinctList)
                                            {
                                                if (!Breakups.IsIncluded) { 
                                                totExl = totExl + Breakups.TaxValue;
                                            %>
                                      
                                        <tr>
                                            <%-- <td> <% =Breakups.TaxName.Replace("_"," ") %>-<%= Breakups.UnitType %> -Excl (Pay @ Hotel) :  <%=itinerary.Roomtype[0].Price.Currency %> <%= Breakups.TaxValue %> / <%=Breakups.FrequencyType %></td> --%>
                                            <td> <% =Breakups.TaxName.Replace("_"," ") %>-<%= Breakups.UnitType %> -Excl (Pay @ Hotel) :  <%=itinerary.Roomtype[0].Price.Currency %> <%= Breakups.TaxValue.ToString("N" + Settings.LoginInfo.DecimalValue) %> </td>
                                            
                                        </tr>
                                        
                                        <%}} %>

                                   
                                       <tr><%if (totExl>0) { %>
                                            <td> Total (pay @ Hotel) : <%=itinerary.Roomtype[0].Price.Currency %> <%=totExl.ToString("N" + Settings.LoginInfo.DecimalValue) %></td>
                                            <%} %>
                                        </tr>
                                      </table>
                                   
                                    
                                   
                                </td>
                            </tr>
    <%} %>
                        </table>

</div>

            
   
   
   <br />      

 <div style="" class="ns-h3 margin-top-10">Guest Details</div>

<div class="table-responsive bg_white"> 
<table width="100%" class="table">

                                                <tr>
                                                    <td class="red_span">
                                                        <b>Lead Guest</b>
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="21%">
                                                        <b>Name :<br />
                                                        </b>
                                                    </td>
                                                    <td width="79%">
                                                        <%  string paxName = "";
                                                            if (itinerary.HotelPassenger.LeadPassenger)
                                                            {
                                                                paxName = itinerary.HotelPassenger.Firstname + " " + itinerary.HotelPassenger.Lastname;
                                                            %>
                                                           <%=paxName%><br />
                                                    </td>
                                                </tr>
                                               <%-- <tr>
                                                    <td>
                                                        <strong>Address :
                                                            <br />
                                                        </strong>
                                                    </td>
                                                    <td>
                                                        <%=itinerary.HotelPassenger.Addressline1%> <%=itinerary.HotelPassenger.Addressline2%><br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong>Mobile no:<br />
                                                        </strong>
                                                    </td>
                                                    <td>
                                                        <%=itinerary.HotelPassenger.Phoneno%><br />
                                                    </td>
                                                </tr>--%>
                                        <tr>
                                            <td>
                                                <b>E-mail ID:</b>
                                            </td>
                                            <td>
                                                <%=itinerary.HotelPassenger.Email%>
                                                <%} %>
                                            </td>
                                        </tr>
                                    </table>

</div> 
      
          
          
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                
            
            
            
                
                <tr>
                    <td>
                        <% string cancelData = "", remarks = "";
                           //if (Session["cSessionId"] != null)
                           {
                               Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
                               if (itinerary.Source == HotelBookingSource.DOTW || itinerary.Source == HotelBookingSource.HotelBeds || itinerary.Source == HotelBookingSource.HotelConnect || itinerary.Source == HotelBookingSource.TBOHotel || itinerary.Source == HotelBookingSource.WST || itinerary.Source == HotelBookingSource.JAC|| itinerary.Source == HotelBookingSource.Illusions|| itinerary.Source == HotelBookingSource.HotelExtranet)    //Modified by brahmam 26.09.2014
                               {
                                   //List<HotelPenality> penaltyInfo = itinerary.PenalityInfo;
                                   //CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi(Session["cSessionId"].ToString());

                                   //if (penaltyInfo == null)
                                   //{
                                   //    penaltyInfo = new List<HotelPenality>();
                                   //}
                                   //cancellationInfo = dotw.GetCancellationPolicy(itinerary, ref penaltyInfo, true);
                                   cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                                   cancellationInfo.Add("HotelPolicy", itinerary.HotelPolicyDetails);
                               }
                               else if (itinerary.Source == HotelBookingSource.RezLive)
                               {
                                   if (itinerary.Source == HotelBookingSource.RezLive)
                                   {
                                       cancelData = itinerary.HotelCancelPolicy;
                                       cancelData = cancelData.Replace(". ", "|");
                                   }
                                   cancellationInfo.Add("CancelPolicy", cancelData);
                               }
                               else if (itinerary.Source == HotelBookingSource.LOH || itinerary.Source == HotelBookingSource.EET || itinerary.Source == HotelBookingSource.GTA || itinerary.Source == HotelBookingSource.Miki|| itinerary.Source == HotelBookingSource.Agoda || itinerary.Source == HotelBookingSource.Yatra  || itinerary.Source==HotelBookingSource.GRN || itinerary.Source==HotelBookingSource.OYO||itinerary.Source==HotelBookingSource.GIMMONIX||itinerary.Source==HotelBookingSource.Offline)
                               {
                                   try
                                   {
                                       cancelData = itinerary.HotelCancelPolicy;
                                       //LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine();
                                       //cancellationInfo = jxe.GetCancellationDetails(itinerary.HotelCode, itinerary.Roomtype[0].RatePlanCode, itinerary.StartDate, itinerary.EndDate, Session["SequenceNumber"].ToString());
                                   }
                                   catch { }
                               }
                               foreach (KeyValuePair<string, string> pair in cancellationInfo)
                               {
                                   switch (pair.Key)
                                   {
                                       case "lastCancellationDate":
                                           break;
                                       case "CancelPolicy":
                                           cancelData = pair.Value;

                                           break;
                                       case "HotelPolicy":
                                           remarks = pair.Value;
                                           break;
                                   }
                               }
                           }
                        
                     %>
                    <div class="glossymenu">
                     
                        
                        
                        
                             <%if (!string.IsNullOrEmpty(remarks))
                              { %>
                            
                            <div style="" class="ns-h3 margin-top-10">Hotel Norms </div>
                        <div class="bg_white pad_10 bor_gray">
                            <div>
                              <%string[] notes = remarks.Split('|'); %>
                                <ul>
                                <%foreach (string note in notes)
                                  {
                                      if (note.Length > 0)
                                      { %>
                                  <li><%=note.Replace("#&#", "") %></li>
                                <%}
                                  } %>
                                </ul>
                            </div>
                        </div>
                        
<% }if (itinerary.Source == HotelBookingSource.GTA || itinerary.Source == HotelBookingSource.Agoda)
                          { if (!string.IsNullOrEmpty(itinerary.SpecialRequest))
				{%>
                         <div style="" class="ns-h3 margin-top-10">Essential Information / Please Note </div>
                        <div class="bg_white pad_10 bor_gray">
                            <div style="padding: 0px 10px 0px 10px">
                              <%string[] EssentialInf = itinerary.SpecialRequest.Split('|'); %>
                                <ul>
                                <%foreach (string Essential in EssentialInf)
                                  {
                                      if (Essential.Length > 0)
                                      { %>
                                  <li><%=Essential%></li>
                                <%}
                                  } %>
                                </ul>
                            </div>
                              </div>
                            <%}} %>
                                    
                         
                                <%if (showVoucherTerms == true)
                                    { %>
                           <div  class="ns-h3 margin-top-10">Cancellation and Charges </div>
                            <div class="bg_white pad_10 bor_gray">
                                
                                    <%
                                     cancelData = cancelData.Replace("^Date and time is calculated based on local time of destination|", "|");
                                     cancelData = cancelData.Replace("^", "|");
                                        string[] cdata = cancelData.Split('|');%>
                                    <ul>
                                        <%foreach (string data in cdata)
    {
        if (data.Length > 0)
        {%>
                                        <li>
                                            <%=data%> </li>
                                        <%}
    } %>
                                    </ul>
                                
                            </div>
                        <%} %>
                        
                        </div>
                        <% if (itinerary.Source == HotelBookingSource.GIMMONIX)
                               {   %>
                        <div style="" class="ns-h3 margin-top-10">Disclaimer </div>
                        <%--<div class="bg_white pad_10 bor_gray">--%>
                        <div  class="table-responsive bg_white">
                            <div>
                              <ul style="float: left;">
                               
                                <%if (!string.IsNullOrEmpty(itinerary.HotelPolicyDetails) && itinerary.Roomtype.Select(c=>c.ChildCount).Count() >1)
                                    {                                        
                                        var disclaimer = itinerary.HotelPolicyDetails.Split('^')[1];                                        
                                            if (!string.IsNullOrEmpty(disclaimer))
                                            {%>
                                    
                                <li><%=disclaimer.Split('|')[0].Replace("<br>", "").Replace("#&#", "")%></li>
                                <%}%>
                             
                              <%} %>
                              <li><label class="bold"> Bed type availabilities depends @ Check-In time </label></li>
                             </ul>  
                            </div>
                        </div>
                         <%} %> 
                          <div style="font-size:10px; text-align:center;" > 
    <%if (itinerary.Source == HotelBookingSource.GIMMONIX)
        {
            for (int i = 0; i < itinerary.NoOfRooms; i++)  %>
                             <%{  if (!string.IsNullOrEmpty(itinerary.Roomtype[i].Gxsupplier))  %>
                        <%{
                                   if (!itinerary.Roomtype[0].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[3].Contains("EPS"))
                                   {%>
                      <label>Booked and payable by :</label><%=itinerary.Roomtype[i].Gxsupplier%><br />
                       <%}
                                       }
                                   }
                               }
                               else
                               { %>
 <%=itinerary.PaymentGuaranteedBy%>
  <%} %>
 </div>
                    </td>
                </tr>
                
                
                
            </table>
            <div style=" float:right; padding-top:20px; padding-bottom:10px; text-align:right">
            <%if (itinerary.Status != CT.BookingEngine.HotelBookingStatus.AwaitingForPayment){%>
            
            
             <asp:Button ID="ImageButton1" OnClientClick="return viewVoucher();" Text="View Voucher" CssClass="but but_b pull-right" 
                            runat="server" /><%}%></div>
             </td>
                </tr>
                
                
                
            </table> 
        
        

   
   
   
   
   
   
    <div id='PrintDiv' runat='server' style='width: 100%;display:none'>
    
            <table width='100%' border='0' cellpadding='0' cellspacing='0' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px;'>
                <tr>
                    <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding: 5px; background-color: #3060a0; color: #fff;'
                        width='25%' align='left'>
                        Reservation Voucher
                    </th>
                    <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-right: solid 1px #ccc;padding: 5px; background-color: #3060a0; color: #fff;'
                        width='75%' align='right'>
                        
                    </th>
                </tr>
                <tr>
                    <td colspan='2' style='padding: 5px; border: solid 1px #c6c6c6;'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td width='50%' valign='top'>
                                    <asp:Image ID='imgLogo' runat='server'  />
                                </td>
                                <td width='50%'>
                                    <div style='line-height: 22px'>
                                        <strong>
                                            <%=itinerary.HotelName %></strong>
                                            <%if(itinerary.Rating!=HotelRating.All)
                                              { %>
                                            (<%=itinerary.Rating%>)</strong>
                                            <%} %>
                                            <br />
                                        <strong>Address :</strong>
                                        <%=itinerary.HotelAddress1 %><br />
                                        <%=itinerary.HotelAddress2 %>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
               
                <tr>
                    <td colspan='2'>
                        <br />
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                              <%if (parentAgent!=null )
                                            {  %>

                            <tr>
                            <td align='left'>
                                    <strong>Booked By :</strong>
                                   <%=parentAgent.Name %>
                                </td>
                                     </tr>   
                    
                                       <%} %>
                            <tr>
                                <td align='left'>
                                    <strong>Date of Issue:</strong>
                                    <%=itinerary.CreatedOn.ToString("dd MMM yyyy hh:mm tt") %>
                                </td>
                                <td align='right'>
                                    <strong>Confirmation No:</strong>
                                    <%if (itinerary.Source != HotelBookingSource.HotelConnect)
                                        {%>
                                    <%=itinerary.ConfirmationNo%>
                (Reference No:
                <%=itinerary.BookingRefNo.Replace("|", "")%>|)
                <%}
                    else
                    { %>
                                    <%=itinerary.ConfirmationNo%>
                                    <%} %>
                                    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
               
                <tr>
                    <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                        <strong>Cozmo Ref No:</strong>
                    </td>
                    <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                        HTL-CT-<%=booking.BookingId %>
                    </td>
                </tr>
                
                <tr>
                    <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                        <strong>Address:</strong>
                    </td>
                    <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                        <%=Settings.LoginInfo.LocationAddress %>
                    </td>
                </tr>
                <tr>
                    <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                        <strong>Emergency Contact No: </strong>
                    </td>
                    <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                        <%=Settings.LoginInfo.AgentPhone %>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <strong>Room no.</strong>
                                </td>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <strong>Room Type</strong>
                                </td>
                                    <td style='padding:5px; border: solid 1px #c6c6c6;'>
                                    <strong>Pax Name(s)</strong>
                                </td>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                 <%if (itinerary.Source != HotelBookingSource.TBOHotel)
                                   { %>
                                      <strong>Meal Plan</strong>
                                    <%}
                                   else
                                   {
                                        %>
                                        <strong>Inclusions</strong>
                                        <%} %>
                                </td>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <strong>No. of Guests</strong>
                                </td>
                            </tr>
                            <%for (int i = 0; i < itinerary.NoOfRooms; i++)  %>
                            <%{ %>
                            <tr>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <%=i+1%>
                                </td>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <%=(itinerary.Roomtype[i].RoomName.Contains("|") ? itinerary.Roomtype[i].RoomName.Split('|')[0] : itinerary.Roomtype[i].RoomName)%>
                                </td>
                                 <td style='padding:5px; border: solid 1px #c6c6c6;'>
                                  
                                  <% string paxname = string.Empty;
                                     for (int k = 0; k < itinerary.Roomtype[i].PassenegerInfo.Count; k++)
                                     {
                                         if (paxname == string.Empty)
                                         {
                                             paxname = itinerary.Roomtype[i].PassenegerInfo[k].Firstname + " " + itinerary.Roomtype[i].PassenegerInfo[k].Lastname;
                                         }
                                         else
                                         {
                                             paxname += "," + itinerary.Roomtype[i].PassenegerInfo[k].Firstname + " " + itinerary.Roomtype[i].PassenegerInfo[k].Lastname;
                                         }
                                     }
                                        
                                         %>
                                         <%=paxname%>
                                </td>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <%= (!string.IsNullOrEmpty(itinerary.Roomtype[i].MealPlanDesc))?itinerary.Roomtype[i].MealPlanDesc:"Room Only"%>
                                </td>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <%=itinerary.Roomtype[i].AdultCount%>
                                    Adult(s)
                                    <%=itinerary.Roomtype[i].ChildCount%>
                                    Child(s)
                                    <%
                                    string childAges=string.Empty;
                                    for (int j = 0; j < itinerary.Roomtype[i].ChildCount; j++)
                                    {
                                        if (itinerary.Roomtype[i].ChildAge.Count > j)
                                        {
                                            if (childAges == string.Empty)
                                            {
                                                childAges = itinerary.Roomtype[i].ChildAge[j].ToString();
                                            }
                                            else
                                            {
                                                childAges += "," + itinerary.Roomtype[i].ChildAge[j].ToString();
                                            }
                                        }
                                    } %>
                                    
                                     <%if (childAges != "")
                                         { %>
                                     Child Ages(<%=childAges%>)
                                     <%}%>
                                </td>
                            </tr>
                            <%} %>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding: 5px; background-color: #3060a0; color: #fff;'
                        width='21%' align='left'>
                        Lead Guest
                    </th>
                    <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding: 5px; background-color: #3060a0; color: #fff;'
                        width='79%' align='right'>
                    </th>
                </tr>
                <tr>
                    <td colspan='2'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;' width='25%'>
                                    <strong>Name :<br />
                                    </strong>
                                </td>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;' width='75%'>
                                    <%  string paxName = "";
                                        if (itinerary.HotelPassenger.LeadPassenger)
                                        {
                                            paxName = itinerary.HotelPassenger.Firstname + " " + itinerary.HotelPassenger.Lastname;
                                    %>
                                    <%=paxName%><br />
                                </td>
                            </tr>
                            <%if (itinerary.Source != HotelBookingSource.UAH)
                                { %>
                            <tr>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <strong>Nationality:<br />
                                    </strong>
                                </td>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <%=itinerary.HotelPassenger.Nationality%><br />
                                </td>
                            </tr>
                            <%} %>
                            <tr>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <strong>Check In Date:
                                        <br />
                                    </strong>
                                </td>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <%=itinerary.StartDate.ToString("dd-MMM-yyy")%><br />
                                </td>
                            </tr>
                            <tr>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <strong>Check Out Date:
                                        <br />
                                    </strong>
                                </td>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <%=itinerary.EndDate.ToString("dd-MMM-yyy")%><br />
                                </td>
                            </tr>
                            <%int adults = 0, childs = 0, total = 0;
                              string mealPlanDesc = string.Empty;
                              foreach (HotelRoom room in itinerary.Roomtype)
                              {
                                  adults += room.AdultCount;
                                  childs += room.ChildCount;
                                  mealPlanDesc = room.MealPlanDesc;


                              }
                              total = adults + childs;
                            %>
                            <tr>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <strong>No. of Guests<br />
                                    </strong>
                                </td>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <%=total %>,
                                    <%=adults%>(Adult(s)),
                                    <%=childs%>
                                    (Child(s))
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <strong>No. of Nights :<br />
                                    </strong>
                                </td>
                                <td style='padding: 5px; border: solid 1px #c6c6c6;'>
                                    <%=nights %><br />
                                </td>
                            </tr>
                            
                            <%} %>
                        </table>
                    </td>
                </tr>
                  <% if (itinerary.Source == HotelBookingSource.GTA || itinerary.Source == HotelBookingSource.Agoda)
                    {
                        if (!string.IsNullOrEmpty(itinerary.SpecialRequest))
                        {%>
                <tr>
                    <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc; padding: 5px; background-color: #3060a0; color: #fff;'
                        width='30%' align='left'>Essential Information / Please Note
                    </th>
                    <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc; padding: 5px; background-color: #3060a0; color: #fff;'
                        width='70%' align='right'></th>
                </tr>
                <tr>
                    <td colspan='2' style='padding: 5px; border: solid 1px #c6c6c6;'>

                        <div style="padding: 0px 10px 0px 10px">
                            <%string[] EssentialInf = itinerary.SpecialRequest.Split('|'); %>
                            <ul>
                                <%foreach (string Essential in EssentialInf)
                                    {
                                        if (Essential.Length > 0)
                                        { %>
                                <li><%=Essential%></li>
                                <%}
                                } %>
                            </ul>
                        </div>

                    </td>
                </tr>
                <%}
                } %>
                <tr>
             <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding: 5px; background-color: #3060a0; color: #fff;'
                        width='30%' align='left'>
                       Hotel Norms
                    </th>
                    <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding: 5px; background-color: #3060a0; color: #fff;'
                        width='70%' align='right'>
                    </th>
                    </tr>
                    <tr>
                    <td colspan='2' style='padding: 5px; border: solid 1px #c6c6c6;'>  
                    <div style='padding: 0px 10px 0px 10px'>
                  <ul style='float: left;'>
                      <%if (!string.IsNullOrEmpty(itinerary.HotelPolicyDetails))
                        { %>
                              <%string[] hotelPolicy = itinerary.HotelPolicyDetails.Split('|');
                                foreach (string norm in hotelPolicy)
                                {
                                    if (norm.Length > 0)
                                    {%>
                            <li> <%=norm.Replace("<br>", "").Replace("#&#", "").Split('^')[0]%></li>
                            <%}
                                } %>
                              <%}
                        
                        else
                        { %>
                              Check-in time at 1400hrs and check-out time 1200hrs Early check-in or late check-out is subject to availability at the time of check-in/check-out at the hotel and cannot be guaranteed at any given point in time 
                              
                              <%} %>
                              
                             </ul>                   
                      </div>
                    </td>
                    </tr>
                <% if (itinerary.Source == HotelBookingSource.GIMMONIX)
                               {   %>
                          
              <tr>
                <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding: 5px; background-color: #3060a0; color: #fff;'
                        width='30%' align='left'>
                       Disclaimer
                    </th>
                    <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding: 5px; background-color: #3060a0; color: #fff;'
                        width='70%' align='right'>
                    </th>
                </tr>
                 <tr>
                    <td colspan="3" style="padding: 5px; border: solid 1px #c6c6c6;">
                        <div style="padding: 0px 10px 0px 10px">
                            <ul style="float: left;">
                               
                                <%if (!string.IsNullOrEmpty(itinerary.HotelPolicyDetails) && itinerary.Roomtype.Select(c=>c.ChildCount).Count() >0)
                                    {                                        
                                        var disclaimer = itinerary.HotelPolicyDetails.Split('^')[1];                                        
                                            if (!string.IsNullOrEmpty(disclaimer))
                                            {%>
                                    
                                <li><%=disclaimer.Split('|')[0].Replace("<br>", "").Replace("#&#", "")%></li>
                                <%}%>
                             
                              <%} %>
                              <li><label class="bold"> Bed type availabilities depends @ Check-In time </label></li>
                             </ul>                   
                      </div>
                    </td>
                </tr>
      <%} %> 
              
                  <%if(showVoucherTerms == true)
                                    { %>
                               <tr>
                 <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding: 5px; background-color: #3060a0; color: #fff;'
                        width='30%' align='left'>
                       Cancellation & Charges
                    </th>
                    <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding: 5px; background-color: #3060a0; color: #fff;'
                        width='70%' align='right'>
                    </th>
                </tr>
             <tr>
                <td colspan='2' style='padding: 5px; border: solid 1px #c6c6c6;'>
                 <div style='padding: 0px 10px 0px 10px'>
                     <ul style='float: left;'>
                         <% string cancelData = "", remarks = "";
                            Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
                            if (itinerary.Source == HotelBookingSource.DOTW)
                            {
                                cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                            }
                            else if (itinerary.Source == HotelBookingSource.RezLive)
                            {
                                if (itinerary.Source == HotelBookingSource.RezLive)
                                {
                                    cancelData = itinerary.HotelCancelPolicy;
                                    cancelData = cancelData.Replace(". ", "|");
                                }
                                cancellationInfo.Add("CancelPolicy", cancelData);
                            }
                            else
                            {
                                cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                            }
                            foreach (KeyValuePair<string, string> pair in cancellationInfo)
                            {
                                switch (pair.Key)
                                {
                                    case "lastCancellationDate":
                                        break;
                                    case "CancelPolicy":
                                        cancelData = pair.Value;

                                        break;
                                    case "HotelPolicy":
                                        remarks = pair.Value;
                                        break;
                                }
                            }
                         %>
                         <%

                              cancelData = cancelData.Replace("^Date and time is calculated based on local time of destination|", "|");
                                     cancelData = cancelData.Replace("^", "|");
                             string[] cdata = cancelData.Split('|');%>
                             <%foreach (string data in cdata)
                               {
                                   if (data.Length > 0)
                                   {%>
                             <li>
                                 <%=data%></li>
                             <%}
                                          } %>
                         </ul>
                 </div>
                </td>
                </tr>
                 
                <tr>
                    <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding: 5px; background-color: #3060a0; color: #fff;'
                        width='21%' align='left'>
                        Terms & Conditions
                    </th>
                    <th style='border-top: solid 1px #ccc; border-bottom: solid 1px #ccc; border-left: solid 1px #ccc;padding: 5px; background-color: #3060a0; color: #fff;'
                        width='79%' align='right'>
                    </th>
                </tr>
                <tr>
                    <td colspan='2' style='padding: 5px; border: solid 1px #c6c6c6;'>
                        <div style='padding: 0px 10px 0px 10px'>
                            <li style='list-style-type: circle'>All rooms are guaranteed on the day of arrival.
                                In the case of no-show, your room(s) will be released and you will subject to the
                                terms and condition of the Cancellation/No-show policy specified at the time you
                                made the booking as well as noted in the confirmation Email. </li>
                            <li style='list-style-type: circle'>The total price for these booking fees not include
                                mini-bar items, telephone bills, laundry service, etc. The hotel will bill you directly.
                            </li>
                            <li style='list-style-type: circle'>In case where breakfast is included with the room
                                rate, please note that certain hotels may charge extra for children travelling with
                                their parents. If applicable, the hotel will bill you directly. Upon arrival, if
                                you have any questions, please verify with the hotel. </li>
                            <li style='list-style-type: circle'>Any complaints related to the respective hotel services,
                                with regards to location, rooms, food, cleaning or other services, the guest will
                                have to directly deal with the hotel. Cozmo Holidays will not be responsible for
                                such complaints. </li>
                            <li style='list-style-type: circle'>The General Hotel Policy: Check-in time at 1400hrs
                                and check-out time 1200hrs Early check-in or late check-out is subject to availability
                                at the time of check-in/check-out at the hotel and cannot be guaranteed at any given
                                point in time </li>
                            <li style='list-style-type: circle'>Interconnecting/ Adjoining rooms/any special requests
                                are always subject to availability at the time of check-in, and Cozmo Holidays will
                                not be responsible for any denial of such rooms to the Customer. </li>
                            <li style='list-style-type: circle'>Most of the hotels will be asking for credit card
                                or cash amount to be paid upon check-in as guaranteed against any expected extras
                                by the guest, Cozmo Holidays will not be responsible in case the guest doesn’t carry
                                a credit card or enough cash money for the same, and the guest has to follow up
                                directly with the hotel for the refund upon check out, Cozmo holidays is not responsible
                                in case of any delay from central bank for credit card refunds. </li>
                                 <%if(itinerary.CityRef != null && itinerary.CityRef == "DUBAI"){ %>
                                <li style="background-color:Yellow;list-style-type: circle">Tourism Dirham Charge to be paid directly by the client 
                                to hotel before check out (AED 10- 20 per room per night). Applicable for Check in from 31st March onward.. </li>
                                <%} %>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>

                  <%} %>
                <tr>
                    <td colspan='2' align='center'>
                <%--      <div style="font-size:8px"> --%>
    <%if (itinerary.Source == HotelBookingSource.GIMMONIX)
        {
            for (int i = 0; i < itinerary.NoOfRooms; i++)  %>
                           <%{  if (!string.IsNullOrEmpty(itinerary.Roomtype[i].Gxsupplier))  %>
                        <%{
                                   if (!itinerary.Roomtype[0].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[3].Contains("EPS"))
                                   {%>
                      <label>Booked and payable by :</label><%=itinerary.Roomtype[i].Gxsupplier%><br />
                       <%}
                                       }
                                   }
                               }
                               else
                               { %>
 <%=itinerary.PaymentGuaranteedBy%>
  <%} %>
 <%-- </div>--%>
                    </td>
                </tr>
            </table>
        </div>        
    

  
    
</asp:Content>
