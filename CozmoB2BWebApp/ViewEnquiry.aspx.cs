﻿using System;
using System.Data;
using CT.BookingEngine;

public partial class ViewEnquiryGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if(Request["EnquireId"] !=null)
            {
                int id = Convert.ToInt32(Request["EnquireId"]);
                DataTable dt = Enquiry.GetRemarks(id);
                if (dt.Rows.Count > 0)
                {
                    dlViewEnquiry.Visible = true;
                    lblMessage.Visible = false;
                    dlViewEnquiry.DataSource = dt;
                    dlViewEnquiry.DataBind();
                }
                else
                {
                    dlViewEnquiry.Visible = false;
                    lblMessage.Visible = true;
                    lblMessage.Text = "No Results Found.";
                }
            }
           
        }
    }
}
