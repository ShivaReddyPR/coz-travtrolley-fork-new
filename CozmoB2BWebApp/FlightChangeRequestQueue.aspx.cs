﻿using CT.AccountingEngine;
using CT.BookingEngine;
using CT.Configuration;
using CT.Core;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.UI.WebControls;

public partial class FlightChangeRequestQueueGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
    protected List<FlightItinerary> itinerary = new List<FlightItinerary>();
    protected BookingDetail[] bookingDetail = new BookingDetail[0];
    protected ServiceRequest[] serviceRequest = new ServiceRequest[0];
    protected Int64 agencyIdCSV;
    string bookingSourceCSV = null;
    string requestTypeCSV = null;
    string serviceRequestTypeCSV = null;
    int agencyTypeCSV;
    bool? isDomestic = null;
    protected List<int> requestTypes = new List<int>();
    protected List<int> requestStatuses = new List<int>();
    private int loggedMemberId;
    protected bool isAssignedRequest;
    protected CT.Core.Queue[] relevantQueues;
    
    protected string lastCanDate = string.Empty;
    protected string agentFilter = string.Empty;
    
    protected string paxFilter = null;
    protected string pnrFilter = null;

    protected List<ChangeRequestQueue> data = new List<ChangeRequestQueue>();
    protected int recordsPerPage;
    protected int noOfPages;
    protected int pageNo;
    protected int queueCount = 0;
    protected string airlineFilter, show;
    protected string requestSourceIdCSV = null;
    protected string ticketFilter = null;
    protected bool isAdmin = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            
            hdfParam.Value = "1";
            Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            if (Settings.LoginInfo != null)
            {

                if (PageNoString.Value != null)
                {
                    pageNo = Convert.ToInt16(PageNoString.Value);
                }
                else
                {
                    pageNo = 1;
                }

                if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId == 0)
                {
                    isAdmin = true;
                }
                recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["BookingQueueRecordsPerPage"]);
                if (!IsPostBack)
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                    txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    hdfParam.Value = "0";
                    InitializeControls();

                    //agentFilter = Settings.LoginInfo.AgentName;
                    if (Settings.LoginInfo.AgentId > 1)
                    {
                        //Added by lokesh on 14-07-2018
                        //Vinay raised this.
                        //Add agentId's for India B2B agents.
                        if (Settings.LoginInfo.AgentId == 187 || Settings.LoginInfo.AgentId == 755)
                        {
                            ddlAgency.Enabled = true;
                        }
                        else
                        {
                            ddlAgency.Enabled = false;
                        }
                    }
                    else
                    {
                        ddlAgency.Enabled = true;
                    }
                    
                    LoadChangeQueues();

                }
                else
                {
                    SetFilters();
                }

                // records per page from configuration
                //StaticData staticInfo = new StaticData();
                //rateOfExList = staticInfo.CurrencyROE;
                

            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }

        }
        catch (Exception ex)
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            Audit.Add(EventType.Ticketing, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    #region Agents and location dropdown binding methods.
    //Added by Anji on 04/12/19, reason B2BAgent and B2B2BAgent functionality implemenation.
    private void InitializeControls()
    {
        try
        {
            Array Statuses = Enum.GetValues(typeof(ServiceRequestStatus));
            foreach (ServiceRequestStatus status in Statuses)
            {
                ListItem item = new ListItem(Enum.GetName(typeof(ServiceRequestStatus), status), ((int)status).ToString());
                ddlBookingStatus.Items.Add(item);
            }
            BindAgent();
            if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER || Settings.LoginInfo.MemberType == MemberType.SUPERVISOR)
            {
                ddlLocation.Enabled = true;
            }
            else
            {
                ddlLocation.Enabled = false;
            }
            BindLocation();
            int b2bAgentId;
            int b2b2bAgentId;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgency.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgency.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgency.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }
            BindB2BAgent(Convert.ToInt32(ddlAgency.SelectedItem.Value));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }

        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    private void BindAgent()
    {
        try
        {
            DataTable dtAgent= AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgency.DataSource = dtAgent;
            ddlAgency.DataTextField = "agent_name";
            ddlAgency.DataValueField = "agent_id";
            ddlAgency.DataBind();
            ddlAgency.Items.Insert(0, new ListItem("--All--", "0"));

        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    private void BindLocation()
    {
        try
        {
            DataTable dtlocatin= LocationMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
            ddlLocation.DataSource = dtlocatin;
            ddlLocation.DataTextField = "location_name";
            ddlLocation.DataValueField = "location_id";
            ddlLocation.DataBind();
            ddlLocation.SelectedIndex = -1;
            ddlLocation.SelectedValue = Convert.ToString(Settings.LoginInfo.LocationID);

        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void BindLocation(int agentId, string type)
    {
        DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);

        ddlLocation.Items.Clear();
        ddlLocation.DataSource = dtLocations;
        ddlLocation.DataTextField = "location_name";
        ddlLocation.DataValueField = "location_id";
        ddlLocation.DataBind();

        ListItem item = new ListItem("All", "-1");
        ddlLocation.Items.Insert(0, item);
        hdfParam.Value = "0";
    }
 //End by Anji.
    #endregion

    void LoadChangeQueues()
    {
        try
        {
            //if (ddlAgency.SelectedIndex > 0)
            //{
            //    AgentMaster agent = new AgentMaster(Convert.ToInt32(ddlAgency.SelectedItem.Value));
            //    agencyTypeCSV = Convert.ToInt32((Agencytype)agent.AgentType);
            //    agencyIdCSV = agent.ID;
            //}

            //Added by Anji on 04/12/19, reason B2BAgent and B2B2BAgent functionality implemenation.
            string agentType = string.Empty;
            if (!IsPostBack)
            {
                agencyIdCSV = Utility.ToInteger(Settings.LoginInfo.AgentId);
            }
            else
            {


                agencyIdCSV = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                if (agencyIdCSV == 0)
                {
                    agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                    }
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = string.Empty; // null Means binding in list all BOOKINGS
                    }
                }
                if (agencyIdCSV > 0 && ddlB2BAgent.SelectedIndex > 0)
                {
                    if (Convert.ToInt32(ddlAgency.SelectedItem.Value) > 1)
                    {
                        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                        {
                            agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                        }
                        else
                        {
                            agencyIdCSV = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                        {
                            agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                        }
                        agencyIdCSV = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                if (agencyIdCSV > 0 && ddlB2B2BAgent.SelectedIndex > 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                    }
                    else
                    {
                        agencyIdCSV = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                    }
                }
                if (Convert.ToInt32(ddlAgency.SelectedItem.Value) != 0)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                        {
                            agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                            agencyIdCSV = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                        }
                    }
                }
            }
            //End by Anji.
            if (requestTypeCSV == null)
            {
                requestTypes.Add((int)RequestType.Void);
                requestTypes.Add((int)RequestType.Refund);
                requestTypes.Add((int)RequestType.Reissuance);
                //rfvoid = "Void";
                //refund = "Refund";
                requestTypeCSV = "1,2,3";
            }
            if (serviceRequestTypeCSV == null)
            {
                if (isAssignedRequest)
                {
                    requestStatuses.Add((int)ServiceRequestStatus.Unassigned);
                    requestStatuses.Add((int)ServiceRequestStatus.Assigned);
                    requestStatuses.Add((int)ServiceRequestStatus.Acknowledged);
                    requestStatuses.Add((int)ServiceRequestStatus.Closed);
                    requestStatuses.Add((int)ServiceRequestStatus.Pending);
                    requestStatuses.Add((int)ServiceRequestStatus.Rejected);
                    requestStatuses.Add((int)ServiceRequestStatus.Completed);
                    serviceRequestTypeCSV = "1,2,3,4,5,6,7";
                }
                //else
                //{
                //    serviceRequestTypeCSV = "1,2,3";
                //}
            }
            isDomestic = true;
            //agencyIdCSV =  Convert.ToInt64(ddlAgency.SelectedItem.Value);
            string endDate = txtToDate.Text;
            endDate += " 23:59:59";
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            data = CT.Core.Queue.GetFlightChangeRequestData(((pageNo - 1) * recordsPerPage) + 1, (pageNo * recordsPerPage), requestTypeCSV, serviceRequestTypeCSV, agencyTypeCSV, bookingSourceCSV, airlineFilter, agencyIdCSV, paxFilter, pnrFilter, ticketFilter, isDomestic, ref queueCount, requestSourceIdCSV, Convert.ToDateTime(txtFromDate.Text, dateFormat), Convert.ToDateTime(endDate, dateFormat));
            CT.Core.Queue[] queues = new CT.Core.Queue[data.Count];
            for (int j = 0; j < data.Count; j++)
            {
                queues[j] = new CT.Core.Queue();
                queues[j].QueueId = data[j].QueueId;
                queues[j].ItemId = data[j].RequestId;
            }
            AssignToVariables(queues);
            string url = "FlightChangeRequestQueue.aspx?pageType=bookingdate";
            if (queueCount > 0)
            {
                if ((queueCount % recordsPerPage) > 0)
                {
                    noOfPages = (queueCount / recordsPerPage) + 1;
                }
                else
                {
                    noOfPages = (queueCount / recordsPerPage);
                }
            }
            else
            {
                noOfPages = 0;
            }
            if (PageNoString.Value != null)
            {
                pageNo = Convert.ToInt16(PageNoString.Value);
            }
            else
            {
                pageNo = 1;
            }

            if (noOfPages > 0)
            {
                show = MetaSearchEngine.PagingJavascript(noOfPages, url, pageNo);
            }

        }
        catch (Exception ex)
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            Audit.Add(EventType.Ticketing, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get Change REquest Queue: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    private void AssignToVariables(CT.Core.Queue[] relevantQueues)
    {
        try
        {
            ArrayList listOfItemId = new ArrayList();
            int numBooking = 0;
            foreach (CT.Core.Queue queue in relevantQueues)
            {
                ServiceRequest sr = new ServiceRequest(queue.ItemId);
                listOfItemId.Add(queue.ItemId);
                numBooking++;
            }
            bookingDetail = new BookingDetail[numBooking];

            serviceRequest = new ServiceRequest[numBooking];
            for (int count = 0; count < listOfItemId.Count; count++)
            {
                ServiceRequest sRequest = new ServiceRequest(Convert.ToInt32(listOfItemId[count]));
                serviceRequest[count] = sRequest;
                BookingDetail bDetail = null;
                try
                {
                    bDetail = new BookingDetail(serviceRequest[count].BookingId);
                    bookingDetail[count] = bDetail;
                }
                catch { }
                Product[] products = BookingDetail.GetProductsLine(serviceRequest[count].BookingId);
                AgentMaster agency = new AgentMaster(bDetail.AgencyId);
                for (int i = 0; i < products.Length; i++)
                {
                    if (products[i] != null)
                    {
                        string productType = Enum.Parse(typeof(ProductType), products[i].ProductTypeId.ToString()).ToString();
                        switch (productType)
                        {
                            case "Flight":
                                FlightItinerary airItinerary = new FlightItinerary(products[i].ProductId);
                                
                                airItinerary.ProductId = products[i].ProductId;
                                airItinerary.ProductType = products[i].ProductType;
                                airItinerary.ProductTypeId = products[i].ProductTypeId;
                                airItinerary.SpecialRequest = sRequest.Data; // sai new property to be add in itinerary bo
                                
                                itinerary.Add(airItinerary);
                                break;
                            case "Car":
                                break;
                            case "Hotel": HotelItinerary itineary = new HotelItinerary();
                                break;
                            default:
                                break;
                        }
                    }
                }
            }


            Session["ServiceRequests"] = serviceRequest;
            dlChangeRequests.DataSource = itinerary;
            dlChangeRequests.DataBind();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.ChangeRequest, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    private void SetFilters()
    {

        if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
        {
            isAssignedRequest = true;
            isAdmin = true;
        }
        //Commented by Anji.
        //if (ddlAgency.SelectedIndex > 0)
        //{
        //    AgentMaster agent = new AgentMaster(Convert.ToInt32(ddlAgency.SelectedItem.Value));
        //    agencyTypeCSV = Convert.ToInt32((Agencytype)agent.AgentType);
        //    agencyIdCSV = agent.ID;
        //}
        if (txtPaxName.Text.Trim().Length > 0)
        {
            paxFilter = txtPaxName.Text;
        }
        if (txtPNR.Text.Trim().Length > 0)
        {
            pnrFilter = txtPNR.Text;
        }

        {
            if (requestTypeCSV == null)
            {
                requestTypeCSV = "1,2,3";
            }
        }
        if (Request["HotelAmendment"] != null)
        {
            if (requestTypeCSV == null)
            {
                requestTypeCSV = "3";
            }
            else
            {
                requestTypeCSV += ",5";
            }
        }

        switch (ddlBookingStatus.SelectedItem.Text)
        {
            case "Unassigned":
                serviceRequestTypeCSV = "1";
                break;
            case "Assigned":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "2";
                }
                else
                {
                    serviceRequestTypeCSV += ",2";
                }
                break;
            case "Acknowledged":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "3";
                }
                else
                {
                    serviceRequestTypeCSV += ",3";
                }
                break;
            case "Completed":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "4";
                }
                else
                {
                    serviceRequestTypeCSV += ",4";
                }
                break;
            case "Rejected":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "5";
                }
                else
                {
                    serviceRequestTypeCSV += ",5";
                }
                break;
            case "Closed":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "6";
                }
                else
                {
                    serviceRequestTypeCSV += ",6";
                }
                break;
            case "Pending":
                if (serviceRequestTypeCSV == null)
                {
                    serviceRequestTypeCSV = "7";
                }
                else
                {
                    serviceRequestTypeCSV += ",7";
                }
                break;
            case "All":
                serviceRequestTypeCSV = "1,2,3,4,5,6,7";
                break;
        }
       
        if (!string.IsNullOrEmpty(txtPaxName.Text))
        {
            paxFilter = txtPaxName.Text.Replace("'", "''").Trim();
            paxFilter = paxFilter.Replace(" ", "");
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LoadChangeQueues();
    }
    protected void dlChangeRequests_ItemCommand(object source, DataListCommandEventArgs e) // code for refunding.
    {
        try
        {
            BookingDetail booking = new BookingDetail();
            List<Ticket> ticketList = new List<Ticket>();

            TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
            TextBox txtSupplierFee = e.Item.FindControl("txtSupplierFee") as TextBox;
            Label lblError = e.Item.FindControl("lblError") as Label;
            if (e.CommandName == "Refund")
            {
                FlightItinerary itinerary = null;
                if (txtAdminFee.Text.Trim().Length > 0 && txtSupplierFee.Text.Trim().Length > 0)
                {
                    int flightId = Convert.ToInt32(e.CommandArgument);
                    itinerary = new FlightItinerary(flightId);
                    int bookingId = itinerary.BookingId;
                    booking = new BookingDetail(bookingId);
                    ticketList = Ticket.GetTicketList(flightId);
                    decimal bookingAmt = 0;
                    //decimal discount = 0;
                    foreach (FlightPassenger passenger in itinerary.Passenger)
                    {
                        if (passenger.Price.PublishedFare > 0)
                        {
                            bookingAmt += passenger.Price.PublishedFare + passenger.Price.Tax + passenger.Price.Markup + passenger.Price.BaggageCharge + passenger.Price.MealCharge + passenger.Price.SeatPrice; //added only b2cMarkup by chandan
                        }
                        else
                        {
                            bookingAmt += passenger.Price.NetFare + passenger.Price.Tax + passenger.Price.BaggageCharge + passenger.Price.MealCharge + passenger.Price.SeatPrice;
                        }
                        if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                        {
                            bookingAmt += passenger.Price.AdditionalTxnFee + passenger.Price.OtherCharges + passenger.Price.SServiceFee + passenger.Price.TransactionFee;
                        }
                        //discount += passenger.Price.Discount;
                    }
                    //bookingAmt -= discount;
                    //bookingAmt = Math.Ceiling(bookingAmt);

                    //serviceRequest = Session["ServiceRequests"] as ServiceRequest[];
                    Dictionary<string, string> CancellationData = new Dictionary<string, string>();
                    if (itinerary.FlightBookingSource == BookingSource.SpiceJet)
                    {
                        CT.BookingEngine.GDS.SpiceJetAPIV1 spiceJet = new CT.BookingEngine.GDS.SpiceJetAPIV1();

                        SourceDetails agentDetails = new SourceDetails();

                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            spiceJet.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            spiceJet.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                            spiceJet.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"];

                            spiceJet.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"].UserID;
                            spiceJet.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"].Password;
                            spiceJet.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SG"].HAP;
                            spiceJet.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            spiceJet.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        }
                        else
                        {
                            spiceJet.AgentBaseCurrency = Settings.LoginInfo.Currency;
                            spiceJet.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            spiceJet.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                            agentDetails = Settings.LoginInfo.AgentSourceCredentials["SG"];

                            spiceJet.LoginName = Settings.LoginInfo.AgentSourceCredentials["SG"].UserID;
                            spiceJet.Password = Settings.LoginInfo.AgentSourceCredentials["SG"].Password;
                            spiceJet.AgentDomain = Settings.LoginInfo.AgentSourceCredentials["SG"].HAP;
                            spiceJet.AgentBaseCurrency = Settings.LoginInfo.Currency;
                            spiceJet.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        }

                        //Added by Lokesh
                        //For India GST Implementation.
                        AgentMaster agentMaster = null;
                        if (Settings.LoginInfo != null && Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            agentMaster = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                            if (agentMaster != null && !string.IsNullOrEmpty(agentMaster.AgentCurrency) && agentMaster.AgentCurrency.Length > 0 && agentMaster.AgentCurrency == "INR")
                            {
                                spiceJet.CurrencyCode = "INR";
                            }
                            else
                            {
                                spiceJet.CurrencyCode = "AED";
                            }
                        }
                        else
                        {
                            if (Settings.LoginInfo != null && !string.IsNullOrEmpty(Settings.LoginInfo.Currency) && Settings.LoginInfo.Currency.Length > 0 && Settings.LoginInfo.Currency == "INR")
                            {
                                spiceJet.CurrencyCode = "INR";
                            }
                            else
                            {
                                spiceJet.CurrencyCode = "AED";
                            }
                        }
                        //spiceJet.LoginName = agentDetails.UserID;
                        //spiceJet.Password = agentDetails.Password;
                        if (Session["sessionId"] != null)
                        {
                            spiceJet.SessionID = Session["sessionId"].ToString();
                        }
                        else
                        {
                            spiceJet.SessionID = Guid.NewGuid().ToString();
                        }
                        spiceJet.BookingSourceFlag = "SG";
                        spiceJet.AppUserID = (int)Settings.LoginInfo.UserID;
                        CancellationData = spiceJet.CancelBooking(itinerary);
                    }


                    else if (itinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
                    {
                        CT.BookingEngine.GDS.SpiceJetAPIV1 spiceJet = new CT.BookingEngine.GDS.SpiceJetAPIV1();

                        SourceDetails agentDetails = new SourceDetails();

                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            spiceJet.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            spiceJet.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                            spiceJet.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"];

                            spiceJet.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"].UserID;
                            spiceJet.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"].Password;
                            spiceJet.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials["SGCORP"].HAP;
                            spiceJet.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            spiceJet.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        }
                        else
                        {
                            spiceJet.AgentBaseCurrency = Settings.LoginInfo.Currency;
                            spiceJet.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            spiceJet.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                            agentDetails = Settings.LoginInfo.AgentSourceCredentials["SGCORP"];

                            spiceJet.LoginName = Settings.LoginInfo.AgentSourceCredentials["SGCORP"].UserID;
                            spiceJet.Password = Settings.LoginInfo.AgentSourceCredentials["SGCORP"].Password;
                            spiceJet.AgentDomain = Settings.LoginInfo.AgentSourceCredentials["SGCORP"].HAP;
                            spiceJet.AgentBaseCurrency = Settings.LoginInfo.Currency;
                            spiceJet.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        }

                        //Added by Lokesh
                        //For India GST Implementation.
                        AgentMaster agentMaster = null;
                        if (Settings.LoginInfo != null && Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            agentMaster = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                            if (agentMaster != null && !string.IsNullOrEmpty(agentMaster.AgentCurrency) && agentMaster.AgentCurrency.Length > 0 && agentMaster.AgentCurrency == "INR")
                            {
                                spiceJet.CurrencyCode = "INR";
                            }
                            else
                            {
                                spiceJet.CurrencyCode = "AED";
                            }
                        }
                        else
                        {
                            if (Settings.LoginInfo != null && !string.IsNullOrEmpty(Settings.LoginInfo.Currency) && Settings.LoginInfo.Currency.Length > 0 && Settings.LoginInfo.Currency == "INR")
                            {
                                spiceJet.CurrencyCode = "INR";
                            }
                            else
                            {
                                spiceJet.CurrencyCode = "AED";
                            }
                        }
                        //spiceJet.LoginName = agentDetails.UserID;
                        //spiceJet.Password = agentDetails.Password;
                        if (Session["sessionId"] != null)
                        {
                            spiceJet.SessionID = Session["sessionId"].ToString();
                        }
                        else
                        {
                            spiceJet.SessionID = Guid.NewGuid().ToString();
                        }
                        spiceJet.BookingSourceFlag = "SGCORP";
                        CancellationData = spiceJet.CancelBooking(itinerary);
                    }
                    //Code added by lokesh on 10/10/2016.
                    //New Source Indigo Integration.
                    else if (itinerary.FlightBookingSource == BookingSource.Indigo)
                    {
                        CT.BookingEngine.GDS.IndigoAPI objIndigo = new CT.BookingEngine.GDS.IndigoAPI();

                        SourceDetails agentDetails = new SourceDetails();

                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            objIndigo.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            objIndigo.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                            objIndigo.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"];

                            objIndigo.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"].UserID;
                            objIndigo.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"].Password;
                            objIndigo.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6E"].HAP;
                        }
                        else
                        {
                            objIndigo.AgentBaseCurrency = Settings.LoginInfo.Currency;
                            objIndigo.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            objIndigo.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                            agentDetails = Settings.LoginInfo.AgentSourceCredentials["6E"];

                            objIndigo.LoginName = Settings.LoginInfo.AgentSourceCredentials["6E"].UserID;
                            objIndigo.Password = Settings.LoginInfo.AgentSourceCredentials["6E"].Password;
                            objIndigo.AgentDomain = Settings.LoginInfo.AgentSourceCredentials["6E"].HAP;
                        }

                        AgentMaster agentMaster = null;
                        if (Settings.LoginInfo != null && Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            agentMaster = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                            if (agentMaster != null && !string.IsNullOrEmpty(agentMaster.AgentCurrency) && agentMaster.AgentCurrency.Length > 0 && agentMaster.AgentCurrency == "INR")
                            {
                                objIndigo.CurrencyCode = "INR";
                            }
                            else
                            {
                                objIndigo.CurrencyCode = "AED";
                            }
                        }
                        else
                        {
                            if (Settings.LoginInfo != null && !string.IsNullOrEmpty(Settings.LoginInfo.Currency) && Settings.LoginInfo.Currency.Length > 0 && Settings.LoginInfo.Currency == "INR")
                            {
                                objIndigo.CurrencyCode = "INR";
                            }
                            else
                            {
                                objIndigo.CurrencyCode = "AED";
                            }
                        }
                        objIndigo.AppUserId = (int)Settings.LoginInfo.UserID;
                        if (Session["sessionId"] != null)
                        {
                            objIndigo.SessionId = Session["sessionId"].ToString();
                        }
                        else
                        {
                            objIndigo.SessionId = Guid.NewGuid().ToString();
                        }
                        objIndigo.BookingSourceFlag = "6E";
                        CancellationData = objIndigo.CancelBooking(itinerary);
                    }

                    else if (itinerary.FlightBookingSource == BookingSource.IndigoCorp)
                    {
                        CT.BookingEngine.GDS.IndigoAPI objIndigo = new CT.BookingEngine.GDS.IndigoAPI();

                        SourceDetails agentDetails = new SourceDetails();

                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            objIndigo.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            objIndigo.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                            objIndigo.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"];

                            objIndigo.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"].UserID;
                            objIndigo.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"].Password;
                            objIndigo.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials["6ECORP"].HAP;
                        }
                        else
                        {
                            objIndigo.AgentBaseCurrency = Settings.LoginInfo.Currency;
                            objIndigo.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            objIndigo.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                            agentDetails = Settings.LoginInfo.AgentSourceCredentials["6ECORP"];

                            objIndigo.LoginName = Settings.LoginInfo.AgentSourceCredentials["6ECORP"].UserID;
                            objIndigo.Password = Settings.LoginInfo.AgentSourceCredentials["6ECORP"].Password;
                            objIndigo.AgentDomain = Settings.LoginInfo.AgentSourceCredentials["6ECORP"].HAP;
                        }

                        AgentMaster agentMaster = null;
                        if (Settings.LoginInfo != null && Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            agentMaster = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                            if (agentMaster != null && !string.IsNullOrEmpty(agentMaster.AgentCurrency) && agentMaster.AgentCurrency.Length > 0 && agentMaster.AgentCurrency == "INR")
                            {
                                objIndigo.CurrencyCode = "INR";
                            }
                            else
                            {
                                objIndigo.CurrencyCode = "AED";
                            }
                        }
                        else
                        {
                            if (Settings.LoginInfo != null && !string.IsNullOrEmpty(Settings.LoginInfo.Currency) && Settings.LoginInfo.Currency.Length > 0 && Settings.LoginInfo.Currency == "INR")
                            {
                                objIndigo.CurrencyCode = "INR";
                            }
                            else
                            {
                                objIndigo.CurrencyCode = "AED";
                            }
                        }
                        objIndigo.AppUserId = (int)Settings.LoginInfo.UserID;
                        if (Session["sessionId"] != null)
                        {
                            objIndigo.SessionId = Session["sessionId"].ToString();
                        }
                        else
                        {
                            objIndigo.SessionId = Guid.NewGuid().ToString();
                        }
                        objIndigo.BookingSourceFlag = "6ECORP";
                        CancellationData = objIndigo.CancelBooking(itinerary);
                    }

                    //Code added by Lokesh on 06/02/2017
                    //The below code is for cancelling the air india express ticket.
                    else if (itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl)
                    {

                        CT.BookingEngine.GDS.AirIndiaExpressApi airIndiaExpress = new CT.BookingEngine.GDS.AirIndiaExpressApi();
                        SourceDetails agentDetails = new SourceDetails();
                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            airIndiaExpress.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            airIndiaExpress.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                            airIndiaExpress.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["IX"];
                        }
                        else
                        {
                            airIndiaExpress.AgentBaseCurrency = Settings.LoginInfo.Currency;
                            airIndiaExpress.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            airIndiaExpress.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                            agentDetails = Settings.LoginInfo.AgentSourceCredentials["IX"];
                        }
                        airIndiaExpress.LogonId = agentDetails.UserID;
                        airIndiaExpress.Password = agentDetails.Password;
                        if (Session["sessionId"] != null)
                        {
                            airIndiaExpress.SessionId = Session["sessionId"].ToString();
                        }
                        else
                        {
                            airIndiaExpress.SessionId = Guid.NewGuid().ToString();
                        }
                        CancellationData = airIndiaExpress.CancelBooking(itinerary);
                    }

                    //GoAir Cancellation
                    else if (itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource==BookingSource.GoAirCorp)
                    {
                        CT.BookingEngine.GDS.GoAirAPI goAirObj = new CT.BookingEngine.GDS.GoAirAPI();
                        if(itinerary.FlightBookingSource==BookingSource.GoAir)
                        {
                            goAirObj.BookingSourceFlag = "G8";
                        }
                        else
                        {
                            goAirObj.BookingSourceFlag = "G8CORP";
                        }

                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            goAirObj.LoginName = Settings.LoginInfo.OnBehalfAgentSourceCredentials[goAirObj.BookingSourceFlag].UserID;
                            goAirObj.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials[goAirObj.BookingSourceFlag].Password;
                            goAirObj.AgentDomain = Settings.LoginInfo.OnBehalfAgentSourceCredentials[goAirObj.BookingSourceFlag].HAP;
                            goAirObj.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            goAirObj.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                            goAirObj.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                            goAirObj.PromoCode = Settings.LoginInfo.OnBehalfAgentSourceCredentials[goAirObj.BookingSourceFlag].PCC;//Added For Corporate booking Purpose
                        }
                        else
                        {
                            goAirObj.LoginName = Settings.LoginInfo.AgentSourceCredentials[goAirObj.BookingSourceFlag].UserID;
                            goAirObj.Password = Settings.LoginInfo.AgentSourceCredentials[goAirObj.BookingSourceFlag].Password;
                            goAirObj.AgentDomain = Settings.LoginInfo.AgentSourceCredentials[goAirObj.BookingSourceFlag].HAP;
                            goAirObj.AgentBaseCurrency = Settings.LoginInfo.Currency;
                            goAirObj.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            goAirObj.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                            goAirObj.PromoCode = Settings.LoginInfo.AgentSourceCredentials[goAirObj.BookingSourceFlag].PCC;//Added For Corporate booking Purpose
                        }

                        //Added by Lokesh
                        //For India GST Implementation.
                        AgentMaster agentMaster = null;
                        if (Settings.LoginInfo != null && Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            agentMaster = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                            if (agentMaster != null && !string.IsNullOrEmpty(agentMaster.AgentCurrency) && agentMaster.AgentCurrency.Length > 0 && agentMaster.AgentCurrency == "INR")
                            {
                                goAirObj.CurrencyCode = "INR";
                            }
                            else
                            {
                                goAirObj.CurrencyCode = "AED";
                            }
                        }
                        else
                        {
                            if (Settings.LoginInfo != null && !string.IsNullOrEmpty(Settings.LoginInfo.Currency) && Settings.LoginInfo.Currency.Length > 0 && Settings.LoginInfo.Currency == "INR")
                            {
                                goAirObj.CurrencyCode = "INR";//For Indian Agency
                            }
                            else
                            {
                                goAirObj.CurrencyCode = "AED";
                            }
                        }
                        if (Session["sessionId"] != null)
                        {
                            goAirObj.SessionId = Session["sessionId"].ToString();
                        }
                        else
                        {
                            goAirObj.SessionId = Guid.NewGuid().ToString();
                        }
                        goAirObj.AppUserId = (int)Settings.LoginInfo.UserID;
                        CancellationData = goAirObj.CancelBooking(itinerary);
                    }



                    else if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        TBOAir.AirV10 tboAir = new TBOAir.AirV10();

                        SourceDetails agentDetails = new SourceDetails();

                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            tboAir.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            tboAir.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                            tboAir.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;

                            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["TA"];
                        }
                        else
                        {
                            tboAir.AgentBaseCurrency = Settings.LoginInfo.Currency;
                            tboAir.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            tboAir.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                            agentDetails = Settings.LoginInfo.AgentSourceCredentials["TA"];
                        }

                        tboAir.LoginName = agentDetails.UserID;
                        tboAir.Password = agentDetails.Password;

                        CancellationData = tboAir.SendChangeRequest(itinerary);
                    }

                    //Added by Lokesh on 01-June-2018.
                    //Amadeus Air Source Void Process.
                    else if (itinerary.FlightBookingSource == BookingSource.Amadeus)
                    {
                        CT.BookingEngine.GDS.AmadeusAPI amadeusAPI = new CT.BookingEngine.GDS.AmadeusAPI();
                        SourceDetails agentDetails = new SourceDetails();
                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["1A"];
                            amadeusAPI.AgentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            amadeusAPI.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                            amadeusAPI.AgentDecimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                            amadeusAPI.UserName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["1A"].UserID;
                            amadeusAPI.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["1A"].Password;
                            amadeusAPI.OfficeId = Settings.LoginInfo.OnBehalfAgentSourceCredentials["1A"].PCC;
                            amadeusAPI.MarketIataCode = Settings.LoginInfo.OnBehalfAgentSourceCredentials["1A"].HAP;
                        }
                        else
                        {
                            agentDetails = Settings.LoginInfo.AgentSourceCredentials["1A"];
                            amadeusAPI.AgentBaseCurrency = Settings.LoginInfo.Currency;
                            amadeusAPI.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            amadeusAPI.AgentDecimalValue = Settings.LoginInfo.DecimalValue;
                            amadeusAPI.UserName = Settings.LoginInfo.AgentSourceCredentials["1A"].UserID;
                            amadeusAPI.Password = Settings.LoginInfo.AgentSourceCredentials["1A"].Password;
                            amadeusAPI.OfficeId = Settings.LoginInfo.AgentSourceCredentials["1A"].PCC;
                            amadeusAPI.MarketIataCode = Settings.LoginInfo.AgentSourceCredentials["1A"].HAP;
                        }
                        amadeusAPI.AppUserID = Convert.ToInt32(Settings.LoginInfo.UserID.ToString());
                        amadeusAPI.SessionID = Guid.NewGuid().ToString();
                        CancellationData =  amadeusAPI.CancelBooking(itinerary);
                    }





                        //bookingAmt = Math.Ceiling(bookingAmt);

                        //serviceRequest = Session["ServiceRequests"] as ServiceRequest[];

                        loggedMemberId = (int)Settings.LoginInfo.UserID;
                    ServiceRequest sr = new ServiceRequest();

                    //TODO: Create a PaymentDetails for cancellation details and update the cancellation charges: Shiva

                    if (CancellationData.ContainsKey("Cancelled") && CancellationData["Cancelled"] == "True")
                    {
                        if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                        {
                            bookingAmt = Math.Ceiling(bookingAmt);
                        }

                        if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                        {
                            bookingAmt -= Convert.ToDecimal(CancellationData["Charges"]);
                            bookingAmt = Math.Ceiling(bookingAmt);
                        }
                        else
                        {
                            bookingAmt -= Convert.ToDecimal(CancellationData["Charges"]);
                        }
                    }
                    //SpiceJet & TBOAir Cancellation will be done by xml request so update booking status with Ledger only when Cancelled online
                    //Rest all we are doing manual cancellation from backend so update booking status with Ledger directly
                    if (((itinerary.FlightBookingSource == BookingSource.Indigo || 
                        itinerary.FlightBookingSource == BookingSource.SpiceJet || 
                        itinerary.FlightBookingSource == BookingSource.TBOAir) && CancellationData.ContainsKey("Cancelled") && CancellationData["Cancelled"] == "True") ||
                        itinerary.FlightBookingSource == BookingSource.AirArabia || 
                        itinerary.FlightBookingSource == BookingSource.UAPI || 
                        itinerary.FlightBookingSource == BookingSource.FlyDubai || 
                        itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || 
                        itinerary.FlightBookingSource == BookingSource.PKFares ||
                        itinerary.FlightBookingSource == BookingSource.Amadeus ||
                        itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || 
                        itinerary.FlightBookingSource == BookingSource.IndigoCorp ||
                        itinerary.FlightBookingSource == BookingSource.GoAir || 
                        itinerary.FlightBookingSource == BookingSource.Jazeera || 
                        itinerary.FlightBookingSource == BookingSource.GoAirCorp ||
                        itinerary.FlightBookingSource == BookingSource.Babylon ||
                        itinerary.FlightBookingSource==BookingSource.FlightInventory)
                    {
                        CT.BookingEngine.CancellationCharges cancellationCharge = new CT.BookingEngine.CancellationCharges();
                        cancellationCharge.AdminFee = Convert.ToDecimal(txtAdminFee.Text);
                        cancellationCharge.SupplierFee = Convert.ToDecimal(txtSupplierFee.Text);
                        cancellationCharge.PaymentDetailId = 0;//pd.PaymentDetailId;
                        cancellationCharge.ReferenceId = ticketList[0].TicketId;
                        cancellationCharge.CancelPenalty = bookingAmt;
                        cancellationCharge.CreatedBy = loggedMemberId;
                        cancellationCharge.ProductType = ProductType.Flight;
                        cancellationCharge.Save();

                        BookingHistory bh = new BookingHistory();
                        bh.BookingId = bookingId;
                        bh.EventCategory = EventCategory.Ticketing;
                        bh.Remarks = "Flight booking refunded. PNR NO-" + itinerary.PNR;
                        bh.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);

                        string reqType = string.Empty;
                        using (System.Transactions.TransactionScope setTransaction = new System.Transactions.TransactionScope())
                        {
                            try
                            {
                                if (booking.Status == BookingStatus.VoidInProgress)
                                {
                                    booking.SetBookingStatus(BookingStatus.Void, Convert.ToInt32(Settings.LoginInfo.UserID));
                                    reqType = "Void";
                                }
                                else if (booking.Status == BookingStatus.RefundInProgress)
                                {
                                    booking.SetBookingStatus(BookingStatus.Refunded, Convert.ToInt32(Settings.LoginInfo.UserID));
                                    reqType = "Refund";
                                }
                                else if (booking.Status == BookingStatus.ModificationInProgress)
                                {
                                    booking.SetBookingStatus(BookingStatus.Modified, Convert.ToInt32(Settings.LoginInfo.UserID));
                                    reqType = "Modification";
                                }
                                bh.Save();
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            setTransaction.Complete();
                        }


                        decimal adminChar = Convert.ToDecimal(txtSupplierFee.Text) + Convert.ToDecimal(txtAdminFee.Text);
                        decimal adminFee = 0;
                        if (adminChar < bookingAmt)
                        {
                            adminFee = adminChar;
                        }
                        ////if (adminFee <= bookingAmt)
                        ////{
                        ////    bookingAmt -= adminFee;
                        ////}
                        //AgentMaster agent = new AgentMaster(booking.AgencyId);
                        //{
                        //    //Settings.LoginInfo.AgentBalance += bookingAmt;
                        //    agent.CreatedBy = Settings.LoginInfo.UserID;
                        //    agent.UpdateBalance(bookingAmt);
                        //}
                        //if (booking.AgencyId == Settings.LoginInfo.AgentId)
                        //{
                        //    Settings.LoginInfo.AgentBalance += bookingAmt;
                        //}
                        serviceRequest = Session["ServiceRequests"] as ServiceRequest[];
                        //Update Queue Status
                        CT.Core.Queue.SetStatus(QueueType.Request, serviceRequest[e.Item.ItemIndex].RequestId, QueueStatus.Completed, loggedMemberId, 0, "Completed");
                        sr.UpdateServiceRequestAssignment(serviceRequest[e.Item.ItemIndex].RequestId, (int)ServiceRequestStatus.Completed, loggedMemberId, (int)ServiceRequestStatus.Completed, null);

                        // Admin & Supplier Fee save in Leadger
                        int invoiceNumber = 0;
                        LedgerTransaction ledgerTxn = new LedgerTransaction();
                        NarrationBuilder objNarration = new NarrationBuilder();
                        invoiceNumber = Invoice.isInvoiceGenerated(ticketList[0].TicketId);
                        Invoice invoice = new Invoice();
                        invoice.Load(invoiceNumber);
                        //ledgerTxn = new LedgerTransaction();
                        ledgerTxn.LedgerId = booking.AgencyId;
                        //Commented by Shiva 09 Nov 2018, as per Vinay only adminFee should be added to Ledger
                        //if (CancellationData != null && CancellationData.ContainsKey("Charges"))
                        //{
                        //    ledgerTxn.Amount = -(adminFee + Convert.ToDecimal(CancellationData["Charges"]));
                        //}
                        //else
                        {
                            ledgerTxn.Amount = -(adminFee);
                        }
                        objNarration.DocNo = invoice.DocTypeCode + invoice.DocumentNumber.ToString();
                        objNarration.TicketNo = itinerary.PNR.Replace("|", "-");
                        objNarration.TravelDate = itinerary.TravelDate.ToShortDateString();
                        objNarration.Remarks = "Flight " + reqType + " Charges";
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.IsLCC = true;
                        ledgerTxn.ReferenceId = ticketList[0].TicketId;
                        if (itinerary.PaymentMode == ModeOfPayment.Credit || itinerary.PaymentMode == ModeOfPayment.CreditLimit)
                        {
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.TicketCancellationCharge;
                        }
                        else if (itinerary.PaymentMode == ModeOfPayment.CreditCard)
                        {
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CardTicketCancellationCharge;
                        }
                        ledgerTxn.Notes = "PNR:" + itinerary.PNR.Replace("|", "-");
                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = loggedMemberId;
                        ledgerTxn.TransType = itinerary.TransactionType;
                        ledgerTxn.PaymentMode = Convert.ToInt32(itinerary.PaymentMode);
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                        //save Refund amount
                        ledgerTxn = new LedgerTransaction();
                        ledgerTxn.LedgerId = booking.AgencyId;
                        ledgerTxn.Amount = bookingAmt;
                        objNarration.PaxName = string.Format("{0} {1}", itinerary.Passenger[0].FirstName, itinerary.Passenger[0].LastName);
                        if (itinerary.PaymentMode == ModeOfPayment.CreditCard )
                        {
                            objNarration.Remarks = "Card Refunded for PNR No -" + itinerary.PNR.Replace("|", "-");
                        }
                        else if (itinerary.PaymentMode == ModeOfPayment.Credit || itinerary.PaymentMode == ModeOfPayment.CreditLimit)
                        {
                            objNarration.Remarks = "Refunded for PNR No -" + itinerary.PNR.Replace("|", "-");
                        }
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.PaymentMode = Convert.ToInt32(itinerary.PaymentMode);
                        //ledgerTxn.IsLCC = true;
                        ledgerTxn.ReferenceId = ticketList[0].TicketId;
                        if (booking.Status == BookingStatus.Void)
                        {
                            if (itinerary.PaymentMode == ModeOfPayment.Credit || itinerary.PaymentMode == ModeOfPayment.CreditLimit)
                            {
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.TicketVoid;
                            }
                            else if (itinerary.PaymentMode == ModeOfPayment.CreditCard)
                            {
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CardTicketVoid;
                            }
                        }
                        else if (booking.Status == BookingStatus.Refunded)
                        {
                            if (itinerary.PaymentMode == ModeOfPayment.Credit || itinerary.PaymentMode == ModeOfPayment.CreditLimit)
                            {
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.TicketRefund;
                            }
                            else if (itinerary.PaymentMode == ModeOfPayment.CreditCard)
                            {
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CardTicketRefund;
                            }
                        }
                        else if (booking.Status == BookingStatus.Modified)
                        {
                            if (itinerary.PaymentMode == ModeOfPayment.Credit || itinerary.PaymentMode == ModeOfPayment.CreditLimit)
                            {
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.TicketModification;
                            }
                            else if (itinerary.PaymentMode == ModeOfPayment.CreditCard)
                            {
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CardTicketModification;
                            }
                        }

                        ledgerTxn.Notes = "Flight PNR" + booking.Status.ToString();
                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = loggedMemberId;
                        ledgerTxn.TransType = itinerary.TransactionType;
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                        // updating agent balance and setting object
                        if (adminFee <= bookingAmt)
                        {
                            bookingAmt -= adminFee;
                        }
                        AgentMaster agent = new AgentMaster(booking.AgencyId);
                        //If payment is made thru Credit then credit the agent balance otherwise DO NOT ADD AGENT BALANCE
                        if (itinerary.TransactionType == "B2B" && (itinerary.PaymentMode == ModeOfPayment.Credit || itinerary.PaymentMode == ModeOfPayment.CreditLimit))
                        {
                            //Settings.LoginInfo.AgentBalance += bookingAmt;
                            agent.CreatedBy = Settings.LoginInfo.UserID;
                            agent.UpdateBalance(bookingAmt);

                            if (booking.AgencyId == Settings.LoginInfo.AgentId)
                            {
                                Settings.LoginInfo.AgentBalance += bookingAmt;
                            }
                        }


                        //Sending Email.
                        Hashtable table = new Hashtable();
                        table.Add("agentName", agent.Name);
                        table.Add("pnr", itinerary.PNR);
                        table.Add("reqType", reqType);

                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                        UserMaster bookedBy = new UserMaster(itinerary.CreatedBy);
                        AgentMaster bookedAgency = new AgentMaster(itinerary.AgencyId);
                        toArray.Add(bookedBy.Email);
                        toArray.Add(bookedAgency.Email1);
                        //toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
                        //string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["HOTEL_CANCEL_MAIL"]).Split(';');
                        //foreach (string cnMail in cancelMails)
                        //{
                        //    toArray.Add(cnMail);
                        //}
                        //string message = "Your request for cancelling hotel <b>" + itineary.HotelName + " </b> has been processed. Confirmation No:(" + itineary.ConfirmationNo + ")";
                        string message = ConfigurationManager.AppSettings["FLIGHT_REFUND"];
                        try
                        {
                            //CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Flight)Response for " + reqType + ". PNR No:(" + itinerary.PNR + ")", message, table);
                            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail       "], toArray, "(Flight)Response for " + reqType + ". PNR No:(" + itinerary.PNR + ")", message, table);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Email, Severity.High, 1, "Failed to send Flight Change Request Email. Reason : " + ex.ToString(), Request["REMOTE_ADDR"]);
                        }
                        Response.Redirect("FlightChangeRequestQueue.aspx", false);
                    }
                    else if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        lblError.Text = "Pending from Supplier.";
                    }                    
                }
                else
                {
                    lblError.Text = "Please enter Admin & Supplier Fee";
                }
            }
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message, "0");
        }
    }
    protected void dlChangeRequests_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                FlightItinerary itinerary = e.Item.DataItem as FlightItinerary;

                if (itinerary != null)
                {
                    Label lblAgencyName = e.Item.FindControl("lblAgencyName") as Label;
                    Label lblAgencyBalance = e.Item.FindControl("lblAgencyBalance") as Label;
                    Label lblAgencyPhone1 = e.Item.FindControl("lblAgencyPhone1") as Label;
                    Label lblAgencyPhone2 = e.Item.FindControl("lblAgencyPhone2") as Label;
                    Label lblAgencyEmail = e.Item.FindControl("lblAgencyEmail") as Label;
                    Label lblAgentName = e.Item.FindControl("lblAgentName") as Label;
                    Label lblAdultCount = e.Item.FindControl("lblAdultCount") as Label;

                    Label lblBookingSource = e.Item.FindControl("lblBookingSource") as Label;
                    Label lblPNR = e.Item.FindControl("lblPNR") as Label;
                    Label lblBookingId = e.Item.FindControl("lblBookingId") as Label;
                    Label lblDayString = e.Item.FindControl("lblDayString") as Label;
                    Label lblTimeString = e.Item.FindControl("lblTimeString") as Label;
                    Label lblLastTicketingDate = e.Item.FindControl("lblLastTicketingDate") as Label;
                    Label lblBookingStatus = e.Item.FindControl("lblBookingStatus") as Label;
                    Label lblSent = e.Item.FindControl("lblSent") as Label;
                    Label lblAirline = e.Item.FindControl("lblAirline") as Label;
                    Label lblFlightNumber = e.Item.FindControl("lblFlightNumber") as Label;
                    Label lblDepartureTime = e.Item.FindControl("lblDepartureTime") as Label;
                    Label lblItineraryString = e.Item.FindControl("lblItineraryString") as Label;
                    Label lblInstantPurchase = e.Item.FindControl("lblInstantPurchase") as Label;
                    Label lblLeadPaxName = e.Item.FindControl("lblLeadPaxName") as Label;
                    Label lblOtherPaxName = e.Item.FindControl("lblOtherPaxName") as Label;

                    
                    Label lblBookingDate = e.Item.FindControl("lblBookingDate") as Label;
                    Label lblTrip = e.Item.FindControl("lblTrip") as Label;
                    Label lblTotal = e.Item.FindControl("lblTotal") as Label;
                    Label lblTicketNo = e.Item.FindControl("lblTicketNo") as Label;
                    HiddenField hdnIndex = e.Item.FindControl("hdnIndex") as HiddenField;
                    Label lblLocation = e.Item.FindControl("lblLocation") as Label;
                    Button btnRefund = e.Item.FindControl("btnRefund") as Button;
                    Label lblAdminCurrency = e.Item.FindControl("lblAdminCurrency") as Label;
                    Label lblSupplierCurrency = e.Item.FindControl("lblSupplierCurrency") as Label;
                    AgentMaster agent = new AgentMaster(itinerary.AgencyId);
                    
                    btnRefund.OnClientClick = "return Validate('" + e.Item.ItemIndex + "');";
                    
                    lblAdminCurrency.Text = agent.AgentCurrency;
                    lblSupplierCurrency.Text = agent.AgentCurrency;
                    lblAgencyBalance.Text = agent.AgentCurrency + " " + agent.CurrentBalance.ToString("N" + agent.DecimalValue);
                    lblAgencyEmail.Text = agent.Email1;
                    lblAgencyName.Text = agent.Name;
                    lblAgencyPhone1.Text = agent.Phone1;
                    lblAgencyPhone2.Text = agent.Phone2;
                    lblAgentName.Text = agent.Name;

                    hdnIndex.Value = e.Item.ItemIndex.ToString();

                    lblPNR.Text = itinerary.PNR;
                    lblPNR.Font.Bold = true;


                    lblLocation.Text = itinerary.LocationName;

                    string daystring = Util.GetDayString(bookingDetail[e.Item.ItemIndex].CreatedOn);
                    string timestring = "";
                    if (daystring == "Today")
                    {
                        TimeSpan tspan = DateTime.UtcNow - bookingDetail[e.Item.ItemIndex].CreatedOn;
                        string time = tspan.Hours.ToString("00") + ":" + tspan.Minutes.ToString("00");
                        timestring = time + " hours Ago " + Util.GetTimeString(Util.UTCToIST(bookingDetail[e.Item.ItemIndex].CreatedOn));
                    }
                    else
                    {
                        timestring = Util.GetTimeString(Util.UTCToIST(bookingDetail[e.Item.ItemIndex].CreatedOn));
                    }

                    if (daystring == "Today" || daystring == "Yesterday")
                    {
                        lblDayString.Text = daystring + " " + Util.GetOnlyDayMon(Util.UTCToIST(bookingDetail[e.Item.ItemIndex].CreatedOn));
                        lblTimeString.Text = timestring;
                    }
                    else
                    {
                        lblDayString.Text = daystring;
                        lblTimeString.Text = timestring;
                    }
                    lblBookingDate.Text = bookingDetail[e.Item.ItemIndex].CreatedOn.ToString("dd MMM yyyy");

                    string lastTicketingDate = string.Empty;
                    if (itinerary.LastTicketDate != null && itinerary.LastTicketDate != DateTime.MinValue)
                    {
                        lastTicketingDate = itinerary.LastTicketDate.ToString("dd MMM yy");
                    }

                    lblBookingStatus.Text = bookingDetail[e.Item.ItemIndex].Status.ToString();
                    lblBookingStatus.Font.Bold = true;

                    lblAirline.Text = (itinerary.Segments.Length > 0 ? itinerary.Segments[0].Airline : itinerary.AirlineCode);
                    lblFlightNumber.Text = (itinerary.Segments.Length > 0 ? itinerary.Segments[0].FlightNumber : "");
                    lblDepartureTime.Text = Util.GetDateString((itinerary.Segments.Length > 0 ? itinerary.Segments[0].DepartureTime : Convert.ToDateTime("01/01/1900")));

                    //if (bookingDetail[e.Item.ItemIndex].Status != BookingStatus.Ticketed && Util.IsToday(bookingDetail[e.Item.ItemIndex].CreatedOn))
                    //{
                    //    //lblInstantPurchase.Text = "<div class='padding-3 width-160 yellow-new'>Instant purchase required.</div>";
                    //}
                    
                    DateTime cancelDate = Convert.ToDateTime(itinerary.TravelDate);
                    cancelDate = Convert.ToDateTime(cancelDate.ToString("MM-dd-yyyy 00:00:00"));

                    if (bookingDetail[e.Item.ItemIndex].Status == BookingStatus.Refunded || bookingDetail[e.Item.ItemIndex].Status == BookingStatus.Modified || bookingDetail[e.Item.ItemIndex].Status == BookingStatus.Void)
                    {
                        if (agent.Country != 3 && (DateTime.Now.CompareTo(cancelDate) > 0))
                        {
                            btnRefund.Visible = true;
                        }
                        else
                        {
                            btnRefund.Visible = false;
                        }
                    }
                    else
                    {
                        btnRefund.Visible = true;
                    }
                    lblLeadPaxName.Text = itinerary.Passenger[0].Title + " " + itinerary.Passenger[0].FirstName + " " + itinerary.Passenger[0].LastName; 
                    int adultCount = 0;
                    decimal total = 0;
                    if (itinerary.Passenger.Length > 0)
                    {
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {

                            if (pax.Type == PassengerType.Adult)
                            {
                                adultCount++;
                            }
                            if (pax.Price != null)
                            {
                                if (pax.Price.NetFare > 0)
                                {
                                    total += (pax.Price.NetFare + pax.Price.Tax + pax.Price.Markup + pax.Price.B2CMarkup + pax.Price.HandlingFeeAmount) - pax.Price.Discount;//Added only B2C markup by chandan on 13062016
                                }
                                else
                                {
                                    total += (pax.Price.PublishedFare + pax.Price.Tax + pax.Price.Markup + pax.Price.B2CMarkup + pax.Price.HandlingFeeAmount) - pax.Price.Discount;//Added only B2C markup by chandan on 13062016
                                }
                                //if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.TBOAir || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.GoAir)
                                //{
                                    total += pax.Price.BaggageCharge + pax.Price.MealCharge + pax.Price.SeatPrice;
                                //}
                                if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    total += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                                }

                                total += pax.Price.OutputVATAmount;
                            }
                        }

                        lblAdultCount.Text = adultCount.ToString();
                        if (itinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam(Total price Ceiling for TBO Source)
                        {
                            lblTotal.Text = agent.AgentCurrency + " " + Math.Ceiling(total).ToString("N" + agent.DecimalValue);
                        }
                        else
                        {
                            lblTotal.Text = agent.AgentCurrency + " " + total.ToString("N" + agent.DecimalValue);
                        }

                        List<string> trips = new List<string>();
                        foreach (FlightInfo segment in itinerary.Segments)
                        {
                            if (!trips.Contains(segment.Origin.CityCode))
                            {
                                trips.Add(segment.Origin.CityCode);
                            }
                            if (!trips.Contains(segment.Destination.CityCode))
                            {
                                trips.Add(segment.Destination.CityCode);
                            }
                        }

                        bool isReturn = false;

                        if (itinerary.Origin == itinerary.Segments[itinerary.Segments.Length - 1].Destination.CityCode)
                        {
                            isReturn = true;
                        }

                        foreach (string city in trips)
                        {
                            if (lblTrip.Text.Length > 0)
                            {
                                lblTrip.Text += "-" + city;
                            }
                            else
                            {
                                lblTrip.Text = city;
                            }
                        }
                        if (isReturn)
                        {
                            lblTrip.Text += "-" + trips[0];
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "");
        }
    }

    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string type = string.Empty;
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            if (agentId == 0)
            {
                if (Convert.ToInt32(ddlAgency.SelectedItem.Value) > 1)
                {
                    type = "AGENT";// AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                }
                else
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                }
            }
            else
            {
                if (agentId == -1)
                {
                    agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                    if (agentId == 0)
                    {
                        type = "BASE";// BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations
                    }
                }
            }
            if (Convert.ToInt32(ddlAgency.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    type = "BASEB2B";// BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations
                }
            }
            BindLocation(agentId, type);
        }
        catch
        { }

    }

    protected void ddlB2B2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "B2B2B";// B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                }
            }
            else if (agentId == -1)
            {
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                }
                //if (agentId == -1)
                //{
                //    type = "BASE";
                //}
            }
            if (Convert.ToInt32(ddlAgency.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = string.Empty;
                    }
                }
            }
            if (Convert.ToInt32(ddlAgency.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                    }
                }
            }
            BindLocation(agentId, type);
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    protected void ddlAgency_SelectedIndexChanged(object sender, EventArgs e)
    {
        int agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
        if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
        BindB2BAgent(agentId);
        BindB2B2BAgent(agentId);
        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
        {
            ddlB2B2BAgent.Enabled = false;
        }
        string type = string.Empty;
        if (agentId == 0)
        {
            type = "BASE";
        }
        BindLocation(Utility.ToInteger(ddlAgency.SelectedItem.Value), type);
    }
}
