﻿using System;
using CT.Roster;
using System.Drawing;
using CT.Core;

public partial class ForGotPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["requestId"] == null || ROSEmployeeMaster.ValidatePasswordChangeRequest(Request["requestId"]) <= 0)
        {
            ResetPasswordMsg.Text = "Access Denied !";
            ResetPasswordMsg.ForeColor = Color.Red;
            ResetPasswordPanel.Visible = false;
        }
        else
        {
            requestId.Value = Request["requestId"];
        }
    }

    protected void ResetPasswordButton_Click(object sender, EventArgs e)
    {
        try
        {
            if (ComparePassword.IsValid)
            {
                int empId = ROSEmployeeMaster.ValidatePasswordChangeRequest(requestId.Value);
                if (empId > 0)
                {
                    ROSEmployeeMaster.UpdateForGotPassword(empId, NewPassword.Text.Trim());
                    ResetPasswordMsg.Text = "Your password has been changed successfully !";
                    ROSEmployeeMaster.DeletePasswordChangeRequest(requestId.Value);
                    ResetPasswordPanel.Visible = false;
                }
                else
                {
                    ResetPasswordMsg.Text = "Access Denied !";
                    ResetPasswordMsg.ForeColor = Color.Red;
                    ResetPasswordPanel.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Failed Update ForgotPassword. ERROR: " + ex.Message, "0");
            ResetPasswordMsg.Text = "Access Denied !";
            ResetPasswordMsg.ForeColor = Color.Red;
            ResetPasswordPanel.Visible = false;
        }
    }
}
