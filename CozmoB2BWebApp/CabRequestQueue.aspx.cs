﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using Newtonsoft.Json;

namespace CozmoB2BWebApp
{
    public partial class CabRequestQueue : CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Dictionary<string, string> List = new Dictionary<string, string>();
                List.Add("AgentType", Settings.LoginInfo.AgentType.ToString());
                List.Add("LocationId", Convert.ToString(Settings.LoginInfo.LocationID));
                List.Add("AgentId", Convert.ToString(Settings.LoginInfo.AgentId));
                List.Add("MemberType", Convert.ToString(Settings.LoginInfo.MemberType));
                hdfAgent.Value = JsonConvert.SerializeObject(List);
            }
        }

        [WebMethod]
        public static string BindPageLoadControls()
        {
            DataSet DropdownDs = GetDropDownValues(Settings.LoginInfo.AgentId);
            string JSONString = JsonConvert.SerializeObject(DropdownDs);
            return JSONString;
        }
        public static DataSet GetDropDownValues(int Agent_id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_LOGIN_AGENT_ID", Agent_id);
                return DBGateway.ExecuteQuery("USP_FlightQueue_Get_DropDown_values", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static ArrayList BindAllAgentTypes(int agentId, string AgentType)
        {
            ArrayList list = new ArrayList();
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, AgentType, agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            if (dtAgents != null && dtAgents.Rows.Count > 0)
            {
                foreach (DataRow dr in dtAgents.Rows)
                {
                    list.Add(new ListItem(
              dr["Agent_Name"].ToString(),
              dr["agent_id"].ToString()
               ));
                }
            }
            return list;
        }
        [WebMethod]
        public static ArrayList BindLocation(int agentId, string type)
        {
            ArrayList list = new ArrayList();
            DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);
            if (dtLocations != null && dtLocations.Rows.Count > 0)
            {
                foreach (DataRow dr in dtLocations.Rows)
                {
                    list.Add(new ListItem(
              dr["location_name"].ToString(),
              dr["location_id"].ToString()
               ));
                }
            }
            return list;
        }
        [WebMethod]
        //[ScriptMethod]
        public static string Search(String FromDate, String ToDate, int LocationId, int AgentFilter, string agentType)

        {
            try
            {
                string JSONString = string.Empty;
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
                IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
                startDate = (FromDate != string.Empty) ? Convert.ToDateTime(FromDate, provider) : DateTime.Now;
                endDate = (ToDate != string.Empty) ? Convert.ToDateTime(Convert.ToDateTime(ToDate, provider).ToString("dd/MM/yyyy 23:59"), provider) : DateTime.Now;
               
                DataSet ds = CabRequestqueue.GetCabRequestQueue(startDate, endDate, AgentFilter, LocationId);
                JSONString = JsonConvert.SerializeObject(ds);

                return JSONString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}