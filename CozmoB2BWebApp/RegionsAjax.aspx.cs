﻿using System;
using System.Collections.Generic;


public partial class RegionsAjax : System.Web.UI.Page
{
    protected List<CT.Core.Region> regionList;
    protected string response = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
            regionList = CT.Core.Region.GetDistinctRegions();

            foreach (CT.Core.Region region in regionList)
            {
                if (response.Length > 0)
                {
                    response += "," + region.RegionCode + "|" + region.RegionName;
                }
                else
                {
                    response = Request["id"] + "#" + region.RegionCode + "|" + region.RegionName;
                }
            }
        }
        catch (Exception ex)
        {
            response = ex.Message;
        }
        Response.Write(response);
        Response.End();
    }
}
