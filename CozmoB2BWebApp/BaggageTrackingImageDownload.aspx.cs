﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Ionic.Zip;
using System.Configuration;
using CT.TicketReceipt.BusinessLayer;

namespace CozmoB2BWebApp
{
    public partial class BaggageTrackingImageDownload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.QueryString["PaxID"]!=null)
            {
                DownloadZipFiles(Convert.ToInt32(Request.QueryString["PaxID"]));
            }
            
        }

        public void DownloadZipFiles(int paxID)
        {
            try
            {
                string BoardingPassfolderPath = ConfigurationManager.AppSettings["BaggageUploads"].ToString()+"\\" + paxID + "\\BoardingPass Copy(s)";
                string PIRfolderpath = ConfigurationManager.AppSettings["BaggageUploads"].ToString() + "\\" + paxID + "\\PIR Copy(s)";

                using (ZipFile zip = new ZipFile())
                {

                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    //zip.AddDirectoryByName("Files");
                    string[] filepaths = Directory.GetFiles(BoardingPassfolderPath);
                    foreach (var file in filepaths)
                    {
                        zip.AddFile(file, "BoardingPass Copy(s)");
                    }
                    string[] pirFiles = Directory.GetFiles(PIRfolderpath);
                    foreach (var file in pirFiles)
                    {
                        zip.AddFile(file, "PIR Copy(s)");
                    }
                    System.Web.HttpContext.Current.Response.Clear();
                    System.Web.HttpContext.Current.Response.BufferOutput = false;
                    string zipName = String.Format("Zip_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                    System.Web.HttpContext.Current.Response.ContentType = "application/zip";
                    System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    zip.Save(System.Web.HttpContext.Current.Response.OutputStream);
                    System.Web.HttpContext.Current.Response.End();

                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "Baggage Tracking  :DownloadZipFiles() ", "0");
            }

        }
    }
}