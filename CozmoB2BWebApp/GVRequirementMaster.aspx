<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="RequirementMasterUI" Title="Requirement Master" EnableEventValidation="false" ValidateRequest="false" Codebehind="GVRequirementMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="dc" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
    <%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<%--<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>--%>
<%--<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="ckeditor/adapters/jquery.js"></script>--%>
 

<%--<script type="text/javascript">
    $(function() {
    CKEDITOR.replace('<%=txtDescription.ClientID %>',
  { filebrowserImageUploadUrl: '/TicketReceipt/Upload.ashx',
      filebrowserUploadUrl: '/TicketReceipt/Upload.ashx'
  }
  ); //path to �Upload.ashx�
    });
</script>--%>
<script type="text/javascript">
    
</script>

    
    <center>
    <table  cellpadding="0" class="label" cellspacing="1" border="0">
                <tr>
                    <td style="height:10px">
                    </td>
                </tr>
                <tr>
                    
                    <td height="23px" align="right"><asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label><span style="color:red">*:</span></td>
                    <td align="left"><asp:DropDownList  ID="ddlCountry"  CssClass="inputDdlEnabled" runat="server" Width="154px" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
                    
                    <td align="right"><asp:Label ID="lblNationality" runat="server" Text="Nationality"></asp:Label><span style="color:red">*:</span></td>
                    <td align="left"><asp:DropDownList  ID="ddlNationality"  CssClass="inputDdlEnabled" runat="server" Width="154px" ></asp:DropDownList></td>               
                                                         
                </tr>
                
                <tr>
                   
                    <td height="23px" align="right"><asp:Label ID="lblVisaType" runat="server" Text="Visa Type"> </asp:Label><span style="color:red">*:</span></td>
                    <td align="left"><asp:DropDownList ID="ddlVisaType"  CssClass="inputDdlEnabled" runat="server" Width="154px" > </asp:DropDownList></td> 
                   
                    <td align="right"><asp:Label ID="lblResidence" runat="server" Text="Residence"></asp:Label><span style="color:red">*:</span></td>
                    <td align="left"><asp:DropDownList  ID="ddlResidence"  CssClass="inputDdlEnabled" runat="server" Width="154px" ></asp:DropDownList></td>                
                   
                </tr>
                <tr>
                   
                     <td height="23px" align="right"><asp:Label ID="lblHeader" runat="server" Text="Header"></asp:Label><span style="color:red">*:</span></td>
                     <td align="left" colspan="3"><asp:TextBox ID="txtHeader" CssClass="form-control inputEnabled" runat="server" Width="370px"></asp:TextBox></td>
               </tr>     
               <tr>
                    <td align="right" valign="top"><asp:Label ID="lblDescription" runat="server" Text="Description"></asp:Label><span style="color:red">*:</span></td>
                    <td align="left" colspan="3">
                 <%--  <asp:TextBox  ID="txtDescription" TextMode="multiLine" Height="100px" CssClass="inputEnabled" runat="server" Width="370px"></asp:TextBox>
                   </td>
                <textarea runat="server" id="txtDescription"></textarea>--%>
                <div>
   <CKEditor:CKEditorControl ID="txtDescription"  runat="server"  FilebrowserImageUploadUrl ="/TicketReceipt/Upload.ashx" FilebrowserUploadUrl="/TicketReceipt/Upload.ashx">
   </CKEditor:CKEditorControl>
</div>
                
                </tr>
                
                
                <tr>
                   <td height="23px" colspan="4" align="right" >
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClientClick="return Save();"  OnClick="btnSave_Click"  />
                        <asp:Button ID="btnCancel" runat="server" Text="Clear" CssClass="button" OnClick="btnCancel_Click"  />
                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click"  />
                              <%--<asp:CheckBox id="chkCopyFrom" runat="server" AutoPostBack="true" Text="Copy" /> --%>
                    </td>                                                     
                </tr>             
                <tr>
                    <td colspan="4" align="left" style="height:20px">
                        <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
                        <asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError"></asp:Label>  
                    </td>
                </tr>
                </table>
    
    
    
    </center>
        
 

<script type="text/javascript">

   function Save()
     {   
     
      


         if (getElement('txtHeader').value == '') addMessage('Header cannot be blank!', '');
       
         if (CKEDITOR != undefined) {
             for (var instance in CKEDITOR.instances)
                 var data = CKEDITOR.instances[instance].getData();
             if (data == '') {
                 addMessage('Description cannot be blank!', '');
             }
         }
        if(getElement('ddlCountry').selectedIndex<=0) addMessage('Please Select Country from the List!','');
        if(getElement('ddlNationality').selectedIndex<=0) addMessage('Please Select Nationality from the List!','');
        if(getElement('ddlVisaType').selectedIndex<=0) addMessage('Please Select VisaType from the List!','');
        if(getElement('ddlResidence').selectedIndex<=0) addMessage('Please Select Residence from the List!','');
       
        
        if (getMessage() != '') {
            alert(getMessage()); clearMessage(); return false;
        }
    
   }
   
</script>
</asp:Content>

<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
<asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="REQ_ID" 
    EmptyDataText="No Requirements List!" AutoGenerateColumns="false" PageSize="10" GridLines="none"  CssClass="grdTable"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
    OnPageIndexChanging="gvSearch_PageIndexChanging" >
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
    <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
    
     <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>    
    <cc1:Filter   ID="HTtxtHeader"  Width="100px" HeaderText="Header" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblHeader" runat="server" Text='<%# Eval("req_header") %>' CssClass="label grdof" ToolTip='<%# Eval("req_header") %>' Width="70px"></asp:Label>
    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>--%>
    </ItemTemplate>    
    
    </asp:TemplateField>   
    
     <%--<asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>    
    <cc1:Filter   ID="HTtxtDescription"  Width="100px" HeaderText="Description" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblDescription" runat="server" Text='<%# Eval("req_description") %>' CssClass="label grdof" ToolTip='<%# Eval("req_description") %>' Width="70px"></asp:Label>
    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>
    </ItemTemplate>    
    
    </asp:TemplateField>--%>    
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtCountry"  Width="150px" CssClass="inputEnabled" HeaderText="Country" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="ITlblCountry" runat="server" Text='<%# Eval("req_country_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("req_country_name") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>  
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtNationality"  Width="150px" CssClass="inputEnabled" HeaderText="Nationality" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="ITlblNationality" runat="server" Text='<%# Eval("req_nationality_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("req_nationality_name") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>    
    
     <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTxtVisaType"  Width="100px" CssClass="inputEnabled" HeaderText="Visa Type" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblCode" runat="server" Text='<%# Eval("req_visa_type_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("req_visa_type_name") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>     
      
      <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtResidence"  Width="100px" CssClass="inputEnabled" HeaderText="Residence" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblName" runat="server" Text='<%# Eval("req_residence_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("req_residence_name") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>   
    
    
   
    </Columns>           
    </asp:GridView>

</asp:Content>

