﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.BookingEngine;


public partial class CreateFleetInvoice : CT.Core.ParentPage
{
    protected UserMaster loggedMember = new UserMaster();
    protected int fleetId;
    protected int agencyId;
    protected string confNo;
    protected AgentMaster agency;
    protected FleetItinerary itinerary = new FleetItinerary();
    protected string routing;
    protected string remarks = string.Empty;
    protected string agencyAddress;
    protected int cityId;
    protected string errorMessage = string.Empty;
    protected LocationMaster location;
    protected Invoice invoice = new Invoice();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Fleet Invoice Details";
        AuthorizationCheck(); //Checking login or not
        loggedMember = new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));
        # region Fleet Invoice
        try
        {

            if (Request["fleetId"] != null && Request["agencyId"] != null && Request["confNo"] != null)
            {
                fleetId = Convert.ToInt32(Request["fleetId"]);
                agencyId = Convert.ToInt32(Request["agencyId"]);
                confNo = Request["confNo"];
            }
            else
            {
                fleetId = Convert.ToInt32(Request.QueryString["fleetId"]);
                agencyId = Convert.ToInt32(Request.QueryString["agencyId"]);

                confNo = Request.QueryString["confNo"];
            }

            //agency = new AgentMaster(Settings.LoginInfo.AgentId);
            agency = new AgentMaster(agencyId);


            string logoPath = "";
            if (agency.ID > 1)
            {
                logoPath = ConfigurationManager.AppSettings["AgentImage"] + agency.ImgFileName;
                imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + logoPath;
            }
            else
            {
                imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + "images/logo.jpg";
            }


            itinerary.Load(fleetId);
            
            if (Request["city"] != null)
            {
                routing = Request["city"];
            }

            if (Request["remarks"] != null)
            {
                remarks = Request["remarks"];
            }

            // Reading agency information.
            DataTable dtCities = CityMaster.GetList(agency.Country, ListStatus.Short, RecordStatus.Activated);
            DataRow[] cities = dtCities.Select("City_Name like '%" + agency.City + "%'");

            if (cities != null && cities.Length > 0)
            {
                cityId = Convert.ToInt32(cities[0]["city_id"]);
            }
            agency = new AgentMaster(agencyId);
            
            // Formatting agency address for display.
            agencyAddress = agency.Address;
            agencyAddress.Trim();
            if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
            {
                agencyAddress += ",";
            }
            if (agency.Address != null && agency.Address.Length > 0)
            {
                agencyAddress += agency.Address;
            }
            agencyAddress.Trim();
            if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
            {
                agencyAddress += ",";
            }

            location = new LocationMaster(itinerary.LocationId);


            location = new LocationMaster(itinerary.LocationId);

            int invoiceNumber = 0;
            try
            {
                // Generating invoice.

                if (itinerary.FleetId > 0)
                {
                    invoiceNumber = Invoice.isInvoiceGenerated(fleetId, ProductType.Car);
                }
                if (invoiceNumber > 0)
                {
                    invoice = new Invoice();
                    invoice.Load(invoiceNumber);
                }
                else
                {
                    invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(fleetId, string.Empty, (int)loggedMember.ID, ProductType.Car, 1);
                    invoice.Load(invoiceNumber);
                }
            }
            catch (Exception exp)
            {
                errorMessage = "Invalid Invoice Number";
                Audit.Add(EventType.Exception, Severity.High, 1, "Exception while generating Invoice.. Message:" + exp.Message, "");
                return;
            }
        }
        catch (Exception exp)
        {
            errorMessage = "Your Session is Expired!";
            Audit.Add(EventType.Exception, Severity.High, 1, "Exception while generating Invoice.. Message:" + exp.Message, "");
        }
        #endregion
    }

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
