﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.Core;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using System.Net;
using CT.AccountingEngine;
using System.Collections.Specialized;
using CCA.Util;
using System.Threading;
using Encryption.AES;
using SafexPay;
using System.Linq;
using CT.Corporate;

public partial class PaymentConfirmation :CT.Core.ParentPage// System.Web.UI.Page
{
    protected SearchResult result;
    int resultId=0;
    string sessionId;
    bool isLCC = false;
    int agencyId = -1;
    protected bool isBookingSuccess = false;
    protected List<Ticket> ticketList;
    protected FlightItinerary flightItinerary;
    protected string IPAddr = string.Empty;
    protected BookingDetail booking;
    protected string inBaggage = "", outBaggage = "";
    protected Airline airline;
    protected string AirlineLogoPath = ConfigurationManager.AppSettings["AirlineLogoPath"];
    protected AgentMaster agency;
    protected decimal charges = 0;
    protected int emailCounter = 1;//Checking for Agent details display in Email body
    protected AutoResetEvent[] BookingEvents = new AutoResetEvent[2];
    protected decimal originalFare = 0m, currentFare = 0m;
    protected LocationMaster location = new LocationMaster();
    protected bool Sendmail;
    protected string[] pageParams = null, ccParams = null;
    protected Dictionary<string, string> PGResponseValues = new Dictionary<string, string>();
    protected bool sendForApproval = true, isCompleteBookingEnabled=false, isAutoTicketingEnabled=false;
    //private BookingResponse bookingResponse = new BookingResponse();
    protected void Page_Load(object sender, EventArgs e)
    {
        pageParams = GenericStatic.GetSetPageParams("PageParams", "", "get").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        ccParams = GenericStatic.GetSetPageParams("CCParams", "", "get").Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries);

        if ((pageParams != null && Session["sessionId"] != null && Session["FlightItinerary"] != null) || (Session["ResultIndex"] != null) || (ccParams != null && pageParams != null)) 
        {
            if (Settings.LoginInfo.IsCorporate == "Y")
                btnHold.Text = "Send For Approval";
            try
            {
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    agencyId = Settings.LoginInfo.OnBehalfAgentID;
                    agency = new AgentMaster(agencyId);
                    lblAgentBalance.Text = agency.AgentCurrency + " " + agency.UpdateBalance(0).ToString("N" + agency.DecimalValue);
                    location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                }
                else
                {
                    agencyId = Settings.LoginInfo.AgentId;
                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                    lblAgentBalance.Text = Settings.LoginInfo.Currency + " " + agency.UpdateBalance(0).ToString("N" + agency.DecimalValue);
                }
                hdnAgencyName.Value = agency.Name;
                hdnAgencyPhone.Value = agency.Phone1;
                
                //Disable when payment mode is credit limit
                if (agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Credit || agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Credit_Limit)
                {                    
                    ddlPaymentType.Disabled = true;
                }
                else if (agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Card)
                {
                    ddlPaymentType.Disabled = false;
                }
                #region old CC Charges Code
                //try
                //{
                //    DataTable dtChrges = AgentCreditCardCharges.GetList();
                //    DataView dv = dtChrges.DefaultView;

                //    // dv.RowFilter = "Productid IN ('" + 1 + "') AND Agentid IN ('" + agencyId + "') AND PGSource IN ('" + (int)CT.AccountingEngine.PaymentGatewaySource.CCAvenue + "')";

                //    // Added by Somasekhar on 28/06/2018
                //    if (Request.Form["encResp"] != null)
                //    {
                //        dv.RowFilter = "Productid IN ('" + 1 + "') AND Agentid IN ('" + agencyId + "') AND PGSource IN ('" + (int)CT.AccountingEngine.PaymentGatewaySource.CCAvenue + "')";
                //    }
                //    else
                //    {
                //        dv.RowFilter = "Productid IN ('" + 1 + "') AND Agentid IN ('" + agencyId + "') AND PGSource IN ('" + (int)CT.AccountingEngine.PaymentGatewaySource.SafexPay + "')";
                //    }
                //   //=========================================================
                //    dtChrges = dv.ToTable();
                //    if (dtChrges != null && dtChrges.Rows.Count > 0)
                //    {
                //        charges = Convert.ToDecimal(dtChrges.Rows[0]["PGCharge"]);
                //    }
                //}
                //catch (Exception ex)
                //{
                //    Audit.Add(EventType.CCAvenue, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
                //} 
                #endregion

                //FlightItinerary itinerary = Session["FlightItinerary"] as FlightItinerary;
                flightItinerary = Session["FlightItinerary"] as FlightItinerary;
              Sendmail=  flightItinerary.Sendmail;
                sessionId = Session["sessionId"].ToString();
                if (pageParams != null)
                {
                    resultId = Convert.ToInt32(pageParams[0]);
                }
                else if (Session["ResultIndex"] != null)
                {
                    resultId = Convert.ToInt32(Session["ResultIndex"]);
                }
                if (Basket.FlightBookingSession.ContainsKey(sessionId))
                {
                    result = Basket.FlightBookingSession[sessionId].Result[resultId - 1];
                    SearchResult[] results = new SearchResult[1];
                    results[0] = result;
                    if (!IsPostBack)//Added by Lokesh on 8Feb,2018 to Avoid RoundTrip Databinding
                    {
                        if (result.ResultBookingSource == BookingSource.AirArabia)
                        {                            
                            if (pageParams.Length == 3 && pageParams[1] != null && pageParams[2] != null)
                            {
                                originalFare = Convert.ToDecimal(pageParams[2].Split('-')[0]);
                                currentFare = Convert.ToDecimal(pageParams[2].Split('-')[1]);
                            }
                            //Update the price (shiva 26Mar2018)
                            //Update the price only intially and not on page refresh since we are summing up values and in order to achieve
                            //that AirLocatorCode is checked for null. If updated already AirLocatorCode='G9' will be assigned
                            if (string.IsNullOrEmpty(flightItinerary.AirLocatorCode))
                            {
                                flightItinerary.AirLocatorCode = "G9";
                                //if (flightItinerary.Passenger.ToList().Sum(p => p.Price.BaggageCharge) > 0)// Not req
                                CalculateBaggageFare();
                            }
                        }
                        //New flight summary logic for showing complete flights
                        List<FlightInfo> Segments = new List<FlightInfo>(result.Flights[0]);
                        if (result.Flights.Length > 1)
                            Segments.AddRange(result.Flights[1]);
                        //dlFlight.DataSource = results;
                        dlFlight.DataSource = Segments;
                        dlFlight.DataBind();
                        dlPaxPrice.DataSource = results;//To avoid exception for Corporate TOTALBOOKINGAMOUNT (line 199)
                        dlPaxPrice.DataBind();
                        //If AUTOTICKETING is defined for this agency then directly ticket the Itinerary and save all corp records as 'A' (Approved) instead of 'P' (Pending)
                        //in BKE_Flight_Policy and Corp_Profile_Transc_Approval tables
                        if (Settings.LoginInfo.IsCorporate == "Y" && result.TravelPolicyResult != null && result.TravelPolicyResult.PolicyBreakingRules != null)
                        {
                            AgentAppConfig appConfig = new AgentAppConfig();
                            appConfig.AgentID = (int)agency.ID;
                            List<AgentAppConfig> configs = appConfig.GetConfigData();

                            if (configs.Exists(x => x.AppKey.ToUpper() == "AUTOTICKETING" && x.AppValue.ToLower() == "true"))
                            {
                                isAutoTicketingEnabled = true;
                                btnHold.Visible = false;
                                imgBtnPayment.Visible = true;
                            }

                            if (configs.Exists(x => x.AppKey.ToUpper() == "COMPLETEBOOKING" && x.AppValue.ToLower() == "true"))
                            {
                                isCompleteBookingEnabled = true;
                            }
                            if (!isAutoTicketingEnabled)//If AUTOTICKETING is set to True in APP_CONFIG then don't validate approvals 
                            {
                                CorporateProfileExpenseDetails expDetails = new CorporateProfileExpenseDetails();
                                List<CorpProfileApproval> profileApprovals = expDetails.Load(Settings.LoginInfo.CorporateProfileId, "T");
                                if (profileApprovals != null)
                                {
                                    bool isDomestic = flightItinerary.IsDomestic;
                                    decimal totalAmount = flightItinerary.GetBookingAmount();

                                    sendForApproval = flightItinerary.FlightPolicy.CheckTripApprovalStatus(isDomestic, totalAmount, profileApprovals);

                                    if (!sendForApproval)
                                    {
                                        btnHold.Visible = false;
                                        imgBtnPayment.Visible = true;
                                        imgBtnPayment.Text = "Book";
                                    }
                                }
                            }

                            if (result.TravelPolicyResult != null && result.TravelPolicyResult.PolicyBreakingRules.ContainsKey("TOTALBOOKINGAMOUNT"))
                            {
                                List<string> bookingValues = result.TravelPolicyResult.PolicyBreakingRules["TOTALBOOKINGAMOUNT"];
                                decimal totalBookingAmount = Convert.ToDecimal(hdnBookingAmount.Value);
                                decimal bookingAmount = 0, thresholdAmount = 0, thresholdValue = 0;
                                try { bookingAmount = Convert.ToDecimal(bookingValues[0]); } catch { }
                                if (result.TravelPolicyResult.PolicyBreakingRules.ContainsKey("BOOKINGTHRESHOLD"))
                                {
                                    List<string> thresholdValues = result.TravelPolicyResult.PolicyBreakingRules["BOOKINGTHRESHOLD"];
                                    if (thresholdValues != null && thresholdValues.Count == 2)
                                    {
                                        try { thresholdValue = Convert.ToDecimal(thresholdValues[0]); } catch { }

                                        if (thresholdValues[1].ToUpper() == "P")
                                        {
                                            thresholdAmount = bookingAmount * thresholdValue / 100;
                                        }
                                        else
                                            thresholdAmount = thresholdValue;
                                    }
                                }

                                if (bookingAmount > 0 && (bookingAmount + thresholdAmount) < totalBookingAmount)
                                {
                                    sendForApproval = true; //hiding Book button
                                    //Stop booking and show a message
                                    ddlPaymentType.Disabled = true;
                                    rblAgentPG.Enabled = false;
                                    chkRules.Enabled = false;
                                    btnHold.Visible = false; //hiding Send for Approval button for GDS flights
                                    imgBtnPayment.Visible = false;
                                    lblMessage.Text = "You are not allowed to book a Ticket beyond your limit : " + agency.AgentCurrency + " " + Math.Round(bookingAmount + thresholdAmount, agency.DecimalValue).ToString("N" + agency.DecimalValue);
                                }
                            }
                        }
                    }
                   
                }
                if (!IsPostBack)//Added by Lokesh on 8Feb,2018 to Avoid RoundTrip Databinding
                {
                    dlPassengers.DataSource = flightItinerary.Passenger;
                    dlPassengers.DataBind();
                    Session["UAPIReprice"] = Session["UAPIURImpresp"] = null;// For Dynamic PCC Reprice

                    //Added By Somasekhar on 02/07/2018  -- for load dynamically Payment Gateways
                    LoadPaymentGateways();

                }
                //Assigning payment mode in case of CreditLimit for Itinerary
                if(agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Credit_Limit)
                    flightItinerary.PaymentMode = ModeOfPayment.CreditLimit;

                IPAddr = flightItinerary.BookUserIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];
                if (!IsPostBack && result != null)
                {
                    //Get G9 Baggage fare quote
                    //Code added by Lokesh on 10/07/2016.
                    if (result.ResultBookingSource == BookingSource.AirArabia || result.ResultBookingSource == BookingSource.FlyDubai || result.ResultBookingSource ==BookingSource.Indigo ||
                        result.ResultBookingSource == BookingSource.SpiceJet || flightItinerary.IsLCC ||  result.ResultBookingSource == BookingSource.AirIndiaExpressIntl || result.ResultBookingSource == BookingSource.IndigoCorp || result.ResultBookingSource == BookingSource.SpiceJetCorp || result.ResultBookingSource == BookingSource.GoAir || result.ResultBookingSource == BookingSource.GoAirCorp || result.ResultBookingSource == BookingSource.Babylon || result.ResultBookingSource == BookingSource.SalamAir)//||result.ResultBookingSource == BookingSource.TBOAir ||)
                    {
                        btnHold.Visible = false;
                        lblHold.Visible = false;
                    }
                    //if (Session["BookingAgencyID"] != null)
                    //  {
                    // int bookingAgentId = (int)Session["BookingAgencyID"];
                    //      AgentMaster agent = new AgentMaster(bookingAgentId);
                    //      lblAgentBalance.Text = agent.AgentCurrency + " " + agent.CurrentBalance.ToString("0,0.00");
                    // }
                    //  else
                    // {
                    //   lblAgentBalance.Text = Settings.LoginInfo.Currency + " " + Settings.LoginInfo.AgentBalance.ToString("0,0.00");
                    // }

                }
                //Load the PG response values from the session as key value pairs
                if(ccParams != null && ccParams.Length > 0)
                {
                    foreach(string val in ccParams)
                    {
                        if (!string.IsNullOrEmpty(val))
                        {
                            string key = val.Split('=')[0];
                            string value = val.Split('=')[1];
                            PGResponseValues.Add(key, value);
                        }
                    }
                }


                if (Request["ErrorPG"] != null)//Error returned from Payment Gateway
                {
                    lblError.Text = Request["ErrorPG"];
                    MultiView1.ActiveViewIndex = 1;
                }
                else if (PGResponseValues.ContainsKey("encResp"))//Payment returned from CCAvenue
                {
                    string orderId = string.Empty;
                    string paymentId = string.Empty;
                    string statusMessage = string.Empty;

                    string workingKey = ConfigurationManager.AppSettings["CCA_Encrypt_Key"];
                    CCACrypto ccaCrypto = new CCACrypto();
                    string encResponse = ccaCrypto.Decrypt(PGResponseValues["encResp"], workingKey);
                    Audit.Add(EventType.Book, Severity.Low, 1, "(Flight)CCAvenue response data : " + encResponse, Request["REMOTE_ADDR"]);
                    NameValueCollection Params = new NameValueCollection();
                    string[] segments = encResponse.Split('&');
                    foreach (string seg in segments)
                    {
                        string[] parts = seg.Split('=');
                        if (parts.Length > 0)
                        {
                            string Key = parts[0].Trim();
                            string Value = parts[1].Trim();
                            Params.Add(Key, Value);
                        }
                    }

                    int paymentInformationId = 0;

                    if (Session["PaymentInformationId"] != null)
                    {
                        paymentInformationId = Convert.ToInt32(Session["PaymentInformationId"]);
                    }

                    if (Params["order_status"] == "Success")
                    {
                        orderId = Params["order_id"];
                        //paymentId = Params["payment_id"];
                        paymentId = Params["tracking_id"];

                        try
                        {
                            CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                            creditCard.PaymentId = paymentId;
                            creditCard.TrackId = orderId;
                            creditCard.PaymentInformationId = paymentInformationId;
                            creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                            creditCard.ReferenceId = flightItinerary.BookingId;
                            creditCard.Remarks = statusMessage;
                            creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.CCAvenue;
                            creditCard.UpdatePaymentDetails();                            
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                        }

                        BookingResponse response = new BookingResponse();
                        statusMessage = "Successful";
                        ddlPaymentType.SelectedIndex = 1;//CreditCard
                        if (!flightItinerary.IsLCC)
                        {
                            response = (BookingResponse)Session["BookingResponse"];
                        }
                        else
                        {
                            try
                            {   
                                response = MakeBooking();
                            }
                            catch
                            {
                                throw;
                            }
                        }

                        int flightId = FlightItinerary.GetFlightId(response.PNR);
                        flightItinerary = new FlightItinerary(flightId);

                        try
                        {
                            CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                            creditCard.PaymentId = paymentId;
                            creditCard.TrackId = orderId;
                            creditCard.PaymentInformationId = paymentInformationId;
                            creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                            creditCard.ReferenceId = flightItinerary.BookingId;
                            creditCard.Remarks = statusMessage;
                            creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.CCAvenue;
                            creditCard.UpdatePaymentDetails();
                            Session["PaymentInformationId"] = null;//clear the credit card id stored in session
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                        }
                        if (response.Status == BookingResponseStatus.Successful)
                            GenerateTicket(response);
                    }
                    else
                    {
                        if (Params["failure_message"] != null && Params["failure_message"].Length > 0)
                        {
                            statusMessage = Params["failure_message"];
                        }
                        else if (Params["status_message"] != null && Params["status_message"].Length > 0)
                        {
                            statusMessage = Params["status_message"];
                        }
                        else
                        {
                            statusMessage = "Payment declined due to invalid details or due to technical error.";
                        }

                        try
                        {
                            CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                            creditCard.PaymentId = paymentId;
                            creditCard.TrackId = orderId;
                            creditCard.PaymentInformationId = paymentInformationId;
                            creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                            creditCard.ReferenceId = flightItinerary.BookingId;
                            creditCard.Remarks = statusMessage;
                            creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.CCAvenue;
                            creditCard.UpdatePaymentDetails();
                            Session["PaymentInformationId"] = null;//clear the credit card id stored in session
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                        }

                        MultiView1.ActiveViewIndex = 1;
                        lblError.Text = "Payment declined due to technical error or due to invalid details";
                    }

                    
                }
                //Added by Somasekhar on 27/06/2018 -- for SafexPay Payment Gateway
                #region SafexPay 
                else if (PGResponseValues.ContainsKey("txn_response"))//Payment returned from SafexPay
                {
                    string orderId = string.Empty;
                    string paymentId = string.Empty;
                    string statusMessage = string.Empty;
                    SafexPayService safexPay = new SafexPayService();
                    CryptoClass aes = new CryptoClass();
                    #region response parameters

                    try
                    {
                        aes.enc_txn_response = (!String.IsNullOrEmpty(PGResponseValues["txn_response"])) ? PGResponseValues["txn_response"].Replace(" ", "+") : string.Empty;
                        aes.enc_pg_details = (!String.IsNullOrEmpty(PGResponseValues["pg_details"])) ? PGResponseValues["pg_details"].Replace(" ", "+") : string.Empty;
                        aes.enc_fraud_details = (!String.IsNullOrEmpty(PGResponseValues["fraud_details"])) ? PGResponseValues["fraud_details"].Replace(" ", "+") : string.Empty;
                        aes.enc_other_details = (!String.IsNullOrEmpty(PGResponseValues["other_details"])) ? PGResponseValues["other_details"].Replace(" ", "+") : string.Empty;

                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, "Error message :" + ex.Message + ex.StackTrace, Request["REMOTE_ADDR"]);
                    }
                    #endregion
                    #region Txn_Resonse
                    try
                    {

                        string txn_response = aes.decrypt(aes.enc_txn_response, safexPay.txnMerchantKey);
                        string[] txn_response_arr = txn_response.Split('|');

                        safexPay.ag_id = (!String.IsNullOrEmpty(txn_response_arr[0])) ? txn_response_arr[0] : string.Empty;
                        safexPay.me_id = (!String.IsNullOrEmpty(txn_response_arr[1])) ? txn_response_arr[1] : string.Empty;
                        safexPay.order_no = (!String.IsNullOrEmpty(txn_response_arr[2])) ? txn_response_arr[2] : string.Empty;
                        safexPay.Amount = (!String.IsNullOrEmpty(txn_response_arr[3])) ? txn_response_arr[3] : string.Empty;
                        safexPay.Country = (!String.IsNullOrEmpty(txn_response_arr[4])) ? txn_response_arr[4] : string.Empty;
                        safexPay.Currency = (!String.IsNullOrEmpty(txn_response_arr[5])) ? txn_response_arr[5] : string.Empty;
                        safexPay.txn_date = (!String.IsNullOrEmpty(txn_response_arr[6])) ? txn_response_arr[6] : string.Empty;
                        safexPay.txn_time = (!String.IsNullOrEmpty(txn_response_arr[7])) ? txn_response_arr[7] : string.Empty;
                        safexPay.ag_ref = (!String.IsNullOrEmpty(txn_response_arr[8])) ? txn_response_arr[8] : string.Empty;
                        safexPay.pg_ref = (!String.IsNullOrEmpty(txn_response_arr[9])) ? txn_response_arr[9] : string.Empty;
                        safexPay.status = (!String.IsNullOrEmpty(txn_response_arr[10])) ? txn_response_arr[10] : string.Empty;
                        safexPay.res_code = (!String.IsNullOrEmpty(txn_response_arr[11])) ? txn_response_arr[11] : string.Empty;
                        safexPay.res_message = (!String.IsNullOrEmpty(txn_response_arr[12])) ? txn_response_arr[12] : string.Empty;

                        Audit.Add(EventType.Book, Severity.Low, 1, "(Flight)SafexPay response data : " + txn_response, Request["REMOTE_ADDR"]);

                        try
                        {
                            //response Log in decrypt format
                            safexPay.WriteResonse(txn_response);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
                        }

                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
                    }
                    #endregion

                    int paymentInformationId = 0;

                    if (Session["PaymentInformationId"] != null)
                    {
                        paymentInformationId = Convert.ToInt32(Session["PaymentInformationId"]);
                    }

                    if (safexPay.status == "Successful")
                    {
                        orderId = safexPay.order_no;
                        //paymentId = Params["payment_id"];
                        paymentId = safexPay.pg_ref;
                        statusMessage = "Successful";

                        try
                        {
                            CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                            creditCard.PaymentId = paymentId;
                            creditCard.TrackId = orderId;
                            creditCard.PaymentInformationId = paymentInformationId;
                            creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                            creditCard.ReferenceId = flightItinerary.BookingId;
                            creditCard.Remarks = statusMessage;
                            creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.SafexPay;
                            creditCard.UpdatePaymentDetails();                            
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                        }

                        BookingResponse response = new BookingResponse();
                        ddlPaymentType.SelectedIndex = 1;//CreditCard
                        if (!flightItinerary.IsLCC)
                        {
                            response = (BookingResponse)Session["BookingResponse"];
                        }
                        else
                        {
                            try
                            {
                                response = MakeBooking();
                            }
                            catch
                            {
                                throw;
                            }
                        }

                        int flightId = FlightItinerary.GetFlightId(response.PNR);
                        flightItinerary = new FlightItinerary(flightId);

                        try
                        {
                            CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                            creditCard.PaymentId = paymentId;
                            creditCard.TrackId = orderId;
                            creditCard.PaymentInformationId = paymentInformationId;
                            creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                            creditCard.ReferenceId = flightItinerary.BookingId;
                            creditCard.Remarks = statusMessage;
                            creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.SafexPay;
                            creditCard.UpdatePaymentDetails();
                            Session["PaymentInformationId"] = null;//clear the credit card id stored in session
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                        }

                        if (response.Status == BookingResponseStatus.Successful)
                            GenerateTicket(response);
                    }
                    else
                    {
                        statusMessage = safexPay.status;

                        try
                        {
                            CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                            creditCard.PaymentId = paymentId;
                            creditCard.TrackId = orderId;
                            creditCard.PaymentInformationId = paymentInformationId;
                            creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                            creditCard.ReferenceId = flightItinerary.BookingId;
                            creditCard.Remarks = statusMessage;
                            creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.SafexPay;
                            creditCard.UpdatePaymentDetails();
                            Session["PaymentInformationId"] = null;//clear the credit card id stored in session
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                        }

                        lblError.Text = safexPay.status == "Failed" ? safexPay.status : "Payment declined due to technical error or due to invalid details";
                        MultiView1.ActiveViewIndex = 1;                        
                    }                    
                }
                #endregion
                
            }
            catch (Exception ex)
            {
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderErrFlight");
                MultiView1.ActiveViewIndex = 1;
                lblError.Text = ex.Message;
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "0");
                Session["FZBaggageDetails"] = null;
                Session["ResultIndex"] = null;//Clear the Result Index session
                Session["BookingResponse"] = null;
                Session["FlightItinerary"] = null;
            }

        }
        else
        {
            if (string.IsNullOrEmpty(hdnPNR.Value))
                Response.Redirect("HotelSearch.aspx?source=Flight", true);
        }
        //if (Settings.LoginInfo.IsOnBehalfOfAgent)
       // {
       //     agencyId = Settings.LoginInfo.OnBehalfAgentID;
       // }
       // else
       // {
       //     agencyId = Settings.LoginInfo.AgentId;
       // }
    }

    private void CalculateBaggageFare()
    {
        try
        {
            FlightItinerary itinerary = Session["FlightItinerary"] as FlightItinerary;
            SearchRequest request = Session["FlightRequest"] as SearchRequest;
            if (result.ResultBookingSource == BookingSource.AirArabia)
            {
                //Hold booking is not allowed for G9

                string[] RPH = new string[result.Flights.Length];
                List<string> rph = new List<string>();
                for (int i = 0; i < result.Flights.Length; i++)
                {
                    for (int j = 0; j < result.Flights[i].Length; j++)
                    {                        
                        rph.Add(result.Flights[i][j].UapiDepartureTime.ToString() + "|" + result.Flights[i][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[i][j].Airline + result.Flights[i][j].FlightNumber);                        
                    }
                }
                RPH = rph.ToArray();
                int arraySize = 0;

           
                arraySize = (itinerary.Passenger.Length - request.InfantCount) * itinerary.Segments.Length;


                
                List<string> paxBaggages = new List<string>();

                for (int i = 0; i < (itinerary.Passenger.Length - request.InfantCount); i++)
                {
                    if (itinerary.Passenger[i].Type != PassengerType.Infant)
                    {
                        string travelerRPH = "";
                        if (itinerary.Passenger[i].Type == PassengerType.Adult || itinerary.Passenger[i].Type == PassengerType.Senior)
                        {
                            travelerRPH = "A" + (i + 1);
                        }
                        else if (itinerary.Passenger[i].Type == PassengerType.Child)
                        {
                            travelerRPH = "C" + (i + 1);
                        }

                        string bagCode = itinerary.Passenger[i].BaggageType;
                        string mealCode = itinerary.Passenger[i].MealType;
                        string seatCode = itinerary.Passenger[i].SeatInfo;

                        for (int k = 0; k < result.Flights.Length; k++)
                        {
                            for (int j = 0; j < result.Flights[k].Length; j++)
                            {
                                if (!string.IsNullOrEmpty(bagCode) && bagCode.Split(',').Length > k)
                                    paxBaggages.Add(bagCode.Split(',')[k].Trim()+"-BAG" + "|" + travelerRPH + "|" + result.Flights[k][j].UapiDepartureTime.ToString() + "|" + result.Flights[k][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[k][j].Airline + result.Flights[k][j].FlightNumber);
                                if(!string.IsNullOrEmpty(mealCode) && mealCode.Split(',').Length>k)
                                    paxBaggages.Add(mealCode.Split(',')[k].Trim() + "-MEAL" + "|" + travelerRPH + "|" + result.Flights[k][j].UapiDepartureTime.ToString() + "|" + result.Flights[k][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[k][j].Airline + result.Flights[k][j].FlightNumber);
                                if (!string.IsNullOrEmpty(seatCode))
                                {
                                    string seatDetail = (seatCode.Split('|').Length > k) ? seatCode.Split('|')[k].Trim() : seatCode;
                                    if (seatDetail.Contains(result.Flights[k][j].Origin.AirportCode + "-" + result.Flights[k][j].Destination.AirportCode))
                                        paxBaggages.Add(seatDetail.Split('(')[0] + "-SEAT" + "|" + travelerRPH + "|" + result.Flights[k][j].UapiDepartureTime.ToString() + "|" + result.Flights[k][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[k][j].Airline + result.Flights[k][j].FlightNumber);
                                }

                            }

                        }
                    }
                }

                CT.BookingEngine.GDS.AirArabia aaObj = new CT.BookingEngine.GDS.AirArabia();
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    aaObj.UserName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].UserID;
                    aaObj.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].Password;
                    aaObj.Code = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].HAP;
                    aaObj.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    aaObj.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                }
                else
                {
                    aaObj.UserName = Settings.LoginInfo.AgentSourceCredentials["G9"].UserID;
                    aaObj.Password = Settings.LoginInfo.AgentSourceCredentials["G9"].Password;
                    aaObj.Code = Settings.LoginInfo.AgentSourceCredentials["G9"].HAP;
                    aaObj.AgentCurrency = Settings.LoginInfo.Currency;
                    aaObj.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                }

                //Get the original price before baggage price recalculation
                decimal fares = itinerary.Passenger.ToList().Sum(p => p.Price.Markup + p.Price.PublishedFare + p.Price.AsvAmount + p.Price.BaggageCharge +p.Price.MealCharge+ p.Price.SeatPrice - p.Price.Discount + p.Price.Tax + p.Price.HandlingFeeAmount);
                originalFare = originalFare > 0 ? originalFare : fares;

                decimal vatAmount = 0m;

                if ((Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                {
                    foreach (FlightPassenger pax in itinerary.Passenger)
                    {
                        List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                        decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup, (location.ID));
                        pax.Price.OutputVATAmount = gstAmount;
                        pax.Price.GSTDetailList = gstTaxList;
                        vatAmount += gstAmount;
                    }
                }
                else if (!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.LocationCountryCode == "IN")
                {
                    foreach (FlightPassenger pax in itinerary.Passenger)
                    {
                        List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                        decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup, (Settings.LoginInfo.LocationID));
                        pax.Price.OutputVATAmount = gstAmount;
                        pax.Price.GSTDetailList = gstTaxList;
                        vatAmount += gstAmount;
                    }

                }
                else
                {
                    if (itinerary.Passenger[0].Price.TaxDetails != null && itinerary.Passenger[0].Price.TaxDetails.OutputVAT != null)
                    {
                        OutputVATDetail outVat = itinerary.Passenger[0].Price.TaxDetails.OutputVAT;

                        decimal markupValue = 0m, handlingFee = 0m, baggageCharge = 0m;
                        markupValue = itinerary.Passenger.ToList().Sum(p => agency.AgentType == (int)AgentType.Agent ? p.Price.Markup + p.Price.AsvAmount : p.Price.Markup);
                        handlingFee = itinerary.Passenger.ToList().Sum(p => p.Price.HandlingFeeAmount);
                        baggageCharge = itinerary.Passenger.ToList().Sum(p => p.Price.BaggageCharge);

                        //Output vat must not be calculated including HandlingFee so deduct it from TotalFare
                        vatAmount = outVat.CalculateVatAmount((decimal)originalFare - handlingFee, markupValue, agency.DecimalValue);
                    }
                }

                originalFare += vatAmount;

                CT.BookingEngine.GDS.AirArabia.SessionData data = (CT.BookingEngine.GDS.AirArabia.SessionData)Basket.BookingSession[sessionId][result.Airline + "-" + result.ResultKey];
                string fareXml = "";
                CookieContainer cookie = new CookieContainer();
                aaObj.AppUserId = Settings.LoginInfo.UserID.ToString();
                aaObj.SessionID = Session["sessionId"].ToString();
                //Modified by Lokesh on 04-April-2018
                //Applicable to Only G9 Source
                //If the customer is travelling from India(Pax will have state code and TaxRegNo)
                Dictionary<string, decimal> PaxBagFares = new Dictionary<string, decimal>();
                string[] baggageFare = aaObj.GetBaggageFare(ref result, request, ref data, ref RPH, ref fareXml, sessionId, ref cookie, ref paxBaggages, itinerary.Passenger[0].GSTStateCode, itinerary.Passenger[0].GSTTaxRegNo, ref PaxBagFares);
                MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(Session["sessionId"].ToString());
                mse.SettingsLoginInfo = Settings.LoginInfo;
                PriceAccounts price = result.Price;
                decimal inputvat = 0m;
                mse.CalculateInputVATForFlight(request.Segments[0].Origin, request.Segments[0].Destination, result.ResultBookingSource, ref price, (decimal)result.TotalFare, ref inputvat);
                result.TotalFare += (double)inputvat;
                result.Tax += (double)inputvat;
                
                int totalpax = request.AdultCount + request.ChildCount + request.InfantCount;

                int count = 0;
                decimal markup = 0, discount = 0;
                //Calculate Markup for the new fare
                for (count = 0; count < result.FareBreakdown.Length; count++)
                {
                    Fare fare = result.FareBreakdown[count];
                    if (count == 0)
                    {
                        fare.TotalFare += (double)(inputvat);
                    }
                    CT.BookingEngine.PriceAccounts tempPrice = CT.AccountingEngine.AccountingEngine.GetPrice(result, count, agencyId, 0, (int)CT.BookingEngine.ProductType.Flight, "B2B");
                    markup += (tempPrice.Markup) * fare.PassengerCount;
                    discount += (tempPrice.Discount * fare.PassengerCount);
                    //Assign result price markup
                    result.Price.Markup = tempPrice.Markup;
                    result.Price.MarkupType = tempPrice.MarkupType;
                    result.Price.MarkupValue = tempPrice.MarkupValue;                    
                    
                    result.Price.Markup = tempPrice.Markup;
                    fare.AgentMarkup = (tempPrice.Markup * fare.PassengerCount);
                    fare.AgentDiscount = (tempPrice.Discount * fare.PassengerCount);
                }
                markup = Math.Round(markup, agency.DecimalValue);
                discount = Math.Round(discount, agency.DecimalValue);
                
                result.TotalFare += (double)markup;
                result.TotalFare -= (double)discount;

                data.SelectedResultInfo = fareXml;
                // TodO ziya: update itineray according to last price quote
                //Update the price (shiva 26Mar2018)
                
                //Modified by Lokesh on 5-April-2018
                //Now Update the Price For all Pax Types
                //Since we are reading the AirArabiaBaggagePriceResponse
                if (itinerary != null && itinerary.Passenger.Length > 0)
                {
                    //decimal roe = 1;
                    //if(aaObj.ExchangeRates.ContainsKey("AED"))
                    //{
                    //    roe = aaObj.ExchangeRates["AED"];
                    //}
                    itinerary.AirLocatorCode = "G9";
                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        if (result.FareBreakdown[i] != null && result.FareBreakdown[i].SupplierFare > 0)
                        {
                            itinerary.Passenger[i].Price.SupplierPrice = (decimal)result.FareBreakdown[i].SupplierFare / result.FareBreakdown[i].PassengerCount;
                            itinerary.Passenger[i].Price.PublishedFare = (decimal)result.FareBreakdown[i].BaseFare / result.FareBreakdown[i].PassengerCount;
                            itinerary.Passenger[i].Price.Tax = (decimal)result.FareBreakdown[i].Tax / result.FareBreakdown[i].PassengerCount;
                            //Update Recalculated Markup written by Shiva
                            itinerary.Passenger[i].Price.Markup = result.FareBreakdown[i].AgentMarkup / result.FareBreakdown[i].PassengerCount;
                            //ReAssign Handling Fee calculated
                            itinerary.Passenger[i].Price.HandlingFeeAmount = result.FareBreakdown[i].HandlingFee / result.FareBreakdown[i].PassengerCount;
                            itinerary.Passenger[i].Price.Discount = result.FareBreakdown[i].AgentDiscount / result.FareBreakdown[i].PassengerCount;
                        }
                        //Update the baggage fares assigned from BaggagePriceQuote. In order to show the popup compare previous fare and updated fare
                        if (itinerary.Passenger[i].Type != PassengerType.Infant)
                        {
                            //originalFare += itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.B2CMarkup + itinerary.Passenger[i].Price.Markup + itinerary.Passenger[i].Price.Tax + itinerary.Passenger[i].Price.BaggageCharge;
                            //currentFare += itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.B2CMarkup + itinerary.Passenger[i].Price.Markup + itinerary.Passenger[i].Price.Tax + itinerary.Passenger[i].Price.HandlingFeeAmount - itinerary.Passenger[i].Price.Discount;
                            string travelerRPH = "";
                            if (itinerary.Passenger[i].Type == PassengerType.Adult || itinerary.Passenger[i].Type == PassengerType.Senior)
                            {
                                travelerRPH = "A" + (i + 1);
                            }
                            else if (itinerary.Passenger[i].Type == PassengerType.Child)
                            {
                                travelerRPH = "C" + (i + 1);
                            }
                            //currentFare += PaxBagFares[travelerRPH];
                            itinerary.Passenger[i].Price.BaggageCharge = (PaxBagFares != null && PaxBagFares.ContainsKey("BAG~" + travelerRPH) ? PaxBagFares["BAG~" + travelerRPH] : 0);
                            itinerary.Passenger[i].Price.MealCharge = (PaxBagFares != null && PaxBagFares.ContainsKey("MEAL~" + travelerRPH) ? PaxBagFares["MEAL~" + travelerRPH] : 0);
                            itinerary.Passenger[i].Price.SeatPrice = (PaxBagFares != null && PaxBagFares.ContainsKey("SEAT~" + travelerRPH) ? PaxBagFares["SEAT~" + travelerRPH] : 0);

                            //Convert into Agent currency
                        }
                    }

                    currentFare = itinerary.Passenger.ToList().Sum(p => p.Price.Markup + p.Price.PublishedFare + p.Price.AsvAmount + p.Price.BaggageCharge + p.Price.MealCharge+p.Price.SeatPrice - p.Price.Discount + p.Price.Tax + p.Price.HandlingFeeAmount);

                    vatAmount = 0;

                    if ((Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                    {
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                            decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup, (location.ID));
                            pax.Price.OutputVATAmount = gstAmount;
                            pax.Price.GSTDetailList = gstTaxList;
                            vatAmount += gstAmount;
                        }
                    }
                    else if (!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.LocationCountryCode == "IN")
                    {
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                            decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup, (Settings.LoginInfo.LocationID));
                            pax.Price.OutputVATAmount = gstAmount;
                            pax.Price.GSTDetailList = gstTaxList;
                            vatAmount += gstAmount;
                        }
                    }
                    else
                    {
                        if (itinerary.Passenger[0].Price.TaxDetails != null && itinerary.Passenger[0].Price.TaxDetails.OutputVAT != null)
                        {
                            OutputVATDetail outVat = itinerary.Passenger[0].Price.TaxDetails.OutputVAT;

                            decimal markupValue = 0m, handlingFee = 0m, baggageCharge = 0m, mealCharge = 0m, seatCharge = 0m;
                            markupValue = itinerary.Passenger.ToList().Sum(p => agency.AgentType == (int)AgentType.Agent ? p.Price.Markup + p.Price.AsvAmount : p.Price.Markup);
                            handlingFee = itinerary.Passenger.ToList().Sum(p => p.Price.HandlingFeeAmount);
                            baggageCharge = itinerary.Passenger.ToList().Sum(p => p.Price.BaggageCharge);
                            mealCharge = itinerary.Passenger.ToList().Sum(p => p.Price.MealCharge);
                            seatCharge = itinerary.Passenger.ToList().Sum(p => p.Price.SeatPrice);

                            //Output vat must not be calculated including HandlingFee so deduct it from TotalFare
                            vatAmount = outVat.CalculateVatAmount((decimal)result.TotalFare + baggageCharge+ mealCharge+seatCharge - handlingFee, markupValue, agency.DecimalValue);
                        }
                    }

                    currentFare += vatAmount;
                    Session["FlightItinerary"] = itinerary;
                }
                originalFare = Math.Round(originalFare, agency.DecimalValue);
                currentFare = Math.Round(currentFare, agency.DecimalValue);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void dlFlight_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                FlightInfo obj = e.Item.DataItem as FlightInfo;

                if (obj != null)
                {
                    Image imgFlightCarrierCode = e.Item.FindControl("imgFlightCarrierCode") as Image;
                    Label lblFlightOriginCode = e.Item.FindControl("lblFlightOriginCode") as Label;
                    Label lblFlightDestinationCode = e.Item.FindControl("lblFlightDestinationCode") as Label;
                    Label lblFlightDuration = e.Item.FindControl("lblFlightDuration") as Label;
                    Label lblOnFlightWayType = e.Item.FindControl("lblOnFlightWayType") as Label;

                    Label lblFlightCode = e.Item.FindControl("lblFlightCode") as Label;
                    Label lblFlightDepartureDate = e.Item.FindControl("lblFlightDepartureDate") as Label;
                    Label lblFlightArrivalDate = e.Item.FindControl("lblFlightArrivalDate") as Label;
                    Label lblDefaultBaggage = e.Item.FindControl("lblDefaultBaggage") as Label;


                    string rootFolder = Airline.logoDirectory + "/";
                    Airline departingAirline = new Airline();
                    departingAirline.Load(obj.Airline);
                    imgFlightCarrierCode.ImageUrl = rootFolder + departingAirline.LogoFile;
                    lblFlightOriginCode.Text = obj.Origin.CityName;
                    lblFlightDestinationCode.Text = obj.Destination.CityName;
                    lblFlightDuration.Text = obj.Duration.Hours + "hrs " + obj.Duration.Minutes + "mins";

                    switch (obj.Stops)
                    {
                        case 0:
                            lblOnFlightWayType.Text = "Non Stop";
                            break;
                        case 1:
                            lblOnFlightWayType.Text = "Single Stop";
                            break;
                        case 2:
                            lblOnFlightWayType.Text = "Two Stops";
                            break;
                        default:
                            lblOnFlightWayType.Text = "Two+ Stops";
                            break;
                    }
                    lblOnFlightWayType.Text += "<br/>" + obj.SegmentFareType;
                    lblFlightCode.Text = departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")" + " " + obj.FlightNumber;
                    lblFlightDepartureDate.Text = obj.DepartureTime.ToString("dd MMM yyyy, HH:mm tt");
                    lblFlightArrivalDate.Text = obj.ArrivalTime.ToString("dd MMM yyyy, HH:mm tt");
                    lblDefaultBaggage.Text = !string.IsNullOrEmpty(obj.DefaultBaggage)? obj.DefaultBaggage: "As Per Airline Policy";
                }
                /* if (dlTicketDetails1.Items.Count > 0)
                 {
                     DataTable tbl = ((DataRowView)e.Item.DataItem).Row.Table;
                     if (e.Item.ItemIndex == tbl.Rows.Count - 1)
                     {

                     }
                 } */

                

            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    protected void dlPaxPrice_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblBaseFare = e.Item.FindControl("lblBaseFare") as Label;
                //Label lblAdults = e.Item.FindControl("lblAdultCount") as Label;
                //Label lblAdultsTotal = e.Item.FindControl("lblTotalAdults") as Label;
                Label lblMarkupTotal = e.Item.FindControl("lblMarkupTotal") as Label;
                //Label lblTotalPrice = e.Item.FindControl("lblTotalPrice") as Label;
                Label lblBaggage = e.Item.FindControl("lblBaggage") as Label;
                Label lblBaggagePrice = e.Item.FindControl("lblBaggagePrice") as Label;
                Label lblTotal = e.Item.FindControl("lblTotal") as Label;
                Label lblTax = e.Item.FindControl("lblTax") as Label;
                Label lblVATAmount = e.Item.FindControl("lblVATAmount") as Label;
                Label lblDiscount = e.Item.FindControl("lblDiscount") as Label;
                Label lblDiscountAmount = e.Item.FindControl("lblDiscountAmount") as Label;
                SearchResult result = e.Item.DataItem as SearchResult;
                FlightItinerary itinerary = Session["FlightItinerary"] as FlightItinerary;

                Label lblMeal = e.Item.FindControl("lblMeal") as Label;
                Label lblMealPrice = e.Item.FindControl("lblMealPrice") as Label;

                Label lblSeat = e.Item.FindControl("lblSeat") as Label;
                Label lblSeatPrice = e.Item.FindControl("lblSeatPrice") as Label;

                if (itinerary != null)
                {
                    int adults = 0;
                    decimal baggageCharge = 0, discount = 0, asvAmount = 0, SeatCharge = 0, dcBaseFare = 0, dcTax = 0;
                    decimal mealCharge = 0;
                    foreach (FlightPassenger pax in itinerary.Passenger)
                    {
                        if (pax.Type == PassengerType.Adult)
                        {
                            adults += 1;
                            
                        }
                        if (itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.IsLCC || itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp)
                        {                            
                            baggageCharge += pax.Price.BaggageCharge;
                        }
                        pax.Price.DecimalPoint = agency.DecimalValue;
                        discount += pax.Price.Discount;
                        asvAmount += pax.Price.AsvAmount;
                        SeatCharge += pax.Price.SeatPrice;
                        dcBaseFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;
                        dcTax += (pax.Price.Tax + pax.Price.Markup + pax.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee);
                    }
                    foreach (FlightPassenger pax in itinerary.Passenger)
                    {
                        if (itinerary.FlightBookingSource == BookingSource.Indigo|| itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource==BookingSource.AirArabia)
                        {
                            mealCharge += pax.Price.MealCharge;
                        }
                        pax.Price.DecimalPoint = agency.DecimalValue;
                    }

                    lblSeat.Visible = lblSeatPrice.Visible = true;
                    lblSeatPrice.Text = agency.AgentCurrency + " " + SeatCharge.ToString("N" + agency.DecimalValue);

                    if (itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource==BookingSource.AirArabia)
                    {
                        lblMeal.Visible = true;
                        lblMealPrice.Visible = true;
                        lblMealPrice.Text = agency.AgentCurrency + " " + mealCharge.ToString("N" + agency.DecimalValue);
                    }

                    discount = Math.Round(discount, agency.DecimalValue);
                    baggageCharge = Math.Round(baggageCharge, agency.DecimalValue);
                    mealCharge = Math.Round(mealCharge, agency.DecimalValue);
                    SeatCharge = Math.Round(SeatCharge, agency.DecimalValue);
                    
                    if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.TBOAir || itinerary.IsLCC || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp)
                    {
                        lblBaggage.Visible = true;
                        lblBaggagePrice.Visible = true;
                        lblBaggagePrice.Text = agency.AgentCurrency + " " + baggageCharge.ToString("N" + agency.DecimalValue);
                        //lblTotalPrice.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.TotalFare) + baggageCharge).ToString("N" + agency.DecimalValue);
                        lblTxnAmount.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.TotalFare) + baggageCharge).ToString("N" + agency.DecimalValue);//lblTotalPrice.Text;

                    }
                    else
                    {
                        //lblTotalPrice.Text = lblTotal.Text;
                        lblTxnAmount.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.TotalFare)).ToString("N" + agency.DecimalValue);
                    }
                    decimal vatAmount = 0m;
                    
                    if ((Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                    {
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();//Exclude Dynamic Markup for GST calculation
                            decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup - pax.Price.DynamicMarkup, (location.ID));
                            pax.Price.OutputVATAmount = gstAmount;
                            pax.Price.GSTDetailList = gstTaxList;
                            vatAmount += gstAmount;
                        }
                        //Calculate GST for additional Corp itineraries
                        if(Session["CriteriaItineraries"] != null)
                        {
                            List<FlightItinerary> itineraries = Session["CriteriaItineraries"] as List<FlightItinerary>;

                            itineraries.ForEach(x =>
                            {
                                x.Passenger.ToList().ForEach(pax =>
                                {
                                    List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();//Exclude Dynamic Markup for GST calculation
                                    decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup - pax.Price.DynamicMarkup, (location.ID));
                                    pax.Price.OutputVATAmount = gstAmount;
                                    pax.Price.GSTDetailList = gstTaxList;
                                });
                            });
                        }
                    }
                    else if(!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.LocationCountryCode == "IN")
                    {
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();//Exclude Dynamic Markup for GST calculation
                            decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup - pax.Price.DynamicMarkup, (Settings.LoginInfo.LocationID));
                            pax.Price.OutputVATAmount = gstAmount;
                            pax.Price.GSTDetailList = gstTaxList;
                            vatAmount += gstAmount;
                        }
                        //Calculate GST for additional Corp itineraries
                        if (Session["CriteriaItineraries"] != null)
                        {
                            List<FlightItinerary> itineraries = Session["CriteriaItineraries"] as List<FlightItinerary>;

                            itineraries.ForEach(x =>
                            {
                                x.Passenger.ToList().ForEach(pax =>
                                {
                                    List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();//Exclude Dynamic Markup for GST calculation
                                    decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup - pax.Price.DynamicMarkup, (location.ID));
                                    pax.Price.OutputVATAmount = gstAmount;
                                    pax.Price.GSTDetailList = gstTaxList;
                                });
                            });
                        }
                    }
                    else
                    {
                        if (itinerary.Passenger[0].Price.TaxDetails != null && itinerary.Passenger[0].Price.TaxDetails.OutputVAT != null)
                        {
                            OutputVATDetail outVat = itinerary.Passenger[0].Price.TaxDetails.OutputVAT;

                            decimal markup = 0m, handlingFee = 0m, dynamicMarkup=0m;
                            foreach (FlightPassenger pax in itinerary.Passenger)
                            {
                                //Exclude Dynamic Markup for VAT calculation
                                markup += (pax.Price.Markup - pax.Price.DynamicMarkup);
                                handlingFee += pax.Price.HandlingFeeAmount;
                                if (agency.AgentType == (int)AgentType.Agent)
                                {
                                    markup += pax.Price.AsvAmount;
                                }
                                dynamicMarkup += pax.Price.DynamicMarkup;
                            }
                            //Output vat must not be calculated including HandlingFee so deduct it from TotalFare
                            vatAmount = outVat.CalculateVatAmount((decimal)result.TotalFare + baggageCharge+ mealCharge+ SeatCharge - handlingFee- dynamicMarkup, markup, agency.DecimalValue);

                            foreach (FlightPassenger pax in itinerary.Passenger)
                            {
                                pax.Price.OutputVATAmount = vatAmount / itinerary.Passenger.Length;
                            }

                            //Calculate GST for additional Corp itineraries
                            if (Session["CriteriaItineraries"] != null)
                            {
                                List<FlightItinerary> itineraries = Session["CriteriaItineraries"] as List<FlightItinerary>;

                                itineraries.ForEach(x =>
                                {
                                    decimal dcMarkup = 0, dcHandlingFee = 0, dcVatAmount = 0, dcTotalFare = 0;
                                    x.Passenger.ToList().ForEach(pax =>
                                    {
                                        dcTotalFare += pax.Price.PublishedFare + pax.Price.Tax + pax.Price.BaggageCharge + 
                                        pax.Price.OtherCharges + pax.Price.MealCharge + pax.Price.SeatPrice;
                                        //Exclude Dynamic Markup for VAT calculation
                                        dcMarkup += (pax.Price.Markup - pax.Price.DynamicMarkup);
                                        dcHandlingFee += pax.Price.HandlingFeeAmount;
                                        if (agency.AgentType == (int)AgentType.Agent)
                                        {
                                            dcMarkup += pax.Price.AsvAmount;
                                        }
                                        
                                    });

                                    dcVatAmount = outVat.CalculateVatAmount(dcTotalFare + dcMarkup - dcHandlingFee, dcMarkup, agency.DecimalValue);

                                    x.Passenger.ToList().ForEach(pax => pax.Price.OutputVATAmount = dcVatAmount / x.Passenger.Length);
                                });
                            }
                        }
                    }
                    result.BaseFare = Math.Round(result.BaseFare, agency.DecimalValue);
                    vatAmount = Math.Round(vatAmount, agency.DecimalValue);
                    lblVATAmount.Text = agency.AgentCurrency + " " + vatAmount.ToString("N" + agency.DecimalValue);
                    lblTxnAmount.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.BaseFare+result.Tax) + baggageCharge + mealCharge + SeatCharge + vatAmount-discount).ToString("N" + agency.DecimalValue);
                    hdnBookingAmount.Value = (Convert.ToDecimal(result.TotalFare) + baggageCharge + mealCharge + SeatCharge + vatAmount).ToString("N" + agency.DecimalValue);
                    //agency.AgentTicketingAllowed = true;
                    //if (Session["Markup"] != null)
                    //{
                    //    lblMarkupTotal.Text = Convert.ToDecimal(Session["Markup"]).ToString("0,0.000");
                    //}
                    //else
                    //{
                    //    lblMarkupTotal.Text = lblTotalPrice.Text;
                    //}
                    decimal markUp = 0;
                    for (int i = 0; i < result.FareBreakdown.Length; i++)
                    {
                        if (result.FareBreakdown[i] != null)
                        {
                            markUp += result.FareBreakdown[i].AgentMarkup; // *result.FareBreakdown[i].PassengerCount;
                            //markUp -= result.FareBreakdown[i].AgentDiscount; // *result.FareBreakdown[i].PassengerCount;
                        }
                    }

                    if (result.ResultBookingSource == BookingSource.TBOAir && Convert.ToBoolean(Session["ISPRiceChangedTBO"]))
                    {
                        markUp = 0; decimal baseFare = 0,tax = 0;
                        for (int i = 0; i < result.FareBreakdown.Length; i++)
                        {
                            if (result.FareBreakdown[i] != null)
                            {
                                markUp += (result.FareBreakdown[i].AgentMarkup); // *result.FareBreakdown[i].PassengerCount;
                                //markUp -= result.FareBreakdown[i].AgentDiscount; // *result.FareBreakdown[i].PassengerCount;
                                baseFare += ((decimal)result.FareBreakdown[i].BaseFare);
                                tax += (result.FareBreakdown[i].Tax);
                            }
                        }

                        if (result.Price.MarkupType == "P")
                        {
                            if (agency.AgentAirMarkupType == "TF")
                            {
                                decimal total = baseFare + (decimal)tax + (result.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee);
                                markUp = total * result.Price.MarkupValue / 100;
                            }
                            else if (agency.AgentAirMarkupType == "BF")
                            {
                                markUp = baseFare * result.Price.MarkupValue / 100;
                            }
                            else if (agency.AgentAirMarkupType == "TX")
                            {
                                markUp = ((decimal)tax + (result.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee)) / result.Price.MarkupValue / 100;
                            }
                        }
                    }

                   

                    if (itinerary.FlightBookingSource != BookingSource.TBOAir)
                    {
                        lblTax.Text = agency.AgentCurrency + " " + (result.Tax + (double)markUp + (double)(itinerary.Passenger[0].Price.AsvElement != "BF" ? asvAmount : 0)).ToString("N" + agency.DecimalValue);
                    }
                    else
                    {
                        //For TBOAir BaseFare contains HandlingFee with OtherCharges so take Tax only
                        //lblTax.Text = agency.AgentCurrency + " " + ((result.Tax) + (double)(markUp + result.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee + (itinerary.Passenger[0].Price.AsvElement != "BF" ? asvAmount : 0))).ToString("N" + agency.DecimalValue);
                        lblTax.Text = agency.AgentCurrency + " " + (dcTax + (itinerary.Passenger[0].Price.AsvElement != "BF" ? asvAmount : 0)).ToString("N" + agency.DecimalValue);
                    }
                    result.BaseFare = Convert.ToDouble(result.BaseFare.ToString("N" + agency.DecimalValue)); 
                    lblBaseFare.Text = agency.AgentCurrency + " " + (result.BaseFare + (double)(itinerary.Passenger[0].Price.AsvElement == "BF" ? asvAmount : 0)).ToString("N" + agency.DecimalValue);
                    double Tax = result.Tax + (double)markUp;
                    Tax = Math.Round(Tax, agency.DecimalValue);
                    decimal dcAsvAmount = Settings.LoginInfo.IsOnBehalfOfAgent ? asvAmount : 0;

                    if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        //lblTotalPrice.Text = lblTotal.Text;
                        //Add Page Level Markup (Addl Markup/asvAmount) to the Total displayed
                        //lblTxnAmount.Text = agency.AgentCurrency + " " + Math.Ceiling((Convert.ToDecimal(result.BaseFare) + (decimal)Tax + baggageCharge + result.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee + vatAmount - discount)).ToString("N" + agency.DecimalValue);
                        //hdnBookingAmount.Value = Math.Ceiling((Convert.ToDecimal(result.BaseFare) + (decimal)Tax + baggageCharge + result.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee + vatAmount - discount)).ToString("N" + agency.DecimalValue);
                        //lblTotal.Text = agency.AgentCurrency + " " + ((Convert.ToDecimal(result.BaseFare) + (decimal)Tax + result.Price.OtherCharges + result.Price.AdditionalTxnFee + result.Price.SServiceFee + result.Price.TransactionFee + asvAmount - discount)).ToString("N" + agency.DecimalValue);
                        lblTxnAmount.Text = agency.AgentCurrency + " " + Math.Ceiling(dcBaseFare + dcTax + baggageCharge + vatAmount + dcAsvAmount - discount).ToString("N" + agency.DecimalValue);
                        hdnBookingAmount.Value = Math.Ceiling(dcBaseFare + dcTax + baggageCharge + vatAmount + dcAsvAmount - discount).ToString("N" + agency.DecimalValue);
                        lblTotal.Text = agency.AgentCurrency + " " + (dcBaseFare + dcTax + asvAmount - discount).ToString("N" + agency.DecimalValue);
                        lblBaseFare.Text = agency.AgentCurrency + " " + (dcBaseFare + (itinerary.Passenger[0].Price.AsvElement == "BF" ? asvAmount : 0)).ToString("N" + agency.DecimalValue);
                    }
                    else
                    {
                        lblTxnAmount.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.BaseFare + Tax) + baggageCharge + mealCharge + SeatCharge + vatAmount + dcAsvAmount - discount).ToString("N" + agency.DecimalValue);
                        lblTotal.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.BaseFare+ Tax) + asvAmount-discount).ToString("N" + agency.DecimalValue);
                    }

                    if (itinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                    {
                        //lblMarkupTotal.Text = agency.AgentCurrency + " " + Math.Ceiling((Convert.ToDecimal(result.BaseFare) + (decimal)Tax + baggageCharge + result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) + asvAmount + vatAmount - discount).ToString("N" + agency.DecimalValue);
                        lblMarkupTotal.Text = agency.AgentCurrency + " " + Math.Ceiling(dcBaseFare + dcTax + baggageCharge + asvAmount + vatAmount - discount).ToString("N" + agency.DecimalValue);
                    }
                    else
                    {
                        lblMarkupTotal.Text = agency.AgentCurrency + " " + ((decimal)(result.BaseFare+ Tax) + asvAmount + baggageCharge + mealCharge + SeatCharge + vatAmount-discount).ToString("N" + agency.DecimalValue);//lblTotalPrice.Text;//agency.AgentCurrency + " " + (Convert.ToDecimal(lblTotalPrice.Text)).ToString("N" + agency.DecimalValue);
                    }
                    if(discount > 0)
                    {
                        lblDiscount.Visible = true;
                        lblDiscountAmount.Visible = true;
                        lblDiscountAmount.Text = "(-)" + agency.AgentCurrency + " " + discount.ToString("N" + agency.DecimalValue);
                    }
                     //Added by Lokesh on 30/03/2018
                     //For G9 the baggage charge is also included in the total fare itself.
                     //So in the Total we will exclude the baggage charge.
                    //if (result != null && result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia)
                    //{
                    //    if (baggageCharge > 0)
                    //    {
                    //        lblTotal.Text = agency.AgentCurrency + " " + (Convert.ToDecimal(result.TotalFare) - (Convert.ToDecimal(baggageCharge)) + asvAmount).ToString("N" + agency.DecimalValue);
                    //        lblMarkupTotal.Text = agency.AgentCurrency + " " + ((decimal)(result.TotalFare) + asvAmount + vatAmount).ToString("N" + agency.DecimalValue);
                    //    }
                    //}
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }
        finally
        {
            //hdnBookingAmount.Value = lblTxnAmount.Text.Split(' ')[1];

        }
    }
    private BookingResponse MakeBooking()
    {
        FlightItinerary itinerary = Session["FlightItinerary"] as FlightItinerary;
        itinerary.HotelReq = chkIsHotReq.Checked;
        if (ddlPaymentType.SelectedIndex == 1 || Session["ResultIndex"] != null)
        {
            itinerary.PaymentMode = ModeOfPayment.CreditCard;
            itinerary.CcPayment = new CCPayment(); // in order to avoid error while saving Itinerary
        }
        
        SearchRequest request = null;
        if (Session["FlightRequest"] != null)
        {
            request = Session["FlightRequest"] as SearchRequest;
        }
        

        string TripId = DateTime.Now.ToString("yyyyMMddHmmssFFF");//Format is Year:9999 Month:12 Date:31 Hour:23 Minutes:59 Second:59 Millisecond:999
        if (Settings.LoginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId != 0 && request.AppliedPolicy)
        {            
            itinerary.TripId = TripId;//Set the tripId for Corporate Booking
            if (Session["CriteriaItineraries"] != null)//Assign the same trip id for all bookings
            {
                List<FlightItinerary> CriteriaItineries = Session["CriteriaItineraries"] as List<FlightItinerary>;
                CriteriaItineries.ForEach(x => x.TripId = TripId);
            }
        }
        MetaSearchEngine mse = new MetaSearchEngine(Session["sessionId"].ToString());
        mse.SettingsLoginInfo = Settings.LoginInfo;        
        
        if (Settings.LoginInfo.IsOnBehalfOfAgent)
        {
            agencyId = Settings.LoginInfo.OnBehalfAgentID;
        }
        else
        {
            agencyId = Settings.LoginInfo.AgentId;
        }
        itinerary.AirLocatorCode = null;//Clear G9 value from saving into DB assigned for checking baggage price update
        BookingResponse bookingResponse = new BookingResponse();
        AgentMaster agent = new AgentMaster(agencyId);        
        decimal currentAgentBalance = agent.UpdateBalance(0);
        lblAgentBalance.Text = agency.AgentCurrency + " " + currentAgentBalance.ToString("N" + agency.DecimalValue);

        if (ddlPaymentType.Value == "Card" || currentAgentBalance >= Convert.ToDecimal(lblTxnAmount.Text.Replace(agency.AgentCurrency, "").Trim()) || hdfAction.Value == "Hold" || (Settings.LoginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId != 0 && request.AppliedPolicy))
        {
            //If TBO Air and non-LCC then allow only ticketing - added by shiva 19-aug-2016
            if (btnHold.Visible || (itinerary.FlightBookingSource == BookingSource.TBOAir && !itinerary.IsLCC))// not LCC and TBO Air GDS
            {
                if (hdfAction.Value == "Ticket")
                {
                    if ((itinerary.FlightBookingSource == BookingSource.TBOAir && itinerary.IsDomestic && result.FareType == "Normal") || itinerary.IsLCC)
                    {
                        bookingResponse = mse.Book(ref itinerary, agencyId, BookingStatus.Ticketed, new UserMaster(Settings.LoginInfo.UserID), true, IPAddr);
                    }
                    else
                    {
                        bookingResponse = mse.Book(ref itinerary, agencyId, BookingStatus.Ready, new UserMaster(Settings.LoginInfo.UserID), true, IPAddr);
                    }
                }
                else if (hdfAction.Value == "Hold")
                {
                    bookingResponse = mse.Book(ref itinerary, agencyId, BookingStatus.Hold, new UserMaster(Settings.LoginInfo.UserID), true, IPAddr);
                }
            }
            else if(itinerary.FlightBookingSource == BookingSource.Babylon)//GDS //THERE WILL BE NO HOLD POSITION
            {
                bookingResponse = mse.Book(ref itinerary, agencyId, BookingStatus.Ready, new UserMaster(Settings.LoginInfo.UserID), true, IPAddr);
            }
            else // LCC
            {
                bookingResponse = mse.Book(ref itinerary, agencyId, BookingStatus.Ticketed, new UserMaster(Settings.LoginInfo.UserID), true, IPAddr);
            }

            if (bookingResponse.Status != BookingResponseStatus.Successful)
            {
                //In case of booking failure clear session 
                if (!string.IsNullOrEmpty(itinerary.TripId))
                {
                    mse.ClearSession();// If selected itinerary is failing, then stopping optional itinerary bookings for corporate
                }

                lblError.Text = bookingResponse.Error;
                CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderErrFlightTest1");
                MultiView1.ActiveViewIndex = 1;
            }
            else if (bookingResponse.Status == BookingResponseStatus.Successful)
            {
                if (Settings.LoginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId != 0 && request.AppliedPolicy)
                {
                    Dictionary<FlightItinerary, BookingResponse> BookingResponses = new Dictionary<FlightItinerary, BookingResponse>();
                    BookingResponses.Add(itinerary, bookingResponse);
                    Session["CorpBookingResponses"] = BookingResponses;
                    Session["TripId"] = itinerary.TripId;
                    
                }
                //If Ticketing is not allowed then clear the results from the Basket
                //which we normally do for normal bookings. 
                if (!agent.AgentTicketingAllowed && Settings.LoginInfo.IsCorporate != "Y")
                {
                    Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "ticket not allowed", Request["REMOTE_ADDR"]);
                    mse.ClearSession();
                }
            }

        }
        else
        {
            lblBalanceError.Text = "Insufficient Credit Balance ! Please contact Admin";
        }
        return bookingResponse;
    }
  
    protected void imgBtnPayment_Click(object sender, EventArgs e)
    {
        try
        {
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////// Agent Currency, Rate of Exchange & Fare Check up ///////////////////////////////////////
            decimal rateOfExchange = 0; decimal roe = 0;
            FlightItinerary itinerary = Session["FlightItinerary"] as FlightItinerary;

            decimal baseFare = 0, tax = 0, resultBaseFare = 0, resultTax = 0;
            if (result.ResultBookingSource != BookingSource.TBOAir)
            {

                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    rateOfExchange = Settings.LoginInfo.OnBehalfAgentExchangeRates[result.Price.SupplierCurrency];
                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                }
                else
                {
                    rateOfExchange = Settings.LoginInfo.AgentExchangeRates[result.Price.SupplierCurrency];
                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                }
                StaticData sd = new StaticData();
                sd.BaseCurrency = agency.AgentCurrency;
                roe = sd.CurrencyROE[result.Price.SupplierCurrency];


                //for (int i = 0; i < result.FareBreakdown.Length; i++)
                //{
                //    baseFare += ((((decimal)result.FareBreakdown[i].BaseFare / rateOfExchange) * roe) * result.FareBreakdown[i].PassengerCount);
                //    tax += ((((decimal)result.FareBreakdown[i].Tax / rateOfExchange) * roe) * result.FareBreakdown[i].PassengerCount);
                //    resultBaseFare += (decimal)result.FareBreakdown[i].BaseFare;
                //    resultTax += result.FareBreakdown[i].Tax;
                //}
                SearchRequest request = null;
                int paxCount = 0;
                if (Session["FlightRequest"] != null)
                {
                    request = Session["FlightRequest"] as SearchRequest;
                    paxCount = request.AdultCount + request.ChildCount + request.InfantCount;
                }

                if (result.Price.MarkupType == "P")
                {
                    if (agency.AgentAirMarkupType == "BF")
                    {
                        baseFare = ((decimal)result.Price.PublishedFare) * roe;
                        baseFare += (baseFare * (result.Price.MarkupValue / 100) * paxCount);
                        resultBaseFare = ((decimal)result.Price.PublishedFare) * rateOfExchange;
                        resultBaseFare += (resultBaseFare * (result.Price.MarkupValue / 100) * paxCount);
                    }
                    else if (agency.AgentAirMarkupType == "TX")
                    {
                        baseFare = ((decimal)result.Price.Tax / roe) * roe;
                        baseFare += (baseFare * (result.Price.MarkupValue / 100) * paxCount);
                        resultBaseFare = ((decimal)result.Price.Tax / rateOfExchange) * roe;
                        resultBaseFare += (resultBaseFare * (result.Price.MarkupValue / 100) * paxCount);
                    }
                    else
                    {
                        baseFare = ((decimal)result.Price.SupplierPrice) * roe;
                        baseFare += (baseFare * (result.Price.MarkupValue / 100) * paxCount);
                        resultBaseFare = ((decimal)result.Price.SupplierPrice) * rateOfExchange;
                        resultBaseFare += (resultBaseFare * (result.Price.MarkupValue / 100) * paxCount);
                    }
                }
                else
                {
                    baseFare = (result.Price.SupplierPrice * roe) + (result.Price.MarkupValue * paxCount);
                    resultBaseFare = (result.Price.SupplierPrice * rateOfExchange) + (result.Price.MarkupValue * paxCount);
                }
            }
            else
            {
                
            }
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (agency.AgentCurrency == result.Currency)
            {
                if (rateOfExchange == roe && Math.Round(resultBaseFare, agency.DecimalValue) >= Math.Round(baseFare, agency.DecimalValue))
                {
                    
                    MetaSearchEngine mse = new MetaSearchEngine(Session["sessionId"].ToString());
                    mse.SettingsLoginInfo = Settings.LoginInfo;

                    if (result.IsLCC)
                    {
                        /**************************************************
                        *               Credit Card Payment
                        * *************************************************/
                        if (ddlPaymentType.Value == "Card")
                        {
                            //Added by Somasekhar on 27/06/2018 -- For SafexPay
                            itinerary.PaymentMode = ModeOfPayment.CreditCard;
                            Session["FlightItinerary"] = itinerary;
                            string pgName = rblAgentPG.SelectedItem.Text;
                            GenericStatic.GetSetPageParams("PageParams", pageParams[0] + "," + pgName, "set");//Save the query string params in the session
                            Response.Redirect("PaymentProcessing.aspx", true);
                            //if (pgName == "SafexPay")
                            //{
                            //    Response.Redirect("PaymentProcessing.aspx?paymentGateway=SafexPay&id=" + Request.QueryString["id"], true);
                            //}
                            ////=====================================================
                            //else if (pgName == "CCAvenue")
                            //{
                            //    Response.Redirect("PaymentProcessing.aspx?paymentGateway=CCAvenue&id=" + Request.QueryString["id"], true);
                            //}
                        }
                    }
                    else//Update payment mode for GDS too
                    {
                        if (ddlPaymentType.Value == "Card")
                        {                            
                            itinerary.PaymentMode = ModeOfPayment.CreditCard;
                            Session["FlightItinerary"] = itinerary;
                        }
                    }
                    if (!result.IsLCC || (result.IsLCC && ddlPaymentType.Value != "Card"))
                    {
                        BookingResponse bookingResponse = MakeBooking(); //sai
                        Session["SeatError"] = bookingResponse.Error;
                        Session["BookingResponse"] = bookingResponse;
                        //BookingResponse bookingResponse = mse.Book(ref itinerary, (int)Settings.LoginInfo.AgentId, BookingStatus.Ready, new UserMaster(Settings.LoginInfo.UserID), true, "");                   

                        sessionId = Session["sessionId"].ToString();
                        resultId = Convert.ToInt32(pageParams[0]);
                        if (bookingResponse.Status == BookingResponseStatus.Successful || bookingResponse.Status == BookingResponseStatus.ReturnFailed)
                        {
                            //For GDS Booking after reservation we need to proceed for payment
                            if (!result.IsLCC)
                            {
                                /*************************************************
                                 *          Credit Card Payment
                                 * *************************************************/
                                if (ddlPaymentType.Value == "Card")
                                {
                                    //Added by Somasekhar on 27/06/2018 -- For SafexPay
                                    string pgName = rblAgentPG.SelectedItem.Text;
                                    GenericStatic.GetSetPageParams("PageParams", pageParams[0] + "," + pgName, "set");//Save the query string params in the session
                                    Response.Redirect("PaymentProcessing.aspx", false);
                                    //if (pgName == "SafexPay")
                                    //{
                                    //    Response.Redirect("PaymentProcessing.aspx?paymentGateway=SafexPay&id=" + Request.QueryString["id"], true);
                                    //}
                                    ////=================================================================
                                    //else if (pgName == "CCAvenue")
                                    //{

                                    //    Response.Redirect("PaymentProcessing.aspx?paymentGateway=CCAvenue&id=" + Request.QueryString["id"], true);
                                    //}

                                }
                            }
                            if (ddlPaymentType.Value != "Card")
                            {
                                hdnPNR.Value = bookingResponse.PNR;
                                GenerateTicket(bookingResponse);
                            }
                        }
                    }
                }
                else
                {
                    lblMessage.Text = "Price has been revised for the Flight. Please search again or contact Customer Suuport.";
                }
            }
            else
            {
                lblMessage.Text = "Price has been revised for the Flight. Please search again or contact Customer Suuport.";
            }
        }
        catch (System.Threading.ThreadAbortException ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, " imgBtnPayment_Click"+ex.ToString(), Request["REMOTE_ADDR"]);
        }
        catch (Exception ex)
        {
            MultiView1.ActiveViewIndex = 1;
            lblError.Text = ex.Message;
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderErrFlightTest12");
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    private void GenerateTicket(BookingResponse bookingResponse)
    {
        MetaSearchEngine mse = new MetaSearchEngine(Session["sessionId"].ToString());
        mse.SettingsLoginInfo = Settings.LoginInfo;      
// IF already there in the session(For Dynamic PCC reprice) 
        mse.clsURImpresp = Session["UAPIURImpresp"] != null ? (UAPIdll.Universal46.UniversalRecordImportRsp)Session["UAPIURImpresp"] : mse.clsURImpresp;
        mse.clsUAPIRepriceResp = Session["UAPIReprice"] != null ? (UAPIdll.Air46.AirPriceRsp)Session["UAPIReprice"] : mse.clsUAPIRepriceResp;

        bookingResponse.PNR = !string.IsNullOrEmpty(bookingResponse.PNR) ? bookingResponse.PNR : mse.clsURImpresp.UniversalRecord.ProviderReservationInfo[0].LocatorCode;

        Dictionary<string, string> ticketData = new Dictionary<string, string>();

        ticketData.Add("corporateCode", "1");
        ticketData.Add("tourCode", "1");
        ticketData.Add("endorsement", "1");
        ticketData.Add("remarks", "1");

        if(flightItinerary.FlightBookingSource == BookingSource.Babylon)
            ticketData.Add("TempTicket", "Yes");

        //result = Basket.FlightBookingSession[sessionId].Result[resultId - 1];  //the given key was not present in dictionary when UAPI //sai
        //Added by Ravi. To continue on ward booking if return booking fails in the Normal Return combination

        TicketingResponse ticketingResponse;
        if (flightItinerary.IsLCC)
        {
            ticketingResponse = new TicketingResponse();
            ticketingResponse.Status = TicketingResponseStatus.Successful;
        }
        else
        {
            UserMaster member = new UserMaster(Settings.LoginInfo.UserID);
            ticketingResponse = mse.Ticket(bookingResponse.PNR, member, ticketData, IPAddr);

            //added sai for Sending Email when status is in Ready or Inprogress 
            FlightItinerary itineraryDB = new FlightItinerary(FlightItinerary.GetFlightId(bookingResponse.PNR));
            bookingResponse.BookingId = itineraryDB.BookingId;
            BookingDetail booking = new BookingDetail(itineraryDB.BookingId);

            if (booking.Status == BookingStatus.Ready || booking.Status == BookingStatus.InProgress)
            {
                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                //toArray.Add("sainadhs86@gmail.com"); // todo Ziyad
                //toArray.Add("sainadhs86@gmail.com"); // todo Ziyad
                //string message = "Booking is in " + booking.Status.ToString().ToUpper() + " state for the following PNR:(" + itinerary.PNR + ")"+;
                //string testMode = ConfigurationManager.AppSettings["TestMode"].ToString() == "True" ? "Test Mode" : string.Empty;
                string message = "Booking is in " + booking.Status.ToString().ToUpper() + " state for the following PNR:(" + flightItinerary.PNR + ").Reason:" + ticketingResponse.Message + "\nAgencyId is " + flightItinerary.AgencyId.ToString() + "\n Logged in Member is " + member.FirstName + "(" + (int)member.ID + ", " + member.LoginName + ")\n";


                //Email.Send(CT.Configuration.ConfigurationSystem.Email["fromEmail"], CT.Configuration.ConfigurationSystem.Email["ErrorNotificationMailingId"], testMode+"Ticketing Failed. There was a problem communicating to the Reservation System.",message);
                Email.Send(CT.Configuration.ConfigurationSystem.Email["fromEmail"], CT.Configuration.ConfigurationSystem.Email["ErrorNotificationMailingId"], "Ticketing Failed. There was a problem communicating to the Reservation System.", message);
            }
        }

        if (ticketingResponse.Status == TicketingResponseStatus.Successful)
        {

            int flightId = FlightItinerary.GetProductIdByBookingId(bookingResponse.BookingId, ProductType.Flight); //Changed Avoding getting flight throught pnr(Flight Inventory)
            flightItinerary = new FlightItinerary(flightId);
            Session["FlightItinerary"] = flightItinerary;
            
            try
            {
                //Check whether tickets are saved or not
                List<Ticket> tickets = new List<Ticket>();
                tickets = Ticket.GetTicketList(flightItinerary.FlightId);
                if (tickets != null && tickets.Count > 0)
                {
                    Invoice invoice = new Invoice();
                    int invoiceNumber = Invoice.isInvoiceGenerated(tickets[0].TicketId, ProductType.Flight);

                    if (invoiceNumber > 0)
                    {
                        invoice.Load(invoiceNumber);
                    }
                    else
                    {
                        invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(flightItinerary.FlightId, "", (int)Settings.LoginInfo.UserID, ProductType.Flight, 1);
                        if (invoiceNumber > 0)
                        {
                            invoice.Load(invoiceNumber);
                            invoice.Status = InvoiceStatus.Paid;
                            invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                            invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                            invoice.UpdateInvoice();
                        }
                    }
                    isBookingSuccess = true;
                    //BindTicketInfo(flightItinerary.FlightId);
                }
                else
                {
                    Audit.Add(EventType.Exception, Severity.High, flightItinerary.CreatedBy, "(Invoice)Invoice not created due to Tickets not found for PNR: " + flightItinerary.PNR, Request.ServerVariables["REMOTE_ADDR"]);
                    //TODO: Send email to Cozmo
                    Email.Send(CT.Configuration.ConfigurationSystem.Email["fromEmail"], CT.Configuration.ConfigurationSystem.Email["ErrorNotificationMailingId"], "Invoice Failed to generate due to no tickets for PNR " + flightItinerary.PNR, "<p>Dear Team,<br/>Invoice generation failed due to Tickets were not saved or found for PNR: " + flightItinerary.PNR + "</p><br/><p>Regards,<br/>Cozmo B2B Auto Emailer</p>");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(Invoice)"+ex.Message, Request.ServerVariables["REMOTE_ADDR"]);
                //TODO: Send email to Cozmo
                Email.Send(CT.Configuration.ConfigurationSystem.Email["fromEmail"], CT.Configuration.ConfigurationSystem.Email["ErrorNotificationMailingId"], "Invoice Failed to generate due to error for PNR " + flightItinerary.PNR, "<p>Dear Team,<br/>Invoice generation failed due to Error while generation of Invoice for PNR: " + flightItinerary.PNR + ".<br/> Error details:" + ex.ToString() + " </p><br/><p>Regards,<br/>Cozmo B2B Auto Emailer</p>");
            }

            //If Payment Mode = On Account deduct agent balance, if Credit Card do not deduct balance
            //if (Session["ResultIndex"] == null)
            if(flightItinerary.PaymentMode != ModeOfPayment.CreditCard)
            {
                decimal currentAgentBalance = 0;
                AgentMaster agent = new AgentMaster(agencyId);
                agent.CreatedBy = Settings.LoginInfo.UserID;
                if (flightItinerary.FlightBookingSource != BookingSource.TBOAir)
                {
                    currentAgentBalance = agent.UpdateBalance(0);
                }
                else //For TBO we are ceiling the Total Amount and need to deduct the same from Agent balance
                {
                    //For all other sources we are deducting the balance from Add_Ticket procedure passenger wise i.e
                    //if 6 pax are there six times we are deducting the balance with pax wise total amount. So in case
                    //of TBO Air if we do pax wise ceiling TotalAmount will be higher than the original amount.

                    decimal ticketAmount = 0;
                    foreach (FlightPassenger pax in flightItinerary.Passenger)
                    {
                        ticketAmount += pax.Price.PublishedFare + pax.Price.Tax + pax.Price.Markup + pax.Price.OtherCharges + pax.Price.BaggageCharge +
                            pax.Price.AdditionalTxnFee + pax.Price.SServiceFee + pax.Price.TransactionFee + pax.Price.OutputVATAmount + pax.Price.HandlingFeeAmount -
                            pax.Price.Discount + pax.Price.MealCharge + pax.Price.SeatPrice + pax.Price.DynamicMarkup + 
                            (Settings.LoginInfo.IsOnBehalfOfAgent ? pax.Price.AsvAmount : 0);
                    }
                    if (flightItinerary.PaymentMode == ModeOfPayment.Credit)
                    {
                        currentAgentBalance = agent.UpdateBalance(-Math.Ceiling(ticketAmount));
                    }
                    else
                    {
                        currentAgentBalance = agent.UpdateBalance(0);
                    }
                }
                //if (Session["BookingAgencyID"] == null)
                if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    Settings.LoginInfo.AgentBalance = currentAgentBalance;
                    Label lblAgentBalance = (Label)Master.FindControl("lblAgentBalance");
                    lblAgentBalance.Text = CT.TicketReceipt.Common.Formatter.ToCurrency(currentAgentBalance);
                }
            }
            Session["FZBaggageDetails"] = null;
            Session["ResultIndex"] = null;//Clear the Result Index session
            Session["BookingResponse"] = null;
            Session["FlightItinerary"] = null;
            Session["PageParams"] = null;
            Session["CCParams"] = null;
            //display ticket
            //if b2b2b agent then save ticketed booking todo ziyad

            SendFlightTicketEmail(flightItinerary);
            if (bookingResponse.Status == BookingResponseStatus.ReturnFailed)
            {
                //ScriptManager.RegisterStartupScript(
                //    this,
                //    typeof(Page),
                //    "Alert",
                //    "<script>alert('Alert');</script>",
                //    false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Your return ticket failed. But onward ticket booking successfull. Please book your return ticket separately'); window.location='" + Request.ApplicationPath + "/ViewBookingForTicket.aspx?bookingid=" + bookingResponse.BookingId + "';", true);
            }
            else
            {                
                string url = "ViewBookingForTicket.aspx?bookingid=" + bookingResponse.BookingId;
                if (Settings.LoginInfo.IsCorporate == "Y")
                    url += "&ticketing=" + (sendForApproval ? "No" : "Yes");
                Response.Redirect(url, false);
            }
        }
        else
        {
            if (ticketingResponse.Status != TicketingResponseStatus.PriceChanged)
            {
                Session["FZBaggageDetails"] = null;
                Session["ResultIndex"] = null;//Clear the Result Index session
                Session["BookingResponse"] = null;
                Session["FlightItinerary"] = null;
                Session["PageParams"] = null;
                Session["CCParams"] = null;
            }
            if (ticketingResponse.Status == TicketingResponseStatus.NotSaved)
            {
                FailedBooking fb = FailedBooking.Load(flightItinerary.PNR);
                int id = fb.FailedBookingId;
                MultiView1.ActiveViewIndex = 1;
                lblError.Text = "There may be an error occured while booking. However PNR is created (" + flightItinerary.PNR + ").<br /> Please contact " + CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"] + " for further assistance." + ticketingResponse.Message;
                //if b2b2b agent then save Hold Booking todo ziyad
                //Redirect to agent ticket page
            }
            else if (ticketingResponse.Status == TicketingResponseStatus.PriceChanged)
            {
                Session["UAPIURImpresp"] = mse.clsURImpresp;
                Session["UAPIReprice"] = mse.clsUAPIRepriceResp;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "javascript:ShowReprice('" + ticketingResponse.Message + "');", true);
                MultiView1.ActiveViewIndex = 0;
            }
            else
            {
                MultiView1.ActiveViewIndex = 1;
                if (flightItinerary.PNR != null)
                {
                    lblError.Text = "There may be an error occured while booking. However PNR is created (" + flightItinerary.PNR + ").<br /> Please contact " + CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"] + " for further assistance.";
                }
                else
                {
                    lblError.Text = "There may be an error occured while booking. Please contact Cozmo for more details " + ticketingResponse.Message;
                }

            }
        }

    }

    protected void btnHold_Click(object sender, EventArgs e)
    {
        BookingResponse bookingResponse = new BookingResponse();
        try
        {
                     
            
            FlightItinerary itinerary = Session["FlightItinerary"] as FlightItinerary;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////// Agent Currency, Rate of Exchange & Fare Check up ///////////////////////////////////////
            decimal rateOfExchange = 0; decimal roe = 0;            

            decimal baseFare = 0, tax = 0, resultBaseFare = 0, resultTax = 0;
            if (result.ResultBookingSource != BookingSource.TBOAir)
            {
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    rateOfExchange = Settings.LoginInfo.OnBehalfAgentExchangeRates[result.Price.SupplierCurrency];
                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                }
                else
                {
                    rateOfExchange = Settings.LoginInfo.AgentExchangeRates[result.Price.SupplierCurrency];
                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                }
                StaticData sd = new StaticData();
                sd.BaseCurrency = agency.AgentCurrency;
                roe = sd.CurrencyROE[result.Price.SupplierCurrency];


                //for (int i = 0; i < result.FareBreakdown.Length; i++)
                //{
                //    baseFare += ((((decimal)result.FareBreakdown[i].BaseFare / rateOfExchange) * roe) * result.FareBreakdown[i].PassengerCount);
                //    tax += ((((decimal)result.FareBreakdown[i].Tax / rateOfExchange) * roe) * result.FareBreakdown[i].PassengerCount);
                //    resultBaseFare += (decimal)result.FareBreakdown[i].BaseFare;
                //    resultTax += result.FareBreakdown[i].Tax;
                //}
                SearchRequest request = null;
                int paxCount = 0;
                if (Session["FlightRequest"] != null)
                {
                    request = Session["FlightRequest"] as SearchRequest;
                    paxCount = request.AdultCount + request.ChildCount + request.InfantCount;
                }

                if (result.Price.MarkupType == "P")
                {
                    if (agency.AgentAirMarkupType == "BF")
                    {
                        baseFare = ((decimal)result.Price.PublishedFare) * roe;
                        baseFare += (baseFare * (result.Price.MarkupValue / 100) * paxCount);
                        resultBaseFare = ((decimal)result.Price.PublishedFare) * rateOfExchange;
                        resultBaseFare += (resultBaseFare * (result.Price.MarkupValue / 100) * paxCount);
                    }
                    else if (agency.AgentAirMarkupType == "TX")
                    {
                        baseFare = ((decimal)result.Price.Tax / roe) * roe;
                        baseFare += (baseFare * (result.Price.MarkupValue / 100) * paxCount);
                        resultBaseFare = ((decimal)result.Price.Tax / rateOfExchange) * roe;
                        resultBaseFare += (resultBaseFare * (result.Price.MarkupValue / 100) * paxCount);
                    }
                    else
                    {
                        baseFare = ((decimal)result.Price.SupplierPrice) * roe;
                        baseFare += (baseFare * (result.Price.MarkupValue / 100) * paxCount);
                        resultBaseFare = ((decimal)result.Price.SupplierPrice) * rateOfExchange;
                        resultBaseFare += (resultBaseFare * (result.Price.MarkupValue / 100) * paxCount);
                    }
                }
                else
                {
                    baseFare = (result.Price.SupplierPrice * roe) + (result.Price.MarkupValue * paxCount);
                    resultBaseFare = (result.Price.SupplierPrice * rateOfExchange) + (result.Price.MarkupValue * paxCount);
                }
            }
            else
            {
                
            }
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (agency.AgentCurrency == result.Currency)
            {
                if (rateOfExchange == roe && Math.Round(resultBaseFare, agency.DecimalValue) >= Math.Round(baseFare, agency.DecimalValue))
                {
                    bookingResponse = MakeBooking();
                    //MetaSearchEngine mse = new MetaSearchEngine(Session["sessionId"].ToString());
                    Dictionary<string, string> ticketData = new Dictionary<string, string>();

                    ticketData.Add("corporateCode", "1");
                    ticketData.Add("tourCode", "1");
                    ticketData.Add("endorsement", "1");
                    ticketData.Add("remarks", "1");
                    // bookingResponse = mse.Book(ref itinerary, (int)Settings.LoginInfo.AgentId, BookingStatus.Hold, new UserMaster(Settings.LoginInfo.UserID), true, "");

                    sessionId = Session["sessionId"].ToString();
                    resultId = Convert.ToInt32(pageParams[0]);
                    //  result = Basket.FlightBookingSession[sessionId].Result[resultId - 1];////the given key was not present in dictionary when UAPI //sai

                    if (bookingResponse.Status == BookingResponseStatus.Successful)
                    {
                        //decimal ticketAmount = 0;
                        //foreach (FlightPassenger pax in itinerary.Passenger)
                        //{
                        //    if (itinerary.Passenger[0].Price.NetFare > 0)
                        //    {
                        //        ticketAmount = pax.Price.NetFare + pax.Price.Tax + pax.Price.Markup;
                        //    }
                        //    else
                        //    {
                        //        ticketAmount = pax.Price.PublishedFare + pax.Price.Tax;
                        //    }
                        //}

                        //Updating Agent Balance

                        /* sai  Settings.LoginInfo.AgentBalance -= ticketAmount;
                                AgentMaster agent = new AgentMaster(agencyId);
                                agent.CreatedBy = Settings.LoginInfo.UserID;
                                agent.UpdateBalance(-ticketAmount); */

                        Session["SeatError"] = bookingResponse.Error;
                        Response.Redirect("ViewBookingForTicket.aspx?bookingid=" + bookingResponse.BookingId, false);
                    }
                    else if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        Response.Redirect("ViewBookingForTicket.aspx?bookingid=" + bookingResponse.BookingId, false);
                    }
                }
                else
                {
                    lblMessage.Text = "Price has been revised for the Flight. Please search again or contact Customer Suuport.";
                }
            }
            else
            {
                lblMessage.Text = "Price has been revised for the Flight. Please search again or contact Customer Suuport.";
            }

        }
        catch (Exception ex)
        {
            MultiView1.ActiveViewIndex = 1;
            lblError.Text = ex.Message;
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "0");
        }
        finally
        {
            Session["FZBaggageDetails"] = null;
            Session["ResultIndex"] = null;//Clear the Result Index session
            Session["BookingResponse"] = null;
            Session["FlightItinerary"] = null;
        }
        if (bookingResponse.Status == BookingResponseStatus.Successful)
        {
            //Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + Request.QueryString["id"]);
            Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + bookingResponse.BookingId, false);
        }
    }
    protected void BindTicketInfo(int FlightId)
    {
        try
        {
            if (FlightId > 0)
            {
                flightItinerary = new FlightItinerary(FlightId);
                agency = new AgentMaster(flightItinerary.AgencyId);
                booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(FlightId));
                if (booking.Status == BookingStatus.Ticketed)
                {
                    ticketList = Ticket.GetTicketList(FlightId);
                }                
            }
            if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai)
            {
                foreach (FlightPassenger pax in flightItinerary.Passenger)
                {
                    if (inBaggage.Length > 0)
                    {
                        inBaggage += ", " + pax.BaggageCode.Split(',')[0];
                    }
                    else
                    {
                        inBaggage = pax.BaggageCode.Split(',')[0];
                    }
                    if (pax.BaggageCode.Split(',').Length > 1)
                    {
                        if (outBaggage.Length > 0)
                        {
                            outBaggage += ", " + pax.BaggageCode.Split(',')[1];
                        }
                        else
                        {
                            outBaggage = pax.BaggageCode.Split(',')[1];
                        }
                    }
                }
            }
            airline = new Airline();
            airline.Load(flightItinerary.ValidatingAirlineCode);

            if (airline.LogoFile.Trim().Length != 0)
            {
                string[] fileExtension = airline.LogoFile.Split('.');
                AirlineLogoPath = AirlineLogoPath + flightItinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected string SendFlightTicketEmail(FlightItinerary itinerary)
    {
        string myPageHTML = string.Empty;
        try
        {
            BindTicketInfo(itinerary.FlightId);
            string logoPath = "";
            int agentId = 0;

            if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agentId = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.OnBehalfAgentID;
            }
            else
            {
                agentId = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId;
            }

            string serverPath = "";

            if (Request.Url.Port > 0)
            {
                serverPath = Request.Url.Scheme+"://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }
            else
            {
                serverPath =Request.Url.Scheme+ "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }

            if (agentId > 1)
            {
                logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"] + new AgentMaster(agentId).ImgFileName;
                imgLogo.ImageUrl = logoPath;
            }
            else
            {
                imgLogo.ImageUrl = serverPath + "/images/logo.jpg";
            }


            
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            EmailDiv.RenderControl(htw);
            myPageHTML = sw.ToString();
            //myPageHTML = myPageHTML.Replace("\"", "/\"");
            myPageHTML = myPageHTML.Replace("none", "block");

            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            //Send email to Location email if FLIGHTTRIPSUMMARY_LOCATION_EMAIL set to true otherwise send to pax email
            try
            {
                //Sending emails to location email
                AgentAppConfig clsAppCnf = new AgentAppConfig();
                clsAppCnf.AgentID = agentId;
                string isLocationEmail = clsAppCnf.GetConfigData().Where(x => x.AppKey.ToUpper() == "FLIGHTTRIPSUMMARY_LOCATION_EMAIL" && x.ProductID == 1).Select(y => y.AppValue).FirstOrDefault();
                if (Settings.LoginInfo.IsCorporate == "Y")
                {
                    if (!string.IsNullOrEmpty(isLocationEmail) && isLocationEmail.ToUpper() == "TRUE")
                    {
                        LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.LocationID);
                        AddLocationEmailsForTicketEmail(ref toArray, locationMaster);
                        
                    }
                    else toArray.Add(itinerary.Passenger[0].Email);
                }
                else
                {
                    toArray.Add(itinerary.Passenger[0].Email);                    
                }

            }
            catch { }
            

            string subject = "Ticket Confirmation - " + itinerary.PNR;
            AgentMaster agency = new AgentMaster(itinerary.AgencyId);
            string bccEmails = string.Empty;
            if (!string.IsNullOrEmpty(agency.Email1))
            {
                bccEmails = agency.Email1;
            }
            if (!string.IsNullOrEmpty(agency.Email2))
            {
                bccEmails = bccEmails + "," + agency.Email2;
            }
            if (Sendmail == true)
            {
                if (ViewState["MailSent"] == null)//If booking in not corporate then send email
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable(), bccEmails);
                    ViewState["MailSent"] = true;
                }
            }
            
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to Send Flight E-ticket Email PNR " + itinerary.PNR + ": reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        return myPageHTML;
    }

    /// <summary>
    /// This function is used to add location emails for corporate agents in order to send ticket voucher. If location emails are not null then mails will be added
    /// </summary>
    /// <param name="lstToMails">List of string with email collection to be sent</param>
    /// <param name="locationMaster">LocationMaster object for adding emails</param>
    private void AddLocationEmailsForTicketEmail(ref List<string> lstToMails, LocationMaster locationMaster)
    {
        List<string> toArray = lstToMails;
        if (!string.IsNullOrEmpty(locationMaster.LocationEmail) && locationMaster.LocationEmail.Contains("@"))
        {
            if (locationMaster.LocationEmail.Contains(","))
            {
                string[] emails = locationMaster.LocationEmail.Split(',');
                emails.ToList().ForEach(e => toArray.Add(e));
            }
            else
            {
                toArray.Add(locationMaster.LocationEmail);
            }
        }
    }


    //Added by lokesh on 4-July-2018
    /// <summary>
    /// This method returns the baggage for GDS suppliers from table BKE_Segment_PTC_Detail
    /// </summary>
    /// <param name="flightId"></param>
    /// <returns></returns>
    protected string GetBaggageForGDS(int flightId,PassengerType paxType)
    {
        string gdsBaggage = string.Empty;
        try
        {
           List<SegmentPTCDetail> segmentPTCDetails = new List<SegmentPTCDetail>();
           List<SegmentPTCDetail> paxTypeSegmentPTCDetails = new List<SegmentPTCDetail>();
            string paxCode = string.Empty;
            switch (paxType)
            {
                case PassengerType.Adult:
                    paxCode = "ADT";
                    break;
                case PassengerType.Child:                  
                     paxCode = "CNN";
                    break;
                case PassengerType.Infant:
                    paxCode = "INF";
                    break;
            }

           segmentPTCDetails =  SegmentPTCDetail.GetSegmentPTCDetail(flightId);
            paxTypeSegmentPTCDetails = segmentPTCDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.PaxType.ToLower().Equals(paxCode.ToLower()); });
            if (paxTypeSegmentPTCDetails != null && paxTypeSegmentPTCDetails.Count >0)
            {
                for(int i=0;i< paxTypeSegmentPTCDetails.Count;i++)
                {
                    if(!string.IsNullOrEmpty(paxTypeSegmentPTCDetails[i].Baggage))
                    {
                        if (string.IsNullOrEmpty(gdsBaggage))
                        {
                            gdsBaggage = !paxTypeSegmentPTCDetails[i].Baggage.ToLower().Contains("piece") ? (paxTypeSegmentPTCDetails[i].Baggage.ToLower().Contains("kg") ? paxTypeSegmentPTCDetails[i].Baggage : paxTypeSegmentPTCDetails[i].Baggage + "Kg") : paxTypeSegmentPTCDetails[i].Baggage;
                        }
                        else
                        {
                            gdsBaggage += "," + (!paxTypeSegmentPTCDetails[i].Baggage.ToLower().Contains("piece") ? (paxTypeSegmentPTCDetails[i].Baggage.ToLower().Contains("kg") ? paxTypeSegmentPTCDetails[i].Baggage : paxTypeSegmentPTCDetails[i].Baggage + "Kg") : paxTypeSegmentPTCDetails[i].Baggage);
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Failed to read baggage" + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        if(string.IsNullOrEmpty(gdsBaggage) && paxType != PassengerType.Infant)
        {
            gdsBaggage = "Airline Norms";
        }
        return gdsBaggage;
    }
    //Added by Sonmasekhar -- for loading Payment gateways
    private void LoadPaymentGateways()
    {
        try
        {
            DataTable dtPaymentGateways = AgentMaster.GetAgentPaymentgateways(agencyId, (int)ProductType.Flight);

            if (dtPaymentGateways.Rows.Count > 0)
            {
                //agentPGCount = dtPaymentGateways.Rows.Count;
                foreach (DataRow dr in dtPaymentGateways.Rows)
                {
                    rblAgentPG.Items.Add(new ListItem(Convert.ToString(dr["name"]), Convert.ToString(dr["pgcharge"])));
                    
                }
                rblAgentPG.ClearSelection();
                if (rblAgentPG.Items.Count > 0)
                {   
                    rblAgentPG.Items[0].Selected = true;
                    charges = Convert.ToDecimal(rblAgentPG.Items[0].Value);
                }

            }


        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to load Agent PaymentGateways " + ". reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
    public void GetConfigData(string appkey)
    {
        AgentAppConfig agentAppConfig = new AgentAppConfig();
        agentAppConfig.AgentID = 1;
        List<AgentAppConfig> agentAppConfigs = agentAppConfig.GetConfigData();
        //string desc = agentAppConfigs.Where(x => Convert.ToInt32(x.Source) == (int)BookingSource.FlightInventory && x.AppKey.ToUpper() == appkey.ToUpper() && x.AppValue.ToUpper() == "TRUE").FirstOrDefault().Description;
        if (agentAppConfigs.Exists(x => x.AppKey == appkey))
        {
            string desc = agentAppConfigs.Where(x => x.AppKey.ToUpper() == appkey.ToUpper() && x.AppValue.ToUpper() == "TRUE").FirstOrDefault().Description;
            if (!string.IsNullOrEmpty(desc))
            {
                if (appkey == "FI_PaymentConfirmation_Alert")
                {
                    lblPaymentConfigData.Text = desc;
                }
                else if (appkey == "FI_Email_Alert")
                {
                    lblEmailConfigData.Text = desc;
                }
            }
        }
    }

    protected void btnRepriceContinue_Click(object sender, EventArgs e)
    {
        BookingResponse bookingResponse = new BookingResponse();
        try
        {
            if (btnHold.Visible || imgBtnPayment.Visible)
            {
                flightItinerary = Session["FlightItinerary"] != null ? (FlightItinerary)Session["FlightItinerary"] : new FlightItinerary();
                GenerateTicket(bookingResponse);
                hdnPNR.Value = string.Empty;
                ClearSession();
                Response.Redirect("ViewBookingForTicket.aspx?bookingid=" + bookingResponse.BookingId, false);
            }
        }
        catch (Exception ex)
        {
            MultiView1.ActiveViewIndex = 1;
            lblError.Text = ex.Message;
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), "0");
        }
        finally
        {
            Session["FZBaggageDetails"] = null;
            Session["ResultIndex"] = null;//Clear the Result Index session
            Session["BookingResponse"] = null;
            Session["FlightItinerary"] = null;
        }
    }

    protected void btnRepriceCancel_Click(object sender, EventArgs e)
    {
        ClearSession();
        Response.Redirect("HotelSearch.aspx?source=Flight", true);
    }

    /// <summary>
    /// To clear all the existing sessions
    /// </summary>
    private void ClearSession()
    {
        Session["FZBaggageDetails"] = Session["ResultIndex"] = Session["BookingResponse"] = Session["FlightItinerary"] =
        Session["FlightItinerary"] = Session["UAPIURImpresp"] = Session["UAPIReprice"] = Session["FlightRequest"] =
        Session["sessionId"] = Session["CorpBookingResponses"] = Session["ISPRiceChangedTBO"] = Session["TripId"] =
        Session["PaymentInformationId"] = null;
        hdnPNR.Value = string.Empty;
    }
}
