﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using CT.Core;
using CT.BookingEngine;
using CT.Configuration;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.MetaSearchEngine;
using CT.Corporate;
using CT.AccountingEngine;
using System.Net;
using System.Web.Script.Serialization;
using CT.TicketReceipt.Web.UI.Controls;
using System.Web.Script.Services;

public partial class SearchHotelHistory : ParentPage //System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            ToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

            InitializeControls();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        try
        {
            Utility.StartupScript(this.Page, "BindHotelSearchGrid()", "BindHotelSearchGrid");
        }
        catch (Exception ex)
        {
           Audit.Add(EventType.HotelSearch, Severity.High, 1, "Failed to Bind HotelSearch Grid Details Info. Reason: " + ex.ToString(), "");
        }
    }

    private void InitializeControls()
    {
        BindAgent();
        BindNationality();
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
        {
            ddlLocations.Enabled = true;
        }
        ddlLocations.DataSource = LocationMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
        ddlLocations.DataTextField = "location_name";
        ddlLocations.DataValueField = "location_id";
        ddlLocations.DataBind();
    }

    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            ddlAgents.DataSource = dtAgents;
            ddlAgents.DataTextField = "Agent_Name";
            ddlAgents.DataValueField = "agent_id";
            ddlAgents.DataBind();
            ddlAgents.Items.Insert(0, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindConsultant(int agentId)
    {
        try
        {
            ddlConsultant.DataSource = UserMaster.GetList(agentId, ListStatus.Short, RecordStatus.All);
            ddlConsultant.DataValueField = "USER_ID";
            ddlConsultant.DataTextField = "USER_FULL_NAME";
            ddlConsultant.DataBind();
            ddlConsultant.Items.Insert(0, new ListItem("--Select User--", "-1"));
        }
        catch { throw; }
    }

    /// <summary>
    /// To bind nationality drop down
    /// </summary>
    private void BindNationality()
    {
        DOTWCountry dotwC = new DOTWCountry();
        Dictionary<string, string> countryList = dotwC.GetAllCountries();

        ddlNationality.DataSource = countryList;
        ddlNationality.DataValueField = "key";
        ddlNationality.DataTextField = "value";
        ddlNationality.DataBind();
        ddlNationality.Items.Insert(0, new ListItem("--Select Nationality--", "-1"));
    }

    protected void ddlAgents_SelectedIndexChanged(object sender, EventArgs e)
    {
        int agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
        if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
        string type = string.Empty;
        if (agentId == 0)
        {
            type = "BASE";
        }
        BindConsultant(agentId);
        BindLocation(Utility.ToInteger(ddlAgents.SelectedItem.Value), type);
    }

    void BindLocation(int agentId, string type)
    {
        DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);

        ddlLocations.Items.Clear();
        ddlLocations.DataSource = dtLocations;
        ddlLocations.DataTextField = "location_name";
        ddlLocations.DataValueField = "location_id";
        ddlLocations.DataBind();

        ListItem item = new ListItem("All", "-1");
        ddlLocations.Items.Insert(0, item);
    }

    #region Bind Grid
    /// <summary>
    ///  To Bind Hotel Search Request History Details
    /// </summary>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <param name="user"></param>
    /// <param name="agent"></param>
    /// <param name="location"></param>
    /// <param name="cityName"></param>
    /// <param name="checkIn"></param>
    /// <param name="checkOut"></param>
    /// <param name="Nationality"></param>
    /// <returns></returns>
    [System.Web.Services.WebMethod]
    public static string BindGrid(string fromDate, string toDate, int user, int agent, int location, string cityName, string checkIn, string checkOut, int Nationality)
    {
        IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
        DateTime fromDateTime = DateTime.ParseExact(fromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime toDateTime = DateTime.ParseExact(toDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime CheckInDate = (checkIn.Length > 0) ? Convert.ToDateTime(checkIn, dateFormat) : DateTime.MinValue;
        DateTime CheckOutDate = (checkOut.Length > 0) ? Convert.ToDateTime(checkOut, dateFormat) : DateTime.MinValue;

        DataTable HotelSearchList = HotelSearchInfo.GetHotelSearchDetails(fromDateTime, toDateTime, (user > 0) ? user : 0, agent,
            (location >= 0) ? location : -1, cityName, CheckInDate, CheckOutDate, Nationality).Tables[0];

        List<HotelSearchInfo> hsList = new List<HotelSearchInfo>();
        foreach (DataRow dr in HotelSearchList.Rows)
        {
            HotelSearchInfo h = new HotelSearchInfo();
            h.HistoryId = Utility.ToInteger(dr["HistoryId"]);
            h.StartDate = Utility.ToDate(dr["CheckInDate"]);
            h.EndDate = Utility.ToDate(dr["CheckOutDate"]);
            h.CityName = Utility.ToString(dr["CityName"]);
            h.NoOfRooms = Utility.ToInteger(dr["NoOfRooms"]);
            h.MinRating = Utility.ToInteger(dr["MinRating"]);
            h.MaxRating = Utility.ToInteger(dr["MaxRating"]);
            h.CountryName = Utility.ToString(dr["CountryName"]);
            h.PassengerNationality = Utility.ToString(dr["PassengerNationality"]);
            h.SearchDateTime = Utility.ToDate(dr["SearchDateTime"]);
            h.AgentName = Utility.ToString(dr["agent_name"]);
            h.LocationName = Utility.ToString(dr["location_name"]);
            h.Sources = Utility.ToString(dr["Sources"]);
            h.UserName = Utility.ToString(dr["UserName"]);
            h.UserId = Utility.ToInteger(dr["UserId"]);
            h.LocationId = Utility.ToInteger(dr["LocationId"]);
            h.AgentId = Utility.ToInteger(dr["AgentId"]);
            h.CityId = Utility.ToInteger(dr["CityId"]);
            h.TransType = Utility.ToString(dr["TransType"]);
            hsList.Add(h);
        }

        string jsonResponse = Newtonsoft.Json.JsonConvert.SerializeObject(hsList);
        return jsonResponse;
    }

    /// <summary>
    /// To Get Room Guest Details
    /// </summary>
    /// <param name="HistoryId"></param>
    /// <returns></returns>
    [System.Web.Services.WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetRoomGuestDetails(int HistoryId)
    {
        try
        {
            DataTable HotelSearchList = HotelSearchInfo.GetRoomGuestDetails(Convert.ToInt32(HistoryId));
            List<RoomGuest> rgList = new List<RoomGuest>();
            foreach (DataRow dr in HotelSearchList.Rows)
            {
                RoomGuest roomGuestobj = new RoomGuest();
                roomGuestobj.Adults = Utility.ToInteger(dr["Adults"]);
                roomGuestobj.childs = Utility.ToInteger(dr["Childs"]);
                roomGuestobj.ChildAge1 = Utility.ToInteger(dr["ChildAge1"]);
                roomGuestobj.ChildAge2 = Utility.ToInteger(dr["ChildAge2"]);
                roomGuestobj.ChildAge3 = Utility.ToInteger(dr["ChildAge3"]);
                roomGuestobj.ChildAge4 = Utility.ToInteger(dr["ChildAge4"]);
                roomGuestobj.ChildAge5 = Utility.ToInteger(dr["ChildAge5"]);
                roomGuestobj.ChildAge6 = Utility.ToInteger(dr["ChildAge6"]);
                rgList.Add(roomGuestobj);
            }
            string JSONString = Newtonsoft.Json.JsonConvert.SerializeObject(rgList);
            return JSONString;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.HotelSearch, Severity.High, 1, "Failed to Get RoomGuest Details Info. Reason: " + ex.ToString(), "");
            return null;
        }
    }

    #endregion

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear();
    }

    private void Clear()
    {
        try
        {
            FromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            ToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            ddlAgents.SelectedIndex = 0;
            txtCityName.Text = string.Empty;
            CheckIn.Text = string.Empty;
            CheckOut.Text = string.Empty;
            ddlLocations.SelectedIndex = -1;
            ddlConsultant.SelectedIndex = -1;
            lblError.Text = string.Empty;
            ddlNationality.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.HotelSearch, Severity.High, 1, "Failed to clear Details. Reason: " + ex.ToString(), "");
        }
    }

}
