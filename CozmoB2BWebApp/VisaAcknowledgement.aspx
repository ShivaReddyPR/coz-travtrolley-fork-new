﻿<%@ Page Language="C#"  AutoEventWireup="true" Inherits="VisaAcknowledgement"  validateRequest="false" Codebehind="VisaAcknowledgement.aspx.cs" %>
<%@ Import Namespace="System.Collections.Generic" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme %>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="<%=Request.Url.Scheme %>://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title >Cozmo Travels</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="Scripts/jsBE/prototype.js"></script>
    <script src="Scripts/jsBE/Utils.js" type="text/javascript"></script>
</head>
<body>
<script type="text/javascript">

    if (window.XMLHttpRequest) {
        Ajax = new window.XMLHttpRequest();
    }
    else {
        Ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }

    function PrintContent() {
        document.getElementById('printDiv').style.display = 'none';
        window.print();
        //try{
        // 
        //var oIframe = document.getElementById('ifrmPrint');
        //var oContent = document.getElementById('divtoprint').innerHTML;
        //var oDoc = (oIframe.contentWindow || oIframe.contentDocument);
        //if (oDoc.document) oDoc = oDoc.document;
        //oDoc.write("<head><title>Visa Acknowledgement</title>");
        //oDoc.write('<link rel="stylesheet" type="text/css" href="style/cozmo-style.css" /></head><body onLoad="self.print()">');
        //oDoc.write(oContent + "</body>");

        //oDoc.close();

        //}
        //catch(e){
        //self.print();
        //}
        setTimeout("document.getElementById('printDiv').style.display='block'", 1000);
    }

    function HideEmailDiv() {
        document.getElementById('LowerEmailSpan').style.display = 'none';
        document.getElementById('addressBox').value = "";        
    }

    function ShowEmailDivD() {
        document.getElementById('LowerEmailSpan').style.display = 'block';
        document.getElementById('SentMessage').innerHTML = '';
        document.getElementById('addressBox').focus();
    }

    function SendMail() {
        var ValidEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var addressList = document.getElementById('addressBox').value;
        if (Trim(addressList) == '') {
            document.getElementById('errMesage').style.display = 'block';
            document.getElementById('errMesage').innerHTML = 'Please enter email id';
            return;
        }
        else if (!ValidEmail.test(Trim(addressList))) {
            document.getElementById('errMesage').style.display = 'block';
            document.getElementById('errMesage').innerHTML = 'Please enter valid email id';
            return;
        }
        else {
            document.getElementById('errMesage').innerHTML = '';
        }
        var visaId = document.getElementById('visaId').value;
        var paramList = 'visaId=' + visaId;
        paramList += "&addressList=" + addressList;
        paramList += "&Type=Acknowledgement";
        var url = "Email_Visa";

        Ajax.onreadystatechange = DisplayMessage;
        Ajax.open('POST', url);
        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        Ajax.send(paramList);
    }
    function DisplayMessage() {
        if (Ajax.readyState == 4) {
            if (Ajax.status == 200) {
                if (Ajax.responseText.length > 0) {
                    document.getElementById('SentMessage').style.display = 'block';
                    document.getElementById('SentMessage').innerHTML = 'Email Sent Successfully';
                    HideEmailDiv();
                }
            }
        }
    }

</script>

    
    <div class="coz_content_container" style="margin-left:30px">
        <div class="row_div" style="width:777px">
            <div id="printDiv" style="float: right; width:100%">
              <span class="fleft" id="SentMessage" style=" width:72%; margin-top:10px; color:Red; font-weight:bold ">&nbsp;</span>
               <span> <a id="btnPrint1" class="mini_menu_link" href="javascript:PrintContent()">Print 
                <img title="Print" src="images/print.gif" style="padding-left: 2;"  alt="Print" /></a>
               | </span>
                <span><a href="javascript:ShowEmailDivD()" >Email Receipt <img title="Email" src="images/email1.gif" style="padding-left: 2;" height="28px"  align="" /></a>
               </span>
            </div>
           
            <div id="divtoprint" class="search_box_main border_no width_100">
                <!-- Search Section start here -->
                <div class="airfare_calender width_100 bg_white">
                <input type="hidden" id="visaId" value="<%=visaId %>" />
                    
                    <div style=" background:#275eae; padding:4px 10px 4px 20px; color:#fff; font-size:12px; font-weight:bold">
                        Visa Acknowledgment Receipt
                    </div>
                    <div class="login">
                        <div class="parent width_98 margin_left_10 border_no">
                            <div class="visa_receipt">
                                <p>
                                    <b>Date of Issue</b><span>:
                                        <%=application.CreatedOn.Day%>-<%=String.Format("{0:MMM}", application.CreatedOn)%>-<%=application.CreatedOn.Year%></span></p>
                                <p>
                                    <b>Trip ID</b><span>: VISA<%=application.CreatedOn.ToString("yy") %><%=String.Format("{0:MM}", application.CreatedOn)%><%=application.VisaId%></span></p>
                              <p>
                                    <b>Applicants</b><span>: Adult
                                        <%=Adult %>
                                        Child
                                        <%=Child%>
                                        Infant
                                        <%=Infant%></span></p>
                            </div>
                            <div class="visa_receipt_txt">
                                <p>
                                    Thank you for choosing cozmotravel.Please find your TRIP ID which will be your future
                                    reference for any details or queries on your application, we will verify if the
                                    documents & details provided by you are in accordance with the UAE Immigration Law
                                    & Policies and update your application status on the MY TRIP Button which you find
                                    at the top right side of the homepage within one hour of submitting your application.
                                    To get the MY TRIP Button on the home page you need to sign in with your user name
                                    and password. User name would be your email ID, password is sent to your email.
                                </p>
                                <p>
                                    If your application meets UAE Immigration Law and Policies your TRIP ID status would
                                    be shown as Eligible and payment can be made online by your credit card using the
                                    make payment link.
                                </p>
                            </div>
                            <table class="visa_applicant_name" cellpadding="0" cellspacing="0" style="font-size:12px">
                                <tr>
                                    <th>
                                        Applicant Name
                                    </th>
                                    <th>
                                        Type of Visa
                                    </th>
                                    <th>
                                        Visa Fee
                                    </th>
                                    <th>
                                        Refundable Security deposit
                                    </th>
                                    <th>
                                        Total Fee
                                    </th>
                                </tr>
                                <% 
                                    int depositFeeFor = 0;
                                    int payDepositFee = 0;
                                    List<string> familyName = new List<string>();
                                   for (int i = 0; i <application.PassengerList.Count; i++)
                                   {
                                        %>
                                <tr>
                                    <td>
                                        <%=application.PassengerList[i].Title%> <%=application.PassengerList[i].Firstname %> <%=application.PassengerList[i].LastName %>
                                    </td>
                                    <td>
                                        <%=visaTypeName%>
                                    </td>
                                    <td>
                                        <% =Math.Round((application.PassengerList[0].Price.NetFare),2)%>
                                    </td>
                                    <% 

                                       // if (!familyName.Contains(application.PassengerList[i].LastName.ToLower()) || i == 0)
                                        {
                                            depositFeeFor++;
                                            familyName.Add(application.PassengerList[i].LastName.ToLower());
                                            payDepositFee = 1;
                                        }%>
                                    <td>
                                        <%=Math.Round(application.PassengerList[0].Price.OtherCharges, 2)%>
                                    </td>
                                   <%-- <%}
                                      else
                                      {
                                          payDepositFee = 0; %>
                                      <td>0.00</td>
                                      <%} %>--%>
                                    <td>
                                        <%=Math.Round(application.PassengerList[0].Price.NetFare + (application.PassengerList[0].Price.OtherCharges * payDepositFee), 2)%>
                                    </td>
                                </tr>
                                <%} %>
                                <tr>
                                    <td class="border_top" colspan="3">
                                    </td>
                                   
                                    <td class="border_top">
                                        <b>Grand Total</b>
                                    </td>
                                    <td class="border_top">
                                        <b>
                                            <%=Math.Round((application.PassengerList[0].Price.NetFare * application.PassengerList.Count) + (application.PassengerList[0].Price.OtherCharges * depositFeeFor), 2)%></b>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                
                <!-- //Search Section start here -->
                <!-- Hotel Section Starts Here -->
            </div>
            <div style="font-size:11px;"><br /><br />
                    <span style="color:Red;">Note:</span> Once passenger exits out of the country and after sending us the exit stamp passport 
                        page we will refund back the Security Deposit Amount. 
                </div>
        </div>
    </div>
   
      <div id="LowerEmailSpan" class="fleft" style="margin-left: 15px;display:none;">                       
       
        <div id="emailBlock" class="width-200 ">
                        <div class="fleft border-y width-200" style="position:absolute; left:370px; top:78px;">
                            <div class="fright text-right padding-5 width-190 light-gray-back">
                            <span class="fleft" id="errMesage" style="color:Red"></span>
                                <img alt="Close" onclick="HideEmailDiv()" src="Images/close.gif" /></div>
                            <div class="fleft padding-5 width-190 white-bg border-top-black">
                                <div class="fleft center width-100 bold">
                                    Enter email address</div>
                                <div class="fleft center width-100 margin-top-5">
                                    <input id="addressBox" name="" type="text" /></div>
                                <div class="fleft center width-100 margin-top-5">
                                    <input onclick="SendMail()" type="button" value="Send mail" /></div>
                                <div class="fleft center width-100 margin-top-5 font-10">
                                    <a href="javascript:HideEmailDiv()">Cancel</a></div>
                            </div>
                        </div>
                    </div>  
                     </div>
    <iframe id="ifrmPrint" src="#" style="width:0px; height:0px; position:absolute; z-index:-1000; "> </iframe>
    </body>
    </html>


