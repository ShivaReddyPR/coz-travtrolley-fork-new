﻿using CT.BookingEngine;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace CozmoB2BWebApp
{
    public partial class ApiHotelResults : CT.Core.ParentPage//System.Web.UI.Page
    {

        protected HotelRequest request = new HotelRequest();
        protected List<AgentAppConfig> liAppconfig = new List<AgentAppConfig>();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                
                if (Session["req"] == null || Settings.LoginInfo == null)
                {
                    Response.Redirect("AbandonSession.aspx", true);
                }
                else
                {
                    request = Session["req"] as HotelRequest;
                    hdnobj.Value = JsonConvert.SerializeObject(request);
                    hdnHotelResults.Value= Session["GXResults"] != null ? JsonConvert.SerializeObject(Session["GXResults"] as HotelSearchResult[]) : string.Empty;
                    hdndecimal.Value= JsonConvert.SerializeObject(Settings.LoginInfo.DecimalValue);
                    hdnSession.Value= Session["cSessionId"] != null ? JsonConvert.SerializeObject(Session["cSessionId"]) : string.Empty;
                    if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.AgentId);
                        hdnBehalfLocation.Value = JsonConvert.SerializeObject(0);
                        LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.LocationID);
                        request.LoginCountryCode = locationMaster.CountryCode!=null? locationMaster.CountryCode:"";
                    }
                    else
                    {
                        hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentID);
                        hdnBehalfLocation.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentLocation);
                        LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                        request.LoginCountryCode = locationMaster.CountryCode != null ? locationMaster.CountryCode : "";
                    }
                    hdnUserId.Value= JsonConvert.SerializeObject(Settings.LoginInfo.UserID);
                    hdnAgentType.Value= JsonConvert.SerializeObject(Settings.LoginInfo.AgentType);

                    if (!string.IsNullOrEmpty(request.Corptravelreason))
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(TravelUtility.GetTravelReasonList("S", Settings.LoginInfo.AgentId, ListStatus.Short));
                        ds.Tables.Add(TravelUtility.GetProfileList(Settings.LoginInfo.CorporateProfileId, Settings.LoginInfo.AgentId, ListStatus.Short));
                        hdnCorpInfo.Value = JsonConvert.SerializeObject(ds);
                    }

                    Session["req"] = request;
                    liAppconfig = new AgentAppConfig { AgentID = Settings.LoginInfo.AgentId }.GetConfigData();
                    

                }
            }
            catch (Exception ex)
            {
                if (Settings.LoginInfo != null)
                {
                    Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get Hotel Search Results. Error: " + ex.Message, Request["REMOTE_ADDR"]);
                }
                else
                {
                    Response.Redirect("AbandonSession.aspx");
                }
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Dictionary<string, string> LoadModifyData()
        {
            try
            {
                DOTWCountry dotwC = new DOTWCountry();
                Dictionary<string, string> countryList = dotwC.GetAllCountries();
               
                return countryList;
            }
            catch
            {
                return null;
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string BindSuppliers()
        {
            try
            {
             DataTable   dtHotelSources = AgentMaster.GetAgentSources(Settings.LoginInfo.AgentId, 2, true);
                DataTable dtGimmonixSuppliers = null;
                // dtHotelSources = dtHotelSources.Select("Name='" + HotelBookingSource.GIMMONIX.ToString() + "'").Count() > 0 ?
                //        new HotelSource().LoadGimmonixSuppliers() : dtHotelSources;
                foreach (DataRow dr in dtHotelSources.Rows)
                {
                    if(Convert.ToString(dr["Name"])=="GIMMONIX")
                    {
                        dtGimmonixSuppliers = new HotelSource().LoadGimmonixSuppliers();
                    }
                }
                Dictionary<string, Object> Data = new Dictionary<string, object>();
                Data.Add("Suppliers", dtHotelSources);
                Data.Add("ChildSuppliers", dtGimmonixSuppliers);
                string JsonString= JsonConvert.SerializeObject(Data);
                return JsonString;
            }
            catch
            {
                return null;
            }
        }
        
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Dictionary<string, string> LoadCity()
        {
            try
            {

                SqlParameter[] paramlist = new SqlParameter[0];
                Dictionary<string, string> CityList = new Dictionary<string, string>();
                using (DataTable dtCity = DBGateway.FillDataTableSP("BKE_HTL_HOTEL_CITY_LIST", paramlist))
                {
                    if (dtCity != null)
                    {
                        foreach (DataRow data in dtCity.Rows)
                        {
                            if (data["CityId"] != DBNull.Value && data["CityName"] != DBNull.Value)
                            {
                                CityList.Add(Convert.ToString(data["CityId"]), Convert.ToString(data["CityName"]));
                            }
                        }
                    }
                }
                return CityList;
            }
            catch
            {
                return null;
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string ModifySearch(string data)
        {
            try
            {
                HotelRequest Request   =  JsonConvert.DeserializeObject<HotelRequest>(data);
              HttpContext.Current.Session["req"] = Request;
                return "Success";
            }
            catch
            {
                return null;
            }
        }
        
        /* Store results into session to restore on page back click */
        [WebMethod(EnableSession = true)]
        public static string UpdateResults(HotelSearchResult[] data,string session)
        {                    
            HttpContext.Current.Session["GXResults"] = data;
            HttpContext.Current.Session["cSessionId"] = session;
            return "Success";            
        }
        /* Store tokenid into session for authentication */
        [WebMethod(EnableSession = true)]
        public static string UpdateTokenId(string bearerToken)
        {
            HttpContext.Current.Session["ApiTokenId"] = bearerToken;
            return "Success";
        }
    }
}
