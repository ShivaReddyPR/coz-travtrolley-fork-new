﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="PassengerDetails" Title="Flight Passenger Details" CodeBehind="PassengerDetails.aspx.cs" %>
<%@ Import Namespace="CT.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <%try
        { %>
    <link rel="stylesheet" href="css/accordian.css" />
    <link rel="stylesheet" type="text/css" href="css/seat-charts.css" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />

    <style>
        
        a {
            color: #b71a4c;
        }
         
        .disabled {
            color: #ccc;
            pointer-events:none;
        }

        .front-indicator {
            
            background-color: #8f858a;
            color: #fff;
            text-align:center;
            padding: 3px;
            border-radius:5px;
            margin:auto;
            display:inline-block;
            width:140px; margin-bottom:10px;
        }

        .wrapper {
            width: 100%;
            text-align: center;
            margin-top: 150px;
        }



        .booking-details {
            float: left;
            text-align: left;
            margin-left: 35px;
            font-size: 12px;
            position: relative;
            height: 401px;
        }

            .booking-details h2 {
                margin: 25px 0 20px 0;
                font-size: 17px;
            }

            .booking-details h3 {
                margin: 5px 5px 0 0;
                font-size: 14px;
            }

        div.seatCharts-cell {
            color: #182C4E;
            height: 30px;
            width: 30px;
            line-height: 30px;
        }

        div.seatCharts-seat {
            color: #FFFFFF;
            cursor: pointer;
        }

        div.seatCharts-row {
            height: 35px;
            width: 450px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
        }

        div.seatCharts-seat.available {
            background-color: #5ec034;
        }

            div.seatCharts-seat.available.first-class {
                /* 	background: url(vip.png); */
                background-color: #3a78c3;
            }

        div.seatCharts-seat.focused {
            background-color: #76B474;
        }

        div.seatCharts-seat.selected {
            background-color: #E6CAC4;
        }

        div.seatCharts-seat.unavailable {
            background-color: #6d6b6c;
        }

        div.seatCharts-container {
            /*border-right: 1px dotted #adadad;
            width: 260px;*/
            padding: 0px 10px 20px 10px;
            float: left;
        }

        div.seatCharts-legend {
            padding-left: 0px;
            position: absolute;
            bottom: 16px;
        }

        ul.seatCharts-legendList {
            padding-left: 0px;
        }

        span.seatCharts-legendDescription {
            margin-left: 5px;
            line-height: 30px;
        }

        .checkout-button {
            display: block;
            margin: 10px 0;
            font-size: 14px;
        }

        #selected-seats {
            max-height: 90px;
            overflow-y: scroll;
            overflow-x: none;
            width: 170px;
        }

        .ui-accordion .ui-accordion-header {  
            padding: 9px 10px 9px 10px;
            font-size: 17px;
            font-weight: 700;
            position:relative;

            color:#fff;
    
        }
  
        .custom-arrow-down,.custom-arrow-up {  
            width:23px; 
            height:23px;
            display:block; 
            position:absolute;
            right:10px;
            top: 13px;
            background:none!important;
            text-indent: inherit !important;
        }
        
        .custom-arrow-up:before,.custom-arrow-down:before{
            font-family: 'Glyphicons Halflings';
            color:#fff;
        }
        
        .custom-arrow-up:before{
                content: "\e113";
        }
        
        .custom-arrow-down:before {
                content: "\e114";
        }
        
        .ui-accordion .ui-accordion-header{
            background-color: #6d6b6c;
        }
        
        .ui-accordion .ui-accordion-header.ui-accordion-header-active{
            background-color: #5ec034;
        }
   
        .ui-accordion .ui-accordion-content {
            padding: 0px 10px 0px 10px;   
            overflow: hidden;
        }
        
        /*body {
	        background: #dadada;
	        font-family: 'Quicksand', sans-serif;
        }*/

        .pax_grid {
	        border-bottom: solid 1px #ccc;
	        padding-bottom: 10px;
	        padding-top: 10px; width:100%;
        }


        .pax_grid:last-child {
          border-bottom: solid 1px #fff;
        }


        .pax_grid .pax_name {
	        font-size: 12px;
	        text-transform: uppercase;
	        font-weight: 700
        }

        .pax_grid .selected_seat,
        .pax_grid .No_selected_seat {
	        font-size: 11px;
	        display: inline-block;
	        width: 42px;
	        height: 26px;
	        line-height: 26px;
	        text-align: center;
	        border-radius: 4px;
        }

        .light_wrapper .selected,
        .light_wrapper .available,
        .light_wrapper .occupied {
	        display: inline-block;
	        width: 20px;
	        height: 20px;
	        text-align: center;
	        border-radius: 4px;
	        padding-left: 20px;
	        line-height: 20px;
	        font-size: 12px;
        }

        .light_wrapper .selected {
	        background-color: #e6cac4;
        }

        .light_wrapper .available {
	        background-color: #5ec034;
        }

        .light_wrapper .occupied {
	        background-color: #6d6b6c;
        }

        .light_wrapper {
	        padding: 10px;
	        background-color: #fff;
	        border: solid 1px #ccc;
        }

        .pax_grid .selected_seat,
        .pax_grid .No_selected_seat {
	        border: solid 1px #dadada;
        }

        .pax_grid .No_selected_seat {
	        background-color: #fff;
	
	
        }

        .pax_grid .selected_seat {
	        background-color: #f6f6f6;
	        position:absolute; top:-4px;
        }

        .pax_grid .currency {
	        color: Gray;
	        font-size: 11px;
        }


        .pax_grid .price { font-size:12px; font-weight:700 }


        .scroll_skeleton {
	        /*max-width: 270px;*/
	        max-height: 530px;
	        text-align: center;
	        margin-top: 20px;
	        margin-left: auto;
	        margin-right: auto;
	        overflow-y: scroll;
	        overflow-x: hidden;
	        scrollbar-color: #c0c0c4 #f0f0f0;
            scrollbar-width: thin;
    
        }

        .term_conditions {
	        font-size: 11px; margin-top:14px;
        }

        .term_conditions a {
	        color: Red
        }

        .btn_reset {
	        border: solid 2px #666666;
	        margin-top: 20px;
        }

        @media(max-width:992px) {
	        .wrapper {
		        width: 100%;
	        }
        }

        .selected_seat, .No_selected_seat   {
        font-size:11px; display:inline-block; 
         width:26px; height:26px; line-height:26px; 
         text-align:center; border-radius:4px; 
         }	
        .selected_seat, .No_selected_seat {   border:solid 1px #dadada;  }

        @media (max-width: 767px) {
            .pax_grid .price {
                font-size: 12px;
            }

            .pax_grid .pax_name {
                font-size: 12px;
            }
        }

        .baggage-wrapper /* Added this style for displaying baggage onward and return */
        {
            /*width: 100%;*/
        }
        .subgray-header {
            height: auto;
            line-height: 20px;
            padding: 5px 10px 5px 10px;
            margin-bottom: 15px;
        }	                            


		.custom-checkbox-style input[type=checkbox]+label:before {
			line-height: 1.2;
		}

	   .chkbox-addpasngr{
			position:absolute; 
			right:10px; 
			top:-20px; 
			display:inline-block;
	   }
		.passenger-label{
			line-height: 2;
		}
        .hidecntrldiv { display:none }

        .form-control::-webkit-input-placeholder { color: lightgray; }
        .form-control::-moz-placeholder { color: lightgray; }
        .form-control:-ms-input-placeholder { color: lightgray; }

        .disabled { color: #ccc; pointer-events:none; }
    </style>

    <script src="scripts/jquery-ui.js"></script>
    <script src="Scripts/jsBE/Utils.js"></script>
    <script src="Scripts/Common/common.js"></script>
    <script src="scripts/accordian.js"></script>
    <script src="scripts/seat-charts.js"></script>
    <script src="scripts/Common/FlexFields.js"></script>
    <%--<script type="text/javascript">

        function IsAlphaNum(e) {

            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            return ((keyCode >= 46 && keyCode <= 58) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122)
                || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
        }

        function IsAlpha(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            return ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122)
                || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
        }

        function IsNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            return ((keyCode >= 46 && keyCode <= 58) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
        }

        /* To call ajax web methods */
        function AjaxCall(Ajaxurl, Inputdata) {

            var obj = '';
        
            $.ajax({
                type: "POST",
                url: Ajaxurl,
                contentType: "application/json; charset=utf-8",
                data: Inputdata,// "{'sOrigin':'" + origin + "','sDestination':'" + destination + "','sPaxnames':'" + PaxNames.toString() + "'}",
                dataType: "json",
                async: false,
                success: function (data) {                    
                    obj = (data == null || data.d == null || data.d == 'undefined' || data.d == '') ? '' : data.d;
                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                }
            });

            return obj;
        }

        /* To bind flex field drop down list dynamically based on dependent flex field selected value */
        function BindFlexdynamicddl(selval, ddlid, ddlquery, cntrl) {

            try {

                ddlquery = (ddlquery == null || ddlquery == 'undefined' || ddlquery == '') ? '' : ddlquery.replace(/@value/g, selval);
                if (cntrl == 'ddl' && ddlquery != '')
                    document.getElementById(ddlid).innerHTML = GetFlexddlData(ddlquery, cntrl);
                else
                    document.getElementById(ddlid).value = ddlquery != '' ? GetFlexddlData(ddlquery, cntrl) : selval;
                document.getElementById(ddlid.replace(cntrl, 'div')).style.display = (selval != '' && selval != '-1') ? 'block' : 'none';
                Setddlval(ddlid, '-1', '');
            }
            catch (exception) {
                var exp = exception;
            }
        }

        /* To get data for flex field drop down list based on flex query */
        function GetFlexddlData(ddlquery, cntrl) {

            try {

                ddlquery = ddlquery.replace(/'/g, '^');        
                var ddldata = (ddlquery == '' || ddlquery == 'undefined' || ddlquery == null) ? '' :
                    JSON.parse(AjaxCall('BookOffline.aspx/GetFlexddlData', "{'sddlQuery':'" + ddlquery + "'}"));
                if (ddldata != '') {

                    var col = [];

                    for (var key in ddldata[0]) {
                        col.push(key);
                    }

                    var ddlhtml = '';

                    if (cntrl == 'ddl') {
                        
                        for (var d = 0; d < ddldata.length; d++) {

                            ddlhtml += '<option value="' + ddldata[d][col[0]] + '">' + ddldata[d][col[1]] + '</option>';
                        }
                    }
                    return cntrl == 'txt' ? ddldata[0][col[0]] : '<option selected="selected" value="">--Select--</option>' + ddlhtml;
                }
                else
                    return cntrl == 'txt' ? '' : '<option selected="selected" value="">--Select--</option>';
            }
            catch (exception) {
                return cntrl == 'txt' ? '' : '<option selected="selected" value="">--Select--</option>';
            }
        }

        function BindFlexFields1() {

            try {
                
                FlexInfo = JSON.parse(document.getElementById('<%=hdnFlexInfo.ClientID %>').value);
                Flexconfigs = document.getElementById('<%=hdnFlexCount.ClientID %>').value == '' ? '' :
                    document.getElementById('<%=hdnFlexCount.ClientID %>').value.split('|');

                for (var p = 0; p < FPaxcount; p++) {

                    GetFlexHTML('divFlexFields' + p, p);
                    document.getElementById('divFlex' + p).style.display = document.getElementById('divFlexFields' + p).style.display = 'block';                
                }  
                    
                $('select').select2();
            }
            catch (exception) {
                var exp = exception;
            }
        }

        /* To Bind Flex fields */
        function GetFlexHTML(divid, paxid) {

            
            if (FlexInfo != null) {

                var flexids = [];
                var currentyear = new Date().getFullYear();                
                                
                $('#' + divid).append('<input type="hidden" id="hdn' + paxid + 'Flex">');
                var FieldCnt = 0;
                $.each(FlexInfo, function (key, col) {

                    flexids.push('|' + col.flexId + col.flexControl + '|');
                    if (col.FlexProductId != null && col.FlexProductId != 'undefined' && col.FlexProductId != '' && col.FlexProductId != '1')
                        return;

                    if (Flexconfigs.length > 0) {

                        var IsBind = false; IslableFound = false;
                        for (var r = 0; r < Flexconfigs.length; r++) {

                            if (!IslableFound)
                                IslableFound =  col.flexLabel.toUpperCase() == Flexconfigs[r].split(',')[1].toUpperCase();
                            if (CorpTravelReason == Flexconfigs[r].split(',')[0] && col.flexLabel.toUpperCase() == Flexconfigs[r].split(',')[1].toUpperCase())
                                IsBind = true;
                        }
                        if (IslableFound && !IsBind) { return; }
                    }
                    
                    var dataevnt = col.flexDataType == 'T' ? '' :
                        col.flexDataType == 'N' ? 'return IsNumeric(event)' : 'return IsAlphaNum(event)';
                    var cntrlhtml = '';
                    if (col.flexControl == 'T')
                        cntrlhtml = AddTextbox(col.flexLabel, paxid + 'Flex' + key, (paxid == 0 ? col.Detail_FlexData : ''), (col.flexMandatoryStatus == 'Y'), dataevnt, '50', '', col.flexDisable);
                    if (col.flexControl == 'D')
                        cntrlhtml = AddCalenderddl(col.flexLabel, paxid + 'Flex' + key, '', (col.flexMandatoryStatus == 'Y'), (currentyear-50), (currentyear+50), col.flexDisable);
                    if (col.flexControl == 'L')
                        cntrlhtml = AddFlexddl(col.flexLabel, paxid + 'Flex' + key, '', (col.flexMandatoryStatus == 'Y'), (col.flexSqlQuery.indexOf('@value') != '-1' ? '' : col.flexSqlQuery), paxid, col.flexDisable);

                    var isDependent = false; var dcntid = '';

                    if (col.FLEXVALID != '' && (flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 || flexids.indexOf('|' + col.FLEXVALID + 'L' + '|') != -1)) {

                        dcntid = flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 ? 'txt' + paxid + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') :
                            'ddl' + paxid + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'L' + '|');

                        if (document.getElementById(dcntid) == null)
                            return;

                        var curflex = col.flexControl == 'L' ? 'ddl' : 'txt';
                        var ocflex = flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 ? 'txt' : 'ddl';

                        isDependent = document.getElementById(dcntid).value == '' || document.getElementById(dcntid).value == '-1';             
                        
                        $('#' + dcntid).on('change', function (e) { BindFlexdynamicddl((ocflex == 'txt' ? e.target.value : e.val), curflex + paxid + 'Flex' + key, col.flexSqlQuery, curflex); });
                    }

                    isDependent = paxid == 0 ? (isDependent && (col.Detail_FlexData == null || col.Detail_FlexData == 'undefined' || col.Detail_FlexData == '')) : isDependent;
                    
                    cntrlhtml = '<div ' + (isDependent ? 'style="display:none"' : '') + ' class="col-md-4 mb-4 ' + (col.flexDisable ? 'disabled' : '') + '" '
                        + 'id="div' + paxid + 'Flex' + key + '">' + cntrlhtml + '</div>';

                    if (FieldCnt == 10) {                            

                        $('#' + divid).append('<div class="collapse" id="' + divid + 'MoreFlex"><div id="' + divid + 'More" class="row padding-0 marbot_10 ">' +
                            cntrlhtml + '</div>');
                        $('#' + divid).append(
                            '<a style="color: rgb(0, 0, 0); margin-left: 0px; padding: 0px;" id="anc' + divid + '" ' +
                            'aria-expanded="false" aria-controls="collapseExample" class="advPax-collapse-btn-new float-left"' +
                            'role="button" data-toggle="collapse" href="#' + divid + 'MoreFlex"></a>');
                    }
                    else if (FieldCnt > 10)
                        $('#' + divid + 'More').append(cntrlhtml);
                    else
                        $('#' + divid).append(cntrlhtml);      

                    if (paxid == 0 && col.flexControl != 'T' && col.Detail_FlexData != null && col.Detail_FlexData != 'undefined' && col.Detail_FlexData != '') {

                        if (dcntid != '') {

                            BindFlexdynamicddl(document.getElementById(dcntid).value, 'ddl' + paxid + 'Flex' + key, col.flexSqlQuery, 'ddl');
                        }
                        if (col.flexControl == 'L' && document.getElementById('ddl' + paxid + 'Flex' + key) != null)
                            Setdropval('ddl' + paxid + 'Flex' + key, col.Detail_FlexData, '');
                        if (col.flexControl == 'D' && document.getElementById('ddldt' + paxid + 'Flex' + key) != null) {

                            document.getElementById('ddldt' + paxid + 'Flex' + key).value = col.Detail_FlexData.split('/')[0];
                            document.getElementById('ddlmn' + paxid + 'Flex' + key).value = col.Detail_FlexData.split('/')[1];
                            document.getElementById('ddlyr' + paxid + 'Flex' + key).value = col.Detail_FlexData.split('/')[2];
                        }
                    }
                    FieldCnt++;                
                });                            
            }
        }

        /* To set normal drop down value based on display text/stored value */
        function Setdropval(id, val, type) {

            if (type == 'text')
                document.getElementById(id).value = $("#" + id + " option").filter(function () { return this.text == val.trim(); }).val();
            else
                document.getElementById(id).value = val.trim();

            if (document.getElementById(id).value == '')
                document.getElementById(id).value = $("#" + id + " option:first").val();
        }

        /* To add mandatory aestrick span */
        function AddMandatory(isreq) {
            return isreq ? '<span class="red_span">*</span>' : '';
        }

        /* To add dynamic text box field */
        function AddTextbox(lable, id, val, Isreq, onkeypress, maxlen, onchange, IsDisable) {        

            return AddMandatory(Isreq) + '<label><strong>' + lable + ': </strong></label> <input id="txt' + id + '" type="text" onkeypress="' + onkeypress +
                '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control" maxlength="' + maxlen + '" ' +
                (onchange != null && onchange != 'undefined' && onchange != '' ? 'onchange="' + onchange + '" ' : '') +
                (val != null && val != 'undefined' && val != '' ? 'value="' + val + '" ' : '') + '>';
        }

        /* To add calender drop downs */
        function AddCalenderddl(lable, id, val, Isreq, startyear, endyear, IsDisable) {        
            return AddMandatory(Isreq) + '<label><strong>' + lable + ': </strong></label> <div style="margin-left: -15px;">' + AddDateddl(id, val, IsDisable) +
                AddMonthddl(id, val, IsDisable) + AddYearddl(id, val, startyear, endyear, IsDisable) + '</div>';
        }

        /* To add calender date drop down */
        function AddDateddl(id, val, IsDisable) {     

            var dates = '';

            for (var d = 1; d <= 31; d++) {

                var dtval = d < 10 ? '0' + d : d;
                dates += '<option value="' + dtval + '">' + dtval + '</option>';
            }
            return '<div class="col-md-4"> <select id="ddldt' + id + '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control">' +
                '<option selected="selected" value="">Day</option>' + dates + '</select> </div>';
        }

        /* To add calender month drop down */
        function AddMonthddl(id, val, IsDisable) {        

            return '<div class="col-md-4"> <select id="ddlmn' + id + '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control">' +
                '<option selected="selected" value="">Month</option> <option value="01">Jan</option>' +
                '<option value="02">Feb</option> <option value="03">Mar</option> <option value="04">Apr</option>' +
                '<option value="05">May</option> <option value="06">Jun</option> <option value="07">Jul</option>' +
                '<option value="08">Aug</option> <option value="09">Sep</option> <option value="10">Oct</option>' +
                '<option value="11">Nov</option> <option value="12">Dec</option> </select> </div>' ;
        }

        /* To add calender year drop down */
        function AddYearddl(id, val, start, end, IsDisable) {     
                
            var years = '';
            for (var y = start; y <= end; y++) {
                years += '<option value="' + y + '">' + y + '</option>';
            }
            return '<div class="col-md-4"> <select id="ddlyr' + id + '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control">' +
                '<option selected="selected" value="">Year</option>' + years + '</select> </div>';
        }

        /* To Bind Flex field drop down list based on flex query */
        function AddFlexddl(lable, id, val, Isreq, ddlquery, paxid, IsDisable) {
            var df = id.substring(1, id.length);
            return AddMandatory(Isreq) + '<label><strong>' + lable + ': </strong></label> <select id="ddl' + id + '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control">' +
                (ddlquery == '' ? '<option selected="selected" value="">--Select--</option>' : (paxid == 0 ? GetFlexddlData(ddlquery, 'ddl')
                : document.getElementById('ddl0' + id.substring(1, id.length)).innerHTML)) +
                '</select>';
        }

        var FlexInfo, FlexHTML, FlexHTMLMore, FlexConfigs = '';
        var PaxFlexHTML = []; var FlexTextBox = [];        
        var FPaxcount = '<%=paxCount %>';
        var CorpTravelReason = '<%=request.CorporateTravelReasonId %>';
        var focusoncntrl, SaveFlexHTML = true;        

        /* To validate flex information for both flight and hotel */
        function ValidateFlex1() {

            focusoncntrl = document.getElementById('<%=hdnFlexInfo.ClientID %>').value != '' && !resetLCCMealFlag;

            if (!focusoncntrl) { return true; }                

            DelErrorClass();

            try {
                
                for (var p = 0; p < FPaxcount; p++) {

                    var paxnum = Math.ceil(p) + 1; var isValid = true;
                    $.each(FlexInfo, function (key, col) {

                        var isCheck = true;
                        if (col.flexMandatoryStatus == 'Y' && document.getElementById('div' + p + 'Flex' + key) != null && 
                            document.getElementById('div' + p + 'Flex' + key).style.display != 'none') {
                                
                            if (col.flexControl == 'T')
                                isCheck = validmandatory('txt' + p + 'Flex' + key, 'textbox', '', 'Please enter passenger-' + paxnum + ' ' + col.flexLabel, false);
                            else if (col.flexControl == 'L' && $('#ddl' + p + 'Flex' + key).children('option').length > 1)
                                isCheck = validmandatory('ddl' + p + 'Flex' + key, 'dropdown', '--Select--', 'Please select passenger-' + paxnum + ' ' + col.flexLabel, false);
                            else if (col.flexControl == 'D' && document.getElementById('ddldt' + p + 'Flex' + key) != null) 
                                isCheck = validdateddl(p + 'Flex' + key, true, 'Please select passenger-' + paxnum + ' ' + col.flexLabel);
                        }                
                        else if (col.flexControl == 'D' && document.getElementById('ddldt' + p + 'Flex' + key) != null)
                            isCheck = validdateddl(p + 'Flex' + key, false, 'Please select passenger-' + paxnum + ' ' + col.flexLabel);

                        isValid = isValid ? isCheck : isValid;
                        if (!isCheck && key > 9 && document.getElementById('ancdivFlexFields' + p) != null) { ExpandAddAPI(p, 'ancdivFlexFields'); }
                    }); 

                    var pid = p == 0 ? '' : Math.ceil(p) - 1;
                    if (!isValid) { ExpandAddAPI(pid, 'ancAPI'); }
                }

                if (focusoncntrl && SaveFlexHTML)
                    SaveFlexInfo();
                return focusoncntrl;
            }
            catch (exception) {
                return true;
            }
        }

        /* To validate date related drop downs */
        function validdateddl(dtField, ismand, message) {

            if (!ismand)
                ismand = (document.getElementById('ddldt' + dtField).value != '' || document.getElementById('ddlmn' + dtField).value != '' ||
                    document.getElementById('ddlyr' + dtField).value != '');

            if (ismand) {

                var isCheck = true;
                isCheck = validmandatory('ddldt' + dtField, 'dropdown', 'Day', message + ' Date', false);
                ismand = ismand ? isCheck : ismand;
                isCheck = validmandatory('ddlmn' + dtField, 'dropdown', 'Month', message + ' Month', false);
                ismand = ismand ? isCheck : ismand;
                isCheck = validmandatory('ddlyr' + dtField, 'dropdown', 'Year', message + ' Year', false);
                ismand = ismand ? isCheck : ismand;
            }
            else
                ismand = true;
            return ismand;
        }

        /* To delete error class for all controls */
        function DelErrorClass() {

            var errmsgs = $('.form-text-error');
            if (errmsgs != null && errmsgs.length > 0) {

                $.each(errmsgs, function (key, cntrl) {

                    $('#' + cntrl.id).removeClass('form-text-error');
                });
            }
        }

        /* Common function to validate text box and ddl controls */
        function validmandatory(cntrlid, cntrltype, defaultval, errmsg, issrvrcntrl) {

            var valid = true;

            cntrlid = (issrvrcntrl && cntrlid.indexOf('ctl00_cphTransaction_') < 0) ? 'ctl00_cphTransaction_' + cntrlid : cntrlid;
            //cntrltype = issrvrcntrl ? cntrltype : 'textbox'; 

            if (document.getElementById(cntrlid) == null)
                return true;

            if (cntrltype == 'dropdown' && document.getElementById('s2id_' + cntrlid).style.display != 'none') {

                var ddlvalue = document.getElementById('s2id_' + cntrlid).innerText.trim();
                if (ddlvalue == defaultval || ddlvalue == '') {
                    $('#s2id_' + cntrlid).parent().addClass('form-text-error');
                    if (focusoncntrl)
                        document.getElementById('s2id_' + cntrlid).focus();
                    valid = false;
                    if (errmsg != null && errmsg != '')
                        ShowError(errmsg);
                }
                else {
                    $('#s2id_' + cntrlid).parent().removeClass('form-text-error');
                }
            }

            if (cntrltype == 'textbox' && document.getElementById(cntrlid).style.display != 'none') {
                var txtvalue = document.getElementById(cntrlid).value.trim();
                if (txtvalue == '' || txtvalue == defaultval || txtvalue == errmsg) {
                    $('#' + cntrlid).addClass('form-text-error');$('#' + cntrlid).val('');
                    if (focusoncntrl)
                        document.getElementById(cntrlid).focus();
                    valid = false;
                    if (errmsg != null && errmsg != '')
                        ShowError(errmsg);
                }
                else {
                    $('#' + cntrlid).removeClass('form-text-error');
                }
            }
            focusoncntrl = focusoncntrl ? valid : focusoncntrl;
        
            return valid;
        }

        /* To display error message in JS */
        function ShowError(errmsg) {

            var errmsgs = $('div.toast-message'); var display = true;
            if (errmsgs != null && errmsgs.length > 0) {

                $.each(errmsgs, function (key, msgs) {

                    if (errmsg == msgs.innerText) {
                        display = false;
                        return false;
                    }
                });
            }
            if (display)
                toastr.error(errmsg);
        }

        /* To save flex data */
        function SaveFlexInfo() {

            try {
                
                if (document.getElementById('<%=hdnPaxFlexInfo.ClientID %>').value == '' || resetLCCMealFlag)
                    return true;

                var PaxFlexInfo = JSON.parse(document.getElementById('<%=hdnPaxFlexInfo.ClientID %>').value);
                var flexcnt = 0;

                for (var p = 0; p < FPaxcount; p++) {                    

                    $.each(FlexInfo, function (key, col) {      
                    
                        var flexdat = '';                    
                                                
                        if (col.flexControl == 'T' && document.getElementById('txt' + p + 'Flex' + key) != null)
                            flexdat = document.getElementById('txt' + p + 'Flex' + key).value;
                        else if (col.flexControl == 'L' && document.getElementById('ddl' + p + 'Flex' + key) != null &&
                            document.getElementById('ddl' + p + 'Flex' + key).value != '' &&
                            document.getElementById('ddl' + p + 'Flex' + key).value != '-1') {
                            flexdat = document.getElementById('ddl' + p + 'Flex' + key).value;
                        }                        
                        else if (col.flexControl == 'D' && document.getElementById('ddldt' + p + 'Flex' + key) != null)
                        {
                            flexdat = document.getElementById('ddldt' + p + 'Flex' + key).value + '/' +
                                document.getElementById('ddlmn' + p + 'Flex' + key).value + '/' + document.getElementById('ddlyr' + p + 'Flex' + key).value;
                            flexdat = flexdat.indexOf('//') != '-1' ? '' : flexdat;
                        }                    
                                        
                        PaxFlexInfo[flexcnt].FlexData = IsEmpty(flexdat) ? '' : flexdat;
                        PaxFlexInfo[flexcnt].FlexId = col.flexId;
                        PaxFlexInfo[flexcnt].FlexLabel = col.flexLabel;
                        PaxFlexInfo[flexcnt].FlexGDSprefix = col.flexGDSprefix;
                        PaxFlexInfo[flexcnt].PaxId = p;
                        PaxFlexInfo[flexcnt].ProductID = 1;

                        if (col.flexControl == 'D') {

                            HoldFlexval('ddldt' + p + 'Flex' + key, col.flexControl);
                            HoldFlexval('ddlmn' + p + 'Flex' + key, col.flexControl);
                            HoldFlexval('ddlyr' + p + 'Flex' + key, col.flexControl);
                        }
                        else
                            HoldFlexval((col.flexControl == 'L' ? 'ddl' : 'txt') + p + 'Flex' + key, col.flexControl);

                        flexcnt++;      
                    }); 
                }                
                
                document.getElementById('<%=hdnPaxFlexInfo.ClientID %>').value = JSON.stringify(PaxFlexInfo);
                GetPaxFlexhtml('G');
            }
            catch (exception) {
                
            }
        }

        /* To set the drop downs selected values and remove other options for post back display */
        function HoldFlexval(cntrlid, cntrl) {

            if (document.getElementById(cntrlid) == null)
                return;

            var selval = document.getElementById(cntrlid).value; 

            if (cntrl != 'T') {                                

                var stext = document.getElementById(cntrlid).selectedOptions[0].text;
                document.getElementById(cntrlid).innerHTML = '';
                document.getElementById(cntrlid).innerHTML = '<option selected="selected" value="' + selval + '">' + stext + '</option>';
                document.getElementById(cntrlid).disabled = true;
                if ($("#s2id_" + cntrlid) != null)
                    $( "#s2id_" + cntrlid).remove();
            }
             
            FlexTextBox.push(cntrlid + '^' + selval + '^' + cntrl);
            $('#' + cntrlid).addClass('disabled');
        }
        
        /* To get and set the Flex HTML on post back */
        function GetPaxFlexhtml(Action) {

            if (Action != 'S') {
                               
                for (var p = 0; p < FPaxcount; p++) {

                    if (Action == 'G')
                        PaxFlexHTML.push(document.getElementById('divFlexFields' + p).innerHTML);
                    else
                        document.getElementById('divFlexFields' + p).innerHTML = PaxFlexHTML[p];
                    document.getElementById('divFlex' + p).style.display = 'block';
                }
            }

            if (Action == 'S') {
                                
                for (var p = 0; p < FlexTextBox.length; p++) {

                    if (FlexTextBox[p].split('^')[2] == 'T')
                        document.getElementById(FlexTextBox[p].split('^')[0]).value = FlexTextBox[p].split('^')[1];
                    else if (document.getElementById("s2id_" + FlexTextBox[p].split('^')[0]) != null)
                        document.getElementById("s2id_" + FlexTextBox[p].split('^')[0]).style.display = 'block';
                }
            }
        }

    </script>--%>

    <script type="text/javascript">        

        function Showseatalert(msg) {

            /* To disable bootstrap modal close on escape button and pop up outside click */

            $("#Seatsalert").modal({
                backdrop: 'static',
                keyboard: false
            });

            CalculateBaggagePrice();

            /* To retain Air arabia GST state code value */
            AirArabiaGSTCntrls();   

            if (document.getElementById('<%=hdnSelStateId.ClientID %>') != null && document.getElementById('<%=ddlGSTStateCode.ClientID %>') != null)
                document.getElementById('<%=ddlGSTStateCode.ClientID %>').value = document.getElementById('<%=hdnSelStateId.ClientID %>').value;
            
            /* To disable meal value assignment to hidden field in case of seat failure */
            resetLCCMealFlag = true;

            /* To retain flex fields HTML on post back */
            if (!IsEmpty(document.getElementById('<%=hdnFlexInfo.ClientID %>').value))
                GetPaxFlexhtml('H');
            SaveFlexHTML = false;

            document.getElementById('divSeatsalertText').innerHTML = msg.replace(/@/g, '</br>');
            $('#Seatsalert').modal('show');
            $('#divSelSeats').removeClass('hidecntrldiv');
        }

        function Cancel(id) {
           $('#'+id).modal('hide');
           window.location.href = 'HotelSearch.aspx?source=Flight';
        }
        
        function Continue(id) {

            /* To set flex fields value on post back */
            if (!IsEmpty(document.getElementById('<%=hdnFlexInfo.ClientID %>').value))
                GetPaxFlexhtml('S');

            var seatalertbtn = document.getElementById('SeatsalertContinue');
            $(seatalertbtn).prop("disabled", true);

            /* To expand ADD API's box for both lead and additional passengers */
            var ancAPI = document.getElementById('ancAPI');
            if (ancAPI.attributes["aria-expanded"].value == 'false')
                ancAPI.click();
            for (var paxind = 0; paxind < PaxNames.length; paxind++) {
                ancAPI = null;
                ancAPI = document.getElementById('ancAPI' + paxind);
                if (ancAPI != null && ancAPI.attributes["aria-expanded"].value == 'false')
                   ancAPI.click();
            }

            $('#' + id).modal('hide');
            showpopup('Failed');

            $(seatalertbtn).prop("disabled", false);
            return false;
        }

        function Saveseatinfo() {

            var counter = 0;
            for (var i = 1; i <= SeatData.SegmentSeat.length; i++) {

                var id = document.getElementById('Seg' + i.toString());
                var segname = id.title;
                for (var c = 1; c <= PaxNames.length; c++) {
                    var seatrow = document.getElementById('Seg' + i.toString() + 'paxrow' + c.toString());
                    var seatid = document.getElementById('Seg' + i.toString() + 'pax' + c.toString() + 'seatno');
                    var priceid = document.getElementById('Seg' + i.toString() + 'pax' + c.toString() + 'price');
                    paxseatdtls[counter].Segment = segname;
                    paxseatdtls[counter].PaxNo = c - 1;
                    paxseatdtls[counter].SeatNo = seatid.innerText;
                    //paxseatdtls[counter].SeatNo = seatid.innerText != '' && c < 3 && i == 2 && SeatsarrayMain[c] != null ? SeatsarrayMain[c] : seatid.innerText;
                    paxseatdtls[counter].Price = seatid.innerText == '' ? '0' : priceid.innerText.substring(priceid.innerText.indexOf(" ") + 1, priceid.innerText.length);
                    if (seatrow.classList.contains('disabled'))
                        paxseatdtls[counter].SeatStatus = 'S'
                    else
                        paxseatdtls[counter].SeatStatus = paxseatdtls[counter].SeatNo == '' ? '' : 'A';
                    counter++;
                }
            }
            $('#<% =txtPaxSeatInfo.ClientID %>').val(JSON.stringify(paxseatdtls)); 
            $('#myModal').modal('hide');
            return false;
        }

        function showpopup(source) {

            SaveFlexHTML = false;
            var seatbtn = document.getElementById('btnSeatselect');
            $(seatbtn).prop("disabled", true);
            $('.modal').css('overflow-y', 'auto');
            <%if (result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia) //Should not validate G9 seat in validate() in Selet Seat button click
        {%>
            document.getElementById('<%=hdnValidateG9Seat.ClientID%>').value = 'NO';
            <%}%>
            if (source == 'Failed')
                BindSegMents(SeatData);
            else {
                if (!validate()) {
                   <%if (result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia) //Should validate G9 Seat continue button click , so making as YES
                   {%>
                    document.getElementById('<%=hdnValidateG9Seat.ClientID%>').value = 'YES';
                  <%}%>
                    $(seatbtn).prop("disabled", false);
                    return false;
                }
            }
            

            if (firstflag) {
                agentdecimal = eval('<%=agency.DecimalValue %>');
                sCurrency = '<%=agency.AgentCurrency %>';
                bHasInfant = '<%=request.InfantCount > 0 %>';
                var paxseatobj = document.getElementById('<%=hdnPaxSeatInfo.ClientID %>').value;
                paxseatdtls = JSON.parse(paxseatobj);
                GetSeats('', '');
            }
            if (!firstflag)
                $('#myModal').modal('show');
            <%if (result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia) //Should validate G9 Seat continue button click , so making as YES
            {%>
            document.getElementById('<%=hdnValidateG9Seat.ClientID%>').value = 'YES';
            <%}%>
            $(seatbtn).prop("disabled", false);
            SaveFlexHTML = true;
        }

        function ResetSeatMap() {

            var seatbtn = document.getElementById('btnSeatreset');
            $(seatbtn).addClass("disabled");

            paxseatinfo = document.getElementById('<%=txtPaxSeatInfo.ClientID %>').value;
            
            if (paxseatinfo != ''){

                paxseatdtls = JSON.parse(paxseatinfo);
                for (var c = 0; c < paxseatdtls.length; c++)
                {
                    if (paxseatdtls[c].SeatStatus != 'S') {                        
                        paxseatdtls[c].SeatNo = '';
                        paxseatdtls[c].Price = '0';
                        paxseatdtls[c].SeatStatus = '';
                    }
                }
                $('#<% =txtPaxSeatInfo.ClientID %>').val(JSON.stringify(paxseatdtls)); 
            }            
            
            resetflag = firstflag = true;
            GetSeats('', '');

            $(seatbtn).removeClass("disabled");

        }

        function BindSegMents(segmentdata) {

            var paxseatobj = document.getElementById('<%=txtPaxSeatInfo.ClientID %>').value;
            if (paxseatobj)
                paxseatdtls = JSON.parse(paxseatobj);
            Selseats = [];
            sSuccessSegments = '';
            var tmpseg = ''; var activesegid = '';
            $('#Segments').children().remove();
            $("#Segments").append('<div id="accordion">');
            
            $.each(segmentdata.SegmentSeat, function (key, value) {

                var TotalPrice = 0;                
                Selseats[key] = '';
                var paxseatassignedcnt = 0; 
                sFailureSegments = '';
                $("#accordion").append('<h3 id="' + value.SegmntID + '" title="' + value.Origin + '-' + value.Destination + '">' +
                    value.Origin + ' - ' + value.Destination + '</h3>');
                $("#accordion").append('<div id= "' + value.SegmntID + 'paxdtls"></div>');
                $('#' + value.SegmntID + 'paxdtls').append(
                    '<div id="divpaxinfo"><table width="100%" id= "' + value.SegmntID + 'tblpaxdtls">' +
                    //'<tr> <td>Passenger Name</td> <td> Price </td> <td> SeatNo </td> </tr>' +
                    '</table ></div>');

                if (PaxNames.length > 0) {

                    for (var i = 1; i <= PaxNames.length; i++) {

                        var seatno = '', sprice = '', seatstatus = '';

                        if (paxseatobj) {

                            $.each(paxseatdtls, function (cnt, seat) {

                                if (!(resetflag == true && seat.SeatStatus == 'A')) {

                                    if (seat.PaxNo == i - 1 && seat.Segment == value.Origin + '-' + value.Destination && seat.SeatNo != '' && seat.SeatNo != 'NoSeat') {
                                        seatno = seat.SeatNo; sprice = seat.Price; TotalPrice += eval(seat.Price);
                                        Selseats[key] = Selseats[key] != null && Selseats[key] != 'undefined' ? Selseats[key] + seatno + '-' + seat.SeatStatus + '|' : seatno + '-' + seat.SeatStatus + '|';
                                    }
                                    seatstatus = seat.PaxNo == i - 1 && seat.Segment == value.Origin + '-' + value.Destination ? seat.SeatStatus : seatstatus;
                                }
                            });

                        }

                        tmpseg = seatstatus == 'F' && tmpseg == '' ? value.SegmntID : tmpseg;
                        activesegid = seatstatus == 'F' && activesegid == '' ? key : activesegid;
                        var seatprice = seatno != '' ? sCurrency + ' ' + sprice : '';
                        var rowclass = seatstatus == 'S' ? 'row disabled' : 'row';
                        paxseatassignedcnt = seatstatus == 'S' ? paxseatassignedcnt + 1 : paxseatassignedcnt;

                        var pname = PaxNames[i - 1];//.slice(0, -2);
                        pname = pname.replace(/-/g, ' ');
                        $('#' + value.SegmntID + 'tblpaxdtls').append(
                            '<div class="pax_grid"><div class="'+ rowclass +'" id="' + value.SegmntID + 'paxrow' + i.toString() + '" >' +
                            '<div class="col-md-7 col-xs-7"><span  id="' + value.SegmntID + 'pax' + i.toString() + 'name" class="pax_name">' + pname + '</span></div>' +
                            '<div class="col-md-2 col-xs-2"><span id="' + value.SegmntID + 'pax' + i.toString() + 'seatno" class="selected_seat cancel-cart-item">' + seatno + '</span ></div >' +                        
                            '<div class="col-md-3 col-xs-3"><span class="price getprice" id="' + value.SegmntID + 'pax' + i.toString() +   'price">'+ seatprice +'</span></div>'+
                        '</div></div>'
                        );
                    }

                    var TotPrice = Selseats[key] != null && Selseats[key] != '' ? sCurrency + ' ' + parseFloat(eval(TotalPrice)).toFixed(agentdecimal) : '';

                    $('#' + value.SegmntID + 'tblpaxdtls').append(
                        '<div class="pax_grid"><div class="row">'+
                        '<div class="col-sm-6"><span class="pax_name">Total Seat Fare:</span></div>' + 
                        //'<div class="col-md-2 col-xs-2"><span id="' + value.SegmntID + 'pax' + i.toString() + 'seatno"class="selected_seat cancel-cart-item"></span></div>' +                        
                        '<div class="col-sm-6"><span class="price" id="' + value.SegmntID + '_Total">' + TotPrice + '</span></div>'+
                        '</div></div>'
                    );
                }


                if (paxseatassignedcnt == PaxNames.length)
                    sSuccessSegments = sSuccessSegments == '' ? value.SegmntID : sSuccessSegments + '|' + value.SegmntID;
            });

            var asgid = tmpseg == '' ? 0 : eval(tmpseg.substring(4, 3)) - 1;

            $(function () {

                var icons = {
                    header: "custom-arrow-down",
                    activeHeader: "custom-arrow-up"
                };

                $("#accordion").accordion({
                    icons: icons,
                    active:asgid,
                    heightStyle: "content"
                });

                $("#toggle").button().on("click", function () {
                    if ($("#accordion").accordion("option", "icons")) {
                        $("#accordion").accordion("option", "icons", null);
                    } else {
                        $("#accordion").accordion("option", "icons", icons);
                    }
                });
            });
            resetflag = false;
            tmpseg = tmpseg == '' ? document.getElementById('Seg1') : document.getElementById(tmpseg);
            LoadSegmentSeats(tmpseg);
        }

        function Getemptyseats(no) {
            var emptyseat = '';
            for (var i = 0; i < no; i++) {
                emptyseat += '|_';
            }
            return emptyseat;
        }

        /* Added .1 sec timer to show processing div on segment seats loading */
        function LoadSegmentSeats(event) {
            document.getElementById('ctl00_upProgress').style.display = 'block';
            setTimeout(function(){ LoadSegmentSeat(event); document.getElementById('ctl00_upProgress').style.display ='none'; }, 100);            
        }

        function LoadSegmentSeat(event) {

            $('#seatslayout').children().remove();
            //$("#seatslayout").append('<div class="container"></div>');
            //$(".container").append('<h1>Please select seats</h1>');
            $("#seatslayout").append('<div id="seat-map"></div>');
            $("#seat-map").append('<div class="front-indicator">Front</div>');

            var header = document.getElementById(event.id);
            currsegid = event.id;
            var segid = event.id.substring(4, 3);
            Seatsarray = []; SeatsarrayMain = [];
            var currentsegment = '';
            var columnlength = 0, rowno = 0, prevrow = 1, prevcolumnlength = 0; coldiff = 0;
            var seatrows = ''; seatype = ''; prevseattype = ''; lastselseatid = '';

            if (firstflag || sSuccessSegments.indexOf(currsegid) != -1 || sFailureSegments.indexOf(currsegid) != -1) {
                $.each(SeatData.SegmentSeat, function (key, value) {
                    if (value.Origin + '-' + value.Destination == header.title) {
                        currentsegment = value;
                    }
                });
            }

            $('#seatslayout').removeClass('disabled');
            if (sSuccessSegments != '' && sSuccessSegments.indexOf(currsegid) != -1)
                $('#seatslayout').addClass('disabled');

            if (firstflag && sFailureSegments == '')
                sFailureSegments = currsegid;

            if (currentsegment == '' || currentsegment.SeatInfoDetails == null) {

                var journy = header.title.split('-');
                GetSeats(journy[0], journy[1]);
                sFailureSegments = sFailureSegments == '' ? currsegid : sFailureSegments + '|' + currsegid;
                $.each(SeatData.SegmentSeat, function (key, value) {
                    if (value.Origin + '-' + value.Destination == header.title) {
                        currentsegment = value;
                    }
                });
            }

            if (currentsegment == '' || currentsegment.SeatInfoDetails == null)
                return false;

            sExitRows = '|';
            $.each(currentsegment.SeatInfoDetails, function (key, value) {
                var seatind = '';
                if (value.RowNo == prevrow) {
                    columnlength++;
                    seatind = value.SeatNo.substring(value.SeatNo.length, value.SeatNo.length - 1);                    
                    if (prevseattype != null && value.SeatType != null && prevseattype != '' && value.SeatType != '') {
                        seatrows += prevseattype == value.SeatType ? 'N|' : '';
                        columnlength = prevseattype == value.SeatType ? Math.ceil(columnlength) + 1 : columnlength;
                    }
                    seatrows += seatind + '|';
                }
                else {
                    if (columnlength > prevcolumnlength) {
                        Seatsarray = [];
                        prevcolumnlength = columnlength;
                        Seatsarray.push(seatrows.slice(0, -1));
                    }
                    
                    columnlength = 1;
                    seatind = value.SeatNo.substring(value.SeatNo.length, value.SeatNo.length - 1);
                    seatrows = seatind + '|';
                }
                prevseattype = value.SeatType;
                prevrow = value.RowNo;
                if (value.AvailablityType != 'Open')
                    SeatsarrayMain.push(value.SeatNo);

                /* Get exit rows from seat map layout to avoid seat selection for childrens */
                if (sExitRows.indexOf('|' + value.RowNo + '|') == -1) {
                    
                    $.each(value.SeatProperty, function (indx, prop) {

                        if (prop.PropName == 'EXITROW') {

                            sExitRows += value.RowNo + '|';
                            return;
                        }                        
                    });
                }
            });

            var maxcollength = Seatsarray[0];

            var id = currsegid.replace('Seg', '');
            var $total = $('#' + currsegid + '_Total'),
                sc = $('#seat-map').seatCharts({
                    map: Selseats[id - 1] != null && Selseats[id - 1] != 'undefined' ? Selseats[id - 1] : '',
                    naming: {
                        segmntdtls: currentsegment,
                        columns: maxcollength,
                        rows: prevrow - 1,
                        top: false,
                        left: false,
                        getLabel: function (character, row, column) {
                            return firstSeatLabel++;
                        },
                    },
                    legend: {
                        node: $('#legend'),
                        items: [
                            ['f', 'available', 'First Class'],
                            ['e', 'available', 'Economy Class'],
                            ['f', 'unavailable', 'Already Booked']
                        ]
                    },
                    click: function () {
                        var segid = this.settings.id.substring(4, 3);
                        if (this.status() == 'available') {
                            
                            if (Selseats[segid - 1] != null && Selseats[segid - 1] != 'undefined' && Selseats[segid - 1] != '' && Selseats[segid - 1].slice(0, -1).split('|').length == PaxNames.length)
                                return 'available';

                            var sSeatError = AssignSeat(this.settings.label, this.settings.Price, segid, 'Add', sCurrency);
                            if (!IsEmpty(sSeatError)) { alert(sSeatError); return 'available'; }

                            var totalprice = recalculateTotal(sc);
                            $total.text(Math.ceil(totalprice) > 0 ? sCurrency + ' ' + totalprice.toLocaleString() : '');

                            return 'selected';
                        } else if (this.status() == 'selected') {

                            var tmpSelseats = '|'+ Selseats[segid - 1];
                            if (Selseats[segid - 1] != null && Selseats[segid - 1] != 'undefined' && tmpSelseats.indexOf('|' + this.settings.label + '-S|') != -1) {
                                alert('Cannot modify the reserved seat');
                                return 'selected';
                            }

                            var segid = this.settings.id.substring(4, 3);

                            //remove the item from our cart
                            AssignSeat(this.settings.label, this.settings.Price, segid, 'Remove', sCurrency);
                            
                            var totalprice = recalculateTotal(sc);
                            $total.text(Math.ceil(totalprice) > 0 ? sCurrency + ' ' + totalprice.toLocaleString() : '');

                            //seat has been vacated
                            return 'available';
                        } else if (this.status() == 'unavailable') {
                            //seat has been already booked
                            return 'unavailable';
                        } else {
                            return this.style();
                        }
                    }
                });

            //this will handle seat no against passeger click event
            $('#' + currsegid + 'tblpaxdtls').on('click', '.cancel-cart-item', function () {
                lastselseatid = this;
                var seatno = this.innerText;
                if (lastselseatid.innerText != '') {
                    sc.get(currsegid + '_' + seatno).click();
                    var changeseatstatus = new Array();
                    changeseatstatus[0] = currsegid + '_' + seatno;
                    sc.get(changeseatstatus).status('available');
                    var seatid = document.getElementById(currsegid + '_' + seatno);
                    var clss = seatid.className;
                    seatid.setAttribute('class', clss.replace('selected', 'available'));
                }
            });

            //This will change the status of previously selected seats
            var disableseats = Selseats[segid - 1] != null && Selseats[segid - 1] != 'undefined' ? Selseats[segid - 1].slice(0, -1).split('|') : '';
            if (disableseats != '') {
                for (var c = 0; c < disableseats.length; c++) {
                    disableseats[c] = currsegid + '_' + disableseats[c].slice(0, -2);
                }
                sc.get(disableseats).status('selected');
            }
        }

        var firstSeatLabel = 1;
        function recalculateTotal(sc) {
            var total = 0;

            //To find every selected seat and sum its price
            var cersegseats = $('#' + currsegid + 'tblpaxdtls .getprice');
            $.each(cersegseats, function (i, el) {
                var price = el.innerText.substring(el.innerText.indexOf(" ") + 1, el.innerText.length);
                if (price != '' && Math.ceil(price) > 0) {
                    total += parseFloat(eval(price));
                }
            });

            return total;
        }

        var SeatData;
        var paxseatdtls;
        var currsegid;
        var totselseats = 0;
        var Selseats = [];
        var Seatsarray = []; SeatsarrayMain = [];
        var paxcount = 0;
        var lastselseatid = '', paxseatinfo = '';
        var PaxNames = [];
        var firstflag = true, resetflag = false, bHasInfant = false;
        var sCurrency = '';
        var sSuccessSegments = '';
        var agentdecimal = 2;
        var sFailureSegments = '', sExitRows = '|';

        //Based on this flag the user selected meal values will be assigned to the hidden field value.
        //Once all the page validations are successful then we will assign this flag to true.
        var resetLCCMealFlag = false;

        function changeseat(event) {
            lastselseatid = event;
            var deselseatno = event.innerText;
            AssignSeat(event.innerText, event.id.substring(4, 3), 'Remove');
            var seatid = document.getElementById('Seg' + event.id.substring(4, 3) + '_' + deselseatno);
            seatid.setAttribute('class', 'seatCharts-seat seatCharts-cell available');
            seatid.setAttribute('aria-checked', false);
        }

        /* To validate and avoid exit row seat selection for child and infant adults */
        function ValExitRow(selseat, paxnameid) {

            var sMsg = '';
            if (sExitRows.indexOf('|' + selseat.slice(0, -1) + '|') > -1)
                sMsg = document.getElementById(paxnameid).innerText.indexOf('(C)') > -1 ? 'Exit row seats cannot be assigned for child passenger' :
                    bHasInfant == 'True' ? 'Exit row seats cannot be assigned for Infant associated journey' : '';
            return sMsg;
        }

        function AssignSeat(selseat, price, segid, Action, currency) {

            var tmpSelseats = '|'+ Selseats[segid - 1];
            Selseats[segid - 1] = '';
            var seatalloted = false, sExitrowMsg = false;

            if (lastselseatid != '' && Action == 'Add') {

                seatalloted = true;
                sExitrowMsg = ValExitRow(selseat, lastselseatid.id.replace('seatno', 'name'));
                if (IsEmpty(sExitrowMsg)) {
                                        
                    document.getElementById(lastselseatid.id.replace('seatno', 'price')).innerText = currency + ' ' + price;
                    lastselseatid.innerText = selseat; lastselseatid = '';
                }
            }

            for (var c = 1; c <= PaxNames.length; c++) {

                var id = document.getElementById('Seg' + segid + 'pax' + c.toString() + 'seatno');
                var priceid = document.getElementById('Seg' + segid + 'pax' + c.toString() + 'price');

                if ((id.innerText == 'SeatNo' || id.innerText == '') && Action == 'Add' && !seatalloted) {

                    seatalloted = true; 
                    sExitrowMsg = ValExitRow(selseat, ('Seg' + segid + 'pax' + c.toString() + 'name'));
                    if (IsEmpty(sExitrowMsg)) {
                        id.innerText = selseat; priceid.innerText = currency + ' ' + price;
                    }
                }

                if (id.innerText == selseat && Action == 'Remove') {
                    id.innerText = ''; priceid.innerText = '';
                }

                if (id.innerText != 'SeatNo' && id.innerText != '') {
                    var seatno = tmpSelseats.indexOf('|' + id.innerText + '-S|') != -1 ? id.innerText + '-S|' : id.innerText + '-A|';
                    Selseats[segid - 1] = Selseats[segid - 1] != null && Selseats[segid - 1] != 'undefined' ? Selseats[segid - 1] + seatno : seatno ;
                }
            }

            return sExitrowMsg;
        }

        function GetSeats(origin, destination) {

            $.ajax({
                type: "POST",
                url: "PassengerDetails.aspx/GetSeatMap",
                contentType: "application/json; charset=utf-8",
                data: "{'sOrigin':'" + origin + "','sDestination':'" + destination + "','sPaxnames':'" + PaxNames.toString() + "'}",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.d == '') {
                        
                        if (firstflag) {
                            $('#myModal').modal('show');
                            firstflag = false;
                        }
                        
                        $('#seatslayout').children().remove();
                        $("#seatslayout").append('<div id="seat-map"></div>');
                        $("#seat-map").append('<div class="front-indicator"><strong>No Seats Available</strong></div>');                     
                        return;
                    }
                    SeatData = JSON.parse(data.d);
                    if (origin == '' && destination == '' && data.d != '') {
                        BindSegMents(SeatData);
                        firstflag = false;
                    }
                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
            });

            return SeatData;
        }

    </script>

    <script type="text/javascript">
        function validate() {
            var isValid = true;
            var paxdtls = '';
            PaxNames = [];
            var count = '<%=(paxCount - 1) %>';
            
            var depDate = '<%=result.Flights[0][0].DepartureTime.ToString("dd/MM/yyyy")%>';
            var travelDate = depDate.split('/');
            var sysdate = new Date(travelDate[2], eval(travelDate[1]) - 1, travelDate[0]);
            var date = new Date(eval(document.getElementById('<%=ddlYear.ClientID %>').value), eval(document.getElementById('<%=ddlMonth.ClientID %>').value)-1, eval(document.getElementById('<%=ddlDay.ClientID %>').value));
            var expDate = new Date(eval(document.getElementById('<%=ddlPEYear.ClientID %>').value), eval(document.getElementById('<%=ddlPEMonth.ClientID %>').value) - 1, eval(document.getElementById('<%=ddlPEDay.ClientID %>').value));
            var one_day = 1000 * 60 * 60 * 24;
                      
            document.getElementById('title').style.display = 'none';
            document.getElementById('fname').style.display = 'none';
            document.getElementById('lname').style.display = 'none';
            <%if (GenderRequired)
        { %>
            document.getElementById('gender').style.display = 'none';
            <%} %>
            <%if (DOBRequired)
        { %>
            document.getElementById('dobDay').style.display = 'none';
            document.getElementById('dobMonth').style.display = 'none';
            document.getElementById('dobYear').style.display = 'none';
            <%} %>
            <%if (PassportNoRequired)
        { %>
            document.getElementById('passport').style.display = 'none';
            <%} %>
            <%if (PassportExpiryRequired)
        { %>
            document.getElementById('peDay').style.display = 'none';
            document.getElementById('peMonth').style.display = 'none';
            document.getElementById('peYear').style.display = 'none';
            <%} %>
            document.getElementById('mobileCountryCode').style.display = 'none';
            document.getElementById('mobile').style.display = 'none';
            <%if (NationalityRequired || result.ResultBookingSource == CT.BookingEngine.BookingSource.FlightInventory || result.ResultBookingSource == CT.BookingEngine.BookingSource.CozmoBus)
        { %>
            document.getElementById('nationality').style.display = 'none';
            <%} %>
            <%if (CountryRequired)
        { %>
            document.getElementById('country').style.display = 'none';
            <%} %>
            <%if (EmailRequired)
        { %>
            document.getElementById('email').style.display = 'none';
            <%} %>
            <%if (AddressRequired)
        { %>
            document.getElementById('address').style.display = 'none';
            <%} %>

            
            if (document.getElementById('<%=ddlTitle.ClientID %>').value == -1) {
                document.getElementById('title').style.display = 'block';
                document.getElementById('title').innerHTML = "Please Select Title";
                isValid = false;
            }
            if (document.getElementById('<%=txtPaxFName.ClientID %>').value.trim().length <= 0) {
                document.getElementById('fname').style.display = 'block';
                document.getElementById('fname').innerHTML = "Please enter Passenger First Name";
                isValid = false;
            }
            else
                paxdtls = document.getElementById('<%=ddlTitle.ClientID %>').value + ' ';
            if (document.getElementById('<%=txtPaxLName.ClientID %>').value.trim().length <= 0) {
                document.getElementById('lname').style.display = 'block';
                document.getElementById('lname').innerHTML = "Please enter Passenger Last Name";
                isValid = false;
            }
            else
                paxdtls = paxdtls + '-' + document.getElementById('<%=txtPaxFName.ClientID %>').value + ' ';
            if (document.getElementById('<%=txtPaxLName.ClientID %>').value.trim().length < 2) {
                document.getElementById('lname').style.display = 'block';
                document.getElementById('lname').innerHTML = "Passenger Last Name Length should be greater than one";
                isValid = false;
            }
            else
                paxdtls = paxdtls + '-' + document.getElementById('<%=txtPaxLName.ClientID %>').value + '(A)';

            PaxNames.push(paxdtls); paxdtls = '';

            <%if (GenderRequired)
        { %>
            if (document.getElementById('<%=ddlGender.ClientID %>').value == "-1") {
                document.getElementById('gender').style.display = 'block';
                document.getElementById('gender').innerHTML = "Please select Gender";
                isValid = false;
            }
            <%} %>
            <%if (DOBRequired)
        { %>
            if (document.getElementById('<%=ddlDay.ClientID %>').value == "-1") {
                document.getElementById('dobDay').style.display = 'block';
                document.getElementById('dobDay').innerHTML = "Please select Birth Day";
                isValid = false;
            }
            if (document.getElementById('<%=ddlMonth.ClientID %>').value == "-1") {
                document.getElementById('dobMonth').style.display = 'block';
                document.getElementById('dobMonth').innerHTML = "Please select Birth Month";
                isValid = false;
            }
            if (document.getElementById('<%=ddlYear.ClientID %>').value == "-1") {
                document.getElementById('dobYear').style.display = 'block';
                document.getElementById('dobYear').innerHTML = "Please select Birth Year";
                isValid = false;
            }
            if (date >= sysdate) {
                document.getElementById('dobYear').style.display = 'block';
                document.getElementById('dobYear').innerHTML = "Date of Birth should be earlier than Departure Date";
                isValid = false;
            }
            if ((Math.ceil(sysdate.getTime() - date.getTime()) / one_day / 365) <= 12) {
                document.getElementById('dobYear').style.display = 'block';
                document.getElementById('dobYear').innerHTML = "Date of Birth should be greater than 12 years";
                isValid = false;
            }
            <%} %>
            <%if (PassportNoRequired)
        { %>
            if (document.getElementById('<%=txtPassportNo.ClientID %>').value.trim().length <= 0) {
                document.getElementById('passport').style.display = 'block';
                document.getElementById('passport').innerHTML = "Please enter Passport Number";
                isValid = false;
            }
            <%} %>
            <%if (PassportExpiryRequired)
        { %>
            if (document.getElementById('<%=ddlPEDay.ClientID %>').value == "-1") {
                document.getElementById('peDay').style.display = 'block';
                document.getElementById('peDay').innerHTML = "Please select Expiry Day";
                isValid = false;
            }
            if (document.getElementById('<%=ddlPEMonth.ClientID %>').value == "-1") {
                document.getElementById('peMonth').style.display = 'block';
                document.getElementById('peMonth').innerHTML = "Please select Expiry Month";
                isValid = false;
            }
            if (document.getElementById('<%=ddlPEYear.ClientID %>').value == "-1") {
                document.getElementById('peYear').style.display = 'block';
                document.getElementById('peYear').innerHTML = "Please select Expiry Year";
                isValid = false;
            }
            if (expDate <= sysdate) {
                document.getElementById('peYear').style.display = 'block';
                document.getElementById('peYear').innerHTML = "Passport expiry date should be later than departure date";
                isValid = false;
            } else if (Math.ceil((expDate.getTime() - sysdate.getTime()) / one_day) < 30) {
                 document.getElementById('peYear').style.display = 'block';
                document.getElementById('peYear').innerHTML = "Passport should be valid for 1 month from the departure date";
                isValid = false;
            }
            <%} %>
            if (document.getElementById('<%=txtMobileCountryCode.ClientID %>').value.trim().length <= 0) {
                document.getElementById('mobileCountryCode').style.display = 'block';
                document.getElementById('mobileCountryCode').innerHTML = 'Please enter country code !!';
                isValid = false;
            }
            if (document.getElementById('<%=txtMobileNo.ClientID %>').value.trim().length <= 0) {
                document.getElementById('mobile').style.display = 'block';
                document.getElementById('mobile').innerHTML = "Please enter Mobile Number";
                isValid = false;
            }
//            if (document.getElementById('<%=txtMobileNo.ClientID %>').value.length <= 0) {
//                document.getElementById('mobile').style.display = 'block';
//                document.getElementById('mobile').innerHTML = 'Please enter valid phone number !!';
//                isValid = false;
//            }
//            if (!document.getElementById('<%=txtMobileNo.ClientID %>').value.indexOf("-") < 0) {
//                document.getElementById('mobile').style.display = 'block';
//                document.getElementById('mobile').innerHTML = 'Please enter country code for phone number !!';
//                isValid = false;
//            }
            <%if (NationalityRequired || result.ResultBookingSource == CT.BookingEngine.BookingSource.FlightInventory || result.ResultBookingSource == CT.BookingEngine.BookingSource.CozmoBus)
        { %>
            if (document.getElementById('<%=ddlNationality.ClientID %>').value == "-1") {
                document.getElementById('nationality').style.display = 'block';
                document.getElementById('nationality').innerHTML = "Please select Nationality";
                isValid = false;
            }
            <%} %>
            <%if (CountryRequired)
        { %>
            if (document.getElementById('<%=ddlCountry.ClientID %>').value == "-1") {
                document.getElementById('country').style.display = 'block';
                document.getElementById('country').innerHTML = "Please select Country";
                isValid = false;
            }
            <%} %>
            
            <%if (EmailRequired)
        { %>
            if (document.getElementById('<%=txtEmail.ClientID %>').value.trim().length <= 0) {
                document.getElementById('email').style.display = 'block';
                document.getElementById('email').innerHTML = "Please enter Email Address";
                isValid = false;
            }//checkEmail function is referred in Common.js added above
            else if (!checkEmail(document.getElementById('<%=txtEmail.ClientID %>').value.trim())) {
                document.getElementById('email').style.display = 'block';
                document.getElementById('email').innerHTML = "Please enter valid Email Address";
                isValid = false;
            }
            <%} %>
            <%if (AddressRequired && CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate == "Y")
        { %>
            if (document.getElementById('<%=txtAddress1.ClientID %>').value.trim().length <= 0) {
                document.getElementById('address').style.display = 'block';
                document.getElementById('address').innerHTML = "Please enter Address";
                isValid = false;
            }
            <%} %>

            //Added by lokesh on 24/08/2018
            //If the lead passenger supplies the GST number then need to supply company name also,it is mandatory
            //If the source is SpiceJet and origin country is india only
            <%if (result != null && result.Flights[0][0].Origin.CountryCode == "IN" && (result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || result.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp))
        {%>
            if (document.getElementById('<%=txtTaxRegNo_SG_GST.ClientID%>') != null && document.getElementById('<%=txtTaxRegNo_SG_GST.ClientID%>').value.trim().length >0 && document.getElementById('<%=txtCompanyName_SG_GST.ClientID%>') != null && document.getElementById('<%=txtCompanyName_SG_GST.ClientID%>').value.trim().length <=0) {
                document.getElementById('gstCompanyName_SG').style.display = 'block';
                document.getElementById('gstCompanyName_SG').innerHTML = "Please enter GST Company Name";
                isValid = false;
            }
         <%} %>

             <%if (result != null && ((result.IsGSTMandatory && result.ResultBookingSource == CT.BookingEngine.BookingSource.TBOAir) || (result.ResultBookingSource == CT.BookingEngine.BookingSource.UAPI && requiredGSTForUAPI)))
        {%>
                if (document.getElementById('<%=txtGSTCompanyAddress.ClientID%>').value.trim().length <= 0) {
                    document.getElementById('gstAddress').style.display = 'block';
                    document.getElementById('gstAddress').innerHTML = "Please enter GST Company Address";
                    isValid = false;
                }
                <%if (result.IsGSTMandatory || requiredGSTForUAPI)
        {%>
                if (document.getElementById('<%=txtGSTCompanyName.ClientID%>').value.trim().length <= 0) {
                    document.getElementById('gstName').style.display = 'block';
                    document.getElementById('gstName').innerHTML = "Please enter GST Company Name";
                    isValid = false;
                }
                <%}%>
                if (document.getElementById('<%=txtGSTContactNumber.ClientID%>').value.trim().length <= 0) {
                    document.getElementById('gstContact').style.display = 'block';
                    document.getElementById('gstContact').innerHTML = "Please enter GST Company Contact Number";
                    isValid = false;
                }
                if (document.getElementById('<%=txtGSTNumber.ClientID%>').value.trim().length <= 0) {
                    document.getElementById('gstNumber').style.display = 'block';
                    document.getElementById('gstNumber').innerHTML = "Please enter GST Registration Number";
                    isValid = false;
                }
                if (document.getElementById('<%=txtGSTCompanyEmail.ClientID%>').value.trim().length <= 0) {
                    document.getElementById('gstEmail').style.display = 'block';
                    document.getElementById('gstEmail').innerHTML = "Please enter GST Company Email";
                    isValid = false;
                }
            <%}%>
        

        if (eval(count) > 0) {
            for (var i = 0; i < eval(count); i++) {
                var paxType = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblPaxType').innerHTML;
                var dob = "dob" + i;
                var pspExpDate = "expDate" + i;
                var dob = new Date(eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlYear').value), eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlMonth').value) - 1, eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlDay').value));
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxtitle').style.display = 'none';
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxfirstname').style.display = 'none';
                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxlastname').style.display = 'none';
                    <%if (GenderRequired)
        { %>
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxgender').style.display = 'none';
                    <%} %>
                    <%if (DOBRequired)
        { %>
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxday').style.display = 'none';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxmonth').style.display = 'none';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').style.display = 'none';
                    <%} %>
                    <%if (PassportNoRequired)
        { %>
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpassportno').style.display = 'none';
                    <%} %>
                    <%if (PassportExpiryRequired)
        { %>
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeday').style.display = 'none';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeMonth').style.display = 'none';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').style.display = 'none';
                    <%} %>
                    <%if (CountryRequired)
        { %>
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxcountry').style.display = 'none';
                    <%} %>
                    
                    <%if (NationalityRequired || result.ResultBookingSource == CT.BookingEngine.BookingSource.FlightInventory || result.ResultBookingSource == CT.BookingEngine.BookingSource.CozmoBus)
        { %>
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxNationality').style.display = 'none';
                    <%} %>
                    
                    <%if (PassportExpiryRequired)
        { %>
                    //var dob = "dob" + i;
                   // var pspExpDate = "expDate" + i;
                    //var paxType = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblPaxType').innerHTML;
                   // var dob = new Date(eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlYear').value), eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlMonth').value) - 1, eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlDay').value));
                    var pspExpDate = new Date(eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPEYear').value), eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPEMonth').value) - 1, eval(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPEDay').value));
                    var currentDate = document.getElementById('<%=hdfCurrentDate.ClientID %>').value;

                    if (currentDate != null && currentDate != "") {
                        var array = currentDate.split(',');
                        var today = new Date(parseInt(array[2]), parseInt(array[1]) - 1, parseInt(array[0]));
                        var adultAge = new Date(parseInt(array[2]) - 13, parseInt(array[1]) - 1, parseInt(array[0]));
                        var childAge = new Date(parseInt(array[2]) - 12, parseInt(array[1]) - 1, parseInt(array[0]));
                        var infantAge = new Date(parseInt(array[2]) - 2, parseInt(array[1]) - 1, parseInt(array[0]));
                    }
                    <%} %>
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPaxTitle').value == "-1") {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxtitle').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxtitle').innerHTML = "Please Select Title";
                        isValid = false;
                    }
                    else
                        paxdtls = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPaxTitle').value + ' ';
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_txtPaxFName').value.trim().length <= 0) {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxfirstname').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxfirstname').innerHTML = "Please enter First Name";
                        isValid = false;
                    }
                    else
                        paxdtls = paxdtls + '-' + document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_txtPaxFName').value + ' ';
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_txtPaxLName').value.trim().length <= 0) {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxlastname').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxlastname').innerHTML = "Please enter Last Name";
                        isValid = false;
                    }
                    else
                        paxdtls = paxdtls + '-' + document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_txtPaxLName').value;

                    if (paxType != "Infant") { paxdtls = paxType == 'Adult' ? paxdtls + '(A)' : paxdtls + '(C)'; PaxNames.push(paxdtls); } paxdtls = '';

                    <%if (GenderRequired)
        { %>
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlGender').value == "-1") {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxgender').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxgender').innerHTML = "Please select Gender";
                        isValid = false;
                    }
                    <%} %>
                    <%if (DOBRequired)
        { %>
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlDay').value == "-1") {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxday').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxday').innerHTML = "Please select Day";
                        isValid = false;
                    }
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlMonth').value == "-1") {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxmonth').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxmonth').innerHTML = "Please select Month";
                        isValid = false;
                    }
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlYear').value == "-1") {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').innerHTML = "Please select Year";
                        isValid = false;
                    }
                    if (paxType == "Adult") {
                        if ((Math.ceil(sysdate.getTime()-dob.getTime())/one_day/365) <= 12) {
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').style.display = 'block';
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').innerHTML = "Adult Age should be greater than 12 years.";
                            isValid = false;
                        }
                    }
                    if (paxType == "Child") {
                        if ((Math.ceil(sysdate.getTime()-dob.getTime())/one_day/365) < 2 || (Math.ceil(sysdate.getTime()-dob.getTime())/one_day/365) > 12) {
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').style.display = 'block';
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').innerHTML = "Child Age should be between (2-12) years.";
                            isValid = false;
                        }
                    }
                    if (paxType == "Infant") {
                        if ((Math.ceil(sysdate.getTime()-dob.getTime()) / one_day / 365) <= 0 || (Math.ceil(sysdate.getTime()-dob.getTime()) / one_day / 365) > 2) {
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').style.display = 'block';
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxyear').innerHTML = "Infants Age should be less than 2 year";
                            isValid = false;
                        }
                    }
                    <%} %>
                    <%if (PassportNoRequired)
        { %>
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_txtPassportNo').value.trim().length <= 0) {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpassportno').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpassportno').innerHTML = "Please enter Passport number";
                        isValid = false;
                    }
                    <%} %>
                    <%if (PassportExpiryRequired)
        { %>
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPEDay').value == "-1") {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeday').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeday').innerHTML = "Please select Expiry Day";
                        isValid = false;
                    }
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPEMonth').value == "-1") {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeMonth').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeMonth').innerHTML = "Please select Expiry Month";
                        isValid = false;
                    }
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlPEYear').value == "-1") {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').innerHTML = "Please select Expiry Year";
                        isValid = false;
                    }
                if (pspExpDate <= sysdate) {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').innerHTML = "Passport expiry date should be later than departure date";
                    isValid = false;
                }
                else if (Math.ceil((pspExpDate.getTime() - sysdate.getTime()) / one_day) < 30) {
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').style.display = 'block';
                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxpeyear').innerHTML = "Passport should be valid for 1 month from departure date";
                    isValid = false;
                }
                    <%} %>
                    <%if (CountryRequired)
        { %>
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlCountry').value == "-1") {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxcountry').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxcountry').innerHTML = "Please select Country";
                        isValid = false;
                    }
                    <%} %>
                    
                    <%if (NationalityRequired || result.ResultBookingSource == CT.BookingEngine.BookingSource.FlightInventory || result.ResultBookingSource == CT.BookingEngine.BookingSource.CozmoBus)
        { %>
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality').value == "-1") {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxNationality').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errPaxNationality').innerHTML = "Please select Nationality";
                        isValid = false;
                    }
                    <%} %>

                
               <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate != "Y" && 1 == 2) %>
            <% {%>
            var n = 0; var isFlexValid = true;
                //Modified by Lokesh : On 15May2017
                if (getElement('hdnFlexCount') != null) {

                    for (var n = 0; n < parseInt(getElement('hdnFlexCount').value); n++) {
                        var err = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblErrorAd' + n);
                        if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblErrorAd' + n) != null) {
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblErrorAd' + n).innerHTML = '';
                        }
                        var ctrltype = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_hdnFlexControlAd' + n).value;
                        if (ctrltype == 'T') {

                            if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_hdnFlexMandatoryAd' + n).value == "Y") {
                                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_txtFlexAd' + n).value == '') {
                                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblErrorAd' + n).innerHTML = 'Please Enter ' + document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_hdnFlexLabelAd' + n).value;
                                    isFlexValid = isValid = false;
                                }
                            }
                        }

                        if (ctrltype == 'D') {
                            if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_hdnFlexMandatoryAd' + n).value == "Y") {
                                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlDayAd' + n).value == "-1" && document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlMonthAd' + n).value == "-1" && document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlYearAd' + n).value == "-1") {
                                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblErrorAd' + n).innerHTML = 'Please select Day,Month,Year';
                                    isFlexValid = isValid = false;
                                }
                                else if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlDayAd' + n).value == "-1") {
                                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblErrorAd' + n).innerHTML = 'Please select Day';
                                    isFlexValid = isValid = false;
                                }
                                else if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlMonthAd' + n).value == "-1") {
                                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblErrorAd' + n).innerHTML = 'Please select Month';
                                    isFlexValid = isValid = false;
                                }
                                else if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlYearAd' + n).value == "-1") {
                                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblErrorAd' + n).innerHTML = 'Please select Year';
                                    isFlexValid = isValid = false;
                                }
                            }
                        }

                        if (ctrltype == 'L') {
                            if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_hdnFlexMandatoryAd' + n).value == "Y") {
                                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlFlexAd' + n).value == "-1") {
                                    document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblErrorAd' + n).innerHTML = 'Please Select ' + document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_hdnFlexLabelAd' + n).value;
                                    isFlexValid = isValid = false;
                                }
                            }
                        }
                        
                    }

                    if (!isFlexValid) { ExpandAddAPI(i, 'ancAPI'); }
                }
                 <% }%>
            }
        }
            var   n = 0; var isFlexValid = true;
            //Modified by Lokesh : On 15May2017
            if (getElement('hdnFlexCount') != null && !resetLCCMealFlag && 1==2) {

                for (var n = 0; n < parseInt(getElement('hdnFlexCount').value) ; n++) {
                    if (getElement('lblError' + n) != null) {
                        getElement('lblError' + n).innerHTML = '';
                    }
                    if (getElement('hdnFlexControl' + n)!=null) {
                         switch (getElement('hdnFlexControl' + n).value) {

                        case 'T':
                            if (getElement('hdnFlexMandatory' + n).value == "Y") {
                                if (getElement('txtFlex' + n).value == '') {
                                    getElement('lblError' + n).innerHTML = 'Please Enter ' + getElement('hdnFlexLabel' + n).value;
                                    isFlexValid = isValid = false;
                                }
                            }
                            break;
                        case 'D':
                            if (getElement('hdnFlexMandatory' + n).value == "Y") {
                                if (getElement('ddlDay' + n).value == "-1" && getElement('ddlMonth' + n).value == "-1" && getElement('ddlYear' + n).value == "-1") {
                                    getElement('lblError' + n).innerHTML = 'Please select Day,Month,Year';
                                    isFlexValid = isValid = false;
                                }
                                else if (getElement('ddlDay' + n).value == "-1") {
                                    getElement('lblError' + n).innerHTML = 'Please select Day';
                                    isFlexValid = isValid = false;
                                }
                                else if (getElement('ddlMonth' + n).value == "-1") {
                                    getElement('lblError' + n).innerHTML = 'Please select Month';
                                    isFlexValid = isValid = false;
                                }
                                else if (getElement('ddlYear' + n).value == "-1") {
                                    getElement('lblError' + n).innerHTML = 'Please select Year';
                                    isFlexValid = isValid = false;
                                }
                            }
                            break;
                        case 'L':
                            if (getElement('hdnFlexMandatory' + n).value == "Y") {
                                if (getElement('ddlFlex' + n).value == "-1") {
                                    getElement('lblError' + n).innerHTML = 'Please Select ' + getElement('hdnFlexLabel' + n).value;
                                    isFlexValid = isValid = false;
                                }
                            }
                            break;
                    }
                    }
                   
                }
                    
                if (!isFlexValid) { ExpandAddAPI('', 'ancAPI'); }
                if (!isFlexValid && n > 9) { ExpandAddAPI('', 'ancfelx'); }

            }
        
        
        
            <%if (result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia)
        { %>
            //Set the selected index of baggage for Air Arabia.
            var paxCount = eval('<%=request.AdultCount + request.ChildCount %>');
            var ddlOut = document.getElementById('<%=ddlOnwardBaggage.ClientID %>');
            var ddlIn = document.getElementById('<%=ddlInwardBaggage.ClientID %>');
            //document.getElementById('<%=hdnOutBagSelection.ClientID %>').value = ddlOut.selectedIndex;            
            document.getElementById('<%=hdnOutBagSelection.ClientID %>').value = $('#ctl00_cphTransaction_ddlOnwardBaggage').select2("data").element[0].index;
            if (ddlIn != null) {
                //document.getElementById('<%=hdnInBagSelection.ClientID %>').value = ddlIn.selectedIndex;
                document.getElementById('<%=hdnInBagSelection.ClientID %>').value = $('#ctl00_cphTransaction_ddlInwardBaggage').select2("data").element[0].index;
            }
            for (i = 0; i < paxCount-1; i++) {
                var out = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardBaggage');
                var inb = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardBaggage');

                document.getElementById('<%=hdnOutBagSelection.ClientID %>').value += "," + $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardBaggage').select2("data").element[0].index;
                if (inb != null) {
                    document.getElementById('<%=hdnInBagSelection.ClientID %>').value += "," + $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardBaggage').select2("data").element[0].index;
                }
            }
            <%} %>



     //For SpiceJet For corporate fares meal is mandatory
     //Added by lokesh on 4th oct,2018.
<%if (mealRequiredSG_Corporate && CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate == "N")
        { %>
         
            var isOnwardCorporateFare='<%=isOnwardCorporateFare%>';
            var isReturnCorporateFare='<%=isReturnCorporateFare%>';
            var paxCount = eval('<%=request.AdultCount + request.ChildCount %>');
            if (document.getElementById('<%=ddlOnwardMeal.ClientID %>') != null &&
                document.getElementById('<%=ddlOnwardMeal.ClientID %>').value == "0" && isOnwardCorporateFare=='True') {
                if (document.getElementById('errOnWardLeadPaxMeal') != null) {
                    document.getElementById('errOnWardLeadPaxMeal').style.display = 'block';
                    document.getElementById('errOnWardLeadPaxMeal').innerHTML = "Please select Meal";
                }
                isValid = false;
            }
            if (document.getElementById('<%=ddlInwardMeal.ClientID %>') != null &&
                document.getElementById('<%=ddlInwardMeal.ClientID %>').value == "0" && isReturnCorporateFare=='True') {
                if (document.getElementById('errInWardLeadPaxMeal') != null) {
                    document.getElementById('errInWardLeadPaxMeal').style.display = 'block';
                    document.getElementById('errInWardLeadPaxMeal').innerHTML = "Please select Meal";
                }
                isValid = false;
            }
            for (i = 0; i < paxCount-1; i++) {
                var out = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardMeal');
                var inb = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardMeal');

                if (out != null && out.value == "0" && isOnwardCorporateFare=='True') {
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardMeal') != null) {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardMeal').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardMeal').innerHTML = "Please select Meal";
                    }
                    isValid = false;
                }
                if (inb != null && inb.value == "0" && isReturnCorporateFare=='True') {
                    if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errInwardMeal') != null) {
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errInwardMeal').style.display = 'block';
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errInwardMeal').innerHTML = "Please select Meal";
                    }
                    isValid = false;
                }
            }
            <%} %>

            //For SpiceJet regarding meal selection addded the below logic
            if (!resetLCCMealFlag) {
<%if (result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || result.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia)
        { %>

                var paxCount = eval('<%=request.AdultCount + request.ChildCount %>');
                var ddlOut = document.getElementById('<%=ddlOnwardMeal.ClientID %>');
                var ddlIn = document.getElementById('<%=ddlInwardMeal.ClientID %>');

                if (document.getElementById('<%=hdnOutMealSelection.ClientID %>') != null) {

                    if (ddlOut != null) {
                        document.getElementById('<%=hdnOutMealSelection.ClientID %>').value = $('#ctl00_cphTransaction_ddlOnwardMeal').select2("data").element[0].index;
                    }
                    else {
                        document.getElementById('<%=hdnOutMealSelection.ClientID %>').value = "0";
                    }
                }
                if (document.getElementById('<%=hdnInMealSelection.ClientID %>') != null) {
                    if (ddlIn != null) {
                        document.getElementById('<%=hdnInMealSelection.ClientID %>').value = $('#ctl00_cphTransaction_ddlInwardMeal').select2("data").element[0].index;
                    }
                    else {
                        document.getElementById('<%=hdnInMealSelection.ClientID %>').value = "0";
                    }
                }
                for (i = 0; i < paxCount - 1; i++) {
                    var out = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardMeal');
                    var inb = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardMeal');
                    if (document.getElementById('<%=hdnOutMealSelection.ClientID %>') != null) {
                        if (out != null) {
                            document.getElementById('<%=hdnOutMealSelection.ClientID %>').value += "," + $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardMeal').select2("data").element[0].index;
                        }
                        else {
                            document.getElementById('<%=hdnOutMealSelection.ClientID %>').value += "," + "0";
                        }
                    }
                    if (document.getElementById('<%=hdnInMealSelection.ClientID %>') != null) {
                        if (inb != null) {
                            document.getElementById('<%=hdnInMealSelection.ClientID %>').value += "," + $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardMeal').select2("data").element[0].index;
                        }
                        else {
                            document.getElementById('<%=hdnInMealSelection.ClientID %>').value += "," + "0";
                        }
                    }
                }
            <%} %>
            }
         
         //Added by Lokesh on 9-sept-2017 to validate the pax additional service nationality   
     //If the result booking source is flight inventory only then validate the pax additional services
        <%if (result.ResultBookingSource == CT.BookingEngine.BookingSource.FlightInventory || result.ResultBookingSource == CT.BookingEngine.BookingSource.CozmoBus)
        { %>
       
            var validateAddServices = validatePaxAddServices(count);
            if(validateAddServices == false)
            {
               document.getElementById('paxAddSerNotification').style.display='block';
               isValid = false;
            }
            else
            {
             document.getElementById('paxAddSerNotification').style.display='none';
            }
            <%} %>


            //Added by Lokesh on 27-Mar-2018 to Validate the GSTIN Details from the customer.

            
            <%if (result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia)
        {%>
            var country = document.getElementById('<%=ddlCountry.ClientID%>');
            var selectedCountryText;            
            selectedCountryText = country.options[country.selectedIndex].text;
            
            var originCountry = '<%=result.Flights[0][0].Origin.CountryCode%>'
            if (originCountry == 'IN') {//NOTE:ALWAYS THE ORIGIN COUNTRY CODE SHOULD BE INDIA ONLY FOR G9 THEN ONLY VALIDATE THE STATE CODE
                if (document.getElementById('pnlGSTDetails').style.display = 'block' && document.getElementById('<%=ddlGSTStateCode.ClientID %>').value == "-1") {
                    document.getElementById('valGSTStateCode').style.display = 'block';
                    document.getElementById('valGSTStateCode').innerText = "Please Select State Code.";
                    isValid = false;
                }
            }
            
            <%}%>
            if (isValid) {
                if (document.getElementById('<%=ddlGSTStateCode.ClientID %>') != null) {
                    document.getElementById('<%=hdnSelStateId.ClientID %>').value = document.getElementById('<%=ddlGSTStateCode.ClientID %>').value;
                }    
            }


            //Checking G9 BUndle Fares Baggage, Meal, Seat Validations
            <%if (result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia)
        { %>
            debugger;
            var onwardG9='<%=isG9OnwardBundled%>';
            var returnG9 = '<%=isG9ReturnBundled%>';
            var leadpaxbag='', leadpaxmeal='', addpaxbag='', addpaxmeal='';
            if (onwardG9 == 'True' || returnG9 == 'True') {
                var paxCount = eval('<%=request.AdultCount + request.ChildCount %>');
                if (document.getElementById('<%=ddlOnwardMeal.ClientID %>') != null &&
                    document.getElementById('<%=ddlOnwardMeal.ClientID %>').value.split('-')[0] == "0" && onwardG9 == 'True') {
                    if (document.getElementById('errOnWardLeadPaxMeal') != null) {
                        document.getElementById('errOnWardLeadPaxMeal').style.display = 'block';
                        document.getElementById('errOnWardLeadPaxMeal').innerHTML = "Please select Meal";
                        leadpaxmeal = 'False';
                        isValid = false;
                    }
                }
                else if(document.getElementById('errOnWardLeadPaxMeal')!=null)
                    document.getElementById('errOnWardLeadPaxMeal').style.display = 'none';

                if (document.getElementById('<%=ddlOnwardBaggage.ClientID %>') != null &&
                    document.getElementById('<%=ddlOnwardBaggage.ClientID %>').value.split('-')[0] == "0" && onwardG9 == 'True') {
                    if (document.getElementById('errOnWardLeadPaxBaggage') != null) {
                        document.getElementById('errOnWardLeadPaxBaggage').style.display = 'block';
                        document.getElementById('errOnWardLeadPaxBaggage').innerHTML = "Please select Baggage";
                        leadpaxbag = 'False';
                        isValid = false;
                    }
                }
                else if(document.getElementById('errOnWardLeadPaxBaggage')!=null)
                    document.getElementById('errOnWardLeadPaxBaggage').style.display = 'none';

                if (document.getElementById('<%=ddlInwardMeal.ClientID %>') != null &&
                    document.getElementById('<%=ddlInwardMeal.ClientID %>').value.split('-')[0] == "0" && returnG9 == 'True') {
                    if (document.getElementById('errInWardLeadPaxMeal') != null) {
                        document.getElementById('errInWardLeadPaxMeal').style.display = 'block';
                        document.getElementById('errInWardLeadPaxMeal').innerHTML = "Please select Meal";
                        leadpaxmeal = 'False';
                        isValid = false;
                    }
                }
                else if(document.getElementById('errInWardLeadPaxMeal')!=null)
                    document.getElementById('errInWardLeadPaxMeal').style.display = 'none';

                if (document.getElementById('<%=ddlInwardBaggage.ClientID %>') != null &&
                    document.getElementById('<%=ddlInwardBaggage.ClientID %>').value.split('-')[0] == "0" && returnG9 == 'True') {
                    if (document.getElementById('errReturnLeadPaxBaggage') != null) {
                        document.getElementById('errReturnLeadPaxBaggage').style.display = 'block';
                        document.getElementById('errReturnLeadPaxBaggage').innerHTML = "Please select Baggage";
                        leadpaxbag = 'False';
                        isValid = false;
                    }
                }
                else if( document.getElementById('errReturnLeadPaxBaggage')!=null)
                    document.getElementById('errReturnLeadPaxBaggage').style.display = 'none';

                for (i = 0; i < paxCount - 1; i++) {
                    var out = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardMeal');
                    var inb = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardMeal');

                    var onwBaggage = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardBaggage');
                    var retBagage = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardBaggage');


                    if (out != null && out.value.split('-')[0] == "0" && onwardG9 == 'True') {
                        if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardMeal') != null) {
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardMeal').style.display = 'block';
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardMeal').innerHTML = "Please select Meal";
                            addpaxmeal = 'False';
                            isValid = false;
                        }
                    }
                    else if(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardMeal')!=null)
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardMeal').style.display = 'block';


                    if (onwBaggage != null && onwBaggage.value.split('-')[0] == "0" && onwardG9 == 'True') {
                        if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardBaggage') != null) {
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardBaggage').style.display = 'block';
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardBaggage').innerHTML = "Please select Baggage";
                            addpaxbag = 'False';
                            isValid = false;
                        }
                    }
                    else if(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardBaggage')!=null)
                       document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errOnwardBaggage').style.display = 'none';

                    if (inb != null && inb.value.split('-')[0] == "0" && returnG9 == 'True') {
                        if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errInwardMeal') != null) {
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errInwardMeal').style.display = 'block';
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errInwardMeal').innerHTML = "Please select Meal";
                            addpaxmeal = 'False';
                            isValid = false;
                        }
                    }
                     else if(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errInwardMeal')!=null)
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errInwardMeal').style.display = 'none';

                    if (retBagage != null && retBagage.value.split('-')[0] == "0" && returnG9 == 'True') {
                        if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errReturnBaggage') != null) {
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errReturnBaggage').style.display = 'block';
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errReturnBaggage').innerHTML = "Please select Baggage";
                            addpaxbag = 'False';
                            isValid = false;
                        }
                    }
                     else if(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errReturnBaggage')!=null)
                        document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_errReturnBaggage').style.display = 'none';

                }
                //SEAT VALIDATION POP UP FOR G9 BUNDLE FARES
                var SSRMsg = '';
                if (document.getElementById('<%=hdnValidateG9Seat.ClientID%>').value == 'YES') {
                    SSRMsg = (leadpaxbag == 'False' || addpaxbag == 'False') ? "BAGGAGE" : SSRMsg;
                    SSRMsg = (leadpaxmeal == 'False' || addpaxmeal == 'False') ? (SSRMsg == '') ? "MEAL" : SSRMsg + ", MEAL" : SSRMsg;
                    SSRMsg = (!CheckG9SeatSelection()) ? (SSRMsg == '') ? "SEAT" : SSRMsg + ", SEAT" : SSRMsg;
              <%--  //var onwfareType = (onwardG9 == 'True') ? '<%=result.FareType.Split(',')[0]%>' : "";
                // var retfareType = (returnG9 == 'True') ? '<%=result.FareType.Split(',')[1]%>' : "";--%>
                    // var fareType = (onwfareType != '' && retfareType != '') ? onwfareType + "," + retfareType : (onwfareType != '') ? onwfareType : (retfareType != '') ? retfareType : "";
                    if (SSRMsg != '') {
                        $("#bundleValidation").modal({
                            backdrop: 'static',
                            keyboard: false
                        });

                        $("#bundleValidation").modal('show');
                        $('#divMsg').html("<b>The fare you have selected included the following :" + SSRMsg + ". </br> Please make your selection to continue</b>");
                        isValid = false;
                    }
                }
            }
            <%}%>

            if (isValid && document.getElementById('<%=hdnFlexInfo.ClientID %>').value != '' && !resetLCCMealFlag)
                isValid = ValidateFlex((SaveFlexHTML ? document.getElementById('<%=hdnPaxFlexInfo.ClientID %>').id : ''), true);
            $('#<%=hdnPaxSubmit.ClientID%>').val(isValid);
            return isValid;
        }

        function CheckVal(){
         var isvalid=true;
          document.getElementById('<%=txtMarkup.ClientID%>').value="0";
          if(document.getElementById('<%=rbtnPercent.ClientID%>').checked==true)
          {  
             document.getElementById('<%=txtMarkup.ClientID%>').maxLength="2";
          }
          if(document.getElementById('<%=rbtnFixed.ClientID%>').checked==true)
          {  
             document.getElementById('<%=txtMarkup.ClientID%>').maxLength="4";
          }
          return isvalid;
       }

        function ExpandAddAPI(paxindx, btnid) {

            try {
                                
                var ancAPI = document.getElementById(btnid + paxindx);
                if (ancAPI.attributes["aria-expanded"].value == 'false')
                    ancAPI.click();                
            }
            catch (exception) {
            }
        }
        
        function validatePaxAddServices(Count){
        
        var paxCount = eval(Count) + 1;
        var valid = true;
        var paxNationalityInvalid =0;
        var nat = document.getElementById("<%=hdnAddNationlities.ClientID%>").value;
         var paxNat = [];  
         for (var i = 0; i < paxCount; i++) {
         
         
         if(i==0){ //Lead Pax Nationality
        
         if (document.getElementById('ctl00_cphTransaction_ddlNationality').value != "-1"){
         
          paxNat.push(document.getElementById('ctl00_cphTransaction_ddlNationality').value);
         
         }
         
         if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality') != null){
         
          if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality').value != "-1"){
          
          
          paxNat.push(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality').value);
          }
          
          }
         
         
         
         }
         
         else{ //Additional Passengers Nationality
         
         if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality') != null){
         
          if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality').value != "-1"){
          
          
          paxNat.push(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlNationality').value);
          }
          
          }
          
          
          
          }
          
          
         
         
         }//EOF for loop
         
         if(paxNat.length >0 && nat.length >0)
          {
              for(var p=0;p<paxNat.length;p++)
              {
                  if (nat.indexOf(paxNat[p]) == -1) {++ paxNationalityInvalid; }
                        
              }
          
          }
           
           if(paxNationalityInvalid >0) 
           {
             valid = false;
           }
           else{
           
               valid = true;
           }       
           return valid;
           
        }
        

        
        function cancelNotification(){
        document.getElementById('paxAddSerNotification').style.display='none';
        }
        
        

        function CalculateBaggagePrice() {
            var source='<%=result.ResultBookingSource.ToString() %>';
            var decimalpoint = eval('<%=agency.DecimalValue %>');
            var paxCount = eval('<%=request.AdultCount + request.ChildCount %>');
            var totalFare = eval(0);
            var baggageprice = eval(0);
            var mealPrice = eval(0);

            var ddlOut = document.getElementById('<%=ddlOnwardBaggage.ClientID %>');
            var ddlIn = document.getElementById('<%=ddlInwardBaggage.ClientID %>');
            var lblout = document.getElementById('<%=lblOutPrice.ClientID %>');
            var lblin = document.getElementById('<%=lblInPrice.ClientID %>');

            var ddlOutMeal = document.getElementById('<%=ddlOnwardMeal.ClientID %>');
            var ddlInMeal = document.getElementById('<%=ddlInwardMeal.ClientID %>');
            var lbloutMeal = document.getElementById('<%=lblOutMealPrice.ClientID %>');
            var lblinMeal = document.getElementById('<%=lblInMealPrice.ClientID %>');

            if (ddlOut != null && ddlOut.value != "No Bag") {
                if (source == 'AirArabia') {
                    totalFare += eval(ddlOut.value.split('-')[1]);
                    baggageprice += eval(ddlOut.value.split('-')[1]);
                }
                else {
                    totalFare += eval(ddlOut.value.split('-')[0]);
                    baggageprice += eval(ddlOut.value.split('-')[0]);
                }
                if (lblout != null) {
                    if (source == "AirArabia") {
                        document.getElementById('<%=lblOutPrice.ClientID %>').value = parseFloat(eval(ddlOut.value.split('-')[1])).toFixed(decimalpoint);
                    }
                    else {
                        document.getElementById('<%=lblOutPrice.ClientID %>').value = parseFloat(eval(ddlOut.value.split('-')[0])).toFixed(decimalpoint);
                    }
                }
            }

            if (ddlOutMeal != null && ddlOutMeal.value.split('-')[0] != "No Meal") {
                if (source == 'AirArabia') {
                    totalFare += eval(ddlOutMeal.value.split('-')[1]);
                    mealPrice += eval(ddlOutMeal.value.split('-')[1]);
                }
                else {
                    totalFare += eval(ddlOutMeal.value.split('-')[0]);
                    mealPrice += eval(ddlOutMeal.value.split('-')[0]);
                }
                if (lbloutMeal != null) {
                    if (source == "AirArabia") 
                        document.getElementById('<%=lblOutMealPrice.ClientID %>').value = parseFloat(eval(ddlOutMeal.value.split('-')[1])).toFixed(decimalpoint);
                    else {
                        if (window.navigator.appCodeName == "Mozilla") {
                            document.getElementById('<%=lblOutMealPrice.ClientID %>').value = parseFloat(eval(ddlOutMeal.value.split('-')[0])).toFixed(decimalpoint);
                        }
                        else {
                            document.getElementById('<%=lblOutMealPrice.ClientID %>').value = parseFloat(eval(ddlOutMeal.value.split('-')[0])).toFixed(decimalpoint);
                        }
                    }
                    
                }
            }

            if (ddlIn != null && ddlIn.value != "No Bag") {
                if (source == "AirArabia") {
                    totalFare += eval(ddlIn.value.split('-')[1]);
                    baggageprice += eval(ddlIn.value.split('-')[1]);
                }
                else {
                    totalFare += eval(ddlIn.value.split('-')[0]);
                    baggageprice += eval(ddlIn.value.split('-')[0]);
                }
                if (lblin != null) {
                    if (source == "AirArabia") {
                        document.getElementById('<%=lblInPrice.ClientID %>').value = parseFloat(eval(ddlIn.value.split('-')[1])).toFixed(decimalpoint);
                    }
                    else {
                        document.getElementById('<%=lblInPrice.ClientID %>').value = parseFloat(eval(ddlIn.value.split('-')[0])).toFixed(decimalpoint);
                    }
                }
            }

            if (ddlInMeal != null && ddlInMeal.value.split('-')[0] != "No Meal") {
                if (source == "AirArabia") {
                    totalFare += eval(ddlInMeal.value.split('-')[1]);
                    mealPrice += eval(ddlInMeal.value.split('-')[1]);
                }
                else {
                    totalFare += eval(ddlInMeal.value.split('-')[0]);
                    mealPrice += eval(ddlInMeal.value.split('-')[0]);
                }
                if (lblinMeal != null) {
                    if (source == "AirArabia")
                        document.getElementById('<%=lblInMealPrice.ClientID %>').value = parseFloat(eval(ddlInMeal.value.split('-')[1])).toFixed(decimalpoint);
                    else {
                        if (window.navigator.appCodeName == "Mozilla") {
                            document.getElementById('<%=lblInMealPrice.ClientID %>').value = parseFloat(eval(ddlInMeal.value.split('-')[0])).toFixed(decimalpoint);
                        }
                        else {
                            document.getElementById('<%=lblInMealPrice.ClientID %>').value = parseFloat(eval(ddlInMeal.value.split('-')[0])).toFixed(decimalpoint);
                        }
                    }
                }
            }
            
            for (i = 0; i < paxCount - 1; i++) {
                var out = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardBaggage');
                var inb = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardBaggage');

                var outMeal = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlOnwardMeal');
                var inbMeal = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlInwardMeal');

                var lblout = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutPrice');
                var lblin = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInPrice');

                var lbloutMeal = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutMealPrice');
                var lblinMeal = document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInMealPrice');



                if (out != null && out.value != "No Bag") {
                    if (source == "AirArabia") {
                        totalFare += eval(out.value.split('-')[1]);
                        baggageprice += eval(out.value.split('-')[1]);
                    }
                    else {
                        totalFare += eval(out.value.split('-')[0]);
                        baggageprice += eval(out.value.split('-')[0]);
                    }
                    if (lblout != null) {
                        if (source == "AirArabia") {
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutPrice').value = parseFloat(eval(out.value.split('-')[1])).toFixed(decimalpoint);
                        }
                        else {
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutPrice').value = parseFloat(eval(out.value.split('-')[0])).toFixed(decimalpoint);
                        }
                    }
                }

                if (outMeal != null && outMeal.value.split('-')[0] != "No Meal") {
                    if (source == "AirArabia") {
                        totalFare += eval(outMeal.value.split('-')[1]);
                        mealPrice += eval(outMeal.value.split('-')[1]);
                    }
                    else {
                        totalFare += eval(outMeal.value.split('-')[0]);
                        mealPrice += eval(outMeal.value.split('-')[0]);
                    }
                    if (lbloutMeal != null) {
                        if (source == "AirArabia")
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutMealPrice').value = parseFloat(eval(outMeal.value.split('-')[1])).toFixed(decimalpoint);
                        else {
                            if (window.navigator.appCodeName == "Mozilla") {
                                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutMealPrice').value = parseFloat(eval(outMeal.value.split('-')[0])).toFixed(decimalpoint);
                            }
                            else {
                                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutMealPrice').value = parseFloat(eval(outMeal.value.split('-')[0])).toFixed(decimalpoint);
                            }
                        }
                    }
                }

                if (inb != null && inb.value != "No Bag") {
                    if (source == "AirArabia") {
                        totalFare += eval(inb.value.split('-')[1]);
                        baggageprice += eval(inb.value.split('-')[1]);
                    }
                    else {
                        totalFare += eval(inb.value.split('-')[0]);
                        baggageprice += eval(inb.value.split('-')[0]);
                    }
                    if (lblin != null) {
                        if (source == "AirArabia") {
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInPrice').value = parseFloat(eval(inb.value.split('-')[1])).toFixed(decimalpoint);
                        }
                        else {
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInPrice').value = parseFloat(eval(inb.value.split('-')[0])).toFixed(decimalpoint);
                        }
                    }
                }


                if (inbMeal != null && inbMeal.value.split('-')[0] != "No Meal") {
                    if (source == "AirArabia") {
                        totalFare += eval(inbMeal.value.split('-')[1]);
                        mealPrice += eval(inbMeal.value.split('-')[1]);
                    }
                    else {
                        totalFare += eval(inbMeal.value.split('-')[0]);
                        mealPrice += eval(inbMeal.value.split('-')[0]);
                    }

                    if (lblinMeal != null) {
                        if (source == "AirArabia")
                            document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInMealPrice').value = parseFloat(eval(inbMeal.value.split('-')[1])).toFixed(decimalpoint);
                        else {
                            if (window.navigator.appCodeName == "Mozilla") {
                                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInMealPrice').value = parseFloat(eval(inbMeal.value.split('-')[0])).toFixed(decimalpoint);
                            }
                            else {
                                document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInMealPrice').value = parseFloat(eval(inbMeal.value.split('-')[0])).toFixed(decimalpoint);
                            }
                        }
                    }
                }

            }

            var airPrice = eval(0);
            var discount;
            if (window.navigator.appCodeName == "Mozilla") {
                airPrice = document.getElementById('<%=lblTotalFare.ClientID %>').value;
            }
            else {
                airPrice = document.getElementById('<%=lblTotalFare.ClientID %>').value;
            }
            airPrice = airPrice.replace(',', '');
            
            
            if (window.navigator.appCodeName == "Mozilla") {
            if(source=="TBOAir") //Added by brahmam (Total price ceiling for TBO Source)
            {
                document.getElementById('<%=lblTotalPrice.ClientID %>').value = "" +Math.ceil(parseFloat((eval(airPrice) + totalFare))).toFixed(decimalpoint);
                }
                else
                {
                document.getElementById('<%=lblTotalPrice.ClientID %>').value = "" + parseFloat(eval(eval(airPrice) + totalFare)).toFixed(decimalpoint);
                }
                document.getElementById('<%=lblBaggageAmt.ClientID %>').value = parseFloat(baggageprice).toFixed(decimalpoint);
                if (document.getElementById('<%=lblMealAmt.ClientID %>') != null) {
                    document.getElementById('<%=lblMealAmt.ClientID %>').value = parseFloat(mealPrice).toFixed(decimalpoint);
                }
            }
            else {
                if(source=="TBOAir"){ //Added by brahmam (Total price ceiling for TBO Source)
                document.getElementById('<%=lblTotalPrice.ClientID %>').value = "" +Math.ceil(parseFloat(eval(airPrice + totalFare))).toFixed(decimalpoint);
                }
                else{
                document.getElementById('<%=lblTotalPrice.ClientID %>').value = "" + parseFloat(eval(airPrice + totalFare)).toFixed(decimalpoint);
                }
                document.getElementById('<%=lblBaggageAmt.ClientID %>').value = parseFloat(baggageprice).toFixed(decimalpoint);
                if (document.getElementById('<%=lblMealAmt.ClientID %>') != null) {
                    document.getElementById('<%=lblMealAmt.ClientID %>').value = parseFloat(mealPrice).toFixed(decimalpoint);
                }
            }
        }
        function TitleChanged(title, i) {

            var ddlTitle, ddlGender;
            ddlTitle = $('#' + title);
            if (i >= 0) {
                ddlGender = $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_ddlGender');
            }
            else {
                ddlGender = $('#<%=ddlGender.ClientID %>');
            }
            if (ddlTitle.val() == 'Mr' ||  ddlTitle.val() == 'MSTR') {
                ddlGender.select2('val', '1');
            }
            else if (ddlTitle.val() == 'Ms' || ddlTitle.val() == 'Mrs') {
                ddlGender.select2('val', '2');
            }
            else {
                ddlGender.select2('val', '-1');
            }

        }

        function RecalculateCost() {
            var source = '<%=result.ResultBookingSource.ToString() %>';
            var asvAmount = 0;
            var decimalpoint= eval('<%=agency.DecimalValue %>');
            var baseFare = eval('<%=result.BaseFare %>');
            var Tax = eval('<%=Tax %>');
            var markup = document.getElementById('<%=txtMarkup.ClientID %>').value;
            if (eval(markup) == 0) {
                document.getElementById('<%=txtMarkup.ClientID %>').value = 0;
            }
            else if (markup.startsWith("0")) {
                markup = eval(document.getElementById('<%=txtMarkup.ClientID %>').value.replace(/\b0+/g, ""));
                document.getElementById('<%=txtMarkup.ClientID %>').value = markup;
            }
            else {
                markup = eval(document.getElementById('<%=txtMarkup.ClientID %>').value);
            }
            if (markup == null || markup == undefined) markup = 0;
            if (document.getElementById('<%=rbtnFare.ClientID %>').checked == true) {
                document.getElementById('<%=hdfAsvElement.ClientID %>').value = "BF";


                if (baseFare > 0) {
                    if (document.getElementById('<%=rbtnPercent.ClientID %>').checked == true) {
                        asvAmount += (baseFare * (markup / 100));
                        baseFare = baseFare + (baseFare * (markup / 100));
                        document.getElementById('<%=hdfAsvType.ClientID %>').value = "P";
                    }
                    else if (document.getElementById('<%=rbtnFixed.ClientID %>').checked == true) {
                        asvAmount += markup;
                        baseFare = eval(baseFare) + eval(markup);
                        document.getElementById('<%=hdfAsvType.ClientID %>').value = "F";
                    }
                    if (window.navigator.appCodeName == "Mozilla") {
                        document.getElementById('<%=lblBaseFare.ClientID %>').value = parseFloat(baseFare).toFixed(decimalpoint);
                        document.getElementById('<%=lblTax.ClientID %>').value = parseFloat(Tax).toFixed(decimalpoint);
                    }
                    else {
                        document.getElementById('<%=lblBaseFare.ClientID %>').value = parseFloat(baseFare).toFixed(decimalpoint);
                        document.getElementById('<%=lblTax.ClientID %>').value = parseFloat(Tax).toFixed(decimalpoint);
                    }

                }
            }
            if (document.getElementById('<%=rbtnTax.ClientID %>').checked == true) {
                document.getElementById('<%=hdfAsvElement.ClientID %>').value = "TF";

                if (Tax > 0) {
                    if (document.getElementById('<%=rbtnPercent.ClientID %>').checked == true) {
                        asvAmount += (Tax * (markup / 100));
                        Tax = Tax + (Tax * (markup / 100));
                        document.getElementById('<%=hdfAsvType.ClientID %>').value = "P";
                    }
                    else if (document.getElementById('<%=rbtnFixed.ClientID %>').checked == true) {
                        asvAmount += markup;
                        Tax = eval(Tax) + eval(markup);
                        document.getElementById('<%=hdfAsvType.ClientID %>').value = "F";
                    }
                    if (window.navigator.appCodeName == "Mozilla") {
                        document.getElementById('<%=lblTax.ClientID %>').value = parseFloat(Tax).toFixed(decimalpoint);
                        document.getElementById('ctl00_cphTransaction_lblBaseFare').value = parseFloat(baseFare).toFixed(decimalpoint);
                    }
                    else {
                        document.getElementById('<%=lblTax.ClientID %>').value = parseFloat(Tax).toFixed(decimalpoint);
                        document.getElementById('ctl00_cphTransaction_lblBaseFare').value = parseFloat(baseFare).toFixed(decimalpoint);
                    }

                }
            }
        document.getElementById('<%=hdfAsvAmount.ClientID %>').value = asvAmount;
        //Re-Calculate Total Fare
            var fare = eval(document.getElementById('<%=lblBaseFare.ClientID %>').value);
            var Taxes = eval(document.getElementById('<%=lblTax.ClientID %>').value);
            var baggage = eval(document.getElementById('<%=lblBaggageAmt.ClientID %>').value);
            var meal = eval(0);
            if (document.getElementById('<%=lblMealAmt.ClientID %>') != null) {
              meal= document.getElementById('<%=lblMealAmt.ClientID %>').value != '' ? eval(document.getElementById('<%=lblMealAmt.ClientID %>').value) : meal;
            }
            var discount=eval(0);
            if(document.getElementById('<%=lblDiscount.ClientID %>') != null){ 
                 discount = eval(document.getElementById('<%=lblDiscount.ClientID %>').value.replace('(-)','').replace(',',''));
            }
            var total =eval(0);
            if (baggage > 0) {
                total = eval(fare + Taxes + baggage)-discount;
            } 
            else {
                total = eval(fare + Taxes)-discount;
            }
            if (meal > 0) {
                if (baggage > 0) {
                    total = eval(fare + Taxes + baggage + meal) - discount;
                }
                else {
                    total = eval(fare + Taxes + meal) - discount;
                }
            }

            document.getElementById('<%=lblTotalFare.ClientID %>').value = parseFloat(eval(total) - eval(baggage) - eval(meal)).toFixed(decimalpoint);
            if(source=="TBOAir") //Added by brahmam (Total price ceiling for TBO Source)
            {
            document.getElementById('<%=lblTotalPrice.ClientID %>').value =Math.ceil(parseFloat(total)).toFixed(decimalpoint);
            }
            else{
             document.getElementById('<%=lblTotalPrice.ClientID %>').value = parseFloat(total).toFixed(decimalpoint);
            }
        return false;
    }
    function Check(id) {
        var val = document.getElementById(id).value;
        if (val == '0') {
            document.getElementById(id).value = '';
        }
    }
    function checkingNumber(id)
    { 
        var clipboardData, pastedData;
        clipboardData = id.clipboardData || window.clipboardData;//clipboardData is the cache memory of browser
        pastedData = clipboardData.getData('Text');
    
        for(var i=0;i<pastedData.length;i++)
        {
            var charCode=pastedData.charCodeAt(i);
            if (charCode > 31 && (charCode < 48 || charCode > 57)) 
            {
                document.getElementById('<%=lblTotalPrice.ClientID %>').value ="";
                alert("Please enter valid phone number");
                return false;
            }
        }
        if(pastedData.length>10)
        {
            document.getElementById('<%=lblTotalPrice.ClientID %>').value ="";
            alert("Please enter valid phone number");
            return false;
        }
        else{
        return true;
        }
    }
    function Set(id) {
        var val = document.getElementById(id).value;
        if (val == '' || val == '0') {
            document.getElementById(id).value = '0';
        }
    }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function CheckG9SeatSelection() {
            debugger;
            var isvalid = true;
            var paxseatobj = document.getElementById('<%=txtPaxSeatInfo.ClientID %>').value;
            if (paxseatobj != '') {
                paxseatdtls = JSON.parse(paxseatobj);
                var bundleSegments = document.getElementById('<%=hdnG9BundleSegments.ClientID%>').value;

                for (var i = 0; i < paxseatdtls.length; i++) {
                    var obj = paxseatdtls[i];
                    for (var key in obj) {
                        var segment = obj["Segment"];
                        var seatNo = obj["SeatNo"];
                        if (bundleSegments.indexOf(segment) != -1 && seatNo == '' && isvalid) {
                            isvalid = false;
                        }
                    }
                }
            }
            else
                isvalid = false;
            return isvalid;
        }
    </script>

    <%--<script type="text/javascript">
        function WindowLoad() {
            
            var paxCount = eval('<%=request.AdultCount + request.ChildCount + request.InfantCount %>');
            if (document.getElementById('<%= lblOutPrice.ClientID %>')) document.getElementById('<%= lblOutPrice.ClientID %>').disabled = true;
            if(document.getElementById('<%= lblOutMealPrice.ClientID %>'))document.getElementById('<%= lblOutMealPrice.ClientID %>').disabled = true;

           
           <%if(request.Type == CT.BookingEngine.SearchType.Return)
           
           {%>
            if (document.getElementById('<%= lblInPrice.ClientID %>')) document.getElementById('<%= lblInPrice.ClientID %>').disabled = true;
            if (document.getElementById('<%= lblInMealPrice.ClientID %>')) document.getElementById('<%= lblInMealPrice.ClientID %>').disabled = true;
           <%} %>
            
            document.getElementById('<%= lblBaggageAmt.ClientID %>').disabled = true;
            if (document.getElementById('<%=lblMealAmt.ClientID %>') != null) {
                document.getElementById('<%= lblMealAmt.ClientID %>').disabled = true;
            }
            document.getElementById('<%= lblTotalPrice.ClientID %>').disabled = true;
            document.getElementById('<%= lblBaseFare.ClientID %>').disabled = true;
            document.getElementById('<%= lblTax.ClientID %>').disabled = true;
            document.getElementById('<%= lblTotalFare.ClientID %>').disabled = true;
            for (i = 0; i < paxCount - 1; i++) {
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutPrice')) document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutPrice').disabled = true;
                if(document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutMealPrice'))document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblOutMealPrice').disabled = true;
                <%if(request.Type == CT.BookingEngine.SearchType.Return)
           
           {%>
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInPrice')) document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInPrice').disabled = true;
                if (document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInMealPrice')) document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + i + '_lblInMealPrice').disabled = true;
                <%} %> 
                
            }
            CalculateBaggagePrice();
        }
        window.onload = WindowLoad;
        window.onclick = WindowLoad; 
    </script>--%>

    <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
            return ret;
        }
        function PriceContinue() {
            //document.getElementById('FareDiff').style.display = 'none';
            $('#FareDiff').modal('hide')
            document.getElementById('<%=imgContinue.ClientID %>').style.display = 'block';
            document.getElementById('priceChange').style.display = 'none';
            document.getElementById('<%=hdfFareAction.ClientID %>').value = "Update";
            document.forms[0].submit();
        }

        function PriceCancel() {
            //document.getElementById('FareDiff').style.display = 'none';
            $('#FareDiff').modal('hide')
            window.location.href = 'HotelSearch.aspx?source=Flight';
        }

//This function will execute when there are any mandatory fields in AddMoreDetails Section.
//Mandatory Fields may be Gender,Passport Expiry ,Passport Number,Nationality,Country or Address Fields.
        function expandMoreDetails() {      
             //Call the below function if there are any mandatory fields in AddMoreDetails Section.
           //Mandatory Fields may be Gender,Passport Expiry ,Passport Number,Nationality,Country or Address Fields.
            $('.advPax-collapse-btn').trigger('click');
        }



        $(function () {
//            var BaggageInputCtrl = $('.baggage-input-ctrl');

//            $('.baggage-wrapper').hide();

//            BaggageInputCtrl.each(function () {
//                var $this = $(this);
//                
//                if ($(this).length) {
//                    $this.closest('.baggage-wrapper').show();
//                }
//            })   
//            
            
            
            //var collapseBtn = $('.advPax-collapse-btn[data-toggle="collapse"]'),
            //    hash = 0;
            //collapseBtn.each(function () {                     
            //    $(this).attr('href', '#paggengerDetailsCollapse-' + (++hash));               
            //})
            //hash = 0;
            //$('.advPx-collapseContent').each(function () {
            //    $(this).attr('id', 'paggengerDetailsCollapse-' + ((++hash)))
            //})
            
            //Call the below function if there are any mandatory fields in AddMoreDetails Section.
           //Mandatory Fields may be Gender,Passport Expiry ,Passport Number,Nationality,Country or Address Fields.

            <%if (GenderRequired || PassportNoRequired || PassportExpiryRequired || NationalityRequired || CountryRequired || AddressRequired || result.ResultBookingSource == CT.BookingEngine.BookingSource.FlightInventory || result.ResultBookingSource == CT.BookingEngine.BookingSource.CozmoBus)
        {%>
            
                 expandMoreDetails(); //This function will automatically expand the add more details section if there are any mandatory fields for better user experience.                    
            <%} %>
            //If isGSTMandatory returned in FareQuote response for TBO then allow GST fields mandatory
            <%if (result != null && ((result.ResultBookingSource == CT.BookingEngine.BookingSource.TBOAir && result.IsGSTMandatory) || (result.ResultBookingSource == CT.BookingEngine.BookingSource.UAPI && requiredGSTForUAPI)))
        { %>
            document.getElementById('divGSTFields').style.display = 'block';
         <%}%>
        })


    </script>

   
    <script type="text/javascript">

        var FPaxcount = 0;
        var CorpTravelReason = 0;
        var SaveFlexHTML = true;        

        $(document).ready(function () {

            FPaxcount = '<%=paxCount %>';
            CorpTravelReason = '<%=request.CorporateTravelReasonId %>';

            /* To bind flex fields on page load */
            if (document.getElementById('<%=hdnFlexInfo.ClientID %>').value != '' && !resetLCCMealFlag)
                BindFlexFields(document.getElementById('<%=hdnFlexInfo.ClientID %>').value, document.getElementById('<%=hdnFlexCount.ClientID %>').value,
                    'divFlex', 'divFlexFields', FPaxcount, CorpTravelReason); 

            /* To remove flex fields div wrapper class */
            if (document.getElementById('<%=hdnFlexInfo.ClientID %>').value == '')
                $('#divFlex0').removeClass('baggage-wrapper');

            /* To hide address fields for corporate, Added by Shiva 11-Oct-2019 : Disable only when Client (-1) or Vendor (-2) is not selected */
            <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate == "Y")
        {
            txtAddress1.Text = string.IsNullOrEmpty(agency.Address) ? "Shj" : agency.Address;//Handled by Shiva for corp bookings as adviced by Ziyad 19 May 2021
        %>
            $('.hideaddress').addClass('hidecntrldiv');
            <% if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.MemberType != CT.TicketReceipt.BusinessLayer.MemberType.TRAVELCORDINATOR)
        {%>
                $('.Corpdisable').addClass('disabled');
                $('.Corpdisable input[type="text"]').attr('disabled', true);
                $('.Corpdisable select').prop('disabled', true);
            <% }
        }%>
            
            /* To show seat selection option for available suppliers */            
        <%if (result != null &&
(result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp
|| (result.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo && request.InfantCount == 0)
|| (result.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp && request.InfantCount == 0)
|| result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp
|| result.ResultBookingSource == CT.BookingEngine.BookingSource.UAPI || result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia))
        { %>
                $('#divSelSeats').removeClass('hidecntrldiv');
            <% }%>

            /* To hide SSR fields for corporate */
            <%if (!IsSSRReq)
        { %>

                   $('.Corppolicy').addClass('hidecntrldiv');
            <% }%>

            /* Load G9 GST controls */
            AirArabiaGSTCntrls();

            <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate == "Y")
        {%>
            expandMoreDetails();
           <% }%>
        });

        /* To prevent double click of controls in the page */
        document.addEventListener( 'dblclick', function(event) {  
            event.preventDefault();  
            event.stopPropagation(); 
          },  true //capturing phase!!
        );

        /* To load G9 GST controls */
        function AirArabiaGSTCntrls() {

            <%if (result != null && result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia && result.Flights[0][0].Origin.CountryCode == "IN") %>
            <% {%>
                GetStatesInfo();
                if (IsEmpty(gstSCode))//For Corporate Agents this will be set.
                   document.getElementById('pnlGSTDetails').style.display = 'block';

                document.getElementById('<%=lblG9BaggageAlertLeadPax.ClientID%>').style.display = 'block';
           <% }
        else
        {%>

                document.getElementById('pnlGSTDetails').style.display = 'none';
                var G9Baggage = document.getElementById('<%=lblG9BaggageAlertLeadPax.ClientID%>');
                if (G9Baggage != null)
                    document.getElementById('<%=lblG9BaggageAlertLeadPax.ClientID%>').style.display = 'none';
            <% }%>

            <%if (result != null && result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia) %>
            <% {%>
                if (document.getElementById('pnlGSTDetails').style.display == 'none') {

                    var country = document.getElementById('ctl00_cphTransaction_ddlCountry');
                    var selectedCountryText = country.options[country.selectedIndex].text;
                    if (selectedCountryText == 'India') {
                        if (IsEmpty(gstSCode))//For Corporate Agents this will be set.
                            document.getElementById('pnlGSTDetails').style.display = 'block';
                        GetStatesInfo();
                    }
                    else 
                        document.getElementById('pnlGSTDetails').style.display = 'none';
                }
            <% }%>
        }

    </script>

    <script type="text/javascript">
        //Added by Lokesh on 28Mar2018 To get the GST State Id's
        //This is only for G9 source .
        //Only For Lead Pax.
        //If the customer is travelling from India only.
        var Ajax; //New Ajax object.
        var gstStateId;
        var gstSCode;
        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        function GetStatesInfo() {
            gstStateId = 'ctl00_cphTransaction_ddlGSTStateCode';
            var paramList = 'requestSource=getStates&id=' + gstStateId + '&countryCode=IN';
            var url = "CityAjax"; 
            Ajax.onreadystatechange = BindGSTStateId;
            Ajax.open('POST', url, false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); 
            Ajax.send(paramList);
            BindStateGSTCode();
       }
   //Added By Hari MAlla 2019-08-26. for defaultly select the gst state code based on agentid and source and appkey.
    function BindStateGSTCode()
    { 

        <%if (result != null && result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia) %>
        <% {%>  
        var nationality = $("#<%=ddlNationality.ClientID%>").val().toUpperCase();  
        var sources = {'AirArabia':'G9'}; // For multiple Sources. 
        var srcCode  = sources["<%= result.ResultBookingSource%>"] ;
        gstSCode = '<%=gstStateCode %>'.trim(); 
        if ( gstSCode != '' )
        {  
             $('#' + getElement('ddlGSTStateCode').id).select2('val', gstSCode);               
        }
        else if (gstSCode == '' && srcCode !=null && srcCode != undefined && nationality=="IN" )
        {      
            $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "PassengerDetails.aspx/GetStateGSTCode",
            data: "{'appkey':'"+srcCode+"_GST_STATECODE','source':'"+ <%=(Int32)result.ResultBookingSource%>+"'}",
            dataType: "json",
            async:false,
            success: function (data) { 
                    if (data.d !=null && data.d !=''){
                        $('#' + getElement('ddlGSTStateCode').id).select2('val', data.d);
                        if (data.d.length > 0) {
                            document.getElementById('pnlGSTDetails').style.display = 'none';
                            gstSCode = data.d;
                        }
                        else {
                            document.getElementById('pnlGSTDetails').style.display = 'block';
                        }
                        
                    }
                }
            });
        }    
        else
        {
            $('#' + getElement('ddlGSTStateCode').id).select2('val', "-1"); 
            document.getElementById('pnlGSTDetails').style.display = 'block';
        } 
    <%} %>
    }
        //Added by lokesh on 6-April-2018
        //This function displays the GST Input fields
        //If the lead pax country of residence is india.
        //If the result booking source is G9 Only
        function ShowHideGSTDetails(id) {

            try {

             <%if (result != null && result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia) %>
                <% {%>
                var country = document.getElementById(id);
                var selectedCountryText = country.options[country.selectedIndex].text;
                if (selectedCountryText == 'India') {
                    if (gstSCode !== undefined && gstSCode.length == 0)//For Corporate Agents this will be set.
                    {
                        document.getElementById('pnlGSTDetails').style.display = 'block';
                    }
                    GetStatesInfo();
                }
                else {
                    document.getElementById('pnlGSTDetails').style.display = 'none';
                    document.getElementById('<%=ddlGSTStateCode.ClientID %>').selectedIndex = -1;
                    document.getElementById('<%=hdnSelStateId.ClientID %>').value = '';

                }

            <% }%>

                <%else%>
                <% {%>
                document.getElementById('pnlGSTDetails').style.display = 'none';
            <% }%>
            }
            catch (exception) {
                var exp = exception;
            }
        }

        //Ajax call which brings complete state id's
        function BindGSTStateId(response) {
            if (Ajax.readyState == 4 && Ajax.status == 200 && Ajax.responseText.length > 0) {       
                        gstStateId = Ajax.responseText.split('#')[0];
                        var ddl = document.getElementById(gstStateId);
                        if (ddl != null) {
                            //ddl.options.length = 0;
                            //var el = document.createElement("option");
                            //el.textContent = "Select State Id";
                            //el.value = "-1";
                            //ddl.add(el, 0);
                            var values = Ajax.responseText.split('#')[1].split(',');
                            for (var i = 0; i < values.length; i++) {
                                var opt = values[i];
                                if (opt.length > 0 && opt.indexOf('|') > 0) {
                                    var el = document.createElement("option");
                                    el.textContent = opt.split('|')[0];
                                    el.value = opt.split('|')[1];
                                    ddl.appendChild(el);
                                }
                            }
                        }
                    }
         }
    </script>

   <!--Search Passenger-->
   <script>

        /* To set bootstrap drop down value based on display text/stored value */
        function Setddlval(id, val, type) {

            if (type == 'text') 
                $("#s2id_" + id).select2('val', $("#" + id + " option").filter(function () { return this.text == val.trim(); }).val());
            else
                $("#s2id_" + id).select2('val', val.trim());

            if (document.getElementById(id).value == '')
                $("#s2id_" + id).select2('val', $("#" + id + " option:first").val());
        }

       /* Function to bind the search passenger filters html and to show the pop up */
       function ShowSearhPassenger(paxindx) {

           /* To disable bootstrap modal close on escape button and pop up outside click */

           $("#SearchPassenger").modal({
                backdrop: 'static',
                keyboard: false
           });

           /* If previously opened pop up and the current pop up button click belongs to the same pax, 
            * it will avoid binding the html once again and just open the existing pop up */
           if (cindex == paxindx && document.getElementById('txtSearchEmpId') != null)
               $('#SearchPassenger').modal('show');
           else {

               cindex = paxindx;
               $('#SearchFilters').children().remove();
               $("#PassengerDetailsList").children().remove();
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchEmpId" class="form-control" placeholder="Please enter employee id" /></div>');
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchFirstName" class="form-control" placeholder="Please enter first name" /></div>');
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchLastName" class="form-control" placeholder="Please enter last name" /></div>');
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchEmail" class="form-control" placeholder="Please enter email" /></div>');

               /* If the current pop up was clicked earlier and has any filter options, same filters will assign to the filter controls and, 
                * will perform the search for the same filters */

               if (SearchFilter[cindex] != null && SearchFilter[cindex] != '') {
                   var Filters = SearchFilter[paxindx].split('|');
                   document.getElementById('txtSearchEmpId').value = Filters[0];
                   document.getElementById('txtSearchFirstName').value = Filters[1];
                   document.getElementById('txtSearchLastName').value = Filters[2];
                   document.getElementById('txtSearchEmail').value = Filters[3];
                   SearchPassenger();
               }

               $('#SearchPassenger').modal('show');
           }
       }


       var SearchFilter = new Array(10);
       var selctedprofiles = new Array(10);
       var cindex;
       var PaxProfilesData;
       var PaxProfilesFlexData;
       var AirlineCode = '';

       /* Added .1 sec timer to show processing div on search passengers results loading */
       function SearchPassenger() {
           document.getElementById('ctl00_upProgress').style.display = 'block';
           setTimeout(function(){ SearchPassengers(); document.getElementById('ctl00_upProgress').style.display ='none'; }, 100);            
       }

       /* Function to search passengers list with the given filters in the pop up */
       function SearchPassengers() {

           $("#PassengerDetailsList").children().remove();
           var EmployeeId = document.getElementById('txtSearchEmpId').value, FstName = document.getElementById('txtSearchFirstName').value,
               LstName = document.getElementById('txtSearchLastName').value; Email = document.getElementById('txtSearchEmail').value;
           AirlineCode = '<%=result.Flights[0][0].Airline%>';

           if (EmployeeId == '' && FstName == '' && LstName == '' && Email == '') {

               SearchFilter[cindex] = '';
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>Please enter any filter option with 3 characters</div>");
               return false;
           }

           if ((EmployeeId != '' && EmployeeId.length < 3) || (FstName != '' && FstName.length < 3) || (LstName != '' && LstName.length < 3) || (Email != '' && Email.length < 3)) {
               SearchFilter[cindex] = '';
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>Please enter minimum 3 characters in the filter</div>");
               return false;
           }

           SearchFilter[cindex] = EmployeeId + '|' + FstName + '|' + LstName + '|' + Email + '|' + AirlineCode;

           $.ajax({
                type: "POST",
                url: "PassengerDetails.aspx/GetPaxProfiles",
                contentType: "application/json; charset=utf-8",
                data: "{'sFilters':'" + SearchFilter[cindex] + "'}",
                dataType: "json",
                async: false,
                success: function (data) {
                    ShowPassengerDetail(data.d);
                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
            });
           
       }

       /* Function to bind passenger search results list to grid */
       function ShowPassengerDetail(results) {

           if (results == '' || results == null || results.length == 0 || results == 'undefined') {
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>No records found</div>");
               return false;
           }
           else {
               PaxProfilesData = JSON.parse(results[0]);
               PaxProfilesFlexData = JSON.parse(results[1]);
           }

           $("#PassengerDetailsList").append('<table id="tblPaxDetails" class="table b2b-corp-table"><tr><th><span>Select</span></th><th><span>Employee Code</span></th><th><span>First Name</span></th><th><span>Last Name</span></th><th><span>Email</span></th><th><span>Passport No</span></th></tr></table>');
           var hasdata = false;
           $.each(PaxProfilesData, function (key, prfdata) {

               if (selctedprofiles.indexOf(prfdata.ProfileId) == -1) {
                   hasdata = true;
                   $("#tblPaxDetails").append('<tr>' +
                       
                       '<td> <span class="glyphicon glyphicon-plus-sign" id="prof_' + prfdata.ProfileId + '" onclick="SelectPaxData(this,' + prfdata.ProfileId + ')" /> </td>' +
                       '<td class="text-left"> <span>' + prfdata.EmployeeId + '</span></td> <td class="text-left"><span>' + prfdata.FName + '</span></td>' +
                       '<td class="text-left"> <span>' + prfdata.LName + '</span></td> <td class="text-left"><span>' + prfdata.Email + '</span></td>' +
                       '<td class="text-left"> <span>' + prfdata.PassportNo + '</span></td>' +
                       '</tr >');
               }
           });

           if (!hasdata) {
               $("#PassengerDetailsList").children().remove();
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>No records found</div>");
               return false;
           }

       }

       /* Function to assign flex field controls data */
       function AssignFlexDataAdPax1(cntrltype, cntrlid, flexData) {

            if (cntrltype == "T")
                document.getElementById("ctl00_cphTransaction_txtFlex" + cntrlid).value = flexData;
            else if (cntrltype == "L") {
                flexData = flexData == '' ? '-1' : flexData;
                $("#s2id_ctl00_cphTransaction_ddlFlex" + cntrlid).select2("val", flexData);
            }
            else{
                var d = flexData.split(' ')[0];
                var date = d.split('/');
                if (date.length == 3) {
                    $("#s2id_ctl00_cphTransaction_ddlMonth" + cntrlid).select2("val", date[0]);
                    $("#s2id_ctl00_cphTransaction_ddlDay" + cntrlid).select2("val", date[1]);
                    $("#s2id_ctl00_cphTransaction_ddlYear" + cntrlid).select2("val", date[2]);
                }
                else {
                    $("#s2id_ctl00_cphTransaction_ddlMonth" + cntrlid).select2("val", '-1');
                    $("#s2id_ctl00_cphTransaction_ddlDay" + cntrlid).select2("val", '-1');
                    $("#s2id_ctl00_cphTransaction_ddlYear" + cntrlid).select2("val", '-1');
                }
            }
       }

       /* Function to assign flex field controls data for additional pax */
       function AssignFlexData1(cntrltype, cntrlid, flexData, paxid) {

            if (cntrltype == "T" && document.getElementById('txt' + paxid + 'Flex' + cntrlid) != null)
                document.getElementById('txt' + paxid + 'Flex' + cntrlid).value = flexData;
            else if (cntrltype == "L" && document.getElementById('ddl' + paxid + 'Flex' + cntrlid) != null) {                
                Setddlval('ddl' + paxid + 'Flex' + cntrlid, flexData, '');
            }
            else if (cntrltype == "D" && document.getElementById('ddldt' + paxid + 'Flex' + cntrlid) != null) {
                var d = flexData.split(' ')[0];
                var date = d.split('/');
                if (date.length == 3) {
                    Setddlval('ddldt' + paxid + 'Flex' + cntrlid, date[0], '');
                    Setddlval('ddlmn' + paxid + 'Flex' + cntrlid, date[1], '');
                    Setddlval('ddlyr' + paxid + 'Flex' + cntrlid, date[2], '');
                }
                else {
                    Setddlval('ddldt' + paxid + 'Flex' + cntrlid, '', '');
                    Setddlval('ddlmn' + paxid + 'Flex' + cntrlid, '', '');
                    Setddlval('ddlyr' + paxid + 'Flex' + cntrlid, '', '');
                }
            }
       }

       /* Function to assign the selected profile data to screen passenger details window */
       function SelectPaxData(event, PrfId) {

           try {
               
               var selpaxdtls = '', tempvar = '', drpdndefval = '-1';//, titles = '|Mr|Ms|Mrs|Dr|';;
               var isAdPax = cindex > 0;
               var paxindx = cindex - 1;
               selctedprofiles[cindex] = PrfId;

               $.each(PaxProfilesData, function (key, prfdata) {

                   if (prfdata.ProfileId == PrfId)
                       selpaxdtls = prfdata;
               });

               if (isAdPax) {

                   tempvar = (selpaxdtls.Title == null || selpaxdtls.Title == '') ? '-1' : selpaxdtls.Title;
                   Setddlval('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPaxTitle', tempvar, '');
                   //$('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPaxTitle').select2("val", tempvar);

                   document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtPaxFName').value = selpaxdtls.FName;
                   document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtPaxLName').value = selpaxdtls.LName;

                   if (selpaxdtls.DOBDate != null && selpaxdtls.DOBDate != '') {
                       $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlDay').select2("val", selpaxdtls.DOBDate);
                       $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlMonth').select2("val", selpaxdtls.DOBMonth);
                       $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlYear').select2("val", selpaxdtls.DOBYear);
                   }
                   else {
                       $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlDay').select2("val", drpdndefval);
                       $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlMonth').select2("val", drpdndefval);
                       $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlYear').select2("val", drpdndefval);
                   }

                   tempvar = (selpaxdtls.Gender == 'M' || selpaxdtls.Gender == '1') ? '1' : (selpaxdtls.Gender == 'F' || selpaxdtls.Gender == '2') ? '2' : '-1';
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlGender').select2("val", tempvar);
                   document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtPassportNo').value = selpaxdtls.PassportNo;

                   if (selpaxdtls.PexpDate != null && selpaxdtls.PexpDate != '') {
                       $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPEDay').select2("val", selpaxdtls.PexpDate);
                       $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPEMonth').select2("val", selpaxdtls.PexpMonth);
                       $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPEYear').select2("val", selpaxdtls.PexpYear);
                   }
                   else {
                       $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPEDay').select2("val", drpdndefval);
                       $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPEMonth').select2("val", drpdndefval);
                       $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlPEYear').select2("val", drpdndefval);
                   }

                   tempvar = selpaxdtls.PassportCOI != null && selpaxdtls.PassportCOI != '' ? selpaxdtls.PassportCOI : '-1';
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlCountry').select2("val", tempvar);

                   tempvar = selpaxdtls.NationalityCode != null && selpaxdtls.NationalityCode != '' ? selpaxdtls.NationalityCode : '-1';
                   $('#ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_ddlNationality').select2("val", tempvar);

                   document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtAirline').value = selpaxdtls.FFNum == null ? '' : AirlineCode;
                   document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtFlight').value = selpaxdtls.FFNum;

                   document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtSeatPref').value = selpaxdtls.SeatPreference;
                   document.getElementById('divSeatPref' + paxindx).style.display =
                       (selpaxdtls.SeatPreference != null && selpaxdtls.SeatPreference != '') ? 'block' : 'none';

                   document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_txtMealPref').value = selpaxdtls.MealRequest;
                   document.getElementById('divMealPref' + paxindx).style.display =
                       (selpaxdtls.MealRequest != null && selpaxdtls.MealRequest != '') ? 'block' : 'none';

                   document.getElementById('ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_hdnProfileId').value = PrfId;

                   $('label[for="ctl00_cphTransaction_dlAdditionalPax_ctl0' + paxindx + '_chkAddPax"]').html('Update Passenger');

                   var ancAPI = document.getElementById('ancAPI' + paxindx);
                   if (ancAPI.attributes["aria-expanded"].value == 'false')
                       ancAPI.click();

               }
               else {

                   tempvar = (selpaxdtls.Title == null || selpaxdtls.Title == '') ? '-1' : selpaxdtls.Title;
                   Setddlval(document.getElementById('<%=ddlTitle.ClientID %>').id, tempvar, '');
                   //$('#s2id_ctl00_cphTransaction_ddlTitle').select2('val', tempvar);

                   document.getElementById('ctl00_cphTransaction_txtPaxFName').value = selpaxdtls.FName;
                   document.getElementById('ctl00_cphTransaction_txtPaxLName').value = selpaxdtls.LName;
                   document.getElementById('ctl00_cphTransaction_txtEmail').value = selpaxdtls.Email;

                   if (selpaxdtls.Mobilephone != null && selpaxdtls.Mobilephone != '') {
                       var mobile = selpaxdtls.Mobilephone.split('-');

                       document.getElementById('ctl00_cphTransaction_txtMobileCountryCode').value = mobile.length > 1 ? mobile[0] : '';
                       document.getElementById('ctl00_cphTransaction_txtMobileNo').value = mobile.length > 1 ? mobile[1] : mobile[0];
                   }

                   if (selpaxdtls.DOBDate != null && selpaxdtls.DOBDate != '') {
                       $('#s2id_ctl00_cphTransaction_ddlDay').select2('val', selpaxdtls.DOBDate);
                       $('#s2id_ctl00_cphTransaction_ddlMonth').select2('val', selpaxdtls.DOBMonth);
                       $('#s2id_ctl00_cphTransaction_ddlYear').select2('val', selpaxdtls.DOBYear);
                   }
                   else {
                       $('#s2id_ctl00_cphTransaction_ddlDay').select2('val', drpdndefval);
                       $('#s2id_ctl00_cphTransaction_ddlMonth').select2('val', drpdndefval);
                       $('#s2id_ctl00_cphTransaction_ddlYear').select2('val', drpdndefval);
                   }

                   tempvar = (selpaxdtls.Gender == 'M' || selpaxdtls.Gender == '1') ? '1' : (selpaxdtls.Gender == 'F' || selpaxdtls.Gender == '2') ? '2' : '-1';
                   $('#s2id_ctl00_cphTransaction_ddlGender').select2('val', tempvar);
                   document.getElementById('ctl00_cphTransaction_txtPassportNo').value = selpaxdtls.PassportNo;

                   if (selpaxdtls.PexpDate != null && selpaxdtls.PexpDate != '') {
                       $('#s2id_ctl00_cphTransaction_ddlPEDay').select2('val', selpaxdtls.PexpDate);
                       $('#s2id_ctl00_cphTransaction_ddlPEMonth').select2('val', selpaxdtls.PexpMonth);
                       $('#s2id_ctl00_cphTransaction_ddlPEYear').select2('val', selpaxdtls.PexpYear);
                   }
                   else {
                       $('#s2id_ctl00_cphTransaction_ddlPEDay').select2('val', drpdndefval);
                       $('#s2id_ctl00_cphTransaction_ddlPEMonth').select2('val', drpdndefval);
                       $('#s2id_ctl00_cphTransaction_ddlPEYear').select2('val', drpdndefval);
                   }

                   tempvar = selpaxdtls.PassportCOI != null && selpaxdtls.PassportCOI != '' ? selpaxdtls.PassportCOI : '-1';
                   $('#s2id_ctl00_cphTransaction_ddlCountry').select2('val', tempvar);

                   ShowHideGSTDetails(document.getElementById('<%=ddlCountry.ClientID%>').id);

                   tempvar = selpaxdtls.NationalityCode != null && selpaxdtls.NationalityCode != '' ? selpaxdtls.NationalityCode : '-1';
                   $('#s2id_ctl00_cphTransaction_ddlNationality').select2('val', selpaxdtls.NationalityCode);

                   document.getElementById('ctl00_cphTransaction_txtAddress1').value = selpaxdtls.Address1;
                   document.getElementById('ctl00_cphTransaction_txtAddress2').value = selpaxdtls.Address2;
                   document.getElementById('ctl00_cphTransaction_txtAirline').value = selpaxdtls.FFNum == null ? '' : AirlineCode;
                   document.getElementById('ctl00_cphTransaction_txtFlight').value = selpaxdtls.FFNum;

                   document.getElementById('ctl00_cphTransaction_txtSeatPref').value = selpaxdtls.SeatPreference;
                   document.getElementById('divSeatPref').style.display =
                       (selpaxdtls.SeatPreference != null && selpaxdtls.SeatPreference != '') ? 'block' : 'none';

                   document.getElementById('ctl00_cphTransaction_txtMealPref').value = selpaxdtls.MealRequest;
                   document.getElementById('divMealPref').style.display =
                       (selpaxdtls.MealRequest != null && selpaxdtls.MealRequest != '') ? 'block' : 'none';

                   document.getElementById('ctl00_cphTransaction_hdnProfileId').value = PrfId;

                   $('label[for="ctl00_cphTransaction_chkAddPax"]').html('Update Passenger');
                   var ancAPI = document.getElementById('ancAPI');
                   if (ancAPI.attributes["aria-expanded"].value == 'false')
                       ancAPI.click();
               }           

               if (!IsEmpty(document.getElementById('<%=hdnFlexInfo.ClientID%>').value))
                   SetPaxFlexData(cindex, PaxProfilesFlexData, PrfId);

               if (1 == 2 && FlexInfo != null && FlexInfo != 'undefined' && FlexInfo != '' && FlexInfo.length > 0) {

                   paxindx = Math.ceil(paxindx) + 1;
                   var flexids = []; 

                   $.each(FlexInfo, function (key, col) {

                       var flxdata = '';
                       flexids.push('|' + col.flexId + col.flexControl + '|');

                       if (col.FLEXVALID != '' && (flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 || flexids.indexOf('|' + col.FLEXVALID + 'L' + '|') != -1)) {

                            var dcntid = flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 ? 'txt' + paxindx + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') :
                                'ddl' + paxindx + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'L' + '|');
                            var curflex = col.flexControl == 'L' ? 'ddl' : 'txt';
                        
                            BindFlexdynamicddl(document.getElementById(dcntid).value, curflex + paxindx + 'Flex' + key, col.flexSqlQuery, curflex); 
                       }

                       $.each(PaxProfilesFlexData, function (sno, prfflxdata) {
                           if (prfflxdata.ProfileId == PrfId && col.flexId == prfflxdata.FlexId)
                               flxdata = prfflxdata.FlexData;
                       });                   

                       if (flxdata != null && flxdata != 'undefined' && flxdata != '')
                           AssignFlexData(col.flexControl, key, (flxdata != '' && flxdata != null ? flxdata : ''), paxindx);
                   });    
               }    
           }
           catch (exception) {
               var exp = exception;
           }
           $('#SearchPassenger').modal('hide');
       }

   </script>
    
    <asp:HiddenField ID="hdfAsvAmount" runat="server" />
    <asp:HiddenField ID="hdfAsvType" runat="server" />
    <asp:HiddenField ID="hdfAsvElement" runat="server" />
    <asp:HiddenField ID="hdfCurrentDate" runat="server" />
    <asp:HiddenField ID="hdfTaxMarkUp" runat="server" />
    <asp:HiddenField ID="hdfOtherCharges" runat="server" Value="0" />
    <asp:HiddenField ID="hdfFareAction" runat="server" Value="Get" />
    <asp:HiddenField ID="hdnSelStateId" runat="server" />
    <!--Meal Selection Variables Should Always Be Here-->
<asp:HiddenField ID="hdnInMealSelection" runat="server" Value="0" />
<asp:HiddenField ID="hdnOutMealSelection" runat="server" Value="0" />
<asp:HiddenField runat="server" ID="hdnFlexInfo"></asp:HiddenField>
<asp:HiddenField runat="server" ID="hdnPaxFlexInfo"></asp:HiddenField>    
    <asp:HiddenField runat="server" ID="hdnPaxSubmit" Value="false" />
    <asp:HiddenField runat="server" ID="hdnG9BundleSegments" Value="" />
    <asp:HiddenField runat="server" ID="hdnValidateG9Seat" Value="YES" />
    <div>
        <div class="paddingbot_10">
            <asp:Label ID="lblTimeChanged" runat="server" Text="Note: Itinerary details have been changed."
                ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label></div>
        <div>
            <div class="col-md-3 padding-0 bg_white">
                <div class="ns-h3">
                    Onward Flight summary</div>
                <table class="table901" id="tblOnwardFlightDetails" runat="server" width="100%" border="0"
                    cellspacing="0" cellpadding="0">
                </table>
                <%if (result != null && result.Flights.Length > 1)
                    { %>
                <div class="ns-h3">
                    Return Flight summary</div>
                <table class="table901" id="tblReturnFlightDetails" runat="server" width="100%" border="0"
                    cellspacing="0" cellpadding="0">
                </table>
                <%} %>
                <div class="ns-h3">
                    Booking Review</div>
                <div>
                    <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="gray-smlheading">
                                <b>Product</b>
                            </td>
                            <td class="gray-smlheading">
                                <b>Price</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AirFare
                                <%--<asp:Label ID="lblPaxCount" runat="server" Text=""></asp:Label>--%>
                            </td>
                            <td valign="bottom">
                                <%=agency.AgentCurrency %>
                                <asp:TextBox ID="lblBaseFare" CssClass="txt24" Text="0.00" Enabled="false" BackColor="White" ForeColor="Black"
                                    BorderColor="White" Style="text-align: right" BorderStyle="None" BorderWidth="0"
                                    runat="server" ReadOnly="true"></asp:TextBox>
                                <%--<asp:Label ID="lblBaseFare" runat="server" Text=""></asp:Label>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Taxes&Fees
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblTax" CssClass="txt24" Text="0.00" Enabled="false" BackColor="White"
                                    ForeColor="Black" BorderColor="White" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                                <%--<asp:Label ID="lblTax" runat="server" Text=""></asp:Label> --%>
                            </td>
                        </tr>
                        <%if (Discount > 0)
                            { %>
                        <tr>
                            <td>
                                Discount
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblDiscount" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Text="0.00" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>                                
                            </td>
                        </tr>
                        <%} %>
                        <tr>
                            <td>
                                Total
                                <%--<asp:Label ID="lblPaxCount1" runat="server" Text=""></asp:Label>--%>
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblTotalFare" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Text="0.00" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server"></asp:TextBox>
                                <%--<asp:Label ID="lblTotalFare" runat="server" Text=""></asp:Label>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Baggage
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblBaggageAmt" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Text="0.00" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                                <%--<asp:Label ID="lblBaggageAmt" runat="server" Text=""></asp:Label>--%>
                            </td>
                        </tr>

                        <%if (result != null && (result.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia)) %>
                        <%{ %>
                        <tr>
                            <td>
                                Meal
                            </td>
                            <td>
                                <%=agency.AgentCurrency %><asp:TextBox ID="lblMealAmt" BackColor="White" ForeColor="Black"
                                    BorderColor="White" CssClass="txt24" Text="0.00" Enabled="false" Style="text-align: right" BorderStyle="None"
                                    BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                                
                            </td>
                        </tr>
                        <%} %>
                        
                        <tr>
                            <td>
                                <strong>Total</strong>
                            </td>
                            <td>
                                <strong>
                                    <%=agency.AgentCurrency %><asp:TextBox ID="lblTotalPrice" BackColor="White" ForeColor="Black"
                                        BorderColor="White" Style="text-align: right" CssClass="txt24" Text="0.00" Enabled="false" BorderStyle="None"
                                        BorderWidth="0" runat="server" ReadOnly="true"></asp:TextBox>
                                    <%--<asp:Label ID="lblTotalPrice" runat="server" Text=""></asp:Label>--%></strong>
                            </td>
                        </tr>
                    </table>
                </div>
                <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate != "Y")
                    { %>
                <div class="ns-h3">
                    Markup</div>
                <div style="padding: 5px;">
                    <table width="100%" border="0">
                        <tr>
                            <td style="padding-right: 2px;" valign="top">
                                Add Markup on
                                <%--<asp:CheckBox ID="chkFare" runat="server" Text="Fare" />
                                        <asp:CheckBox ID="ChkTax" runat="server" Text="Tax" />--%>
                                <asp:RadioButton ID="rbtnFare" runat="server" Text="Fare" GroupName="markup" Checked="true" />
                                <asp:RadioButton ID="rbtnTax" runat="server" Text="Tax" GroupName="markup" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="70%">
                                    <tr>
                                        <td style="padding-right: 2px;">
                                            <strong>Add Markup</strong>
                                        </td>
                                        <td style="padding-right: 2px;">
                                            <label>
                                                <asp:TextBox ID="txtMarkup" runat="server" CssClass="form-control" MaxLength="2" Width="70px" onpaste="return false;"
                                                    ondrop="return false;" onkeypress="return isNumber(event)" onfocus="Check(this.id);"
                                                    onBlur="Set(this.id);" Text="0"></asp:TextBox>
                                            </label>
                                        </td>
                                        <td>
                                            <label>
                                                <asp:RadioButton ID="rbtnPercent" runat="server" Text="P" GroupName="cost" Checked="true" onchange="return CheckVal();" />
                                            </label>
                                        </td>
                                        <td align="right">
                                            <label>
                                                <asp:RadioButton ID="rbtnFixed" runat="server" Text="F" GroupName="cost" onchange="return CheckVal();" />
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class=" pad_14">
                                    <asp:Button ID="imgBtnCost" runat="server" CssClass="button-normal" Text="Recalculate Cost"
                                        OnClientClick="return RecalculateCost();" />
                                </label>
                            </td>
                        </tr>
                    </table>
                </div>
                <%} %>
            </div>
            <div class="col-md-9">
                <div class="bg_white" style="border: solid 1px #ccc; border-top: 0px; margin-bottom: 10px;
                    padding-bottom: 10px;">
                    <div class="ns-h3">
                        Please enter Passenger(s) Details</div>
                    <div class="subgray-header">
                        <div class="row">
                            <div class="col-xs-12 col-lg-12">
                                    <b class="passenger-label">Passenger 1 - Adult</b>
                                
                                  
                                    <a id="ancAPI" class="btn btn-primary advPax-collapse-btn float-right" role="button" data-toggle="collapse" href="#paggengerDetailsCollapse-1"
                                        aria-expanded="false" aria-controls="collapseExample">Add API's </a>
                            

                                        <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate != "Y")
                                            {%>  
                                             <button type="button" class="btn btn-link float-right" onclick="ShowSearhPassenger(0)">Search Passenger <span class="glyphicon glyphicon-search"></span></button>
                                          <%} %>
                                 <asp:CheckBox runat="server" ID="chksendmail" Checked="true" Text="send Email" CssClass="float-right custom-checkbox-table" Style="display:inline-block;"/>

                                    
                           </div>
                                  
                        </div>
                       
                        

                    </div>
                    <div class="pax-details-wrap">

                             
                        <div class="col-xs-12 col-lg-12">                        
                         	
							<asp:CheckBox runat="server" ID="chkAddPax" CssClass="custom-checkbox-style dark chkbox-addpasngr" Text="Add Passenger"></asp:CheckBox> 
                          
                            <div class="form-horizontal">
                                <div class="form-group Corpdisable">
                                    <div class="col-md-3 col-lg-2">
                                        <label>
                                            <strong>Title<span class="red_span">*</span></strong>
                                        </label>
                                        <asp:DropDownList ID="ddlTitle" runat="server" CssClass="form-control" onchange="TitleChanged(this.id,'-1');">
                                            <asp:ListItem Selected="True" Value="-1">Select</asp:ListItem>
                                            <asp:ListItem Value="Mr">Mr.</asp:ListItem>
                                            <asp:ListItem Value="Ms">Ms.</asp:ListItem>
                                            <asp:ListItem Value="Mrs">Mrs.</asp:ListItem>
                                            <asp:ListItem Value="Dr">Dr.</asp:ListItem>
                                        </asp:DropDownList>
                                        <b class="red_span" style="display: none" id="title"></b>
                                    </div>
                                    <div class="col-md-3">
                                        <label>
                                            <strong>First Name<span class="red_span">*</span></strong></label>
                                        <asp:TextBox ID="txtPaxFName" runat="server" CssClass="form-control" onkeypress="return IsAlphaNumeric(event);"
                                            ondrop="return false;" onpaste="return true;"></asp:TextBox>
                                        <b class="red_span" style="display: none" id="fname"></b>
                                    </div>
                                    <div class="col-md-3">
                                        <label>
                                            <strong>Last Name<span class="red_span">*</span></strong></label>
                                        <asp:TextBox ID="txtPaxLName" runat="server" CssClass="form-control" onkeypress="return IsAlphaNumeric(event);"
                                            ondrop="return false;" onpaste="return true;"></asp:TextBox>
                                        <b class="red_span" style="display: none" id="lname"></b>
                                    </div>
                          
                                    <div class="col-md-3">
                                        <label>
                                            <strong>Email
                                                <%if (EmailRequired)
                                                    { %><span class="red_span">*</span><%} %></strong></label>
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" CausesValidation="True"
                                            ValidationGroup="email"></asp:TextBox>
                                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email format"
                                            ControlToValidate="txtEmail" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="email" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                                        <b class="red_span" style="display: none" id="email"></b>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label>
                                            <strong>Mobile <span class="red_span">*</span></strong>
                                        </label>
                                        <div class="input-group tel-input-addon">
                                            <div class="input-group-addon">
                                                <asp:TextBox ID="txtMobileCountryCode" runat="server" CssClass="form-control pull-left"
                                                    onkeypress="return isNumber(event)" MaxLength="3"></asp:TextBox>
                                            </div>
                                            <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control pull-left" onkeypress="return isNumber(event)"
                                                onpaste="return checkingNumber(this.id);"></asp:TextBox>
                                            <b class="red_span" style="display: none" id="mobileCountryCode"></b><b class="red_span"
                                                style="display: none" id="mobile"></b>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            <strong>D.O.B.
                                                <%if (DOBRequired)
                                                    { %><span class="red_span">*</span><%} %>
                                            </strong>
                                        </label>
                                        <div class="row no-gutter">
                                            <div class="col-xs-4">
                                                <asp:DropDownList ID="ddlDay" runat="server" AppendDataBoundItems="True" CssClass=" form-control pull-left">
                                                    <asp:ListItem Selected="True" Value="-1">Day</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-xs-4">
                                                <asp:DropDownList ID="ddlMonth" runat="server" AppendDataBoundItems="True" CssClass=" form-control pull-left">
                                                    <asp:ListItem Selected="True" Value="-1">Month</asp:ListItem>
                                                    <asp:ListItem Value="01">Jan</asp:ListItem>
                                                    <asp:ListItem Value="02">Feb</asp:ListItem>
                                                    <asp:ListItem Value="03">Mar</asp:ListItem>
                                                    <asp:ListItem Value="04">Apr</asp:ListItem>
                                                    <asp:ListItem Value="05">May</asp:ListItem>
                                                    <asp:ListItem Value="06">Jun</asp:ListItem>
                                                    <asp:ListItem Value="07">Jul</asp:ListItem>
                                                    <asp:ListItem Value="08">Aug</asp:ListItem>
                                                    <asp:ListItem Value="09">Sep</asp:ListItem>
                                                    <asp:ListItem Value="10">Oct</asp:ListItem>
                                                    <asp:ListItem Value="11">Nov</asp:ListItem>
                                                    <asp:ListItem Value="12">Dec</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-xs-4">
                                                <asp:DropDownList ID="ddlYear" runat="server" AppendDataBoundItems="True" CssClass=" form-control pull-left">
                                                    <asp:ListItem Selected="True" Value="-1">Year</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <b class="red_span" style="display: none" id="dobDay"></b><b class="red_span" style="display: none"
                                                id="dobMonth"></b><b class="red_span" style="display: none" id="dobYear"></b>
                                        </div>
                                    </div>
                                     
                                     <div class="col-md-3">
                                        <label>
                                        <strong>Destination Phone</strong>
                                        </label>
                                        <div class="input-group tel-input-addon">
                                            <div class="input-group-addon">
                                                <asp:TextBox ID="txtDestCntCode" runat="server" CssClass="form-control pull-left"
                                                    onkeypress="return isNumber(event)" MaxLength="3"></asp:TextBox>
                                            </div>
                                            <asp:TextBox ID="txtDestPhoneNo" runat="server" CssClass="form-control pull-left" onkeypress="return isNumber(event)"
                                                onpaste="return checkingNumber(this.id);"></asp:TextBox>
                                          
                                        </div>
                                    </div>
                                        
                                  
                                        



                                </div>
                                    <!--Added by lokesh on 27-Mar-2018 -->
                                    <!--Applicable to only AirArabiaSource and also for only Lead PAX -->
                                    <!-- Fields should be displayed only for the customers travelling from INDIA" -->
                                    <!--Capture GSTIN  number from customer  -->

                                
                                <div class="form-group Corpdisable" style="display:none;" id="pnlGSTDetails">
                                    <div class="col-md-3">
                                        <label>
                                            <strong>State Code<span class="red_span">*</span>
                                                </strong></label>
                                          <div class="row no-gutter">
                                                <div class="col-xs-6">
                                       <asp:DropDownList runat="server" ID="ddlGSTStateCode">
                                            <asp:ListItem Selected="True" Value="-1">Select State Code</asp:ListItem>
                                            
                                       </asp:DropDownList>
                                                    <span class="red_span" id="valGSTStateCode" style="display:none;"></span>
                                                    </div>
                                              </div>
                                        
                                        <div class="clearfix">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label>
                                            <strong>Tax Registration Number
                                                </strong></label>
                                     <asp:TextBox MaxLength="50" CssClass="form-control pull-left" runat="server" ID="txtGSTRegNum"></asp:TextBox>
                                        <span class="red_span" id="valGSTRegNum" style="display:none;"></span>
                           
                                        <div class="clearfix">
                                        </div>
                                    </div>

                                </div>

                                <!-- Added by lokesh on 21-Aug-2018-->
                                <!-- Applicable to only spice jet source-->
                                <!--Fields should be displayed only for the customers travelling from INDIA-->
                                <!--Capture TaxRegNumber number from customer  -->

                                <%if (requiredGSTForSG_6E && result != null && (result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || result.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp)) %>
                                <%{ %>
                                <div class="form-group Corpdisable"  id="pnlGSTDetails_SG">

                                      <div class="col-md-3">
                                        <label>
                                            <strong>Tax Registration Number
                                                </strong></label>
                                     <asp:TextBox MaxLength="15"  CssClass="form-control pull-left" runat="server" ID="txtTaxRegNo_SG_GST"></asp:TextBox>
                                       
                           
                                        <div class="clearfix">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label>
                                            <strong>Company Name
                                                </strong></label>
                                     <asp:TextBox  CssClass="form-control pull-left" runat="server" ID="txtCompanyName_SG_GST"></asp:TextBox>
                                     <span class="red_span" id="gstCompanyName_SG" style="display: none;"></span> 
                                        <div class="clearfix">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label>
                                            <strong>Official Email
                                                </strong></label>
                                     <asp:TextBox  CssClass="form-control pull-left" runat="server" ID="txtGSTOfficialEmail"></asp:TextBox>
                                     <span class="red_span" id="gstOfficialEmail_SG" style="display: none;"></span> 
                                        <div class="clearfix">
                                        </div>
                                    </div>

                                </div>
                                <%} %>

                                <!--GST input fields for TBOAir added by shiva-->
                                 <div class="form-group Corpdisable" style="display:none" id="divGSTFields">
                                     <div class="col-md-4">
                                         <label>
                                             <strong>GST Company Address
                                                 <%if (result.IsGSTMandatory || requiredGSTForUAPI)
                                                     { %>
                                                 <span class="red_span">*</span>
                                                 <%} %>
                                             </strong>
                                         </label>
                                         <asp:TextBox ID="txtGSTCompanyAddress" runat="server" CssClass="form-control"></asp:TextBox>
                                         <span class="red_span" id="gstAddress" style="display: none;"></span>
                                     </div>
                                     <div class="col-md-4">
                                         <label>
                                             <strong>GST Company Contact Number
                                                 <%if (result.IsGSTMandatory || requiredGSTForUAPI)
                                                     { %>
                                                 <span class="red_span">*</span>
                                                 <%} %>
                                             </strong>
                                         </label>
                                         <asp:TextBox ID="txtGSTContactNumber" runat="server" CssClass="form-control"></asp:TextBox>
                                         <span class="red_span" id="gstContact" style="display: none;"></span>
                                     </div>
                                     <%if (result.IsGSTMandatory || requiredGSTForUAPI)
                                         { %>
                                     <div class="col-md-4">
                                         <label>
                                             <strong>GST Company Name
                                                 <%if (result.IsGSTMandatory || requiredGSTForUAPI)
                                                     { %>
                                                 <span class="red_span">*</span>
                                                 <%} %>
                                             </strong>
                                         </label>
                                         <asp:TextBox ID="txtGSTCompanyName" runat="server" CssClass="form-control"></asp:TextBox>
                                         <span class="red_span" id="gstName" style="display: none;"></span>
                                     </div>
                                     <%} %>
                                     <div class="col-md-4">
                                         <label>
                                             <strong>GST Number
                                                 <%if (result.IsGSTMandatory || requiredGSTForUAPI)
                                                     { %>
                                                 <span class="red_span">*</span>
                                                 <%} %>
                                             </strong>
                                         </label>
                                         <asp:TextBox ID="txtGSTNumber" runat="server" CssClass="form-control"></asp:TextBox>
                                         <span class="red_span" id="gstNumber" style="display: none;"></span>
                                     </div>
                                     <div class="col-md-4">
                                         <label>
                                             <strong>GST Company Email
                                                 <%if (result.IsGSTMandatory || requiredGSTForUAPI)
                                                     { %>
                                                 <span class="red_span">*</span>
                                                 <%} %>
                                             </strong>
                                         </label>
                                         <asp:TextBox ID="txtGSTCompanyEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                         <span class="red_span" id="gstEmail" style="display: none;"></span>
                                     </div>
                                 </div>                
                                    <asp:HiddenField ID="hdnFlexCount" runat="server" Value="0" />
                                 <div class="form-group baggage-wrapper col-xs-12 col-lg-12 Corppolicy" runat="server" id="lblOnBagWrapper" visible="false">
                                           
                                            <asp:Label ID="lblBaggage" runat="server" Text="" Font-Bold="true"  Visible="false"></asp:Label>



                                  
                                     <!-- Added by lokesh on 29-Mar-2018 for G9 Source only if the customer is travelling from India -->
                                     
                                      <asp:Label runat="server" CssClass="red_span" style="display:none" ID="lblG9BaggageAlertLeadPax">(Excluding SGST & GST Tax Components)</asp:Label>
                                     
                                            <div class="row">
                                                <%if (result != null && (result.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp))%>
                                                <%{ %>
                                              
                                              
                                              <div id="lccLeadPaxBaggagelbl" runat="server" class="col-md-12">  <span style="color:black;font-weight: bold;"> Select Baggage</span></div> 



                                                <%} %>
                                                
                                                
                                                 
                                                 
                                                <%--<%if (result != null && (result.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp)) %>--%>
                                               



                                                 <div class="col-md-12"> 
                                                 
                                                 
                                                   <div class="row"> 


                                             <div runat="server" class="col-md-6" id="leadPaxOnBagDiv">

                                                <div>  </div>



<div>
 Onward: <asp:Label ID="lblOnBag" runat="server" CssClass="sm-text-baggage d-inline" Text="" Visible="false"></asp:Label> </div>

<div class="input-group">

<asp:DropDownList ID="ddlOnwardBaggage" Style="margin-bottom: 0;" CssClass="form-control pull-left baggage-input-ctrl"
runat="server" Visible="false" onchange="CalculateBaggagePrice()">
</asp:DropDownList>

<div class="input-group-text">
  
    <asp:TextBox ID="lblOutPrice" Text="0.00" Enabled="false" style="width:50px"  BorderStyle="None" BorderWidth="0" runat="server"></asp:TextBox>

    
  

  </div>
      <asp:HiddenField ID="hdnOutBagSelection" runat="server" Value="0" />
    
</div>
 <b class="red_span" style="display: none" id="errOnWardLeadPaxBaggage"></b>




                                                  
                                                    



                                                   
                                                </div>




                                             <div class="col-md-6" runat="server" id="leadPaxRetBagDiv">
                                                

                                                <div>  </div>
                                                   
                                                   



                          <%if(request.Type == CT.BookingEngine.SearchType.Return)
                                         { %>
<div> Return: <asp:Label ID="lblInBag" runat="server" CssClass="sm-text-baggage d-inline" Text="" Visible="false"></asp:Label> </div>

 

  <%} %>  


<div class="input-group">

                                   


           <%if(request.Type == CT.BookingEngine.SearchType.Return)
                                         { %>    

<asp:DropDownList ID="ddlInwardBaggage" Style="margin-bottom: 0;" CssClass="form-control pull-left "
                                                            runat="server" Visible="false" onchange="CalculateBaggagePrice()">
                                                        </asp:DropDownList>



<div class="input-group-text">

    
    <asp:TextBox style="width:50px" ID="lblInPrice" Text="0.00" Enabled="false" BorderStyle="None" BorderWidth="0" runat="server"></asp:TextBox>


    
     
  </div>

      <asp:HiddenField ID="hdnInBagSelection" runat="server" Value="0" />
      

<%} %>  

</div>
<b class="red_span" style="display: none" id="errReturnLeadPaxBaggage"></b>




                                                   
                                                   
                                                    



                                                    
                                                </div>


                                                   </div>
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 <div class="row"> 


                                                                  
                                               <%if (result != null && (result.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || result.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia)) %>
                                                <%{ %>
                                               
                                               
<div runat="server" id="leadPaxMealLblOnw" class="col-md-12 mt-3">  <span style="color:black;font-weight: bold;"> Select Meal</span></div> 
                                               

                                                <div runat="server" class="col-md-6" id="leadPaxOnMealDiv">
                                               

                                                <div> <asp:Label ID="lblOnMeal" runat="server" CssClass="sm-text-baggage" Text="" Visible="false"></asp:Label></div>


<div> Onward Meal: </div>

<div class="input-group">




<asp:DropDownList ID="ddlOnwardMeal" Style="margin-bottom: 0;" CssClass="form-control pull-left baggage-input-ctrl"
                                                            runat="server" Visible="false" onchange="CalculateBaggagePrice()">
                                                        </asp:DropDownList>

<div class="input-group-text">
  
    <asp:TextBox Text="0.00" Enabled="false" style="width:50px" ID="lblOutMealPrice" BorderStyle="None" BorderWidth="0" runat="server"></asp:TextBox>



  </div>
      
</div>

                                                    <b class="red_span" style="display: none" id="errOnWardLeadPaxMeal"></b>





                                                    
                                                    
                                                </div>



                                               

                                                <%} %>

                                               




                                                

                                                <%if (result != null && (result.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet || result.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAir || result.ResultBookingSource == CT.BookingEngine.BookingSource.GoAirCorp || result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia)) %>
                                                <%{ %>


                                                  <div runat="server" class="col-md-6" id="leadPaxRetMealDiv">

                                                  <div> <asp:Label ID="lblInMeal" runat="server" CssClass="sm-text-baggage" Text="" Visible="false"></asp:Label> </div>






  <%if(request.Type == CT.BookingEngine.SearchType.Return)
                                         { %>

<div> Return Meal:   </div>

    <%} %>

  



<div class="input-group">



<asp:DropDownList ID="ddlInwardMeal" Style="margin-bottom: 0;" CssClass="form-control pull-left "
                                                            runat="server" Visible="false" onchange="CalculateBaggagePrice()">
                                                        </asp:DropDownList>
  


       <%if(request.Type == CT.BookingEngine.SearchType.Return)
                                         { %>   
<div class="input-group-text">

   
    <asp:TextBox ID="lblInMealPrice" Text="0.00" Enabled="false" style="width:50px" BorderStyle="None" BorderWidth="0" runat="server"></asp:TextBox>
    
   

  </div>

   
         
                                                       
                 <asp:HiddenField ID="hdnFlexCount1" runat="server" Value="0" />

  <%} %>  



</div>


                                                    <b class="red_span" style="display: none" id="errInWardLeadPaxMeal"></b>



                                                    








                                                    
                                               
                                                </div>



                                                <%} %>


                                                   </div>
                                                 
                                                 
                                                 </div>



                                               

                                               
                                            


                                                


                              

                                            </div>
                                        </div>

                                <div class="clear"></div>

                            
                                <div class="collapse advPx-collapseContent" id="paggengerDetailsCollapse-1">
                                    <div class="well">
                                        <div class="Corpdisable">
                                            <div class="form-group">
                                                <div class="col-md-3 col-lg-2">
                                                    <label>
                                                        <strong>Gender
                                                            <%if (GenderRequired)
                                                                { %><span class="red_span">*</span><%} %></strong></label>
                                                    <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control">
                                                        <asp:ListItem Selected="True" Value="-1">Gender</asp:ListItem>
                                                        <asp:ListItem Value="1">Male</asp:ListItem>
                                                        <asp:ListItem Value="2">Female</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <b class="red_span" style="display: none" id="gender"></b>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>
                                                        <strong>Passport No
                                                            <%if (PassportNoRequired)
                                                                { %>
                                                            <span class="red_span">*</span><%} %></strong></label>
                                                    <asp:TextBox ID="txtPassportNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <b class="red_span" style="display: none" id="passport"></b>
                                                </div>
                                                <div class="col-md-5">
                                                    <label>
                                                        <strong>Passport Exp
                                                            <%if (PassportExpiryRequired)
                                                                { %><span class="red_span">*</span><%} %></strong></label>
                                                    <div class="row no-gutter">
                                                        <div class="col-xs-4">
                                                            <asp:DropDownList ID="ddlPEDay" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left">
                                                                <asp:ListItem Selected="True" Value="-1">Day</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <asp:DropDownList ID="ddlPEMonth" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left">
                                                                <asp:ListItem Selected="True" Value="-1">Month</asp:ListItem>
                                                                <asp:ListItem Value="01">Jan</asp:ListItem>
                                                                <asp:ListItem Value="02">Feb</asp:ListItem>
                                                                <asp:ListItem Value="03">Mar</asp:ListItem>
                                                                <asp:ListItem Value="04">Apr</asp:ListItem>
                                                                <asp:ListItem Value="05">May</asp:ListItem>
                                                                <asp:ListItem Value="06">Jun</asp:ListItem>
                                                                <asp:ListItem Value="07">Jul</asp:ListItem>
                                                                <asp:ListItem Value="08">Aug</asp:ListItem>
                                                                <asp:ListItem Value="09">Sep</asp:ListItem>
                                                                <asp:ListItem Value="10">Oct</asp:ListItem>
                                                                <asp:ListItem Value="11">Nov</asp:ListItem>
                                                                <asp:ListItem Value="12">Dec</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <asp:DropDownList ID="ddlPEYear" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left">
                                                                <asp:ListItem Selected="True" Value="-1">Year</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <b class="red_span" style="display: none" id="peDay"></b><b class="red_span" style="display: none"
                                                            id="peMonth"></b><b class="red_span" style="display: none" id="peYear"></b>
                                                    </div>
                                                </div>
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label>
                                                        <strong>Country of Issue
                                                            <%if (CountryRequired)
                                                                { %>
                                                            <span class="red_span">*</span><%} %></strong></label>
                                                    <!-- Modified by Lokesh on 6-APril-2018-->
                                                    <!-- If the country of residence is India-->
                                                     <!-- If the result booking source is AirArabia-->
                                                      <!-- Then GST State Code is mandatory-->
                                                    <asp:DropDownList  ID="ddlCountry" runat="server" AppendDataBoundItems="True" CssClass="form-control" onchange="ShowHideGSTDetails(this.id)">
                                                        <asp:ListItem  Value="-1">Select Country</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <b class="red_span" style="display: none" id="country"></b>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>
                                                        <strong>Nationality
							    <%if (NationalityRequired || result.ResultBookingSource == CT.BookingEngine.BookingSource.FlightInventory || result.ResultBookingSource == CT.BookingEngine.BookingSource.CozmoBus)
                                    { %>
                                                              <span class="red_span">*</span><%} %></strong></label>
                                                    <asp:DropDownList ID="ddlNationality" runat="server" AppendDataBoundItems="True"
                                                        CssClass="form-control" onchange="BindStateGSTCode();">
                                                        <asp:ListItem Selected="True" Value="-1">Select Nationality</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <b class="red_span" style="display: none" id="nationality"></b>
                                                </div>
                                                <div class="col-md-3" >
                                                    <label>
                                                        <strong>Frequent Flyer </strong>
                                                    </label>
                                                    <div class="input-group tel-input-addon">
                                                        <div class="input-group-addon" >
                                                            <asp:TextBox ID="txtAirline" runat="server" CssClass="form-control pull-left"></asp:TextBox>
                                                        </div>
                                                        <asp:TextBox ID="txtFlight" runat="server" CssClass="form-control pull-left"></asp:TextBox>
                                                        <asp:HiddenField runat="server" ID="hdnProfileId" Value=""/>
                                                    </div>
                                                </div>
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                            <div class="form-group hideaddress">
                                                <div class="col-md-4">
                                                    <label>
                                                        <strong>Address
                                                            <%if (AddressRequired)
                                                                { %>
                                                            <span class="red_span">*</span><%} %></strong></label>
                                                    <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <b class="red_span" style="display: none" id="address"></b>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        &nbsp;</label>
                                                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div id="divSeatPref" style="display:none" class="col-md-4">
                                                    <label>
                                                        <strong>Seat Preference</strong></label>
                                                    <asp:TextBox ID="txtSeatPref" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                    <b class="red_span" style="display: none" id="address"></b>
                                                </div>
                                                <div id="divMealPref" style="display:none" class="col-md-4">
                                                    <label><strong>
                                                        Meal Preference</strong></label>
                                                    <asp:TextBox ID="txtMealPref" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                        


                                        
                                        
                                        
                                            <table style="display: none">
                                                <tr>
                                                    <td height="27">
                                                        <strong>Alternate No.</strong>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtAlternateNo" runat="server" CssClass="form-control" onkeypress="return isNumber(event)"
                                                            Visible="false"></asp:TextBox>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <div class="form-group baggage-wrapper" id="divFlex0" style="display: none" >

                                            <div class="row padding-0 marbot_10" id="divFlexFields0"></div>


                                            <div class="row padding-0 marbot_10" id="tblFlexFields" runat="server">
                                            <%--<table id="tblFlexFields" runat="server" border="0" cellspacing="0" cellpadding="0"></table>--%>
                                        </div>
                                            <a style="color: rgb(0, 0, 0); margin-left: 0px; padding: 0px; display:none;" id="ancfelx" class="advPax-collapse-btn-new float-left" role="button" data-toggle="collapse" href="#FlexFiledscollapse"
                                                aria-expanded="false" aria-controls="collapseExample"></a>
                                            <div class="collapse" id="FlexFiledscollapse">
                                                                             <div class="row padding-0 marbot_10 "  id="tblFlexFields1" runat="server" visible="false">
                                            <%--<table id="tblFlexFields" runat="server" border="0" cellspacing="0" cellpadding="0"></table>--%>
                                        </div>

                                            </div>               
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wrap10">
                        <asp:DataList ID="dlAdditionalPax" runat="server" Width="100%" OnItemDataBound="dlAddtionalPax_ItemDataBound">
                            <ItemTemplate>
                           

                             <div class="subgray-header">
                                    <div class="row">
                                        <div class="col-xs-12 col-lg-12">
                                             <strong  class="passenger-label">
                                                    <asp:Label ID="lblPaxType" runat="server" Text='<%#Eval("Type") %>'></asp:Label></strong>
                                            
                                            <a id="ancAPI<%#Container.ItemIndex%>" class="btn btn-primary collapse-btn advPax-collapse-btn  float-right" role="button" data-toggle="collapse" href="#adpaggengerDetailsCollapse-<%#Container.ItemIndex%>"
                                                aria-expanded="false" aria-controls="collapseExample">ADD API's </a>
                                                  <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate != "Y")
                                                      {%>  
                                                  <button type="button" class="btn btn-link float-right" onclick="ShowSearhPassenger(<%#Container.ItemIndex+1%>)">Search Passenger <span class="glyphicon glyphicon-search"></span></button>
                                                  <%} %>
                                        </div>
                                     
                                    </div>

                                </div>


                                <div class="pax-details-wrap">
                                    <div class="col-xs-12 col-lg-12">
                                       <asp:CheckBox runat="server" ID="chkAddPax" CssClass="custom-checkbox-style dark chkbox-addpasngr" Text="Add Passenger"></asp:CheckBox>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-md-3 col-lg-2">
                                                    <label>
                                                        <strong>Title <span class="red_span">*</span></strong>
                                                    </label>
                                                    <asp:DropDownList CssClass="form-control" ID="ddlPaxTitle" runat="server">
                                                        <asp:ListItem Selected="True" Value="-1">Select</asp:ListItem>
                                                        <asp:ListItem Value="Mr" Text="Mr."></asp:ListItem>
                                                        <asp:ListItem Value="Ms" Text="Ms."></asp:ListItem>
                                                        <asp:ListItem Value="Dr" Text="Dr."></asp:ListItem>
                                                        <asp:ListItem Value="MSTR" Text="Master"></asp:ListItem>
                                                        <asp:ListItem Value="CHD" Text="Child"></asp:ListItem>
                                                        <asp:ListItem Value="INF" Text="Infant"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <b class="red_span" style="display: none" id="errPaxtitle" runat="server"></b>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        <strong>First Name <span class="red_span">*</span></strong></label>
                                                    <asp:TextBox ID="txtPaxFName" CssClass="form-control" runat="server" CausesValidation="true"
                                                        ValidationGroup="email" onkeypress="return IsAlphaNumeric(event);" ondrop="return false;"
                                                        onpaste="return true;"></asp:TextBox>
                                                    <b class="red_span" style="display: none" id="errPaxfirstname" runat="server"></b>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        <strong>Last Name <span class="red_span">*</span></strong></label>
                                                    <asp:TextBox ID="txtPaxLName" CssClass="form-control" runat="server" CausesValidation="true"
                                                        ValidationGroup="email" onkeypress="return IsAlphaNumeric(event);" ondrop="return false;"
                                                        onpaste="return true;"></asp:TextBox>
                                                    <b class="red_span" style="display: none" id="errPaxlastname" runat="server"></b>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>
                                                        <strong>D.O.B.
                                                            <%if (DOBRequired)
                                                              { %><span class="red_span">*</span><%} %>
                                                        </strong>
                                                    </label>
                                                    <div class="row no-gutter">
                                                        <div class="col-xs-4">
                                                            <asp:DropDownList ID="ddlDay" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left"
                                                                CausesValidation="true" ValidationGroup="email">
                                                                <asp:ListItem Selected="True" Value="-1">Day</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <asp:DropDownList ID="ddlMonth" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left"
                                                                CausesValidation="true" ValidationGroup="email">
                                                                <asp:ListItem Selected="True" Value="-1">Month</asp:ListItem>
                                                                <asp:ListItem Value="01">Jan</asp:ListItem>
                                                                <asp:ListItem Value="02">Feb</asp:ListItem>
                                                                <asp:ListItem Value="03">Mar</asp:ListItem>
                                                                <asp:ListItem Value="04">Apr</asp:ListItem>
                                                                <asp:ListItem Value="05">May</asp:ListItem>
                                                                <asp:ListItem Value="06">Jun</asp:ListItem>
                                                                <asp:ListItem Value="07">Jul</asp:ListItem>
                                                                <asp:ListItem Value="08">Aug</asp:ListItem>
                                                                <asp:ListItem Value="09">Sep</asp:ListItem>
                                                                <asp:ListItem Value="10">Oct</asp:ListItem>
                                                                <asp:ListItem Value="11">Nov</asp:ListItem>
                                                                <asp:ListItem Value="12">Dec</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <asp:DropDownList ID="ddlYear" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left"
                                                                CausesValidation="true" ValidationGroup="email">
                                                                <asp:ListItem Selected="True" Value="-1">Year</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <b class="red_span" style="display: none" id="errPaxday" runat="server"></b><b class="red_span"
                                                            style="display: none" id="errPaxmonth" runat="server"></b><b class="red_span" style="display: none"
                                                                id="errPaxyear" runat="server"></b>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group baggage-wrapper col-xs-12 col-lg-12 Corppolicy" runat="server" id="lblOnBagWrapperAddPax" visible="false">
                                                            <label>
                                                                <strong>
                                                                    <asp:Label ID="lblBaggage" runat="server" Text="Select Baggage " Visible="false"></asp:Label></strong></label>
                                                                     <br />
                                                                    <asp:Label runat="server" CssClass="red_span"  ID="lblG9BaggageAlertAddPax">(Excluding CGST and GST Tax Components)</asp:Label>
                                                            <div class="row">
                                                                

                                                                <div class="col-md-12"> 
                                                              
                                                               <div class="row"> 
                                                               
                                                                <div class="col-md-6" id="addPaxOnBag" runat="server">

                                                 

                                                                <div>  Onward: <asp:Label ID="lblOnBag" CssClass="sm-text-baggage d-inline" runat="server" Text="" Visible="false"></asp:Label></div>
                                                                    <div class="input-group">
                                                                        
                                                                        <asp:DropDownList CssClass="form-control pull-left baggage-input-ctrl" ID="ddlOnwardBaggage"
                                                                            Style="margin-bottom: 0;" runat="server" Visible="false" onchange="CalculateBaggagePrice()">
                                                                        </asp:DropDownList>
                                                                      <div class="input-group-text">
                                                                            <asp:TextBox Width="50px" ID="lblOutPrice" Text="0.00" Enabled="false" BorderStyle="None" BorderWidth="0" runat="server"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                     <b class="red_span" style="display: none" runat="server" id="errOnwardBaggage"></b>
                                                                </div>

                                                                 <div class="col-md-6" id="addPaxRetBag" runat="server">


                                                             



     <%if(request.Type == CT.BookingEngine.SearchType.Return)
                                         { %>
                                                                    
                                                           <div> Return: <asp:Label ID="lblInBag" runat="server" Text="" CssClass="sm-text-baggage d-inline" Visible="false"></asp:Label>   </div>
                                                                            
                                                                      
                                                                        
                       <%} %>  


                                                                    <div class="input-group">
                 
                                                                        
                                                                        
                                                                        
                                                                        <asp:DropDownList ID="ddlInwardBaggage" runat="server" Style="margin-bottom: 0;"
                                                                            CssClass="form-control pull-left" Visible="false" onchange="CalculateBaggagePrice()">
                                                                        </asp:DropDownList>
                                                                        
    <%if(request.Type == CT.BookingEngine.SearchType.Return)
                                         { %>                                                                    
                                              <div class="input-group-text">
                                                                            <asp:TextBox ID="lblInPrice" CssClass="pull-left" Text="0.00" Enabled="false" Width="50px" BorderStyle="None"
                                                                                BorderWidth="0" runat="server"></asp:TextBox>
                                                                        </div>
        <%} %>                                                                
                                                                        
                                                                    </div>
                                                                    <b class="red_span" style="display: none" runat="server" id="errReturnBaggage"></b>
                                                                </div>

                                                               

                                                                 </div> 

                                                                <div class="row"> 
                                                                
                                                                
                         
               

                                                                <div class="col-md-6" id="addPaxOnwardMeal" runat="server">

 <div id="addPaxlblMeal" runat="server" class="mt-3">  <span style="color:black;font-weight: bold;"> Select Meal</span></div> 

                                                                <div> <asp:Label ID="lblOnMeal" CssClass="sm-text-baggage" runat="server" Text="" Visible="false"></asp:Label></div>



                                                                <div>Onward Meal:</div>
                                                                <div class="input-group">

                                                                    <asp:DropDownList CssClass="form-control pull-left baggage-input-ctrl" ID="ddlOnwardMeal"
                                                                        Style="margin-bottom: 0;" runat="server" Visible="false" onchange="CalculateBaggagePrice()">
                                                                    </asp:DropDownList>

                                                                    <div class="input-group-text">
                                                                        <asp:TextBox Width="50px" ID="lblOutMealPrice" Text="0.00" Enabled="false" BorderStyle="None" BorderWidth="0" runat="server"></asp:TextBox>
                                                                    </div>

                                                                </div>
                                                                <b class="red_span" style="display: none" runat="server" id="errOnwardMeal"></b>


                                                            </div>


                                                                    <div class="col-md-6" id="addPaxReturnMeal" runat="server">


 <div class="mt-3"> &nbsp;   </div>

                <%if(request.Type == CT.BookingEngine.SearchType.Return)
                                         { %>
                                                                    
                  <div>  Return Meal: </div>
                                                                           
                                                                        
                                                                        
                       <%} %>  



                                                                <div class="input-group">




                                                                    <asp:DropDownList ID="ddlInwardMeal" runat="server" Style="margin-bottom: 0;"
                                                                        CssClass="form-control pull-left" Visible="false" onchange="CalculateBaggagePrice()">
                                                                    </asp:DropDownList>


                                                                    <%if(request.Type == CT.BookingEngine.SearchType.Return)
                                         { %>

                                                                    <div class="input-group-text">
                                                                        <asp:TextBox ID="lblInMealPrice" Text="0.00" CssClass="pull-left" Enabled="false" Width="50px" BorderStyle="None"
                                                                            BorderWidth="0" runat="server"></asp:TextBox>
                                                                    </div>


                                                                    <%} %>
                                                                </div>
                                                                <b class="red_span" style="display: none" runat="server" id="errInwardMeal"></b>


                                                                    <asp:Label ID="lblInMeal" runat="server" CssClass="sm-text-baggage" Visible="false"></asp:Label>
                                                                </div>


                                                                </div> 

                                                                </div>
                                                            
                                                            
                                                            
                                                            </div>







                                                        </div>
                                                        <div class="clearfix"></div>
                                           
                                            <div class="collapse advPx-collapseContent" id="adpaggengerDetailsCollapse-<%#Container.ItemIndex%>">
                                                <div class="well">
                                                    <div class="form-group">
                                                        <div class="col-md-3 col-lg-2">
                                                            <label>
                                                                <strong>Gender
                                                                    <%if (GenderRequired)
                                                                      { %><span class="red_span">*</span><%} %></strong>
                                                            </label>
                                                            <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control" CausesValidation="true"
                                                                ValidationGroup="email">
                                                                <asp:ListItem Selected="True" Value="-1">Gender</asp:ListItem>
                                                                <asp:ListItem Value="1">Male</asp:ListItem>
                                                                <asp:ListItem Value="2">Female</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <b class="red_span" style="display: none" id="errPaxgender" runat="server"></b>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>
                                                                <strong>Passport No
                                                                    <%if (PassportNoRequired)
                                                                      { %><span class="red_span">*</span><%} %></strong></label>
                                                            <asp:TextBox ID="txtPassportNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <b class="red_span" style="display: none" id="errPaxpassportno" runat="server"></b>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <label>
                                                                <strong>Passport Exp
                                                                    <%if (PassportExpiryRequired)
                                                                      { %><span class="red_span">*</span><%} %></strong>
                                                            </label>
                                                            <div class="row no-gutter">
                                                                <div class="col-xs-4">
                                                                    <asp:DropDownList ID="ddlPEDay" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left">
                                                                        <asp:ListItem Selected="True" Value="-1">Day</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="col-xs-4">
                                                                    <asp:DropDownList ID="ddlPEMonth" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left">
                                                                        <asp:ListItem Selected="True" Value="-1">Month</asp:ListItem>
                                                                        <asp:ListItem Value="01">Jan</asp:ListItem>
                                                                        <asp:ListItem Value="02">Feb</asp:ListItem>
                                                                        <asp:ListItem Value="03">Mar</asp:ListItem>
                                                                        <asp:ListItem Value="04">Apr</asp:ListItem>
                                                                        <asp:ListItem Value="05">May</asp:ListItem>
                                                                        <asp:ListItem Value="06">Jun</asp:ListItem>
                                                                        <asp:ListItem Value="07">Jul</asp:ListItem>
                                                                        <asp:ListItem Value="08">Aug</asp:ListItem>
                                                                        <asp:ListItem Value="09">Sep</asp:ListItem>
                                                                        <asp:ListItem Value="10">Oct</asp:ListItem>
                                                                        <asp:ListItem Value="11">Nov</asp:ListItem>
                                                                        <asp:ListItem Value="12">Dec</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="col-xs-4">
                                                                    <asp:DropDownList ID="ddlPEYear" runat="server" AppendDataBoundItems="True" CssClass="form-control pull-left">
                                                                        <asp:ListItem Selected="True" Value="-1">Year</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <b class="red_span" style="display: none" id="errPaxpeday" runat="server"></b><b
                                                                    class="red_span" style="display: none" id="errPaxpeMonth" runat="server"></b>
                                                                <b class="red_span" style="display: none" id="errPaxpeyear" runat="server"></b>
                                                            </div>
                                                        
                                                        </div>       <div class="clearfix"> </div>
                                                        <div class="col-md-3">
                                                            <label>
                                                                <strong>Country Of Issue
                                                                    <%if (CountryRequired)
                                                                      { %>
                                                                    <span class="red_span">*</span><%} %></strong></label>
                                                            <asp:DropDownList ID="ddlCountry" runat="server" AppendDataBoundItems="True" CssClass="form-control"
                                                                CausesValidation="true" ValidationGroup="email">
                                                                <asp:ListItem Selected="True" Value="-1">Select Country</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <b class="red_span" style="display: none" id="errPaxcountry" runat="server"></b>
                                                        </div>
                                                        <!--Lokesh 10May2017 : Added Nationality for additional pax-->
                                                           <div class="col-md-3">
                                                            <label>
                                                                <strong>Nationality
                                                                    <%if (NationalityRequired || result.ResultBookingSource == CT.BookingEngine.BookingSource.FlightInventory || result.ResultBookingSource == CT.BookingEngine.BookingSource.CozmoBus) 
                                                                      { %>
                                                                    <span class="red_span">*</span><%} %></strong></label>
                                                            <asp:DropDownList ID="ddlNationality" onchange="BindStateGSTCode();" runat="server" AppendDataBoundItems="True"
                                                                CssClass="form-control" CausesValidation="true" ValidationGroup="email">
                                                                <asp:ListItem  Value="-1">Select Nationality</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <b class="red_span" style="display: none" id="errPaxNationality" runat="server">
                                                            </b>
                                                        </div>
                                                        <!-- Praveen 18April2019 : Added FF Number for additional pax -->
                                                        <div class="col-md-3" >
                                                            <label>
                                                                <strong>Frequent Flyer </strong>
                                                            </label>
                                                            <div class="input-group tel-input-addon">
                                                                <div class="input-group-addon" >
                                                                    <asp:TextBox ID="txtAirline" runat="server" CssClass="form-control pull-left"></asp:TextBox>
                                                                </div>
                                                                <asp:TextBox ID="txtFlight" runat="server" CssClass="form-control pull-left"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div id="divSeatPref<%#Container.ItemIndex%>" style="display:none" class="col-md-3">
                                                            <label>
                                                                <strong>Seat Preference</strong></label>
                                                            <asp:TextBox ID="txtSeatPref" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            <b class="red_span" style="display: none" id="B3" runat="server"></b>
                                                        </div>
                                                        <div id="divMealPref<%#Container.ItemIndex%>" style="display:none" class="col-md-3">
                                                            <label>
                                                                <strong>Meal Preference</strong></label>
                                                            <asp:TextBox ID="txtMealPref" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            <b class="red_span" style="display: none" id="B4" runat="server"></b>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdnProfileId" Value=""/>
                                                        <div class="clearfix">
                                                        </div>
                                                        <div class="form-group baggage-wrapper"  id="divFlex<%#Container.ItemIndex+1%>" style="display:none">
                                                            <div class="row padding-0 marbot_10" id="divFlexFields<%#Container.ItemIndex+1%>" style="padding:15px">
                                                            <div class="row padding-0 marbot_10" id="tblFlexFieldsAd" style="padding:15px" runat="server">
                                                        </div></div>
                                                        <div class="clearfix">
                                                        </div>
                                                    </div>
                                              
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </ItemTemplate>
                        </asp:DataList>
                        <div>
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Text=""></asp:Label>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </div>
                <div class="hidecntrldiv Corppolicy" id="divSelSeats">
                    <a id="btnSeatselect" class="btn btn-info btn-lg" data-toggle="modal" onclick="showpopup(''); return false;">Select seats</a>
                </div>
                <div>
                    <label style="padding-right: 10px; float: right; text-align: right">
                        <asp:Button ID="imgContinue" CssClass="button-normal" Text="Continue" runat="server"
                            CausesValidation="true" OnClientClick="return validate();" OnClick="imgContinue_Click"
                            ValidationGroup="email" />
                    </label>
                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <%if (result.ResultBookingSource == CT.BookingEngine.BookingSource.TBOAir && hdfFareAction.Value != "Updated")
        { %>
    <%double baseFare = 0, newBaseFare = 0;
        decimal tax = 0, newTax = 0, handlingFee = 0, markup = 0, discount = 0;
        if (fares != null && fares.Length > 0 && errorMessage.Length == 0)
        {
            int count = 0;
            foreach (CT.BookingEngine.Fare objFare in fares)
            {
                count += objFare.PassengerCount;
            }
            if (Session["OtherCharges"] != null)
            {
                hdfOtherCharges.Value = Session["OtherCharges"].ToString();
            }
            for (int i = 0; i < fares.Length; i++)
            {
                if (result.FareBreakdown.Length > i)
                {
                    //if (result.FareBreakdown[i].BaseFare != fares[i].BaseFare || result.FareBreakdown[i].Tax != fares[i].Tax)
                    {
                        baseFare += result.FareBreakdown[i].BaseFare + (double)result.FareBreakdown[i].HandlingFee;
                        newBaseFare += fares[i].BaseFare;
                        tax += result.FareBreakdown[i].Tax + result.FareBreakdown[i].AgentMarkup - result.FareBreakdown[i].AgentDiscount;
                        newTax += fares[i].Tax;
                        CT.BookingEngine.Fare fare = fares[i];
                        if (i == result.FareBreakdown.Length - 1)
                        {
                            tax += Convert.ToDecimal(hdfOtherCharges.Value);
                            newTax += ((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee));

                            if (result.Price.MarkupType == "P")
                            {
                                if (agency.AgentAirMarkupType == "TF")
                                {
                                    markup += (((decimal)((decimal)newBaseFare + newTax)) * result.Price.MarkupValue / 100);
                                }
                                else if (agency.AgentAirMarkupType == "BF")
                                {
                                    markup += (((decimal)newBaseFare * result.Price.MarkupValue / 100));
                                }
                                else if (agency.AgentAirMarkupType == "TX")
                                {
                                    markup += (((decimal)newTax) * result.Price.MarkupValue / 100);
                                }
                            }

                            if (result.Price.DiscountType == "P")
                            {
                                if (agency.AgentAirMarkupType == "BF")
                                {
                                    discount += (decimal)newBaseFare * (result.Price.DiscountValue / 100);
                                }
                                else if (agency.AgentAirMarkupType == "TX")
                                {
                                    discount += (decimal)newTax * (result.Price.DiscountValue / 100);
                                }
                                else if (agency.AgentAirMarkupType == "TF")
                                {
                                    discount += ((decimal)newBaseFare + newTax) * (result.Price.DiscountValue / 100);
                                }
                            }
                            else
                            {
                                discount += result.Price.DiscountValue;
                            }
                        }
                    }
                }
            }
            if (result.Price.MarkupType == "F")
            {
                markup += (result.Price.MarkupValue * paxCount);
            }
            if (result.LoginCountryCode == "IN")
            {
                if (result.Price.HandlingFeeType == "P")
                {
                    handlingFee = ((decimal)newBaseFare + newTax + markup) * result.Price.HandlingFeeValue / 100;
                    newBaseFare += (double)handlingFee;
                }
                else
                {
                    newBaseFare += (double)result.Price.HandlingFeeValue * paxCount;
                }
            }
            if (result.MarkupThreshold != null)
            {

                decimal dmarkup = 0m;
                CT.BookingEngine.DynamicMarkupThreshold threshold = result.MarkupThreshold;
                if (result.MarkupThreshold.MarkupType == "P")
                {
                    dmarkup = (decimal)((double)newTax + newBaseFare) * (threshold.MarkupValue / 100);
                }
                else
                {
                    dmarkup = threshold.MarkupValue * paxCount;
                }
                markup += dmarkup;
            }
            newBaseFare = Math.Round(newBaseFare, agency.DecimalValue);
            baseFare = Math.Round(baseFare, agency.DecimalValue);
            newTax = Math.Round(newTax + markup - discount, agency.DecimalValue);
            tax = Math.Round(tax, agency.DecimalValue);
        }%>
    <%if (errorMessage.Length == 0)
        {
            if ((newBaseFare - baseFare != 0 || newTax - tax != 0))
            {%>


         <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="FareRuleHead">  Price for the Itinerary has been changed!</h4>
              </div>
              <div class="modal-body">
                    <div id="FareRuleBody" style="text-align: center">
            <div>
                <span id="priceChange" style="color: Red; font-size: 14px; font-weight: bold; display: none;">
                    Price has been changed for this Itinerary</span></div>
            <div>
                <div class="col-md-6">
                    <b>Original Fare :</b>
                    <label>
                        <%=Math.Ceiling((baseFare + (double)tax)).ToString("N" + agency.DecimalValue)%></label>
                </div>
                <div class="col-md-6">
                    <b>Changed Fare :</b>
                    <label style="color: Red">
                        <%=Math.Ceiling(newBaseFare + (double)newTax).ToString("N" + agency.DecimalValue)%>
                    </label>
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="padtop_4">
                <input type="button" id="Continue" class="btn but_b" value="Continue Booking" onclick="PriceContinue()" />
            </div>
            <div class="padtop_4">
                <input type="button" id="Cancel" class="btn but_b" value="Search Again" onclick="PriceCancel()"
                    style="width: 150px;" />
            </div>
        </div>
               </div>
            </div>
          </div>
        </div>



    <script type="text/javascript">
        //document.getElementById('FareDiff').style.display = 'block';
        $('#FareDiff').modal('show')
        document.getElementById('<%=imgContinue.ClientID %>').style.display = 'none';
        document.getElementById('priceChange').style.display = 'block';
    </script>

    <%}
        }
        else
        {%> 

        <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">           
              <div class="modal-body">
   <center>
            <h2>
                <%=errorMessage%></h2>
            <br />
            <br />
            <input type="button" id="btnCancel" value="Search Again" onclick="PriceCancel()" />
        </center>

     </div>
            </div>
          </div>
        </div>





    <%}
        }
        else if (result.ResultBookingSource == CT.BookingEngine.BookingSource.UAPI && hdfFareAction.Value != "Updated")
        {
            if (priceRsp != null && priceRsp.AirPriceResult != null && priceRsp.AirPriceResult.Length > 0 && priceRsp.AirPriceResult[0].AirPricingSolution != null && priceRsp.AirPriceResult[0].AirPricingSolution.Length > 0)
            {
                /**********************************************************************************************************
                 * Compare the current Base Fare, Tax & Total Fare with the values in PriceRsp
                 * ********************************************************************************************************/


                decimal Total = (decimal)result.TotalFare;

                decimal newTotal = 0;
                decimal newTax = 0;
                decimal newBasePrice = 0;

                CT.BookingEngine.SearchResult res = new CT.BookingEngine.SearchResult();
                res.Currency = result.Currency;
                res.ResultBookingSource = result.ResultBookingSource;
                res.ResultId = result.ResultId;
                res.Flights = result.Flights;
                res.Price = result.Price;
                res.FareBreakdown = fares;
                res.LoginCountryCode = result.LoginCountryCode;
                //Calculate Markup for the new price
                decimal markup = 0, discount = 0;
                for (int count = 0; count < fares.Length; count++)
                {
                    CT.BookingEngine.PriceAccounts tempPrice = CT.AccountingEngine.AccountingEngine.GetPrice(res, count, (int)agency.ID, 0, (int)CT.BookingEngine.ProductType.Flight, "B2B");
                    newBasePrice += (decimal)(res.FareBreakdown[count].BaseFare) + res.FareBreakdown[count].HandlingFee;
                    newTax += (res.FareBreakdown[count].Tax);
                    newTotal = newBasePrice + newTax;
                    markup += (tempPrice.Markup) * result.FareBreakdown[count].PassengerCount;
                    discount += (tempPrice.Discount) * result.FareBreakdown[count].PassengerCount;

                    if (result.MarkupThreshold != null)
                    {
                        res.MarkupThreshold = result.MarkupThreshold;
                        decimal dmarkup = 0m;
                        CT.BookingEngine.DynamicMarkupThreshold threshold = result.MarkupThreshold;
                        if (result.MarkupThreshold.MarkupType == "P")
                        {
                            dmarkup = (decimal)(res.FareBreakdown[count].TotalFare) * (threshold.MarkupValue / 100);
                        }
                        else
                        {
                            dmarkup = threshold.MarkupValue * res.FareBreakdown[count].PassengerCount;
                        }
                        markup += dmarkup;
                    }
                }
                newTotal += Math.Round(markup, agency.DecimalValue);//Add markup to the total fare
                newTotal -= Math.Round(discount, agency.DecimalValue);
                //If any of the values are not same, show the popup with price difference
                if (Math.Round(Total, agency.DecimalValue) != Math.Round(newTotal, agency.DecimalValue))
                {
                    result.BaseFare = (double)newBasePrice;
                    result.Tax = (double)newTax;
                    result.TotalFare = (double)newTotal;
    %>

        <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="FareRuleHead">  Price for the Itinerary has been changed!</h4>
              </div>
              <div class="modal-body">

 <div class="" id="FareRuleBody" style="overflow: auto; width: 100%; text-align: center">
            <div>
                <span id="priceChange" style="color: Red; font-size: 14px; font-weight: bold; display: none;">
                    Price has been changed for this Itinerary</span></div>
            <div>
                <div class="col-md-6">
                    <b>Original Fare :</b>
                    <label>
                        <%=(Math.Round(Total, agency.DecimalValue)).ToString("N" + agency.DecimalValue)%></label>
                </div>
                <div class="col-md-6">
                    <b>Changed Fare :</b>
                    <label style="color: Red">
                        <%=Math.Round(newTotal, agency.DecimalValue).ToString("N" + agency.DecimalValue)%>
                    </label>
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="padtop_4">
                <input type="button" id="Continue" class="btn but_b" value="Continue Booking" onclick="PriceContinue()" />
            </div>
            <div class="padtop_4">
                <input type="button" id="Cancel" class="btn but_b" value="Search Again" onclick="PriceCancel()"
                    style="width: 150px;" />
            </div>
        </div>

     </div>
            </div>
          </div>
        </div>

    <script type="text/javascript">
        //document.getElementById('FareDiff').style.display = 'block';
        $('#FareDiff').modal('show')
        document.getElementById('<%=imgContinue.ClientID %>').style.display = 'none';
        document.getElementById('priceChange').style.display = 'block';
    </script>

    <%}
            else
            {
                Session["TimesChanged"] = null;
            }
        }%>
    <% else if (errorMessage.Length > 0)
        { %>


             <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
           
              <div class="modal-body">
      <center>
            <h2>
                <%=errorMessage%></h2>
            <br />
            <br />
            <input type="button" id="btnCancel" value="Search Again" onclick="PriceCancel()" />
        </center>

     </div>
            </div>
          </div>
        </div>


    <%}
        }
        else if (result.ResultBookingSource == CT.BookingEngine.BookingSource.AirArabia && G9Result != null && G9Result.FareBreakdown != null)
        { //Calculate Markup for the new price
            decimal markup = 0;
            decimal newTotal = 0;
            decimal newTax = 0;
            decimal newBasePrice = 0;

            //for (int count = 0; count < G9Result.FareBreakdown.Length; count++)
            //{
            //    //CT.BookingEngine.PriceAccounts tempPrice = CT.AccountingEngine.AccountingEngine.GetPrice(G9Result, count, (int)agency.ID, 0, (int)CT.BookingEngine.ProductType.Flight, "B2B");
            //    newBasePrice += (decimal)(G9Result.FareBreakdown[count].BaseFare);
            //    newTax += (G9Result.FareBreakdown[count].Tax);
            //    newTotal = newBasePrice + newTax;
            //    markup += Math.Round(G9Result.FareBreakdown[count].AgentMarkup, agency.DecimalValue);
            //}
            newTotal = (decimal)G9Result.TotalFare;//Add markup to the total fare

            if (Math.Round(result.TotalFare, agency.DecimalValue).ToString("N" + agency.DecimalValue) != Math.Round(newTotal, agency.DecimalValue).ToString("N" + agency.DecimalValue))
            {%>

             <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="FareRuleHead">  Price for the Itinerary has been changed!</h4>
              </div>
              <div class="modal-body">
  <div class="" id="FareRuleBody" style="overflow: auto; width: 100%; text-align: center">
            <div>
                <span id="priceChange" style="color: Red; font-size: 14px; font-weight: bold; display: none;">
                    Price has been changed for this Itinerary</span></div>
            <div>
                <div class="col-md-6">
                    <b>Original Fare :</b>
                    <label>
                        <%=(Math.Round(result.TotalFare, agency.DecimalValue)).ToString("N" + agency.DecimalValue)%></label>
                </div>
                <div class="col-md-6">
                    <b>Changed Fare :</b>
                    <label style="color: Red">
                        <%=Math.Round(newTotal, agency.DecimalValue).ToString("N" + agency.DecimalValue)%>
                    </label>
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="padtop_4">
                <input type="button" id="Continue" class="btn but_b" value="Continue Booking" onclick="PriceContinue()" />
            </div>
            <div class="padtop_4">
                <input type="button" id="Cancel" class="btn but_b" value="Search Again" onclick="PriceCancel()"
                    style="width: 150px;" />
            </div>
        </div>

     </div>
            </div>
          </div>
        </div>

    <script type="text/javascript">
        //document.getElementById('FareDiff').style.display = 'block';
        $('#FareDiff').modal('show')
        document.getElementById('<%=imgContinue.ClientID %>').style.display = 'none';
        document.getElementById('priceChange').style.display = 'block';
    </script>

    <%}
        }
        else if ((result.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || (result.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp) || (result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJet) || (result.ResultBookingSource == CT.BookingEngine.BookingSource.SpiceJetCorp) || (result.ResultBookingSource == CT.BookingEngine.BookingSource.Babylon)) && hdfFareAction.Value != "Updated" && indigoPriceChnaged && errorMessage.Length == 0 && oldTotalIndigo != newTotalIndigo)
        {%>



        <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="FareRuleHead">  Price for the Itinerary has been changed!</h4>
              </div>
              <div class="modal-body">
    <div class="" id="FareRuleBody" style="overflow: auto; width: 100%; text-align: center">
            <div>
                <span id="priceChange" style="color: Red; font-size: 14px; font-weight: bold; display: none;">
                    Price has been changed for this Itinerary</span></div>
            <div>
                <div class="col-md-6">
                    <b>Original Fare :</b>
                    <label>
                        <%=(Math.Round(oldTotalIndigo, agency.DecimalValue)).ToString("N" + agency.DecimalValue)%></label>
                </div>
                <div class="col-md-6">
                    <b>Changed Fare :</b>
                    <label style="color: Red">
                        <%=Math.Round(newTotalIndigo, agency.DecimalValue).ToString("N" + agency.DecimalValue)%></label>
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="padtop_4">
                <input type="button" id="btnIndigoPriceContinue" class="btn but_b" value="Continue Booking"
                    onclick="PriceContinue()" />
            </div>
            <div class="padtop_4">
                <input type="button" id="btnIndigoPriceCancel" class="btn but_b" value="Search Again"
                    onclick="PriceCancel()" style="width: 150px;" />
            </div>
        </div>

     </div>
            </div>
          </div>
        </div>

    <script type="text/javascript">
        //document.getElementById('FareDiff').style.display = 'block';
        $('#FareDiff').modal('show')
        document.getElementById('<%=imgContinue.ClientID %>').style.display = 'none';
        document.getElementById('priceChange').style.display = 'block';
    </script>

    <%}%>
    <% else if (errorMessage.Length > 0 && (result.ResultBookingSource == CT.BookingEngine.BookingSource.Indigo || result.ResultBookingSource == CT.BookingEngine.BookingSource.IndigoCorp))
        { %>

      <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="FareRuleHead"> Price Alert</h4>
              </div>
              <div class="modal-body">

  <div class="" id="FareRuleBody" style="overflow: auto; width: 100%; text-align: center">
            <div>
                <span id="priceChange" style="color: Red; font-size: 14px; font-weight: bold; display: none;">
                    <%=errorMessage %></span>
            </div>
            <div class="padtop_4">
                <input type="button" id="btnIndigoReprice" class="btn but_b" value="Search Again"
                    onclick="PriceCancel()" style="width: 150px;" />
            </div>
        </div>
     </div>
            </div>
          </div>
        </div>



      

        <script type="text/javascript">
            document.getElementById('FareDiff').style.display = 'block';
            document.getElementById('priceChange').style.display = 'block';      
        </script>

        <%}
            else if (result.ResultBookingSource == CT.BookingEngine.BookingSource.PKFares && Session["RepricedResult"] != null)
            {

                G9Result = Session["RepricedResult"] as CT.BookingEngine.SearchResult;
                decimal markup = 0;
                decimal newTotal = 0;
                decimal newTax = 0;
                decimal newBasePrice = 0;

                newTotal = (decimal)G9Result.TotalFare;//Add markup to the total fare

                if (Math.Ceiling((decimal)result.TotalFare) != Math.Ceiling(newTotal))
                {%>


       <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="FareRuleHead">  Price for the Itinerary has been changed!</h4>
              </div>
              <div class="modal-body">
                  <div class="" id="FareRuleBody" style="overflow: auto; width: 100%; text-align: center">
            <div>
                <span id="priceChange" style="color: Red; font-size: 14px; font-weight: bold; display: none;">Price has been changed for this Itinerary</span>
            </div>
            <div>
                <div class="col-md-6">
                    <b>Original Fare :</b>
                    <label>
                        <%=(Math.Round(result.TotalFare, agency.DecimalValue)).ToString("N" + agency.DecimalValue)%></label>
                </div>
                <div class="col-md-6">
                    <b>Changed Fare :</b>
                    <label style="color: Red">
                        <%=Math.Round(newTotal, agency.DecimalValue).ToString("N" + agency.DecimalValue)%>
                    </label>
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="padtop_4">
                <input type="button" id="Continue" class="btn but_b" value="Continue Booking" onclick="PriceContinue()" />
            </div>
            <div class="padtop_4">
                <input type="button" id="Cancel" class="btn but_b" value="Search Again" onclick="PriceCancel()"
                    style="width: 150px;" />
            </div>
        </div>

              </div>
            </div>
          </div>
        </div>

        <script type="text/javascript">
            //document.getElementById('FareDiff').style.display = 'block';
            $('#FareDiff').modal('show')
        document.getElementById('<%=imgContinue.ClientID %>').style.display = 'none';
        document.getElementById('priceChange').style.display = 'block';
        </script>
        <%}
            } %>
        <asp:HiddenField runat="server" ID="hdnAddNationlities" />
         
        
        <div id="paxAddSerNotification" style="display: none;" class="price_change_block">
            <div class="head_bg">
                <h5>
                    <center>
                        Notification!</center>
                </h5>
            </div>
            <div class="body" style="overflow: auto; width: 100%; text-align: center">
                <div>
                    <span style="color: Red; font-size: 14px; font-weight: bold;">Selected Nationality is
                        not authorised to book this flight!</span></div>
                <div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="padtop_4">
                    <input type="button" id="btnAddSerCancel" class="btn but_b" value="Close" onclick="cancelNotification()"
                        style="width: 150px;" />
                </div>
            </div>
        </div>



    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn btn-default" onclick="Saveseatinfo()" >Save seats</button>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body"> 
                    <div class="row">
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="Segments"></div>
                                </div>
                                <div class="col-md-12 term_conditions"> 
                                    To ensure the safety of passengers, including yours, adults traveling with Infants, pregnant ladies, those who require wheelchair and who require safety assistance in case of emergency evacuation cannot be assigned emergency exit seats, including 1st row on few aircraft.
                                    Emergency exit rows and 1st row of some aircraft cannot be assigned to adults traveling with Infants, pregnant ladies, those who require wheelchair and who require safety assistance in case of emergency evacuation. For further details, please read the <a href="#">T&C</a>.
                                </div>
                                <div class="col-md-12">
                                    <a id="btnSeatreset" onclick="return ResetSeatMap(); return false;" class="btn btn_reset"> RESET </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-12"> 
                                    <div class="light_wrapper">
                                        <div class="col-md-4 col-xs-4"> <span class="selected"> &nbsp;Selected </span>  </div>
                                        <div class="col-md-4 col-xs-4"> <span class="available"> &nbsp;Available </span>   </div>
                                        <div class="col-md-4 col-xs-4"> <span class="occupied"> &nbsp;Occupied </span>  </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="scroll_skeleton"> 
                                        <div id="seatslayout"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="Saveseatinfo()" >Save seats</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <asp:TextBox runat="server" style="display:none" ID="txtPaxSeatInfo" />
                    <asp:HiddenField ID="hdnPaxSeatInfo" runat="server" />
                </div>
            </div>
        </div>
    </div>
    

    <div class="modal fade pymt-modal" data-backdrop="static" id="Seatsalert" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content" style="width:500px;">
              <div class="modal-header">
                  <h4 class="modal-title" id="SeatsalertLabel">Seat assignment alert</h4>
              </div>
              <div class="modal-body">
                <div id="divSeatsalertText" style="color:Red; font-size:14px; font-weight:bold; padding:10px;"></div>                  
                <div class="padtop_4">
                    <a id="SeatsalertContinue" class="btn but_b" onclick="Continue('Seatsalert')" >Please select different seats</a>
                    <asp:Button ID="btnContinue" CssClass="btn but_b" Text="Continue" runat="server" style="padding-left: 10px;margin-left: 50px;"
                            CausesValidation="true" OnClientClick="return validate();" OnClick="imgContinue_Click"
                            ValidationGroup="email" />
                </div>
              </div>
            </div>
          </div>
        </div>

    <!-- Search Passenger Modal -->
    <div id="SearchPassenger" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search Passenger</h4>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                    <div id="SearchFilters" class="row custom-gutter"></div>
                    </div>
                </div>
                <div class="modal-footer"> <button type="button" class="button" onclick="SearchPassenger(); return false">Search</button> </div>
                <div id="PassengerDetailsList" class="px-3 table table-responsive"></div>
            </div>
        </div>
    </div>
    <!--Search Passenger Modal End-->



    <div id="bundleValidation" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                   <div id="divMsg"></div>
                </div>
                
            </div>
        </div>
    </div>
    <script>
        <!--Hide price change continue button if corporate booking -->
        $(document).ready(function () {
        <% CT.Core.AgentAppConfig clsAppCnf = new CT.Core.AgentAppConfig();
        clsAppCnf.AgentID = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId;
        bool showContinue = clsAppCnf.GetConfigData().Exists(k => k.AppKey.ToUpper() == "PRICECHANGECONTINUE" && k.AppValue.ToUpper() == "FALSE");
        if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate == "Y" && showContinue)
        {%>
            $(":button[value='Continue Booking']").hide();
        <%}%>
           
        });
    </script>
    <%}catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID, "(PassengerDetails.aspx)Error due to :"+ex.ToString(), Request["REMOTE_ADDR"]);
            Response.Redirect("ErrorPage.aspx?Err=" + ex.Message.ToString().Replace("\n", "").Replace("\r", ""), false);
        }%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>


