﻿using System;
using System.Data;
using System.Web.UI;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using System.Configuration;
using System.Collections;
using CT.Core;
using System.IO;

public partial class ViewBookingForFixedDepartureGUI :CT.Core.ParentPage// System.Web.UI.Page
{
  protected DataTable ActHdr;
    protected long id;
    protected AgentMaster agency;
    protected double total = 0, paidAmount = 0,discount = 0;
    protected DataTable dtFDDetails = new DataTable();
    protected long activityId;

    protected Activity activity = null;
    protected CT.TicketReceipt.BusinessLayer.AgentMaster agent;
    DataSet ds;
    string rootFolder = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FixedUploadDocument"]);
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                id = Convert.ToInt64(Request.QueryString["bookingId"]);
                ds = Activity.GetActivityQueueDetails(id);
                if (!IsPostBack)
                {
                    lblMessage.Text = string.Empty;
                    if (Request.QueryString["bookingId"] != null && Request.QueryString["bookingId"] != string.Empty)
                    {
                        id = Convert.ToInt64(Request.QueryString["bookingId"]);
                        ddlSettlementMode.DataSource = UserMaster.GetMemberTypeList("settlement_mode").Tables[0];
                        ddlSettlementMode.DataTextField = "FIELD_TEXT";
                        ddlSettlementMode.DataValueField = "FIELD_VALUE";
                        ddlSettlementMode.DataBind();

                        ddlSettlementMode.SelectedIndex = 0;

                        ddlCreditCard.DataSource = CreditCardMaster.GetList(ListStatus.Short, RecordStatus.Activated);
                        ddlCreditCard.DataTextField = "Card_Name";
                        ddlCreditCard.DataValueField = "Card_Charge";
                        ddlCreditCard.DataBind();

                        lblCharge.Text = ddlCreditCard.Items[0].Value;
                        hdnCardType.Value = ddlCreditCard.SelectedItem.Text;

                        Load(id);
                        hdnBookingId.Value = id.ToString();

                        DocumentImage.SavePath = rootFolder;
                        string uploadImg = Convert.ToString(id);
                        DocumentImage.FileName = uploadImg;
                        string AgentImgPath = Server.MapPath("~/" + rootFolder + "/") + uploadImg;
                        
                    }
                }
                else
                {
                     if (ds.Tables[0].Rows[0]["PaymentStatus"].ToString() == "0")
                     {
                         Load(id);
                     }
                }
                BindFallow();
                BindVoucher(id);
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed Load View Booking FixDep. Error: " + ex.Message, Request["REMOTE_ADDR"]);
        }
    }

    void BindVoucher(long id)
    {
        activity = new Activity();
        activity.GetActivityForQueue(id);
        int agentId = Convert.ToInt32(activity.TransactionHeader.Rows[0]["AgencyId"]);
        agent = new CT.TicketReceipt.BusinessLayer.AgentMaster(agentId);
        BindVoucher(activity);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="activity"></param>
    void BindVoucher(Activity activity)
    {
        try
        {
            lblActivityName.Text = activity.Name;
            lblBookingDate.Text = Convert.ToDateTime(activity.TransactionHeader.Rows[0]["Booking"]).ToString("dd MMM yyyy");
            lblTransactionDate.Text = lblBookingDate.Text;
            lblCity.Text = activity.City;
            lblCountry.Text = activity.Country;
            //lblDuration.Text = activity.DurationHours.ToString();
            lblLocation.Text = activity.City + ", " + activity.Country;
            lblTotal.Text = agent.AgentCurrency + " " + Convert.ToDecimal(activity.TransactionHeader.Rows[0]["TotalPrice"]).ToString("N" + agent.DecimalValue);

            lblDropPoint.Text = activity.DropoffLocation;
            lblPickupPoint.Text = activity.PickupLocation;
            lblPickupTime.Text = activity.PickupDate.ToString("hh:mm tt");
            lblBookingRef.Text = activity.TransactionHeader.Rows[0]["TripId"].ToString();
            lblPassengers.Text = (Convert.ToInt32(activity.TransactionHeader.Rows[0]["Adult"]) + Convert.ToInt32(activity.TransactionHeader.Rows[0]["Child"]) + Convert.ToInt32(activity.TransactionHeader.Rows[0]["Infant"])).ToString();
            //Audit.Add(EventType.Exception, Severity.High, 1, "tra" + activity.TransactionDetail.Rows.Count.ToString(), "0");
            DataTable dtPassengers = activity.TransactionDetail;

            for (int i = 0; i < dtPassengers.Rows.Count; i++)
            {
                if (i == 0)//Lead Passenger
                {
                    DataRow row = dtPassengers.Rows[i];
                    lblLeadPassenger.Text = row["FirstName"].ToString() + " " + row["LastName"].ToString();
                    lblGuestName.Text = lblLeadPassenger.Text;
                    lblNationality.Text = row["Nationality"].ToString();
                    lblPhone.Text = row["phone"].ToString();
                }
                //lblPassengers.Text = dtPassengers.Rows.Count.ToString();
            }
            //Audit.Add(EventType.Exception, Severity.High, 1, "4: ", "0");
        }
        catch { throw; }
    }

    private void Load(long id)
    {
        try
        {
            ds = Activity.GetActivityQueueDetails(id);
            ActHdr = ds.Tables[0];
            agency = new AgentMaster(Convert.ToInt32(ActHdr.Rows[0]["AgencyId"]));
            dlPaxDetails.DataSource = ds.Tables[1];
            dlPaxDetails.DataBind();
            //if (Convert.ToInt32(ActHdr.Rows[0]["AgencyId"]) == 1)
            //{
                DataTable dt = Activity.GetFixedDeparturePayments(id);
                gvFDPayments.DataSource = dt;
                gvFDPayments.DataBind();

                string creditCard = "";
                paidAmount = 0;
                decimal ccCharge = 0;
                discount = 0;
                //Get the balance amount to be paid
                foreach (DataRow row in dt.Rows)
                {
                    total = Convert.ToDouble(row["TotalPrice"]);
                    paidAmount += Convert.ToDouble(row["Amount"]);
                    creditCard = row["CreditCardType"].ToString();
                    ccCharge += Convert.ToDecimal(row["CCCharge"]);
                    discount += Convert.ToDouble(row["PromotionAmount"]);
                }
                total = Math.Ceiling(total - discount);
                paidAmount = Math.Ceiling(paidAmount);
                lblTotalPayable.Text = Math.Ceiling(paidAmount).ToString("N3");
                lblDiscount.Text = Math.Ceiling(discount).ToString("N3");
                lblBalancePayable.Text = Math.Ceiling(total - paidAmount).ToString("N3");
                lblCCCharge.Text = ccCharge.ToString("N3");
                lblTotalAmount.Text = Settings.LoginInfo.Currency + " " + Math.Ceiling(total - paidAmount);
                lblBalance.Text = Settings.LoginInfo.Currency + " " + Math.Ceiling(total - paidAmount);
                hdnPaidAmount.Value = Convert.ToString(paidAmount);
               

                activityId = Convert.ToInt64(dt.Rows[0]["ActivityId"]);
                txtCard.Text = "0";
                txtCash.Text = "0";
                txtCredit.Text = "0";
                ddlCreditCard.SelectedIndex = 0;
                ddlSettlementMode.SelectedIndex = 0;
            //}
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void lnkShowVoucher_Click(object sender, EventArgs e)
    {
        Response.Redirect("FixedDepartureVoucher.aspx?ID=" + hdnBookingId.Value, false);
    }
    protected void btnPayment_Click(object sender, EventArgs e)
    {
        try
        {
            id = Convert.ToInt64(Request.QueryString["bookingId"]);
            dtFDDetails = Activity.GetFixedDeparturePayments(id);

            foreach (DataRow row in dtFDDetails.Rows)
            {
                total = Convert.ToDouble(row["TotalPrice"]);
                paidAmount += Convert.ToDouble(row["Amount"]);
                discount += Convert.ToDouble(row["PromotionAmount"]);
            }

            total = Math.Ceiling(total);
            paidAmount = Math.Ceiling(paidAmount);
            activityId = Convert.ToInt64(dtFDDetails.Rows[0]["ActivityId"]);
            dtFDDetails.Rows.Clear();
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            decimal cash = Convert.ToDecimal(txtCash.Text), card = Convert.ToDecimal(txtCard.Text), credit = Convert.ToDecimal(txtCredit.Text);
            //card += card * (Convert.ToDecimal(ddlCreditCard.SelectedValue) / 100);
            card = Math.Ceiling(card);
            int mode = Convert.ToInt32(ddlSettlementMode.SelectedItem.Value);
            if (mode == 1)
            {
                DataRow row = dtFDDetails.NewRow();
                row["TranxId"] = -1;
                row["ATHDId"] = activityId;
                row["SettlementMode"] = (int)PaymentMode.Cash;
                row["Amount"] = cash;
                row["BalanceAmount"] = Math.Ceiling(Convert.ToDecimal(total - paidAmount)) - (cash);
                row["Currency"] = Settings.LoginInfo.Currency;
                row["ExchangeRate"] = Convert.ToDecimal(1.00);
                row["CreditCardId"] = -1;
                row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                row["PaymentType"] = (int)PaymentMode.Cash;
                row["CCCharge"] = 0;
                if (txtDepDate.Text.Trim().Length > 0)
                {
                    try
                    {
                        row["NextPaymentDate"] = Convert.ToDateTime(txtDepDate.Text.Trim(), dateFormat);
                    }
                    catch { }
                }
                else
                {
                    row["NextPaymentDate"] = DBNull.Value;
                }
                dtFDDetails.Rows.Add(row);
            }
            else if (mode == 2)
            {
                DataRow row = dtFDDetails.NewRow();
                row["TranxId"] = -1;
                row["ATHDId"] = activityId;
                row["SettlementMode"] = (int)PaymentMode.Credit;
                row["Amount"] = credit;
                row["BalanceAmount"] = Math.Ceiling(Convert.ToDecimal(total - paidAmount)) - (credit);
                row["Currency"] = Settings.LoginInfo.Currency;
                row["ExchangeRate"] = Convert.ToDecimal(1.00);
                row["CreditCardId"] = -1;
                row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                row["PaymentType"] = (int)PaymentMode.Credit;
                row["CCCharge"] = 0;
                if (txtDepDate.Text.Trim().Length > 0)
                {
                    try
                    {
                        row["NextPaymentDate"] = Convert.ToDateTime(txtDepDate.Text.Trim(), dateFormat);
                    }
                    catch { }
                }
                else
                {
                    row["NextPaymentDate"] = DBNull.Value;
                }
                dtFDDetails.Rows.Add(row);
            }
            else if (mode == 3)
            {
                DataTable dtCreditCards = CreditCardMaster.GetList(ListStatus.Short, RecordStatus.Activated);
                DataRow row = dtFDDetails.NewRow();
                row["TranxId"] = -1;
                row["ATHDId"] = activityId;
                row["SettlementMode"] = (int)PaymentMode.Card;
                row["Amount"] = card;
                row["BalanceAmount"] = Math.Ceiling(Convert.ToDecimal(total - paidAmount)) - (card);
                row["Currency"] = Settings.LoginInfo.Currency;
                row["ExchangeRate"] = Convert.ToDecimal(1.00);
                row["CreditCardId"] = dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Id"];
                row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                row["CCCharge"] = Math.Ceiling(Convert.ToDecimal(txtCard.Text.Trim()) * Convert.ToDecimal(dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Charge"]) / 100);
                row["PaymentType"] = (int)PaymentMode.Card;
                if (txtDepDate.Text.Trim().Length > 0)
                {
                    try
                    {
                        row["NextPaymentDate"] = Convert.ToDateTime(txtDepDate.Text.Trim(), dateFormat);
                    }
                    catch { }
                }
                else
                {
                    row["NextPaymentDate"] = DBNull.Value;
                }
                dtFDDetails.Rows.Add(row);
            }
            else if (mode == 4)
            {
                DataRow row = dtFDDetails.NewRow();
                row["TranxId"] = -1;
                row["ATHDId"] = activityId;
                row["SettlementMode"] = (int)PaymentMode.Cash_Credit;
                row["Amount"] = cash;
                row["BalanceAmount"] = Math.Ceiling(Convert.ToDecimal(total - paidAmount)) - (cash);
                row["Currency"] = Settings.LoginInfo.Currency;
                row["ExchangeRate"] = Convert.ToDecimal(1.00);
                row["CreditCardId"] = -1;
                row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                row["PaymentType"] = (int)PaymentMode.Cash;
                row["CCCharge"] = 0;
                if (txtDepDate.Text.Trim().Length > 0)
                {
                    try
                    {
                        row["NextPaymentDate"] = Convert.ToDateTime(txtDepDate.Text.Trim(), dateFormat);
                    }
                    catch { }
                }
                else
                {
                    row["NextPaymentDate"] = DBNull.Value;
                }
                dtFDDetails.Rows.Add(row);

                DataRow row1 = dtFDDetails.NewRow();
                row1["TranxId"] = -1;
                row1["ATHDId"] = activityId;
                row1["SettlementMode"] = (int)PaymentMode.Cash_Credit;
                row1["Amount"] = credit;
                row1["BalanceAmount"] = Math.Ceiling(Convert.ToDecimal(total - paidAmount)) - (cash + credit);
                row1["Currency"] = Settings.LoginInfo.Currency;
                row1["ExchangeRate"] = Convert.ToDecimal(1.00);
                row1["CreditCardId"] = -1;
                row1["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                row1["PaymentType"] = (int)PaymentMode.Credit;
                row1["CCCharge"] = 0;
                if (txtDepDate.Text.Trim().Length > 0)
                {
                    try
                    {
                        row1["NextPaymentDate"] = Convert.ToDateTime(txtDepDate.Text.Trim(), dateFormat);
                    }
                    catch { }
                }
                else
                {
                    row1["NextPaymentDate"] = DBNull.Value;
                }
                dtFDDetails.Rows.Add(row1);
            }
            else if (mode == 5)
            {
                DataTable dtCreditCards = CreditCardMaster.GetList(ListStatus.Short, RecordStatus.Activated);
                DataRow row = dtFDDetails.NewRow();
                row["TranxId"] = -1;
                row["ATHDId"] = activityId;
                row["SettlementMode"] = (int)PaymentMode.Cash_Card;
                row["Amount"] = cash;
                row["BalanceAmount"] = Math.Ceiling(Convert.ToDecimal(total - paidAmount)) - (cash);
                row["Currency"] = Settings.LoginInfo.Currency;
                row["ExchangeRate"] = Convert.ToDecimal(1.00);
                row["CreditCardId"] = dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Id"]; ;
                row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                row["CCCharge"] = 0;// Math.Ceiling(Convert.ToDecimal(txtCard.Text.Trim()) * Convert.ToDecimal(dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Charge"]) / 100);
                row["PaymentType"] = (int)PaymentMode.Cash;
                if (txtDepDate.Text.Trim().Length > 0)
                {
                    try
                    {
                        row["NextPaymentDate"] = Convert.ToDateTime(txtDepDate.Text.Trim(), dateFormat);
                    }
                    catch { }
                }
                else
                {
                    row["NextPaymentDate"] = DBNull.Value;
                }
                dtFDDetails.Rows.Add(row);

                DataRow row1 = dtFDDetails.NewRow();
                row1["TranxId"] = -1;
                row1["ATHDId"] = activityId;
                row1["SettlementMode"] = (int)PaymentMode.Cash_Card;
                row1["Amount"] = card;
                row1["BalanceAmount"] = Math.Ceiling(Convert.ToDecimal(total - paidAmount)) - (cash + card);
                row1["Currency"] = Settings.LoginInfo.Currency;
                row1["ExchangeRate"] = Convert.ToDecimal(1.00);
                row1["CreditCardId"] = dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Id"]; ;
                row1["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                row1["CCCharge"] = Math.Ceiling(Convert.ToDecimal(txtCard.Text.Trim()) * Convert.ToDecimal(dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Charge"]) / 100);
                row1["PaymentType"] = (int)PaymentMode.Card;
                if (txtDepDate.Text.Trim().Length > 0)
                {
                    try
                    {
                        row1["NextPaymentDate"] = Convert.ToDateTime(txtDepDate.Text.Trim(), dateFormat);
                    }
                    catch { }
                }
                else
                {
                    row1["NextPaymentDate"] = DBNull.Value;
                }
                dtFDDetails.Rows.Add(row1);
            }
            else if (mode == 6)
            {
                DataTable dtCreditCards = CreditCardMaster.GetList(ListStatus.Short, RecordStatus.Activated);
                DataRow row = dtFDDetails.NewRow();
                row["TranxId"] = -1;
                row["ATHDId"] = activityId;
                row["SettlementMode"] = (int)PaymentMode.Cash_Credit_Card;
                row["Amount"] = cash;
                row["BalanceAmount"] = Math.Ceiling(Convert.ToDecimal(total - paidAmount)) - cash;
                row["Currency"] = Settings.LoginInfo.Currency;
                row["ExchangeRate"] = Convert.ToDecimal(1.00);
                row["CreditCardId"] = dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Id"];
                row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                row["CCCharge"] = 0;// Math.Ceiling(Convert.ToDecimal(txtCard.Text.Trim()) * Convert.ToDecimal(dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Charge"]) / 100);
                row["PaymentType"] = (int)PaymentMode.Cash;
                if (txtDepDate.Text.Trim().Length > 0)
                {
                    try
                    {
                        row["NextPaymentDate"] = Convert.ToDateTime(txtDepDate.Text.Trim(), dateFormat);
                    }
                    catch { }
                }
                else
                {
                    row["NextPaymentDate"] = DBNull.Value;
                }
                dtFDDetails.Rows.Add(row);

                DataRow row1 = dtFDDetails.NewRow();
                row1["TranxId"] = -1;
                row1["ATHDId"] = activityId;
                row1["SettlementMode"] = (int)PaymentMode.Cash_Credit_Card;
                row1["Amount"] = credit;
                row1["BalanceAmount"] = Math.Ceiling(Convert.ToDecimal(total - paidAmount)) - (cash + credit);
                row1["Currency"] = Settings.LoginInfo.Currency;
                row1["ExchangeRate"] = Convert.ToDecimal(1.00);
                row1["CreditCardId"] = dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Id"]; ;
                row1["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                row1["CCCharge"] = 0;// Math.Ceiling(Convert.ToDecimal(txtCard.Text.Trim()) * Convert.ToDecimal(dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Charge"]) / 100);
                row1["PaymentType"] = (int)PaymentMode.Credit;
                if (txtDepDate.Text.Trim().Length > 0)
                {
                    try
                    {
                        row1["NextPaymentDate"] = Convert.ToDateTime(txtDepDate.Text.Trim(), dateFormat);
                    }
                    catch { }
                }
                else
                {
                    row1["NextPaymentDate"] = DBNull.Value;
                }
                dtFDDetails.Rows.Add(row1);

                DataRow row2 = dtFDDetails.NewRow();
                row2["TranxId"] = -1;
                row2["ATHDId"] = activityId;
                row2["SettlementMode"] = (int)PaymentMode.Cash_Credit_Card;
                row2["Amount"] = card;
                row2["BalanceAmount"] = Math.Ceiling(Convert.ToDecimal(total - paidAmount)) - (cash + credit + card);
                row2["Currency"] = Settings.LoginInfo.Currency;
                row2["ExchangeRate"] = Convert.ToDecimal(1.00);
                row2["CreditCardId"] = dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Id"]; ;
                row2["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                row2["CCCharge"] = Math.Ceiling(Convert.ToDecimal(txtCard.Text.Trim()) * Convert.ToDecimal(dtCreditCards.Select("Card_Name='" + hdnCardType.Value + "'")[0]["Card_Charge"]) / 100);
                row2["PaymentType"] = (int)PaymentMode.Card;
                if (txtDepDate.Text.Trim().Length > 0)
                {
                    try
                    {
                        row2["NextPaymentDate"] = Convert.ToDateTime(txtDepDate.Text.Trim(), dateFormat);
                    }
                    catch { }
                }
                else
                {
                    row2["NextPaymentDate"] = DBNull.Value;
                }
                dtFDDetails.Rows.Add(row2);
            }
            else if (mode == 7 || mode == 8)
            {
                DataRow row = dtFDDetails.NewRow();
                row["TranxId"] = -1;
                row["ATHDId"] = activityId;
                row["SettlementMode"] = (int)PaymentMode.Employee;
                row["Amount"] = cash;
                row["BalanceAmount"] = Math.Ceiling(Convert.ToDecimal(total - paidAmount)) - (cash);
                row["Currency"] = Settings.LoginInfo.Currency;
                row["ExchangeRate"] = Convert.ToDecimal(1.00);
                row["CreditCardId"] = -1;
                row["CreatedBy"] = (int)Settings.LoginInfo.UserID;
                row["PaymentType"] = (int)PaymentMode.Employee;
                row["CCCharge"] = 0;
                if (txtDepDate.Text.Trim().Length > 0)
                {
                    try
                    {
                        row["NextPaymentDate"] = Convert.ToDateTime(txtDepDate.Text.Trim(), dateFormat);
                    }
                    catch { }
                }
                else
                {
                    row["NextPaymentDate"] = DBNull.Value;
                }
                dtFDDetails.Rows.Add(row);
            }

            Activity.SaveFixedDeparturePaymentDetails(id, activityId, total, paidAmount, dtFDDetails, discount);
            //DataSet ds = Activity.GetActivityQueueDetails(id);
            string serverPath = "";
            string logoPath = "";
            if (Request.Url.Port > 0)
            {
                serverPath =Request.Url.Scheme+ "://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }
            else
            {
                serverPath =Request.Url.Scheme+ "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }
            //to show agent logo
            if (agent.ID > 1)
            {
                logoPath = serverPath + ConfigurationManager.AppSettings["AgentImage"] + agent.ImgFileName;
                imgHeaderLogo.ImageUrl = logoPath;
            }
            else
            {
                imgHeaderLogo.ImageUrl = serverPath + "images/logo.jpg";
            }
            if (txtDepDate.Text.Trim().Length == 0)
            {

                printableArea.Style.Add("display", "block");
                string myPageHTML = "";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                printableArea.RenderControl(htw);
                myPageHTML = sw.ToString();

                string EmailText = "<table style='width:100%;'><tr><td>Dear&nbsp;&nbsp;" + activity.TransactionDetail.Rows[0]["FirstName"].ToString() + "</td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td colspan='2'>Thank you for booking your Holiday with us  </td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td colspan='2'>We look forward to welcoming you </td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td>Please find the details of your booking </td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr></table>";

                string EmailText1 = "<table style='width:100%;'><tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                       + "<tr><td colspan='2'>Thank you for choosing Cozmo Travel. </td></tr>"
                                        + "<tr><td colspan='2'>Holidays Specialist Team</td></tr></table>";

                myPageHTML = EmailText + "</br>" + myPageHTML + "</br>" + EmailText1;
                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                toArray.Add(activity.TransactionDetail.Rows[0]["Email"].ToString());
                toArray.Add(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentEmail);
                toArray.Add(ConfigurationManager.AppSettings["holidaySupport"].ToString());
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "FixedDeparture Confirmation - " + activity.TransactionHeader.Rows[0]["TripId"].ToString(), myPageHTML, new Hashtable());
                printableArea.Style.Add("display", "none");

                string myPageHTML1 = "<p>Dear " + ds.Tables[1].Rows[0]["LastName"].ToString() + ",</p><p> FixedDeparture Booking is Confirmed.</p><p>Greetings,</br> Cozmo Travel</p>";
                System.Collections.Generic.List<string> toArray1 = new System.Collections.Generic.List<string>();
                toArray.Add(ds.Tables[1].Rows[0]["Email"].ToString());
                toArray.Add(ConfigurationManager.AppSettings["holidaySupport"].ToString());
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray1, "FixedDeparture Confirmation - " + ds.Tables[0].Rows[0]["TripId"].ToString(), myPageHTML1, new Hashtable());
            }
            else
            {
                if (txtDepDate.Text.Trim().Length > 0)
                {
                    System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                    toArray.Add(ds.Tables[1].Rows[0]["Email"].ToString());
                    string EmailText = "<table style='width:100%;'><tr><td>Dear&nbsp;&nbsp;" + ds.Tables[1].Rows[0]["FirstName"].ToString() + "</td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td colspan='2'>This is a reminder for you to make the balance payment which applies to this booking.</br> It is important to note the cancellation policy shown and any charges</br> which will apply if this booking is cancelled. </td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;Below are the booking details</td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td>Trip Id:</td><td>" + ds.Tables[0].Rows[0]["TripId"].ToString() + "</td></tr>"
                                        + "<tr><td>Departure Date:</td><td>" + Convert.ToDateTime(ds.Tables[0].Rows[0]["Booking"]).ToString("dd-MMM-yyyy") + "</td></tr>"
                                        + "<tr><td>Lead Name:</td><td>" + ds.Tables[1].Rows[0]["FirstName"].ToString() + " " + ds.Tables[1].Rows[0]["LastName"].ToString() + "</td></tr>"
                                        + "<tr><td>Total Passengers:</td><td>Adult: " + ds.Tables[0].Rows[0]["Adult"].ToString() + " Child: " + ds.Tables[0].Rows[0]["Child"].ToString() + " Infant: " + ds.Tables[0].Rows[0]["Infant"].ToString() + "</td></tr>"
                                        + "<tr><td>Room Type:</td><td>" + ds.Tables[1].Rows[0]["RoomType"].ToString() + "</td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td colspan='2'>Thank you for choosing Cozmo Travel. </td></tr>"
                                        + "<tr><td colspan='2'>Holidays Specialist Team</td></tr></table>";
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEMail"], ConfigurationManager.AppSettings["holidaySupport"], toArray, "Payment reminder", EmailText, new Hashtable());
                }
            }
            txtDepDate.Text = "";
            Load(id);
        }
        catch (Exception ex)
        {
           Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed FixDep Update paymentDetails. Error: " + ex.Message, Request["REMOTE_ADDR"]);
        }
        
    }
    protected void Update_Click(object sender, EventArgs e)
    {
        try
        {
            //DataSet ds = Activity.GetActivityQueueDetails(id);
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            id = Convert.ToInt64(Request.QueryString["bookingId"]);
            DataTable dtFDVisaFollowup = Activity.GetFixedDepartureVisaFollowUp(id);
            if (txtVisaDate.Text != "")
            {
                dtFDVisaFollowup.Rows.Clear();
                DataRow visaRow = dtFDVisaFollowup.NewRow();
                visaRow["ATHId"] = id;
                visaRow["Visa_date"] = Convert.ToDateTime(txtVisaDate.Text, dateFormat);
                visaRow["MailRequired"] = chkmail.Checked == true ? "Y" : "N";
                visaRow["Visa_created_by"] = (int)Settings.LoginInfo.UserID;
                dtFDVisaFollowup.Rows.Add(visaRow);
                Activity.SaveVisaFallowUp(dtFDVisaFollowup);

                if (chkmail.Checked)
                {
                    System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                    toArray.Add(ds.Tables[1].Rows[0]["Email"].ToString());
                    string EmailText = "<table style='width:100%;'><tr><td>Dear &nbsp;" + ds.Tables[1].Rows[0]["FirstName"].ToString() + "</td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td>Below are the documents required from for visa application before (Date selected in calendar in Visa Date Follow-up):</td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Application form filled and signed</td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Visa fees ( non refundable )</td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Two Photos white background (PHOTO’S NOT OLDER THAN 6 MONTHS).</td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Original passport- must be valid for six months.</td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Passport older than 10 years will not be accepted. (Issued in 2004)</td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Residence Visa should be valid for three months from return date. </td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•Six months bank statement with Bank stamp/original.</td></tr>"
                                        + "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;•NOC/Salary Certificate from the Company- if partner need trade license copy.</td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td>(Please note the above documents are subject to change as per the embassy terms, please re check with our visa consultant on the current requirements)</td></tr>"
                                        +"<tr><td></td></tr>"
                                        + "<tr><td></td></tr>"
                                        + "<tr><td>Thank you for choosing Cozmo Travel.</td></tr>"
                                        + "<tr><td>Holidays Specialist Team</td></tr></table>";

                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEMail"], ConfigurationManager.AppSettings["holidaySupport"], toArray, "VisaFollowUP", EmailText, new Hashtable());
                }
                
            }
            txtVisaDate.Text = "";
            chkmail.Checked = false;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed FixDep Visa FollowUP. Error: " + ex.Message, Request["REMOTE_ADDR"]);
        }
        Load(id);
        BindFallow();
    }
    void BindFallow()
    {
        try
        {
            id = Convert.ToInt64(Request.QueryString["bookingId"]);
            DataTable dtFDVisaFollowup = Activity.GetFixedDepartureVisaFollowUp(id);
            dlFollowUp.DataSource = dtFDVisaFollowup;
            dlFollowUp.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(DocumentImage.FileExtension))
            {
                string serverPath = "";

                if (Request.Url.Port > 0)
                {
                    serverPath =Request.Url.Scheme+ "://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                }
                else
                {
                    serverPath =Request.Url.Scheme+ "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                }
              
                //DataSet ds = Activity.GetActivityQueueDetails(id);
                string fileName = DocumentImage.FileName + DocumentImage.FileExtension;
                string filePath = Server.MapPath("~/" + rootFolder + "/") + DocumentImage.FileName + DocumentImage.FileExtension; 
                int count=  Activity.SaveDocumentHeader(id, filePath, fileName);
                string[] attachments=new string[1];
                attachments[0]=filePath;
                //string logoPath = "";
                //divImg.Style.Add("display", "block");
                //logoPath = serverPath + rootFolder + fileName;
                //imgLogo.ImageUrl = logoPath;
                //string myPageHTML1 = "";
                //System.IO.StringWriter sw = new System.IO.StringWriter();
                //HtmlTextWriter htw = new HtmlTextWriter(sw);
                //divImg.RenderControl(htw);
                //myPageHTML1 = sw.ToString();
                if (count > 0)
                {
                    lblMessage.Text = "Successfully Upload";
                    lblMessage.ForeColor = System.Drawing.Color.LimeGreen;
                    string myPageHTML = "<p>Dear " + ds.Tables[1].Rows[0]["LastName"].ToString() + ",</p><p> FixedDeparture Booking is Confirmed.</p><p>Greetings,</br> Cozmo Travel</p>";
                    System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                    toArray.Add(ds.Tables[1].Rows[0]["Email"].ToString());
                    toArray.Add(ConfigurationManager.AppSettings["holidaySupport"].ToString());
                   // CT.Core1.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "FixedDeparture Confirmation - " + ds.Tables[0].Rows[0]["TripId"].ToString(), myPageHTML, new Hashtable(), attachments);
                    //divImg.Style.Add("display", "none");
                }
            }
            else
            {
                lblMessage.Text = "Please upload Document";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
            Load(id);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed FixDep Upload Document. Error: " + ex.Message, Request["REMOTE_ADDR"]);
        }
    }
}
