using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using CT.Core;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.Script.Services;
using System.Web.Services;
using Newtonsoft.Json;

public partial class AgentMasterUI :CT.Core.ParentPage// System.Web.UI.Page
{
    private string AGENT_SESSION = "_AgentMaster";

    private string AGENT_SEARCH_SESSION = "_AgentSearchList";
    private string DOCUMENTS_SESSION = "_TranxDocuments";
    protected int AgentId;
    DataTable dtProducts;
    CheckBoxList chkSources;
    DataTable dtAgentSources;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                Session["AgentRegisterFiles"] = null;
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
                if (!IsPostBack && Request.QueryString["RegId"] != null) // For Editing from REgtisterList Page
                {
                    long regId = Utility.ToLong(Request.QueryString["RegId"]);
                    EditAgentDet(regId);
                    ScriptManager.RegisterStartupScript(this, GetType(), "UploadedReceipts", "UploadedReceipts('" + regId + "','Reg');", true);
                }
                lblSuccessMsg.Text = string.Empty;
                AgentId = Settings.LoginInfo.AgentId;

            }
            else
            {
                int parentAgent;
                if (Convert.ToInt32(ddlParentAgent.SelectedItem.Value) > 0)
                {
                    parentAgent = Convert.ToInt32(ddlParentAgent.SelectedItem.Value);
                }
                else
                {
                    parentAgent = Settings.LoginInfo.AgentId;
                }
                BindControls(parentAgent);
                //ScriptManager.RegisterStartupScript(this, GetType(), "initdocs", "initdocs('" + parentAgent + "');", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "InitVATDropZone", "InitVATDropZone();", true);
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);

        }
    }
  
    private void InitializePageControls()
    {
        try
        {
            //string rootFolder = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["AgentImage"]);
            string rootFolder = Utility.ToString(ConfigurationManager.AppSettings["AgentImage"]);
            agentImage.SavePath = rootFolder;
            string AgentImg = Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss")).Replace(":", "");
            AgentImg = AgentImg.Replace("-", "");
            agentImage.FileName = AgentImg;
            string AgentImgPath = Server.MapPath("~/" + rootFolder + "/") + AgentImg;
            hdfAgentImg.Value = AgentImgPath;

             
             BindCountry();
             BindNationality();
             BindCurrency();
             BindParentAgentList();
             BindAgentProduct(Settings.LoginInfo.AgentId);
             BindTheme();
            BindSalesExecutives();
             Clear();
            //if (Settings.LoginInfo.AgentId <= 1) ddlAgentType.SelectedValue = "B2B";
            //else ddlAgentType.SelectedValue = "B2B2B";
        }
        catch { throw; }
    }
    //Load Theme Folders
    void BindTheme()
    {
        try
        {
            string themePath = Server.MapPath("~/App_Themes");
            if (Directory.Exists(themePath))
            {
                DirectoryInfo[] dirTheme = new DirectoryInfo(@themePath).GetDirectories("*.*", SearchOption.AllDirectories);
                if (dirTheme != null)
                {
                    foreach (DirectoryInfo theme in dirTheme)
                    {
                        ListItem item = new ListItem(theme.Name, theme.Name);
                        ddlTheme.Items.Add(item);
                    }
                }
                ddlTheme.SelectedValue = Convert.ToString("Default");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindCountry()
    {
        try
        {
            ddlCountry.DataSource = CountryMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            ddlCountry.DataValueField = "COUNTRY_ID";
            ddlCountry.DataTextField = "COUNTRY_NAME";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--select country--", "-1"));
           

        }
        catch { throw; }
    }


    private void BindSalesExecutives()
    {
        try
        {
            ddlSalesExec.DataSource = UserMaster.GetSalesExecList(0);
            ddlSalesExec.DataValueField = "userId";
            ddlSalesExec.DataTextField = "Uer_name";
            ddlSalesExec.DataBind();
            ddlSalesExec.Items.Insert(0, new ListItem("--select Executive--", "-1"));


        }
        catch { throw; }
    }
    private void BindParentAgentList()
    {
        try
        {
            string agentType = string.Empty;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                agentType = Utility.ToString(Settings.LoginInfo.AgentType);
            }
            ddlParentAgent.DataSource = AgentMaster.GetList(1, agentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//TODO
            ddlParentAgent.DataValueField = "agent_id";
            ddlParentAgent.DataTextField = "agent_name";
            ddlParentAgent.DataBind();
            ddlParentAgent.Items.Insert(0, new ListItem("-- All --", "-1"));
            if (Settings.LoginInfo.AgentId > 0)
                ddlParentAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
            else ddlParentAgent.SelectedValue = "-1";
            if (Settings.LoginInfo.AgentType==AgentType.BaseAgent || Settings.LoginInfo.AgentType==AgentType.Agent) ddlParentAgent.Enabled = true;
            else
                ddlParentAgent.Enabled = false;

        }
        catch { throw; }
    }
    private void BindCurrency()
    {
        try
        {
            ddlCurrency.DataSource = CurrencyMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            ddlCurrency.DataValueField = "CURRENCY_ID";
            ddlCurrency.DataTextField = "CURRENCY_CODE";
            ddlCurrency.DataBind();
            ddlCurrency.SelectedIndex = 0;
        }
        catch { throw; }
    }
    private void BindNationality()
    {
        try
        {
            ddlNationality.DataSource = NationalityMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            ddlNationality.DataValueField = "nationality_id";
            ddlNationality.DataTextField = "nationality_name";
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("--select Nationality--", "-1"));


        }
        catch { throw; }
    }
 

    protected AgentMaster CurrentObject
    {
        get
        {
            return (AgentMaster)Session[AGENT_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(AGENT_SESSION);
            }
            else
            {
                Session[AGENT_SESSION] = value;
            }

        }
    }

    private DataTable DocumentDetails
    {
        get
        {
            return (DataTable)Session[DOCUMENTS_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["doc_id"] };
            Session[DOCUMENTS_SESSION] = value;
        }
    }

    private void Clear()
    {
        try
        {
             
            AgentMaster tempAgent = new AgentMaster(-1);
            txtCode.Text = string.Empty;
            txtName.Text = string.Empty;
            txtState.Text = string.Empty;
            txtCity.Text = string.Empty;
            ddlCountry.SelectedIndex = 0;
            txtPOBox.Text = string.Empty;
            txtEmail1.Text = string.Empty;
            txtEmail2.Text = string.Empty;
            txtWebsite.Text = string.Empty;
            txtPhone1.Text = string.Empty;
            txtPhone2.Text = string.Empty;
            txtFax.Text = string.Empty;
            txtTelex.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtAgentDecimal.Text = "2";
            txtWarLevel.Text = "0";
            ddlMatchingStatus.SelectedIndex = 0;
            ddlCurrency.SelectedIndex = 0;
            txtLicenseNo.Text = string.Empty;
            dcLicExpDate.Clear();
            txtPassportNo.Text = string.Empty;
            dcpptIssuedDate.Clear();
            dcpptExpOn.Clear();
            ddlNationality.SelectedIndex = 0;
            ddlGrntrStatus.SelectedIndex = 0;
            ddlVisaSubmission.SelectedIndex = 0;
            txtCurrentBalance.Text = Formatter.ToCurrency(0);
            lblSuccessMsg.Text = string.Empty;
            lblSuccessMsg.Visible = false;
            btnSave.Text = "Save";
            chkagentProduct.SelectedIndex = -1;
            chkBlock.Checked = false;
            txtRemarks.Text = string.Empty;
            ddlParentAgent.SelectedValue = Settings.LoginInfo.AgentId == 0 ? "-1" : Utility.ToString(Settings.LoginInfo.AgentId);
            ddlAgentType.Enabled = true;
            chkAgentRouting.Checked = false;
            chkAgentReturnFare.Checked = false;
            //if (Settings.LoginInfo.AgentId > 1)
            //{
            //    ddlAgentType.SelectedValue = Convert.ToString((int)AgentType.B2B2B);
            //    ddlAgentType.Enabled = false;
            //}
            //else
            //{
            //    ddlAgentType.SelectedValue = "3";// default 'b2b'
            //}
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgentType.SelectedValue = "3";
                ddlParentAgent.Enabled = true;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgentType.SelectedValue = "4";
                ddlAgentType.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgentType.SelectedValue = "4";
            }
            ddlTheme.SelectedValue = Convert.ToString("Default");
            //if (Settings.LoginInfo.AgentType==AgentType.BaseAgent)
            //{
            //    ddlAgentType.SelectedValue = "B2B";
            //    ddlParentAgent.Enabled = true;
            //}
            //else
            //{
            //    ddlAgentType.SelectedValue = "B2B2B";
            //    ddlParentAgent.Enabled = false;
            //}
            CurrentObject = null;
            imgPreview.Style.Add("display", "none");
            lnkView.Text = "";
            agentImage.Clear();
            hdfCount.Value = "0";
            hdnAgentId.Value = "0";
            hdfRegId.Value = "0";
            ddlPaymentMode.SelectedIndex = 0; //Added by brahma PaymentMode
            if (Settings.LoginInfo.MemberType == MemberType.SUPER)
            {
                divProducts.Visible = true;
            }
            else
            {
                divProducts.Visible = false;
            }
            //Added By Hari MAlla on 2019-11-04.
            ddlCardType.SelectedValue = "-1";
            txtCardExpiry.Text =string.Empty;
            txtCardNo.Text = string.Empty;
            ddlSalesExec.SelectedIndex = 0;
            txtCopyRight.Text = string.Empty;
            divUploadFiles.Visible = false;
        }
        catch
        {
            throw;
        }
    }

    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[AGENT_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["AGENT_ID"] };
            Session[AGENT_SEARCH_SESSION] = value;
        }
    }
    private void Edit(long id)
    {
        try
        {
            AgentMaster  agent = new AgentMaster(id);
            CurrentObject = agent;
            //string rootFolder = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["AgentImage"]);
            string rootFolder = Utility.ToString(ConfigurationManager.AppSettings["AgentImage"]);
            agentImage.SavePath = rootFolder;
            txtCode.Text = agent.Code.ToUpper();
            hdnAgentId.Value = Utility.ToString(agent.ID);
            txtName.Text = agent.Name;
            txtState.Text = agent.State;
            txtCity.Text = agent.City;
            try { 
            ddlCountry.SelectedValue = Utility.ToString(agent.Country);
            }
            catch(Exception ex) { };
            txtPOBox.Text = agent.POBox;
            txtEmail1.Text = agent.Email1;
            txtEmail2.Text = agent.Email2;            
            txtWebsite.Text = agent.Website;
            txtPhone1.Text = agent.Phone1;
            txtPhone2.Text= agent.Phone2;
            txtFax.Text = agent.Fax;
            txtTelex.Text = agent.Telex;                
            txtAddress.Text = agent.Address;
            txtWarLevel.Text = Formatter.ToCurrency(agent.AlertLevel);
            ddlParentAgent.SelectedValue = Utility.ToString(agent.AgentParantId) == "0" ? "-1" : Utility.ToString(agent.AgentParantId);
            ddlAgentType.SelectedValue = Utility.ToString(agent.AgentType);
            txtAgentDecimal.Text =Utility.ToString(agent.DecimalValue);
            //Added by brahmam 24.09.2016
            ddlPaymentMode.SelectedValue = Convert.ToString((int)agent.PaymentMode);
            txtTempCredit.Text = agent.CreditBuffer.ToString("N" + agent.DecimalValue);
            txtCreditDays.Text = agent.CreditDays.ToString();
            txtCreditLimit.Text = agent.CreditLimit.ToString("N" + agent.DecimalValue);
            txtTempCredit.Enabled = false;
            txtCreditDays.Enabled = false;
            txtCreditLimit.Enabled = false;
            if (ddlPaymentMode.SelectedValue == "9") ddlPaymentMode.Enabled = false;
            //brahmam 24.09.2013
            tblActiveSources.Rows.Clear();
            BindAgentProduct(Utility.ToInteger(ddlParentAgent.SelectedItem.Value));
            String[] productList = agent.AgentProduct.Split(',');
            foreach (string Product in productList)
            {
                if (chkagentProduct.Items.FindByValue(Product) != null)
                {
                    chkagentProduct.Items.FindByValue(Product).Selected = true;
                    //if (Utility.ToInteger(Product) == 4)
                    //    Utility.StartupScript(this.Page, "showFeeDetails();", "");
                }
            }
            hdnCount.Value = "1";
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                LoadControls();
            }
            if (agent.ImgFileName != string.Empty)
            {
                agentImage.DefaultImageUrl = "~/images/common/Preview.png";
                lnkView.Visible = true;
                lnkView.Text = "View Image";
                hdfImgPath.Value = rootFolder + "" + agent.ImgFileName;
                agentImage.FileExtension = null;

            }
            txtCurrentBalance.Text = Formatter.ToCurrency(agent.CurrentBalance);
            txtLicenseNo.Text = agent.LicenseNo;
            dcLicExpDate.Value = Utility.ToDate(agent.LicenseNoExpDate);
            if (dcLicExpDate.Value == DateTime.MinValue) dcLicExpDate.Clear();
            txtPassportNo.Text = agent.PassportNo;
            dcpptIssuedDate.Value=Utility.ToDate(agent.PassportIssueDate);
            if (dcpptIssuedDate.Value == DateTime.MinValue) dcpptIssuedDate.Clear();
            dcpptExpOn.Value = Utility.ToDate(agent.PassportExpDate);
            if (dcpptExpOn.Value == DateTime.MinValue) dcpptExpOn.Clear();
            if (Utility.ToString(agent.Nationality) == "0")
                ddlNationality.SelectedIndex = 0;
            else ddlNationality.SelectedValue = Utility.ToString(agent.Nationality);
            
            if(agent.GrntrStatus == "")
                ddlGrntrStatus.SelectedIndex=0;
            else
                ddlGrntrStatus.SelectedValue = agent.GrntrStatus;

            if (agent.VisaSubmissionStatus == "")
                ddlVisaSubmission.SelectedIndex = 0;
            else
                ddlVisaSubmission.SelectedValue = agent.VisaSubmissionStatus;


            if (agent.AgentBlockStatus == "Y") chkBlock.Checked = true;
            else chkBlock.Checked = false;
            txtRemarks.Text = agent.AgentRemarks;
            if (agent.ReduceBalanceStatus == string.Empty)
                ddlMatchingStatus.SelectedIndex = 0;
            else
                ddlMatchingStatus.SelectedValue = agent.ReduceBalanceStatus;
            if (agent.AgentCurrency == string.Empty)
                ddlCurrency.SelectedIndex = 0;
            else
            {
                ListItem item = ddlCurrency.Items.FindByText(agent.AgentCurrency);
                ddlCurrency.ClearSelection();
                item.Selected = true;
            }
            if (agent.NFlex1 != "")
            {
                ddlTheme.SelectedValue = agent.NFlex1;
            }
            if(agent.SalesExecId!=0)
            ddlSalesExec.SelectedValue = Utility.ToString(agent.SalesExecId);
            else
                ddlSalesExec.SelectedIndex = -1;
            txtCopyRight.Text = agent.NFlex2;
            btnSave.Text = "Update";
            btnClear.Text = "Cancel";
            ddlParentAgent.Enabled = false;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                lnkUpdateSourceCredentials.Visible = true;
            }
            else
            {
                lnkUpdateSourceCredentials.Visible = false;
            }
            ddlAgentType.Enabled = false;
            chkAgentRouting.Checked = agent.IsRoutingEnabled;
            chkAgentReturnFare.Checked = agent.IsReturnFareEnabled;
            //Added BY hari Malla on 2019-11-04.
            if (!string.IsNullOrEmpty(agent.agent_cc_type) && agent.agent_cc_type != "-1")
            {
                ddlCardType.SelectedValue = agent.agent_cc_type;
            }
            if (!string.IsNullOrEmpty(agent.agent_cc_exp_date))
            {
                txtCardExpiry.Text = agent.agent_cc_exp_date;
            }
            if (!string.IsNullOrEmpty(agent.agent_CreditCard))
            {
                txtCardNo.Text = GenericStatic.DecryptData(agent.agent_CreditCard);
            }
            divUploadFiles.Visible = true;
            ScriptManager.RegisterStartupScript(this, GetType(), "UploadedReceipts", "UploadedReceipts('" + id + "','Agent');", true);
        }
        catch
        {
            throw;
        }
    }


    //done by venkatesh Method starting
    private void EditAgentDet(long id)
    {
        try
        {
            AgentRegistration agent = new AgentRegistration(id);
            hdfRegId.Value = Utility.ToString(agent.ID);
            //txtName.Text = agent.OwnerName;
            txtName.Text = agent.AgencyName;
            txtPOBox.Text = agent.PostBoxno;
            txtEmail1.Text = agent.Email;
            txtWebsite.Text = agent.Website;
            txtPhone1.Text = agent.Phoneno;
            txtAddress.Text = agent.Address;
            if (agent.currency == string.Empty)
                ddlCurrency.SelectedIndex = 0;
            else
            {
                ListItem item = ddlCurrency.Items.FindByText(agent.currency);
                ddlCurrency.ClearSelection();
                item.Selected = true;
            }

            txtLicenseNo.Text = agent.TradeLicenseNo;
            if (agent.ExpiryDate != DateTime.MinValue)
            {
                dcLicExpDate.Value = Utility.ToDate(agent.ExpiryDate);
            }
            divUploadFiles.Visible = true;
            
        }
        catch
        {
            throw;
        }
    }

    private void Save()
    {
        try
        {

            //if (ddlParentAgent.SelectedItem.Value == "-1" && ddlAgentType.SelectedItem.Value != "AGENCY")
            //{ throw new Exception("Please select ParentAgent from the list !"); };

            List<string> FilesUploaded = new List<string>();

            AgentMaster agent;
            if (CurrentObject == null)
            {
                agent   = new AgentMaster();
            }
            else
            {
                agent = CurrentObject;
            }


            agent.Code = txtCode.Text.Trim();
            agent.Name =txtName.Text.Trim() ;
            string agentName = txtName.Text.Trim();
            agent.State = txtState.Text.Trim();            
            agent.City = txtCity.Text.Trim();
            agent.Country = Utility.ToInteger(ddlCountry.SelectedItem.Value);
            agent.POBox =  txtPOBox.Text.Trim();
            agent.Email1 = txtEmail1.Text.Trim();
            agent.Email2 = txtEmail2.Text.Trim();
            agent.Website = txtWebsite.Text.Trim();
            agent.Phone1 = txtPhone1.Text.Trim();
            agent.Phone2 = txtPhone2.Text.Trim();
            agent.Fax = txtFax.Text.Trim();
            agent.Telex = txtTelex.Text.Trim();
            agent.AlertLevel= Utility.ToDecimal(txtWarLevel.Text.Trim());
            agent.Address = txtAddress.Text.Trim();
            agent.Status = Settings.ACTIVE;
            agent.LicenseNo = txtLicenseNo.Text;
            agent.LicenseNoExpDate = dcLicExpDate.Value;
            agent.PassportNo = txtPassportNo.Text.Trim();
            agent.PassportIssueDate = dcpptIssuedDate.Value;
            agent.PassportExpDate = dcpptExpOn.Value;
            agent.Nationality = Utility.ToLong(ddlNationality.SelectedItem.Value);
            agent.GrntrStatus = Utility.ToString(ddlGrntrStatus.SelectedItem.Value);
            agent.DecimalValue = Utility.ToInteger(txtAgentDecimal.Text);
            agent.IsRoutingEnabled = chkAgentRouting.Checked;
            agent.IsReturnFareEnabled = chkAgentReturnFare.Checked;            
            agent.CreditLimit = Convert.ToDouble(txtCreditLimit.Text);
            agent.CreditDays = Convert.ToInt32(txtCreditDays.Text);
            agent.CreditBuffer = Convert.ToDouble(txtTempCredit.Text);
            switch (ddlAgentType.SelectedValue.ToUpper())
            {
                case "BASEAGENT":
                    agent.AgentType = (int)AgentType.BaseAgent;
                    break;
                case "AGENT":
                    agent.AgentType = (int)AgentType.Agent;
                    break;
                case "B2B":
                    agent.AgentType = (int)AgentType.B2B;
                    break;
                case "B2B2B":
                    agent.AgentType = (int)AgentType.B2B2B;
                    break;
            }
            //brahmam 24.09.2013
            string Product = "";
            //Label lblProduct;
            
            //string selectedSources = "";
            //string unSelectedSources = "";
            //bool flgGlobalVisa = false;
            Dictionary<int, string> Sources = new Dictionary<int, string>();
            List<string> activeSources = new List<string>();

            for (int i = 0; i < chkagentProduct.Items.Count; i++)
            {
                if (chkagentProduct.Items[i].Selected)
                {
                    Product += chkagentProduct.Items[i].Value + ",";
                    chkSources = (CheckBoxList)tblActiveSources.FindControl("chkSources_" + i.ToString());
                    for (int j = 0; j < chkSources.Items.Count; j++)
                    {
                        if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                        {
                            if (chkSources.Items[j].Selected)
                            {
                                Sources.Add(Utility.ToInteger(chkSources.Items[j].Value), "A");
                                activeSources.Add(Utility.ToString(chkSources.Items[j].Text)); //bind only selected sources
                                //selectedSources += chkSources.Items[j].Value + ",";
                            }
                            else
                            {
                                Sources.Add(Utility.ToInteger(chkSources.Items[j].Value), "D");
                                //unSelectedSources += chkSources.Items[j].Value + ",";
                            }
                        }
                        else
                        {
                            Sources.Add(Utility.ToInteger(chkSources.Items[j].Value), "A");
                            activeSources.Add(Utility.ToString(chkSources.Items[j].Text));
                        }
                    }
                }
                else
                {
                    chkSources = (CheckBoxList)tblActiveSources.FindControl("chkSources_" + i.ToString());
                    for (int j = 0; j < chkSources.Items.Count; j++)
                    {
                        Sources.Add(Utility.ToInteger(chkSources.Items[j].Value), "D");
                        //if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                        //{
                        //    if (chkSources.Items[j].Selected)
                        //    {
                        //        Sources.Add(Utility.ToInteger(chkSources.Items[j].Value), "D");
                        //        //unSelectedSources += chkSources.Items[j].Value + ",";
                        //    }
                        //}
                        //else
                        //{
                        //    Sources.Add(Utility.ToInteger(chkSources.Items[j].Value), "D");
                        //}
                    }
                }
            }
            agent.Sources = Sources;
            //agent.SelectedSources = selectedSources.ToString().TrimEnd(',');
            //agent.UnSelectedSources = unSelectedSources.ToString().TrimEnd(',');
            agent.AgentProduct = Product.ToString().TrimEnd(',');
            //agent.VisaSubmissionStatus = Utility.ToString(ddlVisaSubmission.SelectedItem.Value);
            agent.AgentBlockStatus = (chkBlock.Checked == true ? "Y" : "N");
            agent.AgentRemarks = txtRemarks.Text.Trim();

            if (!string.IsNullOrEmpty(agentImage.FileExtension))
            {
                agent.ImgFileName = agentImage.FileName + agentImage.FileExtension;
            }
            agent.AgentCurrency = ddlCurrency.SelectedItem.Text;
            agent.ReduceBalanceStatus = ddlMatchingStatus.SelectedItem.Value;
            agent.CopyKey = Utility.ToInteger(hdfCopyKey.Value);
            agent.AgentParantId = Utility.ToInteger(ddlParentAgent.SelectedItem.Value); // Settings.LoginInfo.AgentId;
            agent.AgentType = Utility.ToInteger(ddlAgentType.SelectedItem.Value);
            //agent.DtDocuments = DocumentDetails; 
            agent.agentMasterReceipts = JsonConvert.DeserializeObject<List<AgentMasterReceipts>>(hdnAgentfileUploads.Value); 
            agent.CreatedBy = Settings.LoginInfo.UserID;
            agent.NFlex1 = ddlTheme.SelectedItem.Value; //brahmam
            agent.VisaSubmissionStatus = "N";// Updating to N for agency who registered thru register page
            agent.PaymentMode = (PaymentMode)Utility.ToInteger(ddlPaymentMode.SelectedItem.Value);
            if (!string.IsNullOrEmpty(txtCardNo.Text.Trim()))
                agent.agent_CreditCard = GenericStatic.EncryptData(txtCardNo.Text.Trim());
            if (ddlCardType.SelectedValue != "-1")
            agent.agent_cc_type =ddlCardType.SelectedValue;
            agent.agent_cc_exp_date = txtCardExpiry.Text.Trim().ToString();
            agent.SalesExecId =Utility.ToLong(ddlSalesExec.SelectedItem.Value);
            agent.NFlex2 = txtCopyRight.Text.Trim();
            agent.Save();

            //To save uploaded receipts in web server
            
            string spath = ConfigurationManager.AppSettings["RegisterReceiptsFilePath"];
            string status = "No files found to upload";

            if (Utility.ToInteger(hdfRegId.Value) > 0) 
            {
                try
                {
                    AgentRegistration.UpdateAgentRegStatus(Utility.ToInteger(hdfRegId.Value),"A");
                }
                catch { }
            }
            string AgentId = Utility.ToString(agent.RetAgentId);

            // Based on parent Agent wise Source Credentials Update       brahmam
            //if (agent.AgentParantId > 1 &&  agent.AgentType==(int)AgentType.B2B2B)
            //{
            //    Dictionary<string, SourceDetails> dtSources = AgentMaster.GetAirlineCredentials(agent.AgentParantId);
            //    foreach (KeyValuePair<string, SourceDetails> source in dtSources)
            //    {
            //        if (activeSources.Contains(source.Key.ToString()))
            //        {
            //            SourceDetails AgentDetails = source.Value;
            //            AgentMaster.UpdateAgentSourceCredentials(Utility.ToInteger(AgentId), source.Key, AgentDetails);
            //        }
            //    }
            //}
            if (!string.IsNullOrEmpty(agentImage.FileExtension))
            {
                //string[] Image1 = hdfAgentImg.Value.Split('.');
                //hdfAgentImg.Value = hdfAgentImg.Value;
                string imgFullPath = hdfAgentImg.Value + agentImage.FileExtension;
                RenameImage(AgentId, imgFullPath);
            }

            try
            {

                if (HttpContext.Current.Session["AgentRegisterFiles"] == null)
                {
                    Label lblMasterError = (Label)this.Master.FindControl("lblError");
                    lblMasterError.Visible = true;
                    lblMasterError.Text = status;
                }


                List<HttpPostedFile> files = HttpContext.Current.Session["AgentRegisterFiles"] as List<HttpPostedFile>;

                if (files == null || files.Count == 0)
                {
                    Label lblMasterError = (Label)this.Master.FindControl("lblError");
                    lblMasterError.Visible = true;
                    lblMasterError.Text = status;
                }


                var tempFileNames = agent.agentMasterReceipts.ToDictionary(item => item.doc_Id, item => item.doc_name);

                if (tempFileNames != null && tempFileNames.Count < 0)
                {
                    Label lblMasterError = (Label)this.Master.FindControl("lblError");
                    lblMasterError.Visible = true;
                    lblMasterError.Text = status;
                }


                foreach (var key in tempFileNames)
                {
                    foreach (HttpPostedFile file in files)
                    {
                        if (key.Value == file.FileName)
                        {
                            if (System.IO.File.Exists(spath + key.Key + "_" + AgentId+ "_" + key.Value.ToString()))
                            {
                                System.IO.File.Delete(spath + key.Key + "_"+ AgentId + "_" + key.Value.ToString());
                            }
                            FilesUploaded.Add(spath + key.Key + "_" + AgentId + "_" + key.Value.ToString());
                            file.SaveAs(spath + key.Key + "_" + AgentId + "_" + key.Value.ToString());
                            files.Remove(file);
                            break;
                        }
                    }
                }


                HttpContext.Current.Session["AgentRegisterFiles"] = null;

                //status = "Success";

            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

            Clear();
            tblActiveSources.Rows.Clear();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage("Agent ", agentName, (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
        }
        catch
        {
            throw;
        }

    }

    protected void RenameImage(string activityId, string imagePath)
    {
        try
        {
            string source = imagePath.Substring(0, imagePath.LastIndexOf("\\") + 1);
            string oldFile = Path.GetFileName(imagePath);
            string fileExtension = Path.GetExtension(imagePath);
            string dFilePath = string.Empty;
            string newfile = activityId + "" + fileExtension;
            if (System.IO.File.Exists(source + newfile)) System.IO.File.Delete(source + newfile);
            System.IO.File.Move(source + oldFile, source + newfile);
            if (System.IO.File.Exists(source + oldFile)) System.IO.File.Delete(source + oldFile);

        }
        catch { }

    }

   protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
            
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
   protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
            tblActiveSources.Rows.Clear();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    
    # region Content Search
    private void bindSearch()
    {
        try
        {
            LoginInfo loginfo = Settings.LoginInfo;
            DataTable dt = AgentMaster.GetList(loginfo.CompanyID,Convert.ToString(Settings.LoginInfo.AgentType),Settings.LoginInfo.AgentId, ListStatus.Long, RecordStatus.All);
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch { throw; }
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            long vsId = Utility.ToLong(gvSearch.SelectedValue);
            Edit(vsId);
            this.Master.HideSearch();
            Utility.StartupScript(this, "ShowHideCreditDetails()", "AgentCredit");
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{ "HTtxtCode", "agent_code" },{"HTtxtName", "agent_name" }, 
            { "HTtxtPhone", "agent_phone1" },{ "HTtxtAddress", "agent_address" }};
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    # endregion
  


    private void BindAgentProduct(int agentId)
    {
        try
        {
            Session["dtProducts"] = null;
            DataTable dtAllProducts = CT.BookingEngine.UpdateMarkup.ProductGetList(ListStatus.Short, RecordStatus.Activated,Settings.LoginInfo.AgentId);
            dtProducts = dtAllProducts.Clone();
            if (agentId > 1)
            {
                string agentProducts = AgentMaster.AgentProducts(agentId);
                string[] agentProductArray = agentProducts.Split(',');
                foreach (DataRow dr in dtAllProducts.Rows)
                {
                    foreach (string product in agentProductArray)
                    {
                        if (product == dr["productTypeId"].ToString())
                        {
                            dtProducts.ImportRow(dr);
                            break;
                        }
                    }
                }
            }
            else
            {
                dtProducts = dtAllProducts;
            }
            chkagentProduct.DataSource = dtProducts;
            chkagentProduct.DataValueField = "productTypeId";
            chkagentProduct.DataTextField = "productType";
            chkagentProduct.DataBind();
            BindControls(agentId);
            Session["dtProducts"] = dtProducts;
             
        }
        catch { throw; }
    }
    
    protected void ddlParentAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                int agentType = AgentMaster.GetAgentType(Utility.ToInteger(ddlParentAgent.SelectedItem.Value));
                if (agentType == (int)AgentType.BaseAgent)
                {
                    ddlAgentType.SelectedValue = "3";
                    ddlAgentType.Enabled = true;
                }
                else if (agentType == (int)AgentType.Agent)
                {
                    ddlAgentType.SelectedValue = "3";
                    ddlAgentType.Enabled = false;
                }
                else if (agentType == (int)AgentType.B2B)
                {
                    ddlAgentType.SelectedValue = "4";
                    ddlAgentType.Enabled = false;
                }
                tblActiveSources.Rows.Clear();
                BindAgentProduct(Utility.ToInteger(ddlParentAgent.SelectedItem.Value));
                if (Request.QueryString["RegId"] != null)
                    ScriptManager.RegisterStartupScript(this, GetType(), "UploadedReceipts", "UploadedReceipts('" + Utility.ToLong(Request.QueryString["RegId"]) + "','Reg');", true);
            }
            //if (Utility.ToInteger(ddlParentAgent.SelectedItem.Value) <= 1) ddlAgentType.SelectedValue = "B2B";
            //else ddlAgentType.SelectedValue = "B2B2B";

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void ddlAgentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                int agentType = AgentMaster.GetAgentType(Utility.ToInteger(ddlParentAgent.SelectedItem.Value));
                if (agentType == (int)AgentType.BaseAgent)
                {
                    if (Utility.ToInteger(ddlAgentType.SelectedItem.Value) == 1 || Utility.ToInteger(ddlAgentType.SelectedItem.Value) == 4)
                    {
                        ddlAgentType.SelectedValue = "3";
                        Utility.Alert(this.Page, "invalid agent type please select agent type as AGENT or B2B from the list");
                    }
                }
                else if (agentType == (int)AgentType.Agent)
                {
                    if (Utility.ToInteger(ddlAgentType.SelectedItem.Value) == 1 || Utility.ToInteger(ddlAgentType.SelectedItem.Value) == 4  || Utility.ToInteger(ddlAgentType.SelectedItem.Value) == 2)
                    {
                        ddlAgentType.SelectedValue = "3";
                        Utility.Alert(this.Page, "invalid agent type please select agent type as B2B from the list");
                    }
                }
                else if (agentType == (int)AgentType.B2B)
                {
                    if (Utility.ToInteger(ddlAgentType.SelectedItem.Value) == 1 || Utility.ToInteger(ddlAgentType.SelectedItem.Value) == 2 || Utility.ToInteger(ddlAgentType.SelectedItem.Value) == 3)
                    {
                        ddlAgentType.SelectedValue = "4";
                        Utility.Alert(this.Page, "invalid agent type please select agent type as B2B2B from the list");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void chkagentProduct_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                LoadControls();
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    private void BindControls(int agentId)
    {
        try
        {
            if (Session["dtProducts"] != null)
            {
                dtProducts = (DataTable)Session["dtProducts"];
            }
            HtmlTableRow hr = new HtmlTableRow();
            for (int i = 0; i < dtProducts.Rows.Count; i++)
            {

                HtmlTableCell tc = new HtmlTableCell();
                int ProductId = Utility.ToInteger(chkagentProduct.Items[i].Value);
                DataTable dtSources;
                if (agentId > 1)
                {
                    dtSources = AgentMaster.GetAgentSources(agentId, ProductId);
                }
                else
                {
                    dtSources = AgentMaster.GetActiveSources(ProductId);
                }

                //DataTable dtSources = AgentMaster.GetActiveSources(ProductId);
                Label lblProduct = new Label();
                lblProduct.ID = "lblProduct_" + i.ToString();
                lblProduct.Text = Utility.ToString(dtProducts.Rows[i]["productType"]);
                lblProduct.Width = new Unit(80, UnitType.Pixel);
                lblProduct.Visible = false;

                chkSources = new CheckBoxList();
                chkSources.ID = "chkSources_" + i.ToString();
                chkSources.DataSource = dtSources;
                if (agentId > 1)
                {
                    chkSources.DataValueField = "SourceId";
                }
                else
                {
                    chkSources.DataValueField = "Id";
                }
                chkSources.DataTextField = "Name";
                chkSources.DataBind();
                chkSources.Visible = false;
                tc.Controls.Add(lblProduct);
                tc.Controls.Add(chkSources);
                tc.Width = "20px";
                tc.VAlign = "top";
                hr.Cells.Add(tc);

            }
            tblActiveSources.Rows.Add(hr);
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex.Message.ToString(), this.Title);
            Utility.WriteLog(ex.InnerException.ToString(), this.Title);
            throw ex;
        }
    }

    private void LoadControls()
    {
        if (hdnAgentId.Value != "0")
        {
            dtAgentSources = AgentMaster.GetAgentSources(Utility.ToInteger(hdnAgentId.Value),-1);
        }
         for (int i = 0; i < chkagentProduct.Items.Count; i++)
         {
             Label lblProduct = (Label)tblActiveSources.FindControl("lblProduct_" + i.ToString());
             chkSources = (CheckBoxList)tblActiveSources.FindControl("chkSources_" + i.ToString());
             if (hdnCount.Value == "1")
             {
                 chkSources.SelectedIndex = -1;
             }
             if (chkagentProduct.Items[i].Selected)
             {
                 for (int j = 0; j < chkSources.Items.Count; j++)
                 {
                     string find = string.Empty;
                     int sourceId = Convert.ToInt32(chkSources.Items[j].Value);
                     //int agent = Utility.ToInteger(hdnAgentId.Value);
                     find = "SourceId='" + sourceId + "'";
                     if (dtAgentSources != null && dtAgentSources.Rows.Count > 0)
                     {
                         DataRow[] foundRows = dtAgentSources.Select(find);
                         if (foundRows.Length > 0)
                         {
                             chkSources.Items.FindByValue(Utility.ToString(foundRows[0]["SourceId"])).Selected = true;
                         }
                     }
                 }
                 lblProduct.Visible = true;
                 chkSources.Visible = true;
                 
             }
             else
             {
                 lblProduct.Visible = false;
                 chkSources.Visible = false;
             }
         }
         hdnCount.Value = "0";
    }

    //To get document details
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string getDocDetails(int id, string status)
    {
        try
        {
            DataTable dt = AgentMaster.GetDocDetails(id, status);
            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            string JSONString = string.Empty;
            if (dt != null && dt.Rows.Count > 0)
                JSONString = JsonConvert.SerializeObject(dt);
            return JSONString;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string deleteReceipts(int agentid, int doc_id)
    {
        try
        {
            string JSONString = AgentMaster.DeleteAgentReceipts(agentid, doc_id, Settings.LoginInfo.UserID);
            
            return JSONString;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


}





