﻿using System;
using System.Collections;
using Visa;
using CT.Core;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;


public partial class VisaApplicationFormDetails : CT.Core.ParentPage
{
    protected VisaApplication application;
    protected SortedList cityList;
    protected SortedList countryList;
    protected SortedList nationalityList;
    protected List<VisaDocument> visaDocumentList;

    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();
        int visaId;
        if (Request.QueryString.Count > 0)
        {
            bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out visaId);
            if (!isnumerice)
            {
                Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                Response.End();

            }
            try
            {
                application = new VisaApplication(visaId);
                countryList = VisaUtility.GetCountryList();
                nationalityList = VisaNationality.GetNationSortredList();
                cityList = VisaUtility.GetAllCity();
                visaDocumentList = VisaDocument.GetDocumentList(application.CountryCode,application.AgencyId);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddApplicationDetails.aspx,Err:" + ex.Message,"");
            }


        }
        else
        {
            Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
            Response.End();
        }



    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
        //if (Session["roleId"] == null)
        //{
        //    String values = "?errMessage=Login Required to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
        //else if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.CozmoVisa))
        //{
        //    String values = "?errMessage=You are not authorised to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
    }
}
