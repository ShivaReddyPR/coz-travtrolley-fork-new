﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CT.Core;
using System.Data.SqlClient;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using System.Web.UI.WebControls;
using System.Data;
using System.Transactions;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Configuration;
using System.IO;
using Ionic.Zip;

namespace CozmoB2BWebApp
{
    public partial class BaggageTrackingQueueGUI : CT.Core.ParentPage
    {
        PagedDataSource pagedData = new PagedDataSource();
        protected string pagingEnable = "style='display:block'";
        protected int agentFilter = 0;
        protected AgentMaster agency;
        protected string baggageRemarks = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    InitializeControls();
                    GetBaggageInsuranceTrackingQueue();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        private void InitializeControls()
        {
            try
            {
                BindAgency();
                int b2bAgentId;
                int b2b2bAgentId;
                if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
                {
                    ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                }
                else if (Settings.LoginInfo.AgentType == AgentType.Agent)
                {
                    ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                    ddlAgency.Enabled = false;
                }
                else if (Settings.LoginInfo.AgentType == AgentType.B2B)
                {
                    ddlAgency.Enabled = false;
                    b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                    ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);
                    ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                    ddlB2BAgent.Enabled = false;
                }
                else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
                {
                    ddlAgency.Enabled = false;
                    ddlB2BAgent.Enabled = false;
                    b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                    b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                    ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);
                    ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                    ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                    ddlB2B2BAgent.Enabled = false;
                }
                BindB2BAgent(Convert.ToInt32(ddlAgency.SelectedItem.Value));
                BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
                {
                    ddlB2B2BAgent.Enabled = false;
                }
                txtCreatedDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");


            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "Baggage INsurance :Clear() ", "0");
            }
        }
        private void BindAgency()
        {
            try
            {
                DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);// AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
                ddlAgency.DataSource = dtAgents;
                ddlAgency.DataTextField = "Agent_Name";
                ddlAgency.DataValueField = "agent_id";
                ddlAgency.DataBind();
                ddlAgency.Items.Insert(0, new ListItem("--All--", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindB2BAgent(int agentId)
        {
            try
            {
                DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
                ddlB2BAgent.DataSource = dtAgents;
                ddlB2BAgent.DataTextField = "Agent_Name";
                ddlB2BAgent.DataValueField = "agent_id";
                ddlB2BAgent.DataBind();
                ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
                ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindB2B2BAgent(int agentId)
        {
            try
            {
                DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
                ddlB2B2BAgent.DataSource = dtAgents;
                ddlB2B2BAgent.DataTextField = "Agent_Name";
                ddlB2B2BAgent.DataValueField = "agent_id";
                ddlB2B2BAgent.DataBind();
                ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
                ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GetBaggageInsuranceTrackingQueue();
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "Baggage INsurance :btnSearch_Click() ", "0");
            }

        }
        public void GetBaggageInsuranceTrackingQueue()
        {
            try
            {
                int agencyId = -1;//Convert.ToInt32(ddlAgency.SelectedItem.Value);
                string agentType = "";
                DateTime createdDate;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                if (txtCreatedDate.Text.Trim().Length > 0)
                {
                    createdDate = Convert.ToDateTime(txtCreatedDate.Text.Trim(), dateFormat);
                }
                else
                {
                    createdDate = Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"));
                }


                //agentFilter = Convert.ToInt32(ddlAgency.SelectedItem.Value);

                if (ddlB2B2BAgent.SelectedIndex > 0)
                {
                    agentType = "B2B2B";
                    agencyId = (ddlB2B2BAgent.SelectedIndex > 1 ? Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) : Convert.ToInt32(ddlAgency.SelectedItem.Value));
                }
                else if (ddlB2BAgent.SelectedIndex > 0)
                {
                    if (ddlB2BAgent.SelectedIndex == 1)
                    {
                        agentType = "AGENT";
                    }
                    else
                    {
                        agentType = "";
                    }
                    //if (ddlB2BAgent.SelectedIndex > 1 && ddlAgency.SelectedIndex > 1)
                    //{
                    //    agentType = "B2B";
                    //}
                    agencyId = (ddlB2BAgent.SelectedIndex > 1 ? Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) : Convert.ToInt32(ddlAgency.SelectedItem.Value));
                }

                else if (ddlAgency.SelectedIndex > 1)
                {
                    if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
                    {
                        agentType = "BASE";
                    }
                    else
                    {

                        agency = new AgentMaster(Convert.ToInt32(ddlAgency.SelectedItem.Value));
                        if (agency.AgentType == 2)
                        {
                            agentType = "";

                        }
                        agencyId = Convert.ToInt32(ddlAgency.SelectedItem.Value);

                    }
                }


                BaggageInsuranceTrackingQueue obj = new BaggageInsuranceTrackingQueue();
                DataTable dtQueueList = obj.GetBaggageInsuranceTrackingQueueList(agencyId, createdDate, txtPNRno.Text, agentType);
                Session["BaggageInsuranceTrackingQueueList"] = dtQueueList;
                dlBaggageInsTrackingQueue.DataSource = dtQueueList;
                dlBaggageInsTrackingQueue.DataBind();
                CurrentPage = 0;
                doPaging();
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "Baggage INsurance :GetBaggageInsuranceQueue() ", "0");
            }
        }

        #region Paging
        public int CurrentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    return 0;
                }
                else
                {
                    return (int)ViewState["_currentPage"];
                }
            }

            set
            {
                ViewState["_currentPage"] = value;
            }
        }
        void doPaging()
        {
            DataTable dt = (DataTable)Session["BaggageInsuranceTrackingQueueList"];
            pagedData.DataSource = dt.DefaultView;
            pagedData.AllowPaging = true;
            pagedData.PageSize = 5;
            Session["count"] = pagedData.PageCount - 1;
            pagedData.CurrentPageIndex = CurrentPage;
            btnPrev.Visible = (!pagedData.IsFirstPage);
            btnFirst.Visible = (!pagedData.IsFirstPage);
            btnNext.Visible = (!pagedData.IsLastPage);
            btnLast.Visible = (!pagedData.IsLastPage);
            lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
            DataView dView = (DataView)pagedData.DataSource;
            DataTable dTable;
            dTable = (DataTable)dView.Table;
            dlBaggageInsTrackingQueue .DataSource = pagedData;
            dlBaggageInsTrackingQueue.DataBind();
            if (dt.Rows.Count <= 0)
            {
                pagingEnable = "style='display:none'";
            }
        }
        protected void btnPrev_Click(object sender, EventArgs e)
        {
            CurrentPage--;
            doPaging();
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            CurrentPage++;
            doPaging();
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 0;
            doPaging();
        }
        protected void btnLast_Click(object sender, EventArgs e)
        {
            CurrentPage = (int)Session["count"];
            doPaging();
        }
        #endregion
        private void Clear()
        {
            try
            {
                txtCreatedDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                txtPNRno.Text = string.Empty;
                //ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.SelectedIndex = -1;
                ddlB2BAgent.SelectedIndex = -1;
                Session["BaggageInsuranceTrackingQueueList"] = null;
                GetBaggageInsuranceTrackingQueue();
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "Baggage INsurance :Clear() ", "0");
            }
        }
    
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "Baggage Request Queue calling clear event", "0");
            }
        }

        protected void dlBaggageInsTrackingQueue_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "InsChangeRequest")
            {
                if (e.CommandArgument.ToString().Length > 0)
                {
                    int PaxID = Convert.ToInt32(e.CommandArgument);
                    DropDownList ddlChangeRequestType = e.Item.FindControl("ddlChangeRequestType") as DropDownList;
                    TextBox txtRemarks = e.Item.FindControl("txtRemarks") as TextBox;
                    BaggagePassengerDetails obj = new BaggagePassengerDetails();
                    obj.UpdatBaggageTrackingStatus(PaxID, txtRemarks.Text);
                    GetBaggageInsuranceTrackingQueue();
                }
            }
        }

        protected void dlBaggageInsTrackingQueue_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                {
                    Label lblViewBooking = e.Item.FindControl("lblViewBooking") as Label;
                    Label lblViewInvoice = e.Item.FindControl("lblViewInvoice") as Label;
                    HtmlTable tblChangeRequest = e.Item.FindControl("tblChangeRequest") as HtmlTable;
                    LinkButton lnkPayment = e.Item.FindControl("lnkPayment") as LinkButton;
                    Button btnRequest = e.Item.FindControl("btnRequest") as Button;
                    Label lblTotal = e.Item.FindControl("lblTotal") as Label;
                    Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                    Label lblRemarksText = e.Item.FindControl("lblremarkvalue") as Label;
                    Label lblremarks = e.Item.FindControl("lblremarks") as Label;
                    DataRowView view = e.Item.DataItem as DataRowView;
                    int id = Convert.ToInt32(view["pax_bi_id"]);
                    string paxname = view["PAXNAME"].ToString();
                    string trackNumber = view["pax_track_number"].ToString();
                    string policyno = view["PAX_POLICY_NO"].ToString();
                    string ticketNo = view["PAX_TICKET_NO"].ToString();
                    string status= view["Pax_BTR_Status"].ToString();
                    string remarks= view["Remarks"].ToString();
                    baggageRemarks = string.Empty;

                    if (status=="True")
                    {
                        status = "CONFIRMED";
                        lblStatus.Text = status;
                        lblStatus.ForeColor = System.Drawing.Color.LimeGreen;
                        lblRemarksText.Text = remarks;
                        lblremarks.Style.Add("display", "block");
                        lblRemarksText.Style.Add("display", "block");
                        baggageRemarks = remarks;
                        
                    }
                        
                    else
                    {
                        status = "PENDING";
                        lblStatus.Text = status;
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        lblremarks.Style.Add("display", "none");
                        lblRemarksText.Style.Add("display", "none");

                    }


                    string pnr = view["PAX_PNR"].ToString();
                    string undeliveredbags = view["pax_noof_undelivered_bags"].ToString();
                    string supportdoc1 = view["pax_support_doc1"].ToString();
                    string supportdoc2 = view["pax_support_doc2"].ToString();
                    string trackdate = view["pax_track_date"].ToString();

                    if (status != "CONFIRMED")
                    {
                        
                        tblChangeRequest.Visible = true;
                        btnRequest.OnClientClick = "return CancelInsPlan('" + e.Item.ItemIndex + "');";
                        
                    }



                    // lblViewBooking.Text = "<input id='Open-" + e.Item.ItemIndex + "' type='button' class='button' value='Open' onclick=\"ViewBooking(" + id + ")\"  />";

                }
            }
            catch (Exception ex)
            {
                //throw ex;
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "dlInsQueue_ItemDataBound ", "0");
            }

        }


        protected void ddlAgency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
                BindB2BAgent(agentId);
                BindB2B2BAgent(agentId);
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "Agency selected Event", "0");
            }
        }

        protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId >= 0)
                {
                    BindB2B2BAgent(agentId);
                    ddlB2B2BAgent.Enabled = true;
                }
                else
                {
                    ddlB2B2BAgent.SelectedIndex = 0;
                    ddlB2B2BAgent.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "ddlB2BAgent selected Event", "0");

            }
        }
    }
}
