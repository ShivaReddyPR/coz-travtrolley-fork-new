﻿<%@ Page Language="C#" %>
	<!DOCTYPE html>
	<html>

	<head id="Head1" runat="server">
		<title>Visa Services</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="images/favicon.ico">


		<link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="css/RegisterStyle.css" rel="stylesheet" type="text/css" />
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<!-- manual css -->

		<link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
		<link href="build/css/main.min.css" rel="stylesheet" type="text/css" />

	</head>

	<body>
		<div class="topHeader2">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<a href="bizzlogin.aspx"><img src="images/logoc.jpg"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="heading_bizz">
			<div class="container">
				<h2> Visa Services</h2>
			</div>
		</div>
		<form runat="server" id="form1">
			<div class="container">
				<div class="innerPage2">
					<div class="row">
						<div class="col-12">
							<p>Sudden or planned international trip? Need an expert guide to navigate the complicated process of applying for visas in your city? Look no further than CCTM. We have 15 branches across India with dedicated staff that have a thorough understanding of visa requirements for 100s of countries and established relationships with all major embassies.
								<br />
								<br />Whether you are applying for Schengen or the US visa, travelling to Azerbaijan for a conference, or going on vacation, our custom-built portal helps reduce filing time and increase approval rates, even for express-requests, so you can rest assured that your visa will come through in time. Rely on us to get your documentation right the first time and take the stress out of filing paperwork.</p>
						</div>
					</div>
				</div>
			</div>
		</form>
		<div class="footer_sml_bizz">Copyright 2019 Cozmo Travel World, India. All Rights Reserved.</div>



	</body>

	</html>
