using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using CT.Roster;

public partial class ROSTariffMasterUI : CT.Core.ParentPage// System.Web.UI.Page
{
    private string ROS_TARIFF_MASTER = "_TariffMaster";
    private string ROS_TARIFF_SEARCH = "_TariffMasterSearch";

    protected void Page_Load(object sender, EventArgs e)
    {
        lblSuccessMsg.Text = string.Empty;
        if (!IsPostBack)
        {
            InitializePageControls();
            
        }
    }


    private void InitializePageControls()
    {
        try
        {
            BindCompany();
            BindArea();
            Clear();

        }
        catch { throw; }


    }
    private void BindCompany()//Company
    {
        try
        {
            ddlCompany.DataSource = ReceiptMaster.GetCompanyList(ListStatus.Short, RecordStatus.Activated, "R");
            ddlCompany.DataValueField = "COMPANY_ID";
            ddlCompany.DataTextField = "COMPANY_NAME";
            ddlCompany.DataBind();
            ddlCompany.Items.Insert(0, new ListItem("-- Company --", "0"));
            ddlCompany.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }
    private void BindArea()
    {
        try
        {
            ddlArea.DataSource = ROSTranxHeader.GetLocationTypewiseList("H");
            ddlArea.DataValueField = "loc_id_pk";
            ddlArea.DataTextField = "loc_name";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("--Select Area--", "0"));
            ddlArea.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }

    private void Clear()
    {
        try {        
            txtCode.Text=string.Empty ;
            ddlCompany.SelectedIndex=0;
            ddlArea.SelectedIndex=0;
            txtAmount.Text =Formatter.ToCurrency("0");
            btnSave.Text = "Save";
            CurrentObject = null;

        }
        catch 
        {throw;}
    
    
    }

    private void Save()
    {
        try
        {
            ROSTariffMaster tariff;
            if (CurrentObject == null)
            {
                tariff = new ROSTariffMaster();
            }
            else
            {
                tariff = CurrentObject;
            }
            string trfCode = ddlCompany.SelectedItem.Text + "_" + ddlArea.SelectedItem.Text;
            tariff.Code = trfCode;
            tariff.CompanyId = Utility.ToInteger(ddlCompany.SelectedItem.Value);
            //string[] trfDetails = Utility.ToInteger(ddlArea.SelectedItem.Value).ToString().Split('~');
            tariff.AreaId = Utility.ToInteger(ddlArea.SelectedItem.Value); 
            tariff.Amount= Utility.ToDecimal(txtAmount.Text.Trim());
            tariff.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
            tariff.Status = Settings.ACTIVE;
            tariff.Save();
            Clear();
            txtCode.Text = trfCode;
            string strMsg = "Tariff " + trfCode + " is created successfully !";
            Utility.Alert(this.Page, strMsg);
            lblSuccessMsg.Text = strMsg;
        }
        catch { throw; }
    
    
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {   
                Save();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
            //StartupScript(this.Page, "alert('" + ex.Message + "');", "Err");
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch { throw; }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {

            this.Master.ShowSearch("Search");
            //Clear();
            BindSearch();
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    private void BindSearch()
    {
        try
        {
            DataTable dt = ROSTariffMaster.GetList(RecordStatus.Activated, ListStatus.Long);
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch { throw; }
    }

    private void Edit(long id)
    {
        try
        {
            ROSTariffMaster trf = new ROSTariffMaster(id);
            CurrentObject = trf;
            txtCode.Text = trf.Code;
            ddlCompany.SelectedValue = Utility.ToString(trf.CompanyId);
            ddlArea.SelectedValue = Utility.ToString(trf.AreaId);
            txtAmount.Text = Formatter.ToCurrency(trf.Amount);        
           
            btnSave.Text = "Update";
        }
        catch
        {
            throw;
        }
    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            long tariffId = Utility.ToLong(gvSearch.SelectedValue);
            Edit(tariffId);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }

    }

    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string[,] textboxesNColumns ={{ "HTtxtCode", "TRF_CODE" },{"HTtxtCompany", "trf_company_name" }, 
             {"HTtxtArea","loc_name"},{"HTtxtAmount","TRF_AMOUNT"}};
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    //private ROSTariffMaster TariffMasterObj
    //{
    //    get
    //    {
    //        return (ROSTariffMaster)Session["_TariffMaster"];
    //    }
    //    set
    //    {
    //        if (value == null) Session.Remove("_TariffMaster");
    //        else Session["_TariffMaster"] = value;
    //    }
    //}
        

    private ROSTariffMaster CurrentObject
    {
        get
        {
            return (ROSTariffMaster)Session[ROS_TARIFF_MASTER];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(ROS_TARIFF_MASTER);
            }
            else
            {
                Session[ROS_TARIFF_MASTER] = value;
            }
        }
    }

    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[ROS_TARIFF_SEARCH];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["TRF_ID"] };
            Session[ROS_TARIFF_SEARCH] = value;
        }
    }
}
