﻿<%@ WebHandler Language="C#" Class="hn_AgentFileUploader" %>
using System;
using System.Web;
using System.Web.SessionState;
using System.Collections.Generic;
using CT.Core;

public class hn_AgentFileUploader : IHttpHandler, IRequiresSessionState
{
    //int numFiles = 1;
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            List<HttpPostedFile> files = new List<HttpPostedFile>();
            if (context.Session["AgentRegisterFiles"] != null)
            {
                files = context.Session["AgentRegisterFiles"] as List<HttpPostedFile>;
            }
            foreach (string s in context.Request.Files)
            {
                HttpPostedFile file = context.Request.Files[s];
                files.Add(file);
            }
            context.Session["AgentRegisterFiles"] = files;
        }
        catch(Exception ex) {
            Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}
