using System;
using System.Collections.Generic;
using CT.BookingEngine;
using CT.MetaSearchEngine;

public partial class FareRulePage : System.Web.UI.Page
{
    protected List<FareRule> fareRule = new List<FareRule>();
    protected int resultId = 0;
    protected bool error = false;
    protected bool sessionError = false;
    protected SearchResult[] results = new SearchResult[0];
    protected SearchResult result = new SearchResult();
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();
        if (!sessionError)
        {
            if (Request["resultId"] != null)
            {
                resultId = Convert.ToInt32(Request["resultId"]);
                //resultId -= 1;
            }
            string sessionId = string.Empty;
            if (Request["sessionId"] != null)
            {
                sessionId = Request["sessionId"];
            }
            results = Basket.FlightBookingSession[sessionId].Result;
            //TODO: Add try catch here
            MetaSearchEngine mse = new MetaSearchEngine(sessionId);
            mse.SettingsLoginInfo = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo;
            mse.AppUserId = (int)CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
            mse.SessionId = sessionId;

            result = results[resultId - 1];
            try
            {
                if (result.ResultBookingSource == BookingSource.PKFares)
                {
                    SearchResult repricedResult = new SearchResult();
                    //Check if previously Repriced or not for the same result. If so send the same result not to call reprice again
                    //If calling for first time then we will update the repricedResult from the MSE and store in session to use the
                    //same in PassengerDetails page 
                    if(Session["RepricedResult"] != null && (Session["RepricedResult"] as SearchResult).ResultId == result.ResultId)
                    {
                        repricedResult = Session["RepricedResult"] as SearchResult;
                    }
                    SearchRequest request = Session["FlightRequest"] as SearchRequest;
                    fareRule = mse.GetFareRule(ref result, request, ref repricedResult);
                    result.FareRules = fareRule;
                    repricedResult.FareRules = fareRule;//Update the Fare rules for the repriced result
                    results[resultId - 1] = result;//Update the original result with fare rules
                    Session["Results"] = results;
                    Session["RepricedResult"] = repricedResult;//Update the session with repriced result
                    if(fareRule.Count == 0)
                    {
                        error = true;
                    }
                }
                else if (result.ResultBookingSource == BookingSource.SpiceJet || result.ResultBookingSource == BookingSource.SpiceJetCorp)
                {
                    SearchRequest request = Session["FlightRequest"] as SearchRequest;
                    if (Request["sessionId"] == null)
                    {
                        mse.SessionId = Guid.NewGuid().ToString();
                    }
                    fareRule = mse.GetFareRule(request, results[resultId - 1]);
                }
                //BABYLON AIR API FARE RULES
                else if(result.ResultBookingSource == BookingSource.Babylon)
                {
                    SearchRequest request = Session["FlightRequest"] as SearchRequest;
                    if (Request["sessionId"] == null)
                    {
                        mse.SessionId = Guid.NewGuid().ToString();
                    }
                    fareRule = mse.GetBabylonAirFareRule(request,result);
                }
                else
                {
                    fareRule = mse.GetFareRule(results[resultId - 1]);
                    results[resultId - 1].FareRules = fareRule;
                }
            }
            catch (BookingEngineException)
            {
                error = true;
            }
        }
    }
    private void AuthorizationCheck()
    {
        
    }
}
