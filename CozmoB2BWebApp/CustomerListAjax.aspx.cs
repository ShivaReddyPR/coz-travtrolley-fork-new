﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using System.Collections.Generic;

public partial class CustomerListAjax : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string response = "";
        if (Request["requestFrom"] == "GetFleetPax")
        {
            try
            {
                int agencyId = Convert.ToInt32(Request["customerOf"]);
                DataTable dtCustomers = FleetPassenger.GetPaxDetailsByPaxName(agencyId, Request["boxText"].Trim());

                if (dtCustomers != null && dtCustomers.Rows.Count > 0)
                {
                    Response.Write("<ul class=\"list-unstyled\" >");
                    foreach (DataRow dr in dtCustomers.Rows)
                    {
                       // Response.Write("<div class=\"fleft padding-bottom-5\" style=\"width:270px\">");
                        Response.Write("<li>");
                       // Response.Write("</span><span class=\"bold\">");
                        Response.Write("<a href=\"javascript:FillDataThroughPopUp('" + dr["paxid"] + "')\">" + dr["firstName"] + " " + dr["lastName"] + " (" + dr["email"] + ") " + "</a>");
                        Response.Write("</li>");
                    }
                    Response.Write("</ul>");

                }
                else
                {
                    Response.Write("We do not have any Customer saved with this name");
                }
            }
            catch (Exception ex)
            {
                response = "Error"+ex.Message;
            }
           Response.Write(response);
        }
        else if (Request["requestFrom"] == "PassengerDetail")
        {
            try
            {
                int paxId = Convert.ToInt32(Request["paxId"]);
                DataTable dtpaxDetails = FleetPassenger.LoadPaxDetails(paxId);
                if (dtpaxDetails != null && dtpaxDetails.Rows.Count > 0)
                {
                    string documentsPath = string.Empty;
                    
                    string uplodedFileNames = string.Empty;

                    string html = string.Empty;

                    DataRow dr = dtpaxDetails.Rows[0];

                    if (dr["documentsPath"] != DBNull.Value && dr["documentsPath"] != null)
                    {
                        documentsPath = Convert.ToString(dr["documentsPath"]);
                        //storing pax documents folder
                        Session["documentsPath"] = documentsPath;
                    }
                    int fileLength = 0;
                    if (dr["fileNames"] != DBNull.Value && dr["fileNames"] != null)
                    {
                        string[] files = Convert.ToString(dr["fileNames"]).Split('|');
                        if (files.Length > 0)
                        {
                            List<string>previouslyUploadedFiles  = new List<string>();
                            previouslyUploadedFiles = files.ToList();
                            //Loading pax all uploaded images
                            Session["previouslyUploadedFiles"] = previouslyUploadedFiles;
                        }
                        fileLength = files.Length;
                        //Binding all uploading documets
                        for (int q = 0; q < files.Length; q++)
                        {
                            html += "<div id ='divFiles" + files[q] + "'>";
                            html += "<table>";
                            html += "<tr>";
                            html += "<td>";
                            html += "<a href='#' onclick=\"javascript:Download('" + (documentsPath+@"\"+files[q]).Replace("\\","//") + "','" + files[q] + "')\">" + files[q] + "</a>";
                            html += "</td>";
                               
                            
     
                            html += "<td>";
                            html += "<input type='button' value='Delete' onclick=\"javascript:deleteFile('" + files[q] + "')\"></input>";
                            html += "</td>";

      

                            html += "</tr>";
                            html += "</table>";
                            html += "</div>";
                        }
                    }

                    response = dr["title"] + ";*;" + dr["firstName"] + ";*;" + dr["lastName"] + ";*;" + dr["email"] + ";*;" + dr["clientId"] + ";*;" + dr["MobileNo"] + ";*;" + dr["licenseNo"] + ";*;" + dr["licenseExpiryDate"] + ";*;" + dr["licenseIssuePlace"] + ";*;" + dr["pickupAddress"] + ";*;" + dr["landmark"] + ";*;" + dr["MobileNo2"] + ";*;" + fileLength + ";*;" + html;
                }
                else
                {
                    response = "Error";
                }
            }
            catch (Exception ex)
            {
                response = "Error"+ex.Message;
            }
            Response.Write(response);
        }
    }
}
