﻿using System;
using CT.Core;
using System.Web.UI;
using CT.TicketReceipt.BusinessLayer;
using Visa;

public partial class AddVisaNationality : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();
        if (!IsPostBack)
        {
           

            if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
            {
                int Id;
                bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out Id);
                if (!isnumerice)
                {
                    Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                    Response.End();

                }
                try
                {
                    VisaNationality nationality = new VisaNationality(Id);

                    if (nationality != null)
                    {

                        txtNationalityCode.Text = nationality.NationalityCode;
                        txtNationalityName.Text = nationality.NationalityName;
                        txtARNationalityName.Text = nationality.NationalityNameAR;
                        btnSave.Text = "Update";
                        Page.Title = "Update Visa Nationality";
                        lblStatus.Text = "Update Visa Nationality";
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaNationality.aspx,Err:" + ex.Message,"");
                }
            }
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            VisaNationality nalionality = new VisaNationality();
            nalionality.NationalityCode = txtNationalityCode.Text.Trim();
            nalionality.NationalityName = txtNationalityName.Text.Trim();
            nalionality.NationalityNameAR = txtARNationalityName.Text.Trim();

            if (btnSave.Text == "Update")
            {
                if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
                {
                    nalionality.NationalityId = Convert.ToInt32(Request.QueryString[0]);
                    nalionality.LastModifiedBy = (int)Settings.LoginInfo.UserID;

                }
            }
            else
            {
                nalionality.IsActive = true;
                nalionality.CreatedBy = (int)Settings.LoginInfo.UserID;
            }
            nalionality.Save();
           
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaNationality,Err:" + ex.Message, "");
        }
        Response.Redirect("VisaNationalityList.aspx");
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
