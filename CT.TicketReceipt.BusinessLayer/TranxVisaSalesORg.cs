using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;

/// <summary>
/// Summary description for HandlingFeeMaster
/// </summary>
 namespace CT.TicketReceipt.BusinessLayer
{
     public enum VisaDispatchStatus
     {
         All = 0,//All
         Pending=78,// Value N
         Popsted=80,//P Operations
         Rejected=82, //R
         Suprevisor= 83, //S Suprevisor
         Approved=65,//A
         Handover = 72,//H hand over to Customer
         //Operations=79,//O=Dispatch to Airport
         BackOffice= 66,//B=Back Office

     }
public class TranxVisaSales
{
    # region members
    long _transactionId;
    string _docType;
    string _docNumber;
    DateTime  _docDate;
    int _adults;
    int _children;
    int _infants;

    string _allPax;
    long _visaFeeId;
    private int _salesTypeId;
    //private int _countryId;
    private int _visaTypeId;
    decimal _visaFee;
    decimal _securityDeposit;
    int _approveId;
    string _remarks;
    DateTime _chequeDate;
    string _chequeNo;
    decimal _chequeAmount;
    decimal _toCollect;
    string _pnrStatus;
    long _ticketId;
    string _pnrNo;
    decimal _ticketFare;
    string _DBONo;
    string _DAPNo;
    string _DAPTrackingNo;
    string _dispatchStatus;
    long _locationId;
    long _companyId;
    string _corpCustomer;
    string _corpLPNno;
    string _status;
    long _createdBy;
    string _createdByName;
    DataTable _dtVisaPaxDetails;
    string _docKeys;
    string _visaType;
    string _saleType;
    # endregion

    # region Properties
    public long TransactionId
    {
        get {return _transactionId ;}
        set { _transactionId = value; }
    }
    public string DocType
    {
        get {return _docType;}
        set { _docType = value; }
    }
    public string DocNumber
    {
        get { return _docNumber; }
        set { _docNumber = value; }
    }

    public DateTime DocDate
    {
        get { return _docDate; }
        set { _docDate = value; }
    }
    public string AllPax
    {
        get { return _allPax; }
        set { _allPax = value; }
    }
    public long VisaFeeId
    {
        get { return _visaFeeId; }
        set { _visaFeeId = value; }
    }
    public int SalesTypeId
    {
        get { return _salesTypeId; }
        set { _salesTypeId = value; }
    }
    public int VisaTypeId
    {
        get { return _visaTypeId; }
        set { _visaTypeId = value; }
    }

    public int Adults
    {
        get { return _adults; }
        set { _adults = value; }
    }
    public int Children
    {
        get { return _children; }
        set { _children = value; }
    }
    public int Infants
    {
        get { return _infants; }
        set { _infants = value; }
    }
    public decimal VisaFee
    {
        get { return _visaFee; }
        set { _visaFee = value; }
    }
    public decimal SecurityDeposit
    {
        get { return _securityDeposit; }
        set { _securityDeposit = value; }
    }
    public int ApproveId
    {
        get { return _approveId; }
        set { _approveId = value; }
    }
    

    public string Remarks
    {
        get { return _remarks; }
        set { _remarks = value; }
    }

    public DateTime ChequeDate
    {
        get { return _chequeDate; }
        set { _chequeDate = value; }
    }
    public string ChequeNo
    {
        get { return _chequeNo; }
        set { _chequeNo = value; }
    }
    public decimal ChequeAmount
    {
        get { return _chequeAmount; }
        set { _chequeAmount = value; }
    }
    public decimal ToCollect
    {
        get { return _toCollect; }
        set { _toCollect = value; }
    }
    public string PnrStatus
    {
        get { return _pnrStatus; }
        set { _pnrStatus = value; }
    }
    public string PnrNo
    {
        get { return _pnrNo; }
        set { _pnrNo = value; }
    }
    public long TicketId
    {
        get { return _ticketId; }
        set { _ticketId = value; }
    }
    public decimal TicketFare
    {
        get { return _ticketFare; }
        set { _ticketFare = value; }
    }
    public string DBONo
    {
        get { return _DBONo; }
        set { _DBONo = value; }
    }
    public string DAPNo
    {
        get { return _DAPNo; }
        set { _DAPNo = value; }
    }
    public string DAPTrackingNo
    {
        get { return _DAPTrackingNo; }
        set { _DAPTrackingNo = value; }
    }
    public string DispatchStatus
    {
        get { return _dispatchStatus; }
        set { _dispatchStatus = value; }
    }
    public long LocationId
    {
        get { return _locationId; }
        set { _locationId = value; }
    }
    public long CompanyId
    {
        get { return _companyId; }
        set { _companyId = value; }
    }
    public string Status
    {
        get { return _status; }
        set { _status = value; }
    }
    public long CreatedBy
    {
        get { return _createdBy; }
        set { _createdBy = value; }
    }
    public string CreatedByName
    {
        get { return _createdByName; }
        set { _createdByName = value; }
    }
    public DataTable PaxDetailsList
    {
        get { return _dtVisaPaxDetails; }
        set{_dtVisaPaxDetails=value;}
    }
    public string DocKeys
    {
        get { return _docKeys; }
        set { _docKeys = value; }
    }
    public string CorpCustomer
    {
        get { return _corpCustomer; }
        set { _corpCustomer = value; }
    }

    public string CorpLPNno
    {
        get { return _corpLPNno; }
        set { _corpLPNno = value; }
    }

    public string VisaType
    {
        get { return _visaType; }
        //set { _docKeys = value; }
    }
    public string SaleType
    {
        get { return _saleType; }
        //set { _corpCustomer = value; }
    }


    # endregion

    # region Constructors

    public TranxVisaSales()
    {
        //
        // TODO: Add constructor logic here
        //
        _transactionId = -1;
        DataSet ds = GetDetails(_transactionId);
        //if (dt != null && dt.Rows.Count > 0)
         UpdateBusinessData(ds);
    }
    public TranxVisaSales(long transactionId)
    {
        try
        {
            DataSet ds = GetDetails(transactionId);
            //if(dt!=null && dt.Rows.Count>0)
            UpdateBusinessData(ds);
        }
        catch { throw; }

    }
    # endregion

    # region Private Methods
    private DataSet GetDetails(long transactionId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[2];
            SqlParameter param;
            param = new SqlParameter("@p_vs_id", SqlDbType.Decimal);
            param.Value = transactionId; param.Size = 8;
            paramArr[0] = param;

            param = new SqlParameter("@p_vs_location_id", SqlDbType.Decimal);
            param.Value = Settings.LoginInfo.LocationID; param.Size = 8;
            paramArr[1] = param;


            return DBGateway.ExecuteQuery("visa_p_visa_sales_getdata", paramArr);
        }catch { throw; }
    }
    private void UpdateBusinessData(DataSet ds)
    {
        try
        {
            if (ds != null)
            {
                if(ds.Tables[0]!=null && ds.Tables[0].Rows.Count>0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                _dtVisaPaxDetails = ds.Tables[1];
            }


        }
        catch { throw; }
    }

    private void UpdateBusinessData(DataRow dr)
    {
        try
        {
            if (dr != null)
            {
                if (_transactionId < 0)
                {
                    _docType = Utility.ToString(dr["doc_type"]);
                    _docDate = Utility.ToDate(dr["doc_date"]);
                }
                else
                {
                    _transactionId = Utility.ToLong(dr["vs_id"]);
                    _docNumber = Utility.ToString(dr["vs_doc_no"]);
                    _docType = Utility.ToString(dr["vs_doc_type"]);
                    _docDate = Utility.ToDate(dr["vs_doc_date"]);
                    _visaType = Utility.ToString(dr["vs_visa_type_name"]);
                    _saleType = Utility.ToString(dr["vs_sales_type_name"]);
                    _visaFee = Utility.ToDecimal(dr["vs_visa_fee"]);
                    _securityDeposit = Utility.ToDecimal(dr["vs_security_deposit"]);
                    _toCollect= Utility.ToDecimal(dr["vs_to_collect"]);
                    _pnrNo = Utility.ToString(dr["vs_pnr_no"]);
                    _ticketFare = Utility.ToDecimal(dr["vs_ticket_fare"]);
                    ////_paxName = Utility.ToString(dr["receipt_pax_name"]);
                    //_pnr = Utility.ToString(dr["receipt_pnr"]);
                    //_adults = Utility.ToInteger(dr["receipt_adults"]);
                    //_children = Utility.ToInteger(dr["receipt_children"]);
                    //_infants = Utility.ToInteger(dr["receipt_infants"]);
                    //_fare = Utility.ToDecimal(dr["receipt_fare"]);
                    //_handlingFee = Utility.ToDecimal(dr["receipt_handling_fee"]);
                    //_handlingId = Utility.ToLong(dr["receipt_handling_id"]);
                    //_totalFare = Utility.ToDecimal(dr["receipt_total_fee"]);
                    _remarks = Utility.ToString(dr["vs_remarks"]);
                    _locationId = Utility.ToLong(dr["vs_location_id"]);
                    _dispatchStatus = Utility.ToString(dr["vs_dispatch_status"]);
                    _status = Utility.ToString(dr["vs_status"]);
                    _createdBy = Utility.ToLong(dr["vs_created_by"]);
                    _createdByName = Utility.ToString(dr["vs_created_name"]);
                }
            }


        }
        catch { throw; }

    }
    public void Save()
    {
        try
        {
        SqlParameter[] paramArr = new SqlParameter[36];
        SqlParameter param = new SqlParameter("@p_vs_id", SqlDbType.BigInt);
        param.Value = _transactionId; param.Size = 8;
        paramArr[0] = param;


        param = new SqlParameter("@p_vs_doc_type", SqlDbType.NVarChar);
        param.Value = _docType; param.Size = 10;
        paramArr[1] = param;

        SqlParameter paramDocNo = new SqlParameter("@p_vs_doc_no", SqlDbType.NVarChar);
        paramDocNo.Direction= ParameterDirection.Output; paramDocNo.Size = 50;
        paramArr[2] = paramDocNo;

        param = new SqlParameter("@p_vs_doc_date", SqlDbType.DateTime);
        param.Value = _docDate; param.Size = 18;
        paramArr[3] = param;

        param = new SqlParameter("@p_vs_adult", SqlDbType.Int);
        param.Value = _adults; param.Size = 8;
        paramArr[4] = param;

        param = new SqlParameter("@p_vs_child", SqlDbType.Int);
        param.Value = _children; param.Size = 8;
        paramArr[5] = param;

        param = new SqlParameter("@p_vs_infant", SqlDbType.Int);
        param.Value = _infants; param.Size = 8;
        paramArr[6] = param;

        paramArr[7] = new SqlParameter("@p_vs_all_pax", _allPax);
        paramArr[8] = new SqlParameter("@p_vs_visa_id", _visaFeeId);
        paramArr[9] = new SqlParameter("@p_vs_sales_type_id", _salesTypeId);
        paramArr[10] = new SqlParameter("@p_vs_visa_type_id", _visaTypeId);
        paramArr[11] = new SqlParameter("@p_vs_visa_fee", _visaFee);
        paramArr[12] = new SqlParameter("@p_vs_security_deposit", _securityDeposit);
        paramArr[13] = new SqlParameter("@p_vs_waive_security_approve_id", _approveId);
        paramArr[14] = new SqlParameter("@p_vs_remarks", _remarks);
        if(_chequeDate!=DateTime.MinValue)paramArr[15] = new SqlParameter("@p_vs_cheque_date", _chequeDate);
        paramArr[16] = new SqlParameter("@p_vs_cheque_no", _chequeNo);
        paramArr[17] = new SqlParameter("@p_vs_cheque_amount", _chequeAmount);
        paramArr[18] = new SqlParameter("@p_vs_to_collect", _toCollect);
        paramArr[19] = new SqlParameter("@p_vs_pnr_status", _pnrStatus);
        paramArr[20] = new SqlParameter("@p_vs_ticket_id", _ticketId);
        paramArr[21] = new SqlParameter("@p_vs_pnr_no", _pnrNo);
        paramArr[22] = new SqlParameter("@p_vs_ticket_fare", _ticketFare);
        paramArr[23] = new SqlParameter("@p_vs_dbo_doc_no", _DBONo);
        paramArr[24] = new SqlParameter("@p_vs_dap_doc_no", _DAPNo);
        paramArr[25] = new SqlParameter("@p_vs_dap_tracking_no", _DAPTrackingNo);
        paramArr[26] = new SqlParameter("@p_vs_dispatch_status", _dispatchStatus);
        paramArr[27] = new SqlParameter("@p_vs_corp_customer", _corpCustomer);
        paramArr[28] = new SqlParameter("@p_vs_corp_lpono", _corpLPNno);

        paramArr[29] = new SqlParameter("@p_vs_status", _status);
        paramArr[30] = new SqlParameter("@p_vs_location_id", _locationId);
        paramArr[31] = new SqlParameter("@p_vs_company_id", _companyId);
        paramArr[32]= new SqlParameter("@p_vs_created_by", _createdBy);



        SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
        paramMsgType.Size = 10;
        paramMsgType.Direction = ParameterDirection.Output;
        paramArr[33] = paramMsgType;

        SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
        paramMsgText.Size = 100;
        paramMsgText.Direction = ParameterDirection.Output;
        paramArr[34] = paramMsgText;

        SqlParameter paramMsgText1 = new SqlParameter("@p_vs_id_ret", SqlDbType.BigInt);
        paramMsgText1.Size = 20;
        paramMsgText1.Direction = ParameterDirection.Output;
        paramArr[35] = paramMsgText1;



        DBGateway.ExecuteNonQuery("visa_p_visa_sales_add_update", paramArr);
        if (Utility.ToString(paramMsgType.Value )== "E")
            throw new Exception(Utility.ToString(paramMsgText.Value));

        _docNumber = Utility.ToString(paramDocNo.Value);
        _transactionId = Utility.ToLong(paramMsgText1.Value);


        if (_dtVisaPaxDetails != null)
        {
            DataTable dt = _dtVisaPaxDetails.GetChanges();
            int recordStatus = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                //SqlCommand cmd = DBGateway.CreateCommand();
                foreach (DataRow dr in dt.Rows)
                {
                    switch (dr.RowState)
                    {
                        case DataRowState.Added: recordStatus = 1;
                            break;
                        case DataRowState.Modified: recordStatus = 2;
                            break;
                        case DataRowState.Deleted: recordStatus = -1;
                            break;
                        default: break;


                    }
                    saveDetails(recordStatus, Utility.ToLong(dr["pax_id"]), _transactionId, Utility.ToString(dr["pax_gurantor_name"]), Utility.ToString(dr["pax_gurantor_mobile"]),
                                Utility.ToString(dr["pax_gurantor_email"]), Utility.ToString(dr["pax_visitor_name"]), Utility.ToString(dr["pax_visitor_passport_no"]), Utility.ToInteger(dr["pax_visitor_nationality_id"]),
                                   Utility.ToString(dr["pax_visitor_mobile"]), Utility.ToString(dr["pax_visitor_email"]), Utility.ToString(dr["pax_status"]), _createdBy);
                }
              //  DBGateway.CloseConnection(cmd);
            }
        }
        if (!string.IsNullOrEmpty(_docKeys))
        {
            string[] arrDocKeys = _docKeys.Split(',');
           // SqlCommand cmd = DBGateway.CreateCommand();
            for (int x = 0; x < arrDocKeys.Length; x++)
            {
                saveDocDetails(1, Utility.ToLong(arrDocKeys[x].ToString()), _transactionId, Settings.ACTIVE, _createdBy);
            }
           // DBGateway.CloseConnection(cmd);
        }
    }
    catch { throw; }
}
    private void saveDetails(int recordStatus,long paxId,long paxHeaderId,string gurantorName,string gurantorMobileNo,string gurantorEmail,string paxName,
                             string paxPassport,int paxNationalityId,string paxMobile,string paxEmail,string paxStatus,long paxCreatedBy)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[16];
            paramArr[0]= new SqlParameter("@p_record_status", recordStatus);
            paramArr[1] = new SqlParameter("@P_PAX_ID", paxId);

            paramArr[2] = new SqlParameter("@p_pax_vsh_id", paxHeaderId);
            paramArr[3] = new SqlParameter("@P_pax_gurantor_name", gurantorName);
            paramArr[4] = new SqlParameter("@P_pax_gurantor_mobile", gurantorMobileNo);
            paramArr[5] = new SqlParameter("@P_pax_gurantor_email", gurantorEmail);
            paramArr[6] = new SqlParameter("@P_pax_visitor_name", paxName);
            paramArr[7] = new SqlParameter("@P_pax_visitor_passport_no", paxPassport);
            paramArr[8] = new SqlParameter("@P_pax_visitor_nationality_id", paxNationalityId);
            paramArr[9] = new SqlParameter("@P_pax_visitor_mobile", paxMobile);
            paramArr[10] = new SqlParameter("@P_pax_visitor_email", paxEmail);
            paramArr[11] = new SqlParameter("@P_pax_status", paxStatus);
            paramArr[12] = new SqlParameter("@P_pax_created_by", paxCreatedBy);

            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[13] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[14] = paramMsgText;

            //SqlParameter paramMsgText1 = new SqlParameter("@p_vs_id_ret", SqlDbType.BigInt);
            //paramMsgText1.Size = 20;
            //paramMsgText1.Direction = ParameterDirection.Output;
            //paramArr[31] = paramMsgText1;


            
            DBGateway.ExecuteNonQuery( "visa_p_visa_sales_details_add_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

            //_docNumber = Utility.ToString(paramDocNo.Value);
            //_transactionId = Utility.ToLong(paramMsgText1.Value);
        }
        catch { throw; }
    }
    private void saveDocDetails(int recordStatus, long docId, long docHeaderId, string docStatus, long docCreatedBy)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[7];
            paramArr[0] = new SqlParameter("@p_record_status", recordStatus);
            paramArr[1] = new SqlParameter("@P_VS_ID", docHeaderId);
            paramArr[2] = new SqlParameter("@P_VS_VISA_DOC_ID", docId);
            paramArr[3] = new SqlParameter("@P_VS_DOC_STATUS", docStatus);
            
            paramArr[4] = new SqlParameter("@P_VS_DOC_CREATED_BY", docCreatedBy);

            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[5] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[6] = paramMsgText;




            DBGateway.ExecuteNonQuery( "visa_p_visa_sales_doc_add_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }
    # endregion

    # region Static Methods
    public static DataSet GetList(long userID, string memberType, long locationId, string dispatchStatus, ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[7];
            paramArr[0] = new SqlParameter("@p_user_id", userID);
            paramArr[1] = new SqlParameter("@p_user_type", memberType);
            paramArr[2] = new SqlParameter("@p_user_location_id", locationId);
            paramArr[3] = new SqlParameter("@P_VS_DISPATCH_STATUS", status);
            paramArr[4] = new SqlParameter("@P_LIST_STATUS", Utility.ToString((char)status));
            paramArr[5] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
            return DBGateway.ExecuteQuery("ct_p_visa_sales_getlist", paramArr);
        }
        catch { throw; }
    }
    public static DataSet GetDetailList(DateTime fromDate,DateTime toDate,int visaTypeId,int salesTypeId,int nationalityId,long userId,string memberType,
        long locationId, long companyId, VisaDispatchStatus dispatchStatus, string dispatchDocType, string passport, string mobile, string vsDocNo,
        string DBOdocNo,string DAPdocNo,string visitorName,string trackingNo,
        long vsUserId,long vsLocationId,ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[22];
            paramArr[0] = new SqlParameter("@p_from_date", fromDate);
            paramArr[1] = new SqlParameter("@p_to_date", toDate);
            if(visaTypeId>0)paramArr[2] = new SqlParameter("@p_vs_visa_type_id", visaTypeId);
            if (salesTypeId > 0) paramArr[3] = new SqlParameter("@p_vs_sales_type_id", salesTypeId);
            if (nationalityId > 0) paramArr[4] = new SqlParameter("@p_vs_nationality_id", nationalityId);
            //if (userId <= 0) userId = Settings.LoginInfo.UserID;
            paramArr[5] = new SqlParameter("@p_user_id", userId);
            paramArr[6] = new SqlParameter("@p_user_type", memberType);
            paramArr[7] = new SqlParameter("@p_location_id", locationId);
            paramArr[8] = new SqlParameter("@p_company_id", companyId);
            if(dispatchStatus!=VisaDispatchStatus.All)paramArr[9] = new SqlParameter("@p_vs_dispatch_status", Utility.ToString((char)dispatchStatus));
            if(!string.IsNullOrEmpty(dispatchDocType))paramArr[10] = new SqlParameter("@p_vs_doc_type", dispatchDocType);
            if (!string.IsNullOrEmpty(passport)) paramArr[11] = new SqlParameter("@P_PAX_PASSPORT_NO", passport);
            if (!string.IsNullOrEmpty(mobile)) paramArr[12] = new SqlParameter("@P_PAX_MOBILE", mobile);
            if (!string.IsNullOrEmpty(vsDocNo)) paramArr[13] = new SqlParameter("@P_VS_DOC_NO", vsDocNo);
            if (!string.IsNullOrEmpty(DBOdocNo)) paramArr[14] = new SqlParameter("@P_PAX_DBO_DOC_NO", DBOdocNo);
            if (!string.IsNullOrEmpty(DAPdocNo)) paramArr[15] = new SqlParameter("@P_PAX_DAP_DOC_NO", DAPdocNo);
            if (!string.IsNullOrEmpty(visitorName)) paramArr[16] = new SqlParameter("@P_PAX_VISITOR_NAME", visitorName);
            if (!string.IsNullOrEmpty(trackingNo)) paramArr[17] = new SqlParameter("@P_PAX_DAP_TRACKING_NO", trackingNo);
            if (vsUserId>0) paramArr[18] = new SqlParameter("@p_vs_user_id", vsUserId);
            if (vsLocationId>0) paramArr[19] = new SqlParameter("@p_vs_location_id", vsLocationId);
            paramArr[20] = new SqlParameter("@p_list_status", recordStatus);
            paramArr[21] = new SqlParameter("@p_record_status", recordStatus);
            return DBGateway.ExecuteQuery("visa_p_visa_sales_getDetailsList", paramArr);
        }
        catch { throw; }
    }


    //public static DataTable GetReportDetails(long transactionId)
    //{
    //    try
    //    {
    //        SqlParameter[] paramArr = new SqlParameter[1];
    //        SqlParameter param = new SqlParameter("@p_receipt_id", SqlDbType.Decimal);
    //        param.Value = transactionId; param.Size = 8;
    //        paramArr[0] = param;

    //        DataTable dt = DBGateway.ExecuteQuery("ct_p_receipt_getdata", paramArr).Tables[0];
    //        return dt;
    //    }
    //    catch { throw; }
    //}
    # endregion
    # region DispatchTo BO

    public static object GetNextDispatchDocSerial(string  docType,long locationId)
    {
        try
        {
            //SqlParameter[] paramArr = new SqlParameter[2];
            //paramArr[0] = new SqlParameter("@P_DOC_TYPE", docType);
            //paramArr[1] = new SqlParameter("@P_DOC_LOCATION_ID", Settings.LoginInfo.LocationID);

            string query=string.Format("SELECT  DBO.GETDISPATCHDOCUMENTSERIAL('{0}',{1})",docType,locationId);
            object obj = DBGateway.ExecuteScalar(query);
            return obj;  
            //if (Utility.ToString(paramMsgType.Value) == "E")
            //    throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }
    public static void SaveDipatchDetails(long paxId, VisaDispatchStatus dispatchStatus, string dispatchNo, string dispatchRemarks, string trackingNo, long createdBy)
    {
        try
        {
            //getting Disptach Document Next Serial
            

            SqlParameter[] paramArr = new SqlParameter[8];
            paramArr[0] = new SqlParameter("@P_PAX_ID", paxId);
            paramArr[1] = new SqlParameter("@P_DISPATCH_STATUS", Utility.ToString((char)dispatchStatus));
            paramArr[2] = new SqlParameter("@P_DISPATCH_NO", dispatchNo);
            paramArr[3] = new SqlParameter("@P_DISPATCH_REMARKS", dispatchRemarks);
            if (!string.IsNullOrEmpty(trackingNo)) paramArr[4] = new SqlParameter("@P_TRACKING_NO", trackingNo);
            paramArr[5] = new SqlParameter("@P_DISPATCH_MODIFIED_BY", createdBy);

            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[6] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[7] = paramMsgText;

            DBGateway.ExecuteNonQuery("visa_p_visa_sales_dispath_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }
    public static void UpdateDocSerial(VisaDispatchStatus dispatchStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[4];
            paramArr[0] = new SqlParameter("@P_DOC_TYPE", Utility.ToString((char)dispatchStatus));
            paramArr[1] = new SqlParameter("@P_LOCATION_ID", Settings.LoginInfo.LocationID);
            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[2] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[3] = paramMsgText;

            DBGateway.ExecuteNonQuery("visa_p_doc_serial_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }
    public static DataSet GetDispatchReportList(VisaDispatchStatus dispatchStatus, string dispatchRefNo, ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[4];
            
            paramArr[0] = new SqlParameter("@p_pax_dispatch_status", Utility.ToString((char)dispatchStatus));
            paramArr[1] = new SqlParameter("@p_pax_dispatch_ref_no", dispatchRefNo); 
            paramArr[2] = new SqlParameter("@p_list_status", recordStatus);
            paramArr[3] = new SqlParameter("@p_record_status", recordStatus);
            return DBGateway.ExecuteQuery("visa_p_visa_sales_getDispatchReportList", paramArr);
        }
        catch { throw; }
    }
    # endregion
    # region History
    public static DataSet GetDetailHistoryList(DateTime fromDate, DateTime toDate, int visaTypeId, int salesTypeId, int nationalityId, long userId, string memberType,
        long locationId, long companyId, VisaDispatchStatus dispatchStatus, string dispatchDocType, string passport, string mobile, string vsDocNo,
        string DBOdocNo, string DAPdocNo, string visitorName, string trackingNo,
        long vsUserId, long vsLocationId, ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[22];
            paramArr[0] = new SqlParameter("@p_from_date", fromDate);
            paramArr[1] = new SqlParameter("@p_to_date", toDate);
            if (visaTypeId > 0) paramArr[2] = new SqlParameter("@p_vs_visa_type_id", visaTypeId);
            if (salesTypeId > 0) paramArr[3] = new SqlParameter("@p_vs_sales_type_id", salesTypeId);
            if (nationalityId > 0) paramArr[4] = new SqlParameter("@p_vs_nationality_id", nationalityId);
            //if (userId <= 0) userId = Settings.LoginInfo.UserID;
            paramArr[5] = new SqlParameter("@p_user_id", userId);
            paramArr[6] = new SqlParameter("@p_user_type", memberType);
            paramArr[7] = new SqlParameter("@p_location_id", locationId);
            paramArr[8] = new SqlParameter("@p_company_id", companyId);
            if (dispatchStatus != VisaDispatchStatus.All) paramArr[9] = new SqlParameter("@p_vs_dispatch_status", Utility.ToString((char)dispatchStatus));
            if (!string.IsNullOrEmpty(dispatchDocType)) paramArr[10] = new SqlParameter("@p_vs_doc_type", dispatchDocType);
            if (!string.IsNullOrEmpty(passport)) paramArr[11] = new SqlParameter("@P_PAX_PASSPORT_NO", passport);
            if (!string.IsNullOrEmpty(mobile)) paramArr[12] = new SqlParameter("@P_PAX_MOBILE", mobile);
            if (!string.IsNullOrEmpty(vsDocNo)) paramArr[13] = new SqlParameter("@P_VS_DOC_NO", vsDocNo);
            if (!string.IsNullOrEmpty(DBOdocNo)) paramArr[14] = new SqlParameter("@P_PAX_DBO_DOC_NO", DBOdocNo);
            if (!string.IsNullOrEmpty(DAPdocNo)) paramArr[15] = new SqlParameter("@P_PAX_DAP_DOC_NO", DAPdocNo);
            if (!string.IsNullOrEmpty(visitorName)) paramArr[16] = new SqlParameter("@P_PAX_VISITOR_NAME", visitorName);
            if (!string.IsNullOrEmpty(trackingNo)) paramArr[17] = new SqlParameter("@P_PAX_DAP_TRACKING_NO", trackingNo);
            if (vsUserId > 0) paramArr[18] = new SqlParameter("@p_vs_user_id", vsUserId);
            if (vsLocationId > 0) paramArr[19] = new SqlParameter("@p_vs_location_id", vsLocationId);
            paramArr[20] = new SqlParameter("@p_list_status", recordStatus);
            paramArr[21] = new SqlParameter("@p_record_status", recordStatus);
            return DBGateway.ExecuteQuery("visa_p_visa_sales_hist_getDetailsList", paramArr);
        }
        catch { throw; }
    }
    public static DataTable GetDispatchDocNoList(ListStatus status, RecordStatus recordStatus)
    {
        try
        {
       
            SqlParameter[] paramArr = new SqlParameter[3];
            paramArr[0] = new SqlParameter("@P_LIST_STATUS", status);
            paramArr[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
            return DBGateway.ExecuteQuery("visa_p_visa_sales_dispatchNo_Getlist", paramArr).Tables[0];
       
        }
        catch { throw; }
    }
    public static DataTable GetVSDocNoList(ListStatus status, RecordStatus recordStatus)
    {
        try
        {

            SqlParameter[] paramArr = new SqlParameter[3];
            paramArr[0] = new SqlParameter("@P_LIST_STATUS", status);
            paramArr[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
            return DBGateway.ExecuteQuery("visa_p_visa_sales_HeaderDocNo_Getlist", paramArr).Tables[0];

        }
        catch { throw; }
    }
    # endregion
}
}