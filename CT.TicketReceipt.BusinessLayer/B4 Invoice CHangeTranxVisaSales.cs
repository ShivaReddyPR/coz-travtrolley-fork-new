using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;

/// <summary>
/// Summary description for HandlingFeeMaster
/// </summary>
 namespace CT.TicketReceipt.BusinessLayer
{
     public enum VisaDispatchStatus
     {
         All = 0,//All
         Pending=78,// Value N
         Posted=80,//P Operations
         Rejected=82, //R
         Suprevisor= 83, //S Suprevisor
         Approved=65,//A
         Handover = 72,//H hand over to Customer
         //Operations=79,//O=Dispatch to Airport
         BackOffice= 66,//B=Back Office
         Acknowldeged = 87,//W Acknowldged
         Hold=76,//L Hold
         Entered = 69,//E Entered
         Exited = 88,//X Exited
         Absconded = 68 ,// D Absconded 
         EnteredFollowup=70,  // F entered Followup
         ExitedFollowup = 89,  // Y exited Followup   (after exit)
         Overstay  =79,  // O Overstay
         Extended=84 , // T Extended
         BulkApproved = 75 //K- Bulk Approved

     }
     public enum PaymentMode
     {
         Cash = 1,
         Credit = 2,
         Card = 3,
         Cash_Credit = 4,
         Cash_Card = 5,
         Cash_Credit_Card = 6,
         Employee = 7,
         Others = 8,
     }
     public enum VisaAccountedStatus
     {
         All = 0,
         NO = 78,
         YES = 89


     }

     public enum AddDispatchStatus
     {
        All = 0,//All
        Pending=78,// Value N Counter
        BackOffice= 66,//B=Back Office
        Posted=80,//P O       
        Confirmed=67//Confirmed
     }
public class TranxVisaSales
{
    # region members
    long _transactionId;
    string _docType;
    string _docNumber;
    DateTime  _docDate;
    int _adults;
    int _children;
    int _infants;

    string _allPax;
    long _visaFeeId;
    private int _salesTypeId;
    //private int _countryId;
    private int _visaTypeId;
    decimal _visaFee;
    decimal _securityDeposit;
    int _approveId;
    string _remarks;
    string _depositMode;
    DateTime _chequeDate;
    string _chequeNo;
    decimal _chequeAmount;
    string _chequeBank;
    decimal _toCollect;
    decimal _discount;
    string _pnrStatus;
    long _ticketId;
    string _pnrNo;
    decimal _ticketFare;
    //string _DBONo;
    //string _DAPNo;
    //string _DAPTrackingNo;
    string _dispatchStatus;
    long _locationId;
    //long _companyId;
    long _agentId;
    decimal _agentBalance;
    string _corpCustomer;
    string _corpLPNno;
    string _status;
    long _createdBy;
    string _createdByName;
    DataTable _dtVisaPaxDetails;
    string _docKeys;
    string _visaDocStatus;

    string _visaTypeName;
    string _salesTypeName;
    string _grntName;
    string _grntVisaNo;
    DateTime _grntVisaExpDate;
    string _grntPassportNo;
    DateTime _grntPassportIssueDate;
    DateTime _grntPassportExpDate;
    string _grntMobileNo;
    string _grntOfficeNo;
    string _grntEmail;
    int _grntNationalityId;
    string _grntNationalityName;
    string _grnReceiptNo;
   // string _paxVisaEntryNo;
    string _appliedThru;
    private string _downloadStatus;
    private decimal _downloadFee;  
    private string _visaUrgentStatus;
    private decimal _visaUrgentFee;
    private  decimal _visaAgentFee;

    private string _visaAirportDepositStatus;
    private decimal _visaAirportDepositFee;
    private string _visaMeetAssistStatus;
    private decimal _visaMeetAssistFee;
    private string _visaQuickCheckOutStatus;
    private decimal _visaQuickCheckOutFee;
    //settlemode
    string _settlementMode;
    string _currencyCode;
    decimal _exchangeRate;
    PaymentModeInfo _cashMode = new PaymentModeInfo();
    PaymentModeInfo _creditMode = new PaymentModeInfo();
    PaymentModeInfo _cardMode = new PaymentModeInfo();
    PaymentModeInfo _employeeMode = new PaymentModeInfo();
    PaymentModeInfo _othersMode = new PaymentModeInfo();
    string _modeRemarks;
    string _crCardName;
    decimal _crCardRate = 0;
    decimal _crCardTotal = 0;
    string _pnrUCCF;
    string _pnrSettMode;
    string _locationAddress;
    string _locationTerms;
    DataTable _dtMobProviders;

    DataTable _dtMappingList;
    DataTable _dtAllocationList;
    DataTable _dtAddServices;
    private string _interfaceStatus="N";
    private string _accountStatus;
    VSORAInterface _objORAinterface = null;
    private string _mappedStatus;

    private long _addDocId;
    private string _addDocName;

    private long _paxIdRet;
    private string _retMessage;
    private string _visaInterface;

    # endregion

    # region Properties
    public long TransactionId
    {
        get {return _transactionId ;}
        set { _transactionId = value; }
    }
    public string DocType
    {
        get {return _docType;}
        set { _docType = value; }
    }
    public string DocNumber
    {
        get { return _docNumber; }
        set { _docNumber = value; }
    }

    public DateTime DocDate
    {
        get { return _docDate; }
        set { _docDate = value; }
    }
    public string AllPax
    {
        get { return _allPax; }
        set { _allPax = value; }
    }
    public long VisaFeeId
    {
        get { return _visaFeeId; }
        set { _visaFeeId = value; }
    }
    public int SalesTypeId
    {
        get { return _salesTypeId; }
        set { _salesTypeId = value; }
    }
    public int VisaTypeId
    {
        get { return _visaTypeId; }
        set { _visaTypeId = value; }
    }

    public int Adults
    {
        get { return _adults; }
        set { _adults = value; }
    }
    public int Children
    {
        get { return _children; }
        set { _children = value; }
    }
    public int Infants
    {
        get { return _infants; }
        set { _infants = value; }
    }
    public decimal VisaFee
    {
        get { return _visaFee; }
        set { _visaFee = value; }
    }
    public decimal SecurityDeposit
    {
        get { return _securityDeposit; }
        set { _securityDeposit = value; }
    }
    public int ApproveId
    {
        get { return _approveId; }
        set { _approveId = value; }
    }
    public string Remarks
    {
        get { return _remarks; }
        set { _remarks = value; }
    }
    public string DepositMode
    {
        get { return _depositMode; }
        set { _depositMode = value; }
    }
    public DateTime ChequeDate
    {
        get { return _chequeDate; }
        set { _chequeDate = value; }
    }
    public string ChequeNo
    {
        get { return _chequeNo; }
        set { _chequeNo = value; }
    }
    public decimal ChequeAmount
    {
        get { return _chequeAmount; }
        set { _chequeAmount = value; }
    }

    public string ChequeBank
    {
        get { return _chequeBank; }
        set { _chequeBank = value; }
    }

    public decimal ToCollect
    {
        get { return _toCollect; }
        set { _toCollect = value; }
    }
    public decimal Discount
    {
        get { return _discount; }
        set { _discount = value; }
    }
    public string PnrStatus
    {
        get { return _pnrStatus; }
        set { _pnrStatus = value; }
    }
    public string PnrNo
    {
        get { return _pnrNo; }
        set { _pnrNo = value; }
    }
    public long TicketId
    {
        get { return _ticketId; }
        set { _ticketId = value; }
    }
    public decimal TicketFare
    {
        get { return _ticketFare; }
        set { _ticketFare = value; }
    }
    //public string DBONo
    //{
    //    get { return _DBONo; }
    //    set { _DBONo = value; }
    //}
    //public string DAPNo
    //{
    //    get { return _DAPNo; }
    //    set { _DAPNo = value; }
    //}
    //public string DAPTrackingNo
    //{
    //    get { return _DAPTrackingNo; }
    //    set { _DAPTrackingNo = value; }
    //}
    public string DispatchStatus
    {
        get { return _dispatchStatus; }
        set { _dispatchStatus = value; }
    }
    public long LocationId
    {
        get { return _locationId; }
        set { _locationId = value; }
    }
    //public long CompanyId
    //{
    //    get { return _companyId; }
    //    set { _companyId = value; }
    //}
    public long AgentId
    {
        get { return _agentId; }
        set { _agentId = value; }
    }

    public decimal AgentBalance
    {
        get { return _agentBalance; }
        set { _agentBalance = value; }
    }

    public string Status
    {
        get { return _status; }
        set { _status = value; }
    }
    public long CreatedBy
    {
        get { return _createdBy; }
        set { _createdBy = value; }
    }
    public string CreatedByName
    {
        get { return _createdByName; }
        set { _createdByName = value; }
    }
    public DataTable PaxDetailsList
    {
        get { return _dtVisaPaxDetails; }
        set{_dtVisaPaxDetails=value;}
    }
    public string DocKeys
    {
        get { return _docKeys; }
        set { _docKeys = value; }
    }
    public string VisaDocStatus
    {
        get { return _visaDocStatus ; }
        set { _visaDocStatus = value; }
    }
    public string CorpCustomer
    {
        get { return _corpCustomer; }
        set { _corpCustomer = value; }
    }

    public string CorpLPNno
    {
        get { return _corpLPNno; }
        set { _corpLPNno = value; }
    }

    public string VisaTypeName
    {
        get { return _visaTypeName; }
        //set { _docKeys = value; }
    }
    public string SalesTypeName
    {
        get { return _salesTypeName; }
        //set { _corpCustomer = value; }
    }
    public string GrntName
    {
        get { return _grntName; }
        set { _grntName = value; }
    }
    public string GrntVisaNo
    {
        get { return _grntVisaNo; }
        set { _grntVisaNo = value; }
    }
    public DateTime GrntVisaExpDate
    {
        get { return _grntVisaExpDate; }
        set { _grntVisaExpDate = value; }
    }
    public string GrntPassportNo
    {
        get { return _grntPassportNo; }
        set { _grntPassportNo = value; }
    }
    public DateTime GrntPassportIssueDate
    {
        get { return _grntPassportIssueDate; }
        set { _grntPassportIssueDate = value; }
    }
    public DateTime GrntPassportExpDate
    {
        get { return _grntPassportExpDate; }
        set { _grntPassportExpDate = value; }
    }
    public string GrntMobileNo
    {
        get { return _grntMobileNo; }
        set { _grntMobileNo = value; }
    }
    public string GrntOfficeNo
    {
        get { return _grntOfficeNo; }
        set { _grntOfficeNo = value; }
    }
    public string GrntEmail
    {
        get { return _grntEmail; }
        set { _grntEmail= value; }
    }
    public int GrntNationalityId
    {
        get { return _grntNationalityId; }
        set { _grntNationalityId = value; }
    }
    public string GrntNationalityName
    {
        get { return _grntNationalityName; }
    }
    public string GrnReceiptNo
    {
        get { return _grnReceiptNo; }
        set { _grnReceiptNo = value; }
    }
    //public string PaxVisaEntryNo
    //{
    //    get { return _paxVisaEntryNo; }
    //    set { _paxVisaEntryNo = value; }
    //}
    public string AppliedThru
    {
        get { return _appliedThru; }
        //set { _appliedThru = value; }
    }

    public string VisaUrgentStatus
    {
        get { return _visaUrgentStatus ; }
        set { _visaUrgentStatus = value; }
    }
    public decimal VisaUrgentFee
    {
        get { return _visaUrgentFee ; }
        set { _visaUrgentFee = value; }
    }


    public string VisaAirportDepositStatus
    {
        get { return _visaAirportDepositStatus; }
        set { _visaAirportDepositStatus = value; }
    }
    public decimal VisaAirportDepositFee
    {
        get { return _visaAirportDepositFee; }
        set { _visaAirportDepositFee = value; }
    }
    public decimal VisaAgentFee
    {
        get { return _visaAgentFee; }
        set { _visaAgentFee = value; }
    }
    public string VisaMeetAssistStatus
    {
        get { return _visaMeetAssistStatus; }
        set { _visaMeetAssistStatus = value; }
    }
    public decimal VisaMeetAssistFee
    {
        get { return _visaMeetAssistFee; }
        set { _visaMeetAssistFee = value; }
    }

    public string VisaQuickCheckOutStatus
    {
        get { return _visaQuickCheckOutStatus; }
        set { _visaQuickCheckOutStatus = value; }
    }
    public decimal VisaQuickCheckOutFee
    {
        get { return _visaQuickCheckOutFee; }
        set { _visaQuickCheckOutFee = value; }
    }
    public string DownloadStatus
    {
        get { return _downloadStatus; }
        set { _downloadStatus = value; }
    }
    public decimal DownloadFee
    {
        get { return _downloadFee; }
        set { _downloadFee = value; }
    }

    public string SettlementMode
    {
        get { return _settlementMode; }
        set { _settlementMode = value; }
    }
    public string CurrencyCode
    {
        get { return _currencyCode; }
        set { _currencyCode = value; }
    }
    public decimal ExchangeRate
    {
        get { return _exchangeRate; }
        set { _exchangeRate = value; }
    }
    public PaymentModeInfo CashMode
    {
        get { return _cashMode; }
        set { _cashMode = value; }
    }
    public PaymentModeInfo CreditMode
    {
        get { return _creditMode; }
        set { _creditMode = value; }
    }
    public PaymentModeInfo CardMode
    {
        get { return _cardMode; }
        set { _cardMode = value; }
    }
    public PaymentModeInfo EmployeeMode
    {
        get { return _employeeMode; }
        set { _employeeMode = value; }
    }
    public PaymentModeInfo OthersMode
    {
        get { return _othersMode; }
        set { _othersMode = value; }
    }
    public string ModeRemarks
    {
        get { return _modeRemarks; }
        set { _modeRemarks = value; }
    }

    public string CRCardName
    {
        get { return _crCardName; }
        set { _crCardName = value; }
    }
    public decimal CRCardRate
    {
        get { return _crCardRate; }
        set { _crCardRate = value; }
    }
    public decimal CRCardTotal
    {
        get { return _crCardTotal; }
        set { _crCardTotal = value; }
    }
    public string PNRUCCF
    {
        get { return _pnrUCCF; }
        set { _pnrUCCF = value; }
    }
    public string PNRSettMode
    {
        get { return _pnrSettMode; }
        set { _pnrSettMode = value; }
    }
    public string LocationAddress
    {
        get { return _locationAddress; }
        set { _locationAddress = value; }
    }

    public string LocationTerms
    {
        get { return _locationTerms; }
        set { _locationTerms = value; }
    }

    public DataTable MobProviders
    {
        get { return _dtMobProviders; }
        set { _dtMobProviders = value; }
    }
    public DataTable MappingList
    {
        get { return _dtMappingList; }
        set { _dtMappingList = value; }
    }
    public DataTable AllocationList
    {
        get { return _dtAllocationList; }
        set { _dtAllocationList = value; }
    }
    public string InterfaceStatus
    {
        get { return _interfaceStatus; }
        set { _interfaceStatus = value; }
    }
    public string AccountStatus
    {
        get { return _accountStatus; }
        set { _accountStatus = value; }
    }
    public VSORAInterface ObjORAinterface
    {
        get { return _objORAinterface; }
        set { _objORAinterface = value; }
    }
    public string MappedStatus
    {
        get { return _mappedStatus; }
        set { _mappedStatus = value; }
    }

    public DataTable AddServices
    {
        get { return _dtAddServices; }
        set { _dtAddServices = value; }
    }

    public string AddDocName
    {
        get { return _addDocName ; }
        set { _addDocName = value; }
    }

    public long  AddDocId
    {
        get { return _addDocId; }
        set { _addDocId = value; }
    }

    public string RetMessage
    {
        get { return _retMessage; }
        set { _retMessage = value; }
    }

    public string VisaInterface
    {
        get { return _visaInterface; }
        set { _visaInterface = value; }
    }
    # endregion

    # region Constructors

    public TranxVisaSales()
    {
        //
        // TODO: Add constructor logic here
        //
        _transactionId = -1;
        DataSet ds = GetDetails(_transactionId);
        //if (dt != null && dt.Rows.Count > 0)
         UpdateBusinessData(ds);
    }
    public TranxVisaSales(long transactionId)
    {
        try
        {
            DataSet ds = GetDetails(transactionId);
            //if(dt!=null && dt.Rows.Count>0)
            UpdateBusinessData(ds);
        }
        catch { throw; }

    }
    public TranxVisaSales(long transactionId,string isExtension)
    {
        try
        {
            DataSet ds = GetDetails(transactionId,isExtension);
            //if(dt!=null && dt.Rows.Count>0)
            UpdateBusinessData(ds);
        }
        catch { throw; }

    }
    # endregion

    # region Private Methods
    private DataSet GetDetails(long transactionId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[5];
            SqlParameter param;
            param = new SqlParameter("@p_vs_id", SqlDbType.Decimal);
            param.Value = transactionId; param.Size = 8;
            paramArr[0] = param;

            param = new SqlParameter("@p_vs_location_id", SqlDbType.Decimal);
            param.Value = Settings.LoginInfo.LocationID; param.Size = 8;
            paramArr[1] = param;

            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[2] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[3] = paramMsgText;
            paramArr[4] = new SqlParameter("@P_VS_AGENT_ID", Settings.LoginInfo.AgentId);

            return DBGateway.ExecuteQuery("visa_p_visa_sales_getdata", paramArr);
        }catch { throw; }
    }
    private DataSet GetDetails(long transactionId,string isExtension)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[6];
            SqlParameter param;
            param = new SqlParameter("@p_vs_id", SqlDbType.Decimal);
            param.Value = transactionId; param.Size = 8;
            paramArr[0] = param;

            param = new SqlParameter("@p_vs_location_id", SqlDbType.Decimal);
            param.Value = Settings.LoginInfo.LocationID; param.Size = 8;
            paramArr[1] = param;

            paramArr[2] = new SqlParameter("@P_IS_EXTENSION", isExtension);

            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[3] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[4] = paramMsgText;
            paramArr[5] = new SqlParameter("@P_VS_AGENT_ID", Settings.LoginInfo.AgentId);


            return DBGateway.ExecuteQuery("visa_p_visa_sales_getdata", paramArr);
        }
        catch { throw; }
    }
    private void UpdateBusinessData(DataSet ds)
    {
        try
        {
            if (ds != null)
            {
                if(ds.Tables[0]!=null && ds.Tables[0].Rows.Count>0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                _dtVisaPaxDetails = ds.Tables[1];
                _dtMobProviders = ds.Tables[2];
                if (TransactionId == -1)
                {
                    
                    _dtMappingList = ds.Tables[3];
                    _dtAllocationList = ds.Tables[4];
                    _interfaceStatus = ds.Tables[5].Rows.Count>0? Utility.ToString(ds.Tables[5].Rows[0]["LOCATION_INTERFACE_STATUS"]):"N";
                }
                
            }


        }
        catch { throw; }
    }

    private void UpdateBusinessData(DataRow dr)
    {
        try
        {
            if (dr != null)
            {
                if (_transactionId < 0)
                {
                    _docType = Utility.ToString(dr["doc_type"]);
                    _docDate = Utility.ToDate(dr["doc_date"]);
                    _agentBalance = Utility.ToDecimal(dr["agentBalance"]);
                }
                else
                {
                    _transactionId = Utility.ToLong(dr["vs_id"]);
                    _docNumber = Utility.ToString(dr["vs_doc_no"]);
                    _docType = Utility.ToString(dr["vs_doc_type"]);
                    _docDate = Utility.ToDate(dr["vs_doc_date"]);
                    _adults = Utility.ToInteger(dr["vs_adult"]);
                    _children = Utility.ToInteger(dr["vs_child"]);
                    _infants = Utility.ToInteger(dr["vs_infant"]);
                    _allPax = Utility.ToString(dr["vs_all_pax"]);
                    _visaTypeId = Utility.ToInteger(dr["vs_visa_type_id"]);
                    _visaTypeName = Utility.ToString(dr["vs_visa_type_name"]);
                    _salesTypeId = Utility.ToInteger(dr["vs_sales_type_id"]);
                    _salesTypeName = Utility.ToString(dr["vs_sales_type_name"]);
                    _visaFeeId = Utility.ToLong(dr["vs_visa_id"]);
                    _visaFee = Utility.ToDecimal(dr["vs_visa_fee"]);
                    _securityDeposit = Utility.ToDecimal(dr["vs_security_deposit"]);
                    _approveId = Utility.ToInteger(dr["vs_waive_security_approve_id"]);
                    _approveId = Utility.ToInteger(dr["vs_waive_security_approve_id"]);
                    _remarks = Utility.ToString(dr["vs_remarks"]);
                    _depositMode = Utility.ToString(dr["vs_deposit_mode"]);
                    _chequeDate = Utility.ToDate(dr["vs_cheque_date"]);
                    _chequeNo= Utility.ToString(dr["vs_cheque_no"]);
                    _chequeAmount = Utility.ToDecimal(dr["vs_cheque_amount"]);
                    _chequeBank = Utility.ToString(dr["vs_cheque_bank"]);
                    _toCollect= Utility.ToDecimal(dr["vs_to_collect"]);
                    _discount= Utility.ToDecimal(dr["vs_discount"]);
                    _pnrStatus = Utility.ToString(dr["vs_pnr_status"]);
                    _ticketId = Utility.ToLong(dr["vs_ticket_id"]);
                    _pnrNo = Utility.ToString(dr["vs_pnr_no"]);
                    _ticketFare = Utility.ToDecimal(dr["vs_ticket_fare"]);
                    //_DBONo = Utility.ToString(dr["vs_dbo_doc_no"]);
                    //_DAPNo = Utility.ToString(dr["vs_dap_doc_no"]);
                    //_DAPTrackingNo = Utility.ToString(dr["vs_dap_tracking_no"]);
                    //_dispatchStatus = Utility.ToString(dr["vs_dispatch_status"]);
                    _corpCustomer = Utility.ToString(dr["vs_corp_customer"]);
                    _corpLPNno= Utility.ToString(dr["vs_corp_lpono"]);
                    _status = Utility.ToString(dr["vs_status"]);
                    _locationId = Utility.ToLong(dr["vs_location_id"]);
                   // _companyId = Utility.ToLong(dr["vs_company_id"]);
                    _agentId = Utility.ToLong(dr["vs_agent_id"]);
                    _createdBy = Utility.ToLong(dr["vs_created_by"]);
                    _createdByName = Utility.ToString(dr["vs_created_name"]);

                    _grntName = Utility.ToString(dr["vs_grnt_name"]);
                    _grntVisaNo = Utility.ToString(dr["vs_grnt_visa_number"]);
                    _grntVisaExpDate = Utility.ToDate(dr["vs_grnt_visa_expiry_date"]);
                    _grntPassportNo = Utility.ToString(dr["vs_grnt_passport_no"]);
                    _grntPassportIssueDate = Utility.ToDate(dr["vs_grnt_passport_issue_date"]);
                    _grntPassportExpDate = Utility.ToDate(dr["vs_grnt_passport_expiry_date"]);
                    _grntMobileNo = Utility.ToString(dr["vs_grnt_mobile_no"]);
                    _grntOfficeNo = Utility.ToString(dr["vs_grnt_office_no"]);
                    _grntEmail = Utility.ToString(dr["vs_grnt_email"]);

                    _grntNationalityId = Utility.ToInteger(dr["vs_grnt_nationality_id"]);
                    _grntNationalityName = Utility.ToString(dr["vs_nationality_name"]);
                    _grnReceiptNo = Utility.ToString(dr["vs_receipt_no"]);
                    _appliedThru = Utility.ToString(dr["vs_visa_applied_thru"]);
                    _visaUrgentStatus = Utility.ToString(dr["visa_urgent_status"]);
                    _visaUrgentFee= Utility.ToDecimal(dr["visa_urgent_fee"]);

                    _visaAirportDepositStatus  = Utility.ToString(dr["vs_airport_deposit_status"]);
                    _visaAirportDepositFee  = Utility.ToDecimal(dr["vs_airport_deposit_fee"]);
                    _visaMeetAssistStatus = Utility.ToString(dr["vs_meet_assist_status"]);
                    _visaMeetAssistFee = Utility.ToDecimal(dr["vs_meet_assist_fee"]);
                    _visaQuickCheckOutStatus = Utility.ToString(dr["vs_quick_checkout_status"]);
                    _visaQuickCheckOutFee  = Utility.ToDecimal(dr["vs_quick_checkout_fee"]);
                    _downloadStatus = Utility.ToString(dr["VS_DOWNLOAD_STATUS"]);
                    _downloadFee = Utility.ToDecimal(dr["VS_DOWNLOAD_FEE"]);

                    //Settlement Details
                    _settlementMode = Utility.ToString(dr["vs_settlement_mode"]);
                    _currencyCode = Utility.ToString(dr["vs_CURRENCY_CODE"]);
                    _exchangeRate = Utility.ToDecimal(dr["vs_EXCHANGE_RATE"]);
                    _cashMode.Set(Utility.ToString(dr["VS_CASH_MODE"]), Utility.ToDecimal(dr["vs_cash_base_amount"]), Utility.ToDecimal(dr["vs_cash_local_amount"]));
                    _creditMode.Set(Utility.ToString(dr["vs_credit_mode"]), Utility.ToDecimal(dr["vs_credit_base_amount"]), Utility.ToDecimal(dr["vs_credit_local_amount"]));
                    _cardMode.Set(Utility.ToString(dr["vs_CARD_MODE"]), Utility.ToDecimal(dr["vs_card_base_amount"]), Utility.ToDecimal(dr["vs_card_local_amount"]));
                    _employeeMode.Set(Utility.ToString(dr["vs_EMPLOYEE_MODE"]), Utility.ToDecimal(dr["vs_employee_base_amount"]), Utility.ToDecimal(dr["vs_employee_local_amount"]));
                    _othersMode.Set(Utility.ToString(dr["vs_OTHERS_MODE"]), Utility.ToDecimal(dr["vs_others_base_amount"]), Utility.ToDecimal(dr["vs_others_local_amount"]));
                    _modeRemarks = Utility.ToString(dr["vs_mode_remarks"]);                   
                    //_crCardRate= Utility.ToString(dr["vs_map_receipt_no"]);
                    _crCardTotal = Utility.ToDecimal(dr["vs_crcard_tot"]);
                    _pnrUCCF = Utility.ToString(dr["VS_PNR_UCCF"]);
                    _pnrSettMode = Utility.ToString(dr["VS_PNR_SETTLEMENT_MODE"]);
                    _locationAddress = Utility.ToString(dr["location_address"]);
                    _locationTerms = Utility.ToString(dr["location_terms"]);
                    _accountStatus = Utility.ToString(dr["VS_ACCOUNTED_STATUS"]);
                    _visaInterface = Utility.ToString(dr["vs_interface_status"]); ;
                    ////_paxName = Utility.ToString(dr["receipt_pax_name"]);
                    //_pnr = Utility.ToString(dr["receipt_pnr"]);
                    //_adults = Utility.ToInteger(dr["receipt_adults"]);
                    //_children = Utility.ToInteger(dr["receipt_children"]);
                    //_infants = Utility.ToInteger(dr["receipt_infants"]);
                    //_fare = Utility.ToDecimal(dr["receipt_fare"]);
                    //_handlingFee = Utility.ToDecimal(dr["receipt_handling_fee"]);
                    //_handlingId = Utility.ToLong(dr["receipt_handling_id"]);
                    //_totalFare = Utility.ToDecimal(dr["receipt_total_fee"]);
                    
                    
                    
                    
                    
                }
            }


        }
        catch { throw; }

    }
    public void Save()
    {
        SqlTransaction trans = null;
        SqlCommand cmd = null;
        try
        {
            //SqlCommand cmd = DBGateway.CreateCommand(trans);
            cmd = new SqlCommand();
            cmd.Connection = DBGateway.CreateConnection();
            cmd.Connection.Open();
            trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
            cmd.Transaction = trans;

            SqlParameter[] paramArr = new SqlParameter[84];
            SqlParameter param = new SqlParameter("@p_vs_id", SqlDbType.BigInt);
            param.Value = _transactionId; param.Size = 8;
            paramArr[0] = param;

            param = new SqlParameter("@p_vs_doc_type", SqlDbType.NVarChar);
            param.Value = _docType; param.Size = 10;
            paramArr[1] = param;

            SqlParameter paramDocNo = new SqlParameter("@p_vs_doc_no", SqlDbType.NVarChar);
            paramDocNo.Direction = ParameterDirection.Output; paramDocNo.Size = 50;
            paramArr[2] = paramDocNo;

            param = new SqlParameter("@p_vs_doc_date", SqlDbType.DateTime);
            param.Value = _docDate; param.Size = 18;
            paramArr[3] = param;

            param = new SqlParameter("@p_vs_adult", SqlDbType.Int);
            param.Value = _adults; param.Size = 8;
            paramArr[4] = param;

            param = new SqlParameter("@p_vs_child", SqlDbType.Int);
            param.Value = _children; param.Size = 8;
            paramArr[5] = param;

            param = new SqlParameter("@p_vs_infant", SqlDbType.Int);
            param.Value = _infants; param.Size = 8;
            paramArr[6] = param;

            paramArr[7] = new SqlParameter("@p_vs_all_pax", _allPax);
            paramArr[8] = new SqlParameter("@p_vs_visa_id", _visaFeeId);
            paramArr[9] = new SqlParameter("@p_vs_sales_type_id", _salesTypeId);
            paramArr[10] = new SqlParameter("@p_vs_visa_type_id", _visaTypeId);
            paramArr[11] = new SqlParameter("@p_vs_visa_fee", _visaFee);
            paramArr[12] = new SqlParameter("@p_vs_security_deposit", _securityDeposit);
            paramArr[13] = new SqlParameter("@p_vs_waive_security_approve_id", _approveId);
            paramArr[14] = new SqlParameter("@p_vs_remarks", _remarks);
            paramArr[15] = new SqlParameter("@p_vs_deposit_mode", _depositMode);
            if (_chequeDate != DateTime.MinValue) paramArr[16] = new SqlParameter("@p_vs_cheque_date", _chequeDate);
            paramArr[17] = new SqlParameter("@p_vs_cheque_no", _chequeNo);
            paramArr[18] = new SqlParameter("@p_vs_cheque_amount", _chequeAmount);
            paramArr[19] = new SqlParameter("@p_vs_to_collect", _toCollect);
            paramArr[20] = new SqlParameter("@p_vs_pnr_status", _pnrStatus);
            paramArr[21] = new SqlParameter("@p_vs_ticket_id", _ticketId);
            paramArr[22] = new SqlParameter("@p_vs_pnr_no", _pnrNo);
            paramArr[23] = new SqlParameter("@p_vs_ticket_fare", _ticketFare);
            //paramArr[24] = new SqlParameter("@p_vs_dbo_doc_no", _DBONo);
            //paramArr[25] = new SqlParameter("@p_vs_dap_doc_no", _DAPNo);
            //paramArr[26] = new SqlParameter("@p_vs_dap_tracking_no", _DAPTrackingNo);
            paramArr[24] = new SqlParameter("@p_vs_dispatch_status", _dispatchStatus);
            paramArr[25] = new SqlParameter("@p_vs_corp_customer", _corpCustomer);
            paramArr[26] = new SqlParameter("@p_vs_corp_lpono", _corpLPNno);

            paramArr[27] = new SqlParameter("@p_vs_grnt_name", _grntName);
            paramArr[28] = new SqlParameter("@P_VS_GRNT_PASSPORT_NO", _grntPassportNo);
            if(_grntPassportIssueDate != DateTime.MinValue) paramArr[29] = new SqlParameter("@P_VS_GRNT_PASSPORT_ISSUE_DATE", _grntPassportIssueDate);
            if(_grntPassportExpDate != DateTime.MinValue) paramArr[30] = new SqlParameter("@P_VS_GRNT_PASSPORT_EXPIRY_DATE", _grntPassportExpDate);
            if (!string.IsNullOrEmpty(_grntVisaNo)) paramArr[31] = new SqlParameter("@P_VS_GRNT_VISA_NUMBER", _grntVisaNo);
            if (_grntVisaExpDate != DateTime.MinValue) paramArr[32] = new SqlParameter("@P_VS_GRNT_VISA_EXPIRY_DATE", _grntVisaExpDate);
            if (_grntNationalityId>0) paramArr[33] = new SqlParameter("@P_VS_GRNT_NATIONALITY_ID", _grntNationalityId);
            paramArr[34] = new SqlParameter("@P_VS_GRNT_MOBILE_NO", _grntMobileNo);
            if(!string.IsNullOrEmpty(_grntOfficeNo))paramArr[35] = new SqlParameter("@P_VS_GRNT_OFFICE_NO", _grntOfficeNo);
            paramArr[36] = new SqlParameter("@P_VS_GRNT_EMAIL", _grntEmail);
            if(!string.IsNullOrEmpty(_grnReceiptNo))paramArr[37] = new SqlParameter("@P_VS_RECEIPT_NO", _grnReceiptNo);
            paramArr[38] = new SqlParameter("@P_VS_DISCOUNT", _discount);
            //vij
            paramArr[39] = new SqlParameter("@P_VISA_URGENT_STATUS", _visaUrgentStatus );
            paramArr[40] = new SqlParameter("@P_VISA_URGENT_FEE", _visaUrgentFee);
            //vij
            paramArr[41] = new SqlParameter("@p_vs_status", _status);
            paramArr[42] = new SqlParameter("@p_vs_location_id", _locationId);
            paramArr[43] = new SqlParameter("@p_vs_agent_id", _agentId);
            paramArr[44] = new SqlParameter("@p_vs_created_by", _createdBy);

            //starting Mode
            paramArr[45] = new SqlParameter("@P_VS_SETTLEMENT_MODE", _settlementMode);
            paramArr[46] = new SqlParameter("@P_VS_CURRENCY_CODE", _currencyCode);
            paramArr[47] = new SqlParameter("@P_VS_EXCHANGE_RATE", _exchangeRate);
            paramArr[48] = new SqlParameter("@P_VS_CASH_MODE", _cashMode.Mode);
            paramArr[49] = new SqlParameter("@P_VS_CASH_BASE_AMOUNT", _cashMode.BaseAmount);
            paramArr[50] = new SqlParameter("@P_VS_CASH_LOCAL_AMOUNT", _cashMode.LocalAmount);
            paramArr[51] = new SqlParameter("@P_VS_CREDIT_MODE", _creditMode.Mode);
            paramArr[52] = new SqlParameter("@P_VS_CREDIT_BASE_AMOUNT", _creditMode.BaseAmount);
            paramArr[53] = new SqlParameter("@P_VS_CREDIT_LOCAL_AMOUNT", _creditMode.LocalAmount);
            paramArr[54] = new SqlParameter("@P_VS_CARD_MODE", _cardMode.Mode);
            paramArr[55] = new SqlParameter("@P_VS_CARD_BASE_AMOUNT", _cardMode.BaseAmount);
            paramArr[56] = new SqlParameter("@P_VS_CARD_LOCAL_AMOUNT", _cardMode.LocalAmount);
            paramArr[57] = new SqlParameter("@P_VS_EMPLOYEE_MODE", _employeeMode.Mode);
            paramArr[58] = new SqlParameter("@P_VS_EMPLOYEE_BASE_AMOUNT", _employeeMode.BaseAmount);
            paramArr[59] = new SqlParameter("@P_VS_EMPLOYEE_LOCAL_AMOUNT", _employeeMode.LocalAmount);
            paramArr[60] = new SqlParameter("@P_VS_OTHERS_MODE", _othersMode.Mode);
            paramArr[61] = new SqlParameter("@P_VS_OTHERS_BASE_AMOUNT", _othersMode.BaseAmount);
            paramArr[62] = new SqlParameter("@P_VS_OTHERS_LOCAL_AMOUNT", _othersMode.LocalAmount);
            paramArr[63] = new SqlParameter("@P_VS_MODE_REMARKS", _modeRemarks);

            if (!string.IsNullOrEmpty(_crCardName)) paramArr[64] = new SqlParameter("@P_VS_CRCARD_NAME", _crCardName);
            paramArr[65] = new SqlParameter("@P_VS_CRCARD_RATE", _crCardRate);
            paramArr[66] = new SqlParameter("@P_VS_CRCARD_TOT", _crCardTotal);
            if (!string.IsNullOrEmpty(_pnrUCCF)) paramArr[67] = new SqlParameter("@P_VS_PNR_UCCF", _pnrUCCF);
            if (!string.IsNullOrEmpty(_pnrSettMode)) paramArr[68] = new SqlParameter("@P_VS_PNR_SETTLEMENT_MODE", _pnrSettMode);
            if (!string.IsNullOrEmpty(_chequeBank)) paramArr[69] = new SqlParameter("@P_VS_CHEQUE_BANK", _chequeBank);
            paramArr[70] = new SqlParameter("@P_vs_airport_deposit_status", _visaAirportDepositStatus);
            paramArr[71] = new SqlParameter("@P_vs_airport_deposit_fee", _visaAirportDepositFee);
            paramArr[72] = new SqlParameter("@P_vs_meet_assist_status", _visaMeetAssistStatus);
            paramArr[73] = new SqlParameter("@P_vs_meet_assist_fee", _visaMeetAssistFee);
            paramArr[74] = new SqlParameter("@P_vs_quick_checkout_status", _visaQuickCheckOutStatus);
            paramArr[75] = new SqlParameter("@P_vs_quick_checkout_fee", _visaQuickCheckOutFee);
            paramArr[76] = new SqlParameter("@P_VS_INTERFACE_STATUS", _mappedStatus);// Interface Status
            paramArr[77] = new SqlParameter("@P_VS_DOWNLOAD_STATUS", _downloadStatus);
            paramArr[78] = new SqlParameter("@P_VS_DOWNLOAD_FEE", _downloadFee);


            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[79] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[80] = paramMsgText;

            SqlParameter paramMsgText1 = new SqlParameter("@p_vs_id_ret", SqlDbType.BigInt);
            paramMsgText1.Size = 20;
            paramMsgText1.Direction = ParameterDirection.Output;
            paramArr[81] = paramMsgText1;
            paramArr[82] = new SqlParameter("@p_vs_visa_agent_fee", _visaAgentFee);
            
            SqlParameter paramMsgText2 = new SqlParameter("@P_RET_AGENT_BALANCE", SqlDbType.Decimal);
            paramMsgText2.Size = 20;
            paramMsgText2.Direction = ParameterDirection.Output;
            paramArr[83] = paramMsgText2;
            //trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
            DBGateway.ExecuteNonQueryDetails(cmd, "visa_p_visa_sales_add_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

            _docNumber = Utility.ToString(paramDocNo.Value);
            _transactionId = Utility.ToLong(paramMsgText1.Value);
            _agentBalance = Utility.ToDecimal(paramMsgText2.Value);

            if (Utility.ToString(paramMsgType.Value) != "S")
            {
                _retMessage = Utility.ToString(paramMsgText.Value);
            }
            else _retMessage = string.Empty;

            if (_dtVisaPaxDetails != null && _dtVisaPaxDetails.Rows.Count>0)
            {
                DataTable dt = _dtVisaPaxDetails.GetChanges();
                int recordStatus = 0;
                if (dt != null && dt.Rows.Count > 0)
                {

                    //try
                    //{
                    foreach (DataRow dr in dt.Rows)
                    {
                        switch (dr.RowState)
                        {
                            case DataRowState.Added: recordStatus = 1;
                                break;
                            case DataRowState.Modified: recordStatus = 2;
                                break;
                            case DataRowState.Deleted: recordStatus = -1;
                                break;
                            default: break;


                        }
                        saveDetails(cmd, recordStatus, Utility.ToLong(dr["pax_id"]), _transactionId, Utility.ToString(dr["pax_visitor_name"]), Utility.ToString(dr["pax_visitor_surname"]),
                                Utility.ToString(dr["pax_visitor_fathername"]), Utility.ToString(dr["pax_visitor_mothername"]), Utility.ToString(dr["pax_visitor_sex"]),
                                Utility.ToDate(dr["pax_visitor_dob"]), Utility.ToString(dr["pax_visitor_pob"]), Utility.ToString(dr["pax_visitor_occupation"]), Utility.ToString(dr["pax_visitor_relegion"]),
                                Utility.ToString(dr["pax_visitor_maritial_status"]), Utility.ToString(dr["pax_visitor_spouse_name"]), Utility.ToString(dr["pax_visitor_passport_no"]), Utility.ToString(dr["pax_visitor_passport_issue_place"]), Utility.ToDate(dr["pax_visitor_passport_issue_date"]),
                                Utility.ToDate(dr["pax_visitor_passport_expiry_date"]),Utility.ToString(dr["pax_visitor_relation"]),Utility.ToString(dr["pax_visitor_accompanied_by"]),
                                Utility.ToInteger(dr["pax_visitor_nationality_id"]), Utility.ToString(dr["pax_visitor_mobile"]), Utility.ToString(dr["pax_visitor_email"]), Utility.ToString(dr["pax_visa_entry_no"]), Utility.ToString(dr["pax_status"]), _createdBy, Utility.ToLong(dr["PAX_PARENT_ID"]), Utility.ToDate(dr["pax_arrival_date"]));

                         SaveAddServiceDetails(cmd, recordStatus,-1, _transactionId, _docNumber, _docDate,_paxIdRet , Utility.ToString(dr["pax_visitor_name"]),
                            Utility.ToString(dr["pax_visitor_passport_no"]), _visaAirportDepositStatus == "Y" ? AddDispatchStatus.Pending : AddDispatchStatus.All,
                            _visaMeetAssistStatus == "Y" ? AddDispatchStatus.Pending : AddDispatchStatus.All,AddDispatchStatus.All, _addDocId , _addDocName, _locationId, _agentId, Utility.ToString(dr["pax_status"]), _createdBy,DateTime.MinValue ,string.Empty );


                    }
                    //}
                    //catch { throw; }
                    //finally
                    //{
                    //    DBGateway.CloseConnection(cmd);
                    //}
                }
            }
            else throw new Exception("Pax Details shloud not be empty");
            if (!string.IsNullOrEmpty(_docKeys))
            {
                string[] arrDocKeys = _docKeys.Split(',');
                // SqlCommand cmd = DBGateway.CreateCommand();
                //try
                //{
                string[] arrDocStatus = _visaDocStatus.Split(',');
                for (int x = 0; x < arrDocKeys.Length-1 ; x++)
                {
                    saveDocDetails(cmd, 1, Utility.ToLong(arrDocKeys[x].ToString()), _transactionId, Settings.ACTIVE,arrDocStatus[x].ToString(), _createdBy,Settings.LoginInfo.AgentId );
                }
                //}
                //catch { throw; }
                //finally
                //{
                //    DBGateway.CloseConnection(cmd);
                //}
            }
            if (_objORAinterface != null)
            {
                _objORAinterface.OtherReference = _docNumber;
                _objORAinterface.VsMapId = _transactionId;
                _objORAinterface.VsMapDocNo = _docNumber;
                _objORAinterface.Save();

                updateReceiptNo(cmd, _transactionId, _objORAinterface.DocNumber);
            }
            trans.Commit();
            //cmd.Connection.Close();
        }
        catch
        {
            trans.Rollback();
            //cmd.Connection.Close();
            throw;
        }
        finally
        {
            cmd.Connection.Close();
        }
    }
    //private void saveDetails(SqlCommand cmd, int recordStatus, long paxId, long paxHeaderId, string gurantorName, string gurantorMobileNo, string gurantorEmail, string paxName,
    //                         string paxPassport, int paxNationalityId, string paxMobile, string paxEmail, string paxStatus, long paxCreatedBy)
    private void saveDetails(SqlCommand cmd, int recordStatus, long paxId, long paxHeaderId, string visitorName,
        string visitorSurname, string visitorFatherName, string visitorMotherName, string visitorSex,
        DateTime visitorDOB, string visitorPOB, string visitorOccupation, string visitorReligion, string visitorMaritialStatus, string visitorSpouseName,
        string visitorPassportNo, string visitorPspIssuePlace, DateTime visitorPspIssueDate, DateTime visitorPspExpDate, string visitorRelation,
        string visitorAccompaniedBy, int visitorNationalityId, string visitorMobile, string visitorEmail, string visitorVisaEntryNo, string visitorStatus, long paxCreatedBy,long visaParentId,DateTime parentArrivalDate)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[34];
            paramArr[0] = new SqlParameter("@p_record_status", recordStatus);
            paramArr[1] = new SqlParameter("@P_PAX_ID", paxId);
            paramArr[2] = new SqlParameter("@p_pax_vsh_id", paxHeaderId);
            paramArr[3] = new SqlParameter("p_pax_visitor_name", visitorName);
            if(!string.IsNullOrEmpty(visitorSurname))paramArr[4] = new SqlParameter("@P_pax_visitor_surname", visitorSurname);
            if (!string.IsNullOrEmpty(visitorFatherName)) paramArr[5] = new SqlParameter("@P_pax_visitor_fathername", visitorFatherName);
            if(!string.IsNullOrEmpty(visitorMotherName)) paramArr[6] = new SqlParameter("@P_pax_visitor_mothername", visitorMotherName);
            if(!string.IsNullOrEmpty(visitorSex)) paramArr[7] = new SqlParameter("@P_pax_visitor_sex", visitorSex);
            if(visitorDOB!=DateTime.MinValue) paramArr[8] = new SqlParameter("@P_pax_visitor_dob", visitorDOB);
            if(!string.IsNullOrEmpty(visitorPOB)) paramArr[9] = new SqlParameter("@P_pax_visitor_pob", visitorPOB);
            if(!string.IsNullOrEmpty(visitorOccupation)) paramArr[10] = new SqlParameter("@P_pax_visitor_occupation", visitorOccupation);
            if(!string.IsNullOrEmpty(visitorReligion)) paramArr[11] = new SqlParameter("@P_pax_visitor_relegion", visitorReligion);
            if (!string.IsNullOrEmpty(visitorMaritialStatus)) paramArr[12] = new SqlParameter("@P_pax_visitor_maritial_status", visitorMaritialStatus);
            if (!string.IsNullOrEmpty(visitorSpouseName)) paramArr[13] = new SqlParameter("@P_pax_visitor_spouse_name", visitorSpouseName);
            paramArr[14] = new SqlParameter("@P_pax_visitor_passport_no", visitorPassportNo);
            if (!string.IsNullOrEmpty(visitorPspIssuePlace)) paramArr[15] = new SqlParameter("@P_pax_visitor_passport_issue_place", visitorPspIssuePlace);
            if (visitorPspIssueDate != DateTime.MinValue) paramArr[16] = new SqlParameter("@P_pax_visitor_passport_issue_date", visitorPspIssueDate);
            if (visitorPspExpDate != DateTime.MinValue) paramArr[17] = new SqlParameter("@P_pax_visitor_passport_expiry_date", visitorPspExpDate);
            if (!string.IsNullOrEmpty(visitorRelation)) paramArr[18] = new SqlParameter("@P_pax_visitor_relation", visitorRelation);
            if (!string.IsNullOrEmpty(visitorAccompaniedBy)) paramArr[19] = new SqlParameter("@P_pax_visitor_accompanied_by", visitorAccompaniedBy);
            if (visitorNationalityId>0) paramArr[20] = new SqlParameter("@P_pax_visitor_nationality_id", visitorNationalityId);
            if (!string.IsNullOrEmpty(visitorMobile)) paramArr[21] = new SqlParameter("@P_pax_visitor_mobile", visitorMobile);
            if (!string.IsNullOrEmpty(visitorEmail)) paramArr[22] = new SqlParameter("@P_pax_visitor_email", visitorEmail);
            if (!string.IsNullOrEmpty(visitorVisaEntryNo)) paramArr[23] = new SqlParameter("@P_pax_visitor_visa_entry_no", visitorVisaEntryNo);
            if (visaParentId > 0) paramArr[24] = new SqlParameter("@P_pax_pax_visa_parent_id", visaParentId);
            if (parentArrivalDate != DateTime.MinValue) paramArr[25] = new SqlParameter("@P_pax_parent_arrival_date", parentArrivalDate);
            paramArr[26] = new SqlParameter("@P_pax_status", visitorStatus);
            paramArr[27] = new SqlParameter("@P_pax_created_by", paxCreatedBy);
            if (_visaAirportDepositStatus == "Y") paramArr[28] = new SqlParameter("@P_pax_hala_status", _visaAirportDepositStatus);
            if (_visaMeetAssistStatus == "Y") paramArr[29] = new SqlParameter("@P_pax_meet_assist_status", _visaMeetAssistStatus);
            paramArr[30] = new SqlParameter("@P_pax_agent_id", Settings.LoginInfo.AgentId);
            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[31] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[32] = paramMsgText;

            SqlParameter paramPaxIdRet = new SqlParameter("@P_PAX_ID_RET", SqlDbType.BigInt);
            paramPaxIdRet.Direction = ParameterDirection.Output; paramPaxIdRet.Size = 100;
            paramArr[33] = paramPaxIdRet;
              
            //SqlParameter paramMsgText1 = new SqlParameter("@p_vs_id_ret", SqlDbType.BigInt);
            //paramMsgText1.Size = 20;
            //paramMsgText1.Direction = ParameterDirection.Output;
            //paramArr[31] = paramMsgText1;



            DBGateway.ExecuteNonQueryDetails(cmd, "visa_p_visa_sales_details_add_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

            _paxIdRet = Utility.ToLong(paramPaxIdRet.Value);
            //_transactionId = Utility.ToLong(paramMsgText1.Value);
        }
        catch { throw; }
    }

    public static void SaveAddServiceDetails(SqlCommand cmd,int recordStatus,long addId,long visaId, string visaDocNo, DateTime docDate, long addPaxId, string paxName, string pptNo,AddDispatchStatus addHala,AddDispatchStatus meetAssist,AddDispatchStatus pinkVisa,long docId,string docName,long locationId,long agentId, string addStatus, long createdBy,DateTime dateOfTravel,string visaNo)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[21];

            param[0]=new SqlParameter("@P_RECORD_STATUS",recordStatus );
            param[1]= new SqlParameter("@P_add_id",addId);
            param[2]= new SqlParameter("@P_add_vsh_id",visaId);
            param[3]= new SqlParameter("@P_add_vs_doc_no",visaDocNo);
            param[4] = new SqlParameter("@P_add_doc_date", docDate );
            if(addPaxId>0) param[5] = new SqlParameter("@P_add_pax_id", addPaxId );
            param[6] = new SqlParameter("@P_add_pax_name", paxName );
            param[7] = new SqlParameter("@P_add_passport_no", pptNo);
            //param[6] = new SqlParameter("@P_add_visa_no",VisaNo);
            if (addHala != AddDispatchStatus.All) param[8] = new SqlParameter("@P_add_hala_status",Utility.ToString((char)addHala));
            if (meetAssist != AddDispatchStatus.All) param[9] = new SqlParameter("@P_add_meet_assist_status", Utility.ToString((char)meetAssist));
            if (pinkVisa != AddDispatchStatus.All) param[10] = new SqlParameter("@P_add_pink_visa_status", Utility.ToString((char)pinkVisa));
           // param[9] = new SqlParameter("@P_add_date_of_travel", dateOfTravel);
            if (docId>0) param[11] = new SqlParameter("@P_add_doc_id", docId);
            if (!string.IsNullOrEmpty(docName)) param[12] = new SqlParameter("@P_add_doc_name", docName);
            //if(!string.IsNullOrEmpty(uploadFileName)) param[12] = new SqlParameter("@P_add_upload_file_name", uploadFileName );
            //if (!string.IsNullOrEmpty(uploadFilepath)) param[13] = new SqlParameter("@P_add_upload_file_path", uploadFilepath);
            //if (!string.IsNullOrEmpty(uploadFileType)) param[14] = new SqlParameter("@P_add_upload_file_type", uploadFileType);
            //if (!string.IsNullOrEmpty(uploadStatus)) param[15] = new SqlParameter("@P_add_upload_status", uploadStatus);

            param[13] = new SqlParameter("@P_add_location_id", locationId);
            param[14] = new SqlParameter("@P_add_agent_id", agentId );
            param[15] = new SqlParameter("@P_add_status", addStatus);
            param[16] = new SqlParameter("@P_add_created_by", createdBy);
            if(!string.IsNullOrEmpty(visaNo)) param[17] = new SqlParameter("@P_add_visa_no", visaNo);
            if (dateOfTravel != DateTime.MinValue) param[18] = new SqlParameter("@P_add_date_of_travel", dateOfTravel);
            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            param[19] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            param[20] = paramMsgText;

            DBGateway.ExecuteNonQueryDetails(cmd, "visa_p_visa_addservice_add_update", param);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));
        }
        
        catch{throw;}
    
    }
    public static object GetNextAddSvcDispatchDocSerial()
    {
        try
        {
            
            string query = string.Format("SELECT ISNULL(MAX(add_dispatch_no),0) + 1 FROM DBO.VISA_T_VISA_ADD_SERVICE_DETAILS");
            object obj = DBGateway.ExecuteScalar(query);
            return obj;
           
        }
        catch { throw; }
    }

    public static void UpdateAddSvcDipatchDetails(long addId,string serviceType, AddDispatchStatus  dispatchStatus, string visaNo,
        long createdBy, string uploadPath, string uploadType, string uploadFile,string uploadStatus,string remarks, DateTime dateOfTravel ,long paxId,long dispatchNo)
    {
        try
        {
            //getting Disptach Document Next Serial


            SqlParameter[] paramArr = new SqlParameter[15];
            paramArr[0] = new SqlParameter("@P_ADD_ID", addId);
            paramArr[1] = new SqlParameter("@P_DISPATCH_STATUS", Utility.ToString((char)dispatchStatus));
            paramArr[2] = new SqlParameter("@P_SERVICE_TYPE", serviceType);
            if (!string.IsNullOrEmpty(visaNo)) paramArr[3] = new SqlParameter("@P_add_visa_no", visaNo);
            if (!string.IsNullOrEmpty(uploadPath)) paramArr[4] = new SqlParameter("@P_add_upload_file_path", uploadPath);
            if (!string.IsNullOrEmpty(uploadType)) paramArr[5] = new SqlParameter("@P_add_upload_file_type", uploadType);
            if (!string.IsNullOrEmpty(uploadFile)) paramArr[6] = new SqlParameter("@P_add_upload_file_name", uploadFile);
            if (!string.IsNullOrEmpty(uploadStatus)) paramArr[7] = new SqlParameter("@P_add_upload_status", uploadStatus); 
            paramArr[8] = new SqlParameter("@P_DISPATCH_MODIFIED_BY", createdBy);
            if (!string.IsNullOrEmpty(remarks)) paramArr[9] = new SqlParameter("@P_add_remarks", remarks);
            if (dateOfTravel > DateTime.MinValue) paramArr[10] = new SqlParameter("@P_add_date_of_travel", dateOfTravel );
            paramArr[11] = new SqlParameter("@P_add_pax_id", paxId);
            paramArr[12] = new SqlParameter("@P_add_dispatch_no", dispatchNo);
            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[13] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[14] = paramMsgText;

            DBGateway.ExecuteNonQuery("visa_p_visa_addservice_dispatch_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }





    public static void saveDocDetails(SqlCommand cmd, int recordStatus, long docId, long docHeaderId, string docStatus, string visaDocStatus, long docCreatedBy,int agentId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[9];
            paramArr[0] = new SqlParameter("@p_record_status", recordStatus);
            paramArr[1] = new SqlParameter("@P_VS_ID", docHeaderId);
            paramArr[2] = new SqlParameter("@P_VS_VISA_DOC_ID", docId);
            paramArr[3] = new SqlParameter("@P_VS_VISA_DOC_STATUS", visaDocStatus);
            paramArr[4] = new SqlParameter("@P_VS_DOC_STATUS", docStatus);

            paramArr[5] = new SqlParameter("@P_VS_DOC_CREATED_BY", docCreatedBy);

            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[6] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[7] = paramMsgText;
            if(agentId>0) paramArr[8] = new SqlParameter("@P_VS_VISA_DOC_AGENT_ID", agentId);



            DBGateway.ExecuteNonQueryDetails(cmd, "visa_p_visa_sales_doc_add_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }
    private void updateReceiptNo(SqlCommand cmd, long vsId, string receiptNo)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[4];
            paramArr[0] = new SqlParameter("@P_VS_ID", vsId);
            paramArr[1] = new SqlParameter("@P_VS_MAP_RECEIPT_NO", receiptNo);

            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[2] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[3] = paramMsgText;
            DBGateway.ExecuteNonQueryDetails(cmd, "VISA_P_VISA_SALES_RECEIPT_UPDATE", paramArr);// ziya IF

        }
        catch { throw; }
    }
    # endregion

    # region Static Methods
    public static DataSet GetList(long userID, string memberType, long locationId, string dispatchStatus, ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[7];
            paramArr[0] = new SqlParameter("@P_USER_ID", userID);
            paramArr[1] = new SqlParameter("@P_USER_TYPE", memberType);
            paramArr[2] = new SqlParameter("@p_user_location_id", locationId);
            paramArr[3] = new SqlParameter("@P_VS_DISPATCH_STATUS", status);
            paramArr[4] = new SqlParameter("@P_LIST_STATUS", Utility.ToString((char)status));
            paramArr[5] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
            return DBGateway.ExecuteQuery("ct_p_visa_sales_getlist", paramArr);
        }
        catch { throw; }
    }
    public static DataSet GetDetailList(DateTime fromDate,DateTime toDate,int visaTypeId,int salesTypeId,int nationalityId,long userId,string memberType,
        long locationId, int agentId, VisaDispatchStatus dispatchStatus, string dispatchDocType, string passport, string mobile, string vsDocNo,
        string DBOdocNo,string DAPdocNo,string visitorName,string trackingNo,
        long vsUserId,long vsLocationId,  ListStatus status, RecordStatus recordStatus,string visaUrgentStatus,string visaHalaStatus,string meetAssistStatus, string quickCheckStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[26];
            if(fromDate!=DateTime.MinValue)paramArr[0] = new SqlParameter("@p_from_date", fromDate);
            paramArr[1] = new SqlParameter("@p_to_date", toDate);
            if(visaTypeId>0)paramArr[2] = new SqlParameter("@p_vs_visa_type_id", visaTypeId);
            if (salesTypeId > 0) paramArr[3] = new SqlParameter("@p_vs_sales_type_id", salesTypeId);
            if (nationalityId > 0) paramArr[4] = new SqlParameter("@p_vs_nationality_id", nationalityId);
            //if (userId <= 0) userId = Settings.LoginInfo.UserID;
            paramArr[5] = new SqlParameter("@p_user_id", userId);
            paramArr[6] = new SqlParameter("@p_user_type", memberType);
            paramArr[7] = new SqlParameter("@p_location_id", locationId);
            if(agentId>0) paramArr[8] = new SqlParameter("@P_VS_AGENT_ID", agentId);
            if(dispatchStatus!=VisaDispatchStatus.All)paramArr[9] = new SqlParameter("@p_vs_dispatch_status", Utility.ToString((char)dispatchStatus));
            if(!string.IsNullOrEmpty(dispatchDocType))paramArr[10] = new SqlParameter("@p_vs_doc_type", dispatchDocType);
            if (!string.IsNullOrEmpty(passport)) paramArr[11] = new SqlParameter("@P_PAX_PASSPORT_NO", passport);
            if (!string.IsNullOrEmpty(mobile)) paramArr[12] = new SqlParameter("@P_PAX_MOBILE", mobile);
            if (!string.IsNullOrEmpty(vsDocNo)) paramArr[13] = new SqlParameter("@P_VS_DOC_NO", vsDocNo);
            if (!string.IsNullOrEmpty(DBOdocNo)) paramArr[14] = new SqlParameter("@P_PAX_DBO_DOC_NO", DBOdocNo);
            if (!string.IsNullOrEmpty(DAPdocNo)) paramArr[15] = new SqlParameter("@P_PAX_DAP_DOC_NO", DAPdocNo);
            if (!string.IsNullOrEmpty(visitorName)) paramArr[16] = new SqlParameter("@P_PAX_VISITOR_NAME", visitorName);
            if (!string.IsNullOrEmpty(trackingNo)) paramArr[17] = new SqlParameter("@P_PAX_DAP_TRACKING_NO", trackingNo);
            if (vsUserId>0) paramArr[18] = new SqlParameter("@p_vs_user_id", vsUserId);
            if (vsLocationId>0) paramArr[19] = new SqlParameter("@p_vs_location_id", vsLocationId);
            if (!string.IsNullOrEmpty(visaUrgentStatus)) paramArr[20] = new SqlParameter("@P_VISA_URGENT_STATUS", visaUrgentStatus);
            if (!string.IsNullOrEmpty(visaHalaStatus)) paramArr[21] = new SqlParameter("@P_VS_AIRPORT_DEPOSIT_STATUS", visaHalaStatus);
            if (!string.IsNullOrEmpty(meetAssistStatus)) paramArr[22] = new SqlParameter("@P_VS_MEET_ASSIST_STATUS", meetAssistStatus);
            if (!string.IsNullOrEmpty(quickCheckStatus)) paramArr[23] = new SqlParameter("@P_VS_QUICK_CHECKOUT_STATUS", quickCheckStatus);
            paramArr[24] = new SqlParameter("@p_list_status", recordStatus);
            paramArr[25] = new SqlParameter("@p_record_status", recordStatus);
            return DBGateway.ExecuteQuery("visa_p_visa_sales_getDetailsList", paramArr);
        }
        catch { throw; }
    }
    public static DataSet GetDetailList(DateTime fromDate, DateTime toDate, int visaTypeId, int salesTypeId, int nationalityId, long userId, string memberType,
       long locationId, int agentId, VisaDispatchStatus dispatchStatus, string dispatchDocType, string passport, string mobile, string vsDocNo,
       string DBOdocNo, string DAPdocNo, string visitorName, string trackingNo,
       long vsUserId, long vsLocationId, ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            return GetDetailList(fromDate, toDate, visaTypeId, salesTypeId, nationalityId, userId, memberType,
                locationId, agentId, dispatchStatus, dispatchDocType, passport, mobile, vsDocNo, DBOdocNo, DAPdocNo, visitorName, trackingNo,
                vsUserId, vsLocationId, status, recordStatus, string.Empty,string.Empty ,string.Empty,string.Empty );

        }
        catch { throw; }
    }
    public static void VSTicketDelete(long ticketId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[3];
            paramArr[0] = new SqlParameter("@p_vs_ticket_id",ticketId);
            
            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[1] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[2] = paramMsgText;
            DBGateway.ExecuteNonQuery("visa_p_visa_ticket_delete", paramArr);
        }
        catch { throw; }
    }

    //public static DataTable GetReportDetails(long transactionId)
    //{
    //    try
    //    {
    //        SqlParameter[] paramArr = new SqlParameter[1];
    //        SqlParameter param = new SqlParameter("@p_receipt_id", SqlDbType.Decimal);
    //        param.Value = transactionId; param.Size = 8;
    //        paramArr[0] = param;

    //        DataTable dt = DBGateway.ExecuteQuery("ct_p_receipt_getdata", paramArr).Tables[0];
    //        return dt;
    //    }
    //    catch { throw; }
    //}

    public static DataTable GetVSSalesAirportDepositDetails(long visaId, int agentId, string reportType)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[4];

            SqlParameter param = new SqlParameter("@P_VS_ID", SqlDbType.BigInt);
            param.Value = visaId;
            paramArr[0] = param;

            if (agentId>0)
            {
                param = new SqlParameter("@P_AGENT_ID", SqlDbType.BigInt);
                param.Value = agentId;
                paramArr[1] = param;
            }

            if (!string.IsNullOrEmpty(reportType))
            {
                param = new SqlParameter("@P_PRINT_REPORT", SqlDbType.NVarChar);
                param.Value = reportType ;
                paramArr[2] = param;
            }

            param = new SqlParameter("@P_USER_ID", SqlDbType.Int);
            param.Value = Settings.LoginInfo.UserID;
            paramArr[3] = param;
            return DBGateway.ExecuteQuery("visa_p_visa_sales_apt_deposit_getlist", paramArr).Tables[0];
        }
        catch { throw; }
    }

    public static DataTable GetVSSalesAirportDepositReport(long visaId, int agentId, string reportType, DateTime fromDate, DateTime toDate)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[6];

            SqlParameter param = new SqlParameter("@P_PRINT_REPORT", SqlDbType.VarChar);
            param.Value = reportType;
            paramArr[0] = param;

            if (visaId > 0)
            {
                param = new SqlParameter("@P_VS_ID", SqlDbType.BigInt);
                param.Value = visaId;
                paramArr[1] = param;
            }
            if (agentId > 0)
            {
                param = new SqlParameter("@P_AGENT_ID", SqlDbType.BigInt);
                param.Value = agentId;
                paramArr[2] = param;
            }


            if (fromDate != DateTime.MinValue)
            {
                param = new SqlParameter("@P_FROM_DATE", SqlDbType.DateTime);
                param.Value = fromDate;
                paramArr[3] = param;
            }

            if (toDate != DateTime.MinValue)
            {
                param = new SqlParameter("@P_TO_DATE", SqlDbType.DateTime);
                param.Value = toDate;
                paramArr[4] = param;
            }
            param = new SqlParameter("@P_USER_ID", SqlDbType.Int);
            param.Value = Settings.LoginInfo.UserID;
            paramArr[5] = param;

            return DBGateway.ExecuteQuery("visa_p_visa_sales_apt_deposit_report_details", paramArr).Tables[0];
        }
        catch { throw; }
    }

    public static DataSet GetVisitorControlManifest(DateTime arrivalFromDate, DateTime arrivalToDate, DateTime departFromDate, DateTime departToDate,
                DateTime rcptFromDate, DateTime rcptToDate, long visaTypeId, long agentId, long locationId, long userId, long consultantId, long vsLocationId, VisaDispatchStatus dispatchStatus, string memberType,
                ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[16];
            if (arrivalFromDate != DateTime.MinValue) paramArr[0] = new SqlParameter("@P_FROM_ARRIVAL_DATE", arrivalFromDate);
            if (arrivalToDate != DateTime.MinValue) paramArr[1] = new SqlParameter("@P_TO_ARRIVAL_DATE", arrivalToDate);
            if (departFromDate != DateTime.MinValue) paramArr[2] = new SqlParameter("@P_FROM_DEPART_DATE", departFromDate);
            if (departToDate != DateTime.MinValue) paramArr[3] = new SqlParameter("@P_TO_DEPART_DATE", departToDate);
            if (rcptFromDate != DateTime.MinValue) paramArr[4] = new SqlParameter("@P_FROM_RCPT_DATE", rcptFromDate);
            if (rcptToDate != DateTime.MinValue) paramArr[5] = new SqlParameter("@P_TO_RCPT_DATE", rcptToDate);
            if (visaTypeId > 0) paramArr[6] = new SqlParameter("@P_VS_VISA_TYPE_ID", visaTypeId);
            if (consultantId > 0) paramArr[7] = new SqlParameter("@P_VS_USER_ID", consultantId);
            //if (userId <= 0) userId = Settings.LoginInfo.UserID;
            paramArr[8] = new SqlParameter("@p_user_id", userId);
            paramArr[9] = new SqlParameter("@p_user_type", memberType);
            paramArr[10] = new SqlParameter("@p_location_id", locationId);
            if (agentId > 0) paramArr[11] = new SqlParameter("@P_VS_AGENT_ID", agentId);
            if (dispatchStatus != VisaDispatchStatus.All) paramArr[12] = new SqlParameter("@p_vs_dispatch_status", Utility.ToString((char)dispatchStatus));
            if (vsLocationId > 0) paramArr[13] = new SqlParameter("@p_vs_location_id", vsLocationId);
            paramArr[14] = new SqlParameter("@p_list_status", status);
            paramArr[15] = new SqlParameter("@p_record_status", recordStatus);
            return DBGateway.ExecuteQuery("visa_p_visa_visitor_control_manifest", paramArr);
        }
        catch { throw; }
    }

    public static DataSet GetAddServiceList(DateTime fromDate, DateTime toDate,string serviceType, long userID, string memberType, long locationId,long tranxLocationId, long agentId, string dispatchStatus, ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[11];
            if (fromDate != DateTime.MinValue) paramArr[0] = new SqlParameter("@P_FROM_DATE", fromDate);
            if (toDate != DateTime.MinValue) paramArr[1] = new SqlParameter("@P_TO_DATE", toDate );
            paramArr[2] = new SqlParameter("@P_SERVICE_TYPE", serviceType);
            paramArr[3] = new SqlParameter("@P_USER_ID", userID);
            paramArr[4] = new SqlParameter("@P_USER_TYPE", memberType);          
            if(locationId>0) paramArr[5] = new SqlParameter("@P_LOCATION_ID", locationId);
            if(tranxLocationId>0) paramArr[6] = new SqlParameter("@P_ADD_LOCATION_ID", tranxLocationId);
            if(agentId>0) paramArr[7] = new SqlParameter("@P_AGENT_ID", agentId );
            if(!string.IsNullOrEmpty(dispatchStatus)) paramArr[8] = new SqlParameter("@P_DISPATCH_STATUS", dispatchStatus );
            paramArr[9] = new SqlParameter("@P_LIST_STATUS", Utility.ToString((int)status));
            paramArr[10] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
            return DBGateway.ExecuteQuery("visa_p_visa_addservice_getList", paramArr);
        }
        catch { throw; }
    }


    public static DataSet GetAddServiceDispatchReport(string serviceType,long dispatchNo)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[2];
            paramArr[0] = new SqlParameter("@P_SERVICE_TYPE", serviceType);
            paramArr[1] = new SqlParameter("@P_ADD_DISPATCH_NO", dispatchNo);
            return DBGateway.ExecuteQuery("visa_p_visa_addservice_dispatch_report", paramArr);
        }
        catch { throw; }
    }

    # endregion

    # region DispatchTo BO

    public static object GetNextDispatchDocSerial(string  docType,long locationId)
    {
        try
        {
            //SqlParameter[] paramArr = new SqlParameter[2];
            //paramArr[0] = new SqlParameter("@P_DOC_TYPE", docType);
            //paramArr[1] = new SqlParameter("@P_DOC_LOCATION_ID", Settings.LoginInfo.LocationID);

            string query=string.Format("SELECT  DBO.GETDISPATCHDOCUMENTSERIAL('{0}',{1})",docType,locationId);
            object obj = DBGateway.ExecuteScalar(query);
            return obj;  
            //if (Utility.ToString(paramMsgType.Value) == "E")
            //    throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }
    public static void SaveDipatchDetails(long paxId, VisaDispatchStatus dispatchStatus, string dispatchNo, string dispatchRemarks, string trackingNo,long createdBy)
    {
        try
        {
            SaveDipatchDetails(paxId, dispatchStatus, dispatchNo, dispatchRemarks, trackingNo,string.Empty, DateTime.MinValue, DateTime.MinValue,
                string.Empty, string.Empty, DateTime.MinValue, DateTime.MinValue, createdBy,string.Empty,string.Empty,-1,string.Empty,string.Empty );
        }
        catch { throw; }
    }

    public static void SaveDipatchDetails(long paxId, VisaDispatchStatus dispatchStatus, string dispatchNo, string dispatchRemarks, string trackingNo,
        string visaNo, DateTime VisaIssueDate, DateTime VisaExpDate, string airlineCode, string flightNo, DateTime arrivalDate, DateTime departureDate, long createdBy, string actualAppliedThru, string paxUID,long visaParentId, string uploadPath, string uploadType)
    {
        try
        {
            //getting Disptach Document Next Serial
            

            SqlParameter[] paramArr = new SqlParameter[20];
            paramArr[0] = new SqlParameter("@P_PAX_ID", paxId);
            paramArr[1] = new SqlParameter("@P_DISPATCH_STATUS", Utility.ToString((char)dispatchStatus));
            paramArr[2] = new SqlParameter("@P_DISPATCH_NO", dispatchNo);
            paramArr[3] = new SqlParameter("@P_DISPATCH_REMARKS", dispatchRemarks);
            if (!string.IsNullOrEmpty(trackingNo)) paramArr[4] = new SqlParameter("@P_TRACKING_NO", trackingNo);
            if (!string.IsNullOrEmpty(visaNo)) paramArr[5] = new SqlParameter("@P_VISA_NO", visaNo);
            if (VisaIssueDate != DateTime.MinValue) paramArr[6] = new SqlParameter("@P_VISA_ISSUE_DATE", VisaIssueDate);
            if (VisaExpDate != DateTime.MinValue) paramArr[7] = new SqlParameter("@P_VISA_EXP_DATE", VisaExpDate);

            if (!string.IsNullOrEmpty(airlineCode)) paramArr[8] = new SqlParameter("@P_AIRLINE_CODE", airlineCode);
            if (!string.IsNullOrEmpty(flightNo)) paramArr[9] = new SqlParameter("@P_FLIGHT_NO", flightNo);
            if (arrivalDate != DateTime.MinValue) paramArr[10] = new SqlParameter("@P_ARRIVAL_DATE", arrivalDate);
            if (departureDate != DateTime.MinValue) paramArr[11] = new SqlParameter("@P_DEPARTURE_DATE", departureDate);
            paramArr[12] = new SqlParameter("@P_DISPATCH_MODIFIED_BY", createdBy);
            if (!string.IsNullOrEmpty(actualAppliedThru)) paramArr[13] = new SqlParameter("@P_ACTUAL_APPLIED_THRU", actualAppliedThru );
            if (!string.IsNullOrEmpty(paxUID)) paramArr[14] = new SqlParameter("@P_PAX_UID", paxUID);
            if (visaParentId > 0) paramArr[15] = new SqlParameter("@P_PAX_VISA_PARENT_ID", visaParentId);
            if (!string.IsNullOrEmpty(uploadPath)) paramArr[16] = new SqlParameter("@P_PAX_VISA_UPLD_PATH", uploadPath);
            if (!string.IsNullOrEmpty(uploadType)) paramArr[17] = new SqlParameter("@P_PAX_VISA_UPLD_TYPE", uploadType);
            
            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[18] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[19] = paramMsgText;

            DBGateway.ExecuteNonQuery("visa_p_visa_sales_dispath_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }
    public static void UpdateAccountedStatus(long vsId, VisaAccountedStatus accStatus, string pnrStatus, long ticketId, long createdBy, string mapReceiptNo)
    {
        try
        {
            //getting Disptach Document Next Serial
            SqlParameter[] paramArr = new SqlParameter[8];
            paramArr[0] = new SqlParameter("@P_VS_ID", vsId);
            paramArr[1] = new SqlParameter("@P_ACCOUNTED_STATUS", Utility.ToString((char)accStatus));
            paramArr[2] = new SqlParameter("@P_PNR_STATUS", pnrStatus);
            if (!string.IsNullOrEmpty(mapReceiptNo)) paramArr[3] = new SqlParameter("@P_MAP_RECEIPT_NO", mapReceiptNo);
            paramArr[4] = new SqlParameter("@P_TICKET_ID", ticketId);
            paramArr[5] = new SqlParameter("@P_MODIFIED_BY", createdBy);

            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[6] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[7] = paramMsgText;

            DBGateway.ExecuteNonQuery("visa_p_visa_sales_accounted_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }
    public static void UpdateAccountedStatus(long vsId, VisaAccountedStatus accStatus, string pnrStatus, long ticketId, long createdBy)
    {
        try
        {
            UpdateAccountedStatus(vsId, accStatus, pnrStatus, ticketId, createdBy, string.Empty);

        }
        catch { }
    }
    public static void UpdateDocSerial(VisaDispatchStatus dispatchStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[4];
            paramArr[0] = new SqlParameter("@P_DOC_TYPE", Utility.ToString((char)dispatchStatus));
            paramArr[1] = new SqlParameter("@P_LOCATION_ID", Settings.LoginInfo.LocationID);
            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[2] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[3] = paramMsgText;

            DBGateway.ExecuteNonQuery("visa_p_doc_serial_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }
    public static DataSet GetDispatchReportList(VisaDispatchStatus dispatchStatus, string dispatchRefNo, ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[4];
            
            paramArr[0] = new SqlParameter("@p_pax_dispatch_status", Utility.ToString((char)dispatchStatus));
            paramArr[1] = new SqlParameter("@p_pax_dispatch_ref_no", dispatchRefNo); 
            paramArr[2] = new SqlParameter("@p_list_status", recordStatus);
            paramArr[3] = new SqlParameter("@p_record_status", recordStatus);
            return DBGateway.ExecuteQuery("visa_p_visa_sales_getDispatchReportList", paramArr);
        }
        catch { throw; }
    }
    public static DataSet GetDashBoardList(long userId, string userType,long locationId, int companyId,
        ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[6];

            paramArr[0] = new SqlParameter("@P_USER_ID", userId);
            paramArr[1] = new SqlParameter("@P_USER_TYPE", userType);
            paramArr[2] = new SqlParameter("@P_LOCATION_ID", locationId);
            paramArr[3] = new SqlParameter("@P_COMPANY_ID", companyId);
            paramArr[4] = new SqlParameter("@p_list_status", recordStatus);
            paramArr[5] = new SqlParameter("@p_record_status", recordStatus);
            return DBGateway.ExecuteQuery("visa_p_dashboard_getDetails", paramArr);
        }
        catch { throw; }
    }
    # endregion

    # region History
    public static DataSet GetDetailHistoryList(DateTime fromDate, DateTime toDate, int visaTypeId, int salesTypeId, int nationalityId, long userId, string memberType,
        long locationId, int agentId, VisaDispatchStatus dispatchStatus, string dispatchDocType, string passport, string mobile, string vsDocNo,
        string DBOdocNo, string DAPdocNo, string visitorName, string trackingNo,
        long vsUserId, long vsLocationId, ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[22];
            paramArr[0] = new SqlParameter("@p_from_date", fromDate);
            paramArr[1] = new SqlParameter("@p_to_date", toDate);
            if (visaTypeId > 0) paramArr[2] = new SqlParameter("@p_vs_visa_type_id", visaTypeId);
            if (salesTypeId > 0) paramArr[3] = new SqlParameter("@p_vs_sales_type_id", salesTypeId);
            if (nationalityId > 0) paramArr[4] = new SqlParameter("@p_vs_nationality_id", nationalityId);
            //if (userId <= 0) userId = Settings.LoginInfo.UserID;
            paramArr[5] = new SqlParameter("@p_user_id", userId);
            paramArr[6] = new SqlParameter("@p_user_type", memberType);
            paramArr[7] = new SqlParameter("@p_location_id", locationId);
            if(agentId>0) paramArr[8] = new SqlParameter("@p_agent_id", agentId );
            if (dispatchStatus != VisaDispatchStatus.All) paramArr[9] = new SqlParameter("@p_vs_dispatch_status", Utility.ToString((char)dispatchStatus));
            if (!string.IsNullOrEmpty(dispatchDocType)) paramArr[10] = new SqlParameter("@p_vs_doc_type", dispatchDocType);
            if (!string.IsNullOrEmpty(passport)) paramArr[11] = new SqlParameter("@P_PAX_PASSPORT_NO", passport);
            if (!string.IsNullOrEmpty(mobile)) paramArr[12] = new SqlParameter("@P_PAX_MOBILE", mobile);
            if (!string.IsNullOrEmpty(vsDocNo)) paramArr[13] = new SqlParameter("@P_VS_DOC_NO", vsDocNo);
            if (!string.IsNullOrEmpty(DBOdocNo)) paramArr[14] = new SqlParameter("@P_PAX_DBO_DOC_NO", DBOdocNo);
            if (!string.IsNullOrEmpty(DAPdocNo)) paramArr[15] = new SqlParameter("@P_PAX_DAP_DOC_NO", DAPdocNo);
            if (!string.IsNullOrEmpty(visitorName)) paramArr[16] = new SqlParameter("@P_PAX_VISITOR_NAME", visitorName);
            if (!string.IsNullOrEmpty(trackingNo)) paramArr[17] = new SqlParameter("@P_PAX_DAP_TRACKING_NO", trackingNo);
            if (vsUserId > 0) paramArr[18] = new SqlParameter("@p_vs_user_id", vsUserId);
            if (vsLocationId > 0) paramArr[19] = new SqlParameter("@p_vs_location_id", vsLocationId);
            paramArr[20] = new SqlParameter("@p_list_status", recordStatus);
            paramArr[21] = new SqlParameter("@p_record_status", recordStatus);
            return DBGateway.ExecuteQuery("visa_p_visa_sales_hist_getDetailsList", paramArr);
        }
        catch { throw; }
    }
    public static DataTable GetDispatchDocNoList(VisaDispatchStatus dispStatus,ListStatus status, RecordStatus recordStatus)
    {
        try
        {
       
            SqlParameter[] paramArr = new SqlParameter[3];
            if (dispStatus != VisaDispatchStatus.All) paramArr[0] = new SqlParameter("@P_DISP_STATUS", (char)dispStatus);
            paramArr[1] = new SqlParameter("@P_LIST_STATUS", status);
            paramArr[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
            return DBGateway.ExecuteQuery("visa_p_visa_sales_dispatchNo_Getlist", paramArr).Tables[0];
       
        }
        catch { throw; }
    }
    public static DataTable GetVSDocNoList(long visaId,ListStatus status, RecordStatus recordStatus)
    {
        try
        {

            SqlParameter[] paramArr = new SqlParameter[3];
            paramArr[0] = new SqlParameter("@P_LIST_STATUS", status);
            paramArr[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
            if (visaId > 0) paramArr[2] = new SqlParameter("@P_VS_ID", visaId);  
            return DBGateway.ExecuteQuery("visa_p_visa_sales_HeaderDocNo_Getlist", paramArr).Tables[0];

        }
        catch { throw; }
    }

    public static DataSet GetPaxList(long visaId, ListStatus listStatus, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[3];
            SqlParameter param = new SqlParameter("@P_PAX_VSH_ID", SqlDbType.BigInt);
            param.Value = visaId;
            paramArr[0] = param;

            param = new SqlParameter("@P_LIST_STATUS", SqlDbType.Int);
            param.Value = listStatus;
            paramArr[1] = param;

            param = new SqlParameter("@P_RECORD_STATUS", SqlDbType.NVarChar);
            param.Value = Utility.ToString((char)recordStatus); param.Size = 10;
            paramArr[2] = param;

            return DBGateway.ExecuteQuery("vis_p_pax_details_getlist", paramArr);
        }
        catch { throw; }
    }
    # endregion

    #region VisaChange
    //Visa Change
    public static DataSet GetVisaChangeDetailsList(DateTime fromDate, DateTime toDate, int visaTypeId, int salesTypeId,
                    long userId, string memberType, long locationId, int agentId, long vsUserId, long vsLocationId, string approvalStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[11];
            if (fromDate != DateTime.MinValue) paramArr[0] = new SqlParameter("@p_from_date", fromDate);
            paramArr[1] = new SqlParameter("@p_to_date", toDate);
            if (visaTypeId > 0) paramArr[2] = new SqlParameter("@p_vs_visa_type_id", visaTypeId);
            if (salesTypeId > 0) paramArr[3] = new SqlParameter("@p_vs_sales_type_id", salesTypeId);
            //if (userId <= 0) userId = Settings.LoginInfo.UserID;
            paramArr[4] = new SqlParameter("@p_user_id", userId);
            paramArr[5] = new SqlParameter("@p_user_type", memberType);
            paramArr[6] = new SqlParameter("@p_location_id", locationId);
            if (agentId > 0) paramArr[7] = new SqlParameter("@P_VS_AGENT_ID", agentId);
            if (!string.IsNullOrEmpty(approvalStatus)) paramArr[8] = new SqlParameter("@P_CHNG_APPROVAL_STATUS", approvalStatus);
            if (vsUserId > 0) paramArr[9] = new SqlParameter("@p_vs_user_id", vsUserId);
            if (vsLocationId > 0) paramArr[10] = new SqlParameter("@p_vs_location_id", vsLocationId);
            return DBGateway.ExecuteQuery("visa_p_visa_change_getlist", paramArr);
        }
        catch { throw; }
    }

    public static DataSet GetDocPathDetails(long visaId,long paxId,long docId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[3];

            paramArr[0] = new SqlParameter("@P_VISA_ID", visaId);
            paramArr[1] = new SqlParameter("@P_PAX_ID", paxId);
            paramArr[2] = new SqlParameter("@P_DOC_ID", docId);
            return DBGateway.ExecuteQuery("visa_p_visa_doc_path_details", paramArr);
        }
        catch { throw; }
    }



    public static void SaveVisaChangeDetails(long changeId, long visaId, long paxId, string visaDocNo, string paxName, string passportNo, DateTime exitDate, string ticketMode, string approvalStatus, string status, long createdBy)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[13];
            paramArr[0] = new SqlParameter("@P_chng_id", changeId);
            paramArr[1] = new SqlParameter("@P_chng_vs_id", visaId);
            paramArr[2] = new SqlParameter("@P_chng_vs_doc_no", visaDocNo);
            paramArr[3] = new SqlParameter("@P_chng_pax_id", paxId);
            paramArr[4] = new SqlParameter("@P_chng_pax_name", paxName);
            paramArr[5] = new SqlParameter("@P_chng_pax_passport_no", passportNo);
            if(exitDate!=DateTime.MinValue) paramArr[6] = new SqlParameter("@P_chng_exit_date", exitDate);
            paramArr[7] = new SqlParameter("@P_chng_ticket_mode", ticketMode);
            paramArr[8] = new SqlParameter("@P_chng_approval_status", approvalStatus);
            paramArr[9] = new SqlParameter("@P_chng_status", status);
            paramArr[10] = new SqlParameter("@P_chng_created_by", createdBy);
            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[11] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[12] = paramMsgText;

            DBGateway.ExecuteNonQuery("visa_p_visa_change_details_add_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }

    #endregion


    # region Retainregion
    public static DataSet GetVisaRetainList(string visaEntryNo, string passport,RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[6];
            
            if (!string.IsNullOrEmpty(visaEntryNo))  paramArr[0] = new SqlParameter("@P_PAX_VISA_ENTRY_NO", visaEntryNo);
            if (!string.IsNullOrEmpty(passport)) paramArr[1] = new SqlParameter("@PAX_VISITOR_PASSPORT_NO", passport);
            if (Settings.LoginInfo.AgentId > 0) paramArr[2] = new SqlParameter("@P_VS_AGENT_ID", Settings.LoginInfo.AgentId);
            if (recordStatus != RecordStatus.All) paramArr[3] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[4] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[5] = paramMsgText;
            return DBGateway.ExecuteQuery("visa_p_visa_sales_retainList", paramArr);
        }
        catch { throw; }
    }

    public static DataSet GetUploadedDocumentList(long visaId, long paxId, ListStatus listStatus, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[4];

            SqlParameter param = new SqlParameter("@P_LIST_STATUS", SqlDbType.Int);
            param.Value = listStatus;
            paramArr[0] = param;

            param = new SqlParameter("@P_RECORD_STATUS", SqlDbType.NVarChar);
            param.Value = Utility.ToString((char)recordStatus); param.Size = 10;
            paramArr[1] = param;

            param = new SqlParameter("@P_UP_VISA_ID", SqlDbType.BigInt);
            param.Value = visaId;
            paramArr[2] = param;

            param = new SqlParameter("@P_UP_PAX_ID", SqlDbType.BigInt);
            param.Value = paxId;
            paramArr[3] = param;

            return DBGateway.ExecuteQuery("VISA_P_DOC_UPLOAD_DETAILS_GETLIST", paramArr);
        }
        catch { throw; }
    }

    public static DataTable GetSalesAdditionalServices(long visaId , ListStatus listStatus, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[3];

            SqlParameter param = new SqlParameter("@P_VS_ID", SqlDbType.BigInt);
            param.Value = visaId;
            paramArr[0] = param;           

            param = new SqlParameter("@P_LIST_STATUS", SqlDbType.Int);
            param.Value = listStatus;
            paramArr[1] = param;

            param = new SqlParameter("@P_RECORD_STATUS", SqlDbType.NVarChar);
            param.Value = Utility.ToString((char)recordStatus); param.Size = 10;
            paramArr[2] = param;

            return DBGateway.ExecuteQuery("visa_p_visa_sales_get_addservices", paramArr).Tables[0] ;

        }
        catch { throw; }
    }
#endregion

    # region Edit Visa Sales Search
    public static DataSet EditVisaGetList(long userID, string memberType, long locationId, VisaDispatchStatus dispatchStatus, int agentId, ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[9];
            paramArr[0] = new SqlParameter("@P_VS_DISPATCH_STATUS", Utility.ToString((char)dispatchStatus));
            paramArr[1] = new SqlParameter("@p_user_id", userID);
            paramArr[2] = new SqlParameter("@p_user_type", memberType);
            paramArr[3] = new SqlParameter("@p_location_id", locationId);
            if(agentId >0) paramArr[4] = new SqlParameter("@p_AGENT_id", agentId);
            paramArr[5] = new SqlParameter("@P_LIST_STATUS", status);
            if (recordStatus!= RecordStatus.All) paramArr[6] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[7] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[8] = paramMsgText;
            return DBGateway.ExecuteQuery("visa_p_visa_sales_getList", paramArr);
        }
        catch { throw; }
    }

    public static DataTable GetVisaCollectList(DateTime fromDate, DateTime toDate, string memberType, long locationId,
          long vsLocationId, VisaAccountedStatus accountedStatus, string settlementMode, string halaDeposite, string download,int agentId,  ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[12];
            if (fromDate != DateTime.MinValue) paramArr[0] = new SqlParameter("@p_from_date", fromDate);
            paramArr[1] = new SqlParameter("@p_to_date", toDate);
            paramArr[2] = new SqlParameter("@p_user_type", memberType);
            paramArr[3] = new SqlParameter("@P_LOCATION_ID", locationId);
            if (vsLocationId > 0) paramArr[4] = new SqlParameter("@p_vs_location_id", vsLocationId);
            if (accountedStatus != VisaAccountedStatus.All) paramArr[5] = new SqlParameter("@P_VS_ACCOUNTED_STATUS", Utility.ToString((char)accountedStatus));
            if (settlementMode != "-1") paramArr[6] = new SqlParameter("@P_SETTLEMENT_MODE", settlementMode);
            if (download != "-1") paramArr[7] = new SqlParameter("@P_VS_DOWNLOAD_STATUS", download);
            if (halaDeposite != "-1") paramArr[8] = new SqlParameter("@P_VS_APT_DEPOSIT_STATUS", halaDeposite);
            if (agentId > 0) paramArr[9] = new SqlParameter("@P_VS_AGENT_ID", agentId); 
            paramArr[10] = new SqlParameter("@p_list_status", recordStatus);
            paramArr[11] = new SqlParameter("@p_record_status", recordStatus);
            return DBGateway.ExecuteQuery("visa_p_visa_sales_getCollectList", paramArr).Tables[0];
        }
        catch { throw; }
    }

    public static DataSet GetVisaSalesList(DateTime fromDate, DateTime toDate, int visaTypeId, int salesTypeId, int nationalityId, long userId, string memberType,
           long locationId, int agentId, VisaDispatchStatus dispatchStatus,// string dispatchDocType, string passport, string mobile, 
            string vsDocNo,
        //string DBOdocNo, string DAPdocNo, string visitorName, string trackingNo,
           long vsUserId, long vsLocationId, VisaAccountedStatus accountedStatus, string settlementMode, string halaDeposite, string download, string urgentStatus, ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[20];
            if (fromDate != DateTime.MinValue) paramArr[0] = new SqlParameter("@p_from_date", fromDate);
            paramArr[1] = new SqlParameter("@p_to_date", toDate);
            if (visaTypeId > 0) paramArr[2] = new SqlParameter("@p_vs_visa_type_id", visaTypeId);
            if (salesTypeId > 0) paramArr[3] = new SqlParameter("@p_vs_sales_type_id", salesTypeId);
            if (nationalityId > 0) paramArr[4] = new SqlParameter("@p_vs_nationality_id", nationalityId);
            //if (userId <= 0) userId = Settings.LoginInfo.UserID;
            paramArr[5] = new SqlParameter("@p_user_id", userId);
            paramArr[6] = new SqlParameter("@p_user_type", memberType);
            paramArr[7] = new SqlParameter("@p_location_id", locationId);
            if (agentId > 0) paramArr[8] = new SqlParameter("@P_VS_AGENT_ID", agentId);
            if (dispatchStatus != VisaDispatchStatus.All) paramArr[9] = new SqlParameter("@p_vs_dispatch_status", Utility.ToString((char)dispatchStatus));
            // if (!string.IsNullOrEmpty(dispatchDocType)) paramArr[10] = new SqlParameter("@p_vs_doc_type", dispatchDocType);
            // if (!string.IsNullOrEmpty(passport)) paramArr[11] = new SqlParameter("@P_PAX_PASSPORT_NO", passport);
            // if (!string.IsNullOrEmpty(mobile)) paramArr[12] = new SqlParameter("@P_PAX_MOBILE", mobile);
            if (!string.IsNullOrEmpty(vsDocNo)) paramArr[10] = new SqlParameter("@P_VS_DOC_NO", vsDocNo);
            // if (!string.IsNullOrEmpty(DBOdocNo)) paramArr[14] = new SqlParameter("@P_PAX_DBO_DOC_NO", DBOdocNo);
            // if (!string.IsNullOrEmpty(DAPdocNo)) paramArr[15] = new SqlParameter("@P_PAX_DAP_DOC_NO", DAPdocNo);
            // if (!string.IsNullOrEmpty(visitorName)) paramArr[16] = new SqlParameter("@P_PAX_VISITOR_NAME", visitorName);
            //if (!string.IsNullOrEmpty(trackingNo)) paramArr[17] = new SqlParameter("@P_PAX_DAP_TRACKING_NO", trackingNo);
            if (vsUserId > 0) paramArr[11] = new SqlParameter("@p_vs_user_id", vsUserId);
            if (vsLocationId > 0) paramArr[12] = new SqlParameter("@p_vs_location_id", vsLocationId);
            if (accountedStatus != VisaAccountedStatus.All) paramArr[13] = new SqlParameter("@P_VS_ACCOUNTED_STATUS", Utility.ToString((char)accountedStatus));
            if (settlementMode != "-1") paramArr[14] = new SqlParameter("@P_SETTLEMENT_MODE", settlementMode);
            if (download != "-1") paramArr[15] = new SqlParameter("@P_VS_DOWNLOAD_STATUS", download);
            if (halaDeposite != "-1") paramArr[16] = new SqlParameter("@P_VS_APT_DEPOSIT_STATUS", halaDeposite);
            if (urgentStatus != "-1") paramArr[17] = new SqlParameter("@P_VS_URGENT_STATUS", urgentStatus);
            paramArr[18] = new SqlParameter("@p_list_status", status);
            paramArr[19] = new SqlParameter("@p_record_status", recordStatus);
            return DBGateway.ExecuteQuery("visa_p_visa_sales_getSalesList", paramArr);
        }
        catch { throw; }
    }
    # endregion

    # region Bulk Upload
    public static void SaveBulkUploadDetails(long uploadId, string uploadDocNo, long visaId, string visaDocNo, long paxId, string paxName, string passportNo, string visaNo, string fileName, string fileType, string status, long createdBy)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[14];
            paramArr[0] = new SqlParameter("@P_blk_upload_ID", uploadId);
            paramArr[1] = new SqlParameter("@P_blk_upload_doc_no", uploadDocNo);
            paramArr[2] = new SqlParameter("@P_blk_vs_id", visaId);
            paramArr[3] = new SqlParameter("@P_blk_vs_doc_no", visaDocNo);
            paramArr[4] = new SqlParameter("@P_blk_pax_id", paxId);
            paramArr[5] = new SqlParameter("@P_blk_pax_name", paxName);
            paramArr[6] = new SqlParameter("@P_blk_passport_no", passportNo);
            paramArr[7] = new SqlParameter("@P_blk_visa_no", visaNo);
            paramArr[8] = new SqlParameter("@P_blk_file_name", fileName);
            paramArr[9] = new SqlParameter("@P_blk_file_type", fileType);
            paramArr[10] = new SqlParameter("@P_blk_status", status);
            paramArr[11] = new SqlParameter("@P_blk_created_by", createdBy);
            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[12] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[13] = paramMsgText;

            DBGateway.ExecuteNonQuery("visa_t_visa_bulk_upload_details_add_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }

    public static DataTable GetPaxDetailsByPassportNo(string passportNo, int agentId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[2];

            paramArr[0] = new SqlParameter("@PAX_VISITOR_PASSPORT_NO", passportNo);
            if (agentId > 0) paramArr[1] = new SqlParameter("@P_VS_AGENT_ID", agentId);
            return DBGateway.ExecuteQuery("visa_p_visa_pax_getDetails", paramArr).Tables[0];
        }
        catch { throw; }
    }
    public static DataSet GetBulkUploadList(string docNo, string status, int createdBy)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[4];

            paramArr[0] = new SqlParameter("@P_blk_upload_doc_no", docNo);
            paramArr[1] = new SqlParameter("@P_blk_status", status);
            if (createdBy > 0) paramArr[2] = new SqlParameter("@P_blk_created_by", createdBy);
            return DBGateway.ExecuteQuery("visa_p_visa_bulk_upload_getList", paramArr);
        }
        catch { throw; }
    }


    public static DataTable GetBulkUploadDocList(DateTime fromDate, DateTime toDate)//to bind the dropdown list control
    {
        try
        {

            SqlParameter[] paramArr = new SqlParameter[4];
            paramArr[0] = new SqlParameter("@P_FROM_DATE", fromDate);
            paramArr[1] = new SqlParameter("@P_TO_DATE", toDate);
            return DBGateway.ExecuteQuery("visa_p_visa_bulk_uploadDoc_list", paramArr).Tables[0];

        }
        catch { throw; }
    }

    public static DataTable GetBulkUploadMatchedAndUnmatchedList(string uploadDocNo, string status)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[2];

            paramArr[0] = new SqlParameter("@P_blk_upload_doc_no", uploadDocNo);
            paramArr[1] = new SqlParameter("@P_blk_status", status);
            //if (createdBy > 0) paramArr[2] = new SqlParameter("@P_blk_created_by", createdBy);
            return DBGateway.ExecuteQuery("visa_p_visa_bulk_upload_details_list", paramArr).Tables[0];
        }
        catch { throw; }
    }
    public static string GetDocumentType(string docType, long locationId)
    {
        try
        {
            //SqlParameter[] paramArr = new SqlParameter[2];

            //paramArr[0] = new SqlParameter("@PAX_VISITOR_PASSPORT_NO", passportNo);
            //if (agentId > 0) paramArr[1] = new SqlParameter("@P_VS_AGENT_ID", agentId);
            object obj = DBGateway.ExecuteScalar("select  dbo.getDocumentType('" + docType + "'," + locationId + ") DOC_TYPE");

            return Utility.ToString(obj);

        }
        catch { throw; }
    }
    # endregion
}
}
