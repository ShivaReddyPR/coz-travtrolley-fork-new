using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{


    public class CachedMaster
    {
        private static DataTable dtCommonCodeList;

        static CachedMaster()
        {
            //try
            {
                dtCommonCodeList = GetCommonList();
            }
           // catch { throw; }
        }
       
        #region Static Methods
        public static DataTable GetCommonList()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_FIELD_TYPE", DBNull.Value);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_field_type_getlist", paramList);
                return dsResult.Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetCommonList(string fieldType)
        {

            try
            {
                if (dtCommonCodeList == null) dtCommonCodeList = GetCommonList();
                DataView filteredData = dtCommonCodeList.DefaultView;
                filteredData.RowFilter = string.Format("FIELD_TYPE='{0}'", fieldType);
                return filteredData.ToTable();
            }
            catch
            {
                throw;
            }
        }
       
        
        # endregion
       
    }
   
}
