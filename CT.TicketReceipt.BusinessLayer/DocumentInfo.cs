using System;
namespace CT.TicketReceipt.BusinessLayer
{
    public class DocumentInfo
    {
        private string number;
        private string name;
        private string issuedAt;
        private DateTime issuedOn;
        private DateTime expiresOn;
        private byte[] image;
        private string fileName;
        private string notes;

        public void Set(string number, string issuedAt, DateTime issuedOn, DateTime expiresOn)
        {
            this.number = number;
            this.issuedAt = issuedAt;
            this.issuedOn = issuedOn;
            this.expiresOn = expiresOn;
        }

        public void Set(string number, string issuedAt, DateTime issuedOn, DateTime expiresOn ,string notes)
        {
            this.number = number;
            this.issuedAt = issuedAt;
            this.issuedOn = issuedOn;
            this.expiresOn = expiresOn;
            this.notes = notes;
        }
        public string Number
        {
            get { return number; }
            set { number = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string IssuedAt
        {
            get { return issuedAt; }
            set { issuedAt = value; }
        }
        public DateTime IssuedOn
        {
            get { return issuedOn; }
            set { issuedOn = value; }
        }
        public DateTime ExpiresOn
        {
            get { return expiresOn; }
            set { expiresOn = value; }
        }
        public byte[] Image
        {
            get
            {
                return this.image;
            }
            set
            {
                this.image = value;
            }
        }
        public string FileName
        {
            get
            {
                return this.fileName;
            }
            set
            {
                this.fileName = value;
            }
        }
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }
        public string Extension
        {
            get
            {
                string extension = System.IO.Path.GetExtension(fileName);
                if (!string.IsNullOrEmpty(extension)) extension = extension.ToUpper();
                return extension;
            }
        }
        public string ContentType
        {
            get
            {
                string contentType = string.Empty;
                switch (Extension.ToUpper())
                {
                    case ".PDF":
                        contentType= "application/pdf";
                        break;
                    case ".XLS":
                        contentType = "application/vnd.ms-excel";
                        break;
                    case ".DOC":
                        contentType = "application/msword";
                        break;
                    case ".PPT":
                        contentType = "application/vnd.ms-powerpoint";
                        break;
                    case ".JPG":
                    case ".JPEG":
                        contentType = "image/jpeg";
                        break;
                    case ".ZIP":
                        contentType = "application/zip";
                        break;
                    default:
                        contentType = "text/HTML";
                        break;
                }
                return contentType;
            }
        }
        public static DocumentInfo GetDocumentInfo(string documentName,string documentType, long key1,long key2,long key3)
        {
            try
            {
                DocumentInfo info = new DocumentInfo();
                //DataSet dsget = DbGateWay.DBGateway.GetDBStructure("Application/eDrive/Common/DocumentContent");
                //DataRow drget = dsget.Tables[0].NewRow();
                //drget["DOCUMENT_NAME"] = documentName.Trim();
                //drget["DOCUMENT_TYPE"] = documentType.Trim();
                //drget["DOCUMENT_KEY_1"] = key1;
                //drget["DOCUMENT_KEY_2"] = key2;
                //drget["DOCUMENT_KEY_3"] = key3;
                //drget["USD_SESSION_ID"] = CurrentUser.SessionId;
                //dsget.Tables[0].Rows.Add(drget);
                //DataTable dtResult = DbGateWay.DBGateway.ExecuteCommand(dsget).Tables[0];
                //if (dtResult != null && dtResult.Rows.Count > 0)
                //{
                //    DataRow dr= dtResult.Rows[0];
                //    info.Number = Utility.ToString(dr["document_number"]);
                //    info.Name = Utility.ToString(dr["document_name"]);
                //    info.IssuedOn = Utility.ToDate(dr["document_valid_from"]);
                //    info.ExpiresOn = Utility.ToDate(dr["document_valid_to"]);
                //    if (dtResult.Rows[0]["document_image"] != DBNull.Value) info.image = (byte[])dr["document_image"];
                //    info.fileName = Utility.ToString(dr["document_file_name"]);
                //}ziya
                return info;
            }
            catch { throw; }
        }
    }
}
