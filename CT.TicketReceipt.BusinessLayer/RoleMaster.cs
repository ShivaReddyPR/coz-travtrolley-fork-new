using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class RoleMaster
    {
        private const long NEW_RECORD = -1;
        #region Member Variables        
        private long _id;        
        private string _roleName;                
        
        private string _status;
        private long _createdBy;
        DataTable roleDetails;
              
        #endregion

        #region Properties
        public long ID
        {
            get { return _id; }            
        }
       
        public string RoleName
        {
            get { return _roleName ; }
            set { _roleName = value; }
        }       
        
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
                

        public DataTable RoleDetails
        {
            get { return roleDetails; }
            set { roleDetails  = value;}            
        }

        #endregion

        #region Constructors
        public RoleMaster()
        {
            _id = -1;

            GetDetails(_id);
               // DataSet ds = GetData(_id);
            //if (dt != null && dt.Rows.Count > 0)
            //UpdateBusinessData(ds);
        }
        public RoleMaster(long id)
        {
            
            
            _id = id;
            GetDetails(id);
        }
        #endregion
        #region Methods
        private void GetDetails(long id)
        {
            
            DataSet ds = GetData(id);
            UpdateBusinessData(ds);           
            
        }
        private DataSet GetData(long id)
        {
            
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ROLE_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_role_master_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {
            
            try
            {
                _id = Utility.ToLong(dr["ROLE_ID"]);
                _roleName  = Utility.ToString(dr["ROLE_NAME"]);
                _status = Utility.ToString(dr["ROLE_STATUS"]);
                _createdBy = Utility.ToLong(dr["ROLE_CREATED_BY"]);                               

            }
            catch
            {
                throw;
            }
        }

   
        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null) 
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        //DataRow  dr = ds.Tables[0].Rows[0];
                        //if(Utility.ToLong(dr["Location_ID"])>0) 
                        UpdateBusinessData(ds.Tables[0].Rows[0]);                        
                    }

                    roleDetails  = ds.Tables[1];
                   
                }

            }
            catch 
            { throw; }

        
        }
        #endregion

        #region Public Methods
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;

            try
            {
                 cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;                
                

                SqlParameter[] paramList = new SqlParameter[10];

                paramList[0] = new SqlParameter("@P_ROLE_ID", _id);
                paramList[1] = new SqlParameter("@P_ROLE_NAME", _roleName );
                paramList[2] = new SqlParameter("@P_ROLE_STATUS", _status);
                paramList[3] = new SqlParameter("@P_ROLE_CREATED_BY", _createdBy);
                paramList[4] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[4].Direction = ParameterDirection.Output;
                paramList[5] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[5].Direction = ParameterDirection.Output;
                paramList[6] = new SqlParameter("@P_ROLE_ID_RET", SqlDbType.BigInt);
                paramList[6].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_role_master_add_update", paramList);
                string messageType = Utility.ToString(paramList[4].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[5].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                _id  = Utility.ToLong(paramList[6].Value);

                if (roleDetails != null)
                {
                    DataTable dt = roleDetails.GetChanges();
                    int recordStatus = 0;
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            switch (dr.RowState)
                            {
                                case DataRowState.Added: recordStatus = 1;
                                    break;
                                case DataRowState.Modified: recordStatus = 2;
                                    break;
                                case DataRowState.Deleted: recordStatus = -1;
                                    break;
                                default: break;
                            }
                            SaveRoleDetails(cmd, recordStatus, Utility.ToLong(dr["rd_id"]), _id,
                                        Utility.ToLong(dr["rd_func_id"]), Utility.ToString(dr["rd_status"]), _createdBy);
                        }
                    }
                }
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                DBGateway.CloseConnection(cmd);
            }
        }

        public void SaveRoleDetails(SqlCommand cmd, int recordStatus,long rdId,long roleId,long  functionId,
                    string rdStatus, long createdBy)
        {
           
            try
            {

                SqlParameter[] paramArr = new SqlParameter[8];
                paramArr[0] = new SqlParameter("@P_RECORD_STATUS", recordStatus );
                paramArr[1] = new SqlParameter("@P_RD_ID", rdId);

                paramArr[2] = new SqlParameter("@P_RD_ROLE_ID", roleId );
                paramArr[3] = new SqlParameter("@P_RD_FUNC_ID", functionId);

                paramArr[4] = new SqlParameter("@P_RD_STATUS", rdStatus);
                paramArr[5] = new SqlParameter("@P_RD_CREATED_BY", createdBy);
               
                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[6] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 200;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[7] = paramMsgText;

             
                DBGateway.ExecuteNonQueryDetails(cmd, "CT_P_ROLE_DETAILS_ADD_UPDATE", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));

               

            }
            catch
            {
                throw;
            }

        }
        public void Delete()
        {            
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LOCATION_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output; 
                
                DBGateway.ExecuteNonQuery("ct_p_location_add_update", paramList);                
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Static Methods
        public static DataTable GetList(ListStatus status, RecordStatus recordStatus)
        {
            
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];

                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus!=RecordStatus.All) paramList[1] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                return DBGateway.ExecuteQuery("ct_p_role_master_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
