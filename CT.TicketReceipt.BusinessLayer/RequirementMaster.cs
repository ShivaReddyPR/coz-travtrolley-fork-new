using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{


    public class RequirementMaster
    {
      private const long NEW_RECORD = -1;
        
      //  private long _id;

        #region Member Variables
        private long _transactionid;       
        private string _code;
        private string _name;
        private string _header;
        private string _description;
        private long _countryId;
        private long _nationalityId;
        private long _visaTypeId;
        private long _residenceId;              
        private string _status;                
        private long _createdBy;        

        #endregion


        #region Properties

        public long TransactionId
        {
            get { return _transactionid ; }
        }

       public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Header
        {
            get { return _header ; }
            set { _header = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public long CountryID
        {
            get { return _countryId; }
            set { _countryId = value; }
        }
        public long NationalityId
        {
            get { return _nationalityId ; }
            set { _nationalityId  = value; }
        }
        public long VisaTypeId
        {
            get { return _visaTypeId; }
            set { _visaTypeId = value; }
        }

        public long ResidenceId
        {
            get { return _residenceId; }
            set { _residenceId = value; }
        }       
       
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        #endregion


        #region Constructors
        public RequirementMaster()
        {
            _transactionid  = NEW_RECORD;
        }
        public RequirementMaster(long id)
        {
            _transactionid  = id;
            getDetails(_transactionid );
        
        }


        #endregion

#region Methods

        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_REQ_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("OBVISA_p_requirement_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _transactionid = Utility.ToLong(dr["req_id"]);
                _code  = Utility.ToString(dr["req_code"]);
                _header  = Utility.ToString(dr["req_header"]);
                _description = Utility.ToString(dr["req_description"]);
                _countryId = Utility.ToLong(dr["req_country_id"]);
                _nationalityId = Utility.ToLong(dr["req_nationality_id"]);

                _visaTypeId = Utility.ToLong(dr["req_visa_type_id"]);
                _residenceId = Utility.ToLong(dr["req_residence_id"]);
                _status = Utility.ToString(dr["req_status"]);
                _createdBy = Utility.ToLong(dr["req_created_by"]);

            }
            catch
            {
                throw;
            }
        }
       
#endregion

#region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[24];

                paramList[0] = new SqlParameter("@P_REQ_ID", _transactionid);
                paramList[1] = new SqlParameter("@P_REQ_CODE", _code);
                paramList[2] = new SqlParameter("@P_REQ_HEADER", _header);
                paramList[3] = new SqlParameter("@P_REQ_DESCRIPTION", _description);
                paramList[4] = new SqlParameter("@P_REQ_COUNTRY_ID", _countryId);
                if (_nationalityId != 0) paramList[5] = new SqlParameter("@P_REQ_NATIONALITY_ID", _nationalityId);
                if (_visaTypeId != 0) paramList[6] = new SqlParameter("@P_REQ_VISA_TYPE_ID", _visaTypeId);
                if (_residenceId != 0) paramList[7] = new SqlParameter("@P_REQ_RESIDENCE_ID", _residenceId);

                paramList[8] = new SqlParameter("@P_REQ_STATUS", _status);
                paramList[9] = new SqlParameter("@P_REQ_CREATED_BY", _createdBy);
                
                
                paramList[10] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[10].Direction = ParameterDirection.Output;
                paramList[11] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[11].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("OBVISA_p_requirement_add_update", paramList);
                string messageType = Utility.ToString(paramList[10].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[11].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_REQ_id", _transactionid );
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("OBVISA_p_requirement_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
#endregion

       
        #region Static Methods

        public static DataTable GetList(ListStatus status, RecordStatus recordStatus)
        {

            try
            {
             
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus!= RecordStatus.All) paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("[OBVISA_p_requirement_getlist]", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

       
        # endregion
       
    }
   
}
