using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class VisaAdvquery
    {
       // private const long NEW_RECORD = -1;
        
       
        #region Static Methods
        public static DataSet GetVisaAdvQueryList(string docNo, DateTime docDate, int visaTypeId, string grntName, string grntPSPNo, DateTime grntPSPIssueDate, DateTime grntPSPExpDate,string grntvisaNo, DateTime grntVisaExpDate,
            long nationalityId,string grntMob,string grntOfficeNo,string grntEmail,string receiptNo,long locationId,long createdBy,string visitorName,string visitorSurName,string visitorFatherName,string visitorMotherName,
            string visitorSex, DateTime visitorDOB, string visitorPOB, string visitorOccupation, string visitorReligion, string visitorMaritalStatus, string visitorPSPNo, string visitorPSPIssuePlace, DateTime visitorPSPIssueDate, DateTime visitorPSPExpDate, string visitorRelation,
            string visitorAccompaniedBy, long visitorNationalityId, string visitorMobile, string visitorEmail, string dispatchNo, string trackingNo, VisaDispatchStatus dispatchStatus, string visaEntryNo, string visaNo,DateTime visaIssueDate,
            DateTime visaExpDate, string airlineCode, string flightNo, DateTime arrivalDate, DateTime departureDate, string editStatus, long paxModifiedBy, DateTime paxModifiedOn,ListStatus listStatus,RecordStatus recordStatus,string paxUID,int agentId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[55];

                if (!string.IsNullOrEmpty(docNo))paramList[0] = new SqlParameter("@p_vs_doc_no", docNo);
                if (docDate!=DateTime.MinValue) paramList[1] = new SqlParameter("@p_vs_doc_date", docDate);
                if (visaTypeId>0) paramList[2] = new SqlParameter("@p_vs_visa_type_id", visaTypeId);
                if (!string.IsNullOrEmpty(grntName)) paramList[3] = new SqlParameter("@p_vs_grnt_name", grntName);
                if (!string.IsNullOrEmpty(grntPSPNo)) paramList[4] = new SqlParameter("@p_vs_grnt_passport_no", grntPSPNo);
                if (grntPSPIssueDate!=DateTime.MinValue) paramList[5] = new SqlParameter("@p_vs_grnt_passport_issue_date", grntPSPIssueDate);
                if (grntPSPExpDate != DateTime.MinValue) paramList[6] = new SqlParameter("@p_vs_grnt_passport_expiry_date", grntPSPExpDate);
                if (!string.IsNullOrEmpty(grntvisaNo)) paramList[7] = new SqlParameter("@p_vs_grnt_visa_number", grntvisaNo);
                if (grntVisaExpDate != DateTime.MinValue) paramList[8] = new SqlParameter("@p_vs_grnt_visa_expiry_date", grntVisaExpDate);
                if (nationalityId>0) paramList[9] = new SqlParameter("@p_vs_grnt_nationality_id", nationalityId);
                if (!string.IsNullOrEmpty(grntMob)) paramList[10] = new SqlParameter("@p_vs_grnt_mobile_no", grntMob);
                if (!string.IsNullOrEmpty(grntOfficeNo)) paramList[11] = new SqlParameter("@p_vs_grnt_office_no", grntOfficeNo);
                if (!string.IsNullOrEmpty(grntEmail)) paramList[12] = new SqlParameter("@p_vs_grnt_email", grntEmail);
                if (!string.IsNullOrEmpty(receiptNo)) paramList[13] = new SqlParameter("@p_vs_receipt_no", receiptNo);
                //if (!string.IsNullOrEmpty(vsStatus)) paramList[1] = new SqlParameter("@p_vs_status", vsStatus);
                if (locationId > 0) paramList[14] = new SqlParameter("@p_vs_location_id", locationId);
                if (createdBy>0) paramList[15] = new SqlParameter("@p_vs_created_by", createdBy);
                if (!string.IsNullOrEmpty(visitorName)) paramList[16] = new SqlParameter("@p_pax_visitor_name", visitorName);
                if (!string.IsNullOrEmpty(visitorSurName)) paramList[17] = new SqlParameter("@p_pax_visitor_surname", visitorSurName);
                if (!string.IsNullOrEmpty(visitorFatherName)) paramList[18] = new SqlParameter("@p_pax_visitor_fathername", visitorFatherName);
                if (!string.IsNullOrEmpty(visitorMotherName)) paramList[19] = new SqlParameter("@p_pax_visitor_mothername", visitorMotherName);
                if (!string.IsNullOrEmpty(visitorSex)) paramList[20] = new SqlParameter("@p_pax_visitor_sex", visitorSex);
                if (visitorDOB!=DateTime.MinValue) paramList[21] = new SqlParameter("@p_pax_visitor_dob", visitorDOB);
                if (!string.IsNullOrEmpty(visitorPOB)) paramList[22] = new SqlParameter("@p_pax_visitor_pob", visitorPOB);
                if (!string.IsNullOrEmpty(visitorOccupation)) paramList[23] = new SqlParameter("@p_pax_visitor_occupation", visitorOccupation);
                if (!string.IsNullOrEmpty(visitorReligion)) paramList[24] = new SqlParameter("@p_pax_visitor_relegion", visitorReligion);
                if (!string.IsNullOrEmpty(visitorMaritalStatus)) paramList[25] = new SqlParameter("@p_pax_visitor_maritial_status", visitorMaritalStatus);
                if (!string.IsNullOrEmpty(visitorPSPNo)) paramList[26] = new SqlParameter("@p_pax_visitor_passport_no", visitorPSPNo);
                if (!string.IsNullOrEmpty(visitorPSPIssuePlace)) paramList[27] = new SqlParameter("@p_pax_visitor_passport_issue_place", visitorPSPIssuePlace);
                if (visitorPSPIssueDate!=DateTime.MinValue) paramList[28] = new SqlParameter("@p_pax_visitor_passport_issue_date", visitorPSPIssueDate);
                if (visitorPSPExpDate != DateTime.MinValue) paramList[29] = new SqlParameter("@p_pax_visitor_passport_expiry_date", visitorPSPExpDate);
                if (!string.IsNullOrEmpty(visitorRelation)) paramList[30] = new SqlParameter("@p_pax_visitor_relation", visitorRelation);
                if (!string.IsNullOrEmpty(visitorAccompaniedBy)) paramList[31] = new SqlParameter("@p_pax_visitor_accompanied_by", visitorAccompaniedBy);
                if (visitorNationalityId>0) paramList[32] = new SqlParameter("@p_pax_visitor_nationality_id", visitorNationalityId);
                if (!string.IsNullOrEmpty(visitorMobile)) paramList[33] = new SqlParameter("@p_pax_visitor_mobile", visitorMobile);
                if (!string.IsNullOrEmpty(visitorEmail)) paramList[34] = new SqlParameter("@p_pax_visitor_email", visitorEmail);

                if (!string.IsNullOrEmpty(dispatchNo)) paramList[35] = new SqlParameter("@p_pax_dbo_doc_no", dispatchNo);
                if (!string.IsNullOrEmpty(trackingNo)) paramList[36] = new SqlParameter("@p_pax_dap_tracking_no", trackingNo);
                if (dispatchStatus!=VisaDispatchStatus.All) paramList[37] = new SqlParameter("@p_pax_dispatch_status", Utility.ToString((char)dispatchStatus));
                if (!string.IsNullOrEmpty(visaEntryNo)) paramList[38] = new SqlParameter("@p_pax_visa_entry_no", visaEntryNo);
                if (!string.IsNullOrEmpty(visaNo)) paramList[39] = new SqlParameter("@p_pax_visa_no", visaNo);
                if (visaIssueDate!=DateTime.MinValue) paramList[40] = new SqlParameter("@p_pax_visa_issue_date", visaIssueDate);
                if (visaExpDate!=DateTime.MinValue) paramList[41] = new SqlParameter("@p_pax_visa_exp_date", visaExpDate);
                if (!string.IsNullOrEmpty(airlineCode)) paramList[42] = new SqlParameter("@p_pax_airline_code", airlineCode);
                if (!string.IsNullOrEmpty(flightNo)) paramList[43] = new SqlParameter("@p_pax_flight_no", flightNo);
                if (arrivalDate!=DateTime.MinValue) paramList[44] = new SqlParameter("@p_pax_arrival_date", arrivalDate);
                if (departureDate!=DateTime.MinValue) paramList[45] = new SqlParameter("@p_pax_departure_date", departureDate);
                if (!string.IsNullOrEmpty(editStatus)) paramList[46] = new SqlParameter("@p_pax_edit_status", editStatus);
                if (paxModifiedBy>0) paramList[47] = new SqlParameter("@p_pax_modified_by", paxModifiedBy);
                if (paxModifiedOn!= DateTime.MinValue) paramList[48] = new SqlParameter("@p_pax_modified_on", paxModifiedOn);
                if (!string.IsNullOrEmpty(paxUID)) paramList[49] = new SqlParameter("@p_pax_UID", paxUID);
                paramList[50] = new SqlParameter("@P_USER_TYPE", Settings.LoginInfo.MemberType.ToString());
               if(Settings.LoginInfo.LocationID >0) paramList[51] = new SqlParameter("@P_LOCATION_ID", Settings.LoginInfo.LocationID);
               if (agentId>0)  paramList[52] = new SqlParameter("@P_VS_AGENT_ID", agentId );
                paramList[53] = new SqlParameter("@P_LIST_STATUS", listStatus);
                paramList[54] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("visa_p_visa_sales_getAdv_Query", paramList);
            }

            catch
            {
                throw;
            }
        }
        public static void UpdateVisaAdvQuery(string docNo,long visaId,long paxId, DateTime docDate, int visaTypeId, string grntName, string grntPSPNo, DateTime grntPSPIssueDate, DateTime grntPSPExpDate, string grntvisaNo, DateTime grntVisaExpDate,
           long nationalityId, string grntMob, string grntOfficeNo, string grntEmail, string receiptNo, long locationId, long createdBy, string visitorName, string visitorSurName, string visitorFatherName, string visitorMotherName,
           string visitorSex, DateTime visitorDOB, string visitorPOB, string visitorOccupation, string visitorReligion, string visitorMaritalStatus, string visitorPSPNo, string visitorPSPIssuePlace, DateTime visitorPSPIssueDate, DateTime visitorPSPExpDate, string visitorRelation,
           string visitorAccompaniedBy, long visitorNationalityId, string visitorMobile, string visitorEmail, string dispatchNo, string trackingNo, VisaDispatchStatus dispatchStatus,string dispatchRemarks, string visaEntryNo, string visaNo, DateTime visaIssueDate,
           DateTime visaExpDate, string airlineCode, string flightNo, DateTime arrivalDate, DateTime departureDate, string editStatus, long paxModifiedBy, DateTime paxModifiedOn, string paxUID,string uploadPath, string uploadType)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[55];

                if (!string.IsNullOrEmpty(docNo)) paramList[0] = new SqlParameter("@p_vs_doc_no", docNo);
                if (docDate != DateTime.MinValue) paramList[1] = new SqlParameter("@p_vs_doc_date", docDate);
                if (visaTypeId > 0) paramList[2] = new SqlParameter("@p_vs_visa_type_id", visaTypeId);
                if (!string.IsNullOrEmpty(grntName)) paramList[3] = new SqlParameter("@p_vs_grnt_name", grntName);
                if (!string.IsNullOrEmpty(grntPSPNo)) paramList[4] = new SqlParameter("@p_vs_grnt_passport_no", grntPSPNo);
                if (grntPSPIssueDate != DateTime.MinValue) paramList[5] = new SqlParameter("@p_vs_grnt_passport_issue_date", grntPSPIssueDate);
                if (grntPSPExpDate != DateTime.MinValue) paramList[6] = new SqlParameter("@p_vs_grnt_passport_expiry_date", grntPSPExpDate);
                if (!string.IsNullOrEmpty(grntvisaNo)) paramList[7] = new SqlParameter("@p_vs_grnt_visa_number", grntvisaNo);
                if (grntVisaExpDate != DateTime.MinValue) paramList[8] = new SqlParameter("@p_vs_grnt_visa_expiry_date", grntVisaExpDate);
                if (nationalityId > 0) paramList[9] = new SqlParameter("@p_vs_grnt_nationality_id", nationalityId);
                if (!string.IsNullOrEmpty(grntMob)) paramList[10] = new SqlParameter("@p_vs_grnt_mobile_no", grntMob);
                if (!string.IsNullOrEmpty(grntOfficeNo)) paramList[11] = new SqlParameter("@p_vs_grnt_office_no", grntOfficeNo);
                if (!string.IsNullOrEmpty(grntEmail)) paramList[12] = new SqlParameter("@p_vs_grnt_email", grntEmail);
                if (!string.IsNullOrEmpty(receiptNo)) paramList[13] = new SqlParameter("@p_vs_receipt_no", receiptNo);
                //if (!string.IsNullOrEmpty(vsStatus)) paramList[1] = new SqlParameter("@p_vs_status", vsStatus);
                if (locationId > 0) paramList[14] = new SqlParameter("@p_vs_location_id", locationId);
                if (createdBy > 0) paramList[15] = new SqlParameter("@p_vs_created_by", createdBy);
                if (!string.IsNullOrEmpty(visitorName)) paramList[16] = new SqlParameter("@p_pax_visitor_name", visitorName);
                if (!string.IsNullOrEmpty(visitorSurName)) paramList[17] = new SqlParameter("@p_pax_visitor_surname", visitorSurName);
                if (!string.IsNullOrEmpty(visitorFatherName)) paramList[18] = new SqlParameter("@p_pax_visitor_fathername", visitorFatherName);
                if (!string.IsNullOrEmpty(visitorMotherName)) paramList[19] = new SqlParameter("@p_pax_visitor_mothername", visitorMotherName);
                if (!string.IsNullOrEmpty(visitorSex)) paramList[20] = new SqlParameter("@p_pax_visitor_sex", visitorSex);
                if (visitorDOB != DateTime.MinValue) paramList[21] = new SqlParameter("@p_pax_visitor_dob", visitorDOB);
                if (!string.IsNullOrEmpty(visitorPOB)) paramList[22] = new SqlParameter("@p_pax_visitor_pob", visitorPOB);
                if (!string.IsNullOrEmpty(visitorOccupation)) paramList[23] = new SqlParameter("@p_pax_visitor_occupation", visitorOccupation);
                if (!string.IsNullOrEmpty(visitorReligion)) paramList[24] = new SqlParameter("@p_pax_visitor_relegion", visitorReligion);
                if (!string.IsNullOrEmpty(visitorMaritalStatus)) paramList[25] = new SqlParameter("@p_pax_visitor_maritial_status", visitorMaritalStatus);
                if (!string.IsNullOrEmpty(visitorPSPNo)) paramList[26] = new SqlParameter("@p_pax_visitor_passport_no", visitorPSPNo);
                if (!string.IsNullOrEmpty(visitorPSPIssuePlace)) paramList[27] = new SqlParameter("@p_pax_visitor_passport_issue_place", visitorPSPIssuePlace);
                if (visitorPSPIssueDate != DateTime.MinValue) paramList[28] = new SqlParameter("@p_pax_visitor_passport_issue_date", visitorPSPIssueDate);
                if (visitorPSPExpDate != DateTime.MinValue) paramList[29] = new SqlParameter("@p_pax_visitor_passport_expiry_date", visitorPSPExpDate);
                if (!string.IsNullOrEmpty(visitorRelation)) paramList[30] = new SqlParameter("@p_pax_visitor_relation", visitorRelation);
                if (!string.IsNullOrEmpty(visitorAccompaniedBy)) paramList[31] = new SqlParameter("@p_pax_visitor_accompanied_by", visitorAccompaniedBy);
                if (visitorNationalityId > 0) paramList[32] = new SqlParameter("@p_pax_visitor_nationality_id", visitorNationalityId);
                if (!string.IsNullOrEmpty(visitorMobile)) paramList[33] = new SqlParameter("@p_pax_visitor_mobile", visitorMobile);
                if (!string.IsNullOrEmpty(visitorEmail)) paramList[34] = new SqlParameter("@p_pax_visitor_email", visitorEmail);

                if (!string.IsNullOrEmpty(dispatchNo)) paramList[35] = new SqlParameter("@p_pax_dbo_doc_no", dispatchNo);
                if (!string.IsNullOrEmpty(trackingNo)) paramList[36] = new SqlParameter("@p_pax_dap_tracking_no", trackingNo);
                if (dispatchStatus != VisaDispatchStatus.All) paramList[37] = new SqlParameter("@p_pax_dispatch_status", Utility.ToString((char)dispatchStatus));
                if (!string.IsNullOrEmpty(visaEntryNo)) paramList[38] = new SqlParameter("@p_pax_visa_entry_no", visaEntryNo);
                if (!string.IsNullOrEmpty(visaNo)) paramList[39] = new SqlParameter("@p_pax_visa_no", visaNo);
                if (visaIssueDate != DateTime.MinValue) paramList[40] = new SqlParameter("@p_pax_visa_issue_date", visaIssueDate);
                if (visaExpDate != DateTime.MinValue) paramList[41] = new SqlParameter("@p_pax_visa_exp_date", visaExpDate);
                if (!string.IsNullOrEmpty(airlineCode)) paramList[42] = new SqlParameter("@p_pax_airline_code", airlineCode);
                if (!string.IsNullOrEmpty(flightNo)) paramList[43] = new SqlParameter("@p_pax_flight_no", flightNo);
                if (arrivalDate != DateTime.MinValue) paramList[44] = new SqlParameter("@p_pax_arrival_date", arrivalDate);
                if (departureDate != DateTime.MinValue) paramList[45] = new SqlParameter("@p_pax_departure_date", departureDate);
                if (!string.IsNullOrEmpty(editStatus)) paramList[46] = new SqlParameter("@p_pax_edit_status", editStatus);
		         paramList[47] = new SqlParameter("@p_pax_UID", paxUID);
                //if (paxModifiedBy > 0) paramList[47] = new SqlParameter("@p_pax_modified_by", paxModifiedBy);
                //if (paxModifiedOn != DateTime.MinValue) paramList[48] = new SqlParameter("@p_pax_modified_on", paxModifiedOn);
                paramList[48] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[48].Direction = ParameterDirection.Output;
                paramList[49] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[49].Direction = ParameterDirection.Output;
                paramList[50] = new SqlParameter("@p_vs_id", visaId);
                paramList[51] = new SqlParameter("@p_pax_id", paxId);
                if (!string.IsNullOrEmpty(dispatchRemarks)) paramList[52] = new SqlParameter("@p_pax_dispatch_remarks", dispatchRemarks);
                if (!string.IsNullOrEmpty(uploadPath)) paramList[53] = new SqlParameter("@P_pax_visa_upld_path", uploadPath);
                if (!string.IsNullOrEmpty(uploadType)) paramList[54] = new SqlParameter("@P_pax_visa_upld_type", uploadType);

                DBGateway.ExecuteNonQuery("visa_p_visa_sales_Adv_Update", paramList);
                string messageType = Utility.ToString(paramList[48].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[49].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }

            catch
            {
                throw;
            }
        }

        public static void SaveFollowUpHistory(long followUp_id , long visaId, long paxId, string visaDocNo, string remarks, string dispatchStatus, string status, long createdBy)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[10];

                paramList[0] = new SqlParameter("@P_flw_id", followUp_id);
                paramList[1] = new SqlParameter("@P_flw_pax_id", paxId );
                paramList[2] = new SqlParameter("@P_flw_vsh_id", visaId);
                paramList[3] = new SqlParameter("@P_flw_visa_doc_no", visaDocNo );
                paramList[4] = new SqlParameter("@P_flw_remarks", remarks);
                paramList[5] = new SqlParameter("@P_flw_dispatch_status", dispatchStatus);                
                paramList[6] = new SqlParameter("@P_flw_status", status);
                paramList[7] = new SqlParameter("@P_flw_created_by", createdBy);
                paramList[8] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[8].Direction = ParameterDirection.Output;
                paramList[9] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[9].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("visa_p_visa_sales_follow_up_add_update", paramList);
                string messageType = Utility.ToString(paramList[8].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[9].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch 
            {
                throw;
            }
        
        }

        public static DataSet  GetFollowUpHistoryList(long visaId, long paxId, ListStatus listStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_flw_vsh_id", visaId );
                paramList[1] = new SqlParameter("@P_flw_pax_id", paxId);
                paramList[2] = new SqlParameter("@P_LIST_STATUS", listStatus);
                return DBGateway.ExecuteQuery("VISA_P_VISA_SALES_FOLLOW_UP_HISTORY_GETLIST", paramList);
            }

            catch { throw; }
        
        }
        public static DataTable GetFollowUpHistoryData(long followUpId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_flw_id", followUpId);              
                return DBGateway.ExecuteQuery("VISA_P_VISA_SALES_FOLLOW_UP_HISTORY_GETDATA", paramList).Tables[0];
            }

            catch { throw; }

        }

        # endregion
       
        
    }
   
}

