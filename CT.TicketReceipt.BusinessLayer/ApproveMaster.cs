using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class ApproveMaster
    {
        private const long NEW_RECORD = -1;
        
        
       
        #region Static Methods
        public static DataTable GetList(ListStatus status,RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];

                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", recordStatus);

                return DBGateway.ExecuteQuery("ct_p_approval_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        # endregion
        public static ApproveDetails GetApproveDetails(long approveId)
        {

            try
            {
                ApproveDetails appDetails = null;
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_APP_ID", approveId);
                DataTable dt = DBGateway.ExecuteQuery("ct_p_Approve_details", paramList).Tables[0];
                if (dt != null)
                {
                    appDetails = new ApproveDetails(approveId);
                    DataRow dr = dt.Rows[0];
                    appDetails.Email= Utility.ToString(dr["APP_EMAIL"]);
                }
                return appDetails;
            }
            catch
            {
                throw;
            }
        }
       
    }
   
   public class ApproveDetails
    {
        #region Member Variables
        private long _id;
        private string _email;
       

        #endregion
        #region Properties
        public long ID
        {
            get { return _id; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        
       

        #endregion
       public ApproveDetails(long id)
        {
            _id = id;
        }


    }
    
   
}
