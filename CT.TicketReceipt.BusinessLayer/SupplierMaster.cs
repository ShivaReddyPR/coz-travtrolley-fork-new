using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{



    public class SupplierMaster
    {
        private const long NEW_RECORD = -1;
        
    
        #region Member Variables
        private long _id;
        private string _code;
        private string _name;
        //private string _currency;
        private string _supplierType;
        private string _serviceType;
        private string _address;
        private string _poBox;
        private string _phoneNo;
        private string _faxNo;
        private string _eMail;
        private string _website;
        private string _contactPerson;
        private long _country;
        private long _city;
        private string _remarks;
        
        private string _status;
        private long _createdBy;
        //private long _nextID;

        #endregion

        #region Properties

        public long ID
        {
            get { return _id; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string SupplierType
        {
            get { return _supplierType ; }
            set { _supplierType = value; }
        }
        public string ServiceType
        {
            get { return _serviceType ; }
            set { _serviceType = value; }
        }
        
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public string POBox
        {
            get { return _poBox; }
            set { _poBox = value; }
        }
        public string PhoneNo
        {
            get { return _phoneNo; }
            set { _phoneNo = value; }
        }
        public string FaxNo
        {
            get { return _faxNo; }
            set { _faxNo = value; }
        }
        public string Email
        {
            get { return _eMail; }
            set { _eMail = value; }
        }
        public string Website
        {
            get { return _website; }
            set { _website = value; }
        }
        public string ContactPerson
        {
            get { return _contactPerson; }
            set { _contactPerson = value; }
        }
        public long Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public long City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string Remarks
        {
            get { return _remarks; }
            set { _remarks = value; }
        }
       
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        } 

        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        
         #endregion

        #region Constructors
        public SupplierMaster()
        {
            _id = NEW_RECORD;
            getDetails(_id);
        }
        public SupplierMaster(long id)
        {
            _id = id;
            getDetails(_id);
        
        }


        #endregion

        #region Methods
        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_SUPP_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("TOUR_SUPPLIER_GETDATA", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                if (_id <= 0)
                {
                    _code = Utility.ToString(dr["SUPP_CODE"]);
                }
                else
                {
                    _id = Utility.ToLong(dr["supp_id"]);
                    _code = Utility.ToString(dr["supp_code"]);
                    _supplierType = Utility.ToString(dr["supp_type"]);
                    _serviceType = Utility.ToString(dr["supp_service"]);
                    _name = Utility.ToString(dr["supp_name"]);
                    _address = Utility.ToString(dr["supp_address"]);
                    _poBox = Utility.ToString(dr["supp_po_box"]);
                    _phoneNo = Utility.ToString(dr["supp_phone"]);
                    _faxNo= Utility.ToString(dr["supp_fax"]);
                    _eMail= Utility.ToString(dr["supp_email"]);
                    _website= Utility.ToString(dr["supp_website"]);
                    _contactPerson= Utility.ToString(dr["supp_contact_person"]);
                    _country = Utility.ToLong(dr["supp_country"]);
                    _city= Utility.ToLong(dr["supp_city"]);
                    _remarks = Utility.ToString(dr["supp_remarks"]);
                    _status = Utility.ToString(dr["supp_status"]);
                    _createdBy = Utility.ToLong(dr["supp_created_by"]);
                }

            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[19];

                paramList[0] = new SqlParameter("@P_supp_id", _id);
                paramList[1] = new SqlParameter("@P_supp_code", _code);
                paramList[2] = new SqlParameter("@P_supp_type", _supplierType );                
                paramList[3] = new SqlParameter("@P_supp_service", _serviceType);
                paramList[4] = new SqlParameter("@P_supp_name", _name);

                paramList[5] = new SqlParameter("@P_supp_address", _address);
                paramList[6] = new SqlParameter("@P_supp_po_box", _poBox);
                paramList[7] = new SqlParameter("@P_supp_phone", _phoneNo);
                paramList[8] = new SqlParameter("@P_supp_fax", _faxNo);

                paramList[9] = new SqlParameter("@P_supp_email", _eMail);
                paramList[10] = new SqlParameter("@P_supp_website", _website);
                paramList[11] = new SqlParameter("@P_supp_contact_person", _contactPerson);
                paramList[12] = new SqlParameter("@P_supp_country", _country );

                paramList[13] = new SqlParameter("@P_supp_city", _city);
                paramList[14] = new SqlParameter("@P_supp_remarks", _remarks);
                paramList[15] = new SqlParameter("@P_supp_status", _status);

                paramList[16] = new SqlParameter("@P_SUPP_CREATED_BY", _createdBy);
                paramList[17] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[17].Direction = ParameterDirection.Output;
                paramList[18] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[18].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("TOUR_SUPPLIER_ADD_UPDATE", paramList);
                string messageType = Utility.ToString(paramList[17].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[18].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        //public void Delete()
        //{
        //    try
        //    {
        //        SqlParameter[] paramList = new SqlParameter[2];
        //        paramList[0] = new SqlParameter("@P_COUNTRY_ID", _id);
        //        paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
        //        paramList[1].Direction = ParameterDirection.Output;

        //        DBGateway.ExecuteNonQuery("ct_p_COUNTRY_add_update", paramList);
        //        string message = Utility.ToString(paramList[1].Value);
        //        if (message != string.Empty) throw new Exception(message);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
        #endregion

        #region Static Methods
        
        public static DataTable GetList(ListStatus status,RecordStatus recordStatus,long country)
        {

            try
            {
             
                SqlParameter[] paramList = new SqlParameter[3];
                
                paramList[0] = new SqlParameter("@P_LIST_STATUS",Utility.ToInteger((int)status));
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                if (country > 0) paramList[2] = new SqlParameter("@P_SUPP_COUNTRY",country );

                return DBGateway.ExecuteQuery("TOUR_SUPPLIER_MASTER_GETLIST", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        # endregion
       
    }
   
}
