using System;
using System.Data;
//using System.Data.OracleClient;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;

/// <summary>
/// Summary description for HandlingFeeMaster
/// </summary>
 namespace CT.TicketReceipt.BusinessLayer
{
     //public enum VisaDispatchStatus
     //{
     //    All = 0,//All
     //    Pending=78,// Value N
     //    Popsted=80,//P Operations
     //    Rejected=82, //R
     //    Suprevisor= 83, //S Suprevisor
     //    Approved=65,//A
     //    Handover = 72,//H hand over to Customer
     //    //Operations=79,//O=Dispatch to Airport
     //    BackOffice= 66,//B=Back Office

     //}
     //public enum PaymentMode
     //{
     //    Cash = 1,
     //    Credit = 2,
     //    Card = 3,
     //    Cash_Credit = 4,
     //    Cash_Card = 5,
     //    Cash_Credit_Card = 6,
     //    Employee = 7,
     //    Others = 8,
     //}
     
     public enum MappingElement
     {
         CASH_BOOK = 1,
         BANK_BOOK=2,
         VISA_FEE=3,
         INS_FEE = 4,
         SEC_DEPOSIT = 5,
         INCOME = 6,
         APT_DEPOSIT = 7,
         CARD_FEE = 8,
         TICKET_FEE = 9

     }
     public class VSORAInterface
     {
         # region members
         long _transactionId;
         string _companyId;
         string _baseDocType;
          string _docType;
         string _docNumber;
         DateTime _docDate;
         string _cashBankCode;
         string _currencyCode;
         decimal _exchangeRate;
         
         PaymentModeInfo _cashMode = new PaymentModeInfo();
         string _otherReference;
         string _chequeNo;
         string _narration;
         string _GLAccounts;
         string _notes;
         string _flex1;
         string _map_location_code;
         string _map_group_code;
         long _createdBy;
         //PaymentModeInfo _creditMode = new PaymentModeInfo();
         //PaymentModeInfo _cardMode = new PaymentModeInfo();
         //PaymentModeInfo _employeeMode = new PaymentModeInfo();
         //PaymentModeInfo _othersMode = new PaymentModeInfo();
         string _modeRemarks;
         DataTable _dtAllocationList;
         long _vsMapId;
         string _vsMapDocNo;
         # endregion

         # region Properties
         public long TransactionId
         {
             get { return _transactionId; }
             set { _transactionId = value; }
         }
         public string CompanyId
         {
             get { return _companyId; }
             set { _companyId= value; }
         }
          public string BaseDocType
         {
             get { return _baseDocType; }
             set { _baseDocType = value; }
         }
         public string DocType
         {
             get { return _docType; }
             set { _docType = value; }
         }
         public string DocNumber
         {
             get { return _docNumber; }
             set { _docNumber = value; }
         }

         public DateTime DocDate
         {
             get { return _docDate; }
             set { _docDate = value; }
         }

         public string CashBankCode
         {
             get { return _cashBankCode; }
             set { _cashBankCode = value; }
         }
        
         public long CreatedBy
         {
             get { return _createdBy; }
             set { _createdBy = value; }
         }
         //public string CreatedByName
         //{
         //    get { return _createdByName; }
         //    set { _createdByName = value; }
         //}
         //public string SettlementMode
         //{
         //    get { return _settlementMode; }
         //    set { _settlementMode = value; }
         //}
         public string CurrencyCode
         {
             get { return _currencyCode; }
             set { _currencyCode= value; }
         }
         public decimal ExchangeRate
         {
             get { return _exchangeRate; }
             set { _exchangeRate= value; }
         }
         public PaymentModeInfo CashMode
         {
             get { return _cashMode; }
             set { _cashMode = value; }
         }

        public string OtherReference
         {
             get { return _otherReference; }
             set { _otherReference= value; }
         }
         public string ChequeNo
         {
             get { return _chequeNo; }
             set { _chequeNo = value; }
         }
         public string Narration
         {
             get { return _narration; }
             set { _narration= value; }
         }
         public string GLAccounts
         {
             get { return _GLAccounts; }
             set { _GLAccounts= value; }
         }
        public string Notes
         {
             get { return _notes; }
             set { _notes= value; }
         }
         public string Flex1
         {
             get { return _flex1; }
             set { _flex1 = value; }
         }
         public DataTable AllocationList
         {
             get { return _dtAllocationList; }
             set { _dtAllocationList = value; }
         }
         //public PaymentModeInfo CreditMode
         //{
         //    get { return _creditMode; }
         //    set { _creditMode = value; }
         //}
         //public PaymentModeInfo CardMode 
         //{
         //    get { return _cardMode; }
         //    set { _cardMode = value; }
         //}
         //public PaymentModeInfo EmployeeMode 
         //{
         //    get { return _employeeMode; }
         //    set { _employeeMode = value; }
         //}
         //public PaymentModeInfo OthersMode 
         //{
         //    get { return _othersMode; }
         //    set { _othersMode = value; }
         //}
         public long VsMapId
         {
             get { return _vsMapId; }
             set { _vsMapId = value; }
         }
         public string VsMapDocNo
         {
             get { return _vsMapDocNo; }
             set { _vsMapDocNo = value; }
         }
         public string Map_location_code
         {
             get { return _map_location_code; }
             set { _map_location_code = value; }
         }
         public string Map_group_code
         {
             get { return _map_group_code; }
             set { _map_group_code = value; }
         }
         
         
         # endregion

         # region Constructors

         public VSORAInterface()
         {
             //
             // TODO: Add constructor logic here
             //
             _transactionId = -1;
             //DataSet ds = GetDetails(_transactionId);
             ////if (dt != null && dt.Rows.Count > 0)
             //UpdateBusinessData(ds);
         }
         //public VSORAInterface(long transactionId)
         //{
         //    try
         //    {
         //        DataSet ds = GetDetails(transactionId);
         //        //if(dt!=null && dt.Rows.Count>0)
         //        UpdateBusinessData(ds);
         //    }
         //    catch { throw; }

         //}
         # endregion

         # region Private Methods
       
         public void Save()
         {
             OracleTransaction trans = null;
             OracleCommand cmd = null;
             try
             {
                 //SqlCommand cmd = DBGateway.CreateCommand(trans);
                 cmd = new OracleCommand();
                 cmd.Connection = ORADBGateway.CreateConnection();
                 cmd.Connection.Open();
                 trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                 //cmd.Transaction = con;

                 OracleParameter[] paramArr = new OracleParameter[21];
                 paramArr[0] = new OracleParameter("p_rh_company", _companyId);

                 paramArr[1] = new OracleParameter("p_rh_base_doc_type", _baseDocType);
                 paramArr[2] = new OracleParameter("p_rh_doc_type", _docType);
                 paramArr[3] = new OracleParameter("p_rh_doc_number", _docNumber);
                 paramArr[3].Direction = ParameterDirection.InputOutput;
                 paramArr[4] = new OracleParameter("p_rh_doc_date", _docDate);
                 paramArr[5] = new OracleParameter("p_rh_cash_bank_code", _cashBankCode);
                 paramArr[6] = new OracleParameter("p_rh_currency", _currencyCode);
                 paramArr[7] = new OracleParameter("p_rh_exchange_rate", _exchangeRate);
                 paramArr[8] = new OracleParameter("p_rh_foreign_amount",_cashMode.BaseAmount);
                 paramArr[9] = new OracleParameter("p_rh_local_amount", _cashMode.LocalAmount);
                 paramArr[10] = new OracleParameter("p_rh_other_reference", _otherReference);
                 paramArr[11] = new OracleParameter("p_rh_cheque_number", _chequeNo);
                 paramArr[12] = new OracleParameter("p_rh_narration", _narration);
                 paramArr[13] = new OracleParameter("p_rh_gl_account", _GLAccounts);
                 paramArr[14] = new OracleParameter("p_rh_notes", _notes);
                 paramArr[15] = new OracleParameter("p_rh_flex_1", _flex1);
                 paramArr[16] = new OracleParameter("p_rh_created_by", "IBV");
                 paramArr[17] = new OracleParameter("p_rh_visa_map_id",_vsMapId);
                 paramArr[18] = new OracleParameter("p_rh_map_doc_number", _vsMapDocNo);
                 paramArr[19] = new OracleParameter("p_rh_map_location_code", _map_location_code);
                 paramArr[20] = new OracleParameter("p_rh_map_group_code", _map_group_code);

                 //trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                 ORADBGateway.ExecuteNonQueryDetails(cmd, "VISA_INTERFACE_MODULE.id_interface_hdr_add_update", paramArr);
                 _docNumber = Utility.ToString(paramArr[3].Value);

                 if (_dtAllocationList!= null)
                 {
                     DataTable dt = _dtAllocationList.GetChanges();
                     int recordStatus = 0;
                     if (dt != null && dt.Rows.Count > 0)
                     {

                         foreach (DataRow dr in dt.Rows)
                         {
                             switch (dr.RowState)
                             {
                                 case DataRowState.Added: recordStatus = 1;
                                     break;
                                 case DataRowState.Modified: recordStatus = 2;
                                     break;
                                 case DataRowState.Deleted: recordStatus = -1;
                                     break;
                                 default: break;


                             }
                             saveDetails(cmd, recordStatus, "1", _baseDocType, DocType, _docNumber, Utility.ToString(dr["ALOC_ALLOCATION_TYPE"]), Utility.ToDecimal(dr["ALOC_LOCAL_AMOUNT"]), Utility.ToDecimal(dr["ALOC_FOREIGN_AMOUNT"]),
                                         Utility.ToString(dr["ALOC_NARRATION"]), Utility.ToString(dr["ALOC_NOTES"]), Utility.ToString(dr["ALOC_DETAIL_1"]), Utility.ToString(dr["ALOC_DETAIL_2"]),
                                            Utility.ToString(dr["ALOC_DETAIL_3"]), Utility.ToString(dr["ALOC_ACCOUNT"]), _vsMapId, Utility.ToString(dr["ALOC_SEG_7"]));
                         }
                     }
                 }
                 trans.Commit();
             }
             catch
             {
                 trans.Rollback();
                 //cmd.Connection.Close();
                 throw;
             }
             finally
             {
                 cmd.Connection.Close();
             }
         }
         private void saveDetails(OracleCommand cmd, int recordStatus, string companyId, string hdrBaseDocType, string hdrDocType,
           string hdrDocNo, string allocationType, decimal localAmount, decimal baseAmount, string narration, string notes, string detail1, string detail2, string detail3, string allocAcct,long vsMapId,string discount)
         {
             try
             {
                 OracleParameter[] paramArr = new OracleParameter[16];
                 paramArr[0] = new OracleParameter("@p_aloc_company", companyId);
                 paramArr[1] = new OracleParameter("@p_aloc_base_doc", hdrBaseDocType);
                 paramArr[2] = new OracleParameter("@p_aloc_doc_type", hdrDocType);
                 paramArr[3] = new OracleParameter("@p_aloc_doc_number", hdrDocNo);
                 paramArr[4] = new OracleParameter("@p_aloc_allocation_type", allocationType);
                 paramArr[5] = new OracleParameter("@p_aloc_foreign_amount", baseAmount);
                 paramArr[6] = new OracleParameter("@p_aloc_local_amount", localAmount);
                 paramArr[7] = new OracleParameter("@p_aloc_narration", narration);
                 paramArr[8] = new OracleParameter("@p_aloc_notes", notes);
                 paramArr[9] = new OracleParameter("@p_aloc_detail_1", detail1);
                 paramArr[10] = new OracleParameter("@p_aloc_detail_2", detail2);
                 paramArr[11] = new OracleParameter("@p_aloc_detail_3", detail3);
                 paramArr[12] = new OracleParameter("@p_aloc_account", allocAcct);
                 paramArr[13] = new OracleParameter("@p_aloc_visa_map_id", VsMapId);
                 paramArr[14] = new OracleParameter("@p_aloc_map_group_code", _map_group_code);
                 paramArr[15] = new OracleParameter("@p_aloc_seg_7", discount);

                 ORADBGateway.ExecuteNonQueryDetails(cmd, "VISA_INTERFACE_MODULE.id_interface_dtl_add_update", paramArr);
                 //if (Utility.ToString(paramMsgType.Value) == "E")
                 //    throw new Exception(Utility.ToString(paramMsgText.Value));

                 //_docNumber = Utility.ToString(paramDocNo.Value);
                 //_transactionId = Utility.ToLong(paramMsgText1.Value);
             }
             catch { throw; }
         }
         public static void GenerateInterfaceReceipt(long vsId, VisaAccountedStatus accStatus, string pnrStatus, long ticketId, long createdBy)
         {
             string _rcptDocNO=string.Empty;
             OracleTransaction trans = null;
             OracleCommand cmd = null;
             try
             {
                 //SqlCommand cmd = DBGateway.CreateCommand(trans);
                 cmd = new OracleCommand();
                 cmd.Connection = ORADBGateway.CreateConnection();
                 cmd.Connection.Open();
                 trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                 //cmd.Transaction = con;

                 OracleParameter[] paramArr = new OracleParameter[3];
                 paramArr[0] = new OracleParameter("p_rh_visa_map_id", vsId);
                 paramArr[1] = new OracleParameter("p_rh_map_group_code", "COZMO");// TODO ..to be added as property later
                 paramArr[2] = new OracleParameter("p_rh_doc_number", _rcptDocNO);
                 paramArr[2].Direction = ParameterDirection.InputOutput;

                 ORADBGateway.ExecuteNonQueryDetails(cmd, "VISA_INTERFACE_MODULE.ID_VISA_MAP_RECEIPT_GENERATE", paramArr);
                 _rcptDocNO= Utility.ToString(paramArr[2].Value);

                TranxVisaSales.UpdateAccountedStatus(vsId, accStatus,pnrStatus, ticketId, createdBy,_rcptDocNO);
                 trans.Commit();
             }
             catch
             {
                 trans.Rollback();
                 //cmd.Connection.Close();
                 throw;
             }
             finally
             {
                 cmd.Connection.Close();
             }
         }
         # endregion
     }
}