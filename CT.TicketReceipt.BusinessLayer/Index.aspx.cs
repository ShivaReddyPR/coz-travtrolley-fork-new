using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;

public partial class IndexUI : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
            }
            
            //lblErrorMsg.Text = string.Empty;
        }
        catch (Exception ex)
        {
        }
    }
    private void InitializePageControls()
    {
        try
        {
            BindVisaCountList();
            BindVisaDelyaedCountList();
        }
        catch { throw; }
    }
    private void BindVisaCountList()
    {
        try
        {
            LoginInfo loginfo=Settings.LoginInfo;
            CommonGrid grid = new CommonGrid();
            DataTable dtDashBoard = DashBorad.GetVisaCountList(loginfo.UserID, loginfo.MemberType.ToString(), loginfo.LocationID, loginfo.CompanyID, ListStatus.Short, RecordStatus.Activated).Tables[0];
            grid.BindGrid(gvDBVisaCount,dtDashBoard);
            DataTable dtTemp = dtDashBoard.Copy();
            txtTotCount.Text=Utility.ToString(dtTemp.Compute("SUM(pax_dispatch_status_count)",""));
        }
        catch { throw; }
    }
    private void BindVisaDelyaedCountList()
    {
        try
        {
            int delayedDays = Utility.ToInteger(System.Configuration.ConfigurationManager.AppSettings["VISA_DELAY_DAYS"]);
            LoginInfo loginfo = Settings.LoginInfo;
            CommonGrid grid = new CommonGrid();
            DataTable dtDashBoard = DashBorad.GetVisaDelayedCountList(VisaDispatchStatus.Approved, delayedDays, loginfo.UserID, loginfo.MemberType.ToString(), loginfo.LocationID, loginfo.CompanyID, ListStatus.Short, RecordStatus.Activated).Tables[0];
            grid.BindGrid(gvDBVisaDelayCount, dtDashBoard);
            DataTable dtTemp = dtDashBoard.Copy();
            txtTotDelayedCount.Text = Utility.ToString(dtTemp.Rows.Count);
        }
        catch { throw; }
    }


    protected void lnkTicketModule_Click(object sender, EventArgs e)
    {
        try
        {
            LoginInfo loginInfo = Settings.LoginInfo;
            switch (loginInfo.MemberType)
            {
                case MemberType.ADMIN:
                    Response.Redirect("UserList.aspx");
                    break;
                case MemberType.CASHIER:
                    Response.Redirect("ReceiptList.aspx");
                    break;
                case MemberType.OPERATIONS:
                    Response.Redirect("ReceiptMaster.aspx");
                    break;
                case MemberType.SUPERVISOR:
                    Response.Redirect("ReceiptList.aspx");
                    break;
            } 
        }
        catch (Exception ex)
        {
        }
    }
    protected void lnkVisaModule_Click(object sender, EventArgs e)
    {
        try
        {
            LoginInfo loginInfo = Settings.LoginInfo;
            if (loginInfo == null)
            {
                Response.Redirect("SessionExpired.aspx");
                // Response.Redirect(string.Format("ErrorPage.aspx?Err={0}", GetGlobalResourceObject("ErrorMessages", "INVALID_USER")));
            }
            switch (loginInfo.MemberType)
            {
                case MemberType.ADMIN:
                    Response.Redirect("VisaSalesList.aspx");
                    break;
                case MemberType.CASHIER:
                    Response.Redirect("VisaSalesList.aspx");
                    break;
                case MemberType.OPERATIONS:
                    Response.Redirect("TranxVisaSales.aspx");
                    break;
                case MemberType.BACKOFFICE:
                    Response.Redirect("DispatchToOP.aspx");
                    break;
                case MemberType.SUPERVISOR:
                    Response.Redirect("VisaSalesList.aspx");
                    break;
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void gvDBVisaCount_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvDBVisaCount.PageIndex = e.NewPageIndex;
            BindVisaCountList();
            //Filter_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    protected void gvDBVisaDelayCount_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvDBVisaDelayCount.PageIndex = e.NewPageIndex;
            BindVisaDelyaedCountList();
            //Filter_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }


    #region Date Format
    protected string IDDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }
    # endregion
}
