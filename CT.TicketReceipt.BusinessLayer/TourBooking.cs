using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;

/// <summary>
/// Summary description for HandlingFeeMaster
/// </summary>
namespace CT.TicketReceipt.BusinessLayer
{
    //public enum PurchaseOrderStatus
    //{
    //    All = 0,
    //    Pending = 80,// Pending
    //    Approved = 65,// Pending
    //    Rejected = 82
    //}
    public class TourBooking
    {
        private const long NEW_RECORD = -1;
        #region Member Variables
        private long _id;
        private DateTime _docDate;
        private string _docType;
        //private string _docType;
        private string _bookingNo;
        private long _customerId;
        private string _leadPaxname;
        private string _paxAddress;
        private string _paxPhone;
        private string _paxMobile;
        private string _paxFax;
        private string _paxEmail;
        private string _paxAgentRefNo;
        private long _adult;
        private long _child;
        private long _infant;
        private long _region;
        private long _locationId;
        private string _notes;
        private string _remarks;
        private string  _settlementMode;
        string _currencyCode;
        decimal _exchangeRate;
        PaymentModeInfo _cashMode = new PaymentModeInfo();
        PaymentModeInfo _creditMode = new PaymentModeInfo();
        PaymentModeInfo _cardMode = new PaymentModeInfo();
        PaymentModeInfo _employeeMode = new PaymentModeInfo();
        PaymentModeInfo _othersMode = new PaymentModeInfo();
        private string _crCardType;
        private string _crCardName;
        private decimal _crCardRate = 0;
        private decimal _crCardTotal = 0;
        private decimal _discount = 0;
        private string _modeRemarks;
        private decimal _totalCollection;
        private decimal _totalProvision;
        private string _bookingStatus;
        private string _status;        
        private long _createdBy;
        private long _retBookingId;
       
        private string _locationName;
        private string _locationAddress;        
        private string _createdByName;
        private string _customerName;
        private string _customerAddress;

        private decimal _totalCost;
        private decimal _totalSellingPrice;
        private decimal _totalMarkup;
        private string _totalGSACommOn;
        private string _totalGSACommType;
        private decimal _totalGSAComm;
        private decimal _totalGSACommAmt;
        private decimal _totalDiscount;
        

        DataTable _dtPaxDetails;
        DataTable _dtHotelDetails;
        DataTable _dtCarDetails;
        DataTable _dtOtherDetails;
        //DataTable _dtCategoryDetails;
        #endregion

        #region Properties
        public long ID
        {
            get { return _id; }
            set {  _id = value ; }
        }
        public DateTime DocDate
        {
            get { return _docDate; }
            set { _docDate = value; }
        }
        public string DocType
        {
            get { return _docType; }
            set { _docType = value; }
        }
        //public string DocType
        //{
        //    get { return _docType; }
        //    set { _docType = value; }
        //}
        public string BookingNo
        {
            get { return _bookingNo ; }
            set { _bookingNo = value; }
        }
        public long CustomerId
        {
            get { return _customerId; }
            set { _customerId = value; }
        }

        public string  LeadPaxName
        {
            get { return _leadPaxname ; }
            set { _leadPaxname = value; }
        }

        public string PaxAddress
        {
            get { return _paxAddress ; }
            set { _paxAddress = value; }
        }

        public string PaxPhone
        {
            get { return _paxPhone ; }
            set { _paxPhone = value; }
        }

        public string PaxMobile
        {
            get { return _paxMobile ; }
            set { _paxMobile = value; }

        }
        
        public string PaxFax
        {
            get { return _paxFax ; }
            set { _paxFax = value; }
        }
        public string PaxEmail
        {
            get { return _paxEmail; }
            set { _paxEmail = value; }
        }

        public string PaxAgentRefNo
        {
            get { return _paxAgentRefNo ; }
            set { _paxAgentRefNo = value; }
        }

        public long  Adult
        {
            get { return _adult; }
            set { _adult = value; }
        }

        public long Child
        {
            get { return _child ; }
            set { _child = value; }
        }

        public long  Infant
        {
            get { return _infant ; }
            set { _infant = value; }
        }

        public long Region  
        {
            get { return _region  ; }
            set { _region = value; }
        }
        public long LocationId
        {

            get { return _locationId; }
            set { _locationId = value; }
        
        }
        public string  SettlementMode
        {
            get{return  _settlementMode; }
            set { _settlementMode = value; }            
        }

        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }
        public string Remarks
        {
            get { return _remarks ; }
            set { _remarks  = value; }
        }

        public string Currency
        {
            get { return _currencyCode; }
            set { _currencyCode = value; }
        }

        public decimal  ExchangeRate
        {
            get { return _exchangeRate; }
            set { _exchangeRate = value; }
        }
        public decimal Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }

        public decimal TotalCollection
        {
            get { return _totalCollection; }
            set { _totalCollection = value; }
        
        }
        public decimal TotalProvision
        {
            get { return _totalProvision; }
            set { _totalProvision = value; }
        }

        public string BookingStatus
        {
            get { return _bookingStatus; }
            set { _bookingStatus = value; }
        }

        public string  Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public long RetBookingId
        {
            get { return _retBookingId ; }
            set { _retBookingId = value; }
        }

        public string LocationName
        {
            get { return _locationName; }
            set { _locationName = value; }
        }

        public string LocationAddress
        {
            get { return _locationAddress; }
            set { _locationAddress = value; }
        }
        public string CustomerAddress
        {
            get { return _customerAddress ; }
            set { _customerAddress = value; }
        }

        public string CreatedByName
        {
            get { return _createdByName; }
            set { _createdByName = value; }
        }
        public string CustomerName
        {
            get { return _customerName; }
            set { _customerName = value; }
        }


        public decimal TotalCost
        {
            get { return _totalCost; }
            set { _totalCost = value; }

        }

        public decimal TotalSellingPrice
        {
            get { return _totalSellingPrice; }
            set { _totalSellingPrice = value; }

        }

        public decimal TotalMarkup
        {
            get { return _totalMarkup; }
            set { _totalMarkup = value; }

        }

        public string TotalGSACommOn
        {
            get { return _totalGSACommOn; }
            set { _totalGSACommOn = value; }

        }

        public string TotalGSACommType
        {
            get { return _totalGSACommType; }
            set { _totalGSACommType = value; }

        }

        public decimal TotalGSAComm
        {
            get { return _totalGSAComm ; }
            set { _totalGSAComm = value; }

        }

        public decimal TotalGSACommAmt
        {
            get { return _totalGSACommAmt; }
            set { _totalGSACommAmt = value; }

        } 

        public decimal TotalDiscount
        {
            get { return _totalDiscount; }
            set { _totalDiscount = value; }

        }





        public DataTable DTPaxDetails
        {
            get { return _dtPaxDetails; }
            set { _dtPaxDetails = value; }
        }

        public DataTable DTHotelDetails
        {
            get { return _dtHotelDetails ; }
            set { _dtHotelDetails = value; }
        }

        public DataTable DTCarDetails
        {
            get { return _dtCarDetails; }
            set { _dtCarDetails = value; }
        }

        public DataTable DTOtherDetails
        {
            get { return _dtOtherDetails; }
            set { _dtOtherDetails = value; }
        }


        //public DataTable DTCategoryDetails
        //{
        //    get { return _dtCategoryDetails; }
        //    set { _dtCategoryDetails = value; }
        //}
        

        public PaymentModeInfo CashMode
        {
            get { return _cashMode; }
            set { _cashMode = value; }
        }
        public PaymentModeInfo CreditMode
        {
            get { return _creditMode; }
            set { _creditMode = value; }
        }
        public PaymentModeInfo CardMode
        {
            get { return _cardMode; }
            set { _cardMode = value; }
        }
        public PaymentModeInfo EmployeeMode
        {
            get { return _employeeMode; }
            set { _employeeMode = value; }
        }
        public PaymentModeInfo OthersMode
        {
            get { return _othersMode; }
            set { _othersMode = value; }
        }

        public string ModeRemarks
        {
            get { return _modeRemarks; }
            set { _modeRemarks = value; }
        }

        public string CRCardType
        {
            get { return _crCardType; }
            set { _crCardType = value; }
        } 
        public string CRCardName
        {
            get { return _crCardName; }
            set { _crCardName = value; }
        }
        public decimal CRCardRate
        {
            get { return _crCardRate; }
            set { _crCardRate = value; }
        }
        public decimal CRCardTotal
        {
            get { return _crCardTotal; }
            set { _crCardTotal = value; }
        }
        #endregion

        #region Constructors
        public TourBooking()
        {
            _id = -1;
            DataSet ds = GetData(_id);
            //if (dt != null && dt.Rows.Count > 0)
            UpdateBusinessData(ds);
        }
        public TourBooking(long id)
        {

            _id = id;
            GetDetails(id);
        }
        #endregion

        #region Methods
        private void GetDetails(long id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);
            //if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            //{
            //    UpdateBusinessData(ds.Tables[0].Rows[0]);
            //}

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_BKH_ID", id);
                paramList[1] = new SqlParameter("@P_BKH_LOCATION_ID", Settings.LoginInfo.LocationID);
                DataSet dsResult = DBGateway.ExecuteQuery("CT_P_TOUR_BOOKING_HEADER_GETDATA", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {

                if (_id < 0)
                {
                    _docDate = Utility.ToDate(dr["DOC_DATE"]);
                    _docType = Utility.ToString(dr["DOC_TYPE"]);
                }
                else
                {
                    _id = Utility.ToLong(dr["bkh_id"]);
                    _docDate = Utility.ToDate(dr["DOC_DATE"]);
                    _docType = Utility.ToString(dr["DOC_TYPE"]);
                    _bookingNo = Utility.ToString(dr["bkh_doc_no"]);
                    _leadPaxname = Utility.ToString(dr["bkh_lead_pax_name"]);
                    _paxAddress = Utility.ToString(dr["bkh_pax_address"]);
                    _customerId =Utility.ToLong(dr["bkh_customer_id"]);
                    _paxPhone = Utility.ToString(dr["bkh_pax_phone"]);
                    _paxMobile= Utility.ToString(dr["bkh_pax_mobile"]);
                    _paxFax= Utility.ToString(dr["bkh_pax_fax"]);
                    _paxEmail= Utility.ToString(dr["bkh_pax_email"]);
                    _paxAgentRefNo= Utility.ToString(dr["bkh_pax_agent_refno"]);
                    _adult=Utility.ToLong(dr["bkh_adult"]);
                    _child =Utility.ToLong(dr["bkh_child"]);
                    _infant=Utility.ToLong(dr["bkh_infant"]);
                    _region =Utility.ToLong(dr["bkh_region"]);
                    _locationId=Utility.ToLong(dr["bkh_location_id"]);
                    _notes = Utility.ToString(dr["bkh_notes"]);
                    _remarks =  Utility.ToString(dr["bkh_remarks"]);
                    _settlementMode = Utility.ToString(dr["bkh_settle_mode"]);
                    _cashMode.LocalAmount =Utility.ToDecimal(dr["bkh_cash"]);
                    _cardMode.LocalAmount =Utility.ToDecimal(dr["bkh_card"]);
                    _creditMode.LocalAmount =Utility.ToDecimal(dr["bkh_credit"]);
                    _employeeMode.LocalAmount =Utility.ToDecimal(dr["bkh_employee"]);
                    _othersMode.LocalAmount =Utility.ToDecimal(dr["bkh_others"]);
                    _modeRemarks = Utility.ToString(dr["bkh_settle_remarks"]); 
                    _currencyCode = Utility.ToString(dr["bkh_currency"]);
                    _exchangeRate = Utility.ToDecimal(dr["bkh_exchange_rate"]);
                    _crCardRate= Utility.ToDecimal(dr["bkh_card_rate"]);
                    _crCardType= Utility.ToString(dr["bkh_card_type"]);
                    _crCardName = Utility.ToString(dr["bkh_card_name"]);
                    _crCardTotal = Utility.ToDecimal(dr["bkh_card_total"]);
                    _discount =  Utility.ToDecimal(dr["bkh_discount"]);
                    _totalCollection = Utility.ToDecimal(dr["bkh_total_collection"]);
                    _totalProvision= Utility.ToDecimal(dr["bkh_total_provision"]);
                    _locationName = Utility.ToString(dr["bkh_location_name"]);
                    _locationAddress = Utility.ToString(dr["bkh_location_address"]);                   
                    _customerName  = Utility.ToString(dr["ph_customer_Name"]);
                    _createdByName = Utility.ToString(dr["createdByName"]);
                    _totalCost = Utility.ToDecimal(dr["bkh_total_cost"]);
                    _totalSellingPrice = Utility.ToDecimal(dr["bkh_total_selling_price"]);
                    _totalMarkup = Utility.ToDecimal(dr["bkh_total_markup"]);
                    _totalGSACommType = Utility.ToString(dr["bkh_total_GSA_comm_type"]);
                    _totalGSACommOn = Utility.ToString(dr["bkh_total_GSA_comm_on"]);
                    _totalGSAComm = Utility.ToDecimal(dr["bkh_total_GSA_comm"]);
                    _totalGSACommAmt = Utility.ToDecimal(dr["bkh_total_GSA_comm_amt"]);
                    _totalDiscount = Utility.ToDecimal(dr["bkh_total_discount"]);
                   
                   
                }
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);

                    if (ds.Tables.Count > 4)
                    {
                        _dtPaxDetails = ds.Tables[1];
                        _dtHotelDetails = ds.Tables[2];
                        _dtCarDetails = ds.Tables[3];
                        _dtOtherDetails = ds.Tables[4];
                    }
                }

            }
            catch
            { throw; }

        }
        #endregion

        #region Public Methods
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;

            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;


                SqlParameter[] paramList = new SqlParameter[49];
                paramList[0] = new SqlParameter("@P_BKH_ID", _id);
                paramList[1] = new SqlParameter("@P_bkh_doc_type", _docType);

                //paramList[2] = new SqlParameter("@P_bkh_doc_no", _docDate);
                paramList[2] = new SqlParameter("@P_bkh_date", _docDate );                                
                paramList[3] = new SqlParameter("@P_bkh_lead_pax_name",_leadPaxname );
                paramList[4] = new SqlParameter("@P_bkh_pax_address", _paxAddress );
                paramList[5] = new SqlParameter("@P_bkh_customer_id", _customerId );
                paramList[6] = new SqlParameter("@P_bkh_pax_phone", _paxPhone );
                paramList[7] = new SqlParameter("@P_bkh_pax_mobile", _paxMobile );

                paramList[8] = new SqlParameter("@P_bkh_pax_fax", _paxFax );
                paramList[9] = new SqlParameter("@P_bkh_pax_email", _paxEmail);
                paramList[10] = new SqlParameter("@P_bkh_pax_agent_refno", _paxAgentRefNo  );
                paramList[11] = new SqlParameter("@P_bkh_adult", _adult  );
                paramList[12] = new SqlParameter("@P_bkh_child", _child );
                paramList[13] = new SqlParameter("@P_bkh_infant", _infant );
                paramList[14] = new SqlParameter("@P_bkh_region", _region );
                paramList[15] = new SqlParameter("@P_bkh_location_id", _locationId);
                paramList[16] = new SqlParameter("@P_bkh_notes", _notes );

                paramList[17] = new SqlParameter("@P_bkh_remarks", _remarks );
                paramList[18] = new SqlParameter("@P_bkh_currency", _currencyCode );
                paramList[19] = new SqlParameter("@P_bkh_exchange_rate", _exchangeRate );
                paramList[20] = new SqlParameter("@P_bkh_settle_mode", _settlementMode );
                paramList[21] = new SqlParameter("@P_bkh_cash", _cashMode.LocalAmount );
                paramList[22] = new SqlParameter("@P_bkh_card", _cardMode.LocalAmount );
                paramList[23] = new SqlParameter("@P_bkh_credit", _creditMode.LocalAmount);
                paramList[24] = new SqlParameter("@P_bkh_employee", _employeeMode.LocalAmount );
                paramList[25] = new SqlParameter("@P_bkh_others", _othersMode.LocalAmount );

                paramList[26] = new SqlParameter("@P_bkh_card_rate", _crCardRate );
                paramList[27] = new SqlParameter("@P_bkh_card_type", _crCardType );
                paramList[28] = new SqlParameter("@P_bkh_card_name", _crCardName );
                paramList[29] = new SqlParameter("@P_bkh_card_total", _crCardTotal );
                paramList[30] = new SqlParameter("@P_bkh_discount", _discount );
                paramList[31] = new SqlParameter("@P_bkh_total_collection", _totalCollection );
                paramList[32] = new SqlParameter("@P_bkh_total_provision", _totalProvision );
                paramList[33] = new SqlParameter("@P_bkh_status", _status);
                paramList[34] = new SqlParameter("@P_bkh_created_by", _createdBy);

                paramList[35] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[35].Direction = ParameterDirection.Output;
                paramList[36] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[36].Direction = ParameterDirection.Output;
                paramList[37] = new SqlParameter("@P_BKH_ID_RET", SqlDbType.BigInt);
                paramList[37].Direction = ParameterDirection.Output;
                paramList[38] = new SqlParameter("@P_bkh_doc_no", SqlDbType.VarChar, 50);
                paramList[38].Direction = ParameterDirection.Output;
                paramList[39] = new SqlParameter("@P_bkh_booking_status", _bookingStatus);
                paramList[40] = new SqlParameter("@P_bkh_settle_remarks", _modeRemarks);

                paramList[41] = new SqlParameter("@P_bkh_total_cost", _totalCost);
                paramList[42] = new SqlParameter("@P_bkh_total_selling_price", _totalSellingPrice );
                paramList[43] = new SqlParameter("@P_bkh_total_markup", _totalMarkup );
                paramList[44] = new SqlParameter("@P_bkh_total_GSA_comm_on", _totalGSACommOn);
                paramList[45] = new SqlParameter("@P_bkh_total_GSA_comm_type", _totalGSACommType );
                paramList[46] = new SqlParameter("@P_bkh_total_GSA_comm", _totalGSAComm );
                paramList[47] = new SqlParameter("@P_bkh_total_GSA_comm_amt", _totalGSACommAmt);
                paramList[48] = new SqlParameter("@P_bkh_total_discount", _totalDiscount );
                DBGateway.ExecuteNonQuery("CT_P_TOUR_BOOKING_HEADER_ADD_UPDATE", paramList);
                string messageType = Utility.ToString(paramList[35].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[35].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                _retBookingId  = Utility.ToLong(paramList[37].Value);
                _bookingNo  = Utility.ToString(paramList[38].Value);
               
                if (_dtPaxDetails != null)
                {
                    DataTable dt = _dtPaxDetails.GetChanges();
                    int recordStatus = 0;
                    if (dt != null && dt.Rows.Count > 0)
                    {
                                                
                        foreach (DataRow dr in dt.Rows)
                        {
                            switch (dr.RowState)
                            {
                                case DataRowState.Added: recordStatus = 1;
                                    break;
                                case DataRowState.Modified: recordStatus = 2;
                                    break;
                                case DataRowState.Deleted: recordStatus = -1;
                                    dr.RejectChanges();
                                    break;
                                default: break;


                            }
                            SavePaxDeatils(cmd, recordStatus, _retBookingId, Utility.ToLong(dr["pax_bkd_id"]), Utility.ToString(dr["pax_bkd_name"]), Utility.ToString(dr["pax_bkd_sex"]), Utility.ToString(dr["pax_bkd_type"]),
                                Utility.ToLong(dr["pax_bkd_nationality"]), Utility.ToString(dr["pax_passport_no"]), Utility.ToString(dr["pax_bkd_airpoprt"]), Utility.ToDate(dr["pax_bkd_arrival_date"]), Utility.ToDate(dr["pax_bkd_depart_date"]), Utility.ToLong(dr["pax_bkd_created_by"]), 
                                Utility.ToString(dr["pax_bkd_status"]));

                        }
                        
                    }
                }
                //Hotel details
                //if (_dtHotelDetails  != null)
                //{
                //    DataTable dt = _dtHotelDetails.GetChanges();
                //    int recordStatus = 0;
                //    if (dt != null && dt.Rows.Count > 0)
                //    {
                                               
                //        foreach (DataRow dr in dt.Rows)
                //        {
                //            switch (dr.RowState)
                //            {
                //                case DataRowState.Added: recordStatus = 1;
                //                    break;
                //                case DataRowState.Modified: recordStatus = 2;
                //                    break;
                //                case DataRowState.Deleted: recordStatus = -1;
                //                    dr.RejectChanges();
                //                    break;
                //                default: break;


                //            }
                //            SaveHotelDeatils(cmd, recordStatus, _retBookingId, Utility.ToLong(dr["htl_bkd_id"]), Utility.ToString(dr["htl_bkd_city"]), Utility.ToDate(dr["htl_bkd_from_date"]), Utility.ToDate(dr["htl_bkd_to_date"]),
                //                Utility.ToString(dr["htl_bkd_service"]),Utility.ToLong(dr["htl_bkd_unit"]), Utility.ToString(dr["htl_bkd_category"]), Utility.ToString(dr["htl_bkd_tour_operator"]), Utility.ToString(dr["htl_bkd_hotel"]), Utility.ToString(dr["htl_bkd_rate_type"]),
                //                 Utility.ToString(dr["htl_bkd_meal_plan"]), Utility.ToString(dr["htl_bkd_currency"]), Utility.ToDecimal(dr["htl_bkd_exch_rate"]), Utility.ToDecimal(dr["htl_bkd_rate"]), Utility.ToDecimal(dr["htl_bkd_cost_rate"]), Utility.ToString(dr["htl_bkd_markup_method"]),
                //                 Utility.ToDecimal(dr["htl_bkd_markup_value"]), Utility.ToDecimal(dr["htl_bkd_discount"]), Utility.ToDecimal(dr["htl_bkd_total"]), Utility.ToString(dr["htl_bkd_sell_currency"]), Utility.ToDecimal(dr["htl_bkd_sell_exch_rate"]), Utility.ToDecimal(dr["htl_bkd_sell_rate"]), Utility.ToString(dr["htl_bkd_narration"]),
                //                 Utility.ToString(dr["htl_bkd_inclusive_information"]), Utility.ToString(dr["htl_bkd_service_desc"]), Utility.ToString(dr["htl_bkd_confirm_no"]), Utility.ToString(dr["htl_bkd_SV_generateYN"]), Utility.ToString(dr["htl_bkd_status"]), Utility.ToLong(dr["htl_bkd_created_by"]));

                //        }
                       
                //    }
                //}

                //Car Rental Details

                //if (_dtCarDetails != null)
                //{
                //    DataTable dt = _dtCarDetails.GetChanges();
                //    int recordStatus = 0;
                //    if (dt != null && dt.Rows.Count > 0)
                //    {

                //        foreach (DataRow dr in dt.Rows)
                //        {
                //            switch (dr.RowState)
                //            {
                //                case DataRowState.Added: recordStatus = 1;
                //                    break;
                //                case DataRowState.Modified: recordStatus = 2;
                //                    break;
                //                case DataRowState.Deleted: recordStatus = -1;
                //                    dr.RejectChanges();
                //                    break;
                //                default: break;


                //            }
                //            SaveCarDeatils(cmd, recordStatus, _retBookingId, Utility.ToLong(dr["car_bkd_id"]), Utility.ToString(dr["car_bkd_city"]), Utility.ToDate(dr["car_bkd_from_date"]), Utility.ToDate(dr["car_bkd_to_date"]),
                //                Utility.ToString(dr["car_bkd_service"]), Utility.ToLong(dr["car_bkd_unit"]), Utility.ToString(dr["car_bkd_category"]), Utility.ToString(dr["car_bkd_tour_operator"]), Utility.ToString(dr["car_bkd_car"]), Utility.ToString(dr["car_bkd_rate_type"]),
                //                 Utility.ToString(dr["car_bkd_transport"]), Utility.ToString(dr["car_bkd_currency"]), Utility.ToDecimal(dr["car_bkd_exch_rate"]), Utility.ToDecimal(dr["car_bkd_rate"]), Utility.ToDecimal(dr["car_bkd_cost_rate"]), Utility.ToString(dr["car_bkd_markup_method"]),
                //                 Utility.ToDecimal(dr["car_bkd_markup_value"]), Utility.ToDecimal(dr["car_bkd_discount"]), Utility.ToDecimal(dr["car_bkd_total"]), Utility.ToString(dr["car_bkd_sell_currency"]), Utility.ToDecimal(dr["car_bkd_sell_exch_rate"]), Utility.ToDecimal(dr["car_bkd_sell_rate"]), Utility.ToString(dr["car_bkd_narration"]),
                //                 Utility.ToString(dr["car_bkd_inclusive_information"]), Utility.ToString(dr["car_bkd_service_desc"]), Utility.ToString(dr["car_bkd_confirm_no"]), Utility.ToString(dr["car_bkd_SV_generateYN"]), Utility.ToString(dr["car_bkd_status"]), Utility.ToLong(dr["car_bkd_created_by"]));

                //        }
                      
                //    }
                //}

                //Other Details

                //if (_dtOtherDetails != null)
                //{
                //    DataTable dt = _dtOtherDetails.GetChanges();
                //    int recordStatus = 0;
                //    if (dt != null && dt.Rows.Count > 0)
                //    {

                //        foreach (DataRow dr in dt.Rows)
                //        {
                //            switch (dr.RowState)
                //            {
                //                case DataRowState.Added: recordStatus = 1;
                //                    break;
                //                case DataRowState.Modified: recordStatus = 2;
                //                    break;
                //                case DataRowState.Deleted: recordStatus = -1;
                //                    dr.RejectChanges();
                //                    break;
                //                default: break;


                //            }
                //            SaveOtherDeatils(cmd, recordStatus, _retBookingId, Utility.ToLong(dr["oth_bkd_id"]), Utility.ToString(dr["oth_bkd_city"]),Utility.ToString(dr["oth_bkd_tour_operator"]),Utility.ToString(dr["oth_bkd_supplier"]),Utility.ToString(dr["oth_bkd_currency"]),
                //                 Utility.ToDecimal(dr["oth_bkd_exch_rate"]),Utility.ToDecimal(dr["oth_bkd_rate"]), Utility.ToDecimal(dr["oth_bkd_cost_rate"]), Utility.ToString(dr["oth_bkd_markup_method"]),Utility.ToDecimal(dr["oth_bkd_markup_value"]),
                //                 Utility.ToDecimal(dr["oth_bkd_discount"]), Utility.ToDecimal(dr["oth_bkd_total"]), Utility.ToString(dr["oth_bkd_sell_currency"]), Utility.ToDecimal(dr["oth_bkd_sell_exch_rate"]), Utility.ToDecimal(dr["oth_bkd_sell_rate"]), Utility.ToString(dr["oth_bkd_narration"]),
                //                 Utility.ToString(dr["oth_bkd_inclusive_information"]), Utility.ToString(dr["oth_bkd_service_desc"]), Utility.ToString(dr["oth_bkd_confirm_no"]), Utility.ToString(dr["oth_bkd_SV_generateYN"]), Utility.ToString(dr["oth_bkd_status"]), Utility.ToLong(dr["oth_bkd_created_by"]));

                //        }
                     
                //    }
                //}

                trans.Commit();
            }
            catch
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public void SavePaxDeatils(SqlCommand cmd, int recordStatus, long bookHeaderId,long bookPaxId, string paxName, string paxSex, string bookingType,long nationality, string passport,string airport,DateTime arrivalDate,DateTime departDate,
            long createdBy, string status)
        {

            try
            {

                SqlParameter[] paramArr = new SqlParameter[15];
                paramArr[0] = new SqlParameter("@P_pax_bkd_id", bookPaxId);
                paramArr[1] = new SqlParameter("@P_pax_bkd_bkh_id", bookHeaderId);
                paramArr[2] = new SqlParameter("@P_pax_bkd_name", paxName);
                paramArr[3] = new SqlParameter("@P_pax_bkd_sex", paxSex);
                paramArr[4] = new SqlParameter("@P_pax_bkd_type", bookingType );              
                paramArr[5] = new SqlParameter("@P_pax_bkd_nationality", nationality );
                paramArr[6] = new SqlParameter("@P_pax_passport_no", passport );
                paramArr[7] = new SqlParameter("@P_pax_bkd_airpoprt", airport);                
               if (arrivalDate!= DateTime.MinValue ) paramArr[8] = new SqlParameter("@P_pax_bkd_arrival_date", arrivalDate );
               if (departDate != DateTime.MinValue) paramArr[9] = new SqlParameter("@P_pax_bkd_depart_date", departDate);
                paramArr[10] = new SqlParameter("@P_pax_bkd_status",status);
                //paramArr[11] = new SqlParameter("@P_pax_bkd_created_by", createdBy);
                paramArr[11] = new SqlParameter("@P_pax_bkd_created_by", Settings.LoginInfo.UserID);
                paramArr[12] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
              
                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[13] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[14] = paramMsgText;

                DBGateway.ExecuteNonQueryDetails(cmd, "CT_P_TOUR_BOOKING_PAX_DETAILS_ADD_UPDATE", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));


            }
            catch
            {
                throw;
            }
        }

        public static string SaveHotelDeatils( int recordStatus, long bookHeaderId, long bookHotelId,long countryId, long  city, DateTime fromDate,
            DateTime todate, string service,long unit,long noOfRooms,string category, long tourOperator,long hotel, long rateType,
            long mealPlan, long roomType, string currency,  decimal ExchRate, decimal rate, decimal costRate,string markupMethod,decimal markupValue,string gsaCommType,decimal gsaCommission,
            decimal discount, decimal sellingTotal, decimal total, decimal baseTotal, decimal baseCostRate, decimal baseMarkupValue, decimal baseSellingPrice, decimal baseDiscount, 
            string sellCurrency, decimal sellExchrate, decimal sellRate, string narration, string inclInformation, string serviceDesc, string confirmNo,
           string SVgeneratedYN,  string status,long createdBy )
        {

            try
            {

                SqlParameter[] paramArr = new SqlParameter[45];
                paramArr[0] = new SqlParameter("@P_htl_bkd_id", bookHotelId );
                paramArr[1] = new SqlParameter("@P_htl_bkd_bkh_id", bookHeaderId);
                paramArr[2] = new SqlParameter("@P_htl_bkd_city", city );
                paramArr[3] = new SqlParameter("@P_htl_bkd_from_date", fromDate );
                paramArr[4] = new SqlParameter("@P_htl_bkd_to_date", todate );
                paramArr[5] = new SqlParameter("@P_htl_bkd_service", service );
                paramArr[6] = new SqlParameter("@P_htl_bkd_unit", unit);
                paramArr[7] = new SqlParameter("@P_htl_bkd_category", category);
                paramArr[8] = new SqlParameter("@P_htl_bkd_tour_operator", tourOperator);
                paramArr[9] = new SqlParameter("@P_htl_bkd_hotel", hotel ) ;
                paramArr[10] = new SqlParameter("@P_htl_bkd_rate_type", rateType);

                paramArr[11] = new SqlParameter("@P_htl_bkd_meal_plan", mealPlan);
                paramArr[12] = new SqlParameter("@P_htl_bkd_currency", currency );
                paramArr[13] = new SqlParameter("@P_htl_bkd_exch_rate", ExchRate);
                paramArr[14] = new SqlParameter("@P_htl_bkd_rate", rate );
                paramArr[15] = new SqlParameter("@P_htl_bkd_cost_rate", costRate );
                paramArr[16] = new SqlParameter("@P_htl_bkd_markup_method", markupMethod );
                paramArr[17] = new SqlParameter("@P_htl_bkd_markup_value", markupValue );
                paramArr[18] = new SqlParameter("@P_htl_bkd_discount", discount );
                paramArr[19] = new SqlParameter("@P_htl_bkd_sell_currency", sellCurrency );
                paramArr[20] = new SqlParameter("@P_htl_bkd_sell_exch_rate", sellExchrate );

                paramArr[21] = new SqlParameter("@P_htl_bkd_sell_rate", sellRate );
                paramArr[22] = new SqlParameter("@P_htl_bkd_narration", narration );
                paramArr[23] = new SqlParameter("@P_htl_bkd_inclusive_information", inclInformation );
                paramArr[24] = new SqlParameter("@P_htl_bkd_service_desc",serviceDesc );
                paramArr[25] = new SqlParameter("@P_htl_bkd_confirm_no", confirmNo );
                paramArr[26] = new SqlParameter("@P_htl_bkd_SV_generateYN", SVgeneratedYN);
                paramArr[27] = new SqlParameter("@P_htl_bkd_status", status );
//                paramArr[28] = new SqlParameter("@P_htl_bkd_created_by", createdBy );                            
                paramArr[28] = new SqlParameter("@P_htl_bkd_created_by", Settings.LoginInfo.UserID);
                paramArr[29] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                paramArr[30] = new SqlParameter("@P_htl_bkd_total", total);
                
                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[31] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[32] = paramMsgText;

                paramArr[33] = new SqlParameter("@P_htl_bkd_voucher_no", SqlDbType.NVarChar);
                //SqlParameter paramMsgText = new SqlParameter("@P_car_bkd_voucher_no", SqlDbType.NVarChar);
                paramArr[33].Size = 50;
                paramArr[33].Direction = ParameterDirection.Output;

                paramArr[34] = new SqlParameter("@P_htl_bkd_room_type", roomType);
                paramArr[35] = new SqlParameter("@P_htl_bkd_GSA_comm_type", gsaCommType);
                paramArr[36] = new SqlParameter("@P_htl_bkd_GSA_commission", gsaCommission);
                paramArr[37] = new SqlParameter("@P_htl_bkd_country_id", countryId);
                paramArr[38] = new SqlParameter("@P_htl_bkd_no_of_rooms", noOfRooms );
                paramArr[39] = new SqlParameter("@P_htl_bkd_selling_total", sellingTotal);
                paramArr[40] = new SqlParameter("@P_htl_bkd_base_total",  baseTotal);

                paramArr[41] = new SqlParameter("@P_htl_bkd_base_cost_rate", baseCostRate);
                paramArr[42] = new SqlParameter("@P_htl_bkd_base_markup_value", baseMarkupValue);
                paramArr[43] = new SqlParameter("@P_htl_bkd_base_discount", baseDiscount);
                paramArr[44] = new SqlParameter("@P_htl_bkd_base_selling_total", baseSellingPrice);
                
                DBGateway.ExecuteNonQuery( "CT_P_TOUR_BOOKING_HOTEL_DETAILS_ADD_UPDATE", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")  
                        throw new Exception(Utility.ToString(paramMsgText.Value));

                    string _hotelVoucherNo = Utility.ToString(paramArr[33].Value);
                    if (recordStatus > 0)
                    {
                        if (string.IsNullOrEmpty(_hotelVoucherNo) && Utility.ToString(paramArr[25].Value) != string.Empty) throw new Exception(Utility.ToString(paramMsgText.Value));
                    }
                    return _hotelVoucherNo;              
            }
            catch
            {
                throw;
            }
        }


        public static string  SaveCarDeatils(int recordStatus, long bookHeaderId, long bookHotelId,long countryId, long city, DateTime fromDate,
           DateTime todate, long pickUp, long unit, long  DropOff, long  tourOperator, long car, long rateType,
           string transport, string currency, decimal ExchRate, decimal rate, decimal costRate, string markupMethod, decimal markupValue, string gsaCommType, decimal gsaCommission,
            decimal discount, decimal sellingTotal, decimal total, decimal baseTotal, decimal baseCostRate, decimal baseMarkupValue, decimal baseSellingPrice, decimal baseDiscount,
            string sellCurrency, decimal sellExchrate, decimal sellRate, string narration, string inclInformation, string serviceDesc, string confirmNo,
          string SVgeneratedYN, string status, long createdBy)
        {

            try
            {

                SqlParameter[] paramArr = new SqlParameter[43];
                paramArr[0] = new SqlParameter("@P_car_bkd_id", bookHotelId);
                paramArr[1] = new SqlParameter("@P_car_bkd_bkh_id", bookHeaderId);
                paramArr[2] = new SqlParameter("@P_car_bkd_city", city);
                paramArr[3] = new SqlParameter("@P_car_bkd_from_date", fromDate);
                paramArr[4] = new SqlParameter("@P_car_bkd_to_date", todate);
                paramArr[5] = new SqlParameter("@P_car_bkd_service", pickUp);
                paramArr[6] = new SqlParameter("@P_car_bkd_unit", unit);
                paramArr[7] = new SqlParameter("@P_car_bkd_category", DropOff);
                paramArr[8] = new SqlParameter("@P_car_bkd_tour_operator", tourOperator);
                paramArr[9] = new SqlParameter("@P_car_bkd_car", car);
                paramArr[10] = new SqlParameter("@P_car_bkd_rate_type", rateType);

                paramArr[11] = new SqlParameter("@P_car_bkd_transport", transport);
                paramArr[12] = new SqlParameter("@P_car_bkd_currency", currency);
                paramArr[13] = new SqlParameter("@P_car_bkd_exch_rate", ExchRate );
                paramArr[14] = new SqlParameter("@P_car_bkd_rate", rate);
                paramArr[15] = new SqlParameter("@P_car_bkd_cost_rate", costRate);
                paramArr[16] = new SqlParameter("@P_car_bkd_markup_method", markupMethod);
                paramArr[17] = new SqlParameter("@P_car_bkd_markup_value", markupValue);
                paramArr[18] = new SqlParameter("@P_car_bkd_discount", discount);
                paramArr[19] = new SqlParameter("@P_car_bkd_sell_currency", sellCurrency);
                paramArr[20] = new SqlParameter("@P_car_bkd_sell_exch_rate", sellExchrate);

                paramArr[21] = new SqlParameter("@P_car_bkd_sell_rate", sellRate);
                paramArr[22] = new SqlParameter("@P_car_bkd_narration", narration);
                paramArr[23] = new SqlParameter("@P_car_bkd_inclusive_information", inclInformation);
                paramArr[24] = new SqlParameter("@P_car_bkd_service_desc", serviceDesc);
                paramArr[25] = new SqlParameter("@P_car_bkd_confirm_no", confirmNo);
                paramArr[26] = new SqlParameter("@P_car_bkd_SV_generateYN", SVgeneratedYN);
                paramArr[27] = new SqlParameter("@P_car_bkd_status", status);
                //paramArr[28] = new SqlParameter("@P_car_bkd_created_by", createdBy);
                paramArr[28] = new SqlParameter("@P_car_bkd_created_by", Settings.LoginInfo.UserID);

                paramArr[29] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                paramArr[30] = new SqlParameter("@P_car_bkd_total", total);

                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[31] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[32] = paramMsgText;

                paramArr[33] = new SqlParameter("@P_car_bkd_voucher_no",  SqlDbType.NVarChar);
                //SqlParameter paramMsgText = new SqlParameter("@P_car_bkd_voucher_no", SqlDbType.NVarChar);
                paramArr[33].Size  = 50;
                paramArr[33].Direction  = ParameterDirection.Output;

                paramArr[34] = new SqlParameter("@P_car_bkd_GSA_comm_type", gsaCommType);
                paramArr[35] = new SqlParameter("@P_car_bkd_GSA_commission", gsaCommission);
                paramArr[36] = new SqlParameter("@P_car_bkd_country_id", countryId );
                paramArr[37] = new SqlParameter("@P_car_bkd_selling_total", sellingTotal );
                paramArr[38] = new SqlParameter("@P_car_bkd_base_total", baseTotal);

                paramArr[39] = new SqlParameter("@P_car_bkd_base_cost_rate", baseCostRate);
                paramArr[40] = new SqlParameter("@P_car_bkd_base_markup_value", baseMarkupValue);
                paramArr[41] = new SqlParameter("@P_car_bkd_base_discount", baseDiscount);
                paramArr[42] = new SqlParameter("@P_car_bkd_base_selling_total", baseSellingPrice);
                DBGateway.ExecuteNonQuery("CT_P_TOUR_BOOKING_CAR_RENTAL_DETAILS_ADD_UPDATE", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));

               string _carVoucherNo    = Utility.ToString(paramArr[33].Value);                
                if (string.IsNullOrEmpty(_carVoucherNo)&& Utility.ToString(paramArr[25].Value)!=string.Empty) throw new Exception(Utility.ToString(paramMsgText.Value));

                return _carVoucherNo;
            }
            catch
            {
                throw;
            }
        }

        public static  string  SaveOtherDeatils(int recordStatus, long bookHeaderId, long bookHotelId, long countryId,long city,long unit, long  tourOperator, long supplier,
            string currency, decimal ExchRate, decimal rate, decimal costRate, string markupMethod, decimal markupValue, string gsaCommType, decimal gsaCommission, 
            decimal discount,decimal sellingTotal, decimal total,decimal baseTotal,decimal baseCostRate,decimal baseMarkupValue,decimal baseSellingPrice,decimal baseDiscount,
            string sellCurrency, decimal sellExchrate, decimal sellRate, string narration, string inclInformation, string serviceDesc, 
            string confirmNo, string SVgeneratedYN, string status, long createdBy, long category, DataTable  dtCategoryDetails)
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;

            try
            {


                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction  = trans;

                SqlParameter[] paramArr = new SqlParameter[39];
                paramArr[0] = new SqlParameter("@P_oth_bkd_id", bookHotelId);
                paramArr[1] = new SqlParameter("@P_oth_bkd_bkh_id", bookHeaderId);
                paramArr[2] = new SqlParameter("@P_oth_bkd_city", city);                              
                paramArr[3] = new SqlParameter("@P_oth_bkd_supplier", supplier);
                paramArr[4] = new SqlParameter("@P_oth_bkd_tour_operator", tourOperator);    
                paramArr[5] = new SqlParameter("@P_oth_bkd_currency", currency);
                paramArr[6] = new SqlParameter("@P_oth_bkd_exch_rate", ExchRate);
                paramArr[7] = new SqlParameter("@P_oth_bkd_rate", rate);
                paramArr[8] = new SqlParameter("@P_oth_bkd_cost_rate", costRate);
                paramArr[9] = new SqlParameter("@P_oth_bkd_markup_method", markupMethod);
                
                paramArr[10] = new SqlParameter("@P_oth_bkd_markup_value", markupValue);
                paramArr[11] = new SqlParameter("@P_oth_bkd_discount", discount);
                paramArr[12] = new SqlParameter("@P_oth_bkd_sell_currency", sellCurrency);
                paramArr[13] = new SqlParameter("@P_oth_bkd_sell_exch_rate", sellExchrate);
                paramArr[14] = new SqlParameter("@P_oth_bkd_sell_rate", sellRate);
                paramArr[15] = new SqlParameter("@P_oth_bkd_narration", narration);
                paramArr[16] = new SqlParameter("@P_oth_bkd_inclusive_information", inclInformation);
                paramArr[17] = new SqlParameter("@P_oth_bkd_service_desc", serviceDesc);
                paramArr[18] = new SqlParameter("@P_oth_bkd_confirm_no", confirmNo);
                paramArr[19] = new SqlParameter("@P_oth_bkd_SV_generateYN", SVgeneratedYN);
                paramArr[20] = new SqlParameter("@P_oth_bkd_status", status);

                //paramArr[21] = new SqlParameter("@P_oth_bkd_created_by", createdBy);
                paramArr[21] = new SqlParameter("@P_oth_bkd_created_by", Settings.LoginInfo.UserID);
                paramArr[22] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                paramArr[23] = new SqlParameter("@P_oth_bkd_total", total );

                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[24] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[25] = paramMsgText;


                paramArr[26] = new SqlParameter("@P_oth_bkd_voucher_no", SqlDbType.NVarChar);
                paramArr[26].Size = 50;
                paramArr[26].Direction = ParameterDirection.Output;

                paramArr[27] = new SqlParameter("@P_oth_bkd_unit", unit);
                paramArr[28] = new SqlParameter("@P_oth_bkd_GSA_comm_type", gsaCommType );
                paramArr[29] = new SqlParameter("@P_oth_bkd_GSA_commission", gsaCommission );
                paramArr[30] = new SqlParameter("@P_oth_bkd_category", category);

                paramArr[31] = new SqlParameter("@P_oth_bkd_ret_id", SqlDbType.BigInt);
                paramArr[31].Size = 20;
                paramArr[31].Direction = ParameterDirection.Output;
                paramArr[32] = new SqlParameter("@P_oth_bkd_country_id", countryId);
                paramArr[33] = new SqlParameter("@P_oth_bkd_selling_total", sellingTotal);
                paramArr[34] = new SqlParameter("@P_oth_bkd_base_total", baseTotal);
                paramArr[35] = new SqlParameter("@P_oth_bkd_base_cost_rate", baseCostRate );
                paramArr[36] = new SqlParameter("@P_oth_bkd_base_markup_value", baseMarkupValue );
                paramArr[37] = new SqlParameter("@P_oth_bkd_base_discount", baseDiscount );
                paramArr[38] = new SqlParameter("@P_oth_bkd_base_selling_total", baseSellingPrice );

                DBGateway.ExecuteNonQueryDetails(cmd ,"CT_P_TOUR_BOOKING_OTHERS_DETAILS_ADD_UPDATE", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));


                string _otherVoucherNo = Utility.ToString(paramArr[26].Value);
                long otherSvcId = Utility.ToLong(paramArr[31].Value);
                if (recordStatus>0)
                if (string.IsNullOrEmpty(_otherVoucherNo) && Utility.ToString(paramArr[18].Value)!=string.Empty) throw new Exception(Utility.ToString(paramMsgText.Value));

               if (dtCategoryDetails != null && dtCategoryDetails.Rows.Count>0)
            {
                //DataTable dt = dtCategoryDetails.GetChanges();
                //int recordStatus = 0;
                //if (dt != null && dt.Rows.Count > 0)
                //{

                    //try
                    //{
                    foreach (DataRow dr in dtCategoryDetails.Rows)
                    {
                        //switch (dr.RowState)
                        //{
                        //    case DataRowState.Added: recordStatus = 1;
                        //        break;
                        //    case DataRowState.Modified: recordStatus = 2;
                        //        break;
                        //    case DataRowState.Deleted: recordStatus = -1;
                        //        break;
                        //    default: break;


                        //}
                        //only insertion
                        SaveCategoryDetails(cmd, recordStatus, category, otherSvcId, bookHeaderId,
                                Utility.ToString(dr["catd_vflex_1_label"]), Utility.ToString(dr["catd_vflex_1_value"]), Utility.ToString(dr["catd_vflex_1_desc"]),
                                Utility.ToString(dr["catd_vflex_2_label"]), Utility.ToString(dr["catd_vflex_2_value"]), Utility.ToString(dr["catd_vflex_2_desc"]),
                                Utility.ToString(dr["catd_vflex_3_label"]), Utility.ToString(dr["catd_vflex_3_value"]), Utility.ToString(dr["catd_vflex_3_desc"]),
                                Utility.ToString(dr["catd_vflex_4_label"]), Utility.ToString(dr["catd_vflex_4_value"]), Utility.ToString(dr["catd_vflex_4_desc"]),
                                Utility.ToString(dr["catd_vflex_5_label"]), Utility.ToString(dr["catd_vflex_5_value"]), Utility.ToString(dr["catd_vflex_5_desc"]),
                                Utility.ToString(dr["catd_vflex_6_label"]), Utility.ToString(dr["catd_vflex_6_value"]), Utility.ToString(dr["catd_vflex_6_desc"]),
                                Utility.ToString(dr["catd_vflex_7_label"]), Utility.ToString(dr["catd_vflex_7_value"]), Utility.ToString(dr["catd_vflex_7_desc"]),
                                Utility.ToString(dr["catd_vflex_8_label"]), Utility.ToString(dr["catd_vflex_8_value"]), Utility.ToString(dr["catd_vflex_8_desc"]),
                                recordStatus == 1 ? "A" : "D", createdBy);

                     }
                   //}
                }
                trans.Commit();
                return _otherVoucherNo;
            }
            catch
            {
                trans.Rollback();
                throw;
            }
        }

        public static void SaveCategoryDetails(SqlCommand cmd,long catId,long categoryId,long otherSvcId,long BookingId,string label1,string value1,string desc1,
            string label2,string value2,string desc2,string label3,string value3,string desc3,string label4,string value4,string desc4,string label5,string value5,string desc5,
            string label6, string value6, string desc6, string label7, string value7, string desc7, string label8, string value8, string desc8, string recordStatus, long createdBy)
        {
            try
            {
                SqlParameter[] paramArr = new SqlParameter[32];
                paramArr[0] = new SqlParameter("@P_catd_id", catId);
                paramArr[1] = new SqlParameter("@P_catd_cat_id", categoryId);
                paramArr[2] = new SqlParameter("@P_catd_oth_id", otherSvcId);
                paramArr[3] = new SqlParameter("@P_catd_bkh_id", BookingId);
                paramArr[4] = new SqlParameter("@P_catd_vflex_1_label", label1);
                paramArr[5] = new SqlParameter("@P_catd_vflex_1_value", value1 );
                paramArr[6] = new SqlParameter("@P_catd_vflex_1_desc", desc1 );
                paramArr[7] = new SqlParameter("@P_catd_vflex_2_label", label2);
                paramArr[8] = new SqlParameter("@P_catd_vflex_2_value", value2 );
                paramArr[9] = new SqlParameter("@P_catd_vflex_2_desc", desc2 );
                paramArr[10] = new SqlParameter("@P_catd_vflex_3_label", label3);
                paramArr[11] = new SqlParameter("@P_catd_vflex_3_value", value3);
                paramArr[12] = new SqlParameter("@P_catd_vflex_3_desc", desc3);
                paramArr[13] = new SqlParameter("@P_catd_vflex_4_label", label4);
                paramArr[14] = new SqlParameter("@P_catd_vflex_4_value", value4);
                paramArr[15] = new SqlParameter("@P_catd_vflex_4_desc", desc4);
                paramArr[16] = new SqlParameter("@P_catd_vflex_5_label", label5);
                paramArr[17] = new SqlParameter("@P_catd_vflex_5_value", value5);
                paramArr[18] = new SqlParameter("@P_catd_vflex_5_desc", desc5);
                paramArr[19] = new SqlParameter("@P_catd_vflex_6_label", label6);
                paramArr[20] = new SqlParameter("@P_catd_vflex_6_value", value6);
                paramArr[21] = new SqlParameter("@P_catd_vflex_6_desc", desc6);
                paramArr[22] = new SqlParameter("@P_catd_vflex_7_label", label7);
                paramArr[23] = new SqlParameter("@P_catd_vflex_7_value", value7);
                paramArr[24] = new SqlParameter("@P_catd_vflex_7_desc", desc7);
                paramArr[25] = new SqlParameter("@P_catd_vflex_8_label", label8);
                paramArr[26] = new SqlParameter("@P_catd_vflex_8_value", value8);
                paramArr[27] = new SqlParameter("@P_catd_vflex_8_desc", desc8 );
                paramArr[28] = new SqlParameter("@P_catd_status", recordStatus);
                paramArr[29] = new SqlParameter("@P_catd_created_by", createdBy);                

                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[30] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[31] = paramMsgText;

                DBGateway.ExecuteNonQueryDetails(cmd, "TOUR_SERVICE_CATEGORY_DETAILS_ADD_UPDATE", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));

            }

            catch { throw; } 
        
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LOCATION_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_location_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Static Methods
      

        public static DataTable GetList(DateTime fromDate, DateTime toDate, string BookingStatus,RecordStatus  status, long userId, string memberType, long locationId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[7];

                if (fromDate != DateTime.MinValue) paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                if (toDate != DateTime.MinValue) paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                if (!string.IsNullOrEmpty(BookingStatus)) paramList[2] = new SqlParameter("@P_BKH_BOOKING_STATUS",BookingStatus);
                if (status != RecordStatus.All) paramList[3] = new SqlParameter("@P_BKH_STATUS", Utility.ToString((char)status ));
                paramList[4] = new SqlParameter("@P_USER_ID", userId);
                paramList[5] = new SqlParameter("@P_MEMBER_TYPE", memberType);
                paramList[6] = new SqlParameter("@P_BKH_LOCATION_ID", locationId);

                return DBGateway.ExecuteQuery("CT_P_TOUR_BOOKING_HEADER_GET_LIST", paramList).Tables[0];
            }
            catch { throw; }
        }


        public static DataTable GetSupplierList(string SupplierType,string service,long country,ListStatus status,RecordStatus recStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[5];

                paramList[0] = new SqlParameter("@P_SUPP_TYPE", SupplierType);
                paramList[1] = new SqlParameter("@P_SUPP_SERVICE", service);
                if(country>=0) paramList[2] = new SqlParameter("@P_SUPP_COUNTRY", country  );
                paramList[3] = new SqlParameter("@P_LIST_STATUS", status);
                if (recStatus != RecordStatus.All) paramList[4] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recStatus ));
                return DBGateway.ExecuteQuery("CT_P_SUPPLIER_MASTER_GETLIST", paramList).Tables[0];
            }
            catch { throw; }
        }

        public static DataTable GetCarList(string carType, ListStatus status, RecordStatus recStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];

                if(!string.IsNullOrEmpty(carType)) paramList[0] = new SqlParameter("@P_TCAR_TYPE", carType);               
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                if (recStatus != RecordStatus.All) paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recStatus));
                return DBGateway.ExecuteQuery("tour_car_getlist", paramList).Tables[0];
            }
            catch { throw; }
        }

        public static DataTable GetTourCommonServiceTypesList(string serviceType, ListStatus status, RecordStatus recStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];

                if (!string.IsNullOrEmpty(serviceType)) paramList[0] = new SqlParameter("@P_TSVC_TYPE", serviceType);
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                if (recStatus != RecordStatus.All) paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recStatus));
                return DBGateway.ExecuteQuery("TOUR_SERVICE_COMMON_TYPES_GETLIST", paramList).Tables[0];
            }
            catch { throw; }
        }

        public static DataTable GetTourCategoryConfigSetup(long  categoryId, ListStatus status)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];

                paramList[0] = new SqlParameter("@P_config_cat_id", categoryId );
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                return DBGateway.ExecuteQuery("TOUR_SERVICE_CATEGORY_CONFIG_SETUP_GETDATA", paramList).Tables[0];
            }
            catch { throw; }
        }

        public static DataTable GetTourServiceCategoryGetData(long catDetailId,long otherSvcId, ListStatus status)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];

                paramList[0] = new SqlParameter("@P_catd_id", catDetailId);
                if (otherSvcId > 0) paramList[1] = new SqlParameter("@P_catd_oth_id", otherSvcId);
                //paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                return DBGateway.ExecuteQuery("TOUR_SERVICE_CATEGORY_DETAILS_GETDATA", paramList).Tables[0];
            }
            catch { throw; }
        }

        public static DataTable GetTourServiceCategoryList(long catDetailId, long otherSvcId, long categoryId,long bookId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[4];

                if (catDetailId > 0) paramList[0] = new SqlParameter("@P_catd_id", catDetailId);
                if (otherSvcId > 0) paramList[1] = new SqlParameter("@P_catd_oth_id", otherSvcId);
                if (categoryId > 0) paramList[2] = new SqlParameter("@P_catd_cat_id", categoryId);
                if (bookId > 0) paramList[3] = new SqlParameter("@P_catd_bkh_id", bookId);
                //paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                return DBGateway.ExecuteQuery("TOUR_SERVICE_CATEGORY_LIST", paramList).Tables[0];
            }
            catch { throw; }
        }
        public  static DataSet GetTourBookingReportDetails(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_BKH_ID", id);
                paramList[1] = new SqlParameter("@P_BKH_LOCATION_ID", Settings.LoginInfo.LocationID);
                DataSet dsResult = DBGateway.ExecuteQuery("TOUR_BOOKING_CONFIRM_VOUCHER_GETDETAILS", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        public static DataSet GetTourBookingCostSheet(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_BKH_ID", id);
                paramList[1] = new SqlParameter("@P_BKH_LOCATION_ID", Settings.LoginInfo.LocationID);
                //DataSet dsResult = DBGateway.ExecuteQuery("TOUR_BOOKING_CONFIRM_VOUCHER_GETDETAILS", paramList);
                DataSet dsResult = DBGateway.ExecuteQuery("TOUR_BOOKING_CONFIRM_VOUCHER_GETDETAILS", paramList);
                
                return dsResult;
            }
            catch
            {
                throw;
            }
        }


        public static DataTable GetTourCityList(int countryId, ListStatus status, RecordStatus recordStatus)
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[3];

                if (countryId >= 0) paramList[0] = new SqlParameter("@P_COUNTRY_ID", countryId);
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("TOUR_BOOKING_CITY_GETLIST", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }


        public static DataTable GetTourExecutiveList(int regionId, ListStatus status, RecordStatus recordStatus)
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[3];

                if (regionId > 0) paramList[0] = new SqlParameter("@P_TEX_REGION_ID", regionId);
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("TOUR_EXECUTIVE_GETLIST", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetTourRegionList( ListStatus status, RecordStatus recordStatus)
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[2];
                
                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("TOUR_REGION_GETLIST", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataSet  GetTourBookingFinanceSalesDetails(DateTime fromDate,DateTime toDate,long locationId,long userId ,long regionId)
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[5];
                if (fromDate != DateTime.MinValue) paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                if (toDate != DateTime.MinValue) paramList[1] = new SqlParameter("@P_TO_DATE", toDate );
                if (locationId > 0) paramList[2] = new SqlParameter("@P_LOCATION_ID", locationId);
                if (userId > 0) paramList[3] = new SqlParameter("@P_USER_ID", userId);
                if (regionId > 0) paramList[4] = new SqlParameter("@P_REGION_ID", regionId);
                return DBGateway.ExecuteQuery("TOUR_BOOKING_FINANCE_SALES_DETAILS", paramList);
            }
            catch
            {
                throw;
            }
        }
        public static DataSet GetTourDestinationDetails(DateTime fromDate, DateTime toDate, long locationId, long userId, long regionId, long countryId,long cityId, long hotelId,long tourOperatorId,long customerId)
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[10];
                if (fromDate != DateTime.MinValue) paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                if (toDate != DateTime.MinValue) paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                if (locationId > 0) paramList[2] = new SqlParameter("@P_LOCATION_ID", locationId);
                if (userId > 0) paramList[3] = new SqlParameter("@P_USER_ID", userId);
                if (regionId > 0) paramList[4] = new SqlParameter("@P_REGION_ID", regionId);
                if (countryId  > 0) paramList[5] = new SqlParameter("@P_COUNTRY_ID", countryId );
                if (cityId > 0) paramList[6] = new SqlParameter("@P_CITY_ID", cityId );
                if (hotelId > 0) paramList[7] = new SqlParameter("@P_HOTEL_ID ", hotelId );
                if (tourOperatorId > 0) paramList[8] = new SqlParameter("@P_TOUR_OPERATOR_ID", tourOperatorId );
                if (customerId > 0) paramList[9] = new SqlParameter("@P_CUSTOMER_ID", customerId);

                return DBGateway.ExecuteQuery("TOUR_BOOKING_DESTINATION_DETAILS", paramList);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        public static DataTable ExecuteListQuery(string queryString)
        {
            try
            {

                return DBGateway.ExecuteQuery(queryString).Tables[0];

            }
            catch
            {
                throw;

            }
        }
    }


}
