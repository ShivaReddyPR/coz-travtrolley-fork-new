﻿using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.TicketReceipt.BusinessLayer
{
   public class SettlementDetailes
    {
        int serviceProductId;
        Int32 serviceReferenceId;
        string settlementType;
        decimal amount;
        string currency;
        string fopDetails1 = string.Empty;
        string fopDetails2 = string.Empty;
        string fopDetails3 = string.Empty;
        string fopDetails4 = string.Empty;
        string fopDetails5 = string.Empty;
        string fopDetails6 = string.Empty;
        string fopDetails7 = string.Empty;
        string fopDetails8 = string.Empty;
        string fopDetailRemarks = string.Empty;
        int status,createdBy,updatedBy;
        DateTime createdOn, updatedOn;
        int fopId = 0;
        public int FopId
        {
            get { return fopId; }
            set { fopId = value; }
        }
        public int ServiceProductId
        {
            get { return serviceProductId; }
            set { serviceProductId = value; }
        }
        public Int32 ServiceReferenceId
        {
            get { return serviceReferenceId; }
            set { serviceReferenceId = value; }
        }
        public string SettlementType
        {
            get { return settlementType; }
            set { settlementType = value; }
        }
        public decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        public string FOPDetail1
        {
            get { return fopDetails1; }
            set { fopDetails1 = value; }
        }
        public string FOPDetail2
        {
            get { return fopDetails2; }
            set { fopDetails2 = value; }
        }
        public string FOPDetail3
        {
            get { return fopDetails3; }
            set { fopDetails3 = value; }
        }
        public string FOPDetail4
        {
            get { return fopDetails4; }
            set { fopDetails4 = value; }
        }
        public string FOPDetail5
        {
            get { return fopDetails5; }
            set { fopDetails5 = value; }
        }
        public string FOPDetail6
        {
            get { return fopDetails6; }
            set { fopDetails6 = value; }
        }
        public string FOPDetail7
        {
            get { return fopDetails7; }
            set { fopDetails7 = value; }
        }
        public string FOPDetail8
        {
            get { return fopDetails8; }
            set { fopDetails8 = value; }
        }
        public string FOPDetailRemarks
        {
            get { return fopDetailRemarks; }
            set { fopDetailRemarks = value; }
        }
        public int Status
        {
            get { return status; }
            set { status = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }
        public int UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }
        public DateTime UpdatedOn
        {
            get { return updatedOn; }
            set { updatedOn = value; }
        }

        public void Save()
        {
            //Trace.TraceInformation("TransferVehicle.Save entered.");
            SqlParameter[] paramList = new SqlParameter[17];
            paramList[0] = new SqlParameter("@ServiceProductId", serviceProductId);
            paramList[1] = new SqlParameter("@ServiceReferenceId", serviceReferenceId);
            paramList[2] = new SqlParameter("@SettlementType", settlementType);
            paramList[3] = new SqlParameter("@Amount", amount);
            paramList[4] = new SqlParameter("@Currency", currency);
            paramList[5] = new SqlParameter("@FOPdetail1", fopDetails1.Equals(string.Empty)? (object)DBNull.Value : fopDetails1);
            paramList[6] = new SqlParameter("@FOPdetail2", fopDetails2.Equals(string.Empty) ? (object)DBNull.Value : fopDetails2);
            paramList[7] = new SqlParameter("@FOPdetail3", fopDetails3.Equals(string.Empty) ? (object)DBNull.Value : fopDetails3);           
            paramList[8] = new SqlParameter("@FOPdetail4", fopDetails4.Equals(string.Empty) ? (object)DBNull.Value : fopDetails4);
            paramList[9] = new SqlParameter("@FOPdetail5", fopDetails5.Equals(string.Empty) ? (object)DBNull.Value : fopDetails5);
            paramList[10] = new SqlParameter("@FOPdetail6", fopDetails6.Equals(string.Empty) ? (object)DBNull.Value : fopDetails6);
            paramList[11] = new SqlParameter("@FOPdetail7", fopDetails7.Equals(string.Empty) ? (object)DBNull.Value : fopDetails7);
            paramList[12] = new SqlParameter("@FOPdetail8", fopDetails8.Equals(string.Empty) ? (object)DBNull.Value : fopDetails8);
            paramList[13] = new SqlParameter("@FOPdetailremarks", fopDetailRemarks);
            paramList[14] = new SqlParameter("@Status", status);
            paramList[15] = new SqlParameter("@CreatedBy", createdBy);
            paramList[16] = new SqlParameter("@ModifiedBy", updatedBy);
            int rowsAffected = DBGateway.ExecuteNonQuerySP("BKE_SAVE_SETTLEMENTDETAILES", paramList);
            //Trace.TraceInformation("TransferVehicle.Save exiting");    
        }

        public void Deactivate()
        {
            //Trace.TraceInformation("TransferVehicle.Save entered.");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@PriceId", serviceReferenceId);           
            int rowsAffected = DBGateway.ExecuteNonQuerySP("USP_DEACTIVATE_SETTLEMENT", paramList);            
        }

        public List<SettlementDetailes> load(int priceId)
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@priceId", priceId);
            DataSet dtSettlemet = DBGateway.FillSP("GET_SETTLEMENT_DETAILS", paramList);
            List<SettlementDetailes> Settlements = new List<SettlementDetailes>();
            foreach (DataRow dr in dtSettlemet.Tables[0].Rows)
            {
                SettlementDetailes settlement = new SettlementDetailes();
                if(dr["amount"]!=DBNull.Value)
                {
                    settlement.Amount = decimal.Parse(dr["amount"].ToString());
                }
                if (dr["currency"] != DBNull.Value)
                {
                    settlement.Currency = dr["currency"].ToString();
                }
                if (dr["settlement_type"] != DBNull.Value)
                {
                    settlement.SettlementType = dr["settlement_type"].ToString();
                }
                if (dr["service_product_id"] != DBNull.Value)
                {
                    settlement.ServiceProductId = int.Parse(dr["service_product_id"].ToString());
                }
                if (dr["service_reference_id"] != DBNull.Value)
                {
                    settlement.ServiceReferenceId = int.Parse(dr["service_reference_id"].ToString());
                }                
                settlement.FOPDetail1 = dr["FOP_detail_1"].ToString();  
                settlement.FOPDetail2 = dr["FOP_detail_2"].ToString();
                settlement.FOPDetail3 = dr["FOP_detail_3"].ToString();
                settlement.FOPDetail4 = dr["FOP_detail_4"].ToString();
                settlement.FOPDetail5 = dr["FOP_detail_5"].ToString();
                settlement.FOPDetail6 = dr["FOP_detail_6"].ToString();
                settlement.FOPDetail7 = dr["FOP_detail_7"].ToString();
                settlement.FOPDetail8 = dr["FOP_detail_8"].ToString();
                settlement.FOPDetailRemarks = dr["FOP_detail_remarks"].ToString();
                settlement.FopId=int.Parse(dr["fop_id"].ToString());
                Settlements.Add(settlement);
            }
            
            return Settlements;
        }
    }

    public class SettlementData
    {
        public List<SettlementDetailes> SettlementObject { get; set; }
    }
}
