using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class HandlingFeeMaster
    {
        private const long NEW_RECORD = -1;
        #region Member Variables
        private long _id;
        private string _element;
        private double _fee;
        private bool _feeStatus;// D - Default, N - Normal
        private string _status;
        private int _agentId;
        private long _createdBy;

        #endregion

        #region Properties
        public long ID
        {
            get { return _id; }
        }
        public string Element
        {
            get { return _element; }
            set { _element = value; }
        }
        public double Fee
        {
            get { return _fee; }
            set { _fee = value; }
        }
        public bool FeeStatus
        {
            get { return _feeStatus; }
            set { _feeStatus = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        #endregion

        #region Constructors
        public HandlingFeeMaster()
        {
            _id = NEW_RECORD;
        }
        public HandlingFeeMaster(long id)
        {


            _id = id;
            getDetails(id);
        }
        #endregion
        #region Methods
        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_HANDLING_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_handling_fee_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _id = Utility.ToLong(dr["HANDLING_ID"]);
                _element = Utility.ToString(dr["HANDLING_ELEMENT"]);
                _fee = Utility.ToDouble(dr["HANDLING_FEE"]);
                _feeStatus = (Utility.ToString(dr["HANDLING_FEE_STATUS"]) == "D");
                _status = Utility.ToString(dr["HANDLING_STATUS"]);
                _createdBy = Utility.ToLong(dr["HANDLING_CREATED_BY"]);



            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[9];

                paramList[0] = new SqlParameter("@P_HANDLING_ID", _id);
                paramList[1] = new SqlParameter("@P_HANDLING_ELEMENT", _element);
                paramList[2] = new SqlParameter("@P_HANDLING_FEE", _fee);
                paramList[3] = new SqlParameter("@P_HANDLING_FEE_STATUS", _feeStatus ? "D" : "N");
                paramList[4] = new SqlParameter("@P_HANDLING_STATUS", _status);
                paramList[5] = new SqlParameter("@P_HANDLING_CREATED_BY", _createdBy);
                paramList[6] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[6].Direction = ParameterDirection.Output;
                paramList[7] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[7].Direction = ParameterDirection.Output;
                paramList[8] = new SqlParameter("@P_HANDLING_AGENT_ID", _agentId);

                DBGateway.ExecuteNonQuery("ct_p_handling_fee_add_update", paramList);
                string messageType = Utility.ToString(paramList[6].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[7].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LOCATION_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_location_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Static Methods
        public static DataTable GetList(int agentId,ListStatus status, RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_HANDLING_AGENT_ID", agentId);
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                return DBGateway.ExecuteQuery("ct_p_handling_fee_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Static Methods
        public static DataTable GetHandlingDetails(int serviceId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@ServiceId", serviceId);                
                return DBGateway.ExecuteQuery("CT_GET_HANDLINGFEE_DETAILS", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
