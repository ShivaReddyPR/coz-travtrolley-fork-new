using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;

/// <summary>
/// Summary description for HandlingFeeMaster
/// </summary>
namespace CT.TicketReceipt.BusinessLayer
{
    public class EdocumentUploader
{
    # region members
    long _transactionId;
    long _visaId;
    string _visaNumber;
    long  _paxId;
    string _paxName;
    long _docId;
    string  _docName;
    string _filePath;
    string _fileName;
    string _fileType;
    
    string _status;
    long _createdBy;
    string _createdByName;    
    
    # endregion

    # region Properties
    public long TransactionId
    {
        get {return _transactionId ;}
        set { _transactionId = value; }
    }
    public long Visaid
    {

        get { return _visaId; }
        set { _visaId = value; }
    }
    public string VisaNumber
    {
        get { return _visaNumber; }
        set { _visaNumber = value; }
    }

    public long PaxId
    {
        get { return _paxId; }
        set { _paxId = value; }
    }


    public string PaxName
    {
        get { return _paxName; }
        set { _paxName = value; }
    }
    public long DocId
    {
        get { return _docId ; }
        set { _docId = value; }
    }

    public string DocName
    {
        get { return _docName; }
        set { _docName = value; }
    }

    public string FilePath
    { 
        get{return _filePath;}
        set { _filePath = value; }
    }
    public string FileName
    {
        get { return _fileName ; }
        set { _fileName  = value; }
    }
    public string FileType
    {
        get { return _fileType; }
        set { _fileType = value; }
    }
   
    public string Status
    {
        get { return _status; }
        set { _status = value; }
    }
    public long CreatedBy
    {
        get { return _createdBy; }
        set { _createdBy = value; }
    }
    public string CreatedByName
    {
        get { return _createdByName; }
        set { _createdByName = value; }
    }
    # endregion

    # region Constructors

    public EdocumentUploader()
    {
        //
        // TODO: Add constructor logic here
        //
        _transactionId = -1;
        //DataTable dt = GetDetails(_transactionId);
        //if (dt != null && dt.Rows.Count > 0)
        //    UpdateBusinessData(dt.Rows[0]);
    }
    //public EdocumentUploader(long transactionId)
    //{
    //    try
    //    {
    //        DataTable dt = GetDetails(transactionId);
    //        if(dt!=null && dt.Rows.Count>0)
    //            UpdateBusinessData(dt.Rows[0]);
    //    }
    //    catch { throw; }

    //}
    # endregion

    # region Private Methods
    private DataTable GetDetails(long transactionId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[2];
            SqlParameter param;
            param = new SqlParameter("@p_receipt_id", SqlDbType.Decimal);
            param.Value = transactionId; param.Size = 8;
            paramArr[0] = param;

            param = new SqlParameter("@p_doc_location_id", SqlDbType.Decimal);
            param.Value = Settings.LoginInfo.LocationID; param.Size = 8;
            paramArr[1] = param;

            
            DataTable dt = DBGateway.ExecuteQuery("ct_p_receipt_getdata", paramArr).Tables[0];
            return dt;
        }catch { throw; }
    }
    //private void UpdateBusinessData(DataRow dr)
    //{
    //    try
    //    {
    //        if (dr != null)
    //        {
    //            if (_transactionId < 0)
    //            {
    //                _docType = Utility.ToString(dr["doc_type"]);
    //                _docDate = Utility.ToDate(dr["receipt_date"]);
    //            }
    //            else
    //            {
    //                _transactionId = Utility.ToLong(dr["receipt_id"]);
    //                _docNumber = Utility.ToString(dr["receipt_number"]);
    //                _docType = Utility.ToString(dr["receipt_type"]);
    //                _docDate = Utility.ToDate(dr["receipt_date"]);
    //                _paxName = Utility.ToString(dr["receipt_pax_name"]);
    //                _pnr = Utility.ToString(dr["receipt_pnr"]);
    //                _adults = Utility.ToInteger(dr["receipt_adults"]);
    //                _children = Utility.ToInteger(dr["receipt_children"]);
    //                _infants = Utility.ToInteger(dr["receipt_infants"]);
    //                _fare = Utility.ToDecimal(dr["receipt_fare"]);
    //                _handlingFee = Utility.ToDecimal(dr["receipt_handling_fee"]);
    //                _handlingId = Utility.ToLong(dr["receipt_handling_id"]);
    //                _totalFare = Utility.ToDecimal(dr["receipt_total_fee"]);
    //                _remarks = Utility.ToString(dr["receipt_remarks"]);
    //                _locationId = Utility.ToLong(dr["location_id"]);
    //                _status = Utility.ToString(dr["receipt_status"]);
    //                _createdBy = Utility.ToLong(dr["receipt_created_by"]);
    //                _createdByName = Utility.ToString(dr["receipt_created_name"]);
    //                _dateOfTravel= Utility.ToDate(dr["receipt_travel_date"]);
    //                _destination = Utility.ToString(dr["receipt_destination"]);
    //            }
    //        }


    //    }
    //    catch { throw; }

    //}
    public void Save()
    {
        try
        {
        SqlParameter[] paramArr = new SqlParameter[14];
        SqlParameter param = new SqlParameter("@P_UP_ID", SqlDbType.BigInt);
        param.Value = _transactionId; param.Size = 8;
        paramArr[0] = param;


        param = new SqlParameter("@P_UP_VISA_ID", SqlDbType.BigInt);
        param.Value = _visaId; param.Size = 8;
        paramArr[1] = param;

        param = new SqlParameter("@P_UP_VISA_NO", SqlDbType.NVarChar);
        param.Value=_visaNumber; param.Size = 50;
        paramArr[2] = param;

        param = new SqlParameter("@P_UP_PAX_ID", SqlDbType.BigInt);
        param.Value = _paxId; param.Size = 8;
        paramArr[3] = param;

        param = new SqlParameter("@P_UP_PAX_NAME", SqlDbType.NVarChar);
        param.Value = _paxName; param.Size = 100;
        paramArr[4] = param;

        param = new SqlParameter("@P_UP_DOC_ID", SqlDbType.BigInt);
        param.Value = _docId; param.Size = 8;
        paramArr[5] = param;

        param = new SqlParameter("@P_UP_DOC_NAME", SqlDbType.NVarChar);
        param.Value = _docName; param.Size = 50;
        paramArr[6] = param;

        param = new SqlParameter("@P_UP_FILE_PATH", SqlDbType.NVarChar);
        param.Value = _filePath; param.Size = 200;
        paramArr[7] = param;

        param = new SqlParameter("@P_UP_FILE_NAME", SqlDbType.NVarChar);
        param.Value = _fileName; param.Size = 300;
        paramArr[8] = param;

        param = new SqlParameter("@P_UP_FILE_TYPE", SqlDbType.NVarChar);
        param.Value = _fileType; param.Size = 8;
        paramArr[9] = param;

        param = new SqlParameter("@P_UP_STATUS", SqlDbType.Char);
        param.Value = _status; param.Size = 1;
        paramArr[10] = param;


        param = new SqlParameter("@P_UP_CREATED_BY", SqlDbType.BigInt);
        param.Value = _createdBy; param.Size = 8; 
        paramArr[11] = param;
        
        SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
        paramMsgType.Size = 10;
        paramMsgType.Direction = ParameterDirection.Output;
        paramArr[12] = paramMsgType;

        SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
        paramMsgText.Size = 100;
        paramMsgText.Direction = ParameterDirection.Output;
        paramArr[13] = paramMsgText;

        DBGateway.ExecuteNonQuery("visa_p_doc_upload_details_add_update", paramArr);
        if (Utility.ToString(paramMsgType.Value )== "E")
            throw new Exception(Utility.ToString(paramMsgText.Value));

        //_docNumber = Utility.ToString(paramDocNo.Value);
        //_transactionId = Utility.ToLong(paramRetValue.Value);
    }
    catch { throw; }
}
    # endregion

    # region Static Methods
    //public  static DataSet GetPaxDetailsList(long visaId,ListStatus listStatus , RecordStatus  recordStatus)
    //{
    //    try
    //    {
    //        SqlParameter[] paramArr = new SqlParameter[3];
    //        SqlParameter param = new SqlParameter("@P_PAX_VSH_ID", SqlDbType.BigInt);
    //        param.Value = visaId ; 
    //        paramArr[0] = param;

    //        param = new SqlParameter("@P_LIST_STATUS", SqlDbType.Int);
    //        param.Value = listStatus; 
    //        paramArr[1] = param;

    //        param = new SqlParameter("@P_RECORD_STATUS", SqlDbType.NVarChar);
    //        param.Value = recordStatus; param.Size = 10;
    //        paramArr[2] = param;
           
    //        return DBGateway.ExecuteQuery("vis_p_pax_details_getlist", paramArr);
    //    }
    //    catch { throw; }
    //}
    //public static DataTable GetReportDetails(long transactionId)
    //{
    //    try
    //    {
    //        SqlParameter[] paramArr = new SqlParameter[1];
    //        SqlParameter param = new SqlParameter("@p_receipt_id", SqlDbType.Decimal);
    //        param.Value = transactionId; param.Size = 8;
    //        paramArr[0] = param;

    //        DataTable dt = DBGateway.ExecuteQuery("ct_p_receipt_getdata", paramArr).Tables[0];
    //        return dt;
    //    }
    //    catch { throw; }
    //}
    # endregion
}
}