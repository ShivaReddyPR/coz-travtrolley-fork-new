using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{


    public class CityMaster
    {
        private const long NEW_RECORD = -1;
        
        #region MemberVerables
        private long _id;
        private string _code;
        private string _name;
        private long _countryId;
        private string _notes;
        private string _status;
        private long _createdBy;
        #endregion

        #region Properties

        public long ID
        {
            get { return _id; }
        }
        public string Code
        {
            get { return _code; }
            set {_code = value ; }
        }

        public string Name
        {
            get { return _name; }
            set{_name= value;}
        }
        public long CountryId
        {
            get { return _countryId; }
            set { _countryId = value; }
        }

        public string Notes
        {
            get { return _notes; }
            set{_notes= value;}
    
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy 
        {
            get{return _createdBy;}
            set { _createdBy = value; } 
        }   
        #endregion

         public CityMaster()
        {
            _id = NEW_RECORD;
        }
        public CityMaster(long id)
        {
            _id = id;
            getDetails(_id);

        }
        #region Methods
        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }

        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_CITY_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_city_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
            
                _id = Utility.ToLong(dr["CITY_ID"]);
                _code = Utility.ToString(dr["CITY_CODE"]);
                _name = Utility.ToString(dr["CITY_NAME"]);
                _countryId = Utility.ToLong(dr["CITY_COUNTRY_ID"]);
                _notes = Utility.ToString(dr["CITY_NOTES"]);
                _status = Utility.ToString(dr["CITY_STATUS"]);
                _createdBy = Utility.ToLong(dr["CITY_CREATED_BY"]);
                // _visaServiceType  = Utility.ToString(dr["COUNTRY_VISA_SERVICE"]);                                                       
            
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[9];

                paramList[0] = new SqlParameter("@P_CITY_ID", _id);
                if(!string.IsNullOrEmpty(_code)) paramList[1] = new SqlParameter("@P_CITY_CODE", _code);
                if (!string.IsNullOrEmpty(_name)) paramList[2] = new SqlParameter("@P_CITY_NAME", _name);
                if(!string.IsNullOrEmpty(_notes)) paramList[3] = new SqlParameter("@P_CITY_NOTES", _notes);
                if(_countryId>0) paramList[4] = new SqlParameter("@P_CITY_COUNTRY_ID", _countryId);
                paramList[5] = new SqlParameter("@P_CITY_STATUS", _status);
                paramList[6] = new SqlParameter("@P_CITY_CREATED_BY", _createdBy);
                paramList[7] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[7].Direction = ParameterDirection.Output;
                paramList[8] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[8].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("[ct_p_city_add_update]", paramList);
                string messageType = Utility.ToString(paramList[7].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[8].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_CITY_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_CITY_MASTER_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        #endregion
        #region Static Methods

        public static DataTable GetList(int countryId,ListStatus status,RecordStatus recordStatus)
        {

            try
            {
                
                SqlParameter[] paramList = new SqlParameter[3];

                if (countryId>0) paramList[0] = new SqlParameter("@P_COUNTRY_ID", countryId);
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("ct_p_city_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        # endregion


        public static DataTable GetAllCities(ListStatus status, RecordStatus recordStatus)
        {
            try
            {

                SqlParameter[] paramList = new SqlParameter[2];               
                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("ct_p_city_getallCitieslist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }



       
    }
   
}
