using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;


namespace CT.TicketReceipt.BusinessLayer
{
    public class SalesTypeMaster
    {
        private const long NEW_RECORD = -1;
        
       
        #region Static Methods
        public static DataTable GetList(int agentId ,ListStatus status,RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                if (agentId > 0) paramList[0] = new SqlParameter("@P_SALES_TYPE_AGENT_ID", agentId);                
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", recordStatus);

                return DBGateway.ExecuteQuery("visa_p_sales_type_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        # endregion
       
    }
   
}
