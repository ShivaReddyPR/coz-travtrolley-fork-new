using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class DashBorad
    {
        private const long NEW_RECORD = -1;
        
        
       
        #region Static Methods
        public static DataSet GetVisaCountList(long userId, string userType, long locationId, int agentId,
       ListStatus status, RecordStatus recordStatus)
        {
            try
            {
                SqlParameter[] paramArr = new SqlParameter[6];

                paramArr[0] = new SqlParameter("@P_USER_ID", userId);
                paramArr[1] = new SqlParameter("@P_USER_TYPE", userType);
                paramArr[2] = new SqlParameter("@P_LOCATION_ID", locationId);
                if (agentId > 0) paramArr[3] = new SqlParameter("@P_AGENT_ID", agentId);
                paramArr[4] = new SqlParameter("@p_list_status", recordStatus);
                paramArr[5] = new SqlParameter("@p_record_status", recordStatus);
                return DBGateway.ExecuteQuery("visa_p_dashboard_getDetails", paramArr);
            }
            catch { throw; }
        }
        public static DataSet GetVisaDelayedCountList(VisaDispatchStatus dispStaus, int days, long userId, string userType, long locationId, int agentId,
      ListStatus status, RecordStatus recordStatus)
        {
            try
            {
                SqlParameter[] paramArr = new SqlParameter[8];

                paramArr[0] = new SqlParameter("@P_DISP_STATUS", Utility.ToString((char)dispStaus));
                paramArr[1] = new SqlParameter("@P_DELAY_DAYS", days);
                paramArr[2] = new SqlParameter("@P_USER_ID", userId);
                paramArr[3] = new SqlParameter("@P_USER_TYPE", userType);
                paramArr[4] = new SqlParameter("@P_LOCATION_ID", locationId);
                if (agentId > 0) paramArr[5] = new SqlParameter("@P_AGENT_ID", agentId);
                paramArr[6] = new SqlParameter("@p_list_status", recordStatus);
                paramArr[7] = new SqlParameter("@p_record_status", Utility.ToString((char)recordStatus));
                return DBGateway.ExecuteQuery("visa_p_delayed_visa_list", paramArr);
            }
            catch { throw; }
        }
        public static DataSet GetVisaOverStayList(long userId, string userType, long locationId, int agentId,
      ListStatus status, RecordStatus recordStatus)
        {
            try
            {
                SqlParameter[] paramArr = new SqlParameter[6];

                paramArr[0] = new SqlParameter("@P_USER_ID", userId);
                paramArr[1] = new SqlParameter("@P_USER_TYPE", userType);
                paramArr[2] = new SqlParameter("@P_LOCATION_ID", locationId);
                if(agentId>0) paramArr[3] = new SqlParameter("@P_AGENT_ID", agentId);
                paramArr[4] = new SqlParameter("@p_list_status", recordStatus);
                paramArr[5] = new SqlParameter("@p_record_status", recordStatus);
                return DBGateway.ExecuteQuery("VISA_P_OVERSTAY_GETDETAILS", paramArr);
            }
            catch { throw; }
        }
        # endregion
       
    }
   
}
