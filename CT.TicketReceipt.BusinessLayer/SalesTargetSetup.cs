using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class BreakInfo
    {
        private string _label;
        private string _column;
        private string _sql;
        private string _controlType;

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }
        public string Column
        {
            get { return _column; }
            set { _column = value; }
        }
        public string SqlQuery
        {
            get { return _sql; }
            set { _sql = value; }
        }

        public string ControlType
        {
            get { return _controlType ; }
            set { _controlType  = value; }
        }
        public BreakInfo()
        {
        }
        public BreakInfo(string label, string column, string sqlQuery,string controlType)
        {
            _label = label;
            _column = column;
            _sql = sqlQuery;
            _controlType = controlType;

        }

        public void Set(object label, object column, object sqlQuery,object controlType)
        {
            _label = Utility.ToString(label);
            _column = Utility.ToString(column);
            _sql = Utility.ToString(sqlQuery);
            _controlType = Utility.ToString(controlType);

        }
    }

    public class AmountInfo
    {
        private string _label;
        private string _column;
        private string _percentRequired;
        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }
        public string Column
        {
            get { return _column; }
            set { _column = value; }
        }
        public string PercentRequired
        {
            get { return _percentRequired; }
            set { _percentRequired = value; }
        }
        public AmountInfo()
        {
        }
        public AmountInfo(string label, string column, string percentRequired)
        {
            _label = label;
            _column = column;
            _percentRequired = percentRequired;

        }
        public void Set(object label, object column, object percentRequired)
        {
            _label = Utility.ToString(label);
            _column = Utility.ToString(column);
            _percentRequired = Utility.ToString(percentRequired);

        }

    }
    public class SalesTargetSetup
    {

        #region Member Variables
        private string _mode;
        private BreakInfo _break1 = new BreakInfo();
        private BreakInfo _break2 = new BreakInfo();
        private BreakInfo _break3 = new BreakInfo();
        private BreakInfo _break4 = new BreakInfo();
        private BreakInfo _break5 = new BreakInfo();
        private BreakInfo _break6 = new BreakInfo();
        private BreakInfo _break7 = new BreakInfo();
        private BreakInfo _break8 = new BreakInfo();
        private BreakInfo _break9 = new BreakInfo();
        private BreakInfo _break10 = new BreakInfo();

        private AmountInfo _amount1 = new AmountInfo();
        private AmountInfo _amount2 = new AmountInfo();
        private AmountInfo _amount3 = new AmountInfo();
        private AmountInfo _amount4 = new AmountInfo();
        private AmountInfo _amount5 = new AmountInfo();
        private string _category;
        private string _createdBy;
        private string _uploadStauts;
        #endregion

        #region Properties

        public string Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }

        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }
        public BreakInfo Break1
        {
            get { return _break1; }
            set { _break1 = value; }
        }
        public BreakInfo Break2
        {
            get { return _break2; }
            set { _break2 = value; }
        }
        public BreakInfo Break3
        {
            get { return _break3; }
            set { _break3 = value; }
        }
        public BreakInfo Break4
        {
            get { return _break4; }
            set { _break4 = value; }
        }
        public BreakInfo Break5
        {
            get { return _break5; }
            set { _break5 = value; }
        }

        public BreakInfo Break6
        {
            get { return _break6; }
            set { _break6 = value; }
        }
        public BreakInfo Break7
        {
            get { return _break7; }
            set { _break7 = value; }
        }
        public BreakInfo Break8
        {
            get { return _break8; }
            set { _break8 = value; }
        }
        public BreakInfo Break9
        {
            get { return _break9; }
            set { _break9 = value; }
        }
        public BreakInfo Break10
        {
            get { return _break10; }
            set { _break10 = value; }
        }

        public AmountInfo Amount1
        {
            get { return _amount1; }
            set { _amount1 = value; }
        }
        public AmountInfo Amount2
        {
            get { return _amount2; }
            set { _amount2 = value; }
        }
        public AmountInfo Amount3
        {
            get { return _amount3; }
            set { _amount3 = value; }
        }
        public AmountInfo Amount4
        {
            get { return _amount4; }
            set { _amount4 = value; }
        }
        public AmountInfo Amount5
        {
            get { return _amount5; }
            set { _amount5 = value; }
        }

        public string CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }


        public string UploadStatus
        {
            get { return _uploadStauts; }
            set { _uploadStauts = value; }
        }
        #endregion

        #region Constructors
        public SalesTargetSetup()
        {
            _mode = "N";
        }
        public SalesTargetSetup(string category)
        {


            _category = category;
            _mode = "E";
            getDetails(category);
        }
        #endregion
        #region Methods
        private void getDetails(string category)
        {

            DataSet ds = GetData(category);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(string category)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_TSETUP_CATEGORY", category);
                DataSet dsResult = DBGateway.ExecuteQuery("SALES_TRAVEL_TARGET_SETUP_GETDATA", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _category = Utility.ToString(dr["TSETUP_CATEGORY"]);
                _break1.Set(dr["TSETUP_BREAK_1_LABEL"], dr["TSETUP_BREAK_1_COLUMN"], dr["TSETUP_BREAK_1_SQL"], dr["TSETUP_BREAK_1_CONTROL"]);
                _break2.Set(dr["TSETUP_BREAK_2_LABEL"], dr["TSETUP_BREAK_2_COLUMN"], dr["TSETUP_BREAK_2_SQL"], dr["TSETUP_BREAK_2_CONTROL"]);
                _break3.Set(dr["TSETUP_BREAK_3_LABEL"], dr["TSETUP_BREAK_3_COLUMN"], dr["TSETUP_BREAK_3_SQL"], dr["TSETUP_BREAK_3_CONTROL"]);
                _break4.Set(dr["TSETUP_BREAK_4_LABEL"], dr["TSETUP_BREAK_4_COLUMN"], dr["TSETUP_BREAK_4_SQL"], dr["TSETUP_BREAK_4_CONTROL"]);
                _break5.Set(dr["TSETUP_BREAK_5_LABEL"], dr["TSETUP_BREAK_5_COLUMN"], dr["TSETUP_BREAK_5_SQL"], dr["TSETUP_BREAK_5_CONTROL"]);

                _break6.Set(dr["TSETUP_BREAK_6_LABEL"], dr["TSETUP_BREAK_6_COLUMN"], dr["TSETUP_BREAK_6_SQL"], dr["TSETUP_BREAK_6_CONTROL"]);
                _break7.Set(dr["TSETUP_BREAK_7_LABEL"], dr["TSETUP_BREAK_7_COLUMN"], dr["TSETUP_BREAK_7_SQL"], dr["TSETUP_BREAK_7_CONTROL"]);
                _break8.Set(dr["TSETUP_BREAK_8_LABEL"], dr["TSETUP_BREAK_8_COLUMN"], dr["TSETUP_BREAK_8_SQL"], dr["TSETUP_BREAK_8_CONTROL"]);
                _break9.Set(dr["TSETUP_BREAK_9_LABEL"], dr["TSETUP_BREAK_9_COLUMN"], dr["TSETUP_BREAK_9_SQL"], dr["TSETUP_BREAK_9_CONTROL"]);
                _break10.Set(dr["TSETUP_BREAK_10_LABEL"], dr["TSETUP_BREAK_10_COLUMN"], dr["TSETUP_BREAK_10_SQL"], dr["TSETUP_BREAK_10_CONTROL"]);

                _amount1.Set(dr["TSETUP_AMOUNT_1_LABEL"], dr["TSETUP_AMOUNT_1_COLUMN"], dr["TSETUP_AMOUNT_1_PERCENT"]);
                _amount2.Set(dr["TSETUP_AMOUNT_2_LABEL"], dr["TSETUP_AMOUNT_2_COLUMN"], dr["TSETUP_AMOUNT_2_PERCENT"]);
                _amount3.Set(dr["TSETUP_AMOUNT_3_LABEL"], dr["TSETUP_AMOUNT_3_COLUMN"], dr["TSETUP_AMOUNT_3_PERCENT"]);
                _amount4.Set(dr["TSETUP_AMOUNT_4_LABEL"], dr["TSETUP_AMOUNT_4_COLUMN"], dr["TSETUP_AMOUNT_4_PERCENT"]);
                _amount5.Set(dr["TSETUP_AMOUNT_5_LABEL"], dr["TSETUP_AMOUNT_5_COLUMN"], dr["TSETUP_AMOUNT_5_PERCENT"]);

                _uploadStauts = Utility.ToString(dr["tsetup_upload_status"]);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[62];

                paramList[0] = new SqlParameter("@P_TSETUP_COMPANY", "1");
                paramList[1] = new SqlParameter("@P_TSETUP_BREAK_1_LABEL", _break1.Label);
                paramList[2] = new SqlParameter("@P_TSETUP_BREAK_1_SQL", _break1.SqlQuery );
                paramList[3] = new SqlParameter("@P_TSETUP_BREAK_1_COLUMN", _break1.Column);
                paramList[4] = new SqlParameter("@P_TSETUP_BREAK_2_LABEL", _break2.Label);
                paramList[5] = new SqlParameter("@P_TSETUP_BREAK_2_SQL", _break2.SqlQuery);
                paramList[6] = new SqlParameter("@P_TSETUP_BREAK_2_COLUMN", _break2.Column);
                paramList[7] = new SqlParameter("@P_TSETUP_BREAK_3_LABEL", _break3.Label);
                paramList[8] = new SqlParameter("@P_TSETUP_BREAK_3_SQL", _break3.SqlQuery);
                paramList[9] = new SqlParameter("@P_TSETUP_BREAK_3_COLUMN", _break3.Column);
                paramList[10] = new SqlParameter("@P_TSETUP_BREAK_4_LABEL", _break4.Label);
                paramList[11] = new SqlParameter("@P_TSETUP_BREAK_4_SQL", _break4.SqlQuery);
                paramList[12] = new SqlParameter("@P_TSETUP_BREAK_4_COLUMN", _break4.Column);
                paramList[13] = new SqlParameter("@P_TSETUP_BREAK_5_LABEL", _break5.Label);
                paramList[14] = new SqlParameter("@P_TSETUP_BREAK_5_SQL", _break5.SqlQuery);
                paramList[15] = new SqlParameter("@P_TSETUP_BREAK_5_COLUMN", _break5.Column);

                paramList[16] = new SqlParameter("@P_TSETUP_BREAK_6_LABEL", _break6.Label);
                paramList[17] = new SqlParameter("@P_TSETUP_BREAK_6_SQL", _break6.SqlQuery);
                paramList[18] = new SqlParameter("@P_TSETUP_BREAK_6_COLUMN", _break6.Column);
                paramList[19] = new SqlParameter("@P_TSETUP_BREAK_7_LABEL", _break7.Label);
                paramList[20] = new SqlParameter("@P_TSETUP_BREAK_7_SQL", _break7.SqlQuery);
                paramList[21] = new SqlParameter("@P_TSETUP_BREAK_7_COLUMN", _break7.Column);
                paramList[22] = new SqlParameter("@P_TSETUP_BREAK_8_LABEL", _break8.Label);
                paramList[23] = new SqlParameter("@P_TSETUP_BREAK_8_SQL", _break8.SqlQuery);
                paramList[24] = new SqlParameter("@P_TSETUP_BREAK_8_COLUMN", _break8.Column);
                paramList[25] = new SqlParameter("@P_TSETUP_BREAK_9_LABEL", _break9.Label);
                paramList[26] = new SqlParameter("@P_TSETUP_BREAK_9_SQL", _break9.SqlQuery);
                paramList[27] = new SqlParameter("@P_TSETUP_BREAK_9_COLUMN", _break9.Column);
                paramList[28] = new SqlParameter("@P_TSETUP_BREAK_10_LABEL", _break10.Label);
                paramList[29] = new SqlParameter("@P_TSETUP_BREAK_10_SQL", _break10.SqlQuery);
                paramList[30] = new SqlParameter("@P_TSETUP_BREAK_10_COLUMN", _break10.Column);

                paramList[31] = new SqlParameter("@P_TSETUP_AMOUNT_1_LABEL", _amount1.Label);
                paramList[32] = new SqlParameter("@P_TSETUP_AMOUNT_1_COLUMN", _amount1.Column);
                paramList[33] = new SqlParameter("@P_TSETUP_AMOUNT_1_PERCENT", _amount1.PercentRequired);
                paramList[34] = new SqlParameter("@P_TSETUP_AMOUNT_2_LABEL", _amount2.Label);
                paramList[35] = new SqlParameter("@P_TSETUP_AMOUNT_2_COLUMN", _amount2.Column);
                paramList[36] = new SqlParameter("@P_TSETUP_AMOUNT_2_PERCENT", _amount2.PercentRequired);
                paramList[37] = new SqlParameter("@P_TSETUP_AMOUNT_3_LABEL", _amount3.Label);
                paramList[38] = new SqlParameter("@P_TSETUP_AMOUNT_3_COLUMN", _amount3.Column);
                paramList[39] = new SqlParameter("@P_TSETUP_AMOUNT_3_PERCENT", _amount3.PercentRequired);
                paramList[40] = new SqlParameter("@P_TSETUP_AMOUNT_4_LABEL", _amount4.Label);
                paramList[41] = new SqlParameter("@P_TSETUP_AMOUNT_4_COLUMN", _amount4.Column);
                paramList[42] = new SqlParameter("@P_TSETUP_AMOUNT_4_PERCENT", _amount4.PercentRequired);
                paramList[43] = new SqlParameter("@P_TSETUP_AMOUNT_5_LABEL", _amount5.Label);
                paramList[44] = new SqlParameter("@P_TSETUP_AMOUNT_5_COLUMN", _amount5.Column);
                paramList[45] = new SqlParameter("@P_TSETUP_AMOUNT_5_PERCENT", _amount5.PercentRequired);


                paramList[46] = new SqlParameter("@P_TSETUP_CATEGORY", _category);
                paramList[47] = new SqlParameter("@P_TSETUP_created_by", _createdBy);
                paramList[48] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[48].Direction = ParameterDirection.Output;
                paramList[49] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[49].Direction = ParameterDirection.Output;
                paramList[50] = new SqlParameter("@P_MODE", _mode);
                paramList[51] = new SqlParameter("@P_TSETUP_BREAK_1_CONTROL", _break1.ControlType);
                paramList[52] = new SqlParameter("@P_TSETUP_BREAK_2_CONTROL", _break2.ControlType);
                paramList[53] = new SqlParameter("@P_TSETUP_BREAK_3_CONTROL", _break3.ControlType);
                paramList[54] = new SqlParameter("@P_TSETUP_BREAK_4_CONTROL", _break4.ControlType);
                paramList[55] = new SqlParameter("@P_TSETUP_BREAK_5_CONTROL", _break5.ControlType);
                paramList[56] = new SqlParameter("@P_TSETUP_BREAK_6_CONTROL", _break6.ControlType);
                paramList[57] = new SqlParameter("@P_TSETUP_BREAK_7_CONTROL", _break7.ControlType);
                paramList[58] = new SqlParameter("@P_TSETUP_BREAK_8_CONTROL", _break8.ControlType);
                paramList[59] = new SqlParameter("@P_TSETUP_BREAK_9_CONTROL", _break9.ControlType);
                paramList[60] = new SqlParameter("@P_TSETUP_BREAK_10_CONTROL", _break10.ControlType);
                paramList[61] = new SqlParameter("@P_TSETUP_UPLOAD_STATUS",_uploadStauts );
                DBGateway.ExecuteNonQuery("SALES_TRAVEL_TARGET_SETUP_ADD_UPDATE", paramList);
                string messageType = Utility.ToString(paramList[48].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[49].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }   
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_TSETUP_CATEGORY", _category);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("CT_P_SALES_TRAVEL_TARGET_SETUP_DELETE", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        
        #endregion

        #region Static Methods
        public static DataTable GetList()
        {

            try
            {
                SqlParameter[] paramList = null;
                return DBGateway.ExecuteQuery("SALES_TRAVEL_TARGET_SETUP_GETLIST", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetColumnsList()
        {

            try
            {
                SqlParameter[] paramList = null;
                return DBGateway.ExecuteQuery("CT_P_SALES_TARGET_COLUMNS_GETLIST", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public static bool IsValidQuery(string queryString)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_SQLQUERY", queryString);
                paramList[1] = new SqlParameter("@P_MSGTEXT", SqlDbType.VarChar, 500);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("CT_P_TRAVEL_SALES_GET_VALIDQUERY", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
                return true;
            }
            catch
            {
                throw;
            
            }
        }

        public static DataTable GetReportData(string category)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_CATEGORY", category);
                return DBGateway.ExecuteQuery("CT_P_TRAVEL_SALES_TARGET_REPORT", paramList).Tables[0];
               
            }
            catch
            {
                throw;

            }
        }

        public static DataTable GetCrystalReportData(string year,string location, string agent, string airline)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@P_TICKET_YEAR", year );
                paramList[1] = new SqlParameter("@P_TICKET_LOCATION", location );
                paramList[2] = new SqlParameter("@P_TICKET_AIRLINE", agent );
                paramList[3] = new SqlParameter("@P_TICKET_AGENT", airline );
                return DBGateway.ExecuteQuery("CT_P_SALES_REPORT", paramList).Tables[0];

            }
            catch
            {
                throw;

            }
        }

        public static DataTable GetTicketSalesBudgetReportData(string category,string year,string fromMonth,string toMonth,string fromLocation,string toLocation,string detailRpt,string accountedYN, string expoPromoYN)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[9];
                paramList[0] = new SqlParameter("@P_SETUP_CATEGORY", category );
                paramList[1] = new SqlParameter("@P_TARGET_YEAR", year);
                paramList[2] = new SqlParameter("@P_FROM_MONTH", fromMonth);
                paramList[3] = new SqlParameter("@P_TO_MONTH", toMonth);
               
                 if (fromLocation == "") 
                     paramList[4] = new SqlParameter("@P_FROM_LOCATION", DBNull.Value);
                 else paramList[4] = new SqlParameter("@P_FROM_LOCATION", fromLocation);

                if(toLocation == "")
                    paramList[5] = new SqlParameter("@P_TO_LOCATION", DBNull.Value);
                else paramList[5] = new SqlParameter("@P_TO_LOCATION", toLocation);

                paramList[6] = new SqlParameter("@P_DETAIL_RPT",detailRpt);
                paramList[7] = new SqlParameter("@P_ACCOUNTED_YN", accountedYN );
                paramList[8] = new SqlParameter("@P_EXPO_PROMO", expoPromoYN );
                
                return DBGateway.ExecuteQuery("CT_P_TICKET_SALES_TARGET_REPORT", paramList).Tables[0];

            }
            catch
            {
                throw;

            }
        }
        public static DataTable GetAirlineList()
        {

            try
            {
                SqlParameter[] paramList = null;
                return DBGateway.ExecuteQuery("CT_P_AIRLINE_MASTER_GETLIST", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetAirlineAgentList()
        {

            try
            {
                SqlParameter[] paramList = null;
                return DBGateway.ExecuteQuery("CT_P_AIRLINE_AGENT_MASTER_GETLIST", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetLocationList()
        {

            try
            {
                SqlParameter[] paramList = null;
                return DBGateway.ExecuteQuery("ct_p_ticket_location_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public static DataTable ExecuteBreakQuery(string queryString)
        {
            try
            {

                return DBGateway.ExecuteQuery(queryString).Tables[0];
                
            }
            catch
            {
                throw;

            }
        }

       

        #endregion
    }

}
