﻿using System;
using System.Data;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;

namespace CT.TicketReceipt.BusinessLayer
{
    public class AgentCreditCardCharges
    {
        int sourceId;
        int agentId;
        int productId;
        int pgSource;
        decimal pgCharges;
        decimal totalAmount;
        string status;        
        int createdBy;

        public AgentCreditCardCharges()
        {
            sourceId = -1;
        }


        public int SourceId
        {
            get { return sourceId; }
            set { sourceId = value; }
        }

        public int AgentId
        {
            get { return agentId; }
            set { agentId = value; }
        }

        public int ProductId
        {
            get { return productId; }
            set { productId = value; }
        }

        public int PGSource
        {
            get { return pgSource; }
            set { pgSource = value; }
        }

        public decimal PGCharges
        {
            get { return pgCharges; }
            set { pgCharges = value; }
        }

        public decimal TotalCharges
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }

        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public string Status
        {
            get { return status; }
            set { status = value; }
        }


        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_AgentID", agentId);
                paramList[1] = new SqlParameter("@P_ProductID", productId);
                paramList[2] = new SqlParameter("@P_PGSource", pgSource);
                paramList[3] = new SqlParameter("@P_PGCharges", pgCharges);
                paramList[4] = new SqlParameter("@P_totalAmount", totalAmount);
                paramList[5] = new SqlParameter("@P_CreatedBy", createdBy);
                if (sourceId == -1)
                {
                    int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddAgentCreditCardCharges", paramList);
                }
                else
                {
                    paramList = new SqlParameter[7];
                    paramList[0] = new SqlParameter("@P_AgentID", agentId);
                    paramList[1] = new SqlParameter("@P_ProductID", productId);
                    paramList[2] = new SqlParameter("@P_PGSource", pgSource);
                    paramList[3] = new SqlParameter("@P_PGCharges", pgCharges);
                    paramList[4] = new SqlParameter("@P_totalAmount", totalAmount);
                    paramList[5] = new SqlParameter("@P_CreatedBy", createdBy);
                    paramList[6] = new SqlParameter("@P_SourceID", sourceId);
                    int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_UpdateAgentCreditCardCharges", paramList);
                }
                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.CCAvenue, Severity.High, createdBy, ex.ToString(), "");
            }
        }

        public static DataTable GetList()
        {
            DataTable dtList = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[0];
                dtList = DBGateway.FillDataTableSP("usp_GetAgentCreditCardCharges", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.CCAvenue, Severity.High, 1, ex.ToString(), "");
            }
            return dtList;
        }
    }
}
