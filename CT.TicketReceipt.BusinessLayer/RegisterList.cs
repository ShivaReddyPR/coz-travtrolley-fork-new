﻿using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;

/// <summary>
/// Summary description for RegisterList
/// </summary>
/// 

namespace CT.TicketReceipt.BusinessLayer
{
    public class RegisterList
    {
        public RegisterList()
        {

        }
        public static DataTable GetList(char AgentStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_Agent_STATUS", AgentStatus);
                return DBGateway.ExecuteQuery("usp_GetAllB2BRegisterList", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static void DeleteUser(long agent_id)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_AGENT_id", agent_id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[2] = new SqlParameter("@P_agent_modified_by", Settings.LoginInfo.UserID);
                paramList[1].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuery("[usp_DeleteB2BRegisteredAgent]", paramList);
                string message = Utility.ToString(paramList[1].Value);
            }
            catch { throw; }
        }


    }
}
