using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class AgentMaster
    {
        private const long NEW_RECORD = -1;

        private long _id;
        private string _code;
        private string _name;
        private string _address;
        private string _city;
        private string _state;
        private int _country;
        private string _pobox;
        private string _phone1;
        private string _phone2;
        private string _fax;
        private string _telex;
        private string _email1;
        private string _email2;
        private decimal _alertLevel;
        private string _website;
        private string _imgFileName;
        private string _nflex1;
        private string _nflex2;
        private string _status;
        private long _createdBy;

        private decimal _currentBalance;
        private string _emailContent1;
        private string _emailContent2;

        private string _licenseNo;
        private DateTime _licenseExpDate;
        private string  _passportNo;
        private DateTime _passportIssueDate;
        private DateTime _passportExpDate;
        private long _nationality;
        private string _grntrStatus;
        private string _visaSubmissionStatus;
        private string _agentCurrency;
        private string _agentBlockStatus;
        private string _agentRemarks;
        private int _agentType;
       // private string _agentCurrency;
        private string _reduceBalanceStatus;
        private long _modifiedBy;

        #region Properties
        public long ID
        {
            get { return _id; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public int Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public string POBox
        {
            get { return _pobox; }
            set { _pobox = value; }
        }
        public string Phone1
        {
            get { return _phone1; }
            set { _phone1 = value; }
        }
        public string Phone2
        {
            get { return _phone2; }
            set { _phone2 = value; }
        }

        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }
        public string Telex
        {
            get { return _telex; }
            set { _telex = value; }
        }
        public string Email1
        {
            get { return _email1; }
            set { _email1 = value; }
        }
        public string Email2
        {
            get { return _email2; }
            set { _email2 = value; }
        }
        public decimal AlertLevel
        {
            get { return _alertLevel; }
            set { _alertLevel = value; }
        }
        public string Website
        {
            get { return _website ; }
            set { _website = value; }
        }
        public string ImgFileName
        {
            get { return _imgFileName; }
            set { _imgFileName = value; }
        }
        public string NFlex1
        {
            get { return _nflex1; }
            set { _nflex1 = value; }
        }
        public string NFlex2
        {
            get { return _nflex2; }
            set { _nflex2 = value; }
        }
        public string  Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }


        public decimal CurrentBalance
        {
            get { return _currentBalance; }
            set { _currentBalance = value; }
        }

        public string EmailContent1
        {

            get { return _emailContent1; }
            set { _emailContent1 = value; }

        }

        public string EmailContent2
        {

            get { return _emailContent2; }
            set { _emailContent2 = value; }

        }

        public string LicenseNo
        {
            get { return _licenseNo; }
            set { _licenseNo = value  ; }
        }


        public DateTime LicenseNoExpDate
        {
            get { return _licenseExpDate; }
            set { _licenseExpDate = value ; }
        }

        public string PassportNo
        {
            get { return _passportNo; }
            set { _passportNo = value; }
        }

        public DateTime PassportIssueDate
        {
            get { return _passportIssueDate; }
            set { _passportIssueDate = value; }
        }

        public DateTime PassportExpDate
        {
            get { return _passportExpDate; }
            set { _passportExpDate =value; }
        }

        public long Nationality
        {
            get { return _nationality; }

            set { _nationality = value; }

        }


        public String GrntrStatus
        {
            get { return _grntrStatus; }
            set { _grntrStatus = value; }
        }

        public string VisaSubmissionStatus
        {
            get { return _visaSubmissionStatus; }
            set { _visaSubmissionStatus = value; }

        }

        public string AgentCurrency
        {
            get { return _agentCurrency; }
            set { _agentCurrency = value; }

        }

        public string AgentBlockStatus
        {
            get { return _agentBlockStatus; }
            set { _agentBlockStatus = value; }

        }


        public string AgentRemarks
        {
            get { return _agentRemarks; }
            set { _agentRemarks = value; }

        }


        public string ReduceBalanceStatus
        {
            get { return _reduceBalanceStatus; }
            set { _reduceBalanceStatus = value; }

        }

        

        public int AgentType
        {
            get { return _agentType; }
            set { _agentType = value; }
        }
        #endregion


        #region Constructors
        public AgentMaster()
        {
            _id = NEW_RECORD;
        }
        public AgentMaster(long id)
        {


            _id = id;
            getDetails((int)id);
            _agentType = 1;
        }
        public AgentMaster(int id)
        {


            _id = id;
            getDetails(id);
            _agentType = 1;
        }
        #endregion


        #region Methods
        private void getDetails(int id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(int id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_AGENT_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_agent_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _id = Utility.ToLong(dr["agent_id"]);
                _code = Utility.ToString(dr["agent_code"]);
                _name = Utility.ToString(dr["agent_name"]);
                _address = Utility.ToString(dr["agent_address"]);
                _city = Utility.ToString(dr["agent_city"]);

                _state  = Utility.ToString(dr["agent_state"]);
                _country  = Utility.ToInteger(dr["agent_country"]);
                _pobox  = Utility.ToString(dr["agent_po_box"]);
                _phone1  = Utility.ToString(dr["agent_phone1"]);
                _phone2  = Utility.ToString(dr["agent_phone2"]);
                _fax  = Utility.ToString(dr["agent_fax"]);
                _telex = Utility.ToString(dr["agent_telex"]);
                _email1 = Utility.ToString(dr["agent_email1"]);
                _email2 = Utility.ToString(dr["agent_email2"]);
                _alertLevel = Utility.ToDecimal(dr["agent_alert_balance"]);
                _website = Utility.ToString(dr["agent_website"]);
                _imgFileName  = Utility.ToString(dr["agent_img_filename"]);
                _nflex1  = Utility.ToString(dr["n_flex_1"]);

                _nflex2  = Utility.ToString(dr["n_flex_2"]);
                _status = Utility.ToString(dr["agent_status"]);
                _createdBy = Utility.ToLong(dr["agent_created_by"]);

                _currentBalance = Utility.ToDecimal(dr["agent_current_balance"]);
                _emailContent1 = Utility.ToString(dr["agent_email_content1"]);
                _emailContent2 = Utility.ToString(dr["agent_email_content2"]);

                _licenseNo = Utility.ToString(dr["agent_license_no"]);
                _licenseExpDate = Utility.ToDate(dr["agent_license_exp_date"]);
                _passportNo  = Utility.ToString(dr["agent_passport_no"]);
                _passportIssueDate  = Utility.ToDate (dr["agent_passport_issue_date"]);
                _passportExpDate  = Utility.ToDate(dr["agent_passport_expiry_date"]);
                _nationality = Utility.ToLong(dr["agent_nationality"]);
                _grntrStatus = Utility.ToString(dr["agent_guarantor_status"]);
                _visaSubmissionStatus= Utility.ToString(dr["agent_visa_submission_status"]);
                _agentCurrency = Utility.ToString(dr["agent_currency"]);
                _agentBlockStatus = Utility.ToString(dr["agent_block_status"]);
                _agentRemarks = Utility.ToString(dr["agent_remarks"]);
                _reduceBalanceStatus = Utility.ToString(dr["agent_reduce_balance"]);
                //_loginName = Utility.ToString(dr["USER_LOGIN_NAME"]);
                //_locationId = Utility.ToLong(dr["USER_LOCATION_ID"]);
                
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[37];

                paramList[0] = new SqlParameter("@P_AGENT_ID", _id);
                paramList[1] = new SqlParameter("@P_AGENT_COMPANY_ID", Settings.LoginInfo.CompanyID );
                paramList[2] = new SqlParameter("@P_AGENT_CODE", _code);
                paramList[3] = new SqlParameter("@P_AGENT_NAME", _name );
                paramList[4] = new SqlParameter("@P_AGENT_ADDRESS", _address );
                paramList[5] = new SqlParameter("@P_AGENT_CITY", _city);
                paramList[6] = new SqlParameter("@P_AGENT_STATE", _state);
                paramList[7] = new SqlParameter("@P_AGENT_COUNTRY", _country);
                paramList[8] = new SqlParameter("@P_AGENT_PO_BOX", _pobox );
                paramList[9] = new SqlParameter("@P_AGENT_PHONE1", _phone1 );
                paramList[10] = new SqlParameter("@P_AGENT_PHONE2", _phone2);
                paramList[11] = new SqlParameter("@P_AGENT_FAX", _fax );

                paramList[12] = new SqlParameter("@P_AGENT_TELEX", _telex );
                paramList[13] = new SqlParameter("@P_AGENT_EMAIL1", _email1 );
                paramList[14] = new SqlParameter("@P_AGENT_EMAIL2", _email2 );
                paramList[15] = new SqlParameter("@P_AGENT_WEBSITE", _website);
                paramList[16] = new SqlParameter("@P_AGENT_IMG_FILENAME", _imgFileName );

                paramList[17] = new SqlParameter("@P_N_FLEX_1", _nflex1);
                paramList[18] = new SqlParameter("@P_N_FLEX_2", _nflex2);
                paramList[19] = new SqlParameter("@P_AGENT_ALERT_BALANCE", _alertLevel);
                paramList[20] = new SqlParameter("@P_AGENT_STATUS", _status);
                paramList[21] = new SqlParameter("@P_AGENT_CREATED_BY", _createdBy );
                paramList[22] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[22].Direction = ParameterDirection.Output;
                paramList[23] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[23].Direction = ParameterDirection.Output;

                paramList[24] = new SqlParameter("@P_AGENT_ID_RET", SqlDbType.Int, 200);
                paramList[24].Direction = ParameterDirection.Output;

                paramList[25] = new SqlParameter("@P_agent_license_no", _licenseNo);
                if (_licenseExpDate != DateTime.MinValue) paramList[26] = new SqlParameter("@P_agent_license_exp_date", _licenseExpDate);
                paramList[27] = new SqlParameter("@P_agent_passport_no", _passportNo);
                if (_passportIssueDate != DateTime.MinValue) paramList[28] = new SqlParameter("@P_agent_passport_issue_date", _passportIssueDate);
                if (_passportExpDate != DateTime.MinValue) paramList[29] = new SqlParameter("@P_agent_passport_expiry_date", _passportExpDate);
                paramList[30] = new SqlParameter("@P_agent_nationality", _nationality);
                paramList[31] = new SqlParameter("@P_agent_guarantor_status", _grntrStatus);
                paramList[32] = new SqlParameter("@P_agent_visa_submission_status", _visaSubmissionStatus);
                paramList[33] = new SqlParameter("@P_AGENT_BLOCK_STATUS", _agentBlockStatus);
                paramList[34] = new SqlParameter("@P_AGENT_REMARKS", _agentRemarks);
                paramList[35] = new SqlParameter("@p_agent_reduce_balance", _reduceBalanceStatus);
                paramList[36] = new SqlParameter("@p_agent_currency", _agentCurrency);

                DBGateway.ExecuteNonQuery("ct_p_agent_add_update", paramList);
                string messageType = Utility.ToString(paramList[22].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[23].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_USER_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_user_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }

        public decimal UpdateBalance(decimal amount)
        {
            try
            {
                decimal currentBalance = 0;
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("p_agent_id", ID);
                paramList[1] = new SqlParameter("p_trans_amount", amount);
                paramList[2] = new SqlParameter("p_modified_by", _createdBy);
                paramList[3] = new SqlParameter("p_current_balance", SqlDbType.Decimal, 10);
                paramList[3].Direction = ParameterDirection.Output;


                DBGateway.ExecuteNonQuerySP("ct_p_agent_balance_update", paramList);

                currentBalance = Utility.ToDecimal(paramList[3].Value);
                return currentBalance;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Static Methods

        # endregion

        #region Static Methods
        public static DataTable GetList(int companyId, ListStatus status, RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];

                if(companyId>0)paramList[0] = new SqlParameter("@P_COMPANY_ID", companyId);
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("ct_p_agent_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        # endregion
    }
} 
