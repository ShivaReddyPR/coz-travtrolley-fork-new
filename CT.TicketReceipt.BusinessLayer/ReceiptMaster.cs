using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;


/// <summary>
/// Summary description for HandlingFeeMaster
/// </summary>
namespace CT.TicketReceipt.BusinessLayer
{
    public class ReceiptMaster
{
    # region members
    long _transactionId;
    string _docType;
    string _docNumber;
    DateTime  _docDate;
    string _paxName;

    string _paxMobileNo;
    

    
    int _adults;
    int _children;
    int _infants;
    decimal _fare;
    
    
    decimal _totalFare;
    long _locationId;
    string _remarks;
    string _status;
    long _createdBy;
    string _createdByName;
    string _settlementMode;
    string _currencyCode;
    decimal _exchangeRate;
    string _cardName;
    decimal _cardRate;
    long _cardId;
    decimal _cardTotal;

    decimal _baseToCollect;
    decimal _localToCollect;

    PaymentModeInfo _cashMode = new PaymentModeInfo();
    PaymentModeInfo _creditMode = new PaymentModeInfo();
    PaymentModeInfo _cardMode = new PaymentModeInfo();
    PaymentModeInfo _employeeMode = new PaymentModeInfo();
    PaymentModeInfo _othersMode = new PaymentModeInfo();
    string _modeRemarks;
    

    decimal _agentBalance;
    int _companyId;
    string _staffId;   //EmpName
    int _vehicleId;
    DateTime _fromDate;
    DateTime _toDate;
    string _notes;
    string _deleteReamrks;
    string _companyName;
    int _agentId;
    string _vehicleName;
    string _pendingCreditRcpts;
    int _employeeId;
    decimal _empTariffAmount;
    # endregion

    # region Properties
    public long TransactionId
    {
        get {return _transactionId ;}
        set { _transactionId = value; }
    }
    public string DocType
    {
        get {return _docType;}
        set { _docType = value; }
    }
    public string DocNumber
    {
        get { return _docNumber; }
        set { _docNumber = value; }
    }

    public DateTime DocDate
    {
        get { return _docDate; }
        set { _docDate = value; }
    }
    public string PaxName
    {
        get { return _paxName; }
        set { _paxName = value; }
    }

    public string PaxMobileNo
    {
        get { return _paxMobileNo; }
        set { _paxMobileNo = value; }
    }

    
    public int Adults
    {
        get { return _adults; }
        set { _adults = value; }
    }
    public int Children
    {
        get { return _children; }
        set { _children = value; }
    }
    public int Infants
    {
        get { return _infants; }
        set { _infants = value; }
    }
    public decimal Fare
    {
        get { return _fare; }
        set { _fare = value; }
    }
    
    public decimal TotalFare
    {
        get { return _totalFare; }
        set { _totalFare = value; }
    }

    public string Remarks
    {
        get { return _remarks; }
        set { _remarks = value; }
    }
    public long LocationId
    {
        get { return _locationId; }
        set { _locationId = value; }
    }
    public string Status
    {
        get { return _status; }
        set { _status = value; }
    }
    public long CreatedBy
    {
        get { return _createdBy; }
        set { _createdBy = value; }
    }
    public string CreatedByName
    {
        get { return _createdByName; }
        set { _createdByName = value; }
    }

    public string SettlementMode
    {
        get { return _settlementMode; }
        set { _settlementMode = value; }
    }
    public string CurrencyCode
    {
        get { return _currencyCode; }
        set { _currencyCode = value; }
    }
    public decimal ExchangeRate
    {
        get { return _exchangeRate; }
        set { _exchangeRate = value; }
    }
    public PaymentModeInfo CashMode
    {
        get { return _cashMode; }
        set { _cashMode = value; }
    }
    public PaymentModeInfo CreditMode
    {
        get { return _creditMode; }
        set { _creditMode = value; }
    }
    public PaymentModeInfo CardMode
    {
        get { return _cardMode; }
        set { _cardMode = value; }
    }
    public PaymentModeInfo EmployeeMode
    {
        get { return _employeeMode; }
        set { _employeeMode = value; }
    }
    public PaymentModeInfo OthersMode
    {
        get { return _othersMode; }
        set { _othersMode = value; }
    }
    public string ModeRemarks
    {
        get { return _modeRemarks; }
        set { _modeRemarks = value; }
    }

    public string CardName
    {
        get { return _cardName; }
        set { _cardName = value; }
    }
    public long CardId
    {
        get { return _cardId; }
        set { _cardId = value; }
    }

    public decimal  CardRate
    {
        get { return _cardRate; }
        set { _cardRate = value; }
    }
    public decimal CardTotal
    {
        get { return _cardTotal; }
        set { _cardTotal = value; }
    }

    public decimal BaseToCollect
    {
        get { return _baseToCollect; }
        set { _baseToCollect = value; }
    }

    public decimal LocalToCollect
    {
        get { return _localToCollect ; }
        set { _localToCollect = value; }
    }    

    public decimal AgentBalance
    {
        get { return _agentBalance; }
        set { _agentBalance = value; }
    }

    public int CompanyId
    {
        get { return _companyId; }
        set {_companyId=value; }       
    }

    public string StaffIdName
    {
        get { return _staffId; }
        set { _staffId = value; }
    }

    public int EmployeeId
    {
        get { return _employeeId; }
        set { _employeeId = value; }
    }

    public int VehicleId
    {
        get { return _vehicleId; }
        set { _vehicleId = value; }
    }

    public DateTime  FromDate
    {
        get { return _fromDate ; }
        set { _fromDate  = value; }
    }
    public DateTime  ToDate
    {
        get { return _toDate ; }
        set { _toDate = value; }
    }

    public string Notes
    {
        get { return _notes; }
        set { _notes = value; }
    }

    public string DeleteRemarks
    {
        get { return _deleteReamrks; }
        set { _deleteReamrks = value; }
    }

    public int AgentId
    {
        get { return _agentId; }
        set { _agentId  = value; }
    }

    public string CompanyName
    {
        get { return _companyName; }
        set { _companyName = value; }
    }
    public string VehicleName
    {
        get { return _vehicleName ; }
        set { _vehicleName = value; }
    }

    public string PendingCreditRcpts
    {
        get { return _pendingCreditRcpts; }
        set { _pendingCreditRcpts = value; }    
    }
    public decimal EmpTariffAmount
    {
        get { return _empTariffAmount; }
        set { _empTariffAmount = value;}
    
    }
    # endregion

    # region Constructors

    public ReceiptMaster()
    {
        //
        // TODO: Add constructor logic here
        //
        _transactionId = -1;
        DataSet ds = GetDetails(_transactionId);
        UpdateBusinessData(ds);
    }
    public ReceiptMaster(long transactionId)
    {
        try
        {
            DataSet ds = GetDetails(transactionId);
            UpdateBusinessData(ds);
        }
        catch { throw; }

    }
    # endregion

    # region Private Methods
    private DataSet  GetDetails(long transactionId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[3];
            SqlParameter param;
            param = new SqlParameter("@p_receipt_id", SqlDbType.Decimal);
            param.Value = transactionId; param.Size = 8;
            paramArr[0] = param;

            param = new SqlParameter("@p_doc_location_id", SqlDbType.Decimal);
            param.Value = Settings.LoginInfo.LocationID; param.Size = 8;
            paramArr[1] = param;
            paramArr[2] = new SqlParameter("@P_RECEIPT_AGENT_ID", Settings.LoginInfo.AgentId);


            DataSet ds = DBGateway.ExecuteQuery("ct_p_receipt_getdata", paramArr);
            return ds;
        }catch { throw; }
    }
    private void UpdateBusinessData(DataSet ds)
    {
        try
        {
            if (ds != null)
            {
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
               
            }


        }
        catch { throw; }
    }


    private void UpdateBusinessData(DataRow dr)
    {
        try
        {
            if (dr != null)
            {
                if (_transactionId < 0)
                {
                    _docType = Utility.ToString(dr["doc_type"]);
                    _docDate = Utility.ToDate(dr["receipt_date"]);
                }
                else
                {
                    _transactionId = Utility.ToLong(dr["receipt_id"]);
                    _docNumber = Utility.ToString(dr["receipt_number"]);
                    _docType = Utility.ToString(dr["receipt_type"]);
                    _docDate = Utility.ToDate(dr["receipt_date"]);
                    _paxName = Utility.ToString(dr["receipt_pax_name"]);
                  
                    _adults = Utility.ToInteger(dr["receipt_adults"]);
                    _children = Utility.ToInteger(dr["receipt_children"]);
                    _infants = Utility.ToInteger(dr["receipt_infants"]);
                    _fare = Utility.ToDecimal(dr["receipt_fare"]);
                  
                  
                    _totalFare = Utility.ToDecimal(dr["receipt_total_fee"]);
                    _remarks = Utility.ToString(dr["receipt_remarks"]);
                    _locationId = Utility.ToLong(dr["location_id"]);
                    _status = Utility.ToString(dr["receipt_status"]);
                    _createdBy = Utility.ToLong(dr["receipt_created_by"]);
                    _createdByName = Utility.ToString(dr["receipt_created_name"]);
                  
                  
                  
                    //Settlement Details
                    _settlementMode = Utility.ToString(dr["RECEIPT_settlement_mode"]);
                    _currencyCode = Utility.ToString(dr["RECEIPT_CURRENCY_CODE"]);
                    _exchangeRate = Utility.ToDecimal(dr["RECEIPT_EXCHANGE_RATE"]);
                    _cashMode.Set(Utility.ToString(dr["RECEIPT_SETTLEMENT_MODE_DESCRIPTION"]), Utility.ToDecimal(dr["cash_base_amount"]), Utility.ToDecimal(dr["cash_local_amount"]));
                    _creditMode.Set(Utility.ToString(dr["RECEIPT_SETTLEMENT_MODE_DESCRIPTION"]), Utility.ToDecimal(dr["credit_base_amount"]), Utility.ToDecimal(dr["credit_local_amount"]));
                    _cardMode.Set(Utility.ToString(dr["RECEIPT_SETTLEMENT_MODE_DESCRIPTION"]), Utility.ToDecimal(dr["card_base_amount"]), Utility.ToDecimal(dr["card_local_amount"]));
                    _employeeMode.Set(Utility.ToString(dr["RECEIPT_SETTLEMENT_MODE_DESCRIPTION"]), Utility.ToDecimal(dr["employee_base_amount"]), Utility.ToDecimal(dr["employee_local_amount"]));
                    _othersMode.Set(Utility.ToString(dr["RECEIPT_SETTLEMENT_MODE_DESCRIPTION"]), Utility.ToDecimal(dr["others_base_amount"]), Utility.ToDecimal(dr["others_local_amount"]));
                    _modeRemarks = Utility.ToString(dr["mode_remarks"]);

                    _cardId = Utility.ToLong(dr["crcard_id"]);
                    _cardName = Utility.ToString(dr["crcard_name"]);
                    _cardRate = Utility.ToDecimal(dr["crcard_rate"]);
                    _cardTotal = Utility.ToDecimal(dr["crcard_total_amount"]);
                    _baseToCollect = Utility.ToDecimal(dr["receipt_base_to_collect"]);
                    _localToCollect  = Utility.ToDecimal(dr["receipt_local_to_collect"]);

                    _paxMobileNo = Utility.ToString(dr["RECEIPT_PAX_MOBILENO"]);
                    _companyName = Utility.ToString(dr["RECEIPT_COMPANY_NAME"]);
                    _staffId = Utility.ToString(dr["receipt_staff_id"]);
                    _vehicleName = Utility.ToString(dr["VEHICLE_NAME"]);
                    _fromDate = Utility.ToDate(dr["receipt_from_date"]);
                    _toDate = Utility.ToDate(dr["receipt_to_date"]);
                    _notes = Utility.ToString(dr["receipt_notes"]);
                  
                }
            }


        }
        catch { throw; }

    }
    public void Save()
    {
          SqlTransaction trans = null;
        SqlCommand cmd = null;
        try
        {

            cmd = new SqlCommand();
            cmd.Connection = DBGateway.CreateConnection();
            cmd.Connection.Open();
            trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
            cmd.Transaction = trans;

        SqlParameter[] paramArr = new SqlParameter[54];
        SqlParameter param = new SqlParameter("@p_receipt_id", SqlDbType.BigInt);
        param.Value = _transactionId; param.Size = 8;
        paramArr[0] = param;


        param = new SqlParameter("@p_receipt_type", SqlDbType.NVarChar);
        param.Value = _docType; param.Size = 10;
        paramArr[1] = param;

        SqlParameter paramDocNo = new SqlParameter("@p_receipt_number", SqlDbType.NVarChar);
        paramDocNo.Direction= ParameterDirection.Output; paramDocNo.Size = 50;
        paramArr[2] = paramDocNo;

        param = new SqlParameter("@p_receipt_date", SqlDbType.DateTime);
        param.Value = _docDate; param.Size = 18;
        paramArr[3] = param;

        param = new SqlParameter("@p_receipt_pax_name", SqlDbType.NVarChar);
        param.Value = _paxName; param.Size = 50;
        paramArr[4] = param;

        if (!string.IsNullOrEmpty(_paxMobileNo)) paramArr[5] = new SqlParameter("@P_RECEIPT_PAX_MOBILENO", _paxMobileNo);

        param = new SqlParameter("@p_receipt_adults", SqlDbType.Int);
        param.Value = _adults; param.Size = 8;
        paramArr[6] = param;

        param = new SqlParameter("@p_receipt_children", SqlDbType.Int);
        param.Value = _children; param.Size = 8;
        paramArr[7] = param;

        param = new SqlParameter("@p_receipt_infants", SqlDbType.Int);
        param.Value = _infants; param.Size = 8;
        paramArr[8] = param;

        param = new SqlParameter("@p_receipt_fare", SqlDbType.Decimal);
        param.Value = _fare; param.Size = 18;
        paramArr[9] = param;       

        param = new SqlParameter("@p_receipt_total_fare", SqlDbType.Decimal);
        param.Value = _totalFare; param.Size = 18;
        paramArr[10] = param;       


        param = new SqlParameter("@p_receipt_remarks", SqlDbType.NVarChar);
        param.Value = _remarks; param.Size = 250;
        paramArr[11] = param;

        param = new SqlParameter("@p_receipt_location_id", SqlDbType.BigInt);
        param.Value = _locationId; param.Size = 18;
        paramArr[12] = param;

        paramArr[13] = new SqlParameter("@P_RECEIPT_AGENT_ID", _agentId );        

        //starting Mode
        paramArr[14] = new SqlParameter("@P_RECEIPT_SETTLEMENT_MODE", _settlementMode);
        paramArr[15] = new SqlParameter("@P_RECEIPT_CURRENCY_CODE", _currencyCode);
        paramArr[16] = new SqlParameter("@P_RECEIPT_EXCHANGE_RATE", _exchangeRate);
        paramArr[17] = new SqlParameter("@P_CASH_MODE", _cashMode.Mode);
        paramArr[18] = new SqlParameter("@P_CASH_BASE_AMOUNT", _cashMode.BaseAmount);
        paramArr[19] = new SqlParameter("@P_CASH_LOCAL_AMOUNT", _cashMode.LocalAmount);
        paramArr[20] = new SqlParameter("@P_CREDIT_MODE", _creditMode.Mode);
        paramArr[21] = new SqlParameter("@P_CREDIT_BASE_AMOUNT", _creditMode.BaseAmount);
        paramArr[22] = new SqlParameter("@P_CREDIT_LOCAL_AMOUNT", _creditMode.LocalAmount);
        paramArr[23] = new SqlParameter("@P_CARD_MODE", _cardMode.Mode);
        paramArr[24] = new SqlParameter("@P_CARD_BASE_AMOUNT", _cardMode.BaseAmount);
        paramArr[25] = new SqlParameter("@P_CARD_LOCAL_AMOUNT", _cardMode.LocalAmount);
        paramArr[26] = new SqlParameter("@P_EMPLOYEE_MODE", _employeeMode.Mode);
        paramArr[27] = new SqlParameter("@P_EMPLOYEE_BASE_AMOUNT", _employeeMode.BaseAmount);
        paramArr[28] = new SqlParameter("@P_EMPLOYEE_LOCAL_AMOUNT", _employeeMode.LocalAmount);
        paramArr[29] = new SqlParameter("@P_OTHERS_MODE", _othersMode.Mode);
        paramArr[30] = new SqlParameter("@P_OTHERS_BASE_AMOUNT", _othersMode.BaseAmount);
        paramArr[31] = new SqlParameter("@P_OTHERS_LOCAL_AMOUNT", _othersMode.LocalAmount);
        paramArr[32] = new SqlParameter("@P_MODE_REMARKS", _modeRemarks);

        paramArr[33] = new SqlParameter("@P_CRCARD_ID", _cardId);
        paramArr[34] = new SqlParameter("@P_CRCARD_NAME", _cardName);
        paramArr[35] = new SqlParameter("@P_CRCARD_RATE", _cardRate);
        paramArr[36] = new SqlParameter("@P_CRCARD_TOTAL_AMOUNT", _cardTotal);
        paramArr[37] = new SqlParameter("@P_RECEIPT_BASE_TO_COLLECT", _baseToCollect );
        paramArr[38] = new SqlParameter("@P_RECEIPT_LOCAL_TO_COLLECT", _localToCollect);
        //ending Mode

        param = new SqlParameter("@p_receipt_status", SqlDbType.NVarChar);
        param.Value = _status; param.Size = 10;
        paramArr[39] = param;


        param = new SqlParameter("@p_receipt_created_by", SqlDbType.BigInt);
        param.Value = _createdBy; param.Size = 8;
        paramArr[40] = param;

        SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
        paramMsgType.Size = 10;
        paramMsgType.Direction = ParameterDirection.Output;
        paramArr[41] = paramMsgType;

        SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
        paramMsgText.Size = 100;
        paramMsgText.Direction = ParameterDirection.Output;
        paramArr[42] = paramMsgText;


        paramArr[43] = new SqlParameter("@P_RECEIPT_COMPANY_ID", _companyId);
        paramArr[44] = new SqlParameter("@P_receipt_staff_id", _staffId );
        paramArr[45] = new SqlParameter("@P_receipt_vehicle_id", _vehicleId);
        if(_fromDate!=DateTime.MinValue) paramArr[46] = new SqlParameter("@P_receipt_from_date", _fromDate );
        if(_toDate!=DateTime.MinValue) paramArr[47] = new SqlParameter("@P_receipt_to_date", _toDate );
        if (!string.IsNullOrEmpty(_notes)) paramArr[48] = new SqlParameter("@P_receipt_notes", _notes );
        if (!string.IsNullOrEmpty(_deleteReamrks)) paramArr[49] = new SqlParameter("@p_vs_delete_remarks", _deleteReamrks);        

        SqlParameter paramRetValue = new SqlParameter("@P_RECEIPT_ID_RET", SqlDbType.BigInt);
        paramRetValue.Size = 100;
        paramRetValue.Direction = ParameterDirection.Output;
        paramArr[50] = paramRetValue;

        SqlParameter paramMsgText2 = new SqlParameter("@P_RET_AGENT_BALANCE", SqlDbType.Decimal);
        paramMsgText2.Size = 20;
        paramMsgText2.Direction = ParameterDirection.Output;
        paramArr[51] = paramMsgText2;

       if(_employeeId >0)paramArr[52] = new SqlParameter("@P_receipt_emp_id", _employeeId);
       if (_employeeId > 0) paramArr[53] = new SqlParameter("@P_receipt_emp_trf_amount", _empTariffAmount);   
       
        DBGateway.ExecuteNonQueryDetails(cmd,"ct_p_receipt_add_update", paramArr);
        if (Utility.ToString(paramMsgType.Value )== "E")
            throw new Exception(Utility.ToString(paramMsgText.Value));

        _docNumber = Utility.ToString(paramDocNo.Value);
        _transactionId = Utility.ToLong(paramRetValue.Value);

        if (!string.IsNullOrEmpty(_pendingCreditRcpts ))
        {
            string[] crRcptIds = _pendingCreditRcpts.Split(',');

            for (int x = 0; x <= crRcptIds.Length - 1; x++)
            {
                UpdateReciptsPayment(cmd, Utility.ToLong(crRcptIds[x].ToString()), _transactionId);
            }
            
        }
            
       trans.Commit();

    }
    catch
    {
        trans.Rollback();

        throw;
    }
    finally
    {
        cmd.Connection.Close();
    }
}

    public static void UpdateReciptsPayment(SqlCommand cmd, long creditRcptId,long cashRcptId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[4];
            paramArr[0] = new SqlParameter("@P_INVOICE_RCPT_ID", creditRcptId);
            paramArr[1] = new SqlParameter("@P_CASH_RECEIPT_ID", cashRcptId);

            SqlParameter paramMsgType = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[2] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@P_MSG_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[3] = paramMsgText;


            DBGateway.ExecuteNonQueryDetails(cmd, "CT_P_RECEIPT_PAYMENT_UPDATE", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }
    # endregion

    # region Static Methods
    public static DataSet GetList(DateTime fromDate, DateTime toDate,string accountedStatus, long userID, string memberType, long locationId, int agentId, string settlementMode, long receiptLocationId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[9];
            paramArr[0] = new SqlParameter("@p_from_date", fromDate);
            paramArr[1] = new SqlParameter("@p_to_date", toDate);
            SqlParameter param = new SqlParameter("@p_user_id", SqlDbType.BigInt);
            param.Value = userID; param.Size = 8;
            paramArr[2] = param;

            param = new SqlParameter("@p_user_type", SqlDbType.NVarChar);
            param.Value = memberType; param.Size = 50;
            paramArr[3] = param;

            param = new SqlParameter("@p_user_location_id", SqlDbType.BigInt);
            param.Value = locationId; param.Size = 16;
            paramArr[4] = param;
            if(accountedStatus!="A") paramArr[5] = new SqlParameter("@P_ACCOUNTED_STATUS", accountedStatus);
            if (settlementMode != "-1") paramArr[6] = new SqlParameter("@p_settlement_mode", settlementMode);
            if (agentId > 0) paramArr[7] = new SqlParameter("@p_receipt_agent_id", agentId);            
            if (receiptLocationId > 0) paramArr[8] = new SqlParameter("@P_RECEIPT_LOCATION_ID", receiptLocationId);
           
            return DBGateway.ExecuteQuery("ct_p_receipt_getlist", paramArr);
        }
        catch { throw; }
    }
    public static DataTable GetReportDetails(long transactionId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[1];
            SqlParameter param = new SqlParameter("@p_receipt_id", SqlDbType.Decimal);
            param.Value = transactionId; param.Size = 8;
            paramArr[0] = param;

            DataTable dt = DBGateway.ExecuteQuery("ct_p_receipt_getdata", paramArr).Tables[0];
            return dt;
        }
        catch { throw; }
    }

    public static void UpdateAccountedStatus(long receiptId, string accStatus, long createdBy)
    {
        try
        {
            //getting Disptach Document Next Serial
            SqlParameter[] paramArr = new SqlParameter[5];
            paramArr[0] = new SqlParameter("@P_RECEIPT_ID", receiptId);
            paramArr[1] = new SqlParameter("@P_ACCOUNTED_STATUS", accStatus);
//            paramArr[2] = new SqlParameter("@P_RECEIPT_AGENT_ID", Settings.LoginInfo.AgentId);
            paramArr[2] = new SqlParameter("@P_MODIFIED_BY", createdBy);

            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[3] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[4] = paramMsgText;

            DBGateway.ExecuteNonQuery("ct_p_receipt_accounted_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }

    public static DataTable GetROSCustomerList(ListStatus listStatus, RecordStatus recStatus)
    {

        try
        {
            SqlParameter[] paramList = new SqlParameter[2];
            if (recStatus != RecordStatus.All) paramList[0] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recStatus));
            paramList[1] = new SqlParameter("@P_LIST_STATUS", Utility.ToInteger(listStatus));
            return DBGateway.ExecuteQuery("P_ROS_CUSTOMER_MASTER_GETLIST", paramList).Tables[0];
        }
        catch { throw; }

    }

    public static DataSet PendingReciptList(int employeeId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[1];
            paramArr[0] = new SqlParameter("@P_RECEIPT_EMP_ID",employeeId );
            DataSet ds = DBGateway.ExecuteQuery("CT_P_PAYMENT_PENDING_RECEIPT_LIST", paramArr);
            return ds;
        }
        catch { throw; }
    }

    public static DataTable  GetEmployeeReceiptList(long empId,int companyId,string serviceStatus,string paidStatus,DateTime fromdate, DateTime toDate)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[6];
            if(empId>0) paramArr[0] = new SqlParameter("@P_EMP_ID", empId);
            if (companyId > 0) paramArr[1] = new SqlParameter("@P_company", companyId);
            if (!string.IsNullOrEmpty(serviceStatus)) paramArr[2] = new SqlParameter("@P_emp_service_status", serviceStatus);
            if (!string.IsNullOrEmpty(paidStatus)) paramArr[3] = new SqlParameter("@P_emp_paid_status", paidStatus);
            if (fromdate != DateTime.MinValue) paramArr[4] = new SqlParameter("@P_emp_recpt_from", fromdate);
            if (toDate != DateTime.MinValue) paramArr[5] = new SqlParameter("@P_emp_recpt_to", toDate);
            DataTable dt = DBGateway.ExecuteQuery("P_ROS_EMPLOYEE_PAYMENT_GETLIST", paramArr).Tables[0];
            return dt;
        }
        catch { throw; }
    }


    public static DataTable GetCompanyList(ListStatus listStatus, RecordStatus recStatus,string companyType)//companyType - R - Roster 
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[3];
            if (recStatus != RecordStatus.All) paramArr[0] = new SqlParameter("@P_RECORD_STATUS", Utility.ToInteger((char)recStatus));
            paramArr[1] = new SqlParameter("@P_LIST_STATUS", Utility.ToInteger(listStatus));
            if (!string.IsNullOrEmpty(companyType)) paramArr[2] = new SqlParameter("@P_COMPANY_TYPE", companyType );
            DataTable dt = DBGateway.ExecuteQuery("ct_p_company_getlist", paramArr).Tables[0];
            return dt;
        }
        catch { throw; }
    }

    public static DataTable GetReceiptStaffList(string staffType, int companyId,ListStatus status)//companyType - R - Roster 
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[3];
            if (!string.IsNullOrEmpty(staffType)) paramArr[0] = new SqlParameter("@P_EMP_TYPE", staffType );
            if (companyId > 0) paramArr[1] = new SqlParameter("@P_COMPANY_ID", companyId );
            paramArr[2] = new SqlParameter("@P_LIST_STATUS", status );
            DataTable dt = DBGateway.ExecuteQuery("P_ROS_RECEIPT_EMPLOYEE_GET_LIST", paramArr).Tables[0];
            return dt;
        }
        catch { throw; }
    }
    # endregion
}
}
