using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{


    public class OBVisaFeeMaster
    {
      private const long NEW_RECORD = -1;
        
      //  private long _id;

        #region Member Variables
        private long _transactionid;       
        private string _code;
        private string _name;        
        private long _countryId;
        private long _nationalityId;
        private long _visaTypeId;
        private long _residenceId;
        
        private string _visaCategory;
        private long _centerId;
        private string _presenceStatus;
        private decimal _adultVisaFee;
        private decimal _childVisaFee;
        private decimal _infantVisaFee;
        private decimal _adultService;
        private decimal _childService;
        private decimal _infantService;
        private string _status;                
        private long _createdBy;        

        #endregion


        #region Properties

        public long TransactionId
        {
            get { return _transactionid ; }
        }

       public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
               
        public long CountryID
        {
            get { return _countryId; }
            set { _countryId = value; }
        }
        public long NationalityId
        {
            get { return _nationalityId ; }
            set { _nationalityId  = value; }
        }
        public long VisaTypeId
        {
            get { return _visaTypeId; }
            set { _visaTypeId = value; }
        }

        public long ResidenceId
        {
            get { return _residenceId; }
            set { _residenceId = value; }
        }

        public long CenterID
        {
            get { return _centerId ; }
            set { _centerId = value; }
        }
        public string  VisaCategory
        {
            get { return _visaCategory; }
            set { _visaCategory = value; }
        }

        public string PresenceStatus
        {
            get { return _presenceStatus; }
            set { _presenceStatus = value; }
        }

        public decimal  AdultVisaFee
        {
            get { return _adultVisaFee ; }
            set { _adultVisaFee  = value; }
        }
        public decimal ChildVisaFee
        {
            get { return _childVisaFee ; }
            set { _childVisaFee = value; }
        }
        public decimal InfantVisaFee
        {
            get { return _infantVisaFee; }
            set { _infantVisaFee  = value; }
        }
        public decimal AdultService
        {
            get { return _adultService; }
            set { _adultService = value; }
        }
        public decimal   ChildService
        {
            get { return _childService ; }
            set { _childService = value; }
        }

        public decimal InfantService
        {
            get { return _infantService ; }
            set { _infantService  = value; }
        }   
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        #endregion


        #region Constructors
        public OBVisaFeeMaster()
        {
            _transactionid  = NEW_RECORD;
        }
        public OBVisaFeeMaster(long id)
        {
            _transactionid  = id;
            getDetails(_transactionid );
        
        }


        #endregion

#region Methods

        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_CHARGE_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("OBVISA_p_visa_fee_getData", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _transactionid = Utility.ToLong(dr["charge_id"]);
                _code  = Utility.ToString(dr["charge_code"]);
                _countryId = Utility.ToLong(dr["charge_country_id"]);
                _nationalityId = Utility.ToLong(dr["charge_nationality_id"]);

                _visaTypeId = Utility.ToLong(dr["charge_visa_type_id"]);
                _residenceId = Utility.ToLong(dr["charge_residence_id"]);
                _centerId = Utility.ToLong(dr["charge_center_id"]);
                _visaCategory =Utility.ToString(dr["charge_visa_category"]);
                _adultVisaFee = Utility.ToDecimal(dr["charge_adult"]);
                _childVisaFee  = Utility.ToDecimal(dr["charge_child"]);
                _infantVisaFee = Utility.ToDecimal(dr["charge_infant"]);
                _adultService = Utility.ToDecimal(dr["charge_acs_adult"]);
                _childService = Utility.ToDecimal(dr["charge_acs_child"]);
                _infantService = Utility.ToDecimal(dr["charge_acs_infant"]);
                _presenceStatus = Utility.ToString(dr["charge_presence_status"]);                
                _status = Utility.ToString(dr["charge_status"]);
                _createdBy = Utility.ToLong(dr["charge_created_by"]);

            }
            catch
            {
                throw;
            }
        }
       
#endregion

#region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[19];

                paramList[0] = new SqlParameter("@P_charge_ID", _transactionid);
                paramList[1] = new SqlParameter("@P_charge_CODE", _code);               
                paramList[2] = new SqlParameter("@P_charge_COUNTRY_ID", _countryId);
                if (_nationalityId != 0) paramList[3] = new SqlParameter("@P_charge_NATIONALITY_ID", _nationalityId);
                if (_visaTypeId != 0) paramList[4] = new SqlParameter("@P_charge_VISA_TYPE_ID", _visaTypeId);
                if (_residenceId != 0) paramList[5] = new SqlParameter("@P_charge_RESIDENCE_ID", _residenceId);
                if (_visaCategory != "0") paramList[6] = new SqlParameter("@P_CHARGE_VISA_CATEGORY", _visaCategory);
                paramList[7] = new SqlParameter("@P_CHARGE_PRESENCE_STATUS", _presenceStatus );
                paramList[8] = new SqlParameter("@P_CHARGE_CENTER_ID", _centerId );
                paramList[9] = new SqlParameter("@P_CHARGE_ACS_ADULT", _adultService);               
                paramList[10] = new SqlParameter("@P_CHARGE_ACS_CHILD", _childService );
                paramList[11] = new SqlParameter("@P_CHARGE_ACS_INFANT", _infantService );
                paramList[12] = new SqlParameter("@P_CHARGE_ADULT", _adultVisaFee);
                paramList[13] = new SqlParameter("@P_CHARGE_CHILD", _childVisaFee);
                paramList[14] = new SqlParameter("@P_CHARGE_INFANT", _infantVisaFee);
                paramList[15] = new SqlParameter("@P_charge_STATUS", _status);
                paramList[16] = new SqlParameter("@P_charge_CREATED_BY", _createdBy);
                
                
                paramList[17] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[17].Direction = ParameterDirection.Output;
                paramList[18] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[18].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("OBVISA_p_visa_fee_add_update", paramList);
                string messageType = Utility.ToString(paramList[17].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[18].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_charge_id", _transactionid );
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("OBVISA_p_visa_fee_getlist", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
#endregion

       
        #region Static Methods

        public static DataTable GetList(ListStatus status, RecordStatus recordStatus)
        {

            try
            {
             
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus!= RecordStatus.All) paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("[OBVISA_p_visa_fee_getlist]", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

       
        # endregion
       
    }
   
}
