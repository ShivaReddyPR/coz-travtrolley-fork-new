using System;
using System.Web;
using System.Data;

namespace CT.TicketReceipt.BusinessLayer
{
    public enum ListStatus
    {
        Short = 1,
        Long = 2

    }
    public enum RecordStatus
    {
        Activated = 65,//A
        Deactivated = 68,
        All=0

    }
    public class Settings
    {
        private const string LOGIN_SESSION = "_LoginInfoB2B";
        private const string MENULIST_SESSION = "_MenuListB2B";
        public  const string ACTIVE = "A";
        public const string DELETED = "D";
        //public const string DELETED = "D";
        //public const string DELETED = "D";
        //public const string DELETED = "D"; 
        private static System.Collections.Generic.List<string> _roleFunctionList;       

        public static System.Collections.Generic.List<string> roleFunctionList
        {
            get
            {
                //roleFunctionList = (Dictionary<int, List<int>>)GetStaticData(roleTask, MemCacheKeys.RoleTaskList);
                return _roleFunctionList;
            }
            //set
            //{
            //    roleFunctionList = value;
            //    //AddInMemCache(roleTask, MemCacheKeys.RoleTaskList);
            //}
        }
        public static LoginInfo LoginInfo
        {
            get 
            {
                LoginInfo loginInfo = (LoginInfo)HttpContext.Current.Session[LOGIN_SESSION];
                return loginInfo; 
            }
            set
            {
                HttpContext.Current.Session[LOGIN_SESSION] = value;
            }
        }
        public static DataSet MenuList
        {
            get
            {
                return (DataSet)HttpContext.Current.Session[MENULIST_SESSION];
            }
            set
            {

                DataRelation relation;
                value.DataSetName = "Menus";
                value.Tables[0].TableName = "Menu";
                //create Relation Parent and Child
                relation = new DataRelation("ParentChild", value.Tables["Menu"].Columns["func_id"], value.Tables["Menu"].Columns["func_Parent_id"], true);
                relation.Nested = true;
                value.Relations.Add(relation);
                HttpContext.Current.Session[MENULIST_SESSION] = value;
                _roleFunctionList = new System.Collections.Generic.List<string>();
                if (value.Tables[0] != null && value.Tables[0].Rows.Count > 0)
                {
                   
                    foreach (DataRow row in value.Tables[0].Rows)
                    {

                        _roleFunctionList.Add(Convert.ToString(row["FUNC_PAGE_LINK"]).ToUpper());
                    }
                }

            }
        }
        private Settings()
        {

        }
        
    }
}
