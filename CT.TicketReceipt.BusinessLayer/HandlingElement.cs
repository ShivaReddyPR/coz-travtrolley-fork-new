﻿using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.TicketReceipt.BusinessLayer
{
    public class HandlingElement
    {
        private int serviceId;
        private int referenceId;
        private int handlingId;
        private int status;
        private decimal amount;
        private int createdBy;
        private int adultCount=0;
        private int childCount = 0;
        private int infantCount = 0;

        public int ServiceId
        {
            get{ return serviceId;}
            set{ serviceId = value;}
        }
        public int ReferenceId
        {
            get { return referenceId; }
            set { referenceId = value; }
        }
        public int HandlingId
        {
            get { return handlingId; }
            set { handlingId = value; }
        }
        public int Status
        {
            get { return status; }
            set { status = value; }
        }
        public decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }      

        public int AdultCount
        {
            get { return adultCount; }
            set { adultCount = value; }
        }

        public int ChildCount
        {
            get { return childCount; }
            set { childCount = value; }
        }

        public int InfantCount
        {
            get { return infantCount; }
            set { infantCount = value; }
        }

        public void Save()
        {
            SqlParameter[] paramList = new SqlParameter[10];
            paramList[0] = new SqlParameter("@ServiceId", serviceId);
            paramList[1] = new SqlParameter("@RefferenceId", referenceId);
            paramList[2] = new SqlParameter("@HandlingId", handlingId);
            paramList[3] = new SqlParameter("@Amount", amount);
            paramList[4] = new SqlParameter("@Status", status);
            paramList[5] = new SqlParameter("@CreatedBy", createdBy);
            paramList[6] = new SqlParameter("@AdultCount", adultCount);
            paramList[7] = new SqlParameter("@ChildCount", childCount);
            paramList[8] = new SqlParameter("@InfantCount", infantCount);
            DBGateway.ExecuteNonQuerySP("BKE_ADD_HANDLINGFEE_ELEMENT", paramList);
        }

        public static List<HandlingElement> Load(int reference)
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ReferenceId", reference);
            DataTable dtHandling = DBGateway.FillDataTableSP("GET_HANDLING_FEE_ELEMENT_DETAILS", paramList);
            List<HandlingElement> ElementArray = new List<HandlingElement>();
            if (dtHandling != null && dtHandling.Rows.Count > 0)
            {
                foreach (DataRow dr in dtHandling.Rows)
                {
                    HandlingElement Element = new HandlingElement();
                    Element.ServiceId = int.Parse(dr["service_id"].ToString());
                    Element.ReferenceId = int.Parse(dr["reference_id"].ToString());
                    Element.HandlingId = int.Parse(dr["handling_id"].ToString());
                    Element.Amount = decimal.Parse(dr["amount"].ToString());
                    Element.AdultCount = int.Parse(dr["adult_count"].ToString());
                    Element.ChildCount = int.Parse(dr["child_count"].ToString());
                    Element.InfantCount = int.Parse(dr["infant_count"].ToString());
                    ElementArray.Add(Element);
                }      
            }
            return ElementArray;
        }

        public static List<HandlingElement> LoadByFlightId(int flightId)
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            DataTable dtHandling = DBGateway.FillDataTableSP("GET_HANDLINGELEMENT_BY_FLIGHT_ID", paramList);
            List<HandlingElement> ElementArray = new List<HandlingElement>();
            if (dtHandling != null && dtHandling.Rows.Count > 0)
            {
                foreach (DataRow dr in dtHandling.Rows)
                {
                    HandlingElement Element = new HandlingElement();
                    Element.ServiceId = int.Parse(dr["service_id"].ToString());
                    Element.ReferenceId = int.Parse(dr["reference_id"].ToString());
                    Element.HandlingId = int.Parse(dr["handling_id"].ToString());
                    Element.Amount = decimal.Parse(dr["amount"].ToString());
                    Element.AdultCount = int.Parse(dr["adult_count"].ToString());
                    Element.ChildCount = int.Parse(dr["child_count"].ToString());
                    Element.InfantCount = int.Parse(dr["infant_count"].ToString());
                    ElementArray.Add(Element);
                }
            }
            return ElementArray;
        }

        public void Update()
        {
            SqlParameter[] paramList = new SqlParameter[10];
            paramList[0] = new SqlParameter("@ServiceId", serviceId);
            paramList[1] = new SqlParameter("@RefferenceId", referenceId);
            paramList[2] = new SqlParameter("@HandlingId", handlingId);
            paramList[3] = new SqlParameter("@Amount", amount);
            paramList[4] = new SqlParameter("@Status", status);
            paramList[5] = new SqlParameter("@CreatedBy", createdBy);
            paramList[6] = new SqlParameter("@AdultCount", adultCount);
            paramList[7] = new SqlParameter("@ChildCount", childCount);
            paramList[8] = new SqlParameter("@InfantCount", infantCount);
            DBGateway.ExecuteNonQuerySP("UPDATE_HANDLINGFEE_ELEMENT_BY_PRICEID", paramList);
        }
    }
}
