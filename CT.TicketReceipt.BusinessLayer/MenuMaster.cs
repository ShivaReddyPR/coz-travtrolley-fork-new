using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class MenuMaster
    {
        private const long NEW_RECORD = -1;
        
        
       
        #region Static Methods
        public static DataSet GetList(long userId)
        {

            try
            {
                SqlParameter[] paramArr = new SqlParameter[4];

                paramArr[0] = new SqlParameter("@P_USER_ID", userId);
                paramArr[1] = new SqlParameter("@P_USER_MEMBER_TYPE", Settings.LoginInfo.MemberType.ToString());
                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[2] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[3] = paramMsgText;

                return DBGateway.ExecuteQuery("ct_p_user_function_getlist", paramArr);
            }
            catch
            {
                throw;
            }
        }
        # endregion
       
    }
   
}
