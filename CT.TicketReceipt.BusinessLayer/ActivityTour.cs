﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace CT.TicketReceipt.BusinessLayer
{

    /// <summary>
    /// Summary description for ActivityTour
    /// </summary>
    public class ActivityTour
    {
        private int New_Record = -1;
        private int tourId;
        private int createdBy;
        private int modifiedBy;
        private string countryCode;
        private string cityCode;
        private string activityName;
        private string url;
        private string activityId;
        


        #region Properties

        public int ID
        {
            get { return tourId; }
            set { tourId = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public int ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; }
        }
        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }


        public string ActivityName
        {
            get { return activityName; }
            set { activityName = value; }
        }

        public string URL
        {
            get { return url; }
            set { url = value; }
        }
        public string ActivityId
        {
            get { return activityId; }
            set { activityId = value; }
        }
       
        #endregion

        public ActivityTour()
        {
            tourId = New_Record;
            //
            // TODO: Add constructor logic here
            //
        }
        public ActivityTour(int id)
        {


            tourId = id;
            getDetails(id);
        }
        private void getDetails(int id)
        {
            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }
        }
        private DataSet GetData(int id)
        {
            //SqlDataReader data = null;
            DataSet ds = new DataSet();
            // SqlConnection con=new SqlConnection(ConfigurationManager.ConnectionStrings["TekTravel_newConnectionString1"].ConnectionString);
            //using (SqlConnection connection = CT.TicketReceipt.DataAccessLayer.DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@TourId", id);
                    //data = CT.TicketReceipt.DataAccessLayer.DBGateway.ExecuteReaderSP("usp_CozmoTour_GetData", paramList, connection);
                    ds = CT.TicketReceipt.DataAccessLayer.DBGateway.FillSP("usp_CozmoTour_GetData", paramList);
                    //if (data != null)
                    //{
                    //    DataTable dt = new DataTable();
                    //    dt.Load(data);
                    //    ds.Tables.Add(dt);
                    //}
                    //connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return ds;
        }

        private void UpdateBusinessData(DataRow dr)
        {

            if (tourId < 0)
            {
                tourId = Convert.ToInt32(dr["TourId"]);
            }
            else
            {
                tourId = Convert.ToInt32(dr["TourId"]);
                countryCode = dr["CountryCode"].ToString();
                cityCode = dr["CityCode"].ToString();
                activityName = dr["ActivityName"].ToString();
                activityId = dr["ActivityId"].ToString();
                url = dr["URL"].ToString();
            }

        }
        public void Save()
        {
            try
            {
                int rowsAffected = 0;


                SqlParameter[] paramList = new SqlParameter[10];


                paramList[0] = new SqlParameter("@TourId", tourId);
                paramList[1] = new SqlParameter("@countryCode", countryCode);
                paramList[2] = new SqlParameter("@cityCode", cityCode);
                paramList[3] = new SqlParameter("@activityName", activityName);
                paramList[4] = new SqlParameter("@url", url);
                paramList[5] = new SqlParameter("@createdBy", createdBy);
                paramList[6] = new SqlParameter("@activityId", "");
                paramList[7] = new SqlParameter("@modifiedBy", modifiedBy);
                paramList[8] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[8].Direction = ParameterDirection.Output;
                paramList[9] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[9].Direction = ParameterDirection.Output;
                rowsAffected = CT.TicketReceipt.DataAccessLayer.DBGateway.ExecuteNonQuerySP("usp_CozmoTours_add_update", paramList);
                string messageType = paramList[8].Value.ToString();
                if (messageType == "E")
                {
                    string message = paramList[9].Value.ToString();
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

        }
     

        #region StaticMethod
        public static DataTable GetList()
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();

            using (SqlConnection connection = CT.TicketReceipt.DataAccessLayer.DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[0];


                    data = CT.TicketReceipt.DataAccessLayer.DBGateway.ExecuteReaderSP("usp_CozmoTour_GetList", paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }

        
        #endregion
    }
}
