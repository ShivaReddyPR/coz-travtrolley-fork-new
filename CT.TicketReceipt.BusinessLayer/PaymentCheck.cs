﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class PaymentCheck
    {
        public DataSet GetRequestQueue(string Status)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@Status", Status);
                DataSet dtResult = DBGateway.ExecuteQuery("paymentCheck_getQueuesByStatus", paramList);
                return dtResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetStatusDetails(string queueName)
        {
            try
            {
                DataSet ds = new DataSet();

                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("Status", typeof(string)));
                dt.Columns.Add(new DataColumn("StatusId", typeof(string)));

                DataRow dr0 = dt.NewRow();
                dr0["Status"] = "Select";
                dr0["StatusId"] = "-1";
                dt.Rows.Add(dr0);
                if (queueName == "Maker")
                {
                    DataRow dr1 = dt.NewRow();
                    dr1["Status"] = "Pending";
                    dr1["StatusId"] = "P";
                    dt.Rows.Add(dr1);
                }
                DataRow dr2 = dt.NewRow();
                dr2["Status"] = "Release for Checker";
                dr2["StatusId"] = "RC";
                dt.Rows.Add(dr2);
                DataRow dr3 = dt.NewRow();
                dr3["Status"] = "Rejected";
                dr3["StatusId"] = "R";
                dt.Rows.Add(dr3);
                //DataRow dr4 = dt.NewRow();
                //dr4["Status"] = "Release from Queue";
                //dr4["StatusId"] = "RQ";
                //dt.Rows.Add(dr4);
                DataRow dr5 = dt.NewRow();
                dr5["Status"] = "Release for App";
                dr5["StatusId"] = "RA";
                dt.Rows.Add(dr5);
                ds.Tables.Add(dt);

                return ds;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public int updateMakerQueueStatus(int uniqueId, string status, int userId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@Id", uniqueId);
                paramList[1] = new SqlParameter("@Status", status);
                paramList[2] = new SqlParameter("@UserId", userId);
                int result = DBGateway.ExecuteNonQuerySP("paymentCheck_UpdateQueueStatusByIds", paramList);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return dtResult;
        }

        public DataTable getUploadFilesbyId(Int64 id)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@Id", id);
                DataTable dt = DBGateway.FillDataTableSP("Queue_GetUploadedFilesById", paramList);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int SaveDocClicks(Int64 ChequeId, Int64 DocId, string IpAdddress, Int64 UserId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@ChequeId", ChequeId);
                paramList[1] = new SqlParameter("@DocId", DocId);
                paramList[2] = new SqlParameter("@IpAddress", IpAdddress);
                paramList[3] = new SqlParameter("@UserId", UserId);

                int result = DBGateway.ExecuteNonQuery("Save_Cheque_Download_Logs", paramList);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveUploadInvoices(Int64 ChequeId, string DocType, string DocName, string DocPath, Int64 UserId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                paramList[0] = new SqlParameter("@ChequeId", ChequeId);
                paramList[1] = new SqlParameter("@DocType", DocType);
                paramList[2] = new SqlParameter("@DocName", DocName);
                paramList[3] = new SqlParameter("@DocPath", DocPath);
                paramList[4] = new SqlParameter("@CreatedBy", UserId);

                int result = DBGateway.ExecuteNonQuery("saveChequeInvoices", paramList);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int saveExcelFileDetails(string DocName,string DocNo,string FileName,string DocType,string DocStatus,string Remarks,Int64 UserId)
        {
            try
            {
                SqlParameter[] paramList=new SqlParameter[7];
                paramList[0] = new SqlParameter("@DocName", DocName);
                paramList[1] = new SqlParameter("@DocNo", DocNo);
                paramList[2] = new SqlParameter("@FileName", FileName);
                paramList[3] = new SqlParameter("@DocType", DocType);
                paramList[4] = new SqlParameter("@DocStatus", DocStatus);
                paramList[5] = new SqlParameter("@Remraks", Remarks);
                paramList[6] = new SqlParameter("@CreatedBy", UserId);
               

                int result = DBGateway.ExecuteNonQuery("SaveExcelUploadDetails", paramList);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
