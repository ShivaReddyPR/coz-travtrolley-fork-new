using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class NationalityMaster
    {
        private const long NEW_RECORD = -1;
        
        
       
        #region Static Methods
        public static DataTable GetList(ListStatus status,RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];

                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", recordStatus);

                return DBGateway.ExecuteQuery("vis_p_nationality_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        # endregion
       
    }
   
}
