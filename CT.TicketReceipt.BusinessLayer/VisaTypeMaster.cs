using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{    
    public class VisaTypeMaster
    {
        private const long NEW_RECORD = -1;
        
        private long _id;
       
        #region Static Methods
        public static DataTable GetList(ListStatus status, RecordStatus recordStatus, int countryId,int agentId)
        {

            try
            {
                return GetList(VisaService.InBound,status, recordStatus,0,agentId );
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetList(VisaService vService, ListStatus status, RecordStatus recordStatus, int countryId,int agentId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                if (vService != VisaService.All) paramList[0] = new SqlParameter("@P_VISA_SERVICE", Utility.ToString((char)vService));
                if(countryId>0) paramList[1] = new SqlParameter("@P_VISA_TYPE_COUNTRY_ID", countryId);
                if (agentId > 0) paramList[2] = new SqlParameter("@P_VISA_TYPE_AGENT_ID", agentId);
                paramList[3] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[4] = new SqlParameter("@P_RECORD_STATUS", recordStatus);

                return DBGateway.ExecuteQuery("visa_p_visa_type_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        # endregion
       
    }
   
}
