using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class CustomerQuery
    {
        private const long NEW_RECORD = -1;
        
       
        #region Static Methods
        public static DataSet GetStatusDetails(string docNo,string passportNo,ListStatus status,RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[4];

                paramList[0] = new SqlParameter("@P_VS_DOC_NO", docNo);
                paramList[1] = new SqlParameter("@P_PAX_PASSPORT_NO", passportNo);
                paramList[2] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[3] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("visa_p_visa_sales_getCustomerStatus", paramList);
            }
            catch
            {
                throw;
            }
        }
        # endregion
       
    }
   
}
