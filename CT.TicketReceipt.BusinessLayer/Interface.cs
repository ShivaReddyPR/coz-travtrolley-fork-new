using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using CT.TicketReceipt.Common;
namespace CT.TicketReceipt.Interface
{
    public enum MappingElement
     {
         CASH_BOOK = 1,
         BANK_BOOK=2,
         VISA_FEE=3,
         INS_FEE = 4,
         SEC_DEPOSIT = 5,
         INCOME = 6,
         APT_DEPOSIT = 7,
         CARD_FEE = 8,
         TICKET_FEE = 9

     }
    public static class UtilityInter
    {
        public  static void SaveInterface(CT.TicketReceipt.BusinessLayer.TranxVisaSales visaSales, DataTable VisaPaxList, DataTable AllocationList,DataTable MappingList, DropDownList ddlAdults, DropDownList ddlChildren, DropDownList ddlInfants, CheckBox chkAllPax, DropDownList ddlSettlementMode,HiddenField hdfPNRSettMode,
            TextBox txtToCollect, TextBox txtCollectSecurity, TextBox txtDiscount, TextBox txtCollectTicketCost, TextBox txtTotCardCharge, TextBox txtChequeAmount, TextBox txtGurantorName, TextBox txtChequeNo, TextBox txtChequeBank,
            HiddenField hdfPNRUCCF, HiddenField  hdfVElmntVisaFee,HiddenField hdfVElmntIncomeFee ,HiddenField hdfVElmntProcessFee,HiddenField hdfVElmntInsuranceFee,HiddenField hdfVElmntAptIncome,HiddenField hdfVElmntMeetAssistDeposit, HiddenField  hdfVElmntMeetAssistIncome,HiddenField hdfVElmntQuickCheckDeposit,
            HiddenField hdfVElmntQuickCheckIncome,HiddenField hdfVElmntDownloadFee,HiddenField hdfVElmntVisaUrgentFee,HiddenField hdfVElmntAptDeposit,RadioButtonList rdblDepositMode ,CheckBox chkUrgentFee,CheckBox chkMeetAssist,CheckBox chkQuickCheck,CheckBox chkAptDeposit,CheckBox chkDownloadFee,DateTime dcDocDate)
        {
            try
            {
                AllocationList.Clear();
                string paxDetails = string.Empty;
                string paxName = string.Empty;
                string pspNO = string.Empty;
                foreach (DataRow drPax in VisaPaxList.Rows)
                {
                    paxName += string.Format("{0},", Utility.ToString(drPax["pax_visitor_name"]));
                    pspNO += string.Format("{0},", Utility.ToString(drPax["pax_visitor_passport_no"]));
                    //paxDetails += string.Format("{0}:{1},", Utility.ToString(drPax["pax_visitor_name"]), Utility.ToString(drPax["pax_visitor_passport_no"]));
                }
                paxDetails = string.Format("{0}/{1}", pspNO, paxName);
                int cntAdultChild = Utility.ToInteger(ddlAdults.SelectedItem.Value) + Utility.ToInteger(ddlChildren.SelectedItem.Value);
                int cntInfant = Utility.ToInteger(ddlInfants.SelectedItem.Value);
                int paxCount = (!chkAllPax.Checked) ? (Utility.ToInteger(ddlAdults.SelectedItem.Value) + Utility.ToInteger(ddlChildren.SelectedItem.Value) + Utility.ToInteger(ddlInfants.SelectedItem.Value)) : 1;
                //int cntInfant= Utility.ToInteger(ddlInfants.SelectedItem.Value);
                //decimal infantFee = Utility.ToDecimal(hdfSalesInfantFee.Value) * Utility.ToInteger(ddlInfants.SelectedItem.Value);
                //decimal infantIncome= Utility.ToDecimal(hdfSalesInfantIncome.Value) * Utility.ToInteger(ddlInfants.SelectedItem.Value);
                //decimal infantVisaFee= Utility.ToDecimal(hdfSalesInfantVfee.Value) * Utility.ToInteger(ddlInfants.SelectedItem.Value);
                //decimal infantIncome = Utility.ToDecimal(hdfSalesInfantIncome.Value) * cntInfant;
                //decimal infantVisaFee = Utility.ToDecimal(hdfSalesInfantVfee.Value) * cntInfant;
                string settlementMode = ddlSettlementMode.SelectedItem.Value.Trim();
                string pnrUCCF = hdfPNRUCCF.Value.Trim();
                string pnrSettMode = hdfPNRSettMode.Value.Trim();
                string headerElement = string.Empty;
                decimal totAmount = Utility.ToDecimal(txtToCollect.Text.Trim());
                decimal ticketFare = Utility.ToDecimal(txtCollectTicketCost.Text.Trim());
                decimal visaFee = (Utility.ToDecimal(hdfVElmntVisaFee.Value.Trim()) * paxCount);//+ infantVisaFee;// +infantFee;
                decimal incomeFee = (Utility.ToDecimal(hdfVElmntIncomeFee.Value.Trim()) * paxCount);// +infantIncome;
                decimal visaProcessFee = Utility.ToDecimal(hdfVElmntProcessFee.Value.Trim()) * paxCount;
                decimal insuranceFee = (Utility.ToDecimal(hdfVElmntInsuranceFee.Value.Trim()) * paxCount);
                decimal airportDeposit = 0;
                decimal airportIncome = (Utility.ToDecimal(hdfVElmntAptIncome.Value.Trim()) * (paxCount));
                decimal meetAssistDeposit = (Utility.ToDecimal(hdfVElmntMeetAssistDeposit.Value.Trim()) * (paxCount));
                decimal meetAssistIncome = (Utility.ToDecimal(hdfVElmntMeetAssistIncome.Value.Trim()) * (paxCount));
                decimal quickCheckDeposit = (Utility.ToDecimal(hdfVElmntQuickCheckDeposit.Value.Trim()) * (paxCount));
                decimal quickCheckIncome = (Utility.ToDecimal(hdfVElmntQuickCheckIncome.Value.Trim()) * (paxCount));
                decimal secCashDeposit = rdblDepositMode.SelectedValue == "CASH" ? Utility.ToDecimal(txtChequeAmount.Text.Trim()) : 0;
                decimal secDeposit = Utility.ToDecimal(txtCollectSecurity.Text.Trim());// +secCashDeposit;
                decimal downloadFee = (Utility.ToDecimal(hdfVElmntDownloadFee.Value.Trim()) * (cntAdultChild + cntInfant));
                decimal urgentFee = (Utility.ToDecimal(hdfVElmntVisaUrgentFee.Value.Trim()) * (cntAdultChild + cntInfant));
                decimal discount = Utility.ToDecimal(txtDiscount.Text.Trim());
                incomeFee = incomeFee - discount;

                visaFee = visaFee + insuranceFee;// insurance element shloud be added into visa fee account
                insuranceFee = 0;
                if (chkUrgentFee.Checked) incomeFee = incomeFee + urgentFee;
                if (rdblDepositMode.SelectedValue == "CASH") incomeFee = incomeFee + Utility.ToDecimal(txtChequeAmount.Text.Trim());
                if (chkMeetAssist.Checked)
                {
                    meetAssistDeposit = (Utility.ToDecimal(hdfVElmntMeetAssistDeposit.Value.Trim()) * (paxCount));
                    meetAssistIncome = (Utility.ToDecimal(hdfVElmntMeetAssistIncome.Value.Trim()) * (paxCount));
                }
                if (chkQuickCheck.Checked)
                {
                    quickCheckDeposit = (Utility.ToDecimal(hdfVElmntQuickCheckDeposit.Value.Trim()) * (paxCount));
                    quickCheckIncome = (Utility.ToDecimal(hdfVElmntQuickCheckIncome.Value.Trim()) * (paxCount));
                }
                decimal crCardFee = Utility.ToDecimal(txtTotCardCharge.Text.Trim());

                totAmount = totAmount - ticketFare;
                if (chkAptDeposit.Checked) airportDeposit = (Utility.ToDecimal(hdfVElmntAptDeposit.Value.Trim()) * (cntAdultChild + cntInfant));

                object[,] arrElementValue ={ { "VISA_FEE", visaFee }, {"INS_FEE",insuranceFee}, {"SEC_DEPOSIT",secDeposit },
                {"INCOME",incomeFee }, {"APT_DEPOSIT",airportDeposit },{string.Empty,0}, {string.Empty,0},{string.Empty,0},{string.Empty,0},{"PROCESS_FEE",visaProcessFee},
                                       {"MEET_ASSIST_DEPOSIT",visaProcessFee},{"MEET_ASSIST_INCOME",visaProcessFee},{"QUICK_CHECKOUT_DEPOSIT",visaProcessFee},{"QUICK_CHECKOUT_INCOME",visaProcessFee}};
                //{"INCOME",incomeFee }, {"APT_DEPOSIT",airportDeposit },{string.Empty,0}, {string.Empty,0},{"DOWNLOAD_FEE",downloadfee},{"APT_DEPOSIT_INCOME",airportIncome}};

                if (settlementMode == "1")// both Cash
                {
                    headerElement = MappingElement.CASH_BOOK.ToString();
                }
                else if (settlementMode == "3") // both crCard
                {
                    headerElement = MappingElement.BANK_BOOK.ToString();
                    arrElementValue[5, 0] = "CARD_FEE";
                    arrElementValue[5, 1] = crCardFee;
                    if (pnrUCCF == "N")
                    {
                        arrElementValue[6, 0] = "TICKET_FEE";
                        arrElementValue[6, 1] = ticketFare;
                        totAmount = totAmount + ticketFare;// card rate applied
                    }
                }
                else if (settlementMode == "4")// Cash & Credit
                {
                    if (pnrSettMode == "2")//Visa=Cash,Ticket=Credit
                    {
                        headerElement = MappingElement.CASH_BOOK.ToString();
                    }
                }
                else if (settlementMode == "5")// Cash & crCredit
                {
                    if (pnrSettMode == "3")//Visa=Cash,Ticket=crCredit
                    {
                        //arrElementValue[5, 0] = "CARD_FEE";
                        //arrElementValue[5, 1] = crCardFee;
                        totAmount = totAmount - crCardFee;// card rate applied
                        headerElement = MappingElement.CASH_BOOK.ToString();
                    }
                    else if (pnrSettMode == "1")//Visa=crCredit,Ticket=Cash
                    {
                        headerElement = MappingElement.BANK_BOOK.ToString();
                        arrElementValue[5, 0] = "CARD_FEE";
                        arrElementValue[5, 1] = crCardFee;
                    }
                }
                if (chkDownloadFee.Checked)
                {
                    arrElementValue[7, 0] = "DOWNLOAD_FEE";
                    arrElementValue[7, 1] = downloadFee;
                }
                if (chkAptDeposit.Checked)
                {
                    arrElementValue[8, 0] = "APT_DEPOSIT_INCOME";
                    arrElementValue[8, 1] = airportIncome;
                }
                if (chkMeetAssist.Checked)
                {
                    arrElementValue[10, 0] = "MEET_ASSIST_DEPOSIT";
                    arrElementValue[10, 1] = meetAssistDeposit;

                    arrElementValue[11, 0] = "MEET_ASSIST_INCOME";
                    arrElementValue[11, 1] = meetAssistIncome;
                }
                if (chkQuickCheck.Checked)
                {
                    arrElementValue[12, 0] = "QUICK_CHECKOUT_DEPOSIT";
                    arrElementValue[12, 1] = quickCheckDeposit;

                    arrElementValue[13, 0] = "QUICK_CHECKOUT_INCOME";
                    arrElementValue[13, 1] = quickCheckIncome;
                }

                DataRow dr = MappingList.Rows.Find(headerElement);
                if (dr == null) throw new Exception("Header Element not Found in Mapping Table");
                CT.TicketReceipt.BusinessLayer.VSORAInterface _objORAinterface = new CT.TicketReceipt.BusinessLayer.VSORAInterface();
                _objORAinterface.BaseDocType = "RCT";
                _objORAinterface.CompanyId = "1";
                _objORAinterface.DocType = Utility.ToString(dr["MAP_DOC_TYPE"]);
                _objORAinterface.DocDate = dcDocDate;
                _objORAinterface.CashBankCode = Utility.ToString(dr["ACCT_CODE"]);//Account Code
                _objORAinterface.CurrencyCode = "AED";
                _objORAinterface.ExchangeRate = 1M;
                _objORAinterface.CashMode.BaseAmount = totAmount;
                _objORAinterface.CashMode.LocalAmount = totAmount;
                //_objORAinterface.OtherReference = txtDocumentNumber.Text.Trim();
                _objORAinterface.ChequeNo = (string.IsNullOrEmpty(txtChequeNo.Text)) ? "123" : txtChequeNo.Text;
                string chequeDetails = string.Format("{0}/{1}/{2}", txtChequeBank.Text.Trim(), txtChequeNo.Text.Trim(), txtChequeAmount.Text.Trim());
                _objORAinterface.Narration = (rdblDepositMode.SelectedValue.ToString() == "CHEQUE" ? chequeDetails : string.Empty) + "/" + paxDetails;
                _objORAinterface.GLAccounts = Utility.ToString(dr["GL_ACCT_CODE"]);
                _objORAinterface.Notes = txtGurantorName.Text.ToUpper().Trim();
                _objORAinterface.Flex1 = "IB VISA";


                //Inter face Details

                for (int i = 0; i < arrElementValue.Length / arrElementValue.Rank; i++)
                {
                    DataRow drMapping = MappingList.Rows.Find(arrElementValue[i, 0]);
                    if (drMapping != null)
                    {
                        _objORAinterface.Map_location_code = Utility.ToString(drMapping["MAP_LOCATION_CODE"]);// TO Assign Location to interface Header,from VMS mapping details
                        //decimal feeAmount = Utility.ToString(arrElementValue[i, 0]) == MappingElement.VISA_FEE.ToString() ? (Utility.ToDecimal(arrElementValue[i, 1]) * cntAdultChild) : Utility.ToDecimal(arrElementValue[i, 1]);
                        decimal feeAmount = Utility.ToDecimal(arrElementValue[i, 1]);
                        if (feeAmount > 0)
                        {
                            DataRow drAllocation = AllocationList.NewRow();
                            drAllocation["ALOC_ALLOCATION_TYPE"] = Utility.ToString(drMapping["MAP_MODULE"]);
                            drAllocation["ALOC_FOREIGN_AMOUNT"] = feeAmount;
                            drAllocation["ALOC_LOCAL_AMOUNT"] = feeAmount;
                            drAllocation["ALOC_NARRATION"] = (rdblDepositMode.SelectedValue.ToString() == "CHEQUE" ? chequeDetails : string.Empty) + "," + paxDetails; ;
                            drAllocation["ALOC_NOTES"] = Utility.ToString(arrElementValue[i, 0]);
                            //if (Utility.ToString(arrElementValue[i, 0]) == MappingElement.INCOME.ToString())
                            if (Utility.ToString(drMapping["MAP_MODULE"]) == "INCOME_GL")
                            {
                                drAllocation["ALOC_DETAIL_1"] = Utility.ToString(drMapping["MAP_LOCATION_CODE"]);
                                drAllocation["ALOC_DETAIL_2"] = Utility.ToString(drMapping["ACCT_CODE"]);
                                if (Utility.ToString(arrElementValue[i, 0]) == "INCOME" && discount > 0)
                                {
                                    //drAllocation["ALOC_SEG_7"] = "Discount:"+discount;
                                    drAllocation["ALOC_SEG_7"] = discount;
                                }

                            }
                            else
                            {
                                drAllocation["ALOC_DETAIL_1"] = Utility.ToString(drMapping["ACCT_CODE"]); ;
                                drAllocation["ALOC_DETAIL_2"] = Utility.ToString(drMapping["ACCT_GROUP"]); ;
                            }
                            //drAllocation["ALOC_DETAIL_3"] = Utility.ToString(arrElementValue[i, 0]);
                            drAllocation["ALOC_ACCOUNT"] = Utility.ToString(drMapping["GL_ACCT_CODE"]);
                            drAllocation["ALOC_VISA_MAP_ID"] = visaSales.TransactionId;
                            AllocationList.Rows.Add(drAllocation);
                        }
                    }
                }
                _objORAinterface.Map_group_code = "COZMO";

                _objORAinterface.AllocationList = AllocationList;
                visaSales.ObjORAinterface = _objORAinterface;
                visaSales.MappedStatus = "Y";

            }
            catch
            {
                throw;
            }
        }
    }
    
}
