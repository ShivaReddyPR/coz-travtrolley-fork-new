using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{


    public class ApplicationCenter
    {
      private const long NEW_RECORD = -1;
        
      //  private long _id;

        #region Member Variables
        private long _transactionid;
        private string _centerType;
        private string _code;
        private string _name;
        private long _country;
        private long _nationality;
        private long _visaType;
        private long _residence;
        private long _city;
        private string _visaCategory;
        private DateTime _submissionFrom;
        private DateTime _submissionTo;
        private DateTime _collectionFrom;
        private DateTime _collectionTo;
        private string _processTime;
        private string _website;
        private string _phone;
        private string _fax;
        private string _address;
        private string _status;
        private long _createdBy;
        private string _workingDays;

        #endregion


        #region Properties

        public long TransactionId
        {
            get { return _transactionid ; }
        }

        public string CenterType
        {
            get { return _centerType; }
            set { _centerType = value; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public long Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public long Nationality
        {
            get { return _nationality ; }
            set { _nationality  = value; }
        }
        public long VisaType
        {
            get { return _visaType; }
            set { _visaType = value; }
        }

        public long Residence
        {
            get { return _residence; }
            set { _residence = value; }
        }
        public long City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string VisaCategory
        {
            get { return _visaCategory; }
            set { _visaCategory  = value; }
        }
        public DateTime  SubmissionFrom
        {
            get { return _submissionFrom ; }
            set { _submissionFrom  = value; }
        }

        public DateTime CollectionFrom
        {
            get { return _collectionFrom ; }
            set { _collectionFrom  = value; }
        }
        public DateTime SubmissionTo
        {
            get { return _submissionTo; }
            set { _submissionTo = value; }
        }
        public DateTime CollectionTo
        {
            get { return _collectionTo; }
            set { _collectionTo = value; }
        }

        public string ProcessTime
        {
            get { return _processTime; }
            set { _processTime = value; }
        }

        public string Website
        {
            get { return _website; }
            set { _website = value; }
        }

        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        #endregion


        #region Constructors
        public ApplicationCenter()
        {
            _transactionid  = NEW_RECORD;
        }
        public ApplicationCenter(long id)
        {
            _transactionid  = id;
            getDetails(_transactionid );
        
        }


        #endregion

#region Methods

        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_CENTER_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("OBVISA_p_app_center_getData", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _transactionid = Utility.ToLong(dr["center_id"]);
                _centerType = Utility.ToString(dr["center_type"]);
                _code = Utility.ToString(dr["center_code"]);
                _name = Utility.ToString(dr["center_name"]);
                _country = Utility.ToLong(dr["center_country_id"]);
                _nationality = Utility.ToLong(dr["center_nationality_id"]);

                _visaType = Utility.ToLong(dr["center_visa_type_id"]);
                _residence = Utility.ToLong(dr["center_residence_id"]);
                _city = Utility.ToLong(dr["center_res_city_id"]);
                _visaCategory = Utility.ToString(dr["center_visa_category"]);
                _submissionFrom  = Utility.ToDate(dr["center_submission_from"]);
                _submissionTo = Utility.ToDate(dr["center_submission_to"]);
                _collectionFrom  = Utility.ToDate(dr["center_collection_from"]);
                _collectionTo = Utility.ToDate(dr["center_collection_to"]);

                _processTime  = Utility.ToString(dr["center_process_time"]);
                _website = Utility.ToString(dr["center_website"]);
                _phone = Utility.ToString(dr["center_phone"]);
                _fax = Utility.ToString(dr["center_fax"]);
                _address = Utility.ToString(dr["center_address"]);
                _status = Utility.ToString(dr["center_status"]);
                _createdBy = Utility.ToLong(dr["center_created_by"]);

            }
            catch
            {
                throw;
            }
        }
       
#endregion

#region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[24];

                paramList[0] = new SqlParameter("@P_CENTER_ID", _transactionid);
                paramList[1] = new SqlParameter("@P_CENTER_TYPE", _centerType );
                paramList[2] = new SqlParameter("@P_CENTER_CODE", _code);
                paramList[3] = new SqlParameter("@P_CENTER_NAME", _name);
                paramList[4] = new SqlParameter("@P_CENTER_COUNTRY_ID", _country);
                if(_nationality!=0) paramList[5] = new SqlParameter("@P_CENTER_NATIONALITY_ID", _nationality);
                if(_visaType!=0) paramList[6] = new SqlParameter("@P_CENTER_VISA_TYPE_ID", _visaType );
                if(_residence!=0) paramList[7] = new SqlParameter("@P_CENTER_RESIDENCE_ID", _residence);
                if(_city!=0) paramList[8] = new SqlParameter("@P_CENTER_RES_CITY_ID", _city);
                if(_visaCategory!="0") paramList[9] = new SqlParameter("@P_CENTER_VISA_CATEGORY", _visaCategory);
                paramList[10] = new SqlParameter("@P_CENTER_SUBMISSION_FROM", _submissionFrom );
                paramList[11] = new SqlParameter("@P_CENTER_SUBMISSION_TO", _submissionTo );
                paramList[12] = new SqlParameter("@P_CENTER_COLLECTION_FROM", _collectionFrom );
                paramList[13] = new SqlParameter("@P_CENTER_COLLECTION_TO", _collectionTo );
                paramList[14] = new SqlParameter("@P_CENTER_PROCESS_TIME", _processTime);
                paramList[15] = new SqlParameter("@P_CENTER_WEBSITE", _website );
                paramList[16] = new SqlParameter("@P_CENTER_PHONE", _phone );
                paramList[17] = new SqlParameter("@P_CENTER_FAX", _fax);
                paramList[18] = new SqlParameter("@P_CENTER_ADDRESS", _address );
                paramList[19] = new SqlParameter("@P_CENTER_WORKING_DAYS", _workingDays );
                paramList[20] = new SqlParameter("@P_CENTER_STATUS", _status);
                paramList[21] = new SqlParameter("@P_CENTER_CREATED_BY", _createdBy);
                
                
                paramList[22] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[22].Direction = ParameterDirection.Output;
                paramList[23] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[23].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("OBVISA_p_app_center_add_update", paramList);
                string messageType = Utility.ToString(paramList[22].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[23].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_center_id", _transactionid );
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_application_center_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
#endregion

       
        #region Static Methods

        public static DataTable GetList(int countryId, ListStatus status, RecordStatus recordStatus)
        {

            try
            {
             
                SqlParameter[] paramList = new SqlParameter[3];
                if (countryId > 0) paramList[0] = new SqlParameter("@P_COUNTRY_ID", countryId);
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus!= RecordStatus.All) paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("[OBVISA_p_App_Center_getlist]", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetAppCenterList(ListStatus status, RecordStatus recordStatus)
        {
            try
            {

                SqlParameter[] paramList = new SqlParameter[2];                
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus != RecordStatus.All) paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("[OBVISA_p_App_Center_getlist]", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        
        
        }
        # endregion
       
    }
   
}
