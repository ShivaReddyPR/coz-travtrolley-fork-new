using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using CT.Core;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;


namespace CT.TicketReceipt.BusinessLayer
{
    public class AgentMaster
    {
        #region Members

        private const long NEW_RECORD = -1;

        private long _id;
        private string _code;
        private string _name;
        private string _address;
        private string _city;
        private string _state;
        private int _country;
        private string _pobox;
        private string _phone1;
        private string _phone2;
        private string _fax;
        private string _telex;
        private string _email1;
        private string _email2;
        private decimal _alertLevel;
        private string _website;
        private string _imgFileName;
        private string _nflex1;
        private string _nflex2;
        private string _status;
        private long _createdBy;

        private decimal _currentBalance;
        private string _emailContent1;
        private string _emailContent2;

        private string _licenseNo;
        private DateTime _licenseExpDate;
        private string _passportNo;
        private DateTime _passportIssueDate;
        private DateTime _passportExpDate;
        private long _nationality;
        private string _grntrStatus;
        private string _visaSubmissionStatus;
        private string _agentCurrency;
        private string _agentBlockStatus;
        private string _agentRemarks;
        private int _agentParantId;
        //private string _agentType;
        private int _agentType;
        private DataTable _dtDocuments;
        private int _retAgentId;
        private int _copyKey;
        private string _agentProduct;
        private int _decimalValue;
        //Agent Fee

        //private long _OBfeeId;
        //private decimal _agentAdultOBFee;
        //private decimal _agentChildOBFee;
        //private decimal _agentInfantOBFee;
        //private long _parentOBFeeId;
        //private decimal _parentAdultOBFee;
        //private decimal _parentChildOBFee;
        //private decimal _parentInfantOBFee;
        //private string _agentOBFeeStatus;
        //Agent Payment Gateway
        private string _agentPaymentGateway;
        // private string _agentCurrency;
        private string _reduceBalanceStatus;
        private string _agentAirMarkupType;
        private string _addnlPaxDetails;
        //private string _selectedSources;
        //private string _unSelectedSources;
        private Dictionary<int, string> _sources;
        private PaymentMode _paymentMode;  //CreditCard Purpose Added by brahmam 14.09.2016
        bool _agentTicketAllowed;//Added by shiva 22 Jan 2017
        bool reqItinerary;
        /// <summary>
        /// Whether Flex Fields by Product need to be shown
        /// </summary>
        bool reqFlexFields;
        /// <summary>
        /// Specifies whether routing search is enabled or not
        /// </summary>
        bool isRoutingEnabled;
        bool isReturnFareEnabled;
        private string _agent_CreditCard;
        private string _agent_cc_exp_date;
        private string _agent_cc_type;
        /// <summary>
        /// Defines credit limit given to agent
        /// </summary>
        private double creditLimit;
        /// <summary>
        /// Defines due date for paying invoice
        /// </summary>
        private int creditDays;
        /// <summary>
        /// Defines additional credit given to agent along with credit limit
        /// </summary>
        private double creditBuffer;
        /// <summary>
        /// To show agent corporate flag
        /// </summary>
        private bool _IsCorporate;
        private long _salesExecId;
        
        #endregion

        #region Properties
        public long ID
        {
            get { return _id; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public int Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public string POBox
        {
            get { return _pobox; }
            set { _pobox = value; }
        }
        public string Phone1
        {
            get { return _phone1; }
            set { _phone1 = value; }
        }
        public string Phone2
        {
            get { return _phone2; }
            set { _phone2 = value; }
        }

        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }
        public string Telex
        {
            get { return _telex; }
            set { _telex = value; }
        }
        public string Email1
        {
            get { return _email1; }
            set { _email1 = value; }
        }
        public string Email2
        {
            get { return _email2; }
            set { _email2 = value; }
        }
        public decimal AlertLevel
        {
            get { return _alertLevel; }
            set { _alertLevel = value; }
        }
        public string Website
        {
            get { return _website ; }
            set { _website = value; }
        }
        public string ImgFileName
        {
            get { return _imgFileName; }
            set { _imgFileName = value; }
        }
        public string NFlex1
        {
            get { return _nflex1; }
            set { _nflex1 = value; }
        }
        public string NFlex2
        {
            get { return _nflex2; }
            set { _nflex2 = value; }
        }
        public string  Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }


        public decimal CurrentBalance
        {
            get { return _currentBalance; }
            set { _currentBalance = value; }
        }

        public string EmailContent1
        {

            get { return _emailContent1; }
            set { _emailContent1 = value; }

        }

        public string EmailContent2
        {

            get { return _emailContent2; }
            set { _emailContent2 = value; }

        }

        public string LicenseNo
        {
            get { return _licenseNo; }
            set { _licenseNo = value  ; }
        }


        public DateTime LicenseNoExpDate
        {
            get { return _licenseExpDate; }
            set { _licenseExpDate = value ; }
        }

        public string PassportNo
        {
            get { return _passportNo; }
            set { _passportNo = value; }
        }

        public DateTime PassportIssueDate
        {
            get { return _passportIssueDate; }
            set { _passportIssueDate = value; }
        }

        public DateTime PassportExpDate
        {
            get { return _passportExpDate; }
            set { _passportExpDate =value; }
        }

        public long Nationality
        {
            get { return _nationality; }

            set { _nationality = value; }

        }


        public String GrntrStatus
        {
            get { return _grntrStatus; }
            set { _grntrStatus = value; }
        }

        public string VisaSubmissionStatus
        {
            get { return _visaSubmissionStatus; }
            set { _visaSubmissionStatus = value; }

        }

        public string AgentCurrency
        {
            get { return _agentCurrency; }
            set { _agentCurrency = value; }

        }

        public string AgentBlockStatus
        {
            get { return _agentBlockStatus; }
            set { _agentBlockStatus = value; }

        }


        public string AgentRemarks
        {
            get { return _agentRemarks; }
            set { _agentRemarks = value; }

        }


        public string ReduceBalanceStatus
        {
            get { return _reduceBalanceStatus; }
            set { _reduceBalanceStatus = value; }

        }

        public int AgentParantId
        {
            get { return _agentParantId; }
            set { _agentParantId = value; }
        }

        public int AgentType
        {
            get { return _agentType; }
            set { _agentType = value; }

        }

        public DataTable DtDocuments
        {
            get { return _dtDocuments; }
            set { _dtDocuments = value; }

        }

        public int CopyKey
        {
            get { return _copyKey; }
            set { _copyKey = value; }
        }
        public string AgentProduct
        {
            get { return _agentProduct; }
            set { _agentProduct = value; }

        }
       // Fee Properties
     
         public string AgentPaymentGateway
        {
            get { return _agentPaymentGateway; }
            set { _agentPaymentGateway = value; }
        }

         public int RetAgentId
         {
             get { return _retAgentId; }
             set { _retAgentId = value; }
         }
public string AgentAirMarkupType
        {
            get { return _agentAirMarkupType; }
            set { _agentAirMarkupType = value; }
        }
        public string AddnlPaxDetails
        {
            get { return _addnlPaxDetails; }
            set { _addnlPaxDetails = value; }
        }
        //public string SelectedSources
        //{
        //    get { return _selectedSources; }
        //    set { _selectedSources = value; }
        //}

        //public string UnSelectedSources
        //{
        //    get { return _unSelectedSources; }
        //    set { _unSelectedSources = value; }
        //}

        public Dictionary<int, string> Sources
        {
            get { return _sources; }
            set { _sources = value; }
        }
        public int DecimalValue
        {
            get { return _decimalValue; }
            set { _decimalValue = value; }
        }
        //cardPayment purpose Added by brahmam 14.09.2016
        public PaymentMode PaymentMode
        {
            get { return _paymentMode; }
            set { _paymentMode = value; }
        }

        public bool AgentTicketingAllowed
        {
            get { return _agentTicketAllowed; }
            set { _agentTicketAllowed = value; }
        }
        /// <summary>
        /// Whether new Download Itinerary option is required or not in the Flight Search.
        /// </summary>
        public bool RequiredItinerary
        {
            get { return reqItinerary; }
            set { reqItinerary = value; }
        }
        /// <summary>
        /// Whether Flex Fields by Product need to be shown
        /// </summary>
        public bool RequiredFlexFields
        {
            get { return reqFlexFields; }
            set { reqFlexFields = value; }
        }

        /// <summary>
        /// Specifies whether routing search is enabled or not
        /// </summary>
        public bool IsRoutingEnabled { get => isRoutingEnabled; set => isRoutingEnabled = value; }
        public bool IsReturnFareEnabled { get => isReturnFareEnabled; set => isReturnFareEnabled = value; }

        public string agent_CreditCard { get => _agent_CreditCard; set => _agent_CreditCard = value; }
        public string agent_cc_exp_date { get => _agent_cc_exp_date; set => _agent_cc_exp_date = value; }
        public string agent_cc_type { get => _agent_cc_type; set => _agent_cc_type = value; }
        /// <summary>
        /// Defines credit limit given to agent
        /// </summary>
        public double CreditLimit { get => creditLimit; set => creditLimit = value; }
        /// <summary>
        /// Defines due date for paying invoice
        /// </summary>
        public int CreditDays { get => creditDays; set => creditDays = value; }
        /// <summary>
        /// Defines additional credit given to agent along with credit limit
        /// </summary>
        public double CreditBuffer { get => creditBuffer; set => creditBuffer = value; }

        /// <summary>
        /// To show agent corporate flag
        /// </summary>
        public bool IsCorporate { get => _IsCorporate; set => _IsCorporate = value; }
 public long SalesExecId
        {
            get { return _salesExecId; }
            set { _salesExecId = value; }
        }

        public List<AgentMasterReceipts> agentMasterReceipts { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public AgentMaster()
        {
            _id = NEW_RECORD;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public AgentMaster(long id)
        {
            _id = id;
            getDetails(id);
        }
        /// <summary>
        /// Get Parent Agent Details
        /// </summary>
        /// <param name="id"></param>
        public AgentMaster(int id,string flag)
        {
            _id = id;
            GetMainParentDetails(id);
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds); 
            //if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            //{
            //    UpdateBusinessData(ds.Tables[0].Rows[0]);
            //}

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_AGENT_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_agent_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Get Parent Agent Details
        /// </summary>
        /// <param name="id"></param>
        public void GetMainParentDetails(int agentId)
        {
            try
            {
                AgentMaster s = new AgentMaster();
                SqlParameter[] paramList = new SqlParameter[1];
                if (agentId > 0) paramList[0] = new SqlParameter("@P_AGENT_ID", agentId);
              DataSet dataset =  DBGateway.ExecuteQuery("ct_p_get_agentparentDetails", paramList);
               UpdateBusinessData(dataset);               
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ds"></param>
        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["agent_id"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }

                   // _dtDocuments = ds.Tables[1];

                }

            }
            catch
            { throw; }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                if (_id < 0)
                {
                    _id = Utility.ToLong(dr["agent_id"]);
                }
                else
                {
                    _id = Utility.ToLong(dr["agent_id"]);
                    _code = Utility.ToString(dr["agent_code"]);
                    _name = Utility.ToString(dr["agent_name"]);
                    _address = Utility.ToString(dr["agent_address"]);
                    _city = Utility.ToString(dr["agent_city"]);

                    _state = Utility.ToString(dr["agent_state"]);
                    _country = Utility.ToInteger(dr["agent_country"]);
                    _pobox = Utility.ToString(dr["agent_po_box"]);
                    _phone1 = Utility.ToString(dr["agent_phone1"]);
                    _phone2 = Utility.ToString(dr["agent_phone2"]);
                    _fax = Utility.ToString(dr["agent_fax"]);
                    _telex = Utility.ToString(dr["agent_telex"]);
                    _email1 = Utility.ToString(dr["agent_email1"]);
                    _email2 = Utility.ToString(dr["agent_email2"]);
                    _alertLevel = Utility.ToDecimal(dr["agent_alert_balance"]);
                    _website = Utility.ToString(dr["agent_website"]);
                    _imgFileName = Utility.ToString(dr["agent_img_filename"]);
                    _nflex1 = Utility.ToString(dr["n_flex_1"]);

                    _nflex2 = Utility.ToString(dr["n_flex_2"]);
                    _status = Utility.ToString(dr["agent_status"]);
                    _createdBy = Utility.ToLong(dr["agent_created_by"]);

                    _currentBalance = Utility.ToDecimal(dr["agent_current_balance"]);
                    _emailContent1 = Utility.ToString(dr["agent_email_content1"]);
                    _emailContent2 = Utility.ToString(dr["agent_email_content2"]);

                    _licenseNo = Utility.ToString(dr["agent_license_no"]);
                    _licenseExpDate = Utility.ToDate(dr["agent_license_exp_date"]);
                    _passportNo = Utility.ToString(dr["agent_passport_no"]);
                    _passportIssueDate = Utility.ToDate(dr["agent_passport_issue_date"]);
                    _passportExpDate = Utility.ToDate(dr["agent_passport_expiry_date"]);
                    _nationality = Utility.ToLong(dr["agent_nationality"]);
                    _grntrStatus = Utility.ToString(dr["agent_guarantor_status"]);
                    _visaSubmissionStatus = Utility.ToString(dr["agent_visa_submission_status"]);
                    _agentCurrency = Utility.ToString(dr["agent_currency"]);
                    _agentBlockStatus = Utility.ToString(dr["agent_block_status"]);
                    _agentRemarks = Utility.ToString(dr["agent_remarks"]);
                    _reduceBalanceStatus = Utility.ToString(dr["agent_reduce_balance"]);
                    _agentParantId = Utility.ToInteger(dr["agent_parent_id"]);
                    _agentType = Utility.ToInteger(dr["agent_type"]);
                    //_agentType = 1;// TODO
                    _agentProduct = Utility.ToString(dr["agent_product"]);
                    // Fee Details
                    _agentAirMarkupType = Utility.ToString(dr["agent_air_markup_type"]);
                    // _agentPaymentGateway = Utility.ToString(dr["agent_pg_status"]);
                    _addnlPaxDetails = Utility.ToString(dr["add_pax_details"]);
                    _decimalValue = Utility.ToInteger(dr["agent_decimal"]);
                    if (dr["agent_payment_mode"] != DBNull.Value)
                    {
                        _paymentMode = (PaymentMode)Utility.ToInteger(dr["agent_payment_mode"]); //cardPayment purpose Added by brahmam 14.09.2016
                    }
                    else
                    {
                        _paymentMode = PaymentMode.Credit;
                    }
                    if (dr["agent_ticket_right"] != DBNull.Value)
                    {
                        _agentTicketAllowed = (dr["agent_ticket_right"].ToString() == "Y" ? true : false);
                    }
                    if (dr["agent_required_itinerary"] != DBNull.Value)
                    {
                        reqItinerary = Convert.ToBoolean(dr["agent_required_itinerary"]);
                    }
                    if (dr["agent_flex_required"] != DBNull.Value)
                    {
                        reqFlexFields = Convert.ToBoolean(dr["agent_flex_required"]);
                    }
                    if (dr["agent_routing"] != DBNull.Value)
                    {
                        isRoutingEnabled = Convert.ToBoolean(dr["agent_routing"]);
                    }
                    if (dr["agent_returnfare"] != DBNull.Value)
                    {
                        isReturnFareEnabled = Convert.ToBoolean(dr["agent_returnfare"]);
                    }
                    if (dr["agent_CreditCard"] != DBNull.Value)
                    {
                        _agent_CreditCard = Convert.ToString(dr["agent_CreditCard"]);
                    }
                    if (dr["agent_cc_type"] != DBNull.Value)
                    {
                        _agent_cc_type = Convert.ToString(dr["agent_cc_type"]);
                    }
                    if (dr["agent_cc_exp_date"] != DBNull.Value)
                    {
                        _agent_cc_exp_date = Convert.ToString(dr["agent_cc_exp_date"]);
                    }
                    creditLimit = (dr["agent_credit_limit"] != DBNull.Value) ? Convert.ToDouble(dr["agent_credit_limit"]) : 0;
                    creditDays = (dr["agent_credit_days"] != DBNull.Value) ? Convert.ToInt32(dr["agent_credit_days"]) : 0;
                    creditBuffer = (dr["agent_Credit_Buffer_Amount"] != DBNull.Value) ? Convert.ToDouble(dr["agent_Credit_Buffer_Amount"]) : 0;
                    IsCorporate = !string.IsNullOrEmpty(Convert.ToString(dr["agent_corporate"])) && Convert.ToString(dr["agent_corporate"]) == "Y";

                    
                    _salesExecId = Utility.ToLong(dr["agent_sales_Executive"]);
                    
                }  
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;  
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                SqlParameter[] paramList = new SqlParameter[48];

                paramList[0] = new SqlParameter("@P_AGENT_ID", _id);
                paramList[1] = new SqlParameter("@P_AGENT_COMPANY_ID", Settings.LoginInfo!=null?Settings.LoginInfo.CompanyID:1);
                paramList[2] = new SqlParameter("@P_AGENT_CODE", _code);
                paramList[3] = new SqlParameter("@P_AGENT_NAME", _name );
                paramList[4] = new SqlParameter("@P_AGENT_ADDRESS", _address );
                paramList[5] = new SqlParameter("@P_AGENT_CITY", _city);
                paramList[6] = new SqlParameter("@P_AGENT_STATE", _state);
                paramList[7] = new SqlParameter("@P_AGENT_COUNTRY", _country);
                paramList[8] = new SqlParameter("@P_AGENT_PO_BOX", _pobox );
                paramList[9] = new SqlParameter("@P_AGENT_PHONE1", _phone1 );
                paramList[10] = new SqlParameter("@P_AGENT_PHONE2", _phone2);
                paramList[11] = new SqlParameter("@P_AGENT_FAX", _fax );

                paramList[12] = new SqlParameter("@P_AGENT_TELEX", _telex );
                paramList[13] = new SqlParameter("@P_AGENT_EMAIL1", _email1 );
                paramList[14] = new SqlParameter("@P_AGENT_EMAIL2", _email2 );
                paramList[15] = new SqlParameter("@P_AGENT_WEBSITE", _website);
                if(!string.IsNullOrEmpty(_imgFileName))  paramList[16] = new SqlParameter("@P_AGENT_IMG_FILENAME", _imgFileName );

                paramList[17] = new SqlParameter("@P_N_FLEX_1", _nflex1);
                paramList[18] = new SqlParameter("@P_N_FLEX_2", _nflex2);
                paramList[19] = new SqlParameter("@P_AGENT_ALERT_BALANCE", _alertLevel);
                paramList[20] = new SqlParameter("@P_AGENT_STATUS", _status);
                paramList[21] = new SqlParameter("@P_AGENT_CREATED_BY", _createdBy );
                paramList[22] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[22].Direction = ParameterDirection.Output;
                paramList[23] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[23].Direction = ParameterDirection.Output;

                paramList[24] = new SqlParameter("@P_AGENT_ID_RET", SqlDbType.Int, 200);
                paramList[24].Direction = ParameterDirection.Output;

                paramList[25] = new SqlParameter("@P_agent_license_no", _licenseNo);
                if (_licenseExpDate != DateTime.MinValue) paramList[26] = new SqlParameter("@P_agent_license_exp_date", _licenseExpDate);
                paramList[27] = new SqlParameter("@P_agent_passport_no", _passportNo);
                if (_passportIssueDate != DateTime.MinValue) paramList[28] = new SqlParameter("@P_agent_passport_issue_date", _passportIssueDate);
                if (_passportExpDate != DateTime.MinValue) paramList[29] = new SqlParameter("@P_agent_passport_expiry_date", _passportExpDate);
                paramList[30] = new SqlParameter("@P_agent_nationality", _nationality);
                paramList[31] = new SqlParameter("@P_agent_guarantor_status", _grntrStatus);
                paramList[32] = new SqlParameter("@P_agent_visa_submission_status", _visaSubmissionStatus);
                paramList[33] = new SqlParameter("@P_AGENT_BLOCK_STATUS", _agentBlockStatus);
                paramList[34] = new SqlParameter("@P_AGENT_REMARKS", _agentRemarks);
                paramList[35] = new SqlParameter("@p_agent_reduce_balance", _reduceBalanceStatus);
                paramList[36] = new SqlParameter("@p_agent_currency", _agentCurrency);
                paramList[37] = new SqlParameter("@P_AGENT_PARENT_ID", _agentParantId);
                if (_agentType > 0) paramList[38] = new SqlParameter("@P_AGENT_TYPE", _agentType);
                if (!string.IsNullOrEmpty(_agentProduct)) paramList[39] = new SqlParameter("@p_Agent_Product", _agentProduct);
                paramList[40] = new SqlParameter("@P_Agent_Decimal", _decimalValue);
                paramList[41] = new SqlParameter("@P_Agent_PaymentMode", (int)PaymentMode);
                paramList[42] = new SqlParameter("@P_Agent_Routing", isRoutingEnabled);
                paramList[43] = new SqlParameter("@P_Agent_ReturnFare", isReturnFareEnabled);
                paramList[44] = new SqlParameter("@P_agent_cc_type", agent_cc_type);
                paramList[45] = new SqlParameter("@P_agent_cc_exp_date", _agent_cc_exp_date);
                paramList[46] = new SqlParameter("@P_agent_CreditCard", agent_CreditCard);
                if (_salesExecId > 0) paramList[47] = new SqlParameter("@P_Agent_sales_Executive", _salesExecId);               
                List<SqlParameter> parameters = new List<SqlParameter>(paramList);
                parameters.Add(new SqlParameter("@P_Agent_Credit_Limit", creditLimit));
                parameters.Add(new SqlParameter("@P_Agent_Credit_Buffer", creditBuffer));
                parameters.Add(new SqlParameter("@P_Agent_Credit_Days", creditDays));
                DBGateway.ExecuteNonQueryDetails(cmd,"ct_p_agent_add_update", parameters.ToArray());
                string messageType = Utility.ToString(paramList[22].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[23].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                _retAgentId = Utility.ToInteger(paramList[24].Value);

                //String[] unSelectedSourceList = _unSelectedSources.Split(',');
                foreach (System.Collections.Generic.KeyValuePair<int, string> pair in _sources)
                {
                    SaveAgentSources(cmd, -1, _retAgentId,pair.Key,pair.Value);
                }
                //for (int i = 0; i < unSelectedSourceList.Length; i++)
                //{
                    
                //    int unSelectedsourceId = Utility.ToInteger(unSelectedSourceList[i]);
                //    if (unSelectedsourceId != 0)
                //    {
                //        SaveAgentSources(cmd, 1, _retAgentId, unSelectedsourceId, "D");
                //    }
                //}
                
                //String[] selectedSourceList = _selectedSources.Split(',');
                //for (int i = 0; i < selectedSourceList.Length; i++)
                //{
                //    int selectedSourceId = Utility.ToInteger(selectedSourceList[i]);
                //    if (selectedSourceId != 0)
                //    {
                //        SaveAgentSources(cmd, -1, _retAgentId, selectedSourceId, "A");
                //    }
                //}

                //if (_dtDocuments != null)
                //{
                //    DataTable dt;
                //    if (_copyKey != 2) dt = _dtDocuments.GetChanges();
                //    dt = _dtDocuments;

                //    int recordStatus = 0;
                //    if (dt != null && dt.Rows.Count > 0)
                //    {

                //        //try
                //        //{
                //        foreach (DataRow dr in dt.Rows)
                //        {
                //            switch (dr.RowState)
                //            {
                //                case DataRowState.Added: recordStatus = 1;
                //                    break;
                //                case DataRowState.Modified: recordStatus = 2;
                //                    break;
                //                case DataRowState.Deleted: recordStatus = -1;
                //                    dr.RejectChanges();
                //                    break;
                //                default: break;


                //            }
                //            SaveDocDetails(cmd, _copyKey == 2 ? 1 : recordStatus, Utility.ToLong(dr["doc_id"]), Utility.ToString(dr["doc_code"]), Utility.ToString(dr["doc_name"]),
                //                                _retAgentId, Utility.ToString(dr["doc_status"]), _createdBy);

                //            recordStatus = 0;
                //        }

                //        //}
                //        //catch { throw; }
                //        //finally
                //        //{
                //        //    DBGateway.CloseConnection(cmd);
                //        //}
                //    }
                //}
                //string[] prodList = _agentProduct.Split(',');
                //foreach (string product in prodList)
                //{
                //    if (Utility.ToInteger(product) == 4)
                //    {
                //        SaveGlobalVisaFee(cmd, _retAgentId);
                //    }
                //}
                //if (_agentOBFeeStatus == Settings.DELETED && _OBfeeId >1)
                //{
                //    SaveGlobalVisaFee(cmd, _retAgentId);
                //}

                trans.Commit();

                if (agentMasterReceipts != null && agentMasterReceipts.Count > 0 && _retAgentId > 0)
                {
                    agentMasterReceipts.Where(x => x != null).ToList().ForEach(x => {

                        x.agent_Id = _retAgentId;
                        x.doc_created_by = Utility.ToInteger(_createdBy);
                        x.Save();
                    });
                }

            }
            catch (Exception ex)
            {
                trans.Rollback();
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "");
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="recordStatus"></param>
        /// <param name="docId"></param>
        /// <param name="docCode"></param>
        /// <param name="docName"></param>
        /// <param name="agentId"></param>
        /// <param name="docStatus"></param>
        /// <param name="createdBy"></param>
        public void SaveAgentDocDetails(long doc_Id, string doc_code, string doc_name, int agentId,string doc_path, string doc_type, long createdBy)
        {

            try
            {
                List<SqlParameter> liParamslist = new List<SqlParameter>();
                liParamslist.Add(new SqlParameter("@doc_Id", doc_Id));
                if (doc_Id == 0)
                    liParamslist[0].Direction = ParameterDirection.Output;
                liParamslist.Add(new SqlParameter("@agent_id", agentId));
                liParamslist.Add(new SqlParameter("@doc_code", doc_code));
                liParamslist.Add(new SqlParameter("@doc_name", doc_name));
                liParamslist.Add(new SqlParameter("@doc_file_path", doc_path));
                liParamslist.Add(new SqlParameter("@doc_file_type", doc_type));
                liParamslist.Add(new SqlParameter("@doc_status", 1));
                liParamslist.Add(new SqlParameter("@P_doc_created_by", createdBy));
                int res = DBGateway.ExecuteNonQuery("usp_SaveAgentReceipts", liParamslist.ToArray());
                doc_Id = Convert.ToInt32(liParamslist[0].Value);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="recordStatus"></param>
        /// <param name="docId"></param>
        /// <param name="docCode"></param>
        /// <param name="docName"></param>
        /// <param name="agentId"></param>
        /// <param name="docStatus"></param>
        /// <param name="createdBy"></param>
        public void SaveDocDetails(SqlCommand cmd, int recordStatus, long docId, string docCode, string docName, int agentId, string docStatus, long createdBy)
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[9];
                paramList[0] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                paramList[1] = new SqlParameter("@P_doc_id", docId);
                paramList[2] = new SqlParameter("@P_doc_code", docCode);
                paramList[3] = new SqlParameter("@P_doc_name", docName);
                paramList[4] = new SqlParameter("@P_doc_agent_id", agentId);
                paramList[5] = new SqlParameter("@P_doc_status", docStatus);
                paramList[6] = new SqlParameter("@P_doc_created_by", createdBy);
                paramList[7] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[7].Direction = ParameterDirection.Output;
                paramList[8] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[8].Direction = ParameterDirection.Output;

                //SqlParameter paramMsgText1 = new SqlParameter("@p_vs_id_ret", SqlDbType.BigInt);
                //paramMsgText1.Size = 20;
                //paramMsgText1.Direction = ParameterDirection.Output;
                //paramArr[31] = paramMsgText1;



                DBGateway.ExecuteNonQueryDetails(cmd, "visa_p_documents_master_add_update", paramList);
                if (Utility.ToString(paramList[7].Value) == "E")
                    throw new Exception(Utility.ToString(paramList[8].Value));

                //_docNumber = Utility.ToString(paramDocNo.Value);
                //_transactionId = Utility.ToLong(paramMsgText1.Value);

            }
            catch
            {
                throw;
            }
        }
        //public void SaveGlobalVisaFee(SqlCommand cmd,int agentId)
        //{
        //    try
        //    {
        //        SqlParameter[] paramList = new SqlParameter[13];
        //        if (_OBfeeId == 0) _OBfeeId = -1;
        //        paramList[0] = new SqlParameter("@P_FEE_ID", _OBfeeId);
        //        paramList[1] = new SqlParameter("@P_AGENT_ID", agentId);
        //        paramList[2] = new SqlParameter("@P_ADULT_FEE", _agentAdultOBFee);
        //        paramList[3] = new SqlParameter("@P_CHILD_FEE", _agentChildOBFee);
        //        paramList[4] = new SqlParameter("@P_INFANT_FEE", _agentInfantOBFee);
        //        if(_parentOBFeeId>=1) paramList[5] = new SqlParameter("@P_PARENT_FEE_ID", _parentOBFeeId);
        //        paramList[6] = new SqlParameter("@P_PARENT_ADULT_FEE", _parentAdultOBFee);
        //        paramList[7] = new SqlParameter("@P_PARENT_CHILD_FEE", _parentChildOBFee);
        //        paramList[8] = new SqlParameter("@P_PARENT_INFANT_FEE", _parentInfantOBFee);
        //        paramList[9] = new SqlParameter("@P_FEE_STATUS", _agentOBFeeStatus);
        //        paramList[10] = new SqlParameter("@P_FEE_CREATED_BY", _createdBy);
        //        paramList[11] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
        //        paramList[11].Direction = ParameterDirection.Output;
        //        paramList[12] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
        //        paramList[12].Direction = ParameterDirection.Output;

        //        DBGateway.ExecuteNonQueryDetails(cmd, "ct_p_agent_fee_add_update", paramList);
        //        if (Utility.ToString(paramList[11].Value) == "E")
        //            throw new Exception(Utility.ToString(paramList[12].Value));
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
        /// <summary>
        /// 
        /// </summary>
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_USER_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_user_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="agentSourceId"></param>
        /// <param name="agentId"></param>
        /// <param name="sourceId"></param>
        /// <param name="status"></param>
        private void SaveAgentSources(SqlCommand cmd,int agentSourceId, int agentId, int sourceId,string status)
        {
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@P_AgentSourceId", agentSourceId);
            paramList[1] = new SqlParameter("@P_AgentId", agentId);
            paramList[2] = new SqlParameter("@P_sourceId", sourceId);
            paramList[3] = new SqlParameter("@P_Status", status);
            paramList[4] = new SqlParameter("@p_CreatedBy", Settings.LoginInfo.AgentId);
            DBGateway.ExecuteNonQueryDetails(cmd, "usp_Agent_Sources_Add", paramList);
        }

        //public void DeleteAgentSources(int agentSourceId, int agentId, int sourceId)
        //{
        //    SqlParameter[] paramList = new SqlParameter[3];
        //    paramList[0] = new SqlParameter("@P_AgentSourceId", agentSourceId);
        //    paramList[1] = new SqlParameter("@P_AgentId", agentId);
        //    paramList[2] = new SqlParameter("@P_sourceId", sourceId);
        //    DBGateway.ExecuteNonQuery("usp_Delete_Agent_Sources", paramList);
        //}
        #endregion


        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="agentType"></param>
        /// <param name="loginAgent"></param>
        /// <param name="status"></param>
        /// <param name="recordStatus"></param>
        /// <returns></returns>
        public static DataTable GetList(int companyId,string agentType,int loginAgent, ListStatus status, RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[5];

                if(companyId>0)paramList[0] = new SqlParameter("@P_COMPANY_ID", companyId);
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                if (!string.IsNullOrEmpty(agentType)) paramList[3] = new SqlParameter("@P_AGENT_TYPE", agentType);
                if (loginAgent > 0) paramList[4] = new SqlParameter("@P_LOGIN_AGENT_ID", loginAgent);

                return DBGateway.ExecuteQuery("ct_p_agent_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentAgent"></param>
        /// <param name="status"></param>
        /// <param name="recordStatus"></param>
        /// <param name="memberType"></param>
        /// <param name="parentLocationId"></param>
        /// <returns></returns>
        public static DataTable SubAgentGetList(int parentAgent, ListStatus status, RecordStatus recordStatus, string memberType, long parentLocationId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[7];

                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                if (parentAgent > 0) paramList[3] = new SqlParameter("@P_LOGIN_AGENT_ID", parentAgent);
                paramList[4] = new SqlParameter("@P_MEMBER_TYPE", memberType);
                if (parentLocationId > 0) paramList[5] = new SqlParameter("@P_AGENT_PARENT_LOCATION_ID", parentLocationId);
                return DBGateway.ExecuteQuery("ct_p_subagent_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        //public static string[] GetParentFeeDetials(long agentId, ListStatus status, RecordStatus recordStatus)
        //{

        //    try
        //    {
        //        string[] retList = new string[4];
        //        SqlParameter[] paramList = new SqlParameter[3];

        //        if (agentId > 0) paramList[0] = new SqlParameter("@P_AGENT_ID", agentId);
        //        paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
        //        paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

        //        DataSet ds = DBGateway.ExecuteQuery("ct_p_agent_get_parent_feedetails", paramList);
        //        if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
        //        {
        //            retList[0] = Utility.ToString(ds.Tables[0].Rows[0]["obvisa_agent_fee_id"]);
        //            retList[1] = Utility.ToString(ds.Tables[0].Rows[0]["obvisa_agent_adult_fee"]);
        //            retList[2] = Utility.ToString(ds.Tables[0].Rows[0]["obvisa_agent_child_fee"]);
        //            retList[3] = Utility.ToString(ds.Tables[0].Rows[0]["obvisa_agent_infant_fee"]);
        //        }
        //        return retList;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns></returns>
        public static DataTable GetActiveSources(int ProductId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                if (ProductId > 0) paramList[0] = new SqlParameter("@P_ProductId", ProductId);
                return DBGateway.ExecuteQuery("usp_Get_Active_Sources", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agentId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public static DataTable GetAgentSources(int agentId,int productId)
        {
            try
            {
                return GetAgentSources(agentId, productId, false);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agentId"></param>
        /// <param name="productId"></param>
        /// <param name="isGimmonix"></param>
        /// <returns></returns>
        public static DataTable GetAgentSources(int agentId, int productId, bool isGimmonix)
        {
            try
            {
               // SqlParameter[] paramList = new SqlParameter[3];
               // if (agentId > 0) paramList[0] = new SqlParameter("@P_AgentId", agentId);
               // paramList[1] = new SqlParameter("@P_ProductId", productId);
               //paramList[2] = new SqlParameter("@P_IsGimmonix", isGimmonix);
               // return DBGateway.ExecuteQuery("usp_Get_Agent_Sources", paramList).Tables[0];
                return GetAgentSources(agentId, productId, isGimmonix, true);
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetAgentSources(int agentId, int productId, bool isGimmonix,bool isOnlineSource)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                if (agentId > 0) paramList[0] = new SqlParameter("@P_AgentId", agentId);
                paramList[1] = new SqlParameter("@P_ProductId", productId);
                paramList[2] = new SqlParameter("@P_IsGimmonix", isGimmonix);
                paramList[3] = new SqlParameter("@P_IsOnlineSource", isOnlineSource);
                return DBGateway.ExecuteQuery("usp_Get_Agent_Sources", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agentId"></param>
        /// <param name="productId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static DataTable GetAgentSourcesAll(int agentId, int productId,int status)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                if (agentId > 0) paramList[0] = new SqlParameter("@P_AgentId", agentId);
                paramList[1] = new SqlParameter("@P_ProductId", productId);
                if(status<2) paramList[2] = new SqlParameter("@P_status", status);
                return DBGateway.ExecuteQuery("usp_Get_Agent_Sources_All", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public static Dictionary<string, SourceDetails> GetAirlineCredentials(int agentId)
        {
            Dictionary<string, SourceDetails> agentCredentials = new Dictionary<string, SourceDetails>();
            return GetAirlineCredentials(agentId, RecordStatus.All);           
        }


        /// <summary>
        /// returns Agent active source credentials only
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public static Dictionary<string, SourceDetails> GetAirlineCredentials(int agentId,RecordStatus status)
        {
            Dictionary<string, SourceDetails> agentCredentials = new Dictionary<string, SourceDetails>();

            DataTable dtCredentials = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_AgentID", agentId);
                if(status!=RecordStatus.All ) paramList[1] = new SqlParameter("@P_Status", Utility.ToString((char)status));
                dtCredentials = DBGateway.FillDataTableSP("usp_GetAirlineCredentials", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (dtCredentials != null)
            {
                foreach (DataRow dr in dtCredentials.Rows)
                {
                    SourceDetails credentials = new SourceDetails();
                    credentials.UserID = dr["xml_userid"].ToString();
                    credentials.Password = dr["xml_password"].ToString();
                    credentials.HAP = dr["xml_hap"].ToString();
                    credentials.PCC = dr["xml_pcc"].ToString();
                    credentials.Type = !string.IsNullOrEmpty(dr["Type"].ToString()) ? Utility.ToInteger(dr["Type"].ToString()) : 0;
                    credentials.CreatePassieveReservation = Convert.ToString(dr["Ispassive"]);
                    if (dr["endpointUrl"] != DBNull.Value)
                    {
                        credentials.EndPoint = dr["endpointUrl"].ToString();
                    }
                    if (dr["preferredLCC"] != DBNull.Value)
                    {
                        credentials.PreferredLCC = dr["preferredLCC"].ToString();
                    }
                    if (dr["preferredGDS"] != DBNull.Value)
                    {
                        credentials.PreferredGDS = dr["preferredGDS"].ToString();
                    }
                    //Added by lokesh on 14Feb2019
                    //Read PrivateFareCodes configured for the agent.
                    if (dr["PrivateFareCodes"] != DBNull.Value)
                    {
                        credentials.PrivateFareCodes = dr["PrivateFareCodes"].ToString();
                    }
                    if (!agentCredentials.ContainsKey(dr["Name"].ToString()))
                    {
                        agentCredentials.Add(dr["Name"].ToString(), credentials);
                    }

                }
            }

            return agentCredentials;
        }
        /// <summary>
        /// Returns the Agent City and Phone Number of Parent Agent. If Parent Agent is not there then Base Agent details will be returned.
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public static string GetParentAgentLocationAndPhone(int agentId)
        {
            string Result = string.Empty;

            //SqlConnection con = new SqlConnection();
            try
            {
                //con = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_LOGIN_AGENT_ID", agentId);
                //SqlDataReader reader = DBGateway.ExecuteReaderSP("ct_p_agent_get_parent", paramList, con);
                using (DataTable dtAgent = DBGateway.FillDataTableSP("ct_p_agent_get_parent", paramList))
                {
                    if (dtAgent != null && dtAgent.Rows.Count > 0)
                    {
                        DataRow reader = dtAgent.Rows[0];
                        if (reader !=null && reader["Agent_Data"] !=DBNull.Value)
                        {
                            Result =Convert.ToString(reader["Agent_Data"]);
                        }
                    }
                    //reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //if (con.State == ConnectionState.Open)
                //{
                //    con.Close();
                //}
            }
            return Result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public static int GetAgentBlockDays(int agentId)
        {          
            try
            {
                int blockdays = 0;
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_AGENT_ID", agentId);
                paramList[1] = new SqlParameter("@P_AGENT_BLOCK_DAYS", SqlDbType.Int);
                paramList[1].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuery("USP_GET_AGENT_BLOCK_DAYS", paramList);
               if(paramList[1].Value!=null)
                {
                    blockdays =Utility.ToInteger(paramList[1].Value);
                }             
                return blockdays;
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agentcode"></param>
        /// <returns></returns>
        public static DataTable GetAgentIdByCode(string agentcode)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_AGENT_CODE", agentcode);
                return DBGateway.FillDataTableSP("USP_GET_AGENT_ID_BY_CODE", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        # region update agent balance
        public static decimal UpdateAgentBalance(int agentId, decimal transactionAmount, int createdBy)
        {
            try
            {
                decimal currentBalance = 0;
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("p_agent_id", agentId);
                paramList[1] = new SqlParameter("p_trans_amount", transactionAmount);
                paramList[2] = new SqlParameter("p_modified_by", createdBy);
                paramList[3] = new SqlParameter("p_current_balance", SqlDbType.Money);
                paramList[3].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuerySP("ct_p_agent_balance_update", paramList);
                currentBalance = Utility.ToDecimal(paramList[3].Value);
                return currentBalance;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region VMSB2BAgentInfo

        public static DataTable GetVMSB2BAgentInfo(int agentId,string memberType)
        {
            DataTable dtAgentInfo = null;

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@agentId", agentId);
                paramList[1] = new SqlParameter("@user_member_type", memberType);
                dtAgentInfo = DBGateway.FillDataTableSP("ct_p_get_vmsb2bagent_info", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtAgentInfo;
        }

        #endregion

        # region update balance
        public decimal UpdateBalance(decimal amount)
        {
            try
            {
                decimal currentBalance = 0;
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("p_agent_id", ID);
                paramList[1] = new SqlParameter("p_trans_amount", amount);
                paramList[2] = new SqlParameter("p_modified_by", _createdBy);
                paramList[3] = new SqlParameter("p_current_balance", SqlDbType.Money);
                paramList[3].Direction = ParameterDirection.Output;
                //paramList[3].Precision = 18;
                //paramList[3].Scale = 2;

                DBGateway.ExecuteNonQuerySP("ct_p_agent_balance_update", paramList);

                currentBalance = Utility.ToDecimal(paramList[3].Value);
                return currentBalance;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void UpdateAgentSourceCredentials(int agentId, string sourceName, SourceDetails details)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[15]; // Added @p_Typ parameter to differntiate the sources with the same name in different products.

                if (agentId > 0) paramList[0] = new SqlParameter("@P_AgentID", agentId);
                if (!string.IsNullOrEmpty(sourceName)) paramList[1] = new SqlParameter("@P_sourceName", sourceName);
                //if (!string.IsNullOrEmpty(details.AgentCode)) paramList[2] = new SqlParameter("@P_AgentCode", details.AgentCode);
                if (!string.IsNullOrEmpty(details.UserID)) paramList[3] = new SqlParameter("@P_UserID", details.UserID);
                if (!string.IsNullOrEmpty(details.Password)) paramList[4] = new SqlParameter("@P_Password", details.Password);
                if (!string.IsNullOrEmpty(details.HAP)) paramList[5] = new SqlParameter("@P_HAP", details.HAP);
                if (!string.IsNullOrEmpty(details.PCC)) paramList[6] = new SqlParameter("@P_PCC", details.PCC);
                paramList[7] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[7].Direction = ParameterDirection.Output;
                paramList[8] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[8].Direction = ParameterDirection.Output;
                paramList[9] = new SqlParameter("@p_Type", details.Type);
                paramList[10] = new SqlParameter("@p_EndPoint", details.EndPoint);
                paramList[11] = new SqlParameter("@p_PreferredLCC", details.PreferredLCC);
                paramList[12] = new SqlParameter("@p_PreferredGDS", details.PreferredGDS);
                paramList[13] = new SqlParameter("@p_PrivateFareCodes", details.PrivateFareCodes);
                paramList[14] = new SqlParameter("@p_Ispassive", details.CreatePassieveReservation);
                DBGateway.ExecuteNonQuery("CT_P_AGENT_SOURCE_CREDENTIALS_UPDATE", paramList);
                string messageType = Utility.ToString(paramList[7].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[8].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public static string AgentProducts(int agentId)
        {
            try
            {
                string agentProducts = string.Empty;
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_AGENT_ID", agentId);
                paramList[1] = new SqlParameter("@P_PRODUCTS_RET", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuerySP("ct_p_get_agentproducts", paramList);
                agentProducts = Utility.ToString(paramList[1].Value);
                return agentProducts;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static int GetAgentType(int agentId)
        {
            try
            {
                int agentType;
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_AGENT_ID", agentId);
                paramList[1] = new SqlParameter("@P_AGENTTYPE_RET", SqlDbType.Int);
                paramList[1].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuerySP("ct_p_get_agenttype", paramList);
                agentType = Utility.ToInteger(paramList[1].Value);
                return agentType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static int GetParentId(int agentId)
        {
            try
            {
                int parentId;
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_AGENT_ID", agentId);
                paramList[1] = new SqlParameter("@P_PARENTID_RET", SqlDbType.Int);
                paramList[1].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuerySP("ct_p_get_agentparentId", paramList);
                parentId = Utility.ToInteger(paramList[1].Value);
                return parentId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetAgentCurrency(int id)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_AGENT_ID", id);
                paramList[1] = new SqlParameter("@P_AGENT_CURRENCY", SqlDbType.NVarChar, 10);
                paramList[1].Direction = ParameterDirection.Output;
                DBGateway.FillDataTableSP("ct_p_agent_getCurrency", paramList);
                return Convert.ToString(paramList[1].Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static string GetParentAgentCurrency(int id, string agentType)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_AGENT_ID", id);
                paramList[1] = new SqlParameter("@P_AGENT_TYPE", agentType);
                paramList[2] = new SqlParameter("@P_AGENT_CURRENCY", SqlDbType.NVarChar, 10);
                paramList[2].Direction = ParameterDirection.Output;
                DBGateway.FillDataTableSP("ct_p_agent_getParentCurrency", paramList);
                return Convert.ToString(paramList[2].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetB2CAgentList()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[0];
                return DBGateway.FillDataTableSP("CT_P_GetB2CAgentList", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Dictionary<string, string> GetActiveSourcesByProduct(int productId)
        {
            Dictionary<string, string> sourceList = new Dictionary<string, string>();
            try
            {
                DataTable dt = GetActiveSources(productId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["Name"] != DBNull.Value && dr["CountryCode"] != DBNull.Value)
                        {
                            sourceList.Add(Utility.ToString(dr["Name"]), Utility.ToString(dr["CountryCode"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sourceList;
        }
        public static DataTable GetAgentPCCDtls(int agentId, string Source)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                if (agentId > 0) paramList[0] = new SqlParameter("@AgentId", agentId);
                paramList[1] = new SqlParameter("@Source", Source);
                return DBGateway.ExecuteQuery("usp_GetPCCDynamicCredentials", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        # endregion

        # region Theme
        public static string[] GetThemByDoamin(string domainName)
        {
            try
            {
                string[] themName = new string[3];
                //string themName = string.Empty;
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@p_agent_domain", domainName);
                SqlDataReader data = DBGateway.ExecuteReaderSP("ct_p_agent_themeBydomain", paramList, connection);
                if (data.Read())
                {
                    
                    themName[0] = data["agent_img_filename"].ToString();
                    themName[1] = data["n_flex_1"].ToString();// theme
                    themName[2] = data["n_flex_2"].ToString();// copy right
                }
                
                
                    data.Close();
                    connection.Close();
                
                return themName;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            //finally
            //{
            //}
        }

        public static string GetCZInvSupplierEmail(string confirmationNo)
        {
            try
            {
                string emailIds = string.Empty;
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_Confirmation_No", confirmationNo);
                SqlDataReader data = DBGateway.ExecuteReaderSP("CZInv_Supplier_Email", paramList, connection);
                if (data.Read())
                {

                    emailIds = data["eMail1"].ToString();
                    
                }


                data.Close();
                connection.Close();

                return emailIds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            //finally
            //{
            //}
        }
        //Loading agentwise balance Added by venktesh 14.10.2016
        public static DataTable GetBalanceSummery(int agentId, DateTime dcFromDate)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                if (agentId > 0) paramList[0] = new SqlParameter("@agentId", agentId);
                paramList[1] = new SqlParameter("@toDate", dcFromDate);
                return DBGateway.ExecuteQuery("ct_p_GetBalanceSummary", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Added by Somasekhar on 29/06/2018 -- For GetAgentPaymentgateways
        public static DataTable GetAgentPaymentgateways(int agentId, int productID)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@p_agentId", agentId);
                paramList[1] = new SqlParameter("@p_productId", productID);
                return DBGateway.ExecuteQuery("usp_GetAgentPaymentGateways", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //==================================================

        #endregion
        public static bool IsCorporateAgent(int agentId)
        {
            bool status = false;
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_AGENT_ID", agentId);
                DataTable dtResult = DBGateway.FillDataTableSP("ct_p_agent_getdata", paramList);
                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    status = !string.IsNullOrEmpty(dtResult.Rows[0]["agent_corporate"].ToString()) ? dtResult.Rows[0]["agent_corporate"].ToString() == "Y" ? true : false : false;
                }
            }
            catch
            {
                throw;
            }
            return status;
        }

        /// <summary>
        /// Retrieves credit limit history for the agent id.
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public static DataTable GetCreditLimitHistory(int agentId)
        {
            DataTable dtHistory = new DataTable();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@P_AgentId", agentId));
                dtHistory = DBGateway.FillDataTableSP("usp_GetCreditLimitHistory", parameters.ToArray());
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to get Agent Credit Limit History (AgentID: " + agentId + "). Reason: " + ex.ToString(), "");
            }

            return dtHistory;
        }

        /// <summary>
        /// Updates credit limit for the agent
        /// </summary>
        public void UpdateCreditLimit()
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@P_HistoryId", 1));
                parameters.Add(new SqlParameter("@P_AgentCode", _code));
                parameters.Add(new SqlParameter("@P_AgentId", _id));
                parameters.Add(new SqlParameter("@P_CreditLimit", creditLimit));
                parameters.Add(new SqlParameter("@P_CreditLimitBuffer", creditBuffer));
                parameters.Add(new SqlParameter("@P_CreditDays", creditDays));
                parameters.Add(new SqlParameter("@P_CreatedBy", _createdBy));

                DBGateway.ExecuteNonQuerySP("usp_agent_credit_limit_add_update", parameters.ToArray());
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to update Agent Credit Limit (AgentID: " + _id + "). Reason: " + ex.ToString(), "");
                throw ex;
            }
        }

        /// <summary>
        /// Returns Active Credit Limit enabled Agents
        /// </summary>
        /// <returns></returns>
        public static DataTable GetCreditLimitAgents()
        {
            DataTable dtAgents = new DataTable();

            try
            {
                dtAgents = DBGateway.FillDataTableSP("usp_GetCreditLimitAgents", null);
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to get Credit Limit Agents. Reason: " + ex.ToString(), "");
            }

            return dtAgents;
        }

        public static string  GetAgentSalesExecDetails(long agentId)// To Show in Login Account Menu
        {

            try
            {
                string execdetails = string.Empty;
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_AGENT_ID", agentId);
                DataTable dt= DBGateway.FillDataTableSP("CT_P_AGENT_SALES_EXEC_DETAILS", paramList);
                if (dt.Rows.Count > 0) execdetails = Utility.ToString(dt.Rows[0]["ExecDetails"]);
                return execdetails;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //To Get the Tunes Insurance credentials for user level access.
        public static List<SourceDetails> GetInsuranceCredentials(string channel)
        {
            //Dictionary<string, SourceDetails> agentCredentials = new Dictionary<string, SourceDetails>();
            List<SourceDetails> sourceDetails = new List<SourceDetails>();
            DataTable dtCredentials = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                if(string.IsNullOrEmpty(channel))
                    paramList[0] = new SqlParameter("@P_Channel", DBNull.Value);
                else
                    paramList[0] = new SqlParameter("@P_Channel", channel);
                dtCredentials = DBGateway.FillDataTableSP("usp_GetTuneInsuranceCredentials", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (dtCredentials != null)
            {
                SourceDetails credentials = new SourceDetails();
                foreach (DataRow dr in dtCredentials.Rows)
                {
                    credentials.UserID = dr["xml_userid"].ToString();
                    credentials.Password = dr["xml_password"].ToString();
                    credentials.HAP = dr["xml_HAP"].ToString();
                    credentials.Type = !string.IsNullOrEmpty(dr["Type"].ToString()) ? Utility.ToInteger(dr["Type"].ToString()) : 0;
                    credentials.EndPoint = (dr["EndpointUrl"] != DBNull.Value) ? dr["EndpointUrl"].ToString() : string.Empty;
                    credentials.CurrencyCode = (dr["CurrencyCode"] != DBNull.Value) ? dr["CurrencyCode"].ToString() : string.Empty;
                    
                    sourceDetails.Add(credentials);
                }
            }

            return sourceDetails;
        }
		// Only For petrofac Report(Temp)
		public static DataTable PetrofacAgentlist()
        {
            DataTable dtAgentsList = new DataTable();

            try
            {
                dtAgentsList = DBGateway.FillDataTableSP("usp_GetPetrofacAgentList", null);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to get Agents List. Reason: " + ex.ToString(), "");
            }

            return dtAgentsList;
        }

        /// <summary>
        /// Returns Return and Routing search enabled or not
        /// </summary>
        /// <param name="agentId"></param>
        /// <returns></returns>
        public static bool FindSearchType(int agentId, bool isRoutingSearch)
        {
            bool isEnabled = false;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@P_AgentId", agentId));
                DataTable dtTypes = DBGateway.FillDataTableSP("usp_GetAgentSearchTypes", parameters.ToArray());

                if(dtTypes != null)
                {
                    foreach(DataRow dr in dtTypes.Rows)
                    {
                        if (Utility.ToBoolean(dr["agent_routing"]) && isRoutingSearch)
                            isEnabled = true;
                        if (Utility.ToBoolean(dr["agent_returnfare"]) && !isRoutingSearch)
                            isEnabled = true;
                    }
                }
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to get Agent " + (isRoutingSearch ? "Routing" : "Return") + " search type . Reason: " + ex.ToString(), "");
            }

            return isEnabled;
        }

        //To get document details data
        public static DataTable GetDocDetails(int id, string status)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@id", id);
                paramList[1] = new SqlParameter("@status", status);

                return DBGateway.FillDataTableSP("usp_GetAgentMstrReceipts", paramList);
            }
            catch
            {
                throw;
            }
        }

        //To Update document Status
        public static string DeleteAgentReceipts(int agentid, int doc_id, long created_by)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@agent_id", agentid);
                paramList[1] = new SqlParameter("@doc_Id", doc_id);
                paramList[2] = new SqlParameter("@doc_created_by",Convert.ToInt32(created_by));
                paramList[3] = new SqlParameter("@StatusMessage", SqlDbType.VarChar, 250);
                paramList[3].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuery("usp_UpdateAgentMstrReceiptsStatus", paramList);
                string message = Utility.ToString(paramList[3].Value);
                return message;
            }
            catch
            {
                throw;
            }
        }
    }


    public class AgentRegistration
    {

        #region private variables
        private long _id;
        private string _agencyName;
        private string _address;
        private string _postBoxno;
        private string _businessNature;
        private string _ownerName;
        private string _phoneno;
        private string _email;
        private string _website;
        private string _tradeLicenseNo;
        private DateTime _expiryDate;
        private int _country;
        private string _currency;
        #endregion

        #region properties
        public long ID { 
            get { return _id; }
            set { _id = value; }
        }
        public string AgencyName
        {
            get { return _agencyName; }
            set { _agencyName = value; }
        }
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public string PostBoxno
        {
            get { return _postBoxno; }
            set { _postBoxno = value; }
        }
        public string BusinessNature
        {
            get { return _businessNature; }
            set { _businessNature = value; }
        }
        public string OwnerName
        {
            get { return _ownerName; }
            set { _ownerName = value; }
        }
        public string Phoneno
        {
            get { return _phoneno; }
            set { _phoneno = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string Website
        {
            get { return _website; }
            set { _website = value; }
        }
        public string TradeLicenseNo
        {
            get { return _tradeLicenseNo; }
            set { _tradeLicenseNo = value; }
        }
        public DateTime ExpiryDate
        {
            get { return _expiryDate; }
            set { _expiryDate = value; }
        }
        public int country
        {
            get { return _country; }
            set { _country = value; }
        }
        public List<AgentReceipts> agentReceipts { get; set; }
        public string currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
        #endregion

        #region constractors
        public AgentRegistration()
        {

        }
        public AgentRegistration(long id)
        {
            _id = id;
            getDetails(id);
        }
        #endregion

        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[14];

                paramList[0] = new SqlParameter("@reg_name", _ownerName);
                paramList[1] = new SqlParameter("@reg_agencyname", _agencyName);
                paramList[2] = new SqlParameter("@reg_address", _address);
                paramList[3] = new SqlParameter("@reg_postbox", _postBoxno);
                paramList[4] = new SqlParameter("@reg_telephone", _phoneno);
                if (!string.IsNullOrEmpty(_businessNature)) paramList[5] = new SqlParameter("@reg_business", _businessNature);
                if(!string.IsNullOrEmpty(_website))paramList[6] = new SqlParameter("@reg_website", _website);
                if(!string.IsNullOrEmpty(_tradeLicenseNo))paramList[7] = new SqlParameter("@reg_license_no", _tradeLicenseNo);
                if (_expiryDate != DateTime.MinValue)
                {
                    paramList[8] = new SqlParameter("@reg_license_expiry", _expiryDate);
                }
                paramList[9] = new SqlParameter("@reg_email", _email);
                paramList[10] = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 250);
                paramList[10].Direction = ParameterDirection.Output;
                paramList[11] = new SqlParameter("@reg_country", _country);
                paramList[12] = new SqlParameter("@reg_id", _id);
                paramList[12].Direction = ParameterDirection.Output;
                paramList[13] = new SqlParameter("@reg_currency", _currency);
                DBGateway.ExecuteNonQuery("CT_P_REGISTER_ADD", paramList);
                string message = Utility.ToString(paramList[10].Value);
                int reg_id = Utility.ToInteger(paramList[12].Value);
                if (agentReceipts != null && agentReceipts.Count > 0 && reg_id > 0)
                {
                    agentReceipts.Where(x => x != null).ToList().ForEach(x => {

                        x.reg_Id = reg_id;
                        x.Save();
                    });
                }
                if (!string.IsNullOrEmpty(message))
                {
                    throw new Exception(message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable GetList( int agentCountry)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@_Agent_Country", agentCountry);
                return DBGateway.ExecuteQuery("CT_P_REGISTER_GETLIST", paramList).Tables[0];
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private void getDetails(long id)
        {
            DataSet ds = GetData(id);
            UpdateBusinessData(ds);
        }

        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@Reg_id", id);
                return DBGateway.ExecuteQuery("CT_P_REGISTER_GETDATA", paramList);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["reg_id"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch(Exception ex)
            { throw ex; }
        }

        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                    _id = Utility.ToLong(dr["reg_id"]);
                    _address = Utility.ToString(dr["reg_address"]);
                    _agencyName = Utility.ToString(dr["reg_agencyname"]); ;
                    _postBoxno = Utility.ToString(dr["reg_postbox"]); ;
                    _businessNature = Utility.ToString(dr["reg_business"]); ;
                    _ownerName = Utility.ToString(dr["reg_name"]); ;
                    _phoneno = Utility.ToString(dr["reg_telephone"]); ;
                    _email = Utility.ToString(dr["reg_email"]); ;
                    _website = Utility.ToString(dr["reg_website"]); ;
                    _tradeLicenseNo = Utility.ToString(dr["reg_license_no"]); ;
                    _expiryDate = Utility.ToDate(dr["reg_license_expiry"]); ;
                    _currency = Utility.ToString(dr["reg_currency"]); ;
            }
            catch
            {
                throw;
            }
        }

        public static void UpdateAgentRegStatus(int Id,string status)
        { 
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_Reg_ID", Id);
                paramList[1] = new SqlParameter("@P_Reg_Status", status);
                DBGateway.ExecuteNonQuery("ct_p_UPDATE_REGISTER_STATUS", paramList);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        //To get document details data
        public static DataTable GetDocDetails(int reg_id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@reg_id", reg_id);

                return DBGateway.FillDataTableSP("usp_GetAgentReceipts", paramList);
            }
            catch
            {
                throw;
            }
        }
    }

    public class AgentReceipts
    {
        /// <summary>
        ///   Doc ID
        /// </summary>
        public int doc_Id { get; set; }
        /// <summary>
        ///   Reg ID
        /// </summary>
        public int reg_Id { get; set; }
        /// <summary>
        ///   Doc Code
        /// </summary>
        public string doc_code { get; set; }
        /// <summary>
        /// Doc Name
        /// </summary>
        public string doc_name { get; set; }
        /// <summary>
        ///   Doc Path
        /// </summary>
        public string doc_path { get; set; }
        /// <summary>
        /// Receipt Doc Type
        /// </summary>
        public string doc_type { get; set; }
        /// <summary>
        ///   Doc Status
        /// </summary>
        public bool doc_Status { get; set; }
        /// <summary>
        ///   Doc Created By
        /// </summary>
        public int doc_created_by { get; set; }
        /// <summary>
        ///   Doc Created On
        /// </summary>
        public string doc_created_on { get; set; }
        /// <summary>
        ///   Doc Modified By
        /// </summary>
        public int doc_modified_by { get; set; }
        /// <summary>
        ///   Doc Modified On
        /// </summary>
        public string doc_modified_on { get; set; }

        public void Save()
        {
            try
            {
                List<SqlParameter> liParamslist = new List<SqlParameter>();
                liParamslist.Add(new SqlParameter("@doc_Id", doc_Id));
                if (doc_Id == 0)
                    liParamslist[0].Direction = ParameterDirection.Output;
                    liParamslist.Add(new SqlParameter("@reg_id", reg_Id));
                    liParamslist.Add(new SqlParameter("@doc_code", doc_code));
                    liParamslist.Add(new SqlParameter("@doc_name", doc_name));
                    liParamslist.Add(new SqlParameter("@doc_file_path", doc_path));
                    liParamslist.Add(new SqlParameter("@doc_file_type", doc_type));
                    liParamslist.Add(new SqlParameter("@doc_status", 1));
                    int res = DBGateway.ExecuteNonQuery("usp_SaveAgentReceipts", liParamslist.ToArray());
                    doc_Id = Convert.ToInt32(liParamslist[0].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }


    public class AgentMasterReceipts
    {
        /// <summary>
        ///   Doc ID
        /// </summary>
        public int doc_Id { get; set; }
        /// <summary>
        ///   Ret Doc ID
        /// </summary>
        public int ret_doc_Id { get; set; }
        /// <summary>
        ///   Reg ID
        /// </summary>
        public int agent_Id { get; set; }
        /// <summary>
        ///   Doc Code
        /// </summary>
        public string doc_code { get; set; }
        /// <summary>
        /// Doc Name
        /// </summary>
        public string doc_name { get; set; }
        /// <summary>
        ///   Doc Path
        /// </summary>
        public string doc_path { get; set; }
        ///  reg Doc Path
        /// </summary>
        public string doc_reg_path { get; set; }
        /// <summary>
        /// Receipt Doc Type
        /// </summary>
        public string doc_type { get; set; }
        /// <summary>
        ///   Doc Status
        /// </summary>
        public bool doc_Status { get; set; }
        /// <summary>
        ///   Doc Created By
        /// </summary>
        public int doc_created_by { get; set; }
        /// <summary>
        ///   Doc Created On
        /// </summary>
        public string doc_created_on { get; set; }
        /// <summary>
        ///   Doc Modified By
        /// </summary>
        public int doc_modified_by { get; set; }
        /// <summary>
        ///   Doc Modified On
        /// </summary>
        public string doc_modified_on { get; set; }

        public void Save()
        {
            try
            {
                ret_doc_Id = (doc_Id == 0)? 0 : doc_Id;
                List<SqlParameter> liParamslist = new List<SqlParameter>();
                liParamslist.Add(new SqlParameter("@doc_Id", doc_Id));
                if (doc_Id == 0)
                    liParamslist[0].Direction = ParameterDirection.Output;
                liParamslist.Add(new SqlParameter("@agent_id", agent_Id));
                liParamslist.Add(new SqlParameter("@doc_code", doc_code));
                liParamslist.Add(new SqlParameter("@doc_name", doc_name));
                liParamslist.Add(new SqlParameter("@doc_file_path", doc_path));
                liParamslist.Add(new SqlParameter("@doc_file_type", doc_type));
                liParamslist.Add(new SqlParameter("@doc_status", 1));
                liParamslist.Add(new SqlParameter("@doc_created_by", doc_created_by));
                liParamslist.Add(new SqlParameter("@doc_ret_id", ret_doc_Id));
                int res = DBGateway.ExecuteNonQuery("usp_SaveAgentDocReceipts", liParamslist.ToArray());
                doc_Id = Convert.ToInt32(liParamslist[0].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }


}
