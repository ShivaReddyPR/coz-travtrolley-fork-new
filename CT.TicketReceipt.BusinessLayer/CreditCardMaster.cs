using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class CreditCardMaster
    {
        private const long NEW_RECORD = -1;
        
        #region Static Methods
        public static DataTable GetList(ListStatus status,RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];

                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("ct_p_credit_card_getlist", paramList).Tables[0];
            }
            catch{throw;}
        }
        # endregion
       
    }
   
}
