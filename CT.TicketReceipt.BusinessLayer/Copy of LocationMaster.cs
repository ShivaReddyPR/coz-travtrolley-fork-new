using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{    
    public class LocationMaster
    {
        private const long NEW_RECORD = -1;
        #region Member Variables        
        private long _id;
        private string _code;
        private string _name;
        private string _docType;
        private string _status;
        private long _createdBy;
        private string _currency;
              
        #endregion

        #region Properties
        public long ID
        {
            get { return _id; }            
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string DocType
        {
            get { return _docType; }
            set { _docType = value; }
        }
         public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        
        #endregion

        #region Constructors
        public LocationMaster()
        {            
            _id = NEW_RECORD;
        }
        public LocationMaster(long id)
        {
            
            
            _id = id;
            getDetails(id);
        }
        #endregion

        #region Methods
        private void getDetails(long id)
        {
            
            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }
            
        }
        private DataSet GetData(long id)
        {
            
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_LOCATION_ID", id);                 
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_location_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {
            
            try
            {
                _id = Utility.ToLong(dr["LOCATION_ID"]);
                _code = Utility.ToString(dr["LOCATION_CODE"]);
                _name = Utility.ToString(dr["LOCATION_NAME"]);
                _docType = Utility.ToString(dr["LOCATION_DOC_TYPE"]);
                 _currency = Utility.ToString(dr["LOCATION_CURRENCY"]);
                _status = Utility.ToString(dr["LOCATION_STATUS"]);
                _createdBy = Utility.ToLong(dr["LOCATION_CREATED_BY"]);
                
                

            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {
            
            try
            {
                SqlParameter[] paramList = new SqlParameter[9];

                paramList[0] = new SqlParameter("@P_LOCATION_ID", _id);
                paramList[1] = new SqlParameter("@P_LOCATION_CODE", _code);
                paramList[2] = new SqlParameter("@P_LOCATION_NAME", _name);
                paramList[3] = new SqlParameter("@P_LOCATION_DOC_TYPE", _docType);
                paramList[4] = new SqlParameter("@P_LOCATION_CURRENCY", _currency);
                paramList[5] = new SqlParameter("@P_LOCATION_STATUS", _status);
                paramList[6] = new SqlParameter("@P_LOCATION_CREATED_BY", _createdBy);
                paramList[7] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[7].Direction = ParameterDirection.Output;
                paramList[8] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[8].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_location_add_update", paramList);
                string messageType = Utility.ToString(paramList[7].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[8].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {            
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LOCATION_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output; 
                
                DBGateway.ExecuteNonQuery("ct_p_location_add_update", paramList);                
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Static Methods
        public static DataTable GetList(ListStatus status, RecordStatus recordStatus)
        {
            
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];

                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                return DBGateway.ExecuteQuery("ct_p_location_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
