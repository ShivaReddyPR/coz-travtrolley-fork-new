using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class Reports
    {
       
       
        public static DataSet GetVisaReconcileList(DateTime fromDate,DateTime toDate,int visaTypeId)
        {

            try
            {
                SqlParameter[] paramArr = new SqlParameter[5];
                paramArr[0] = new SqlParameter("@p_from_date", fromDate);
                paramArr[1] = new SqlParameter("@p_to_date", toDate);
                if (visaTypeId > 0) paramArr[2] = new SqlParameter("@p_vs_visa_type_id", visaTypeId);
                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[3] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[4] = paramMsgText;

                return DBGateway.ExecuteQuery("visa_p_visa_sales_reco_getList", paramArr);
            }
            catch
            {
                throw;
            }
        }
        public static DataSet GetTypistIncentiveDetails(DateTime fromDate, DateTime toDate, long visaTypeId,long salesTypeId,long userId,long locationId,string reportType,long incentiveCount)
        {

            try
            {
                SqlParameter[] paramArr = new SqlParameter[8];
                paramArr[0] = new SqlParameter("@p_from_date", fromDate);
                paramArr[1] = new SqlParameter("@p_to_date", toDate);
                if (visaTypeId > 0) paramArr[2] = new SqlParameter("@p_vs_visa_type_id", visaTypeId);
                if (salesTypeId > 0) paramArr[3] = new SqlParameter("@P_VS_SALES_TYPE_ID", salesTypeId);
                if (userId > 0) paramArr[4] = new SqlParameter("@P_USER_ID", userId );
                if (locationId  > 0) paramArr[5] = new SqlParameter("@P_VS_LOCATION_ID", locationId );
                paramArr[6] = new SqlParameter("@P_REPORT_TYPE", reportType );
                if (incentiveCount > 0) paramArr[7] = new SqlParameter("@P_INCENTIVE_COUNT", incentiveCount);
                return DBGateway.ExecuteQuery("visa_p_visa_sales_typist_incentive_details", paramArr);
            }
            catch
            {
                throw;
            }
        }

        //public static DataSet GetAgentCreditBalance(int agentId)
        //{

        //    try
        //    {
        //        SqlParameter[] paramList = new SqlParameter[1];
        //        if (agentId > 0) paramList[0] = new SqlParameter("@P_AGENT_ID", agentId);
        //        return DBGateway.ExecuteQuery("ct_p_agent_credit_balance", paramList);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
       
    }
   
 
    
   
}
