using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;

/// <summary>
/// Summary description for HandlingFeeMaster
/// </summary>
 namespace CT.TicketReceipt.BusinessLayer
{
public class ReceiptMaster
{
    # region members
    long _transactionId;
    string _docType;
    string _docNumber;
    DateTime  _docDate;
    string _paxName;

    string _paxMobileNo;
    string _paxEmail;

    string _pnr;
    int _adults;
    int _children;
    int _infants;
    decimal _fare;
    decimal _handlingFee;
    long  _handlingId;
    decimal _totalFare;
    long _locationId;
    string _remarks;
    string _status;
    long _createdBy;
    string _createdByName;
    DateTime _dateOfTravel;
    string _destination;
    string _packageStatus = "T";//TODO T-Ticket Module,V- Visa Module
    string _settlementMode;
    string _currencyCode;
    decimal _exchangeRate;
    string _cardName;
    decimal _cardRate;
    long _cardId;
    decimal _cardTotal;

    decimal _baseToCollect;
    decimal _localToCollect;

    PaymentModeInfo _cashMode = new PaymentModeInfo();
    PaymentModeInfo _creditMode = new PaymentModeInfo();
    PaymentModeInfo _cardMode = new PaymentModeInfo();
    PaymentModeInfo _employeeMode = new PaymentModeInfo();
    PaymentModeInfo _othersMode = new PaymentModeInfo();
    string _modeRemarks;
    # endregion

    # region Properties
    public long TransactionId
    {
        get {return _transactionId ;}
        set { _transactionId = value; }
    }
    public string DocType
    {
        get {return _docType;}
        set { _docType = value; }
    }
    public string DocNumber
    {
        get { return _docNumber; }
        set { _docNumber = value; }
    }

    public DateTime DocDate
    {
        get { return _docDate; }
        set { _docDate = value; }
    }
    public string PaxName
    {
        get { return _paxName; }
        set { _paxName = value; }
    }

    public string PaxMobileNo
    {
        get { return _paxMobileNo; }
        set { _paxMobileNo = value; }
    }

    public string PaxEmail
    {
        get { return _paxEmail; }
        set { _paxEmail = value; }
    }


    public string PNR
    {
        get { return _pnr; }
        set { _pnr = value; }
    }

    public int Adults
    {
        get { return _adults; }
        set { _adults = value; }
    }
    public int Children
    {
        get { return _children; }
        set { _children = value; }
    }
    public int Infants
    {
        get { return _infants; }
        set { _infants = value; }
    }
    public decimal Fare
    {
        get { return _fare; }
        set { _fare = value; }
    }
    public decimal HandlingFee
    {
        get { return _handlingFee; }
        set { _handlingFee = value; }
    }
    public long HandlingId
    {
        get { return _handlingId; }
        set { _handlingId = value; }
    }
    public decimal TotalFare
    {
        get { return _totalFare; }
        set { _totalFare = value; }
    }

    public string Remarks
    {
        get { return _remarks; }
        set { _remarks = value; }
    }
    public long LocationId
    {
        get { return _locationId; }
        set { _locationId = value; }
    }
    public string Status
    {
        get { return _status; }
        set { _status = value; }
    }
    public long CreatedBy
    {
        get { return _createdBy; }
        set { _createdBy = value; }
    }
    public string CreatedByName
    {
        get { return _createdByName; }
        set { _createdByName = value; }
    }
    public DateTime DateOfTravel
    {
        get { return _dateOfTravel; }
        set { _dateOfTravel= value; }
    }
    public string Destination
    {
        get { return _destination; }
        set { _destination = value; }
    }


    public string PackageStatus
    {
        get { return _packageStatus; }
        set { _packageStatus = value; }
    }

    public string SettlementMode
    {
        get { return _settlementMode; }
        set { _settlementMode = value; }
    }
    public string CurrencyCode
    {
        get { return _currencyCode; }
        set { _currencyCode = value; }
    }
    public decimal ExchangeRate
    {
        get { return _exchangeRate; }
        set { _exchangeRate = value; }
    }
    public PaymentModeInfo CashMode
    {
        get { return _cashMode; }
        set { _cashMode = value; }
    }
    public PaymentModeInfo CreditMode
    {
        get { return _creditMode; }
        set { _creditMode = value; }
    }
    public PaymentModeInfo CardMode
    {
        get { return _cardMode; }
        set { _cardMode = value; }
    }
    public PaymentModeInfo EmployeeMode
    {
        get { return _employeeMode; }
        set { _employeeMode = value; }
    }
    public PaymentModeInfo OthersMode
    {
        get { return _othersMode; }
        set { _othersMode = value; }
    }
    public string ModeRemarks
    {
        get { return _modeRemarks; }
        set { _modeRemarks = value; }
    }

    public string CardName
    {
        get { return _cardName; }
        set { _cardName = value; }
    }
    public long CardId
    {
        get { return _cardId; }
        set { _cardId = value; }
    }

    public decimal  CardRate
    {
        get { return _cardRate; }
        set { _cardRate = value; }
    }
    public decimal CardTotal
    {
        get { return _cardTotal; }
        set { _cardTotal = value; }
    }

    public decimal BaseToCollect
    {
        get { return _baseToCollect; }
        set { _baseToCollect = value; }
    }

    public decimal LocalToCollect
    {
        get { return _localToCollect ; }
        set { _localToCollect = value; }
    }

    # endregion

    # region Constructors

    public ReceiptMaster()
    {
        //
        // TODO: Add constructor logic here
        //
        _transactionId = -1;
        DataTable dt = GetDetails(_transactionId);
        if (dt != null && dt.Rows.Count > 0)
            UpdateBusinessData(dt.Rows[0]);
    }
    public ReceiptMaster(long transactionId)
    {
        try
        {
            DataTable dt = GetDetails(transactionId);
            if(dt!=null && dt.Rows.Count>0)
                UpdateBusinessData(dt.Rows[0]);
        }
        catch { throw; }

    }
    # endregion

    # region Private Methods
    private DataTable GetDetails(long transactionId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[2];
            SqlParameter param;
            param = new SqlParameter("@p_receipt_id", SqlDbType.Decimal);
            param.Value = transactionId; param.Size = 8;
            paramArr[0] = param;

            param = new SqlParameter("@p_doc_location_id", SqlDbType.Decimal);
            param.Value = Settings.LoginInfo.LocationID; param.Size = 8;
            paramArr[1] = param;

            
            DataTable dt = DBGateway.ExecuteQuery("ct_p_receipt_getdata", paramArr).Tables[0];
            return dt;
        }catch { throw; }
    }
    private void UpdateBusinessData(DataRow dr)
    {
        try
        {
            if (dr != null)
            {
                if (_transactionId < 0)
                {
                    _docType = Utility.ToString(dr["doc_type"]);
                    _docDate = Utility.ToDate(dr["receipt_date"]);
                }
                else
                {
                    _transactionId = Utility.ToLong(dr["receipt_id"]);
                    _docNumber = Utility.ToString(dr["receipt_number"]);
                    _docType = Utility.ToString(dr["receipt_type"]);
                    _docDate = Utility.ToDate(dr["receipt_date"]);
                    _paxName = Utility.ToString(dr["receipt_pax_name"]);
                    _pnr = Utility.ToString(dr["receipt_pnr"]);
                    _adults = Utility.ToInteger(dr["receipt_adults"]);
                    _children = Utility.ToInteger(dr["receipt_children"]);
                    _infants = Utility.ToInteger(dr["receipt_infants"]);
                    _fare = Utility.ToDecimal(dr["receipt_fare"]);
                    _handlingFee = Utility.ToDecimal(dr["receipt_handling_fee"]);
                    _handlingId = Utility.ToLong(dr["receipt_handling_id"]);
                    _totalFare = Utility.ToDecimal(dr["receipt_total_fee"]);
                    _remarks = Utility.ToString(dr["receipt_remarks"]);
                    _locationId = Utility.ToLong(dr["location_id"]);
                    _status = Utility.ToString(dr["receipt_status"]);
                    _createdBy = Utility.ToLong(dr["receipt_created_by"]);
                    _createdByName = Utility.ToString(dr["receipt_created_name"]);
                    _dateOfTravel= Utility.ToDate(dr["receipt_travel_date"]);
                    _destination = Utility.ToString(dr["receipt_destination"]);
                    _packageStatus = Utility.ToString(dr["receipt_package_status"]);
                    //Settlement Details
                    _settlementMode = Utility.ToString(dr["RECEIPT_settlement_mode"]);
                    _currencyCode = Utility.ToString(dr["RECEIPT_CURRENCY_CODE"]);
                    _exchangeRate = Utility.ToDecimal(dr["RECEIPT_EXCHANGE_RATE"]);
                    _cashMode.Set(Utility.ToString(dr["RECEIPT_SETTLEMENT_MODE_DESCRIPTION"]), Utility.ToDecimal(dr["cash_base_amount"]), Utility.ToDecimal(dr["cash_local_amount"]));
                    _creditMode.Set(Utility.ToString(dr["RECEIPT_SETTLEMENT_MODE_DESCRIPTION"]), Utility.ToDecimal(dr["credit_base_amount"]), Utility.ToDecimal(dr["credit_local_amount"]));
                    _cardMode.Set(Utility.ToString(dr["RECEIPT_SETTLEMENT_MODE_DESCRIPTION"]), Utility.ToDecimal(dr["card_base_amount"]), Utility.ToDecimal(dr["card_local_amount"]));
                    _employeeMode.Set(Utility.ToString(dr["RECEIPT_SETTLEMENT_MODE_DESCRIPTION"]), Utility.ToDecimal(dr["employee_base_amount"]), Utility.ToDecimal(dr["employee_local_amount"]));
                    _othersMode.Set(Utility.ToString(dr["RECEIPT_SETTLEMENT_MODE_DESCRIPTION"]), Utility.ToDecimal(dr["others_base_amount"]), Utility.ToDecimal(dr["others_local_amount"]));
                    _modeRemarks = Utility.ToString(dr["mode_remarks"]);

                    _cardId = Utility.ToLong(dr["crcard_id"]);
                    _cardName = Utility.ToString(dr["crcard_name"]);
                    _cardRate = Utility.ToDecimal(dr["crcard_rate"]);
                    _cardTotal = Utility.ToDecimal(dr["crcard_total_amount"]);
                    _baseToCollect = Utility.ToDecimal(dr["receipt_base_to_collect"]);
                    _localToCollect  = Utility.ToDecimal(dr["receipt_local_to_collect"]);

                    _paxMobileNo = Utility.ToString(dr["RECEIPT_PAX_MOBILENO"]);
                    _paxEmail = Utility.ToString(dr["RECEIPT_PAX_EMAIL"]);

                }
            }


        }
        catch { throw; }

    }
    public void Save()
    {
        try
        {
        SqlParameter[] paramArr = new SqlParameter[51];
        SqlParameter param = new SqlParameter("@p_receipt_id", SqlDbType.BigInt);
        param.Value = _transactionId; param.Size = 8;
        paramArr[0] = param;


        param = new SqlParameter("@p_receipt_type", SqlDbType.NVarChar);
        param.Value = _docType; param.Size = 10;
        paramArr[1] = param;

        SqlParameter paramDocNo = new SqlParameter("@p_receipt_number", SqlDbType.NVarChar);
        paramDocNo.Direction= ParameterDirection.Output; paramDocNo.Size = 50;
        paramArr[2] = paramDocNo;

        param = new SqlParameter("@p_receipt_date", SqlDbType.DateTime);
        param.Value = _docDate; param.Size = 18;
        paramArr[3] = param;

        param = new SqlParameter("@p_receipt_pax_name", SqlDbType.NVarChar);
        param.Value = _paxName; param.Size = 50;
        paramArr[4] = param;

        param = new SqlParameter("@p_receipt_pnr", SqlDbType.NVarChar);
        param.Value = _pnr; param.Size = 50;
        paramArr[5] = param;

        param = new SqlParameter("@p_receipt_adults", SqlDbType.Int);
        param.Value = _adults; param.Size = 8;
        paramArr[6] = param;

        param = new SqlParameter("@p_receipt_children", SqlDbType.Int);
        param.Value = _children; param.Size = 8;
        paramArr[7] = param;

        param = new SqlParameter("@p_receipt_infants", SqlDbType.Int);
        param.Value = _infants; param.Size = 8;
        paramArr[8] = param;

        param = new SqlParameter("@p_receipt_fare", SqlDbType.Decimal);
        param.Value = _fare; param.Size = 18;
        paramArr[9] = param;

        param = new SqlParameter("@p_receipt_handling_id", SqlDbType.BigInt);
        param.Value = _handlingId; param.Size = 8;
        paramArr[10] = param;

        param = new SqlParameter("@p_receipt_handling_fee", SqlDbType.Decimal);
        param.Value = _handlingFee; param.Size = 18;
        paramArr[11] = param;

        param = new SqlParameter("@p_receipt_total_fare", SqlDbType.Decimal);
        param.Value = _totalFare; param.Size = 18;
        paramArr[12] = param;

       


        param = new SqlParameter("@p_receipt_remarks", SqlDbType.NVarChar);
        param.Value = _remarks; param.Size = 250;
        paramArr[13] = param;



        param = new SqlParameter("@p_receipt_location_id", SqlDbType.BigInt);
        param.Value = _locationId; param.Size = 18;
        paramArr[14] = param;

        paramArr[15] = new SqlParameter("@p_receipt_package_status", _packageStatus);

        //starting Mode
        paramArr[16] = new SqlParameter("@P_RECEIPT_SETTLEMENT_MODE", _settlementMode);
        paramArr[17] = new SqlParameter("@P_RECEIPT_CURRENCY_CODE", _currencyCode);
        paramArr[18] = new SqlParameter("@P_RECEIPT_EXCHANGE_RATE", _exchangeRate);
        paramArr[19] = new SqlParameter("@P_CASH_MODE", _cashMode.Mode);
        paramArr[20] = new SqlParameter("@P_CASH_BASE_AMOUNT", _cashMode.BaseAmount);
        paramArr[21] = new SqlParameter("@P_CASH_LOCAL_AMOUNT", _cashMode.LocalAmount);
        paramArr[22] = new SqlParameter("@P_CREDIT_MODE", _creditMode.Mode);
        paramArr[23] = new SqlParameter("@P_CREDIT_BASE_AMOUNT", _creditMode.BaseAmount);
        paramArr[24] = new SqlParameter("@P_CREDIT_LOCAL_AMOUNT", _creditMode.LocalAmount);
        paramArr[25] = new SqlParameter("@P_CARD_MODE", _cardMode.Mode);
        paramArr[26] = new SqlParameter("@P_CARD_BASE_AMOUNT", _cardMode.BaseAmount);
        paramArr[27] = new SqlParameter("@P_CARD_LOCAL_AMOUNT", _cardMode.LocalAmount);
        paramArr[28] = new SqlParameter("@P_EMPLOYEE_MODE", _employeeMode.Mode);
        paramArr[29] = new SqlParameter("@P_EMPLOYEE_BASE_AMOUNT", _employeeMode.BaseAmount);
        paramArr[30] = new SqlParameter("@P_EMPLOYEE_LOCAL_AMOUNT", _employeeMode.LocalAmount);
        paramArr[31] = new SqlParameter("@P_OTHERS_MODE", _othersMode.Mode);
        paramArr[32] = new SqlParameter("@P_OTHERS_BASE_AMOUNT", _othersMode.BaseAmount);
        paramArr[33] = new SqlParameter("@P_OTHERS_LOCAL_AMOUNT", _othersMode.LocalAmount);
        paramArr[34] = new SqlParameter("@P_MODE_REMARKS", _modeRemarks);

        paramArr[35] = new SqlParameter("@P_CRCARD_ID", _cardId);
        paramArr[36] = new SqlParameter("@P_CRCARD_NAME", _cardName);
        paramArr[37] = new SqlParameter("@P_CRCARD_RATE", _cardRate);
        paramArr[38] = new SqlParameter("@P_CRCARD_TOTAL_AMOUNT", _cardTotal);
        paramArr[39] = new SqlParameter("@P_RECEIPT_BASE_TO_COLLECT", _baseToCollect );
        paramArr[40] = new SqlParameter("@P_RECEIPT_LOCAL_TO_COLLECT", _localToCollect);
        //ending Mode

        param = new SqlParameter("@p_receipt_status", SqlDbType.NVarChar);
        param.Value = _status; param.Size = 10;
        paramArr[41] = param;


        param = new SqlParameter("@p_receipt_created_by", SqlDbType.BigInt);
        param.Value = _createdBy; param.Size = 8;
        paramArr[42] = param;

        if (_dateOfTravel!= DateTime.MinValue) paramArr[43] = new SqlParameter("@P_RECEIPT_TRAVEL_DATE", _dateOfTravel);
        if (!string.IsNullOrEmpty(Destination)) paramArr[44] = new SqlParameter("@P_RECEIPT_DESTINATION", _destination);
        paramArr[45] = new SqlParameter("@P_RECEIPT_COMPANY_ID", Settings.LoginInfo.CompanyID);
        if (!string.IsNullOrEmpty(_paxMobileNo)) paramArr[46] = new SqlParameter("@P_RECEIPT_PAX_MOBILENO", _paxMobileNo);
        if (!string.IsNullOrEmpty(_paxEmail)) paramArr[47] = new SqlParameter("@P_RECEIPT_PAX_EMAIL", _paxEmail);

        SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
        paramMsgType.Size = 10;
        paramMsgType.Direction = ParameterDirection.Output;
        paramArr[48] = paramMsgType;

        SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
        paramMsgText.Size = 100;
        paramMsgText.Direction = ParameterDirection.Output;
        paramArr[49] = paramMsgText;

        SqlParameter paramRetValue = new SqlParameter("@P_RECEIPT_ID_RET", SqlDbType.BigInt);
        paramRetValue.Size = 100;
        paramRetValue.Direction = ParameterDirection.Output;
        paramArr[50] = paramRetValue;
        


        DBGateway.ExecuteNonQuery("ct_p_receipt_add_update", paramArr);
        if (Utility.ToString(paramMsgType.Value )== "E")
            throw new Exception(Utility.ToString(paramMsgText.Value));

        _docNumber = Utility.ToString(paramDocNo.Value);
        _transactionId = Utility.ToLong(paramRetValue.Value);
    }
    catch { throw; }
}
    # endregion

    # region Static Methods
    public static DataSet GetList(DateTime fromDate, DateTime toDate, long userID, string memberType, long locationId, int companyId, string settlementMode, long receiptLocationId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[8];
            paramArr[0] = new SqlParameter("@p_from_date", fromDate);
            paramArr[1] = new SqlParameter("@p_to_date", toDate);
            SqlParameter param = new SqlParameter("@p_user_id", SqlDbType.BigInt);
            param.Value = userID; param.Size = 8;
            paramArr[2] = param;

            param = new SqlParameter("@p_user_type", SqlDbType.NVarChar);
            param.Value = memberType; param.Size = 50;
            paramArr[3] = param;

            param = new SqlParameter("@p_user_location_id", SqlDbType.BigInt);
            param.Value = locationId; param.Size = 16;
            paramArr[4] = param;
            paramArr[5] = new SqlParameter("@p_receipt_company_id", companyId);
            if (settlementMode != "-1") paramArr[6] = new SqlParameter("@p_settlement_mode", settlementMode);
            if (receiptLocationId > 0) paramArr[7] = new SqlParameter("@P_RECEIPT_LOCATION_ID", receiptLocationId);

            return DBGateway.ExecuteQuery("ct_p_receipt_getlist", paramArr);
        }
        catch { throw; }
    }
    public static DataTable GetReportDetails(long transactionId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[1];
            SqlParameter param = new SqlParameter("@p_receipt_id", SqlDbType.Decimal);
            param.Value = transactionId; param.Size = 8;
            paramArr[0] = param;

            DataTable dt = DBGateway.ExecuteQuery("ct_p_receipt_getdata", paramArr).Tables[0];
            return dt;
        }
        catch { throw; }
    }
    # endregion
}
}
