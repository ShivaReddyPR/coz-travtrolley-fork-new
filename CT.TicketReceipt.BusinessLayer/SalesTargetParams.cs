
using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{

    public class SalesTargetParams
    {
        private const long NEW_RECORD = -1;

        #region Member Variables
        
        private long _id;
        private string _targetType;
        private string _break1;
        private string _break2;
        private string _break3;
        private string _break4;
        private string _break5;
        private string _break6;
        private string _break7;
        private string _break8;
        private string _break9;
        private string _break10;

        private long _year;
        private long _month;
        private long _period;
        private string _targetTransType;

        private double  _amount1 ;
        private double _amount1_percent;
        private double _amount2 ;
        private double _amount2_percent;
        private double _amount3 ;
        private double _amount3_percent;
        private double _amount4 ;
        private double _amount4_percent;
        private double _amount5;
        private double _amount5_percent;

        private string _category;
        private string _createdBy;

        private string _break1_desc;
        private string _break2_desc;
        private string _break3_desc;
        private string _break4_desc;
        private string _break5_desc;
        private string _break6_desc;
        private string _break7_desc;
        private string _break8_desc;
        private string _break9_desc;
        private string _break10_desc;
        private string _uploadPath;
        private string _uploadType;
        #endregion

        #region Properties


        public long ID
        {
            get { return _id; }
        }

        public string TergetType
        {
            get { return _targetType; }
            set { _targetType = value; }
        }
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }
        public string Break1
        {
            get { return _break1; }
            set { _break1 = value; }
        }
        public string Break2
        {
            get { return _break2; }
            set { _break2 = value; }
        }
        public string Break3
        {
            get { return _break3; }
            set { _break3 = value; }
        }
        public string Break4
        {
            get { return _break4; }
            set { _break4 = value; }
        }
        public string Break5
        {
            get { return _break5; }
            set { _break5 = value; }
        }


        public string Break6
        {
            get { return _break6; }
            set { _break6 = value; }
        }

        public string Break7
        {
            get { return _break7; }
            set { _break7 = value; }
        }

        public string Break8
        {
            get { return _break8; }
            set { _break8 = value; }
        }

        public string Break9
        {
            get { return _break9; }
            set { _break9 = value; }
        }

        public string Break10
        {
            get { return _break10; }
            set { _break10 = value; }
        }

        public long Year
        {
            get { return _year; }
            set { _year = value; }
        }
        public long Month
        {
            get { return _month;}
            set { _month = value; }
        }       

        public long Period
        {
            get { return _period; }
            set { _period = value; }
        }

        public string TergetTransType
        {
            get { return _targetTransType; }
            set { _targetTransType = value; }
        }
        public string UploadPath
        {
            get { return _uploadPath; }
            set { _uploadPath = value; }
        }

        public string UploadType
        {
            get { return _uploadType; }
            set { _uploadType = value; }
        }

        public double Amount1
        {
            get { return _amount1; }
            set { _amount1 = value; }
        }
        public double Amount2
        {
            get { return _amount2; }
            set { _amount2 = value; }
        }
        public double Amount3
        {
            get { return _amount3; }
            set { _amount3 = value; }
        }
        public double Amount4
        {
            get { return _amount4; }
            set { _amount4 = value; }
        }
        public double Amount5
        {
            get { return _amount5; }
            set { _amount5 = value; }
        }

        public string CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public string Break1_desc
        {
            get { return _break1_desc; }
            set { _break1_desc = value; }
        }
        public string Break2_desc
        {
            get { return _break2_desc; }
            set { _break2_desc = value; }
        }
        public string Break3_desc
        {
            get { return _break3_desc; }
            set { _break3_desc = value; }
        }
        public string Break4_desc
        {
            get { return _break4_desc; }
            set { _break4_desc = value; }
        }
        public string Break5_desc
        {
            get { return _break5_desc; }
            set { _break5_desc = value; }
        }


        public string Break6_desc
        {
            get { return _break6_desc; }
            set { _break6_desc = value; }
        }

        public string Break7_desc
        {
            get { return _break7_desc; }
            set { _break7_desc = value; }
        }

        public string Break8_desc
        {
            get { return _break8_desc; }
            set { _break8_desc = value; }
        }

        public string Break9_desc
        {
            get { return _break9_desc; }
            set { _break9_desc = value; }
        }

        public string Break10_desc
        {
            get { return _break10_desc; }
            set { _break10_desc = value; }
        }

        #endregion

        #region Constructors
        public SalesTargetParams()
        {
            _id = -1;
        }
        public SalesTargetParams(long id)
        {


            _id = id;
        
            getDetails(id);
        }
        #endregion

        #region Methods
        private void getDetails(long refNo)
        {

            DataSet ds = GetData(refNo);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(long refNo)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_TARGET_REFERENCE", refNo);
                DataSet dsResult = DBGateway.ExecuteQuery("CT_P_TRAVEL_SALES_TARGET_GETDATA", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                if (_id < 0)
                {
                    _id = Utility.ToLong(dr["target_reference"]);
                }
                else
                {
                    _category = Utility.ToString(dr["SETUP_CATEGORY"]);
                    _targetType = Utility.ToString(dr["TARGET_TYPE"]);
                    _break1 = Utility.ToString(dr["TARGET_BREAK_1"]);
                    _break2 = Utility.ToString(dr["TARGET_BREAK_2"]);
                    _break3 = Utility.ToString(dr["TARGET_BREAK_3"]);
                    _break4 = Utility.ToString(dr["TARGET_BREAK_4"]);
                    _break5 = Utility.ToString(dr["TARGET_BREAK_5"]);
                    _break6 = Utility.ToString(dr["TARGET_BREAK_6"]);
                    _break7 = Utility.ToString(dr["TARGET_BREAK_7"]);
                    _break8 = Utility.ToString(dr["TARGET_BREAK_8"]);
                    _break9 = Utility.ToString(dr["TARGET_BREAK_9"]);
                    _break10 = Utility.ToString(dr["TARGET_BREAK_10"]);


                _year = Utility.ToLong(dr["TARGET_YEAR"]);
                _month = Utility.ToLong(dr["TARGET_MONTH"]);
                _period = Utility.ToLong(dr["TARGET_PERIOD"]);
                _targetTransType = Utility.ToString(dr["TARGET_TRANS_TYPE"]);

                    _amount1 = Utility.ToDouble(dr["TARGET_AMOUNT_1"]);
                    _amount1_percent = Utility.ToDouble(dr["TARGET_AMOUNT_1_PERCENT"]);
                    _amount2 = Utility.ToDouble(dr["TARGET_AMOUNT_2"]);
                    _amount2_percent = Utility.ToDouble(dr["TARGET_AMOUNT_2_PERCENT"]);
                    _amount3 = Utility.ToDouble(dr["TARGET_AMOUNT_3"]);
                    _amount3_percent = Utility.ToDouble(dr["TARGET_AMOUNT_3_PERCENT"]);
                    _amount4 = Utility.ToDouble(dr["TARGET_AMOUNT_4"]);
                    _amount4_percent = Utility.ToDouble(dr["TARGET_AMOUNT_4_PERCENT"]);
                    _amount5 = Utility.ToDouble(dr["TARGET_AMOUNT_5"]);
                    _amount5_percent = Utility.ToDouble(dr["TARGET_AMOUNT_5_PERCENT"]);
                    _break1_desc = Utility.ToString(dr["TARGET_BREAK_1_DESC"]);
                    _break2_desc = Utility.ToString(dr["TARGET_BREAK_2_DESC"]);
                    _break3_desc = Utility.ToString(dr["TARGET_BREAK_3_DESC"]);
                    _break4_desc = Utility.ToString(dr["TARGET_BREAK_4_DESC"]);
                    _break5_desc = Utility.ToString(dr["TARGET_BREAK_5_DESC"]);
                    _break6_desc = Utility.ToString(dr["TARGET_BREAK_6_DESC"]);
                    _break7_desc = Utility.ToString(dr["TARGET_BREAK_7_DESC"]);
                    _break8_desc = Utility.ToString(dr["TARGET_BREAK_8_DESC"]);
                    _break9_desc = Utility.ToString(dr["TARGET_BREAK_9_DESC"]);
                    _break10_desc = Utility.ToString(dr["TARGET_BREAK_10_DESC"]);
                    _uploadPath = Utility.ToString(dr["TARGET_UPLOAD_PATH"]);
                    _uploadType = Utility.ToString(dr["TARGET_UPLOAD_TYPE"]);
                } 
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[43];

                paramList[0] = new SqlParameter("@P_TARGET_COMPANY", "1");
                paramList[1] = new SqlParameter("@P_TARGET_TYPE", _targetType );
                paramList[2] = new SqlParameter("@P_TARGET_BREAK_1", _break1);
                paramList[3] = new SqlParameter("@P_TARGET_BREAK_2", _break2);
                paramList[4] = new SqlParameter("@P_TARGET_BREAK_3", _break3);
                paramList[5] = new SqlParameter("@P_TARGET_BREAK_4", _break4);
                paramList[6] = new SqlParameter("@P_TARGET_BREAK_5", _break5);
                paramList[7] = new SqlParameter("@P_TARGET_BREAK_6", _break6);
                paramList[8] = new SqlParameter("@P_TARGET_BREAK_7", _break7);
                paramList[9] = new SqlParameter("@P_TARGET_BREAK_8", _break8);
                paramList[10] = new SqlParameter("@P_TARGET_BREAK_9", _break9);
                paramList[11] = new SqlParameter("@P_TARGET_BREAK_10", _break10);
                paramList[12] = new SqlParameter("@P_TARGET_YEAR", _year );
                paramList[13] = new SqlParameter("@P_TARGET_MONTH", _month );
                paramList[14] = new SqlParameter("@P_TARGET_PERIOD", _period);
                paramList[15] = new SqlParameter("@P_TARGET_TRANS_TYPE", _targetTransType );
                paramList[16] = new SqlParameter("@P_TARGET_AMOUNT_1", _amount1 );
                paramList[17] = new SqlParameter("@P_TARGET_AMOUNT_1_PERCENT", _amount1_percent );
                paramList[18] = new SqlParameter("@P_TARGET_AMOUNT_2", _amount2);
                paramList[19] = new SqlParameter("@P_TARGET_AMOUNT_2_PERCENT", _amount2_percent);
                paramList[20] = new SqlParameter("@P_TARGET_AMOUNT_3", _amount3);

                paramList[21] = new SqlParameter("@P_TARGET_AMOUNT_3_PERCENT", _amount3_percent);
                paramList[22] = new SqlParameter("@P_TARGET_AMOUNT_4", _amount4);
                paramList[23] = new SqlParameter("@P_TARGET_AMOUNT_4_PERCENT", _amount4_percent);
                paramList[24] = new SqlParameter("@P_TARGET_AMOUNT_5", _amount5);
                paramList[25] = new SqlParameter("@P_TARGET_AMOUNT_5_PERCENT", _amount5_percent);
                paramList[26] = new SqlParameter("@P_SETUP_CATEGORY", _category );

                paramList[27] = new SqlParameter("@P_SETUP_CREATED_BY", _createdBy);
                paramList[28] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[28].Direction = ParameterDirection.Output;
                paramList[29] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[29].Direction = ParameterDirection.Output;
                paramList[30] = new SqlParameter("@P_TARGET_REFERENCE", _id);
                paramList[31] = new SqlParameter("@P_TARGET_BREAK_1_DESC", _break1_desc);
                paramList[32] = new SqlParameter("@P_TARGET_BREAK_2_DESC", _break2_desc);
                paramList[33] = new SqlParameter("@P_TARGET_BREAK_3_DESC", _break3_desc);
                paramList[34] = new SqlParameter("@P_TARGET_BREAK_4_DESC", _break4_desc);
                paramList[35] = new SqlParameter("@P_TARGET_BREAK_5_DESC", _break5_desc);
                paramList[36] = new SqlParameter("@P_TARGET_BREAK_6_DESC", _break6_desc);
                paramList[37] = new SqlParameter("@P_TARGET_BREAK_7_DESC", _break7_desc);
                paramList[38] = new SqlParameter("@P_TARGET_BREAK_8_DESC", _break8_desc);
                paramList[39] = new SqlParameter("@P_TARGET_BREAK_9_DESC", _break9_desc);
                paramList[40] = new SqlParameter("@P_TARGET_BREAK_10_DESC", _break10_desc);
                if(!string.IsNullOrEmpty(_uploadPath)) paramList[41] = new SqlParameter("@P_TARGET_UPLOAD_PATH", _uploadPath);
                if(!string.IsNullOrEmpty(_uploadType)) paramList[42] = new SqlParameter("@P_TARGET_UPLOAD_TYPE", _uploadType);

                DBGateway.ExecuteNonQuery("CT_P_TRAVEL_SALES_TARGET_ADD_UPDATE", paramList);
                string messageType = Utility.ToString(paramList[28].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[29].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_TARGET_REFERENCE", _id );
                
                DBGateway.ExecuteNonQuery("CT_P_TRAVEL_SALES_TARGET_DELETE", paramList);
                
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Static Methods
        public static DataTable GetList(string category, long year)
        {
            DataTable dtList = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0]= new SqlParameter("@P_SETUP_CATEGORY",category);
                paramList[1] = new SqlParameter("@P_TARGET_YEAR", year);
                dtList= DBGateway.ExecuteQuery("CT_P_TRAVEL_SALES_TARGET_GETLIST", paramList).Tables[0];
                string messageType = Utility.ToString(paramList[28].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[29].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
            return dtList;
        }

        public static void UpdateTargetSetupParams(long refNo,string path,string type,long createdBy)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_TARGET_REFERENCE", refNo);
                paramList[1] = new SqlParameter("@P_TARGET_UPLOAD_PATH", path);
                paramList[2] = new SqlParameter("@P_TARGET_UPLOAD_TYPE", type);
                paramList[3] = new SqlParameter("@P_setup_created_by", createdBy);
                paramList[4] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[4].Direction = ParameterDirection.Output;
                paramList[5] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[5].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("CT_P_TRAVEL_SALES_TARGET_UPDATE", paramList);
                string messageType = Utility.ToString(paramList[4].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[5].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
              

        #endregion
    }

}
