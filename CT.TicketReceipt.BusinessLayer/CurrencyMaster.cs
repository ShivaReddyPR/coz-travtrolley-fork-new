using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class CurrencyMaster
    {
        public const long NEW_RECORD = -1;
        #region Member Variables

        private long _transactionId;
        private string _code;
        private string _name;
        private long _countryId;
        private string _status;
        private long _createdBy;

        #endregion

        #region Properties

        public long Id
        {
            get { return _transactionId; }
            set { _transactionId = value; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public long CountryId
        {
            get { return _countryId; }
            set { _countryId = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }        
        }
#endregion

        public CurrencyMaster()
        {
            _transactionId = NEW_RECORD;
        }

        public CurrencyMaster(long id)
        {
            _transactionId = id;
            getDetails(id);
        }
        #region 
        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }

        //private void getDetails(long id)
        //{

        //    DataSet ds = GetData(id);
        //    if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
        //    {
        //        UpdateBusinessData(ds.Tables[0].Rows[0]);
        //    }

        //}

        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_CURRENCY_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("VISA_P_CURRENCY_GETDATA", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _transactionId = Utility.ToLong(dr["CURRENCY_ID"]);
                _code = Utility.ToString(dr["CURRENCY_CODE"]);
                _name = Utility.ToString(dr["CURRENCY_NAME"]);
                //_countryId = Utility.ToLong(dr["CURRENCY_COUNTRY"]);
                _status = Utility.ToString(dr["CURRENCY_STATUS"]);                                
                _createdBy = Utility.ToLong(dr["CURRENCY_CREATED_BY"]);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[8];

                paramList[0] = new SqlParameter("@P_CURRENCY_ID", _transactionId);
                paramList[1] = new SqlParameter("@P_CURRENCY_CODE", _code);
                paramList[2] = new SqlParameter("@P_CURRENCY_NAME", _name);
                if(_countryId >0)paramList[3] = new SqlParameter("@P_CURRENCY_COUNTRY", _countryId);
                paramList[4] = new SqlParameter("@P_CURRENCY_STATUS", _status);
                paramList[5] = new SqlParameter("@P_CURRENCY_CREATED_BY", _createdBy);
                paramList[6] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[6].Direction = ParameterDirection.Output;
                paramList[7] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[7].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("VISA_p_currency_add_update", paramList);
                string messageType = Utility.ToString(paramList[6].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[7].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_CURRENCY_ID", _transactionId);

                DBGateway.ExecuteNonQuery("VISA_P_CURRENCY_DELETE", paramList);
               // string message = Utility.ToString(paramList[1].Value);
               // if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        
        #endregion

        #region Static Methods


        public static DataTable GetList(ListStatus listStatus, RecordStatus recStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", listStatus);
                if(recStatus != RecordStatus.All) paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recStatus));
                paramList[2] = new SqlParameter("@P_BASE_CURRENCY", DBNull.Value);
                return DBGateway.ExecuteQuery("VISA_P_CURRENCY_GETLIST", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetList(string baseCurrency, ListStatus listStatus, RecordStatus recStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", listStatus);
                if (recStatus != RecordStatus.All) paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recStatus));
                if (baseCurrency == string.Empty)
                {
                    paramList[2] = new SqlParameter("@P_BASE_CURRENCY", DBNull.Value);
                }
                else
                {
                    paramList[2] = new SqlParameter("@P_BASE_CURRENCY", baseCurrency);
                }
                return DBGateway.ExecuteQuery("VISA_P_CURRENCY_GETLIST", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        #endregion

    }
    
}

