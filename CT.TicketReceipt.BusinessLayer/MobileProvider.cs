using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class MobileProvider
    {
        private const long NEW_RECORD = -1;
        #region Member Variables        
        private long _id;
        private string _providerCode;
        private string _provider;        
        private long _maxLength;
        private long _location;
        private string _status;
        private long _createdBy;        
        #endregion

        #region Properties
        public long ID
        {
            get { return _id; }            
        }
        public string ProviderCode
        {
            get { return _providerCode; }
            set { _providerCode = value; }
        }
        public string Provider
        {
            get { return _provider; }
            set { _provider = value; }
        }
     
        public long Maxlength 
        {
            get { return _maxLength; }
            set { _maxLength = value; }
        }
        public long Location
        {
            get { return _location ; }
            set { _location  = value; }
        }
       
         public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }       

        #endregion

        #region Constructors
        public MobileProvider()
        {
            _id = NEW_RECORD;
        
        }
        public MobileProvider(long id)
        {
            
            
            _id = id;
            GetDetails(id);
        }
        #endregion

        #region Methods
        private void GetDetails(long id)
        {
            
            DataSet ds = GetData(id);
           // UpdateBusinessData(ds);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }
            
        }
        private DataSet GetData(long id)
        {
            
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_MOB_PROV_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_mobile_providers_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {
            
            try
            {
                _id = Utility.ToLong(dr["mob_prov_id"]);
                _providerCode = Utility.ToString(dr["mob_prov_code"]);                
                _location = Utility.ToLong(dr["mob_prov_location_id"]);
                _maxLength = Utility.ToLong(dr["mob_prov_maxlength"]);                
                _provider = Utility.ToString(dr["mob_prov_description"]);                
                _status = Utility.ToString(dr["mob_prov_status"]);
                _createdBy = Utility.ToLong(dr["mob_prov_created_by"]);
                

            }
            catch
            {
                throw;
            }
        }

     
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[9];
                paramList[0] = new SqlParameter("@P_MOB_PROV_ID", _id);
                paramList[1] = new SqlParameter("@P_mob_prov_code", _providerCode);
                paramList[2] = new SqlParameter("@P_mob_prov_location_id", Location);
                paramList[3] = new SqlParameter("@P_mob_prov_maxlength", _maxLength);
                paramList[4] = new SqlParameter("@P_mob_prov_description", _provider);
                paramList[5] = new SqlParameter("@P_mob_prov_status", _status);
                paramList[6] = new SqlParameter("@P_mob_prov_created_by", _createdBy);
                paramList[7] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[7].Direction = ParameterDirection.Output;
                paramList[8] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[8].Direction = ParameterDirection.Output;


                DBGateway.ExecuteNonQuery("ct_p_mobile_providers_add_update", paramList);
                string messageType = Utility.ToString(paramList[7].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[8].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
               
                throw;
            }
         
        }

      
        public void Delete()
        {            
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LOCATION_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output; 
                
                DBGateway.ExecuteNonQuery("ct_p_location_add_update", paramList);                
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Static Methods
        public static DataTable GetList(long locationId,ListStatus status, RecordStatus recordStatus)
        {
            
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];                
                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", (int)recordStatus);
                if (locationId > 0) paramList[2] = new SqlParameter("@P_LOCATION_ID", locationId);
                return DBGateway.ExecuteQuery("ct_p_mobile_providers_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}

