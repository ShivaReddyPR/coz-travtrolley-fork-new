using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class ExpenseMaster
    {
        private const long NEW_RECORD = -1;

        private long _transactionId;
        private string _code;
        private string _description;
        private string _remarks;
        private string _status;
        private long _createdBy;


        #region Properties
        public long TransactionId
        {
            get { return _transactionId; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string Remarks
        {
            get { return _remarks; }
            set { _remarks = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        #endregion

        #region Constructors
        public ExpenseMaster()
        {
            _transactionId = NEW_RECORD;
        }
        public ExpenseMaster(long id)
        {


            _transactionId = id;
            getDetails(id);
        }
        #endregion

        #region Static Methods
        public static DataTable GetList(ListStatus status, RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];

                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus != RecordStatus.All) paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("OBVISA_P_EXPENSE_GETLIST", paramList).Tables[0];
            }

            catch
            {
                throw;
            }
        }
        #endregion

        #region Methods
        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_EXP_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("OBVISA_p_expense_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _transactionId = Utility.ToLong(dr["EXP_ID"]);
                _code = Utility.ToString(dr["EXP_CODE"]);
                _description = Utility.ToString(dr["EXP_DESCRIPTION"]);
                _remarks = Utility.ToString(dr["EXP_REMARKS"]);
                _status = Utility.ToString(dr["EXP_STATUS"]);
                _createdBy = Utility.ToLong(dr["EXP_CREATED_BY"]);

            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[8];

                paramList[0] = new SqlParameter("@P_EXP_ID", _transactionId);
                paramList[1] = new SqlParameter("@P_EXP_CODE", _code);
                paramList[2] = new SqlParameter("@P_EXP_DESCRIPTION", _description);
                paramList[3] = new SqlParameter("@P_EXP_REMARKS", _remarks);
                paramList[4] = new SqlParameter("@P_EXP_STATUS", _status);
                paramList[5] = new SqlParameter("@P_EXP_CREATED_BY", _createdBy);
                paramList[6] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[6].Direction = ParameterDirection.Output;
                paramList[7] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[7].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("OBVISA_p_expense_add_update", paramList);
                string messageType = Utility.ToString(paramList[6].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[7].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_EXP_ID", _transactionId);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("OBVISA_p_expense_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
     