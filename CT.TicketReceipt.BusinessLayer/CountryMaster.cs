using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{

    public enum VisaService
    {
        All=0,
        InBound=73,
        OutBound=79,
    }
    public class CountryMaster
    {
        private const long NEW_RECORD = -1;
        
    
        #region Member Variables
        private long _id;
        private string _code;
        private string _name;
        private string _currency;
        private string _visaType;

        private VisaService  _visaServiceType;

        private string _visaFormName;
        private string _visaFormType;
        private string  _visaFormPath;
        private string _status;
        private long _createdBy;
        private long _nextID;

        #endregion

        #region Properties

        public long ID
        {
            get { return _id; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string Currency
        {
            get { return _currency ; }
            set { _currency = value; }
        }
        public string VisaType
        {
            get { return _visaType; }
            set { _visaType = value; }
        }
        public  VisaService VisaServiceType
        {
            get { return _visaServiceType; }
            set { _visaServiceType = value; }
        }

        public string VisaFormName
        {
            get { return _visaFormName ; }
            set { _visaFormName = value; }
        }
        public string VisaFormType
        {
            get { return _visaFormType; }
            set { _visaFormType = value; }
        }
        public string VisaFormPath
        {
            get { return _visaFormPath; }
            set { _visaFormPath = value; }
        }
       
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        } 

        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public long NextID
        {
            get { return _nextID; }
            set { _nextID = value; }
        } 
         #endregion

        #region Constructors
        public CountryMaster()
        {
            _id = NEW_RECORD;
        }
        public CountryMaster(long id)
        {
            _id = id;
            getDetails(_id);
        
        }


        #endregion

        #region Methods
        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_COUNTRY_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_country_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                if (_id <= 0)
                {
                    _nextID = Utility.ToLong(dr["nextID"]);
                }
                else
                {
                    _id = Utility.ToLong(dr["COUNTRY_ID"]);
                    _code = Utility.ToString(dr["COUNTRY_CODE"]);
                    _name = Utility.ToString(dr["COUNTRY_NAME"]);
                    _currency = Utility.ToString(dr["COUNTRY_CURRENCY"]);
                    _visaType = Utility.ToString(dr["COUNTRY_VISA_TYPE"]);
                    // _visaServiceType  = Utility.ToString(dr["COUNTRY_VISA_SERVICE"]);
                    if (Utility.ToString(dr["COUNTRY_VISA_SERVICE"]) == string.Empty)
                    {
                        _visaServiceType = VisaService.All;
                    }
                    else
                    {
                        _visaServiceType = (VisaService)(Convert.ToInt32(Convert.ToChar(dr["COUNTRY_VISA_SERVICE"])));
                    }
                    _visaFormType = Utility.ToString(dr["COUNTRY_APP_FORM_TYPE"]);
                    _visaFormName = Utility.ToString(dr["COUNTRY_APP_FORM_NAME"]);
                    _visaFormPath = Utility.ToString(dr["COUNTRY_APP_FORM_PATH"]);
                    _status = Utility.ToString(dr["COUNTRY_STATUS"]);
                    _createdBy = Utility.ToLong(dr["COUNRTY_CREATED_BY"]);
                }

            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[13];

                paramList[0] = new SqlParameter("@P_COUNTRY_ID", _id);
                paramList[1] = new SqlParameter("@P_COUNTRY_CODE", _code);
                paramList[2] = new SqlParameter("@P_COUNTRY_NAME", _name);
                paramList[3] = new SqlParameter("@P_COUNTRY_CURRENCY", _currency);
                paramList[4] = new SqlParameter("@P_COUNTRY_VISA_TYPE", _visaType );
                if (_visaServiceType != VisaService.All) paramList[5] = new SqlParameter("@P_COUNTRY_VISA_SERVICE", (char)(_visaServiceType));
                                           
                paramList[6] = new SqlParameter("@P_COUNTRY_APP_FORM_NAME", _visaFormName );
                paramList[7] = new SqlParameter("@P_COUNTRY_APP_FORM_TYPE", _visaFormType);
                paramList[8] = new SqlParameter("@P_COUNTRY_APP_FORM_PATH", _visaFormPath);
                paramList[9] = new SqlParameter("@P_COUNTRY_STATUS", _status);
                paramList[10] = new SqlParameter("@P_COUNTRY_CREATED_BY", _createdBy);
                paramList[11] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[11].Direction = ParameterDirection.Output;
                paramList[12] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[12].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_COUNTRY_add_update", paramList);
                string messageType = Utility.ToString(paramList[11].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[12].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_COUNTRY_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_COUNTRY_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Static Methods
        public static DataTable GetList(ListStatus status, RecordStatus recordStatus)
        {

            try
            {
                return GetList(VisaService.All, status, recordStatus);
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetList(VisaService vService,ListStatus status,RecordStatus recordStatus)
        {

            try
            {
             
                SqlParameter[] paramList = new SqlParameter[3];

                if (vService != VisaService.All) paramList[0] = new SqlParameter("@P_VISA_SERVICE", Utility.ToString((char)vService));
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", recordStatus);

                return DBGateway.ExecuteQuery("ct_p_country_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        # endregion
       
    }
   
}
