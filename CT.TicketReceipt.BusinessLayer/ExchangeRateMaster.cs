using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class ExchangeRateMaster
    {
        public const long NEW_RECORD = -1;
        #region Member Variables

        private long _transactionId;
        private string _currencyCode;
        private decimal _rate;
        private string _remarks;
        private string _status;
        private long _createdBy;
        private string _baseCurrencyCode;
        #endregion

        #region Properties

        public long Id
        {
            get { return _transactionId; }
            set { _transactionId = value; }
        }
        public string CurrencyCode
        {
            get { return _currencyCode; }
            set { _currencyCode = value; }
        }

        public decimal Rate
        {
            get { return _rate; }
            set { _rate = value; }
            
        }

        public string Remarks
        {
            get { return _remarks; }
            set { _remarks = value; }
        }



        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }        
        }
        public string BaseCurrencyCode
        {
            get
            {
                return this._baseCurrencyCode;
            }
            set
            {
                this._baseCurrencyCode = value;
            }
        }
#endregion

        public ExchangeRateMaster()
        {
            _transactionId = NEW_RECORD;
        }

        public ExchangeRateMaster(long id)
        {
            _transactionId = id;
            getDetails(id);
        }
        #region 
        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }

        //private void getDetails(long id)
        //{

        //    DataSet ds = GetData(id);
        //    if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
        //    {
        //        UpdateBusinessData(ds.Tables[0].Rows[0]);
        //    }

        //}

        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_EX_RATE_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("VISA_p_exchange_rate_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _transactionId = Utility.ToLong(dr["EX_RATE_ID"]);
                _currencyCode = Utility.ToString(dr["EX_RATE_CURRENCY_CODE"]);
                _rate = Utility.ToDecimal(dr["EX_RATE_RATE"]);
                _remarks = Utility.ToString(dr["EX_RATE_REMARKS"]);
                _status = Utility.ToString(dr["EX_RATE_STATUS"]);
                _createdBy = Utility.ToLong(dr["EX_RATE_CREATED_BY"]);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[9];

                paramList[0] = new SqlParameter("@P_EX_RATE_ID", _transactionId);
                paramList[1] = new SqlParameter("@P_EX_RATE_CURRENCY_CODE", _currencyCode);
                paramList[2] = new SqlParameter("@P_EX_RATE_RATE", _rate);
                paramList[3] = new SqlParameter("@P_EX_RATE_REMARKS", _remarks);
                paramList[4] = new SqlParameter("@P_EX_RATE_STATUS", _status);
                paramList[5] = new SqlParameter("@P_EX_RATE_CREATED_BY", _createdBy);
                paramList[6] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[6].Direction = ParameterDirection.Output;
                paramList[7] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[7].Direction = ParameterDirection.Output;
                paramList[8] = new SqlParameter("@P_EX_RATE_BASE_CURRENCY", this._baseCurrencyCode);
                DBGateway.ExecuteNonQuery("VISA_p_exchange_rate_add_update", paramList);
                string messageType = Utility.ToString(paramList[6].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[7].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_CURRENCY_ID", _transactionId);

                DBGateway.ExecuteNonQuery("VISA_P_CURRENCY_DELETE", paramList);
               // string message = Utility.ToString(paramList[1].Value);
               // if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        
        #endregion

        #region Static Methods


        public static DataTable GetList(ListStatus listStatus, RecordStatus recStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", listStatus);
                if(recStatus != RecordStatus.All) paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recStatus));
                return DBGateway.ExecuteQuery("VISA_p_exchange_rate_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetListByCurrnctCode(string currencyCode)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_CURRENCY_CODE", currencyCode);
                return DBGateway.ExecuteQuery("VISA_P_EXCHANGE_RATE_GET_BY_CURRNCY_CODE", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        #endregion

    }
    
}
