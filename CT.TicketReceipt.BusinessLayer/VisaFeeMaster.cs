using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class VisaFeeMaster
    {
        private const long NEW_RECORD = -1;
        #region Member Variables
        private long _id;
        private string _code;
        private int _salesTypeId ;
        private int _countryId ;
        private int _visaTypeId ;
        private string _appliedThru ;
        private decimal _visaFee;
        private decimal _visaInsuranceFee;
        private decimal _visaIncome;
        private decimal _visaTotalCost;
        private decimal _airportDeposit;        
        private decimal _airportDepositIncome;
        private decimal  _meetAssistDeposit;
        private decimal _meetAssistIncome;
        private decimal  _quickCheckOutDeposit;
        private decimal _quickCheckOutIncome;
        private decimal _downloadFee;
        private decimal _visaUrgentFee;
        private decimal _processFee;
        private decimal _securitytDeposit;
        private decimal _agentFee;
        private int _agentId;
        private long _locationId;
        private string _status;
        private long _createdBy;

        #endregion

        #region Properties
        public long ID
        {
            get { return _id; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public int SalesTypeId
        {
            get { return _salesTypeId; }
            set { _salesTypeId = value; }
        }
        public int CountryId
        {
            get { return _countryId; }
            set { _countryId= value; }
        }
        public int VisaTypeId
        {
            get { return _visaTypeId; }
            set { _visaTypeId= value; }
        }
        public string AppliedThru
        {
            get { return _appliedThru; }
            set { _appliedThru= value; }
        }
        public decimal VisaFee
        {
            get { return _visaFee; }
            set { _visaFee= value; }
        }

        public decimal VisaInsuranceFee
        {
            get { return _visaInsuranceFee; }
            set { _visaInsuranceFee= value; }
        }
        public decimal VisaIncome
        {
            get { return _visaIncome; }
            set { _visaIncome= value; }
        }
        public decimal VisaTotalCost
        {
            get { return _visaTotalCost; }
            set { _visaTotalCost= value; }
        }
        public decimal AirportDeposit
        {
            get { return _airportDeposit; }
            set { _airportDeposit= value; }
        }

        public decimal AirportDepositIncome
        {
            get { return _airportDepositIncome; }
            set { _airportDepositIncome = value; }
        }

        public decimal  MeetAssistDeposit
        {
            get { return _meetAssistDeposit; }
            set { _meetAssistDeposit = value; }
        }
        public decimal MeetAssistIncome
        {
            get { return _meetAssistIncome; }
            set { _meetAssistIncome  = value; }
        }

        public decimal  QuickCheckOutDeposit
        {
            get { return _quickCheckOutDeposit ; }
            set { _quickCheckOutDeposit = value; }
        }
        public decimal QuickCheckOutIncome
        {
            get { return _quickCheckOutIncome ; }
            set { _quickCheckOutIncome = value; }
        }

        public decimal DownloadFee
        {
            get { return _downloadFee ; }
            set { _downloadFee = value; }
        }

        public decimal VisaUrgentFee
        {
            get { return _visaUrgentFee ; }
            set { _visaUrgentFee = value; }
        }

        public decimal ProcessFee
        {
            get { return _processFee; }
            set { _processFee = value; }
        }


        public decimal SecuritytDeposit
        {
            get { return _securitytDeposit; }
            set { _securitytDeposit= value; }
        }

        public decimal AgentFee
        {
            get { return _agentFee; }
            set { _agentFee = value; }

        } 
        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public long LocationId
        {
            get { return _locationId; }
            set { _locationId = value; }
        }
        #endregion

        #region Constructors
        public VisaFeeMaster()
        {
            _id = NEW_RECORD;
        }
        public VisaFeeMaster(long id)
        {


            _id = id;
            getDetails(id);
        }
        #endregion
        #region Methods
        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_VISA_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("visa_p_visa_fee_getData", paramList); // TObe written proc
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _id = Utility.ToLong(dr["visa_id"]);
               _locationId = Utility.ToLong(dr["visa_location_id"]);
                _agentId = Utility.ToInteger(dr["visa_agent_id"]);
                _code = Utility.ToString(dr["visa_code"]);
                _salesTypeId = Utility.ToInteger(dr["visa_sales_type_id"]);
                _countryId = Utility.ToInteger(dr["visa_country_id"]);
                _visaTypeId = Utility.ToInteger(dr["visa_visa_type_id"]);
                _appliedThru = Utility.ToString(dr["visa_applied_thru"]);
                _visaFee = Utility.ToDecimal(dr["visa_fee"]);
                _visaInsuranceFee = Utility.ToDecimal(dr["visa_insurance_fee"]);
                _visaIncome = Utility.ToDecimal(dr["visa_income"]);
                _visaTotalCost = Utility.ToDecimal(dr["visa_total_cost"]);
                _airportDeposit = Utility.ToDecimal(dr["visa_airport_deposit"]);
                _airportDepositIncome = Utility.ToDecimal(dr["visa_airport_income"]);
                _meetAssistDeposit = Utility.ToDecimal(dr["visa_meet_assist_deposit"]);
                _meetAssistIncome = Utility.ToDecimal(dr["visa_meet_assist_income"]);
                _quickCheckOutDeposit = Utility.ToDecimal(dr["visa_quick_checkout_deposit"]);
                _quickCheckOutIncome = Utility.ToDecimal(dr["visa_quick_checkout_income"]);
                _securitytDeposit = Utility.ToDecimal(dr["visa_security_deposit"]);
                _downloadFee = Utility.ToDecimal(dr["visa_download_fee"]);
                _visaUrgentFee = Utility.ToDecimal(dr["visa_urgent_fee"]);
                _processFee = Utility.ToDecimal(dr["visa_process_fee"]);
                _agentFee = Utility.ToDecimal(dr["visa_agent_fee"]);
                
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[27];

                paramList[0] = new SqlParameter("@P_VISA_ID", _id);
                //paramList[1] = new SqlParameter("@P_VISA_CODE", _code);
                paramList[1] = new SqlParameter("@P_VISA_CODE", _code);
                paramList[2] = new SqlParameter("@P_VISA_SALES_TYPE_ID", _salesTypeId);
                paramList[3] = new SqlParameter("@P_VISA_COUNTRY_ID", _countryId);
                paramList[4] = new SqlParameter("@P_VISA_VISA_TYPE_ID", _visaTypeId);
                paramList[5] = new SqlParameter("@P_VISA_APPLIED_THRU", _appliedThru);
                paramList[6] = new SqlParameter("@P_VISA_FEE", _visaFee);
                paramList[7] = new SqlParameter("@P_VISA_INSURANCE_FEE", _visaInsuranceFee);
                paramList[8] = new SqlParameter("@P_VISA_INCOME", _visaIncome);
                paramList[9] = new SqlParameter("@P_VISA_TOTAL_COST", _visaTotalCost);
                paramList[10] = new SqlParameter("@P_VISA_AIRPORT_DEPOSIT", _airportDeposit);
                paramList[11] = new SqlParameter("@P_VISA_SECURITY_DEPOSIT", _securitytDeposit);
                paramList[12] = new SqlParameter("@P_VISA_AGENT_ID", _agentId );
                paramList[13] = new SqlParameter("@P_VISA_STATUS", _status);
                paramList[14] = new SqlParameter("@P_VISA_CREATED_BY", _createdBy);
                paramList[15] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[15].Direction = ParameterDirection.Output;
                paramList[16] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[16].Direction = ParameterDirection.Output;
                paramList[17] = new SqlParameter("@P_VISA_AIRPORT_INCOME", _airportDepositIncome );
                paramList[18] = new SqlParameter("@P_VISA_MEET_ASSIST_DEPOSIT", _meetAssistDeposit);
                paramList[19] = new SqlParameter("@P_VISA_MEET_ASSIST_INCOME", _meetAssistIncome );
                paramList[20] = new SqlParameter("@P_VISA_QUICK_CHECKOUT_DEPOSIT", _quickCheckOutDeposit);
                paramList[21] = new SqlParameter("@P_VISA_QUICK_CHECKOUT_INCOME", _quickCheckOutIncome);
                paramList[22] = new SqlParameter("@P_VISA_DOWNLOAD_FEE", _downloadFee);
                paramList[23] = new SqlParameter("@P_VISA_URGENT_FEE", _visaUrgentFee);
                paramList[24] = new SqlParameter("@P_VISA_PROCESS_FEE", _processFee);                
                paramList[25] = new SqlParameter("@P_VISA_AGENT_FEE", _agentFee);
                paramList[26] = new SqlParameter("@P_VISA_LOCATION_ID", _locationId);
                DBGateway.ExecuteNonQuery("visa_p_visa_fee_add_update", paramList);
                string messageType = Utility.ToString(paramList[15].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[16].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()//To DO
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LOCATION_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_location_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Static Methods
        public static DataTable GetList(long locationId, int agentId, ListStatus listStatus, RecordStatus recStatus,string memberType )
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                if (locationId > 0) paramList[0] = new SqlParameter("@P_LOCATION_ID", locationId);
                if (agentId > 0) paramList[1] = new SqlParameter("@P_VISA_AGENT_ID", agentId);
                paramList[2] = new SqlParameter("@P_LIST_STATUS",(int) listStatus);
                if (recStatus != RecordStatus.All) paramList[3] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recStatus));
                if(!string.IsNullOrEmpty(memberType)) paramList[4]= new SqlParameter("@P_USER_TYPE",memberType);
                return DBGateway.ExecuteQuery("visa_p_visa_fee_getlist", paramList).Tables[0];

            }
            catch
            {
                throw;
            }
        }
        public static VisaFeeDetails GetVisaFeeDetails(long visaFeeId)
        {

            try
            {
                VisaFeeDetails feeDetails=null;
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@P_VISA_ID", visaFeeId);
                paramList[1] = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar,10);
                paramList[1].Direction = ParameterDirection.Output;
                paramList[2] = new SqlParameter("@P_MSG_text",  SqlDbType.NVarChar,100);
                paramList[2].Direction = ParameterDirection.Output;
                DataTable dt=DBGateway.ExecuteQuery("visa_p_visa_fee_details", paramList).Tables[0];
                if (dt != null)
                {
                    feeDetails= new VisaFeeDetails(visaFeeId);
                    DataRow dr = dt.Rows[0];
                    feeDetails.Code = Utility.ToString(dr["VISA_CODE"]);
                    feeDetails.AppliedThru= Utility.ToString(dr["VISA_APPLIED_THRU"]);
                    feeDetails.VisaFee = Utility.ToDecimal(dr["VISA_FEE"]);
                    feeDetails.VisaInsuranceFee= Utility.ToDecimal(dr["VISA_INSURANCE_FEE"]);
                    feeDetails.VisaIncome= Utility.ToDecimal(dr["VISA_INCOME"]);
                    feeDetails.VisaProcessFee = Utility.ToDecimal(dr["VISA_PROCESS_FEE"]);
                    feeDetails.VisaTotalCost= Utility.ToDecimal(dr["VISA_TOTAL_COST"]);

                    feeDetails.AirportDeposit = Utility.ToDecimal(dr["VISA_AIRPORT_DEPOSIT"]);
                    feeDetails.AirportDepositIncome = Utility.ToDecimal(dr["VISA_AIRPORT_INCOME"]);
                    feeDetails.MeetAssistDeposit = Utility.ToDecimal(dr["VISA_MEET_ASSIST_DEPOSIT"]);
                    feeDetails.MeetAssistIncome = Utility.ToDecimal(dr["VISA_MEET_ASSIST_INCOME"]);
                    feeDetails.QuickCheckOutDeposit = Utility.ToDecimal(dr["VISA_QUICK_CHECKOUT_DEPOSIT"]);
                    feeDetails.QuickCheckOutIncome = Utility.ToDecimal(dr["VISA_QUICK_CHECKOUT_INCOME"]);                    

                    feeDetails.SecuritytDeposit= Utility.ToDecimal(dr["VISA_SECURITY_DEPOSIT"]);
                    feeDetails.DownLoadFee = Utility.ToDecimal(dr["VISA_DOWNLOAD_FEE"]);
                    feeDetails.VisaUrgentFee = Utility.ToDecimal(dr["VISA_URGENT_FEE"]);
                    feeDetails.VisaAgentFee = Utility.ToDecimal(dr["VISA_AGENT_FEE"]);
                    feeDetails.SalesTypeActivity = Utility.ToString(dr["SALES_TYPE_ACTIVITY"]);
                    feeDetails.SalesTypeId = Utility.ToInteger(dr["VISA_SALES_TYPE_ID"]);
                    feeDetails.VisaTypeId = Utility.ToInteger(dr["VISA_VISA_TYPE_ID"]);
                    feeDetails.SalesTypeInfantFee= Utility.ToDecimal(dr["SALES_TYPE_INFANT"]);
                    feeDetails.VisaTypeActivity = Utility.ToString(dr["VISA_TYPE_ACTIVITIY"]);
                }
                return feeDetails;
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
    public class VisaFeeDetails
    {
        #region Member Variables
        private long _id;
        private string _code;
        private int _salesTypeId;
        //private int _salesTypeId;
        //private int _countryId;
        private int _visaTypeId;
        private string _appliedThru;
        private decimal _visaFee;
        private decimal _visaInsuranceFee;
        private decimal _visaIncome;
        private decimal _visaProcessFee;
        private decimal _visaTotalCost;

        private decimal _airportDeposit;
        private decimal _airportDepositIncome;
        private decimal _meetAssistDeposit;
        private decimal _meetAssistIncome;
        private decimal _quickCheckOutDeposit;
        private decimal _quickCheckOutIncome;
        private decimal _visaAgentFee;
        private decimal _securitytDeposit;
        private decimal _downloadFee;
        private decimal _visaUrgentFee;
        private decimal _salesTypeInfantFee;
        private string _salesTypeActivity;
        private string _visaTypeActivity;
        //private string _salesTypeActivity;
        //private int _companyId;
        //private string _status;
        //private long _createdBy;

        #endregion
        #region Properties
        public long ID
        {
            get { return _id; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        //public int SalesTypeId
        //{
        //    get { return _salesTypeId; }
        //    set { _salesTypeId = value; }
        //}
        //public int CountryId
        //{
        //    get { return _countryId; }
        //    set { _countryId = value; }
        //}
        //public int VisaTypeId
        //{
        //    get { return _visaTypeId; }
        //    set { _visaTypeId = value; }
        //}
        public string AppliedThru
        {
            get { return _appliedThru; }
            set { _appliedThru = value; }
        }
        public decimal VisaFee
        {
            get { return _visaFee; }
            set { _visaFee = value; }
        }

        public decimal VisaInsuranceFee
        {
            get { return _visaInsuranceFee; }
            set { _visaInsuranceFee = value; }
        }
        public decimal VisaIncome
        {
            get { return _visaIncome; }
            set { _visaIncome = value; }
        }
        public decimal VisaProcessFee
        {
            get { return _visaProcessFee; }
            set { _visaProcessFee = value; }
        }
        public decimal VisaTotalCost
        {
            get { return _visaTotalCost; }
            set { _visaTotalCost = value; }
        }
        public decimal AirportDeposit
        {
            get { return _airportDeposit; }
            set { _airportDeposit = value; }
        }


        public decimal AirportDepositIncome
        {
            get { return _airportDepositIncome; }
            set { _airportDepositIncome = value; }
        }

        public decimal MeetAssistDeposit
        {
            get { return _meetAssistDeposit; }
            set { _meetAssistDeposit = value; }
        }
        public decimal MeetAssistIncome
        {
            get { return _meetAssistIncome; }
            set { _meetAssistIncome = value; }
        }

        public decimal QuickCheckOutDeposit
        {
            get { return _quickCheckOutDeposit; }
            set { _quickCheckOutDeposit = value; }
        }
        public decimal QuickCheckOutIncome
        {
            get { return _quickCheckOutIncome; }
            set { _quickCheckOutIncome = value; }
        }

        public decimal SecuritytDeposit
        {
            get { return _securitytDeposit; }
            set { _securitytDeposit = value; }
        }
        public decimal DownLoadFee
        {
            get { return _downloadFee ; }
            set { _downloadFee  = value; }
        }
        public decimal VisaUrgentFee
        {
            get { return _visaUrgentFee; }
            set { _visaUrgentFee  = value; }
        }
        public string SalesTypeActivity
        {
            get { return _salesTypeActivity; }
            set { _salesTypeActivity = value; }
        }
        public int SalesTypeId
        {
            get { return _salesTypeId; }
            set { _salesTypeId = value; }
        }
        public decimal SalesTypeInfantFee
        {
            get { return _salesTypeInfantFee; }
            set { _salesTypeInfantFee = value; }
        }
        public int VisaTypeId
        {
            get { return _visaTypeId; }
            set { _visaTypeId = value; }
        }
        public string VisaTypeActivity
        {
            get { return _visaTypeActivity; }
            set { _visaTypeActivity = value; }
        }
        public decimal VisaAgentFee
        {
            get { return _visaAgentFee; }
            set { _visaAgentFee = value; }
        }
        //public int CompanyId
        //{
        //    get { return _companyId; }
        //    set { _companyId = value; }
        //}
        //public string Status
        //{
        //    get { return _status; }
        //    set { _status = value; }
        //}
        //public long CreatedBy
        //{
        //    get { return _createdBy; }
        //    set { _createdBy = value; }
        //}

        #endregion
        public VisaFeeDetails(long id)
        {
            _id = id;
        }


    }
}
