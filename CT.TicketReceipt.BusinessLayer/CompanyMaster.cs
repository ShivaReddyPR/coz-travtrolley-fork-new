using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class CompanyMaster
    {
        private const long NEW_RECORD = -1;
        
        //private long _id;
        //private string _firstName;
        //private string _lastName;
        //private string _email;
        #region Static Methods
        public static DataTable GetList(ListStatus status,RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];

                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", recordStatus);

                return DBGateway.ExecuteQuery("ct_p_company_getlist", paramList).Tables[0];
            }

            catch
            {
                throw;
            }
        }
        # endregion
        //private string _loginName;
        //private string _password;
        //private long _locationId;
        //private MemberType _memberType;
        //private string _address;
        //private string _status;
        //private long _createdBy;


       // #region Properties
       /* public long ID
        {
            get { return _id; }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string LoginName
        {
            get { return _loginName; }
            set { _loginName = value; }
        }
        public string Password
        {            
            set { _password = value; }
        }
        public long LocationID
        {
            get { return _locationId; }
            set { _locationId = value; }
        }
        public MemberType MemeberType
        {
            get { return _memberType; }
            set { _memberType = value; }
        }
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        #endregion

        #region Constructors
        public UserMaster()
        {
            _id = NEW_RECORD;
        }
        public UserMaster(long id)
        {


            _id = id;
            getDetails(id);
        }
        #endregion
        #region Methods
        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_USER_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_user_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _id = Utility.ToLong(dr["USER_ID"]);
                _firstName = Utility.ToString(dr["USER_FIRST_NAME"]);
                _lastName = Utility.ToString(dr["USER_LAST_NAME"]);

                _email = Utility.ToString(dr["USER_EMAIL"]);
                _loginName = Utility.ToString(dr["USER_LOGIN_NAME"]);
                _locationId = Utility.ToLong(dr["USER_LOCATION_ID"]);
                string type = Utility.ToString(dr["USER_MEMBER_TYPE"]);
                if (type == MemberType.ADMIN.ToString())
                {
                    _memberType = MemberType.ADMIN;
                }
                else if (type == MemberType.CASHIER.ToString())
                {
                    _memberType = MemberType.CASHIER;
                }
                else if (type == MemberType.OPERATIONS.ToString())
                {
                    _memberType = MemberType.OPERATIONS;
                }
                
                _address = Utility.ToString(dr["USER_ADDRESS"]);
                _status = Utility.ToString(dr["USER_STATUS"]);
                _createdBy = Utility.ToLong(dr["USER_CREATED_BY"]);



            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[13];

                paramList[0] = new SqlParameter("@P_USER_ID", _id);
                paramList[1] = new SqlParameter("@P_USER_FIRST_NAME", _firstName);
                paramList[2] = new SqlParameter("@P_USER_LAST_NAME", _lastName);
                paramList[3] = new SqlParameter("@P_USER_EMAIL", _email);
                paramList[4] = new SqlParameter("@P_USER_LOGIN_NAME", _loginName);
                paramList[5] = new SqlParameter("@P_USER_PASSWORD", _password);
                paramList[6] = new SqlParameter("@P_USER_LOCATION_ID", _locationId);
                paramList[7] = new SqlParameter("@P_USER_MEMBER_TYPE", _memberType.ToString());
                paramList[8] = new SqlParameter("@P_USER_ADDRESS", _address);
                paramList[9] = new SqlParameter("@P_USER_STATUS", _status);
                paramList[10] = new SqlParameter("@P_USER_CREATED_BY", _createdBy);
                paramList[11] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[11].Direction = ParameterDirection.Output;
                paramList[12] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[12].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_user_add_update", paramList);
                string messageType = Utility.ToString(paramList[11].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[12].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_USER_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_user_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Static Methods
        public static DataTable GetList()
        {

            try
            {
                SqlParameter[] paramList = null;
                return DBGateway.ExecuteQuery("ct_p_user_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public static bool IsAuthenticatedUser(string loginName, string password)
        {
            //In this method we are setting login info session object

            try
            {               

                SqlParameter[] paramList = new SqlParameter[4];

                paramList[0] = new SqlParameter("@P_USER_LOGIN_NAME", loginName);
                paramList[1] = new SqlParameter("@P_USER_PASSWORD", password);

                paramList[2] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[2].Direction = ParameterDirection.Output;
                paramList[3] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[3].Direction = ParameterDirection.Output;

                DataSet ds = DBGateway.ExecuteQuery("ct_p_user_authentication", paramList);
                string messageType = Utility.ToString(paramList[2].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[3].Value);
                    if (message != string.Empty) throw new Exception(message);
                }

                if (ds != null & ds.Tables.Count != 0)
                {
                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        LoginInfo loginInfo = new LoginInfo(ds.Tables[0].Rows[0]);
                        Settings.LoginInfo = loginInfo;
                    }
                }
                else
                {
                    throw new Exception("User Id/password does not exist !");
                }
                
                return true;
            }
            catch
            {
                throw;
            }
        }
       
        public static DataSet GetMemberTypeList(string type)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_FIELD_TYPE", type);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_field_type_getlist", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        public static void ChangePassword(string loginName, string currentPassword, string newPassword)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_LOGIN_NAME", loginName);
                paramList[1] = new SqlParameter("@P_PASSWORD_CURRENT", currentPassword);
                paramList[2] = new SqlParameter("@P_PASSWORD_NEW", newPassword);

                paramList[3] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[3].Direction = ParameterDirection.Output;

                paramList[4] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[4].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_user_change_password", paramList);

                string messageType = Utility.ToString(paramList[3].Value);
                string messageText= Utility.ToString(paramList[4].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[3].Value);
                    if (message != string.Empty) throw new Exception(messageText);
                }
            }
            catch
            {
                throw;
            }
        }*/
        
       // #endregion
    }
   
}
