using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class LocationMaster
    {
        private const long NEW_RECORD = -1;
        #region Member Variables        
        private long _id;
        private string _code;
        private string _name;
        //private string _docType;
        private string _address;
        private string _terms;
        private string _interfaceStatus;
        private string _currency;
        private string _status;        
        private long _createdBy;
        private long _retLocationId;
        /// <summary>
        /// Added for loading B2B agent id while editing
        /// </summary>
        private int _parentAgentId;
        private int _agentId;
        private string _countryCode;
        private int _stateId;
        private string _parentCode;
        private string _gstNumber;
        private string _locationEmail; //Added for to get the email id from location_master table for the corporate
        
        private string _locationMapCode;
        #endregion

        #region Properties
        public long ID
        {
            get { return _id; }            
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        //public string DocType
        //{
        //    get { return _docType; }
        //    set { _docType = value; }
        //}
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public string Terms
        {
            get { return _terms ; }
            set { _terms  = value; }
        }

        public string InterfaceStatus
        {
            get { return _interfaceStatus; }
            set { _interfaceStatus = value; }
        }


        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }            

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public long RetLocationId
        {
            get { return _retLocationId; }
            set { _retLocationId = value; }
        }
        /// <summary>
        /// Added for loading B2B agent id while editing
        /// </summary>
        public int ParentAgentId
        {
            get { return _parentAgentId; }
            set { _parentAgentId = value; }
        }

        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }

        public string CountryCode
        {
            get
            {
                return _countryCode;
            }

            set
            {
                _countryCode = value;
            }
        }

        public int StateId
        {
            get
            {
                return _stateId;
            }
            set
            {
                _stateId = value;
            }
        }
        public string ParentCode
        {
            get
            {
                return _parentCode;
            }
            set
            {
                _parentCode = value;
            }
        }
        public string GstNumber
        {
            get
            {
                return _gstNumber;
            }
            set
            {
                _gstNumber = value;
            }
        }
        public string LocationEmail  //Added for to get the email id from location_master table for the corporate.
        {
            get { return _locationEmail; }
            set { _locationEmail = value; }
        }       

        public string LocationMapCode
        {
            get { return _locationMapCode; }
            set { _locationMapCode = value; }
        }
        #endregion

        #region Constructors
        public LocationMaster()
        {            
            _id = -1;
        }
        public LocationMaster(long id)
        {
            
            
            _id = id;
            GetDetails(id);
        }
        #endregion

        #region Methods
        private void GetDetails(long id)
        {
            
            DataSet ds = GetData(id);
            UpdateBusinessData(ds);
            //if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            //{
            //    UpdateBusinessData(ds.Tables[0].Rows[0]);
            //}
            
        }
        private DataSet GetData(long id)
        {
            
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_LOCATION_ID", id);                 
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_location_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {
            
            try
            {
                _id = Utility.ToLong(dr["LOCATION_ID"]);
                _code = Utility.ToString(dr["LOCATION_CODE"]);
                _name = Utility.ToString(dr["LOCATION_NAME"]);
                _agentId = Utility.ToInteger(dr["LOCATION_AGENT_ID"]);
                _address = Utility.ToString(dr["LOCATION_ADDRESS"]);
                _terms = Utility.ToString(dr["LOCATION_TERMS"]);
                //_interfaceStatus = Utility.ToString(dr["LOCATION_INTERFACE_STATUS"]);
                _status = Utility.ToString(dr["LOCATION_STATUS"]);
                _createdBy = Utility.ToLong(dr["LOCATION_CREATED_BY"]);
                _currency = Utility.ToString(dr["LOCATION_CURRENCY"]);
                if (dr["agent_parent_id"] != DBNull.Value)
                {
                    _parentAgentId = Utility.ToInteger(dr["agent_parent_id"]);
                }
                else
                {
                    _parentAgentId = -1;
                }
                if (dr["location_country_code"] != DBNull.Value)
                {
                    _countryCode = Utility.ToString(dr["location_country_code"]);
                }

               
                _stateId = Utility.ToInteger(dr["location_state_id"]);
                _parentCode = Utility.ToString(dr["location_parent_code"]);
                _gstNumber = Utility.ToString(dr["location_GSTIN"]);
                _locationEmail = Utility.ToString(dr["location_email"]); //getting email id from location_master table
               
				_locationMapCode = Utility.ToString(dr["map_code"]);
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null) 
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow  dr = ds.Tables[0].Rows[0];
                        if(Utility.ToLong(dr["Location_ID"])>0) UpdateBusinessData(ds.Tables[0].Rows[0]);                        
                    }

                   
                }

            }
            catch 
            { throw; }

        }
        #endregion

        #region Public Methods
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;

            try
            {
                 cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;                
                

                SqlParameter[] paramList = new SqlParameter[18];
                paramList[0] = new SqlParameter("@P_LOCATION_ID", _id);
                paramList[1] = new SqlParameter("@P_LOCATION_CODE", _code);
                paramList[2] = new SqlParameter("@P_LOCATION_NAME", _name);
               if (_agentId>0) paramList[3] = new SqlParameter("@P_LOCATION_AGENT_ID", _agentId);                
                paramList[4] = new SqlParameter("@P_LOCATION_ADDRESS", _address);
                paramList[5] = new SqlParameter("@P_LOCATION_TERMS", _terms);
                //paramList[6] = new SqlParameter("@P_LOCATION_INTERFACE_STATUS", _interfaceStatus);
                paramList[7] = new SqlParameter("@P_LOCATION_STATUS", _status);
                paramList[8] = new SqlParameter("@P_LOCATION_CREATED_BY", _createdBy);

               

                paramList[9] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[9].Direction = ParameterDirection.Output;
                paramList[10] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[10].Direction = ParameterDirection.Output;
                //paramList[11] = new SqlParameter("@P_LOCATION_ID_RET", SqlDbType.BigInt);
                //paramList[11].Direction = ParameterDirection.Output;
                paramList[12] = new SqlParameter("@P_LOCATION_CURRENCY", _currency);

                paramList[13] = new SqlParameter("@P_LOCATION_COUNTRY_CODE", _countryCode);
                paramList[14] = new SqlParameter("@P_LOCATION_STATE_ID", _stateId);
                paramList[15] = new SqlParameter("@P_LOCATION_PARENT_CODE", _parentCode);
                paramList[16] = new SqlParameter("@P_LOCATION_GSTIN", _gstNumber);               
			if (!string.IsNullOrEmpty(_locationMapCode)) paramList[17] = new SqlParameter("@p_Location_Map_Code", _locationMapCode);
				DBGateway.ExecuteNonQuery("ct_p_location_add_update", paramList);
                string messageType = Utility.ToString(paramList[9].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[10].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                //_retLocationId = Utility.ToLong(paramList[11].Value);

                trans.Commit();
            }
            catch
            {
                trans.Rollback();
                throw;
            }
         finally
         {
             cmd.Connection.Close();
         }
        }

      
        public void Delete()
        {            
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LOCATION_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output; 
                
                DBGateway.ExecuteNonQuery("ct_p_location_add_update", paramList);                
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }

        

        #endregion

        #region Static Methods
        public static DataTable GetList(int agentId, ListStatus status, RecordStatus recordStatus,string type)
        {
            
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                if(agentId>0) paramList[0] = new SqlParameter("@P_LOCATION_AGENT_ID", agentId );
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                paramList[3] = new SqlParameter("@P_AGENT_TYPE", type);
                return DBGateway.ExecuteQuery("ct_p_location_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        //Added By Brahmam To Get The Gst Details   16.05.2018
        public static DataTable GetGSTList(long locationId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_VS_LOCATION_ID", locationId);
                return DBGateway.ExecuteQuery("usp_GetGSTValues", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetTourRegionList(ListStatus status, RecordStatus recordStatus, string regionType, string countryId)
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[4];

                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                if (!string.IsNullOrEmpty(regionType)) paramList[2] = new SqlParameter("@P_REGION_TYPE", regionType);
                if (!string.IsNullOrEmpty(countryId)) paramList[3] = new SqlParameter("@P_COUNTRY_CODE", countryId);

                return DBGateway.ExecuteQuery("TOUR_REGION_GETLIST", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        //-----Added by somasekhar on 28/09/2018-----
        // To Set Nationality and Residency as INDIA for BehalfofAgent Login Country code IN
        public static string GetCountryCodeByLocationId(int locationId)
        {
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@locationId", locationId);
            paramList[1] = new SqlParameter("@countryCode", System.Data.SqlDbType.VarChar, 5);
            paramList[1].Direction = System.Data.ParameterDirection.Output;
            int retVal = DBGateway.ExecuteNonQuerySP("usp_GetCountryCodeByLocId", paramList);
            if (paramList[1].Value != DBNull.Value)
            {
                return Convert.ToString(paramList[1].Value);
            }
            else
            {
                return string.Empty;
            }
        }
        public static DataTable GetAgentParentLocations(int agentId,string agentType )
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                if (agentId > 0) paramList[0] = new SqlParameter("@AgentID", agentId);
                paramList[1] = new SqlParameter("@AgentType", agentType);
                return DBGateway.ExecuteQuery("usp_GetAgentParentLocations", paramList).Tables[0];
            }
            catch
            {
                throw;
            }

        }
        #endregion
    }
}

