using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class DocumentsMaster
    {        
        private const long NEW_RECORD = -1;

        #region Member Variables
        private long _transactionid;
        private string _code;
        private string _name;                
        private long _countryId;
        private long _nationalityId;
        private long _visaTypeId;
        private long _residenceId;
        private string _status;
        private long _createdBy;       
#endregion     
        #region Properties

        public long TransactionId
        {
            get { return _transactionid; }
        }

        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
      
        public long CountryID
        {
            get { return _countryId; }
            set { _countryId = value; }
        }
        public long NationalityId
        {
            get { return _nationalityId; }
            set { _nationalityId = value; }
        }
        public long VisaTypeId
        {
            get { return _visaTypeId; }
            set { _visaTypeId = value; }
        }

        public long ResidenceId
        {
            get { return _residenceId; }
            set { _residenceId = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
          #endregion

          #region Constructors
        public DocumentsMaster()
        {
            _transactionid  = NEW_RECORD;
        }
        public DocumentsMaster(long id)
        {
            _transactionid  = id;
            getDetails(_transactionid );
        
        }


        #endregion

        #region Methods

        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_DOC_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("OBVISA_p_documents_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _transactionid = Utility.ToLong(dr["doc_id"]);
                _code = Utility.ToString(dr["doc_code"]);
                _name  = Utility.ToString(dr["doc_name"]);               
                _countryId = Utility.ToLong(dr["doc_country_id"]);
                _nationalityId = Utility.ToLong(dr["doc_nationality_id"]);

                _visaTypeId = Utility.ToLong(dr["doc_visa_type_id"]);
                _residenceId = Utility.ToLong(dr["doc_residence_id"]);
                _status = Utility.ToString(dr["doc_status"]);
                _createdBy = Utility.ToLong(dr["doc_created_by"]);

            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[24];

                paramList[0] = new SqlParameter("@P_DOC_ID", _transactionid);
                paramList[1] = new SqlParameter("@P_DOC_CODE", _code);
                paramList[2] = new SqlParameter("@P_DOC_NAME", _name );                
                paramList[3] = new SqlParameter("@P_DOC_COUNTRY_ID", _countryId);
                if (_nationalityId != 0) paramList[4] = new SqlParameter("@P_DOC_NATIONALITY_ID", _nationalityId);
                if (_visaTypeId != 0) paramList[5] = new SqlParameter("@P_DOC_VISA_TYPE_ID", _visaTypeId);
                if (_residenceId != 0) paramList[6] = new SqlParameter("@P_DOC_RESIDENCE_ID", _residenceId);

                paramList[7] = new SqlParameter("@P_DOC_STATUS", _status);
                paramList[8] = new SqlParameter("@P_DOC_CREATED_BY", _createdBy);


                paramList[9] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[9].Direction = ParameterDirection.Output;
                paramList[10] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[10].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("OBVISA_p_documents_add_update", paramList);
                string messageType = Utility.ToString(paramList[9].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[10].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_REQ_id", _transactionid);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("OBVISA_p_requirement_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        #endregion


        #region Static Methods
        public static DataTable GetList(int agentId,ListStatus status,RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                if(agentId>0) paramList[0] = new SqlParameter("@P_DOC_AGENT_ID", agentId);
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", recordStatus);

                return DBGateway.ExecuteQuery("vis_p_documents_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetDocumentsList(ListStatus status, RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus != RecordStatus.All) paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                return DBGateway.ExecuteQuery("OBVISA_p_documents_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetUploadDocList( int agentId,ListStatus status, RecordStatus recordStatus, long visaId, long paxId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                if(agentId>0) paramList[0] = new SqlParameter("@P_DOC_AGENT_ID", agentId);
                paramList[1] = new SqlParameter("@P_LIST_STATUS", Utility.ToInteger((char)status));
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                if (visaId > 0) paramList[3] = new SqlParameter("@P_VS_ID", visaId);
                if (paxId > 0) paramList[4] = new SqlParameter("@P_PAX_ID", paxId);

                return DBGateway.ExecuteQuery("VISA_P_DOC_UPLOAD_GETLIST", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        # endregion
       
    }
   
}
