using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;

/// <summary>
/// Summary description for HandlingFeeMaster
/// </summary>
 namespace CT.TicketReceipt.BusinessLayer
{
     public enum OBVisaDispatchStatus
     {
         All = 0,//All
         Pending = 78,// Value N
         Submitted = 83,//S Submitted
         Dispatched = 80, //P Dispatched
         Rejected = 82, //R
         Approved = 65,//A
         Delivered = 68,//H hand over to Customer
         //Operations=79,//O=Dispatch to Airport
         //BackOffice = 66,//B=Back Office
         //Acknowldeged = 87,//W Acknowldged

     }
public class TranxOBVisaSales
{
    # region members
    long _transactionId;
    string _docType;
    string _docNumber;
    DateTime  _docDate;
   
    string _passportType;//S
    private int _countryId;//S
    private string  _countryName;//S
    private long _countryCityId;//S
    private int _nationalityId;//S
    private string _nationalityName;//S
    private int _visaTypeId;
    string _visaTypeName;//S
    //string _salesTypeName;//S
    private int _residencyId;//S
    private long _resCityId;//S
    private string _visaCategory;//S
    private long _appCenterId;//S
    private DateTime _ttTravelDate;//S
    int _adults;
    int _children;
    int _infants;
    decimal _totVisaFee;
    decimal _addCharges;
    //string _addChargesRemarks;
    decimal _discount;
    string _remarks;
    //string _depositMode;
  
    long _locationId;
    string _locationTerms;
    string _locationAdds;
    long _companyId;
    string _corpCustomer;
    string _corpLPNno;
    RecordStatus _status;
    long _createdBy;
    string _createdByName;
    DataTable _dtVisaPaxDetails;
    DataTable _dtPaxDocDetails;
    string _docKeys;

    string _settlementMode;
    string _currencyCode;
    decimal _exchangeRate;
    PaymentModeInfo _cashMode = new PaymentModeInfo();
    PaymentModeInfo _creditMode = new PaymentModeInfo();
    PaymentModeInfo _cardMode = new PaymentModeInfo();
    PaymentModeInfo _employeeMode = new PaymentModeInfo();
    PaymentModeInfo _othersMode = new PaymentModeInfo();
    string _modeRemarks;
    # endregion

    # region Properties
    public long TransactionId
    {
        get {return _transactionId ;}
        set { _transactionId = value; }
    }
    public string DocType
    {
        get {return _docType;}
        set { _docType = value; }
    }
    public string DocNumber
    {
        get { return _docNumber; }
        set { _docNumber = value; }
    }

    public DateTime DocDate
    {
        get { return _docDate; }
        set { _docDate = value; }
    }
    
  
    public int VisaTypeId
    {
        get { return _visaTypeId; }
        set { _visaTypeId = value; }
    }
    public string  VisaTypeName
    {
        get { return _visaTypeName; }
        set { _visaTypeName = value; }
    }

    public int Adults
    {
        get { return _adults; }
        set { _adults = value; }
    }
    public int Children
    {
        get { return _children; }
        set { _children = value; }
    }
    public int Infants
    {
        get { return _infants; }
        set { _infants = value; }
    }
    public decimal TotVisaFee
    {
        get { return _totVisaFee; }
        set { _totVisaFee = value; }
    }
    public decimal AddCharges
    {
        get { return _addCharges; }
        set { _addCharges = value; }
    }
    //public string AddChargesRemarks
    //{
    //    get { return _addChargesRemarks; }
    //    set { _addChargesRemarks = value; }
    //}



    public string PassportType
    {
        get { return _passportType; }
        set { _passportType = value; }
    }
    public int CountryId
    {
        get { return _countryId; }
        set { _countryId = value; }
    }
    public string CountryName
    {
        get { return _countryName; }
        set { _countryName = value; }
    }

    public long CountryCityId
    {
        get { return _countryCityId; }
        set { _countryCityId= value; }
    }

    public int NationalityId
    {
        get { return _nationalityId; }
        set { _nationalityId = value; }
    }
    public string NationalityName
    {
        get { return _nationalityName; }
        set { _nationalityName = value; }
    }

    public int ResidencyId
    {
        get { return _residencyId; }
        set { _residencyId= value; }
    }
    public long ResCityId
    {
        get { return _resCityId; }
        set { _resCityId= value; }
    }

    public string  VisaCategory
    {
        get { return _visaCategory; }
        set { _visaCategory = value; }
    }

    public long AppCenterId
    {
        get { return _appCenterId; }
        set { _appCenterId = value; }
    }

    public DateTime TTTravelDate
    {
        get { return _ttTravelDate; }
        set { _ttTravelDate= value; }
    }




    

    public string Remarks
    {
        get { return _remarks; }
        set { _remarks = value; }
    }
    //public string DepositMode
    //{
    //    get { return _depositMode; }
    //    set { _depositMode = value; }
    //}
   
    public decimal Discount
    {
        get { return _discount; }
        set { _discount = value; }
    }
   
 
    public long LocationId
    {
        get { return _locationId; }
        set { _locationId = value; }
    }
    public string LocationTerms
    {
        get { return _locationTerms; }
        set { _locationTerms = value; }
    }
    public string LocationAdds
    {
        get { return _locationAdds; }
        set { _locationAdds = value; }
    }

    public long CompanyId
    {
        get { return _companyId; }
        set { _companyId = value; }
    }
    public RecordStatus Status
    {
        get { return _status; }
        set { _status = value; }
    }
    public long CreatedBy
    {
        get { return _createdBy; }
        set { _createdBy = value; }
    }
    public string CreatedByName
    {
        get { return _createdByName; }
        set { _createdByName = value; }
    }
    public DataTable PaxDetailsList
    {
        get { return _dtVisaPaxDetails; }
        set{_dtVisaPaxDetails=value;}
    }
    public DataTable PaxDocDetails
    {
        get { return _dtPaxDocDetails; }
        set { _dtPaxDocDetails = value; }
    }
    public string DocKeys
    {
        get { return _docKeys; }
        set { _docKeys = value; }
    }
    public string CorpCustomer
    {
        get { return _corpCustomer; }
        set { _corpCustomer = value; }
    }

    public string CorpLPNno
    {
        get { return _corpLPNno; }
        set { _corpLPNno = value; }
    }


    public string SettlementMode
    {
        get { return _settlementMode; }
        set { _settlementMode = value; }
    }
    public string CurrencyCode
    {
        get { return _currencyCode; }
        set { _currencyCode = value; }
    }
    public decimal ExchangeRate
    {
        get { return _exchangeRate; }
        set { _exchangeRate = value; }
    }
    public PaymentModeInfo CashMode
    {
        get { return _cashMode; }
        set { _cashMode = value; }
    }
    public PaymentModeInfo CreditMode
    {
        get { return _creditMode; }
        set { _creditMode = value; }
    }
    public PaymentModeInfo CardMode
    {
        get { return _cardMode; }
        set { _cardMode = value; }
    }
    public PaymentModeInfo EmployeeMode
    {
        get { return _employeeMode; }
        set { _employeeMode = value; }
    }
    public PaymentModeInfo OthersMode
    {
        get { return _othersMode; }
        set { _othersMode = value; }
    }
    public string ModeRemarks
    {
        get { return _modeRemarks; }
        set { _modeRemarks = value; }
    }

   
    # endregion

    # region Constructors

    public TranxOBVisaSales()
    {
        //
        // TODO: Add constructor logic here
        //
        _transactionId = -1;
        DataSet ds = GetDetails(_transactionId);
        //if (dt != null && dt.Rows.Count > 0)
        UpdateBusinessData(ds);
    }
    public TranxOBVisaSales(long transactionId)
    {
        try
        {
            DataSet ds = GetDetails(transactionId);
            //if(dt!=null && dt.Rows.Count>0)
            UpdateBusinessData(ds);
        }
        catch { throw; }

    }
    # endregion

    //# region Private Methods
    private DataSet GetDetails(long transactionId)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[4];
            SqlParameter param;
            param = new SqlParameter("@p_fvs_id", SqlDbType.Decimal);
            param.Value = transactionId; param.Size = 8;
            paramArr[0] = param;

            param = new SqlParameter("@p_fvs_location_id", SqlDbType.Decimal);
            param.Value = Settings.LoginInfo.LocationID; param.Size = 8;
            paramArr[1] = param;


            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[2] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[3] = paramMsgText;


            return DBGateway.ExecuteQuery("obvisa_p_visa_sales_getdata", paramArr);
        }
        catch { throw; }
    }
    private void UpdateBusinessData(DataSet ds)
    {
        try
        {
            if (ds != null)
            {
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                _dtVisaPaxDetails = ds.Tables[1];
                if(_transactionId>0)_dtPaxDocDetails = ds.Tables[2];
            }


        }
        catch { throw; }
    }

    private void UpdateBusinessData(DataRow dr)
    {
        try
        {
            if (dr != null)
            {
                if (_transactionId < 0)
                {
                    _docType = Utility.ToString(dr["fdoc_type"]);
                    _docDate = Utility.ToDate(dr["fdoc_date"]);
                }
                else
                {
                    _transactionId = Utility.ToLong(dr["fvs_id"]);
                    _docNumber = Utility.ToString(dr["fvs_doc_no"]);
                    //_docType = Utility.ToString(dr["vs_doc_type"]);
                    _docDate = Utility.ToDate(dr["fvs_doc_date"]);
                    _countryName = Utility.ToString(dr["country_name"]);
                    _nationalityName = Utility.ToString(dr["NATIONALITY_NAME"]);
                    _adults = Utility.ToInteger(dr["fvs_adult"]);
                    _children = Utility.ToInteger(dr["fvs_child"]);
                    _infants = Utility.ToInteger(dr["fvs_infant"]);
                    _visaTypeId = Utility.ToInteger(dr["fvs_visa_type_id"]);
                    _visaTypeName = Utility.ToString(dr["visa_type_name"]);
                    _totVisaFee = Utility.ToDecimal(dr["FVS_TOTAL_VISA_FEE"]);
                    _addCharges = Utility.ToDecimal(dr["FVS_ADD_CHARGES"]);
                    
                    _settlementMode = Utility.ToString(dr["fvs_settlement_mode"]);
                    _currencyCode = Utility.ToString(dr["fvs_CURRENCY_CODE"]);
                    _exchangeRate = Utility.ToDecimal(dr["fvs_EXCHANGE_RATE"]);
                    _cashMode.Set(Utility.ToDecimal(dr["fvs_cash_base_amount"]), Utility.ToDecimal(dr["fvs_cash_local_amount"]));
                    _creditMode.Set(Utility.ToDecimal(dr["fvs_credit_base_amount"]), Utility.ToDecimal(dr["fvs_credit_local_amount"]));
                    _cardMode.Set(Utility.ToDecimal(dr["fvs_card_base_amount"]), Utility.ToDecimal(dr["fvs_card_local_amount"]));
                    _employeeMode.Set(Utility.ToDecimal(dr["fvs_employee_base_amount"]), Utility.ToDecimal(dr["fvs_employee_local_amount"]));
                    _othersMode.Set(Utility.ToDecimal(dr["fvs_others_base_amount"]), Utility.ToDecimal(dr["fvs_others_local_amount"]));
                    _modeRemarks = Utility.ToString(dr["fvs_mode_remarks"]);
                    _createdBy = Utility.ToLong(dr["fvs_created_by"]);
                    _createdByName = Utility.ToString(dr["fvs_created_name"]);
                    _locationTerms = Utility.ToString(dr["LOCATION_TERMS"]);
                    _locationAdds = Utility.ToString(dr["LOCATION_ADDRESS"]);
                    //_salesTypeId = Utility.ToInteger(dr["vs_sales_type_id"]);
                    //_salesTypeName = Utility.ToString(dr["vs_sales_type_name"]);
                    //_visaFeeId = Utility.ToLong(dr["vs_visa_id"]);
                    //_visaFee = Utility.ToDecimal(dr["vs_visa_fee"]);
                    //_securityDeposit = Utility.ToDecimal(dr["vs_security_deposit"]);
                    //_approveId = Utility.ToInteger(dr["vs_waive_security_approve_id"]);
                    //_approveId = Utility.ToInteger(dr["vs_waive_security_approve_id"]);
                    //_remarks = Utility.ToString(dr["vs_remarks"]);
                    //_depositMode = Utility.ToString(dr["vs_deposit_mode"]);
                    //_chequeDate = Utility.ToDate(dr["vs_cheque_date"]);
                    //_chequeNo = Utility.ToString(dr["vs_cheque_no"]);
                    //_chequeAmount = Utility.ToDecimal(dr["vs_cheque_amount"]);
                    //_toCollect = Utility.ToDecimal(dr["vs_to_collect"]);
                    //_discount = Utility.ToDecimal(dr["vs_discount"]);
                    //_pnrStatus = Utility.ToString(dr["vs_pnr_status"]);
                    //_ticketId = Utility.ToLong(dr["vs_ticket_id"]);
                    //_pnrNo = Utility.ToString(dr["vs_pnr_no"]);
                    //_ticketFare = Utility.ToDecimal(dr["vs_ticket_fare"]);
                    ////_DBONo = Utility.ToString(dr["vs_dbo_doc_no"]);
                    ////_DAPNo = Utility.ToString(dr["vs_dap_doc_no"]);
                    ////_DAPTrackingNo = Utility.ToString(dr["vs_dap_tracking_no"]);
                    ////_dispatchStatus = Utility.ToString(dr["vs_dispatch_status"]);
                    //_corpCustomer = Utility.ToString(dr["vs_corp_customer"]);
                    //_corpLPNno = Utility.ToString(dr["vs_corp_lpono"]);
                    //_status = Utility.ToString(dr["vs_status"]);
                    //_locationId = Utility.ToLong(dr["vs_location_id"]);
                    //_companyId = Utility.ToLong(dr["vs_company_id"]);
                    //_createdBy = Utility.ToLong(dr["vs_created_by"]);
                    //_createdByName = Utility.ToString(dr["vs_created_name"]);

                    //_grntName = Utility.ToString(dr["vs_grnt_name"]);
                    //_grntVisaNo = Utility.ToString(dr["vs_grnt_visa_number"]);
                    //_grntVisaExpDate = Utility.ToDate(dr["vs_grnt_visa_expiry_date"]);
                    //_grntPassportNo = Utility.ToString(dr["vs_grnt_passport_no"]);
                    //_grntPassportIssueDate = Utility.ToDate(dr["vs_grnt_passport_issue_date"]);
                    //_grntPassportExpDate = Utility.ToDate(dr["vs_grnt_passport_expiry_date"]);
                    //_grntMobileNo = Utility.ToString(dr["vs_grnt_mobile_no"]);
                    //_grntOfficeNo = Utility.ToString(dr["vs_grnt_office_no"]);
                    //_grntEmail = Utility.ToString(dr["vs_grnt_email"]);






                }
            }


        }
        catch { throw; }

    }
    public void Save()
    {
        SqlTransaction trans = null;
        SqlCommand cmd = null;
        try
        {
            //SqlCommand cmd = DBGateway.CreateCommand(trans);
            cmd = new SqlCommand();
            cmd.Connection = DBGateway.CreateConnection();
            cmd.Connection.Open();
            trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
            cmd.Transaction = trans;
            SqlParameter[] paramArr = new SqlParameter[43];

            paramArr[0] = new SqlParameter("@p_fvs_id", _transactionId);
            paramArr[1] = new SqlParameter("@p_fvs_doc_type", _docType);

            SqlParameter paramDocNo = new SqlParameter("@p_fvs_doc_no", SqlDbType.NVarChar);
            paramDocNo.Direction = ParameterDirection.Output; paramDocNo.Size = 50;
            paramArr[2] = paramDocNo;

            paramArr[3]= new SqlParameter("@p_fvs_doc_date", _docDate);
            paramArr[4] = new SqlParameter("@P_FVS_PASSPORT_TYPE", _passportType);
            paramArr[5] = new SqlParameter("@P_FVS_COUNTRY_ID", _countryId);
            paramArr[6] = new SqlParameter("@P_FVS_COUNTRY_CITY_ID", _countryCityId);
            paramArr[7] = new SqlParameter("@P_FVS_NATIONALITY_ID", _nationalityId);
            paramArr[8] = new SqlParameter("@P_FVS_VISA_TYPE_ID", _visaTypeId);
            paramArr[9] = new SqlParameter("@P_FVS_RESIDENCE_ID", _residencyId);
            if(_resCityId>0)paramArr[10] = new SqlParameter("@P_FVS_RES_CITY_ID", _resCityId);
            paramArr[11] = new SqlParameter("@P_FVS_VISA_CATEGORY", _visaCategory);
            paramArr[12] = new SqlParameter("@P_FVS_APP_CENTER_ID", _appCenterId);
            if(_ttTravelDate!=Utility.ToDate(""))paramArr[13] = new SqlParameter("@P_FVS_TENT_TRAVEL_DATE", _ttTravelDate);
            paramArr[14] = new SqlParameter("@P_FVS_ADULT", _adults);
            paramArr[15] = new SqlParameter("@P_FVS_CHILD", _children);
            paramArr[16] = new SqlParameter("@P_FVS_INFANT", _infants);
            paramArr[17] = new SqlParameter("@P_FVS_TOTAL_VISA_FEE", _totVisaFee);
            paramArr[18] = new SqlParameter("@P_FVS_DISCOUNT", _discount);

            paramArr[19] = new SqlParameter("@P_FVS_SETTLEMENT_MODE", _settlementMode);
            paramArr[20] = new SqlParameter("@P_FVS_CURRENCY_CODE", _currencyCode);
            paramArr[21] = new SqlParameter("@P_FVS_EXCHANGE_RATE", _exchangeRate);
            paramArr[22] = new SqlParameter("@P_FVS_CASH_BASE_AMOUNT", _cashMode.BaseAmount);
            paramArr[23] = new SqlParameter("@P_FVS_CASH_LOCAL_AMOUNT", _cashMode.LocalAmount);
            paramArr[24] = new SqlParameter("@P_FVS_CREDIT_BASE_AMOUNT", _creditMode.BaseAmount);
            paramArr[25] = new SqlParameter("@P_FVS_CREDIT_LOCAL_AMOUNT", _creditMode.LocalAmount);
            paramArr[26] = new SqlParameter("@P_FVS_CARD_BASE_AMOUNT", _cardMode.BaseAmount);
            paramArr[27] = new SqlParameter("@P_FVS_CARD_LOCAL_AMOUNT", _cardMode.LocalAmount);
            paramArr[28] = new SqlParameter("@P_FVS_EMPLOYEE_BASE_AMOUNT", _employeeMode.BaseAmount);
            paramArr[29] = new SqlParameter("@P_FVS_EMPLOYEE_LOCAL_AMOUNT", _employeeMode.LocalAmount);
            paramArr[30] = new SqlParameter("@P_FVS_OTHERS_BASE_AMOUNT", _othersMode.BaseAmount);
            paramArr[31] = new SqlParameter("@P_FVS_OTHERS_LOCAL_AMOUNT", _othersMode.LocalAmount);
            paramArr[32] = new SqlParameter("@P_FVS_MODE_REMARKS", _modeRemarks);

            paramArr[33] = new SqlParameter("@P_FVS_CORP_CUSTOMER", _corpCustomer);
            paramArr[34] = new SqlParameter("@P_FVS_CORP_LPONO", _corpLPNno);
            paramArr[35] = new SqlParameter("@P_FVS_REMARKS", _remarks);
            paramArr[36] = new SqlParameter("@P_FVS_STATUS", Utility.ToString((char)_status)) ;
            paramArr[37] = new SqlParameter("@P_FVS_LOCATION_ID", _locationId);
            paramArr[38] = new SqlParameter("@P_FVS_COMPANY_ID", _companyId);
            paramArr[39] = new SqlParameter("@P_FVS_CREATED_BY", _createdBy    );

            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[40] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[41] = paramMsgText;

            SqlParameter paramMsgText1 = new SqlParameter("@p_fvs_id_ret", SqlDbType.BigInt);
            paramMsgText1.Size = 20;
            paramMsgText1.Direction = ParameterDirection.Output;
            paramArr[42] = paramMsgText1;

            //trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
            DBGateway.ExecuteNonQueryDetails(cmd, "OBvisa_P_Visa_Sales_Add_Update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

            _docNumber = Utility.ToString(paramDocNo.Value);
            _transactionId = Utility.ToLong(paramMsgText1.Value);


            if (_dtVisaPaxDetails != null)
            {
                DataTable dt = _dtVisaPaxDetails.GetChanges();
                int recordStatus = 0;
                if (dt != null && dt.Rows.Count > 0)
                {

                    //try
                    //{
                    foreach (DataRow dr in dt.Rows)
                    {
                        switch (dr.RowState)
                        {
                            case DataRowState.Added: recordStatus = 1;
                                break;
                            case DataRowState.Modified: recordStatus = 2;
                                break;
                            case DataRowState.Deleted: recordStatus = -1;
                                break;
                            default: break;


                        }
                        saveDetails(cmd, recordStatus, Utility.ToLong(dr["FPAX_ID"]), _transactionId, Utility.ToString(dr["FPAX_NAME"]), Utility.ToString(dr["FPAX_PROFESSION"]),
                                 Utility.ToString(dr["FPAX_MARITAL_STATUS"]), Utility.ToString(dr["FPAX_GENDER"]), Utility.ToString(dr["FPAX_EMAIL"]),
                                Utility.ToString(dr["FPAX_MOBILE"]), Utility.ToString(dr["FPAX_PHONE"]), Utility.ToString(dr["FPAX_FAX"]), Utility.ToString(dr["FPAX_STREET_ADDS"]),
                                Utility.ToString(dr["FPAX_CITY"]), Utility.ToString(dr["FPAX_STATE"]), Utility.ToString(dr["FPAX_ZIP_CODE"]), Utility.ToInteger(dr["FPAX_COUNTRY_ID"]),
                                Utility.ToString(dr["FPAX_PASSPORT_TYPE"]), Utility.ToString(dr["FPAX_PASSPORT_NO"]), Utility.ToDate(dr["FPAX_PSP_ISSUE_DATE"]),
                                Utility.ToString(dr["FPAX_PSP_ISSUE_PLACE"]), Utility.ToDate(dr["FPAX_PSP_EXPIRY_DATE"]), string.Empty,Utility.ToString(dr["FPAX_FORM_SUBMIT_STATUS"]) ,  RecordStatus.Activated, _createdBy, Utility.ToString(dr["FPAX_DOC_KEYS"]));
                    }
                    //}
                    //catch { throw; }
                    //finally
                    //{
                    //    DBGateway.CloseConnection(cmd);
                    //}
                }
            }
    //        if (!string.IsNullOrEmpty(_docKeys))
    //        {
    //            string[] arrDocKeys = _docKeys.Split(',');
    //            // SqlCommand cmd = DBGateway.CreateCommand();
    //            //try
    //            //{
    //            for (int x = 0; x < arrDocKeys.Length; x++)
    //            {
    //                saveDocDetails(cmd, 1, Utility.ToLong(arrDocKeys[x].ToString()), _transactionId, Settings.ACTIVE, _createdBy);
    //            }
    //            //}
    //            //catch { throw; }
    //            //finally
    //            //{
    //            //    DBGateway.CloseConnection(cmd);
    //            //}
    //        }
            //cmd.Transaction.Commit();
            trans.Commit();
            //cmd.Connection.Close();
        }
        catch
        {
            trans.Rollback();
            //cmd.Connection.Close();
            throw;
        }
        finally
        {
            cmd.Connection.Close();
        }
    }
    ////private void saveDetails(SqlCommand cmd, int recordStatus, long paxId, long paxHeaderId, string gurantorName, string gurantorMobileNo, string gurantorEmail, string paxName,
    ////                         string paxPassport, int paxNationalityId, string paxMobile, string paxEmail, string paxStatus, long paxCreatedBy)
    private void saveDetails(SqlCommand cmd, int recordStatus, long paxId, long paxHeaderId, string paxName,
        string profession, string maritialStatus, string gender, string email, string mobile,string phone,string fax,string streetAdds,
        string city, string state, string zipCode, int countryId, string passportType,
        string passportNo, DateTime PspIssueDate,string PspIssuePlace,  DateTime PspExpDate,string remarks, string submitStatus,RecordStatus status, long createdBy,string docKeys)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[28];
            paramArr[0] = new SqlParameter("@p_record_status", recordStatus);
            paramArr[1] = new SqlParameter("@p_fpax_id", paxId);
            paramArr[2] = new SqlParameter("@p_fpax_vsh_id", paxHeaderId);
            paramArr[3] = new SqlParameter("@P_fpax_name", paxName);
            if (!string.IsNullOrEmpty(profession)) paramArr[4] = new SqlParameter("@P_fpax_profession", profession);
            if (!string.IsNullOrEmpty(maritialStatus)) paramArr[5] = new SqlParameter("@P_fpax_marital_status", maritialStatus);
            paramArr[6] = new SqlParameter("@P_fpax_gender", gender);
            if (!string.IsNullOrEmpty(email)) paramArr[7] = new SqlParameter("@P_fpax_email", email);
            if (!string.IsNullOrEmpty(mobile)) paramArr[8] = new SqlParameter("@P_fpax_mobile", mobile);
            if (!string.IsNullOrEmpty(phone)) paramArr[9] = new SqlParameter("@P_fpax_phone", phone);
            if (!string.IsNullOrEmpty(fax)) paramArr[10] = new SqlParameter("@P_fpax_fax", fax);
            if (!string.IsNullOrEmpty(streetAdds)) paramArr[11] = new SqlParameter("@p_fpax_street_adds", streetAdds);
            if (!string.IsNullOrEmpty(city)) paramArr[12] = new SqlParameter("@P_fpax_city", city);
            if (!string.IsNullOrEmpty(state)) paramArr[13] = new SqlParameter("@P_fpax_state", state);
            if (!string.IsNullOrEmpty(zipCode)) paramArr[14] = new SqlParameter("@P_fpax_zip_code", zipCode);
            paramArr[15] = new SqlParameter("@P_fpax_country_id", countryId);
            paramArr[16] = new SqlParameter("@P_fpax_passport_type", passportType);
            paramArr[17] = new SqlParameter("@P_fpax_passport_no",passportNo);
            if (PspIssueDate!=Utility.ToDate("")) paramArr[18] = new SqlParameter("@P_fpax_psp_issue_date", PspIssueDate);
            if (!string.IsNullOrEmpty(PspIssuePlace)) paramArr[19] = new SqlParameter("@P_fpax_psp_issue_place", PspIssuePlace);
            if (PspExpDate!=Utility.ToDate("")) paramArr[20] = new SqlParameter("@P_fpax_psp_expiry_date", PspExpDate);
            if (!string.IsNullOrEmpty(remarks)) paramArr[21] = new SqlParameter("@P_fpax_remarks", remarks);
            paramArr[22] = new SqlParameter("@P_fpax_form_submit_status", submitStatus);
            paramArr[23] = new SqlParameter("@P_fpax_status", Utility.ToString((char)status));
            paramArr[24] = new SqlParameter("@P_fpax_created_by", createdBy);

            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[25] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[26] = paramMsgText;
            SqlParameter paramRetID= new SqlParameter("@P_FPAX_ID_RET", SqlDbType.BigInt);
            paramRetID.Size = 12;
            paramRetID.Direction = ParameterDirection.Output;
            paramArr[27] = paramRetID;

            
            DBGateway.ExecuteNonQueryDetails(cmd, "OBvisa_p_visa_sales_details_add_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

            if (!string.IsNullOrEmpty(docKeys))
            {
                string[] arrDocKeys = docKeys.Split(',');
                for (int x = 0; x < arrDocKeys.Length-1; x++)
                {
                    saveDocDetails(cmd, recordStatus, 1, _transactionId, Utility.ToLong(paramRetID.Value), Utility.ToLong(arrDocKeys[x].ToString()), _createdBy);
                }
               
            }
        }
        catch { throw; }
    }
    private void saveDocDetails(SqlCommand cmd, int recordStatus, long docId, long docVsId, long docPaxId, long docHeaderId, long docCreatedBy)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[8];
            paramArr[0] = new SqlParameter("@p_record_status", recordStatus);
            paramArr[1] = new SqlParameter("@P_DD_ID", docId);
            paramArr[2] = new SqlParameter("@P_DD_FVS_ID", docVsId);
            paramArr[3] = new SqlParameter("@P_DD_FPAX_ID", docPaxId);
            paramArr[4] = new SqlParameter("@P_DD_DOC_ID", docHeaderId);

            paramArr[5] = new SqlParameter("@P_DD_DOC_CREATED_BY", docCreatedBy);
            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[6] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[7] = paramMsgText;




            DBGateway.ExecuteNonQueryDetails(cmd, "OBvisa_p_Visa_Sales_Doc_Add_Update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }
    //# endregion

    # region Static Methods
    public static DataSet GetEnquiryDetails(OBVisaParamInfo paramInfo, ListStatus listStatus, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[11];
            paramArr[0] = new SqlParameter("@P_PASSPORT_TYPE", paramInfo.PassportType);
            paramArr[1] = new SqlParameter("@P_COUNTRY_ID", paramInfo.CountryId);
            paramArr[2] = new SqlParameter("@P_NATIONALITY_ID", paramInfo.NationalityId);
            paramArr[3] = new SqlParameter("@P_VISA_TYPE_ID", paramInfo.VisaTypeId);
            paramArr[4] = new SqlParameter("@P_VISA_CATEGORY", paramInfo.VisaCategory);
            paramArr[5] = new SqlParameter("@P_COUNTRY_RES_ID", paramInfo.CountryResId);
            paramArr[6] = new SqlParameter("@P_CITY_RES_ID", paramInfo.CityResId);
            paramArr[7] = new SqlParameter("@p_list_status", listStatus);
            paramArr[8] = new SqlParameter("@p_record_status", Utility.ToString((char)recordStatus));
            SqlParameter paramMsgType = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[9] = paramMsgType;
            SqlParameter paramMsgText = new SqlParameter("@P_MSG_TEXT", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[10] = paramMsgText;
            return DBGateway.ExecuteQuery("OBvisa_p_Get_Enquiry_Details", paramArr);
        }
        catch { throw; }
    }
    public static DataSet GetDetailList(DateTime fromDate, DateTime toDate, int countryId, int nationalityId, int residencyId, string visaCategory,int visaTypeId, long appCenterId, OBVisaDispatchStatus dispatchStatus,
        string dispatchDocType, long fvsUserId, long fvsLocationId, long userId, string memberType,long locationId, long companyId, 
         ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[18];
            if (fromDate != DateTime.MinValue) paramArr[0] = new SqlParameter("@p_from_date", fromDate);
            paramArr[1] = new SqlParameter("@p_to_date", toDate);
            if (countryId > 0) paramArr[2] = new SqlParameter("@p_fvs_country_id ", countryId);
            if (nationalityId > 0) paramArr[3] = new SqlParameter("@P_fvs_nationality_id", nationalityId);
            if (residencyId > 0) paramArr[4] = new SqlParameter("@P_fvs_residence_id", residencyId);
            if (!string.IsNullOrEmpty(visaCategory)) paramArr[5] = new SqlParameter("@P_fvs_visa_category", visaCategory);
            if (visaTypeId > 0) paramArr[6] = new SqlParameter("@P_fvs_visa_type_id", visaTypeId);
            if (appCenterId > 0) paramArr[7] = new SqlParameter("@P_fvs_app_center_id", appCenterId);
            if (dispatchStatus != OBVisaDispatchStatus.All) paramArr[8] = new SqlParameter("@P_FPAX_DISPATCH_STATUS", Utility.ToString((char)dispatchStatus));
            if (!string.IsNullOrEmpty(dispatchDocType)) paramArr[9] = new SqlParameter("@p_fvs_doc_type", dispatchDocType);
            if (fvsUserId > 0) paramArr[10] = new SqlParameter("@p_fvs_user_id", fvsUserId);
            if (fvsLocationId > 0) paramArr[11] = new SqlParameter("@p_fvs_location_id", fvsLocationId);
            //if (userId <= 0) userId = Settings.LoginInfo.UserID;
            paramArr[12] = new SqlParameter("@p_user_id", userId);
            paramArr[13] = new SqlParameter("@p_user_type", memberType);
            paramArr[14] = new SqlParameter("@p_location_id", locationId);
            paramArr[15] = new SqlParameter("@p_company_id", companyId);
            paramArr[16] = new SqlParameter("@p_list_status", recordStatus);
            if (recordStatus!=RecordStatus.All) paramArr[17] = new SqlParameter("@p_record_status", Utility.ToString((char)recordStatus));
            return DBGateway.ExecuteQuery("OBvisa_p_visa_sales_getDetailsList", paramArr);
        }
        catch { throw; }
    }
    //public static void VSTicketDelete(long ticketId)
    //{
    //    try
    //    {
    //        SqlParameter[] paramArr = new SqlParameter[3];
    //        paramArr[0] = new SqlParameter("@p_vs_ticket_id",ticketId);
            
    //        SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
    //        paramMsgType.Size = 10;
    //        paramMsgType.Direction = ParameterDirection.Output;
    //        paramArr[1] = paramMsgType;

    //        SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
    //        paramMsgText.Size = 100;
    //        paramMsgText.Direction = ParameterDirection.Output;
    //        paramArr[2] = paramMsgText;
    //        DBGateway.ExecuteNonQuery("visa_p_visa_ticket_delete", paramArr);
    //    }
    //    catch { throw; }
    //}

    ////public static DataTable GetReportDetails(long transactionId)
    ////{
    ////    try
    ////    {
    ////        SqlParameter[] paramArr = new SqlParameter[1];
    ////        SqlParameter param = new SqlParameter("@p_receipt_id", SqlDbType.Decimal);
    ////        param.Value = transactionId; param.Size = 8;
    ////        paramArr[0] = param;

    ////        DataTable dt = DBGateway.ExecuteQuery("ct_p_receipt_getdata", paramArr).Tables[0];
    ////        return dt;
    ////    }
    ////    catch { throw; }
    ////}
    //# endregion
    //# region DispatchTo BO

    public static object GetNextDispatchDocSerial(string docType, long locationId)
    {
        try
        {

            string query = string.Format("SELECT  DBO.GETOBDISPATCHDOCUMENTSERIAL('{0}',{1})", docType, locationId);
            object obj = DBGateway.ExecuteScalar(query);
            return obj;

        }
        catch { throw; }
    }
    //public static void SaveDipatchDetails(long paxId, VisaDispatchStatus dispatchStatus, string dispatchNo, string dispatchRemarks, string trackingNo,long createdBy)
    //{
    //    try
    //    {
    //        SaveDipatchDetails(paxId, dispatchStatus, dispatchNo, dispatchRemarks, trackingNo, string.Empty, DateTime.MinValue, DateTime.MinValue,
    //            string.Empty, string.Empty, DateTime.MinValue, DateTime.MinValue, createdBy);
    //    }
    //    catch { throw; }
    //}

    public static void SaveDipatchDetails(long vsId, long paxId, OBVisaDispatchStatus dispatchStatus, string dispatchNo, string dispatchRemarks, string trackingNo,
        string visaNo, DateTime VisaIssueDate, DateTime VisaExpDate, DateTime VisaCollDate,int chargesId,decimal chargesAmnt,string chargesRemarks,string chargesStatus,long createdBy)
    {
        try
        {
            //getting Disptach Document Next Serial


            SqlParameter[] paramArr = new SqlParameter[17];
            if (vsId > 0) paramArr[0] = new SqlParameter("@P_FVS_ID", vsId);
            paramArr[1] = new SqlParameter("@P_FPAX_ID", paxId);
            paramArr[2] = new SqlParameter("@P_DISPATCH_STATUS", Utility.ToString((char)dispatchStatus));
            paramArr[3] = new SqlParameter("@P_DISPATCH_NO", dispatchNo);
            paramArr[4] = new SqlParameter("@P_DISPATCH_REMARKS", dispatchRemarks);
            if (!string.IsNullOrEmpty(trackingNo)) paramArr[5] = new SqlParameter("@P_TRACKING_NO", trackingNo);
            if (!string.IsNullOrEmpty(visaNo)) paramArr[6] = new SqlParameter("@P_VISA_NO", visaNo);
            if (VisaIssueDate != DateTime.MinValue) paramArr[7] = new SqlParameter("@P_VISA_ISSUE_DATE", VisaIssueDate);
            if (VisaExpDate != DateTime.MinValue) paramArr[8] = new SqlParameter("@P_VISA_EXP_DATE", VisaExpDate);
            if (VisaCollDate != DateTime.MinValue) paramArr[9] = new SqlParameter("@P_VISA_COLL_DATE", VisaCollDate);
            if (chargesId > 0) paramArr[10] = new SqlParameter("@P_FPAX_CHARGES_ID", chargesId);
            if (chargesAmnt > 0) paramArr[11] = new SqlParameter("@P_FPAX_CHARGES", chargesAmnt);
            if (!string.IsNullOrEmpty(chargesRemarks)) paramArr[12] = new SqlParameter("@P_FPAX_CHARGES_REMARKS", chargesRemarks);
            if (!string.IsNullOrEmpty(chargesStatus)) paramArr[13] = new SqlParameter("@P_FVS_CHARGES_STATUS", chargesStatus);
            paramArr[14] = new SqlParameter("@P_DISPATCH_MODIFIED_BY", createdBy);

            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[15] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[16] = paramMsgText;

            DBGateway.ExecuteNonQuery("OBvisa_p_dispatch_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }
    public static void UpdateDocSerial(OBVisaDispatchStatus dispatchStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[4];
            paramArr[0] = new SqlParameter("@P_DOC_TYPE", Utility.ToString((char)dispatchStatus));
            paramArr[1] = new SqlParameter("@P_LOCATION_ID", Settings.LoginInfo.LocationID);
            SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[2] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[3] = paramMsgText;

            DBGateway.ExecuteNonQuery("OBvisa_p_doc_serial_update", paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));

        }
        catch { throw; }
    }
    public static DataSet GetOBVisaDispatchReportList(OBVisaDispatchStatus dispatchStatus, string dispatchRefNo, ListStatus status, RecordStatus recordStatus)
    {
        try
        {
            SqlParameter[] paramArr = new SqlParameter[4];

            paramArr[0] = new SqlParameter("@P_FPAX_DISPATCH_STATUS", Utility.ToString((char)dispatchStatus));
            paramArr[1] = new SqlParameter("@P_FPAX_DISPATCH_REF_NO", dispatchRefNo);
            paramArr[2] = new SqlParameter("@p_list_status", recordStatus);
            paramArr[3] = new SqlParameter("@p_record_status", recordStatus);
            return DBGateway.ExecuteQuery("OBvisa_p_visa_sales_getReportList", paramArr);
        }
        catch { throw; }
    }
    //public static DataSet GetDispatchReportList(VisaDispatchStatus dispatchStatus, string dispatchRefNo, ListStatus status, RecordStatus recordStatus)
    //public static DataSet GetPaxDetailsList(long visaId, ListStatus listStatus, RecordStatus recordStatus)
    //{
    //    try
    //    {
    //        SqlParameter[] paramArr = new SqlParameter[3];
    //        SqlParameter param = new SqlParameter("@P_PAX_VSH_ID", SqlDbType.BigInt);
    //        param.Value = visaId;
    //        paramArr[0] = param;

    //        param = new SqlParameter("@P_LIST_STATUS", SqlDbType.Int);
    //        param.Value =listStatus;
    //        paramArr[1] = param;

    //        param = new SqlParameter("@P_RECORD_STATUS", SqlDbType.NVarChar);
    //        param.Value = Utility.ToString((char)recordStatus); param.Size = 10;
    //        paramArr[2] = param;

    //        return DBGateway.ExecuteQuery("vis_p_pax_details_getlist", paramArr);
    //    }
    //    catch { throw; }
    //}
    //public static DataSet GetDispatchReportList(VisaDispatchStatus dispatchStatus, string dispatchRefNo, ListStatus status, RecordStatus recordStatus)
    //{
    //    try
    //    {
    //        SqlParameter[] paramArr = new SqlParameter[4];
            
    //        paramArr[0] = new SqlParameter("@p_pax_dispatch_status", Utility.ToString((char)dispatchStatus));
    //        paramArr[1] = new SqlParameter("@p_pax_dispatch_ref_no", dispatchRefNo); 
    //        paramArr[2] = new SqlParameter("@p_list_status", recordStatus);
    //        paramArr[3] = new SqlParameter("@p_record_status", recordStatus);
    //        return DBGateway.ExecuteQuery("visa_p_visa_sales_getDispatchReportList", paramArr);
    //    }
    //    catch { throw; }
    //}
    //public static DataSet GetDashBoardList(long userId, string userType,long locationId, int companyId,
    //    ListStatus status, RecordStatus recordStatus)
    //{
    //    try
    //    {
    //        SqlParameter[] paramArr = new SqlParameter[6];

    //        paramArr[0] = new SqlParameter("@P_USER_ID", userId);
    //        paramArr[1] = new SqlParameter("@P_USER_TYPE", userType);
    //        paramArr[2] = new SqlParameter("@P_LOCATION_ID", locationId);
    //        paramArr[3] = new SqlParameter("@P_COMPANY_ID", companyId);
    //        paramArr[4] = new SqlParameter("@p_list_status", recordStatus);
    //        paramArr[5] = new SqlParameter("@p_record_status", recordStatus);
    //        return DBGateway.ExecuteQuery("visa_p_dashboard_getDetails", paramArr);
    //    }
    //    catch { throw; }
    //}
    //# endregion
    //# region History
    //public static DataSet GetDetailHistoryList(DateTime fromDate, DateTime toDate, int visaTypeId, int salesTypeId, int nationalityId, long userId, string memberType,
    //    long locationId, long companyId, VisaDispatchStatus dispatchStatus, string dispatchDocType, string passport, string mobile, string vsDocNo,
    //    string DBOdocNo, string DAPdocNo, string visitorName, string trackingNo,
    //    long vsUserId, long vsLocationId, ListStatus status, RecordStatus recordStatus)
    //{
    //    try
    //    {
    //        SqlParameter[] paramArr = new SqlParameter[22];
    //        paramArr[0] = new SqlParameter("@p_from_date", fromDate);
    //        paramArr[1] = new SqlParameter("@p_to_date", toDate);
    //        if (visaTypeId > 0) paramArr[2] = new SqlParameter("@p_vs_visa_type_id", visaTypeId);
    //        if (salesTypeId > 0) paramArr[3] = new SqlParameter("@p_vs_sales_type_id", salesTypeId);
    //        if (nationalityId > 0) paramArr[4] = new SqlParameter("@p_vs_nationality_id", nationalityId);
    //        //if (userId <= 0) userId = Settings.LoginInfo.UserID;
    //        paramArr[5] = new SqlParameter("@p_user_id", userId);
    //        paramArr[6] = new SqlParameter("@p_user_type", memberType);
    //        paramArr[7] = new SqlParameter("@p_location_id", locationId);
    //        paramArr[8] = new SqlParameter("@p_company_id", companyId);
    //        if (dispatchStatus != VisaDispatchStatus.All) paramArr[9] = new SqlParameter("@p_vs_dispatch_status", Utility.ToString((char)dispatchStatus));
    //        if (!string.IsNullOrEmpty(dispatchDocType)) paramArr[10] = new SqlParameter("@p_vs_doc_type", dispatchDocType);
    //        if (!string.IsNullOrEmpty(passport)) paramArr[11] = new SqlParameter("@P_PAX_PASSPORT_NO", passport);
    //        if (!string.IsNullOrEmpty(mobile)) paramArr[12] = new SqlParameter("@P_PAX_MOBILE", mobile);
    //        if (!string.IsNullOrEmpty(vsDocNo)) paramArr[13] = new SqlParameter("@P_VS_DOC_NO", vsDocNo);
    //        if (!string.IsNullOrEmpty(DBOdocNo)) paramArr[14] = new SqlParameter("@P_PAX_DBO_DOC_NO", DBOdocNo);
    //        if (!string.IsNullOrEmpty(DAPdocNo)) paramArr[15] = new SqlParameter("@P_PAX_DAP_DOC_NO", DAPdocNo);
    //        if (!string.IsNullOrEmpty(visitorName)) paramArr[16] = new SqlParameter("@P_PAX_VISITOR_NAME", visitorName);
    //        if (!string.IsNullOrEmpty(trackingNo)) paramArr[17] = new SqlParameter("@P_PAX_DAP_TRACKING_NO", trackingNo);
    //        if (vsUserId > 0) paramArr[18] = new SqlParameter("@p_vs_user_id", vsUserId);
    //        if (vsLocationId > 0) paramArr[19] = new SqlParameter("@p_vs_location_id", vsLocationId);
    //        paramArr[20] = new SqlParameter("@p_list_status", recordStatus);
    //        paramArr[21] = new SqlParameter("@p_record_status", recordStatus);
    //        return DBGateway.ExecuteQuery("visa_p_visa_sales_hist_getDetailsList", paramArr);
    //    }
    //    catch { throw; }
    //}
    //public static DataTable GetDispatchDocNoList(VisaDispatchStatus dispStatus,ListStatus status, RecordStatus recordStatus)
    //{
    //    try
    //    {
       
    //        SqlParameter[] paramArr = new SqlParameter[3];
    //        if (dispStatus != VisaDispatchStatus.All) paramArr[0] = new SqlParameter("@P_DISP_STATUS", (char)dispStatus);
    //        paramArr[1] = new SqlParameter("@P_LIST_STATUS", status);
    //        paramArr[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
    //        return DBGateway.ExecuteQuery("visa_p_visa_sales_dispatchNo_Getlist", paramArr).Tables[0];
       
    //    }
    //    catch { throw; }
    //}
    //public static DataTable GetVSDocNoList(ListStatus status, RecordStatus recordStatus)
    //{
    //    try
    //    {

    //        SqlParameter[] paramArr = new SqlParameter[3];
    //        paramArr[0] = new SqlParameter("@P_LIST_STATUS", status);
    //        paramArr[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
    //        return DBGateway.ExecuteQuery("visa_p_visa_sales_HeaderDocNo_Getlist", paramArr).Tables[0];

    //    }
    //    catch { throw; }
    //}
    //# endregion
    //# region Edit Visa Sales Search
    //public static DataSet EditVisaGetList(long userID, string memberType, long locationId, VisaDispatchStatus dispatchStatus, int companyId, ListStatus status, RecordStatus recordStatus)
    //{
    //    try
    //    {
    //        SqlParameter[] paramArr = new SqlParameter[9];
    //        paramArr[0] = new SqlParameter("@P_VS_DISPATCH_STATUS", Utility.ToString((char)dispatchStatus));
    //        paramArr[1] = new SqlParameter("@p_user_id", userID);
    //        paramArr[2] = new SqlParameter("@p_user_type", memberType);
    //        paramArr[3] = new SqlParameter("@p_location_id", locationId);
    //        paramArr[4] = new SqlParameter("@p_company_id", companyId);
    //        paramArr[5] = new SqlParameter("@P_LIST_STATUS", status);
    //        if (recordStatus!= RecordStatus.All) paramArr[6] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
    //        SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
    //        paramMsgType.Size = 10;
    //        paramMsgType.Direction = ParameterDirection.Output;
    //        paramArr[7] = paramMsgType;

    //        SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
    //        paramMsgText.Size = 100;
    //        paramMsgText.Direction = ParameterDirection.Output;
    //        paramArr[8] = paramMsgText;
    //        return DBGateway.ExecuteQuery("visa_p_visa_sales_getList", paramArr);
    //    }
    //    catch { throw; }
    //}
    # endregion
}
}

