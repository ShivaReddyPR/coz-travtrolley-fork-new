﻿using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.TicketReceipt.BusinessLayer
{
   public class utilityproductListData
    {
        #region Fields and their Properties
        private const int NEW_RECORD = -1;

        private int _Agent_id;
        public int Agetn_ID
        {
            get { return _Agent_id; }
            set { _Agent_id = value; }
        }
        private String _Code;
        public string CODE
        {
            get { return _Code; }
            set { _Code = value; }
        }
        private String _Value;
        public string VALUE
        {
            get { return _Value; }
            set { _Value = value; }
        }
        private string _Description;
        public string DESCRIPTION
        {
            get { return _Description; }
            set { _Description = value; }
        }
        private int _Sequence;
        public int SEQUENCE
        {
            get { return _Sequence; }
            set { _Sequence = value; }
        }
        private Boolean _status;
        public Boolean STATUS
        {
            get { return _status; }
            set { _status = value; }
        }
        private int _Id;
        public int ID
        {
            get { return _Id; }
            set { _Id = value; }
        }
        private long _CreatedBy;
        public long CREATED_BY
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        #endregion
        #region Constructors
        public utilityproductListData()
        {
            _Id = NEW_RECORD;
        }
        public utilityproductListData(int id)
        {


            _Id = id;
            getDetails(id);
        }
        #endregion
        private void getDetails(int id)
        {

            DataSet ds = GetData(id);
            UpdateBusinessData(ds);
          
        }
        private DataSet GetData(int id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_UtilityProductDataList_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToLong(dr["LST_ID"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }

                    // _dtDocuments = ds.Tables[1];

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                _Id = Utility.ToInteger(dr["LST_ID"]);
                _Code = Utility.ToString(dr["LST_CODE"]);
                _Value = Utility.ToString(dr["LST_VALUE"]);
                _Description = Utility.ToString(dr["LST_DESCP"]);
                _Sequence = Utility.ToInteger(dr["LST_SEQ"]);
                _status = Utility.ToBoolean(dr["LST_STATUS"]);
                _Agent_id = Utility.ToInteger(dr["LST_AgentID"]);

            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


            public void Save(int _productid)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@P_Id", _Id));
                parameters.Add(new SqlParameter("@P_Agentid", _Agent_id));
                parameters.Add(new SqlParameter("@P_Code", _Code));
                parameters.Add(new SqlParameter("@P_Value", _Value));
                parameters.Add(new SqlParameter("@P_Description", _Description));
                parameters.Add(new SqlParameter("@P_Sequence",_Sequence));
                parameters.Add(new SqlParameter("@P_Status", _status));
                parameters.Add(new SqlParameter("@P_Productid", _productid));
                parameters.Add(new SqlParameter("@P_Createdby", _CreatedBy));
                SqlParameter sqlParameter = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                sqlParameter.Direction = ParameterDirection.Output;
                parameters.Add(sqlParameter);
                SqlParameter parameter = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 100);
                parameter.Direction = ParameterDirection.Output;
                parameters.Add(parameter);

                DBGateway.ExecuteNonQuerySP("usp_UtilityProduct_add_update", parameters.ToArray());
                string messageType = Utility.ToString(sqlParameter.Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(parameter.Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetList(int loginAgent,int pid)
        {

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                if (loginAgent > 0) parameters.Add(new SqlParameter("@P_Agentid", loginAgent));
                parameters.Add(new SqlParameter("@P_productid", pid));

                return DBGateway.ExecuteQuery("Usp_UtilityProduct_getlist", parameters.ToArray()).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public static  string GetExistingcode(string code,int agentid)
        {


            try
            {
                SqlParameter[] parameter = new SqlParameter[3];
                parameter[0] = new SqlParameter("@p_code", code);
                parameter[1] = new SqlParameter("@p_agentid", agentid);
                parameter[2] = new SqlParameter("@value", SqlDbType.VarChar,100);
                parameter[2].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuery("usp_UtilityProduct_code", parameter);
                string value = Utility.ToString(parameter[2].Value);
                return value;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }
        public static DataTable GetParentTableDetails(int agentid,string code)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                if (agentid > 0) parameters.Add(new SqlParameter("@P_Agentid", agentid));
                parameters.Add(new SqlParameter("@P_code", code));

                return DBGateway.ExecuteQuery("Usp_UtilityProductParentDetails_getlist", parameters.ToArray()).Tables[0];
            }
            catch
            {
                throw;
            }
        }

    }
}
