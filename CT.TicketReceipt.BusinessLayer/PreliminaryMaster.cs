using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{


    public class PreliminaryMaster
    {
      private const long NEW_RECORD = -1;
        
      //  private long _id;

        #region Member Variables
        private long _transactionid;       
        private string _code;
        private string _name;
        private string _passportType;
        private long _countryId;
        private long _nationalityId;
        private long _visaTypeId;
        private long _residenceId;      
        private string _visaCategory;               
        private string _status;
        private string _onArrivalStatus;
        private string _interviewStatus;
        private long _createdBy;        

        #endregion


        #region Properties

        public long TransactionId
        {
            get { return _transactionid ; }
        }

       public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string PassportType
        {
            get { return _passportType; }
            set { _passportType = value; }
        }
        public long CountryID
        {
            get { return _countryId; }
            set { _countryId = value; }
        }
        public long NationalityId
        {
            get { return _nationalityId ; }
            set { _nationalityId  = value; }
        }
        public long VisaTypeId
        {
            get { return _visaTypeId; }
            set { _visaTypeId = value; }
        }

        public long ResidenceId
        {
            get { return _residenceId; }
            set { _residenceId = value; }
        }
       
        public string VisaCategory
        {
            get { return _visaCategory; }
            set { _visaCategory  = value; }
        }
        public string OnArrivalStatus
        {
            get { return _onArrivalStatus; }
            set { _onArrivalStatus = value; }
        }
        public string InterviewStatus
        {
            get { return _interviewStatus; }
            set { _interviewStatus = value; }
        }
        
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        #endregion


        #region Constructors
        public PreliminaryMaster()
        {
            _transactionid  = NEW_RECORD;
        }
        public PreliminaryMaster(long id)
        {
            _transactionid  = id;
            getDetails(_transactionid );
        
        }


        #endregion

#region Methods

        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_PD_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("OBVISA_p_preliminary_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _transactionid = Utility.ToLong(dr["pd_id"]);
                _passportType = Utility.ToString(dr["pd_passport_type"]);
                _code = Utility.ToString(dr["pd_code"]);
                _name = Utility.ToString(dr["pd_description"]);
                _countryId = Utility.ToLong(dr["pd_country_id"]);
                _nationalityId = Utility.ToLong(dr["pd_nationality_id"]);

                _visaTypeId = Utility.ToLong(dr["pd_visa_type_id"]);
                _residenceId = Utility.ToLong(dr["pd_residence_id"]);
                _visaCategory = Utility.ToString(dr["pd_visa_category"]);
                _onArrivalStatus = Utility.ToString(dr["pd_onarrival_status"]);
                _interviewStatus = Utility.ToString(dr["pd_interview_status"]);
                _status = Utility.ToString(dr["pd_status"]);
                _createdBy = Utility.ToLong(dr["pd_created_by"]);

            }
            catch
            {
                throw;
            }
        }
       
#endregion

#region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[24];

                paramList[0] = new SqlParameter("@P_PD_ID", _transactionid);
                paramList[1] = new SqlParameter("@P_PD_CODE", _code);
                if (_passportType != "0") paramList[2] = new SqlParameter("@P_PD_PASSPORT_TYPE", _passportType);
                paramList[3] = new SqlParameter("@P_PD_COUNTRY_ID", _countryId);
                if(_nationalityId!=0) paramList[4] = new SqlParameter("@P_PD_NATIONALITY_ID", _nationalityId);
                if (_visaTypeId != 0) paramList[5] = new SqlParameter("@P_PD_VISA_TYPE_ID", _visaTypeId);
                if (_residenceId != 0) paramList[6] = new SqlParameter("@P_PD_RESIDENCE_ID", _residenceId);                
                if (_visaCategory != "0") paramList[7] = new SqlParameter("@P_PD_VISA_CATEGORY", _visaCategory);
                paramList[8] = new SqlParameter("@P_PD_ONARRIVAL_STATUS", _onArrivalStatus);
                paramList[9] = new SqlParameter("@P_PD_INTERVIEW_STATUS", _interviewStatus);
                paramList[10] = new SqlParameter("@P_PD_STATUS", _status);
                paramList[11] = new SqlParameter("@P_PD_CREATED_BY", _createdBy);
                
                
                paramList[12] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[12].Direction = ParameterDirection.Output;
                paramList[13] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[13].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("obvisa_p_preliminary_add_update", paramList);
                string messageType = Utility.ToString(paramList[12].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[13].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_PD_id", _transactionid );
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("obvisa_p_preliminary_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
#endregion

       
        #region Static Methods

        public static DataTable GetList(int countryId, ListStatus status, RecordStatus recordStatus)
        {

            try
            {
             
                SqlParameter[] paramList = new SqlParameter[3];
                if (countryId > 0) paramList[0] = new SqlParameter("@P_PD_ID", countryId);
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus!= RecordStatus.All) paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("[OBVISA_p_preliminary_getlist]", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetPriliminaryList(ListStatus status, RecordStatus recordStatus)
        {
            try
            {

                SqlParameter[] paramList = new SqlParameter[2];                
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus != RecordStatus.All) paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("[OBVISA_p_preliminary_getlist]", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        
        
        }
        # endregion
       
    }
   
}
