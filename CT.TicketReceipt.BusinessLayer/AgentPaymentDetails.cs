using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class AgentPaymentDetails
    {
        private const long NEW_RECORD = -1;

        private long _id;
        private int  _agentId;
        private string _receiptNo;
        private DateTime _receiptDate;
        private decimal _amount;
        private string _paymentMode;
        private string _chequeNo;
        private DateTime  _chequeDate;
        private string _bankName;
        private string _bankBranch;
        private string _bankAccount;
        private string _remarks;
        private string _depositSlipPath;
        private string _depositSlipType;
        private string _status;
        private long _createdBy;
        private string  _createdByName;
        private static decimal _agentBalance;
        private decimal _invBalance;
        private string _agentName;
        private string _currency;
        private decimal _exchRate;
        private string _subAgentCurrency;
        private decimal _subAgentExchRate;
        private decimal _subAgentAmount;
        private string _docType;
        private string _docBase;
        private string _tranxType;
        private string _tranxProvision;
        private int _locationId;
        private int _subAgentId;
        private int _transRefId;
        private string _transRefName;
        private string _transRole; //AGENT or SUBAGENT
        private string _approvalStatus;

        #region Properties
        public long ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public string ReceiptNo
        {
            get { return _receiptNo; }
            set { _receiptNo = value; }
        }
        public DateTime ReceiptDate
        {
            get { return _receiptDate; }
            set { _receiptDate = value; }
        }
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        public string PayMode
        {
            get { return _paymentMode ; }
            set { _paymentMode = value; }
        }
        public string  ChequeNo
        {
            get { return _chequeNo; }
            set { _chequeNo = value; }
        }
        public DateTime  ChequeDate
        {
            get { return _chequeDate; }
            set { _chequeDate = value; }
        }
        public string BankName
        {
            get { return _bankName; }
            set { _bankName = value; }
        }
        public string BankBranch
        {
            get { return _bankBranch; }
            set { _bankBranch = value; }
        }

        public string BankAccount
        {
            get { return _bankAccount; }
            set { _bankAccount = value; }
        }
        public string Remarks
        {
            get { return _remarks; }
            set { _remarks = value; }
        }
        public string DepositSliPath
        {
            get { return _depositSlipPath ; }
            set { _depositSlipPath = value; }
        }
        public string DepositSlipType
        {
            get { return _depositSlipType; }
            set { _depositSlipType = value; }
        }
      
        public string  Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public string  CreatedByName
        {
            get { return _createdByName; }
            set { _createdByName = value; }
        }

        public static decimal AgentBalane
        {
            get { return _agentBalance; }
            set { _agentBalance = value; }

        }
        public decimal InvBalance
        {
            get { return _invBalance; }
            set { _invBalance = value; }

        }

        public string  AgentName
        {
            get { return _agentName; }
            set { _agentName = value; }
        }
        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }

        public decimal ExchRate
        {
            get { return _exchRate ; }
            set { _exchRate  = value; }
        }

        public string DocType
        {
            get { return _docType; }
            set { _docType = value; }
        }

        public string DocBase
        {
            get { return _docBase; }
            set { _docBase = value; }
        }
        public string TranxType
        {
            get { return _tranxType; }
            set { _tranxType = value; }
        }
        public string TranxProvision
        {
            get { return _tranxProvision; }
            set { _tranxProvision = value; }
        }
        public int LocationId
        {
            get { return _locationId; }
            set { _locationId = value; }
        }
        public int SubAgentId
        {
            get { return _subAgentId; }
            set { _subAgentId = value; }
        }
        public int TransReftId
        {
            get { return _transRefId; }
            set { _transRefId = value; }
        }

        public string TransRefName  //agent name or subagent name
        {
            get { return _transRefName; }
            set { _transRefName = value; }
        }

        public string TransRole  //agent or subagent 
        {
            get { return _transRole; }
            set { _transRole = value; }
        }
        public string SubAgentCurrency
        {
            get { return _subAgentCurrency; }
            set { _subAgentCurrency = value; }
        }
        public decimal SubAgentExchangeRate
        {
            get { return _subAgentExchRate; }
            set { _subAgentExchRate = value; }
        }
        public decimal SubAgentAmount
        {
            get { return _subAgentAmount; }
            set { _subAgentAmount = value; }
        }

        public string ApprovalStatus
        {
            get { return _approvalStatus; }
            set { _approvalStatus = value; }
        }
        #endregion


        #region Constructors
        public AgentPaymentDetails()
        {
            _id = NEW_RECORD;
        }
        public AgentPaymentDetails(long id)
        {


            _id = id;
            getDetails(id);
        }
        #endregion


        #region Methods
        private void getDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }

        }
        private DataSet GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_AP_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_agent_payment_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                if (_id < 0)
                {
                    _id = Utility.ToLong(dr["ap_file_id"]);
                }
                else
                {
                    _id = Utility.ToLong(dr["ap_id"]);
                    _agentId = Utility.ToInteger(dr["ap_agent_id"]);
                    _receiptNo = Utility.ToString(dr["ap_reciept_no"]);
                    _receiptDate = Utility.ToDate(dr["ap_receipt_date"]);
                    _amount = Utility.ToDecimal(dr["ap_amount"]);

                    _paymentMode = Utility.ToString(dr["ap_payment_mode"]);
                    _chequeNo = Utility.ToString(dr["ap_cheque_no"]);
                    _chequeDate = Utility.ToDate(dr["ap_cheque_date"]);
                    _bankName = Utility.ToString(dr["ap_bank_name"]);
                    _bankBranch = Utility.ToString(dr["ap_bank_branch"]);
                    _bankAccount = Utility.ToString(dr["ap_bank_account"]);
                    _remarks = Utility.ToString(dr["ap_remarks"]);
                    _depositSlipPath = Utility.ToString(dr["ap_deposit_slip_path"]);
                    _depositSlipType= Utility.ToString(dr["ap_deposit_slip_type"]);
                    _status = Utility.ToString(dr["ap_status"]);
                    _createdBy = Utility.ToLong(dr["ap_created_by"]);
                    _invBalance = Utility.ToDecimal(dr["ap_bal_amount"]);
                    _agentName = Utility.ToString(dr["agent_name"]);
                    _createdByName = Utility.ToString(dr["ap_created_by_name"]);
                    _currency = Utility.ToString(dr["ap_currency"]);
                    _tranxProvision = Utility.ToString(dr["ap_tranx_provision"]);
                    _tranxType = Utility.ToString(dr["ap_tranx_type"]);
                    _docType = Utility.ToString(dr["ap_doc_type"]);
                    _transRefName = Utility.ToString(dr["ref_name"]);
                    _transRefId = Utility.ToInteger(dr["ap_ref_id"]);
                    _transRole = Utility.ToString(dr["ap_transfer_role"]);
                    //_loginName = Utility.ToString(dr["USER_LOGIN_NAME"]);
                    //_locationId = Utility.ToLong(dr["USER_LOCATION_ID"]);
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[29];

                
                paramList[0] = new SqlParameter("@P_AP_ID", _id);
                paramList[1] = new SqlParameter("@P_AP_AGENT_ID", _agentId);
                paramList[2] = new SqlParameter("@P_AP_RECIEPT_NO", _receiptNo );
                paramList[3] = new SqlParameter("@P_AP_RECEIPT_DATE", _receiptDate );
                paramList[4] = new SqlParameter("@P_AP_AMOUNT", _amount);
                paramList[5] = new SqlParameter("@P_AP_PAYMENT_MODE", _paymentMode);
                paramList[6] = new SqlParameter("@P_AP_CHEQUE_NO", _chequeNo );
                //paramList[7] = new SqlParameter("@P_AP_CHEQUE_DATE", _chequeDate ); Updated by Sainadh 04 Sep 2014z
                if (_chequeDate != DateTime.MinValue) paramList[7] = new SqlParameter("@P_AP_CHEQUE_DATE", _chequeDate);
                paramList[8] = new SqlParameter("@P_AP_BANK_NAME", _bankName);
                paramList[9] = new SqlParameter("@P_AP_BANK_BRANCH", _bankBranch );
                paramList[10] = new SqlParameter("@P_AP_BANK_ACCOUNT", _bankAccount );
                paramList[11] = new SqlParameter("@P_AP_REMARKS", _remarks );

                paramList[12] = new SqlParameter("@P_AP_DEPOSIT_SLIP_PATH", _depositSlipPath);
                paramList[13] = new SqlParameter("@P_AP_DEPOSIT_SLIP_TYPE", _depositSlipType);
                paramList[14] = new SqlParameter("@P_AP_STATUS", _status);
                paramList[15] = new SqlParameter("@P_AP_CREATED_BY", _createdBy);
                paramList[16] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[16].Direction = ParameterDirection.Output;
                paramList[17] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[17].Direction = ParameterDirection.Output;
                paramList[18] = new SqlParameter("@P_AP_RECIEPT_NO_RET", SqlDbType.VarChar, 200);
                paramList[18].Direction = ParameterDirection.Output;

                paramList[19] = new SqlParameter("@P_AP_ID_RET", SqlDbType.BigInt,8);
                paramList[19].Direction = ParameterDirection.Output;
                paramList[20] = new SqlParameter("@P_AP_CURRENCY", _currency );
                paramList[21] = new SqlParameter("@P_AP_EXCH_RATE",_exchRate );
                paramList[22] = new SqlParameter("@P_AP_DOC_TYPE", _docType);
                paramList[23] = new SqlParameter("@P_AP_DOC_BASE", _docBase);
                paramList[24] = new SqlParameter("@P_AP_TRANX_TYPE", _tranxType);
                paramList[25] = new SqlParameter("@P_AP_TRANX_PROVISION", _tranxProvision);
                paramList[26] = new SqlParameter("@P_AP_LOCATION_ID", _locationId);
                paramList[27] = new SqlParameter("@P_CURRENT_BALANCE", SqlDbType.VarChar, 200);
                paramList[27].Direction = ParameterDirection.Output;
               if(!string.IsNullOrEmpty(_approvalStatus))
                {
                    paramList[28] = new SqlParameter("@P_ap_approval_status", _approvalStatus);
                }
                DBGateway.ExecuteNonQuery("ct_p_agent_payment_add_update", paramList);
                string messageType = Utility.ToString(paramList[16].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[17].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                _receiptNo=Utility.ToString(paramList[18].Value);
                if(string.IsNullOrEmpty(_receiptNo))
                {
                    throw new Exception("Failed to generate Receipt number|002");
                }
                _id = Utility.ToLong(paramList[19].Value);
                if(_id<=0)
                {
                    throw new Exception("Failed to generate PaymentId|002");
                }
                _agentBalance =Utility.ToDecimal(paramList[27].Value);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public void SavePaymentTransfer()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[32];

                paramList[0] = new SqlParameter("@P_AP_ID", _id);
                paramList[1] = new SqlParameter("@P_AP_AGENT_ID", _agentId);
                paramList[2] = new SqlParameter("@P_AP_RECIEPT_NO", _receiptNo);
                paramList[3] = new SqlParameter("@P_AP_RECEIPT_DATE", _receiptDate);
                paramList[4] = new SqlParameter("@P_AP_AMOUNT", _amount);
                paramList[5] = new SqlParameter("@P_AP_PAYMENT_MODE", _paymentMode);
                paramList[6] = new SqlParameter("@P_AP_CHEQUE_NO", _chequeNo);
                if (_chequeDate != DateTime.MinValue)
                    paramList[7] = new SqlParameter("@P_AP_CHEQUE_DATE", _chequeDate);
                else
                    paramList[7] = new SqlParameter("@P_AP_CHEQUE_DATE", DBNull.Value);
                paramList[8] = new SqlParameter("@P_AP_BANK_NAME", _bankName);
                paramList[9] = new SqlParameter("@P_AP_BANK_BRANCH", _bankBranch);
                paramList[10] = new SqlParameter("@P_AP_BANK_ACCOUNT", _bankAccount);
                paramList[11] = new SqlParameter("@P_AP_REMARKS", _remarks);

                paramList[12] = new SqlParameter("@P_AP_DEPOSIT_SLIP_PATH", _depositSlipPath);
                paramList[13] = new SqlParameter("@P_AP_DEPOSIT_SLIP_TYPE", _depositSlipType);
                paramList[14] = new SqlParameter("@P_AP_STATUS", _status);
                paramList[15] = new SqlParameter("@P_AP_CREATED_BY", _createdBy);
                paramList[16] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[16].Direction = ParameterDirection.Output;
                paramList[17] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[17].Direction = ParameterDirection.Output;
                paramList[18] = new SqlParameter("@P_AP_RECIEPT_NO_RET", SqlDbType.VarChar, 200);
                paramList[18].Direction = ParameterDirection.Output;

                paramList[19] = new SqlParameter("@P_AP_ID_RET", SqlDbType.BigInt, 8);
                paramList[19].Direction = ParameterDirection.Output;
                paramList[20] = new SqlParameter("@P_AP_CURRENCY", _currency);
                paramList[21] = new SqlParameter("@P_AP_EXCH_RATE", _exchRate);
                paramList[22] = new SqlParameter("@P_AP_DOC_TYPE", _docType);
                paramList[23] = new SqlParameter("@P_AP_DOC_BASE", _docBase);
                paramList[24] = new SqlParameter("@P_AP_TRANX_TYPE", _tranxType);
                paramList[25] = new SqlParameter("@P_AP_TRANX_PROVISION", _tranxProvision);
                paramList[26] = new SqlParameter("@P_AP_LOCATION_ID", _locationId);
                paramList[27] = new SqlParameter("@P_AP_SUBAGENT_ID", _subAgentId);
                paramList[28] = new SqlParameter("@P_AP_TRANSFER_ROLE", _transRole);
                paramList[29] = new SqlParameter("@P_AP_SUBAGENT_CURRENCY", _subAgentCurrency);
                paramList[30] = new SqlParameter("@P_AP_SUBAGENT_EXCH_RATE", _subAgentExchRate);
                paramList[31] = new SqlParameter("@P_AP_SUBAGENT_AMOUNT", _subAgentAmount);

                DBGateway.ExecuteNonQuery("ct_p_agent_payment_transfer_update", paramList);
                string messageType = Utility.ToString(paramList[16].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[17].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                _receiptNo = Utility.ToString(paramList[18].Value);
                _id = Utility.ToLong(paramList[19].Value);
            }
            catch
            {
                throw;
            }
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_USER_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_user_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        #endregion


        #region Static Methods
        public static DataTable GetList(int agentId,string agentType, ListStatus status, RecordStatus recordStatus, string approvalStatus, string tranxType)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                if(agentId>0)paramList[0] = new SqlParameter("@P_AP_AGENT_ID", agentId );
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus != RecordStatus.All) paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                paramList[3] = new SqlParameter("@P_AP_APPROVAL_STATUS", approvalStatus);
                if (!string.IsNullOrEmpty(tranxType)) paramList[4] = new SqlParameter("@P_AP_TRANX_TYPE", tranxType);
                paramList[5] = new SqlParameter("@P_AGENT_TYPE", agentType);

                return DBGateway.ExecuteQuery("ct_p_agent_payment_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        //public static DataTable GetPaymentApprovalList(int agentId, ListStatus status, RecordStatus recordStatus)
        //{

        //    try
        //    {
        //        SqlParameter[] paramList = new SqlParameter[3];
        //        paramList[0] = new SqlParameter("@P_AP_AGENT_ID", agentId);
        //        paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
        //        if (recordStatus != RecordStatus.All) paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

        //        return DBGateway.ExecuteQuery("ct_p_agent_payment_approval_list", paramList).Tables[0];
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        public static DataTable GetPaymentApprovalList(DateTime fromDate, DateTime toDate, int agentId, string agentType,string approvalStatus, string tranxType, string transProvision, string PaymentMode, ListStatus status, RecordStatus recordStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[9];
                paramList[0] = new SqlParameter("@P_FromDate", fromDate);
                paramList[1] = new SqlParameter("@P_ToDate", toDate);
                paramList[2] = new SqlParameter("@P_AP_AGENT_ID", agentId);
                if (!string.IsNullOrEmpty(approvalStatus)) paramList[3] = new SqlParameter("@P_ApprovalStatus", approvalStatus);
                if (!string.IsNullOrEmpty(tranxType)) paramList[4] = new SqlParameter("@P_TranxType", tranxType);
                if (!string.IsNullOrEmpty(transProvision)) paramList[5] = new SqlParameter("@P_Tranx_provision", transProvision);
                if (!string.IsNullOrEmpty(PaymentMode)) paramList[6] = new SqlParameter("@P_PaymentMode", PaymentMode);
                if (recordStatus != RecordStatus.All) paramList[7] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                paramList[8] = new SqlParameter("@P_AGENT_TYPE", agentType);
                return DBGateway.FillDataTableSP("ct_p_agent_payment_approval_list", paramList);
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetAgentTranxPaymentList(int agentId, ListStatus status, RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_TP_AGENT_ID", agentId);
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("ct_p_agent_tranx_payment_list", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public static decimal UpdateAgentPayment(long paymentId, int agentId, decimal agentAmount, long modifiedBy, string Approval_status)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[8];

                paramList[0] = new SqlParameter("@P_AP_ID", paymentId);
                paramList[1] = new SqlParameter("@P_AP_AGENT_ID", agentId);
                paramList[2] = new SqlParameter("@P_AP_AMOUNT", agentAmount);
                paramList[3] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[3].Direction = ParameterDirection.Output;
                paramList[4] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[4].Direction = ParameterDirection.Output;
                SqlParameter paramMsgText1 = new SqlParameter("@P_CURRENT_BALANCE", SqlDbType.Decimal);
                paramMsgText1.Size = 20;
                paramMsgText1.Direction = ParameterDirection.Output;
                paramList[5] = paramMsgText1;
                paramList[6] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                paramList[7] = new SqlParameter("@P_approval_status", Approval_status);
                DBGateway.ExecuteNonQuery("ct_p_agent_payment_approval", paramList);
                _agentBalance = Utility.ToDecimal(paramMsgText1.Value);
               

                //Leadger Transaction
                //CT.TicketReceipt.LedgerTransaction ledgerTxn;
                //CT.BookingEngine.NarrationBuilder objNarration = new CT.BookingEngine.NarrationBuilder();
                //ledgerTxn = new CT.BookingEngine.LedgerTransaction();
                //ledgerTxn.LedgerId = Utility.ToInteger(agentId);
                //ledgerTxn.Amount = Utility.ToDecimal(agentAmount) * -1;
                //objNarration.Remarks = "Top UP";
                //ledgerTxn.Narration = objNarration;
                //ledgerTxn.IsLCC = true;
                //ledgerTxn.ReferenceId = Utility.ToInteger(paymentId);
                //ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.ManualEntry;
                //ledgerTxn.Notes = "";
                //ledgerTxn.Date = DateTime.UtcNow;
                //ledgerTxn.CreatedBy = Settings.LoginInfo.AgentId;
                //ledgerTxn.Save();
                return _agentBalance;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static decimal UpdateAgentTranxPayment(long tranxPayId, long paymentId, long agentId, decimal tranxAmnt)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[7];

                paramList[0] = new SqlParameter("@P_TP_ID", tranxPayId);
                paramList[1] = new SqlParameter("@P_TP_PAYMENT_ID", paymentId);
                paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
                paramList[3] = new SqlParameter("@P_TRANX_AMOUNT", tranxAmnt);
                paramList[4] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[4].Direction = ParameterDirection.Output;
                paramList[5] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[5].Direction = ParameterDirection.Output;
                SqlParameter paramMsgText1 = new SqlParameter("@P_AGENT_BALANCE", SqlDbType.Decimal);
                paramMsgText1.Size = 20;
                paramMsgText1.Direction = ParameterDirection.Output;
                paramList[6] = paramMsgText1;
                DBGateway.ExecuteNonQuery("ct_p_agent_tranx_payment_update", paramList);
                _agentBalance = Utility.ToDecimal(paramMsgText1.Value);
                return _agentBalance;
            }
            catch
            {
                throw;
            }
        }



        public static string SaveVisaInvoice(long invoiceId, string docBase, string docType, string docNo, int agentId, int locationId,
                    string customerCode, string customerName, DateTime invoiceDate, string invoiceBase, string invoiceCurrency, decimal exchRate,
                    decimal baseAmount, decimal localAmount, string remarks, string notes, string status, long createdBy, DataTable agentInvoiceDetails)
        {

            SqlTransaction trans = null;
            SqlCommand cmd = null;

            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;


                SqlParameter[] paramList = new SqlParameter[22];

                paramList[0] = new SqlParameter("@P_INV_ID", invoiceId);
                paramList[1] = new SqlParameter("@P_INV_DOC_BASE", docBase);
                paramList[2] = new SqlParameter("@P_INV_DOC_TYPE", docType);
                paramList[3] = new SqlParameter("@P_INV_DOC_NO", docNo);
                paramList[4] = new SqlParameter("@P_INV_AGENT_ID", agentId);
                paramList[5] = new SqlParameter("@P_INV_LOCATION_ID", locationId);
                paramList[6] = new SqlParameter("@P_INV_CUSTOMER_CODE", customerCode);
                paramList[7] = new SqlParameter("@P_INV_CUSTOMER_NAME", customerName);
                paramList[8] = new SqlParameter("@P_INV_DATE", invoiceDate);
                paramList[9] = new SqlParameter("@P_INV_CURRENCY", invoiceCurrency);
                paramList[10] = new SqlParameter("@P_INV_EXCHANGE_RATE", exchRate);
                paramList[11] = new SqlParameter("@P_INV_BASE_AMOUNT", baseAmount);
                paramList[12] = new SqlParameter("@P_INV_LOCAL_AMOUNT", localAmount);
                paramList[13] = new SqlParameter("@P_INV_REMARKS", remarks);
                paramList[14] = new SqlParameter("@P_INV_NOTES", notes);
                paramList[15] = new SqlParameter("@P_INV_STATUS", status);
                paramList[16] = new SqlParameter("@P_INV_CREATED_BY", createdBy);

                paramList[17] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[17].Direction = ParameterDirection.Output;
                paramList[18] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[18].Direction = ParameterDirection.Output;
                paramList[19] = new SqlParameter("@P_INV_DOC_NO_RET", SqlDbType.VarChar, 200);
                paramList[19].Direction = ParameterDirection.Output;

                paramList[20] = new SqlParameter("@P_INV_ID_RET", SqlDbType.BigInt, 8);
                paramList[20].Direction = ParameterDirection.Output;

                paramList[21] = new SqlParameter("@P_INV_BASE", invoiceBase);

                DBGateway.ExecuteNonQuery("visa_p_visa_invoice_header_add_update", paramList);
                string messageType = Utility.ToString(paramList[17].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[18].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                long invoiceIdRet = Utility.ToLong(paramList[20].Value);
                string invDocNo = Utility.ToString(paramList[19].Value);

                if (agentInvoiceDetails != null)
                {
                    DataTable dtInv = agentInvoiceDetails.GetChanges();

                    int recordStatus = 0;
                    if (dtInv != null && dtInv.Rows.Count > 0)
                    {

                        //try
                        //{
                        foreach (DataRow dr in dtInv.Rows)
                        {
                            switch (dr.RowState)
                            {
                                case DataRowState.Added: recordStatus = 1;
                                    break;
                                case DataRowState.Modified: recordStatus = 2;
                                    break;
                                case DataRowState.Deleted: recordStatus = -1;
                                    dr.RejectChanges();
                                    break;
                                default: break;


                            }
                            SaveVisaInvoiceDetails(cmd, recordStatus, -1, invoiceIdRet, Utility.ToLong(dr["vs_id"]), Utility.ToLong(dr["pax_id"]), Utility.ToLong(dr["pay_id"]),
                                                    Utility.ToDecimal(dr["pay_base_agent_collection"]), invDocNo, invoiceCurrency, exchRate, Utility.ToDecimal(dr["pay_base_agent_collection"]),
                                            Utility.ToDecimal(dr["pay_local_agent_collection"]), 0, Utility.ToString(dr["pax_inv_edit_status"]), Utility.ToDecimal(dr["pax_inv_currency_amnt"]) / exchRate,
                                            Utility.ToDecimal(dr["pax_inv_currency_amnt"]), Settings.ACTIVE, createdBy);
                        }

                        //}
                        //catch { throw; }
                        //finally
                        //{
                        //    DBGateway.CloseConnection(cmd);
                        //}
                    }
                }



                trans.Commit();
                return invDocNo + '~' + Utility.ToString(invoiceIdRet);
            }
            catch
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }


        public static void SaveVisaInvoiceDetails(SqlCommand cmd, int recordStatus, long invId, long invHeaderId, long visaId, long paxId, long payId,
                        decimal totalCost, string invDocNo, string invCurrency, decimal invExchRate, decimal InvoiceBaseAmount, decimal InvoiceLocalAmount,
                       decimal InvoiceTranxAmount, string invEditStatus, decimal modifiedBaseAmount, decimal modifiedLocalAmount, string status, long createdBy)
        {

            try
            {

                SqlParameter[] paramArr = new SqlParameter[20];
                paramArr[0] = new SqlParameter("@P_RECORD_STATUS", recordStatus);

                paramArr[1] = new SqlParameter("@P_invd_ID", invId);
                paramArr[2] = new SqlParameter("@P_invd_header_id", invHeaderId);
                paramArr[3] = new SqlParameter("@P_invd_visa_id", visaId);
                paramArr[4] = new SqlParameter("@P_invd_pax_id", paxId);
                paramArr[5] = new SqlParameter("@P_invd_pay_id", payId);

                paramArr[6] = new SqlParameter("@P_invd_visa_doc_no", invDocNo);
                paramArr[7] = new SqlParameter("@P_invd_total_cost", totalCost);

                paramArr[8] = new SqlParameter("@P_invd_currency", invCurrency);
                paramArr[9] = new SqlParameter("@P_invd_exchange_rate", invExchRate);

                paramArr[10] = new SqlParameter("@P_invd_base_amount", InvoiceBaseAmount);
                paramArr[11] = new SqlParameter("@P_invd_local_amount", InvoiceLocalAmount);
                paramArr[12] = new SqlParameter("@P_invd_tranx_amount", InvoiceTranxAmount);

                paramArr[13] = new SqlParameter("@P_invd_edit_status", invEditStatus);

                paramArr[14] = new SqlParameter("@P_invd_base_modified_amount", modifiedBaseAmount);
                paramArr[15] = new SqlParameter("@P_invd_local_modified_amount", modifiedLocalAmount);

                paramArr[16] = new SqlParameter("@P_invd_status", status);
                paramArr[17] = new SqlParameter("@P_invd_created_by", createdBy);

                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[18] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[19] = paramMsgText;

                //SqlParameter paramMsgText1 = new SqlParameter("@p_vs_id_ret", SqlDbType.BigInt);
                //paramMsgText1.Size = 20;
                //paramMsgText1.Direction = ParameterDirection.Output;
                //paramArr[31] = paramMsgText1;



                DBGateway.ExecuteNonQueryDetails(cmd, "visa_p_visa_invoice_details_add_update", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));

                //_docNumber = Utility.ToString(paramDocNo.Value);
                //_transactionId = Utility.ToLong(paramMsgText1.Value);

            }
            catch
            {
                throw;
            }
        }




        public static DataSet GetAgentInvoiceTranxDetails(DateTime fromDate, DateTime toDate, string invoiceBase, int agentId, long locationId, string currency, decimal exchangeRate, ListStatus status, RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[9];
                paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
                if (locationId > 0) paramList[3] = new SqlParameter("@P_VS_LOCATION_ID", locationId);
                paramList[4] = new SqlParameter("@P_CURRENCY", currency);
                paramList[5] = new SqlParameter("@P_EXCH_RATE", exchangeRate);
                paramList[6] = new SqlParameter("@P_INVOICE_BASE", invoiceBase);
                paramList[7] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus != RecordStatus.All) paramList[8] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));

                return DBGateway.ExecuteQuery("visa_p_visa_agent_tranx_getlist", paramList);
            }
            catch
            {
                throw;
            }
        }


        public static DataSet GetAgentInvoiceDetails(long invoiceId, DateTime fromDate, DateTime toDate, int agentId, ListStatus status)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                if (invoiceId > 0) paramList[0] = new SqlParameter("@P_INV_ID", invoiceId);
                if (fromDate != DateTime.MinValue) paramList[1] = new SqlParameter("@P_FROM_DATE", fromDate);
                if (toDate != DateTime.MinValue) paramList[2] = new SqlParameter("@P_TO_DATE", toDate);
                if (agentId > 0) paramList[3] = new SqlParameter("@P_INV_AGENT_ID", agentId);
                paramList[4] = new SqlParameter("@P_LIST_STATUS", status);
                return DBGateway.ExecuteQuery("visa_p_visa_invoice_getdata", paramList);
            }
            catch
            {
                throw;
            }

        }

        public static string GetReceiptNo(int paymentId)
        {
            SqlParameter[] paramList = new SqlParameter[1];
            if (paymentId > 0) paramList[0] = new SqlParameter("@P_AP_ID", paymentId);
            DataTable dt= DBGateway.ExecuteQuery("usp_GetReceiptNo", paramList).Tables[0];
            return Utility.ToString(dt.Rows[0]["ap_reciept_no"]);
             
        }

        # endregion
    }
} 
