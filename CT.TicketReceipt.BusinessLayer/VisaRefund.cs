using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{


    public class VisaRefund
    {
        private const long NEW_RECORD = -1;
        
    
        #region Member Variables
        long _transactionId;
        string _docType;
        string _docNumber;
        DateTime  _docDate;

        string _refundType;
        long  _vsId;
        
        string _vsDocNo;
        string _mapReceiptNo;
        DateTime _vsDocDate;
        long _paxId;
        string _paxPassportNo;
        string _gurantorName;
        string _gurantorMobile;
        DataTable _paxDetails;
        string _actualSettlementMode;
        string _actualSettlementModeDesc;
        string _actualCurrencyCode;
        decimal _actualExchangeRate;
        decimal _crCardRate = 0;
        decimal _crCardTotal = 0;
        long _ticketId;
        int _crCardId= 0;

        RefundElementInfo _visaFee=new RefundElementInfo();
        RefundElementInfo _securityDeposit=new RefundElementInfo();
        RefundElementInfo _extraCollection=new RefundElementInfo();
        RefundElementInfo _ccCharges=new RefundElementInfo();
        RefundElementInfo _ticketFare=new RefundElementInfo();
        RefundElementInfo _ticketHandlingFee=new RefundElementInfo();
        RefundElementInfo _grandTotal=new RefundElementInfo();
        
        PaymentModeInfo _actualCashMode = new PaymentModeInfo();
        PaymentModeInfo _actualCreditMode = new PaymentModeInfo();
        PaymentModeInfo _actualCardMode = new PaymentModeInfo();
        PaymentModeInfo _actualEmployeeMode = new PaymentModeInfo();
        PaymentModeInfo _actualOthersMode = new PaymentModeInfo();

        PaymentModeInfo _refundCashMode = new PaymentModeInfo();
        PaymentModeInfo _refundCreditMode = new PaymentModeInfo();
        PaymentModeInfo _refundCardMode = new PaymentModeInfo();
        PaymentModeInfo _refundEmployeeMode = new PaymentModeInfo();
        PaymentModeInfo _refundOthersMode = new PaymentModeInfo();
        string _settlementRemarks;
        

        #endregion

        #region Properties

        public long TransactionId
        {
            get { return _transactionId; }
        }
        public string DocType
        {
            set { _docType=value; }
            get { return _docType; }
         
        }
        public string DocNumber
        {
            set { _docNumber=value; }
            get { return _docNumber; }
        }
        public DateTime DocDate
        {
            set { _docDate=value; }
            get { return _docDate; }
        }
        public string RefundType
        {
            set { _refundType=value; }
            get { return _refundType; }
        }
        public long VsId
        {
            set { _vsId=value; }
            get { return _vsId; }
        }
        
        public string VsDocNo
        {
            set { _vsDocNo=value; }
            get { return _vsDocNo; }
        }
        public DateTime VsDocDate
        {
            set { _vsDocDate=value; }
            get { return _vsDocDate; }
        }
        public string MapReceiptNo
        {
            set { _mapReceiptNo = value; }
            get { return _mapReceiptNo; }
        }
        public long PaxId
        {
            set { _paxId = value; }
            get { return _paxId; }
        }
        public string PaxPassportNo
        {
            set { _paxPassportNo= value; }
            get { return _paxPassportNo; }
        }
        public string GurantorName
        {
            set { _gurantorName=value; }
            get { return _gurantorName; }
        }
        public string GurantorMobile
        {
            set { _gurantorMobile=value; }
            get { return _gurantorMobile; }
        }
        public DataTable PaxDetails
        {
            set { _paxDetails=value; }
            get { return _paxDetails; }
        }
        public string ActualSettlementMode
        {
            get { return _actualSettlementMode; }
            set { _actualSettlementMode = value; }
        }
        public string ActualSettlementModeDesc
        {
            get { return _actualSettlementModeDesc; }
            set { _actualSettlementModeDesc = value; }
        }
        public string ActualCurrencyCode
        {
            get { return _actualCurrencyCode; }
            set { _actualCurrencyCode = value; }
        }
        public decimal ActualExchangeRate
        {
            get { return _actualExchangeRate; }
            set { _actualExchangeRate = value; }
        }
        public decimal CrCardRate
        {
            get { return _crCardRate; }
            set { _crCardRate = value; }
        }
        public decimal CrCardTotal
        {
            get { return _crCardTotal; }
            set { _crCardTotal = value; }
        }
        public long TicketId
        {
            get { return _ticketId; }
            set { _ticketId = value; }
        }
        public int CrCardId
        {
            get { return _crCardId; }
            set { _crCardId = value; }
        }
        public RefundElementInfo VisaFee
        {
            set { _visaFee=value; }
            get { return _visaFee; }
        }
        public RefundElementInfo SecurityDeposit
        {
            set { _securityDeposit=value; }
            get { return _securityDeposit; }
        }
        public RefundElementInfo ExtraCollection
        {
            set { _extraCollection=value; }
            get { return _extraCollection; }
        }
        public RefundElementInfo CcCharges
        {
            set { _ccCharges=value; }
            get { return _ccCharges; }
        }
        public RefundElementInfo TicketFare
        {
            set { _ticketFare=value; }
            get { return _ticketFare; }
        }
        public RefundElementInfo TicketHandlingFee
        {
            set { _ticketHandlingFee=value; }
            get { return _ticketHandlingFee; }
        }
        public RefundElementInfo GrandTotal
        {
            set { _grandTotal=value; }
            get { return _grandTotal; }
        }

        public PaymentModeInfo ActualCashMode
        {
            set { _actualCashMode=value; }
            get { return _actualCashMode; }
        }

       public PaymentModeInfo ActualCreditMode
        {
            set { _actualCreditMode=value; }
            get { return _actualCreditMode; }
        }
        public PaymentModeInfo ActualCardMode
        {
            set { _actualCardMode=value; }
            get { return _actualCardMode; }
        }
        public PaymentModeInfo ActualEmployeeMode
        {
            set { _actualEmployeeMode=value; }
            get { return _actualEmployeeMode; }
        }
        public PaymentModeInfo ActualOthersMode
        {
            set { _actualOthersMode=value; }
            get { return _actualOthersMode; }
        }

        public PaymentModeInfo RefundCashMode
        {
            set { _refundCashMode=value; }
            get { return _refundCashMode; }
        }

       public PaymentModeInfo RefundCreditMode
        {
            set { _refundCreditMode=value; }
            get { return _refundCreditMode; }
        }
        public PaymentModeInfo RefundCardMode
        {
            set { _refundCardMode=value; }
            get { return _refundCardMode; }
        }
        public PaymentModeInfo RefundEmployeeMode
        {
            set { _refundEmployeeMode=value; }
            get { return _refundEmployeeMode; }
        }
        public PaymentModeInfo RefundOthersMode
        {
            set { _refundOthersMode=value; }
            get { return _refundOthersMode; }
        }
        public string SettlementRemarks
        {
            set { _settlementRemarks = value; }
            get { return _settlementRemarks; }
        }
       
        #endregion

        #region Constructors
        public VisaRefund()
        {
            _transactionId = NEW_RECORD;
        }
        //public VisaRefund(long id)
        //{
        //    _transactionId = id;
        //    //getDetails(_id);
        
        //}


        #endregion

        #region Methods
        public  void GetDetails()
        {
            DataSet ds = GetData();
            if (ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count<=0)
                throw new Exception("Search Not Found !");
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }
            _paxDetails = ds.Tables[1];

        }
        //private DataSet GetData(long id)
        private DataSet GetData()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[7];
                if (_vsId!=0) paramList[0] = new SqlParameter("@P_VS_ID", _vsId);
                //paramList[0] = new SqlParameter("@P_VS_ID", _vsId);
                if (!string.IsNullOrEmpty(_vsDocNo)) paramList[1] = new SqlParameter("@P_VS_DOC_NO", _vsDocNo);
                if (_paxId > 0) paramList[2] = new SqlParameter("@P_PAX_ID", _paxId);
                if (!string.IsNullOrEmpty(_paxPassportNo)) paramList[3] = new SqlParameter("@P_PAX_PASSPORT_NO", _paxPassportNo);
                paramList[4] = new SqlParameter("@P_VS_LOCATION_ID", Settings.LoginInfo.LocationID);
                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramList[5] = paramMsgType;
                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramList[6] = paramMsgText;


                DataSet dsResult = DBGateway.ExecuteQuery("VISA_P_VISA_REFUND_GET_DETAILS", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                if (_vsId < 0)
                {
                    _docDate = Utility.ToDate(dr["DOC_DATE"]);
                    _docType = Utility.ToString(dr["DOC_TYPE"]);
                }
                else
                {
                    _vsDocNo = Utility.ToString(dr["vs_doc_no"]);
                    _vsId = Utility.ToLong(dr["vs_id"]);
                    _mapReceiptNo = Utility.ToString(dr["vs_receipt_no"]);
                    _vsDocDate = Utility.ToDate(dr["vs_doc_date"]);
                    _gurantorName = Utility.ToString(dr["vs_grnt_name"]);
                    _gurantorMobile = Utility.ToString(dr["vs_grnt_mobile_no"]);
                    _ticketId = Utility.ToLong(dr["vs_ticket_id"]);
                    _visaFee = new RefundElementInfo(Utility.ToDecimal(dr["vs_visa_fee"]), Utility.ToDecimal(dr["RF_VISA_FEE"]), 0, 0, 0);
                    _securityDeposit = new RefundElementInfo(Utility.ToDecimal(dr["VS_SECURITY_DEPOSIT"]), Utility.ToDecimal(dr["RF_SEC_DEPOSIT"]), 0, 0, 0);
                    //_securityDeposit = new RefundElementInfo(Utility.ToDecimal(dr["CoOUNTRY_ID"]), Utility.ToDecimal(dr["CoOUNTRY_ID"]), 0, 0, 0);
                    _extraCollection = new RefundElementInfo(Utility.ToDecimal(dr["vs_cheque_amount"]), Utility.ToDecimal(dr["RF_EXT_COLLECTION"]), 0, 0, 0);
                    _ccCharges = new RefundElementInfo(Utility.ToDecimal(dr["vs_crcard_tot"]), Utility.ToDecimal(dr["RF_CC_FEE"]), 0, 0, 0);
                    _ticketFare = new RefundElementInfo(Utility.ToDecimal(dr["RECEIPT_FARE"]), Utility.ToDecimal(dr["RF_TICKET_FARE"]), 0, 0, 0);
                    _ticketHandlingFee = new RefundElementInfo(Utility.ToDecimal(dr["RECEIPT_HANDLING_FEE"]), Utility.ToDecimal(dr["RF_TICKET_HANDLING_FEE"]), 0, 0, 0);

                    _actualSettlementMode = Utility.ToString(dr["vs_settlement_mode"]);
                    _actualSettlementModeDesc = Utility.ToString(dr["vs_settlement_mode_desc"]);
                    //_grandTotal = new RefundElementInfo(Utility.ToDecimal(dr["CoOUNTRY_ID"]), Utility.ToDecimal(dr["CoOUNTRY_ID"]), 0, 0, 0);

                    _actualCashMode.Set("Cash", Utility.ToDecimal(dr["vs_cash_base_amount"]), Utility.ToDecimal(dr["vs_cash_local_amount"]));
                    _actualCreditMode.Set("Credit", Utility.ToDecimal(dr["vs_credit_base_amount"]), Utility.ToDecimal(dr["vs_credit_local_amount"]));
                    _actualCardMode.Set("Card", Utility.ToDecimal(dr["vs_card_base_amount"]), Utility.ToDecimal(dr["vs_card_local_amount"]));
                    _actualEmployeeMode.Set("Employee", Utility.ToDecimal(dr["vs_employee_base_amount"]), Utility.ToDecimal(dr["vs_employee_local_amount"]));
                    _actualOthersMode.Set("Others", Utility.ToDecimal(dr["vs_others_base_amount"]), Utility.ToDecimal(dr["vs_others_local_amount"]));


                    //PaymentModeInfo _refundCashMode = new PaymentModeInfo();
                    //PaymentModeInfo _refundCreditMode = new PaymentModeInfo();
                    //PaymentModeInfo _refundCardMode = new PaymentModeInfo();
                    //PaymentModeInfo _refundEmployeeMode = new PaymentModeInfo();
                    //PaymentModeInfo _refundOthersMode = new PaymentModeInfo();
                }

            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[40];

                paramList[0] = new SqlParameter("@p_rf_vs_id", _vsId);
                if(_paxId>0) paramList[1] = new SqlParameter("@p_rf_pax_id", _paxId);
                paramList[2] = new SqlParameter("@p_rf_visa_fee", _visaFee.RefundAmount);
                paramList[3] = new SqlParameter("@p_rf_visa_fee_charge", _visaFee.RefundCharges);
                paramList[4] = new SqlParameter("@p_rf_sec_deposit", _securityDeposit.RefundAmount);
                paramList[5] = new SqlParameter("@p_rf_sec_deposit_charge", _securityDeposit.RefundCharges);
                paramList[6] = new SqlParameter("@p_rf_ext_collection", _extraCollection.RefundAmount);
                paramList[7] = new SqlParameter("@p_rf_ext_collection_charge", _extraCollection.RefundCharges);
                paramList[8] = new SqlParameter("@p_rf_cc_fee", _ccCharges.RefundAmount);
                paramList[9] = new SqlParameter("@p_rf_cc_fee_charge", _ccCharges.RefundCharges);
                paramList[10] = new SqlParameter("@p_rf_cc_rate", _crCardRate);

                if (_ticketId > 0)
                {
                    paramList[11] = new SqlParameter("@p_rf_ticket_id", _ticketId);
                    paramList[12] = new SqlParameter("@p_rf_ticket_fare", _ticketFare.RefundAmount);
                    paramList[13] = new SqlParameter("@p_rf_ticket_fare_charge", _ticketFare.RefundCharges);
                    paramList[14] = new SqlParameter("@p_rf_ticket_handling_fee", _ticketHandlingFee.RefundAmount);
                    paramList[15] = new SqlParameter("@p_rf_ticket_handling_charge", _ticketHandlingFee.RefundCharges);
                }
                paramList[16] = new SqlParameter("@p_rf_total_fee", _grandTotal.RefundCharges);
                paramList[17] = new SqlParameter("@p_rf_payable", _grandTotal.Payable);
                paramList[18] = new SqlParameter("@p_rf_currency", _actualCurrencyCode);
                paramList[19] = new SqlParameter("@p_rf_exchange_rate", _actualExchangeRate);

                paramList[20] = new SqlParameter("@p_rf_settlement_mode", _actualSettlementMode);
                paramList[21] = new SqlParameter("@p_rf_cash_base_amount", _actualCashMode.BaseAmount);
                paramList[22] = new SqlParameter("@p_rf_cash_local_amount", _actualCashMode.LocalAmount);
                paramList[23] = new SqlParameter("@p_rf_credit_base_amount", _actualCreditMode.BaseAmount);
                paramList[24] = new SqlParameter("@p_rf_credit_local_amount", _actualCreditMode.LocalAmount);
                paramList[25] = new SqlParameter("@p_rf_card_base_amount", _actualCardMode.BaseAmount);
                paramList[26] = new SqlParameter("@p_rf_card_local_amount", _actualCardMode.LocalAmount);
                paramList[27] = new SqlParameter("@p_rf_employee_base_amount", _actualEmployeeMode.BaseAmount);
                paramList[28] = new SqlParameter("@p_rf_employee_local_amount", _actualEmployeeMode.LocalAmount);
                paramList[29] = new SqlParameter("@p_rf_others_base_amount", _actualOthersMode.BaseAmount);
                paramList[30] = new SqlParameter("@p_rf_others_local_amount", _actualOthersMode.LocalAmount);
                paramList[31] = new SqlParameter("@p_rf_settlement_remarks", _settlementRemarks);
                if (_crCardId > 0) paramList[32] = new SqlParameter("@p_rf_crCard_id", _crCardId);
                paramList[33] = new SqlParameter("@p_vs_refund_status", _refundType);
                paramList[34] = new SqlParameter("@p_rf_location_id", Settings.LoginInfo.LocationID);
                paramList[35] = new SqlParameter("@P_rf_DOC_TYPE", _docType);
                paramList[36] = new SqlParameter("@P_rf_DOC_DATE", _docDate);
                paramList[37] = new SqlParameter("@P_RF_DOC_NO",SqlDbType.NVarChar,50);
                paramList[37].Direction = ParameterDirection.Output;

                paramList[38] = new SqlParameter("@p_rf_id_ret", SqlDbType.BigInt, 10);
                paramList[38].Direction = ParameterDirection.Output;
                

                paramList[39] = new SqlParameter("@p_rf_created_by", Settings.LoginInfo.UserID);

                DBGateway.ExecuteNonQuery("visa_p_visa_refund_update", paramList);
                _docNumber = paramList[37].Value.ToString();
                _transactionId = Utility.ToLong(paramList[38].Value);
                
            }
            catch
            {
                throw;
            }
        }
       
        #endregion

        #region Static Methods
        public static DataTable GetList(ListStatus status, RecordStatus recordStatus)
        {

            try
            {
                return GetList(VisaService.All, status, recordStatus);
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetList(VisaService vService,ListStatus status,RecordStatus recordStatus)
        {

            try
            {
             
                SqlParameter[] paramList = new SqlParameter[3];

                if (vService != VisaService.All) paramList[0] = new SqlParameter("@P_VISA_SERVICE", Utility.ToString((char)vService));
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", recordStatus);

                return DBGateway.ExecuteQuery("ct_p_country_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetPaxList(string vsDocNo,string passportNo)
        {

            try
            {

                SqlParameter[] paramList = new SqlParameter[5];

                if (!string.IsNullOrEmpty(vsDocNo)) paramList[0] = new SqlParameter("@P_VS_DOC_NO", vsDocNo);
                paramList[1] = new SqlParameter("@P_PAX_PASSPORT_NO", passportNo);
                paramList[2] = new SqlParameter("@P_VS_LOCATION_ID", Settings.LoginInfo.LocationID);
                paramList[3] = new SqlParameter("@P_MSG_TYPE", "");
                paramList[3].Direction=ParameterDirection.Output;
                paramList[4] = new SqlParameter("@P_MSG_TEXT", "");
                paramList[4].Direction = ParameterDirection.Output;
                return DBGateway.ExecuteQuery("visa_p_visa_pax_getList", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }

        public static DataSet GetPrintRefundDetails(long refundId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_RF_ID", refundId);
                paramList[1] = new SqlParameter("@P_MSG_TYPE", "");
                paramList[1].Direction = ParameterDirection.Output;
                paramList[2] = new SqlParameter("@P_MSG_TEXT", "");
                paramList[2].Direction = ParameterDirection.Output;
                return DBGateway.ExecuteQuery("visa_p_visa_refund_get_data", paramList);
            }
            catch
            {
                throw;
            }
        
        
        }
        # endregion
       
    }
   
}

