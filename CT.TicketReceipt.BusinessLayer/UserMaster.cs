using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class UserMaster
    {
        private const long NEW_RECORD = -1;
        #region Member Variables
        private long _id;
        private string _firstName;
        private string _lastName;
        private string _email;
        private string _loginName;
        private string _password;
        private long _locationId;
        private long _companyId;
        private int _agentId;
        private MemberType _memberType;
        private string _address;
        private string _status;
        private long _createdBy;
        private DataTable userRole;
        private int _parentAgentId;
        private string _transType;
        private bool _primaryAgent=false;
        private string _mobileNo;
        private string _userCode;
        #endregion

        #region Properties
        public long ID
        {
            get { return _id; }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string LoginName
        {
            get { return _loginName; }
            set { _loginName = value; }
        }
        public string Password
        {            
            set { _password = value; }
        }
        public long LocationID
        {
            get { return _locationId; }
            set { _locationId = value; }
        }
        public long CompanyId
        {
            get { return _companyId; }
            set { _companyId= value; }
        }
        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public MemberType MemeberType
        {
            get { return _memberType; }
            set { _memberType = value; }
        }
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public DataTable UseRole
        {
            get { return userRole; }
            set { userRole = value; }        
        }

        public int ParentAgentId
        {
            get { return _parentAgentId; }
            set { _parentAgentId = value; }
        }
        public string TransType
        {
            get { return _transType; }
            set { _transType = value; }
        }
        public bool PrimaryAgent
        {
            get{return _primaryAgent;}
            set { _primaryAgent = value; }
        }

        public string MobileNo
        {
            get { return _mobileNo; }
            set { _mobileNo = value; }
        }
        public string UserCode
        {
            get { return _userCode; }
            set { _userCode = value; }
        }
        #endregion

        #region Constructors
        public UserMaster()
        {
            _id = NEW_RECORD;
            getDetails(_id,string.Empty);
        }

        public UserMaster(long id)
        {
            _id = id;
            getDetails(id,string.Empty);
        }
        public UserMaster(long id,string agentType)
        {
            _id = id;
            getDetails(id, agentType);
        }
        #endregion
        #region Methods
        private void getDetails(long id,string agentType)
        {

            DataSet ds = GetData(id,agentType);
            if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(ds.Tables[0].Rows[0]);
            }
            userRole = ds.Tables[1];
        }
        private DataSet GetData(long id,string agentType)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_USER_ID", id);
                paramList[1] = new SqlParameter("@P_USER_AGENT_TYPE", agentType);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_user_getdata", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataSet ds)
        {
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        //DataRow  dr = ds.Tables[0].Rows[0];
                        //if(Utility.ToLong(dr["Location_ID"])>0) 
                        UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }

                    userRole  = ds.Tables[1];

                }


            }
            catch { throw; }        
        
        }
        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                _id = Utility.ToLong(dr["USER_ID"]);
                _firstName = Utility.ToString(dr["USER_FIRST_NAME"]);
                _lastName = Utility.ToString(dr["USER_LAST_NAME"]);

                _email = Utility.ToString(dr["USER_EMAIL"]);
                _loginName = Utility.ToString(dr["USER_LOGIN_NAME"]);
                _locationId = Utility.ToLong(dr["USER_LOCATION_ID"]);
                _agentId = Utility.ToInteger(dr["USER_AGENT_ID"]);
                if (dr["agent_parent_id"] != DBNull.Value)
                {
                    _parentAgentId = Utility.ToInteger(dr["agent_parent_id"]);
                }
                else
                {
                    _parentAgentId = -1;
                }
                string type = Utility.ToString(dr["USER_MEMBER_TYPE"]);
                if (type == MemberType.ADMIN.ToString())
                {
                    _memberType = MemberType.ADMIN;
                }
                else if (type == MemberType.CASHIER.ToString())
                {
                    _memberType = MemberType.CASHIER;
                }
                else if (type == MemberType.OPERATIONS.ToString())
                {
                    _memberType = MemberType.OPERATIONS;
                }
                else if (type == MemberType.BACKOFFICE.ToString())
                {
                    _memberType = MemberType.BACKOFFICE;
                }
                else if (type == MemberType.SUPERVISOR.ToString())
                {
                    _memberType = MemberType.SUPERVISOR;
                }
                else if (type == MemberType.GVOPERATIONS.ToString())
                {
                    _memberType = MemberType.GVOPERATIONS;
                }
                else if (type == MemberType.HALASERVICEUSER.ToString())
                {
                    _memberType = MemberType.HALASERVICEUSER;
                }
                else if (type == MemberType.TRAVELCORDINATOR.ToString())
                {
                    _memberType = MemberType.TRAVELCORDINATOR;
                }
                else if (type == MemberType.SALESEXECUTIVE.ToString())
                {
                    _memberType = MemberType.SALESEXECUTIVE;
                }
                _address = Utility.ToString(dr["USER_ADDRESS"]);
                _status = Utility.ToString(dr["USER_STATUS"]);
                _createdBy = Utility.ToLong(dr["USER_CREATED_BY"]);
                _transType = Utility.ToString(dr["user_TransType"]);
                _primaryAgent = Utility.ToBoolean(dr["user_primary_agent"]);
                _mobileNo = Utility.ToString(dr["user_mobile_no"]);
                _userCode = Utility.ToString(dr["user_code"]);

            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;      

                SqlParameter[] paramList = new SqlParameter[19];

                paramList[0] = new SqlParameter("@P_USER_ID", _id);
                paramList[1] = new SqlParameter("@P_USER_FIRST_NAME", _firstName);
                paramList[2] = new SqlParameter("@P_USER_LAST_NAME", _lastName);
                paramList[3] = new SqlParameter("@P_USER_EMAIL", _email);
                paramList[4] = new SqlParameter("@P_USER_LOGIN_NAME", _loginName);
                paramList[5] = new SqlParameter("@P_USER_PASSWORD", CT.Core.GenericStatic.EncryptData(_password));
                paramList[6] = new SqlParameter("@P_USER_LOCATION_ID", _locationId);
                paramList[7] = new SqlParameter("@P_USER_AGENT_ID", _agentId);
                paramList[8] = new SqlParameter("@P_USER_MEMBER_TYPE", _memberType.ToString());
                paramList[9] = new SqlParameter("@P_USER_ADDRESS", _address);
                paramList[10] = new SqlParameter("@P_USER_STATUS", _status);
                paramList[11] = new SqlParameter("@P_USER_CREATED_BY", _createdBy);
                paramList[12] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[12].Direction = ParameterDirection.Output;
                paramList[13] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[13].Direction = ParameterDirection.Output;
                paramList[14] = new SqlParameter("@P_USER_ID_RET", SqlDbType.BigInt);
                paramList[14].Direction = ParameterDirection.Output;
                paramList[15] = new SqlParameter("@P_USER_TRANSTYPE", _transType);
                paramList[16] = new SqlParameter("@P_USER_PRIMARY_AGENT", _primaryAgent);
               if(!string.IsNullOrEmpty(_mobileNo)) paramList[17] = new SqlParameter("@P_USER_MOBILE_NO", _mobileNo);
			   if (!string.IsNullOrEmpty(_userCode)) paramList[18] = new SqlParameter("@P_USER_Code", _userCode);
		        
				DBGateway.ExecuteNonQueryDetails(cmd,"ct_p_user_add_update", paramList);
                string messageType = Utility.ToString(paramList[12].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[13].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                _id = Utility.ToLong(paramList[14].Value);

                 if (userRole!=null)
                 {
                     DataTable dt = userRole.GetChanges();
                     int recordStatus = 0;
                     if (dt != null && dt.Rows.Count > 0)
                     {
                         foreach (DataRow dr in dt.Rows)
                         {
                             switch (dr.RowState)
                             {
                                 case DataRowState.Added: recordStatus = 1;
                                     break;
                                 case DataRowState.Modified: recordStatus = 2;
                                     break;
                                 case DataRowState.Deleted: recordStatus = -1;
                                     break;
                                 default: break;

                             }
                             SaveUserRoles(cmd, recordStatus, Utility.ToLong(dr["urd_id"]), _id,
                                         Utility.ToLong(dr["Role_id"]), Utility.ToString(dr["urd_status"]), _createdBy);
                         }
                     }
                 }
                 trans.Commit();
            }
            catch
            {
                trans.Rollback();
                throw;
                
            }
            finally
            {
                DBGateway.CloseConnection(cmd);
            }
        }
        public void SaveUserRoles(SqlCommand cmd, int recordStatus, long urdId, long userId, long roleId, string status, long createdBy)
        {

            try
            {
                SqlParameter[] paramArr = new SqlParameter[8];
                paramArr[0] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                paramArr[1] = new SqlParameter("@P_URD_ID", urdId);

                paramArr[2] = new SqlParameter("@P_URD_USER_ID", userId);
                paramArr[3] = new SqlParameter("@P_URD_ROLE_ID", roleId);

                paramArr[4] = new SqlParameter("@P_URD_STATUS", status);
                paramArr[5] = new SqlParameter("@P_URD_CREATED_BY", createdBy);

                SqlParameter paramMsgType = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[6] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@P_MSG_TEXT", SqlDbType.NVarChar);
                paramMsgText.Size = 200;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[7] = paramMsgText;


                DBGateway.ExecuteNonQueryDetails(cmd, "ct_p_user_role_add_update", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));


            }
            catch { throw; }
        
        }
        public void Delete()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_USER_ID", _id);
                paramList[1] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[1].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("ct_p_user_add_update", paramList);
                string message = Utility.ToString(paramList[1].Value);
                if (message != string.Empty) throw new Exception(message);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Static Methods
       
        public static DataTable GetList(int agentId,ListStatus status,RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                paramList[0] = new SqlParameter("@P_USER_LOCATION_ID", Settings.LoginInfo.LocationID);
                if(agentId>0)paramList[1] = new SqlParameter("@P_USER_AGENT_ID", agentId );
                paramList[2] = new SqlParameter("@P_USER_TYPE", Settings.LoginInfo.MemberType.ToString());
                paramList[3] = new SqlParameter("@P_LIST_STATUS", status);
                //if(recordStatus != RecordStatus.All) paramList[4] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                if (recordStatus != RecordStatus.All) paramList[4] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                return DBGateway.ExecuteQuery("ct_p_user_getlist", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        public static bool IsAuthenticatedUser(string loginName, string password,int companyId)
        {
            //In this method we are setting login info session object

            try
            {               

                SqlParameter[] paramList = new SqlParameter[5];

                paramList[0] = new SqlParameter("@P_USER_LOGIN_NAME", loginName);
                paramList[1] = new SqlParameter("@P_USER_PASSWORD", CT.Core.GenericStatic.EncryptData(password));
                paramList[2] = new SqlParameter("@P_USER_COMPANY_ID", companyId);

                paramList[3] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[3].Direction = ParameterDirection.Output;
                paramList[4] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[4].Direction = ParameterDirection.Output;
                
                DataSet ds = DBGateway.ExecuteQuery("ct_p_user_authentication", paramList);
                string messageType = Utility.ToString(paramList[3].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[4].Value);
                    if (message != string.Empty) throw new Exception(message);
                }

                if (ds != null & ds.Tables.Count != 0)
                {
                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        LoginInfo loginInfo = new LoginInfo(ds.Tables[0].Rows[0]);
                        try
                        {
                            CT.Core.StaticData sd = new CT.Core.StaticData();
                            sd.BaseCurrency = loginInfo.Currency;
                            loginInfo.AgentExchangeRates = sd.CurrencyROE;

                            Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(loginInfo.AgentId);

                            loginInfo.AgentSourceCredentials = AgentCredentials;

                        }
                        catch { throw; }
                        
                        Settings.LoginInfo = loginInfo;
                        Settings.MenuList = MenuMaster.GetList(Settings.LoginInfo.UserID);
                    }
                    else
                    {
                        throw new Exception("User Id/password does not exist !");
                    }
                }
                else
                {
                    throw new Exception("User Id/password does not exist !");
                }
                
                return true;
            }
            catch
            {
                throw;
            }
        }
        public static bool silentLogin(string emailId)
        {
            bool status = false;
            try
            {
                List<SqlParameter> paramList = new  List<SqlParameter>();
                paramList.Add(new SqlParameter("@emailId", emailId));
                DataTable ds = DBGateway.FillDataTableSP("USP_UserSilentLogin", paramList.ToArray());
                if (ds != null & ds.Rows.Count > 0)
                {
                    status = true;
                    LoginInfo loginInfo = new LoginInfo(ds.Rows[0]);
                    try
                    {
                        CT.Core.StaticData sd = new CT.Core.StaticData();
                        sd.BaseCurrency = loginInfo.Currency;
                        loginInfo.AgentExchangeRates = sd.CurrencyROE;
                        Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(loginInfo.AgentId);
                        loginInfo.AgentSourceCredentials = AgentCredentials;
                    }
                    catch { throw; } 
                    Settings.LoginInfo = loginInfo;
                    Settings.MenuList = MenuMaster.GetList(Settings.LoginInfo.UserID); 
                }
                else
                {
                    throw new Exception("User Email does not exist !");
                }                
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return status;
        }
        public static LoginInfo GetB2CUser(int userId)
        {
            //In this method we are setting login info session object
            LoginInfo loginInfo = null;
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                
                paramList[0] = new SqlParameter("@P_USER_ID", userId);
                paramList[1] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[1].Direction = ParameterDirection.Output;
                paramList[2] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[2].Direction = ParameterDirection.Output;

                DataSet ds = DBGateway.ExecuteQuery("ct_p_b2c_user_getdata", paramList);
                string messageType = Utility.ToString(paramList[1].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[2].Value);
                    if (message != string.Empty) throw new Exception(message);
                }

                if (ds != null & ds.Tables.Count != 0)
                {
                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        loginInfo = new LoginInfo(ds.Tables[0].Rows[0]);
                        try
                        {
                            CT.Core.StaticData sd = new CT.Core.StaticData();
                            sd.BaseCurrency = loginInfo.Currency;
                            loginInfo.AgentExchangeRates = sd.CurrencyROE;

                            Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(loginInfo.AgentId);

                            loginInfo.AgentSourceCredentials = AgentCredentials;

                        }
                        catch { throw; }

                        
                    }
                    else
                    {
                        throw new Exception("User Id/password does not exist !");
                    }
                }
                else
                {
                    throw new Exception("User Id/password does not exist !");
                }

                return loginInfo;
            }
            catch
            {
                throw;
            }
        }


        public static DataSet GetMemberTypeList(string type)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_FIELD_TYPE", type);
                DataSet dsResult = DBGateway.ExecuteQuery("ct_p_field_type_getlist", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        public static void ChangePassword(string loginName, string currentPassword, string newPassword)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                paramList[0] = new SqlParameter("@P_LOGIN_NAME", loginName);
                paramList[1] = new SqlParameter("@P_PASSWORD_CURRENT", CT.Core.GenericStatic.EncryptData(currentPassword));
                paramList[2] = new SqlParameter("@P_PASSWORD_NEW", CT.Core.GenericStatic.EncryptData(newPassword));

                paramList[3] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[3].Direction = ParameterDirection.Output;

                paramList[4] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[4].Direction = ParameterDirection.Output;
                
                DBGateway.ExecuteNonQuery("ct_p_user_change_password", paramList);

                string messageType = Utility.ToString(paramList[3].Value);
                string messageText= Utility.ToString(paramList[4].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[3].Value);
                    if (message != string.Empty) throw new Exception(messageText);
                }
            }
            catch
            {
                throw;
            }
        }
        public static string RequestPasswordChange(string emailId)
        {
            try
            {
                if (emailId == null || emailId.Length == 0)
                {
                    throw new ArgumentException("Email id should not be left empty.");
                }
                string guid = Guid.NewGuid().ToString();
                SqlParameter[] paramList = new SqlParameter[]
			    {
				    new SqlParameter("@emailId", emailId),
				    new SqlParameter("@requestId", guid),
				    new SqlParameter("@retVal", 1)
			    };
                paramList[2].Direction = ParameterDirection.Output;
                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_SavePwdChangeRequest", paramList);
                int memberId = (int)paramList[2].Value;
                if (memberId <= 0)
                {
                    throw new ArgumentException("EmailId does not exist in database.");
                }
                return guid;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static int ValidatePasswordChangeRequest(string requestId)
        {
            {
                if (requestId == null || requestId.Length == 0)
                {
                    throw new ArgumentException("requestId should be non empty string");
                }
                int num = 0;
                SqlConnection connection = DBGateway.GetConnection();
                SqlDataReader sqlDataReader = DBGateway.ExecuteReaderSP("usp_IsValidPasswordChangeRequest", new SqlParameter[]
			    {
				    new SqlParameter("@requestId", requestId)
			    }, connection);
                if (sqlDataReader.Read())
                {
                    num = (int)sqlDataReader["memberId"];
                }
                sqlDataReader.Close();
                connection.Close();
                return num;
            }
        }

        public static void DeletePasswordChangeRequest(string requestId)
        {
            try
            {
                if (requestId == null || requestId.Length == 0)
                {
                    throw new ArgumentException("requestId should be non empty string");
                }
                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_DeletePasswordChangeRequest", new SqlParameter[]
			    {
				    new SqlParameter("@requestId", requestId)
			    });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static DataTable GetList(int agentId, ListStatus status, RecordStatus recordStatus, MemberType memberType)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                if (agentId > 0) paramList[0] = new SqlParameter("@P_USER_AGENT_ID", agentId);
                paramList[1] = new SqlParameter("@P_USER_TYPE", memberType.ToString());
                paramList[2] = new SqlParameter("@P_LIST_STATUS", status);
                if (recordStatus != RecordStatus.All) paramList[3] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                return DBGateway.ExecuteQuery("ct_p_user_getlistby_MemeberType", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        //public static DataTable GetUserByEmailId(string emailId)
        //{

        //    try
        //    {
        //        SqlParameter[] paramList = new SqlParameter[1];
        //        paramList[0] = new SqlParameter("@P_EMAIL_ID", emailId);
        //        DataTable dtResult = DBGateway.ExecuteQuery("ct_p_getuser_by_email", paramList).Tables[0];
        //        return dtResult;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
        public static long GetPrimaryUserId(long agentId)
        {
            try
            {
                long userId;
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_AgentID", agentId);
                paramList[1] = new SqlParameter("@P_USERID_RET", SqlDbType.Int);
                paramList[1].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuery("ct_p_get_primaryuser_Id", paramList);
                userId = Utility.ToInteger(paramList[1].Value);
                return userId;
            }
            catch
            {
                throw;
            }
        }
        public static string GetPasswordByLoginName(string loginname)
        {
            string Result = string.Empty;
            SqlConnection con = new SqlConnection();
            try
            {
                con = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_USER_LOGIN_NAME", loginname);
                SqlDataReader reader = DBGateway.ExecuteReaderSP("CT_P_GETPASSWORD_BY_LOGINNAME", paramList, con);

                while (reader.Read())
                {
                    Result = CT.Core.GenericStatic.DecryptData(reader["USER_PASSWORDHASH"].ToString());
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            return Result;
        }

        /// <summary>
        /// This method is used in corp approval process where we will send Login Username of the first approver in the trip email
        /// </summary>
        /// <param name="profileId">Corporate ProfileId of the first Approver</param>
        /// <returns></returns>
        public static string GetLoginNameForCorpProfile(int profileId)
        {
            string UserName = string.Empty;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@ProfileId", profileId));

                DataTable dtLogin = DBGateway.FillDataTableSP("usp_GetLoginUserNameForCorpProfile", parameters.ToArray());

                if (dtLogin.Rows.Count > 0)
                    UserName = dtLogin.Rows[0]["user_login_name"].ToString();
            }
            catch(Exception ex)
            {
                throw new Exception("Failed to get Login name for corp profile: " + profileId + ". Reason : " + ex.ToString(), ex);
            }

            return UserName;
        }


        public static LocationMaster GetLocationForProfileId(int profileId)
        {
            LocationMaster location = new LocationMaster();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@P_ProfileId", profileId));

                DataTable dtData = DBGateway.FillDataTableSP("usp_GetLocationIdForProfileId", parameters.ToArray());

                if (dtData.Rows.Count > 0)
                {
                    int locationId = Convert.ToInt32(dtData.Rows[0]["user_location_id"]);
                    location = new LocationMaster(locationId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to read location country code for profileid: " + profileId + ". Reason: " + ex.ToString(), ex);
            }

            return location;
        }

        public static DataTable GetSalesExecList(long agentId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];               
                if (agentId > 0) paramList[0] = new SqlParameter("@P_AGENT_ID", agentId);
                return DBGateway.ExecuteQuery("CT_P_SALES_EXECUTIVE_USERS_GETLIST", paramList).Tables[0];
            }
            catch(Exception e)
            {
                throw;
            }
        }
        public static DataTable GetBankDetails(string status)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_FIELD_STATUS", status);
                DataTable dsResult = DBGateway.ExecuteQuery("ct_t_bank_get_details", paramList).Tables[0];
                return dsResult;
            }
            catch
            {
                throw;
            }
        }

        public static long SaveLoginDetils(long loginId, long userId, string loginName, long locationId, string browser, string ipAddress, string remoteUser, string culture, string status, string remarks,string loginType)
        {
            long loginIdRet = 0;
            try
            {
                SqlParameter[] paramList = new SqlParameter[14];
                paramList[0] = new SqlParameter("@P_login_id", loginId);
                if (userId > 0) paramList[1] = new SqlParameter("@P_user_id", userId);
                if (!string.IsNullOrEmpty(loginName)) paramList[2] = new SqlParameter("@P_login_name", loginName);

                if (locationId > 0) paramList[3] = new SqlParameter("@P_location_id", locationId);
                if (!string.IsNullOrEmpty(browser)) paramList[4] = new SqlParameter("@P_browser", browser);
                if (!string.IsNullOrEmpty(ipAddress)) paramList[5] = new SqlParameter("@P_ip_address", ipAddress);
                if (!string.IsNullOrEmpty(remoteUser)) paramList[6] = new SqlParameter("@P_remote_user", remoteUser);
                if (!string.IsNullOrEmpty(culture)) paramList[7] = new SqlParameter("@P_culture", culture);
                paramList[8] = new SqlParameter("@P_status", status);
                if (!string.IsNullOrEmpty(remarks)) paramList[9] = new SqlParameter("@P_Remarks", remarks);

                paramList[10] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[10].Direction = ParameterDirection.Output;

                paramList[11] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[11].Direction = ParameterDirection.Output;

                paramList[12] = new SqlParameter("@P_login_id_Ret", SqlDbType.BigInt, 20);
                paramList[12].Direction = ParameterDirection.Output;
                paramList[13] = new SqlParameter("@P_loginType", loginType);

                DBGateway.ExecuteNonQuery("CT_P_LOGIN_DETAILS_ADD_UPDATE", paramList);

                string messageType = Utility.ToString(paramList[10].Value);
                string messageText = Utility.ToString(paramList[11].Value);
                if (messageType == "E")
                {
                    //string message = Utility.ToString(paramList[11].Value);
                    if (messageText != string.Empty) throw new Exception(messageText);
                }
                loginIdRet = Utility.ToLong(paramList[12].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return loginIdRet;
        }

        public static DataTable GetAllUsers(UserStatus status)
        {
            DataTable dtUsers = new DataTable();
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@Status", status));
                dtUsers = DBGateway.FillDataTableSP("usp_GetAllUsers", parameters.ToArray());
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return dtUsers;
        }

        public static int EncryptPasswords(int userId, string password)
        {
            int records = 0;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                if (password != null)
                {
                    string encPassword = CT.Core.GenericStatic.EncryptData(password);
                    parameters.Add(new SqlParameter("@P_PASSWORD_HASH", encPassword));
                    parameters.Add(new SqlParameter("@P_USER_ID", userId));
                    records = DBGateway.ExecuteNonQuerySP("usp_UpdateEncryptedPassword", parameters.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save encrypted passwords. Reason:" + ex);
            }
            return records;
        }


        public static bool AuthenticateTerminalUser(string userCode, string password)
        {
            bool validated = false;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@P_UserCode", userCode));
                parameters.Add(new SqlParameter("@P_Password", password));
                DataTable dt = DBGateway.FillDataTableSP("usp_AuthenticateTerminalUser", parameters.ToArray());

                if (dt != null && dt.Rows.Count > 0)
                {
                    validated = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return validated;
        }
        #endregion
    }
    public enum MemberType
    {
        SUPER = 0,
        ADMIN = 1,
        CASHIER = 2,
        OPERATIONS = 3,
        BACKOFFICE = 4,
        SUPERVISOR = 5,
        B2C=6,
        GVOPERATIONS=7,
        HALASERVICEUSER=8//Added by Ganesh on 27-Apr-2018, regarding Hala service queue
            ,TRAVELCORDINATOR = 9//Added by ziyad for Corporate
            , SALESEXECUTIVE =10
            , VMSUSER = 11
    }

    public enum UserStatus
    {
        All = 0,
        Active = 1,
        InActive = 2
    }

    public class UserSpecialAccess
    {
        long userId;
        string moduleName;
        string accessType;
        int accessValue;
        bool status;
        long createdBy;

        public long UserId
        {
            get => userId;
            set => userId = value;
        }

        public string ModuleName
        {
            get => moduleName;
            set => moduleName = value;
        }

        public string AccessType
        {
            get => accessType;
            set => accessType = value;
        }

        public int AccessValue
        {
            get => accessValue;
            set => accessValue = value;
        }
        public bool Status
        {
            get => status;
            set => status = value;
        }
        public long CreatedBy
        {
            get => createdBy;
            set => createdBy = value;
        }

        public static List<UserSpecialAccess> GetUserSpecialAccesses(long userId) //Get the User special access list 
        {
            List<UserSpecialAccess> userAccessList = new List<UserSpecialAccess>();
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_UserId", userId);
                DataTable dtAccessList = DBGateway.FillDataTableSP("usp_GetUser_Special_AccessList", paramList);
                if (dtAccessList != null)
                {
                    foreach (DataRow dr in dtAccessList.Rows)
                    {
                        UserSpecialAccess user = new UserSpecialAccess();
                        user.UserId = Convert.ToInt32(dr["user_id"]);
                        user.AccessType = dr["access_type"].ToString();
                        user.AccessValue = Convert.ToInt32(dr["access_value"]);
                        user.status = Convert.ToBoolean(dr["status"]);

                        userAccessList.Add(user);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return userAccessList;
        }

        public static int SaveUserSpecialAccess(UserSpecialAccess userSpecialAccess) //Save/Update User Special Access
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_UserId", userSpecialAccess.userId);
                if(string.IsNullOrEmpty(userSpecialAccess.moduleName))
                    paramList[1] = new SqlParameter("@P_ModuleName", DBNull.Value);
                else
                   paramList[1] = new SqlParameter("@P_ModuleName", userSpecialAccess.moduleName);
                paramList[2] = new SqlParameter("@P_AccessType", userSpecialAccess.accessType);
                paramList[3] = new SqlParameter("@P_AccessValue", userSpecialAccess.accessValue);
                paramList[4] = new SqlParameter("@P_Status", userSpecialAccess.status);
                paramList[5] = new SqlParameter("@P_CreatedBy", userSpecialAccess.createdBy);

                return DBGateway.ExecuteNonQuery("usp_Save_User_SpecialAccess", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable LoadUserAccess() //Load User special access from field type master 
        {
            SqlParameter[] param = new SqlParameter[0];
            return DBGateway.FillDataTableSP("GetDefaultUserAccess", param);
        }

    }
  
}

