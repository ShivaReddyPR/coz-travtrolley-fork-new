using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.Common;

namespace CT.TicketReceipt.BusinessLayer
{
    /// <summary>
    /// Summary description for LoginInfo
    /// </summary>
    /// \
    public enum AgentType
    {
        BaseAgent = 1,
        Agent = 2,
        B2B = 3,
        B2B2B=4
    }
    [Serializable]
    public class LoginInfo
    {
        #region Member Variables
        private const string LOGIN_SESSION = "_LoginInfo";
        private long _userID;
        private string _firstName;
        private string _fullName;
        private string _lastName;
        private string _loginName;
        private string _email;
        private MemberType _memberType;
        private long _locationID;
        private string _locationName;
        private int _companyId;
        private string _currency;
        private decimal _exchangeRate;
            
        private string _locationAddress;
        private string _locationTerms; 
        private string _contactDetails;

        private int _agentId;
        private string _agentName;
        private decimal _agentBalance;
        private string _agentPhone;
        private string _agentEmail;
        private string _agentGrntrStatus;
        private string _agentVisaSubmissionStatus;
        private string _agentLogoPath;
        private string _agentProduct;
        private int _decimalValue;
        private AgentType _agentType;
        private Dictionary<string, SourceDetails> _sourceCredentials;
        private Dictionary<string, decimal> _agentExchangeRates;
        private bool _isOnBehalf;
        private string _agentTheme;
        private string _copyRight;
        /// <summary>
        /// Original Logged in Agent.
        /// </summary>
        private int _onBehalfAgentId;
        private string _onBehalfAgentCurrency;
        private int _onBehalfAgentDecimalValue;
        private Dictionary<string, decimal> _onBehalfAgentExchangeRates;
        private Dictionary<string, SourceDetails> _onBehalfAgentSourceCredentials;
        private string _transType;
        private int _corporateProfileId=0;
        string _isCorporate = "N";
        string _corporateProfileGrade = string.Empty;
        //For Vat Purpose  14.12.2017 Added By Brahmam
        string _locationCountryCode;
        //AgentBlock Status for All Bookings before checking booking blocked or not Added By brahmam
        bool _agentBlock;

        private int _onBehalfAgentLocation;  //Added for GST purpose for SG and 6E
        /// <summary>
        /// Specifies whether routing search is enabled for the agent or not
        /// </summary>
        private bool isRoutingEnabled;
        private bool isReturnFareEnabled;
        private string _salesExecDetails;
        #endregion

        #region Properties
        public long UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }
        public string FullName
        {
            get
            {
                _fullName = _firstName + " " + _lastName;
                return _fullName; 
            }
           //set { _fullName = value; }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LocationName
        {
            get { return _locationName; }
            set { _locationName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string LoginName
        {
            get { return _loginName; }
            set { _loginName = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public MemberType MemberType
        {
            get { return _memberType; }
            set { _memberType = value; }
        }
        public long LocationID
        {
            get { return _locationID; }
            set { _locationID = value; }
        }
        public string Currency
        {
            get { return _currency ; }
            set { _currency = value; }
        }
        public decimal  ExchangeRate
        {
            get { return _exchangeRate; }
            set { _exchangeRate = value; }
        }
        public int CompanyID
        {
            get { return _companyId; }
            set { _companyId = value; }
        }

        public string LocationAddress
        {
            get { return _locationAddress; }
            set { _locationAddress = value; }
        }
        public string LocationTerms
        {
            get { return _locationTerms; }
            set { _locationTerms = value; }
        }

        public string ContactDetails
        {
            get { return _contactDetails; }
            set { _contactDetails = value; }
        }

        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public string AgentName
        {
            get { return _agentName; }
            set { _agentName = value; }
        }
        public decimal AgentBalance
        {
            get { return _agentBalance; }
            set { _agentBalance = value; }
        }
        public string AgentPhone
        {
            get { return _agentPhone; }
            set { _agentPhone = value; }
        }
        public string AgentEmail
        {
            get { return _agentEmail; }
            set { _agentEmail = value; }
        }

        public String AgentGrntrStatus
        {
            get { return _agentGrntrStatus ; }
            set { value = _agentGrntrStatus; }
        }

        public string AgentVisaSubmissionStatus
        {
            get { return _agentVisaSubmissionStatus; }
            set { value = _agentVisaSubmissionStatus; }

        }
        public string AgentLogoPath
        {
            get { return _agentLogoPath; }
            set { value = _agentLogoPath; }

        }

        public string AgentProduct
        {
            get { return _agentProduct; }
            set { value = _agentProduct; }

        }
        public int DecimalValue
        {
            get { return _decimalValue; }
            set { _decimalValue = value; }
        }
        public AgentType AgentType
        {

            get { return _agentType; }
            set { _agentType = value; }

        }
        public string TransType
        {
            get { return _transType; }
            set { _transType = value; }
        }
        

        /// <summary>
        /// Source wise Credential store.
        /// </summary>
        public Dictionary<string, SourceDetails> AgentSourceCredentials
        {
            get { return _sourceCredentials; }
            set { _sourceCredentials = value; }
        }

        /// <summary>
        /// Exchanges rates against Agent base currency.
        /// </summary>
        public Dictionary<string, decimal> AgentExchangeRates
        {
            get { return _agentExchangeRates; }
            set { _agentExchangeRates = value; }
        }

        /// <summary>
        /// Set it 'True' when you are making On Behalf booking.                      
        /// Important: Please set 'False' on Hotel Search page_load method and set 'True' only when behalf Agent selection is made.
        /// <example>IsOnBehalfOfAgent=True|False</example>
        /// </summary>          
        public bool IsOnBehalfOfAgent
        {
            get { return _isOnBehalf; }
            set { _isOnBehalf = value; }
        }
        /// <summary>
        /// Original Logged in Agent.
        /// </summary>
        public int OnBehalfAgentID
        {
            get { return _onBehalfAgentId; }
            set { _onBehalfAgentId = value; }
        }

        public string OnBehalfAgentCurrency
        {
            get { return _onBehalfAgentCurrency; }
            set { _onBehalfAgentCurrency = value; }
        }

        public int OnBehalfAgentDecimalValue
        {
            get { return _onBehalfAgentDecimalValue; }
            set { _onBehalfAgentDecimalValue = value; }
        }

        public Dictionary<string, decimal> OnBehalfAgentExchangeRates
        {
            get { return _onBehalfAgentExchangeRates; }
            set { _onBehalfAgentExchangeRates = value; }
        }

        public Dictionary<string, SourceDetails> OnBehalfAgentSourceCredentials
        {
            get { return _onBehalfAgentSourceCredentials; }
            set { _onBehalfAgentSourceCredentials = value; }
        }
        public string AgentTheme
        {
            get { return _agentTheme; }
            set { _agentTheme = value; }
        }
        public string CopyRight
        {
            get { return _copyRight; }
            set { _copyRight = value; }
        }

        public int CorporateProfileId
        {
            get { return _corporateProfileId; }
            set { _corporateProfileId = value; }
        }
        public string IsCorporate
        {
            get { return _isCorporate; }
            set { _isCorporate = value; }
        }
       
         public string CorporateProfileGrade
         {
             get { return _corporateProfileGrade; }
             set { _corporateProfileGrade = value; }
         }

        public string LocationCountryCode
        {
            get
            {
                return _locationCountryCode;
            }

            set
            {
                _locationCountryCode = value;
            }
        }

        public bool AgentBlock
        {
            get
            {
                return _agentBlock;
            }

            set
            {
                _agentBlock = value;
            }
        }

        public int OnBehalfAgentLocation
        {
            get
            {
                return _onBehalfAgentLocation;
            }

            set
            {
                _onBehalfAgentLocation = value;
            }
        }
        /// <summary>
        /// Specifies whether routing search is enabled for the agent or not
        /// </summary>
        public bool IsRoutingEnabled { get => isRoutingEnabled; set => isRoutingEnabled = value; }
        public bool IsReturnFareEnabled { get => isReturnFareEnabled; set => isReturnFareEnabled = value; }


        public string SalesExecDetails
        {
            get { return _salesExecDetails; }
            set {  _salesExecDetails= value ; }
        }
        #endregion

        #region Constructor
        public LoginInfo(DataRow dr)
        {
            try
            {
                _userID = Utility.ToLong(dr["USER_ID"]);
                _firstName = Utility.ToString(dr["USER_FIRST_NAME"]);
                _lastName = Utility.ToString(dr["USER_LAST_NAME"]);

                _loginName = Utility.ToString(dr["USER_LOGIN_NAME"]);
                _locationID = Utility.ToLong(dr["USER_LOCATION_ID"]);
                _locationName = Utility.ToString(dr["LOCATION_NAME"]);
                _locationAddress = Utility.ToString(dr["LOCATION_ADDRESS"]);
                _contactDetails = Utility.ToString(dr["location_contact_details"]);
                _locationTerms = Utility.ToString(dr["LOCATION_TERMS"]);
                _companyId = Utility.ToInteger(dr["USER_COMPANY_ID"]);
                _email = Utility.ToString(dr["USER_EMAIL"]);
                string type = Utility.ToString(dr["USER_MEMBER_TYPE"]);
                //_currency = Utility.ToString(dr["Location_currency"]);
                _currency = Utility.ToString(dr["agent_currency"]);

                //_exchangeRate = Utility.ToDecimal(dr["EX_RATE_RATE"]);
                _agentId = Utility.ToInteger(dr["USER_AGENT_ID"]);
                _agentName = Utility.ToString(dr["USER_AGENT_NAME"]);
                _agentBalance = Utility.ToDecimal(dr["AGENTBALANCE"]);
                _agentPhone = Utility.ToString(dr["AGENT_PHONE1"]);
                _agentEmail = Utility.ToString(dr["AGENT_EMAIL1"]);
                _agentGrntrStatus = Utility.ToString(dr["AGENT_GUARANTOR_STATUS"]); ;
                _agentVisaSubmissionStatus = Utility.ToString(dr["AGENT_VISA_SUBMISSION_STATUS"]);
                _agentProduct = Utility.ToString(dr["AGENT_PRODUCT"]);
                _agentLogoPath = Utility.ToString(dr["agent_img_filename"]);
                _agentType = (AgentType)Utility.ToInteger(dr["AGENT_TYPE"]);
                _decimalValue = Utility.ToInteger(dr["AGENT_DECIMAL"]);
                _agentTheme = Utility.ToString(dr["AGENT_THEME"]);
                _copyRight = Utility.ToString(dr["AGENT_COPY_RIGHT"]);


                if (type == MemberType.SUPER.ToString())
                {
                    _memberType = MemberType.SUPER;
                }
                else if (type == MemberType.ADMIN.ToString())
                {
                    _memberType = MemberType.ADMIN;
                }
                else if (type == MemberType.CASHIER.ToString())
                {
                    _memberType = MemberType.CASHIER;
                }
                else if (type == MemberType.OPERATIONS.ToString())
                {
                    _memberType = MemberType.OPERATIONS;
                }
                else if (type == MemberType.BACKOFFICE.ToString())
                {
                    _memberType = MemberType.BACKOFFICE;
                }
                else if (type == MemberType.SUPERVISOR.ToString())
                {
                    _memberType = MemberType.SUPERVISOR;
                }
                else if (type == MemberType.GVOPERATIONS.ToString())
                {
                    _memberType = MemberType.GVOPERATIONS;
                }
                else if (type == MemberType.HALASERVICEUSER.ToString())
                {
                    _memberType = MemberType.HALASERVICEUSER;
                }
                else if (type == MemberType.TRAVELCORDINATOR.ToString())
                {
                    _memberType = MemberType.TRAVELCORDINATOR;
                }
                _transType = Utility.ToString(dr["user_TransType"]);
                _corporateProfileId = Utility.ToInteger(dr["user_corp_profile_id"]); ;
                _isCorporate = Utility.ToString(dr["agent_corporate"]);
                _locationCountryCode = Utility.ToString(dr["location_country_code"]);
                string blockStatus = Utility.ToString(dr["agent_block_status"]);
                if (!string.IsNullOrEmpty(blockStatus) && blockStatus.ToUpper() == "Y") //Here Y Means Bookings Block
                {
                    _agentBlock = true;
                }
                else
                {
                    _agentBlock = false;
                }
                isRoutingEnabled = Utility.ToBoolean(dr["agent_routing"]);
                isReturnFareEnabled = Utility.ToBoolean(dr["agent_returnfare"]);
            }
            catch
            {
                throw;
            }
        }

        public LoginInfo()
        {
        }
        #endregion
        public static LoginInfo GetLoginInfo(int UserId)
        {
            //In this method we are setting login info session object

            try
            {

                SqlParameter[] paramList = new SqlParameter[4];

                paramList[0] = new SqlParameter("@P_USER_ID", UserId);
                paramList[1] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[1].Direction = ParameterDirection.Output;
                paramList[2] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[2].Direction = ParameterDirection.Output;
                //paramList[3] = new SqlParameter("@P_USER_LoginName", DBNull.Value);

                DataSet ds = DataAccessLayer.DBGateway.ExecuteQuery("ct_p_GetLoginInfo", paramList);
                string messageType = Utility.ToString(paramList[1].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[2].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                LoginInfo loginInfo = new LoginInfo(ds.Tables[0].Rows[0]);
                try
                {
                    CT.Core.StaticData sd = new CT.Core.StaticData();
                    sd.BaseCurrency = loginInfo.Currency;
                    loginInfo.AgentExchangeRates = sd.CurrencyROE;
                    int parentAgentid = loginInfo.IsCorporate == "Y" ? AgentMaster.GetParentId(loginInfo.AgentId) : 0;
                    Dictionary<string, SourceDetails> AgentCredentials = parentAgentid>0 ? AgentMaster.GetAirlineCredentials(parentAgentid) : AgentMaster.GetAirlineCredentials(loginInfo.AgentId);

                    loginInfo.AgentSourceCredentials = AgentCredentials;

                }
                catch { throw; }
                return loginInfo;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To get login info using login name
        /// </summary>
        /// <param name="LoginName"></param>
        /// <returns></returns>
        public static LoginInfo GetLoginInfo(string ApiKey, int Product)
        {
            //In this method we are setting login info session object

            try
            {

                SqlParameter[] paramList = new SqlParameter[5];

                paramList[0] = new SqlParameter("@P_USER_ID", "0");
                paramList[1] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[1].Direction = ParameterDirection.Output;
                paramList[2] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[2].Direction = ParameterDirection.Output;
                paramList[3] = new SqlParameter("@P_USER_APIKEY", ApiKey);
                paramList[4] = new SqlParameter("@P_PRODUCT", Product);

                DataSet ds = DataAccessLayer.DBGateway.ExecuteQuery("ct_p_GetLoginInfo", paramList);
                string messageType = Utility.ToString(paramList[1].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[2].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                LoginInfo loginInfo = new LoginInfo(ds.Tables[0].Rows[0]);
                try
                {
                    CT.Core.StaticData sd = new CT.Core.StaticData();
                    sd.BaseCurrency = loginInfo.Currency;
                    loginInfo.AgentExchangeRates = sd.CurrencyROE;

                    Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(loginInfo.AgentId);

                    loginInfo.AgentSourceCredentials = AgentCredentials;

                }
                catch { throw; }
                return loginInfo;
            }
            catch
            {
                throw;
            }
        }
    }
    [Serializable]
    public struct SourceDetails
    {        
        /// <summary>
        /// Source UserID
        /// </summary>
        public string UserID;
        /// <summary>
        ///  Source Password
        /// </summary>
        public string Password;
        /// <summary>
        /// Airline HAP
        /// </summary>
        public string HAP;
        /// <summary>
        /// Airline PCC
        /// </summary>
        public string PCC;
        /// <summary>
        /// AgentCode/Company Code used in Hotel Source.
        /// </summary>
        public string AgentCode;
        /// <summary>
        /// Type Added to differnatiate Sources which are having the same name in different product, to updateSource Credentilas for the selected products,
        /// </summary>
        public int Type; // Added by  Ravi

        /// <summary>
        /// Endpoint URL 
        /// </summary>
        public string EndPoint;

        public string PreferredLCC; //Added by Lokesh For TBOAIR 

        public String PreferredGDS;//Added by Lokesh For TBOAIR 

        public string PrivateFareCodes; //Added by Lokesh For UAPI AccountCodes 

        public string CreatePassieveReservation; //Added by Anji For Rerservation 

        public string CurrencyCode;//Added by suresh for Tune Insurance channel wise currency code


    }
}
