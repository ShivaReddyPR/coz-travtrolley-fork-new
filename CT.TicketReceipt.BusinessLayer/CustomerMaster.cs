using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.TicketReceipt.BusinessLayer
{
    public class CustomerMaster
    {
        private const long NEW_RECORD = -1;



        #region Static Methods
        public static DataTable GetList(long customerId,ListStatus status, RecordStatus recordStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];

                if(customerId > 0) paramList[0] = new SqlParameter("@P_CUSTOMER_ID", customerId );
                paramList[1] = new SqlParameter("@P_LIST_STATUS", status);
                paramList[2] = new SqlParameter("@P_RECORD_STATUS", recordStatus);

                return DBGateway.ExecuteQuery("CT_P_CUSTOMER_GETLIST", paramList).Tables[0];
            }
            catch { throw; }
        }
        # endregion

    }

}
