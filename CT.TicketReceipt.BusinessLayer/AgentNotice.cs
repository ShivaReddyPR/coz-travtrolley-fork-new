﻿using System;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;

namespace CT.TicketReceipt.BusinessLayer
{
    /// <summary>
    /// Summary description for AgentNotice
    /// </summary>
    public class AgentNotice
    {
        #region variables
        private const long NEW_RECORD = -1;
        private long _noticeId;
        private int _agencyId;
        private string _subject;
        private string _message;
        private string _readStatus;
        private string _status;
        private long _createdBy;
        #endregion
        #region Properties
        public long NoticeId
        {
            get { return _noticeId; }
            set { _noticeId = value; }
        }
        public int AgencyId
        {
            get { return _agencyId; }
            set { _agencyId = value; }
        }
        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
        public string ReadStatus
        {
            get { return _readStatus; }
            set { _readStatus = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public long CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        #endregion

        public AgentNotice()
        {
            _noticeId = NEW_RECORD;
        }
        public AgentNotice(long id)
        {
            _noticeId = id;
            getDetails(_noticeId);
        }
        private void getDetails(long id)
        {
            DataSet data = GetData(id);
            if (data.Tables[0] != null & data.Tables[0].Rows.Count == 1)
            {
                UpdateBusinessData(data.Tables[0].Rows[0]);
            }
        }
        private DataSet GetData(long id)
        {
            DataSet result;
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@NoticeId", id);
                DataSet dataSet = DBGateway.ExecuteQuery("BKE_AGENT_NOTICE_GETDATA", paramList);
                result = dataSet;
            }
            catch
            {
                throw;
            }
            return result;
        }
        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                _noticeId = Convert.ToInt64(dr["noticeId"]);
                _agencyId = Convert.ToInt32(dr["agencyId"]);
                _subject = Convert.ToString(dr["subject"]);
                _message = Convert.ToString(dr["message"]);
                _readStatus = Convert.ToString(dr["readStatus"]);
                _status = Convert.ToString(dr["status"]);
            }
            catch
            {
                throw;
            }
        }
        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[9];
                paramList[0] = new SqlParameter("@NoticeId", _noticeId);
                paramList[1] = new SqlParameter("@AgencyId", _agencyId);
                paramList[2] = new SqlParameter("@Subject", _subject);
                paramList[3] = new SqlParameter("@Message", _message);
                paramList[4] = new SqlParameter("@ReadStatus", _readStatus);
                paramList[5] = new SqlParameter("@Status", _status);
                paramList[6] = new SqlParameter("@CreatedBy", _createdBy);
                paramList[7] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[7].Direction = ParameterDirection.Output;
                paramList[8] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[8].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuery("BKE_AGENT_NOTICE_ADD_UPDATE", paramList);
                string msg = Convert.ToString(paramList[7].Value);
                if (msg == "E")
                {
                    string text = Convert.ToString(paramList[8].Value);
                    if (text != string.Empty)
                    {
                        throw new Exception(text);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetList(ListStatus status, RecordStatus recordStatus)
        {
            DataTable result;
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@LIST_STATUS", status);
                paramList[1] = new SqlParameter("@RECORD_STATUS", Convert.ToString((char)recordStatus));

                result = DBGateway.ExecuteQuery("[BKE_AGENT_NOTICE_GETLIST]", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
            return result;
        }
        public static DataSet GetMessages(int Agencyid)
        {
            DataSet result;
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@AgencyId", Agencyid);
                DataSet dataSet = DBGateway.ExecuteQuery("BKE_GET_AGENT_NOTICES", paramList);
                result = dataSet;
            }
            catch
            {
                throw;
            }
            return result;
        }

    }
}