﻿using System;
using System.Collections.Generic;
using System.Text;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using System.Data;
using CT.Core;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

namespace CT.BookingEngine
{

    public class PromoMaster
    {
        #region PromoVariables
        int promoId;
        string promoCode;
        string promoName;
        int productId;
        DateTime bookDateFrom;
        DateTime bookDateTo;
        DateTime travelDateFrom;
        DateTime travelDateTo;
        string propertyCode;
        decimal minTranxAmount;
        int minPaxCount;
        int minRoomCount;
        string promoCity;
        string promoSource;
        string discountType;
        decimal discountValue;
        int stockInHand;
        int stockInUse;
        string status;
        int promoMemberId;
        DateTime createdOn;
        int createdBy;
        DateTime modifiedOn;
        int modifiedBy;
        private const int NEW_RECORD = -1;
        private int minNight;
        #endregion

        #region Promo Properties
        public int PromoId
        {
            get { return promoId; }
            set { promoId = value; }
        }
        public string PromoCode
        {
            get { return promoCode; }
            set { promoCode = value; }
        }
        public string PromoName
        {
            get { return promoName; }
            set { promoName = value; }
        }
        public int ProductID
        {
            get { return productId; }
            set { productId = value; }
        }
        public DateTime BookDateFrom
        {
            get { return bookDateFrom; }
            set { bookDateFrom = value; }
        }
        public DateTime BookDateTo
        {
            get { return bookDateTo; }
            set { bookDateTo = value; }
        }
        public DateTime TravelDateFrom
        {
            get { return travelDateFrom; }
            set { travelDateFrom = value; }
        }
        public DateTime TravelDateTo
        {
            get { return travelDateTo; }
            set { travelDateTo = value; }
        }
        public string PropertyCode
        {
            get { return propertyCode; }
            set { propertyCode = value; }
        }
        public decimal MinTranxAmount
        {
            get { return minTranxAmount; }
            set { minTranxAmount = value; }
        }
        public int MinPaxCount
        {
            get { return minPaxCount; }
            set { minPaxCount = value; }
        }
        public int MinRoomCount
        {
            get { return minRoomCount; }
            set { minRoomCount = value; }
        }
        public string PromoCity
        {
            get { return promoCity; }
            set { promoCity = value; }
        }
        public string PromoSource
        {
            get { return promoSource; }
            set { promoSource = value; }
        }
        public string DoscountType
        {
            get { return discountType; }
            set { discountType = value; }
        }
        public decimal DiscountValue
        {
            get { return discountValue; }
            set { discountValue = value; }
        }
        public int StockInHand
        {
            get { return stockInHand; }
            set { stockInHand = value; }
        }
        public int StockInUse
        {
            get { return stockInUse; }
            set { stockInUse = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        public int PromoMemberId
        {
            get { return promoMemberId; }
            set { promoMemberId = value; }
        }
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public DateTime ModifiedOn
        {
            get { return modifiedOn; }
            set { modifiedOn = value; }
        }
        public int ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; }
        }
        public int MinNight
        {
            get { return minNight; }
            set { minNight = value; }
        }
        #endregion
        public PromoMaster()
        {
            promoId = NEW_RECORD;
        }
        public PromoMaster(int Id)
        {
            promoId = Id;
            getDetails(promoId);
        }
        private void getDetails(int id)
        {
            try
            {
                DataSet ds = GetData(id);
                UpdateBusinessData(ds);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Load Promo Master Details. Error: " + ex.Message, "0");
            }
           
        }
        public DataSet GetData(int id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@Promo_Id", id);
                DataSet dsResult = DBGateway.ExecuteQuery("CT_P_Promo_GetData", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToInteger(dr["Promo_Id"]) > 0) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }


                }

            }
            catch
            { throw; }

        }

        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                if (promoId < 0)
                {
                    promoId = Utility.ToInteger(dr["Promo_Id"]);
                }
                else
                {
                    promoId = Utility.ToInteger(dr["Promo_Id"]);

                    promoCode = Utility.ToString(dr["Promo_Code"]);

                    promoName = Utility.ToString(dr["Promo_Name"]);

                    ProductID = Utility.ToInteger(dr["Promo_Product_Id"]);

                    bookDateFrom = Utility.ToDate(dr["Promo_Book_Date_From"]);

                    bookDateTo = Utility.ToDate(dr["Promo_Book_Date_To"]);

                    travelDateFrom = Utility.ToDate(dr["Promo_Travel_Date_From"]);

                    travelDateTo = Utility.ToDate(dr["Promo_Travel_Date_To"]);

                    propertyCode = Utility.ToString(dr["Promo_Property_Code"]);

                    minTranxAmount = Utility.ToDecimal(dr["Promo_Min_Tranx_Amount"]);

                    minPaxCount = Utility.ToInteger(dr["Promo_Min_Pax_Count"]);

                    minRoomCount = Utility.ToInteger(dr["Promo_Min_Room_Count"]);

                    promoCity = Utility.ToString(dr["Promo_City"]);

                    promoSource = Utility.ToString(dr["Promo_Source"]);

                    discountType = Utility.ToString(dr["Promo_Discount_Type"]);

                    discountValue = Utility.ToDecimal(dr["Promo_Discount_Value"]);

                    stockInHand = Utility.ToInteger(dr["Promo_StockInHand"]);

                    stockInUse = Utility.ToInteger(dr["Promo_StockInUse"]);

                    status = Utility.ToString(dr["Promo_Status"]);

                    createdBy = Utility.ToInteger(dr["Promo_CreatedBy"]);

                    createdOn = Utility.ToDate(dr["Promo_CreatedOn"]);

                    modifiedOn = Utility.ToDate(dr["Promo_ModifiedOn"]);

                    PromoMemberId = Utility.ToInteger(dr["Promo_Member_Id"]);

                    MinNight = Utility.ToInteger(dr["promo_Min_Nights"]);
                }


            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Load Promo Master Details. Error: "+ex.Message, "0");
                throw ex;
            }

        }
        #region PublicMethod
        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[23];
                paramList[0] = new SqlParameter("@Promo_Id", promoId);
                paramList[1] = new SqlParameter("@promo_ProductId", productId);
                paramList[2] = new SqlParameter("@promo_Code", promoCode);
                paramList[3] = new SqlParameter("@promo_Name", promoName);
                paramList[4] = new SqlParameter("@minTranxAmount", minTranxAmount);
                paramList[5] = new SqlParameter("@promo_DiscountType", discountType);
                paramList[6] = new SqlParameter("@promo_DiscountValue", discountValue);
                paramList[7] = new SqlParameter("@promo_StockInHand", stockInHand);
                paramList[8] = new SqlParameter("@promo_Status", status);
                if (bookDateFrom != DateTime.MinValue) paramList[9] = new SqlParameter("@bookingDate_From", bookDateFrom);
                if (bookDateTo != DateTime.MinValue) paramList[10] = new SqlParameter("@bookingDate_To", bookDateTo);
                if (travelDateFrom != DateTime.MinValue) paramList[11] = new SqlParameter("@travelDate_From", travelDateFrom);
                if (travelDateTo != DateTime.MinValue) paramList[12] = new SqlParameter("@travelDate_To", travelDateTo);
                if(minPaxCount > 0) paramList[13] = new SqlParameter("@minPaxCount", minPaxCount);
                paramList[14] = new SqlParameter("@createdBy", createdBy);
                paramList[15] = new SqlParameter("@promo_MemberId", promoMemberId);
                if (!string.IsNullOrEmpty(promoSource)) paramList[16] = new SqlParameter("@promo_Source", promoSource);
                if (!string.IsNullOrEmpty(promoCity)) paramList[17] = new SqlParameter("@promo_City", promoCity);
                paramList[18] = new SqlParameter("@MinRoomCount", minRoomCount);
                if (!string.IsNullOrEmpty(propertyCode)) paramList[19] = new SqlParameter("@promo_Property_Code", propertyCode);
                paramList[20] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar,10);
                paramList[20].Direction = ParameterDirection.Output;
                paramList[21] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar,100);
                paramList[21].Direction = ParameterDirection.Output;
                paramList[22] = new SqlParameter("@minNight", minNight);
                int rowAffected = DBGateway.ExecuteNonQuery("CT_P_Promo_AddUpdate", paramList);

                string messageType = Utility.ToString(paramList[20].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[21].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
                throw ex;
            }
        }
        #endregion

        #region Static Method

        /* Retrieving Promo details*/
        public static DataTable GetProductPromotions(int productId, DateTime bookDateFrom, DateTime travelDateFrom, string propertyCode, decimal minTranxAmount, int minPaxCount, int minRoomCount, string promoCity, string promoSource, int promoMemberId, int minNights)
        {
            DataTable dtPromotions = null;
            try
            {
                SqlParameter[] paramList = new SqlParameter[11];
                paramList[0] = new SqlParameter("@P_ProductId", productId);
                paramList[1] = new SqlParameter("@P_BookingDate", bookDateFrom);
                paramList[2] = new SqlParameter("@P_TravelDate", travelDateFrom);
                paramList[3] = new SqlParameter("@P_PropertyCode", propertyCode);
                paramList[4] = new SqlParameter("@P_MinTranxAmount", minTranxAmount);
                paramList[5] = new SqlParameter("@P_MinPaxCount", minPaxCount);
                paramList[6] = new SqlParameter("@P_MinRoomCount", minRoomCount);
                paramList[7] = new SqlParameter("@P_City", promoCity);
                paramList[8] = new SqlParameter("@P_Source", promoSource);
                paramList[9] = new SqlParameter("@P_MemberId", promoMemberId);
                paramList[10] = new SqlParameter("@P_MinNights", minNights);
                dtPromotions = DBGateway.ExecuteQuery("usp_GetProductPromotions", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
                throw ex;
            }
            return dtPromotions;
        }

        
        public static DataTable GetList(RecordStatus recordStatus, ListStatus listStatus)
        {
            
          
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                paramList[1] = new SqlParameter("@P_LIST_STATUS", Utility.ToInteger(listStatus));
                return DBGateway.ExecuteQuery("CT_P_Promo_GetList", paramList).Tables[0];
               
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "Failed To Get Promo List Data. ERROR: "+ex.Message, "0");
                throw ex;
            }
           
        }


        #endregion



    }

    public class PromoDetails
    {
        #region PromoDetailsVariables

        int promoTranxId;
        int promoId;
        string promoCode;
        int productId;
        string discountType;
        decimal discountAmount;
        decimal discountValue;
        int referenceId;
        int createdBy;
        #endregion

        #region PromoDetailsProperties
        public int PromoTranxId
        {
            get { return promoTranxId; }
            set { promoTranxId = value; }
        }
        public int PromoId
        {
            get { return promoId; }
            set { promoId = value; }
        }

        public string PromoCode
        {
            get { return promoCode; }
            set { promoCode = value; }
        }

        public int ProductId
        {
            get { return productId; }
            set { productId = value; }
        }
        public string DiscountType
        {
            get { return discountType; }
            set { discountType = value; }
        }
        public decimal DiscountAmount
        {
            get { return discountAmount; }
            set { discountAmount = value; }
        }
        public decimal DiscountValue
        {
            get { return discountValue; }
            set { discountValue = value; }
        }
        public int ReferenceId
        {
            get { return referenceId; }
            set { referenceId = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        #endregion

        public void Save()
        {
            try
            {
                SqlParameter[] param = new SqlParameter[8];

                param[0] = new SqlParameter("@promoId", promoId);
                param[1] = new SqlParameter("@promoCode", promoCode);
                param[2] = new SqlParameter("@promoProductId", productId);
                param[3] = new SqlParameter("@promoDiscountType", discountType);
                param[4] = new SqlParameter("@PromoDiscountAmount", discountAmount);
                param[5] = new SqlParameter("@PromoDiscountValue", discountValue);
                param[6] = new SqlParameter("@PromoReferenceId", referenceId);
                param[7] = new SqlParameter("@PromoCreatedBy", createdBy);
                DBGateway.ExecuteNonQuerySP("usp_AddPromoDetails", param);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
            }
        }

        public static DataTable GetPromoDetails(int productReferenceId)
        {
            DataTable dtPromoDetails = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ItineraryId", productReferenceId);

                dtPromoDetails = DBGateway.FillDataTableSP("usp_GetPromoTranxDetails", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(PromoDetails)Failed to load Promo Details for ItineraryId : " + productReferenceId + ", Error:" + ex.ToString(), "");
            }

            return dtPromoDetails;
        }

     
    }
}
